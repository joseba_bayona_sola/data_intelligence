
MASTER
	Stores (1 - 2 - 3)

	Manufacturer (1 - 2 - 3)

	Supplier (1)

	Categories (1 - 2 - 3)

	Products (1 - 2 - 3)

	Countries (1 - 2 - 3)

	Business Channels (1 - 2 - 3)

	Coupon Codes (1 - 2 - 3)


	-- 

	Payment Methods (3)

	Card Type (3)

	Telesales (2 - 3)

	Customer Order Seq (2 - 3)

	Days to Despatch (2 - 3)



CUSTOMERS
	Customers (1 - 2 - 3)

		Customer Channel

		Country Type

	Customers Address (1)


ORDERS
	Order Type (2 - 3)

	Order Lifecycle (2 - 3)


	Order Parameters (2 - 3)

	Order Qty Price (2 - 3)


	Order Header (1 - 2 - 3)

	Order Line (1 - 2 - 3)


INVOICES

	Invoice Parameters (2 - 3)

	Invoice Qty Price (2 - 3)


	Invoice Header (1 - 2 - 3)

	Invoice Line (1 - 2 - 3)

SHIPMENTS
	Shipment Carrier (2 - 3)

	Shipment Methods (2 - 3)


	Shipment Parameters (2 - 3)

	Shipment Qty Price (2 - 3)


	Shipment Header (1 - 2 - 3)

	Shipment Line (1 - 2 - 3)


