
select customer_id, 
	customer_firstname, customer_lastname, 
	customer_email, 
	billing_prefix, 
	store_name, 
	customer_first_order_date
from 
	(select customer_id, 
		customer_firstname, customer_lastname, customer_email, 
		billing_prefix, store_name, customer_first_order_date, 
		row_number() over(partition by customer_id order by document_date desc) as rno 
	from DW_Proforma.dbo.order_headers
	where customer_id not in 
		(select customer_id from DW_GetLenses.dbo.customers) and 
		left(order_no,5)='VISIO') rs 
where rno=1

-- Flow