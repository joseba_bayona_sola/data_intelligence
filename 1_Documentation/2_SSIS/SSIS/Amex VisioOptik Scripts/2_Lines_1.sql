SELECT 
	mo.*,
	CAST(CAST(mold.power AS DECIMAL(6,2)) AS CHAR(20))AS POWER,
	CASE mold.base_curve WHEN 'M' THEN '8.6' WHEN 'FM' THEN '8.4' WHEN 'SM' THEN '7.5' ELSE mold.base_curve END base_curve,
	CAST(CAST(mold.diameter AS DECIMAL(6,1)) AS CHAR(20)) AS diameter,
	CASE WHEN CAST(mold.`add` AS DECIMAL(6,2))=0.00 THEN LEFT(mold.`add`,1) ELSE CAST(mold.`add` AS DECIMAL(6,2)) END AS `add`,
	CAST(CAST(mold.axis AS UNSIGNED)  AS CHAR(20)) AS axis,
	CASE WHEN mold.cylinder IN ('D','N') THEN mold.cylinder ELSE '' END AS dominant,
	CASE WHEN mold.cylinder NOT IN ('D','N') THEN mold.cylinder ELSE '' END AS cylinder,
	mold.old_item_id, 
	mold.ProductID,
	mold.Quantity,
	CAST(base_sub_total_inc_vat * (mold.row_total/ol_total.ol_total) AS DECIMAL(16,12)) AS row_total,
	mold.eye,
	mold.color

FROM 
	migrate_orderlinedata mold
INNER JOIN 
	(SELECT old_order_id, 
		base_discount_amount, base_discount_amount_exc_vat, base_grand_total, base_shipping_amount,
		created_at, invoice_date, shipment_date,
		CurrencyID, DomainName, ExchangeRate, 
		tax_rate, tax_free_amount, 
		orderstatusname,
		base_grand_total+base_discount_amount_exc_vat* (1+tax_rate/100)- base_shipping_amount_exc_vat * (1+tax_rate/100)
			-tax_free_amount-(base_discount_amount_exc_vat * (1+ tax_rate/100) - base_discount_amount) AS base_sub_total_inc_vat

	FROM 
			migrate_orderdata mo
		LEFT OUTER JOIN 
			(SELECT STR_TO_DATE(paymentdate,'%Y%m%d') AS invoice_date,user_visible_order_id 
			FROM 
					jss_orders_headers joh
				INNER JOIN 
					migrate_order_id_mapping mom ON mom.internal_order_id=joh.orderID
			WHERE IFNULL(joh.paymentDate,'')<>'') joh ON joh.user_visible_order_id=mo.old_order_id
		LEFT OUTER JOIN 
			(SELECT MIN(dispatchdate) AS shipment_date, user_visible_order_id 
			FROM 
					(SELECT orderid, STR_TO_DATE(dispatchDate,'%Y%m%d') AS dispatchdate 
					FROM jss_dispatches
					UNION
					SELECT orderid, STR_TO_DATE(DATE,'%Y%m%d') AS dispatchdate 
					FROM amex_shipments) jd
				INNER JOIN 
					migrate_order_id_mapping mom ON mom.internal_order_id=jd.orderid
			GROUP BY mom.user_visible_order_id) jd ON jd.user_visible_order_id=mo.old_order_id) mo ON mo.old_order_id=mold.old_order_id
		INNER JOIN
			(SELECT old_order_id, SUM(row_total) AS ol_total		
			FROM migrate_orderlinedata
			GROUP BY old_order_id) ol_total ON ol_total.old_order_id=mold.old_order_id
