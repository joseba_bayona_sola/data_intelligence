USE [DW_Proforma]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateOrderSeqLifeCycle]    Script Date: 24/10/2016 15:42:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[spUpdateOrderSeqLifeCycle] as
begin


------------------------------ CUSTOMER ORDER SEQ ---------------------------------------

	-- ORDER HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id, document_type = 'ORDER'
	update oh
	set oh.customer_order_seq_no=os.ORDER_SEQ
	from 
			order_headers oh
		inner join 
			order_seq os on os.order_ID = oh.order_id and os.CUSTOMER_ID = oh.customer_id AND os.DOCUMENT_TYPE = 'ORDER'
	update oh
	set oh.customer_order_seq_no=os.ORDER_SEQ
	from 
			dw_getlenses.dbo.order_headers oh
		inner join 
			order_seq os on os.order_ID = oh.order_id and os.CUSTOMER_ID = oh.customer_id AND os.DOCUMENT_TYPE = 'ORDER'

	-- INVOICE HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id, document_type = 'ORDER'
	update ih
	set ih.customer_order_seq_no=os.ORDER_SEQ
	from 
			invoice_headers ih
		inner join 
			order_seq os on os.order_ID = ih.order_id and os.CUSTOMER_ID = ih.customer_id AND os.DOCUMENT_TYPE = 'ORDER'
	update ih
	set ih.customer_order_seq_no=os.ORDER_SEQ
	from 
			dw_getlenses.dbo.invoice_headers ih
		inner join 
			order_seq os on os.order_ID = ih.order_id and os.CUSTOMER_ID = ih.customer_id AND os.DOCUMENT_TYPE = 'ORDER'

	-- SHIPMENT HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id, document_type = 'ORDER'
	update sh
	set sh.customer_order_seq_no=os.ORDER_SEQ
	from 
			shipment_headers sh
		inner join 
			order_seq os on os.order_ID = sh.order_id and os.CUSTOMER_ID = sh.customer_id AND os.DOCUMENT_TYPE = 'ORDER'
	update sh
	set sh.customer_order_seq_no=os.ORDER_SEQ
	from 
			dw_getlenses.dbo.shipment_headers sh
		inner join 
			order_seq os on os.order_ID = sh.order_id and os.CUSTOMER_ID = sh.customer_id AND os.DOCUMENT_TYPE = 'ORDER'


------------------------------ CUSTOMER ORDER SEQ ---------------------------------------

	-- ORDER HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id
	update oh
	set oh.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			order_headers oh
		inner join 
			order_lifecycle olc on olc.order_id = oh.order_id and olc.CUSTOMER_ID = oh.customer_id
	update oh
	set oh.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			dw_getlenses.dbo.order_headers oh
		inner join 
			order_lifecycle olc on olc.order_id = oh.order_id and olc.CUSTOMER_ID = oh.customer_id

	-- INVOICE HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id
	update ih
	set ih.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			invoice_headers ih
		inner join 
			order_lifecycle olc on olc.order_id = ih.order_id and olc.CUSTOMER_ID = ih.customer_id
	update ih
	set ih.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			dw_getlenses.dbo.invoice_headers ih
		inner join 
			order_lifecycle olc on olc.order_id = ih.order_id and olc.CUSTOMER_ID = ih.customer_id

	-- SHIPMENT HEADER (PROFORMA - GETLENSES) ---> ON order_id, customer_id
	update sh
	set sh.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			shipment_headers sh
		inner join 
			order_lifecycle olc on olc.order_id = sh.order_id and olc.CUSTOMER_ID = sh.customer_id
	update sh
	set sh.order_lifecycle=olc.ORDER_LIFECYCLE
	from 
			dw_getlenses.dbo.shipment_headers sh
		inner join 
			order_lifecycle olc on olc.order_id = sh.order_id and olc.CUSTOMER_ID = sh.customer_id

------------------------------ CUSTOMER FIRST ORDER DATE ---------------------------------------

	-- PROFORMA (CUSTOMERS) ---> customer_id
	update c
	set 
		c.first_order_date=rs.first_order_date,
		c.last_order_date=rs.last_order_date,
		c.num_of_orders=rs.num_of_orders 
	from 
			dbo.customers c
		inner join 
			(select os.customer_id, 
				min(os.created_at) as first_order_date, max(os.created_at) as last_order_date,
				(select top 1 order_seq 
				from order_seq tos 
				where os.customer_id=tos.customer_id order by created_at desc) as num_of_orders
			from order_seq os
			group by os.customer_id) rs on rs.CUSTOMER_ID=c.customer_id

	-- GETLENSES (CUSTOMERS) ---> customer_id 
	update c
	set 
		c.first_order_date=rs.first_order_date,
		c.last_order_date=rs.last_order_date,
		c.num_of_orders=rs.num_of_orders 
	from 
			dw_getlenses.dbo.customers c
		inner join 
			(select 
				os.customer_id,
				min(os.created_at) as first_order_date, max(os.created_at) as last_order_date,
				(select top 1 order_seq 
				from order_seq tos 
				where os.customer_id=tos.customer_id order by created_at desc) as num_of_orders
			from order_seq os
			group by os.customer_id) rs on rs.CUSTOMER_ID=c.customer_id
end