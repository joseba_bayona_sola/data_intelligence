SELECT 
	mo.old_order_id,
	created_at, invoice_date, shipment_date,
	mo.email,
	OrderStatusName,
	mo.old_customer_id,
	cfod.customer_first_order_date,
	billing_prefix, billing_firstname, billing_lastname, billing_company,
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_telephone, billing_country,
	shipping_prefix, shipping_firstname, shipping_lastname, shipping_company,
	shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_description, shipping_telephone,
	order_currency as CurrencyID, 
	IFNULL(total_qty,0) AS total_qty,
	CAST((base_grand_total+base_discount_amount- base_shipping_amount) AS DECIMAL(29,4)) AS sub_total,
	CASE WHEN IFNULL(has_lens,0)>0 THEN 1 ELSE 0 END has_lens,
	ExchangeRate,
	base_shipping_amount, base_grand_total, base_discount_amount,
	replace(DomainName,'www.','') as DomainName,
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat ,
	tax_free_amount, tax_rate

FROM 
	migrate_orderdata mo
LEFT OUTER JOIN 
	(SELECT old_order_id,
		MAX(
			CASE WHEN POWER IS NOT NULL THEN 1 ELSE 0 END+
			CASE WHEN base_curve IS NOT NULL THEN 1 ELSE 0 END+
			CASE WHEN diameter IS NOT NULL THEN 1 ELSE 0 END+
			CASE WHEN `add` IS NOT NULL THEN 1 ELSE 0 END+
			CASE WHEN axis IS NOT NULL THEN 1 ELSE 0 END+
			CASE WHEN cylinder IS NOT NULL THEN 1 ELSE 0 END) AS has_lens, 
		SUM(Quantity) AS total_qty 
	FROM migrate_orderlinedata GROUP BY old_order_id) mol ON mol.old_order_id=mo.old_order_id
LEFT OUTER JOIN
	(SELECT old_customer_id, MIN(created_at) AS customer_first_order_date 
	FROM migrate_orderdata
	GROUP BY old_customer_id) cfod ON cfod.old_customer_id=mo.old_customer_id
where orderstatusname is not null;