
select old_customer_id,
case when email ='' then null else email end as email
from migrate_customerdata

SELECT 
	mo.old_order_id,
	created_at, joh.invoice_date, jd.shipment_date,
	mo.email,
	OrderStatusName,
	mo.old_customer_id,
	cfod.customer_first_order_date,
	billing_prefix, billing_firstname, billing_lastname, billing_company, 
	billing_street1, billing_street2, billing_street3, billing_street4, billing_postcode, billing_fax, billing_telephone, BillingCountryID,
	shipping_prefix, shipping_firstname, shipping_lastname, shipping_company,
	shipping_street1, shipping_street2, shipping_street3, shipping_street4, shipping_postcode, ShippingCountryID, shipping_description,
	shipping_fax, shipping_telephone,
	CurrencyID, 
	IFNULL(total_qty,0) AS total_qty,
	CAST(base_grand_total+base_discount_amount_exc_vat* (1+tax_rate/100)
	- base_shipping_amount_exc_vat * (1+tax_rate/100) 
	-tax_free_amount-(base_discount_amount_exc_vat * (1+ tax_rate/100) - base_discount_amount) AS DECIMAL(38,12)) AS sub_total,
	CASE WHEN IFNULL(has_lens,0)>0 THEN 1 ELSE 0 END has_lens,
	ExchangeRate,
	base_shipping_amount, base_grand_total, base_discount_amount,
	DomainName,
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat ,
	tax_free_amount, tax_rate,
	channel,
	source_code,
	cctype,
	paymentname
FROM 
	migrate_orderdata mo
LEFT OUTER JOIN 
	(SELECT old_order_id,
		MAX(
			CASE WHEN POWER IS NOT NULL THEN 1 ELSE 0 END +
			CASE WHEN base_curve IS NOT NULL THEN 1 ELSE 0 END +
			CASE WHEN diameter IS NOT NULL THEN 1 ELSE 0 END +
			CASE WHEN `add` IS NOT NULL THEN 1 ELSE 0 END +
			CASE WHEN axis IS NOT NULL THEN 1 ELSE 0 END +
			CASE WHEN cylinder IS NOT NULL THEN 1 ELSE 0 END) AS has_lens, 
		SUM(Quantity) AS total_qty 
	FROM migrate_orderlinedata 
	GROUP BY old_order_id) mol ON mol.old_order_id = mo.old_order_id
LEFT OUTER JOIN
	(SELECT old_customer_id, MIN(created_at) AS customer_first_order_date 
	FROM migrate_orderdata
	GROUP BY old_customer_id) cfod ON cfod.old_customer_id=mo.old_customer_id
LEFT OUTER JOIN 
	(SELECT STR_TO_DATE(paymentdate,'%Y%m%d') AS invoice_date, user_visible_order_id, cctype,paymentname 
	FROM 
			jss_orders_headers joh
		INNER JOIN 
			migrate_order_id_mapping mom ON mom.internal_order_id=joh.orderID
	WHERE IFNULL(joh.paymentDate,'') <> '') joh ON joh.user_visible_order_id=mo.old_order_id
LEFT OUTER JOIN 
	(SELECT MIN(dispatchdate) AS shipment_date, user_visible_order_id 
	FROM 
			(SELECT orderid, STR_TO_DATE(dispatchDate,'%Y%m%d') AS dispatchdate 
			FROM jss_dispatches
			UNION
			SELECT orderid,STR_TO_DATE(DATE,'%Y%m%d') AS dispatchdate 
			FROM amex_shipments) jd
		INNER JOIN 
			migrate_order_id_mapping mom ON mom.internal_order_id=jd.orderid
	GROUP BY mom.user_visible_order_id) jd ON jd.user_visible_order_id = mo.old_order_id;