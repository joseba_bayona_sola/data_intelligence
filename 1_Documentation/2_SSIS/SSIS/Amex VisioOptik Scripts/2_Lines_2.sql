SELECT 
	mo.*,
	CAST(CAST(mold.power AS DECIMAL(6,2)) AS CHAR(20))AS POWER,
	CASE mold.base_curve WHEN 'M' THEN '8.6' WHEN 'FM' THEN '8.4' WHEN 'SM' THEN '7.5' ELSE mold.base_curve END base_curve,
	CAST(CAST(mold.diameter AS DECIMAL(6,1)) AS CHAR(20)) AS diameter,
	CASE WHEN CAST(mold.`add` AS DECIMAL(6,2))=0.00 THEN LEFT(mold.`add`,1) ELSE CAST(mold.`add` AS DECIMAL(6,2)) END AS `add`,
	CAST(CAST(mold.axis AS UNSIGNED)  AS CHAR(20)) AS axis,
	CASE WHEN mold.cylinder IN ('D','N') THEN mold.cylinder ELSE '' END AS dominant,
	CASE WHEN mold.cylinder NOT IN ('D','N') THEN mold.cylinder ELSE '' END AS cylinder,
	mold.old_item_id,
	mold.product_id AS ProductID,
	mold.Quantity,
	row_total,
	mold.eye,
	mold.color
FROM 
	migrate_orderlinedata mold
INNER JOIN 
	(SELECT old_order_id, 
		base_discount_amount, base_discount_amount_exc_vat, base_grand_total, base_shipping_amount, 
		created_at, invoice_date, shipment_date,
		order_currency AS CurrencyID, REPLACE(DomainName,'www.','') AS DomainName, ExchangeRate, 
		tax_rate, IFNULL(tax_free_amount,0) AS tax_free_amount, 
		orderstatusname,
		base_grand_total+base_discount_amount_exc_vat* (1+tax_rate/100)- base_shipping_amount_exc_vat * (1+tax_rate/100)
			-IFNULL(tax_free_amount,0)-(base_discount_amount_exc_vat * (1+ tax_rate/100) - base_discount_amount) AS base_sub_total_inc_vat
	FROM migrate_orderdata) mo ON mo.old_order_id=mold.old_order_id;