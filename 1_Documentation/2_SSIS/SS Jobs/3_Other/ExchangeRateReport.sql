declare @sql as varchar(max)='select * from openquery(magento,''select 
cast(created_at as date) as `Date`,
order_currency_code,
base_to_global_rate,
count(*) as `Total Orders`
from magento01.sales_flat_order
where created_at>=date_add(cast(now() as date),interval -7 day)
and order_currency_code<>''''GBP''''
group by cast(created_at as date),
order_currency_code,
base_to_global_rate
order by cast(created_at as date),
order_currency_code,
base_to_global_rate
'')'

create table ##temptable([Date] date,order_currency_code varchar(3),base_to_global_rate numeric(7,4),total_orders int) 

insert into ##temptable
exec(@sql)


if exists(select * from ##temptable) 

    EXEC msdb.dbo.sp_send_dbmail
      @profile_name = 'TestEmail',
      @recipients = 'Stephen.Odetunde@visiondirect.co.uk',
      @subject = 'Exchange Rates Over last 7 days',
      @body = 'Exchange Rates Over last 7 days:',
      @execute_query_database = 'msdb',
      @query = 'select * from ##temptable'
	  	  
		  
drop table ##temptable