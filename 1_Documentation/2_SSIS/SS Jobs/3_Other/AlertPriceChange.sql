select *  into ##temptable_alertpricechange from openquery(magento,'SELECT * FROM marketing.missing_prices')


if exists(select * from ##temptable_alertpricechange) 

    EXEC sp_send_dbmail
      @profile_name = 'TestEmail',
      @recipients = 'stephen.odetunde@visiondirect.co.uk;Daniel.Harrison@visiondirect.co.uk',
      @subject = 'Recent Price Changes',
      @body = 'Check prices still exists for all stores:',
      @execute_query_database = 'msdb',
      @query = 'select * from ##temptable_alertpricechange'
	  	  
		  
drop table ##temptable_alertpricechange