

declare @curdate as date=getdate()-1

declare @sql as varchar(max)='select * from openquery(magento,''SELECT sfo.increment_id,sfo.created_at FROM sales_flat_order sfo 
INNER JOIN sales_flat_order_payment pay ON sfo.entity_id = pay.parent_id 
WHERE method = ''''storecredit'''' and sfo.created_at>=''''' + cast(@curdate as varchar(20))+''''''')'

create table ##temptable(increment_id varchar(30),created_at smalldatetime) 

insert into ##temptable
exec(@sql)


if exists(select * from ##temptable) 

    EXEC sp_send_dbmail
      @profile_name = 'TestEmail',
      @recipients = 'Anas.Nasarullah@visiondirect.co.uk',
      @subject = 'Storecredit Anomalies',
      @body = 'Storecredit Anomalies:',
      @execute_query_database = 'msdb',
      @query = 'select * from ##temptable'
	  	  
		  
drop table ##temptable