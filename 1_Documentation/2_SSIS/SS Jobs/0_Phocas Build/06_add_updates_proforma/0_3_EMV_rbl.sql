USE [DW_Proforma]
GO

ALTER PROCEDURE [dbo].[EMV_rbl] AS

BEGIN
	
	-- products	
	-- ##EMV_last_order_item
	-- ##EMV_last_shipment
	-- ##EMV_top_order_item
	-- ##EMV_websites
	-- ##EMV_last_order
	-- ##EMV_top_order0 - ##EMV_top_order0rev
	-- ##EMV_top_order - ##EMV_top_orderrev
	-- ##EMV_top_order_presc

	-- EMV_sync_list

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'EMV_rbl started')

	
		DELETE FROM products WHERE product_id < 0

		INSERT INTO products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -1 as product_id, category_id, 'Dailies' as name, 'GEN-DAILIES' as sku , 'lens', 'Default', 0
			from categories 
			where type = 'Spherical' AND feature = 'Standard' AND category = 'Dailies' 
				AND product_type = 'Contact Lenses - Dailies')

		INSERT INTO products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -2 as product_id, category_id, 'Monthlies & Other Lenses' as name, 'GEN-MONTHLY' as sku, 'lens', 'Default', 0
			from categories 
			where type = 'Spherical' AND feature = 'Standard' AND category = 'Monthlies' 
				AND product_type = 'Contact Lenses - Monthlies & Other')

		INSERT INTO products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -3 as product_id, category_id, 'Solutions & Eye Care' as name, 'GEN-SOLUTION' as sku, 'solution', 'Default', 0
			from categories 
			where category = 'Multi-purpose Solutions' 
				AND product_type = 'Solutions & Eye Care')

		INSERT INTO products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -4 as product_id, category_id, 'Other' as name, 'GEN-OTHER' as sku, 'othe', 'Default', 0
			from categories 
			where category = 'Other' 
				AND product_type = 'Other')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'inserted products finished')



		if OBJECT_ID('tempdb..##EMV_last_order_item') IS NOT NULL 
		BEGIN
			drop table ##EMV_last_order_item;
		END

		select	MIN(sf.store_id) customer_store_id, 
			MIN(t0.store_id) order_store_id, t0.customer_id, t1.product_id, 
			MAX(pr1.name) name, MAX(pr1.sku) sku, MAX(pr1.url_path) url, 
			MAX(t0.created_at) last_order_date, MAX(t0.order_id) last_order_id, MAX(t0.source) last_order_source, 
			MAX(t1.line_id) last_order_item_id, 
			MAX(cgr.category) report_category, 
			ISNULL(CASE WHEN  cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER') report_group_category,
			ROW_NUMBER() OVER (
				PARTITION BY t0.customer_id, ISNULL(CASE WHEN cgr.category = 'Colours' THEN 'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER')
				ORDER BY t0.customer_id DESC, ISNULL(CASE WHEN cgr.category = 'Colours' THEN 'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER'), max(t0.created_at) DESC, COUNT(*) DESC) 
				AS [RowNumber],
			CAST('2000-01-01' AS date) AS order_date
		into ##EMV_last_order_item
		FROM 
			[dbo].[customers] c0, 
			[dbo].[all_order_headers] t0, 
			[dbo].[all_order_lines] t1, 
			[dbo].[products] pr1, 
			categories cgr, 
			dw_stores_full sf
		WHERE 
			c0.customer_id = t0.customer_id AND 
			t0.order_id = t1.order_id AND t0.source = t1.source AND 
			t1.product_id = pr1.product_id AND 
			t0.status not in ('canceled', 'closed') AND 
			pr1.category_id=cgr.category_id AND pr1.category_id is not null ANd pr1.store_name = 'Default' AND 
			sf.store_name = c0.store_name AND 
			(pr1.promotional_product != 1  AND t1.price != 0 or pr1.sku like '%GEN%') AND 
			pr1.sku != 'BROCHURE'
		GROUP BY t0.customer_id, t1.product_id,  cgr.category, cgr.product_type;

		CREATE NONCLUSTERED INDEX [idx_1] ON ##EMV_last_order_item([customer_id] ASC, [report_category] ASC, [RowNumber] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [idx_2] ON ##EMV_last_order_item ([order_store_id], [product_id])

		PRINT(GETDATE())
		PRINT('built ##EMV_last_order_item')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_last_order_item finished')



		-- Get information of the customers last shipment
		if OBJECT_ID('tempdb..##EMV_last_shipment') IS NOT NULL 
		BEGIN
			drop table ##EMV_last_shipment;
		END

		select MAX(created_at) last_shipped, customer_id, source, order_id 
		into ##EMV_last_shipment
		from [dbo].[all_shipment_headers] 
		group by customer_id, order_id, source;

		CREATE CLUSTERED INDEX [idx_1] ON ##EMV_last_shipment([customer_id] ASC, [order_id] ASC, [last_shipped] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		PRINT(GETDATE())
		PRINT('built ####EMV_last_shipment')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_last_shipment finished')


		-- list of top items on each order
		if OBJECT_ID('tempdb..##EMV_top_order_item') IS NOT NULL 
		BEGIN
			drop table ##EMV_top_order_item;
		END

		select	
				t0.customer_id, t0.order_id, t0.source order_source, 
				MIN(t0.store_id) order_store_id, 
				t1.product_id, 
				MAX(pr1.name) name, MAX(pr1.sku) sku, 
				MAX(t0.created_at) created_at, 
				MAX(ISNULL(CASE WHEN cgr.category = '' THEN cgr.product_type ELSE cgr.category END ,'OTHER')) report_group_category,
				ROW_NUMBER() OVER (
					PARTITION BY t0.order_id, t0.source
					ORDER BY t0.customer_id, t1.product_id DESC, COUNT(*) DESC) AS [RowNumber]
		into ##EMV_top_order_item
		FROM 
				[dbo].[all_order_headers] t0, 
				[dbo].[all_order_lines] t1, 
				[dbo].[products] pr1
			LEFT JOIN 
				categories cgr ON pr1.category_id = cgr.category_id
		WHERE 
			t0.order_id = t1.order_id AND t0.source = t1.source AND 
			t1.product_id = pr1.product_id AND 
			(pr1.promotional_product != 1  AND t1.price != 0 or pr1.sku like '%GEN%') AND 
			pr1.sku != 'BROCHURE' ANd pr1.store_name = 'Default'
		GROUP BY t0.customer_id, t0.order_id, t0.source, t1.product_id;

		PRINT(GETDATE())
		PRINT('built ##EMV_top_order_item')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_top_order_item finished')


		-- build extended websites table, 
		if OBJECT_ID('tempdb..##EMV_websites')  IS NOT NULL BEGIN
			drop table ##EMV_websites;
		END
  
		select t0.*, 
			cast(isnull(cfg.value,'0') as varchar(max)) surveysweb_on -- 1 - surveysweb module is on
		into ##EMV_websites
		FROM 
				[dbo].[dw_stores_full] t0
			left outer join 
				dw_core_config_data cfg on t0.store_id = cfg.store_id and cfg.path = 'surveysweb/surveys/active' 

		CREATE NONCLUSTERED INDEX [idx_web2] ON ##EMV_websites([store_id] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		PRINT(GETDATE())
		PRINT('built ##EMV_websites')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_websites finished')


		-- build a list of all reminders set up on orders
		if OBJECT_ID('tempdb..##EMV_last_order')  IS NOT NULL BEGIN
		  drop table ##EMV_last_order;
		END
  
		select	
			customer_id, order_id, t1.store_id, t1.surveysweb_on, source, 
			status, created_at, 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id ORDER BY t0.customer_id, t0.created_at) AS [RowNumber], 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id ORDER BY t0.customer_id, t0.created_at DESC) AS [RowNumberRev],
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id, t1.surveysweb_on ORDER BY t0.customer_id, t0.created_at) AS [RowNumberSurv], 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id, t1.surveysweb_on ORDER BY t0.customer_id, t0.created_at DESC) AS [RowNumberSurvRev]
        
		into ##EMV_last_order
		FROM 
			[dbo].[all_order_headers] t0, 
			##EMV_websites t1
		where 
			t0.store_id=t1.store_id AND 
			t0.status not in ('canceled', 'closed');
						 
		CREATE NONCLUSTERED INDEX [idx_6] ON ##EMV_last_order([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_last_order finished')	
	
		PRINT(GETDATE())
		PRINT('built ##EMV_last_order')


		-- build 2 lists of all reminders set up on orders
		-- one for all reminders in the future
		-- second for reminders in the past

		if OBJECT_ID('tempdb..##EMV_top_order0')  IS NOT NULL 
		BEGIN
			drop table ##EMV_top_order0;
		END
		if OBJECT_ID('tempdb..##EMV_top_order0rev')  IS NOT NULL 
		BEGIN
			drop table ##EMV_top_order0rev;
		END
  
		select	
				customer_id, order_id, 'current_db' order_source, sf.store_id order_store_id,  
				document_date created_at, 
				cast(reminder_date as date) reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period
        
		into ##EMV_top_order0
		FROM 
			[dbo].[order_headers] t0, 
			dw_stores_full sf
					
		where t0.reminder_date IS NOT NULL -- AND ISDATE(t0.reminder_date)=1
			AND reminder_type IS NOT NULL -- AND reminder_type!='none'
			AND t0.status not in ('canceled', 'closed') 
			AND document_type = 'ORDER' 
			AND sf.store_name = t0.store_name;
						 
		select * 
		into ##EMV_top_order0rev 
		from ##EMV_top_order0;

		-- remove all expired entries
		delete from ##EMV_top_order0 where reminder_date < GETDATE();
		-- remove all not expired entries
		delete from ##EMV_top_order0rev where reminder_date >= GETDATE();
  
		PRINT(GETDATE())
		PRINT('built ##EMV_top_order0')

		insert into dw_sync_status VALUES ('EMV_rbl', getdate(), '##EMV_top_order0 finished')	


		-- create copy of above but numbered
		if OBJECT_ID('tempdb..##EMV_top_order') IS NOT NULL 
		BEGIN
			drop table ##EMV_top_order;
		END
		if OBJECT_ID('tempdb..##EMV_top_orderrev') IS NOT NULL 
		BEGIN
			drop table ##EMV_top_orderrev;
		END
  
		-- create table exactly as above + number (so next order has 1, next 2 and so on)
		select *,
			ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY reminder_date, order_id DESC) AS [RowNumber]
        into ##EMV_top_order
		FROM ##EMV_top_order0;

		-- as obove but latest is 1, previous 2
		select *,
			ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY reminder_date DESC, order_id DESC) AS [RowNumber]
        into ##EMV_top_orderrev
		FROM ##EMV_top_order0rev;

		CREATE NONCLUSTERED INDEX [idx_2p] ON ##EMV_top_order ([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [idx_2r] ON ##EMV_top_orderrev ([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		PRINT(GETDATE())
		PRINT('built ##EMV_top_orderrev')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'##EMV_top_orderrev finished')	


		-- update product(order item) details, for correct store
		UPDATE t1
		SET
			t1.name = p1.name, t1.sku = p1.sku, t1.product_id = p1.product_id, 
			-- t1.url ='http://www.'+t2.name+'/'+p1.url_path
			-- t1.url =''+ [dbo].[func_get_config_value] ('web/unsecure/base_url', t1.order_store_id) +''+p1.url_path
			t1.url ='' + cast(cfg.value as varchar(max)) + '' + p1.url_path
		FROM
			##EMV_last_order_item t1, 
			dw_stores_full t2,
			products p1,
			dw_core_config_data cfg 
		WHERE 
			t2.store_id = t1.order_store_id AND 
			p1.store_name = t2.store_name AND 
			t1.product_id = p1.product_id and
			cfg.store_id = t1.order_store_id and
			cfg.path='web/unsecure/base_url'

		UPDATE t1
		SET
			t1.name=p1.name, t1.sku =p1.sku, t1.product_id =p1.product_id
		FROM
			##EMV_top_order_item t1, 
			dw_stores_full sf,
			products p1 
		WHERE
			sf.store_id = t1.order_store_id AND 
			sf.store_name = p1.store_name AND
			t1.product_id = p1.product_id;


		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'update product(order item) details, for correct store finished')	
  
		PRINT(GETDATE())
		PRINT('bupdate product(order item) details, for correct store')


		-- list of next expiring prescriptions for each order
		if OBJECT_ID('tempdb..##EMV_top_order_presc')  IS NOT NULL BEGIN
		  drop table ##EMV_top_order_presc;
		END

		SELECT
			MAX(wearer.customer_id) customer_id, max(order_headers.order_id) order_id, max(order_headers.order_no) order_no, 
			'current_db' order_source, max(sf.store_id) order_store_id, 
			
			max(wearer.name) name, max(wearer.DOB) DOB, 
			max(wearer_prescription.date_last_prescription) date_last_prescription, max(wearer_prescription.optician_name) optician_name,
			wearer_prescription.id,
			MAX(reminder_presc) reminder_presc, MAX(reminder_type) reminder_type,
			ROW_NUMBER() OVER (PARTITION BY wearer_order_item.order_id ORDER BY wearer_prescription.date_last_prescription DESC) AS [RowNumber]
		INTO ##EMV_top_order_presc
		FROM         
				dw_wearer wearer 
			INNER JOIN
				dw_wearer_prescription wearer_prescription ON wearer.id = wearer_prescription.wearer_id 
			INNER JOIN
				dw_wearer_order_item wearer_order_item ON wearer_prescription.id = wearer_order_item.wearer_prescription_id 
			INNER JOIN
				order_headers ON CAST(wearer_order_item.order_id as varchar) = order_headers.order_no
			LEFT JOIN 
				dw_stores_full sf ON sf.store_name = order_headers.store_name
		 WHERE
		  reminder_presc='on' and reminder_date > GETDATE() and
		  order_headers.status not in ('canceled', 'closed') AND order_headers.document_type = 'ORDER'
		group by wearer_prescription.id, wearer_order_item.order_id, wearer_prescription.date_last_prescription
		order by wearer_order_item.order_id DESC;

		PRINT(GETDATE())
		PRINT('Built ##EMV_top_order_presc')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'EMV_top_order_presc finished')	



		-- ***************************
		-- rebuild main table
		-- ***************************

		-- TRUNCATE TABLE [dbo].[EMV_sync_list];
		DELETE FROM [dbo].[EMV_sync_list] WHERE SOURCE='Magento';

		INSERT INTO [dbo].[EMV_sync_list](CLIENT_ID, WEBSITE_ID, STORE_ID, CLIENTURN, 
			FIRSTNAME, LASTNAME, EMAIL, TITLE, SOURCE, 
			REGISTRATION_DATE, SEGMENT_2, WEBSITE_UNSUBSCRIBE, SEGMENT_GEOG) 
		SELECT 
		   customer_id, c.website_id, sf.store_id, customer_id, 
		   firstname, lastname, email, prefix, 'Magento', 
		   created_at, referafriend_code, 
		   CASE WHEN unsubscribe_all = 'Yes' or unsubscribe_all = '1'  THEN 1 ELSE 0 END, 
		   c.old_access_cust_no     
		from 
			[dbo].[customers] c, 
			dw_stores_full sf	
		WHERE c.store_name is not NULL AND sf.store_name = c.store_name;
 
 
		UPDATE t1
		SET
			t1.FIRST_ORDER_DATE = oFi.created_at,
			t1.LAST_ORDER_DATE = oLa.created_at, t1.LAST_ORDER_ID = oLa.order_id, t1.LAST_ORDER_SOURCE = oLa.source,
			t1.NUM_OF_ORDERS = ISNULL(oLa.RowNumber,0),
			t1.SEGMENT_LIFECYCLE = 
				(CASE
					WHEN (oLa.surveysweb_on = 1) THEN 'SURVEY' -- if last order is survey
					WHEN ((oLa.RowNumber > 0) AND (DATEADD(month, -15, GETDATE()) > oLa.created_at)) THEN 'LAPSED' -- last order older than 15 months
					WHEN ((oLa.RowNumber > 1) AND (DATEADD(month, 15, oLa2.created_at) < oLa.created_at))  THEN 'REACTIVATED' -- time between last order and previous > 15 months, last orderer leas than 15m ago
					ELSE CASE
						WHEN ((oLa.RowNumberSurv = 0) OR (oLa.RowNumberSurv IS NULL)) THEN 'RNB' 
						WHEN (oLaNSurv.RowNumberSurv = 1) THEN 'NEW' 
						WHEN (oLaNSurv.RowNumberSurv > 1) THEN 'REGULAR'
						END
				END)
			FROM
					[dbo].[EMV_sync_list] t1 
				LEFT JOIN 
					##EMV_last_order oFi ON oFi.customer_id = t1.CLIENT_ID AND oFi.RowNumber = 1 -- first order (including surveys)
				LEFT JOIN 
					##EMV_last_order OLa ON oLa.customer_id = t1.CLIENT_ID AND oLa.RowNumberRev = 1 -- last order (including surveys)
				LEFT JOIN 
					##EMV_last_order OLa2 ON oLa2.customer_id = t1.CLIENT_ID AND oLa2.RowNumberRev = 2 -- one before last order (including surveys)
				LEFT JOIN 
					##EMV_last_order oFiNSurv ON oFiNSurv.customer_id = t1.CLIENT_ID AND oFiNSurv.RowNumberSurv = 1 AND oFinSurv.surveysweb_on = 0 -- first not-Survey order
				LEFT JOIN 
					##EMV_last_order oLaNSurv ON oLaNSurv.customer_id = t1.CLIENT_ID AND oLaNSurv.RowNumberSurvRev=1 AND oLaNSurv.surveysweb_on = 0; -- last not-Survey order
						
		PRINT(GETDATE())
		PRINT('Built basic [EMV_sync_list]')

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'Built basic [EMV_sync_list] finished')	



		UPDATE t1
		SET
			t1.STORE_ID = ISNULL(t0.store_id, t1.STORE_ID)
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN
				[dbo].[all_order_headers] t0 ON  t0.order_id = t1.LAST_ORDER_ID AND t0.source = t1.LAST_ORDER_SOURCE;

		PRINT(GETDATE())
		PRINT('Update store id')
	
		UPDATE t1
		SET
			t1.LAST_LOGGED_IN_DATE = ISNULL(t2.login_at, t1.LAST_ORDER_DATE)
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				(SELECT customer_id, MAX(login_at) login_at 
				FROM log_customer customer GROUP BY customer_id) t2 ON t2.customer_id = t1.CLIENT_ID;	

		PRINT(GETDATE())
		PRINT('Update last logged in')

		UPDATE t1
		SET
			t1.STORE_ID = t2.order_store_id, 
			t1.ORDER_REMINDER_DATE = t2.reminder_date, t1.ORDER_REMINDER_ID = t2.order_id, t1.ORDER_REMINDER_SOURCE= 'current_db', --t2.order_source, reminders only in one db
			t1.ORDER_REMINDER_NO = t2.reminder_mobile, t1.ORDER_REMINDER_PREF = t2.reminder_type, 
			t1.ORDER_REMINDER_REORDER_URL = cast(isnull(cfg.value,'') as varchar(max)) + 'sales/order/reorder/order_id/' + CAST(t2.order_id AS varchar) + '/'
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN	
				##EMV_top_order t2 ON t2.customer_id = t1.CLIENT_ID AND t2.RowNumber=1
			left join 
				dw_core_config_data cfg on cfg.store_id=t1.store_id and cfg.path='web/secure/base_url'
		WHERE t2.order_id IS NOT NULL;

		UPDATE t1
		SET
			t1.STORE_ID = t2.order_store_id,
			t1.ORDER_REMINDER_DATE = t2.reminder_date, t1.ORDER_REMINDER_ID = t2.order_id, t1.ORDER_REMINDER_SOURCE= 'current_db', --t2.order_source, reminders only in one db
			t1.ORDER_REMINDER_NO = t2.reminder_mobile, t1.ORDER_REMINDER_PREF = t2.reminder_type,
			t1.ORDER_REMINDER_REORDER_URL = cast(isnull(cfg.value,'') as varchar(max)) + 'sales/order/reorder/order_id/' + CAST(t2.order_id AS varchar) + '/'
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN	
				##EMV_top_orderrev t2 	ON t2.customer_id = t1.CLIENT_ID AND t2.RowNumber=1
			left join 
				dw_core_config_data cfg on cfg.store_id=t1.store_id and cfg.path='web/secure/base_url'
		WHERE t2.order_id IS NOT NULL 
			and (t1.ORDER_REMINDER_DATE = '' OR t1.ORDER_REMINDER_DATE is null);

		PRINT(GETDATE())
		PRINT('Update reminder ')


		-------- set store id back for our migrated stores ------------
		UPDATE t1
		SET t1.STORE_ID = 22 --visiondirect.nl
		FROM [dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID IN (5, 6) --eur.nl

		UPDATE t1
		SET t1.STORE_ID = 23 --visiondirect.es
		FROM [dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 17 --eur.es

		UPDATE t1
		SET t1.STORE_ID = 21 --visiondirect.ie
		FROM [dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 3 --gl.ie

		UPDATE t1
		SET t1.STORE_ID = 24 --visiondirect.it
		FROM [dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 10 --gl.it

		UPDATE t1
		SET t1.STORE_ID = 20 --visiondirect.co.ik
		FROM [dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID IN (1,2,4) --gl.uk/nl/postoptics


		------------ migration related store ids --------------
		-- use one lens name as product for reminder
		UPDATE t1
 		SET t1.ORDER_REMINDER_PRODUCT = t3.name
 		FROM
	 			[dbo].[EMV_sync_list] t1 
 			LEFT JOIN 
				##EMV_top_order_item t3 ON t1.ORDER_REMINDER_ID = t3.order_id AND t3.order_source = 'current_db' 
					AND t3.report_group_category IN ('Dailies', 'Two Weeklies', 'Monthlies', 'Torics for Astigmatism', 'Multifocals', 'Colours');

		-- use any product if still empty
		UPDATE t1
 		SET t1.ORDER_REMINDER_PRODUCT = t3.name
 		FROM
	 			[dbo].[EMV_sync_list] t1 
 			LEFT JOIN 
				##EMV_top_order_item t3 ON t1.ORDER_REMINDER_ID = t3.order_id AND t3.order_source = 'current_db' 
 		WHERE t1.ORDER_REMINDER_PRODUCT IS NULL OR t1.ORDER_REMINDER_PRODUCT = '';

		PRINT(GETDATE())
		PRINT('Update reminder product ')
 

		UPDATE t1
		SET	
			t1.PRESCRIPTION_EXPIRY_DATE = cast(pr1.date_last_prescription as date),
			t1.PRESCRIPTION_WEARER = 
				(CASE WHEN (pr1.name = 'myself') THEN (t1.FIRSTNAME+' '+t1.LASTNAME+'''s') ELSE pr1.name+'''s' END),
			t1.PRESCRIPTION_OPTICIAN = pr1.optician_name
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				[dbo].[order_headers] t0 ON t0.customer_id = t1.CLIENT_ID 
			LEFT JOIN 
				##EMV_top_order_presc pr1  ON pr1.order_id = t0.order_id 
		WHERE t0.reminder_presc = 'on' AND pr1.RowNumber = 1;
	
		-- dateofbirth from prescription
		UPDATE t1
		SET	t1.DATEOFBIRTH = w1.DOB
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				dw_wearer w1 ON w1.customer_id=t1.CLIENT_ID and w1.name='myself'
		WHERE 1=1;
	
		PRINT(GETDATE())
		PRINT('Update prescription')
	
		-- country based on default billing address
		UPDATE t1
		SET	t1.COUNTRY = country_id
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				customers t2 ON t2.customer_id=t1.CLIENT_ID
			LEFT JOIN 
				customer_addresses t3 ON t3.address_id=t2.default_billing;
		
		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-01' 
		FROM [dbo].[EMV_sync_list] t1, bounces t2
		WHERE t1.[EMAIL] = t2.email COLLATE SQL_Latin1_General_CP1_CI_AS;

		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-02' 
		FROM [dbo].[EMV_sync_list] t1, unsubscribes t2
		WHERE t1.[EMAIL] = t2.email COLLATE SQL_Latin1_General_CP1_CI_AS;
		
		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-03' 
		FROM [dbo].[EMV_sync_list] t1
		WHERE t1.[EMAIL] LIKE '%@polocal.com' OR t1.[EMAIL] LIKE '%@gelocal.com';	

		
		--create tmp list of languages store to save time 
		SELECT 
			LANGUAGE = UPPER(LEFT( cast(cfg1.value as varchar(max)) ,2)),
			CURRENCY= cast(cfg2.value as varchar(max)),
			t0.store_id
		INTO #local_config
		FROM 
				dw_stores_full t0
			inner join 
				dw_core_config_data cfg1 on cfg1.store_id=t0.store_id and cfg1.path='general/locale/code'
			inner join 
				dw_core_config_data cfg2 on cfg2.store_id=t0.store_id and cfg2.path='currency/options/base'

		UPDATE t1
		SET
			t1.EMAIL_ORIGINE = sf.store_name, t1.EMAIL_ORIGIN = sf.store_name,
			t1.GENDER = (CASE WHEN t3.GENDER IS NOT NULL THEN t3.GENDER ELSE '' END),
			t1.LANGUAGE=  lc.LANGUAGE, t1.CURRENCY = lc.CURRENCY
		FROM
				[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				EMV_gender_prefix t3 ON t3.title = t1.title
			LEFT JOIN 
				dw_stores_full sf ON sf.store_id = t1.STORE_ID
			LEFT JOIN 
				#local_config lc ON lc.store_id = sf.store_id
		
		PRINT(GETDATE())
		PRINT('Update currency')
		-- update customer's store url


		UPDATE t1
		SET	t1.[STORE_URL] = cast(cfg.value as varchar(max))
		FROM
				[dbo].[EMV_sync_list] t1
			left join 
				dw_core_config_data cfg on cfg.path = 'web/unsecure/base_url' and cfg.store_id = t1.store_id;
			
		-- ************************************	
	
		UPDATE t1
		SET
			t1.[DAILIES_STORE_ID1]=t2.order_store_id,
			t1.[DAILIES_SKU1]=t2.product_id, t1.[DAILIES_NAME1]=t2.name, t1.[DAILIES_URL1]=t2.url,
			t1.[DAILIES_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Dailies' AND 
			t2.customer_id = t1.CLIENT_ID;	

		UPDATE t1
		SET
			t1.[DAILIES_STORE_ID2]=t2.order_store_id,
			t1.[DAILIES_SKU2]=t2.product_id, t1.[DAILIES_NAME2]=t2.name, t1.[DAILIES_URL2]=t2.url
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Contact Lenses - Dailies' AND 
			t2.customer_id = t1.CLIENT_ID;
	
		UPDATE t1
		SET
			t1.[MONTHLIES_STORE_ID1]=t2.order_store_id,
			t1.[MONTHLIES_SKU1]=t2.product_id, t1.[MONTHLIES_NAME1]=t2.name, t1.[MONTHLIES_URL1]=t2.url,
			t1.[MONTHLIES_CATEGORY1]=t2.report_category, 
			t1.[MONTHLIES_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE	
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Monthlies & Other' AND 
			t2.customer_id = t1.CLIENT_ID;	

		UPDATE t1
		SET
			t1.[MONTHLIES_STORE_ID2]=t2.order_store_id,
			t1.[MONTHLIES_SKU2]=t2.product_id, t1.[MONTHLIES_NAME2]=t2.name, t1.[MONTHLIES_URL2]=t2.url,
			t1.[MONTHLIES_CATEGORY2]=t2.report_category
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Contact Lenses - Monthlies & Other' AND 
			t2.customer_id = t1.CLIENT_ID;

		UPDATE t1
		SET
			t1.[COLOURS_STORE_ID1]=t2.order_store_id,
			t1.[COLOURS_SKU1]=t2.product_id, t1.[COLOURS_NAME1]=t2.name, t1.[COLOURS_URL1]=t2.url,
			t1.[COLOURS_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Colours' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[COLOURS_STORE_ID2]=t2.order_store_id,
			t1.[COLOURS_SKU2]=t2.product_id, t1.[COLOURS_NAME2]=t2. name, t1.[COLOURS_URL2]=t2.url
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Contact Lenses - Colours' AND
			t2.customer_id = t1.CLIENT_ID;
	
		UPDATE t1
		SET
			t1.[SOLUTIONS_STORE_ID1]=t2.order_store_id,
			t1.[SOLUTIONS_SKU1]=t2.product_id, t1.[SOLUTIONS_NAME1]=t2.name, t1.[SOLUTIONS_URL1]=t2.url,
			t1.[SOLUTIONS_CATEGORY1]=t2.report_category,
			t1.[SOLUTIONS_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Solutions & Eye Care' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[SOLUTIONS_STORE_ID2]=t2.order_store_id,
			t1.[SOLUTIONS_SKU2]=t2.product_id, t1.[SOLUTIONS_NAME2]=t2.name, t1.[SOLUTIONS_URL2]=t2.url,
			t1.[SOLUTIONS_CATEGORY2]=t2.report_category
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Solutions & Eye Care' AND
			t2.customer_id = t1.CLIENT_ID;

		
		UPDATE t1
		SET
			t1.[OTHER_STORE_ID1]=t2.order_store_id,
			t1.[OTHER_SKU1]=t2.product_id, t1.[OTHER_NAME1]=t2.name, t1.[OTHER_URL1]=t2.url,
			t1.[OTHER_CATEGORY1]=t2.report_category, 
			t1.[OTHER_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Other' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[OTHER_STORE_ID2]=t2.order_store_id,
			t1.[OTHER_SKU2]=t2.product_id, t1.[OTHER_NAME2]=t2. name, t1.[OTHER_URL2]=t2.url,
			t1.[OTHER_CATEGORY2]=t2.report_category
		FROM
				[dbo].[EMV_sync_list] t1, 
				##EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Other' AND
			t2.customer_id = t1.CLIENT_ID;

		PRINT(GETDATE())
		PRINT('Update product last bought fields')
		

		--set the last shipping date 
		UPDATE t1	
		SET	t1.[EMVADMIN5] = t2.last_shipped
		FROM
			[dbo].[EMV_sync_list] t1, 
			##EMV_last_shipment t2
		WHERE 
			t1.CLIENT_ID = t2.customer_id AND t1.LAST_ORDER_ID = t2.order_id AND t1.LAST_ORDER_SOURCE = t2.source;

		UPDATE t1	
		SET	t1.[EMVADMIN1] = t1.[LAST_ORDER_ID]
		FROM [dbo].[EMV_sync_list] t1

		PRINT(GETDATE())
		PRINT('Update last order it');


		--update with  atoken for auto login

		--TODO: nneds to included if used for emv
		UPDATE	t1 
		SET		
			t1.SEGMENT_3 = CAST(c.customer_id as varchar(100)) + '-' + 
				SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', LEFT(password_hash,10)+cast(customer_id as varchar(100))+REPLACE(CONVERT(VARCHAR(10), GETDATE(), 126), '/', '-'))), 3, 32)
		FROM	
				[EMV_sync_list] t1 
			INNER JOIN 
				customers c ON c.customer_id = t1.CLIENT_ID

		insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'Many Updates finished')	


		--add last product columns based on prioity
		--select * into [EMV_sync_list] from EMV_sync_list
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [OTHER_LAST_ORDER_DATE] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [OTHER_NAME1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [OTHER_NAME2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [OTHER_SKU1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [OTHER_SKU2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [OTHER_URL1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET  [LAST_PRODUCT_URL2] = [OTHER_URL2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET  [LAST_PRODUCT_TYPE] = 'OTHER' 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''


		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [SOLUTIONS_LAST_ORDER_DATE] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [SOLUTIONS_NAME1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [SOLUTIONS_NAME2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [SOLUTIONS_SKU1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [SOLUTIONS_SKU2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [SOLUTIONS_URL1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [SOLUTIONS_URL2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'SOLUTIONS' 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''

		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [COLOURS_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [COLOURS_LAST_ORDER_DATE]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [COLOURS_NAME1], [LENS_NAME1] = [COLOURS_NAME1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [COLOURS_NAME2], [LENS_NAME2] = [COLOURS_NAME2]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [COLOURS_SKU1], [LENS_SKU1] = [COLOURS_SKU1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [COLOURS_SKU2], [LENS_SKU2] = [COLOURS_SKU2] 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [COLOURS_URL1], [LENS_URL1] = [COLOURS_URL1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [COLOURS_URL2], [LENS_URL2] = [COLOURS_URL2] 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'COLOURS' 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''

		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [MONTHLIES_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [MONTHLIES_LAST_ORDER_DATE] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [MONTHLIES_NAME1], [LENS_NAME1] = [MONTHLIES_NAME1] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [MONTHLIES_NAME2], [LENS_NAME2] = [MONTHLIES_NAME2]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [MONTHLIES_SKU1], [LENS_SKU1] = [MONTHLIES_SKU1]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [MONTHLIES_SKU2], [LENS_SKU2] = [MONTHLIES_SKU2]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [MONTHLIES_URL1], [LENS_URL1] = [MONTHLIES_URL1]
		WHERE  [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [MONTHLIES_URL2], [LENS_URL2] = [MONTHLIES_URL2] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'MONTHLIES' 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''

		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [DAILIES_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [DAILIES_LAST_ORDER_DATE]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [DAILIES_NAME1], [LENS_NAME1] = [DAILIES_NAME1]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [DAILIES_NAME2], [LENS_NAME2] = [DAILIES_NAME2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [DAILIES_SKU1], [LENS_SKU1] = [DAILIES_SKU1]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [DAILIES_SKU2], [LENS_SKU2] = [DAILIES_SKU2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [DAILIES_URL1], [LENS_URL1] = [DAILIES_URL1] 
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [DAILIES_URL2], [LENS_URL2] = [DAILIES_URL2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''
		UPDATE [EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'DAILIES' 
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''



		DELETE FROM products WHERE product_id < 0

		--update category name with localised version 
		UPDATE t1
		SET t1.MONTHLIES_CATEGORY1 = t2.category_local
		FROM 
				EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.MONTHLIES_CATEGORY1 = t2.category AND t1.MONTHLIES_STORE_ID1 = t2.store_id
		UPDATE t1
		SET t1.MONTHLIES_CATEGORY2 = t2.category_local
		FROM 
				EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.MONTHLIES_CATEGORY2 = t2.category AND t1.MONTHLIES_STORE_ID2 = t2.store_id
		UPDATE t1
		SET t1.SOLUTIONS_CATEGORY1 = t2.category_local
		FROM 
				EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.SOLUTIONS_CATEGORY1 = t2.category AND t1.SOLUTIONS_STORE_ID1 = t2.store_id
		UPDATE t1
		SET t1.SOLUTIONS_CATEGORY2 = t2.category_local
		FROM 
				EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.SOLUTIONS_CATEGORY2 = t2.category AND t1.SOLUTIONS_STORE_ID2 = t2.store_id


	insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'Final Updates finished')	

	insert into dw_sync_status VALUES ('EMV_rbl',getdate(),'EMV_rbl finished')

END

