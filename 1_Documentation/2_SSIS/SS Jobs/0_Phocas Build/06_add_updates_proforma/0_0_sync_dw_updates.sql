USE [DW_Proforma]
GO


ALTER PROCEDURE [dbo].[sync_dw_updates]

	 @sync_from datetime2 = NULL 
AS

BEGIN
	

	SET NOCOUNT ON;

	insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_updates started')

		EXEC sync_dw_extra_table
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_extra_table finished')

		EXEC sync_dw_extra_updates
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_extra_updates finished')
	
		EXEC EMV_rbl
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'EMV_rbl finished')
	
		EXEC update_emv_cols
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'update_emv_cols finished')

		EXEC create_order_line_sums
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'create_order_line_sums finished')

	insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_updates finished')
END









