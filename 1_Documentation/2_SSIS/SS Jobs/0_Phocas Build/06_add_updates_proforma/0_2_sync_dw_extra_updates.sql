USE [DW_Proforma]
GO


ALTER PROCEDURE [dbo].[sync_dw_extra_updates] AS 

	--order
	UPDATE oh 
	SET oh.reorder_date = NULL
	FROM 
			order_headers oh 
		INNER JOIN 
			max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
	WHERE oh.order_id != mo.max_order_iod

	--invoice
	UPDATE oh 
	SET oh.reorder_date = NULL
	FROM 
			invoice_headers oh 
		INNER JOIN 
			max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
	WHERE oh.order_id != mo.max_order_iod

	--shipment
	UPDATE oh 
	SET oh.reorder_date = NULL
	FROM 
			shipment_headers oh 
		INNER JOIN 
			max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
	WHERE oh.order_id != mo.max_order_iod
