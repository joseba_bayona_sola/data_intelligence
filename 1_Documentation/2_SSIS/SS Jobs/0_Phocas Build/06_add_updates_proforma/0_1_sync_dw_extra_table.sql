USE [DW_Proforma]
GO


ALTER PROCEDURE [dbo].[sync_dw_extra_table] as

	/**--orders
	UPDATE order_headers set reorder_date =  null where reminder_type != 'reorder'
	UPDATE order_headers set reorder_profile_id = null where reminder_type != 'reorder' and  order_type != 'Automatic'

	--invoices
	UPDATE invoice_headers set reorder_date =  null where reminder_type != 'reorder'
	UPDATE invoice_headers set reorder_profile_id = null where reminder_type != 'reorder' and  order_type != 'Automatic'

	--shipments
	UPDATE shipment_headers set reorder_date =  null where reminder_type != 'reorder'
	UPDATE shipment_headers set reorder_profile_id = null where reminder_type != 'reorder' and  order_type != 'Automatic'
	**/


	IF OBJECT_ID ('dbo.max_rorder_order') IS NOT NULL BEGIN
		drop table max_rorder_order
	END

	SELECT reorder_profile_id, max(order_id) max_order_iod
	INTO max_rorder_order
	FROM order_headers
	WHERE document_type = 'order' and 
		reorder_profile_id IS NOT NULL AND reorder_profile_id != 0 and reorder_date is not null
	GROUP BY reorder_profile_id


	ALTER TABLE max_rorder_order ALTER COLUMN reorder_profile_id INT NOT NULL
	ALTER TABLE max_rorder_order ALTER COLUMN max_order_iod INT NOT NULL

	alter table max_rorder_order add primary key (reorder_profile_id, max_order_iod) 

