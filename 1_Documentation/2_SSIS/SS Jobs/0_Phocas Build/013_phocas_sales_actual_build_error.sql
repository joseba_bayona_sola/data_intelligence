

set nocount on

--### SALES ACTUAL ##########

if datename(weekday,getdate()) in ('Saturday')
--#####################Full Summarised Build ###############################
	begin
		exec xp_cmdshell 'C:\Phocas\programs\vacuum\vacETL.exe C:\Phocas\vac5_files\VD_Sales_Actual.vac5 Sales_Actual_Weekly_Build_Full';

		if exists(
			select * 
			from
				(SELECT cast(BulkColumn as varchar(max)) as logs 
				FROM OPENROWSET(BULK N'C:\Phocas\errors\Sales_Actual_Weekly_Build_Full.log',SINGLE_BLOB) AS Document) logs
			where logs.logs like '%System.%Exception:%'
			)
				RAISERROR ('Phocas Build has been completed. However, there is an Application Exception in the log. Please check the Phocas error logs in "C:\Phocas\errors\Sales_Actual_Weekly_Build_Full.log".',16,1);
		else
			begin
				EXEC [DW_GetLenses]..spRebuildIndexes 5,95,'Phocas_Sales_Actual_Live';
				exec [DW_GetLenses].Phocas.spCleanCacheSQLPlans 'Phocas_Sales_Actual_Live','with _CacheCTE_';
			end
		
		--drop the loader database to release disk space
		if exists(select * from sys.databases where name='Phocas_Sales_Actual_Loader')
			begin
				ALTER DATABASE [Phocas_Sales_Actual_Loader] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE;
				DROP DATABASE [Phocas_Sales_Actual_Loader];
			end
	end

else
--#####################Partial unsummarised Build#############################
	begin
	
		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_datareader] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_datawriter] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_ddladmin] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];

		exec xp_cmdshell 'C:\Phocas\programs\vacuum\vacETL.exe C:\Phocas\vac5_files\VD_Sales_Actual.vac5 Sales_Actual_Delta_Build_Partial';

		if exists(
			select * 
			from
				(SELECT cast(BulkColumn as varchar(max)) as logs 
				FROM OPENROWSET(BULK N'C:\Phocas\errors\Sales_Actual_Delta_Build_Partial.log',SINGLE_BLOB) AS Document) logs
			where logs.logs like '%System.%Exception:%'
			)
				RAISERROR ('Phocas Build has been completed. However, there is an Application Exception in the log. Please check the Phocas error logs in "C:\Phocas\errors\Sales_Actual_Delta_Build_Partial.log".',16,1);
		else
			begin
				EXEC [DW_GetLenses]..spRebuildIndexes 5,95,'Phocas_Sales_Actual_Live';
				exec [DW_GetLenses].Phocas.spCleanCacheSQLPlans 'Phocas_Sales_Actual_Live','with _CacheCTE_';
			end

		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_datareader] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_datawriter] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_Actual_Live];
		ALTER ROLE [db_ddladmin] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
	end


-- Errors

Phocas Build,Error,,GLSQLDW05,Phocas Build,,,The job failed.  The Job was invoked by User dw.  
The last step to run was step 13 (Build Phocas Database (SALES ACTUAL)).,09:30:22,0,0,SysAdmin,,,0

Executed as user: GETLENSES\sqlbackup. 

Executed ALTER INDEX [ix_grp25] ON [Phocas_Sales_Actual_Live].[dbo].[Entity_23] REORGANIZE [SQLSTATE 01000] (Message 0)  
Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[Entity_24] REORGANIZE [SQLSTATE 01000] (Message 0)  
Transaction (Process ID 51) was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction. [SQLSTATE 40001] (Error 1205)  
Executed ALTER INDEX [ix_LF] ON [Phocas_Sales_Actual_Live].[dbo].[Entity_25] REORGANIZE [SQLSTATE 01000] (Error 0)  
Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[Entity_26] REORGANIZE [SQLSTATE 01000] (Error 0)  
Executed ALTER INDEX [ix_LF] ON [Phocas_Sales_Actual_Live].[dbo].[Entity_27] REORGANIZE [SQLSTATE 01000] (Error 0).  

The step failed.


Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[Entity_24] REORGANIZE
Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[Entity_26] REORGANIZE
Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[Quanta] REORGANIZE
Executed ALTER INDEX ALL ON [Phocas_Sales_Actual_Live].[dbo].[SummaryCommands] REORGANIZE