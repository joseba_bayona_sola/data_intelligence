--### SALES PROFORMA ##########
--#####################Full Summarised Build ###############################

if datename(weekday,getdate()) in ('Sunday')

	begin 
		exec xp_cmdshell 'C:\Phocas\programs\vacuum\vacETL.exe C:\Phocas\vac5_files\VD_Sales_Proforma.vac5 Sales_Proforma_Weekly_Build_Full';
		if exists(select * from
			(SELECT cast(BulkColumn as varchar(max)) as logs FROM OPENROWSET(BULK N'C:\Phocas\errors\Sales_Proforma_Weekly_Build_Full.log',SINGLE_BLOB) AS Document) logs
				where logs.logs like '%System.%Exception:%')
				RAISERROR ('Phocas Build has been completed. However, there is an Application Exception in the log. Please check the Phocas error logs in "C:\Phocas\errors\Sales_Proforma_Weekly_Build_Full.log".',16,1);
		else
			begin
				EXEC [DW_GetLenses]..spRebuildIndexes 5,95,'Phocas_Sales_Proforma_Live';
				exec [DW_GetLenses].Phocas.spCleanCacheSQLPlans 'Phocas_Sales_Proforma_Live','with _CacheCTE_';

			end
		
		--drop the loader database to release disk space
		if exists(select * from sys.databases
				where name='Phocas_Sales_Proforma_Loader')
			begin
				ALTER DATABASE [Phocas_Sales_Proforma_Loader] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE;
				DROP DATABASE [Phocas_Sales_Proforma_Loader];
			end
	
	end 
 else
--#####################Partial unsummarised Build#############################
	begin
		
		USE [Phocas_Sales_Proforma_Live];
		ALTER ROLE [db_datareader] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_proforma_Live];
		ALTER ROLE [db_datawriter] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_proforma_Live];
		ALTER ROLE [db_ddladmin] drop MEMBER [NT AUTHORITY\NETWORK SERVICE];

		exec xp_cmdshell 'C:\Phocas\programs\vacuum\vacETL.exe C:\Phocas\vac5_files\VD_Sales_proforma.vac5 Sales_proforma_Delta_Build_Partial';
		if exists(select * from
			(SELECT cast(BulkColumn as varchar(max)) as logs FROM OPENROWSET(BULK N'C:\Phocas\errors\Sales_proforma_Delta_Build_Partial.log',SINGLE_BLOB) AS Document) logs
				where logs.logs like '%System.%Exception:%')
				RAISERROR ('Phocas Build has been completed. However, there is an Application Exception in the log. Please check the Phocas error logs in "C:\Phocas\errors\Sales_proforma_Delta_Build_Partial.log".',16,1);
		else
			begin
				EXEC [DW_GetLenses]..spRebuildIndexes 5,95,'Phocas_Sales_proforma_Live';
				exec [DW_GetLenses].Phocas.spCleanCacheSQLPlans 'Phocas_Sales_proforma_Live','with _CacheCTE_';

			end

		USE [Phocas_Sales_proforma_Live];
		ALTER ROLE [db_datareader] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_proforma_Live];
		ALTER ROLE [db_datawriter] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
		USE [Phocas_Sales_proforma_Live];
		ALTER ROLE [db_ddladmin] ADD MEMBER [NT AUTHORITY\NETWORK SERVICE];
		

	end
