USE [DW_GetLenses]
GO

ALTER procedure [dbo].[fix_missing_products] with recompile as 
begin
	insert into products (store_name, 
		name, product_id, status, category_id, sku, 
		created_at, updated_at, 
		product_type, product_lifecycle)

		select store_name,
			name, product_id, [status], isnull(category_id,37), sku,
			created_at, updated_at, 
			isnull(product_type, 'Other'), product_lifecycle
		from 
			(select *,
				row_number() over(partition by product_id,sku order by created_at) rno 
			from 
				(SELECT 'default' as store_name, 
					name, product_id, 2 as [status], category_id, dbo.getParamValue('sku','-sku'+sku,'-') as sku,
					document_date as created_at, document_updated_at as updated_at, 
					product_type, 'manual_entry' as product_lifecycle
      			FROM DW_GetLenses.dbo.order_lines l
				where not exists (select product_id from products p where p.product_id = l.product_id) 
				union
				SELECT 'default' as store_name, 
					name, product_id, 2 as [status], category_id, dbo.getParamValue('sku','-sku'+sku,'-') as sku, 
					document_date as created_at, document_updated_at as updated_at, 
					product_type, 'manual_entry' as product_lifecycle
				FROM DW_GetLenses.dbo.invoice_lines l
				where not exists (select product_id from products p where p.product_id=l.product_id) 
				union
				SELECT 'default' as store_name, 
					name, product_id, 2 as [status], category_id, dbo.getParamValue('sku','-sku'+sku,'-') as sku, 
					document_date as created_at, document_updated_at as updated_at, 
					product_type, 'manual_entry' as product_lifecycle
      			FROM DW_GetLenses.dbo.shipment_lines l
				where not exists (select product_id from products p where p.product_id=l.product_id)) rs) rs where rno=1
end