USE [DW_GetLenses]
GO

ALTER PROC [dbo].[create_order_line_sums] AS 

	if OBJECT_ID('dbo.dw_order_line_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_order_line_sum
	END
	if OBJECT_ID('dbo.dw_order_line_product_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_order_line_product_sum
	END

	if OBJECT_ID('dbo.dw_invoice_line_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_invoice_line_sum
	END
	if OBJECT_ID('dbo.dw_invoice_line_product_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_invoice_line_product_sum
	END

	if OBJECT_ID('dbo.dw_shipment_line_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_shipment_line_sum
	END
	if OBJECT_ID('dbo.dw_shipment_line_product_sum') IS NOT NULL 
	BEGIN 
		drop table  dw_shipment_line_product_sum
	END


	select document_id, document_type,
		CASE WHEN document_type IN ('CANCEL','CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_order_line_sum
	from order_lines
	group by document_id,document_type

	select document_id, document_type, product_id,
		CASE WHEN document_type IN ('CANCEL','CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_order_line_product_sum
	from order_lines
	group by document_id,document_type,product_id


	select document_id, document_type,
		CASE WHEN document_type IN ('CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_invoice_line_sum
	from invoice_lines
	group by document_id,document_type

	select document_id, document_type, product_id,
		CASE WHEN document_type IN ('CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_invoice_line_product_sum
	from invoice_lines
	group by document_id,document_type,product_id


	select document_id, document_type,
		CASE WHEN document_type IN ('CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_shipment_line_sum
	from shipment_lines
	group by document_id,document_type

	select document_id, document_type, product_id,
		CASE WHEN document_type IN ('CREDITMEMO') THEN -( count(*)) ELSE count(*) END row_count
	INTO dw_shipment_line_product_sum
	from shipment_lines
	group by document_id,document_type,product_id


	CREATE NONCLUSTERED INDEX IDX_dw_order_line_sum  ON dw_order_line_sum (document_id ASC, document_type ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	CREATE NONCLUSTERED INDEX IDX_dw_order_line_product_sum ON dw_order_line_product_sum (document_id ASC, document_type ASC, product_id aSC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)


	CREATE NONCLUSTERED INDEX IDX_dw_invoice_line_sum ON dw_invoice_line_sum(document_id ASC, document_type ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	CREATE NONCLUSTERED INDEX IDX_dw_invoice_line_product_sum ON dw_invoice_line_product_sum (document_id ASC, document_type ASC, product_id aSC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)


	CREATE NONCLUSTERED INDEX IDX_dw_shipment_line_sum ON dw_shipment_line_sum (document_id ASC, document_type ASC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

	CREATE NONCLUSTERED INDEX IDX_dw_shipment_line_product_sum ON dw_shipment_line_product_sum (document_id ASC, document_type ASC, product_id aSC)
		WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)




