USE [DW_GetLenses]
GO


ALTER PROC [dbo].[update_emv_cols] AS
	UPDATE c
	SET 
		[emvadmin1] = emv.[emvadmin1], [emvadmin2] = emv.emvadmin2, [emvadmin3] = emv.emvadmin3, [emvadmin4] = emv.emvadmin4, [emvadmin5] = emv.emvadmin5,
		[language] = emv.language,
		[last_logged_in_date] = emv.last_logged_in_date,
		[card_expiry_date] = emv.card_expiry_date,
		[segment_lifecycle] = emv.segment_lifecycle, [segment_usage] = emv.segment_usage, [segment_geog] = emv.segment_geog, [segment_purch_behaviour] = emv.segment_purch_behaviour,
		[segment_eysight] = emv.segment_eysight, [segment_sport] = emv.segment_sport, [segment_professional] = emv.segment_professional, [segment_lifestage] = emv.segment_lifestage, [segment_vanity] = emv.segment_vanity,
		[segment] = emv.segment, [segment_2] = emv.segment_2, [segment_3] = emv.segment_3, [segment_4] = emv.segment_4, 
		c.[gender] = emv.[GENDER]
	FROM 
			customers c 
		INNER JOIN 
			EMV_sync_list emv ON emv.client_id = c.customer_id


	UPDATE oh 
	SET oh.customer_business_channel = c.business_channel
	FROM 
			order_headers oh 
		INNER JOIN 
			customers c ON c.customer_id = oh.customer_id

	UPDATE oh 
	SET oh.customer_first_order_date = c.first_order_date
	FROM 
			invoice_headers oh 
		INNER JOIN 
			customers c ON c.customer_id = oh.customer_id
	UPDATE oh 
	SET oh.customer_business_channel = c.business_channel
	FROM 
			invoice_headers oh 
		INNER JOIN 
			customers c ON c.customer_id = oh.customer_id

	UPDATE oh 
	SET oh.customer_first_order_date = c.first_order_date
	FROM 
			shipment_headers oh 
		INNER JOIN 
			customers c ON c.customer_id = oh.customer_id
	UPDATE oh 
	SET oh.customer_business_channel = c.business_channel
	FROM 
			shipment_headers oh 
		INNER JOIN 
			customers c ON c.customer_id = oh.customer_id

