USE [DW_GetLenses]
GO


ALTER PROCEDURE [dbo].[sync_dw_updates]
	 @keep_sync_status_from date = NULL 
AS
BEGIN
	

	insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_updates started')

		exec fix_missing_customers
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'fix_missing_customers finished')

		exec fix_missing_products
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'fix_missing_products finished')

		EXEC sync_dw_extra_table
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_extra_table finished')

		EXEC sync_dw_extra_updates
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_extra_updates finished')
	
		EXEC EMV_rbl
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'EMV_rbl finished')
	
		EXEC update_emv_cols
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'update_emv_cols finished')

		EXEC create_order_line_sums
		insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'create_order_line_sums finished')

	insert into dw_sync_status VALUES ('sync_dw_updates',getdate(),'sync_dw_updates finished')

	
	if @keep_sync_status_from is not null
		begin
			delete from dw_sync_status where event_time<@keep_sync_status_from
			insert into dw_sync_status VALUES ('sync_dw_status',getdate(),'erased sync_dw_status history prior to ' + format(@keep_sync_status_from,'dd-MMM-yyyy'))
		end
	
END








