USE [DW_GetLenses]
GO

ALTER procedure [dbo].[fix_missing_customers] with recompile as
begin 
	declare @last_customer_id as int=(select max(customer_id) from customers)

	select *
	into #TempData
	from
		(SELECT customer_id,
			coalesce(customer_email,billing_email,shipping_email) as email, 
			store_name, order_id, 
			document_date, document_updated_at,
			customer_prefix, customer_firstname, customer_middlename, customer_lastname, customer_suffix, customer_gender, customer_dob,
			billing_telephone, referafriend_code,
			customer_first_order_date, business_channel
		from order_headers oh
		where customer_id is null 
			or not exists (select * from V_Customers c where c.customer_id=oh.customer_id)
		union
		SELECT customer_id,
			coalesce(customer_email,billing_email,shipping_email) as email,
			store_name, order_id,
			document_date, document_updated_at,
			customer_prefix, customer_firstname, customer_middlename, customer_lastname, customer_suffix, customer_gender, customer_dob,
			billing_telephone, referafriend_code,
			customer_first_order_date, business_channel
		from invoice_headers ih
		where customer_id is null 
			or not exists(select * from V_Customers c where c.customer_id=ih.customer_id)
		union
		SELECT customer_id,
			coalesce(customer_email,billing_email,shipping_email) as email, 
			store_name, order_id,
			document_date, document_updated_at,
			customer_prefix, customer_firstname, customer_middlename, customer_lastname, customer_suffix, customer_gender, customer_dob,
			billing_telephone, referafriend_code, 
			customer_first_order_date, business_channel
		from shipment_headers sh
		where customer_id is null 
			or not exists(select * from V_Customers c where c.customer_id=sh.customer_id)) rs

	create clustered index IX_TempData_email on #TempData (email, document_date, order_id, store_name)


	insert into customers(customer_id, email,
		website_group, website_id, store_name,
		created_at, updated_at,
		prefix, firstname, middlename, lastname, suffix,
		gender, dob,
		cus_phone, referafriend_code,
		first_order_date, business_channel,
		old_customer_id)

		select customer_id, email,
			website_group, store_id, store_name,
			document_date, document_updated_at,
			customer_prefix, customer_firstname, customer_middlename, customer_lastname, customer_suffix,
			isnull(customer_gender,''), customer_dob,
			billing_telephone, referafriend_code,
			customer_first_order_date, business_channel,
			'manual_entry' 
		from
			(select isnull(rs1.customer_id,new_cust_id.customer_id) as customer_id, rs1.email,
				stores.website_group, stores.store_id, rs1.store_name,
				document_date, document_updated_at, 
				customer_prefix, customer_firstname, customer_middlename, customer_lastname, customer_suffix, 
				customer_gender, customer_dob, 
				billing_telephone, referafriend_code, 
				customer_first_order_date, business_channel,
				row_number() over (partition by isnull(rs1.customer_id, new_cust_id.customer_id) order by document_date) rno
			from 
					#TempData rs1
				inner join 
					v_stores_all stores on stores.store_name=rs1.store_name
				left outer join
					(select email, 
						customer_id = @last_customer_id+row_number() over(order by email)
					from #TempData 
					where customer_id is null
					group by email) new_cust_id on new_cust_id.email=rs1.email) rs where rno=1

	update oh
	set oh.customer_id = rs.customer_id, oh.customer_suffix = 'manual_entry'
	from
			order_headers oh
		inner join 
			(select distinct new_cust_id.customer_id, rs1.order_id 
			from 
					#TempData rs1
				inner join
					(select email, 
						customer_id = @last_customer_id+row_number() over(order by email)
					from #TempData 
					where customer_id is null
					group by email) new_cust_id on new_cust_id.email=rs1.email) rs on rs.order_id=oh.order_id

	update ih
	set ih.customer_id = rs.customer_id, ih.customer_suffix='manual_entry'
	from 
			invoice_headers ih
		inner join 
			(select distinct new_cust_id.customer_id, rs1.order_id 
			from 
					#TempData rs1
				inner join
					(select email,
						customer_id=@last_customer_id+row_number() over(order by email)
					from #TempData 
					where customer_id is null
					group by email) new_cust_id on new_cust_id.email=rs1.email) rs on rs.order_id=ih.order_id

	update sh
	set sh.customer_id=rs.customer_id, sh.customer_suffix='manual_entry'
	from
			shipment_headers sh
		inner join 
			(select distinct new_cust_id.customer_id, rs1.order_id 
			from 
					#TempData rs1
				inner join
					(select email, 
						customer_id=@last_customer_id+row_number() over(order by email)
					from #TempData 
					where customer_id is null
					group by email) new_cust_id on new_cust_id.email=rs1.email) rs on rs.order_id=sh.order_id


	drop table #TempData

end

