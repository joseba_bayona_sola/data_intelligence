USE [DW_GetLenses]
GO


create procedure [Phocas].[spCleanCacheSQLPlans](@db_name as varchar(50),@prefix as varchar(50))
as
begin

	declare @hdl as varbinary(64)

	declare hdl_cursor cursor for
		SELECT plan_handle
		FROM 
				sys.dm_exec_cached_plans pl
			CROSS APPLY 
				sys.dm_exec_sql_text(plan_handle) AS st
		WHERE text like @prefix+'%'+@db_name+'%'

	open hdl_cursor
	fetch next from hdl_cursor into @hdl

		while @@fetch_status=0
		begin
			DBCC FREEPROCCACHE (@hdl)
			fetch next from hdl_cursor into @hdl
		end

	close hdl_cursor
	deallocate hdl_cursor 
end

GO


