declare @jobname as varchar(50),@stepname as varchar(50),@last_run_date as varchar(50),
@last_run_time as varchar(50),@last_run_duration as varchar(50),
@last_run_retries as varchar(50),@last_run_outcome as varchar(50)

declare @jobdesc as varchar(500)

select
@jobname='Job: '+j.name,
@stepname='Step: '+js.step_name,
@last_run_date='last run date: '+cast(js.last_run_date as varchar(8)),
@last_run_time='last run time: '+cast(js.last_run_time as varchar(8)),
@last_run_duration='last run duration: '+cast(js.last_run_duration as varchar(10)),
@last_run_retries='last run retries: '+cast(js.last_run_retries as varchar(10)),
@last_run_outcome='last run outcome: '+
case js.last_run_outcome
when 0 then 'Failed'
when 1 then 'Succeeded'
when 2 then 'Retry'
when 3 then 'Canceled'
when 5 then 'Unknown'
end


from 
dbo.sysjobs j
inner join dbo.sysjobsteps js on j.job_id=js.job_id
where j.name='Phocas Build'
and js.step_name='Run additional updates (DW_GetLenses)'

set @jobdesc=@jobname+char(13)+@stepname+char(13)+@last_run_date+char(13)+@last_run_time+char(13)+@last_run_duration+char(13)+@last_run_retries+char(13)+@last_run_outcome 


EXEC sp_send_dbmail
      @profile_name = 'TestEmail',
      @recipients = 'mohammed.khan@visiondirect.co.uk',
	  @copy_recipients='Stephen.Odetunde@visiondirect.co.uk;umar.farooq@visiondirect.co.uk',
      @subject = 'Phocas Build',
      @body = @jobdesc