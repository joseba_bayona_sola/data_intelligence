USE [msdb]
GO

/****** Object:  Job [Feeds_ZanoxES]    Script Date: 24/10/2016 14:01:26 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 24/10/2016 14:01:26 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Feeds_ZanoxES', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Mohammed', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Feeds_ZanoxES]    Script Date: 24/10/2016 14:01:26 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Feeds_ZanoxES', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/ISSERVER "\"\SSISDB\Feeds\VisionDirect_feeds\main.dtsx\"" /SERVER glsqldw05 /X86 /Par "\"$Project::param_LocalFileLocation\"";"\"C:\All Feeds\Zanox\"" /Par "\"$Project::param_LogFileLocation\"";"\"C:\All Feeds\Zanox\logs\"" /Par "\"$Project::param_OpenSSHKeyLocation\"";"\"C:\SSIS\ssis\VisionDirect_feeds\OpenSSHKey\GetOpticsAWSKey2013.enc\"" /Par "\"$Project::param_RemoteFileLocation\"";"\"/network-fileshares/media-live/wysiwyg/productsfeed\"" /Par "\"$Project::param_SFTPServer\"";"\"mediaftp.internal.visiondirect.info\"" /Par "\"$Project::param_SFTPUser\"";ubuntu /Par "\"$Project::param_SQLDB\"";"\"dw_getlenses\"" /Par "\"$Project::param_SQLScriptLocation\"";"\"C:\All Feeds\Zanox\scripts\zanox-feed-visiondirectES.sql\"" /Par "\"$Project::param_SQLServer\"";"\".\"" /Par "\"$Project::param_delimiter\"";"\",\"" /Par "\"$Project::param_do_not_upload(Boolean)\"";False /Par "\"$Project::param_doc_type\"";csv /Par "\"$Project::param_email_to\"";"\"mohammed.khan@visiondirect.co.uk;julia.rossi@visiondirect.it\"" /Par "\"$Project::param_feed_desc\"";"\"Feed for Zanox ES\"" /Par "\"$Project::param_feed_name\"";"\"zanox-feed-visiondirectES.csv\"" /Par "\"$Project::param_file_encoding\"";"\"UTF-8\"" /Par "\"$Project::param_keep_logs_for_days(Int32)\"";7 /Par "\"$Project::param_smtp_server\"";"\"10.0.1.15\"" /Par "\"$Project::param_use_version_no(Boolean)\"";False /Par "\"$ServerOption::LOGGING_LEVEL(Int16)\"";3 /Par "\"$ServerOption::SYNCHRONIZED(Boolean)\"";True /CALLERINFO SQLAGENT /REPORTING E', 
		@database_name=N'master', 
		@flags=0, 
		@proxy_name=N'SSISCredential'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


