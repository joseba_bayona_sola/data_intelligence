
=format(Fields!period.Value,"MMMM, yyyy")

=iif(Fields!customer_order_seq_no.Value<=12,Fields!customer_order_seq_no.Value,"13+")

=iif(Parameters!currency.Value=1,
	switch(
		Parameters!measure.Value=1,sum(Fields!global_revenue_ex_VAT.Value),
		Parameters!measure.Value=2,sum(Fields!global_revenue_ex_VAT.Value)-sum(Fields!global_prof_fee.Value),
		Parameters!measure.Value=3,sum(Fields!global_revenue_in_VAT.Value),
		Parameters!measure.Value=4,sum(Fields!global_VAT.Value),
		Parameters!measure.Value=5,sum(Fields!global_prof_fee.Value),
		Parameters!measure.Value=6,sum(Fields!customer_count.Value)
		),
	switch(
		Parameters!measure.Value=1,sum(Fields!local_revenue_ex_VAT.Value),
		Parameters!measure.Value=2,sum(Fields!local_revenue_ex_VAT.Value)-sum(Fields!local_prof_fee.Value),
		Parameters!measure.Value=3,sum(Fields!local_revenue_in_VAT.Value),
		Parameters!measure.Value=4,sum(Fields!local_VAT.Value),
		Parameters!measure.Value=5,sum(Fields!local_prof_fee.Value),
		Parameters!measure.Value=6,sum(Fields!customer_count.Value))
		)
=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure.Value=6,"#,0","'" & lookup(Parameters!currency.Value,Fields!id.value,Fields!symbol.Value,"currency") & "'#,0.00"),"")


=Fields!month_diff.Value

-- Row Group

	-- Name = cohort
	-- Group On = =Fields!period.Value AND =iif(Fields!customer_order_seq_no.Value<=12,Fields!customer_order_seq_no.Value,9999)

	-- Sorting = =Fields!period.Value

-- Column Group
	-- Name = month_no
	-- Group On = =Fields!month_diff.Value

	-- =Fields!month_diff.Value

	-- Filters = =Fields!month_diff.Value <= =Parameters!max_no_of_columns.Value



=iif(
	isnothing(Sum(Fields!next_order_customer_count.Value))=False,
	code.setRunTot(
		code.getAmount(Parameters!currency.Value,Parameters!measure.Value,Parameters!measure_in.Value,
			Sum(Fields!next_order_global_revenue_ex_VAT.Value),
			Sum(Fields!next_order_global_revenue_ex_VAT.Value)-sum(Fields!next_order_global_prof_fee.Value),
			sum(Fields!next_order_global_revenue_in_VAT.Value),
			sum(Fields!next_order_global_VAT.Value), 
			sum(Fields!next_order_global_prof_fee.Value),
			
			Sum(Fields!next_order_local_revenue_ex_VAT.Value),
			Sum(Fields!next_order_local_revenue_ex_VAT.Value)-sum(Fields!next_order_global_prof_fee.Value),
			sum(Fields!next_order_local_revenue_in_VAT.Value),
			sum(Fields!next_order_local_VAT.Value), 
			sum(Fields!next_order_local_prof_fee.Value),
			Sum(Fields!next_order_customer_count.Value), 
			
			reportitems("txtTotalAmount").value)), "")

=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure_in.Value=1,
	iif(Parameters!measure.Value=6,"#,0","'" & lookup(Parameters!currency.Value,Fields!id.value,Fields!symbol.Value,"currency") & "'#,0.00"),"#,0.00%"),"")

----------------------------------------------------------------------

-- Report Properties - Code

public runtotal as double=0


public function getAmount( _
byval currency as integer, _
byval measure as integer, _
byval measure_in as integer, _
global_revenue_ex_VAT as double, _
global_revenue_ex_VAT_after_prof_fee as double, _
global_revenue_in_VAT as double, _
global_VAT as double, _
global_prof_fee as double, _
local_revenue_ex_VAT as double, _
local_revenue_ex_VAT_after_prof_fee as double, _
local_revenue_in_VAT as double, _
local_VAT as double, _
local_prof_fee as double, _
customer_count as integer, _
TotalAmount as double) as double


if currency=1 then
	if measure_in=1 then
		select measure		
		case 1: return global_revenue_ex_VAT
		case 2: return global_revenue_ex_VAT-global_prof_fee
		case 3: return global_revenue_in_VAT
		case 4: return global_VAT
		case 5:	return global_prof_fee
		case 6: return customer_count
		end select 
	else
		select measure		
		case 1: return global_revenue_ex_VAT/TotalAmount
		case 2: return global_revenue_ex_VAT-global_prof_fee/TotalAmount
		case 3: return global_revenue_in_VAT/TotalAmount
		case 4: return global_VAT/TotalAmount
		case 5:	return global_prof_fee/TotalAmount
		case 6: return customer_count/TotalAmount
		end select 
	end if
else
	if measure_in=1 then
		select measure		
		case 1: return local_revenue_ex_VAT
		case 2: return local_revenue_ex_VAT-global_prof_fee
		case 3: return local_revenue_in_VAT
		case 4: return local_VAT
		case 5:	return local_prof_fee
		case 6: return customer_count
		end select 
	else
		select measure		
		case 1: return local_revenue_ex_VAT/TotalAmount
		case 2: return local_revenue_ex_VAT-global_prof_fee/TotalAmount
		case 3: return local_revenue_in_VAT/TotalAmount
		case 4: return local_VAT/TotalAmount
		case 5:	return local_prof_fee/TotalAmount
		case 6: return customer_count/TotalAmount
		end select 
	end if	
end if


end function



public function setRunTot(byval amnt as double) as double
	runtotal=runtotal+amnt
	return amnt
end function

public function getRunTot()
dim retTotal as double=runtotal
	runtotal =0
	return retTotal
end function


----------------------------------------------------------------------

=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure.Value=6,"#,0","'" & lookup(Parameters!currency.Value,Fields!id.value,Fields!symbol.Value,"currency") & "'#,0.00"),"")

=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure_in.Value=1,
	iif(Parameters!measure.Value=6,"#,0","'" & lookup(Parameters!currency.Value,Fields!id.value,Fields!symbol.Value,"currency") & "'#,0.00"),"#,0.00%"),"")

=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure_in.Value=1,
	iif(Parameters!measure.Value=6,"#,0","'" & lookup(Parameters!currency.Value,Fields!id.value,Fields!symbol.Value,"currency") & "'#,0.00"),"#,0.00%"),"")


=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure.Value=2,"#,0","�#,0.00"),"")


=iif(isnothing(Fields!customer_count.Value)=False,
	iif(Parameters!measure_in.Value=1,
		iif(Parameters!measure.Value=2,"#,0","�#,0.00"),"#,0.00%"),"")
