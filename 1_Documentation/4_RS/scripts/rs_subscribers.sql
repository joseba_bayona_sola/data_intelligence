USE ReportServer
go

SELECT
    d.SubscriptionID
   ,e.name AS ReportName
   ,e.path AS ReportPath
   ,d.LastStatus
   ,d.LastRunTime
   ,u.UserName
FROM
    dbo.Subscriptions d 
JOIN 
    dbo.Users AS u ON d.ownerid = u.UserID
JOIN 
    dbo.Catalog e ON itemid = report_oid
WHERE
    d.LastStatus = 'Failure sending mail: The report server has encountered a configuration error. Mail will not be resent.'