
select 
	store_name, 
	order_no, order_date, invoice_no, invoice_date, shipment_no, shipment_date, 
	document_type, status, 

	case when rno=1 then global_total_exc_vat else null end as global_revenue_ex_VAT,
	case when rno=1 then global_prof_fee else null end as global_prof_fee,
	case when rno=1 then global_total_inc_vat else null end as global_revenue_in_VAT,
	case when rno=1 then global_total_vat else null end as global_VAT,

	case when rno=1 then local_total_exc_vat else null end as local_revenue_ex_VAT,
	case when rno=1 then local_prof_fee else null end as local_prof_fee,
	case when rno=1 then local_total_inc_vat else null end as local_revenue_in_VAT,
	case when rno=1 then local_total_vat else null end as local_VAT,

	order_currency_code

from
	(select 
		ih.store_name,
		ih.order_no, ih.order_date, ih.invoice_no, ih.invoice_date, sh.shipment_no, sh.shipment_date,
		ih.document_type, ih.status,

		ih.global_total_exc_vat, ih.global_prof_fee, ih.global_total_inc_vat, ih.global_total_vat,
		ih.local_total_exc_vat, ih.local_prof_fee, ih.local_total_inc_vat, ih.local_total_vat,

		ih.order_currency_code,

		row_number() over(partition by ih.document_id order by shipment_date) as rno

	from
			(select * 
			from invoice_headers 
			union all 
			select * 
			from dw_proforma.dbo.invoice_headers 
			where document_date>='01-feb-2104' and store_name <>'visiooptik.fr') ih 
		left outer join 
			shipment_headers sh on sh.document_type <> 'creditmemo' and ih.order_id = sh.order_id 
	where ih.document_date >=  convert(varchar(11),@from_date,106) and ih.document_Date<dateadd(day,1, convert(varchar(11),@to_date,106) 
		and (sh.shipment_date>=dateadd(day,1,'''+convert(varchar(11),@exc_shipmentdate,106)+''') or sh.shipment_date is null)
		and ih.status in (@status))rs'

