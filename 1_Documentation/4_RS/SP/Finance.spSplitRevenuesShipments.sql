USE [DW_GetLenses]
GO

ALTER procedure [Finance].[spSplitRevenuesShipments](@from_date as date='01-sep-14', @to_date as date='31-dec-14') as

begin

	create table #FirstOrders(
		customer_id int,
		website varchar(100),
		src varchar(20))

	insert into #FirstOrders(customer_id)
		select distinct s.customer_id
		FROM v_shipments s 
		WHERE s.document_date BETWEEN @from_date AND @to_date

	create unique nonclustered index IX_FirstOrders on #FirstOrders(customer_id) include(website,src)

	update fo
	set fo.website = co.store_name, fo.src = co.src
	from
			#FirstOrders fo
		inner join
			(select o.customer_id,
				case o.source
					when 'visio_db' then 'VisioOptik'
					when 'lensbase_db' then 'lensbase' + s.tld
					when 'masterlens_db' then 'masterlens' + s.tld
					else s.store_name 
				end as store_name,
				case when source='getlenses' then 'gl' else 'hist' end as src,
				row_number() over(partition by o.customer_id order by o.order_date asc) as rno
			from 
					(select sh.customer_id, 'getlenses' as source, store_id, order_date 
					from 
							shipment_headers sh 
						inner join 
							dw_stores_full s on s.store_name = sh.store_name 
						inner join 
							#FirstOrders fo on fo.customer_id = sh.customer_id
					union
					select olc.customer_id, source, store_id, created_at 
					from 
							dw_proforma.dbo.order_lifecycle olc 
						inner join 
							#FirstOrders fo on fo.customer_id = olc.customer_id
					union
					select oh.customer_id, source, store_id, created_at 
					from 
							dbo.dw_hist_order oh 
						inner join 
							#FirstOrders fo on fo.customer_id = oh.customer_id) o
				inner join
					V_Stores_All s on s.store_id = o.store_id) co on (co.customer_id = fo.customer_id) and co.rno = 1

		SELECT 
			s.store_name AS [Current Site],
			CASE 
				when s.store_name = firstorder.website and firstorder.src = 'gl' then 'New'
				else firstorder.website 
			end AS [Previous Site],
			DATEname(MONTH,s.document_date) + '-' + DATENAME(YEAR,s.document_date) AS [Date],

			SUM(s.global_line_total_exc_vat) AS [revenue_ex_VAT], sum(s.global_line_total_inc_vat) as [revenue_in_VAT], sum(s.global_line_total_vat) as [VAT],
			sum(s.global_prof_fee) as [prof_fee]

		FROM 
				v_shipments s
			inner join 
				#FirstOrders firstorder on firstorder.customer_id = s.customer_id
		WHERE 
			s.document_date BETWEEN @from_date AND @to_date
		GROUP BY 
			s.store_name,
			CASE 
				when s.store_name = firstorder.website and firstorder.src = 'gl' then 'New'
				else firstorder.website 
			end,
			DATENAME(MONTH,s.document_date) + '-' + DATENAME(YEAR,s.document_date)

	drop table #FirstOrders

end

