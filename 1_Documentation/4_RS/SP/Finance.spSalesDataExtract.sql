USE [DW_GetLenses]

-- v_invoice_headers
-- #Temp_v_invoices / From v_invoices (document_date between @from_date and @to_date)
-- v_shipments

-- v_stores_all

ALTER procedure [Finance].[spSalesDataExtract](@from_date as date, @to_date as date, @document_type as varchar(50)='invoices') with recompile
as

begin


if @document_type='invoices'
begin

	select * into #Temp_v_invoices 
	from v_invoices 
	where document_date between @from_date and @to_date

	create nonclustered index ix_uniqueid on #Temp_v_invoices(uniqueid) 
	create nonclustered index ix_storename on #Temp_v_invoices(store_name)
	create nonclustered index ix_productid on #Temp_v_invoices(product_id) include (global_line_total_exc_vat,product_count)
	create nonclustered index ix_documentdate on #Temp_v_invoices(document_date) include (document_count)
	create nonclustered index ix_shippingcountryid on #Temp_v_invoices(shipping_country_id)


	select 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- Total Customers MTD, New Customers MTD

		sum(case when i.product_id=2317 then i.global_line_total_exc_vat else 0 end) as product_total_exc_vat_2317, -- #Aura Orders £
		sum(case when i.product_id=2317 then i.product_count else 0 end) as product_count_2317, -- #Aura Orders
		
		sum(i.local_line_total_exc_vat_for_localtoglobal) as local_total, sum(i.global_line_total_exc_vat_for_localtoglobal) as global_total, -- Local Total, Global Total ???

		sum(case when i.document_date=@to_date then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date=@to_date and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday -- New Customers Day
	
	from 
			v_invoice_headers ih
		inner join 
			#Temp_v_invoices i on ih.uniqueid=i.uniqueid
		inner join 
			(select distinct website, store_name from v_stores_all) st on i.store_name=st.store_name
	where i.store_name <> 'getlenses.it'
	group by 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end as [billing_country_id],
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD, Total Customers MTD
		
		0,0,0,0,0,0
	from 
			v_invoice_headers ih
		inner join 
			#Temp_v_invoices i on ih.uniqueid=i.uniqueid
	where i.store_name = 'visiondirect.co.uk'
	group by 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end
	
	drop table #Temp_v_invoices
end


else if @document_type='shipments'
	select 
		case 
			when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,

	sum(s.global_line_total_exc_vat) as global_line_total_exc_vat -- Shipped MTD

	from 
			v_shipments s
		inner join 
			(select distinct website,store_name from v_stores_all) st on st.store_name=s.store_name
	where 
		s.document_date between @from_date and @to_date and 
		s.store_name<>'getlenses.it'
	group by
		case 
			when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select
		case 
			when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] -- Shipped MTD 
			else 'Other countries' 
		end as [shipping_country_id], 
		
		sum(s.global_line_total_exc_vat) as global_line_total_exc_vat
	from 
		v_shipments s
	where 
		s.document_date between @from_date and @to_date and 
		s.store_name='visiondirect.co.uk'
	group by 
		case 
			when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] 
			else 'Other countries' 
		end

end
