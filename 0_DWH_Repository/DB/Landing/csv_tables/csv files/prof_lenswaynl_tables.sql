
delete from Landing.csv.lenswaynl_oh
where isnumeric(base_grand_total) = 0

update Landing.csv.lenswaynl_ol
set eye = null
where eye = 'NULL'

update Landing.csv.lenswaynl_ol
set dominant = null
where dominant = 'NULL'

----------------------------------------------------------

select magento_id, category_code, multiplier
from Landing.csv.lenswaynl_product_mapping

	select count(*)
	from Landing.csv.lenswaynl_oh

select top 1000 order_number, website_name, 
	customer_id, email_address, convert(datetime, order_date) order_date,
	postal_code, country, 
	payment_method, shipping_description, 
	convert(decimal(12, 4), base_grand_total) base_grand_total, convert(decimal(12, 4), base_shipping_amount) base_shipping_amount, 
	convert(decimal(12, 4), base_discount_amount) base_discount_amount, order_currency
	-- base_grand_total, base_shipping_amount, base_discount_amount, order_currency
from Landing.csv.lenswaynl_oh
order by order_date desc

	select count(*)
	from Landing.csv.lenswaynl_ol

select top 1000 line_id, order_number, 
	product_id, category_code, product_name, 
	eye, base_curve, diameter, power, cylinder, axis, addition, dominant, color, 
	ordered_quantity, 
	convert(decimal(12, 4), row_subtotal) row_subtotal, convert(decimal(12, 4), ol_discount) ol_discount
from Landing.csv.lenswaynl_ol
order by order_number desc

---------------------------------------------------------------

select oh1.old_order_id, oh2.order_number, oh1.magento_order_id, oh1.new_increment_id, 
	oh1.old_customer_id, oh2.customer_id, oh1.magento_customer_id, 
	oh1.email, oh2.email_address, 
	oh1.created_at, oh2.order_date, 
	oh2.website_name
from 
		Landing.migra_lenswaynl.migrate_orderdata_magento oh1
	left join
		Landing.csv.lenswaynl_oh oh2 on oh1.old_order_id = oh2.order_number
-- where oh2.order_number is null
where oh2.order_number is not null and oh1.old_customer_id <> oh2.customer_id
order by oh1.created_at desc

----------------

select oh.customer_id, c.old_customer_id,
	oh.num_orders
from
		(select customer_id, count(*) num_orders
		from Landing.csv.lenswaynl_oh
		group by customer_id) oh
	left join
		Landing.migra_lenswaynl.migrate_customerdata c on oh.customer_id = c.old_customer_id
where c.old_customer_id is null
order by oh.customer_id

select c1.customer_id, c2.old_customer_id, c2.new_magento_id,
	c1.email_address, c2.email, c1.num_orders, 
	count(*) over (partition by c1.customer_id) num_rep
from
		(select oh.customer_id, c.old_customer_id, oh.email_address, 
			oh.num_orders
		from
				(select customer_id, email_address, count(*) num_orders
				from Landing.csv.lenswaynl_oh
				group by customer_id, email_address) oh
			left join
				Landing.migra_lenswaynl.migrate_customerdata c on oh.customer_id = c.old_customer_id
		where c.old_customer_id is null) c1
	left join
		Landing.migra_lenswaynl.migrate_customerdata c2 on c1.email_address = c2.email
order by num_rep desc, c1.customer_id


----------------

select ol.product_id, p.magento_id,
	ol.category_code, p.category_code, 
	p.multiplier,
	ol.num_lines
from
		(select product_id, category_code, count(*) num_lines
		from Landing.csv.lenswaynl_ol
		group by product_id, category_code) ol
	left join
		Landing.csv.lenswaynl_product_mapping p on ol.product_id = p.magento_id and ol.category_code = p.category_code
where p.magento_id is null
order by ol.product_id, ol.category_code

