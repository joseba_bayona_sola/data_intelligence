use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_act_activity_group_type
drop trigger map.trg_act_activity_group_type;
go 

create trigger map.trg_act_activity_group_type on map.act_activity_group_type
after insert
as
begin
	set nocount on

	merge into map.act_activity_group_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.activity_group, '') = isnull(src.activity_group, '')) and (isnull(trg.activity_type, '') = isnull(src.activity_type, ''))
	when not matched 
		then
			insert (activity_group, activity_type, idETLBatchRun)
				values (src.activity_group, src.activity_type, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_act_migrate_store_date
drop trigger map.trg_act_migrate_store_date;
go 

create trigger map.trg_act_migrate_store_date on map.act_migrate_store_date
after insert
as
begin
	set nocount on

	merge into map.act_migrate_store_date_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_id, 0) = isnull(src.store_id, 0)) 

	when matched and 
		((isnull(trg.store_name, '') <> isnull(src.store_name, '')) or (isnull(trg.migration_date, '') <> isnull(src.migration_date, '')))
		then
			update set
				trg.store_name = src.store_name, trg.migration_date = src.migration_date, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (store_id, store_name, migration_date, idETLBatchRun)
				values (src.store_id, src.store_name, src.migration_date, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_act_website_migrate_date
drop trigger map.trg_act_website_migrate_date;
go 

create trigger map.trg_act_website_migrate_date on map.act_website_migrate_date
after insert
as
begin
	set nocount on

	merge into map.act_website_migrate_date_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.website_group, '') = isnull(src.website_group, '')) 

	when matched and 
		((isnull(trg.migration_date, '') <> isnull(src.migration_date, '')) or (isnull(trg.migration, '') <> isnull(src.migration, '')))
		then
			update set
				trg.migration_date = src.migration_date, trg.migration = src.migration, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (website_group, migration_date, migration, idETLBatchRun)
				values (src.website_group, src.migration_date, src.migration, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_act_website_register_mapping
drop trigger map.trg_act_website_register_mapping;
go 

create trigger map.trg_act_website_register_mapping on map.act_website_register_mapping
after insert
as
begin
	set nocount on

	merge into map.act_website_register_mapping_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_id, 0) = isnull(src.store_id, 0)) 

	when matched and 
		(isnull(trg.store_id_register, 0) <> isnull(src.store_id_register, 0))
		then
			update set
				trg.store_id_register = src.store_id_register, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_id, store_id_register, idETLBatchRun)
				values (src.store_id, src.store_id_register, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_act_website_old_access_cust_no
drop trigger map.trg_act_website_old_access_cust_no;
go 

create trigger map.trg_act_website_old_access_cust_no on map.act_website_old_access_cust_no
after insert
as
begin
	set nocount on

	merge into map.act_website_old_access_cust_no_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.old_access_cust_no, '') = isnull(src.old_access_cust_no, '')) 

	when matched and 
		(isnull(trg.store_id, 0) <> isnull(src.store_id, 0))
		then
			update set
				trg.store_id = src.store_id, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (old_access_cust_no, store_id, idETLBatchRun)
				values (src.old_access_cust_no, src.store_id, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_act_rank_cr_time_mm
drop trigger map.trg_act_rank_cr_time_mm;
go 

create trigger map.trg_act_rank_cr_time_mm on map.act_rank_cr_time_mm
after insert
as
begin
	set nocount on

	merge into map.act_rank_cr_time_mm_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.min_rank, 0) = isnull(src.min_rank, 0)) 
	when matched and
		((isnull(trg.max_rank, 0) <> isnull(src.max_rank, 0)) or (isnull(trg.rank_cr_time_mm, '') <> isnull(src.rank_cr_time_mm, '')) or
		(isnull(trg.cr_time_name, '') <> isnull(src.cr_time_name, '')))

		then 
			update set
				trg.max_rank = src.max_rank, trg.rank_cr_time_mm = src.rank_cr_time_mm, 
				trg.cr_time_name = src.cr_time_name,
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (min_rank, max_rank, rank_cr_time_mm, cr_time_name, idETLBatchRun)
				values (src.min_rank, src.max_rank, src.rank_cr_time_mm, src.cr_time_name, src.idETLBatchRun);
end;
go 


