use Landing
go 

drop table map.gen_store_type
go 
drop table map.gen_store_type_aud
go 

drop table map.gen_website_group
go 
drop table map.gen_website_group_aud
go 


drop table map.gen_masterlens_store
go 
drop table map.gen_masterlens_store_aud
go 


drop table map.gen_migrate_store
go 
drop table map.gen_migrate_store_aud
go 


drop table map.gen_store_company
go 
drop table map.gen_store_company_aud
go 


drop table map.gen_store_time
go 
drop table map.gen_store_time_aud
go 


drop table map.gen_countries
go 
drop table map.gen_countries_aud
go 



drop table map.gen_customer_type
go 
drop table map.gen_customer_type_aud
go 

drop table map.gen_customer_status
go 
drop table map.gen_customer_status_aud
go 



drop table map.gen_migrate_customer_store
go 
drop table map.gen_migrate_customer_store_aud
go 



drop table map.gen_market
go 
drop table map.gen_market_aud
go 

drop table map.gen_store_market_rel
go 
drop table map.gen_store_market_rel_aud
go 



drop table map.gen_customer_unsubscribe
go 
drop table map.gen_customer_unsubscribe_aud
go 