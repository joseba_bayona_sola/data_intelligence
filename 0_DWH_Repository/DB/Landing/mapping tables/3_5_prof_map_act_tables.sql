
use Landing
go

-- act_activity_group_type
select activity_group, activity_type, 
	idETLBatchRun, ins_ts
from Landing.map.act_activity_group_type

select activity_group, activity_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_activity_group_type_aud


-- act_migrate_store_date
select store_id, store_name, migration_date, 
	idETLBatchRun, ins_ts
from Landing.map.act_migrate_store_date

select store_id, store_name, migration_date, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_migrate_store_date_aud


-- act_lapsed_num_days
select lapsed_num_days, 
	idETLBatchRun, ins_ts
from Landing.map.act_lapsed_num_days



-- act_website_migrate_date
select website_group, migration_date, migration,
	idETLBatchRun, ins_ts
from Landing.map.act_website_migrate_date

select website_group, migration_date, migration,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_website_migrate_date_aud



-- act_website_register_mapping
select store_id, store_id_register,
	idETLBatchRun, ins_ts
from Landing.map.act_website_register_mapping

select store_id, store_id_register,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_website_register_mapping_aud



-- act_website_old_access_cust_no
select old_access_cust_no, store_id, 
	idETLBatchRun, ins_ts
from Landing.map.act_website_old_access_cust_no

select old_access_cust_no, store_id, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_website_old_access_cust_no_aud


select min_rank, max_rank, rank_cr_time_mm, cr_time_name,
	idETLBatchRun, ins_ts
from Landing.map.act_rank_cr_time_mm

select min_rank, max_rank, rank_cr_time_mm, cr_time_name,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.act_rank_cr_time_mm_aud

