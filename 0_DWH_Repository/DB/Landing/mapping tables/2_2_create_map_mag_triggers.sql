use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_mag_eav_attributes
drop trigger map.trg_mag_eav_attributes;
go 

create trigger map.trg_mag_eav_attributes on map.mag_eav_attributes
after insert
as
begin
	set nocount on

	merge into map.mag_eav_attributes_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.entity_type_code, '') = isnull(src.entity_type_code, '')) and (isnull(trg.attribute_code, '') = isnull(src.attribute_code, ''))
	when matched and
		(isnull(trg.backend_type, '') <> isnull(src.backend_type, ''))
		then 
			update set
				trg.backend_type = src.backend_type, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (entity_type_code, attribute_code, backend_type, idETLBatchRun)
				values (src.entity_type_code, src.attribute_code, src.backend_type, src.idETLBatchRun);
end;
go 