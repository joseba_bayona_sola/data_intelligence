use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_prod_product_type
drop trigger map.trg_prod_product_type;
go 

create trigger map.trg_prod_product_type on map.prod_product_type
after insert
as
begin
	set nocount on

	merge into map.prod_product_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_type, '') = isnull(src.product_type, '')) 

	when not matched 
		then
			insert (product_type, idETLBatchRun)
				values (src.product_type, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_category
drop trigger map.trg_prod_category;
go 

create trigger map.trg_prod_category on map.prod_category
after insert
as
begin
	set nocount on

	merge into map.prod_category_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_type, '') = isnull(src.product_type, '')) and (isnull(trg.category, '') = isnull(src.category, ''))  and
			(isnull(trg.category_magento, '') = isnull(src.category_magento, ''))  

	when not matched 
		then
			insert (product_type, category, category_magento, idETLBatchRun)
				values (src.product_type, src.category, src.category_magento, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_cl_type
drop trigger map.trg_prod_cl_type;
go 

create trigger map.trg_prod_cl_type on map.prod_cl_type
after insert
as
begin
	set nocount on

	merge into map.prod_cl_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.cl_type, '') = isnull(src.cl_type, '')) and (isnull(trg.cl_type_magento, '') = isnull(src.cl_type_magento, ''))  

	when not matched 
		then
			insert (cl_type, cl_type_magento, idETLBatchRun)
				values (src.cl_type, src.cl_type_magento, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_cl_feature
drop trigger map.trg_prod_cl_feature;
go 

create trigger map.trg_prod_cl_feature on map.prod_cl_feature
after insert
as
begin
	set nocount on

	merge into map.prod_cl_feature_aud with (tablock) as trg
	using inserted src
		on ((isnull(trg.cl_feature, '') = isnull(src.cl_feature, '')) and (isnull(trg.cl_feature_magento, '') = isnull(src.cl_feature_magento, ''))) 

	when not matched 
		then
			insert (cl_feature, cl_feature_magento, idETLBatchRun)
				values (src.cl_feature, src.cl_feature_magento, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_exclude_products
drop trigger map.trg_prod_exclude_products;
go 

create trigger map.trg_prod_exclude_products on map.prod_exclude_products
after insert
as
begin
	set nocount on

	merge into map.prod_exclude_products_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0)) 
	when matched and 
		((isnull(trg.product_name, '') <> isnull(src.product_name, '')) or (isnull(trg.info, '') <> isnull(src.info, '')) )
		then
			update set
				trg.product_name = src.product_name, trg.info = src.info, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (product_id, product_name, info, idETLBatchRun)
				values (src.product_id, src.product_name, src.info, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_visibility
drop trigger map.trg_prod_visibility;
go 

create trigger map.trg_prod_visibility on map.prod_visibility
after insert
as
begin
	set nocount on

	merge into map.prod_visibility_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.visibility, 0) = isnull(src.visibility, 0)) 
	when matched and 
		(isnull(trg.visibility_name, '') <> isnull(src.visibility_name, '')) 
		then
			update set
				trg.visibility_name = src.visibility_name, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (visibility, visibility_name, idETLBatchRun)
				values (src.visibility, src.visibility_name, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_param_base_curve
drop trigger map.trg_prod_param_base_curve;
go 

create trigger map.trg_prod_param_base_curve on map.prod_param_base_curve
after insert
as
begin
	set nocount on

	merge into map.prod_param_base_curve_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.base_curve, '') = isnull(src.base_curve, '')) 

	when not matched 
		then
			insert (base_curve, idETLBatchRun)
				values (src.base_curve, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_param_diameter
drop trigger map.trg_prod_param_diameter;
go 

create trigger map.trg_prod_param_diameter on map.prod_param_diameter
after insert
as
begin
	set nocount on

	merge into map.prod_param_diameter_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.diameter, '') = isnull(src.diameter, '')) 

	when not matched 
		then
			insert (diameter, idETLBatchRun)
				values (src.diameter, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_param_power
drop trigger map.trg_prod_param_power;
go 

create trigger map.trg_prod_param_power on map.prod_param_power
after insert
as
begin
	set nocount on

	merge into map.prod_param_power_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.power, '') = isnull(src.power, '')) 

	when not matched 
		then
			insert (power, idETLBatchRun)
				values (src.power, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_param_cylinder
drop trigger map.trg_prod_param_cylinder;
go 

create trigger map.trg_prod_param_cylinder on map.prod_param_cylinder
after insert
as
begin
	set nocount on

	merge into map.prod_param_cylinder_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.cylinder, '') = isnull(src.cylinder, '')) 

	when not matched 
		then
			insert (cylinder, idETLBatchRun)
				values (src.cylinder, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_param_axis
drop trigger map.trg_prod_param_axis;
go 

create trigger map.trg_prod_param_axis on map.prod_param_axis
after insert
as
begin
	set nocount on

	merge into map.prod_param_axis_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.axis, '') = isnull(src.axis, '')) 

	when not matched 
		then
			insert (axis, idETLBatchRun)
				values (src.axis, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_param_addition
drop trigger map.trg_prod_param_addition;
go 

create trigger map.trg_prod_param_addition on map.prod_param_addition
after insert
as
begin
	set nocount on

	merge into map.prod_param_addition_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.addition, '') = isnull(src.addition, '')) 

	when not matched 
		then
			insert (addition, idETLBatchRun)
				values (src.addition, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_param_dominance
drop trigger map.trg_prod_param_dominance;
go 

create trigger map.trg_prod_param_dominance on map.prod_param_dominance
after insert
as
begin
	set nocount on

	merge into map.prod_param_dominance_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.dominance, '') = isnull(src.dominance, '')) 

	when not matched 
		then
			insert (dominance, idETLBatchRun)
				values (src.dominance, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_product_30_90
drop trigger map.trg_prod_product_30_90;
go 

create trigger map.trg_prod_product_30_90 on map.prod_product_30_90
after insert
as
begin
	set nocount on

	merge into map.prod_product_30_90_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0) and isnull(trg.product_id_new, 0) = isnull(src.product_id_new, 0)) 
	when matched and
		(isnull(trg.size_new, 0) <> isnull(src.size_new, 0)) 
		then
			update set
				trg.size_new = src.size_new, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_id, product_id_new, size_new, idETLBatchRun)
				values (src.product_id, src.product_id_new, src.size_new, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_product_aura
drop trigger map.trg_prod_product_aura;
go 

create trigger map.trg_prod_product_aura on map.prod_product_aura
after insert
as
begin
	set nocount on

	merge into map.prod_product_aura_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0)) 
	when not matched 
		then
			insert (product_id, idETLBatchRun)
				values (src.product_id, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_product_type_oh
drop trigger map.trg_prod_product_type_oh;
go 

create trigger map.trg_prod_product_type_oh on map.prod_product_type_oh
after insert
as
begin
	set nocount on

	merge into map.prod_product_type_oh_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_type_oh, '') = isnull(src.product_type_oh, '')) 
	when matched and
		(isnull(trg.priority, 0) <> isnull(src.priority, 0)) 
		then
			update set
				trg.priority = src.priority, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_type_oh, priority, idETLBatchRun)
				values (src.product_type_oh, src.priority, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_product_category_rel
drop trigger map.trg_prod_product_category_rel;
go 

create trigger map.trg_prod_product_category_rel on map.prod_product_category_rel
after insert
as
begin
	set nocount on

	merge into map.prod_product_category_rel_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0)) 
	when matched and
		(isnull(trg.product_name, '') <> isnull(src.product_name, '')) or (isnull(trg.category_magento, '') <> isnull(src.category_magento, '')) 
		then
			update set
				trg.product_name = src.product_name, trg.category_magento = src.category_magento, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_id, product_name, category_magento, idETLBatchRun)
				values (src.product_id, src.product_name, src.category_magento, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_prod_product_colour
drop trigger map.trg_prod_product_colour;
go 

create trigger map.trg_prod_product_colour on map.prod_product_colour
after insert
as
begin
	set nocount on

	merge into map.prod_product_colour_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0)) 
	when matched and
		(isnull(trg.product_name, '') <> isnull(src.product_name, ''))
		then
			update set
				trg.product_name = src.product_name, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_id, product_name, idETLBatchRun)
				values (src.product_id, src.product_name, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_prod_product_family_group
drop trigger map.trg_prod_product_family_group;
go 

create trigger map.trg_prod_product_family_group on map.prod_product_family_group
after insert
as
begin
	set nocount on

	merge into map.prod_product_family_group_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_family_group, '') = isnull(src.product_family_group, '')) 
	when matched and
		(isnull(trg.product_family_group_name, '') <> isnull(src.product_family_group_name, '')) 
		then
			update set
				trg.product_family_group_name = src.product_family_group_name, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_family_group, product_family_group_name, idETLBatchRun)
				values (src.product_family_group, src.product_family_group_name, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_prod_product_family_group_pf
drop trigger map.trg_prod_product_family_group_pf;
go 

create trigger map.trg_prod_product_family_group_pf on map.prod_product_family_group_pf
after insert
as
begin
	set nocount on

	merge into map.prod_product_family_group_pf_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_id, 0) = isnull(src.product_id, 0)) 
	when matched and
		(isnull(trg.product_name, '') <> isnull(src.product_name, '')) or (isnull(trg.product_family_group, '') <> isnull(src.product_family_group, '')) 
		then
			update set
				trg.product_name = src.product_name, trg.product_family_group = src.product_family_group, 
				trg.upd_ts = getutcdate()		
	when not matched 
		then
			insert (product_id, product_name, product_family_group, idETLBatchRun)
				values (src.product_id, src.product_name, src.product_family_group, src.idETLBatchRun);
end;
go 