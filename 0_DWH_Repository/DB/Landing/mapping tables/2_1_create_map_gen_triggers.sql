use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_gen_store_type
drop trigger map.trg_gen_store_type;
go 

create trigger map.trg_gen_store_type on map.gen_store_type
after insert
as
begin
	set nocount on

	merge into map.gen_store_type_aud with (tablock) as trg
	using inserted src
		on ((isnull(trg.store_name, '') = isnull(src.store_name, '')) and (isnull(trg.tld, '') = isnull(src.tld, '')))
	when matched and
		(isnull(trg.store_type, '') <> isnull(src.store_type, ''))
		then 
			update set
				trg.store_type = src.store_type, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_name, tld, store_type, idETLBatchRun)
				values (src.store_name, src.tld, src.store_type, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_gen_website_group
drop trigger map.trg_gen_website_group;
go 

create trigger map.trg_gen_website_group on map.gen_website_group
after insert
as
begin
	set nocount on

	merge into map.gen_website_group_aud with (tablock) as trg
	using inserted src
		on ((isnull(trg.store_name, '') = isnull(src.store_name, '')) and (isnull(trg.name, '') = isnull(src.name, '')))
	when matched and
		(isnull(trg.website_group, '') <> isnull(src.website_group, ''))
		then 
			update set
				trg.website_group = src.website_group, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_name, name, website_group, idETLBatchRun)
				values (src.store_name, src.name, src.website_group, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_gen_masterlens_store
drop trigger map.trg_gen_masterlens_store;
go 

create trigger map.trg_gen_masterlens_store on map.gen_masterlens_store
after insert
as
begin
	set nocount on

	merge into map.gen_masterlens_store_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_name, '') = isnull(src.store_name, ''))
	when matched and
		((isnull(trg.store_type, '') <> isnull(src.store_type, '') or isnull(trg.website_group, '') <> isnull(src.website_group, '')))
		then 
			update set
				trg.store_type = src.store_type, trg.website_group = src.website_group, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_name, store_type, website_group, idETLBatchRun)
				values (src.store_name, src.store_type, src.website_group, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_migrate_store
drop trigger map.trg_gen_migrate_store;
go 

create trigger map.trg_gen_migrate_store on map.gen_migrate_store
after insert
as
begin
	set nocount on

	merge into map.gen_migrate_store_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_name, '') = isnull(src.store_name, ''))
	when matched and
		((isnull(trg.store_id, 0) <> isnull(src.store_id, 0) or
		isnull(trg.store_type, '') <> isnull(src.store_type, '') or isnull(trg.website_group, '') <> isnull(src.website_group, '')))
		then 
			update set
				trg.store_id = src.store_id, 
				trg.store_type = src.store_type, trg.website_group = src.website_group, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_id, store_name, store_type, website_group, idETLBatchRun)
				values (src.store_id, src.store_name, src.store_type, src.website_group, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_gen_store_company
drop trigger map.trg_gen_store_company;
go 

create trigger map.trg_gen_store_company on map.gen_store_company
after insert
as
begin
	set nocount on

	merge into map.gen_store_company_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.company_name, '') = isnull(src.company_name, '')) and (isnull(trg.store_name, '') = isnull(src.store_name, ''))

	when not matched 
		then
			insert (company_name, store_name, idETLBatchRun)
				values (src.company_name, src.store_name, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_time
drop trigger map.trg_gen_time;
go 

create trigger map.trg_gen_time on map.gen_time
after insert
as
begin
	set nocount on

	merge into map.gen_time_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.time_name, '') = isnull(src.time_name, '')) 

	when matched and
			((isnull(trg.hour_name, '') <> isnull(src.hour_name, '')) or (isnull(trg.day_part, '') <> isnull(src.day_part, '')))
		then
			update set
				trg.hour_name = src.hour_name,  
				trg.day_part = src.day_part,  
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (time_name, hour_name, hour, minute, day_part, idETLBatchRun)
				values (src.time_name, src.hour_name, src.hour, src.minute, src.day_part, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_countries
drop trigger map.trg_gen_countries;
go 

create trigger map.trg_gen_countries on map.gen_countries
after insert
as
begin
	set nocount on

	merge into map.gen_countries_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.country_code, '') = isnull(src.country_code, '')) 

	when matched and
			(isnull(trg.country_name, '') <> isnull(src.country_name, '') or 
			isnull(trg.country_type, '') <> isnull(src.country_type, '') or isnull(trg.country_zone, '') <> isnull(src.country_zone, '') or isnull(trg.country_continent, '') <> isnull(src.country_continent, '') or
			isnull(trg.country_state, '') <> isnull(src.country_state, ''))
		then
			update set
				trg.country_name = src.country_name,  
				trg.country_type = src.country_type, trg.country_zone = src.country_zone, trg.country_continent = src.country_continent,  
				trg.country_state = src.country_state, 
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (country_code, country_name, country_type, country_zone, country_continent, country_state, idETLBatchRun)
				values (src.country_code, src.country_name, src.country_type, src.country_zone, src.country_continent, src.country_state, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_customer_type
drop trigger map.trg_gen_customer_type;
go 

create trigger map.trg_gen_customer_type on map.gen_customer_type
after insert
as
begin
	set nocount on

	merge into map.gen_customer_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.customer_type, '') = isnull(src.customer_type, '')) 

	when matched and
			(isnull(trg.description, '') <> isnull(src.description, ''))
		then
			update set
				trg.description = src.description,  
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (customer_type, description, idETLBatchRun)
				values (src.customer_type, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_customer_status
drop trigger map.trg_gen_customer_status;
go 

create trigger map.trg_gen_customer_status on map.gen_customer_status
after insert
as
begin
	set nocount on

	merge into map.gen_customer_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.customer_status, '') = isnull(src.customer_status, '')) 

	when matched and
			(isnull(trg.description, '') <> isnull(src.description, ''))
		then
			update set
				trg.description = src.description,  
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (customer_status, description, idETLBatchRun)
				values (src.customer_status, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_gen_migrate_customer_store
drop trigger map.trg_gen_migrate_customer_store;
go 

create trigger map.trg_gen_migrate_customer_store on map.gen_migrate_customer_store
after insert
as
begin
	set nocount on

	merge into map.gen_migrate_customer_store_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.migrate_store_code, '') = isnull(src.migrate_store_code, '')) 

	when matched and
			(isnull(trg.migrate_customer_store, '') <> isnull(src.migrate_customer_store, '') or isnull(trg.migrate_customer_f, '') <> isnull(src.migrate_customer_f, ''))
		then
			update set
				trg.migrate_customer_store = src.migrate_customer_store, trg.migrate_customer_f = src.migrate_customer_f,  
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (migrate_store_code, migrate_customer_store, migrate_customer_f, idETLBatchRun)
				values (src.migrate_store_code, src.migrate_customer_store, src.migrate_customer_f, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_market
drop trigger map.trg_gen_market;
go 

create trigger map.trg_gen_market on map.gen_market
after insert
as
begin
	set nocount on

	merge into map.gen_market_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.market_id, 0) = isnull(src.market_id, 0)) 

	when matched and
			(isnull(trg.market_name, '') <> isnull(src.market_name, ''))
		then
			update set
				trg.market_name = src.market_name, 
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (market_id, market_name, idETLBatchRun)
				values (src.market_id, src.market_name, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_gen_store_market_rel
drop trigger map.trg_gen_store_market_rel;
go 

create trigger map.trg_gen_store_market_rel on map.gen_store_market_rel
after insert
as
begin
	set nocount on

	merge into map.gen_store_market_rel_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_id, 0) = isnull(src.store_id, 0) and isnull(trg.store_name, '') = isnull(src.store_name, '') and isnull(trg.country, '') = isnull(src.country, '')) 

	when matched and
			(isnull(trg.market_id, 0) <> isnull(src.market_id, 0) or isnull(trg.market_name, '') <> isnull(src.market_name, ''))
		then
			update set
				trg.market_id = src.market_id, trg.market_name = src.market_name, 
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (store_id, store_name, country, market_id, market_name, idETLBatchRun)
				values (src.store_id, src.store_name, src.country, src.market_id, src.market_name, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_gen_customer_unsubscribe
drop trigger map.trg_gen_customer_unsubscribe;
go 

create trigger map.trg_gen_customer_unsubscribe on map.gen_customer_unsubscribe
after insert
as
begin
	set nocount on

	merge into map.gen_customer_unsubscribe_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.customer_unsubscribe, '') = isnull(src.customer_unsubscribe, '')) 

	when matched and
			(isnull(trg.description, '') <> isnull(src.description, ''))
		then
			update set
				trg.description = src.description,  
				trg.upd_ts = getutcdate()	
						 
	when not matched 
		then
			insert (customer_unsubscribe, description, idETLBatchRun)
				values (src.customer_unsubscribe, src.description, src.idETLBatchRun);
end;
go 
