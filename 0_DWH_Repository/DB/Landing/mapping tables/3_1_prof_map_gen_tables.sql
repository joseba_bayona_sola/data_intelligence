
use Landing
go

select store_name, tld, store_type, 
	idETLBatchRun, ins_ts
from Landing.map.gen_store_type

select *
from 
	Landing.map.gen_store_type t1
inner join
	Landing.map.gen_store_type t2 on (isnull(t1.store_name, '') = isnull(t2.store_name, '')) and (isnull(t1.tld, '') = isnull(t2.tld, ''))

select store_name, tld, store_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_store_type_aud



select store_name, store_type, website_group,
	idETLBatchRun, ins_ts
from Landing.map.gen_masterlens_store

select store_name, store_type, website_group,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_masterlens_store_aud



select store_id, store_name, store_type, website_group,
	idETLBatchRun, ins_ts
from Landing.map.gen_migrate_store

select store_id, store_name, store_type, website_group,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_migrate_store_aud



select company_name, store_name, 
	idETLBatchRun, ins_ts
from Landing.map.gen_store_company

select company_name, store_name, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_store_company_aud



select time_name, hour_name, hour, minute, day_part, 
	idETLBatchRun, ins_ts
from Landing.map.gen_time

select time_name, hour_name, hour, minute, day_part, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_time_aud


select country_code, country_name, country_type, country_zone, country_continent, country_state,
	idETLBatchRun, ins_ts
from Landing.map.gen_countries

select country_code, country_name, country_type, country_zone, country_continent, country_state,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_countries_aud




select customer_type, description, 
	idETLBatchRun, ins_ts
from Landing.map.gen_customer_type

select customer_type, description,  
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_customer_type_aud


select customer_status, description,  
	idETLBatchRun, ins_ts
from Landing.map.gen_customer_status

select customer_status, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_customer_status_aud



select migrate_store_code, migrate_customer_store, migrate_customer_f,  
	idETLBatchRun, ins_ts
from Landing.map.gen_migrate_customer_store

select migrate_store_code, migrate_customer_store, migrate_customer_f,  
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_migrate_customer_store_aud




select market_id, market_name,  
	idETLBatchRun, ins_ts
from Landing.map.gen_market

select market_id, market_name,  
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_market_aud


select store_id, store_name, country, market_id, market_name,  
	idETLBatchRun, ins_ts
from Landing.map.gen_store_market_rel

select store_id, store_name, country, market_id, market_name,  
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_store_market_rel_aud




select customer_unsubscribe, description,  
	idETLBatchRun, ins_ts
from Landing.map.gen_customer_unsubscribe

select customer_unsubscribe, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_customer_unsubscribe_aud