use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.prod_product_type
create table map.prod_product_type(
	product_type	varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.prod_product_type add constraint [DF_map_prod_product_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_type_aud
create table map.prod_product_type_aud(
	product_type	varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	upd_ts			datetime);
go

alter table map.prod_product_type_aud add constraint [DF_map_prod_product_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_category
create table map.prod_category(
	product_type		varchar(255) NOT NULL,
	category			varchar(255) NOT NULL,
	category_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_category add constraint [DF_map_prod_category_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_category_aud
create table map.prod_category_aud(
	product_type		varchar(255) NOT NULL,
	category			varchar(255) NOT NULL,
	category_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_category_aud add constraint [DF_map_prod_category_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_cl_type
create table map.prod_cl_type(
	cl_type				varchar(255) NOT NULL,
	cl_type_magento		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_cl_type add constraint [DF_map_prod_cl_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_cl_type_aud
create table map.prod_cl_type_aud(
	cl_type				varchar(255) NOT NULL,
	cl_type_magento		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_cl_type_aud add constraint [DF_map_prod_cl_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.prod_cl_feature
create table map.prod_cl_feature(
	cl_feature			varchar(255) NOT NULL,
	cl_feature_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_cl_feature add constraint [DF_map_prod_cl_feature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_cl_feature_aud
create table map.prod_cl_feature_aud(
	cl_feature			varchar(255) NOT NULL,
	cl_feature_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_cl_feature_aud add constraint [DF_map_prod_cl_feature_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.prod_exclude_products
create table map.prod_exclude_products(
	product_id			int NOT NULL,
	product_name		varchar(255) NOT NULL,
	info				varchar(255),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_exclude_products add constraint [DF_map_prod_exclude_products_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_exclude_products_aud
create table map.prod_exclude_products_aud(
	product_id			int NOT NULL,
	product_name		varchar(255) NOT NULL,
	info				varchar(255),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_exclude_products_aud add constraint [DF_map_prod_exclude_products_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_visibility
create table map.prod_visibility(
	visibility			int NOT NULL,
	visibility_name		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_visibility add constraint [DF_map_prod_visibility_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_visibility_aud
create table map.prod_visibility_aud(
	visibility			int NOT NULL,
	visibility_name		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_visibility_aud add constraint [DF_map_prod_visibility_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.prod_param_base_curve
create table map.prod_param_base_curve(
	base_curve			char(3),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_base_curve add constraint [DF_map_prod_param_curve_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_base_curve_aud
create table map.prod_param_base_curve_aud(
	base_curve			char(3),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_base_curve_aud add constraint [DF_map_prod_param_base_curve_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_diameter
create table map.prod_param_diameter(
	diameter			char(4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_diameter add constraint [DF_map_prod_diameter_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_diameter_aud
create table map.prod_param_diameter_aud(
	diameter			char(4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_diameter_aud add constraint [DF_map_prod_param_diameter_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_power
create table map.prod_param_power(
	power				char(6),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_power add constraint [DF_map_prod_power_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_power_aud
create table map.prod_param_power_aud(
	power				char(6),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_power_aud add constraint [DF_map_prod_param_power_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_cylinder
create table map.prod_param_cylinder(
	cylinder			char(5),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_cylinder add constraint [DF_map_prod_cylinder_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_cylinder_aud
create table map.prod_param_cylinder_aud(
	cylinder			char(5),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_cylinder_aud add constraint [DF_map_prod_param_cylinder_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_axis
create table map.prod_param_axis(
	axis				char(3),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_axis add constraint [DF_map_prod_axis_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_axis_aud
create table map.prod_param_axis_aud(
	axis				char(3),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_axis_aud add constraint [DF_map_prod_param_axis_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_addition
create table map.prod_param_addition(
	addition			char(4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_addition add constraint [DF_map_prod_addition_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_addition_aud
create table map.prod_param_addition_aud(
	addition			char(4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_addition_aud add constraint [DF_map_prod_param_addition_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_param_dominance
create table map.prod_param_dominance(
	dominance			char(1),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_param_dominance add constraint [DF_map_prod_dominance_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_param_dominance_aud
create table map.prod_param_dominance_aud(
	dominance			char(1),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_param_dominance_aud add constraint [DF_map_prod_param_dominance_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.prod_product_30_90
create table map.prod_product_30_90(
	product_id			int NOT NULL, 
	product_id_new		int NOT NULL, 
	size_new			int NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_product_30_90 add constraint [DF_map_prod_product_30_90_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_30_90_aud
create table map.prod_product_30_90_aud(
	product_id			int NOT NULL, 
	product_id_new		int NOT NULL, 
	size_new			int NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_product_30_90_aud add constraint [DF_map_prod_product_30_90_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_product_aura
create table map.prod_product_aura(
	product_id			int NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_product_aura add constraint [DF_map_prod_product_aura_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_aura_aud
create table map.prod_product_aura_aud(
	product_id			int NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_product_aura_aud add constraint [DF_map_prod_product_aura_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_product_type_oh
create table map.prod_product_type_oh(
	product_type_oh		varchar(255) NOT NULL,
	priority			int NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_product_type_oh add constraint [DF_map_prod_product_type_oh_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_type_oh_aud
create table map.prod_product_type_oh_aud(
	product_type_oh		varchar(255) NOT NULL,
	priority			int NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_product_type_oh_aud add constraint [DF_map_prod_product_type_oh_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.prod_product_category_rel
create table map.prod_product_category_rel(
	product_id			int NOT NULL, 
	product_name		varchar(255) NOT NULL, 
	category_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_product_category_rel add constraint [DF_map_prod_product_category_rel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_category_rel_aud
create table map.prod_product_category_rel_aud(
	product_id			int NOT NULL, 
	product_name		varchar(255) NOT NULL, 
	category_magento	varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_product_category_rel_aud add constraint [DF_map_prod_product_category_rel_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.prod_product_colour
create table map.prod_product_colour(
	product_id			int NOT NULL, 
	product_name		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.prod_product_colour add constraint [DF_map_prod_product_colour_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_colour_aud
create table map.prod_product_colour_aud(
	product_id			int NOT NULL, 
	product_name		varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	upd_ts				datetime);
go

alter table map.prod_product_colour_aud add constraint [DF_map_prod_product_colour_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.prod_product_family_group
create table map.prod_product_family_group(
	product_family_group		varchar(255) NOT NULL,
	product_family_group_name	varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.prod_product_family_group add constraint [DF_map_prod_product_family_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_family_group_aud
create table map.prod_product_family_group_aud(
	product_family_group		varchar(255) NOT NULL,
	product_family_group_name	varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	upd_ts						datetime);
go

alter table map.prod_product_family_group_aud add constraint [DF_map_prod_product_family_group_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.prod_product_family_group_pf
create table map.prod_product_family_group_pf(
	product_id				int NOT NULL, 
	product_name			varchar(255) NOT NULL, 
	product_family_group	varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.prod_product_family_group_pf add constraint [DF_map_prod_prod_product_family_group_pf_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.prod_product_family_group_pf_aud
create table map.prod_product_family_group_pf_aud(
	product_id				int NOT NULL, 
	product_name			varchar(255) NOT NULL, 
	product_family_group	varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime);
go

alter table map.prod_product_family_group_pf_aud add constraint [DF_map_prod_product_family_group_pf_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
