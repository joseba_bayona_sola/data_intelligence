use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.mag_eav_attributes
create table map.mag_eav_attributes(
	entity_type_code		varchar(255) NULL,
	attribute_code			varchar(255) NULL,
	backend_type			varchar(255) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.mag_eav_attributes add constraint [DF_map_mag_eav_attributes_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.mag_eav_attributes_aud
create table map.mag_eav_attributes_aud(
	entity_type_code		varchar(255) NULL,
	attribute_code			varchar(255) NULL,
	backend_type			varchar(255) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.mag_eav_attributes_aud add constraint [DF_map_mag_eav_attributes_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 