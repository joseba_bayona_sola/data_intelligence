use Landing
go

-- vat_countries_registered
select countries_registered_code, country_type, country_code, country_registered_name,
	idETLBatchRun, ins_ts
from Landing.map.vat_countries_registered

select countries_registered_code, country_type, country_code, country_registered_name,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_countries_registered_aud


-- vat_vat_type_rate
select vat_type_rate_code, vat_type_rate_name,
	idETLBatchRun, ins_ts
from Landing.map.vat_vat_type_rate

select vat_type_rate_code, vat_type_rate_name,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_vat_type_rate_aud


-- vat_product_type_vat
select product_type_vat,
	idETLBatchRun, ins_ts
from Landing.map.vat_product_type_vat

select product_type_vat,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_product_type_vat_aud





-- vat_vat_rate
select countries_registered_code, product_type_vat, vat_type_rate_code, vat_time_period_start, vat_time_period_finish, 
	vat_rate, active,
	idETLBatchRun, ins_ts
from Landing.map.vat_vat_rate

select countries_registered_code, product_type_vat, vat_type_rate_code, vat_time_period_start, vat_time_period_finish, 
	vat_rate, active,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_vat_rate_aud


-- vat_prof_fee_rate
select countries_registered_code, product_type_vat, vat_time_period_start, vat_time_period_finish, 
	prof_fee_rate, active,
	idETLBatchRun, ins_ts
from Landing.map.vat_prof_fee_rate

select countries_registered_code, product_type_vat, vat_time_period_start, vat_time_period_finish, 
	prof_fee_rate, active,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_prof_fee_rate_aud



-- vat_prof_fee_fixed_value
select countries_registered_code, order_value_min, order_value_max,
	prof_fee_fixed_value, active,
	idETLBatchRun, ins_ts
from Landing.map.vat_prof_fee_fixed_value

select countries_registered_code, order_value_min, order_value_max,
	prof_fee_fixed_value, active,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_prof_fee_fixed_value_aud




-- vat_countries_registered_store
select countries_registered_code, store_id,
	idETLBatchRun, ins_ts
from Landing.map.vat_countries_registered_store

select countries_registered_code, store_id,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.vat_countries_registered_store_aud
