use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.sales_order_stage
create table map.sales_order_stage(
	order_stage		varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.sales_order_stage add constraint [DF_map_sales_order_stage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_order_stage_aud
create table map.sales_order_stage_aud(
	order_stage		varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.sales_order_stage_aud add constraint [DF_map_sales_order_stage_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_order_status
create table map.sales_order_status(
	order_status	varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.sales_order_status add constraint [DF_map_sales_order_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_order_status_aud
create table map.sales_order_status_aud(
	order_status	varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.sales_order_status_aud add constraint [DF_map_sales_order_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_order_type
create table map.sales_order_type(
	order_type		varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.sales_order_type add constraint [DF_map_sales_order_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_order_type_aud
create table map.sales_order_type_aud(
	order_type		varchar(20) NOT NULL,
	description		varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.sales_order_type_aud add constraint [DF_map_sales_order_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_order_status_magento
create table map.sales_order_status_magento(
	order_status_magento		varchar(32) NOT NULL,
	order_status_magento_code	varchar(32) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.sales_order_status_magento add constraint [DF_map_sales_order_status_magento_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_order_status_magento_aud
create table map.sales_order_status_magento_aud(
	order_status_magento		varchar(32) NOT NULL,
	order_status_magento_code	varchar(32) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.sales_order_status_magento_aud add constraint [DF_map_sales_order_status_magento_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_payment_method
create table map.sales_payment_method(
	payment_method			varchar(20) NOT NULL,
	payment_method_code		varchar(255) NOT NULL,
	description				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_payment_method add constraint [DF_map_sales_payment_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_payment_method_aud
create table map.sales_payment_method_aud(
	payment_method			varchar(20) NOT NULL,
	payment_method_code		varchar(255) NOT NULL,
	description				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.sales_payment_method_aud add constraint [DF_map_sales_payment_method_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_cc_type
create table map.sales_cc_type(
	cc_type			varchar(50) NOT NULL,
	card_type_code	varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.sales_cc_type add constraint [DF_map_sales_cc_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_cc_type_aud
create table map.sales_cc_type_aud(
	cc_type			varchar(50) NOT NULL,
	card_type_code	varchar(255) NOT NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.sales_cc_type_aud add constraint [DF_map_sales_cc_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_shipping_method
create table map.sales_shipping_method(
	shipping_method				varchar(255) NOT NULL,
	shipping_method_code		varchar(255) NOT NULL,
	shipping_method_code_erp	varchar(50),
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.sales_shipping_method add constraint [DF_map_sales_shipping_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_shipping_method_aud
create table map.sales_shipping_method_aud(
	shipping_method				varchar(255) NOT NULL,
	shipping_method_code		varchar(255) NOT NULL,
	shipping_method_code_erp	varchar(50),
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.sales_shipping_method_aud add constraint [DF_map_sales_shipping_method_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_price_type
create table map.sales_price_type(
	price_type			varchar(40) NOT NULL,
	description			varchar(255) NOT NULL,
	priority			int NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_price_type add constraint [DF_map_sales_price_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_price_type_aud
create table map.sales_price_type_aud(
	price_type			varchar(40) NOT NULL,
	description			varchar(255) NOT NULL,
	priority			int NOT NULL,	
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_price_type_aud add constraint [DF_map_sales_price_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.sales_channel
create table map.sales_channel(
	channel				varchar(50), 
	channel_name		varchar(50), 
	description			varchar(255),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_channel add constraint [DF_map_sales_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_channel_aud
create table map.sales_channel_aud(
	channel				varchar(50), 
	channel_name		varchar(50), 
	description			varchar(255),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_channel_aud add constraint [DF_map_sales_channel_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_marketing_channel
create table map.sales_marketing_channel(
	marketing_channel		varchar(50), 
	marketing_channel_name	varchar(50), 
	sup_marketing_channel	varchar(50), 
	description				varchar(255),
	ga						char(1),
	bc						char(1),
	cc						char(1),
	ext						char(1),
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_marketing_channel add constraint [DF_map_sales_marketing_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_marketing_channel_aud
create table map.sales_marketing_channel_aud(
	marketing_channel		varchar(50), 
	marketing_channel_name	varchar(50), 
	sup_marketing_channel	varchar(50), 
	description				varchar(255),
	ga						char(1),
	bc						char(1),
	cc						char(1),
	ext						char(1),
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.sales_marketing_channel_aud add constraint [DF_map_sales_marketing_channel_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_affiliate
create table map.sales_affiliate(
	publisher_id		int, 
	affiliate_name		varchar(255), 
	affiliate_website	varchar(255), 
	affiliate_network	varchar(50), 
	affiliate_type		varchar(50), 
	affiliate_status	varchar(20),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_affiliate add constraint [DF_map_sales_affiliate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_affiliate_aud
create table map.sales_affiliate_aud(
	publisher_id		int, 
	affiliate_name		varchar(255), 
	affiliate_website	varchar(255), 
	affiliate_network	varchar(50), 
	affiliate_type		varchar(50), 
	affiliate_status	varchar(20),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_affiliate_aud add constraint [DF_map_sales_affiliate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_prescription_method
create table map.sales_prescription_method(
	prescription_method	varchar(50) NOT NULL, 
	description			varchar(255) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_prescription_method add constraint [DF_map_sales_prescription_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_prescription_method_aud
create table map.sales_prescription_method_aud(
	prescription_method	varchar(50) NOT NULL, 
	description			varchar(255) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_prescription_method_aud add constraint [DF_map_sales_prescription_method_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_rank_customer_order_seq_no
create table map.sales_rank_customer_order_seq_no(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_seq_no			char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_rank_customer_order_seq_no add constraint [DF_map_sales_rank_customer_order_seq_no_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_rank_customer_order_seq_no_aud
create table map.sales_rank_customer_order_seq_no_aud(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_seq_no			char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_rank_customer_order_seq_no_aud add constraint [DF_map_sales_rank_customer_order_seq_no_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.sales_rank_qty_time
create table map.sales_rank_qty_time(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_qty_time		char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_rank_qty_time add constraint [DF_map_sales_rank_qty_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_rank_qty_time_aud
create table map.sales_rank_qty_time_aud(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_qty_time		char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_rank_qty_time_aud add constraint [DF_map_sales_rank_qty_time_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.sales_rank_freq_time
create table map.sales_rank_freq_time(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_freq_time		char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_rank_freq_time add constraint [DF_map_sales_rank_freq_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_rank_freq_time_aud
create table map.sales_rank_freq_time_aud(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_freq_time		char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_rank_freq_time_aud add constraint [DF_map_sales_rank_freq_time_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_rank_shipping_days
create table map.sales_rank_shipping_days(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_shipping_days	char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_rank_shipping_days add constraint [DF_map_sales_rank_shipping_days_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_rank_shipping_days_aud
create table map.sales_rank_shipping_days_aud(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_shipping_days	char(2) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_rank_shipping_days_aud add constraint [DF_map_sales_rank_shipping_days_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.sales_summer_time_period
create table map.sales_summer_time_period(
	year				int NOT NULL, 
	from_date			datetime NOT NULL, 
	to_date				datetime NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.sales_summer_time_period add constraint [DF_map_sales_summer_time_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.sales_summer_time_period_aud
create table map.sales_summer_time_period_aud(
	year				int NOT NULL, 
	from_date			datetime NOT NULL, 
	to_date				datetime NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.sales_summer_time_period_aud add constraint [DF_map_sales_summer_time_period_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

