use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.gen_store_type
create table map.gen_store_type(
	store_name		varchar(255) NULL,
	tld				varchar(255) NULL,
	store_type		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_store_type add constraint [DF_map_gen_store_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_store_type_aud
create table map.gen_store_type_aud(
	store_name		varchar(255) NULL,
	tld				varchar(255) NULL,
	store_type		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_store_type_aud add constraint [DF_map_gen_store_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.gen_website_group
create table map.gen_website_group(
	store_name		varchar(255) NULL,
	name			varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_website_group add constraint [DF_map_gen_website_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_website_group_aud
create table map.gen_website_group_aud(
	store_name		varchar(255) NULL,
	name			varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_website_group_aud add constraint [DF_map_gen_website_group_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.gen_masterlens_store
create table map.gen_masterlens_store(
	store_name		varchar(255) NULL,
	store_type		varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_masterlens_store add constraint [DF_map_gen_masterlens_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_masterlens_store_aud
create table map.gen_masterlens_store_aud(
	store_name		varchar(255) NULL,
	store_type		varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_masterlens_store_aud add constraint [DF_map_gen_masterlens_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.gen_migrate_store
create table map.gen_migrate_store(
	store_id		int NOT NULL,
	store_name		varchar(255) NULL,
	store_type		varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_migrate_store add constraint [DF_map_gen_migrate_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_migrate_store_aud
create table map.gen_migrate_store_aud(
	store_id		int NOT NULL,
	store_name		varchar(255) NULL,
	store_type		varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_migrate_store_aud add constraint [DF_map_gen_migrate_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.gen_store_company
create table map.gen_store_company(
	company_name	varchar(255) NULL,
	store_name		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_store_company add constraint [DF_map_gen_store_company_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_masterlens_store_aud
create table map.gen_store_company_aud(
	company_name	varchar(255) NULL,
	store_name		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_store_company_aud add constraint [DF_map_gen_store_company_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.gen_time
create table map.gen_time(
	time_name		varchar(10) NOT NULL,
	hour_name		varchar(10) NOT NULL,
	hour			char(2) NOT NULL,
	minute			char(2) NOT NULL,
	day_part		varchar(20) NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_time add constraint [DF_map_gen_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_time_aud
create table map.gen_time_aud(
	time_name		varchar(10) NOT NULL,
	hour_name		varchar(10) NOT NULL,
	hour			char(2) NOT NULL,
	minute			char(2) NOT NULL,
	day_part		varchar(20) NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_time_aud add constraint [DF_map_gen_time_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.gen_countries
create table map.gen_countries(
	country_code		varchar(50) NOT NULL,
	country_name		varchar(50) NOT NULL,
	country_type		varchar(10) NOT NULL,
	country_zone		varchar(10) NOT NULL,
	country_continent	varchar(20) NOT NULL,
	country_state		varchar(10) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.gen_countries add constraint [DF_map_gen_countries_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_countries_aud
create table map.gen_countries_aud(
	country_code		varchar(50) NOT NULL,
	country_name		varchar(50) NOT NULL,
	country_type		varchar(10) NOT NULL,
	country_zone		varchar(10) NOT NULL,
	country_continent	varchar(20) NOT NULL,
	country_state		varchar(10) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.gen_countries_aud add constraint [DF_map_gen_countries_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.gen_customer_type
create table map.gen_customer_type(
	customer_type		varchar(50) NOT NULL,
	description			varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.gen_customer_type add constraint [DF_map_gen_customer_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_customer_type_aud
create table map.gen_customer_type_aud(
	customer_type		varchar(50) NOT NULL,
	description			varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.gen_customer_type_aud add constraint [DF_map_gen_customer_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.gen_customer_status
create table map.gen_customer_status(
	customer_status		varchar(50) NOT NULL,
	description			varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.gen_customer_status add constraint [DF_map_gen_customer_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_customer_status_aud
create table map.gen_customer_status_aud(
	customer_status		varchar(50) NOT NULL,
	description			varchar(255) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.gen_customer_status_aud add constraint [DF_map_gen_customer_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.gen_migrate_customer_store
create table map.gen_migrate_customer_store(
	migrate_store_code		varchar(50) NOT NULL,
	migrate_customer_store	varchar(50) NOT NULL,
	migrate_customer_f		char(1) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.gen_migrate_customer_store add constraint [DF_map_gen_migrate_customer_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_migrate_customer_store_aud
create table map.gen_migrate_customer_store_aud(
	migrate_store_code		varchar(50) NOT NULL,
	migrate_customer_store	varchar(50) NOT NULL,
	migrate_customer_f		char(1) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.gen_migrate_customer_store_aud add constraint [DF_map_gen_migrate_customer_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.gen_market
create table map.gen_market(
	market_id				int NOT NULL, 
	market_name				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.gen_market add constraint [DF_map_gen_market_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_market_aud
create table map.gen_market_aud(
	market_id				int NOT NULL, 
	market_name				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.gen_market_aud add constraint [DF_map_gen_market_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.gen_store_market_rel
create table map.gen_store_market_rel(
	store_id				int, 
	store_name				varchar(255),
	country					char(2), 
	market_id				int NOT NULL, 
	market_name				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.gen_store_market_rel add constraint [DF_map_gen_store_market_rel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_store_market_rel_aud
create table map.gen_store_market_rel_aud(
	store_id				int, 
	store_name				varchar(255),
	country					char(2), 
	market_id				int NOT NULL, 
	market_name				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.gen_store_market_rel_aud add constraint [DF_map_gen_store_market_rel_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.gen_customer_unsubscribe
create table map.gen_customer_unsubscribe(
	customer_unsubscribe	varchar(10) NOT NULL,
	description				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.gen_customer_unsubscribe add constraint [DF_map_gen_customer_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.gen_customer_unsubscribe_aud
create table map.gen_customer_unsubscribe_aud(
	customer_unsubscribe	varchar(10) NOT NULL,
	description				varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.gen_customer_unsubscribe_aud add constraint [DF_map_gen_customer_unsubscribe_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

