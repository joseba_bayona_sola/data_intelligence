use Landing
go

-- mag_eav_attributes
select entity_type_code, attribute_code, backend_type, 
	idETLBatchRun, ins_ts
from Landing.map.mag_eav_attributes

select entity_type_code, attribute_code, backend_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.mag_eav_attributes_aud

