use Landing
go 

------------------------------------------------------------------------
----------------------- warehouseshipments$receipt --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receipt_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.whship_receipt_aud

	select idETLBatchRun, count(*) num_rows
	from mend.whship_receipt_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.whship_receipt_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
			status, shipmentType, 
			dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
			totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
			lock,
			carriagefix, intercofix,
			auditComment,
			supplieradviceref,
			createdDate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.whship_receipt_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

----------------------------------------------------------------------------------
----------------------- warehouseshipments$receipt_warehouse --------------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receipt_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------
----------------------- warehouseshipments$receipt_supplier ---------------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receipt_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------
----------------------- inventory$warehousestockitembatch_receipt ---------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_warehousestockitembatch_receipt_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------
----------------------- inventory$warehousestockitembatch_receipthistory ---------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_warehousestockitembatch_receipthistory_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------------------------------------------------------------------------
----------------------- warehouseshipments$backtobackreceipt_purchaseorder ---------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_backtobackreceipt_purchaseorder_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptforcancelleditems ---------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptforcancelleditems_aud
	group by idETLBatchRun
	order by idETLBatchRun







----------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptline ---------------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.whship_receiptline_aud

	select idETLBatchRun, count(*) num_rows
	from mend.whship_receiptline_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.whship_receiptline_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			line, stockItem,
			status, syncStatus, syncResolution, created, processed,
			quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
			unitCost, 
			auditComment, 
			createdDate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.whship_receiptline_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-------------------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptline_shipment ---------------------
-------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptline_shipment_aud
	group by idETLBatchRun
	order by idETLBatchRun

-------------------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptline_supplier ---------------------
-------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptline_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptline_purchaseorderline ---------------------
----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptline_purchaseorderline_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------------------
----------------------- warehouseshipments$receiptline_snapreceiptline -----------------------
----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_receiptline_snapreceiptline_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------------------
----------------------- inventory$warehousestockitembatch_receiptline ------------------------
----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.whship_warehousestockitembatch_receiptline_aud
	group by idETLBatchRun
	order by idETLBatchRun