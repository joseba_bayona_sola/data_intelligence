
------------------------ Profile Scripts ----------------------------

use Landing
go

------------------- calendar$shippingmethod -------------------

select *
from mend.cal_shippingmethod

select id, 
	magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
	leadtime, letterRate1, letterRate2, tracked, invoicedocs,
	defaultweight, minweight, scurrimappable,
	mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
	mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime,
	idETLBatchRun, ins_ts
from mend.cal_shippingmethod

select id, 
	magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
	leadtime, letterRate1, letterRate2, tracked, invoicedocs,
	defaultweight, minweight, scurrimappable,
	mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
	mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_shippingmethod_aud

select id, 
	magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
	leadtime, letterRate1, letterRate2, tracked, invoicedocs,
	defaultweight, minweight, scurrimappable,
	mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
	mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.cal_shippingmethod_aud_hist

------------------- calendar$shippingmethods_warehouse -------------------

select *
from mend.cal_shippingmethods_warehouse

select shippingmethodid, warehouseid,
	idETLBatchRun, ins_ts
from mend.cal_shippingmethods_warehouse

select shippingmethodid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_shippingmethods_warehouse_aud

------------------- calendar$shippingmethods_destinationcountry -------------------

select *
from mend.cal_shippingmethods_destinationcountry

select shippingmethodid, countryid,
	idETLBatchRun, ins_ts
from mend.cal_shippingmethods_destinationcountry

select shippingmethodid, countryid,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_shippingmethods_destinationcountry_aud





------------------- calendar$defaultcalendar -------------------

select *
from mend.cal_defaultcalendar

select id, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts
from mend.cal_defaultcalendar

select id, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_defaultcalendar_aud

select id, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.cal_defaultcalendar_aud_hist

------------------- calendar$defaultcalendar_warehouse -------------------

select *
from mend.cal_defaultcalendar_warehouse

select defaultcalendarid, warehouseid,
	idETLBatchRun, ins_ts
from mend.cal_defaultcalendar_warehouse

select defaultcalendarid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_defaultcalendar_warehouse_aud



------------------- calendar$defaultwarehousesuppliercalender -------------------

select *
from mend.cal_defaultwarehousesuppliercalender

select id, 
	mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
	mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
	mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts
from mend.cal_defaultwarehousesuppliercalender

select id, 
	mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
	mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
	mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_defaultwarehousesuppliercalender_aud

select id, 
	mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
	mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
	mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.cal_defaultwarehousesuppliercalender_aud_hist

------------------- calendar$cal_defaultwarehousesuppliercalender_warehouse -------------------

select *
from mend.cal_defaultwarehousesuppliercalender_warehouse

select defaultwarehousesuppliercalenderid, warehouseid,
	idETLBatchRun, ins_ts
from mend.cal_defaultwarehousesuppliercalender_warehouse

select defaultwarehousesuppliercalenderid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.cal_defaultwarehousesuppliercalender_warehouse_aud

