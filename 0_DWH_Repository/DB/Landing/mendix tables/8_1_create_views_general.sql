
Use Landing
go 

--------------- WAREHOUSE RELATED -----------------------------

drop view mend.gen_wh_warehouse_v 
go 

create view mend.gen_wh_warehouse_v as
	select id warehouseid, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore
	from mend.wh_warehouse_aud
go




drop view mend.gen_wh_warehouse_calendar_v 
go 

create view mend.gen_wh_warehouse_calendar_v  as
	select w.warehouseid, c.id defaultcalendarid, 
		w.code, w.name, w.warehouseType, 
		c.mondayWorking, convert(varchar(8), c.mondayCutOff, 108) mondayCutOff, 
		c.tuesdayWorking, convert(varchar(8), c.tuesdayCutOff, 108) tuesdayCutOff, 
		c.wednesdayWorking, convert(varchar(8), c.wednesdayCutOff, 108) wednesdayCutOff, 
		c.thursdayWorking, convert(varchar(8), c.thursdayCutOff, 108) thursdayCutOff, 
		c.fridayWorking, convert(varchar(8), c.fridayCutOff, 108) fridayCutOff, 
		c.saturdayWorking, convert(varchar(8), c.saturdayCutOff, 108) saturdayCutOff, 
		c.sundayWorking, convert(varchar(8), c.sundayCutOff, 108) sundayCutOff, 
		c.timeHorizonWeeks
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.cal_defaultcalendar_warehouse_aud cw on w.warehouseid = cw.warehouseid
		inner join
			Landing.mend.cal_defaultcalendar_aud c on cw.defaultcalendarid = c.id
go


drop view mend.gen_wh_warehouse_shippingmethod_v 
go 

create view mend.gen_wh_warehouse_shippingmethod_v as
	select w.warehouseid, sm.id shippingmethodid, 
		w.code, w.name, w.warehouseType, 
		sm.magentoShippingDescription, sm.magentoShippingMethod, sm.scurriShippingMethod, sm.snapShippingMethod, 
		sm.leadtime, sm.letterRate1, sm.letterRate2, sm.tracked, sm.invoicedocs,
		sm.defaultweight, sm.minweight, sm.scurrimappable,
		convert(varchar(8), sm.mondayDefaultCutOff, 108) mondayDefaultCutOff, sm.mondayWorking, sm.mondayDeliveryWorking, convert(varchar(8), sm.mondayCollectionTime, 108) mondayCollectionTime, 
		convert(varchar(8), sm.tuesdayDefaultCutOff, 108) tuesdayDefaultCutOff, sm.tuesdayWorking, sm.tuesdayDeliveryWorking, convert(varchar(8), sm.tuesdayCollectionTime, 108) tuesdayCollectionTime,
		convert(varchar(8), sm.wednesdayDefaultCutOff, 108) wednesdayDefaultCutOff, sm.wednesdayWorking, sm.wednesdayDeliveryWorking, convert(varchar(8), sm.wednesdayCollectionTime, 108) wednesdayCollectionTime,
		convert(varchar(8), sm.thursdayDefaultCutOff, 108) thursdayDefaultCutOff, sm.thursdayWorking, sm.thursdayDeliveryWorking, convert(varchar(8), sm.thursdayCollectionTime, 108) thursdayCollectionTime,
		convert(varchar(8), sm.fridayDefaultCutOff, 108) fridayDefaultCutOff, sm.fridayWorking, sm.fridayDeliveryWorking, convert(varchar(8), sm.mondayCollectionTime, 108) fridayCollectionTime,
		convert(varchar(8), sm.saturdayDefaultCutOff, 108) saturdayDefaultCutOff, sm.saturdayWorking, sm.saturdayDeliveryWorking, convert(varchar(8), sm.saturdayCollectionTime, 108) saturdayCollectionTime,
		convert(varchar(8), sm.sundayDefaultCutOff, 108) sundayDefaultCutOff, sm.sundayWorking, sm.sundayDeliveryWorking, convert(varchar(8), sm.sundayCollectionTime, 108) sundayCollectionTime
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.cal_shippingmethods_warehouse_aud smw on w.warehouseid = smw.warehouseid
		inner join
			Landing.mend.cal_shippingmethod_aud sm on smw.shippingmethodid = sm.id
go

drop view mend.gen_wh_warehouse_supplier_v 
go 

create view mend.gen_wh_warehouse_supplier_v as
	select w.warehouseid, sr.id supplierroutineid, 
		w.code, w.name, w.warehouseType, 
		sr.supplierName, 
		sr.mondayDefaultCanSupply, convert(varchar(8), sr.mondayOrderCutOffTime, 108) mondayOrderCutOffTime, sr.mondayDefaultCanOrder, 
		sr.tuesdayDefaultCanSupply, convert(varchar(8), sr.tuesdayOrderCutOffTime, 108) tuesdayOrderCutOffTime, sr.tuesdayDefaultCanOrder, 
		sr.wednesdayDefaultCanSupply, convert(varchar(8), sr.wednesdayOrderCutOffTime, 108) wednesdayOrderCutOffTime, sr.wednesdayDefaultCanOrder, 
		sr.thursdayDefaultCanSupply, convert(varchar(8), sr.thursdayOrderCutOffTime, 108) thursdayOrderCutOffTime, sr.thursdayDefaultCanOrder, 
		sr.fridayDefaultCanSupply, convert(varchar(8), sr.fridayOrderCutOffTime, 108) fridayOrderCutOffTime, sr.fridayDefaultCanOrder, 
		sr.saturdayDefaultCanSupply, convert(varchar(8), sr.saturdayOrderCutOffTime, 108) saturdayOrderCutOffTime, sr.saturdayDefaultCanOrder, 
		sr.sundayDefaultCanSupply, convert(varchar(8), sr.sundayOrderCutOffTime, 108) sundayOrderCutOffTime, sr.sundayDefaultCanOrder
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.wh_warehousesupplier_warehouse wsw on w.warehouseid = wsw.warehouseid
		inner join
			Landing.mend.wh_supplierroutine_warehousesupplier_aud srw on wsw.warehousesupplierid = srw.warehousesupplierID
		inner join
			Landing.mend.wh_supplierroutine_aud sr on srw.supplierRoutineID = sr.id
go

--------------- SUPPLIER RELATED -----------------------------

drop view mend.gen_supp_supplier_v 
go 

create view mend.gen_supp_supplier_v as
	select id supplier_id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		defaultLeadTime,
		flatFileConfigurationToUse,		
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime
	from Landing.mend.supp_supplier_aud
go


drop view mend.gen_supp_supplierprice_v 
go 

create view mend.gen_supp_supplierprice_v as
	select s.supplier_id, sp.id supplierpriceid,
		s.supplierID, s.supplierName, s.currencyCode, s.supplierType, 
		sp.subMetaObjectName,
		sp.unitPrice, sp.leadTime, sp.totalWarehouseUnitPrice, sp.orderMultiple,
		sp.directPrice, sp.expiredPrice,
		sp.totallt, sp.shiplt, sp.transferlt,
		sp.comments,
		sp.lastUpdated,
		sp.createdDate, sp.changedDate, 
		stp.moq, stp.effectiveDate, stp.expiryDate, stp.maxqty
	from 
		Landing.mend.gen_supp_supplier_v s
	left join
		Landing.mend.supp_supplierprices_supplier_aud ssp on s.supplier_id = ssp.supplierid
	left join	
		Landing.mend.supp_supplierprice_aud sp on ssp.supplierpriceid = sp.id
	left join
		Landing.mend.supp_standardprice_aud stp on sp.id = stp.id
go



--------------- PRODUCT RELATED -----------------------------

-- productfamily ??

drop view mend.gen_prod_productfamilypacksize_v 
go 

create view mend.gen_prod_productfamilypacksize_v as
	select pspf.productfamilyid, pspf.packsizeid, 
		dense_rank() over (partition by pf.magentoProductID order by size, pspf.packsizeid) productfamilysizerank,
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pspf.productfamilyid end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name, pf.status,
		ps.description, ps.size, 
		ps.packagingNo, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, 
		ps.weight, ps.height, ps.width, ps.depth 
	from 
			Landing.mend.prod_productfamily_aud pf
		left join
			Landing.mend.prod_packsize_productfamily_aud pspf on pf.id = pspf.productfamilyid
		right join
			Landing.mend.prod_packsize_aud ps on pspf.packsizeid = ps.id
go


drop view mend.gen_prod_productfamilypacksize_supplierprice_v 
go 

create view mend.gen_prod_productfamilypacksize_supplierprice_v as

	select pfp.productfamilyid, pfp.packsizeid, sp.supplier_id, sp.supplierpriceid,
		pfp.magentoProductID_int, pfp.magentoSKU, pfp.name, pfp.status, pfp.productFamilySizeRank, pfp.size, 
		sp.supplierID, sp.supplierName, sp.subMetaObjectName, sp.unitPrice, sp.currencyCode, sp.leadTime
	from 
			Landing.mend.gen_prod_productfamilypacksize_v pfp
		left join	
			Landing.mend.supp_supplierprices_packsize_aud spp on pfp.packsizeid = spp.packsizeid
		left join
			Landing.mend.gen_supp_supplierprice_v sp on spp.supplierpriceid = sp.supplierpriceid
go


drop view mend.gen_prod_product_v 
go 

create view mend.gen_prod_product_v as
	select pf.id productfamilyid, p.id productid,
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pf.id end magentoProductID_int,
		pf.magentoSKU, pf.name, 
		p.valid, p.SKU SKU_product, p.oldSKU, p.description, 
		cl.bc, cl.di, cl.po, cl.cy, cl.ax,  cl.ad, cl._do, cl.co, cl.co_EDI
	from 
			Landing.mend.prod_productfamily_aud pf
		left join
			Landing.mend.prod_product_productfamily_aud pfp on pf.id = pfp.productfamilyid
		left join
			Landing.mend.prod_product_aud p on pfp.productid = p.id
		left join
			Landing.mend.prod_contactlens_aud cl on p.id = cl.id
go	

drop view mend.gen_prod_stockitem_v 
go 

create view mend.gen_prod_stockitem_v as
	select p.productfamilyid, p.productid, si.id stockitemid, 
		p.magentoProductID_int,
		p.magentoSKU, p.name, 
		p.valid, p.SKU_product, p.oldSKU, p.description, 
		si.packSize, si.SKU, -- si.oldSKU, 
		p.bc, p.di, p.po, p.cy, p.ax, p.ad, p._do, p.co, p.co_EDI, 
		si.manufacturerArticleID, si.manufacturerCodeNumber,
		si.SNAPUploadStatus
	from 
			Landing.mend.gen_prod_product_v p
		left join
			Landing.mend.prod_stockitem_product_aud sip on p.productid = sip.productid
		left join
			Landing.mend.prod_stockitem_aud si on sip.stockitemid = si.id
go


--------------- STOCK RELATED -----------------------------

drop view mend.gen_wh_warehousestockitem_v 
go 

create view mend.gen_wh_warehousestockitem_v as
	select si.productfamilyid, si.productid, si.stockitemid, wsi.id warehousestockitemid,
		si.magentoProductID_int,
		si.magentoSKU, si.name, si.SKU_product, si.packSize, si.SKU, 
		si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, si.co_EDI, 
		w.code, w.name warehouse_name, wsi.id_string, 
		wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		wsi.outstandingallocation, wsi.onhold,
		wsi.stockingmethod
	from 
			Landing.mend.gen_prod_stockitem_v si
		left join
			Landing.mend.wh_warehousestockitem_stockitem_aud wsisi on si.stockitemid = wsisi.stockitemid 	
		left join
			Landing.mend.wh_warehousestockitem_aud wsi on wsisi.warehousestockitemid = wsi.id
		left join
			Landing.mend.wh_located_at_aud wsiw on wsi.id = wsiw.warehousestockitemid
		left join
			Landing.mend.gen_wh_warehouse_v w on wsiw.warehouseid = w.warehouseid
go


drop view mend.gen_wh_warehousestockitembatch_v 
go 

create view mend.gen_wh_warehousestockitembatch_v as
	select wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, wsib.id warehousestockitembatchid,
		wsi.magentoProductID_int,
		wsi.magentoSKU, wsi.name, wsi.SKU_product, wsi.packSize, wsi.SKU, 
		wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.co_EDI, 
		wsi.code, wsi.warehouse_name, wsi.id_string, 
		wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		wsi.outstandingallocation, wsi.onhold,
		wsi.stockingmethod, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.autoadjusted, wsib.arriveddate,  
		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, -- wsib.forwardDemand, 
		wsib.confirmedDate, 
		wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, wsib.currency
	from 
			Landing.mend.gen_wh_warehousestockitem_v wsi
		left join
			Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsiwsib on wsi.warehousestockitemid = wsiwsib.warehousestockitemid
		left join
			Landing.mend.wh_warehousestockitembatch_aud wsib on wsiwsib.warehousestockitembatchid = wsib.id
go


drop view mend.gen_wh_warehousestockitembatch_issue_v 
go 

create view mend.gen_wh_warehousestockitembatch_issue_v as
	select wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, bsi.id batchstockissueid,
		wsib.magentoProductID_int,
		wsib.magentoSKU, wsib.name, wsib.SKU_product, wsib.packSize, wsib.SKU, 
		wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
		wsib.outstandingallocation, wsib.onhold,
		wsib.stockingmethod, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
		wsib.confirmedDate, 
		wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency, 
		bsi.issueid, bsi.createddate, bsi.issuedquantity issuedquantity_issue 
	from 
			Landing.mend.gen_wh_warehousestockitembatch_v wsib
		left join
			Landing.mend.wh_batchstockissue_warehousestockitembatch_aud bsiwsib on wsib.warehousestockitembatchid = bsiwsib.warehousestockitembatchid
		left join
			Landing.mend.wh_batchstockissue_aud bsi on bsiwsib.batchstockissueid = bsi.id
go

drop view mend.gen_wh_warehousestockitembatch_issue_alloc_v 
go 

create view mend.gen_wh_warehousestockitembatch_issue_alloc_v as
	select wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, wsibi.batchstockissueid, bsa.id batchstockallocationid,
		wsibi.magentoProductID_int,
		wsibi.magentoSKU, wsibi.name, wsibi.SKU_product, wsibi.packSize, wsibi.SKU, 
		wsibi.bc, wsibi.di, wsibi.po, wsibi.cy, wsibi.ax, wsibi.ad, wsibi._do, wsibi.co, wsibi.co_EDI, 
		wsibi.code, wsibi.warehouse_name, wsibi.id_string, 
		wsibi.availableqty, wsibi.actualstockqty, wsibi.allocatedstockqty, wsibi.forwarddemand, wsibi.dueinqty, wsibi.stocked, 
		wsibi.outstandingallocation, wsibi.onhold,
		wsibi.stockingmethod, 
		wsibi.batch_id,
		wsibi.fullyallocated, wsibi.fullyIssued, wsibi.status, wsibi.arriveddate,  
		wsibi.receivedquantity, wsibi.allocatedquantity, wsibi.issuedquantity, wsibi.onHoldQuantity, wsibi.disposedQuantity, 
		wsibi.confirmedDate, 
		wsibi.productUnitCost, wsibi.totalUnitCost, wsibi.exchangeRate, wsibi.currency, 
		wsibi.issueid, wsibi.createddate, wsibi.issuedquantity_issue, 
		bsa.fullyissued fullyissued_alloc, bsa.cancelled, bsa.allocatedquantity allocatedquantity_alloc, bsa.allocatedunits, bsa.issuedquantity issuedquantity_alloc 
	from 
			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
		full join
			Landing.mend.wh_batchstockissue_batchstockallocation_aud bsibsa on wsibi.batchstockissueid = bsibsa.batchstockissueid 
		full join
			Landing.mend.wh_batchstockallocation_aud bsa on bsibsa.batchstockallocationid = bsa.id
go


drop view mend.gen_wh_warehousestockitembatch_repair_v 
go 

create view mend.gen_wh_warehousestockitembatch_repair_v as
	select rsb.id repairstockbatchsid,
		wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, 
		rsb.batchtransaction_id, rsb.reference, rsb.date,
		rsb.oldallocation, rsb.newallocation, rsb.oldissue, rsb.newissue, rsb.processed, 
		wsib.magentoProductID_int,
		wsib.magentoSKU, wsib.name, wsib.SKU_product, wsib.packSize, wsib.SKU, 
		wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
		wsib.outstandingallocation, wsib.onhold,
		wsib.stockingmethod, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
		wsib.confirmedDate, 
		wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency
	from 
			Landing.mend.wh_repairstockbatchs_aud rsb 
		inner join
			Landing.mend.wh_repairstockbatchs_warehousestockitembatch_aud rsbwsib on rsb.id = rsbwsib.repairstockbatchsid 
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on rsbwsib.warehousestockitembatchid = wsib.warehousestockitembatchid 
go


drop view mend.gen_wh_stockmovement_v 
go 

create view mend.gen_wh_stockmovement_v as

	select sm.id stockmovementid, wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, 
		sm.moveid, sm.movelineid, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.dateCreated, sm.dateClosed,
		wsi.magentoProductID_int,
		wsi.magentoSKU, wsi.name, wsi.SKU_product, wsi.packSize, wsi.SKU, 
		wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.co_EDI, 
		wsi.code, wsi.warehouse_name, wsi.id_string, 
		wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		wsi.outstandingallocation, wsi.onhold,
		wsi.stockingmethod
	from 
			Landing.mend.wh_stockmovement_aud sm 
		left join
			Landing.mend.wh_stockmovement_warehousestockitem_aud smwsi on sm.id = smwsi.stockmovementid 
		left join
			Landing.mend.gen_wh_warehousestockitem_v wsi on smwsi.warehousestockitemid = wsi.warehousestockitemid 
go


drop view mend.gen_wh_batchstockmovement_v 
go 

create view mend.gen_wh_batchstockmovement_v as

	select bsm.id batchstockmovementid, sm.stockmovementid, sm.productfamilyid, sm.productid, sm.stockitemid, sm.warehousestockitemid, wsib.warehousestockitembatchid,
		sm.moveid, sm.movelineid, bsm.batchstockmovement_id, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.dateCreated, sm.dateClosed,
		bsm.quantity, bsm.comment,
		sm.magentoProductID_int,
		sm.magentoSKU, sm.name, sm.SKU_product, sm.packSize, sm.SKU, 
		sm.bc, sm.di, sm.po, sm.cy, sm.ax, sm.ad, sm._do, sm.co, sm.co_EDI, 
		sm.code, sm.warehouse_name, sm.id_string, 
		sm.availableqty, sm.actualstockqty, sm.allocatedstockqty, sm.forwarddemand, sm.dueinqty, sm.stocked, 
		sm.outstandingallocation, sm.onhold,
		sm.stockingmethod, 
		wsib.batch_id
	from 
			Landing.mend.gen_wh_stockmovement_v sm
		full join
			Landing.mend.wh_batchstockmovement_stockmovement_aud bsmsm on sm.stockmovementid = bsmsm.stockmovementid
		full join
			Landing.mend.wh_batchstockmovement_aud bsm on bsmsm.batchstockmovementid = bsm.id
		left join
			Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud bsmwsi on bsm.id = bsmwsi.batchstockmovementid
		left join	
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsmwsi.warehousestockitembatchid = wsib.warehousestockitembatchid
go



--------------- ORDER RELATED -----------------------------

drop view mend.gen_order_order_v 
go 

create view mend.gen_order_order_v as
	select o.id orderid, bv.id basevaluesid, oc.retailcustomerid, os.magwebstoreid,
		case when (isnumeric(o.magentoOrderID) = 1) then convert(bigint, o.magentoOrderID) else o.id end magentoOrderID_int, 
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		o._type, o.shipmentStatus, 
		o.allocationStatus, o.allocationStrategy, o.allocatedDate, 
		o.labelRenderer,  
		o.shippingMethod, o.shippingDescription, 
		bv.subtotal, bv.subtotalInclTax, bv.subtotalInvoiced, bv.subtotalCanceled, bv.subtotalRefunded, 
		bv.discountAmount, bv.discountInvoiced, bv.discountCanceled, bv.discountRefunded, 
		bv.shippingAmount, bv.shippingInvoiced, bv.shippingCanceled, bv.shippingRefunded, 					 
		bv.adjustmentNegative, bv.adjustmentPositive, 					
		bv.custBalanceAmount, 
		bv.customerBalanceAmount, bv.customerBalanceInvoiced, bv.customerBalanceRefunded, bv.customerBalanceTotRefunded, bv.customerBalanceTotalRefunded, 
		bv.grandTotal, bv.totalInvoiced, bv.totalCanceled, bv.totalRefunded,
		bv.toOrderRate, bv.toGlobalRate, bv.currencyCode
	from	
			Landing.mend.order_order_aud o
		inner join
			Landing.mend.order_basevalues_order_aud obv on o.id = obv.orderid
		inner join
			Landing.mend.order_basevalues_aud bv on obv.basevaluesid = bv.id
		inner join
			Landing.mend.order_order_customer_aud oc on o.id = oc.orderid
		inner join
			Landing.mend.order_order_magwebstore_aud os on o.id = os.orderid
go

drop view mend.gen_order_orderline_v 
go 

create view mend.gen_order_orderline_v as
	select o.orderid, o.basevaluesid, o.retailcustomerid, o.magwebstoreid, ol.id orderlineid, olp.productid,
		o.magentoOrderID_int, 
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped,
		p.magentoProductID_int, p.name, ola.sku, -- param ??
		ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityIssued, ola.quantityAllocated, ola.quantityShipped, 
		ola.basePrice, ola.baseRowPrice, 
		ola.allocated, 
		ola.orderLineType, ola.subMetaObjectName
	from 
			Landing.mend.gen_order_order_v o
		inner join
			Landing.mend.order_orderline_order_aud olo on o.orderid = olo.orderid
		inner join
			Landing.mend.order_orderline_aud ol on olo.orderlineid = ol.id
		inner join
			Landing.mend.order_orderlineabstract_aud ola on ol.id = ola.id -- Needed? If making the query slow
		inner join
			Landing.mend.order_orderline_product_aud olp on ol.id = olp.orderlineabstractid
		inner join
			Landing.mend.gen_prod_product_v p on olp.productid = p.productid
go

drop view mend.gen_order_orderlinemagento_v 
go 

create view mend.gen_order_orderlinemagento_v as
	select o.orderid, o.basevaluesid, o.retailcustomerid, o.magwebstoreid, olm.id orderlinemagentoid, olp.productid,
		o.magentoOrderID_int, 
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		olm.lineAdded, olm.deduplicate, olm.fulfilled,
		p.magentoProductID_int, p.name, ola.sku, -- param ??
		ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityIssued, ola.quantityAllocated, ola.quantityShipped, 
		ola.basePrice, ola.baseRowPrice, 
		ola.allocated, 
		ola.orderLineType, ola.subMetaObjectName
	from 
			Landing.mend.gen_order_order_v o
		inner join
			Landing.mend.order_orderlinemagento_order_aud olmo on o.orderid = olmo.orderid
		inner join
			Landing.mend.order_orderlinemagento_aud olm on olmo.orderlinemagentoid = olm.id
		inner join
			Landing.mend.order_orderlineabstract_aud ola on olm.id = ola.id -- Needed? If making the query slow
		inner join
			Landing.mend.order_orderline_product_aud olp on olm.id = olp.orderlineabstractid
		inner join
			Landing.mend.gen_prod_product_v p on olp.productid = p.productid
go

--------------- SHIPMENT RELATED -----------------------------

drop view mend.gen_ship_customershipment_v 
go 

create view mend.gen_ship_customershipment_v as
	select s.id customershipmentid, o.orderid, 
		o.magentoOrderID_int, o.incrementID, o.createdDate createdDate_order, o.orderStatus,
		s.orderIncrementID, s.shipmentNumber, 
		s.status, s.warehouseMethod, s.labelRenderer, 
		s.shippingMethod, s.shippingDescription, 
		s.notify, s.fullyShipped, s.syncedToMagento, 	
		s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate,  
		lcu.timestamp snap_timestamp,
		s.createdDate, s.changedDate
	from 
			Landing.mend.ship_customershipment_aud s
		inner join
			Landing.mend.ship_customershipment_order_aud so on s.id = so.customershipmentid
		inner join
			Landing.mend.gen_order_order_v o on so.orderid = o.orderid
		left join
			Landing.mend.ship_customershipment_progress_aud lcus on s.id = lcus.customershipmentid
		left join
			Landing.mend.ship_lifecycle_update_aud lcu on lcus.lifecycle_updateid = lcu.id
go

--------------- ALLOCATION RELATED -----------------------------

drop view mend.gen_all_stockallocationtransaction_v 
go 

create view mend.gen_all_stockallocationtransaction_v as
	select sat.id orderlinestockallocationtransactionid, btsat.batchstockallocationid,
		satw.warehouseid, sato.orderid, satol.orderlineid, satwsi.warehousestockitemid, satwsib.warehousestockitembatchid, satps.packsizeid, 
		o.magentoOrderID_int, o.incrementID, w.name wh_name,
		sat.transactionID, sat.timestamp,
		sat.allocationType, sat.recordType, 
		sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
		sat.stockAllocated, sat.cancelled, 
		sat.issuedDateTime
	from 
			Landing.mend.all_orderlinestockallocationtransaction_aud sat
		left join
			Landing.mend.all_at_warehouse_aud satw on sat.id = satw.orderlinestockallocationtransactionid
		left join
			Landing.mend.gen_wh_warehouse_v w on satw.warehouseid = w.warehouseid
		inner join
			Landing.mend.all_at_order_aud sato on sat.id = sato.orderlinestockallocationtransactionid
		inner join
			Landing.mend.gen_order_order_v o on sato.orderid = o.orderid
		inner join
			Landing.mend.all_at_orderline_aud satol on sat.id = satol.orderlinestockallocationtransactionid
		inner join
			Landing.mend.all_at_warehousestockitem_aud satwsi on sat.id = satwsi.orderlinestockallocationtransactionid
		inner join
			Landing.mend.all_at_batch_aud satwsib on sat.id = satwsib.orderlinestockallocationtransactionid
		left join
			Landing.mend.all_at_packsize_aud satps on sat.id = satps.orderlinestockallocationtransactionid
		left join
			Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction_aud btsat on sat.id = btsat.orderlinestockallocationtransactionid
go

drop view mend.gen_all_magentoallocation_v 
go 

create view mend.gen_all_magentoallocation_v as
	select ma.id magentoallocationid, sat.orderlinestockallocationtransactionid, sat.batchstockallocationid,
		ma.shipped, ma.cancelled cancelled_magentoall, 
		ma.quantity,
		sat.warehouseid, sat.orderid, sat.orderlineid, sat.warehousestockitemid, sat.warehousestockitembatchid, sat.packsizeid, -- sat.customershipmentlineid, 
		sat.magentoOrderID_int, sat.incrementID, sat.wh_name,
		sat.transactionID, sat.timestamp,
		sat.allocationType, sat.recordType, 
		sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
		sat.stockAllocated, sat.cancelled, 
		sat.issuedDateTime
	from 
			Landing.mend.gen_all_stockallocationtransaction_v sat
		full join
			Landing.mend.all_magentoallocatio_orderlinestockallocationtransac_aud masat on sat.orderlinestockallocationtransactionid = masat.orderlinestockallocationtransactionid 
		full join
			Landing.mend.all_magentoallocation_aud ma on masat.magentoallocationid = ma.id
go

--------------- PURCHASE ORDERS RELATED -----------------------------

drop view mend.gen_purc_purchaseorder_v 
go 

create view mend.gen_purc_purchaseorder_v  as
	select po.id purchaseorderid, w.warehouseid, s.supplier_id, 
		w.code, w.name warehouse_name, 
		s.supplierID, s.supplierName,
		po.ordernumber, po.source, po.status, 
		po.leadtime, 
		po.itemcost, po.totalprice, po.formattedprice, 
		po.duedate, po.receiveddate, po.createddate, 
		po.approveddate, po.approvedby, po.confirmeddate, po.confirmedby, po.submitteddate, po.submittedby, po.completeddate, po.completedby
	from 
			mend.purc_purchaseorder_aud po
		inner join
			mend.purc_purchaseorder_warehouse_aud pow on po.id = pow.purchaseorderid
		inner join	
			Landing.mend.gen_wh_warehouse_v w on pow.warehouseid = w.warehouseid
		left join
			Landing.mend.purc_purchaseorder_supplier pos on po.id = pos.purchaseorderid
			-- Landing.mend.purc_purchaseorder_supplier_aud pos on po.id = pos.purchaseorderid
		left join
			Landing.mend.gen_supp_supplier_v s on pos.supplierid = s.supplier_id
go


drop view mend.gen_purc_purchaseorderlineheader_v 
go 

create view mend.gen_purc_purchaseorderlineheader_v  as
	select id purchaseorderlineheaderid, po.purchaseorderid, po.warehouseid, po.supplier_id, polhpf.productfamilyid, polhps.packsizeid, polhsp.supplierpriceid,
		po.code, po.warehouse_name, 
		po.supplierID, po.supplierName,
		po.ordernumber, po.source, po.status, po.createddate,
		po.leadtime, 
		po.itemcost, po.totalprice, 
		polh.status status_lh, polh.problems, 
		polh.lines, polh.items, polh.totalprice total_price_lh
	from 
			Landing.mend.gen_purc_purchaseorder_v po 
		left join
			Landing.mend.purc_purchaseorderlineheader_purchaseorder_aud polhpo on po.purchaseorderid = polhpo.purchaseorderid
		left join
			Landing.mend.purc_purchaseorderlineheader_aud polh on polhpo.purchaseorderlineheaderid = polh.id 
		left join
			 Landing.mend.purc_purchaseorderlineheader_productfamily_aud polhpf on polh.id = polhpf.purchaseorderlineheaderid
		left join
			 Landing.mend.purc_purchaseorderlineheader_packsize_aud polhps on polh.id = polhps.purchaseorderlineheaderid
		left join	 
			 Landing.mend.purc_purchaseorderlineheader_supplierprices_aud polhsp on polh.id = polhsp.purchaseorderlineheaderid
go


drop view mend.gen_purc_purchaseorderline_v 
go 

create view mend.gen_purc_purchaseorderline_v  as
	select id purchaseorderlineid, polh.purchaseorderlineheaderid, polh.purchaseorderid, polh.warehouseid, polh.supplier_id, polh.productfamilyid, polh.packsizeid, polh.supplierpriceid, polsi.stockitemid,
		polh.code, polh.warehouse_name, 
		polh.supplierID, polh.supplierName,
		polh.ordernumber, polh.source, polh.status, polh.createddate,
		polh.leadtime, 
		polh.itemcost, polh.totalprice, 
		polh.status_lh, polh.problems, 
		polh.lines, polh.items, polh.total_price_lh,
		pol.po_lineID, 
		pol.quantity, pol.quantityReceived, pol.quantityDelivered, pol.quantityInTransit, pol.quantityPlanned, pol.quantityOpen, pol.quantityUnallocated, pol.quantityCancelled, pol.quantityExWorks, pol.quantityRejected,
		pol.lineprice, 
		pol.backtobackduedate
	from 
			Landing.mend.gen_purc_purchaseorderlineheader_v polh 
		full join
			Landing.mend.purc_purchaseorderline_purchaseorderlineheader polpolh on polh.purchaseorderlineheaderid = polpolh.purchaseorderlineheaderid 
		full join	
			Landing.mend.purc_purchaseorderline_aud pol on polpolh.purchaseorderlineid = pol.id 
		left join
			Landing.mend.purc_purchaseorderline_stockitem_aud polsi on pol.id = polsi.purchaseorderlineid
go


--------------- SHORTAGE RELATED -----------------------------

drop view mend.gen_short_shortageheader_v 
go 

create view mend.gen_short_shortageheader_v  as
	select shh.id shortageheaderid, s.supplier_id, shhws.warehouseid warehouseid_src, shhwd.warehouseid warehouse_dest, shhps.packsizeid,
		shh.wholesale, 
		ws.code code_src, ws.name warehouse_name_src, wd.code code_dest, wd.name warehouse_name_dest, s.supplierID, s.supplierName
	from 
			Landing.mend.short_shortageheader_aud shh
		inner join
			Landing.mend.short_shortageheader_sourcewarehouse shhws on shh.id = shhws.shortageheaderid
		inner join
			Landing.mend.gen_wh_warehouse_v ws on shhws.warehouseid = ws.warehouseid
		left join
			Landing.mend.short_shortageheader_destinationwarehouse shhwd on shh.id = shhwd.shortageheaderid
		left join
			Landing.mend.gen_wh_warehouse_v wd on shhwd.warehouseid = wd.warehouseid
		inner join
			Landing.mend.short_shortageheader_supplier shhs on shh.id = shhs.shortageheaderid
		inner join
			Landing.mend.gen_supp_supplier_v s on shhs.supplierid = s.supplier_id
		left join
			Landing.mend.short_shortageheader_packsize shhps on shh.id = shhps.shortageheaderid
go

drop view mend.gen_short_shortage_v 
go 

create view mend.gen_short_shortage_v  as			
	select sh.id shortage_id, shh.shortageheaderid, shh.supplier_id, shh.warehouseid_src, shh.warehouse_dest, shh.packsizeid, 
		shpol.purchaseorderlineid, shp.productid, shsi.stockitemid, shwsi.warehousestockitemid, -- shsp.standardpriceid, 
		shh.wholesale, 
		shh.code_src, shh.warehouse_name_src, shh.code_dest, shh.warehouse_name_dest, shh.supplierID, shh.supplierName,
		sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
		-- sh.wholesale, 
		sh.status, 
		sh.unitquantity, sh.packquantity, 
		sh.requireddate, 
		sh.createdDate
	from 
			Landing.mend.gen_short_shortageheader_v shh 
		left join
			Landing.mend.short_shortage_shortageheader_aud shshh on shh.shortageheaderid = shshh.shortageheaderid
		left join
			Landing.mend.short_shortage_aud sh on shshh.shortageid = sh.id 
		left join
			Landing.mend.short_shortage_purchaseorderline_aud shpol on sh.id = shpol.shortageid
		left join
			Landing.mend.short_shortage_product_aud shp on sh.id = shp.shortageid
		left join	
			Landing.mend.short_shortage_stockitem_aud shsi on sh.id = shsi.shortageid
		left join	
			Landing.mend.short_shortage_warehousestockitem_aud shwsi on sh.id = shwsi.shortageid
		-- left join	
			-- Landing.mend.short_shortage_standardprice_aud shsp on sh.id = shsp.shortageid
go
	

drop view mend.gen_short_orderlineshortage_v 
go 

create view mend.gen_short_orderlineshortage_v  as		

	select olsh.id orderlineshortageid, sh.shortage_id, sh.shortageheaderid, sh.supplier_id, sh.warehouseid_src, sh.warehouse_dest, sh.packsizeid,
		sh.purchaseorderlineid, sh.productid, sh.stockitemid, sh.warehousestockitemid, olshol.orderlineid, -- sh.standardpriceid, 
		sh.wholesale, 
		sh.code_src, sh.warehouse_name_src, sh.code_dest, sh.warehouse_name_dest, sh.supplierID, sh.supplierName, 
		sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
		sh.status, 
		sh.unitquantity, sh.packquantity, 
		sh.requireddate, 
		sh.createdDate,
		olsh.orderlineshortageid orderlineshortage_id,
		olsh.orderlineshortagetype,
		olsh.unitquantity unitquantity_ol, olsh.packquantity packquantity_ol,
		olsh.shortageprogress, olsh.adviseddate, 
		olsh.createdDate createdDate_ol
	from 
			Landing.mend.gen_short_shortage_v sh
		left join
			Landing.mend.short_orderlineshortage_shortage_aud olshsh on sh.shortage_id = olshsh.shortageid 
		left join
			Landing.mend.short_orderlineshortage_aud olsh on olshsh.orderlineshortageid = olsh.id
		left join
			Landing.mend.short_orderlineshortage_orderline_aud olshol on olsh.id = olshol.orderlineshortageid
go



--------------- WAREHOUSE SHIPMENT RELATED -----------------------------

drop view mend.gen_whship_shipment_v 
go 

create view mend.gen_whship_shipment_v as
	select whs.id shipmentid, w.warehouseid, s.supplier_id, -- wsib_whs.warehousestockitembatchid,
		whs.shipmentID shipment_ID, whs.orderNumber, whs.receiptID, whs.receiptNumber, whs.invoiceRef,
		w.code, w.name warehouse_name, s.supplierID, s.supplierName,
		whs.status, whs.shipmentType, 
		whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
		whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
		whs.lock,
		whs.auditComment,
		whs.createdDate
	from 
			Landing.mend.whship_shipment_aud whs
		inner join
			Landing.mend.whship_shipment_warehouse_aud whsw on whs.id = whsw.shipmentid
		inner join
			Landing.mend.gen_wh_warehouse_v w on whsw.warehouseid = w.warehouseid
		left join
			Landing.mend.whship_shipment_supplier_aud whss on whs.id = whss.shipmentid
		left join
			Landing.mend.gen_supp_supplier_v s on whss.supplierid = s.supplier_id
		-- inner join
			-- Landing.mend.whship_warehousestockitembatch_shipment_aud wsib_whs on whs.id = wsib_whs.shipmentid
go

drop view mend.gen_whship_shipment_warehousestockitembatch_v 
go 

create view mend.gen_whship_shipment_warehousestockitembatch_v as			
	select whs.shipmentid, whs.warehouseid, whs.supplier_id, whswsib.warehousestockitembatchid,
		whs.shipment_ID, whs.orderNumber, whs.receiptID, whs.receiptNumber, whs.invoiceRef,
		whs.code, whs.warehouse_name, whs.supplierID, whs.supplierName,
		whs.status, whs.shipmentType, 
		whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
		whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
		whs.lock,
		whs.auditComment,
		whs.createdDate
	from 
			Landing.mend.gen_whship_shipment_v whs
		inner join
			Landing.mend.whship_warehousestockitembatch_shipment_aud whswsib on whs.shipmentid = whswsib.shipmentid
go

drop view mend.gen_whship_shipmentorderline_v 
go 

create view mend.gen_whship_shipmentorderline_v as

	select whsol.id shipmentorderlineid, whs.shipmentid, whs.warehouseid, whs.supplier_id, whsolpol.purchaseorderlineid, whsolrl.snapreceiptlineid, whsolol.warehousestockitembatchid,
		whs.shipment_ID, whs.orderNumber, whs.receiptID, whs.receiptNumber, -- whs.invoiceRef,
		whs.code, whs.warehouse_name, whs.supplierID, whs.supplierName,
		whs.status, whs.shipmentType, 
		whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
		whs.totalItemPrice, 
		whs.createdDate,
		whsol.line, whsol.stockItem,
		whsol.status status_ol, whsol.syncStatus, whsol.processed, -- whsol.syncResolution, whsol.created, 
		whsol.quantityOrdered, whsol.quantityReceived, whsol.quantityAccepted, whsol.quantityRejected, whsol.quantityReturned, 
		whsol.unitCost
		--auditComment
	from 
			Landing.mend.gen_whship_shipment_v whs 
		full join -- right	
			Landing.mend.whship_shipmentorderline_shipment_aud whsolwhs on whs.shipmentid = whsolwhs.shipmentid 
		full join -- right
			Landing.mend.whship_shipmentorderline_aud whsol on whsolwhs.shipmentorderlineid = whsol.id
		left join
			Landing.mend.whship_shipmentorderline_purchaseorderline_aud whsolpol on whsol.id = whsolpol.shipmentorderlineid
		left join
			Landing.mend.whship_shipmentorderline_snapreceiptline_aud whsolrl on whsol.id = whsolrl.shipmentorderlineid
		left join
			Landing.mend.whship_warehousestockitembatch_shipmentorderline whsolol on whsol.id = whsolol.shipmentorderlineid
go


--------------- INTERSITE TRANSFERS RELATED -----------------------------

drop view mend.gen_inter_transferheader_v 
go 

create view mend.gen_inter_transferheader_v as

	select th.id transferheaderid, thpo_s.purchaseorderid purchaseorderid_s, thpo_t.purchaseorderid purchaseorderid_t, 
		th.transfernum, th.allocatedstrategy, 
		th.totalremaining,
		th.createddate, -- th.changeddate		 
		po_t.code code_t, po_t.warehouse_name warehouse_name_t, po_t.supplierID supplierID_t, po_t.supplierName supplierName_t, po_t.ordernumber ordernumber_t, 
		po_s.code code_s, po_s.warehouse_name warehouse_name_s, po_s.supplierID supplierID_s, po_s.supplierName supplierName_s, po_s.ordernumber ordernumber_s
	from 
			Landing.mend.inter_transferheader_aud th
		left join
			Landing.mend.inter_supplypo_transferheader_aud thpo_s on th.id = thpo_s.transferheaderid
		left join
			Landing.mend.gen_purc_purchaseorder_v po_s on thpo_s.purchaseorderid = po_s.purchaseorderid
		left join
			Landing.mend.inter_transferpo_transferheader_aud thpo_t on th.id = thpo_t.transferheaderid
		left join
			Landing.mend.gen_purc_purchaseorder_v po_t on thpo_t.purchaseorderid = po_t.purchaseorderid
go


drop view mend.gen_inter_intransitbatchheader_v 
go 

create view mend.gen_inter_intransitbatchheader_v as

	select ibh.id intransitbatchheaderid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, ibhcs.customershipmentid, -- cs.orderid, 
		th.transfernum, th.allocatedstrategy, th.totalremaining, th.createddate, th.ordernumber_t, th.ordernumber_s, 
		ibh.createddate createddate_ibh
		-- , cs.magentoOrderID_int, cs.incrementID
	from 
			Landing.mend.gen_inter_transferheader_v th 
		left join
			Landing.mend.inter_intransitbatchheader_transferheader_aud ibhth on th.transferheaderid = ibhth.transferheaderid
		left join
			Landing.mend.inter_intransitbatchheader_aud ibh on ibhth.intransitbatchheaderid = ibh.id 
		left join
			Landing.mend.inter_intransitbatchheader_customershipment_aud ibhcs on ibh.id = ibhcs.intransitbatchheaderid
		-- left join
			-- Landing.mend.gen_ship_customershipment_v cs on ibhcs.customershipmentid = cs.customershipmentid
go


drop view mend.gen_inter_intransitstockitembatch_th_v 
go 

create view mend.gen_inter_intransitstockitembatch_th_v as
	select id intransitstockitembatchid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, isibsi.stockitemid, isibwsh.shipmentid,
		-- ibh.intransitbatchheaderid, ibh.customershipmentid, 
		th.transfernum, th.allocatedstrategy, th.totalremaining, th.createddate, th.ordernumber_s, th.ordernumber_t, 
		-- ibh.createddate_ibh, ibh.magentoOrderID_int, ibh.incrementID,
		isib.issuedquantity, isib.remainingquantity, 
		isib.used,
		isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
		isib.currency,
		isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
		isib.createddate createddate_isib
	from
			Landing.mend.gen_inter_transferheader_v th 
		left join
			Landing.mend.inter_intransitstockitembatch_transferheader_aud isibth on th.transferheaderid = isibth.transferheaderid
		left join
			Landing.mend.inter_intransitstockitembatch_aud isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem_aud isibsi on isib.id = isibsi.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitstockitembatch_shipment_aud isibwsh on isib.id = isibwsh.intransitstockitembatchid
go


drop view mend.gen_inter_intransitstockitembatch_ibh_v 
go 

create view mend.gen_inter_intransitstockitembatch_ibh_v as
	select id intransitstockitembatchid, ibh.intransitbatchheaderid, ibh.transferheaderid, ibh.purchaseorderid_s, ibh.purchaseorderid_t, ibh.customershipmentid, 
		isibsi.stockitemid, isibwsh.shipmentid,
		ibh.transfernum, ibh.allocatedstrategy, ibh.totalremaining, ibh.createddate, ibh.ordernumber_s, ibh.ordernumber_t, 
		ibh.createddate_ibh, -- ibh.magentoOrderID_int, ibh.incrementID,
		isib.issuedquantity, isib.remainingquantity, 
		isib.used,
		isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
		isib.currency,
		isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
		isib.createddate createddate_isib
	from
			Landing.mend.gen_inter_intransitbatchheader_v ibh 
		left join
			Landing.mend.inter_intransitstockitembatch_intransitbatchheader_aud isibth on ibh.intransitbatchheaderid = isibth.intransitbatchheaderid
		left join
			Landing.mend.inter_intransitstockitembatch_aud isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem_aud isibsi on isib.id = isibsi.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitstockitembatch_shipment_aud isibwsh on isib.id = isibwsh.intransitstockitembatchid
go

--------------- SNAP RECEIPT RELATED -----------------------------

drop view mend.gen_rec_snapreceipt_v 
go 

create view mend.gen_rec_snapreceipt_v as

	select sr.id snapreceiptid, srsh.shipmentid,
		sr.receiptid, 
		sr.lines, sr.lineqty, 	
		sr.receiptStatus, sr.receiptprocessStatus, sr.processDetail,  sr.priority, 
		sr.status, sr.stockstatus, 
		sr.ordertype, sr.orderclass, 
		sr.suppliername, sr.consignmentID, 
		sr.weight, sr.actualWeight, sr.volume, 
		sr.dateDueIn, sr.dateReceipt, sr.dateCreated, sr.dateArrival, sr.dateClosed,  
		sr.datesfixed
	from 
			Landing.mend.rec_snapreceipt_aud sr
		left join
			Landing.mend.rec_snapreceipt_shipment_aud srsh on sr.id = srsh.snapreceiptid
go


drop view mend.gen_rec_snapreceiptline_v 
go 

create view mend.gen_rec_snapreceiptline_v as

	select srl.id snapreceiptlineid, sr.snapreceiptid, sr.shipmentid, srlsi.stockitemid,
		sr.receiptid, sr.lines, sr.lineqty, 	
		sr.receiptStatus, sr.receiptprocessStatus, sr.processDetail, sr.ordertype, sr.orderclass, sr.dateCreated,
		srl.line, srl.skuid, 
		srl.status, srl.level, srl.unitofmeasure, 
		srl.qtyOrdered, srl.qtyAdvised, srl.qtyReceived, srl.qtyDueIn, srl.qtyRejected
	from 
			Landing.mend.gen_rec_snapreceipt_v sr 
		full join
			Landing.mend.rec_snapreceiptline_snapreceipt_aud srlsr on sr.snapreceiptid = srlsr.snapreceiptid 
		full join
			Landing.mend.rec_snapreceiptline_aud srl on srlsr.snapreceiptlineid = srl.id
		left join
			Landing.mend.rec_snapreceiptline_stockitem_aud srlsi on srl.id = srlsi.snapreceiptlineid
go



--------------- VAT RELATED -----------------------------

drop view mend.gen_vat_commodity_v 
go 

create view mend.gen_vat_commodity_v as

	select c.id commodityid, cpf.productfamilyid,
		c.commoditycode, c.commodityname, 
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else cpf.productfamilyid end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name
	from 
			Landing.mend.vat_commodity_aud c
		inner join
			Landing.mend.vat_productfamily_commodity_aud cpf on c.id = cpf.commodityid
		inner join
			Landing.mend.prod_productfamily_aud pf on cpf.productfamilyid = pf.id
go


drop view mend.gen_vat_dispensingservicesrate_v 
go 

create view mend.gen_vat_dispensingservicesrate_v as

	select dsr.id dispensingservicesrateid, c.commodityid, c.productfamilyid, dsrco.countryid,
		c.commoditycode, c.commodityname, dsr.dispensingservicesrate, dsr.rateeffectivedate, dsr.rateenddate,
		c.magentoProductID_int, c.magentoProductID, c.magentoSKU, c.name
	from 
			Landing.mend.vat_dispensingservicesrate_aud dsr
		inner join
			Landing.mend.vat_dispensingservices_commodity_aud dsrc on dsr.id = dsrc.dispensingservicesrateid
		full join
			Landing.mend.gen_vat_commodity_v c on dsrc.commodityid = c.commodityid
		left join
			Landing.mend.vat_dispensingservicesrate_country_aud dsrco on dsr.id = dsrco.dispensingservicesrateid
go



drop view mend.gen_vat_vatrating_v 
go 

create view mend.gen_vat_vatrating_v as

	select vrat.id vatratingid, c.id countryid, vr.id vatrateid,
		vrat.vatratingcode, vrat.vatratingdescription, 
		c.countryname, 
		vr.vatrate, vr.vatrateeffectivedate, vr.vatrateenddate
	from 
			Landing.mend.vat_vatrating_aud vrat
		inner join
			Landing.mend.vat_vatrating_country_aud vratc on vrat.id = vratc.vatratingid
		inner join
			Landing.mend.comp_country_aud c on vratc.countryid = c.id
		inner join
			Landing.mend.vat_vatrate_vatrating_aud vratvr on vrat.id = vratvr.vatratingid
		inner join
			Landing.mend.vat_vatrate_aud vr on vratvr.vatrateid = vr.id
go


drop view mend.gen_vat_vatschedule_v 
go 

create view mend.gen_vat_vatschedule_v as

	select vs.id vatscheduleid, vrat.vatratingid, vrat.countryid, vrat.vatrateid, c.id commodityid,
		vrat.vatratingcode, vrat.vatratingdescription, vrat.countryname, vrat.vatrate, vrat.vatrateeffectivedate, vrat.vatrateenddate,
		c.commoditycode, c.commodityname,
		vs.vatsaletype, vs.taxationarea, vs.scheduleeffectivedate, vs.scheduleenddate
	from 
			Landing.mend.vat_vatschedule_aud vs
		inner join
			Landing.mend.vat_vatschedule_vatrating_aud vsvrat on vs.id = vsvrat.vatscheduleid
		inner join
			Landing.mend.gen_vat_vatrating_v vrat on vsvrat.vatratingid = vrat.vatratingid
		inner join
			Landing.mend.vat_vatschedule_commodity_aud vsc on vs.id = vsc.vatscheduleid
		inner join
			Landing.mend.vat_commodity c on vsc.commodityid = c.id
go



