
---------- Script creacion de tablas ----------

use Landing
go

------------------- stockallocation$orderlinestockallocationtransaction -------------------

create table mend.all_orderlinestockallocationtransaction (
	id								bigint NOT NULL, 
	transactionID					bigint,
	timestamp						datetime2,
	allocationType					varchar(8), 
	recordType						varchar(10), 
	allocatedUnits					decimal(28,8), 
	allocatedStockQuantity			decimal(28,8), 
	issuedQuantity					decimal(28,8), 
	packSize						decimal(28,8), 
	stockAllocated					bit, 
	cancelled						bit, 
	issuedDateTime					datetime2,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.all_orderlinestockallocationtransaction add constraint [PK_mend_all_orderlinestockallocationtransaction]
	primary key clustered (id);
go
alter table mend.all_orderlinestockallocationtransaction add constraint [DF_mend_all_orderlinestockallocationtransaction_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_orderlinestockallocationtransaction_aud(
	id								bigint NOT NULL, 
	transactionID					bigint,
	timestamp						datetime2,
	allocationType					varchar(8), 
	recordType						varchar(10), 
	allocatedUnits					decimal(28,8), 
	allocatedStockQuantity			decimal(28,8), 
	issuedQuantity					decimal(28,8), 
	packSize						decimal(28,8), 
	stockAllocated					bit, 
	cancelled						bit, 
	issuedDateTime					datetime2,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.all_orderlinestockallocationtransaction_aud add constraint [PK_mend_all_orderlinestockallocationtransaction_aud]
	primary key clustered (id);
go
alter table mend.all_orderlinestockallocationtransaction_aud add constraint [DF_mend_all_orderlinestockallocationtransaction_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_orderlinestockallocationtransaction_aud_hist(
	id								bigint NOT NULL, 
	transactionID					bigint,
	timestamp						datetime2,
	allocationType					varchar(8), 
	recordType						varchar(10), 
	allocatedUnits					decimal(28,8), 
	allocatedStockQuantity			decimal(28,8), 
	issuedQuantity					decimal(28,8), 
	packSize						decimal(28,8), 
	stockAllocated					bit, 
	cancelled						bit, 
	issuedDateTime					datetime2,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.all_orderlinestockallocationtransaction_aud_hist add constraint [DF_mend.all_orderlinestockallocationtransaction_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ stockallocation$at_order ------------

create table mend.all_at_order (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	orderid									bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_order add constraint [PK_mend_all_at_order]
	primary key clustered (orderlinestockallocationtransactionid, orderid);
go
alter table mend.all_at_order add constraint [DF_mend_all_at_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_order_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	orderid									bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_order_aud add constraint [PK_mend_all_at_order_aud]
	primary key clustered (orderlinestockallocationtransactionid, orderid);
go
alter table mend.all_at_order_aud add constraint [DF_mend_all_at_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ stockallocation$at_orderline ------------

create table mend.all_at_orderline (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	orderlineid									bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_orderline add constraint [PK_mend_all_at_orderline]
	primary key clustered (orderlinestockallocationtransactionid, orderlineid);
go
alter table mend.all_at_orderline add constraint [DF_mend_all_at_orderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_orderline_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	orderlineid								bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_orderline_aud add constraint [PK_mend_all_at_orderline_aud]
	primary key clustered (orderlinestockallocationtransactionid, orderlineid);
go
alter table mend.all_at_orderline_aud add constraint [DF_mend_all_at_orderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ stockallocation$at_warehouse ------------

create table mend.all_at_warehouse (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehouseid								bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_warehouse add constraint [PK_mend_all_at_warehouse]
	primary key clustered (orderlinestockallocationtransactionid, warehouseid);
go
alter table mend.all_at_warehouse add constraint [DF_mend_all_at_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_warehouse_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehouseid								bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_warehouse_aud add constraint [PK_mend_all_at_warehouse_aud]
	primary key clustered (orderlinestockallocationtransactionid, warehouseid);
go
alter table mend.all_at_warehouse_aud add constraint [DF_mend_all_at_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ stockallocation$at_warehousestockitem ------------

create table mend.all_at_warehousestockitem (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehousestockitemid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_warehousestockitem add constraint [PK_mend_all_at_warehousestockitem]
	primary key clustered (orderlinestockallocationtransactionid, warehousestockitemid);
go
alter table mend.all_at_warehousestockitem add constraint [DF_mend_all_at_warehousestockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_warehousestockitem_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehousestockitemid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_warehousestockitem_aud add constraint [PK_mend_all_at_warehousestockitem_aud]
	primary key clustered (orderlinestockallocationtransactionid, warehousestockitemid);
go
alter table mend.all_at_warehousestockitem_aud add constraint [DF_mend_all_at_warehousestockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ stockallocation$at_batch ------------

create table mend.all_at_batch (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehousestockitembatchid				bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_batch add constraint [PK_mend_all_at_batch]
	primary key clustered (orderlinestockallocationtransactionid, warehousestockitembatchid);
go
alter table mend.all_at_batch add constraint [DF_mend_all_at_batch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_batch_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	warehousestockitembatchid				bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_batch_aud add constraint [PK_mend_all_at_batch_aud]
	primary key clustered (orderlinestockallocationtransactionid, warehousestockitembatchid);
go
alter table mend.all_at_batch_aud add constraint [DF_mend_all_at_batch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ stockallocation$at_packsize ------------

create table mend.all_at_packsize (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	packsizeid								bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_at_packsize add constraint [PK_mend_all_at_packsize]
	primary key clustered (orderlinestockallocationtransactionid, packsizeid);
go
alter table mend.all_at_packsize add constraint [DF_mend_all_at_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_at_packsize_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	packsizeid								bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_at_packsize_aud add constraint [PK_mend_all_at_packsize_aud]
	primary key clustered (orderlinestockallocationtransactionid, packsizeid);
go
alter table mend.all_at_packsize_aud add constraint [DF_mend_all_at_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ stockallocatio$orderlinestockallocationtransa_customershipmentl ------------

create table mend.all_orderlinestockallocationtransa_customershipmentl (
	orderlinestockallocationtransactionid	bigint NOT NULL,
	customershipmentlineid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_orderlinestockallocationtransa_customershipmentl add constraint [PK_mend_all_orderlinestockallocationtransa_customershipmentl]
	primary key clustered (orderlinestockallocationtransactionid, customershipmentlineid);
go
alter table mend.all_orderlinestockallocationtransa_customershipmentl add constraint [DF_mend_all_orderlinestockallocationtransa_customershipmentl_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_orderlinestockallocationtransa_customershipmentl_aud(
	orderlinestockallocationtransactionid	bigint NOT NULL,
	customershipmentlineid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_orderlinestockallocationtransa_customershipmentl_aud add constraint [PK_mend_all_orderlinestockallocationtransa_customershipmentl_aud]
	primary key clustered (orderlinestockallocationtransactionid, customershipmentlineid);
go
alter table mend.all_orderlinestockallocationtransa_customershipmentl_aud add constraint [DF_mend_all_orderlinestockallocationtransa_customershipmentl_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go








------------ stockallocation$magentoallocation ------------

create table mend.all_magentoallocation (
	id								bigint NOT NULL, 
	shipped							bit, 
	cancelled						bit, 
	quantity						decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.all_magentoallocation add constraint [PK_mend_all_magentoallocation]
	primary key clustered (id);
go
alter table mend.all_magentoallocation add constraint [DF_mend_all_magentoallocation_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_magentoallocation_aud(
	id								bigint NOT NULL, 
	shipped							bit, 
	cancelled						bit, 
	quantity						decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.all_magentoallocation_aud add constraint [PK_mend_all_magentoallocation_aud]
	primary key clustered (id);
go
alter table mend.all_magentoallocation_aud add constraint [DF_mend_all_magentoallocation_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_magentoallocation_aud_hist(
	id								bigint NOT NULL, 
	shipped							bit, 
	cancelled						bit, 
	quantity						decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.all_magentoallocation_aud_hist add constraint [DF_mend.all_magentoallocation_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ stockallocatio$magentoallocatio_orderlinestockallocationtransac ------------

create table mend.all_magentoallocatio_orderlinestockallocationtransac (
	magentoallocationid						bigint NOT NULL, 
	orderlinestockallocationtransactionid	bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.all_magentoallocatio_orderlinestockallocationtransac add constraint [PK_mend_all_magentoallocatio_orderlinestockallocationtransac]
	primary key clustered (magentoallocationid,orderlinestockallocationtransactionid);
go
alter table mend.all_magentoallocatio_orderlinestockallocationtransac add constraint [DF_mend_all_magentoallocatio_orderlinestockallocationtransac_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.all_magentoallocatio_orderlinestockallocationtransac_aud(
	magentoallocationid						bigint NOT NULL, 
	orderlinestockallocationtransactionid	bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.all_magentoallocatio_orderlinestockallocationtransac_aud add constraint [PK_mend_all_magentoallocatio_orderlinestockallocationtransac_aud]
	primary key clustered (magentoallocationid,orderlinestockallocationtransactionid);
go
alter table mend.all_magentoallocatio_orderlinestockallocationtransac_aud add constraint [DF_mend_all_magentoallocatio_orderlinestockallocationtransac_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go