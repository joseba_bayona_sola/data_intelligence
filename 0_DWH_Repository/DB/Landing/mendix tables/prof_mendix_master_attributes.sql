
-- 1_supplier

select supplierType, count(*)
from Landing.mend.supp_supplier_aud
group by supplierType
order by supplierType

select flatFileConfigurationToUse, count(*)
from Landing.mend.supp_supplier_aud
group by flatFileConfigurationToUse
order by flatFileConfigurationToUse

-- 

select subMetaObjectName, count(*)
from Landing.mend.supp_supplierprice_aud
group by subMetaObjectName
order by subMetaObjectName

-- 2 company

-- 3 product

select status, count(*)
from Landing.mend.prod_productfamily_aud
group by status
order by status

select productFamilyType, count(*)
from Landing.mend.prod_productfamily_aud
group by productFamilyType
order by productFamilyType

-- 

select productType, count(*)
from Landing.mend.prod_product_aud
group by productType
order by productType

-- 

select SNAPUploadStatus, count(*)
from Landing.mend.prod_stockitem_aud
group by SNAPUploadStatus
order by SNAPUploadStatus

-- 

select productGroup1, count(*)
from Landing.mend.prod_packsize_aud
group by productGroup1
order by productGroup1

select productGroup2, count(*)
from Landing.mend.prod_packsize_aud
group by productGroup2 
order by productGroup2

select productGroup2Type, count(*)
from Landing.mend.prod_packsize_aud
group by productGroup2Type
order by productGroup2Type

-- 4 warehouse

-- 5 warehouse SI

select stockingmethod, count(*)
from Landing.mend.wh_warehousestockitem_aud
group by stockingmethod
order by stockingmethod

-- 

select status, count(*)
from Landing.mend.wh_warehousestockitembatch_aud
group by status
order by status

select fullyallocated, fullyIssued, count(*)
from Landing.mend.wh_warehousestockitembatch_aud
group by fullyallocated, fullyIssued
order by fullyallocated, fullyIssued

-- 

select stockadjustment, count(*)
from Landing.mend.wh_stockmovement_aud
group by stockadjustment
order by stockadjustment

select movementtype, movetype, count(*)
from Landing.mend.wh_stockmovement_aud
group by movementtype, movetype
order by movementtype, movetype

select _class, count(*)
from Landing.mend.wh_stockmovement_aud
group by _class
order by _class

--

select batchstockmovementtype, count(*)
from Landing.mend.wh_batchstockmovement_aud
group by batchstockmovementtype
order by batchstockmovementtype

-- 6 order

select orderStatus, count(*)
from mend.order_order_aud
group by orderStatus
order by orderStatus

select _type, count(*)
from mend.order_order_aud
group by _type
order by _type

select shipmentStatus, count(*)
from mend.order_order_aud
group by shipmentStatus
order by shipmentStatus

select allocationStatus, count(*)
from mend.order_order_aud
group by allocationStatus
order by allocationStatus

select allocationStrategy, count(*)
from mend.order_order_aud
group by allocationStrategy
order by allocationStrategy

select labelRenderer, count(*)
from mend.order_order_aud
group by labelRenderer
order by labelRenderer

-- 

select orderLineType, count(*)
from Landing.mend.order_orderlineabstract_aud
group by orderLineType
order by orderLineType

select subMetaObjectName, count(*)
from Landing.mend.order_orderlineabstract_aud
group by subMetaObjectName
order by subMetaObjectName

-- 

select allocationStatus, count(*)
from Landing.mend.order_shippinggroup_aud
group by allocationStatus
order by allocationStatus

select shipmentStatus, count(*)
from Landing.mend.order_shippinggroup_aud
group by shipmentStatus
order by shipmentStatus


-- 7 Calendar

-- 8 Shipments

select status, count(*)
from mend.ship_customershipment_aud
group by status
order by status

select warehouseMethod, count(*)
from mend.ship_customershipment_aud
group by warehouseMethod
order by warehouseMethod

select labelRenderer, count(*)
from mend.ship_customershipment_aud
group by labelRenderer
order by labelRenderer

-- 

select status, count(*)
from Landing.mend.ship_customershipmentline_aud
group by status
order by status

-- 

select status, count(*)
from Landing.mend.ship_lifecycle_update_aud
group by status
order by status

-- 

select service, count(*)
from Landing.mend.ship_api_updates
group by service
order by service

select status, count(*)
from Landing.mend.ship_api_updates
group by status
order by status

select operation, count(*)
from Landing.mend.ship_api_updates
group by operation
order by operation

-- 9 Stock Allocation

select allocationType, count(*)
from Landing.mend.all_orderlinestockallocationtransaction_aud
group by allocationType
order by allocationType

select recordType, count(*)
from Landing.mend.all_orderlinestockallocationtransaction_aud
group by recordType
order by recordType


-- 10 Purchase Orders

select source, count(*)
from Landing.mend.purc_purchaseorder_aud
group by source
order by source

select status, count(*)
from Landing.mend.purc_purchaseorder_aud
group by status
order by status

-- 

select status, count(*)
from Landing.mend.purc_purchaseorderlineheader_aud
group by status
order by status

select problems, count(*)
from Landing.mend.purc_purchaseorderlineheader_aud
group by problems
order by problems


-- 11 Shortages

select status, count(*)
from Landing.mend.short_shortage_aud
group by status
order by status

-- 

select orderlineshortagetype, count(*)
from Landing.mend.short_orderlineshortage_aud
group by orderlineshortagetype
order by orderlineshortagetype


-- 12 WH Shipments

select status, count(*)
from Landing.mend.whship_shipment_aud
group by status
order by status

select shipmentType, count(*)
from Landing.mend.whship_shipment_aud
group by shipmentType
order by shipmentType

-- 

select status, count(*)
from Landing.mend.whship_shipmentorderline_aud
group by status
order by status

select syncStatus, count(*)
from Landing.mend.whship_shipmentorderline_aud
group by syncStatus
order by syncStatus

select syncResolution, count(*)
from Landing.mend.whship_shipmentorderline_aud
group by syncResolution
order by syncResolution

select processed, count(*)
from Landing.mend.whship_shipmentorderline_aud
group by processed
order by processed


-- 13 Intersite Transfers

select allocatedstrategy, count(*)
from Landing.mend.inter_transferheader_aud
group by allocatedstrategy
order by allocatedstrategy

-- 14 SNAP Receipts

select receiptStatus, count(*)
from Landing.mend.rec_snapreceipt_aud
group by receiptStatus
order by receiptStatus

select receiptprocessStatus, count(*)
from Landing.mend.rec_snapreceipt_aud
group by receiptprocessStatus
order by receiptprocessStatus

select processDetail, count(*)
from Landing.mend.rec_snapreceipt_aud
group by processDetail
order by processDetail

select ordertype, count(*)
from Landing.mend.rec_snapreceipt_aud
group by ordertype
order by ordertype

select orderclass, count(*)
from Landing.mend.rec_snapreceipt_aud
group by orderclass
order by orderclass

select suppliername, count(*)
from Landing.mend.rec_snapreceipt_aud
group by suppliername
order by suppliername


select receiptStatus, count(*)
from Landing.mend.rec_snapreceipt_aud
group by receiptStatus
order by receiptStatus

-- 

select status, count(*)
from Landing.mend.rec_snapreceiptline_aud
group by status
order by status

select unitofmeasure, count(*)
from Landing.mend.rec_snapreceiptline_aud
group by unitofmeasure
order by unitofmeasure

