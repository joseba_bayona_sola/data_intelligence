
------------------------ Profile Scripts ----------------------------

use Landing
go

------------------- customershipments$customershipment -------------------

select *
from mend.ship_customershipment

select id, 
	shipmentID, partitionID0_120,
	shipmentNumber, orderIncrementID, 
	status, statusChange, 
	processingFailed, warehouseMethod, labelRenderer, 
	shippingMethod, shippingDescription, paymentMethod, 	
	notify, fullyShipped, syncedToMagento, 	
	shipmentValue, shippingTotal, toFollowValue, 
	hold, intersite, 
	dispatchDate, expectedShippingDate, expectedDeliveryDate,  
	createdDate, changeddate,
	idETLBatchRun, ins_ts
from mend.ship_customershipment

select top 1000 id, 
	shipmentID, partitionID0_120,
	shipmentNumber, orderIncrementID, 
	status, statusChange, 
	processingFailed, warehouseMethod, labelRenderer, 
	shippingMethod, shippingDescription, paymentMethod, 	
	notify, fullyShipped, syncedToMagento, 	
	shipmentValue, shippingTotal, toFollowValue, 
	hold, intersite, 
	dispatchDate, expectedShippingDate, expectedDeliveryDate,  
	createdDate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipment_aud

select id, 
	shipmentID, partitionID0_120,
	shipmentNumber, orderIncrementID, 
	status, statusChange, 
	processingFailed, warehouseMethod, labelRenderer, 
	shippingMethod, shippingDescription, paymentMethod, 	
	notify, fullyShipped, syncedToMagento, 	
	shipmentValue, shippingTotal, toFollowValue, 
	hold, intersite, 
	dispatchDate, expectedShippingDate, expectedDeliveryDate,  
	createdDate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_customershipment_aud_hist

------------------- customershipments$customershipment_warehouse -------------------

select *
from mend.ship_customershipment_warehouse

select customershipmentid, warehouseid,
	idETLBatchRun, ins_ts
from mend.ship_customershipment_warehouse

select top 1000 customershipmentid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipment_warehouse_aud

------------------- customershipments$customershipment_order -------------------

select *
from mend.ship_customershipment_order

select customershipmentid, orderid,
	idETLBatchRun, ins_ts
from mend.ship_customershipment_order

select top 1000 customershipmentid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipment_order_aud






------------------- customershipments$lifecycle_update -------------------

select *
from mend.ship_lifecycle_update

select id, status, timestamp,
	idETLBatchRun, ins_ts
from mend.ship_lifecycle_update

select top 1000 id, status, timestamp,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_lifecycle_update_aud

select id, status, timestamp,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_lifecycle_update_aud_hist

------------------- customershipments$customershipment_progress -------------------

select *
from mend.ship_customershipment_progress

select top 1000 lifecycle_updateid, customershipmentid,
	idETLBatchRun, ins_ts
from mend.ship_customershipment_progress

select lifecycle_updateid, customershipmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipment_progress_aud

------------------- customershipments$api_updates -------------------

select *
from mend.ship_api_updates

select id, 
	service, status, operation, 
	timestamp, 
	detail,
	idETLBatchRun, ins_ts
from mend.ship_api_updates

select top 1000 id, 
	service, status, operation, 
	timestamp, 
	detail,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_api_updates_aud

select id, 
	service, status, operation, 
	timestamp, 
	detail,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_api_updates_aud_hist

------------------- customershipments$api_updates_customershipment -------------------

select *
from mend.ship_api_updates_customershipment

select top 1000 api_updatesid, customershipmentid,
	idETLBatchRun, ins_ts
from mend.ship_api_updates_customershipment

select api_updatesid,customershipmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_api_updates_customershipment_aud






------------------- customershipments$consignment -------------------

select *
from mend.ship_consignment

select id, 
	order_number, identifier, 
	warehouse_id, 
	consignment_number, 
	shipping_method, service_id, carrier, service, 
	delivery_instructions, tracking_url, 
	order_value, currency,
	create_date,
	idETLBatchRun, ins_ts
from mend.ship_consignment

select top 1000 id, 
	order_number, identifier, 
	warehouse_id, 
	consignment_number, 
	shipping_method, service_id, carrier, service, 
	delivery_instructions, tracking_url, 
	order_value, currency,
	create_date,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_consignment_aud

select id, 
	order_number, identifier, 
	warehouse_id, 
	consignment_number, 
	shipping_method, service_id, carrier, service, 
	delivery_instructions, tracking_url, 
	order_value, currency,
	create_date,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_consignment_aud_hist

------------------- customershipments$scurriconsignment_shipment -------------------

select *
from mend.ship_scurriconsignment_shipment

select consignmentid,customershipmentid,
	idETLBatchRun, ins_ts
from mend.ship_scurriconsignment_shipment

select top 1000 consignmentid,customershipmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_scurriconsignment_shipment_aud

------------------- customershipments$packagedetail -------------------

select *
from mend.ship_packagedetail

select id, 
	tracking_number, description, 
	width, length, height,
	idETLBatchRun, ins_ts
from mend.ship_packagedetail

select top 1000 id, 
	tracking_number, description, 
	width, length, height,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_packagedetail_aud

select id, 
	tracking_number, description, 
	width, length, height,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_packagedetail_aud_hist

------------------- customershipments$consignmentpackages -------------------

select *
from mend.ship_consignmentpackages

select top 1000 packagedetailid, consignmentid,
	idETLBatchRun, ins_ts
from mend.ship_consignmentpackages

select packagedetailid,consignmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_consignmentpackages_aud







------------------- customershipments$customershipmentline -------------------

select *
from mend.ship_customershipmentline

select id, 
	shipmentLineID, lineNumber, status, variableBreakdown, 
	stockItemID, productName, productDescription, eye, 
	quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
	packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
	price, priceFormatted, 
	subTotal, subTotal_formatted, 
	VATRate, VAT, VAT_formatted,
	fullyShipped,  
	BOMLine, 
	system$changedBy,
	idETLBatchRun, ins_ts
from mend.ship_customershipmentline

select top 1000 id, 
	shipmentLineID, lineNumber, status, variableBreakdown, 
	stockItemID, productName, productDescription, eye, 
	quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
	packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
	price, priceFormatted, 
	subTotal, subTotal_formatted, 
	VATRate, VAT, VAT_formatted,
	fullyShipped,  
	BOMLine, 
	system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipmentline_aud

select id, 
	shipmentLineID, lineNumber, status, variableBreakdown, 
	stockItemID, productName, productDescription, eye, 
	quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
	packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
	price, priceFormatted, 
	subTotal, subTotal_formatted, 
	VATRate, VAT, VAT_formatted,
	fullyShipped,  
	BOMLine, 
	system$changedBy,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.ship_customershipmentline_aud_hist

------------------- customershipments$customershipmentline_customershipment -------------------

select *
from mend.ship_customershipmentline_customershipment

select customershipmentlineid, customershipmentid,
	idETLBatchRun, ins_ts
from mend.ship_customershipmentline_customershipment

select top 1000 customershipmentlineid,customershipmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipmentline_customershipment_aud

------------------- customershipments$customershipmentline_product -------------------

select *
from mend.ship_customershipmentline_product

select customershipmentlineid, productid,
	idETLBatchRun, ins_ts
from mend.ship_customershipmentline_product

select top 1000 customershipmentlineid,productid,
	idETLBatchRun, ins_ts, upd_ts
from mend.ship_customershipmentline_product_aud

