use Landing
go 

------------------- orderprocessing$order -------------------

select * 
from mend.order_order_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ orderprocessing$basevalues ------------

select * 
from mend.order_basevalues_aud_v
where num_records > 1
order by id, idETLBatchRun



------------ orderprocessing$order_statusupdate ------------

select * 
from mend.order_order_statusupdate_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ orderprocessing$orderprocessingtransaction ------------

select * 
from mend.order_orderprocessingtransaction_aud_v
where num_records > 1
order by id, idETLBatchRun



------------ orderprocessing$orderlineabstract ------------

select * 
from mend.order_orderlineabstract_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ orderprocessing$orderlinemagento ------------

select * 
from mend.order_orderlinemagento_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ orderprocessing$orderline ------------

select * 
from mend.order_orderline_aud_v
where num_records > 1
order by id, idETLBatchRun



------------ orderprocessing$shippinggroup ------------

select * 
from mend.order_shippinggroup_aud_v
where num_records > 1
order by id, idETLBatchRun