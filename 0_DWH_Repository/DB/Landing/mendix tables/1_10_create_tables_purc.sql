
---------- Script creacion de tablas ----------

use Landing
go

------------------- purchaseorders$purchaseorder -------------------

create table mend.purc_purchaseorder (
	id								bigint NOT NULL,
	ordernumber						varchar(9), 
	source							varchar(12), 
	status							varchar(200), 
	potype							varchar(18), 
	leadtime						int, 
	itemcost						decimal(28,8), 
	totalprice						decimal(28,8), 
	formattedprice					varchar(200), 
	duedate							datetime2, 
	receiveddate					datetime2, 
	createddate						datetime2, 
	approveddate					datetime2, 
	approvedby						varchar(200), 
	confirmeddate					datetime2, 
	confirmedby						varchar(200), 
	submitteddate					datetime2, 
	submittedby						varchar(200), 
	completeddate					datetime2,	
	completedby						varchar(200),
	transferleadtime				int,
	transitleadtime					int,
	spoleadtime						int,
	spoduedate						datetime2,
	tpoleadtime						int,
	tpotransferleadtime				int,
	tpotransferdate					datetime2,
	tpoduedate						datetime2,
	requiredshipdate				datetime2,
	requiredtransferdate			datetime2,
	estimatedpaymentdate			datetime2,
	estimateddepositdate			datetime2,
	supplierreference				varchar(200),
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorder add constraint [PK_mend_purc_purchaseorder]
	primary key clustered (id);
go
alter table mend.purc_purchaseorder add constraint [DF_mend_purc_purchaseorder_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorder_aud(
	id								bigint NOT NULL,
	ordernumber						varchar(9), 
	source							varchar(12), 
	status							varchar(200), 
	potype							varchar(18), 
	leadtime						int, 
	itemcost						decimal(28,8), 
	totalprice						decimal(28,8), 
	formattedprice					varchar(200), 
	duedate							datetime2, 
	receiveddate					datetime2, 
	createddate						datetime2, 
	approveddate					datetime2, 
	approvedby						varchar(200), 
	confirmeddate					datetime2, 
	confirmedby						varchar(200), 
	submitteddate					datetime2, 
	submittedby						varchar(200), 
	completeddate					datetime2,	
	completedby						varchar(200),
	transferleadtime				int,
	transitleadtime					int,
	spoleadtime						int,
	spoduedate						datetime2,
	tpoleadtime						int,
	tpotransferleadtime				int,
	tpotransferdate					datetime2,
	tpoduedate						datetime2,
	requiredshipdate				datetime2,
	requiredtransferdate			datetime2,
	estimatedpaymentdate			datetime2,
	estimateddepositdate			datetime2,
	supplierreference				varchar(200),
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorder_aud add constraint [PK_mend_purc_purchaseorder_aud]
	primary key clustered (id);
go
alter table mend.purc_purchaseorder_aud add constraint [DF_mend_purc_purchaseorder_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorder_aud_hist(
	id								bigint NOT NULL,
	ordernumber						varchar(9), 
	source							varchar(12), 
	status							varchar(200), 
	potype							varchar(18), 
	leadtime						int, 
	itemcost						decimal(28,8), 
	totalprice						decimal(28,8), 
	formattedprice					varchar(200), 
	duedate							datetime2, 
	receiveddate					datetime2, 
	createddate						datetime2, 
	approveddate					datetime2, 
	approvedby						varchar(200), 
	confirmeddate					datetime2, 
	confirmedby						varchar(200), 
	submitteddate					datetime2, 
	submittedby						varchar(200), 
	completeddate					datetime2,	
	completedby						varchar(200),
	transferleadtime				int,
	transitleadtime					int,
	spoleadtime						int,
	spoduedate						datetime2,
	tpoleadtime						int,
	tpotransferleadtime				int,
	tpotransferdate					datetime2,
	tpoduedate						datetime2,
	requiredshipdate				datetime2,
	requiredtransferdate			datetime2,
	estimatedpaymentdate			datetime2,
	estimateddepositdate			datetime2,
	supplierreference				varchar(200),
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.purc_purchaseorder_aud_hist add constraint [DF_mend.purc_purchaseorder_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------------- purchaseorders$purchaseorder_supplier -------------------

create table mend.purc_purchaseorder_supplier (
	purchaseorderid					bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorder_supplier add constraint [PK_mend_purc_purchaseorder_supplier]
	primary key clustered (purchaseorderid,supplierid);
go
alter table mend.purc_purchaseorder_supplier add constraint [DF_mend_purc_purchaseorder_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorder_supplier_aud(
	purchaseorderid					bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorder_supplier_aud add constraint [PK_mend_purc_purchaseorder_supplier_aud]
	primary key clustered (purchaseorderid,supplierid);
go
alter table mend.purc_purchaseorder_supplier_aud add constraint [DF_mend_purc_purchaseorder_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorder_warehouse -------------------

create table mend.purc_purchaseorder_warehouse (
	purchaseorderid					bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorder_warehouse add constraint [PK_mend_purc_purchaseorder_warehouse]
	primary key clustered (purchaseorderid,warehouseid);
go
alter table mend.purc_purchaseorder_warehouse add constraint [DF_mend_purc_purchaseorder_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorder_warehouse_aud(
	purchaseorderid					bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorder_warehouse_aud add constraint [PK_mend_purc_purchaseorder_warehouse_aud]
	primary key clustered (purchaseorderid,warehouseid);
go
alter table mend.purc_purchaseorder_warehouse_aud add constraint [DF_mend_purc_purchaseorder_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------------- purchaseorders$purchaseorderlineheader -------------------

create table mend.purc_purchaseorderlineheader (
	id								bigint NOT NULL,
	status							varchar(200), 
	problems						varchar(100), 
	lines							decimal(28,8), 
	items							decimal(28,8), 
	totalprice						decimal(28,8), 
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderlineheader add constraint [PK_mend_purc_purchaseorderlineheader]
	primary key clustered (id);
go
alter table mend.purc_purchaseorderlineheader add constraint [DF_mend_purc_purchaseorderlineheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_aud(
	id								bigint NOT NULL,
	status							varchar(200), 
	problems						varchar(100), 
	lines							decimal(28,8), 
	items							decimal(28,8), 
	totalprice						decimal(28,8), 
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderlineheader_aud add constraint [PK_mend_purc_purchaseorderlineheader_aud]
	primary key clustered (id);
go
alter table mend.purc_purchaseorderlineheader_aud add constraint [DF_mend_purc_purchaseorderlineheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_aud_hist(
	id								bigint NOT NULL,
	status							varchar(200), 
	problems						varchar(100), 
	lines							decimal(28,8), 
	items							decimal(28,8), 
	totalprice						decimal(28,8), 
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.purc_purchaseorderlineheader_aud_hist add constraint [DF_mend.purc_purchaseorderlineheader_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderlineheader_purchaseorder -------------------

create table mend.purc_purchaseorderlineheader_purchaseorder (
	purchaseorderlineheaderid		bigint NOT NULL,
	purchaseorderid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderlineheader_purchaseorder add constraint [PK_mend_purc_purchaseorderlineheader_purchaseorder]
	primary key clustered (purchaseorderlineheaderid,purchaseorderid);
go
alter table mend.purc_purchaseorderlineheader_purchaseorder add constraint [DF_mend_purc_purchaseorderlineheader_purchaseorder_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_purchaseorder_aud(
	purchaseorderlineheaderid		bigint NOT NULL,
	purchaseorderid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderlineheader_purchaseorder_aud add constraint [PK_mend_purc_purchaseorderlineheader_purchaseorder_aud]
	primary key clustered (purchaseorderlineheaderid,purchaseorderid);
go
alter table mend.purc_purchaseorderlineheader_purchaseorder_aud add constraint [DF_mend_purc_purchaseorderlineheader_purchaseorder_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderlineheader_productfamily -------------------

create table mend.purc_purchaseorderlineheader_productfamily (
	purchaseorderlineheaderid		bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderlineheader_productfamily add constraint [PK_mend_purc_purchaseorderlineheader_productfamily]
	primary key clustered (purchaseorderlineheaderid,productfamilyid);
go
alter table mend.purc_purchaseorderlineheader_productfamily add constraint [DF_mend_purc_purchaseorderlineheader_productfamily_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_productfamily_aud(
	purchaseorderlineheaderid		bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderlineheader_productfamily_aud add constraint [PK_mend_purc_purchaseorderlineheader_productfamily_aud]
	primary key clustered (purchaseorderlineheaderid,productfamilyid);
go
alter table mend.purc_purchaseorderlineheader_productfamily_aud add constraint [DF_mend_purc_purchaseorderlineheader_productfamily_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderlineheader_packsize -------------------

create table mend.purc_purchaseorderlineheader_packsize (
	purchaseorderlineheaderid		bigint NOT NULL,
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderlineheader_packsize add constraint [PK_mend_purc_purchaseorderlineheader_packsize]
	primary key clustered (purchaseorderlineheaderid,packsizeid);
go
alter table mend.purc_purchaseorderlineheader_packsize add constraint [DF_mend_purc_purchaseorderlineheader_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_packsize_aud(
	purchaseorderlineheaderid		bigint NOT NULL,
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderlineheader_packsize_aud add constraint [PK_mend_purc_purchaseorderlineheader_packsize_aud]
	primary key clustered (purchaseorderlineheaderid,packsizeid);
go
alter table mend.purc_purchaseorderlineheader_packsize_aud add constraint [DF_mend_purc_purchaseorderlineheader_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderlineheader_supplierprices -------------------

create table mend.purc_purchaseorderlineheader_supplierprices (
	purchaseorderlineheaderid		bigint NOT NULL,
	supplierpriceid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderlineheader_supplierprices add constraint [PK_mend_purc_purchaseorderlineheader_supplierprices]
	primary key clustered (purchaseorderlineheaderid,supplierpriceid);
go
alter table mend.purc_purchaseorderlineheader_supplierprices add constraint [DF_mend_purc_purchaseorderlineheader_supplierprices_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderlineheader_supplierprices_aud(
	purchaseorderlineheaderid		bigint NOT NULL,
	supplierpriceid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderlineheader_supplierprices_aud add constraint [PK_mend_purc_purchaseorderlineheader_supplierprices_aud]
	primary key clustered (purchaseorderlineheaderid,supplierpriceid);
go
alter table mend.purc_purchaseorderlineheader_supplierprices_aud add constraint [DF_mend_purc_purchaseorderlineheader_supplierprices_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------------- purchaseorders$purchaseorderline -------------------

create table mend.purc_purchaseorderline (
	id								bigint NOT NULL, 
	po_lineID						bigint, 
	stockItemID						bigint,
	quantity						decimal(28,8), 
	quantityReceived				decimal(28,8), 
	quantityDelivered				decimal(28,8), 
	quantityInTransit				decimal(28,8), 
	quantityPlanned					decimal(28,8), 
	quantityOpen					decimal(28,8), 
	quantityUnallocated				decimal(28,8), 
	quantityCancelled				decimal(28,8), 
	quantityExWorks					decimal(28,8), 
	quantityRejected				decimal(28,8),
	lineprice						decimal(28,8), 
	backtobackduedate				datetime2,
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderline add constraint [PK_mend_purc_purchaseorderline]
	primary key clustered (id);
go
alter table mend.purc_purchaseorderline add constraint [DF_mend_purc_purchaseorderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderline_aud(
	id								bigint NOT NULL, 
	po_lineID						bigint, 
	stockItemID						bigint,
	quantity						decimal(28,8), 
	quantityReceived				decimal(28,8), 
	quantityDelivered				decimal(28,8), 
	quantityInTransit				decimal(28,8), 
	quantityPlanned					decimal(28,8), 
	quantityOpen					decimal(28,8), 
	quantityUnallocated				decimal(28,8), 
	quantityCancelled				decimal(28,8), 
	quantityExWorks					decimal(28,8), 
	quantityRejected				decimal(28,8),
	lineprice						decimal(28,8), 
	backtobackduedate				datetime2,
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderline_aud add constraint [PK_mend_purc_purchaseorderline_aud]
	primary key clustered (id);
go
alter table mend.purc_purchaseorderline_aud add constraint [DF_mend_purc_purchaseorderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderline_aud_hist(
	id								bigint NOT NULL, 
	po_lineID						bigint, 
	stockItemID						bigint,
	quantity						decimal(28,8), 
	quantityReceived				decimal(28,8), 
	quantityDelivered				decimal(28,8), 
	quantityInTransit				decimal(28,8), 
	quantityPlanned					decimal(28,8), 
	quantityOpen					decimal(28,8), 
	quantityUnallocated				decimal(28,8), 
	quantityCancelled				decimal(28,8), 
	quantityExWorks					decimal(28,8), 
	quantityRejected				decimal(28,8),
	lineprice						decimal(28,8), 
	backtobackduedate				datetime2,
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.purc_purchaseorderline_aud_hist add constraint [DF_mend.purc_purchaseorderline_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderline_purchaseorderlineheader -------------------

create table mend.purc_purchaseorderline_purchaseorderlineheader (
	purchaseorderlineid				bigint NOT NULL,
	purchaseorderlineheaderid		bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderline_purchaseorderlineheader add constraint [PK_mend_purc_purchaseorderline_purchaseorderlineheader]
	primary key clustered (purchaseorderlineid,purchaseorderlineheaderid);
go
alter table mend.purc_purchaseorderline_purchaseorderlineheader add constraint [DF_mend_purc_purchaseorderline_purchaseorderlineheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderline_purchaseorderlineheader_aud(
	purchaseorderlineid				bigint NOT NULL,
	purchaseorderlineheaderid		bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderline_purchaseorderlineheader_aud add constraint [PK_mend_purc_purchaseorderline_purchaseorderlineheader_aud]
	primary key clustered (purchaseorderlineid,purchaseorderlineheaderid);
go
alter table mend.purc_purchaseorderline_purchaseorderlineheader_aud add constraint [DF_mend_purc_purchaseorderline_purchaseorderlineheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$purchaseorderline_stockitem -------------------

create table mend.purc_purchaseorderline_stockitem (
	purchaseorderlineid				bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.purc_purchaseorderline_stockitem add constraint [PK_mend_purc_purchaseorderline_stockitem]
	primary key clustered (purchaseorderlineid,stockitemid);
go
alter table mend.purc_purchaseorderline_stockitem add constraint [DF_mend_purc_purchaseorderline_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.purc_purchaseorderline_stockitem_aud(
	purchaseorderlineid				bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.purc_purchaseorderline_stockitem_aud add constraint [PK_mend_purc_purchaseorderline_stockitem_aud]
	primary key clustered (purchaseorderlineid,stockitemid);
go
alter table mend.purc_purchaseorderline_stockitem_aud add constraint [DF_mend_purc_purchaseorderline_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go