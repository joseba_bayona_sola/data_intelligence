
------------------------ Drop tables -------------------------------

use Landing
go

------------ warehouseshipments$receipt ------------

drop table mend.whship_receipt;
go
drop table mend.whship_receipt_aud;
go
drop table mend.whship_receipt_aud_hist;
go

------------ warehouseshipments$receipt_warehouse ------------

drop table mend.whship_receipt_warehouse;
go
drop table mend.whship_receipt_warehouse_aud;
go

------------ warehouseshipments$receipt_supplier ------------

drop table mend.whship_receipt_supplier;
go
drop table mend.whship_receipt_supplier_aud;
go

------------ inventory$warehousestockitembatch_receipt ------------

drop table mend.whship_warehousestockitembatch_receipt;
go
drop table mend.whship_warehousestockitembatch_receipt_aud;
go

------------ inventory$warehousestockitembatch_receipt ------------

drop table mend.whship_warehousestockitembatch_receipthistory;
go
drop table mend.whship_warehousestockitembatch_receipthistory_aud;
go

------------ inventory$warehousestockitembatch_receipt ------------

drop table mend.whship_backtobackreceipt_purchaseorder;
go
drop table mend.whship_backtobackreceipt_purchaseorder_aud;
go

------------ inventory$warehousestockitembatch_receipt ------------

drop table mend.whship_receiptforcancelleditems;
go
drop table mend.whship_receiptforcancelleditems_aud;
go







------------ warehouseshipments$receiptline ------------

drop table mend.whship_receiptline;
go
drop table mend.whship_receiptline_aud;
go
drop table mend.whship_receiptline_aud_hist;
go

------------ warehouseshipments$receiptline_shipment ------------

drop table mend.whship_receiptline_receipt;
go
drop table mend.whship_receiptline_receipt_aud;
go

------------ warehouseshipments$receiptline_supplier ------------

drop table mend.whship_receiptline_supplier;
go
drop table mend.whship_receiptline_supplier_aud;
go

------------ warehouseshipments$receiptline_purchaseorderline ------------

drop table mend.whship_receiptline_purchaseorderline;
go
drop table mend.whship_receiptline_purchaseorderline_aud;
go

------------ warehouseshipments$receiptline_snapreceiptline ------------

drop table mend.whship_receiptline_snapreceiptline;
go
drop table mend.whship_receiptline_snapreceiptline_aud;
go

------------ inventory$warehousestockitembatch_receiptline ------------

drop table mend.whship_warehousestockitembatch_receiptline;
go
drop table mend.whship_warehousestockitembatch_receiptline_aud;
go