
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ orderprocessing$order ------------

select *
from mend.order_order

select top 1000 id, 
	incrementID, magentoOrderID, orderStatus, 
	orderLinesMagento, orderLinesDeduped,
	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder,
	createdDate, changeddate, 
	idETLBatchRun, ins_ts
from mend.order_order

select top 1000 id, 
	incrementID, magentoOrderID, orderStatus, 
	orderLinesMagento, orderLinesDeduped,
	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder,
	createdDate, changeddate, 
	idETLBatchRun, ins_ts, upd_ts
from mend.order_order_aud

select id, 
	incrementID, magentoOrderID, orderStatus, 
	orderLinesMagento, orderLinesDeduped,
	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder,
	createdDate, changeddate, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_order_aud_hist

------------ orderprocessing$order_customer ------------

select	*
from mend.order_order_customer

select top 1000 orderid, retailcustomerid,
	idETLBatchRun, ins_ts
from mend.order_order_customer

select top 1000 orderid, retailcustomerid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_order_customer_aud

------------ orderprocessing$order_magwebstore ------------

select	*
from mend.order_order_magwebstore

select top 1000 orderid, magwebstoreid,
	idETLBatchRun, ins_ts
from mend.order_order_magwebstore

select top 1000 orderid, magwebstoreid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_order_magwebstore_aud

------------ orderprocessing$basevalues ------------

select *
from mend.order_basevalues

select top 1000 id, 
	subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
	discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
	shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
	adjustmentNegative, adjustmentPositive, 				
	custBalanceAmount, 
	customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, totalInvoiced, totalCanceled, totalRefunded,
	toOrderRate, toGlobalRate, currencyCode,
	mutualamount,
	idETLBatchRun, ins_ts
from mend.order_basevalues

select top 1000 id, 
	subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
	discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
	shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
	adjustmentNegative, adjustmentPositive, 					
	custBalanceAmount, 
	customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, totalInvoiced, totalCanceled, totalRefunded,
	toOrderRate, toGlobalRate, currencyCode,
	mutualamount,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_basevalues_aud

select id, 
	subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
	discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
	shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
	adjustmentNegative, adjustmentPositive, 					
	custBalanceAmount, 
	customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, totalInvoiced, totalCanceled, totalRefunded,
	toOrderRate, toGlobalRate, currencyCode,
	mutualamount,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_basevalues_aud_hist

------------ orderprocessing$basevalues_order ------------

select	*
from mend.order_basevalues_order

select top 1000 basevaluesid, orderid,
	idETLBatchRun, ins_ts
from mend.order_basevalues_order

select top 1000 basevaluesid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_basevalues_order_aud






------------ orderprocessing$order_statusupdate ------------

select *
from mend.order_order_statusupdate

select top 1000 id, status, timestamp,
	idETLBatchRun, ins_ts
from mend.order_order_statusupdate

select top 1000 id, status, timestamp,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_order_statusupdate_aud

select id, status, timestamp,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_order_statusupdate_aud_hist

------------ orderprocessing$statusupdate_order ------------

select	*
from mend.order_statusupdate_order

select top 1000 order_statusupdateid, orderid,
	idETLBatchRun, ins_ts
from mend.order_statusupdate_order

select top 1000 order_statusupdateid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_statusupdate_order_aud



------------ orderprocessing$orderprocessingtransaction ------------

select *
from mend.order_orderprocessingtransaction

select top 1000 id, orderReference, status, detail, timestamp,
	idETLBatchRun, ins_ts
from mend.order_orderprocessingtransaction

select top 1000 id, orderReference, status, detail, timestamp,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderprocessingtransaction_aud

select id, orderReference, status, detail, timestamp,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_orderprocessingtransaction_aud_hist

------------ orderprocessing$orderprocessingtransaction_order ------------

select	*
from mend.order_orderprocessingtransaction_order

select top 1000 orderprocessingtransactionid, orderid,
	idETLBatchRun, ins_ts
from mend.order_orderprocessingtransaction_order

select top 1000 orderprocessingtransactionid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderprocessingtransaction_order_aud






------------ orderprocessing$orderlineabstract ------------

select *
from mend.order_orderlineabstract

select top 1000 id, 
	magentoItemID, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
	basePrice, baseRowPrice, 
	allocated, 
	orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 
	netPrice, netRowPrice,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts
from mend.order_orderlineabstract

select top 1000 id, 
	magentoItemID, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
	basePrice, baseRowPrice, 
	allocated, 
	orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 
	netPrice, netRowPrice,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderlineabstract_aud

select id, 
	magentoItemID, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
	basePrice, baseRowPrice, 
	allocated, 
	orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 
	netPrice, netRowPrice,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_orderlineabstract_aud_hist

------------ orderprocessing$orderlinemagento ------------

select *
from mend.order_orderlinemagento

select top 1000 id, lineAdded, deduplicate, fulfilled,
	idETLBatchRun, ins_ts
from mend.order_orderlinemagento

select top 1000 id, lineAdded, deduplicate, fulfilled,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderlinemagento_aud

select id, lineAdded, deduplicate, fulfilled,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_orderlinemagento_aud_hist

------------ orderprocessing$orderlinemagento_order ------------

select *
from mend.order_orderlinemagento_order

select top 1000 orderlinemagentoid, orderid,
	idETLBatchRun, ins_ts
from mend.order_orderlinemagento_order

select top 1000 orderlinemagentoid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderlinemagento_order_aud


------------ orderprocessing$orderline ------------

select *
from mend.order_orderline

select top 1000 id, status, packsOrdered, packsAllocated, packsShipped,
	idETLBatchRun, ins_ts
from mend.order_orderline

select top 1000 id, status, packsOrdered, packsAllocated, packsShipped,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderline_aud

select id, status, packsOrdered, packsAllocated, packsShipped,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_orderline_aud_hist

------------ orderprocessing$orderline_order ------------

select *
from mend.order_orderline_order

select top 1000 orderlineid, orderid,
	idETLBatchRun, ins_ts
from mend.order_orderline_order

select top 1000 orderlineid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderline_order_aud






------------ orderprocessing$shippinggroup ------------

select *
from mend.order_shippinggroup

select top 1000 id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc, cancelled,
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.order_shippinggroup

select top 1000 id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc, cancelled,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_shippinggroup_aud

select id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc, cancelled,
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.order_shippinggroup_aud_hist

------------ orderprocessing$shippinggroup_order ------------

select *
from mend.order_shippinggroup_order

select top 1000 shippinggroupid, orderid,
	idETLBatchRun, ins_ts
from mend.order_shippinggroup_order

select top 1000 shippinggroupid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_shippinggroup_order_aud

------------ orderprocessing$orderlineabstract_shippinggroup ------------

select *
from mend.order_orderlineabstract_shippinggroup

select top 1000 orderlineabstractid, shippinggroupid,
	idETLBatchRun, ins_ts
from mend.order_orderlineabstract_shippinggroup

select top 1000 orderlineabstractid, shippinggroupid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderlineabstract_shippinggroup_aud




------------ orderprocessing$orderline_product ------------

select *
from mend.order_orderline_product

select top 1000 orderlineabstractid, productid,
	idETLBatchRun, ins_ts
from mend.order_orderline_product

select top 1000 orderlineabstractid, productid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderline_product_aud

------------ orderprocessing$orderline_requiredstockitem ------------

select *
from mend.order_orderline_requiredstockitem

select top 1000 orderlineabstractid, stockitemid,
	idETLBatchRun, ins_ts
from mend.order_orderline_requiredstockitem

select top 1000 orderlineabstractid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.order_orderline_requiredstockitem_aud