
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- vat$commodity -------------------

drop trigger mend.trg_vat_commodity;
go 

create trigger mend.trg_vat_commodity on mend.vat_commodity
after insert
as
begin
	set nocount on

	merge into mend.vat_commodity_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.commoditycode, 0), isnull(trg.commodityname, ' ')
		intersect
		select 
			isnull(src.commoditycode, 0), isnull(src.commodityname, ' '))
			
		then
			update set
				trg.commoditycode = src.commoditycode, trg.commodityname = src.commodityname, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				commoditycode, commodityname, 
				idETLBatchRun)

				values (src.id,
					src.commoditycode, src.commodityname,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_vat_commodity_aud;
go 

create trigger mend.trg_vat_commodity_aud on mend.vat_commodity_aud
for update, delete
as
begin
	set nocount on

	insert mend.vat_commodity_aud_hist (id,
		commoditycode, commodityname,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.commoditycode, d.commodityname,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- product$productfamily_commodity -------------------

drop trigger mend.trg_vat_productfamily_commodity;
go 

create trigger mend.trg_vat_productfamily_commodity on mend.vat_productfamily_commodity
after insert
as
begin
	set nocount on

	merge into mend.vat_productfamily_commodity_aud with (tablock) as trg
	using inserted src
		on (trg.productfamilyid = src.productfamilyid and trg.commodityid = src.commodityid)
	when not matched 
		then
			insert (productfamilyid, commodityid, 
				idETLBatchRun)

				values (src.productfamilyid, src.commodityid,
					src.idETLBatchRun);
end; 
go



------------------- vat$dispensingservicesrate -------------------

drop trigger mend.trg_vat_dispensingservicesrate;
go 

create trigger mend.trg_vat_dispensingservicesrate on mend.vat_dispensingservicesrate
after insert
as
begin
	set nocount on

	merge into mend.vat_dispensingservicesrate_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.dispensingservicesrate, 0), isnull(trg.rateeffectivedate, ' '), isnull(trg.rateenddate, ' ')
		intersect
		select 
			isnull(src.dispensingservicesrate, 0), isnull(src.rateeffectivedate, ' '), isnull(src.rateenddate, ' '))
			
		then
			update set
				trg.dispensingservicesrate = src.dispensingservicesrate, trg.rateeffectivedate = src.rateeffectivedate, trg.rateenddate = src.rateenddate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				dispensingservicesrate, rateeffectivedate, rateenddate, 
				idETLBatchRun)

				values (src.id,
					src.dispensingservicesrate, src.rateeffectivedate, src.rateenddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_vat_dispensingservicesrate_aud;
go 

create trigger mend.trg_vat_dispensingservicesrate_aud on mend.vat_dispensingservicesrate_aud
for update, delete
as
begin
	set nocount on

	insert mend.vat_dispensingservicesrate_aud_hist (id,
		dispensingservicesrate, rateeffectivedate, rateenddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.dispensingservicesrate, d.rateeffectivedate, d.rateenddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- vat$dispensingservices_commodity -------------------

drop trigger mend.trg_vat_dispensingservices_commodity;
go 

create trigger mend.trg_vat_dispensingservices_commodity on mend.vat_dispensingservices_commodity
after insert
as
begin
	set nocount on

	merge into mend.vat_dispensingservices_commodity_aud with (tablock) as trg
	using inserted src
		on (trg.dispensingservicesrateid = src.dispensingservicesrateid and trg.commodityid = src.commodityid)
	when not matched 
		then
			insert (dispensingservicesrateid, commodityid, 
				idETLBatchRun)

				values (src.dispensingservicesrateid, src.commodityid,
					src.idETLBatchRun);
end; 
go

------------------- vat$dispensingservicesrate_country -------------------

drop trigger mend.trg_vat_dispensingservicesrate_country;
go 

create trigger mend.trg_vat_dispensingservicesrate_country on mend.vat_dispensingservicesrate_country
after insert
as
begin
	set nocount on

	merge into mend.vat_dispensingservicesrate_country_aud with (tablock) as trg
	using inserted src
		on (trg.dispensingservicesrateid = src.dispensingservicesrateid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (dispensingservicesrateid, countryid, 
				idETLBatchRun)

				values (src.dispensingservicesrateid, src.countryid,
					src.idETLBatchRun);
end; 
go






------------------- vat$vatrating -------------------

drop trigger mend.trg_vat_vatrating;
go 

create trigger mend.trg_vat_vatrating on mend.vat_vatrating
after insert
as
begin
	set nocount on

	merge into mend.vat_vatrating_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.vatratingcode, ' '), isnull(trg.vatratingdescription, ' ')
		intersect
		select 
			isnull(src.vatratingcode, ' '), isnull(src.vatratingdescription, ' '))
			
		then
			update set
				trg.vatratingcode = src.vatratingcode, trg.vatratingdescription = src.vatratingdescription,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				vatratingcode, vatratingdescription, 
				idETLBatchRun)

				values (src.id,
					src.vatratingcode, src.vatratingdescription,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_vat_vatrating_aud;
go 

create trigger mend.trg_vat_vatrating_aud on mend.vat_vatrating_aud
for update, delete
as
begin
	set nocount on

	insert mend.vat_vatrating_aud_hist (id,
		vatratingcode, vatratingdescription,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.vatratingcode, d.vatratingdescription,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- vat$vatrating_country -------------------

drop trigger mend.trg_vat_vatrating_country;
go 

create trigger mend.trg_vat_vatrating_country on mend.vat_vatrating_country
after insert
as
begin
	set nocount on

	merge into mend.vat_vatrating_country_aud with (tablock) as trg
	using inserted src
		on (trg.vatratingid = src.vatratingid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (vatratingid, countryid, 
				idETLBatchRun)

				values (src.vatratingid, src.countryid,
					src.idETLBatchRun);
end; 
go




------------------- vat$vatrate -------------------

drop trigger mend.trg_vat_vatrate;
go 

create trigger mend.trg_vat_vatrate on mend.vat_vatrate
after insert
as
begin
	set nocount on

	merge into mend.vat_vatrate_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.vatrate, 0), isnull(trg.vatrateeffectivedate, ' '), isnull(trg.vatrateenddate, ' '),
			isnull(trg.createddate, ' '), isnull(trg.changeddate, ' '),
			isnull(trg.system$owner, 0), isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.vatrate, 0), isnull(src.vatrateeffectivedate, ' '), isnull(src.vatrateenddate, ' '),
			isnull(src.createddate, ' '), isnull(src.changeddate, ' '),
			isnull(src.system$owner, 0), isnull(src.system$changedby, 0))
			
		then
			update set
				trg.vatrate = src.vatrate, trg.vatrateeffectivedate = src.vatrateeffectivedate, trg.vatrateenddate = src.vatrateenddate,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.system$owner = src.system$owner, trg.system$changedby = src.system$changedby,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				vatrate, vatrateeffectivedate, vatrateenddate, 
				createddate, changeddate, 
				system$owner, system$changedby, 
				idETLBatchRun)

				values (src.id,
					src.vatrate, src.vatrateeffectivedate, src.vatrateenddate, 
					src.createddate, src.changeddate, 
					src.system$owner, src.system$changedby,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_vat_vatrate_aud;
go 

create trigger mend.trg_vat_vatrate_aud on mend.vat_vatrate_aud
for update, delete
as
begin
	set nocount on

	insert mend.vat_vatrate_aud_hist (id,
		vatrate, vatrateeffectivedate, vatrateenddate, 
		createddate, changeddate, 
		system$owner, system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.vatrate, d.vatrateeffectivedate, d.vatrateenddate, 
			d.createddate, d.changeddate, 
			d.system$owner, d.system$changedby,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- vat$vatrate_vatrating -------------------

drop trigger mend.trg_vat_vatrate_vatrating;
go 

create trigger mend.trg_vat_vatrate_vatrating on mend.vat_vatrate_vatrating
after insert
as
begin
	set nocount on

	merge into mend.vat_vatrate_vatrating_aud with (tablock) as trg
	using inserted src
		on (trg.vatrateid = src.vatrateid and trg.vatratingid = src.vatratingid)
	when not matched 
		then
			insert (vatrateid, vatratingid, 
				idETLBatchRun)

				values (src.vatrateid, src.vatratingid,
					src.idETLBatchRun);
end; 
go







------------------- vat$vatschedule -------------------

drop trigger mend.trg_vat_vatschedule;
go 

create trigger mend.trg_vat_vatschedule on mend.vat_vatschedule
after insert
as
begin
	set nocount on

	merge into mend.vat_vatschedule_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.vatsaletype, ' '), isnull(trg.taxationarea, ' '), isnull(trg.scheduleeffectivedate, ' '), isnull(trg.scheduleenddate, ' ') 
		intersect
		select 
			isnull(src.vatsaletype, ' '), isnull(src.taxationarea, ' '), isnull(src.scheduleeffectivedate, ' '), isnull(src.scheduleenddate, ' '))
			
		then
			update set
				trg.vatsaletype = src.vatsaletype, trg.taxationarea = src.taxationarea, trg.scheduleeffectivedate = src.scheduleeffectivedate, trg.scheduleenddate = src.scheduleenddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, 
				idETLBatchRun)

				values (src.id,
					src.vatsaletype, src.taxationarea, src.scheduleeffectivedate, src.scheduleenddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_vat_vatschedule_aud;
go 

create trigger mend.trg_vat_vatschedule_aud on mend.vat_vatschedule_aud
for update, delete
as
begin
	set nocount on

	insert mend.vat_vatschedule_aud_hist (id,
		vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.vatsaletype, d.taxationarea, d.scheduleeffectivedate, d.scheduleenddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- vat$vatschedule_vatrating -------------------

drop trigger mend.trg_vat_vatschedule_vatrating;
go 

create trigger mend.trg_vat_vatschedule_vatrating on mend.vat_vatschedule_vatrating
after insert
as
begin
	set nocount on

	merge into mend.vat_vatschedule_vatrating_aud with (tablock) as trg
	using inserted src
		on (trg.vatscheduleid = src.vatscheduleid and trg.vatratingid = src.vatratingid)
	when not matched 
		then
			insert (vatscheduleid, vatratingid, 
				idETLBatchRun)

				values (src.vatscheduleid, src.vatratingid,
					src.idETLBatchRun);
end; 
go

------------------- vat$vatschedule_commodity -------------------

drop trigger mend.trg_vat_vatschedule_commodity;
go 

create trigger mend.trg_vat_vatschedule_commodity on mend.vat_vatschedule_commodity
after insert
as
begin
	set nocount on

	merge into mend.vat_vatschedule_commodity_aud with (tablock) as trg
	using inserted src
		on (trg.vatscheduleid = src.vatscheduleid and trg.commodityid = src.commodityid)
	when not matched 
		then
			insert (vatscheduleid, commodityid, 
				idETLBatchRun)

				values (src.vatscheduleid, src.commodityid,
					src.idETLBatchRun);
end; 
go