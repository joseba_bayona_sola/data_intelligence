
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ product$productcategory ------------

select *
from mend.prod_productcategory

select id,
	code, name,
	idETLBatchRun, ins_ts
from mend.prod_productcategory

select id,
	code, name,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_productcategory_aud

select id,
	code, name,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_productcategory_aud_hist

------------ product$productfamily ------------

select *
from mend.prod_productfamily

select id,
	magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
	productFamilyType, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts
from mend.prod_productfamily

select id,
	magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
	productFamilyType, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_productfamily_aud

select id,
	magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
	productFamilyType, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_productfamily_aud_hist

------------ product$productfamily_productcategory ------------

select *
from mend.prod_productfamily_productcategory

select productfamilyid, productcategoryid,
	idETLBatchRun, ins_ts
from mend.prod_productfamily_productcategory

select productfamilyid, productcategoryid,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_productfamily_productcategory_aud




------------ product$product ------------

select *
from mend.prod_product

select id,
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts
from mend.prod_product

select top 1000 id,
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_product_aud

select id,
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_product_aud_hist

------------ product$product_productfamily ------------

select *
from mend.prod_product_productfamily

select productid, productfamilyid,
	idETLBatchRun, ins_ts
from mend.prod_product_productfamily

select productid, productfamilyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_product_productfamily_aud




------------ product$stockitem ------------

select *
from mend.prod_stockitem

select id,
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed,
	createdDate, changedDate, 
	idETLBatchRun, ins_ts
from mend.prod_stockitem

select top 1000 id,
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed,
	createdDate, changedDate, 
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_stockitem_aud

select id,
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed,
	createdDate, changedDate, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_stockitem_aud_hist

------------ product$stockitem_product ------------

select *
from mend.prod_stockitem_product

select stockitemid, productid,
	idETLBatchRun, ins_ts
from mend.prod_stockitem_product

select stockitemid, productid,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_stockitem_product_aud




------------ product$contactlens ------------

select *
from mend.prod_contactlens

select id, 
	bc, di, po, 
	cy, ax,  
	ad, _do, co, co_EDI,
	idETLBatchRun, ins_ts
from mend.prod_contactlens

select id, 
	bc, di, po, 
	cy, ax,  
	ad, _do, co, co_EDI,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_contactlens_aud

select id, 
	bc, di, po, 
	cy, ax,  
	ad, _do, co, co_EDI,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_contactlens_aud_hist





------------ product$packsize ------------

select *
from mend.prod_packsize

select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	height, width, weight, depth, 
	productGroup1, productGroup2, productGroup2Type, 
	breakable,
	packsperpallet, packspercarton, 
	idETLBatchRun, ins_ts
from mend.prod_packsize

select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	height, width, weight, depth, 
	productGroup1, productGroup2, productGroup2Type, 
	breakable,
	packsperpallet, packspercarton, 
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_packsize_aud

select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	height, width, weight, depth, 
	productGroup1, productGroup2, productGroup2Type, 
	breakable,
	packsperpallet, packspercarton, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.prod_packsize_aud_hist

------------ product$packsize_productfamily ------------

select *
from mend.prod_packsize_productfamily

select packsizeid, productfamilyid,
	idETLBatchRun, ins_ts
from mend.prod_packsize_productfamily

select packsizeid, productfamilyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_packsize_productfamily_aud

------------ product$stockitem_packsize ------------

select *
from mend.prod_stockitem_packsize

select packsizeid, stockitemid,
	idETLBatchRun, ins_ts
from mend.prod_stockitem_packsize

select packsizeid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.prod_stockitem_packsize_aud