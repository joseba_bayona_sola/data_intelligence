
------------------------ Drop tables -------------------------------

use Landing
go

------------ orderprocessing$order ------------

drop table mend.order_order;
go
drop table mend.order_order_aud;
go
drop table mend.order_order_aud_hist;
go

------------ orderprocessing$order_customer ------------

drop table mend.order_order_customer;
go
drop table mend.order_order_customer_aud;
go

------------ orderprocessing$order_magwebstore ------------

drop table mend.order_order_magwebstore;
go
drop table mend.order_order_magwebstore_aud;
go


------------ orderprocessing$basevalues ------------

drop table mend.order_basevalues;
go
drop table mend.order_basevalues_aud;
go
drop table mend.order_basevalues_aud_hist;
go

------------ orderprocessing$basevalues_order ------------

drop table mend.order_basevalues_order;
go
drop table mend.order_basevalues_order_aud;
go





------------ orderprocessing$order_statusupdate ------------

drop table mend.order_order_statusupdate;
go
drop table mend.order_order_statusupdate_aud;
go
drop table mend.order_order_statusupdate_aud_hist;
go

------------ orderprocessing$statusupdate_order ------------

drop table mend.order_statusupdate_order;
go
drop table mend.order_statusupdate_order_aud;
go

------------ orderprocessing$orderprocessingtransaction ------------

drop table mend.order_orderprocessingtransaction;
go
drop table mend.order_orderprocessingtransaction_aud;
go
drop table mend.order_orderprocessingtransaction_aud_hist;
go

------------ orderprocessing$orderprocessingtransaction_order ------------

drop table mend.order_orderprocessingtransaction_order;
go
drop table mend.order_orderprocessingtransaction_order_aud;
go





------------ orderprocessing$orderlineabstract ------------

drop table mend.order_orderlineabstract;
go
drop table mend.order_orderlineabstract_aud;
go
drop table mend.order_orderlineabstract_aud_hist;
go

------------ orderprocessing$orderlinemagento ------------

drop table mend.order_orderlinemagento;
go
drop table mend.order_orderlinemagento_aud;
go
drop table mend.order_orderlinemagento_aud_hist;
go

------------ orderprocessing$orderlinemagento_order ------------


drop table mend.order_orderlinemagento_order;
go
drop table mend.order_orderlinemagento_order_aud;
go

------------ orderprocessing$orderline ------------

drop table mend.order_orderline;
go
drop table mend.order_orderline_aud;
go
drop table mend.order_orderline_aud_hist;
go

------------ orderprocessing$orderline_order ------------

drop table mend.order_orderline_order;
go
drop table mend.order_orderline_order_aud;
go





------------ orderprocessing$shippinggroup ------------

drop table mend.order_shippinggroup;
go
drop table mend.order_shippinggroup_aud;
go
drop table mend.order_shippinggroup_aud_hist;
go

------------ orderprocessing$shippinggroup_order ------------

drop table mend.order_shippinggroup_order;
go
drop table mend.order_shippinggroup_order_aud;
go

------------ orderprocessing$orderlineabstract_shippinggroup ------------

drop table mend.order_orderlineabstract_shippinggroup;
go
drop table mend.order_orderlineabstract_shippinggroup_aud;
go





------------ orderprocessing$orderline_product ------------

drop table mend.order_orderline_product;
go
drop table mend.order_orderline_product_aud;
go

------------ orderprocessing$orderline_requiredstockitem ------------

drop table mend.order_orderline_requiredstockitem;
go
drop table mend.order_orderline_requiredstockitem_aud;
go