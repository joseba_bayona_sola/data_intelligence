
------------------------------ SP -----------------------------------

use Landing
go

------------------- purchaseorders$shortageheader -------------------

drop procedure mend.srcmend_lnd_get_short_shortageheader
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortageheader
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_short_shortageheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, wholesale, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id, wholesale 
					from public."purchaseorders$shortageheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

--------------- purchaseorders$shortageheader_supplier ---------------

drop procedure mend.srcmend_lnd_get_short_shortageheader_supplier
go

-- ============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ============================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortageheader_supplier
-- ============================================================================================

create procedure mend.srcmend_lnd_get_short_shortageheader_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageheaderid shortageheaderid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageheaderid", "suppliers$supplierid" 
					from public."purchaseorders$shortageheader_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ purchaseorders$shortageheader_sourcewarehouse ------------

drop procedure mend.srcmend_lnd_get_short_shortageheader_sourcewarehouse
go

-- ===================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortageheader_sourcewarehouse
-- ===================================================================================================

create procedure mend.srcmend_lnd_get_short_shortageheader_sourcewarehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageheaderid shortageheaderid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageheaderid", "inventory$warehouseid" 
					from public."purchaseorders$shortageheader_sourcewarehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ purchaseorders$shortageheader_destinationwarehouse ------------

drop procedure mend.srcmend_lnd_get_short_shortageheader_destinationwarehouse
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortageheader_destinationwarehouse
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortageheader_destinationwarehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageheaderid shortageheaderid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageheaderid", "inventory$warehouseid" 
					from public."purchaseorders$shortageheader_destinationwarehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortageheader_packsize -------------------

drop procedure mend.srcmend_lnd_get_short_shortageheader_packsize
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortageheader_packsize
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortageheader_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageheaderid shortageheaderid, product$packsizeid packsizeid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageheaderid", "product$packsizeid" 
					from public."purchaseorders$shortageheader_packsize"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go









------------------- purchaseorders$shortage -------------------

drop procedure mend.srcmend_lnd_get_short_shortage
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				shortageid, purchaseordernumber, ordernumbers,
				wholesale, 
				status, 
				unitquantity, packquantity, 
				requireddate, 
				createdDate, changedDate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						shortageid, purchaseordernumber, ordernumbers,
						wholesale, 
						status, 
						unitquantity, packquantity, 
						requireddate, 
						createdDate, changedDate
					from public."purchaseorders$shortage"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_shortageheader -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_shortageheader
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_shortageheader
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_shortageheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, purchaseorders$shortageheaderid shortageheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "purchaseorders$shortageheaderid" 
					from public."purchaseorders$shortage_shortageheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_purchaseorderline -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_purchaseorderline
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_purchaseorderline
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_purchaseorderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, purchaseorders$purchaseorderlineid purchaseorderlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "purchaseorders$purchaseorderlineid" 
					from public."purchaseorders$shortage_purchaseorderline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_supplier -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_supplier
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_supplier
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "suppliers$supplierid" 
					from public."purchaseorders$shortage_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_product -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_product
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_product
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, product$productid productid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "product$productid" 
					from public."purchaseorders$shortage_product"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_standardprice -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_standardprice
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_standardprice
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_standardprice
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, suppliers$standardpriceid standardpriceid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "suppliers$standardpriceid" 
					from public."purchaseorders$shortage_standardprice"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_stockitem -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_stockitem
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_stockitem
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "product$stockitemid" 
					from public."purchaseorders$shortage_stockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$shortage_warehousestockitem -------------------

drop procedure mend.srcmend_lnd_get_short_shortage_warehousestockitem
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_warehousestockitem
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_shortage_warehousestockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$shortageid shortageid, inventory$warehousestockitemid warehousestockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$shortageid", "inventory$warehousestockitemid" 
					from public."purchaseorders$shortage_warehousestockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------------- purchaseorders$orderlineshortage -------------------

drop procedure mend.srcmend_lnd_get_short_orderlineshortage
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_shortage_warehousestockitem
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_orderlineshortage
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				orderlineshortageid,
				orderlineshortagetype,
				unitquantity, packquantity,
				shortageprogress, adviseddate, 
				createdDate, changedDate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						orderlineshortageid,
						orderlineshortagetype,
						unitquantity, packquantity,
						shortageprogress, adviseddate, 
						createdDate, changedDate
					from public."purchaseorders$orderlineshortage"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$orderlineshortage_shortage -------------------

drop procedure mend.srcmend_lnd_get_short_orderlineshortage_shortage
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_orderlineshortage_shortage
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_orderlineshortage_shortage
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$orderlineshortageid orderlineshortageid, purchaseorders$shortageid shortageid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$orderlineshortageid", "purchaseorders$shortageid" 
					from public."purchaseorders$orderlineshortage_shortage"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$orderlineshortage_orderline -------------------

drop procedure mend.srcmend_lnd_get_short_orderlineshortage_orderline
go

-- ========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 19-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - short_orderlineshortage_orderline
-- ========================================================================================================

create procedure mend.srcmend_lnd_get_short_orderlineshortage_orderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$orderlineshortageid orderlineshortageid, orderprocessing$orderlineid orderlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$orderlineshortageid", "orderprocessing$orderlineid" 
					from public."purchaseorders$orderlineshortage_orderline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go