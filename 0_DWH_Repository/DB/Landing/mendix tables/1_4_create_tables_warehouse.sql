
---------- Script creacion de tablas ----------

use Landing
go

------------ inventory$warehouse ------------

create table mend.wh_warehouse (
	id								bigint NOT NULL,
	code							bigint,
	name							varchar(200),
	shortName						varchar(200),
	warehouseType					varchar(11),
	snapWarehouse					bit,
	snapCode						varchar(200),
	scurriCode						varchar(200),
	active							bit,
	wms_active						bit,
	useHoldingLabels				bit,
	stockcheckignore				bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehouse add constraint [PK_mend_wh_warehouse]
	primary key clustered (id);
go
alter table mend.wh_warehouse add constraint [DF_mend_wh_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehouse_aud(
	id								bigint NOT NULL,
	code							bigint,
	name							varchar(200),
	shortName						varchar(200),
	warehouseType					varchar(11),
	snapWarehouse					bit,
	snapCode						varchar(200),
	scurriCode						varchar(200),
	active							bit,
	wms_active						bit,
	useHoldingLabels				bit,
	stockcheckignore				bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehouse_aud add constraint [PK_mend_wh_warehouse_aud]
	primary key clustered (id);
go
alter table mend.wh_warehouse_aud add constraint [DF_mend_wh_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehouse_aud_hist(
	id								bigint NOT NULL,
	code							bigint,
	name							varchar(200),
	shortName						varchar(200),
	warehouseType					varchar(11),
	snapWarehouse					bit,
	snapCode						varchar(200),
	scurriCode						varchar(200),
	active							bit,
	wms_active						bit,
	useHoldingLabels				bit,
	stockcheckignore				bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_warehouse_aud_hist add constraint [DF_mend.wh_warehouse_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ inventory$warehousesupplier_warehouse ------------

create table mend.wh_warehousesupplier_warehouse (
	warehousesupplierid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousesupplier_warehouse add constraint [PK_mend_wh_warehousesupplier_warehouse]
	primary key clustered (warehousesupplierid, warehouseid);
go
alter table mend.wh_warehousesupplier_warehouse add constraint [DF_mend_wh_warehousesupplier_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousesupplier_warehouse_aud(
	warehousesupplierid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,	
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousesupplier_warehouse_aud add constraint [PK_mend_wh_warehousesupplier_warehouse_aud]
	primary key clustered (warehousesupplierid, warehouseid);
go
alter table mend.wh_warehousesupplier_warehouse_aud add constraint [DF_mend_wh_warehousesupplier_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ inventory$warehousesupplier_supplier ------------

create table mend.wh_warehousesupplier_supplier (
	warehousesupplierid				bigint NOT NULL, 
	supplierid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousesupplier_supplier add constraint [PK_mend_wh_warehousesupplier_supplier]
	primary key clustered (warehousesupplierid, supplierid);
go
alter table mend.wh_warehousesupplier_supplier add constraint [DF_mend_wh_warehousesupplier_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousesupplier_supplier_aud(
	warehousesupplierid				bigint NOT NULL, 
	supplierid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousesupplier_supplier_aud add constraint [PK_mend_wh_warehousesupplier_supplier_aud]
	primary key clustered (warehousesupplierid, supplierid);
go
alter table mend.wh_warehousesupplier_supplier_aud add constraint [DF_mend_wh_warehousesupplier_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go









------------ inventory$supplierroutine ------------

create table mend.wh_supplierroutine (
	id								bigint NOT NULL,
	supplierName					varchar(200),
	mondayDefaultCanSupply			bit,
	mondayOrderCutOffTime			datetime2,
	mondayDefaultCanOrder			bit,
	tuesdayDefaultCanSupply			bit,
	tuesdayOrderCutOffTime			datetime2,
	tuesdayDefaultCanOrder			bit,
	wednesdayDefaultCanSupply		bit,
	wednesdayOrderCutOffTime		datetime2,
	wednesdayDefaultCanOrder		bit,
	thursdayDefaultCanSupply		bit,
	thursdayOrderCutOffTime			datetime2,
	thursdayDefaultCanOrder			bit,
	fridayDefaultCanSupply			bit,
	fridayOrderCutOffTime			datetime2,
	fridayDefaultCanOrder			bit,
	saturdayDefaultCanSupply		bit,
	saturdayOrderCutOffTime			datetime2,
	saturdayDefaultCanOrder			bit,
	sundayDefaultCanSupply			bit,
	sundayOrderCutOffTime			datetime2,
	sundayDefaultCanOrder			bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_supplierroutine add constraint [PK_mend_wh_supplierroutine]
	primary key clustered (id);
go
alter table mend.wh_supplierroutine add constraint [DF_mend_wh_supplierroutine_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_supplierroutine_aud(
	id								bigint NOT NULL,
	supplierName					varchar(200),
	mondayDefaultCanSupply			bit,
	mondayOrderCutOffTime			datetime2,
	mondayDefaultCanOrder			bit,
	tuesdayDefaultCanSupply			bit,
	tuesdayOrderCutOffTime			datetime2,
	tuesdayDefaultCanOrder			bit,
	wednesdayDefaultCanSupply		bit,
	wednesdayOrderCutOffTime		datetime2,
	wednesdayDefaultCanOrder		bit,
	thursdayDefaultCanSupply		bit,
	thursdayOrderCutOffTime			datetime2,
	thursdayDefaultCanOrder			bit,
	fridayDefaultCanSupply			bit,
	fridayOrderCutOffTime			datetime2,
	fridayDefaultCanOrder			bit,
	saturdayDefaultCanSupply		bit,
	saturdayOrderCutOffTime			datetime2,
	saturdayDefaultCanOrder			bit,
	sundayDefaultCanSupply			bit,
	sundayOrderCutOffTime			datetime2,
	sundayDefaultCanOrder			bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_supplierroutine_aud add constraint [PK_mend_wh_supplierroutine_aud]
	primary key clustered (id);
go
alter table mend.wh_supplierroutine_aud add constraint [DF_mend_wh_supplierroutine_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_supplierroutine_aud_hist(
	id								bigint NOT NULL,
	supplierName					varchar(200),
	mondayDefaultCanSupply			bit,
	mondayOrderCutOffTime			datetime2,
	mondayDefaultCanOrder			bit,
	tuesdayDefaultCanSupply			bit,
	tuesdayOrderCutOffTime			datetime2,
	tuesdayDefaultCanOrder			bit,
	wednesdayDefaultCanSupply		bit,
	wednesdayOrderCutOffTime		datetime2,
	wednesdayDefaultCanOrder		bit,
	thursdayDefaultCanSupply		bit,
	thursdayOrderCutOffTime			datetime2,
	thursdayDefaultCanOrder			bit,
	fridayDefaultCanSupply			bit,
	fridayOrderCutOffTime			datetime2,
	fridayDefaultCanOrder			bit,
	saturdayDefaultCanSupply		bit,
	saturdayOrderCutOffTime			datetime2,
	saturdayDefaultCanOrder			bit,
	sundayDefaultCanSupply			bit,
	sundayOrderCutOffTime			datetime2,
	sundayDefaultCanOrder			bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_supplierroutine_aud_hist add constraint [DF_mend.wh_supplierroutine_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$supplierroutine_warehousesupplier ------------

create table mend.wh_supplierroutine_warehousesupplier (
	supplierRoutineID				bigint NOT NULL,			
	warehouseSupplierID				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_supplierroutine_warehousesupplier add constraint [PK_mend_wh_supplierroutine_warehousesupplier]
	primary key clustered (supplierRoutineID, warehouseSupplierID);
go
alter table mend.wh_supplierroutine_warehousesupplier add constraint [DF_mend_wh_supplierroutine_warehousesupplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_supplierroutine_warehousesupplier_aud(
	supplierRoutineID				bigint NOT NULL,			
	warehouseSupplierID				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_supplierroutine_warehousesupplier_aud add constraint [PK_mend_wh_supplierroutine_warehousesupplier_aud]
	primary key clustered (supplierRoutineID, warehouseSupplierID);
go
alter table mend.wh_supplierroutine_warehousesupplier_aud add constraint [DF_mend_wh_supplierroutine_warehousesupplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go