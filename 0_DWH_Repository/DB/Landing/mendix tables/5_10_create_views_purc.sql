use Landing
go 

------------ purchaseorders$purchaseorder ------------

drop view mend.purc_purchaseorder_aud_v
go 

create view mend.purc_purchaseorder_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		ordernumber, source, status, potype,
		leadtime, 
		itemcost, totalprice, formattedprice, 
		duedate, receiveddate, createddate, 
		approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
		transferleadtime, transitleadtime, 
		spoleadtime, spoduedate, 
		tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
		requiredshipdate, requiredtransferdate, 
		estimatedpaymentdate, estimateddepositdate,
		supplierreference,
		changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			ordernumber, source, status, potype,
			leadtime, 
			itemcost, totalprice, formattedprice, 
			duedate, receiveddate, createddate, 
			approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
			transferleadtime, transitleadtime, 
			spoleadtime, spoduedate, 
			tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
			requiredshipdate, requiredtransferdate, 
			estimatedpaymentdate, estimateddepositdate,
			supplierreference,
			changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.purc_purchaseorder_aud
		union
		select 'H' record_type, id, 
			ordernumber, source, status, potype,
			leadtime, 
			itemcost, totalprice, formattedprice, 
			duedate, receiveddate, createddate, 
			approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
			transferleadtime, transitleadtime, 
			spoleadtime, spoduedate, 
			tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
			requiredshipdate, requiredtransferdate, 
			estimatedpaymentdate, estimateddepositdate,
			supplierreference,
			changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorder_aud_hist) t
go




------------------- purchaseorders$purchaseorderlineheader -------------------

drop view mend.purc_purchaseorderlineheader_aud_v
go

create view mend.purc_purchaseorderlineheader_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		status, problems, 
		lines, items, totalprice, 
		createddate, changeddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			status, problems, 
			lines, items, totalprice, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.purc_purchaseorderlineheader_aud
		union
		select 'H' record_type, id, 
			status, problems, 
			lines, items, totalprice, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorderlineheader_aud_hist) t
go




------------------- purchaseorders$purchaseorderline -------------------

drop view mend.purc_purchaseorderline_aud_v
go

create view mend.purc_purchaseorderline_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		po_lineID, stockItemID,
		quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
		lineprice, 
		backtobackduedate,
		createdDate, changedDate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			po_lineID, stockItemID,
			quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
			lineprice, 
			backtobackduedate,
			createdDate, changedDate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.purc_purchaseorderline_aud
		union
		select 'H' record_type, id, 
			po_lineID, stockItemID,
			quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
			lineprice, 
			backtobackduedate,
			createdDate, changedDate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorderline_aud_hist) t
go