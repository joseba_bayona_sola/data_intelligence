
--------------- WAREHOUSE RELATED -----------------------------

	select warehouseid, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore
	from Landing.mend.gen_wh_warehouse_v
	order by code

	select warehouseid, defaultcalendarid, 
		code, name, warehouseType, 
		mondayWorking, mondayCutOff, 
		tuesdayWorking, tuesdayCutOff, 
		wednesdayWorking, wednesdayCutOff, 
		thursdayWorking, thursdayCutOff, 
		fridayWorking, fridayCutOff, 
		saturdayWorking, saturdayCutOff, 
		sundayWorking, sundayCutOff, 
		timeHorizonWeeks
	from Landing.mend.gen_wh_warehouse_calendar_v
	order by code

	select warehouseid, shippingmethodid, 
		code, name, warehouseType, 
		magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
		leadtime, letterRate1, letterRate2, tracked, invoicedocs,
		defaultweight, minweight, scurrimappable,
		mondayDefaultCutOff, mondayWorking, mondayDeliveryWorking, mondayCollectionTime, 
		tuesdayDefaultCutOff, tuesdayWorking, tuesdayDeliveryWorking, tuesdayCollectionTime,
		wednesdayDefaultCutOff, wednesdayWorking, wednesdayDeliveryWorking, wednesdayCollectionTime,
		thursdayDefaultCutOff, thursdayWorking, thursdayDeliveryWorking, thursdayCollectionTime,
		fridayDefaultCutOff, fridayWorking, fridayDeliveryWorking, fridayCollectionTime,
		saturdayDefaultCutOff, saturdayWorking, saturdayDeliveryWorking, saturdayCollectionTime,
		sundayDefaultCutOff, sundayWorking, sundayDeliveryWorking, sundayCollectionTime
	from Landing.mend.gen_wh_warehouse_shippingmethod_v 
	order by code, magentoShippingDescription

	select warehouseid, supplierroutineid, 
		code, name, warehouseType, 
		supplierName, 
		mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
		tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
		wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
		thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
		fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder, 
		saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder, 
		sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder
	from Landing.mend.gen_wh_warehouse_supplier_v
	order by code, supplierName


--------------- SUPPLIER RELATED -----------------------------

	select supplier_id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		defaultLeadTime,
		flatFileConfigurationToUse,		
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime
	from Landing.mend.gen_supp_supplier_v
	order by supplierName

	select supplier_id, supplierpriceid,
		supplierID, supplierName, currencyCode, supplierType, 
		subMetaObjectName,
		unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
		directPrice, expiredPrice,
		totallt, shiplt, transferlt,
		comments,
		lastUpdated,
		createdDate, changedDate, 
		moq, effectiveDate, expiryDate, maxqty
	from Landing.mend.gen_supp_supplierprice_v
	order by supplierName, subMetaObjectName, unitPrice

--------------- PRODUCT RELATED -----------------------------

	select productfamilyid, packsizeid, 
		productfamilysizerank,
		magentoProductID_int,
		magentoProductID, magentoSKU, name, status,
		description, size, 
		packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
		weight, height, width, depth 
	from Landing.mend.gen_prod_productfamilypacksize_v 
	order by magentoProductID_int, productFamilySizeRank
	-- order by packsizeid

	select productfamilyid, packsizeid, supplier_id, supplierpriceid,
		magentoProductID_int, magentoSKU, name, status, productFamilySizeRank, size, 
		supplierID, supplierName, subMetaObjectName, unitPrice, currencyCode, leadTime
	from Landing.mend.gen_prod_productfamilypacksize_supplierprice_v
	order by magentoProductID_int, productFamilySizeRank, supplierName


	select top 1000 productfamilyid, productid,
		magentoProductID_int,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI
	from Landing.mend.gen_prod_product_v
	where magentoProductID_int = 1083
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co

	select top 1000 productfamilyid, productid, stockitemid, 
		magentoProductID_int,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		packSize, SKU, -- si.oldSKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		manufacturerArticleID, manufacturerCodeNumber,
		SNAPUploadStatus
	from Landing.mend.gen_prod_stockitem_v
	where magentoProductID_int = 1083
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize


--------------- STOCK RELATED -----------------------------

	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod
	from Landing.mend.gen_wh_warehousestockitem_v 
	where magentoProductID_int = 1083
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id,
		fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, -- wsib.forwardDemand, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency
	from Landing.mend.gen_wh_warehousestockitembatch_v 
	where magentoProductID_int = 1083
		and sku = '01083B2D4CT0000000000301'
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code, batch_id

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, batchstockissueid,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id,
		fullyallocated, fullyIssued, status, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency, 
		issueid, createddate, issuedquantity_issue 
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v
	where magentoProductID_int = 1083
		and sku = '01083B2D4CT0000000000301'
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code, batch_id, issueid

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, batchstockissueid, batchstockallocationid,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id,
		fullyallocated, fullyIssued, status, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency, 
		issueid, createddate, issuedquantity_issue, 
		fullyissued_alloc, cancelled, allocatedquantity_alloc, allocatedunits, issuedquantity_alloc 
	from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v
	where magentoProductID_int = 1083
		and sku = '01083B2D4CT0000000000301'
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code, batch_id, issueid

	select repairstockbatchsid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, 
		batchtransaction_id, reference, date,
		oldallocation, newallocation, oldissue, newissue, processed, 

		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id,
		fullyallocated, fullyIssued, status, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code, batch_id, reference

	select stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		dateCreated, dateClosed,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod
	from Landing.mend.gen_wh_stockmovement_v
	order by dateCreated
	
	select batchstockmovementid, stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		moveid, movelineid, batchstockmovement_id, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		dateCreated, dateClosed,
		quantity, comment,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id
	from Landing.mend.gen_wh_batchstockmovement_v
	order by moveid

--------------- ORDER RELATED -----------------------------

	select top 1000 orderid, basevaluesid, retailcustomerid, magwebstoreid,
		magentoOrderID_int, 
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		_type, shipmentStatus, 
		allocationStatus, allocationStrategy, allocatedDate, 
		labelRenderer,  
		shippingMethod, shippingDescription, 
		subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
		discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
		shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
		adjustmentNegative, adjustmentPositive, 					
		custBalanceAmount, 
		customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
		grandTotal, totalInvoiced, totalCanceled, totalRefunded,
		toOrderRate, toGlobalRate, currencyCode
	from Landing.mend.gen_order_order_v
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	-- where magentoOrderID_int = 5485780847
	order by magentoOrderID_int desc
	

	select top 1000 orderid, basevaluesid, retailcustomerid, magwebstoreid, orderlineid, productid,
		magentoOrderID_int, 
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		status, packsOrdered, packsAllocated, packsShipped,
		magentoProductID_int, name, sku, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName
	from Landing.mend.gen_order_orderline_v
	-- where magentoOrderID_int between 5280000 and 5280393
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	order by magentoOrderID_int desc

	select top 1000 orderid, basevaluesid, retailcustomerid, magwebstoreid, orderlinemagentoid, productid,
		magentoOrderID_int, 
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		lineAdded, deduplicate, fulfilled,
		sku, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName
	from Landing.mend.gen_order_orderlinemagento_v
	-- where magentoOrderID_int between 5280000 and 5280393
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	order by magentoOrderID_int desc

--------------- SHIPMENT RELATED -----------------------------

	select top 1000 customershipmentid, orderid, 
		magentoOrderID_int, incrementID, createdDate_order, orderStatus,
		orderIncrementID, shipmentNumber, 
		status, warehouseMethod, labelRenderer, 
		shippingMethod, shippingDescription, 
		notify, fullyShipped, syncedToMagento, 	
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		snap_timestamp,
		createdDate, changedDate
	from Landing.mend.gen_ship_customershipment_v
	order by magentoOrderID_int desc

--------------- ALLOCATION RELATED -----------------------------

	select top 1000 orderlinestockallocationtransactionid, batchstockallocationid
		warehouseid, orderid, orderlineid, warehousestockitemid, warehousestockitembatchid, packsizeid, -- customershipmentlineid, 
		magentoOrderID_int, incrementID, wh_name,
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime
	from Landing.mend.gen_all_stockallocationtransaction_v 
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)

	select top 1000 magentoallocationid, orderlinestockallocationtransactionid, batchstockallocationid,
		shipped, cancelled_magentoall, 
		quantity,
		warehouseid, orderid, orderlineid, warehousestockitemid, packsizeid, -- customershipmentlineid, 
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime
	from Landing.mend.gen_all_magentoallocation_v
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)

--------------- PURCHASE ORDERS RELATED -----------------------------

	select purchaseorderid, warehouseid, supplier_id, 
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, 
		leadtime, 
		itemcost, totalprice, formattedprice, 
		duedate, receiveddate, createddate, 
		approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby
	from Landing.mend.gen_purc_purchaseorder_v 
	order by ordernumber

	select purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, createddate,
		leadtime, 
		itemcost, totalprice, 
		status_lh, problems, 
		lines, items, total_price_lh
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	order by ordernumber, supplierid

	select top 1000 purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid, stockitemid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, createddate,
		leadtime, 
		itemcost, totalprice, 
		status_lh, problems, 
		lines, items, total_price_lh, 
		po_lineID, 
		quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
		lineprice, 
		backtobackduedate
	from Landing.mend.gen_purc_purchaseorderline_v
	order by ordernumber, supplierid, po_lineid

--------------- SHORTAGE RELATED -----------------------------

	select shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName
	from Landing.mend.gen_short_shortageheader_v 
	order by shortageheaderid

	select shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate
	from Landing.mend.gen_short_shortage_v
	order by shortageheaderid, shortageid

	select orderlineshortageid, shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, orderlineid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate,
		orderlineshortage_id,
		orderlineshortagetype,
		unitquantity_ol, packquantity_ol,
		shortageprogress, adviseddate, 
		createdDate_ol
	from Landing.mend.gen_short_orderlineshortage_v
	order by shortageheaderid, shortageid, orderlineshortage_id


--------------- WAREHOUSE SHIPMENT RELATED -----------------------------

	select shipmentid, warehouseid, supplier_id, -- warehousestockitembatchid,
		shipment_ID, orderNumber, receiptID, receiptNumber, invoiceRef,
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		auditComment,
		createdDate
	from Landing.mend.gen_whship_shipment_v 
	order by createddate desc

	select top 10000 shipmentid, warehouseid, supplier_id, warehousestockitembatchid,
		shipment_ID, orderNumber, receiptID, receiptNumber, invoiceRef,
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		auditComment,
		createdDate
	from Landing.mend.gen_whship_shipment_warehousestockitembatch_v
	order by createddate desc, warehousestockitembatchid

	select top 1000 shipmentorderlineid, shipmentid, warehouseid, supplier_id, purchaseorderlineid, snapreceiptlineid, warehousestockitembatchid,
		shipment_ID, orderNumber, receiptID, receiptNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, 
		createdDate,
		line, stockItem,
		status_ol, syncStatus, processed, 
		quantityOrdered, quantityReceived, quantityAccepted, quantityRejected, quantityReturned, 
		unitCost
	from Landing.mend.gen_whship_shipmentorderline_v
	order by createddate desc, line


--------------- INTERSITE TRANSFERS RELATED -----------------------------
	
	select transferheaderid, purchaseorderid_s, purchaseorderid_t, 
		transfernum, allocatedstrategy, 
		totalremaining,
		createddate, 
		code_s, warehouse_name_s, supplierID_s, supplierName_s, ordernumber_s, 
		code_t, warehouse_name_t, supplierID_t, supplierName_t, ordernumber_t
	from Landing.mend.gen_inter_transferheader_v
	order by transfernum

	select intransitbatchheaderid, transferheaderid, purchaseorderid_s, purchaseorderid_t, -- customershipmentid, orderid, 
		transfernum, allocatedstrategy, totalremaining, createddate, ordernumber_s, ordernumber_t,
		createddate_ibh
		--, magentoOrderID_int, incrementID
	from Landing.mend.gen_inter_intransitbatchheader_v
	order by transfernum, createddate_ibh

	select intransitstockitembatchid, transferheaderid, intransitbatchheaderid, purchaseorderid_s, purchaseorderid_t, customershipmentid, stockitemid, shipmentid,
		transfernum, allocatedstrategy, totalremaining, createddate, ordernumber_s, ordernumber_t, 
		createddate_ibh, magentoOrderID_int, incrementID,
		issuedquantity, remainingquantity, 
		used,
		productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
		currency,
		groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
		createddate_isib
	from Landing.mend.gen_inter_intransitstockitembatch_v
	order by transfernum, createddate_ibh, stockitemid

--------------- SNAP RECEIPT RELATED -----------------------------

	select snapreceiptid, shipmentid,
		receiptid, 
		lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail,  priority, 
		status, stockstatus, 
		ordertype, orderclass, 
		suppliername, consignmentID, 
		weight, actualWeight, volume, 
		dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
		datesfixed
	from Landing.mend.gen_rec_snapreceipt_v
	order by receiptid

	select snapreceiptlineid, snapreceiptid, shipmentid, stockitemid,
		receiptid, lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail, ordertype, orderclass, 
		line, skuid, 
		status, level, unitofmeasure, 
		qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected
	from Landing.mend.gen_rec_snapreceiptline_v
	order by receiptid, line

--------------- VAT RELATED -----------------------------

	select commodityid, productfamilyid,
		commoditycode, commodityname, 
		magentoProductID_int, magentoProductID, magentoSKU, name
	from Landing.mend.gen_vat_commodity_v
	order by commoditycode, magentoProductID_int

	select dispensingservicesrateid, commodityid, productfamilyid, countryid,
		commoditycode, commodityname, dispensingservicesrate, rateeffectivedate, rateenddate,
		magentoProductID_int, magentoProductID, magentoSKU, name
	from Landing.mend.gen_vat_dispensingservicesrate_v
	order by commoditycode, magentoProductID_int

	select vatratingid, countryid, vatrateid,
		vatratingcode, vatratingdescription, 
		countryname, 
		vatrate, vatrateeffectivedate, vatrateenddate
	from Landing.mend.gen_vat_vatrating_v
	order by countryname, vatratingdescription

	select vatscheduleid, vatratingid, countryid, vatrateid, commodityid,
		vatratingcode, vatratingdescription, countryname, vatrate, vatrateeffectivedate, vatrateenddate,
		commoditycode, commodityname,
		vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate
	from Landing.mend.gen_vat_vatschedule_v
	order by countryname, vatratingdescription, commoditycode, vatsaletype

