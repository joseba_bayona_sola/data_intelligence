
------------------------------- Triggers ----------------------------------

use Landing
go

------------ product$productcategory ------------

drop trigger mend.trg_prod_productcategory;
go 

create trigger mend.trg_prod_productcategory on mend.prod_productcategory
after insert
as
begin
	set nocount on

	merge into mend.prod_productcategory_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.code, 0), isnull(trg.name, ' ')
		intersect
		select 
			isnull(src.code, 0), isnull(src.name, ' '))	
		then
			update set
				trg.code = src.code, trg.name = src.name, trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
						code, name,
						idETLBatchRun)

				values (src.id,
							src.code, src.name,
							src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_productcategory_aud;
go 

create trigger mend.trg_prod_productcategory_aud on mend.prod_productcategory_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_productcategory_aud_hist (id,
		code, name,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.code, d.name,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$productfamily ------------

drop trigger mend.trg_prod_productfamily;
go 

create trigger mend.trg_prod_productfamily on mend.prod_productfamily
after insert
as
begin
	set nocount on

	merge into mend.prod_productfamily_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.magentoProductID, ' '), isnull(trg.magentoSKU, ' '), isnull(trg.name, ' '), isnull(trg.manufacturerProductFamily, ' '), isnull(trg.manufacturer, ' '), isnull(trg.status, ' '),
			isnull(trg.productFamilyType, ' '),
			isnull(trg.promotionalProduct, ' '), isnull(trg.updateSNAPDescription, ' '), isnull(trg.createToOrder, ' '),
			isnull(trg.createdDate, ' '), -- isnull(trg.changedDate, ' '),
			isnull(trg.system$owner, 0), isnull(trg.system$changedBy, 0)

		intersect
		select 
			isnull(src.magentoProductID, ' '), isnull(src.magentoSKU, ' '), isnull(src.name, ' '), isnull(src.manufacturerProductFamily, ' '), isnull(trg.manufacturer, ' '), isnull(src.status, ' '),
			isnull(src.productFamilyType, ' '),
			isnull(src.promotionalProduct, ' '), isnull(src.updateSNAPDescription, ' '), isnull(src.createToOrder, ' '),
			isnull(src.createdDate, ' '), -- isnull(src.changedDate, ' '),
			isnull(src.system$owner, 0), isnull(src.system$changedBy, 0))	
		then
			update set
				trg.magentoProductID = src.magentoProductID, trg.magentoSKU = src.magentoSKU, trg.name = src.name, 
				trg.manufacturerProductFamily = src.manufacturerProductFamily, trg.manufacturer = src.manufacturer, trg.status = src.status, 
				trg.productFamilyType = src.productFamilyType,
				trg.promotionalProduct = src.promotionalProduct, trg.updateSNAPDescription = src.updateSNAPDescription, trg.createToOrder = src.createToOrder, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$owner = src.system$owner, trg.system$changedBy = src.system$changedBy,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
				productFamilyType, 
				promotionalProduct, updateSNAPDescription, createToOrder, 
				createdDate, changedDate, 
				system$owner, system$changedBy,
				idETLBatchRun)

				values (src.id,
					src.magentoProductID, src.magentoSKU, src.name, src.manufacturerProductFamily, src.manufacturer, src.status, 
					src.productFamilyType, 
					src.promotionalProduct, src.updateSNAPDescription, src.createToOrder, 
					src.createdDate, src.changedDate, 
					src.system$owner, src.system$changedBy,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_productfamily_aud;
go 

create trigger mend.trg_prod_productfamily_aud on mend.prod_productfamily_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_productfamily_aud_hist (id,
		magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
		productFamilyType, 
		promotionalProduct, updateSNAPDescription, createToOrder, 
		createdDate, changedDate, 
		system$owner, system$changedBy,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.magentoProductID, d.magentoSKU, d.name, d.manufacturerProductFamily, d.manufacturer, d.status, 
			d.productFamilyType, 
			d.promotionalProduct, d.updateSNAPDescription, d.createToOrder, 
			d.createdDate, d.changedDate, 
			d.system$owner, d.system$changedBy,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$productfamily_productcategory ------------

drop trigger mend.trg_prod_productfamily_productcategory;
go 

create trigger mend.trg_prod_productfamily_productcategory on mend.prod_productfamily_productcategory
after insert
as
begin
	set nocount on

	merge into mend.prod_productfamily_productcategory_aud with (tablock) as trg
	using inserted src
		on (trg.productfamilyid = src.productfamilyid and trg.productcategoryid = src.productcategoryid)
	when not matched 
		then
			insert (productfamilyid, productcategoryid, 
				idETLBatchRun)

				values (src.productfamilyid, src.productcategoryid,
					src.idETLBatchRun);
end; 
go

------------ product$product ------------

drop trigger mend.trg_prod_product;
go 

create trigger mend.trg_prod_product on mend.prod_product
after insert
as
begin
	set nocount on

	merge into mend.prod_product_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.productType, ' '), isnull(trg.valid, ' '),
			isnull(trg.SKU, ' '), isnull(trg.oldSKU, ' '), isnull(trg.description, ' '),
			isnull(trg.isBOM, ' '), isnull(trg.displaySubProductOnPackingSlip, ' '),
			isnull(trg.createdDate, ' '), -- isnull(trg.changedDate, ' '),
			isnull(trg.system$owner, ' '), isnull(trg.system$changedBy, ' ')
		intersect
		select 
			isnull(src.productType, ' '), isnull(src.valid, ' '),
			isnull(src.SKU, ' '), isnull(src.oldSKU, ' '), isnull(src.description, ' '),
			isnull(src.isBOM, ' '), isnull(src.displaySubProductOnPackingSlip, ' '),
			isnull(src.createdDate, ' '), -- isnull(src.changedDate, ' '),
			isnull(src.system$owner, ' '), isnull(src.system$changedBy, ' '))	
		then
			update set
				trg.productType = src.productType, trg.valid = src.valid,
				trg.SKU = src.SKU, trg.oldSKU = src.oldSKU, trg.description = src.description,
				trg.isBOM = src.isBOM, trg.displaySubProductOnPackingSlip = src.displaySubProductOnPackingSlip,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$owner = src.system$owner, trg.system$changedBy = src.system$changedBy, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				productType, valid, 
				SKU, oldSKU, description, 
				isBOM, displaySubProductOnPackingSlip,
				createdDate, changedDate, 
				system$owner, system$changedBy,
				idETLBatchRun)

				values (src.id,
					src.productType, src.valid, 
					src.SKU, src.oldSKU, src.description, 
					src.isBOM, src.displaySubProductOnPackingSlip,
					src.createdDate, src.changedDate, 
					src.system$owner, src.system$changedBy,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_product_aud;
go 

create trigger mend.trg_prod_product_aud on mend.prod_product_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_product_aud_hist (id,
		productType, valid, 
		SKU, oldSKU, description, 
		isBOM, displaySubProductOnPackingSlip,
		createdDate, changedDate, 
		system$owner, system$changedBy,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.productType, d.valid, 
			d.SKU, d.oldSKU, d.description, 
			d.isBOM, d.displaySubProductOnPackingSlip,
			d.createdDate, d.changedDate, 
			d.system$owner, d.system$changedBy,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$product_productfamily ------------

drop trigger mend.trg_prod_product_productfamily;
go 

create trigger mend.trg_prod_product_productfamily on mend.prod_product_productfamily
after insert
as
begin
	set nocount on

	merge into mend.prod_product_productfamily_aud with (tablock) as trg
	using inserted src
		on (trg.productid = src.productid and trg.productfamilyid = src.productfamilyid)
	when not matched 
		then
			insert (productid, productfamilyid, 
				idETLBatchRun)

				values (src.productid, src.productfamilyid,
					src.idETLBatchRun);
end; 
go

------------ product$stockitem ------------

drop trigger mend.trg_prod_stockitem;
go 

create trigger mend.trg_prod_stockitem on mend.prod_stockitem
after insert
as
begin
	set nocount on

	merge into mend.prod_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.SKU, ' '), isnull(trg.oldSKU, ' '), isnull(trg.SNAPDescription, ' '), isnull(trg.packSize, 0),
			isnull(trg.manufacturerArticleID, ' '), isnull(trg.manufacturerCodeNumber, ' '),
			isnull(trg.SNAPUploadStatus, ' '),
			isnull(trg.changed, 0) 
			-- , isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '),
		intersect
		select 
			isnull(src.SKU, ' '), isnull(src.oldSKU, ' '), isnull(src.SNAPDescription, ' '), isnull(src.packSize, 0),
			isnull(src.manufacturerArticleID, ' '), isnull(src.manufacturerCodeNumber, ' '),
			isnull(src.SNAPUploadStatus, ' '),
			isnull(src.changed, 0)
			-- , isnull(src.createdDate, ' '), isnull(trg.changedDate, ' '),
			)	
		then
			update set
				trg.SKU = src.SKU, trg.oldSKU = src.oldSKU, trg.SNAPDescription = src.SNAPDescription, trg.packSize = src.packSize, 
				trg.manufacturerArticleID = src.manufacturerArticleID, trg.manufacturerCodeNumber = src.manufacturerCodeNumber,
				trg.SNAPUploadStatus = src.SNAPUploadStatus,
				trg.changed = src.changed, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				SKU, oldSKU, SNAPDescription, packSize, 
				manufacturerArticleID, manufacturerCodeNumber,
				SNAPUploadStatus, 
				changed,
				createdDate, changedDate, 
				idETLBatchRun)

				values (src.id,
					src.SKU, src.oldSKU, src.SNAPDescription, src.packSize,
					src.manufacturerArticleID, src.manufacturerCodeNumber,
					src.SNAPUploadStatus,
					src.changed,
					src.createdDate, src.changedDate, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_stockitem_aud;
go 

create trigger mend.trg_prod_stockitem_aud on mend.prod_stockitem_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_stockitem_aud_hist (id,
		SKU, oldSKU, SNAPDescription, packSize, 
		manufacturerArticleID, manufacturerCodeNumber,
		SNAPUploadStatus, 
		changed,
		createdDate, changedDate, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.SKU, d.oldSKU, d.SNAPDescription, d.packSize,
			d.manufacturerArticleID, d.manufacturerCodeNumber,
			d.SNAPUploadStatus,
			d.changed,
			d.createdDate, d.changedDate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$stockitem_product ------------

drop trigger mend.trg_prod_stockitem_product;
go 

create trigger mend.trg_prod_stockitem_product on mend.prod_stockitem_product
after insert
as
begin
	set nocount on

	merge into mend.prod_stockitem_product_aud with (tablock) as trg
	using inserted src
		on (trg.stockitemid = src.stockitemid and trg.productid = src.productid)
	when not matched 
		then
			insert (stockitemid, productid, 
				idETLBatchRun)

				values (src.stockitemid, src.productid,
					src.idETLBatchRun);
end; 
go

------------ product$contactlens ------------

drop trigger mend.trg_prod_contactlens;
go 

create trigger mend.trg_prod_contactlens on mend.prod_contactlens
after insert
as
begin
	set nocount on

	merge into mend.prod_contactlens_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.bc, ' '), isnull(trg.di, ' '), isnull(trg.po, ' '),
			isnull(trg.cy, ' '), isnull(trg.ax, ' '),
			isnull(trg.ad, ' '), isnull(trg._do, ' '), isnull(trg.co, ' '), isnull(trg.co_EDI, ' ')
		intersect
		select 
			isnull(src.bc, ' '), isnull(src.di, ' '), isnull(src.po, ' '),
			isnull(src.cy, ' '), isnull(src.ax, ' '),
			isnull(src.ad, ' '), isnull(src._do, ' '), isnull(src.co, ' '), isnull(src.co_EDI, ' '))	
		then
			update set
				trg.bc = src.bc, trg.di = src.di, trg.po = src.po, 
				trg.cy = src.cy, trg.ax = src.ax,
				trg.ad = src.ad, trg._do = src._do, trg.co = src.co, trg.co_EDI = src.co_EDI, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				bc, di, po, 
				cy, ax,  
				ad, _do, co, co_EDI,
				idETLBatchRun)

				values (src.id,
					src.bc, src.di, src.po,
					src.cy, src.ax,
					src.ad, src._do, src.co, src.co_EDI,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_contactlens_aud;
go 

create trigger mend.trg_prod_contactlens_aud on mend.prod_contactlens_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_contactlens_aud_hist (id,
		bc, di, po, 
		cy, ax,  
		ad, _do, co, co_EDI,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.bc, d.di, d.po,
			d.cy, d.ax,
			d.ad, d._do, d.co, d.co_EDI,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$packsize ------------

drop trigger mend.trg_prod_packsize;
go 

create trigger mend.trg_prod_packsize on mend.prod_packsize
after insert
as
begin
	set nocount on

	merge into mend.prod_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.description, ' '), isnull(trg.size, 0),
			isnull(trg.packagingNo, ' '), isnull(trg.allocationPreferenceOrder, 0), isnull(trg.purchasingPreferenceOrder, 0),
			isnull(trg.height, 0), isnull(trg.width, 0), isnull(trg.weight, 0), isnull(trg.depth, 0),
			isnull(src.productGroup1, ' '), isnull(trg.productGroup2, ' '), isnull(trg.productGroup2Type, ' '),
			isnull(trg.breakable, 0), 
			isnull(trg.packsperpallet, 0), isnull(trg.packspercarton, 0)
		intersect
		select 
			isnull(src.description, ' '), isnull(src.size, 0),
			isnull(src.packagingNo, ' '), isnull(src.allocationPreferenceOrder, 0), isnull(src.purchasingPreferenceOrder, 0),
			isnull(src.height, 0), isnull(src.width, 0), isnull(src.weight, 0), isnull(src.depth, 0),
			isnull(src.productGroup1, ' '), isnull(src.productGroup2, ' '), isnull(src.productGroup2Type, ' '),
			isnull(src.breakable, 0), 
			isnull(src.packsperpallet, 0), isnull(src.packspercarton, 0))	
		then
			update set
				trg.description = src.description, trg.size = src.size,
				trg.packagingNo = src.packagingNo, trg.allocationPreferenceOrder = src.allocationPreferenceOrder, trg.purchasingPreferenceOrder = src.purchasingPreferenceOrder, 
				trg.height = src.height, trg.width = src.width, trg.weight = src.weight, trg.depth = src.depth,
				trg.productGroup1 = src.productGroup1, trg.productGroup2 = src.productGroup2, trg.productGroup2Type = src.productGroup2Type,
				trg.breakable = src.breakable, 
				trg.packsperpallet = src.packsperpallet, trg.packspercarton = src.packspercarton, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				description, size, 
				packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
				height, width, weight, depth, 
				productGroup1, productGroup2, productGroup2Type, 
				breakable,
				packsperpallet, packspercarton, 
				idETLBatchRun)

				values (src.id,
					src.description, src.size,
					src.packagingNo, src.allocationPreferenceOrder, src.purchasingPreferenceOrder,
					src.height, src.width, src.weight, src.depth,
					src.productGroup1, src.productGroup2, src.productGroup2Type,
					src.breakable,
					src.packsperpallet, src.packspercarton, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_prod_packsize_aud;
go 

create trigger mend.trg_prod_packsize_aud on mend.prod_packsize_aud
for update, delete
as
begin
	set nocount on

	insert mend.prod_packsize_aud_hist (id,
		description, size, 
		packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
		height, width, weight, depth, 
		productGroup1, productGroup2, productGroup2Type, 
		breakable,
		packsperpallet, packspercarton, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.description, d.size,
			d.packagingNo, d.allocationPreferenceOrder, d.purchasingPreferenceOrder,
			d.height, d.width, d.weight, d.depth,
			d.productGroup1, d.productGroup2, d.productGroup2Type,
			d.breakable,
			d.packsperpallet, d.packspercarton, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ product$packsize_productfamily ------------

drop trigger mend.trg_prod_packsize_productfamily;
go 

create trigger mend.trg_prod_packsize_productfamily on mend.prod_packsize_productfamily
after insert
as
begin
	set nocount on

	merge into mend.prod_packsize_productfamily_aud with (tablock) as trg
	using inserted src
		on (trg.packsizeid = src.packsizeid and trg.productfamilyid = src.productfamilyid)
	when not matched 
		then
			insert (packsizeid, productfamilyid, 
				idETLBatchRun)

				values (src.packsizeid, src.productfamilyid,
					src.idETLBatchRun);
end; 
go

------------ product$stockitem_packsize ------------

drop trigger mend.trg_prod_stockitem_packsize;
go 

create trigger mend.trg_prod_stockitem_packsize on mend.prod_stockitem_packsize
after insert
as
begin
	set nocount on

	merge into mend.prod_stockitem_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.packsizeid = src.packsizeid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (packsizeid, stockitemid, 
				idETLBatchRun)

				values (src.packsizeid, src.stockitemid,
					src.idETLBatchRun);
end; 
go