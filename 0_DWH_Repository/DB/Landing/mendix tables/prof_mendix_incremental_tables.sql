
-- product$product (createdDate, changedDate) --> OK
select top 1000 id,
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.prod_product_aud
-- where convert(date, createdDate) <> convert(date, changedDate)
order by idETLBatchRun desc, changedDate desc

-- product$stockitem --> NO (product - stockitem_product)
select top 1000 id,
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.prod_stockitem_aud
order by idETLBatchRun desc


-- inventory$warehousestockitem --> NO (product - stockitem_product - stockitem - warehousestockitem_stockitem DOESN'T WORK)
select top 1000 id, 
	id_string, 
	availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
	outstandingallocation, onhold,
	stockingmethod, 
	changed,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_warehousestockitem_aud
where upd_ts is not null
order by idETLBatchRun desc
 
-- inventory$warehousestockitembatch
select top 1000 id,
	batch_id,
	fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
	receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
	groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
	productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
	exchangeRate, currency,
	createdDate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_warehousestockitembatch_aud
where upd_ts is not null
order by idETLBatchRun desc


-- inventory$batchstockallocation
select top 1000 id, 
	issueid, createddate, issuedquantity, 
	cancelled, shippeddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_batchstockissue_aud
order by createddate desc

-- inventory$batchstockissue
select top 1000 id, 
	fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_batchstockallocation_aud
where createddate is not null
order by createddate desc

-- orderprocessing$order
select top 10000 id, 
	incrementID, magentoOrderID, createdDate, orderStatus, 
	orderLinesMagento, orderLinesDeduped,
	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder,
	idETLBatchRun, ins_ts
from Landing.mend.order_order_aud
--where id in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
order by createdDate desc

select top 1000 id, 
	subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
	discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
	shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
	adjustmentNegative, adjustmentPositive, 					
	custBalanceAmount, 
	customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, totalInvoiced, totalCanceled, totalRefunded,
	toOrderRate, toGlobalRate, currencyCode,
	mutualamount,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.order_basevalues_aud

select top 1000 id, 
	magentoItemID, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
	basePrice, baseRowPrice, 
	allocated, 
	orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 
	netPrice, netRowPrice,
	createdDate, changedDate, 
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts
from Landing.mend.order_orderlineabstract

select top 1000 id, lineAdded, deduplicate, fulfilled,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.order_orderlinemagento_aud

select top 1000 id, status, packsOrdered, packsAllocated, packsShipped,
	idETLBatchRun, ins_ts
from Landing.mend.order_orderline_aud

-- orderprocessing$shippinggroup
select top 1000 id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc, cancelled,
	idETLBatchRun, ins_ts
from Landing.mend.order_shippinggroup_aud

-- customershipments$customershipment

-- customershipments$consignment


-- stockallocation$orderlinestockallocationtransaction
select top 1000 id, 
	transactionID, timestamp,
	allocationType, recordType, 
	allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
	stockAllocated, cancelled, 
	issuedDateTime,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.all_orderlinestockallocationtransaction_aud

-- stockallocation$magentoallocation
select id, 
	shipped, cancelled, 
	quantity,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.all_magentoallocation_aud


-- warehouseshipments$shipment
-- warehouseshipments$shipmentorderline
select top 1000 id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	carriagefix, intercofix,
	auditComment,
	createdDate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.whship_shipment_aud
where upd_ts is not null

select top 1000 id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
	unitCost, 
	auditComment,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.whship_shipmentorderline_aud
where upd_ts is not null

-- snapreceipts$snapreceipt
-- snapreceipts$snapreceiptline
select top 1000 id, receiptid, 
	lines, lineqty, 	
	receiptStatus, receiptprocessStatus, processDetail,  priority, 
	status, stockstatus, 
	ordertype, orderclass, 
	suppliername, consignmentID, 
	weight, actualWeight, volume, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.rec_snapreceipt_aud
where upd_ts is not null

select id, 
	line, skuid, 
	status, level, unitofmeasure, 
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.rec_snapreceiptline_aud
where upd_ts is not null

-----------------------------------------------------------------

-- inventory$stockmovement
select top 1000 id, 
	moveid, movelineid, 
	stockadjustment, movementtype, movetype, _class, reasonid, status, 
	skuid, qtyactioned, 
	fromstate, tostate, 
	fromstatus, tostatus, 
	operator, supervisor,
	dateCreated, dateClosed,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_stockmovement_aud
where upd_ts is not null

-- inventory$batchstockmovement
select top 1000 id, batchstockmovement_id, batchstockmovementtype, 
	quantity, comment,
	createddate, system$owner,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.wh_batchstockmovement_aud
where upd_ts is not null

-- purchaseorders$purchaseorder
-- purchaseorders$purchaseorderlineheader
-- purchaseorders$purchaseorderline
select id, 
	ordernumber, source, status, potype,
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby,
	transferleadtime, transitleadtime, 
	spoleadtime, spoduedate, 
	tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
	requiredshipdate, requiredtransferdate, 
	estimatedpaymentdate, estimateddepositdate,
	changeddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.purc_purchaseorder_aud
where upd_ts is not null

select top 1000 id, 
	status, problems, 
	lines, items, totalprice, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.purc_purchaseorderlineheader_aud

select top 1000 id, 
	po_lineID, stockItemID,
	quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
	lineprice, 
	backtobackduedate,
	createdDate, changedDate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.purc_purchaseorderline_aud

-- intersitetransfer$transferheader
-- intersitetransfer$intransitbatchheader
-- intersitetransfer$intransitstockitembatch

select top 1000 id, 
	transfernum, allocatedstrategy, 
	totalremaining,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.inter_transferheader_aud
where upd_ts is not null