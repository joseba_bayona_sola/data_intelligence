
---------- Script creacion de tablas ----------

use Landing
go

------------ product$productcategory ------------

create table mend.prod_productcategory (
	id								bigint NOT NULL,
	code							varchar(200),
	name							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_productcategory add constraint [PK_mend_prod_productcategory]
	primary key clustered (id);
go
alter table mend.prod_productcategory add constraint [DF_mend_prod_productcategory_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_productcategory_aud(
	id								bigint NOT NULL,
	code							varchar(200),
	name							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_productcategory_aud add constraint [PK_mend_prod_productcategory_aud]
	primary key clustered (id);
go
alter table mend.prod_productcategory_aud add constraint [DF_mend_prod_productcategory_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_productcategory_aud_hist(
	id								bigint NOT NULL,
	code							varchar(200),
	name							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_productcategory_aud_hist add constraint [DF_mend.prod_productcategory_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$productfamily ------------

create table mend.prod_productfamily (
	id								bigint NOT NULL,
	magentoProductID				varchar(6),
	magentoSKU						varchar(200),
	name							varchar(200),
	manufacturerProductFamily		varchar(200),
	manufacturer					varchar(200),
	status							varchar(20),
	productFamilyType				varchar(13),
	promotionalProduct				bit,
	updateSNAPDescription			bit,
	createToOrder					bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_productfamily add constraint [PK_mend_prod_productfamily]
	primary key clustered (id);
go
alter table mend.prod_productfamily add constraint [DF_mend_prod_productfamily_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_productfamily_aud(
	id								bigint NOT NULL,
	magentoProductID				varchar(6),
	magentoSKU						varchar(200),
	name							varchar(200),
	manufacturerProductFamily		varchar(200),
	manufacturer					varchar(200),
	status							varchar(20),
	productFamilyType				varchar(13),
	promotionalProduct				bit,
	updateSNAPDescription			bit,
	createToOrder					bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_productfamily_aud add constraint [PK_mend_prod_productfamily_aud]
	primary key clustered (id);
go
alter table mend.prod_productfamily_aud add constraint [DF_mend_prod_productfamily_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_productfamily_aud_hist(
	id								bigint NOT NULL,
	magentoProductID				varchar(6),
	magentoSKU						varchar(200),
	name							varchar(200),
	manufacturerProductFamily		varchar(200),
	manufacturer					varchar(200),
	status							varchar(20),
	productFamilyType				varchar(13),
	promotionalProduct				bit,
	updateSNAPDescription			bit,
	createToOrder					bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_productfamily_aud_hist add constraint [DF_mend.prod_productfamily_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$productfamily_productcategory ------------

create table mend.prod_productfamily_productcategory (
	productfamilyid					bigint NOT NULL,
	productcategoryid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_productfamily_productcategory add constraint [PK_mend_prod_productfamily_productcategory]
	primary key clustered (productfamilyid, productcategoryid);
go
alter table mend.prod_productfamily_productcategory add constraint [DF_mend_prod_productfamily_productcategory_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_productfamily_productcategory_aud(
	productfamilyid					bigint NOT NULL,
	productcategoryid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_productfamily_productcategory_aud add constraint [PK_mend_prod_productfamily_productcategory_aud]
	primary key clustered (productfamilyid, productcategoryid);
go
alter table mend.prod_productfamily_productcategory_aud add constraint [DF_mend_prod_productfamily_productcategory_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$product ------------

create table mend.prod_product (
	id								bigint NOT NULL,
	productType						varchar(200),
	valid							bit,
	SKU								varchar(200),
	oldSKU							varchar(200),
	description						varchar(200),
	isBOM							bit,
	displaySubProductOnPackingSlip	bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_product add constraint [PK_mend_prod_product]
	primary key clustered (id);
go
alter table mend.prod_product add constraint [DF_mend_prod_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_product_aud(
	id								bigint NOT NULL,
	productType						varchar(200),
	valid							bit,
	SKU								varchar(200),
	oldSKU							varchar(200),
	description						varchar(200),
	isBOM							bit,
	displaySubProductOnPackingSlip	bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_product_aud add constraint [PK_mend_prod_product_aud]
	primary key clustered (id);
go
alter table mend.prod_product_aud add constraint [DF_mend_prod_product_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_product_aud_hist(
	id								bigint NOT NULL,
	productType						varchar(200),
	valid							bit,
	SKU								varchar(200),
	oldSKU							varchar(200),
	description						varchar(200),
	isBOM							bit,
	displaySubProductOnPackingSlip	bit,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_product_aud_hist add constraint [DF_mend.prod_product_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$product_productfamily ------------

create table mend.prod_product_productfamily (
	productid						bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_product_productfamily add constraint [PK_mend_prod_product_productfamily]
	primary key clustered (productid, productfamilyid);
go
alter table mend.prod_product_productfamily add constraint [DF_mend_prod_product_productfamily_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_product_productfamily_aud(
	productid						bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_product_productfamily_aud add constraint [PK_mend_prod_product_productfamily_aud]
	primary key clustered (productid, productfamilyid);
go
alter table mend.prod_product_productfamily_aud add constraint [DF_mend_prod_product_productfamily_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$stockitem ------------

create table mend.prod_stockitem (
	id								bigint NOT NULL,
	SKU								varchar(200),
	oldSKU							varchar(200),
	SNAPDescription					varchar(30),
	packSize						int,
	manufacturerArticleID			varchar(200),
	manufacturerCodeNumber			varchar(200),
	SNAPUploadStatus				varchar(200),
	changed							bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_stockitem add constraint [PK_mend_prod_stockitem]
	primary key clustered (id);
go
alter table mend.prod_stockitem add constraint [DF_mend_prod_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_stockitem_aud(
	id								bigint NOT NULL,
	SKU								varchar(200),
	oldSKU							varchar(200),
	SNAPDescription					varchar(30),
	packSize						int,
	manufacturerArticleID			varchar(200),
	manufacturerCodeNumber			varchar(200),
	SNAPUploadStatus				varchar(200),
	changed							bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_stockitem_aud add constraint [PK_mend_prod_stockitem_aud]
	primary key clustered (id);
go
alter table mend.prod_stockitem_aud add constraint [DF_mend_prod_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_stockitem_aud_hist(
	id								bigint NOT NULL,
	SKU								varchar(200),
	oldSKU							varchar(200),
	SNAPDescription					varchar(30),
	packSize						int,
	manufacturerArticleID			varchar(200),
	manufacturerCodeNumber			varchar(200),
	SNAPUploadStatus				varchar(200),
	changed							bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_stockitem_aud_hist add constraint [DF_mend.prod_stockitem_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$stockitem_product ------------

create table mend.prod_stockitem_product (
	stockitemid						bigint NOT NULL,
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_stockitem_product add constraint [PK_mend_prod_stockitem_product]
	primary key clustered (stockitemid, productid);
go
alter table mend.prod_stockitem_product add constraint [DF_mend_prod_stockitem_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_stockitem_product_aud(
	stockitemid						bigint NOT NULL,
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_stockitem_product_aud add constraint [PK_mend_prod_stockitem_product_aud]
	primary key clustered (stockitemid, productid);
go
alter table mend.prod_stockitem_product_aud add constraint [DF_mend_prod_stockitem_product_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$contactlens ------------

create table mend.prod_contactlens (
	id								bigint NOT NULL,
	bc								varchar(10),
	di								varchar(10),
	po								varchar(10),
	cy								varchar(10),
	ax								varchar(10),
	ad								varchar(10),
	_do								varchar(10),
	co								varchar(20),
	co_EDI							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_contactlens add constraint [PK_mend_prod_contactlens]
	primary key clustered (id);
go
alter table mend.prod_contactlens add constraint [DF_mend_prod_contactlens_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_contactlens_aud(
	id								bigint NOT NULL,
	bc								varchar(10),
	di								varchar(10),
	po								varchar(10),
	cy								varchar(10),
	ax								varchar(10),
	ad								varchar(10),
	_do								varchar(10),
	co								varchar(20),
	co_EDI							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_contactlens_aud add constraint [PK_mend_prod_contactlens_aud]
	primary key clustered (id);
go
alter table mend.prod_contactlens_aud add constraint [DF_mend_prod_contactlens_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.prod_contactlens_aud_hist(
	id								bigint NOT NULL,
	bc								varchar(10),
	di								varchar(10),
	po								varchar(10),
	cy								varchar(10),
	ax								varchar(10),
	ad								varchar(10),
	_do								varchar(10),
	co								varchar(20),
	co_EDI							varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_contactlens_aud_hist add constraint [DF_mend.prod_contactlens_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$packsize ------------

create table mend.prod_packsize (
	id								bigint NOT NULL,
	description						varchar(200),
	size							decimal(28,8),
	packagingNo						varchar(1),
	allocationPreferenceOrder		int,
	purchasingPreferenceOrder		int,
	height							decimal(28,8),
	width							decimal(28,8),
	weight							decimal(28,8),
	depth							decimal(28,8),
	productGroup1					varchar(200),		
	productGroup2					varchar(200),
	productGroup2Type				varchar(200),
	breakable						bit,
	packsperpallet					int, 
	packspercarton					int, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_packsize add constraint [PK_mend_prod_packsize]
	primary key clustered (id);
go
alter table mend.prod_packsize add constraint [DF_mend_prod_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_packsize_aud(
	id								bigint NOT NULL,
	description						varchar(200),
	size							decimal(28,8),
	packagingNo						varchar(1),
	allocationPreferenceOrder		int,
	purchasingPreferenceOrder		int,
	height							decimal(28,8),
	width							decimal(28,8),
	weight							decimal(28,8),
	depth							decimal(28,8),
	productGroup1					varchar(200),		
	productGroup2					varchar(200),
	productGroup2Type				varchar(200),
	breakable						bit,
	packsperpallet					int, 
	packspercarton					int, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_packsize_aud add constraint [PK_mend_prod_packsize_aud]
	primary key clustered (id);
go
alter table mend.prod_packsize_aud add constraint [DF_mend_prod_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_packsize_aud_hist(
	id								bigint NOT NULL,
	description						varchar(200),
	size							decimal(28,8),
	packagingNo						varchar(1),
	allocationPreferenceOrder		int,
	purchasingPreferenceOrder		int,
	height							decimal(28,8),
	width							decimal(28,8),
	weight							decimal(28,8),
	depth							decimal(28,8),
	productGroup1					varchar(200),		
	productGroup2					varchar(200),
	productGroup2Type				varchar(200),
	breakable						bit,
	packsperpallet					int, 
	packspercarton					int, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.prod_packsize_aud_hist add constraint [DF_mend.prod_packsize_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$packsize_productfamily ------------

create table mend.prod_packsize_productfamily (
	packsizeid						bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_packsize_productfamily add constraint [PK_mend_prod_packsize_productfamily]
	primary key clustered (packsizeid, productfamilyid);
go
alter table mend.prod_packsize_productfamily add constraint [DF_mend_prod_packsize_productfamily_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_packsize_productfamily_aud(
	packsizeid						bigint NOT NULL,
	productfamilyid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_packsize_productfamily_aud add constraint [PK_mend_prod_packsize_productfamily_aud]
	primary key clustered (packsizeid, productfamilyid);
go
alter table mend.prod_packsize_productfamily_aud add constraint [DF_mend_prod_packsize_productfamily_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ product$stockitem_packsize ------------

create table mend.prod_stockitem_packsize (
	packsizeid						bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.prod_stockitem_packsize add constraint [PK_mend_prod_stockitem_packsize]
	primary key clustered (packsizeid, stockitemid);
go
alter table mend.prod_stockitem_packsize add constraint [DF_mend_prod_stockitem_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.prod_stockitem_packsize_aud(
	packsizeid						bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.prod_stockitem_packsize_aud add constraint [PK_mend_prod_stockitem_packsize_aud]
	primary key clustered (packsizeid, stockitemid);
go
alter table mend.prod_stockitem_packsize_aud add constraint [DF_mend_prod_stockitem_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go