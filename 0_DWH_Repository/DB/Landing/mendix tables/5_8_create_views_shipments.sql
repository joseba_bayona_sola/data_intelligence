use Landing
go 

------------------- customershipments$customershipment -------------------

drop view mend.ship_customershipment_aud_v
go

create view mend.ship_customershipment_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, statusChange, 
		processingFailed, warehouseMethod, labelRenderer, 
		shippingMethod, shippingDescription, paymentMethod, 	
		notify, fullyShipped, syncedToMagento, 	
		shipmentValue, shippingTotal, toFollowValue, 
		hold, intersite, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate, changeddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			shipmentID, partitionID0_120,
			shipmentNumber, orderIncrementID, 
			status, statusChange, 
			processingFailed, warehouseMethod, labelRenderer, 
			shippingMethod, shippingDescription, paymentMethod, 	
			notify, fullyShipped, syncedToMagento, 	
			shipmentValue, shippingTotal, toFollowValue, 
			hold, intersite, 
			dispatchDate, expectedShippingDate, expectedDeliveryDate,  
			createdDate, changeddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_customershipment_aud
		union
		select 'H' record_type, id, 
			shipmentID, partitionID0_120,
			shipmentNumber, orderIncrementID, 
			status, statusChange, 
			processingFailed, warehouseMethod, labelRenderer, 
			shippingMethod, shippingDescription, paymentMethod, 	
			notify, fullyShipped, syncedToMagento, 	
			shipmentValue, shippingTotal, toFollowValue, 
			hold, intersite, 
			dispatchDate, expectedShippingDate, expectedDeliveryDate,  
			createdDate, changeddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_customershipment_aud_hist) t
go




------------------- customershipments$lifecycle_update -------------------

drop view mend.ship_lifecycle_update_aud_v
go

create view mend.ship_lifecycle_update_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		status, timestamp, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			status, timestamp, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_lifecycle_update_aud
		union
		select 'H' record_type, id, 
			status, timestamp, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_lifecycle_update_aud_hist) t
go

------------------- customershipments$api_updates -------------------

drop view mend.ship_api_updates_aud_v
go

create view mend.ship_api_updates_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		service, status, operation, 
		timestamp, 
		detail, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			service, status, operation, 
			timestamp, 
			detail, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_api_updates_aud
		union
		select 'H' record_type, id, 
			service, status, operation, 
			timestamp, 
			detail, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_api_updates_aud_hist) t
go





------------------- customershipments$consignment -------------------

drop view mend.ship_consignment_aud_v
go

create view mend.ship_consignment_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		order_number, identifier, 
		warehouse_id, 
		consignment_number, 
		shipping_method, service_id, carrier, service, 
		delivery_instructions, tracking_url, 
		order_value, currency,
		create_date, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			order_number, identifier, 
			warehouse_id, 
			consignment_number, 
			shipping_method, service_id, carrier, service, 
			delivery_instructions, tracking_url, 
			order_value, currency,
			create_date, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_consignment_aud
		union
		select 'H' record_type, id, 
			order_number, identifier, 
			warehouse_id, 
			consignment_number, 
			shipping_method, service_id, carrier, service, 
			delivery_instructions, tracking_url, 
			order_value, currency,
			create_date, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_consignment_aud_hist) t
go

------------------- customershipments$packagedetail -------------------

drop view mend.ship_packagedetail_aud_v
go

create view mend.ship_packagedetail_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		tracking_number, description, 
		width, length, height, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			tracking_number, description, 
			width, length, height, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_packagedetail_aud
		union
		select 'H' record_type, id, 
			tracking_number, description, 
			width, length, height, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_packagedetail_aud_hist) t
go




------------------- customershipments$customershipmentline -------------------

drop view mend.ship_customershipmentline_aud_v
go

create view mend.ship_customershipmentline_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		shipmentLineID, lineNumber, status, variableBreakdown, 
		stockItemID, productName, productDescription, eye, 
		quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
		packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
		price, priceFormatted, 
		subTotal, subTotal_formatted, 
		VATRate, VAT, VAT_formatted,
		fullyShipped,  
		BOMLine, 
		system$changedBy, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			shipmentLineID, lineNumber, status, variableBreakdown, 
			stockItemID, productName, productDescription, eye, 
			quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
			packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
			price, priceFormatted, 
			subTotal, subTotal_formatted, 
			VATRate, VAT, VAT_formatted,
			fullyShipped,  
			BOMLine, 
			system$changedBy, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.ship_customershipmentline_aud
		union
		select 'H' record_type, id, 
			shipmentLineID, lineNumber, status, variableBreakdown, 
			stockItemID, productName, productDescription, eye, 
			quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
			packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
			price, priceFormatted, 
			subTotal, subTotal_formatted, 
			VATRate, VAT, VAT_formatted,
			fullyShipped,  
			BOMLine, 
			system$changedBy, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_customershipmentline_aud_hist) t
go