
---------- Script creacion de tablas ----------

use Landing
go

------------------- customershipments$customershipment -------------------

create table mend.ship_customershipment (
	id								bigint NOT NULL, 
	shipmentID						int,
	partitionID0_120				int,
	shipmentNumber					varchar(16),
	orderIncrementID				varchar(200), 
	status							varchar(200),
	statusChange					bit, 
	processingFailed				bit, 
	warehouseMethod					varchar(11),
	labelRenderer					varchar(6), 
	shippingMethod					varchar(200),
	shippingDescription				varchar(200),
	paymentMethod					varchar(200), 
	notify							bit,
	fullyShipped					bit,
	syncedToMagento					bit, 	
	shipmentValue					decimal(28,8),
	shippingTotal					decimal(28,8),
	toFollowValue					decimal(28,8), 
	hold							bit,
	intersite						bit, 
	dispatchDate					datetime2,
	expectedShippingDate			datetime2,
	expectedDeliveryDate			datetime2,  
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipment add constraint [PK_mend_ship_customershipment]
	primary key clustered (id);
go
alter table mend.ship_customershipment add constraint [DF_mend_ship_customershipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipment_aud(
	id								bigint NOT NULL, 
	shipmentID						int,
	partitionID0_120				int,
	shipmentNumber					varchar(16),
	orderIncrementID				varchar(200), 
	status							varchar(200),
	statusChange					bit, 
	processingFailed				bit, 
	warehouseMethod					varchar(11),
	labelRenderer					varchar(6), 
	shippingMethod					varchar(200),
	shippingDescription				varchar(200),
	paymentMethod					varchar(200), 
	notify							bit,
	fullyShipped					bit,
	syncedToMagento					bit, 	
	shipmentValue					decimal(28,8),
	shippingTotal					decimal(28,8),
	toFollowValue					decimal(28,8), 
	hold							bit,
	intersite						bit, 
	dispatchDate					datetime2,
	expectedShippingDate			datetime2,
	expectedDeliveryDate			datetime2,  
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipment_aud add constraint [PK_mend_ship_customershipment_aud]
	primary key clustered (id);
go
alter table mend.ship_customershipment_aud add constraint [DF_mend_ship_customershipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipment_aud_hist(
	id								bigint NOT NULL, 
	shipmentID						int,
	partitionID0_120				int,
	shipmentNumber					varchar(16),
	orderIncrementID				varchar(200), 
	status							varchar(200),
	statusChange					bit, 
	processingFailed				bit, 
	warehouseMethod					varchar(11),
	labelRenderer					varchar(6), 
	shippingMethod					varchar(200),
	shippingDescription				varchar(200),
	paymentMethod					varchar(200), 
	notify							bit,
	fullyShipped					bit,
	syncedToMagento					bit, 	
	shipmentValue					decimal(28,8),
	shippingTotal					decimal(28,8),
	toFollowValue					decimal(28,8), 
	hold							bit,
	intersite						bit, 
	dispatchDate					datetime2,
	expectedShippingDate			datetime2,
	expectedDeliveryDate			datetime2,  
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_customershipment_aud_hist add constraint [DF_mend.ship_customershipment_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$customershipment_warehouse -------------------

create table mend.ship_customershipment_warehouse (
	customershipmentid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipment_warehouse add constraint [PK_mend_ship_customershipment_warehouse]
	primary key clustered (customershipmentid,warehouseid);
go
alter table mend.ship_customershipment_warehouse add constraint [DF_mend_ship_customershipment_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipment_warehouse_aud(
	customershipmentid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipment_warehouse_aud add constraint [PK_mend_ship_customershipment_warehouse_aud]
	primary key clustered (customershipmentid,warehouseid);
go
alter table mend.ship_customershipment_warehouse_aud add constraint [DF_mend_ship_customershipment_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$customershipment_order -------------------

create table mend.ship_customershipment_order (
	customershipmentid				bigint NOT NULL, 
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipment_order add constraint [PK_mend_ship_customershipment_order]
	primary key clustered (customershipmentid,orderid);
go
alter table mend.ship_customershipment_order add constraint [DF_mend_ship_customershipment_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipment_order_aud(
	customershipmentid				bigint NOT NULL, 
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipment_order_aud add constraint [PK_mend_ship_customershipment_order_aud]
	primary key clustered (customershipmentid,orderid);
go
alter table mend.ship_customershipment_order_aud add constraint [DF_mend_ship_customershipment_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------------- customershipments$lifecycle_update -------------------

create table mend.ship_lifecycle_update (
	id								bigint NOT NULL,
	status							varchar(19),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_lifecycle_update add constraint [PK_mend_ship_lifecycle_update]
	primary key clustered (id);
go
alter table mend.ship_lifecycle_update add constraint [DF_mend_ship_lifecycle_update_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_lifecycle_update_aud(
	id								bigint NOT NULL,
	status							varchar(19),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_lifecycle_update_aud add constraint [PK_mend_ship_lifecycle_update_aud]
	primary key clustered (id);
go
alter table mend.ship_lifecycle_update_aud add constraint [DF_mend_ship_lifecycle_update_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_lifecycle_update_aud_hist(
	id								bigint NOT NULL,
	status							varchar(19),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_lifecycle_update_aud_hist add constraint [DF_mend.ship_lifecycle_update_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------------- customershipments$customershipment_progress -------------------

create table mend.ship_customershipment_progress (
	lifecycle_updateid				bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipment_progress add constraint [PK_mend_ship_customershipment_progress]
	primary key clustered (lifecycle_updateid,customershipmentid);
go
alter table mend.ship_customershipment_progress add constraint [DF_mend_ship_customershipment_progress_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipment_progress_aud(
	lifecycle_updateid				bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipment_progress_aud add constraint [PK_mend_ship_customershipment_progress_aud]
	primary key clustered (lifecycle_updateid,customershipmentid);
go
alter table mend.ship_customershipment_progress_aud add constraint [DF_mend_ship_customershipment_progress_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------------- customershipments$api_updates -------------------

create table mend.ship_api_updates (
	id								bigint NOT NULL, 
	service							varchar(6),
	status							varchar(7),
	operation						varchar(200), 
	timestamp						datetime2, 
	detail							varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_api_updates add constraint [PK_mend_ship_api_updates]
	primary key clustered (id);
go
alter table mend.ship_api_updates add constraint [DF_mend_ship_api_updates_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_api_updates_aud(
	id								bigint NOT NULL, 
	service							varchar(6),
	status							varchar(7),
	operation						varchar(200), 
	timestamp						datetime2, 
	detail							varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_api_updates_aud add constraint [PK_mend_ship_api_updates_aud]
	primary key clustered (id);
go
alter table mend.ship_api_updates_aud add constraint [DF_mend_ship_api_updates_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_api_updates_aud_hist(
	id								bigint NOT NULL, 
	service							varchar(6),
	status							varchar(7),
	operation						varchar(200), 
	timestamp						datetime2, 
	detail							varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_api_updates_aud_hist add constraint [DF_mend.ship_api_updates_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$api_updates_customershipment -------------------

create table mend.ship_api_updates_customershipment (
	api_updatesid					bigint NOT NULL, 
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_api_updates_customershipment add constraint [PK_mend_ship_api_updates_customershipment]
	primary key clustered (api_updatesid,customershipmentid);
go
alter table mend.ship_api_updates_customershipment add constraint [DF_mend_ship_api_updates_customershipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_api_updates_customershipment_aud(
	api_updatesid					bigint NOT NULL, 
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_api_updates_customershipment_aud add constraint [PK_mend_ship_api_updates_customershipment_aud]
	primary key clustered (api_updatesid,customershipmentid);
go
alter table mend.ship_api_updates_customershipment_aud add constraint [DF_mend_ship_api_updates_customershipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------------- customershipments$consignment -------------------

create table mend.ship_consignment (
	id								bigint NOT NULL, 
	order_number					varchar(200), 
	identifier						varchar(200), 
	warehouse_id					varchar(200), 
	consignment_number				varchar(200), 
	shipping_method					varchar(1000),
	service_id						varchar(200), 
	carrier							varchar(200),
	service							varchar(200), 
	delivery_instructions			varchar(200), 
	tracking_url					varchar(200), 
	order_value						varchar(200), 
	currency						varchar(200),
	create_date						varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_consignment add constraint [PK_mend_ship_consignment]
	primary key clustered (id);
go
alter table mend.ship_consignment add constraint [DF_mend_ship_consignment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_consignment_aud(
	id								bigint NOT NULL, 
	order_number					varchar(200), 
	identifier						varchar(200), 
	warehouse_id					varchar(200), 
	consignment_number				varchar(200), 
	shipping_method					varchar(1000),
	service_id						varchar(200), 
	carrier							varchar(200),
	service							varchar(200), 
	delivery_instructions			varchar(200), 
	tracking_url					varchar(200), 
	order_value						varchar(200), 
	currency						varchar(200),
	create_date						varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_consignment_aud add constraint [PK_mend_ship_consignment_aud]
	primary key clustered (id);
go
alter table mend.ship_consignment_aud add constraint [DF_mend_ship_consignment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_consignment_aud_hist(
	id								bigint NOT NULL, 
	order_number					varchar(200), 
	identifier						varchar(200), 
	warehouse_id					varchar(200), 
	consignment_number				varchar(200), 
	shipping_method					varchar(1000),
	service_id						varchar(200), 
	carrier							varchar(200),
	service							varchar(200), 
	delivery_instructions			varchar(200), 
	tracking_url					varchar(200), 
	order_value						varchar(200), 
	currency						varchar(200),
	create_date						varchar(1000),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_consignment_aud_hist add constraint [DF_mend.ship_consignment_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$scurriconsignment_shipment -------------------

create table mend.ship_scurriconsignment_shipment (
	consignmentid					bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_scurriconsignment_shipment add constraint [PK_mend_ship_scurriconsignment_shipment]
	primary key clustered (consignmentid,customershipmentid);
go
alter table mend.ship_scurriconsignment_shipment add constraint [DF_mend_ship_scurriconsignment_shipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_scurriconsignment_shipment_aud(
	consignmentid					bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_scurriconsignment_shipment_aud add constraint [PK_mend_ship_scurriconsignment_shipment_aud]
	primary key clustered (consignmentid,customershipmentid);
go
alter table mend.ship_scurriconsignment_shipment_aud add constraint [DF_mend_ship_scurriconsignment_shipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$packagedetail -------------------

create table mend.ship_packagedetail (
	id								bigint NOT NULL, 
	tracking_number					varchar(200), 
	description						varchar(200), 
	width							decimal(28,8), 
	length							decimal(28,8), 
	height							decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_packagedetail add constraint [PK_mend_ship_packagedetail]
	primary key clustered (id);
go
alter table mend.ship_packagedetail add constraint [DF_mend_ship_packagedetail_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_packagedetail_aud(
	id								bigint NOT NULL, 
	tracking_number					varchar(200), 
	description						varchar(200), 
	width							decimal(28,8), 
	length							decimal(28,8), 
	height							decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_packagedetail_aud add constraint [PK_mend_ship_packagedetail_aud]
	primary key clustered (id);
go
alter table mend.ship_packagedetail_aud add constraint [DF_mend_ship_packagedetail_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_packagedetail_aud_hist(
	id								bigint NOT NULL, 
	tracking_number					varchar(200), 
	description						varchar(200), 
	width							decimal(28,8), 
	length							decimal(28,8), 
	height							decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_packagedetail_aud_hist add constraint [DF_mend.ship_packagedetail_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$consignmentpackages -------------------

create table mend.ship_consignmentpackages (
	packagedetailid					bigint NOT NULL,
	consignmentid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_consignmentpackages add constraint [PK_mend_ship_consignmentpackages]
	primary key clustered (packagedetailid,consignmentid);
go
alter table mend.ship_consignmentpackages add constraint [DF_mend_ship_consignmentpackages_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_consignmentpackages_aud(
	packagedetailid					bigint NOT NULL,
	consignmentid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_consignmentpackages_aud add constraint [PK_mend_ship_consignmentpackages_aud]
	primary key clustered (packagedetailid,consignmentid);
go
alter table mend.ship_consignmentpackages_aud add constraint [DF_mend_ship_consignmentpackages_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------------- customershipments$customershipmentline -------------------

create table mend.ship_customershipmentline (
	id								bigint NOT NULL, 
	shipmentLineID					bigint, 
	lineNumber						decimal(28,8), 
	status							varchar(13), 
	variableBreakdown				varchar(1000), 
	stockItemID						bigint, 
	productName						varchar(200), 
	productDescription				varchar(200), 
	eye								varchar(200), 
	quantityDescription				varchar(200), 
	itemQuantityOrdered				decimal(28,8), 
	itemQuantityIssued				decimal(28,8), 
	itemQuantityShipped				decimal(28,8), 
	itemQuantityPreviouslyShipped	decimal(28,8), 
	packQuantityOrdered				int, 
	packQuantityIssued				int, 
	packQuantityShipped				int, 
	price							decimal(28,8), 
	priceFormatted					varchar(200), 
	subTotal						decimal(28,8), 
	subTotal_formatted				varchar(200),
	VATRate							varchar(50), 
	VAT								decimal(28,8), 
	VAT_formatted					varchar(200),
	fullyShipped					bit,  
	BOMLine							bit, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipmentline add constraint [PK_mend_ship_customershipmentline]
	primary key clustered (id);
go
alter table mend.ship_customershipmentline add constraint [DF_mend_ship_customershipmentline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipmentline_aud(
	id								bigint NOT NULL, 
	shipmentLineID					bigint, 
	lineNumber						decimal(28,8), 
	status							varchar(13), 
	variableBreakdown				varchar(1000), 
	stockItemID						bigint, 
	productName						varchar(200), 
	productDescription				varchar(200), 
	eye								varchar(200), 
	quantityDescription				varchar(200), 
	itemQuantityOrdered				decimal(28,8), 
	itemQuantityIssued				decimal(28,8), 
	itemQuantityShipped				decimal(28,8), 
	itemQuantityPreviouslyShipped	decimal(28,8), 
	packQuantityOrdered				int, 
	packQuantityIssued				int, 
	packQuantityShipped				int, 
	price							decimal(28,8), 
	priceFormatted					varchar(200), 
	subTotal						decimal(28,8), 
	subTotal_formatted				varchar(200),
	VATRate							varchar(50), 
	VAT								decimal(28,8), 
	VAT_formatted					varchar(200),
	fullyShipped					bit,  
	BOMLine							bit, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipmentline_aud add constraint [PK_mend_ship_customershipmentline_aud]
	primary key clustered (id);
go
alter table mend.ship_customershipmentline_aud add constraint [DF_mend_ship_customershipmentline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipmentline_aud_hist(
	id								bigint NOT NULL, 
	shipmentLineID					bigint, 
	lineNumber						decimal(28,8), 
	status							varchar(13), 
	variableBreakdown				varchar(1000), 
	stockItemID						bigint, 
	productName						varchar(200), 
	productDescription				varchar(200), 
	eye								varchar(200), 
	quantityDescription				varchar(200), 
	itemQuantityOrdered				decimal(28,8), 
	itemQuantityIssued				decimal(28,8), 
	itemQuantityShipped				decimal(28,8), 
	itemQuantityPreviouslyShipped	decimal(28,8), 
	packQuantityOrdered				int, 
	packQuantityIssued				int, 
	packQuantityShipped				int, 
	price							decimal(28,8), 
	priceFormatted					varchar(200), 
	subTotal						decimal(28,8), 
	subTotal_formatted				varchar(200),
	VATRate							varchar(50), 
	VAT								decimal(28,8), 
	VAT_formatted					varchar(200),
	fullyShipped					bit,  
	BOMLine							bit, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.ship_customershipmentline_aud_hist add constraint [DF_mend.ship_customershipmentline_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$customershipmentline_customershipment -------------------

create table mend.ship_customershipmentline_customershipment (
	customershipmentlineid			bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipmentline_customershipment add constraint [PK_mend_ship_customershipmentline_customershipment]
	primary key clustered (customershipmentlineid,customershipmentid);
go
alter table mend.ship_customershipmentline_customershipment add constraint [DF_mend_ship_customershipmentline_customershipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipmentline_customershipment_aud(
	customershipmentlineid			bigint NOT NULL,
	customershipmentid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipmentline_customershipment_aud add constraint [PK_mend_ship_customershipmentline_customershipment_aud]
	primary key clustered (customershipmentlineid,customershipmentid);
go
alter table mend.ship_customershipmentline_customershipment_aud add constraint [DF_mend_ship_customershipmentline_customershipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- customershipments$customershipmentline_product -------------------

create table mend.ship_customershipmentline_product (
	customershipmentlineid			bigint NOT NULL,
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.ship_customershipmentline_product add constraint [PK_mend_ship_customershipmentline_product]
	primary key clustered (customershipmentlineid,productid);
go
alter table mend.ship_customershipmentline_product add constraint [DF_mend_ship_customershipmentline_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.ship_customershipmentline_product_aud(
	customershipmentlineid			bigint NOT NULL,
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.ship_customershipmentline_product_aud add constraint [PK_mend_ship_customershipmentline_product_aud]
	primary key clustered (customershipmentlineid,productid);
go
alter table mend.ship_customershipmentline_product_aud add constraint [DF_mend_ship_customershipmentline_product_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- inventory$warehousestockitembatch_shipment -------------------

------------------- inventory$warehousestockitembatch_shipmenthistory -------------------

------------------- inventory$warehousestockitembatch_shipmentorderline -------------------