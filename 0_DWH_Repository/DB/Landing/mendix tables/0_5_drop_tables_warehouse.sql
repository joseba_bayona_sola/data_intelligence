
------------------------ Drop tables -------------------------------

use Landing
go

------------ inventory$warehousestockitem ------------ 

drop table mend.wh_warehousestockitem;
go
drop table mend.wh_warehousestockitem_aud;
go
drop table mend.wh_warehousestockitem_aud_hist;
go

------------ inventory$located_at ------------

drop table mend.wh_located_at;
go
drop table mend.wh_located_at_aud;
go

------------ inventory$warehousestockitem_stockitem ------------

drop table mend.wh_warehousestockitem_stockitem;
go
drop table mend.wh_warehousestockitem_stockitem_aud;
go




------------ inventory$warehousestockitembatch ------------

drop table mend.wh_warehousestockitembatch;
go
drop table mend.wh_warehousestockitembatch_aud;
go
drop table mend.wh_warehousestockitembatch_aud_hist;
go

------------ inventory$warehousestockitembatch_warehousestockitem ------------

drop table mend.wh_warehousestockitembatch_warehousestockitem;
go
drop table mend.wh_warehousestockitembatch_warehousestockitem_aud;
go




------------ inventory$batchstockissue ------------

drop table mend.wh_batchstockissue;
go
drop table mend.wh_batchstockissue_aud;
go
drop table mend.wh_batchstockissue_aud_hist;
go

------------ inventory$batchstockissue_warehousestockitembatch ------------

drop table mend.wh_batchstockissue_warehousestockitembatch;
go
drop table mend.wh_batchstockissue_warehousestockitembatch_aud;
go

------------ inventory$batchstockallocation ------------

drop table mend.wh_batchstockallocation;
go
drop table mend.wh_batchstockallocation_aud;
go
drop table mend.wh_batchstockallocation_aud_hist;
go

------------ inventory$batchstockissue_batchstockallocation ------------

drop table mend.wh_batchstockissue_batchstockallocation;
go
drop table mend.wh_batchstockissue_batchstockallocation_aud;
go

------------ inventory$batchtransaction_orderlinestockallocationtransaction ------------

drop table mend.wh_batchtransaction_orderlinestockallocationtransaction;
go
drop table mend.wh_batchtransaction_orderlinestockallocationtransaction_aud;
go



------------ inventory$repairstockbatchs ------------

drop table mend.wh_repairstockbatchs;
go
drop table mend.wh_repairstockbatchs_aud;
go
drop table mend.wh_repairstockbatchs_aud_hist;
go

------------ inventory$repairstockbatchs_warehousestockitembatch ------------

drop table mend.wh_repairstockbatchs_warehousestockitembatch;
go
drop table mend.wh_repairstockbatchs_warehousestockitembatch_aud;
go





------------ inventory$stockmovement ------------

drop table mend.wh_stockmovement;
go
drop table mend.wh_stockmovement_aud;
go
drop table mend.wh_stockmovement_aud_hist;
go

------------ inventory$stockmovement_warehousestockitem ------------

drop table mend.wh_stockmovement_warehousestockitem;
go
drop table mend.wh_stockmovement_warehousestockitem_aud;
go





------------ inventory$batchstockmovement ------------

drop table mend.wh_batchstockmovement;
go
drop table mend.wh_batchstockmovement_aud;
go
drop table mend.wh_batchstockmovement_aud_hist;
go

------------ inventory$batchstockmovement_stockmovement ------------

drop table mend.wh_batchstockmovement_stockmovement;
go
drop table mend.wh_batchstockmovement_stockmovement_aud;
go

------------ inventory$batchstockmovement_warehousestockitembatch ------------

drop table mend.wh_batchstockmovement_warehousestockitembatch;
go
drop table mend.wh_batchstockmovement_warehousestockitembatch_aud;
go