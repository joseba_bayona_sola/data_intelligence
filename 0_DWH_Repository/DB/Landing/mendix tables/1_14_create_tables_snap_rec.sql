
---------- Script creacion de tablas ----------

use Landing
go

------------------- snapreceipts$snapreceipt -------------------

create table mend.rec_snapreceipt (
	id								bigint NOT NULL, 
	receiptid						varchar(25), 
	lines							decimal(28,8), 
	lineqty							decimal(28,8),
	receiptStatus					varchar(9), 
	receiptprocessStatus			varchar(8), 
	processDetail					varchar(100),  
	priority						varchar(2), 
	status							varchar(2), 
	stockstatus						varchar(2), 
	ordertype						varchar(3), 
	orderclass						varchar(10), 
	suppliername					varchar(50), 
	consignmentID					varchar(25), 
	weight							decimal(28,8), 
	actualWeight					decimal(28,8),
	volume							decimal(28,8), 
	dateDueIn						datetime2, 
	dateReceipt						datetime2, 
	dateCreated						datetime2, 
	dateArrival						datetime2, 
	dateClosed						datetime2,  
	datesfixed						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.rec_snapreceipt add constraint [PK_mend_rec_snapreceipt]
	primary key clustered (id);
go
alter table mend.rec_snapreceipt add constraint [DF_mend_rec_snapreceipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceipt_aud(
	id								bigint NOT NULL, 
	receiptid						varchar(25), 
	lines							decimal(28,8), 
	lineqty							decimal(28,8),
	receiptStatus					varchar(9), 
	receiptprocessStatus			varchar(8), 
	processDetail					varchar(100),  
	priority						varchar(2), 
	status							varchar(2), 
	stockstatus						varchar(2), 
	ordertype						varchar(3), 
	orderclass						varchar(10), 
	suppliername					varchar(50), 
	consignmentID					varchar(25), 
	weight							decimal(28,8), 
	actualWeight					decimal(28,8),
	volume							decimal(28,8), 
	dateDueIn						datetime2, 
	dateReceipt						datetime2, 
	dateCreated						datetime2, 
	dateArrival						datetime2, 
	dateClosed						datetime2,  
	datesfixed						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.rec_snapreceipt_aud add constraint [PK_mend_rec_snapreceipt_aud]
	primary key clustered (id);
go
alter table mend.rec_snapreceipt_aud add constraint [DF_mend_rec_snapreceipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceipt_aud_hist(
	id								bigint NOT NULL, 
	receiptid						varchar(25), 
	lines							decimal(28,8), 
	lineqty							decimal(28,8),
	receiptStatus					varchar(9), 
	receiptprocessStatus			varchar(8), 
	processDetail					varchar(100),  
	priority						varchar(2), 
	status							varchar(2), 
	stockstatus						varchar(2), 
	ordertype						varchar(3), 
	orderclass						varchar(10), 
	suppliername					varchar(50), 
	consignmentID					varchar(25), 
	weight							decimal(28,8), 
	actualWeight					decimal(28,8),
	volume							decimal(28,8), 
	dateDueIn						datetime2, 
	dateReceipt						datetime2, 
	dateCreated						datetime2, 
	dateArrival						datetime2, 
	dateClosed						datetime2,  
	datesfixed						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.rec_snapreceipt_aud_hist add constraint [DF_mend.rec_snapreceipt_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ snapreceipts$snapreceipt_receipt ------------

create table mend.rec_snapreceipt_receipt (
	snapreceiptid					bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.rec_snapreceipt_receipt add constraint [PK_mend_rec_snapreceipt_receipt]
	primary key clustered (snapreceiptid,receiptid);
go
alter table mend.rec_snapreceipt_receipt add constraint [DF_mend_rec_snapreceipt_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceipt_receipt_aud(
	snapreceiptid					bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.rec_snapreceipt_receipt_aud add constraint [PK_mend_rec_snapreceipt_receipt_aud]
	primary key clustered (snapreceiptid,receiptid);
go
alter table mend.rec_snapreceipt_receipt_aud add constraint [DF_mend_rec_snapreceipt_receipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go





------------ snapreceipts$snapreceiptline ------------

create table mend.rec_snapreceiptline (
	id								bigint NOT NULL, 
	line							varchar(25),
	skuid							varchar(50), 
	status							varchar(9), 
	level							varchar(2), 
	unitofmeasure					varchar(2), 
	qtyOrdered						decimal(28,8), 
	qtyAdvised						decimal(28,8), 
	qtyReceived						decimal(28,8), 
	qtyDueIn						decimal(28,8), 
	qtyRejected						decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.rec_snapreceiptline add constraint [PK_mend_rec_snapreceiptline]
	primary key clustered (id);
go
alter table mend.rec_snapreceiptline add constraint [DF_mend_rec_snapreceiptline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceiptline_aud(
	id								bigint NOT NULL, 
	line							varchar(25),
	skuid							varchar(50), 
	status							varchar(9), 
	level							varchar(2), 
	unitofmeasure					varchar(2), 
	qtyOrdered						decimal(28,8), 
	qtyAdvised						decimal(28,8), 
	qtyReceived						decimal(28,8), 
	qtyDueIn						decimal(28,8), 
	qtyRejected						decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.rec_snapreceiptline_aud add constraint [PK_mend_rec_snapreceiptline_aud]
	primary key clustered (id);
go
alter table mend.rec_snapreceiptline_aud add constraint [DF_mend_rec_snapreceiptline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceiptline_aud_hist(
	id								bigint NOT NULL, 
	line							varchar(25),
	skuid							varchar(50), 
	status							varchar(9), 
	level							varchar(2), 
	unitofmeasure					varchar(2), 
	qtyOrdered						decimal(28,8), 
	qtyAdvised						decimal(28,8), 
	qtyReceived						decimal(28,8), 
	qtyDueIn						decimal(28,8), 
	qtyRejected						decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.rec_snapreceiptline_aud_hist add constraint [DF_mend.rec_snapreceiptline_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ snapreceipts$snapreceiptline_snapreceipt ------------

create table mend.rec_snapreceiptline_snapreceipt (
	snapreceiptlineid				bigint NOT NULL,
	snapreceiptid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.rec_snapreceiptline_snapreceipt add constraint [PK_mend_rec_snapreceiptline_snapreceipt]
	primary key clustered (snapreceiptlineid,snapreceiptid);
go
alter table mend.rec_snapreceiptline_snapreceipt add constraint [DF_mend_rec_snapreceiptline_snapreceipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceiptline_snapreceipt_aud(
	snapreceiptlineid				bigint NOT NULL,
	snapreceiptid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.rec_snapreceiptline_snapreceipt_aud add constraint [PK_mend_rec_snapreceiptline_snapreceipt_aud]
	primary key clustered (snapreceiptlineid,snapreceiptid);
go
alter table mend.rec_snapreceiptline_snapreceipt_aud add constraint [DF_mend_rec_snapreceiptline_snapreceipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ snapreceipts$snapreceiptline_stockitem ------------

create table mend.rec_snapreceiptline_stockitem (
	snapreceiptlineid				bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.rec_snapreceiptline_stockitem add constraint [PK_mend_rec_snapreceiptline_stockitem]
	primary key clustered (snapreceiptlineid,stockitemid);
go
alter table mend.rec_snapreceiptline_stockitem add constraint [DF_mend_rec_snapreceiptline_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.rec_snapreceiptline_stockitem_aud(
	snapreceiptlineid				bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.rec_snapreceiptline_stockitem_aud add constraint [PK_mend_rec_snapreceiptline_stockitem_aud]
	primary key clustered (snapreceiptlineid,stockitemid);
go
alter table mend.rec_snapreceiptline_stockitem_aud add constraint [DF_mend_rec_snapreceiptline_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go