
---------- Script creacion de tablas ----------

use Landing
go

------------ inventory$warehousestockitem ------------

create table mend.wh_warehousestockitem (
	id								bigint NOT NULL,
	id_string						varchar(200),
	availableqty					decimal(28,8),
	actualstockqty					decimal(28,8),
	allocatedstockqty				decimal(28,8),
	forwarddemand					decimal(28,8),
	dueinqty						decimal(28,8),
	stocked							bit,
	outstandingallocation			decimal(28,8),
	onhold							decimal(28,8),
	stockingmethod					varchar(200),
	changed							bit,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousestockitem add constraint [PK_mend_wh_warehousestockitem]
	primary key clustered (id);
go
alter table mend.wh_warehousestockitem add constraint [DF_mend_wh_warehousestockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitem_aud(
	id								bigint NOT NULL,
	id_string						varchar(200),
	availableqty					decimal(28,8),
	actualstockqty					decimal(28,8),
	allocatedstockqty				decimal(28,8),
	forwarddemand					decimal(28,8),
	dueinqty						decimal(28,8),
	stocked							bit,
	outstandingallocation			decimal(28,8),
	onhold							decimal(28,8),
	stockingmethod					varchar(200),
	changed							bit,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousestockitem_aud add constraint [PK_mend_wh_warehousestockitem_aud]
	primary key clustered (id);
go
alter table mend.wh_warehousestockitem_aud add constraint [DF_mend_wh_warehousestockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitem_aud_hist(
	id								bigint NOT NULL,
	id_string						varchar(200),
	availableqty					decimal(28,8),
	actualstockqty					decimal(28,8),
	allocatedstockqty				decimal(28,8),
	forwarddemand					decimal(28,8),
	dueinqty						decimal(28,8),
	stocked							bit,
	outstandingallocation			decimal(28,8),
	onhold							decimal(28,8),
	stockingmethod					varchar(200),
	changed							bit,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_warehousestockitem_aud_hist add constraint [DF_mend.wh_warehousestockitem_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$located_at ------------

create table mend.wh_located_at (
	warehousestockitemid			bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_located_at add constraint [PK_mend_wh_located_at]
	primary key clustered (warehousestockitemid, warehouseid);
go
alter table mend.wh_located_at add constraint [DF_mend_wh_located_at_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_located_at_aud(
	warehousestockitemid			bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_located_at_aud add constraint [PK_mend_wh_located_at_aud]
	primary key clustered (warehousestockitemid, warehouseid);
go
alter table mend.wh_located_at_aud add constraint [DF_mend_wh_located_at_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$warehousestockitem_stockitem ------------

create table mend.wh_warehousestockitem_stockitem (
	warehousestockitemid			bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousestockitem_stockitem add constraint [PK_mend_wh_warehousestockitem_stockitem]
	primary key clustered (warehousestockitemid, stockitemid);
go
alter table mend.wh_warehousestockitem_stockitem add constraint [DF_mend_wh_warehousestockitem_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitem_stockitem_aud(
	warehousestockitemid			bigint NOT NULL,
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousestockitem_stockitem_aud add constraint [PK_mend_wh_warehousestockitem_stockitem_aud]
	primary key clustered (warehousestockitemid, stockitemid);
go
alter table mend.wh_warehousestockitem_stockitem_aud add constraint [DF_mend_wh_warehousestockitem_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ inventory$warehousestockitembatch ------------

create table mend.wh_warehousestockitembatch (
	id								bigint NOT NULL,
	batch_id						bigint NOT NULL,
	fullyallocated					bit,
	fullyIssued						bit,
	status							varchar(200),
	autoadjusted					bit,
	arriveddate						datetime2,
	receivedquantity				decimal(28,8),
	allocatedquantity				decimal(28,8),
	issuedquantity					decimal(28,8),
	onHoldQuantity					decimal(28,8),
	disposedQuantity				decimal(28,8),
	forwardDemand					decimal(28,8),
	negativeFreeToSell				bit,
	registeredQuantity				decimal(28,8),
	groupFIFODate					datetime2,
	confirmedDate					datetime2,
	stockRegisteredDate				datetime2,
	receiptCreatedDate				datetime2,
	productUnitCost					decimal(28,8),
	carriageUnitCost				decimal(28,8),
	inteCoCarriageUnitCost			decimal(28,8),
	dutyUnitCost					decimal(28,8),
	totalUnitCost					decimal(28,8),
	totalUnitCostIncInterCo			decimal(28,8),
	interCoProfitUnitCost 			decimal(28,8),
	exchangeRate					decimal(28,8),
	currency						varchar(4),
	createdDate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousestockitembatch add constraint [PK_mend_wh_warehousestockitembatch]
	primary key clustered (id);
go
alter table mend.wh_warehousestockitembatch add constraint [DF_mend_wh_warehousestockitembatch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitembatch_aud(
	id								bigint NOT NULL,
	batch_id						bigint NOT NULL,
	fullyallocated					bit,
	fullyIssued						bit,
	status							varchar(200),
	autoadjusted					bit,
	arriveddate						datetime2,
	receivedquantity				decimal(28,8),
	allocatedquantity				decimal(28,8),
	issuedquantity					decimal(28,8),
	onHoldQuantity					decimal(28,8),
	disposedQuantity				decimal(28,8),
	forwardDemand					decimal(28,8),
	negativeFreeToSell				bit,
	registeredQuantity				decimal(28,8),
	groupFIFODate					datetime2,
	confirmedDate					datetime2,
	stockRegisteredDate				datetime2,
	receiptCreatedDate				datetime2,
	productUnitCost					decimal(28,8),
	carriageUnitCost				decimal(28,8),
	inteCoCarriageUnitCost			decimal(28,8),
	dutyUnitCost					decimal(28,8),
	totalUnitCost					decimal(28,8),
	totalUnitCostIncInterCo			decimal(28,8),
	interCoProfitUnitCost 			decimal(28,8),
	exchangeRate					decimal(28,8),
	currency						varchar(4),
	createdDate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousestockitembatch_aud add constraint [PK_mend_wh_warehousestockitembatch_aud]
	primary key clustered (id);
go
alter table mend.wh_warehousestockitembatch_aud add constraint [DF_mend_wh_warehousestockitembatch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitembatch_aud_hist(
	id								bigint NOT NULL,
	batch_id						bigint NOT NULL,
	fullyallocated					bit,
	fullyIssued						bit,
	status							varchar(200),
	autoadjusted					bit,
	arriveddate						datetime2,
	receivedquantity				decimal(28,8),
	allocatedquantity				decimal(28,8),
	issuedquantity					decimal(28,8),
	onHoldQuantity					decimal(28,8),
	disposedQuantity				decimal(28,8),
	forwardDemand					decimal(28,8),
	negativeFreeToSell				bit,
	registeredQuantity				decimal(28,8),
	groupFIFODate					datetime2,
	confirmedDate					datetime2,
	stockRegisteredDate				datetime2,
	receiptCreatedDate				datetime2,
	productUnitCost					decimal(28,8),
	carriageUnitCost				decimal(28,8),
	inteCoCarriageUnitCost			decimal(28,8),
	dutyUnitCost					decimal(28,8),
	totalUnitCost					decimal(28,8),
	totalUnitCostIncInterCo			decimal(28,8),
	interCoProfitUnitCost 			decimal(28,8),
	exchangeRate					decimal(28,8),
	currency						varchar(4),
	createdDate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_warehousestockitembatch_aud_hist add constraint [DF_mend.wh_warehousestockitembatch_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$warehousestockitembatch_warehousestockitem ------------

create table mend.wh_warehousestockitembatch_warehousestockitem (
	warehousestockitembatchid		bigint NOT NULL,
	warehousestockitemid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_warehousestockitembatch_warehousestockitem add constraint [PK_mend_wh_warehousestockitembatch_warehousestockitem]
	primary key clustered (warehousestockitembatchid, warehousestockitemid);
go
alter table mend.wh_warehousestockitembatch_warehousestockitem add constraint [DF_mend_wh_warehousestockitembatch_warehousestockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_warehousestockitembatch_warehousestockitem_aud(
	warehousestockitembatchid		bigint NOT NULL,
	warehousestockitemid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_warehousestockitembatch_warehousestockitem_aud add constraint [PK_mend_wh_warehousestockitembatch_warehousestockitem_aud]
	primary key clustered (warehousestockitembatchid, warehousestockitemid);
go
alter table mend.wh_warehousestockitembatch_warehousestockitem_aud add constraint [DF_mend_wh_warehousestockitembatch_warehousestockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go





------------ inventory$batchstockissue ------------

create table mend.wh_batchstockissue (
	id								bigint NOT NULL,
	issueid							bigint,
	createddate						datetime2,
	changeddate						datetime2,
	issuedquantity					decimal(28,8),
	cancelled						bit,
	shippeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_batchstockissue add constraint [PK_mend_wh_batchstockissue]
	primary key clustered (id);
go
alter table mend.wh_batchstockissue add constraint [DF_mend_wh_batchstockissue_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.wh_batchstockissue_aud(
	id								bigint NOT NULL,
	issueid							bigint,
	createddate						datetime2,
	changeddate						datetime2,
	issuedquantity					decimal(28,8),
	cancelled						bit,
	shippeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_batchstockissue_aud add constraint [PK_mend_wh_batchstockissue_aud]
	primary key clustered (id);
go
alter table mend.wh_batchstockissue_aud add constraint [DF_mend_wh_batchstockissue_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockissue_aud_hist(
	id								bigint NOT NULL,
	issueid							bigint,
	createddate						datetime2,
	changeddate						datetime2,
	issuedquantity					decimal(28,8),
	cancelled						bit,
	shippeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_batchstockissue_aud_hist add constraint [DF_mend.wh_batchstockissue_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$batchstockissue_warehousestockitembatch ------------

create table mend.wh_batchstockissue_warehousestockitembatch (
	batchstockissueid				bigint NOT NULL,
	warehousestockitembatchid		bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_batchstockissue_warehousestockitembatch add constraint [PK_mend_wh_batchstockissue_warehousestockitembatch]
	primary key clustered (batchstockissueid, warehousestockitembatchid);
go
alter table mend.wh_batchstockissue_warehousestockitembatch add constraint [DF_mend_wh_batchstockissue_warehousestockitembatch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockissue_warehousestockitembatch_aud(
	batchstockissueid				bigint NOT NULL,
	warehousestockitembatchid		bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_batchstockissue_warehousestockitembatch_aud add constraint [PK_mend_wh_batchstockissue_warehousestockitembatch_aud]
	primary key clustered (batchstockissueid, warehousestockitembatchid);
go
alter table mend.wh_batchstockissue_warehousestockitembatch_aud add constraint [DF_mend_wh_batchstockissue_warehousestockitembatch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ inventory$batchstockallocation ------------

create table mend.wh_batchstockallocation (
	id								bigint NOT NULL,
	fullyissued						bit,
	cancelled						bit,
	allocatedquantity				decimal(28,8),
	allocatedunits					decimal(28,8),
	issuedquantity					decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_batchstockallocation add constraint [PK_mend_wh_batchstockallocation]
	primary key clustered (id);
go
alter table mend.wh_batchstockallocation add constraint [DF_mend_wh_batchstockallocation_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockallocation_aud(
	id								bigint NOT NULL,
	fullyissued						bit,
	cancelled						bit,
	allocatedquantity				decimal(28,8),
	allocatedunits					decimal(28,8),
	issuedquantity					decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_batchstockallocation_aud add constraint [PK_mend_wh_batchstockallocation_aud]
	primary key clustered (id);
go
alter table mend.wh_batchstockallocation_aud add constraint [DF_mend_wh_batchstockallocation_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockallocation_aud_hist(
	id								bigint NOT NULL,
	fullyissued						bit,
	cancelled						bit,
	allocatedquantity				decimal(28,8),
	allocatedunits					decimal(28,8),
	issuedquantity					decimal(28,8),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.wh_batchstockallocation_aud_hist add constraint [DF_mend.wh_batchstockallocation_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$batchstockissue_batchstockallocation ------------

create table mend.wh_batchstockissue_batchstockallocation (
	batchstockissueid				bigint NOT NULL,
	batchstockallocationid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.wh_batchstockissue_batchstockallocation add constraint [PK_mend_wh_batchstockissue_batchstockallocation]
	primary key clustered (batchstockissueid, batchstockallocationid);
go
alter table mend.wh_batchstockissue_batchstockallocation add constraint [DF_mend_wh_batchstockissue_batchstockallocation_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockissue_batchstockallocation_aud(
	batchstockissueid				bigint NOT NULL,
	batchstockallocationid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.wh_batchstockissue_batchstockallocation_aud add constraint [PK_mend_wh_batchstockissue_batchstockallocation_aud]
	primary key clustered (batchstockissueid, batchstockallocationid);
go
alter table mend.wh_batchstockissue_batchstockallocation_aud add constraint [DF_mend_wh_batchstockissue_batchstockallocation_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$batchtransaction_orderlinestockallocationtransaction ------------

create table mend.wh_batchtransaction_orderlinestockallocationtransaction (
	batchstockallocationid					bigint NOT NULL,
	orderlinestockallocationtransactionid	bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_batchtransaction_orderlinestockallocationtransaction add constraint [PK_mend_wh_batchtransaction_orderlinestockallocationtransaction]
	primary key clustered (batchstockallocationid, orderlinestockallocationtransactionid);
go
alter table mend.wh_batchtransaction_orderlinestockallocationtransaction add constraint [DF_mend_wh_batchtransaction_orderlinestockallocationtransaction_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchtransaction_orderlinestockallocationtransaction_aud(
	batchstockallocationid					bigint NOT NULL,
	orderlinestockallocationtransactionid	bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_batchtransaction_orderlinestockallocationtransaction_aud add constraint [PK_mend_wh_batchtransaction_orderlinestockallocationtransaction_aud]
	primary key clustered (batchstockallocationid, orderlinestockallocationtransactionid);
go
alter table mend.wh_batchtransaction_orderlinestockallocationtransaction_aud add constraint [DF_mend_wh_batchtransaction_orderlinestockallocationtransaction_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go





------------ inventory$repairstockbatchs ------------

create table mend.wh_repairstockbatchs (
	id										bigint NOT NULL,
	batchtransaction_id						int,
	reference								bigint,
	date									datetime2,
	oldallocation							decimal(28,8),
	newallocation							decimal(28,8),
	oldissue								decimal(28,8),
	newissue								decimal(28,8),
	processed								bit,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_repairstockbatchs add constraint [PK_mend_wh_wh_repairstockbatchs]
	primary key clustered (id);
go
alter table mend.wh_repairstockbatchs add constraint [DF_mend_wh_wh_repairstockbatchs_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_repairstockbatchs_aud(
	id										bigint NOT NULL,
	batchtransaction_id						int,
	reference								bigint,
	date									datetime2,
	oldallocation							decimal(28,8),
	newallocation							decimal(28,8),
	oldissue								decimal(28,8),
	newissue								decimal(28,8),
	processed								bit,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_repairstockbatchs_aud add constraint [PK_mend_wh_repairstockbatchs_aud]
	primary key clustered (id);
go
alter table mend.wh_repairstockbatchs_aud add constraint [DF_mend_wh_repairstockbatchs_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_repairstockbatchs_aud_hist(
	id										bigint NOT NULL,
	batchtransaction_id						int,
	reference								bigint,
	date									datetime2,
	oldallocation							decimal(28,8),
	newallocation							decimal(28,8),
	oldissue								decimal(28,8),
	newissue								decimal(28,8),
	processed								bit,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	aud_type								char(1) NOT NULL, 
	aud_dateFrom							datetime NOT NULL, 
	aud_dateTo								datetime NOT NULL);
go 

alter table mend.wh_repairstockbatchs_aud_hist add constraint [DF_mend.wh_repairstockbatchs_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$repairstockbatchs_warehousestockitembatch ------------

create table mend.wh_repairstockbatchs_warehousestockitembatch (
	repairstockbatchsid						bigint NOT NULL,
	warehousestockitembatchid				bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_repairstockbatchs_warehousestockitembatch add constraint [PK_mend_wh_repairstockbatchs_warehousestockitembatch]
	primary key clustered (repairstockbatchsid, warehousestockitembatchid);
go
alter table mend.wh_repairstockbatchs_warehousestockitembatch add constraint [DF_mend_wh_repairstockbatchs_warehousestockitembatch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_repairstockbatchs_warehousestockitembatch_aud(
	repairstockbatchsid					bigint NOT NULL,
	warehousestockitembatchid			bigint NOT NULL,
	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL,
	upd_ts								datetime);
go 

alter table mend.wh_repairstockbatchs_warehousestockitembatch_aud add constraint [PK_mend_wh_repairstockbatchs_warehousestockitembatch_aud]
	primary key clustered (repairstockbatchsid, warehousestockitembatchid);
go
alter table mend.wh_repairstockbatchs_warehousestockitembatch_aud add constraint [DF_mend_wh_repairstockbatchs_warehousestockitembatch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go









------------ inventory$stockmovement ------------

create table mend.wh_stockmovement (
	id										bigint NOT NULL, 
	moveid									varchar(20), 
	movelineid								varchar(20), 
	stockadjustment							varchar(8), 
	movementtype							varchar(6), 
	movetype								varchar(20), 
	_class									varchar(25), 
	reasonid								varchar(10), 
	status									varchar(2), 
	skuid									varchar(50), 
	qtyactioned								int, 
	fromstate								varchar(5), 
	tostate									varchar(5), 
	fromstatus								varchar(2), 
	tostatus								varchar(2), 
	operator								varchar(20), 
	supervisor								varchar(20),
	dateCreated								datetime2, 
	dateClosed								datetime2,
	createddate								datetime2,
	changeddate								datetime2,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_stockmovement add constraint [PK_mend_wh_stockmovement]
	primary key clustered (id);
go
alter table mend.wh_stockmovement add constraint [DF_mend_wh_stockmovement_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_stockmovement_aud(
	id										bigint NOT NULL, 
	moveid									varchar(20), 
	movelineid								varchar(20), 
	stockadjustment							varchar(8), 
	movementtype							varchar(6), 
	movetype								varchar(20), 
	_class									varchar(25), 
	reasonid								varchar(10), 
	status									varchar(2), 
	skuid									varchar(50), 
	qtyactioned								int, 
	fromstate								varchar(5), 
	tostate									varchar(5), 
	fromstatus								varchar(2), 
	tostatus								varchar(2), 
	operator								varchar(20), 
	supervisor								varchar(20),
	dateCreated								datetime2, 
	dateClosed								datetime2,
	createddate								datetime2,
	changeddate								datetime2,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_stockmovement_aud add constraint [PK_mend_wh_stockmovement_aud]
	primary key clustered (id);
go
alter table mend.wh_stockmovement_aud add constraint [DF_mend_wh_stockmovement_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_stockmovement_aud_hist(
	id										bigint NOT NULL, 
	moveid									varchar(20), 
	movelineid								varchar(20), 
	stockadjustment							varchar(8), 
	movementtype							varchar(6), 
	movetype								varchar(20), 
	_class									varchar(25), 
	reasonid								varchar(10), 
	status									varchar(2), 
	skuid									varchar(50), 
	qtyactioned								int, 
	fromstate								varchar(5), 
	tostate									varchar(5), 
	fromstatus								varchar(2), 
	tostatus								varchar(2), 
	operator								varchar(20), 
	supervisor								varchar(20),
	dateCreated								datetime2, 
	dateClosed								datetime2,
	createddate								datetime2,
	changeddate								datetime2,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	aud_type								char(1) NOT NULL, 
	aud_dateFrom							datetime NOT NULL, 
	aud_dateTo								datetime NOT NULL);
go 

alter table mend.wh_stockmovement_aud_hist add constraint [DF_mend.wh_stockmovement_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$stockmovement_warehousestockitem ------------

create table mend.wh_stockmovement_warehousestockitem (
	stockmovementid							bigint NOT NULL,
	warehousestockitemid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_stockmovement_warehousestockitem add constraint [PK_mend_wh_stockmovement_warehousestockitem]
	primary key clustered (stockmovementid, warehousestockitemid);
go
alter table mend.wh_stockmovement_warehousestockitem add constraint [DF_mend_wh_stockmovement_warehousestockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_stockmovement_warehousestockitem_aud(
	stockmovementid							bigint NOT NULL,
	warehousestockitemid					bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_stockmovement_warehousestockitem_aud add constraint [PK_mend_wh_stockmovement_warehousestockitem_aud]
	primary key clustered (stockmovementid, warehousestockitemid);
go
alter table mend.wh_stockmovement_warehousestockitem_aud add constraint [DF_mend_wh_stockmovement_warehousestockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------ inventory$batchstockmovement ------------

create table mend.wh_batchstockmovement (
	id										bigint NOT NULL,
	batchstockmovement_id					bigint, 
	batchstockmovementtype					varchar(15),
	quantity								decimal(28,8),
	comment									varchar(200),
	createddate								datetime2,
	changeddate								datetime2,
	system$owner							bigint,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_batchstockmovement add constraint [PK_mend_wh_batchstockmovement]
	primary key clustered (id);
go
alter table mend.wh_batchstockmovement add constraint [DF_mend_wh_batchstockmovement_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockmovement_aud(
	id										bigint NOT NULL,
	batchstockmovement_id					bigint, 
	batchstockmovementtype					varchar(15),
	quantity								decimal(28,8),
	comment									varchar(200),
	createddate								datetime2,
	changeddate								datetime2,
	system$owner							bigint,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_batchstockmovement_aud add constraint [PK_mend_wh_batchstockmovement_aud]
	primary key clustered (id);
go
alter table mend.wh_batchstockmovement_aud add constraint [DF_mend_wh_batchstockmovement_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockmovement_aud_hist(
	id										bigint NOT NULL,
	batchstockmovement_id					bigint, 
	batchstockmovementtype					varchar(15),
	quantity								decimal(28,8),
	comment									varchar(200),
	createddate								datetime2,
	changeddate								datetime2,
	system$owner							bigint,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	aud_type								char(1) NOT NULL, 
	aud_dateFrom							datetime NOT NULL, 
	aud_dateTo								datetime NOT NULL);
go 

alter table mend.wh_batchstockmovement_aud_hist add constraint [DF_mend.wh_batchstockmovement_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$batchstockmovement_stockmovement ------------

create table mend.wh_batchstockmovement_stockmovement (
	batchstockmovementid					bigint NOT NULL,
	stockmovementid							bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_batchstockmovement_stockmovement add constraint [PK_mend_wh_batchstockmovement_stockmovement]
	primary key clustered (batchstockmovementid, stockmovementid);
go
alter table mend.wh_batchstockmovement_stockmovement add constraint [DF_mend_wh_batchstockmovement_stockmovement_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockmovement_stockmovement_aud(
	batchstockmovementid					bigint NOT NULL,
	stockmovementid							bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_batchstockmovement_stockmovement_aud add constraint [PK_mend_wh_batchstockmovement_stockmovement_aud]
	primary key clustered (batchstockmovementid, stockmovementid);
go
alter table mend.wh_batchstockmovement_stockmovement_aud add constraint [DF_mend_wh_batchstockmovement_stockmovement_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ inventory$batchstockmovement_warehousestockitembatch ------------

create table mend.wh_batchstockmovement_warehousestockitembatch (
	batchstockmovementid					bigint NOT NULL,
	warehousestockitembatchid				bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

alter table mend.wh_batchstockmovement_warehousestockitembatch add constraint [PK_mend_wh_batchstockmovement_warehousestockitembatch]
	primary key clustered (batchstockmovementid, warehousestockitembatchid);
go
alter table mend.wh_batchstockmovement_warehousestockitembatch add constraint [DF_mend_wh_batchstockmovement_warehousestockitembatch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.wh_batchstockmovement_warehousestockitembatch_aud(
	batchstockmovementid					bigint NOT NULL,
	warehousestockitembatchid				bigint NOT NULL,
	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL,
	upd_ts									datetime);
go 

alter table mend.wh_batchstockmovement_warehousestockitembatch_aud add constraint [PK_mend_wh_batchstockmovement_warehousestockitembatch_aud]
	primary key clustered (batchstockmovementid, warehousestockitembatchid);
go
alter table mend.wh_batchstockmovement_warehousestockitembatch_aud add constraint [DF_mend_wh_batchstockmovement_warehousestockitembatch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
