
------------------------------- Triggers ----------------------------------

use Landing
go

------------------------------- purchaseorders$purchaseorder -------------------------------

drop trigger mend.trg_purc_purchaseorder;
go 

create trigger mend.trg_purc_purchaseorder on mend.purc_purchaseorder
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorder_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.ordernumber, ' '), isnull(trg.source, ' '), isnull(trg.status, ' '), isnull(trg.potype, ' '), 
			isnull(trg.leadtime, 0),
			isnull(trg.itemcost, 0), isnull(trg.totalprice, 0), isnull(trg.formattedprice, ' '),
			isnull(trg.duedate, ' '), isnull(trg.receiveddate, ' '), isnull(trg.createddate, ' '),
			isnull(trg.approveddate, ' '), isnull(trg.approvedby, ' '), isnull(trg.confirmeddate, ' '), isnull(trg.confirmedby, ' '), isnull(trg.submitteddate, ' '), isnull(trg.submittedby, ' '), isnull(trg.completeddate, ' '), isnull(trg.completedby, ' '), 
			isnull(trg.transferleadtime, 0), isnull(trg.transitleadtime, 0), 
			isnull(trg.spoleadtime, 0), isnull(trg.spoduedate, ' '), 
			isnull(trg.tpoleadtime, 0), isnull(trg.tpoduedate, ' '), -- isnull(trg.tpotransferleadtime, 0), isnull(trg.tpotransferdate, ' '), 
			isnull(trg.requiredshipdate, ' '), isnull(trg.requiredtransferdate, ' '), 
			isnull(trg.estimatedpaymentdate, ' '), isnull(trg.estimateddepositdate, ' '), 
			isnull(trg.supplierreference, ' ')
			-- , isnull(trg.changeddate, ' ')

		intersect
		select 
			isnull(src.ordernumber, ' '), isnull(src.source, ' '), isnull(src.status, ' '), isnull(src.potype, ' '), 
			isnull(src.leadtime, 0),
			isnull(src.itemcost, 0), isnull(src.totalprice, 0), isnull(src.formattedprice, ' '),
			isnull(src.duedate, ' '), isnull(src.receiveddate, ' '), isnull(src.createddate, ' '),
			isnull(src.approveddate, ' '), isnull(src.approvedby, ' '), isnull(src.confirmeddate, ' '), isnull(src.confirmedby, ' '), isnull(src.submitteddate, ' '), isnull(src.submittedby, ' '), isnull(src.completeddate, ' '), isnull(src.completedby, ' '),
			isnull(src.transferleadtime, 0), isnull(src.transitleadtime, 0), 
			isnull(src.spoleadtime, 0), isnull(src.spoduedate, ' '), 
			isnull(src.tpoleadtime, 0), isnull(src.tpoduedate, ' '), -- isnull(src.tpotransferleadtime, 0), isnull(src.tpotransferdate, ' '), 
			isnull(src.requiredshipdate, ' '), isnull(src.requiredtransferdate, ' '), 
			isnull(src.estimatedpaymentdate, ' '), isnull(src.estimateddepositdate, ' '),
			isnull(src.supplierreference, ' ')
			-- , isnull(src.changeddate, ' ')
			)		
		then
			update set
				trg.ordernumber = src.ordernumber, trg.source = src.source, trg.status = src.status, trg.potype = src.potype, 
				trg.leadtime = src.leadtime,
				trg.itemcost = src.itemcost, trg.totalprice = src.totalprice, trg.formattedprice = src.formattedprice,
				trg.duedate = src.duedate, trg.receiveddate = src.receiveddate, trg.createddate = src.createddate,
				trg.approveddate = src.approveddate, trg.approvedby = src.approvedby, trg.confirmeddate = src.confirmeddate, trg.confirmedby = src.confirmedby, trg.submitteddate = src.submitteddate, trg.submittedby = src.submittedby, trg.completeddate = src.completeddate, trg.completedby = src.completedby, 
				trg.transferleadtime = src.transferleadtime, trg.transitleadtime = src.transitleadtime, 
				trg.spoleadtime = src.spoleadtime, trg.spoduedate = src.spoduedate, 
				trg.tpoleadtime = src.tpoleadtime, trg.tpotransferleadtime = src.tpotransferleadtime, trg.tpotransferdate = src.tpotransferdate, trg.tpoduedate = src.tpoduedate, 
				trg.requiredshipdate = src.requiredshipdate, trg.requiredtransferdate = src.requiredtransferdate, 
				trg.estimatedpaymentdate = src.estimatedpaymentdate, trg.estimateddepositdate = src.estimateddepositdate, 
				trg.supplierreference = src.supplierreference, 
				trg.changeddate = src.changeddate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				ordernumber, source, status, potype,
				leadtime, 
				itemcost, totalprice, formattedprice, 
				duedate, receiveddate, createddate, 
				approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
				transferleadtime, transitleadtime, 
				spoleadtime, spoduedate, 
				tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
				requiredshipdate, requiredtransferdate, 
				estimatedpaymentdate, estimateddepositdate,
				supplierreference,
				changeddate,
				idETLBatchRun)

				values (src.id,
					src.ordernumber, src.source, src.status, src.potype,
					src.leadtime, 
					src.itemcost, src.totalprice, src.formattedprice, 
					src.duedate, src.receiveddate, src.createddate, 
					src.approveddate, src.approvedby, src.confirmeddate, src.confirmedby, src.submitteddate, src.submittedby, src.completeddate, src.completedby,
					src.transferleadtime, src.transitleadtime, 
					src.spoleadtime, src.spoduedate, 
					src.tpoleadtime, src.tpotransferleadtime, src.tpotransferdate, src.tpoduedate, 
					src.requiredshipdate, src.requiredtransferdate, 
					src.estimatedpaymentdate, src.estimateddepositdate,
					src.supplierreference,
					src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_purc_purchaseorder_aud;
go 

create trigger mend.trg_purc_purchaseorder_aud on mend.purc_purchaseorder_aud
for update, delete
as
begin
	set nocount on

	insert mend.purc_purchaseorder_aud_hist (id,
		ordernumber, source, status, potype,
		leadtime, 
		itemcost, totalprice, formattedprice, 
		duedate, receiveddate, createddate, 
		approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby,
		transferleadtime, transitleadtime, 
		spoleadtime, spoduedate, 
		tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
		requiredshipdate, requiredtransferdate, 
		estimatedpaymentdate, estimateddepositdate,
		supplierreference,
		changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.ordernumber, d.source, d.status, d.potype,
			d.leadtime, 
			d.itemcost, d.totalprice, d.formattedprice, 
			d.duedate, d.receiveddate, d.createddate, 
			d.approveddate, d.approvedby, d.confirmeddate, d.confirmedby, d.submitteddate, d.submittedby, d.completeddate, d.completedby,
			d.transferleadtime, d.transitleadtime, 
			d.spoleadtime, d.spoduedate, 
			d.tpoleadtime, d.tpotransferleadtime, d.tpotransferdate, d.tpoduedate, 
			d.requiredshipdate, d.requiredtransferdate, 
			d.estimatedpaymentdate, d.estimateddepositdate,
			d.supplierreference,
			d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$purchaseorder_supplier -------------------

drop trigger mend.trg_purc_purchaseorder_supplier;
go 

create trigger mend.trg_purc_purchaseorder_supplier on mend.purc_purchaseorder_supplier
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorder_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderid = src.purchaseorderid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (purchaseorderid, supplierid, 
				idETLBatchRun)

				values (src.purchaseorderid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$purchaseorder_warehouse -------------------

drop trigger mend.trg_purc_purchaseorder_warehouse;
go 

create trigger mend.trg_purc_purchaseorder_warehouse on mend.purc_purchaseorder_warehouse
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorder_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderid = src.purchaseorderid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (purchaseorderid, warehouseid, 
				idETLBatchRun)

				values (src.purchaseorderid, src.warehouseid,
					src.idETLBatchRun);
end; 
go







------------------- purchaseorders$purchaseorderlineheader -------------------

drop trigger mend.trg_purc_purchaseorderlineheader;
go 

create trigger mend.trg_purc_purchaseorderlineheader on mend.purc_purchaseorderlineheader
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderlineheader_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.status, ' '), isnull(trg.problems, ' '),
			isnull(trg.lines, 0), isnull(trg.items, 0), isnull(trg.totalprice, 0),
			isnull(trg.createddate, ' ')
			-- , isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.status, ' '), isnull(src.problems, ' '),
			isnull(src.lines, 0), isnull(src.items, 0), isnull(src.totalprice, 0),
			isnull(src.createddate, ' ')
			-- , isnull(src.changeddate, ' ')
			)		
		then
			update set
				trg.status = src.status, trg.problems = src.problems,
				trg.lines = src.lines, trg.items = src.items,trg.totalprice = src.totalprice,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				status, problems, 
				lines, items, totalprice, 
				createddate, changeddate, 
				idETLBatchRun)

				values (src.id,
					src.status, src.problems, 
					src.lines, src.items, src.totalprice, 
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_purc_purchaseorderlineheader_aud;
go 

create trigger mend.trg_purc_purchaseorderlineheader_aud on mend.purc_purchaseorderlineheader_aud
for update, delete
as
begin
	set nocount on

	insert mend.purc_purchaseorderlineheader_aud_hist (id,
		status, problems, 
		lines, items, totalprice, 
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.status, d.problems, 
			d.lines, d.items, d.totalprice, 
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$purchaseorderlineheader_purchaseorder -------------------

drop trigger mend.trg_purc_purchaseorderlineheader_purchaseorder;
go 

create trigger mend.trg_purc_purchaseorderlineheader_purchaseorder on mend.purc_purchaseorderlineheader_purchaseorder
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderlineheader_purchaseorder_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineheaderid = src.purchaseorderlineheaderid and trg.purchaseorderid = src.purchaseorderid)
	when not matched 
		then
			insert (purchaseorderlineheaderid, purchaseorderid, 
				idETLBatchRun)

				values (src.purchaseorderlineheaderid, src.purchaseorderid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$purchaseorderlineheader_productfamily -------------------

drop trigger mend.trg_purc_purchaseorderlineheader_productfamily;
go 

create trigger mend.trg_purc_purchaseorderlineheader_productfamily on mend.purc_purchaseorderlineheader_productfamily
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderlineheader_productfamily_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineheaderid = src.purchaseorderlineheaderid and trg.productfamilyid = src.productfamilyid)
	when not matched 
		then
			insert (purchaseorderlineheaderid, productfamilyid, 
				idETLBatchRun)

				values (src.purchaseorderlineheaderid, src.productfamilyid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$purchaseorderlineheader_packsize -------------------

drop trigger mend.trg_purc_purchaseorderlineheader_packsize;
go 

create trigger mend.trg_purc_purchaseorderlineheader_packsize on mend.purc_purchaseorderlineheader_packsize
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderlineheader_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineheaderid = src.purchaseorderlineheaderid and trg.packsizeid = src.packsizeid)
	when not matched 
		then
			insert (purchaseorderlineheaderid, packsizeid, 
				idETLBatchRun)

				values (src.purchaseorderlineheaderid, src.packsizeid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$purchaseorderlineheader_supplierprices -------------------

drop trigger mend.trg_purc_purchaseorderlineheader_supplierprices;
go 

create trigger mend.trg_purc_purchaseorderlineheader_supplierprices on mend.purc_purchaseorderlineheader_supplierprices
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderlineheader_supplierprices_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineheaderid = src.purchaseorderlineheaderid and trg.supplierpriceid = src.supplierpriceid)
	when not matched 
		then
			insert (purchaseorderlineheaderid, supplierpriceid, 
				idETLBatchRun)

				values (src.purchaseorderlineheaderid, src.supplierpriceid,
					src.idETLBatchRun);
end; 
go







------------------- purchaseorders$purchaseorderline -------------------

drop trigger mend.trg_purc_purchaseorderline;
go 

create trigger mend.trg_purc_purchaseorderline on mend.purc_purchaseorderline
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderline_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.po_lineID, 0), isnull(trg.stockItemID, 0),
			isnull(trg.quantity, 0), isnull(trg.quantityReceived, 0), --  isnull(trg.quantityDelivered, 0), 
			isnull(trg.quantityInTransit, 0), isnull(trg.quantityPlanned, 0), isnull(trg.quantityOpen, 0), isnull(trg.quantityUnallocated, 0), isnull(trg.quantityCancelled, 0), isnull(trg.quantityExWorks, 0), isnull(trg.quantityRejected, 0),
			isnull(trg.lineprice, 0),
			isnull(trg.backtobackduedate, ' '),
			isnull(trg.createdDate, ' ')
			-- , isnull(trg.changedDate, ' ')
		intersect
		select 
			isnull(src.po_lineID, 0), isnull(src.stockItemID, 0),
			isnull(src.quantity, 0), isnull(src.quantityReceived, 0), -- isnull(src.quantityDelivered, 0), 
			isnull(src.quantityInTransit, 0), isnull(src.quantityPlanned, 0), isnull(src.quantityOpen, 0), isnull(src.quantityUnallocated, 0), isnull(src.quantityCancelled, 0), isnull(src.quantityExWorks, 0), isnull(src.quantityRejected, 0),
			isnull(src.lineprice, 0),
			isnull(src.backtobackduedate, ' '),
			isnull(src.createdDate, ' ') 
			-- , isnull(src.changedDate, ' ')
			)		
		then
			update set
				trg.po_lineID = src.po_lineID, trg.stockItemID = src.stockItemID,
				trg.quantity = src.quantity, trg.quantityReceived = src.quantityReceived, -- trg.quantityDelivered = src.quantityDelivered, 
				trg.quantityInTransit = src.quantityInTransit, trg.quantityPlanned = src.quantityPlanned, trg.quantityOpen = src.quantityOpen, trg.quantityUnallocated = src.quantityUnallocated, trg.quantityCancelled = src.quantityCancelled, trg.quantityExWorks = src.quantityExWorks, trg.quantityRejected = src.quantityRejected,
				trg.lineprice = src.lineprice, 
				trg.backtobackduedate = src.backtobackduedate,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				po_lineID, stockItemID,
				quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
				lineprice, 
				backtobackduedate,
				createdDate, changedDate,
				idETLBatchRun)

				values (src.id,
					src.po_lineID, src.stockItemID,
					src.quantity, src.quantityReceived, src.quantityDelivered, src.quantityInTransit, src.quantityPlanned, src.quantityOpen, src.quantityUnallocated, src.quantityCancelled, src.quantityExWorks, src.quantityRejected,
					src.lineprice, 
					src.backtobackduedate,
					src.createdDate, src.changedDate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_purc_purchaseorderline_aud;
go 

create trigger mend.trg_purc_purchaseorderline_aud on mend.purc_purchaseorderline_aud
for update, delete
as
begin
	set nocount on

	insert mend.purc_purchaseorderline_aud_hist (id,
		po_lineID, stockItemID,
		quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
		lineprice, 
		backtobackduedate,
		createdDate, changedDate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.po_lineID, d.stockItemID,
			d.quantity, d.quantityReceived, d.quantityDelivered, d.quantityInTransit, d.quantityPlanned, d.quantityOpen, d.quantityUnallocated, d.quantityCancelled, d.quantityExWorks, d.quantityRejected,
			d.lineprice, 
			d.backtobackduedate,
			d.createdDate, d.changedDate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$purchaseorderline_purchaseorderlineheader -------------------

drop trigger mend.trg_purc_purchaseorderline_purchaseorderlineheader;
go 

create trigger mend.trg_purc_purchaseorderline_purchaseorderlineheader on mend.purc_purchaseorderline_purchaseorderlineheader
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderline_purchaseorderlineheader_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineid = src.purchaseorderlineid and trg.purchaseorderlineheaderid = src.purchaseorderlineheaderid)
	when not matched 
		then
			insert (purchaseorderlineid, purchaseorderlineheaderid, 
				idETLBatchRun)

				values (src.purchaseorderlineid, src.purchaseorderlineheaderid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$purchaseorderline_stockitem -------------------

drop trigger mend.trg_purc_purchaseorderline_stockitem;
go 

create trigger mend.trg_purc_purchaseorderline_stockitem on mend.purc_purchaseorderline_stockitem
after insert
as
begin
	set nocount on

	merge into mend.purc_purchaseorderline_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.purchaseorderlineid = src.purchaseorderlineid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (purchaseorderlineid, stockitemid, 
				idETLBatchRun)

				values (src.purchaseorderlineid, src.stockitemid,
					src.idETLBatchRun);
end; 
go