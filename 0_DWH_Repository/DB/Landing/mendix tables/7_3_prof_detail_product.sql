use Landing
go 

------------------------------------------------------------------------
----------------------- product$productcategory ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_productcategory_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_productcategory_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_productcategory_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_productcategory_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			code, name, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_productcategory_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$productfamily ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_productfamily_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_productfamily_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_productfamily_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_productfamily_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
			productFamilyType, 
			promotionalProduct, updateSNAPDescription, createToOrder, 
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_productfamily_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$productfamily_productcategory ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_productfamily_productcategory_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- product$product ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_product_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_product_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_product_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_product_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			productType, valid, 
			SKU, oldSKU, description, 
			isBOM, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_product_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$product_productfamily ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_product_productfamily_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- product$stockitem ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_stockitem_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_stockitem_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_stockitem_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			SKU, oldSKU, SNAPDescription, packSize, 
			manufacturerArticleID, manufacturerCodeNumber,
			SNAPUploadStatus, 
			changed, 
			createdDate, changedDate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_stockitem_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$stockitem_product ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_stockitem_product_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- product$contactlens ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_contactlens_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_contactlens_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_contactlens_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_contactlens_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			bc, di, po, 
			cy, ax,  
			ad, _do, co, co_EDI, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_contactlens_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$packsize ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_packsize_aud

	select idETLBatchRun, count(*) num_rows
	from mend.prod_packsize_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.prod_packsize_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			description, size, 
			packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
			height, width, weight, depth, 
			productGroup1, productGroup2, productGroup2Type, 
			breakable, 
			packsperpallet, packspercarton, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_packsize_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- product$packsize_productfamily ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_packsize_productfamily_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- product$stockitem_packsize ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.prod_stockitem_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun