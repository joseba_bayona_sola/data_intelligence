use Landing
go 

------------------------------------------------------------------------
---------------------- inventory$warehousestockitem --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousestockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select convert(date, upd_ts), count(*) num_rows
	from mend.wh_warehousestockitem_aud
	group by convert(date, upd_ts)
	order by convert(date, upd_ts)

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehousestockitem_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_warehousestockitem_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehousestockitem_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			id_string, 
			availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
			outstandingallocation, onhold,
			stockingmethod, 
			changed, 
			createddate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehousestockitem_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
---------------------- inventory$located_at --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_located_at_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
---------------- inventory$warehousestockitem_stockitem ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousestockitem_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
-------------------- inventory$warehousestockitembatch -----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousestockitembatch_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehousestockitembatch_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_warehousestockitembatch_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehousestockitembatch_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id,
			batch_id,
			fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
			receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
			groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
			productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
			exchangeRate, currency,
			createddate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehousestockitembatch_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
--------- inventory$warehousestockitembatch_warehousestockitem ---------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousestockitembatch_warehousestockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
-------------------- inventory$batchstockissue -------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockissue_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockissue_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_batchstockissue_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockissue_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			issueid, createddate, changeddate, issuedquantity, 
			cancelled, shippeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockissue_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
--------- inventory$batchstockissue_warehousestockitembatch ------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockissue_warehousestockitembatch_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
-------------------- inventory$batchstockallocation --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockallocation_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockallocation_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_batchstockallocation_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockallocation_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
			createddate, changeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockallocation_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
--------- inventory$batchstockissue_batchstockallocation ---------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockissue_batchstockallocation_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------
--------- inventory$batchtransaction_orderlinestockallocationtransaction ---------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchtransaction_orderlinestockallocationtransaction_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
-------------------- inventory$repairstockbatchs -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_repairstockbatchs_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_repairstockbatchs_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_repairstockbatchs_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_repairstockbatchs_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			batchtransaction_id, reference, date,
			oldallocation, newallocation, oldissue, newissue, processed, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_repairstockbatchs_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


----------------------------------------------------------------------------------------
--------- inventory$repairstockbatchs_warehousestockitembatch ---------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_repairstockbatchs_warehousestockitembatch_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
-------------------- inventory$stockmovement ---------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_stockmovement_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_stockmovement_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_stockmovement_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_stockmovement_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			moveid, movelineid, 
			stockadjustment, movementtype, movetype, _class, reasonid, status, 
			skuid, qtyactioned, 
			fromstate, tostate, 
			fromstatus, tostatus, 
			operator, supervisor,
			dateCreated, dateClosed, 
			createddate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_stockmovement_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

----------------------------------------------------------------------------------------
---------------- inventory$stockmovement_warehousestockitem -------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_stockmovement_warehousestockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
-------------------- inventory$batchstockmovement ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockmovement_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockmovement_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_batchstockmovement_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_batchstockmovement_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			batchstockmovement_id, batchstockmovementtype, 
			quantity, comment, 
			createddate, changeddate, system$owner,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockmovement_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

----------------------------------------------------------------------------------------
---------------- inventory$batchstockmovement_stockmovement ----------------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockmovement_stockmovement_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------
---------------- inventory$batchstockmovement_warehousestockitembatch ------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_batchstockmovement_warehousestockitembatch_aud
	group by idETLBatchRun
	order by idETLBatchRun
