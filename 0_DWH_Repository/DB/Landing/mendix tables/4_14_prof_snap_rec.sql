
------------------------ Profile Scripts ----------------------------

use Landing
go

------------------- snapreceipts$snapreceipt -------------------

select *
from mend.rec_snapreceipt

select id, receiptid, 
	lines, lineqty, 	
	receiptStatus, receiptprocessStatus, processDetail,  priority, 
	status, stockstatus, 
	ordertype, orderclass, 
	suppliername, consignmentID, 
	weight, actualWeight, volume, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed,
	idETLBatchRun, ins_ts
from mend.rec_snapreceipt

select id, receiptid, 
	lines, lineqty, 	
	receiptStatus, receiptprocessStatus, processDetail,  priority, 
	status, stockstatus, 
	ordertype, orderclass, 
	suppliername, consignmentID, 
	weight, actualWeight, volume, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed,
	idETLBatchRun, ins_ts, upd_ts
from mend.rec_snapreceipt_aud

select id, receiptid, 
	lines, lineqty, 	
	receiptStatus, receiptprocessStatus, processDetail,  priority, 
	status, stockstatus, 
	ordertype, orderclass, 
	suppliername, consignmentID, 
	weight, actualWeight, volume, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.rec_snapreceipt_aud_hist

------------ snapreceipts$snapreceipt_receipt ------------

select *
from mend.rec_snapreceipt_receipt

select snapreceiptid, receiptid,
	idETLBatchRun, ins_ts
from mend.rec_snapreceipt_receipt

select snapreceiptid, receiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.rec_snapreceipt_receipt_aud





------------ snapreceipts$snapreceiptline ------------

select *
from mend.rec_snapreceiptline

select id, 
	line, skuid, 
	status, level, unitofmeasure, 
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected,
	idETLBatchRun, ins_ts
from mend.rec_snapreceiptline

select top 1000 id, 
	line, skuid, 
	status, level, unitofmeasure, 
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected,
	idETLBatchRun, ins_ts, upd_ts
from mend.rec_snapreceiptline_aud

select id, 
	line, skuid, 
	status, level, unitofmeasure, 
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.rec_snapreceiptline_aud_hist

------------ snapreceipts$snapreceiptline_snapreceipt ------------

select *
from mend.rec_snapreceiptline_snapreceipt

select snapreceiptlineid, snapreceiptid,
	idETLBatchRun, ins_ts
from mend.rec_snapreceiptline_snapreceipt

select snapreceiptlineid, snapreceiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.rec_snapreceiptline_snapreceipt_aud

------------ snapreceipts$snapreceiptline_stockitem ------------

select *
from mend.rec_snapreceiptline_stockitem

select snapreceiptlineid, stockitemid,
	idETLBatchRun, ins_ts
from mend.rec_snapreceiptline_stockitem

select snapreceiptlineid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.rec_snapreceiptline_stockitem_aud
