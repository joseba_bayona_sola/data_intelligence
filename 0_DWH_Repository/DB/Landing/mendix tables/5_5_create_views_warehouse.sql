use Landing
go 

------------ inventory$warehousestockitem ------------
drop view mend.wh_warehousestockitem_aud_v
go 

create view mend.wh_warehousestockitem_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id,  
		id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		changed,
		createdDate, changedDate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			id_string, 
			availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
			outstandingallocation, onhold,
			stockingmethod, 
			changed, 
			createdDate, changedDate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_warehousestockitem_aud
		union
		select 'H' record_type, id, 
			id_string, 
			availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
			outstandingallocation, onhold,
			stockingmethod, 
			changed, 
			createdDate, changedDate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehousestockitem_aud_hist) t
go

------------ inventory$warehousestockitembatch ------------
drop view mend.wh_warehousestockitembatch_aud_v
go 

create view mend.wh_warehousestockitembatch_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id,
			batch_id,
			fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
			receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
			groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
			productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
			exchangeRate, currency,
			createdDate, changedDate,
			idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,
			batch_id,
			fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
			receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
			groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
			productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
			exchangeRate, currency,
			createdDate, changedDate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_warehousestockitembatch_aud
		union
		select 'H' record_type, id,
			batch_id,
			fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
			receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
			groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
			productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
			exchangeRate, currency,
			createdDate, changedDate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehousestockitembatch_aud_hist) t
go

------------ inventory$batchstockissue ------------
drop view mend.wh_batchstockissue_aud_v
go 

create view mend.wh_batchstockissue_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		issueid, createddate, changeddate, issuedquantity, 
		cancelled, shippeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			issueid, createddate, changeddate, issuedquantity, 
			cancelled, shippeddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_batchstockissue_aud
		union
		select 'H' record_type, id, 
			issueid, createddate, changeddate, issuedquantity, 
			cancelled, shippeddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockissue_aud_hist) t
go

------------ inventory$batchstockallocation ------------
drop view mend.wh_batchstockallocation_aud_v
go 

create view mend.wh_batchstockallocation_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,  
			fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_batchstockallocation_aud
		union
		select 'H' record_type, id, 
			fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockallocation_aud_hist) t
go

------------ inventory$repairstockbatchs ------------
drop view mend.wh_repairstockbatchs_aud_v
go 

create view mend.wh_repairstockbatchs_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		batchtransaction_id, reference, date,
		oldallocation, newallocation, oldissue, newissue, processed,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,  
			batchtransaction_id, reference, date,
			oldallocation, newallocation, oldissue, newissue, processed,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_repairstockbatchs_aud
		union
		select 'H' record_type, id, 
			batchtransaction_id, reference, date,
			oldallocation, newallocation, oldissue, newissue, processed,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_repairstockbatchs_aud_hist) t
go


------------ inventory$stockmovement ------------
drop view mend.wh_stockmovement_aud_v
go

create view mend.wh_stockmovement_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, 
		fromstatus, tostatus, 
		operator, supervisor,
		dateCreated, dateClosed,
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,  
			moveid, movelineid, 
			stockadjustment, movementtype, movetype, _class, reasonid, status, 
			skuid, qtyactioned, 
			fromstate, tostate, 
			fromstatus, tostatus, 
			operator, supervisor,
			dateCreated, dateClosed,
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_stockmovement_aud
		union
		select 'H' record_type, id, 
			moveid, movelineid, 
			stockadjustment, movementtype, movetype, _class, reasonid, status, 
			skuid, qtyactioned, 
			fromstate, tostate, 
			fromstatus, tostatus, 
			operator, supervisor,
			dateCreated, dateClosed,
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_stockmovement_aud_hist) t
go

------------ inventory$batchstockmovement ------------
drop view mend.wh_batchstockmovement_aud_v
go

create view mend.wh_batchstockmovement_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		batchstockmovement_id, batchstockmovementtype, 
		quantity, comment,
		createddate, system$owner,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,  
			batchstockmovement_id, batchstockmovementtype, 
			quantity, comment,
			createddate, changeddate, system$owner,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_batchstockmovement_aud
		union
		select 'H' record_type, id, 
			batchstockmovement_id, batchstockmovementtype, 
			quantity, comment,
			createddate, changeddate, system$owner,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_batchstockmovement_aud_hist) t
go



