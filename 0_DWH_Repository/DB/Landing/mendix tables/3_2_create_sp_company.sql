
------------------------------ SP -----------------------------------

use Landing
go

------------ companyorganisation$company ------------

drop procedure mend.srcmend_lnd_get_comp_company
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_company
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_company
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select id,
			companyregnum, companyname,
			code, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select id,
				companyregnum, companyname,
				code 
			from public."companyorganisation$company"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ companyorganisation$magwebstore ------------

drop procedure mend.srcmend_lnd_get_comp_magwebstore
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_magwebstore
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_magwebstore
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select id,
			storeid, storename, language, country,
			storecurrencycode, storetoorderrate, storetobaserate,
			snapcode, 
			dispensingstatement, 
			system$owner, system$changedby,
			createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select id,
				storeid, storename, language, country,
				storecurrencycode, storetoorderrate, storetobaserate,
				snapcode, 
				dispensingstatement, 
				"system$owner", "system$changedby",
				createddate, changeddate 
			from public."companyorganisation$magwebstore"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ countrymanagement$country ------------

drop procedure mend.srcmend_lnd_get_comp_country
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_country
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_country
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select id,
			countryname, iso_2, iso_3, numcode, 
			currencyformat, 
			taxationarea, standardvatrate, reducedvatrate, 
			selected, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select id,
				countryname, iso_2, iso_3, numcode, 
				currencyformat, 
				taxationarea, standardvatrate, reducedvatrate, 
				selected 
			from public."countrymanagement$country"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ companyorganisation$currency ------------

drop procedure mend.srcmend_lnd_get_comp_currency
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_currency
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_currency
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select id,
			currencycode, currencyname, standardrate, spotrate, 
			laststandardupdate, lastspotupdate, 
			createddate, changeddate, system$owner, system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select id,
				currencycode, currencyname, standardrate, spotrate, 
				laststandardupdate, lastspotupdate, 
				createddate, changeddate, "system$owner", "system$changedby"
			from public."countrymanagement$currency"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ companyorganisation$country_currency ------------

drop procedure mend.srcmend_lnd_get_comp_country_currency
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_country_currency
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_country_currency
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select countrymanagement$countryid countryid, countrymanagement$currencyid currencyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "countrymanagement$countryid", "countrymanagement$currencyid"
			from public."countrymanagement$country_currency"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ companyorganisation$spotrate ------------

drop procedure mend.srcmend_lnd_get_comp_spotrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_spotrate
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_spotrate
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select id,
			value, datasource, 
			createddate, effectivedate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select id, 
				value, datasource, 
				createddate, effectivedate
			from public."countrymanagement$spotrate"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ companyorganisation$currentspotrate_currency ------------

drop procedure mend.srcmend_lnd_get_comp_currentspotrate_currency
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_currentspotrate_currency
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_currentspotrate_currency
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select countrymanagement$spotrateid spotrateid, countrymanagement$currencyid currencyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "countrymanagement$spotrateid", "countrymanagement$currencyid"
			from public."countrymanagement$currentspotrate_currency"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ companyorganisation$historicalspotrate_currency ------------

drop procedure mend.srcmend_lnd_get_comp_historicalspotrate_currency
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - comp_historicalspotrate_currency
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_comp_historicalspotrate_currency
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select countrymanagement$spotrateid spotrateid, countrymanagement$currencyid currencyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "countrymanagement$spotrateid", "countrymanagement$currencyid"
			from public."countrymanagement$historicalspotrate_currency"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

