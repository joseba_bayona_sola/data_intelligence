use Landing
go 

------------------------------------------------------------------------
-------------------------- vat$commodity -------------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_commodity_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_commodity_aud

	select idETLBatchRun, count(*) num_rows
	from mend.vat_commodity_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_commodity_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			commoditycode, commodityname, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_commodity_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
--------------------  product$productfamily_commodity ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_productfamily_commodity_aud
	group by idETLBatchRun
	order by idETLBatchRun



------------------------------------------------------------------------
------------------- vat$dispensingservicesrate -------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_dispensingservicesrate_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_dispensingservicesrate_aud

	select idETLBatchRun, count(*) num_rows
	from mend.vat_dispensingservicesrate_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_dispensingservicesrate_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			dispensingservicesrate, rateeffectivedate, rateenddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_dispensingservicesrate_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
--------------------  vat$dispensingservices_commodity -----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_dispensingservices_commodity_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
--------------------  vat$dispensingservicesrate_country ---------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_dispensingservicesrate_country_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------------
-------------------------- vat$vatrating -------------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatrating_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatrating_aud

	select idETLBatchRun, count(*) num_rows
	from mend.vat_vatrating_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatrating_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			vatratingcode, vatratingdescription, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatrating_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
-------------------------- vat$vatrating_country -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatrating_country_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
-------------------------- vat$vatrate ---------------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatrate_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatrate_aud

	select idETLBatchRun, count(*) num_rows
	from mend.vat_vatrate_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatrate_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			vatrate, vatrateeffectivedate, vatrateenddate, 
			createddate, changeddate, 
			system$owner, system$changedby, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatrate_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
-------------------------- vat$vatrate_vatrating -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatrate_vatrating_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------------
-------------------------- vat$vatschedule -----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatschedule_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatschedule_aud

	select idETLBatchRun, count(*) num_rows
	from mend.vat_vatschedule_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.vat_vatschedule_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatschedule_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
-------------------------- vat$vatschedule_vatrating -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatschedule_vatrating_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
-------------------------- vat$vatschedule_commodity -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.vat_vatschedule_commodity_aud
	group by idETLBatchRun
	order by idETLBatchRun