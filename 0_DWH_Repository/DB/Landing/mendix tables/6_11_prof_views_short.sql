use Landing
go 

----------------------- purchaseorders$shortageheader ----------------------------

select * 
from mend.short_shortageheader_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- purchaseorders$shortage ----------------------------

select * 
from mend.short_shortage_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- purchaseorders$orderlineshortage -------------------

select * 
from mend.short_orderlineshortage_aud_v
where num_records > 1
order by id, idETLBatchRun