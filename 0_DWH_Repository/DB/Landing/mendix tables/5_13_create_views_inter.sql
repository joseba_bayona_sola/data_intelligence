use Landing
go 

------------ intersitetransfer$transferheader ------------

drop view mend.inter_transferheader_aud_v 
go

create view mend.inter_transferheader_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		transfernum, allocatedstrategy, 
		totalremaining,
		createddate, changeddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			transfernum, allocatedstrategy, 
			totalremaining,
			createddate, changeddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.inter_transferheader_aud
		union
		select 'H' record_type, id, 
			transfernum, allocatedstrategy, 
			totalremaining,
			createddate, changeddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_transferheader_aud_hist) t
go




------------------- intersitetransfer$intransitbatchheader -------------------

drop view mend.inter_intransitbatchheader_aud_v
go 

create view mend.inter_intransitbatchheader_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		createddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			createddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.inter_intransitbatchheader_aud
		union
		select 'H' record_type, id, 
			createddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_intransitbatchheader_aud_hist) t
go




------------------- intersitetransfer$intransitstockitembatch -------------------

drop view mend.inter_intransitstockitembatch_aud_v
go

create view mend.inter_intransitstockitembatch_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		issuedquantity, remainingquantity, 
		used,
		productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
		currency,
		groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
		createddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			issuedquantity, remainingquantity, 
			used,
			productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
			currency,
			groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
			createddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.inter_intransitstockitembatch_aud
		union
		select 'H' record_type, id, 
			issuedquantity, remainingquantity, 
			used,
			productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
			currency,
			groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
			createddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_intransitstockitembatch_aud_hist) t
go