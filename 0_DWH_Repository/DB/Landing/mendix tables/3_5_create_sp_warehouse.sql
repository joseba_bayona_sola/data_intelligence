
------------------------------ SP -----------------------------------

use Landing
go

------------ inventory$warehousestockitem ------------

drop procedure mend.srcmend_lnd_get_wh_warehousestockitem
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehousestockitem
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousestockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				id_string, 
				availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
				outstandingallocation, onhold,
				stockingmethod, 
				changed, 
				createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id,
						id_string, 
						availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
						outstandingallocation, onhold,
						stockingmethod, 
						changed, 
						createddate, changeddate
					from public."inventory$warehousestockitem"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')				
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$located_at ------------

drop procedure mend.srcmend_lnd_get_wh_located_at
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_located_at
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_located_at
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitemid warehousestockitemid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select l."inventory$warehousestockitemid", l."inventory$warehouseid" 					
					from 
							public."inventory$located_at" l
						inner join
							public."inventory$warehousestockitem" wsi on l."inventory$warehousestockitemid" = wsi.id
					where wsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or wsi.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')	
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$warehousestockitem_stockitem ------------

drop procedure mend.srcmend_lnd_get_wh_warehousestockitem_stockitem
go

-- ==============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==============================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehousestockitem_stockitem
-- ==============================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousestockitem_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitemid warehousestockitemid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select wsisi."inventory$warehousestockitemid", wsisi."product$stockitemid" 					
					from 
							public."inventory$warehousestockitem_stockitem" wsisi
						inner join
							public."inventory$warehousestockitem" wsi on wsisi."inventory$warehousestockitemid" = wsi.id					
					where wsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or wsi.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')										
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventory$warehousestockitembatch ------------

drop procedure mend.srcmend_lnd_get_wh_warehousestockitembatch
go

-- =========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- =========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehousestockitembatch
-- =========================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousestockitembatch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				batch_id,
				fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
				receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
				groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
				productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
				exchangeRate, currency,
				createdDate, changeddate,' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					batch_id,
					fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
					receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
					groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
					productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
					exchangeRate, currency,
					createdDate, changeddate 					
				from public."inventory$warehousestockitembatch"
				where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')								
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$warehousestockitembatch_warehousestockitem ------------

drop procedure mend.srcmend_lnd_get_wh_warehousestockitembatch_warehousestockitem
go

-- ============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehousestockitembatch_warehousestockitem
-- ============================================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousestockitembatch_warehousestockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitembatchid warehousestockitembatchid, inventory$warehousestockitemid warehousestockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select wsibwsi."inventory$warehousestockitembatchid", wsibwsi."inventory$warehousestockitemid"
				from 
						public."inventory$warehousestockitembatch_warehousestockitem" wsibwsi
					inner join
						public."inventory$warehousestockitem" wsi on wsibwsi."inventory$warehousestockitemid" = wsi.id						
					inner join
						public."inventory$warehousestockitembatch" wsib on wsibwsi."inventory$warehousestockitembatchid" = wsib.id
				where
					(wsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or wsi.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or
					(wsib.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or wsib.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventory$batchstockissue ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockissue
go

-- =================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- =================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockissue
-- =================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockissue
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)
		
	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				issueid, createddate, changeddate, issuedquantity, 
				cancelled, shippeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					issueid, createddate, changeddate, issuedquantity, 
					cancelled, shippeddate
				from public."inventory$batchstockissue"
				where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') or
					changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchstockissue_warehousestockitembatch ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockissue_warehousestockitembatch
go

-- ======================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-02-2018	Joseba Bayona Sola	Add INCR Filtering (Join)
-- ======================================================================================================
-- Description: Reads data from Mendix Table through Open Query - batchstockissue_warehousestockitembatch
-- ======================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockissue_warehousestockitembatch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$batchstockissueid batchstockissueid, inventory$warehousestockitembatchid warehousestockitembatchid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select bsiwsib."inventory$batchstockissueid", bsiwsib."inventory$warehousestockitembatchid"
				from 
						public."inventory$batchstockissue_warehousestockitembatch" bsiwsib
					inner join
						public."inventory$batchstockissue" bsi on bsiwsib."inventory$batchstockissueid" = bsi.id 
				where bsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') or
					bsi.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchstockallocation ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockallocation
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockallocation
-- ======================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockallocation
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
				createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
					createddate, changeddate
				from public."inventory$batchstockallocation"
				where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')				
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchstockissue_batchstockallocation ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockissue_batchstockallocation
go

-- ======================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-02-2018	Joseba Bayona Sola	Add INCR Filtering (Join)
-- ======================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockissue_batchstockallocation
-- ======================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockissue_batchstockallocation
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$batchstockissueid batchstockissueid, inventory$batchstockallocationid batchstockallocationid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select bsibsa."inventory$batchstockissueid", bsibsa."inventory$batchstockallocationid"
				from 
						public."inventory$batchstockissue_batchstockallocation" bsibsa
					inner join
						public."inventory$batchstockissue" bsi on bsibsa."inventory$batchstockissueid" = bsi.id 
					inner join
						public."inventory$batchstockallocation" bsa on bsibsa."inventory$batchstockallocationid" = bsa.id				
				where 
					(bsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') 
						or bsi.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or
					(bsa.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or bsa.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))			
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchtransaction_orderlinestockallocationtransaction ------------

drop procedure mend.srcmend_lnd_get_wh_batchtransaction_orderlinestockallocationtransaction
go

-- ======================================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-02-2018	Joseba Bayona Sola	Add INCR Filtering (Join)
-- ======================================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchtransaction_orderlinestockallocationtransaction
-- ======================================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchtransaction_orderlinestockallocationtransaction
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$batchstockallocationid batchstockallocationid, stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select bsaolsat."inventory$batchstockallocationid" , bsaolsat."stockallocation$orderlinestockallocationtransactionid"
				from 
						public."inventory$batchtransaction_orderlinestockallocationtransaction" bsaolsat
					inner join
						public."inventory$batchstockallocation" bsa on bsaolsat."inventory$batchstockallocationid" = bsa.id				
				where
					(bsa.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or bsa.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))				
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventory$repairstockbatchs ------------

drop procedure mend.srcmend_lnd_get_wh_repairstockbatchs
go

-- ===================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_repairstockbatchs
-- ===================================================================================

create procedure mend.srcmend_lnd_get_wh_repairstockbatchs
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				batchtransaction_id, reference, date,
				oldallocation, newallocation, oldissue, newissue, processed, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					batchtransaction_id, reference, date,
					oldallocation, newallocation, oldissue, newissue, processed
				from public."inventory$repairstockbatchs"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$repairstockbatchs_warehousestockitembatch ------------

drop procedure mend.srcmend_lnd_get_wh_repairstockbatchs_warehousestockitembatch
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 16-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_repairstockbatchs_warehousestockitembatch
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_repairstockbatchs_warehousestockitembatch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$repairstockbatchsid repairstockbatchsid, inventory$warehousestockitembatchid warehousestockitembatchid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$repairstockbatchsid", "inventory$warehousestockitembatchid"
				from public."inventory$repairstockbatchs_warehousestockitembatch"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventory$stockmovement ------------

drop procedure mend.srcmend_lnd_get_wh_stockmovement
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 18-07-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_stockmovement
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_stockmovement
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				moveid, movelineid, 
				stockadjustment, movementtype, movetype, _class, reasonid, status, 
				skuid, qtyactioned, 
				fromstate, tostate, 
				fromstatus, tostatus, 
				operator, supervisor,
				dateCreated, dateClosed, 
				createddate, changeddate,' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					moveid, movelineid, 
					stockadjustment, movementtype, movetype, _class, reasonid, status, 
					skuid, qtyactioned, 
					fromstate, tostate, 
					fromstatus, tostatus, 
					operator, supervisor,
					dateCreated, dateClosed, 
					createddate, changeddate
				from public."inventory$stockmovement"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$stockmovement_warehousestockitem ------------

drop procedure mend.srcmend_lnd_get_wh_stockmovement_warehousestockitem
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 18-07-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_stockmovement_warehousestockitem
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_stockmovement_warehousestockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$stockmovementid stockmovementid, inventory$warehousestockitemid warehousestockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$stockmovementid", "inventory$warehousestockitemid"
				from public."inventory$stockmovement_warehousestockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventory$batchstockmovement ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockmovement
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 18-07-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockmovement
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockmovement
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, batchstockmovement_id, batchstockmovementtype, 
				quantity, comment, 
				createddate, changeddate, system$owner, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, batchstockmovement_id, batchstockmovementtype, 
					quantity, comment, 
					createddate, changeddate, "system$owner"
				from public."inventory$batchstockmovement"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchstockmovement_stockmovement ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockmovement_stockmovement
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 18-07-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockmovement_stockmovement
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockmovement_stockmovement
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$batchstockmovementid batchstockmovementid, inventory$stockmovementid stockmovementid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$batchstockmovementid", "inventory$stockmovementid"
				from public."inventory$batchstockmovement_stockmovement"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ inventory$batchstockmovement_warehousestockitembatch ------------

drop procedure mend.srcmend_lnd_get_wh_batchstockmovement_warehousestockitembatch
go

-- ===========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 18-07-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_batchstockmovement_warehousestockitembatch
-- ===========================================================================================================

create procedure mend.srcmend_lnd_get_wh_batchstockmovement_warehousestockitembatch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$batchstockmovementid batchstockmovementid, inventory$warehousestockitembatchid warehousestockitembatchid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$batchstockmovementid", "inventory$warehousestockitembatchid"
				from public."inventory$batchstockmovement_warehousestockitembatch"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go