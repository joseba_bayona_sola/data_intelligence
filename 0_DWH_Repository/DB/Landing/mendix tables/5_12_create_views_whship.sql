use Landing
go 

------------ warehouseshipments$receipt ------------
drop view mend.whship_receipt_aud_v
go 

create view mend.whship_receipt_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		carriagefix, intercofix,
		auditComment,
		supplieradviceref,
		createdDate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
			status, shipmentType, 
			dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
			totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
			lock,
			carriagefix, intercofix,
			auditComment,
			supplieradviceref,
			createdDate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.whship_receipt_aud
		union
		select 'H' record_type, id, 
			shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
			status, shipmentType, 
			dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
			totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
			lock,
			carriagefix, intercofix,
			auditComment,
			supplieradviceref,
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.whship_receipt_aud_hist) t
go

------------------- warehouseshipments$receiptline -------------------

drop view mend.whship_receiptline_aud_v
go 

create view mend.whship_receiptline_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		line, stockItem,
		status, syncStatus, syncResolution, created, processed,
		quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
		unitCost, 
		auditComment, 
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			line, stockItem,
			status, syncStatus, syncResolution, created, processed,
			quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
			unitCost, 
			auditComment,
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.whship_receiptline_aud
		union
		select 'H' record_type, id, 
			line, stockItem,
			status, syncStatus, syncResolution, created, processed,
			quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
			unitCost, 
			auditComment, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.whship_receiptline_aud_hist) t
go

