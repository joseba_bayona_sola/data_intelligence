use Landing
go 

------------------------------------------------------------------------
----------------------- comp_company ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_company_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_company_aud

	select idETLBatchRun, count(*) num_rows
	from mend.comp_company_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_company_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			companyregnum, companyname,
			code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_company_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- comp_magwebstore ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_magwebstore_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_magwebstore_aud

	select idETLBatchRun, count(*) num_rows
	from mend.comp_magwebstore_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_magwebstore_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			storeid, storename, language, country,
			storecurrencycode, storetoorderrate, storetobaserate,
			snapcode, 
			dispensingstatement, 
			system$owner, system$changedby
			createddate, changeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_magwebstore_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- comp_country ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_country_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_country_aud

	select idETLBatchRun, count(*) num_rows
	from mend.comp_country_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_country_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			countryname, iso_2, iso_3, numcode, 
			currencyformat, 
			taxationarea, standardvatrate, reducedvatrate, 
			selected, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_country_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom



------------------------------------------------------------------------
----------------------- comp_currency ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_currency_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_currency_aud

	select idETLBatchRun, count(*) num_rows
	from mend.comp_currency_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_currency_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			currencycode, currencyname, standardrate, spotrate, 
			laststandardupdate, lastspotupdate, 
			createddate, changeddate, system$owner, system$changedby,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_currency_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom



------------------------------------------------------------------------
----------------------- comp_country_currency ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_country_currency_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
----------------------- comp_spotrate ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_spotrate_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_spotrate_aud

	select idETLBatchRun, count(*) num_rows
	from mend.comp_spotrate_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.comp_spotrate_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			value, datasource, 
			createddate, effectivedate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_spotrate_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


------------------------------------------------------------------------
----------------------- comp_currentspotrate_currency ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_currentspotrate_currency_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
----------------------- comp_historicalspotrate_currency ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.comp_historicalspotrate_currency_aud
	group by idETLBatchRun
	order by idETLBatchRun
