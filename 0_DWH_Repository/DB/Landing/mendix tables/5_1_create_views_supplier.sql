use Landing
go 

drop view mend.supp_supplier_aud_v;
go
drop view mend.supp_supplierprice_aud_v;
go
drop view mend.supp_standardprice_aud_v;
go

------------------- supp_supplier -------------------

create view mend.supp_supplier_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		vatCode, defaultLeadTime,
		email, telephone, fax,
		flatFileConfigurationToUse,
		street1, street2, street3, city, region, postcode,
		vendorCode, vendorShortName, vendorStoreNumber, 
		defaulttransitleadtime,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
			vatCode, defaultLeadTime,
			email, telephone, fax,
			flatFileConfigurationToUse,
			street1, street2, street3, city, region, postcode,
			vendorCode, vendorShortName, vendorStoreNumber, 
			defaulttransitleadtime,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.supp_supplier_aud
		union
		select 'H' record_type, id, 
			supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
			vatCode, defaultLeadTime,
			email, telephone, fax,
			flatFileConfigurationToUse,
			street1, street2, street3, city, region, postcode,
			vendorCode, vendorShortName, vendorStoreNumber, 
			defaulttransitleadtime,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_supplier_aud_hist) t
go

------------------- supp_supplierprice -------------------

create view mend.supp_supplierprice_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		subMetaObjectName,
		unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
		directPrice, expiredPrice,
		totallt, shiplt, transferlt, 
		comments,
		lastUpdated,
		createdDate, changedDate,
		system$owner, system$changedBy, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			subMetaObjectName,
			unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
			directPrice, expiredPrice,
			totallt, shiplt, transferlt, 
			comments,
			lastUpdated,
			createdDate, changedDate,
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.supp_supplierprice_aud
		union
		select 'H' record_type, id, 
			subMetaObjectName,
			unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
			directPrice, expiredPrice,
			totallt, shiplt, transferlt, 
			comments,
			lastUpdated,
			createdDate, changedDate,
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_supplierprice_aud_hist) t
go

------------------- supp_standardprice -------------------

create view mend.supp_standardprice_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id,
		moq, effectiveDate, expiryDate, maxqty,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			moq, effectiveDate, expiryDate, maxqty,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.supp_standardprice_aud
		union
		select 'H' record_type, id, 
			moq, effectiveDate, expiryDate, maxqty,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_standardprice_aud_hist) t
go

--------------------------------------

