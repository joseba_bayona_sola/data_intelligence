use Landing
go 

------------------------------------------------------------------------
----------------------- customershipments$customershipment -------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipment_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_customershipment_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_customershipment_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_customershipment_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			shipmentID, partitionID0_120,
			shipmentNumber, orderIncrementID, 
			status, statusChange, 
			processingFailed, warehouseMethod, labelRenderer, 
			shippingMethod, shippingDescription, paymentMethod, 	
			notify, fullyShipped, syncedToMagento, 	
			shipmentValue, shippingTotal, toFollowValue, 
			hold, intersite, 
			dispatchDate, expectedShippingDate, expectedDeliveryDate,  
			createdDate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_customershipment_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

----------------------------------------------------------------------------------
----------------------- customershipments$customershipment_warehouse -------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipment_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------
----------------------- customershipments$customershipment_order -----------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipment_order_aud
	group by idETLBatchRun
	order by idETLBatchRun



------------------------------------------------------------------------
----------------------- customershipments$lifecycle_update -------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_lifecycle_update_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_lifecycle_update_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_lifecycle_update_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_lifecycle_update_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			status, timestamp,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_lifecycle_update_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

----------------------------------------------------------------------------------
----------------------- customershipments$customershipment_progress --------------
----------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipment_progress_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
----------------------- customershipments$api_updates ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_api_updates_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_api_updates_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_api_updates_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_api_updates_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			service, status, operation, 
			timestamp, 
			detail,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_api_updates_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-------------------------------------------------------------------------------------
----------------------- customershipments$api_updates_customershipment --------------
-------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_api_updates_customershipment_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
----------------------- customershipments$consignment ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_consignment_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_consignment_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_consignment_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_consignment_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			order_number, identifier, 
			warehouse_id, 
			consignment_number, 
			shipping_method, service_id, carrier, service, 
			delivery_instructions, tracking_url, 
			order_value, currency,
			create_date
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_consignment_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-------------------------------------------------------------------------------------
----------------------- customershipments$scurriconsignment_shipment ----------------
-------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_scurriconsignment_shipment_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- customershipments$packagedetail ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_packagedetail_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_packagedetail_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_packagedetail_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_packagedetail_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			tracking_number, description, 
			width, length, height,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_packagedetail_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-------------------------------------------------------------------------------------
----------------------- customershipments$consignmentpackages -----------------------
-------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_consignmentpackages_aud
	group by idETLBatchRun
	order by idETLBatchRun




-------------------------------------------------------------------------------
----------------------- customershipments$customershipmentline ----------------
-------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipmentline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_packagedetail_aud

	select idETLBatchRun, count(*) num_rows
	from mend.ship_customershipmentline_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.ship_customershipmentline_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			shipmentLineID, lineNumber, status, variableBreakdown, 
			stockItemID, productName, productDescription, eye, 
			quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
			packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
			price, priceFormatted, 
			subTotal, subTotal_formatted, 
			VATRate, VAT, VAT_formatted,
			fullyShipped,  
			BOMLine, 
			system$changedBy,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.ship_customershipmentline_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-------------------------------------------------------------------------------------------------------
----------------------- customershipments$customershipmentline_customershipment -----------------------
-------------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipmentline_customershipment_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------------
----------------------- customershipments$customershipmentline_product -----------------------
----------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.ship_customershipmentline_product_aud
	group by idETLBatchRun
	order by idETLBatchRun