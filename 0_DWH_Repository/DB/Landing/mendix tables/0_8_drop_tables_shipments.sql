
------------------------ Drop tables -------------------------------

use Landing
go

------------------- customershipments$customershipment -------------------

drop table mend.ship_customershipment;
go
drop table mend.ship_customershipment_aud;
go
drop table mend.ship_customershipment_aud_hist;
go

------------------- customershipments$customershipment_warehouse -------------------

drop table mend.ship_customershipment_warehouse;
go
drop table mend.ship_customershipment_warehouse_aud;
go

------------------- customershipments$customershipment_order -------------------

drop table mend.ship_customershipment_order;
go
drop table mend.ship_customershipment_order_aud;
go





------------------- customershipments$lifecycle_update -------------------

drop table mend.ship_lifecycle_update;
go
drop table mend.ship_lifecycle_update_aud;
go
drop table mend.ship_lifecycle_update_aud_hist;
go

------------------- customershipments$customershipment_progress -------------------

drop table mend.ship_customershipment_progress;
go
drop table mend.ship_customershipment_progress_aud;
go

------------------- customershipments$api_updates -------------------

drop table mend.ship_api_updates;
go
drop table mend.ship_api_updates_aud;
go
drop table mend.ship_api_updates_aud_hist;
go

------------------- customershipments$api_updates_customershipment -------------------

drop table mend.ship_api_updates_customershipment;
go
drop table mend.ship_api_updates_customershipment_aud;
go





------------------- customershipments$consignment -------------------

drop table mend.ship_consignment;
go
drop table mend.ship_consignment_aud;
go
drop table mend.ship_consignment_aud_hist;
go

------------------- customershipments$scurriconsignment_shipment -------------------

drop table mend.ship_scurriconsignment_shipment;
go
drop table mend.ship_scurriconsignment_shipment_aud;
go

------------------- customershipments$packagedetail -------------------

drop table mend.ship_packagedetail;
go
drop table mend.ship_packagedetail_aud;
go
drop table mend.ship_packagedetail_aud_hist;
go

------------------- customershipments$consignmentpackages -------------------

drop table mend.ship_consignmentpackages;
go
drop table mend.ship_consignmentpackages_aud;
go





------------------- customershipments$customershipmentline -------------------

drop table mend.ship_customershipmentline;
go
drop table mend.ship_customershipmentline_aud;
go
drop table mend.ship_customershipmentline_aud_hist;
go

------------------- customershipments$customershipmentline_customershipment -------------------

drop table mend.ship_customershipmentline_customershipment;
go
drop table mend.ship_customershipmentline_customershipment_aud;
go

------------------- customershipments$customershipmentline_product -------------------

drop table mend.ship_customershipmentline_product;
go
drop table mend.ship_customershipmentline_product_aud;
go