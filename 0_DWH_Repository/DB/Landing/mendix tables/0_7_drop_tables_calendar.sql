
------------------------ Drop tables -------------------------------

use Landing
go

------------ calendar$shippingmethod ------------

drop table mend.cal_shippingmethod;
go
drop table mend.cal_shippingmethod_aud;
go
drop table mend.cal_shippingmethod_aud_hist;
go

------------ calendar$shippingmethods_warehouse ------------

drop table mend.cal_shippingmethods_warehouse;
go
drop table mend.cal_shippingmethods_warehouse_aud;
go

------------ calendar$shippingmethods_destinationcountry ------------

drop table mend.cal_shippingmethods_destinationcountry;
go
drop table mend.cal_shippingmethods_destinationcountry_aud;
go




------------ calendar$defaultcalendar ------------

drop table mend.cal_defaultcalendar;
go
drop table mend.cal_defaultcalendar_aud;
go
drop table mend.cal_defaultcalendar_aud_hist;
go

------------ calendar$defaultcalendar_warehouse ------------

drop table mend.cal_defaultcalendar_warehouse;
go
drop table mend.cal_defaultcalendar_warehouse_aud;
go




------------ calendar$defaultwarehousesuppliercalender ------------

drop table mend.cal_defaultwarehousesuppliercalender;
go
drop table mend.cal_defaultwarehousesuppliercalender_aud;
go
drop table mend.cal_defaultwarehousesuppliercalender_aud_hist;
go

------------ calendar$defaultwarehousesuppliercalender_warehouse ------------

drop table mend.cal_defaultwarehousesuppliercalender_warehouse;
go
drop table mend.cal_defaultwarehousesuppliercalender_warehouse_aud;
go


