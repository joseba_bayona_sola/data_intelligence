
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ companyorganisation$company ------------

select *
from mend.comp_company

select id,
	companyregnum, companyname,
	code, 
	idETLBatchRun, ins_ts
from mend.comp_company

select id,
	companyregnum, companyname,
	code,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_company_aud

select id,
	companyregnum, companyname,
	code,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.comp_company_aud_hist

------------ companyorganisation$magwebstore ------------

select *
from mend.comp_magwebstore

select id,
	storeid, storename, language, country,
	storecurrencycode, storetoorderrate, storetobaserate,
	snapcode, 
	dispensingstatement, 
	system$owner, system$changedby
	createddate, changeddate, 
	idETLBatchRun, ins_ts
from mend.comp_magwebstore

select id,
	storeid, storename, language, country,
	storecurrencycode, storetoorderrate, storetobaserate,
	snapcode, 
	dispensingstatement, 
	system$owner, system$changedby
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_magwebstore_aud

select id,
	storeid, storename, language, country,
	storecurrencycode, storetoorderrate, storetobaserate,
	snapcode, 
	dispensingstatement, 
	system$owner, system$changedby
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.comp_magwebstore_aud_hist



------------ countrymanagement$country ------------

select *
from mend.comp_country

select id,
	countryname, iso_2, iso_3, numcode, 
	currencyformat, 
	taxationarea, standardvatrate, reducedvatrate, 
	selected, 
	idETLBatchRun, ins_ts
from mend.comp_country

select id,
	countryname, iso_2, iso_3, numcode, 
	currencyformat, 
	taxationarea, standardvatrate, reducedvatrate, 
	selected,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_country_aud

select id,
	countryname, iso_2, iso_3, numcode, 
	currencyformat, 
	taxationarea, standardvatrate, reducedvatrate, 
	selected,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.comp_country_aud_hist


------------ countrymanagement$currency ------------

select *
from mend.comp_currency

select id,
	currencycode, currencyname, standardrate, spotrate, 
	laststandardupdate, lastspotupdate, 
	createddate, changeddate, system$owner, system$changedby,
	idETLBatchRun, ins_ts
from mend.comp_currency

select id,
	currencycode, currencyname, standardrate, spotrate, 
	laststandardupdate, lastspotupdate, 
	createddate, changeddate, system$owner, system$changedby,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_currency_aud

select id,
	currencycode, currencyname, standardrate, spotrate, 
	laststandardupdate, lastspotupdate, 
	createddate, changeddate, system$owner, system$changedby,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.comp_currency_aud_hist



------------ countrymanagement$country_currency ------------

select *
from mend.comp_country_currency

select countryid, currencyid,
	idETLBatchRun, ins_ts
from mend.comp_country_currency

select countryid, currencyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_country_currency_aud




------------ countrymanagement$spotrate ------------

select *
from mend.comp_spotrate

select id,
	value, datasource, 
	createddate, effectivedate,
	idETLBatchRun, ins_ts
from mend.comp_spotrate

select id,
	value, datasource, 
	createddate, effectivedate,
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_spotrate_aud

select id,
	value, datasource, 
	createddate, effectivedate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.comp_spotrate_aud_hist


------------ countrymanagement$currentspotrate_currency ------------

select *
from mend.comp_currentspotrate_currency

select spotrateid, currencyid, 
	idETLBatchRun, ins_ts
from mend.comp_currentspotrate_currency

select spotrateid, currencyid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_currentspotrate_currency_aud


------------ countrymanagement$historicalspotrate_currency ------------

select *
from mend.comp_historicalspotrate_currency

select spotrateid, currencyid, 
	idETLBatchRun, ins_ts
from mend.comp_historicalspotrate_currency

select spotrateid, currencyid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.comp_historicalspotrate_currency_aud
