use Landing
go 

------------------- vat$commodity -------------------

drop view mend.vat_commodity_aud_v 
go 

create view mend.vat_commodity_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		commoditycode, commodityname, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			commoditycode, commodityname, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.vat_commodity_aud
		union
		select 'H' record_type, id, 
			commoditycode, commodityname, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_commodity_aud_hist) t
go

------------------- vat$dispensingservicesrate -------------------

drop view mend.vat_dispensingservicesrate_aud_v
go 

create view mend.vat_dispensingservicesrate_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		dispensingservicesrate, rateeffectivedate, rateenddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			dispensingservicesrate, rateeffectivedate, rateenddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.vat_dispensingservicesrate_aud
		union
		select 'H' record_type, id, 
			dispensingservicesrate, rateeffectivedate, rateenddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_dispensingservicesrate_aud_hist) t
go






------------------- vat$vatrating -------------------

drop view mend.vat_vatrating_aud_v 
go

create view mend.vat_vatrating_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		vatratingcode, vatratingdescription, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			vatratingcode, vatratingdescription, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.vat_vatrating_aud
		union
		select 'H' record_type, id, 
			vatratingcode, vatratingdescription, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatrating_aud_hist) t
go

------------------- vat$vatrate -------------------

drop view mend.vat_vatrate_aud_v 
go 

create view mend.vat_vatrate_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		vatrate, vatrateeffectivedate, vatrateenddate, 
		createddate, changeddate, 
		system$owner, system$changedby, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			vatrate, vatrateeffectivedate, vatrateenddate, 
			createddate, changeddate, 
			system$owner, system$changedby, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.vat_vatrate_aud
		union
		select 'H' record_type, id, 
			vatrate, vatrateeffectivedate, vatrateenddate, 
			createddate, changeddate, 
			system$owner, system$changedby, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatrate_aud_hist) t
go





------------------- vat$vatschedule -------------------

drop view mend.vat_vatschedule_aud_v 
go

create view mend.vat_vatschedule_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.vat_vatschedule_aud
		union
		select 'H' record_type, id, 
			vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.vat_vatschedule_aud_hist) t
go