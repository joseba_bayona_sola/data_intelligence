use Landing
go 

------------ stockallocation$orderlinestockallocationtransaction ------------
drop view mend.all_orderlinestockallocationtransaction_aud_v 
go

create view mend.all_orderlinestockallocationtransaction_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime, 
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			transactionID, timestamp,
			allocationType, recordType, 
			allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
			stockAllocated, cancelled, 
			issuedDateTime, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.all_orderlinestockallocationtransaction_aud
		union
		select 'H' record_type, id, 
			transactionID, timestamp,
			allocationType, recordType, 
			allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
			stockAllocated, cancelled, 
			issuedDateTime, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.all_orderlinestockallocationtransaction_aud_hist) t
go

------------ stockallocation$magentoallocation ------------

drop view mend.all_magentoallocation_aud_v 
go 

create view mend.all_magentoallocation_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		shipped, cancelled, 
		quantity, 
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			shipped, cancelled, 
			quantity, 
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.all_magentoallocation_aud
		union
		select 'H' record_type, id, 
			shipped, cancelled, 
			quantity,
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.all_magentoallocation_aud_hist) t
go