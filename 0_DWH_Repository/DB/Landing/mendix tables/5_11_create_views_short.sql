use Landing
go 

------------------- purchaseorders$shortageheader -------------------

drop view mend.short_shortageheader_aud_v
go

create view mend.short_shortageheader_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		wholesale, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			wholesale, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.short_shortageheader_aud
		union
		select 'H' record_type, id, 
			wholesale, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_shortageheader_aud_hist) t
go




------------------- purchaseorders$shortage -------------------

drop view mend.short_shortage_aud_v 
go 

create view mend.short_shortage_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		shortageid, purchaseordernumber, ordernumbers,
		wholesale, 
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate, changedDate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			shortageid, purchaseordernumber, ordernumbers,
			wholesale, 
			status, 
			unitquantity, packquantity, 
			requireddate, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.short_shortage_aud
		union
		select 'H' record_type, id, 
			shortageid, purchaseordernumber, ordernumbers,
			wholesale, 
			status, 
			unitquantity, packquantity, 
			requireddate, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_shortage_aud_hist) t
go





------------------- purchaseorders$orderlineshortage -------------------

drop view mend.short_orderlineshortage_aud_v 
go 

create view mend.short_orderlineshortage_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		orderlineshortageid,
		orderlineshortagetype,
		unitquantity, packquantity,
		shortageprogress, adviseddate, 
		createdDate, changedDate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			orderlineshortageid,
			orderlineshortagetype,
			unitquantity, packquantity,
			shortageprogress, adviseddate, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.short_orderlineshortage_aud
		union
		select 'H' record_type, id, 
			orderlineshortageid,
			orderlineshortagetype,
			unitquantity, packquantity,
			shortageprogress, adviseddate, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_orderlineshortage_aud_hist) t
go