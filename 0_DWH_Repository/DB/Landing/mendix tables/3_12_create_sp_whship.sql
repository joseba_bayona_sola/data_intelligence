
------------------------------ SP -----------------------------------

use Landing
go

------------------- warehouseshipments$receipt -------------------

drop procedure mend.srcmend_lnd_get_whship_receipt
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_receipt
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_whship_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
				status, shipmentType, 
				dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
				totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
				lock,
				carriagefix, intercofix,
				auditComment,
				supplieradviceref,
				createdDate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			 'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
						status, shipmentType, 
						dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
						totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
						lock,
						carriagefix, intercofix,
						auditComment,
						supplieradviceref,
						createdDate, changeddate
					from public."warehouseshipments$receipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

--------------- warehouseshipments$receipt_warehouse ---------------

drop procedure mend.srcmend_lnd_get_whship_receipt_warehouse
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipment_warehouse
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receipt_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptid receiptid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptid", "inventory$warehouseid" 
					from public."warehouseshipments$receipt_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ warehouseshipments$receipt_supplier ------------

drop procedure mend.srcmend_lnd_get_whship_receipt_supplier
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipment_supplier
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receipt_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptid receiptid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptid", "suppliers$supplierid" 
					from public."warehouseshipments$receipt_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- inventory$warehousestockitembatch_shipment -------------------

drop procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receipt
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_warehousestockitembatch_shipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitembatchid warehousestockitembatchid, warehouseshipments$receiptid receiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "inventory$warehousestockitembatchid", "warehouseshipments$receiptid" 
					from public."inventory$warehousestockitembatch_receipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------------- inventory$warehousestockitembatch_shipmenthistory -------------------

drop procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receipthistory
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_warehousestockitembatch_shipmenthistory
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receipthistory
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitembatchid warehousestockitembatchid, warehouseshipments$receiptid receiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "inventory$warehousestockitembatchid", "warehouseshipments$receiptid" 
					from public."inventory$warehousestockitembatch_receipthistory"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------------- inventory$backtobackreceipt_purchaseorder -------------------

drop procedure mend.srcmend_lnd_get_whship_backtobackreceipt_purchaseorder
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_backtobackreceipt_purchaseorder
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_backtobackreceipt_purchaseorder
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptid receiptid, purchaseorders$purchaseorderid purchaseorderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptid", "purchaseorders$purchaseorderid" 
					from public."warehouseshipments$backtobackreceipt_purchaseorder"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------------- inventory$warehousestockitembatch_shipmenthistory -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptforcancelleditems
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_receiptforcancelleditems
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptforcancelleditems
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptid1 receiptid1, warehouseshipments$receiptid2 receiptid2, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptid1", "warehouseshipments$receiptid2"
					from public."warehouseshipments$receiptforcancelleditems"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go











------------------- warehouseshipments$shipmentorderline -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptline
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipmentorderline
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				line, stockItem,
				status, syncStatus, syncResolution, created, processed,
				quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
				unitCost, 
				auditComment, 
				createdDate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						line, stockItem,
						status, syncStatus, syncResolution, created, processed,
						quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
						unitCost, 
						auditComment, 
						createdDate, changeddate
					from public."warehouseshipments$receiptline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- warehouseshipments$shipmentorderline_shipment -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptline_receipt
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipmentorderline_shipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptline_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptlineid receiptlineid, warehouseshipments$receiptid receiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptlineid", "warehouseshipments$receiptid" 
					from public."warehouseshipments$receiptline_receipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- warehouseshipments$shipmentorderline_supplier -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptline_supplier
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipmentorderline_supplier
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptline_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptlineid receiptlineid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptlineid", "suppliers$supplierid" 
					from public."warehouseshipments$receiptline_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- warehouseshipments$shipmentorderline_purchaseorderline -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptline_purchaseorderline
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipmentorderline_purchaseorderline
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptline_purchaseorderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptlineid receiptid, purchaseorders$purchaseorderlineid purchaseorderlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptlineid", "purchaseorders$purchaseorderlineid" 
					from public."warehouseshipments$receiptline_purchaseorderline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- warehouseshipments$shipmentorderline_snapreceiptline -------------------

drop procedure mend.srcmend_lnd_get_whship_receiptline_snapreceiptline
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_shipmentorderline_snapreceiptline
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_receiptline_snapreceiptline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select warehouseshipments$receiptlineid receiptlineid, snapreceipts$snapreceiptlineid snapreceiptlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "warehouseshipments$receiptlineid", "snapreceipts$snapreceiptlineid" 
					from public."warehouseshipments$receiptline_snapreceiptline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- inventory$warehousestockitembatch_shipmentorderline -------------------

drop procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receiptline
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - whship_warehousestockitembatch_shipmentorderline
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_whship_warehousestockitembatch_receiptline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$warehousestockitembatchid warehousestockitembatchid, warehouseshipments$receiptlineid receiptlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "inventory$warehousestockitembatchid", "warehouseshipments$receiptlineid" 
					from public."inventory$warehousestockitembatch_receiptline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go