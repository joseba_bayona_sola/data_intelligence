
---------- Script creacion de tablas ----------

use Landing
go

------------------- warehouseshipments$receipt -------------------

create table mend.whship_receipt (
	id								bigint NOT NULL,
	shipmentid						varchar(11),
	ordernumber						varchar(12),
	receiptid						varchar(16),
	receiptnumber					int,
	invoiceref						varchar(200),
	status							varchar(200),
	shipmenttype					varchar(12),
	duedate							datetime2,
	arriveddate						datetime2,
	confirmeddate					datetime2,
	stockregistereddate				datetime2,
	totalitemprice					decimal(28,8),
	intercompanycarriageprice		decimal(28,8),
	inboundcarriageprice			decimal(28,8),
	intercompanyprofit				decimal(28,8),
	duty							decimal(28,8),
	lock							bit,	
	carriagefix						bit, 
	intercofix						bit,
	auditcomment					varchar(500),
	supplieradviceref				varchar(15),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receipt add constraint [PK_mend_whship_receipt]
	primary key clustered (id);
go
alter table mend.whship_receipt add constraint [DF_mend_whship_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receipt_aud(
	id								bigint NOT NULL,
	shipmentid						varchar(11),
	ordernumber						varchar(12),
	receiptid						varchar(16),
	receiptnumber					int,
	invoiceref						varchar(200),
	status							varchar(200),
	shipmenttype					varchar(12),
	duedate							datetime2,
	arriveddate						datetime2,
	confirmeddate					datetime2,
	stockregistereddate				datetime2,
	totalitemprice					decimal(28,8),
	intercompanycarriageprice		decimal(28,8),
	inboundcarriageprice			decimal(28,8),
	intercompanyprofit				decimal(28,8),
	duty							decimal(28,8),
	lock							bit,	
	carriagefix						bit, 
	intercofix						bit,
	auditcomment					varchar(500),
	supplieradviceref				varchar(15),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receipt_aud add constraint [PK_mend_whship_receipt_aud]
	primary key clustered (id);
go
alter table mend.whship_receipt_aud add constraint [DF_mend_whship_receipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receipt_aud_hist(
	id								bigint NOT NULL,
	shipmentid						varchar(11),
	ordernumber						varchar(12),
	receiptid						varchar(16),
	receiptnumber					int,
	invoiceref						varchar(200),
	status							varchar(200),
	shipmenttype					varchar(12),
	duedate							datetime2,
	arriveddate						datetime2,
	confirmeddate					datetime2,
	stockregistereddate				datetime2,
	totalitemprice					decimal(28,8),
	intercompanycarriageprice		decimal(28,8),
	inboundcarriageprice			decimal(28,8),
	intercompanyprofit				decimal(28,8),
	duty							decimal(28,8),
	lock							bit,	
	carriagefix						bit, 
	intercofix						bit,
	auditcomment					varchar(500),
	supplieradviceref				varchar(15),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.whship_receipt_aud_hist add constraint [DF_mend.whship_receipt_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receipt_warehouse -------------------

create table mend.whship_receipt_warehouse (
	receiptid						bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receipt_warehouse add constraint [PK_mend_whship_receipt_warehouse]
	primary key clustered (receiptid,warehouseid);
go
alter table mend.whship_receipt_warehouse add constraint [DF_mend_whship_receipt_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receipt_warehouse_aud(
	receiptid						bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receipt_warehouse_aud add constraint [PK_mend_whship_receipt_warehouse_aud]
	primary key clustered (receiptid,warehouseid);
go
alter table mend.whship_receipt_warehouse_aud add constraint [DF_mend_whship_receipt_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receipt_supplier -------------------

create table mend.whship_receipt_supplier (
	receiptid						bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receipt_supplier add constraint [PK_mend_whship_receipt_supplier]
	primary key clustered (receiptid,supplierid);
go
alter table mend.whship_receipt_supplier add constraint [DF_mend_whship_receipt_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receipt_supplier_aud(
	receiptid						bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receipt_supplier_aud add constraint [PK_mend_whship_receipt_supplier_aud]
	primary key clustered (receiptid,supplierid);
go
alter table mend.whship_receipt_supplier_aud add constraint [DF_mend_whship_receipt_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- inventory$warehousestockitembatch_receipt -------------------

create table mend.whship_warehousestockitembatch_receipt (
	warehousestockitembatchid		bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_warehousestockitembatch_receipt add constraint [PK_mend_whship_warehousestockitembatch_receipt]
	primary key clustered (warehousestockitembatchid,receiptid);
go
alter table mend.whship_warehousestockitembatch_receipt add constraint [DF_mend_whship_warehousestockitembatch_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_warehousestockitembatch_receipt_aud(
	warehousestockitembatchid		bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_warehousestockitembatch_receipt_aud add constraint [PK_mend_whship_warehousestockitembatch_receipt_aud]
	primary key clustered (warehousestockitembatchid,receiptid);
go
alter table mend.whship_warehousestockitembatch_receipt_aud add constraint [DF_mend_whship_warehousestockitembatch_receipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------------- inventory$warehousestockitembatch_receipthistory -------------------

create table mend.whship_warehousestockitembatch_receipthistory (
	warehousestockitembatchid		bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_warehousestockitembatch_receipthistory add constraint [PK_mend_whship_warehousestockitembatch_receipthistory]
	primary key clustered (warehousestockitembatchid,receiptid);
go
alter table mend.whship_warehousestockitembatch_receipthistory add constraint [DF_mend_whship_warehousestockitembatch_receipthistory_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_warehousestockitembatch_receipthistory_aud(
	warehousestockitembatchid		bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_warehousestockitembatch_receipthistory_aud add constraint [PK_mend_whship_warehousestockitembatch_receipthistory_aud]
	primary key clustered (warehousestockitembatchid,receiptid);
go
alter table mend.whship_warehousestockitembatch_receipthistory_aud add constraint [DF_mend_whship_warehousestockitembatch_receipthistory_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------------- inventory$backtobackreceipt_purchaseorder -------------------

create table mend.whship_backtobackreceipt_purchaseorder (
	receiptid						bigint NOT NULL,
	purchaseorderid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_backtobackreceipt_purchaseorder add constraint [PK_mend_whship_backtobackreceipt_purchaseorder]
	primary key clustered (receiptid, purchaseorderid);
go
alter table mend.whship_backtobackreceipt_purchaseorder add constraint [DF_mend_whship_backtobackreceipt_purchaseorder_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_backtobackreceipt_purchaseorder_aud(
	receiptid						bigint NOT NULL,
	purchaseorderid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_backtobackreceipt_purchaseorder_aud add constraint [PK_mend_whship_backtobackreceipt_purchaseorder_aud]
	primary key clustered (receiptid, purchaseorderid);
go
alter table mend.whship_backtobackreceipt_purchaseorder_aud add constraint [DF_mend_whship_backtobackreceipt_purchaseorder_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------------- inventory$receiptforcancelleditems -------------------

create table mend.whship_receiptforcancelleditems (
	receiptid1						bigint NOT NULL,
	receiptid2						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptforcancelleditems add constraint [PK_mend_whship_receiptforcancelleditems]
	primary key clustered (receiptid1, receiptid2);
go
alter table mend.whship_receiptforcancelleditems add constraint [DF_mend_whship_receiptforcancelleditems_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptforcancelleditems_aud(
	receiptid1						bigint NOT NULL,
	receiptid2						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptforcancelleditems_aud add constraint [PK_mend_whship_receiptforcancelleditems_aud]
	primary key clustered (receiptid1, receiptid2);
go
alter table mend.whship_receiptforcancelleditems_aud add constraint [DF_mend_whship_receiptforcancelleditems_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------------- warehouseshipments$receiptline -------------------

create table mend.whship_receiptline (
	id								bigint NOT NULL,
	line							varchar(200),
	stockitem						varchar(200),
	status							varchar(14),
	syncstatus						varchar(14),
	syncresolution					varchar(14),
	created							datetime2,
	processed						varchar(11),
	quantityordered					decimal(28,8),
	quantityreceived				decimal(28,8),
	quantityaccepted				decimal(28,8),
	quantityrejected				decimal(28,8),
	quantityreturned				decimal(28,8),
	unitcost						decimal(28,8),
	auditcomment					varchar(500),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptline add constraint [PK_mend_whship_receiptline]
	primary key clustered (id);
go
alter table mend.whship_receiptline add constraint [DF_mend_whship_receiptline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptline_aud(
	id								bigint NOT NULL,
	line							varchar(200),
	stockitem						varchar(200),
	status							varchar(14),
	syncstatus						varchar(14),
	syncresolution					varchar(14),
	created							datetime2,
	processed						varchar(11),
	quantityordered					decimal(28,8),
	quantityreceived				decimal(28,8),
	quantityaccepted				decimal(28,8),
	quantityrejected				decimal(28,8),
	quantityreturned				decimal(28,8),
	unitcost						decimal(28,8),
	auditcomment					varchar(500),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptline_aud add constraint [PK_mend_whship_receiptline_aud]
	primary key clustered (id);
go
alter table mend.whship_receiptline_aud add constraint [DF_mend_whship_receiptline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.whship_receiptline_aud_hist(
	id								bigint NOT NULL,
	line							varchar(200),
	stockitem						varchar(200),
	status							varchar(14),
	syncstatus						varchar(14),
	syncresolution					varchar(14),
	created							datetime2,
	processed						varchar(11),
	quantityordered					decimal(28,8),
	quantityreceived				decimal(28,8),
	quantityaccepted				decimal(28,8),
	quantityrejected				decimal(28,8),
	quantityreturned				decimal(28,8),
	unitcost						decimal(28,8),
	auditcomment					varchar(500),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.whship_receiptline_aud_hist add constraint [DF_mend.whship_receiptline_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receiptline_receipt -------------------

create table mend.whship_receiptline_receipt (
	receiptlineid				bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptline_receipt add constraint [PK_mend_whship_receiptline_receipt]
	primary key clustered (receiptlineid,receiptid);
go
alter table mend.whship_receiptline_receipt add constraint [DF_mend_whship_receiptline_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptline_receipt_aud(
	receiptlineid				bigint NOT NULL,
	receiptid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptline_receipt_aud add constraint [PK_mend_whship_receiptline_receipt_aud]
	primary key clustered (receiptlineid,receiptid);
go
alter table mend.whship_receiptline_receipt_aud add constraint [DF_mend_whship_receiptline_receipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receiptline_supplier -------------------

create table mend.whship_receiptline_supplier (
	receiptlineid				bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptline_supplier add constraint [PK_mend_whship_receiptline_supplier]
	primary key clustered (receiptlineid,supplierid);
go
alter table mend.whship_receiptline_supplier add constraint [DF_mend_whship_receiptline_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptline_supplier_aud(
	receiptlineid				bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptline_supplier_aud add constraint [PK_mend_whship_receiptline_supplier_aud]
	primary key clustered (receiptlineid,supplierid);
go
alter table mend.whship_receiptline_supplier_aud add constraint [DF_mend_whship_receiptline_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receiptline_purchaseorderline -------------------

create table mend.whship_receiptline_purchaseorderline (
	receiptlineid				bigint NOT NULL,
	purchaseorderlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptline_purchaseorderline add constraint [PK_mend_whship_receiptline_purchaseorderline]
	primary key clustered (receiptlineid,purchaseorderlineid);
go
alter table mend.whship_receiptline_purchaseorderline add constraint [DF_mend_whship_receiptline_purchaseorderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptline_purchaseorderline_aud(
	receiptlineid				bigint NOT NULL,
	purchaseorderlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptline_purchaseorderline_aud add constraint [PK_mend_whship_receiptline_purchaseorderline_aud]
	primary key clustered (receiptlineid,purchaseorderlineid);
go
alter table mend.whship_receiptline_purchaseorderline_aud add constraint [DF_mend_whship_receiptline_purchaseorderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- warehouseshipments$receiptline_snapreceiptline -------------------

create table mend.whship_receiptline_snapreceiptline (
	receiptlineid				bigint NOT NULL,
	snapreceiptlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_receiptline_snapreceiptline add constraint [PK_mend_whship_receiptline_snapreceiptline]
	primary key clustered (receiptlineid,snapreceiptlineid);
go
alter table mend.whship_receiptline_snapreceiptline add constraint [DF_mend_whship_receiptline_snapreceiptline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_receiptline_snapreceiptline_aud(
	receiptlineid				bigint NOT NULL,
	snapreceiptlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_receiptline_snapreceiptline_aud add constraint [PK_mend_whship_receiptline_snapreceiptline_aud]
	primary key clustered (receiptlineid,snapreceiptlineid);
go
alter table mend.whship_receiptline_snapreceiptline_aud add constraint [DF_mend_whship_receiptline_snapreceiptline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- inventory$warehousestockitembatch_receiptline -------------------

create table mend.whship_warehousestockitembatch_receiptline (
	warehousestockitembatchid		bigint NOT NULL,
	receiptlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.whship_warehousestockitembatch_receiptline add constraint [PK_mend_whship_warehousestockitembatch_receiptline]
	primary key clustered (warehousestockitembatchid,receiptlineid);
go
alter table mend.whship_warehousestockitembatch_receiptline add constraint [DF_mend_whship_warehousestockitembatch_receiptline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.whship_warehousestockitembatch_receiptline_aud(
	warehousestockitembatchid		bigint NOT NULL,
	receiptlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.whship_warehousestockitembatch_receiptline_aud add constraint [PK_mend_whship_warehousestockitembatch_receiptline_aud]
	primary key clustered (warehousestockitembatchid,receiptlineid);
go
alter table mend.whship_warehousestockitembatch_receiptline_aud add constraint [DF_mend_whship_warehousestockitembatch_receiptline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go