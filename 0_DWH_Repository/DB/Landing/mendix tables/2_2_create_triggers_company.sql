
------------------------------- Triggers ----------------------------------

use Landing
go

--------------------------- companyorganisation$company ----------------------------

drop trigger mend.trg_comp_company;
go 

create trigger mend.trg_comp_company on mend.comp_company
after insert
as
begin
	set nocount on

	merge into mend.comp_company_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.companyregnum, ' '), isnull(trg.companyname, ' '), isnull(trg.code, ' ')
		intersect
		select 
			isnull(src.companyregnum, ' '), isnull(src.companyname, ' '), isnull(src.code, ' '))	
		then
			update set
				trg.companyregnum = src.companyregnum, trg.companyname = src.companyname, trg.code = src.code, trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				companyregnum, companyname, code, 
				idETLBatchRun)

				values (src.id,
					src.companyregnum, src.companyname, src.code,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_comp_company_aud;
go 

create trigger mend.trg_comp_company_aud on mend.comp_company_aud
for update, delete
as
begin
	set nocount on

	insert mend.comp_company_aud_hist (id,
		companyregnum, companyname, code,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.companyregnum, d.companyname, d.code, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

--------------------------- companyorganisation$magwebstore ----------------------------

drop trigger mend.trg_comp_magwebstore;
go 

create trigger mend.trg_comp_magwebstore on mend.comp_magwebstore
after insert
as
begin
	set nocount on

	merge into mend.comp_magwebstore_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.storeid, ' '), isnull(trg.storename, ' '), isnull(trg.language, ' '), isnull(trg.country, ' '),
			isnull(trg.storecurrencycode, ' '), isnull(trg.storetoorderrate, 0), isnull(trg.storetobaserate, 0),
			isnull(trg.snapcode, ' '),
			isnull(trg.dispensingstatement, ' '),
			isnull(trg.system$owner, 0), isnull(trg.system$changedby, 0),
			isnull(trg.createddate, ' '), isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.storeid, ' '), isnull(src.storename, ' '), isnull(src.language, ' '), isnull(src.country, ' '),
			isnull(src.storecurrencycode, ' '), isnull(src.storetoorderrate, 0), isnull(src.storetobaserate, 0),
			isnull(src.snapcode, ' '),
			isnull(src.dispensingstatement, ' '),
			isnull(src.system$owner, 0), isnull(src.system$changedby, 0),
			isnull(src.createddate, ' '), isnull(src.changeddate, ' '))	
		then
			update set
				trg.storeid = src.storeid, trg.storename = src.storename, trg.language = src.language, trg.country = src.country, 
				trg.storecurrencycode = src.storecurrencycode, trg.storetoorderrate = src.storetoorderrate, trg.storetobaserate = src.storetobaserate,
				trg.snapcode = src.snapcode,
				trg.dispensingstatement = src.dispensingstatement,
				trg.system$owner = src.system$owner, trg.system$changedby = src.system$changedby,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				storeid, storename, language, country,
				storecurrencycode, storeToOrderRate, storeToBaseRate,
				snapcode, 
				dispensingstatement, 
				system$owner, system$changedby,
				createddate, changeddate, 
				idETLBatchRun)
			values (src.id,
				src.storeid, src.storename, src.language, src.country, 
				src.storecurrencycode, src.storetoorderrate, src.storetobaserate,
				src.snapcode, 
				src.dispensingstatement, 
				src.system$owner, src.system$changedby,
				src.createddate, src.changeddate,
				src.idETLBatchRun);
end; 
go


drop trigger mend.trg_comp_magwebstore_aud;
go 

create trigger mend.trg_comp_magwebstore_aud on mend.comp_magwebstore_aud
for update, delete
as
begin
	set nocount on

	insert mend.comp_magwebstore_aud_hist (id,
		storeid, storename, language, country, 
		storecurrencycode, storeToOrderRate, storeToBaseRate,
		snapcode, 
		dispensingstatement, 
		system$owner, system$changedby,
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.storeid, d.storename, d.language, d.country, 
			d.storecurrencycode, d.storeToOrderRate, d.storeToBaseRate,
			d.snapcode, 
			d.dispensingstatement, 
			d.system$owner, d.system$changedby,
			d.createddate, d.changeddate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

--------------------------- countrymanagement$country ----------------------------

drop trigger mend.trg_comp_country;
go 

create trigger mend.trg_comp_country on mend.comp_country
after insert
as
begin
	set nocount on

	merge into mend.comp_country_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.countryname, ' '), isnull(trg.iso_2, ' '), isnull(trg.iso_3, ' '), isnull(trg.numcode, 0),
			isnull(trg.currencyformat, ' '),
			isnull(trg.taxationarea, ' '), isnull(trg.standardvatrate, 0), isnull(trg.reducedvatrate, 0),
			isnull(trg.selected, ' ')
		intersect
		select 
			isnull(src.countryname, ' '), isnull(src.iso_2, ' '), isnull(src.iso_3, ' '), isnull(src.numcode, 0),
			isnull(src.currencyformat, ' '),
			isnull(src.taxationarea, ' '), isnull(src.standardvatrate, 0), isnull(src.reducedvatrate, 0),
			isnull(src.selected, ' '))	
		then
			update set
				trg.countryname = src.countryname, trg.iso_2 = src.iso_2, trg.iso_3 = src.iso_3, trg.numcode = src.numcode, 
				trg.currencyformat = src.currencyformat,
				trg.taxationarea = src.taxationarea, trg.standardvatrate = src.standardvatrate, trg.reducedvatrate = src.reducedvatrate,
				trg.selected = src.selected,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				countryname, iso_2, iso_3, numcode, 
				currencyformat, 
				taxationarea, standardvatrate, reducedvatrate, 
				selected, 
				idETLBatchRun)
			values (src.id,
				src.countryname, src.iso_2, src.iso_3, src.numcode, 
				src.currencyformat, 
				src.taxationarea, src.standardvatrate, src.reducedvatrate, 
				src.selected,
				src.idETLBatchRun);
end; 
go

drop trigger mend.trg_comp_country_aud;
go 

create trigger mend.trg_comp_country_aud on mend.comp_country_aud
for update, delete
as
begin
	set nocount on

	insert mend.comp_country_aud_hist (id,
		countryname, iso_2, iso_3, numcode, 
		currencyformat, 
		taxationarea, standardvatrate, reducedvatrate, 
		selected,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.countryname, d.iso_2, d.iso_3, d.numcode, 
			d.currencyformat, 
			d.taxationarea, d.standardvatrate, d.reducedvatrate, 
			d.selected, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go




--------------------------- countrymanagement$currency ----------------------------

drop trigger mend.trg_comp_currency;
go 

create trigger mend.trg_comp_currency on mend.comp_currency
after insert
as
begin
	set nocount on

	merge into mend.comp_currency_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.currencycode, ' '), isnull(trg.currencyname, ' '), isnull(trg.standardrate, 0), isnull(trg.spotrate, 0), 
			isnull(trg.laststandardupdate, ' '), isnull(trg.lastspotupdate, ' '), isnull(trg.createddate, ' '), isnull(trg.changeddate, ' '), 
			isnull(trg.system$owner, 0), isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.currencycode, ' '), isnull(src.currencyname, ' '), isnull(src.standardrate, 0), isnull(src.spotrate, 0), 
			isnull(src.laststandardupdate, ' '), isnull(src.lastspotupdate, ' '), isnull(src.createddate, ' '), isnull(src.changeddate, ' '), 
			isnull(src.system$owner, 0), isnull(src.system$changedby, 0))	
		then
			update set
				trg.currencycode = src.currencycode, trg.currencyname = src.currencyname, trg.standardrate = src.standardrate, trg.spotrate = src.spotrate, 
				trg.laststandardupdate = src.laststandardupdate, trg.lastspotupdate = src.lastspotupdate, trg.createddate = src.createddate, trg.changeddate = src.changeddate, 
				trg.system$owner = src.system$owner, trg.system$changedby = src.system$changedby, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				currencycode, currencyname, standardrate, spotrate, 
				laststandardupdate, lastspotupdate, 
				createddate, changeddate, system$owner, system$changedby,
				idETLBatchRun)
			values (src.id,
				src.currencycode, src.currencyname, src.standardrate, src.spotrate, 
				src.laststandardupdate, src.lastspotupdate, 
				src.createddate, src.changeddate, src.system$owner, src.system$changedby,
				src.idETLBatchRun);
end; 
go

drop trigger mend.trg_comp_currency_aud;
go 

create trigger mend.trg_comp_currency_aud on mend.comp_currency_aud
for update, delete
as
begin
	set nocount on

	insert mend.comp_currency_aud_hist (id,
		currencycode, currencyname, standardrate, spotrate, 
		laststandardupdate, lastspotupdate, 
		createddate, changeddate, system$owner, system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.currencycode, d.currencyname, d.standardrate, d.spotrate, 
			d.laststandardupdate, d.lastspotupdate, 
			d.createddate, d.changeddate, d.system$owner, d.system$changedby,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go


--------------------------- countrymanagement$country_currency ----------------------------

drop trigger mend.trg_comp_country_currency;
go 

create trigger mend.trg_comp_country_currency on mend.comp_country_currency
after insert
as
begin
	set nocount on

	merge into mend.comp_country_currency_aud with (tablock) as trg
	using inserted src
		on (trg.countryid = src.countryid and trg.currencyid = src.currencyid)

	when not matched 
		then
			insert (countryid, currencyid,
				idETLBatchRun)
			values (src.countryid, src.currencyid,
				src.idETLBatchRun);
end; 
go





--------------------------- countrymanagement$spotrate ----------------------------

drop trigger mend.trg_comp_spotrate;
go 

create trigger mend.trg_comp_spotrate on mend.comp_spotrate
after insert
as
begin
	set nocount on

	merge into mend.comp_spotrate_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.value, 0), isnull(trg.datasource, ' '), 
			isnull(trg.createddate, ' '), isnull(trg.effectivedate, ' ')
		intersect
		select 
			isnull(src.value, 0), isnull(src.datasource, ' '), 
			isnull(src.createddate, ' '), isnull(src.effectivedate, ' '))	
		then
			update set
				trg.value = src.value, trg.datasource = src.datasource, 
				trg.createddate = src.createddate, trg.effectivedate = src.effectivedate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				value, datasource, 
				createddate, effectivedate,
				idETLBatchRun)
			values (src.id,
				src.value, src.datasource, 
				src.createddate, src.effectivedate,
				src.idETLBatchRun);
end; 
go

drop trigger mend.trg_comp_spotrate_aud;
go 

create trigger mend.trg_comp_spotrate_aud on mend.comp_spotrate_aud
for update, delete
as
begin
	set nocount on

	insert mend.comp_spotrate_aud_hist (id,
		value, datasource, 
		createddate, effectivedate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.value, d.datasource, 
			d.createddate, d.effectivedate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go


--------------------------- countrymanagement$currentspotrate_currency ----------------------------

drop trigger mend.trg_comp_currentspotrate_currency;
go 

create trigger mend.trg_comp_currentspotrate_currency on mend.comp_currentspotrate_currency
after insert
as
begin
	set nocount on

	merge into mend.comp_currentspotrate_currency_aud with (tablock) as trg
	using inserted src
		on (trg.spotrateid = src.spotrateid and trg.currencyid = src.currencyid)

	when not matched 
		then
			insert (spotrateid, currencyid, 
				idETLBatchRun)
			values (src.spotrateid, src.currencyid, 
				src.idETLBatchRun);
end; 
go


--------------------------- countrymanagement$historicalspotrate_currency ----------------------------

drop trigger mend.trg_comp_historicalspotrate_currency;
go 

create trigger mend.trg_comp_historicalspotrate_currency on mend.comp_historicalspotrate_currency
after insert
as
begin
	set nocount on

	merge into mend.comp_historicalspotrate_currency_aud with (tablock) as trg
	using inserted src
		on (trg.spotrateid = src.spotrateid and trg.currencyid = src.currencyid)

	when not matched 
		then
			insert (spotrateid, currencyid, 
				idETLBatchRun)
			values (src.spotrateid, src.currencyid, 
				src.idETLBatchRun);
end; 
go