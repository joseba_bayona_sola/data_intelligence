use Landing
go 

------------------------------------------------------------------------
----------------------- intersitetransfer$transferheader ---------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_transferheader_aud

	select idETLBatchRun, count(*) num_rows
	from mend.inter_transferheader_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_transferheader_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			transfernum, allocatedstrategy, 
			totalremaining,
			createddate, changeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_transferheader_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------- intersitetransfer$supplypo_transferheader ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_supplypo_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
------------- purchaseorders$transferpo_transferheader ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_transferpo_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
------------- orderprocessing$order_transferheader ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_order_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
------------- intersitetransfer$intransitbatchheader -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitbatchheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_intransitbatchheader_aud

	select idETLBatchRun, count(*) num_rows
	from mend.inter_intransitbatchheader_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_intransitbatchheader_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, createddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_intransitbatchheader_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitbatchheader_transferheader ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitbatchheader_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitbatchheader_customershipment ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitbatchheader_customershipment_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------------
------------- intersitetransfer$intransitstockitembatch ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitstockitembatch_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_intransitstockitembatch_aud

	select idETLBatchRun, count(*) num_rows
	from mend.inter_intransitstockitembatch_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.inter_intransitstockitembatch_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			issuedquantity, remainingquantity, 
			used,
			productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
			currency,
			groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
			createddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.inter_intransitstockitembatch_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitstockitembatch_transferheader ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitstockitembatch_transferheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitstockitembatch_intransitbatchheader ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitstockitembatch_intransitbatchheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitstockitembatch_stockitem ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitstockitembatch_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------------------
------------- intersitetransfer$intransitstockitembatch_receipt ----------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.inter_intransitstockitembatch_receipt_aud
	group by idETLBatchRun
	order by idETLBatchRun