
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ purchaseorders$purchaseorder ------------

select *
from mend.purc_purchaseorder

select id, 
	ordernumber, source, status, potype,
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby,
	transferleadtime, transitleadtime, 
	spoleadtime, spoduedate, 
	tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
	requiredshipdate, requiredtransferdate, 
	estimatedpaymentdate, estimateddepositdate,
	supplierreference,
	changeddate,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorder

select id, 
	ordernumber, source, status, potype,
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby,
	transferleadtime, transitleadtime, 
	spoleadtime, spoduedate, 
	tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
	requiredshipdate, requiredtransferdate, 
	estimatedpaymentdate, estimateddepositdate,
	supplierreference,
	changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorder_aud

select id, 
	ordernumber, source, status, potype,
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby,
	transferleadtime, transitleadtime, 
	spoleadtime, spoduedate, 
	tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
	requiredshipdate, requiredtransferdate, 
	estimatedpaymentdate, estimateddepositdate,
	supplierreference,
	changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.purc_purchaseorder_aud_hist

------------------- purchaseorders$purchaseorder_supplier -------------------

select *
from mend.purc_purchaseorder_supplier

select purchaseorderid, supplierid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorder_supplier

select purchaseorderid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorder_supplier_aud

------------------- purchaseorders$purchaseorder_warehouse -------------------

select *
from mend.purc_purchaseorder_warehouse

select purchaseorderid, warehouseid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorder_warehouse

select purchaseorderid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorder_warehouse_aud








------------------- purchaseorders$purchaseorderlineheader -------------------

select *
from mend.purc_purchaseorderlineheader

select id, 
	status, problems, 
	lines, items, totalprice, 
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderlineheader

select id, 
	status, problems, 
	lines, items, totalprice, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderlineheader_aud

select id, 
	status, problems, 
	lines, items, totalprice, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.purc_purchaseorderlineheader_aud_hist

------------------- purchaseorders$purchaseorderlineheader_purchaseorder -------------------

select *
from mend.purc_purchaseorderlineheader_purchaseorder

select purchaseorderlineheaderid, purchaseorderid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderlineheader_purchaseorder

select purchaseorderlineheaderid, purchaseorderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderlineheader_purchaseorder_aud

------------------- purchaseorders$purchaseorderlineheader_productfamily -------------------

select *
from mend.purc_purchaseorderlineheader_productfamily

select purchaseorderlineheaderid, productfamilyid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderlineheader_productfamily

select purchaseorderlineheaderid, productfamilyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderlineheader_productfamily_aud

------------------- purchaseorders$purchaseorderlineheader_packsize -------------------

select *
from mend.purc_purchaseorderlineheader_packsize

select purchaseorderlineheaderid, packsizeid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderlineheader_packsize

select purchaseorderlineheaderid, packsizeid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderlineheader_packsize_aud

------------------- purchaseorders$purchaseorderlineheader_supplierprices -------------------

select *
from mend.purc_purchaseorderlineheader_supplierprices

select purchaseorderlineheaderid, supplierpriceid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderlineheader_supplierprices

select purchaseorderlineheaderid, supplierpriceid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderlineheader_supplierprices_aud








------------------- purchaseorders$purchaseorderline -------------------

select *
from mend.purc_purchaseorderline

select id, 
	po_lineID, stockItemID,
	quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
	lineprice, 
	backtobackduedate,
	createdDate, changedDate,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderline

select top 1000 id, 
	po_lineID, stockItemID,
	quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
	lineprice, 
	backtobackduedate,
	createdDate, changedDate,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderline_aud

select id, 
	po_lineID, stockItemID,
	quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
	lineprice, 
	backtobackduedate,
	createdDate, changedDate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.purc_purchaseorderline_aud_hist

------------------- purchaseorders$purchaseorderline_purchaseorderlineheader -------------------

select *
from mend.purc_purchaseorderline_purchaseorderlineheader

select purchaseorderlineid, purchaseorderlineheaderid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderline_purchaseorderlineheader

select purchaseorderlineid, purchaseorderlineheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderline_purchaseorderlineheader_aud

------------------- purchaseorders$purchaseorderline_stockitem -------------------

select *
from mend.purc_purchaseorderline_stockitem

select purchaseorderlineid, stockitemid,
	idETLBatchRun, ins_ts
from mend.purc_purchaseorderline_stockitem

select purchaseorderlineid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.purc_purchaseorderline_stockitem_aud