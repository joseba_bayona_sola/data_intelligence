
------------------------------ SP -----------------------------------

use Landing
go

------------ product$productcategory ------------

drop procedure mend.srcmend_lnd_get_prod_productcategory
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_productcategory
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_prod_productcategory
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
					code, name, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id,
						code, name 
					from public."product$productcategory"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$productfamily ------------

drop procedure mend.srcmend_lnd_get_prod_productfamily
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_productfamily
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_prod_productfamily
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
				productFamilyType, 
				promotionalProduct, updateSNAPDescription, createToOrder, 
				createdDate, changedDate, 
				system$owner, system$changedBy, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
					productFamilyType, 
					promotionalProduct, updateSNAPDescription, createToOrder, 
					createdDate, changedDate, 
					"system$owner", "system$changedby" 
				from public."product$productfamily"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$productfamily_productcategory ------------

drop procedure mend.srcmend_lnd_get_prod_productfamily_productcategory
go

-- =================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =================================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_productfamily_productcategory
-- =================================================================================================

create procedure mend.srcmend_lnd_get_prod_productfamily_productcategory
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$productfamilyid productfamilyid, product$productcategoryid productcategoryid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select "product$productfamilyid", "product$productcategoryid"
					from public."product$productfamily_productcategory"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------ product$product ------------

drop procedure mend.srcmend_lnd_get_prod_product
go

-- ===========================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_product
-- ===========================================================================

create procedure mend.srcmend_lnd_get_prod_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				productType, valid, 
				SKU, oldSKU, description, 
				isBOM, displaySubProductOnPackingSlip,
				createdDate, changedDate, 
				system$owner, system$changedBy, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id,
						productType, valid, 
						SKU, oldSKU, description, 
						isBOM, displaySubProductOnPackingSlip,
						createdDate, changedDate, 
						"system$owner", "system$changedby"
					from public."product$product"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$product_productfamily ------------

drop procedure mend.srcmend_lnd_get_prod_product_productfamily
go

-- =========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =========================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_product_productfamily
-- =========================================================================================

create procedure mend.srcmend_lnd_get_prod_product_productfamily
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$productid productid, product$productfamilyid productfamilyid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select "product$productid", "product$productfamilyid"
					from public."product$product_productfamily"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------ product$stockitem ------------

drop procedure mend.srcmend_lnd_get_prod_stockitem
go

-- =============================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	06-12-2017	Joseba Bayona Sola	Add createddate, changeddate
-- =============================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_stockitem
-- =============================================================================

create procedure mend.srcmend_lnd_get_prod_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				SKU, oldSKU, SNAPDescription, packSize, 
				manufacturerArticleID, manufacturerCodeNumber,
				SNAPUploadStatus, 
				changed, 
				createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id,
						SKU, oldSKU, SNAPDescription, packSize, 
						manufacturerArticleID, manufacturerCodeNumber,
						SNAPUploadStatus, 
						changed, 
						createddate, changeddate
					from public."product$stockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$stockitem_product ------------

drop procedure mend.srcmend_lnd_get_prod_stockitem_product
go

-- =====================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =====================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_stockitem_product
-- =====================================================================================

create procedure mend.srcmend_lnd_get_prod_stockitem_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$stockitemid stockitemid, product$productid productid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "product$stockitemid", "product$productid"
					from public."product$stockitem_product"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$contactlens ------------

drop procedure mend.srcmend_lnd_get_prod_contactlens
go

-- ===============================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===============================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_contactlens
-- ===============================================================================

create procedure mend.srcmend_lnd_get_prod_contactlens
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				bc, di, po, 
				cy, ax,  
				ad, _do, co, co_EDI, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id,
						bc, di, po, 
						cy, ax,  
						ad, _do, co, co_EDI
					from public."product$contactlens"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------ product$packsize ------------

drop procedure mend.srcmend_lnd_get_prod_packsize
go

-- ============================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ============================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_packsize
-- ============================================================================

create procedure mend.srcmend_lnd_get_prod_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				description, size, 
				packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
				height, width, weight, depth, 
				productGroup1, productGroup2, productGroup2Type, 
				breakable, 
				packsperpallet, packspercarton, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id,
						description, size, 
						packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
						height, width, weight, depth, 
						productGroup1, productGroup2, productGroup2Type, 
						breakable, 
						packsperpallet, packspercarton
					from public."product$packsize"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$packsize_productfamily ------------

drop procedure mend.srcmend_lnd_get_prod_packsize_productfamily
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_packsize_productfamily
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_prod_packsize_productfamily
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$packsizeid packsizeid, product$productfamilyid productfamilyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "product$packsizeid", "product$productfamilyid"
					from public."product$packsize_productfamily"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$stockitem_packsize ------------

drop procedure mend.srcmend_lnd_get_prod_stockitem_packsize
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 08-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - prod_stockitem_packsize
-- ======================================================================================

create procedure mend.srcmend_lnd_get_prod_stockitem_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$packsizeid packsizeid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "product$packsizeid", "product$stockitemid"
					from public."product$stockitem_packsize"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go