
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ warehouseshipments$receipt ------------

select *
from mend.whship_receipt

select id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	carriagefix, intercofix,
	auditComment,
	supplieradviceref,
	createdDate, changeddate,
	idETLBatchRun, ins_ts
from mend.whship_receipt

select id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	carriagefix, intercofix,
	auditComment,
	supplieradviceref,
	createdDate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receipt_aud

select id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	carriagefix, intercofix,
	auditComment,
	supplieradviceref,
	createdDate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.whship_receipt_aud_hist

------------ warehouseshipments$receipt_warehouse ------------

select *
from mend.whship_receipt_warehouse

select receiptid, warehouseid,
	idETLBatchRun, ins_ts
from mend.whship_receipt_warehouse

select receiptid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receipt_warehouse_aud

------------ warehouseshipments$receipt_supplier ------------

select *
from mend.whship_receipt_supplier

select receiptid, supplierid,
	idETLBatchRun, ins_ts
from mend.whship_receipt_supplier

select receiptid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receipt_supplier_aud

------------------- inventory$warehousestockitembatch_receipt -------------------

select *
from mend.whship_warehousestockitembatch_receipt

select warehousestockitembatchid, receiptid,
	idETLBatchRun, ins_ts
from mend.whship_warehousestockitembatch_receipt

select warehousestockitembatchid, receiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_warehousestockitembatch_receipt_aud



------------------- inventory$warehousestockitembatch_receipthistory -------------------

select *
from mend.whship_warehousestockitembatch_receipthistory

select warehousestockitembatchid, receiptid,
	idETLBatchRun, ins_ts
from mend.whship_warehousestockitembatch_receipthistory

select warehousestockitembatchid, receiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_warehousestockitembatch_receipthistory_aud


------------------- inventory$backtobackreceipt_purchaseorder -------------------

select *
from mend.whship_backtobackreceipt_purchaseorder

select receiptid, purchaseorderid,
	idETLBatchRun, ins_ts
from mend.whship_backtobackreceipt_purchaseorder

select receiptid, purchaseorderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_backtobackreceipt_purchaseorder_aud


------------------- inventory$receiptforcancelleditems -------------------

select *
from mend.whship_receiptforcancelleditems

select receiptid1, receiptid2, 
	idETLBatchRun, ins_ts
from mend.whship_receiptforcancelleditems

select receiptid1, receiptid2,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptforcancelleditems_aud








------------------- warehouseshipments$receiptline -------------------

select *
from mend.whship_receiptline

select top 1000  id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
	unitCost, 
	auditComment,
	idETLBatchRun, ins_ts
from mend.whship_receiptline

select top 1000 id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
	unitCost, 
	auditComment,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptline_aud

select id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
	unitCost, 
	auditComment,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.whship_receiptline_aud_hist

------------------- warehouseshipments$shipmentorderline_receipt -------------------

select *
from mend.whship_receiptline_receipt

select top 1000 receiptlineid, receiptid,
	idETLBatchRun, ins_ts
from mend.whship_receiptline_receipt

select receiptlineid, receiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptline_receipt_aud

------------------- warehouseshipments$receiptline_supplier -------------------

select *
from mend.whship_receiptline_supplier

select receiptlineid, supplierid,
	idETLBatchRun, ins_ts
from mend.whship_receiptline_supplier

select receiptlineid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptline_supplier_aud

------------------- warehouseshipments$receiptline_purchaseorderline -------------------

select *
from mend.whship_receiptline_purchaseorderline

select receiptlineid, purchaseorderlineid,
	idETLBatchRun, ins_ts
from mend.whship_receiptline_purchaseorderline

select receiptlineid, purchaseorderlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptline_purchaseorderline_aud

------------------- warehouseshipments$receiptline_snapreceiptline -------------------

select *
from mend.whship_receiptline_snapreceiptline

select receiptlineid, snapreceiptlineid,
	idETLBatchRun, ins_ts
from mend.whship_receiptline_snapreceiptline

select receiptlineid, snapreceiptlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_receiptline_snapreceiptline_aud

------------------- inventory$warehousestockitembatch_receiptline -------------------

select count(*)
from mend.whship_warehousestockitembatch_receiptline

select warehousestockitembatchid, receiptlineid,
	idETLBatchRun, ins_ts
from mend.whship_warehousestockitembatch_receiptline

select warehousestockitembatchid, receiptlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.whship_warehousestockitembatch_receiptline_aud