use Landing
go 

------------------------------------------------------------------------
----------------------- calendar$shippingmethod ------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_shippingmethod_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_shippingmethod_aud

	select idETLBatchRun, count(*) num_rows
	from mend.cal_shippingmethod_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_shippingmethod_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
			leadtime, letterRate1, letterRate2, tracked, invoicedocs,
			defaultweight, minweight, scurrimappable,
			mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking
			mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_shippingmethod_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------- calendar$shippingmethods_warehouse -----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_shippingmethods_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
-------------- calendar$shippingmethods_destinationcountry -------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_shippingmethods_destinationcountry_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
----------------------- calendar$defaultcalendar -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_defaultcalendar_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_defaultcalendar_aud

	select idETLBatchRun, count(*) num_rows
	from mend.cal_defaultcalendar_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_defaultcalendar_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_defaultcalendar_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
-------------- calendar$defaultcalendar_warehouse ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_defaultcalendar_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
-------------- calendar$defaultwarehousesuppliercalender ---------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_defaultwarehousesuppliercalender_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_defaultwarehousesuppliercalender_aud

	select idETLBatchRun, count(*) num_rows
	from mend.cal_defaultwarehousesuppliercalender_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.cal_defaultwarehousesuppliercalender_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
			mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
			mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_defaultwarehousesuppliercalender_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

---------------------------------------------------------------------------------------------
-------------- calendar$cal_defaultwarehousesuppliercalender_warehouse ----------------------
---------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.cal_defaultwarehousesuppliercalender_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun