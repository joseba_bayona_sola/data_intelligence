use Landing
go 

----------------------- product$productcategory ----------------------------

select * 
from mend.prod_productcategory_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$productfamily ----------------------------

select * 
from mend.prod_productfamily_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$productfamily_productcategory ----------------------------

select * 
from mend.prod_productfamily_productcategory_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$product ----------------------------

select * 
from mend.prod_product_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$product_productfamily ----------------------------

select * 
from mend.prod_product_productfamily_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$stockitem ----------------------------

select * 
from mend.prod_stockitem_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$stockitem_product ----------------------------

select * 
from mend.prod_stockitem_product_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$contactlens ----------------------------

select * 
from mend.prod_contactlens_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$packsize ----------------------------

select * 
from mend.prod_packsize_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$packsize_productfamily ----------------------------

select * 
from mend.prod_packsize_productfamily_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- product$stockitem_packsize ----------------------------

select * 
from mend.prod_stockitem_packsize_aud_v
where num_records > 1
order by id, idETLBatchRun

