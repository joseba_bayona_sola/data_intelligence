
---------- Script creacion de tablas ----------

use Landing
go

------------------- calendar$shippingmethod -------------------

create table mend.cal_shippingmethod (
	id								bigint NOT NULL, 
	magentoShippingDescription		varchar(200),
	magentoShippingMethod			varchar(200),
	scurriShippingMethod			varchar(200),
	snapShippingMethod				varchar(20), 
	leadtime						int,
	letterRate1						bit,
	letterRate2						bit,
	tracked							bit,
	invoicedocs						bit,
	defaultweight					decimal(28, 8),
	minweight						decimal(28, 8),
	scurrimappable					bit, 
	mondayDefaultCutOff				datetime2,
	tuesdayDefaultCutOff			datetime2,
	wednesdayDefaultCutOff			datetime2,
	thursdayDefaultCutOff			datetime2,
	fridayDefaultCutOff				datetime2,
	saturdayDefaultCutOff			datetime2,
	sundayDefaultCutOff				datetime2, 
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayDeliveryWorking			bit,
	tuesdayDeliveryWorking			bit,
	wednesdayDeliveryWorking		bit,
	thursdayDeliveryWorking			bit,
	fridayDeliveryWorking			bit,
	saturdayDeliveryWorking			bit,
	sundayDeliveryWorking			bit,
	mondayCollectionTime			datetime2,
	tuesdayCollectionTime			datetime2,
	wednesdayCollectionTime			datetime2,
	thursdayCollectionTime			datetime2,
	fridayCollectionTime			datetime2,
	saturdayCollectionTime			datetime2,
	sundayCollectionTime			datetime2,								
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_shippingmethod add constraint [PK_mend_cal_shippingmethod]
	primary key clustered (id);
go
alter table mend.cal_shippingmethod add constraint [DF_mend_cal_shippingmethod_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_shippingmethod_aud(
	id								bigint NOT NULL, 
	magentoShippingDescription		varchar(200),
	magentoShippingMethod			varchar(200),
	scurriShippingMethod			varchar(200),
	snapShippingMethod				varchar(20), 
	leadtime						int,
	letterRate1						bit,
	letterRate2						bit,
	tracked							bit,
	invoicedocs						bit,
	defaultweight					decimal(28, 8),
	minweight						decimal(28, 8),
	scurrimappable					bit, 
	mondayDefaultCutOff				datetime2,
	tuesdayDefaultCutOff			datetime2,
	wednesdayDefaultCutOff			datetime2,
	thursdayDefaultCutOff			datetime2,
	fridayDefaultCutOff				datetime2,
	saturdayDefaultCutOff			datetime2,
	sundayDefaultCutOff				datetime2, 
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayDeliveryWorking			bit,
	tuesdayDeliveryWorking			bit,
	wednesdayDeliveryWorking		bit,
	thursdayDeliveryWorking			bit,
	fridayDeliveryWorking			bit,
	saturdayDeliveryWorking			bit,
	sundayDeliveryWorking			bit,
	mondayCollectionTime			datetime2,
	tuesdayCollectionTime			datetime2,
	wednesdayCollectionTime			datetime2,
	thursdayCollectionTime			datetime2,
	fridayCollectionTime			datetime2,
	saturdayCollectionTime			datetime2,
	sundayCollectionTime			datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_shippingmethod_aud add constraint [PK_mend_cal_shippingmethod_aud]
	primary key clustered (id);
go
alter table mend.cal_shippingmethod_aud add constraint [DF_mend_cal_shippingmethod_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_shippingmethod_aud_hist(
	id								bigint NOT NULL, 
	magentoShippingDescription		varchar(200),
	magentoShippingMethod			varchar(200),
	scurriShippingMethod			varchar(200),
	snapShippingMethod				varchar(20), 
	leadtime						int,
	letterRate1						bit,
	letterRate2						bit,
	tracked							bit,
	invoicedocs						bit,
	defaultweight					decimal(28, 8),
	minweight						decimal(28, 8),
	scurrimappable					bit, 
	mondayDefaultCutOff				datetime2,
	tuesdayDefaultCutOff			datetime2,
	wednesdayDefaultCutOff			datetime2,
	thursdayDefaultCutOff			datetime2,
	fridayDefaultCutOff				datetime2,
	saturdayDefaultCutOff			datetime2,
	sundayDefaultCutOff				datetime2, 
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayDeliveryWorking			bit,
	tuesdayDeliveryWorking			bit,
	wednesdayDeliveryWorking		bit,
	thursdayDeliveryWorking			bit,
	fridayDeliveryWorking			bit,
	saturdayDeliveryWorking			bit,
	sundayDeliveryWorking			bit,
	mondayCollectionTime			datetime2,
	tuesdayCollectionTime			datetime2,
	wednesdayCollectionTime			datetime2,
	thursdayCollectionTime			datetime2,
	fridayCollectionTime			datetime2,
	saturdayCollectionTime			datetime2,
	sundayCollectionTime			datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.cal_shippingmethod_aud_hist add constraint [DF_mend.cal_shippingmethod_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- calendar$shippingmethods_warehouse -------------------

create table mend.cal_shippingmethods_warehouse (
	shippingmethodid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,  						
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_shippingmethods_warehouse add constraint [PK_mend_cal_shippingmethods_warehouse]
	primary key clustered (shippingmethodid,warehouseid);
go
alter table mend.cal_shippingmethods_warehouse add constraint [DF_mend_cal_shippingmethods_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_shippingmethods_warehouse_aud(
	shippingmethodid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_shippingmethods_warehouse_aud add constraint [PK_mend_cal_shippingmethods_warehouse_aud]
	primary key clustered (shippingmethodid,warehouseid);
go
alter table mend.cal_shippingmethods_warehouse_aud add constraint [DF_mend_cal_shippingmethods_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- calendar$shippingmethods_destinationcountry -------------------

create table mend.cal_shippingmethods_destinationcountry (
	shippingmethodid				bigint NOT NULL,
	countryid						bigint NOT NULL,  						
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_shippingmethods_destinationcountry add constraint [PK_mend_cal_shippingmethods_destinationcountry]
	primary key clustered (shippingmethodid,countryid);
go
alter table mend.cal_shippingmethods_destinationcountry add constraint [DF_mend_cal_shippingmethods_destinationcountry_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_shippingmethods_destinationcountry_aud(
	shippingmethodid				bigint NOT NULL,
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_shippingmethods_destinationcountry_aud add constraint [PK_mend_cal_shippingmethods_destinationcountry_aud]
	primary key clustered (shippingmethodid,countryid);
go
alter table mend.cal_shippingmethods_destinationcountry_aud add constraint [DF_mend_cal_shippingmethods_destinationcountry_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------------- calendar$defaultcalendar -------------------

create table mend.cal_defaultcalendar (
	id								bigint NOT NULL,
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayCutOff					datetime2,
	tuesdayCutOff					datetime2,
	wednesdayCutOff					datetime2,
	thursdayCutOff					datetime2,
	fridayCutOff					datetime2,
	saturdayCutOff					datetime2,
	sundayCutOff					datetime2, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,								
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_defaultcalendar add constraint [PK_mend_cal_defaultcalendar]
	primary key clustered (id);
go
alter table mend.cal_defaultcalendar add constraint [DF_mend_cal_defaultcalendar_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultcalendar_aud(
	id								bigint NOT NULL,
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayCutOff					datetime2,
	tuesdayCutOff					datetime2,
	wednesdayCutOff					datetime2,
	thursdayCutOff					datetime2,
	fridayCutOff					datetime2,
	saturdayCutOff					datetime2,
	sundayCutOff					datetime2, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_defaultcalendar_aud add constraint [PK_mend_cal_defaultcalendar_aud]
	primary key clustered (id);
go
alter table mend.cal_defaultcalendar_aud add constraint [DF_mend_cal_defaultcalendar_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultcalendar_aud_hist(
	id								bigint NOT NULL,
	mondayWorking					bit,
	tuesdayWorking					bit,
	wednesdayWorking				bit,
	thursdayWorking					bit,
	fridayWorking					bit,
	saturdayWorking					bit,
	sundayWorking					bit, 
	mondayCutOff					datetime2,
	tuesdayCutOff					datetime2,
	wednesdayCutOff					datetime2,
	thursdayCutOff					datetime2,
	fridayCutOff					datetime2,
	saturdayCutOff					datetime2,
	sundayCutOff					datetime2, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.cal_defaultcalendar_aud_hist add constraint [DF_mend.cal_defaultcalendar_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- calendar$defaultcalendar_warehouse -------------------

create table mend.cal_defaultcalendar_warehouse (
	defaultcalendarid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,  								
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_defaultcalendar_warehouse add constraint [PK_mend_cal_defaultcalendar_warehouse]
	primary key clustered (defaultcalendarid,warehouseid);
go
alter table mend.cal_defaultcalendar_warehouse add constraint [DF_mend_cal_defaultcalendar_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultcalendar_warehouse_aud(
	defaultcalendarid				bigint NOT NULL,
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_defaultcalendar_warehouse_aud add constraint [PK_mend_cal_defaultcalendar_warehouse_aud]
	primary key clustered (defaultcalendarid,warehouseid);
go
alter table mend.cal_defaultcalendar_warehouse_aud add constraint [DF_mend_cal_defaultcalendar_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go





------------------- calendar$defaultwarehousesuppliercalender -------------------

create table mend.cal_defaultwarehousesuppliercalender (
	id								bigint NOT NULL, 
	mondayCanSupply					bit,
	tuesdayCanSupply				bit,
	wednesdayCanSupply				bit,
	thursdayCanSupply				bit,
	fridayCanSupply					bit,
	saturdayCanSupply				bit,
	sundayCanSupply					bit, 
	mondayCutOffTime				datetime2,
	tuesdayCutOffTime				datetime2,
	wednesdayCutOffTime				datetime2,
	thursdayCutOffTime				datetime2,
	fridayCutOffTime				datetime2,
	saturdayCutOffTime				datetime2,
	sundayCutOffTime				datetime2, 
	mondayCanOrder					bit,
	tuesdayCanOrder					bit,
	wednesdayCanOrder				bit,
	thursdayCanOrder				bit,
	fridayCanOrder					bit,
	saturdayCanOrder				bit,
	sundayCanOrder					bit, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,								
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.cal_defaultwarehousesuppliercalender add constraint [PK_mend_cal_defaultwarehousesuppliercalender]
	primary key clustered (id);
go
alter table mend.cal_defaultwarehousesuppliercalender add constraint [DF_mend_cal_defaultwarehousesuppliercalender_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultwarehousesuppliercalender_aud(
	id								bigint NOT NULL, 
	mondayCanSupply					bit,
	tuesdayCanSupply				bit,
	wednesdayCanSupply				bit,
	thursdayCanSupply				bit,
	fridayCanSupply					bit,
	saturdayCanSupply				bit,
	sundayCanSupply					bit, 
	mondayCutOffTime				datetime2,
	tuesdayCutOffTime				datetime2,
	wednesdayCutOffTime				datetime2,
	thursdayCutOffTime				datetime2,
	fridayCutOffTime				datetime2,
	saturdayCutOffTime				datetime2,
	sundayCutOffTime				datetime2, 
	mondayCanOrder					bit,
	tuesdayCanOrder					bit,
	wednesdayCanOrder				bit,
	thursdayCanOrder				bit,
	fridayCanOrder					bit,
	saturdayCanOrder				bit,
	sundayCanOrder					bit, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.cal_defaultwarehousesuppliercalender_aud add constraint [PK_mend_cal_defaultwarehousesuppliercalender_aud]
	primary key clustered (id);
go
alter table mend.cal_defaultwarehousesuppliercalender_aud add constraint [DF_mend_cal_defaultwarehousesuppliercalender_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultwarehousesuppliercalender_aud_hist(
	id								bigint NOT NULL, 
	mondayCanSupply					bit,
	tuesdayCanSupply				bit,
	wednesdayCanSupply				bit,
	thursdayCanSupply				bit,
	fridayCanSupply					bit,
	saturdayCanSupply				bit,
	sundayCanSupply					bit, 
	mondayCutOffTime				datetime2,
	tuesdayCutOffTime				datetime2,
	wednesdayCutOffTime				datetime2,
	thursdayCutOffTime				datetime2,
	fridayCutOffTime				datetime2,
	saturdayCutOffTime				datetime2,
	sundayCutOffTime				datetime2, 
	mondayCanOrder					bit,
	tuesdayCanOrder					bit,
	wednesdayCanOrder				bit,
	thursdayCanOrder				bit,
	fridayCanOrder					bit,
	saturdayCanOrder				bit,
	sundayCanOrder					bit, 
	timeHorizonWeeks				int,
	createdDate						datetime2,
	changedDate						datetime2, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.cal_defaultwarehousesuppliercalender_aud_hist add constraint [DF_mend.cal_defaultwarehousesuppliercalender_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- calendar$defaultwarehousesuppliercalender_warehouse -------------------

create table mend.cal_defaultwarehousesuppliercalender_warehouse (
	defaultwarehousesuppliercalenderid	bigint NOT NULL,
	warehouseid							bigint NOT NULL,
	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go

alter table mend.cal_defaultwarehousesuppliercalender_warehouse add constraint [PK_mend_cal_defaultwarehousesuppliercalender_warehouse]
	primary key clustered (defaultwarehousesuppliercalenderid,warehouseid);
go
alter table mend.cal_defaultwarehousesuppliercalender_warehouse add constraint [DF_mend_cal_defaultwarehousesuppliercalender_warehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.cal_defaultwarehousesuppliercalender_warehouse_aud(
	defaultwarehousesuppliercalenderid	bigint NOT NULL,
	warehouseid							bigint NOT NULL,
	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL,
	upd_ts								datetime);
go 

alter table mend.cal_defaultwarehousesuppliercalender_warehouse_aud add constraint [PK_mend_cal_defaultwarehousesuppliercalender_warehouse_aud]
	primary key clustered (defaultwarehousesuppliercalenderid,warehouseid);
go
alter table mend.cal_defaultwarehousesuppliercalender_warehouse_aud add constraint [DF_mend_cal_defaultwarehousesuppliercalender_warehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go