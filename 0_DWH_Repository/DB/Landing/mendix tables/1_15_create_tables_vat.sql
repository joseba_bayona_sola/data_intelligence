
---------- Script creacion de tablas ----------

use Landing
go

------------------- vat$commodity -------------------

create table mend.vat_commodity (
	id								bigint NOT NULL, 
	commoditycode					int, 
	commodityname					varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_commodity add constraint [PK_mend_vat_commodity]
	primary key clustered (id);
go
alter table mend.vat_commodity add constraint [DF_mend_vat_commodity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_commodity_aud(
	id								bigint NOT NULL, 
	commoditycode					int, 
	commodityname					varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_commodity_aud add constraint [PK_mend_vat_commodity_aud]
	primary key clustered (id);
go
alter table mend.vat_commodity_aud add constraint [DF_mend_vat_commodity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_commodity_aud_hist(
	id								bigint NOT NULL, 
	commoditycode					int, 
	commodityname					varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.vat_commodity_aud_hist add constraint [DF_mend.vat_commodity_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- product$productfamily_commodity -------------------

create table mend.vat_productfamily_commodity (
	productfamilyid					bigint NOT NULL, 
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_productfamily_commodity add constraint [PK_mend_vat_productfamily_commodity]
	primary key clustered (productfamilyid,commodityid);
go
alter table mend.vat_productfamily_commodity add constraint [DF_mend_vat_productfamily_commodity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_productfamily_commodity_aud(
	productfamilyid					bigint NOT NULL, 
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_productfamily_commodity_aud add constraint [PK_mend_vat_productfamily_commodity_aud]
	primary key clustered (productfamilyid,commodityid);
go
alter table mend.vat_productfamily_commodity_aud add constraint [DF_mend_vat_productfamily_commodity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------------- vat$dispensingservicesrate -------------------

create table mend.vat_dispensingservicesrate (
	id								bigint NOT NULL, 
	dispensingservicesrate			decimal(28,8), 
	rateeffectivedate				datetime2, 
	rateenddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_dispensingservicesrate add constraint [PK_mend_vat_dispensingservicesrate]
	primary key clustered (id);
go
alter table mend.vat_dispensingservicesrate add constraint [DF_mend_vat_dispensingservicesrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_dispensingservicesrate_aud(
	id								bigint NOT NULL, 
	dispensingservicesrate			decimal(28,8), 
	rateeffectivedate				datetime2, 
	rateenddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_dispensingservicesrate_aud add constraint [PK_mend_vat_dispensingservicesrate_aud]
	primary key clustered (id);
go
alter table mend.vat_dispensingservicesrate_aud add constraint [DF_mend_vat_dispensingservicesrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_dispensingservicesrate_aud_hist(
	id								bigint NOT NULL, 
	dispensingservicesrate			decimal(28,8), 
	rateeffectivedate				datetime2, 
	rateenddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.vat_dispensingservicesrate_aud_hist add constraint [DF_mend.vat_dispensingservicesrate_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$dispensingservices_commodity -------------------

create table mend.vat_dispensingservices_commodity (
	dispensingservicesrateid		bigint NOT NULL, 
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_dispensingservices_commodity add constraint [PK_mend_vat_dispensingservices_commodity]
	primary key clustered (dispensingservicesrateid,commodityid);
go
alter table mend.vat_dispensingservices_commodity add constraint [DF_mend_vat_dispensingservices_commodity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_dispensingservices_commodity_aud(
	dispensingservicesrateid		bigint NOT NULL, 
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_dispensingservices_commodity_aud add constraint [PK_mend_vat_dispensingservices_commodity_aud]
	primary key clustered (dispensingservicesrateid,commodityid);
go
alter table mend.vat_dispensingservices_commodity_aud add constraint [DF_mend_vat_dispensingservices_commodity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$dispensingservicesrate_country -------------------

create table mend.vat_dispensingservicesrate_country (
	dispensingservicesrateid		bigint NOT NULL, 
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_dispensingservicesrate_country add constraint [PK_mend_vat_dispensingservicesrate_country]
	primary key clustered (dispensingservicesrateid,countryid);
go
alter table mend.vat_dispensingservicesrate_country add constraint [DF_mend_vat_dispensingservicesrate_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_dispensingservicesrate_country_aud(
	dispensingservicesrateid		bigint NOT NULL, 
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_dispensingservicesrate_country_aud add constraint [PK_mend_vat_dispensingservicesrate_country_aud]
	primary key clustered (dispensingservicesrateid,countryid);
go
alter table mend.vat_dispensingservicesrate_country_aud add constraint [DF_mend_vat_dispensingservicesrate_country_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go










------------------- vat$vatrating -------------------

create table mend.vat_vatrating (
	id								bigint NOT NULL, 
	vatratingcode					varchar(12), 
	vatratingdescription			varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatrating add constraint [PK_mend_vat_vatrating]
	primary key clustered (id);
go
alter table mend.vat_vatrating add constraint [DF_mend_vat_vatrating_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrating_aud(
	id								bigint NOT NULL, 
	vatratingcode					varchar(12), 
	vatratingdescription			varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatrating_aud add constraint [PK_mend_vat_vatrating_aud]
	primary key clustered (id);
go
alter table mend.vat_vatrating_aud add constraint [DF_mend_vat_vatrating_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrating_aud_hist(
	id								bigint NOT NULL, 
	vatratingcode					varchar(12), 
	vatratingdescription			varchar(200),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.vat_vatrating_aud_hist add constraint [DF_mend.vat_vatrating_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$vatrating_country -------------------

create table mend.vat_vatrating_country (
	vatratingid						bigint NOT NULL, 
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatrating_country add constraint [PK_mend_vat_vatrating_country]
	primary key clustered (vatratingid,countryid);
go
alter table mend.vat_vatrating_country add constraint [DF_mend_vat_vatrating_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrating_country_aud(
	vatratingid						bigint NOT NULL, 
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatrating_country_aud add constraint [PK_mend_vat_vatrating_country_aud]
	primary key clustered (vatratingid,countryid);
go
alter table mend.vat_vatrating_country_aud add constraint [DF_mend_vat_vatrating_country_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------------- vat$vatrate -------------------

create table mend.vat_vatrate (
	id								bigint NOT NULL, 
	vatrate							decimal(28,8), 
	vatrateeffectivedate			datetime2, 
	vatrateenddate					datetime2, 
	createddate						datetime2, 
	changeddate						datetime2, 
	system$owner					bigint, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatrate add constraint [PK_mend_vat_vatrate]
	primary key clustered (id);
go
alter table mend.vat_vatrate add constraint [DF_mend_vat_vatrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrate_aud(
	id								bigint NOT NULL, 
	vatrate							decimal(28,8), 
	vatrateeffectivedate			datetime2, 
	vatrateenddate					datetime2, 
	createddate						datetime2, 
	changeddate						datetime2, 
	system$owner					bigint, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatrate_aud add constraint [PK_mend_vat_vatrate_aud]
	primary key clustered (id);
go
alter table mend.vat_vatrate_aud add constraint [DF_mend_vat_vatrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrate_aud_hist(
	id								bigint NOT NULL, 
	vatrate							decimal(28,8), 
	vatrateeffectivedate			datetime2, 
	vatrateenddate					datetime2, 
	createddate						datetime2, 
	changeddate						datetime2, 
	system$owner					bigint, 
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.vat_vatrate_aud_hist add constraint [DF_mend.vat_vatrate_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$vatrate_vatrating -------------------

create table mend.vat_vatrate_vatrating (
	vatrateid						bigint NOT NULL,
	vatratingid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatrate_vatrating add constraint [PK_mend_vat_vatrate_vatrating]
	primary key clustered (vatrateid,vatratingid);
go
alter table mend.vat_vatrate_vatrating add constraint [DF_mend_vat_vatrate_vatrating_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatrate_vatrating_aud(
	vatrateid						bigint NOT NULL,
	vatratingid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatrate_vatrating_aud add constraint [PK_mend_vat_vatrate_vatrating_aud]
	primary key clustered (vatrateid,vatratingid);
go
alter table mend.vat_vatrate_vatrating_aud add constraint [DF_mend_vat_vatrate_vatrating_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go








------------------- vat$vatschedule -------------------

create table mend.vat_vatschedule (
	id								bigint NOT NULL, 
	vatsaletype						varchar(19), 
	taxationarea					varchar(5), 
	scheduleeffectivedate			datetime2, 
	scheduleenddate					datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatschedule add constraint [PK_mend_vat_vatschedule]
	primary key clustered (id);
go
alter table mend.vat_vatschedule add constraint [DF_mend_vat_vatschedule_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatschedule_aud(
	id								bigint NOT NULL, 
	vatsaletype						varchar(19), 
	taxationarea					varchar(5), 
	scheduleeffectivedate			datetime2, 
	scheduleenddate					datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatschedule_aud add constraint [PK_mend_vat_vatschedule_aud]
	primary key clustered (id);
go
alter table mend.vat_vatschedule_aud add constraint [DF_mend_vat_vatschedule_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatschedule_aud_hist(
	id								bigint NOT NULL, 
	vatsaletype						varchar(19), 
	taxationarea					varchar(5), 
	scheduleeffectivedate			datetime2, 
	scheduleenddate					datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.vat_vatschedule_aud_hist add constraint [DF_mend.vat_vatschedule_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$vatschedule_vatrating -------------------

create table mend.vat_vatschedule_vatrating (
	vatscheduleid					bigint NOT NULL,
	vatratingid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatschedule_vatrating add constraint [PK_mend_vat_vatschedule_vatrating]
	primary key clustered (vatscheduleid,vatratingid);
go
alter table mend.vat_vatschedule_vatrating add constraint [DF_mend_vat_vatschedule_vatrating_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatschedule_vatrating_aud(
	vatscheduleid					bigint NOT NULL,
	vatratingid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatschedule_vatrating_aud add constraint [PK_mend_vat_vatschedule_vatrating_aud]
	primary key clustered (vatscheduleid,vatratingid);
go
alter table mend.vat_vatschedule_vatrating_aud add constraint [DF_mend_vat_vatschedule_vatrating_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- vat$vatschedule_commodity -------------------

create table mend.vat_vatschedule_commodity (
	vatscheduleid					bigint NOT NULL,
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.vat_vatschedule_commodity add constraint [PK_mend_vat_vatschedule_commodity]
	primary key clustered (vatscheduleid,commodityid);
go
alter table mend.vat_vatschedule_commodity add constraint [DF_mend_vat_vatschedule_commodity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.vat_vatschedule_commodity_aud(
	vatscheduleid					bigint NOT NULL,
	commodityid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.vat_vatschedule_commodity_aud add constraint [PK_mend_vat_vatschedule_commodity_aud]
	primary key clustered (vatscheduleid,commodityid);
go
alter table mend.vat_vatschedule_commodity_aud add constraint [DF_mend_vat_vatschedule_commodity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go