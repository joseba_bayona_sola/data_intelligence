
select id, 
	magentoProductID, magentoSKU, name, status
from Landing.mend.prod_productfamily
order by magentoProductID

select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	weight, height, width, depth 
from Landing.mend.prod_packsize

select packsizeid, productfamilyid
from Landing.mend.prod_packsize_productfamily

-- 

select pspf.productfamilyid, pspf.packsizeid, 
	pf.magentoProductID, pf.magentoSKU, pf.name, pf.status,
	ps.description, ps.size, 
	ps.packagingNo, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, 
	ps.weight, ps.height, ps.width, ps.depth 
from 
		Landing.mend.prod_productfamily_aud pf
	left join
		Landing.mend.prod_packsize_productfamily pspf on pf.id = pspf.productfamilyid
	right join
		Landing.mend.prod_packsize ps on pspf.packsizeid = ps.id
order by pf.magentoProductID, ps.size