
------------------------ Drop tables -------------------------------

use Landing
go

------------ vat$commodity ------------

drop table mend.vat_commodity;
go
drop table mend.vat_commodity_aud;
go
drop table mend.vat_commodity_aud_hist;
go

------------ product$productfamily_commodity ------------

drop table mend.vat_productfamily_commodity;
go
drop table mend.vat_productfamily_commodity_aud;
go


------------ vat$dispensingservicesrate ------------

drop table mend.vat_dispensingservicesrate;
go
drop table mend.vat_dispensingservicesrate_aud;
go
drop table mend.vat_dispensingservicesrate_aud_hist;
go

------------ vat$dispensingservices_commodity ------------

drop table mend.vat_dispensingservices_commodity;
go
drop table mend.vat_dispensingservices_commodity_aud;
go

------------ vat$dispensingservicesrate_country ------------

drop table mend.vat_dispensingservicesrate_country;
go
drop table mend.vat_dispensingservicesrate_country_aud;
go








------------ vat$vatrating ------------

drop table mend.vat_vatrating;
go
drop table mend.vat_vatrating_aud;
go
drop table mend.vat_vatrating_aud_hist;
go

------------ vat$vatrating_country ------------

drop table mend.vat_vatrating_country;
go
drop table mend.vat_vatrating_country_aud;
go

------------ vat$vatrate ------------

drop table mend.vat_vatrate;
go
drop table mend.vat_vatrate_aud;
go
drop table mend.vat_vatrate_aud_hist;
go

------------ vat$vatrate_vatrating ------------

drop table mend.vat_vatrate_vatrating;
go
drop table mend.vat_vatrate_vatrating_aud;
go








------------ vat$vatschedule ------------

drop table mend.vat_vatschedule;
go
drop table mend.vat_vatschedule_aud;
go
drop table mend.vat_vatschedule_aud_hist;
go

------------ vat$vatschedule_vatrating ------------

drop table mend.vat_vatschedule_vatrating;
go
drop table mend.vat_vatschedule_vatrating_aud;
go

------------ vat$vatschedule_commodity ------------

drop table mend.vat_vatschedule_commodity;
go
drop table mend.vat_vatschedule_commodity_aud;
go
