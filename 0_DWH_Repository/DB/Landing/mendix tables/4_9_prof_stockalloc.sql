
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ stockallocation$orderlinestockallocationtransaction ------------

select *
from mend.all_orderlinestockallocationtransaction

select top 1000 id, 
	transactionID, timestamp,
	allocationType, recordType, 
	allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
	stockAllocated, cancelled, 
	issuedDateTime,
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.all_orderlinestockallocationtransaction

select top 1000 id, 
	transactionID, timestamp,
	allocationType, recordType, 
	allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
	stockAllocated, cancelled, 
	issuedDateTime,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_orderlinestockallocationtransaction_aud

select id, 
	transactionID, timestamp,
	allocationType, recordType, 
	allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
	stockAllocated, cancelled, 
	issuedDateTime,
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.all_orderlinestockallocationtransaction_aud_hist

------------ stockallocation$at_order ------------

select *
from mend.all_at_order

select orderlinestockallocationtransactionid, orderid,
	idETLBatchRun, ins_ts
from mend.all_at_order

select orderlinestockallocationtransactionid, orderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_order_aud

------------ stockallocation$at_orderline ------------

select *
from mend.all_at_orderline

select orderlinestockallocationtransactionid, orderlineid,
	idETLBatchRun, ins_ts
from mend.all_at_orderline

select orderlinestockallocationtransactionid, orderlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_orderline_aud

------------ stockallocation$at_warehouse ------------

select *
from mend.all_at_warehouse

select orderlinestockallocationtransactionid, warehouseid,
	idETLBatchRun, ins_ts
from mend.all_at_warehouse

select orderlinestockallocationtransactionid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_warehouse_aud

------------ stockallocation$at_warehousestockitem ------------

select *
from mend.all_at_warehousestockitem

select orderlinestockallocationtransactionid, warehousestockitemid,
	idETLBatchRun, ins_ts
from mend.all_at_warehousestockitem

select orderlinestockallocationtransactionid, warehousestockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_warehousestockitem_aud

------------ stockallocation$at_batch ------------

select *
from mend.all_at_batch

select orderlinestockallocationtransactionid, warehousestockitembatchid,
	idETLBatchRun, ins_ts
from mend.all_at_batch

select orderlinestockallocationtransactionid, warehousestockitembatchid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_batch_aud

------------ stockallocation$at_packsize ------------

select *
from mend.all_at_packsize

select orderlinestockallocationtransactionid, packsizeid,
	idETLBatchRun, ins_ts
from mend.all_at_packsize

select orderlinestockallocationtransactionid, packsizeid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_at_packsize_aud

------------ stockallocatio$orderlinestockallocationtransa_customershipmentl ------------

select *
from mend.all_orderlinestockallocationtransa_customershipmentl

select orderlinestockallocationtransactionid, customershipmentlineid,
	idETLBatchRun, ins_ts
from mend.all_orderlinestockallocationtransa_customershipmentl

select orderlinestockallocationtransactionid, customershipmentlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_orderlinestockallocationtransa_customershipmentl_aud








------------ stockallocation$magentoallocation ------------

select *
from mend.all_magentoallocation

select top 1000 id, 
	shipped, cancelled, 
	quantity,
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.all_magentoallocation

select top 1000 id, 
	shipped, cancelled, 
	quantity,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_magentoallocation_aud

select id, 
	shipped, cancelled, 
	quantity,
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.all_magentoallocation_aud_hist

------------ stockallocatio$magentoallocatio_orderlinestockallocationtransac ------------

select *
from mend.all_magentoallocatio_orderlinestockallocationtransac

select magentoallocationid, orderlinestockallocationtransactionid,
	idETLBatchRun, ins_ts
from mend.all_magentoallocatio_orderlinestockallocationtransac

select magentoallocationid, orderlinestockallocationtransactionid,
	idETLBatchRun, ins_ts, upd_ts
from mend.all_magentoallocatio_orderlinestockallocationtransac_aud