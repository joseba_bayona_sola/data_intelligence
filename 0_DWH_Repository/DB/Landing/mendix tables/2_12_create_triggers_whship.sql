
------------------------------- Triggers ----------------------------------

use Landing
go

------------ warehouseshipments$receipt ------------

drop trigger mend.trg_whship_receipt;
go 

create trigger mend.trg_whship_receipt on mend.whship_receipt
after insert
as
begin
	set nocount on

	merge into mend.whship_receipt_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.shipmentid, ' '), isnull(trg.ordernumber, ' '), isnull(trg.receiptid, ' '), isnull(trg.receiptnumber, 0), isnull(trg.invoiceref, ' '),
			isnull(trg.status, ' '), isnull(trg.shipmenttype, ' '), 
			isnull(trg.duedate, ' '), isnull(trg.arriveddate, ' '), isnull(trg.confirmeddate, ' '), isnull(trg.stockregistereddate, ' '),
			isnull(trg.totalitemprice, 0), isnull(trg.intercompanycarriageprice, 0), isnull(trg.inboundcarriageprice, 0), isnull(trg.intercompanyprofit, 0), isnull(trg.duty, 0),
			isnull(trg.lock, 0),
			isnull(trg.carriagefix, 0), isnull(trg.intercofix, 0), 
			isnull(trg.auditcomment, ' '),
			isnull(trg.supplieradviceref , ' '),
			isnull(trg.createddate, ' ') --, isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.shipmentid, ' '), isnull(src.ordernumber, ' '), isnull(src.receiptid, ' '), isnull(src.receiptnumber, 0), isnull(src.invoiceref, ' '),
			isnull(src.status, ' '), isnull(src.shipmenttype, ' '), 
			isnull(src.duedate, ' '), isnull(src.arriveddate, ' '), isnull(src.confirmeddate, ' '), isnull(src.stockregistereddate, ' '),
			isnull(src.totalitemprice, 0), isnull(src.intercompanycarriageprice, 0), isnull(src.inboundcarriageprice, 0), isnull(src.intercompanyprofit, 0), isnull(src.duty, 0),
			isnull(src.lock, 0),
			isnull(src.carriagefix, 0), isnull(src.intercofix, 0), 
			isnull(src.auditcomment, ' '),
			isnull(src.supplieradviceref , ' '),
			isnull(src.createddate, ' ') --, isnull(src.changeddate, ' ')
			)
			
		then
			update set
				trg.shipmentid = src.shipmentid, trg.ordernumber = src.ordernumber,
				trg.receiptid = src.receiptid, trg.receiptnumber = src.receiptnumber, trg.invoiceref = src.invoiceref,
				trg.status = src.status, trg.shipmenttype = src.shipmenttype, 
				trg.duedate = src.duedate, trg.arriveddate = src.arriveddate, trg.confirmeddate = src.confirmeddate, trg.stockregistereddate = src.stockregistereddate,
				trg.totalitemprice = src.totalitemprice, trg.intercompanycarriageprice = src.intercompanycarriageprice, trg.inboundcarriageprice = src.inboundcarriageprice,
				trg.intercompanyprofit = src.intercompanyprofit, trg.duty = src.duty,
				trg.lock = src.lock,
				trg.carriagefix = src.carriagefix, trg.intercofix = src.intercofix,
				trg.auditcomment = src.auditcomment,
				trg.supplieradviceref  = src.supplieradviceref ,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
				status, shipmentType, 
				dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
				totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
				lock,
				carriagefix, intercofix,
				auditComment,
				supplieradviceref,
				createdDate, changeddate,
				idETLBatchRun)

				values (src.id, 
					src.shipmentID, src.orderNumber, src.receiptID, src.receiptNumber, src.invoiceRef,
					src.status, src.shipmentType, 
					src.dueDate, src.arrivedDate, src.confirmedDate, src.stockRegisteredDate, 
					src.totalItemPrice, src.interCompanyCarriagePrice, src.inboundCarriagePrice, src.interCompanyProfit, src.duty,
					src.lock,
					src.carriagefix, src.intercofix,
					src.auditComment,
					src.supplieradviceref,
					src.createdDate, src.changeddate,
					src.idETLBatchRun);
end; 
go

drop trigger mend.trg_whship_receipt_aud;
go 

create trigger mend.trg_whship_receipt_aud on mend.whship_receipt_aud
for update, delete
as
begin
	set nocount on

	insert mend.whship_receipt_aud_hist (id, 
		shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		carriagefix, intercofix,
		auditComment,
		supplieradviceref,
		createdDate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.shipmentID, d.orderNumber, d.receiptID, d.receiptNumber, d.invoiceRef,
			d.status, d.shipmentType, 
			d.dueDate, d.arrivedDate, d.confirmedDate, d.stockRegisteredDate, 
			d.totalItemPrice, d.interCompanyCarriagePrice, d.inboundCarriagePrice, d.interCompanyProfit, d.duty,
			d.lock,
			d.carriagefix, d.intercofix,
			d.auditComment,
			d.supplieradviceref ,
			d.createdDate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- warehouseshipments$receipt_warehouse -------------------

drop trigger mend.trg_whship_receipt_warehouse;
go 

create trigger mend.trg_whship_receipt_warehouse on mend.whship_receipt_warehouse
after insert
as
begin
	set nocount on

	merge into mend.whship_receipt_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.receiptid = src.receiptid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (receiptid, warehouseid, 
				idETLBatchRun)

				values (src.receiptid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------ warehouseshipments$receipt_supplier ------------

drop trigger mend.trg_whship_receipt_supplier;
go 

create trigger mend.trg_whship_receipt_supplier on mend.whship_receipt_supplier
after insert
as
begin
	set nocount on

	merge into mend.whship_receipt_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.receiptid = src.receiptid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (receiptid, supplierid, 
				idETLBatchRun)

				values (src.receiptid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------------- inventory$warehousestockitembatch_receipt -------------------

drop trigger mend.trg_whship_warehousestockitembatch_receipt;
go 

create trigger mend.trg_whship_warehousestockitembatch_receipt on mend.whship_warehousestockitembatch_receipt
after insert
as
begin
	set nocount on

	merge into mend.whship_warehousestockitembatch_receipt_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitembatchid = src.warehousestockitembatchid and trg.receiptid = src.receiptid)
	when not matched 
		then
			insert (warehousestockitembatchid, receiptid, 
				idETLBatchRun)

				values (src.warehousestockitembatchid, src.receiptid,
					src.idETLBatchRun);
end; 
go



------------------- inventory$warehousestockitembatch_receipthistory -------------------

drop trigger mend.trg_whship_warehousestockitembatch_receipthistory;
go 

create trigger mend.trg_whship_warehousestockitembatch_receipthistory on mend.whship_warehousestockitembatch_receipthistory
after insert
as
begin
	set nocount on

	merge into mend.whship_warehousestockitembatch_receipthistory_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitembatchid = src.warehousestockitembatchid and trg.receiptid = src.receiptid)
	when not matched 
		then
			insert (warehousestockitembatchid, receiptid, 
				idETLBatchRun)

				values (src.warehousestockitembatchid, src.receiptid,
					src.idETLBatchRun);
end; 
go



------------------- inventory$backtobackreceipt_purchaseorder -------------------

drop trigger mend.trg_whship_backtobackreceipt_purchaseorder;
go 

create trigger mend.trg_whship_backtobackreceipt_purchaseorder on mend.whship_backtobackreceipt_purchaseorder
after insert
as
begin
	set nocount on

	merge into mend.whship_backtobackreceipt_purchaseorder_aud with (tablock) as trg
	using inserted src
		on (trg.receiptid = src.receiptid and trg.purchaseorderid = src.purchaseorderid)
	when not matched 
		then
			insert (receiptid, purchaseorderid,
				idETLBatchRun)

				values (src.receiptid, src.purchaseorderid,
					src.idETLBatchRun);
end; 
go




------------------- inventory$receiptforcancelleditems -------------------

drop trigger mend.trg_whship_receiptforcancelleditems;
go 

create trigger mend.trg_whship_receiptforcancelleditems on mend.whship_receiptforcancelleditems
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptforcancelleditems_aud with (tablock) as trg
	using inserted src
		on (trg.receiptid1 = src.receiptid1 and trg.receiptid2 = src.receiptid2)
	when not matched 
		then
			insert (receiptid1, receiptid2,  
				idETLBatchRun)

				values (src.receiptid1, src.receiptid2, 
					src.idETLBatchRun);
end; 
go



------------------- warehouseshipments$receiptline -------------------

drop trigger mend.trg_whship_receiptline;
go 

create trigger mend.trg_whship_receiptline on mend.whship_receiptline
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptline_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.line, ' '), isnull(trg.stockitem, ' '),
			isnull(trg.status, ' '), isnull(trg.syncstatus, ' '), isnull(trg.syncresolution, ' '), isnull(trg.created, ' '), isnull(trg.processed, ' '),
			isnull(trg.quantityordered, 0), isnull(trg.quantityreceived, 0), isnull(trg.quantityaccepted, 0), isnull(trg.quantityrejected, 0), isnull(trg.quantityreturned, 0), 
			isnull(trg.unitcost, 0),
			isnull(trg.auditcomment, ' '), 
			isnull(trg.createddate, ' ') --, isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.line, ' '), isnull(src.stockitem, ' '),
			isnull(src.status, ' '), isnull(src.syncstatus, ' '), isnull(src.syncresolution, ' '), isnull(src.created, ' '), isnull(src.processed, ' '),
			isnull(src.quantityordered, 0), isnull(src.quantityreceived, 0), isnull(src.quantityaccepted, 0), isnull(src.quantityrejected, 0), isnull(src.quantityreturned, 0), 
			isnull(src.unitcost, 0),
			isnull(src.auditcomment, ' '), 
			isnull(src.createddate, ' ') --, isnull(src.changeddate, ' ')
			)
			
		then
			update set
				trg.line = src.line, trg.stockitem = src.stockitem,
				trg.status = src.status, trg.syncstatus = src.syncstatus, trg.syncresolution = src.syncresolution, trg.created = src.created, trg.processed = src.processed,
				trg.quantityordered = src.quantityordered, trg.quantityreceived = src.quantityreceived, trg.quantityaccepted = src.quantityaccepted, 
				trg.quantityrejected = src.quantityrejected, trg.quantityreturned = src.quantityreturned, 
				trg.unitcost = src.unitcost,
				trg.auditcomment = src.auditcomment,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				line, stockItem,
				status, syncStatus, syncResolution, created, processed,
				quantityOrdered, quantityReceived, quantityAccepted, quantityRejected, quantityReturned, 
				unitCost, 
				auditComment, 
				createdDate, changeddate,
				idETLBatchRun)

				values (src.id, 
					src.line, src.stockItem,
					src.status, src.syncStatus, src.syncResolution, src.created, src.processed,
					src.quantityOrdered, src.quantityReceived, src.quantityAccepted, src.quantityRejected, src.quantityReturned, 
					src.unitCost, 
					src.auditComment,
					src.createdDate, src.changeddate,
					src.idETLBatchRun);
end; 
go

drop trigger mend.trg_whship_receiptline_aud;
go 

create trigger mend.trg_whship_receiptline_aud on mend.whship_receiptline_aud
for update, delete
as
begin
	set nocount on

	insert mend.whship_receiptline_aud_hist (id, 
		line, stockItem,
		status, syncStatus, syncResolution, created, processed,
		quantityOrdered, quantityReceived, quantityAccepted, quantityRejected, quantityReturned, 
		unitCost, 
		auditComment,
		createdDate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.line, d.stockItem,
			d.status, d.syncStatus, d.syncResolution, d.created, d.processed,
			d.quantityOrdered, d.quantityReceived, d.quantityAccepted, d.quantityRejected, d.quantityReturned, 
			d.unitCost, 
			d.auditComment,
			d.createdDate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- warehouseshipments$receiptline_receipt -------------------

drop trigger mend.trg_whship_receiptline_receipt;
go 

create trigger mend.trg_whship_receiptline_receipt on mend.whship_receiptline_receipt
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptline_receipt_aud with (tablock) as trg
	using inserted src
		on (trg.receiptlineid = src.receiptlineid and trg.receiptid = src.receiptid)
	when not matched 
		then
			insert (receiptlineid, receiptid, 
				idETLBatchRun)

				values (src.receiptlineid, src.receiptid,
					src.idETLBatchRun);
end; 
go

------------------- warehouseshipments$receiptline_supplier -------------------

drop trigger mend.trg_whship_receiptline_supplier;
go 

create trigger mend.trg_whship_receiptline_supplier on mend.whship_receiptline_supplier
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptline_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.receiptlineid = src.receiptlineid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (receiptlineid, supplierid, 
				idETLBatchRun)

				values (src.receiptlineid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------------- warehouseshipments$receiptline_purchaseorderline -------------------

drop trigger mend.trg_whship_receiptline_purchaseorderline;
go 

create trigger mend.trg_whship_receiptline_purchaseorderline on mend.whship_receiptline_purchaseorderline
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptline_purchaseorderline_aud with (tablock) as trg
	using inserted src
		on (trg.receiptlineid = src.receiptlineid and trg.purchaseorderlineid = src.purchaseorderlineid)
	when not matched 
		then
			insert (receiptlineid, purchaseorderlineid, 
				idETLBatchRun)

				values (src.receiptlineid, src.purchaseorderlineid,
					src.idETLBatchRun);
end; 
go

------------------- warehouseshipments$receiptline_snapreceiptline -------------------

drop trigger mend.trg_whship_receiptline_snapreceiptline;
go 

create trigger mend.trg_whship_receiptline_snapreceiptline on mend.whship_receiptline_snapreceiptline
after insert
as
begin
	set nocount on

	merge into mend.whship_receiptline_snapreceiptline_aud with (tablock) as trg
	using inserted src
		on (trg.receiptlineid = src.receiptlineid and trg.snapreceiptlineid = src.snapreceiptlineid)
	when not matched 
		then
			insert (receiptlineid, snapreceiptlineid, 
				idETLBatchRun)

				values (src.receiptlineid, src.snapreceiptlineid,
					src.idETLBatchRun);
end; 
go

------------------- inventory$warehousestockitembatch_receiptline -------------------

drop trigger mend.trg_whship_warehousestockitembatch_receiptline;
go 

create trigger mend.trg_whship_warehousestockitembatch_receiptline on mend.whship_warehousestockitembatch_receiptline
after insert
as
begin
	set nocount on

	merge into mend.whship_warehousestockitembatch_receiptline_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitembatchid = src.warehousestockitembatchid and trg.receiptlineid = src.receiptlineid)
	when not matched 
		then
			insert (warehousestockitembatchid, receiptlineid, 
				idETLBatchRun)

				values (src.warehousestockitembatchid, src.receiptlineid,
					src.idETLBatchRun);
end; 
go