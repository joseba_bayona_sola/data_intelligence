use Landing
go 

----------------------- inventory$warehouse ----------------------------

select * 
from mend.wh_warehouse_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$supplierroutine ------------

select * 
from mend.wh_supplierroutine_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$supplierroutine_warehousesupplier ------------

select * 
from mend.wh_supplierroutine_warehousesupplier_aud_v
where num_records > 1
order by id, idETLBatchRun