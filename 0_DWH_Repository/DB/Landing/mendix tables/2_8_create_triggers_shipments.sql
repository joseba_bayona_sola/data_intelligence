
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- customershipments$customershipment -------------------

drop trigger mend.trg_ship_customershipment;
go 

create trigger mend.trg_ship_customershipment on mend.ship_customershipment
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipment_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.shipmentID, 0), isnull(trg.partitionID0_120, 0),
			isnull(trg.shipmentNumber, ' '), isnull(trg.orderIncrementID, ' '),
			isnull(trg.status, ' '), isnull(trg.statusChange, ' '),
			isnull(trg.processingFailed, ' '), isnull(trg.warehouseMethod, ' '), isnull(trg.labelRenderer, ' '),
			isnull(trg.shippingMethod, ' '), isnull(trg.shippingDescription, ' '), isnull(trg.paymentMethod, ' '),
			isnull(trg.notify, ' '), isnull(trg.fullyShipped, ' '), isnull(trg.syncedToMagento, ' '),
			isnull(trg.shipmentValue, 0), isnull(trg.shippingTotal, 0), isnull(trg.toFollowValue, 0),
			isnull(trg.hold, ' '), isnull(trg.intersite, ' '),
			isnull(trg.dispatchDate, ' '), isnull(trg.expectedShippingDate, ' '), isnull(trg.expectedDeliveryDate, ' '),
			isnull(trg.createdDate, ' ') -- isnull(trg.changedDate, ' ')
		intersect
		select 
			isnull(src.shipmentID, 0), isnull(src.partitionID0_120, 0), 
			isnull(src.shipmentNumber, ' '), isnull(src.orderIncrementID, ' '),
			isnull(src.status, ' '), isnull(src.statusChange, ' '),
			isnull(src.processingFailed, ' '), isnull(src.warehouseMethod, ' '), isnull(src.labelRenderer, ' '),
			isnull(src.shippingMethod, ' '), isnull(src.shippingDescription, ' '), isnull(src.paymentMethod, ' '),
			isnull(src.notify, ' ' ), isnull(src.fullyShipped, ' '), isnull(src.syncedToMagento, ' '),
			isnull(src.shipmentValue, 0), isnull(src.shippingTotal, 0), isnull(src.toFollowValue, 0),
			isnull(src.hold, ' '), isnull(src.intersite, ' '),
			isnull(src.dispatchDate, ' '), isnull(src.expectedShippingDate, ' '), isnull(src.expectedDeliveryDate, ' '),
			isnull(src.createdDate, ' ') -- isnull(src.changedDate, ' ')
			)
			
		then
			update set
				trg.shipmentID = src.shipmentID, trg.partitionID0_120 = src.partitionID0_120,
				trg.shipmentNumber = src.shipmentNumber, trg.orderIncrementID = src.orderIncrementID, 
				trg.status = src.status, trg.statusChange = src.statusChange, 
				trg.processingFailed = src.processingFailed, trg.warehouseMethod = src.warehouseMethod, trg.labelRenderer = src.labelRenderer,
				trg.shippingMethod = src.shippingMethod, trg.shippingDescription = src.shippingDescription, trg.paymentMethod = src.paymentMethod,
				trg.notify = src.notify, trg.fullyShipped = src.fullyShipped, trg.syncedToMagento = src.syncedToMagento,
				trg.shipmentValue = src.shipmentValue, trg.shippingTotal = src.shippingTotal, trg.toFollowValue = src.toFollowValue,
				trg.hold = src.hold, trg.intersite = src.intersite, 
				trg.dispatchDate = src.dispatchDate, trg.expectedShippingDate = src.expectedShippingDate, trg.expectedDeliveryDate = src.expectedDeliveryDate,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,  
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				shipmentID, partitionID0_120,
				shipmentNumber, orderIncrementID, 
				status, statusChange, 
				processingFailed, warehouseMethod, labelRenderer, 
				shippingMethod, shippingDescription, paymentMethod, 	
				notify, fullyShipped, syncedToMagento, 	
				shipmentValue, shippingTotal, toFollowValue, 
				hold, intersite, 
				dispatchDate, expectedShippingDate, expectedDeliveryDate,  
				createdDate, changedDate,
				idETLBatchRun)

				values (src.id,
					src.shipmentID, src.partitionID0_120,
					src.shipmentNumber, src.orderIncrementID, 
					src.status, src.statusChange, 
					src.processingFailed, src.warehouseMethod, src.labelRenderer, 
					src.shippingMethod, src.shippingDescription, src.paymentMethod, 	
					src.notify, src.fullyShipped, src.syncedToMagento, 	
					src.shipmentValue, src.shippingTotal, src.toFollowValue, 
					src.hold, src.intersite, 
					src.dispatchDate, src.expectedShippingDate, src.expectedDeliveryDate,  
					src.createdDate, src.changedDate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_customershipment_aud;
go 

create trigger mend.trg_ship_customershipment_aud on mend.ship_customershipment_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_customershipment_aud_hist (id,
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, statusChange, 
		processingFailed, warehouseMethod, labelRenderer, 
		shippingMethod, shippingDescription, paymentMethod, 	
		notify, fullyShipped, syncedToMagento, 	
		shipmentValue, shippingTotal, toFollowValue, 
		hold, intersite, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate, changedDate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.shipmentID, d.partitionID0_120,
			d.shipmentNumber, d.orderIncrementID, 
			d.status, d.statusChange, 
			d.processingFailed, d.warehouseMethod, d.labelRenderer, 
			d.shippingMethod, d.shippingDescription, d.paymentMethod, 	
			d.notify, d.fullyShipped, d.syncedToMagento, 	
			d.shipmentValue, d.shippingTotal, d.toFollowValue, 
			d.hold, d.intersite, 
			d.dispatchDate, d.expectedShippingDate, d.expectedDeliveryDate,  
			d.createdDate, d.changedDate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$customershipment_warehouse -------------------

drop trigger mend.trg_ship_customershipment_warehouse;
go 

create trigger mend.trg_ship_customershipment_warehouse on mend.ship_customershipment_warehouse
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipment_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.customershipmentid = src.customershipmentid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (customershipmentid, warehouseid, 
				idETLBatchRun)

				values (src.customershipmentid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------------- customershipments$customershipment_order -------------------

drop trigger mend.trg_ship_customershipment_order;
go 

create trigger mend.trg_ship_customershipment_order on mend.ship_customershipment_order
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipment_order_aud with (tablock) as trg
	using inserted src
		on (trg.customershipmentid = src.customershipmentid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (customershipmentid, orderid, 
				idETLBatchRun)

				values (src.customershipmentid, src.orderid,
					src.idETLBatchRun);
end; 
go





------------------- customershipments$lifecycle_update -------------------

drop trigger mend.trg_ship_lifecycle_update;
go 

create trigger mend.trg_ship_lifecycle_update on mend.ship_lifecycle_update
after insert
as
begin
	set nocount on

	merge into mend.ship_lifecycle_update_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.status, ' '), isnull(trg.timestamp, ' ')
		intersect
		select 
			isnull(src.status, ' '), isnull(src.timestamp, ' '))
			
		then
			update set
				trg.status = src.status, trg.timestamp = src.timestamp,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				status, timestamp, 
				idETLBatchRun)

				values (src.id,
					src.status, src.timestamp,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_lifecycle_update_aud;
go 

create trigger mend.trg_ship_lifecycle_update_aud on mend.ship_lifecycle_update_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_lifecycle_update_aud_hist (id,
		status, timestamp, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.status, d.timestamp,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$customershipment_progress -------------------

drop trigger mend.trg_ship_customershipment_progress;
go 

create trigger mend.trg_ship_customershipment_progress on mend.ship_customershipment_progress
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipment_progress_aud with (tablock) as trg
	using inserted src
		on (trg.lifecycle_updateid = src.lifecycle_updateid and trg.customershipmentid = src.customershipmentid)
	when not matched 
		then
			insert (lifecycle_updateid, customershipmentid, 
				idETLBatchRun)

				values (src.lifecycle_updateid, src.customershipmentid,
					src.idETLBatchRun);
end; 
go

------------------- customershipments$api_updates -------------------

drop trigger mend.trg_ship_api_updates;
go 

create trigger mend.trg_ship_api_updates on mend.ship_api_updates
after insert
as
begin
	set nocount on

	merge into mend.ship_api_updates_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.service, ' '), isnull(trg.status, ' '), isnull(trg.operation, ' '),
			isnull(trg.timestamp, ' '),
			isnull(trg.detail, ' ')
		intersect
		select 
			isnull(src.service, ' '), isnull(src.status, ' '), isnull(src.operation, ' '),
			isnull(src.timestamp, ' '),
			isnull(src.detail, ' '))
			
		then
			update set
				trg.service = src.service, trg.status = src.status, trg.operation = src.operation,
				trg.timestamp = src.timestamp,
				trg.detail = src.detail,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				service, status, operation, 
				timestamp, 
				detail, 
				idETLBatchRun)

				values (src.id,
					src.service, src.status, src.operation,
					src.timestamp,
					src.detail,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_api_updates_aud;
go 

create trigger mend.trg_ship_api_updates_aud on mend.ship_api_updates_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_api_updates_aud_hist (id,
		service, status, operation, 
		timestamp, 
		detail,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.service, d.status, d.operation, 
			d.timestamp, 
			d.detail,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$api_updates_customershipment -------------------

drop trigger mend.trg_ship_api_updates_customershipment;
go 

create trigger mend.trg_ship_api_updates_customershipment on mend.ship_api_updates_customershipment
after insert
as
begin
	set nocount on

	merge into mend.ship_api_updates_customershipment_aud with (tablock) as trg
	using inserted src
		on (trg.api_updatesid = src.api_updatesid and trg.customershipmentid = src.customershipmentid)
	when not matched 
		then
			insert (api_updatesid, customershipmentid, 
				idETLBatchRun)

				values (src.api_updatesid, src.customershipmentid,
					src.idETLBatchRun);
end; 
go





------------------- customershipments$consignment -------------------

drop trigger mend.trg_ship_consignment;
go 

create trigger mend.trg_ship_consignment on mend.ship_consignment
after insert
as
begin
	set nocount on

	merge into mend.ship_consignment_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.order_number, ' '), isnull(trg.identifier, ' '),
			isnull(trg.warehouse_id, ' '),
			isnull(trg.consignment_number, ' '),
			isnull(trg.shipping_method, ' '), isnull(trg.service_id, ' '), isnull(trg.carrier, ' '), isnull(trg.service, ' '),
			isnull(trg.delivery_instructions, ' '), isnull(trg.tracking_url, ' '),
			isnull(trg.order_value, ' '), isnull(trg.currency, ' '),
			isnull(trg.create_date, ' ')
		intersect
		select 
			isnull(src.order_number, ' '), isnull(src.identifier, ' '),
			isnull(src.warehouse_id, ' '),
			isnull(src.consignment_number, ' '),
			isnull(src.shipping_method, ' '), isnull(src.service_id, ' '), isnull(src.carrier, ' '), isnull(src.service, ' '),
			isnull(src.delivery_instructions, ' '), isnull(src.tracking_url, ' '),
			isnull(src.order_value, ' '), isnull(src.currency, ' '),
			isnull(src.create_date, ' '))
			
		then
			update set
				trg.order_number = src.order_number, trg.identifier = src.identifier, 
				trg.warehouse_id = src.warehouse_id,
				trg.consignment_number = src.consignment_number,
				trg.shipping_method = src.shipping_method, trg.service_id = src.service_id, trg.carrier = src.carrier, trg.service = src.service,
				trg.delivery_instructions = src.delivery_instructions, trg.tracking_url = src.tracking_url,
				trg.order_value = src.order_value, trg.currency = src.currency,
				trg.create_date = src.create_date,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				order_number, identifier, 
				warehouse_id, 
				consignment_number, 
				shipping_method, service_id, carrier, service, 
				delivery_instructions, tracking_url, 
				order_value, currency,
				create_date, 
				idETLBatchRun)

				values (src.id,
					src.order_number, src.identifier, 
					src.warehouse_id, 
					src.consignment_number, 
					src.shipping_method, src.service_id, src.carrier, src.service, 
					src.delivery_instructions, src.tracking_url, 
					src.order_value, src.currency,
					src.create_date,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_consignment_aud;
go 

create trigger mend.trg_ship_consignment_aud on mend.ship_consignment_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_consignment_aud_hist (id,
		order_number, identifier, 
		warehouse_id, 
		consignment_number, 
		shipping_method, service_id, carrier, service, 
		delivery_instructions, tracking_url, 
		order_value, currency,
		create_date,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.order_number, d.identifier, 
			d.warehouse_id, 
			d.consignment_number, 
			d.shipping_method, d.service_id, d.carrier, d.service, 
			d.delivery_instructions, d.tracking_url, 
			d.order_value, d.currency,
			d.create_date,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$scurriconsignment_shipment -------------------

drop trigger mend.trg_ship_scurriconsignment_shipment;
go 

create trigger mend.trg_ship_scurriconsignment_shipment on mend.ship_scurriconsignment_shipment
after insert
as
begin
	set nocount on

	merge into mend.ship_scurriconsignment_shipment_aud with (tablock) as trg
	using inserted src
		on (trg.consignmentid = src.consignmentid and trg.customershipmentid = src.customershipmentid)
	when not matched 
		then
			insert (consignmentid, customershipmentid, 
				idETLBatchRun)

				values (src.consignmentid, src.customershipmentid,
					src.idETLBatchRun);
end; 
go

------------------- customershipments$packagedetail -------------------

drop trigger mend.trg_ship_packagedetail;
go 

create trigger mend.trg_ship_packagedetail on mend.ship_packagedetail
after insert
as
begin
	set nocount on

	merge into mend.ship_packagedetail_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.tracking_number, ' '), isnull(trg.description, ' '),
			isnull(trg.width, 0), isnull(trg.length, 0), isnull(trg.height, 0)
		intersect
		select 
			isnull(src.tracking_number, ' '), isnull(src.description, ' '),
			isnull(src.width, 0), isnull(src.length, 0), isnull(src.height, 0))
			
		then
			update set
				trg.tracking_number = src.tracking_number, trg.description = src.description, 
				trg.width = src.width, trg.length = src.length, trg.height = src.height,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				tracking_number, description, 
				width, length, height, 
				idETLBatchRun)

				values (src.id,
					src.tracking_number, src.description, 
					src.width, src.length, src.height,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_packagedetail_aud;
go 

create trigger mend.trg_ship_packagedetail_aud on mend.ship_packagedetail_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_packagedetail_aud_hist (id,
		tracking_number, description, 
		width, length, height,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.tracking_number, d.description, 
			d.width, d.length, d.height,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$consignmentpackages -------------------

drop trigger mend.trg_ship_consignmentpackages;
go 

create trigger mend.trg_ship_consignmentpackages on mend.ship_consignmentpackages
after insert
as
begin
	set nocount on

	merge into mend.ship_consignmentpackages_aud with (tablock) as trg
	using inserted src
		on (trg.packagedetailid = src.packagedetailid and trg.consignmentid = src.consignmentid)
	when not matched 
		then
			insert (packagedetailid, consignmentid, 
				idETLBatchRun)

				values (src.packagedetailid, src.consignmentid,
					src.idETLBatchRun);
end; 
go





------------------- customershipments$customershipmentline -------------------

drop trigger mend.trg_ship_customershipmentline;
go 

create trigger mend.trg_ship_customershipmentline on mend.ship_customershipmentline
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipmentline_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.shipmentLineID, 0), isnull(trg.lineNumber, 0), isnull(trg.status, ' '), isnull(trg.variableBreakdown, ' '),
			isnull(trg.stockItemID, 0), isnull(trg.productName, ' '), isnull(trg.productDescription, ' '), isnull(trg.eye, ' '),
			isnull(trg.quantityDescription, ' '), isnull(trg.itemQuantityOrdered, 0), isnull(trg.itemQuantityIssued, 0), isnull(trg.itemQuantityShipped, 0), isnull(trg.itemQuantityPreviouslyShipped, 0),
			isnull(trg.packQuantityOrdered, 0), isnull(trg.packQuantityIssued, 0), isnull(trg.packQuantityShipped, 0),
			isnull(trg.price, 0), isnull(trg.priceFormatted, ' '),
			isnull(trg.subTotal, 0), isnull(trg.subTotal_formatted, ' '),
			isnull(trg.VATRate, ' '), isnull(trg.VAT, 0), isnull(trg.VAT_formatted, ' '),
			isnull(trg.fullyShipped, ' '),
			isnull(trg.BOMLine, ' '), 
			isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.shipmentLineID, 0), isnull(src.lineNumber, 0), isnull(src.status, ' '), isnull(src.variableBreakdown, ' '),
			isnull(src.stockItemID, 0), isnull(src.productName, ' '), isnull(src.productDescription, ' '), isnull(src.eye, ' '),
			isnull(src.quantityDescription, ' '), isnull(src.itemQuantityOrdered, 0), isnull(src.itemQuantityIssued, 0), isnull(src.itemQuantityShipped, 0), isnull(src.itemQuantityPreviouslyShipped, 0),
			isnull(src.packQuantityOrdered, 0), isnull(src.packQuantityIssued, 0), isnull(src.packQuantityShipped, 0),
			isnull(src.price, 0), isnull(src.priceFormatted, ' '),
			isnull(src.subTotal, 0), isnull(src.subTotal_formatted, ' '),
			isnull(src.VATRate, ' '), isnull(src.VAT, 0), isnull(src.VAT_formatted, ' '),
			isnull(src.fullyShipped, ' '),
			isnull(src.BOMLine, ' '), 
			isnull(src.system$changedby, 0))
			
		then
			update set
				trg.shipmentLineID = src.shipmentLineID, trg.lineNumber = src.lineNumber, trg.status = src.status, trg.variableBreakdown = src.variableBreakdown,
				trg.stockItemID = src.stockItemID, trg.productName = src.productName, trg.productDescription = src.productDescription, trg.eye = src.eye, 
				trg.quantityDescription = src.quantityDescription, trg.itemQuantityOrdered = src.itemQuantityOrdered, trg.itemQuantityIssued = src.itemQuantityIssued, 
				trg.itemQuantityShipped = src.itemQuantityShipped, trg.itemQuantityPreviouslyShipped = src.itemQuantityPreviouslyShipped,
				trg.packQuantityOrdered = src.packQuantityOrdered, trg.packQuantityIssued = src.packQuantityIssued, trg.packQuantityShipped = src.packQuantityShipped,
				trg.price = src.price, trg.priceFormatted = src.priceFormatted, 
				trg.subTotal = src.subTotal, trg.subTotal_formatted = src.subTotal_formatted,
				trg.VATRate = src.VATRate, trg.VAT = src.VAT, trg.VAT_formatted = src.VAT_formatted,
				trg.fullyShipped = src.fullyShipped,
				trg.BOMLine = src.BOMLine,
				trg.system$changedby = src.system$changedby,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				shipmentLineID, lineNumber, status, variableBreakdown, 
				stockItemID, productName, productDescription, eye, 
				quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
				packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
				price, priceFormatted, 
				subTotal, subTotal_formatted, 
				VATRate, VAT, VAT_formatted,
				fullyShipped,  
				BOMLine, 
				system$changedby,
				idETLBatchRun)

				values (src.id,
					src.shipmentLineID, src.lineNumber, src.status, src.variableBreakdown, 
					src.stockItemID, src.productName, src.productDescription, src.eye, 
					src.quantityDescription, src.itemQuantityOrdered, src.itemQuantityIssued, src.itemQuantityShipped, src.itemQuantityPreviouslyShipped, 
					src.packQuantityOrdered, src.packQuantityIssued, src.packQuantityShipped, 
					src.price, src.priceFormatted, 
					src.subTotal, src.subTotal_formatted, 
					src.VATRate, src.VAT, src.VAT_formatted,
					src.fullyShipped,  
					src.BOMLine, 
					src.system$changedby,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_ship_customershipmentline_aud;
go 

create trigger mend.trg_ship_customershipmentline_aud on mend.ship_customershipmentline_aud
for update, delete
as
begin
	set nocount on

	insert mend.ship_customershipmentline_aud_hist (id,
		shipmentLineID, lineNumber, status, variableBreakdown, 
		stockItemID, productName, productDescription, eye, 
		quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
		packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
		price, priceFormatted, 
		subTotal, subTotal_formatted, 
		VATRate, VAT, VAT_formatted,
		fullyShipped,  
		BOMLine, 
		system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.shipmentLineID, d.lineNumber, d.status, d.variableBreakdown, 
			d.stockItemID, d.productName, d.productDescription, d.eye, 
			d.quantityDescription, d.itemQuantityOrdered, d.itemQuantityIssued, d.itemQuantityShipped, d.itemQuantityPreviouslyShipped, 
			d.packQuantityOrdered, d.packQuantityIssued, d.packQuantityShipped, 
			d.price, d.priceFormatted, 
			d.subTotal, d.subTotal_formatted, 
			d.VATRate, d.VAT, d.VAT_formatted,
			d.fullyShipped,  
			d.BOMLine, 
			d.system$changedby,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- customershipments$customershipmentline_customershipment -------------------

drop trigger mend.trg_ship_customershipmentline_customershipment;
go 

create trigger mend.trg_ship_customershipmentline_customershipment on mend.ship_customershipmentline_customershipment
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipmentline_customershipment_aud with (tablock) as trg
	using inserted src
		on (trg.customershipmentlineid = src.customershipmentlineid and trg.customershipmentid = src.customershipmentid)
	when not matched 
		then
			insert (customershipmentlineid, customershipmentid, 
				idETLBatchRun)

				values (src.customershipmentlineid, src.customershipmentid,
					src.idETLBatchRun);
end; 
go

------------------- customershipments$customershipmentline_product -------------------

drop trigger mend.trg_ship_customershipmentline_product;
go 

create trigger mend.trg_ship_customershipmentline_product on mend.ship_customershipmentline_product
after insert
as
begin
	set nocount on

	merge into mend.ship_customershipmentline_product_aud with (tablock) as trg
	using inserted src
		on (trg.customershipmentlineid = src.customershipmentlineid and trg.productid = src.productid)
	when not matched 
		then
			insert (customershipmentlineid, productid, 
				idETLBatchRun)

				values (src.customershipmentlineid, src.productid,
					src.idETLBatchRun);
end; 
go