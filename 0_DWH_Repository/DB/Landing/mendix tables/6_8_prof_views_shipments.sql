use Landing
go 

------------------- customershipments$customershipment -------------------

select * 
from mend.ship_customershipment_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- customershipments$lifecycle_update -------------------

select * 
from mend.ship_lifecycle_update_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- customershipments$api_updates -------------------

select * 
from mend.ship_api_updates_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- customershipments$consignment -------------------

select * 
from mend.ship_consignment_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- customershipments$packagedetail -------------------

select * 
from mend.ship_packagedetail_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- customershipments$customershipmentline -------------------

select * 
from mend.ship_customershipmentline_aud_v
where num_records > 1
order by id, idETLBatchRun

