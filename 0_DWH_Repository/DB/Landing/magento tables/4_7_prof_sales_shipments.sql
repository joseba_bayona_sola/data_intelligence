use Landing
go 

----------------------- sales_flat_shipment ----------------------------

select *
from mag.sales_flat_shipment;

select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	total_qty, 
	total_weight, 
	shipping_description, 
	idETLBatchRun, ins_ts
from mag.sales_flat_shipment;

select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	total_qty, 
	total_weight, 
	shipping_description, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_shipment_aud;

select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	total_qty, 
	total_weight, 
	shipping_description, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.sales_flat_shipment_aud_hist;


----------------------- sales_flat_shipment_item ----------------------------

select *
from mag.sales_flat_shipment_item;

select entity_id, order_item_id, parent_id, 
	product_id, sku, name, description, 
	qty, 
	weight, 
	price, 
	row_total, 
	idETLBatchRun, ins_ts
from mag.sales_flat_shipment_item;

select entity_id, order_item_id, parent_id, 
	product_id, sku, name, description, 
	qty, 
	weight, 
	price, 
	row_total, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_shipment_item_aud;

select entity_id, order_item_id, parent_id, 
	product_id, sku, name, description, 
	qty, 
	weight, 
	price, 
	row_total, 
	idETLBatchRun, ins_ts, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.sales_flat_shipment_item_aud_hist;

