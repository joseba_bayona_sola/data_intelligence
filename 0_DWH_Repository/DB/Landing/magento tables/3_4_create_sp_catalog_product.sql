
use Landing
go 

----- 

drop procedure mag.srcmag_lnd_get_catalog_product_entity
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, 
				entity_type_id, attribute_set_id, 
				type_id, sku, required_options, has_options, 
				created_at, updated_at  
			from magento01.catalog_product_entity ' +
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')			
			'')'

	set @sql_2 = '
		select entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, 
				entity_type_id, attribute_set_id, 
				type_id, sku, required_options, has_options, 
				created_at, updated_at  
			from magento01.catalog_product_entity '')'

	
	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

--

drop procedure mag.srcmag_lnd_get_catalog_product_entity_datetime
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_datetime
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_datetime
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_datetime cei on ce.entity_id = cei.entity_id ' + 
			'where ce.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or ce.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
		'')'

	set @sql_2 = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_datetime cei on ce.entity_id = cei.entity_id '')'

	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

--

drop procedure mag.srcmag_lnd_get_catalog_product_entity_decimal
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_decimal
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_decimal
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_decimal cei on ce.entity_id = cei.entity_id ' + 
			'where ce.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or ce.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
		'')'

	set @sql_2 = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_decimal cei on ce.entity_id = cei.entity_id '')'

	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

--

drop procedure mag.srcmag_lnd_get_catalog_product_entity_int
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_int
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_int
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_int cei on ce.entity_id = cei.entity_id ' + 
			'where ce.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or ce.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
		'')'

	set @sql_2 = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_int cei on ce.entity_id = cei.entity_id '')'

	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

--

drop procedure mag.srcmag_lnd_get_catalog_product_entity_text
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_text
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_text
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_text cei on ce.entity_id = cei.entity_id ' + 
			'where ce.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or ce.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
		'')'

	set @sql_2 = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_text cei on ce.entity_id = cei.entity_id '')'

	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

--

drop procedure mag.srcmag_lnd_get_catalog_product_entity_varchar
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	03-03-2017	Joseba Bayona Sola	Add temporarely a SQL 2nd string that do a full load of data from Magento
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_varchar
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_varchar
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max), @sql_2 varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_varchar cei on ce.entity_id = cei.entity_id ' + 
			'where ce.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or ce.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
		'')'

	set @sql_2 = '
		select value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.store_id, cei.value ' + 
			'from 
				magento01.catalog_product_entity ce 
			inner join
				magento01.catalog_product_entity_varchar cei on ce.entity_id = cei.entity_id '')'

	exec(@sql_2)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

-----------------------------------------

drop procedure mag.srcmag_lnd_get_catalog_product_entity_tier_price
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_entity_tier_price
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_entity_tier_price
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select value_id, 
				entity_id, customer_group_id, all_groups, website_id, 
				value, qty, promo_key ' + 
			'from magento01.catalog_product_entity_tier_price '')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

---- 

drop procedure mag.srcmag_lnd_get_catalog_category_product
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_category_product
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_category_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select category_id, product_id, position,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select category_id, product_id, position ' + 
			'from magento01.catalog_category_product '')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

---- 

drop procedure mag.srcmag_lnd_get_catalog_product_website
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - catalog_product_website
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_catalog_product_website
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select product_id, website_id,  ' + cast(@idETLBatchRun as varchar(20)) + 
		' from openquery(MAGENTO, 
			''select product_id, website_id ' + 
			'from magento01.catalog_product_website '')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

---- 
