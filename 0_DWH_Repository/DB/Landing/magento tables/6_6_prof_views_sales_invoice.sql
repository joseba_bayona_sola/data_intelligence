use Landing
go 

----------------------- sales_flat_invoice ----------------------------

select * 
from mag.sales_flat_invoice_aud_v
where num_records > 1
order by num_records, entity_id, idETLBatchRun

----------------------- sales_flat_invoice_item ----------------------------

select * 
from mag.sales_flat_invoice_item_aud_v
where num_records > 1
order by num_records, parent_id, entity_id, idETLBatchRun

