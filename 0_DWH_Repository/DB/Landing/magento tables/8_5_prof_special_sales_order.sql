use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_order ----------------------------
------------------------------------------------------------------------

select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids,
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	old_order_id,
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_aud
order by created_at desc

	select store_id, count(*)
	from mag.sales_flat_order_aud
	group by store_id
	order by store_id

	select state, count(*)
	from mag.sales_flat_order_aud
	group by state
	order by state

	select status, count(*)
	from mag.sales_flat_order_aud
	group by status
	order by status

	select shipping_description, count(*)
	from mag.sales_flat_order_aud
	group by shipping_description
	order by shipping_description

	select coupon_code, count(*)
	from mag.sales_flat_order_aud
	group by coupon_code
	order by coupon_code

	select applied_rule_ids, count(*)
	from mag.sales_flat_order_aud
	group by applied_rule_ids
	order by applied_rule_ids

	select affilBatch, affilCode, affilUserRef, count(*)
	from mag.sales_flat_order_aud
	group by affilBatch, affilCode, affilUserRef
	order by affilBatch, affilCode, affilUserRef

	select reminder_type, count(*)
	from mag.sales_flat_order_aud
	group by reminder_type
	order by reminder_type

	select reminder_period, count(*)
	from mag.sales_flat_order_aud
	group by reminder_period
	order by reminder_period

	select reminder_follow_sent, reminder_sent, count(*)
	from mag.sales_flat_order_aud
	group by reminder_follow_sent, reminder_sent
	order by reminder_follow_sent, reminder_sent

		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent
		from mag.sales_flat_order_aud
		-- where reminder_mobile is not null
		where reminder_presc is not null

	select reorder_on_flag, count(*)
	from mag.sales_flat_order_aud
	group by reorder_on_flag
	order by reorder_on_flag

	select automatic_reorder, count(*)
	from mag.sales_flat_order_aud
	group by automatic_reorder
	order by automatic_reorder

		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			reorder_on_flag, reorder_profile_id, automatic_reorder
		from mag.sales_flat_order_aud
		where reorder_profile_id is not null


		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			referafriend_code, referafriend_referer
		from mag.sales_flat_order_aud
		where referafriend_code is not null

	select telesales_method_code, count(*)
	from mag.sales_flat_order_aud
	group by telesales_method_code
	order by telesales_method_code

	select telesales_admin_username, count(*)
	from mag.sales_flat_order_aud
	group by telesales_admin_username
	order by telesales_admin_username

		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			telesales_method_code, telesales_admin_username
		from mag.sales_flat_order_aud
		-- where telesales_method_code is not null
		where telesales_admin_username is not null


	select presc_verification_method, count(*)
	from mag.sales_flat_order_aud
	group by presc_verification_method
	order by presc_verification_method

		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			presc_verification_method, prescription_verification_type
		from mag.sales_flat_order_aud

		select top 1000 entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			postoptics_source, postoptics_auto_verification
		from mag.sales_flat_order_aud

	select postoptics_source, count(*)
	from mag.sales_flat_order_aud
	group by postoptics_source
	order by postoptics_source

	select postoptics_auto_verification, count(*)
	from mag.sales_flat_order_aud
	group by postoptics_auto_verification
	order by postoptics_auto_verification
		
			-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.sales_flat_order_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select * 
	from mag.sales_flat_order_aud_v
	where num_records > 1
	order by entity_id, idETLBatchRun
	-- order by num_records desc, entity_id, idETLBatchRun

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.sales_flat_order_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	---- state
	select entity_id, count(distinct state) num_dist
	from mag.sales_flat_order_aud_v
	group by entity_id
	having count(distinct state) > 1
	order by num_dist desc

	select o2.num_dist, o1.*
	from
			mag.sales_flat_order_aud_v o1
		inner join
			(select entity_id, count(distinct state) num_dist
			from mag.sales_flat_order_aud_v
			group by entity_id
			having count(distinct state) > 1) o2 on o1.entity_id = o2.entity_id
	order by o2.num_dist desc, o1.entity_id, o1.idETLBatchRun


	---- status
	select entity_id, count(distinct status) num_dist
	from mag.sales_flat_order_aud_v
	group by entity_id
	having count(distinct status) > 1
	order by num_dist desc

	select o2.num_dist, o1.*
	from
			mag.sales_flat_order_aud_v o1
		inner join
			(select entity_id, count(distinct status) num_dist
			from mag.sales_flat_order_aud_v
			group by entity_id
			having count(distinct status) > 1) o2 on o1.entity_id = o2.entity_id
	order by o2.num_dist desc, o1.entity_id, o1.idETLBatchRun

	---- total_qty_ordered
	select entity_id, count(distinct total_qty_ordered) num_dist
	from mag.sales_flat_order_aud_v
	group by entity_id
	having count(distinct total_qty_ordered) > 1
	order by num_dist desc



	---- base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		and base_subtotal_canceled is not null
		-- and base_subtotal_refunded is not null
	order by entity_id, record_type, ins_ts

	---- base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		-- and base_shipping_canceled is not null 
		and base_shipping_refunded is not null -- and base_shipping_refunded <> base_shipping_amount
	order by entity_id, record_type, ins_ts

	---- base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded 
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		-- and base_discount_canceled is not null 
		and base_discount_refunded is not null -- and base_discount_refunded <> base_discount_refunded
	order by entity_id, record_type, ins_ts

	---- base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		-- and base_customer_balance_amount <> 0
		and base_customer_balance_refunded is not null -- and base_customer_balance_refunded <> base_customer_balance_refunded
	order by entity_id, record_type, ins_ts
		
	---- base_grand_total, 
	---- base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		and base_total_refunded is not null
	order by entity_id, record_type, ins_ts
	
	-- ALL
	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	where num_records > 1
		and base_customer_balance_amount <> 0
		-- and base_shipping_amount is not null and  base_shipping_amount <> 0  
		-- and base_subtotal_refunded is not null
	order by entity_id, record_type, ins_ts

	-------------------------------------------------------------------------

	select top 1000 -- record_type, num_records,
		count(*) over (),
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_subtotal + base_shipping_amount + base_discount_amount - base_customer_balance_amount base_grand_calc,
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, 
		base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, 
		base_subtotal_refunded + base_shipping_refunded + base_discount_refunded - base_customer_balance_refunded base_total_refunded_calc,
		base_total_due,
		ins_ts
	from mag.sales_flat_order_aud -- _v
	-- where base_subtotal <> base_subtotal_invoiced or base_subtotal_invoiced is null
	-- where base_grand_total <> base_subtotal + base_shipping_amount + base_discount_amount - base_customer_balance_amount
	-- where base_total_refunded is not null and base_total_refunded <> ISNULL(base_total_offline_refunded, 0) + ISNULL(base_total_online_refunded, 0)
	-- where base_total_refunded is not null and base_total_refunded <> base_subtotal_refunded + base_shipping_refunded + base_discount_refunded - base_customer_balance_refunded
	where base_total_canceled is not null and base_total_canceled <> base_grand_total
	order by entity_id desc -- , record_type, ins_ts

	select status, state, count(*)
	from mag.sales_flat_order_aud_v
	where base_total_refunded is not null  
	group by status, state
	order by status, state

	select status, state, count(*)
	from mag.sales_flat_order_aud_v
	where base_total_canceled is not null  
	group by status, state
	order by status, state

------------------------------------------------------------------------
----------------------- sales_flat_order_item ----------------------------
------------------------------------------------------------------------

select top 100 item_id, order_id, store_id, created_at, updated_at,
	product_id, sku, name, description, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	weight, row_weight, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, 
	base_discount_amount, discount_percent, 
	base_amount_refunded,
	product_type, product_options, is_virtual, is_lens, lens_group_eye,
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_item_aud
order by item_id desc;

	select store_id, count(*)
	from mag.sales_flat_order_item_aud
	group by store_id
	order by store_id;

	select product_id, count(*)
	from mag.sales_flat_order_item_aud
	group by product_id
	order by product_id;

	select product_id, name, count(*)
	from mag.sales_flat_order_item_aud
	group by product_id, name
	order by product_id, name;

	select product_type, count(*)
	from mag.sales_flat_order_item_aud
	group by product_type
	order by product_type;

	select is_virtual, count(*)
	from mag.sales_flat_order_item_aud
	group by is_virtual
	order by is_virtual;

	select is_lens, count(*)
	from mag.sales_flat_order_item_aud
	group by is_lens
	order by is_lens;

	select lens_group_eye, count(*)
	from mag.sales_flat_order_item_aud
	group by lens_group_eye
	order by lens_group_eye;


	-- description
	select top 1000 *
	from mag.sales_flat_order_item_aud
	where description is not null
	
	-- qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 

	select qty_ordered, count(*)
	from mag.sales_flat_order_item_aud
	group by qty_ordered
	order by qty_ordered;

	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	-- where qty_canceled <> 0 and qty_ordered <> qty_canceled;
	-- where qty_invoiced <> 0 and qty_ordered <> qty_invoiced;
	-- where qty_refunded <> 0 and qty_ordered <> qty_refunded;
	where qty_shipped <> 0 and qty_ordered <> qty_shipped;
	-- where qty_backordered <> 0 
	-- where qty_returned <> 0 

	-- weight, row_weight, 
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		qty_ordered, weight, row_weight, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	where qty_ordered * weight <> row_weight
	order by order_id desc

	-- base_price, price, base_price_incl_tax, 
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		base_price, price, base_price_incl_tax, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	where base_price <> price or base_price <> base_price_incl_tax
	order by order_id desc

	-- base_cost, 
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		base_cost, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	order by order_id desc

	-- base_row_total, 
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		qty_ordered, base_price, base_price_incl_tax, base_row_total, qty_ordered * base_price base_row_total_calc, abs(base_row_total - (qty_ordered * base_price)) dif_calc,
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	where qty_ordered * base_price <> base_row_total
	order by dif_calc desc, order_id desc

	-- base_discount_amount, discount_percent, 
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		base_row_total, discount_percent, base_discount_amount, 
		base_row_total * discount_percent / 100 base_discount_amount_calc, abs(base_discount_amount - (base_row_total * discount_percent / 100)) dif_calc,
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	where base_discount_amount <> 0 
		and base_discount_amount <> base_row_total * discount_percent / 100
	order by dif_calc desc, order_id desc

	-- base_amount_refunded
	select top 1000 count(*) over (), item_id, order_id, store_id, created_at, updated_at,
		product_id, sku, name, 
		qty_ordered, qty_refunded,
		base_row_total, base_amount_refunded,
		qty_refunded * base_price base_amount_refunded_calc, abs(base_amount_refunded - (qty_refunded * base_price)) dif_calc,
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_order_item_aud
	where qty_refunded * base_price <> base_amount_refunded
	-- where (qty_refunded <> 0 and base_amount_refunded = 0) or (qty_refunded = 0 and base_amount_refunded <> 0)
	-- where qty_refunded <> 0 and (qty_ordered = qty_refunded) and (base_row_total <> base_amount_refunded)
	-- where qty_refunded <> 0 and (qty_ordered <> qty_refunded) and (base_row_total = base_amount_refunded)
	-- where qty_refunded <> 0 and (qty_ordered <> qty_refunded)
	order by dif_calc desc, order_id desc

	--------------------------------------------------------------------------

	select p.product_id, si.product_id, p.num
	from 
			(select product_id, count(*) num
			from mag.sales_flat_order_item_aud
			group by product_id) p
		left join 
			(select distinct product_id
			from mag.edi_stock_item) si on p.product_id = si.product_id
	where si.product_id is null
	order by p.product_id

	select p.num, oi.item_id, oi.order_id, oi.store_id, oi.created_at, oi.updated_at,
		oi.product_id, oi.sku, oi.name, 
		count(*) over (partition by oi.product_id) count_prod, 
		dense_rank() over (order by oi.product_id) rank_prod
	from
		(select p.product_id, p.num
		from 
				(select product_id, count(*) num
				from mag.sales_flat_order_item_aud
				group by product_id) p
			left join 
				(select distinct product_id
				from mag.edi_stock_item) si on p.product_id = si.product_id
		where si.product_id is null) p
	inner join
		mag.sales_flat_order_item_aud oi on p.product_id = oi.product_id
	order by p.product_id, oi.order_id

	select p.product_id, p.sku, sis.product_id, sis.sku, num
	from
			(select product_id, sku, count(*) num
			from mag.sales_flat_order_item_aud
			group by product_id, sku) p
		inner join 
			(select distinct product_id
			from mag.edi_stock_item) si on p.product_id = si.product_id
		left join
			(select distinct product_id, product_code sku
			from mag.edi_stock_item) sis on p.product_id = sis.product_id and p.sku = sis.sku
	where sis.sku is null
	order by p.product_id, p.sku;

	--------------------------------------------------------------------------
	------------------------- CARDINALITIES ----------------------------------

	select top 1000 
		oh.entity_id, oh.increment_id, oi.item_id, oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, 
		oh.state, oh.status, 
		oh.total_qty_ordered, oi.qty_ordered, oh.base_subtotal, oi.base_row_total, 
		oi.qty_canceled, oh.base_subtotal_canceled, 
		oi.qty_refunded, oh.base_subtotal_refunded, oi.base_amount_refunded
	from
			(select *
			from mag.sales_flat_order_aud) oh
		full join
			(select *
			from mag.sales_flat_order_item_aud) oi on oh.entity_id = oi.order_id
	where oh.entity_id is null or oi.order_id is null
	order by oh.entity_id desc, oi.item_id

	select top 1000 count(*) over () num_tot,
		count(*) over (partition by oh.entity_id) num_lines,
		oh.entity_id, oh.increment_id, oi.item_id, oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, 
		oh.state, oh.status, 
		oi.product_id, oi.name, 
		oh.total_qty_ordered, oi.qty_ordered, sum(oi.qty_ordered) over (partition by oh.entity_id) qty_ordered_sum, 
		oh.base_subtotal, oi.base_row_total, sum(oi.base_row_total) over (partition by oh.entity_id) base_row_total_sum,
		oi.qty_canceled, oh.base_subtotal_canceled, 
		oi.qty_refunded, oh.base_subtotal_refunded, oi.base_amount_refunded, sum(oi.base_amount_refunded) over (partition by oh.entity_id) base_amount_refunded_sum
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_refunded, 
				base_discount_amount, base_discount_refunded
			from mag.sales_flat_order_aud) oh
		inner join
			(select item_id, order_id, 
				product_id, sku, name, 
				qty_ordered, qty_canceled, qty_refunded, 
				base_price, base_row_total, 
				base_discount_amount, discount_percent, 
				base_amount_refunded
			from mag.sales_flat_order_item_aud) oi on oh.entity_id = oi.order_id
	-- where base_subtotal_canceled is not null 
	-- where base_subtotal_refunded is not null 
	order by oh.entity_id desc, oi.item_id

	select -- top 10000 
		count(*) over () num_tot, count(*) over (partition by entity_id) num_lines, *, 
		abs(base_subtotal - base_row_total_sum) dif_1, 
		abs(base_subtotal_refunded - base_amount_refunded_sum) dif_2
	from
		(select 
			oh.entity_id, oh.increment_id, oi.item_id, oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, 
			oh.state, oh.status, 
			oi.product_id, oi.name, 
			oh.total_qty_ordered, oi.qty_ordered, sum(oi.qty_ordered) over (partition by oh.entity_id) qty_ordered_sum, 
			oh.base_subtotal, oi.base_row_total, sum(oi.base_row_total) over (partition by oh.entity_id) base_row_total_sum,
			oi.qty_canceled, oh.base_subtotal_canceled, 
			oi.qty_refunded, oh.base_subtotal_refunded, oi.base_amount_refunded, sum(oi.base_amount_refunded) over (partition by oh.entity_id) base_amount_refunded_sum
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_subtotal, base_subtotal_canceled, base_subtotal_refunded, 
					base_discount_amount, base_discount_refunded
				from mag.sales_flat_order_aud) oh
			inner join
				(select item_id, order_id, 
					product_id, sku, name, 
					qty_ordered, qty_canceled, qty_refunded, 
					base_price, base_row_total, 
					base_discount_amount, discount_percent, 
					base_amount_refunded
				from mag.sales_flat_order_item_aud) oi on oh.entity_id = oi.order_id) t
	where total_qty_ordered <> qty_ordered_sum order by entity_id desc, item_id
	-- where base_subtotal <> base_row_total_sum and abs(base_subtotal - base_row_total_sum) > 1 order by dif_1 desc, entity_id desc, item_id
	-- where base_subtotal_refunded <> base_amount_refunded_sum and abs(base_subtotal_refunded - base_amount_refunded_sum) > 1 order by dif_2 desc, entity_id desc, item_id
	
