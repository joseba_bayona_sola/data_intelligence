use Landing
go

----------------------- review_entity ----------------------------

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts
	from Landing.mag.review_entity

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.review_entity_aud

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.review_entity_aud_hist


----------------------- review_status ----------------------------

	select status_id, status_code, 
		idETLBatchRun, ins_ts
	from Landing.mag.review_status

	select status_id, status_code, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.review_status_aud

	select status_id, status_code, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.review_status_aud_hist


----------------------- review ----------------------------

	select review_id, created_at, entity_id, status_id, entity_pk_value, 
		idETLBatchRun, ins_ts
	from Landing.mag.review

	select review_id, created_at, entity_id, status_id, entity_pk_value, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.review_aud

	select review_id, created_at, entity_id, status_id, entity_pk_value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.review_aud_hist

----------------------- review_store ----------------------------

	select review_id, store_id, 
		idETLBatchRun, ins_ts
	from Landing.mag.review_store

	select review_id, store_id, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.review_store_aud

	select review_id, store_id, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.review_store_aud_hist

----------------------- review_detail ----------------------------

	select detail_id, review_id, store_id, 
		title, detail, nickname, customer_id, 
		idETLBatchRun, ins_ts
	from Landing.mag.review_detail

	select detail_id, review_id, store_id, 
		title, detail, nickname, customer_id, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.review_detail_aud

	select detail_id, review_id, store_id, 
		title, detail, nickname, customer_id, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.review_detail_aud_hist





----------------------- rating_entity ----------------------------

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts
	from Landing.mag.rating_entity

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.rating_entity_aud

	select entity_id, entity_code, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.rating_entity_aud_hist


----------------------- rating ----------------------------
	
	select rating_id, entity_id, rating_code, position,
		idETLBatchRun, ins_ts
	from Landing.mag.rating

	select rating_id, entity_id, rating_code, position,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.rating_aud

	select rating_id, entity_id, rating_code, position,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.rating_aud_hist


----------------------- rating_option ----------------------------

	select option_id, rating_id, code, value, position, 
		idETLBatchRun, ins_ts
	from Landing.mag.rating_option

	select option_id, rating_id, code, value, position, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.rating_option_aud

	select option_id, rating_id, code, value, position, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.rating_option_aud_hist

	----------------------- rating_option_vote ----------------------------
	
	select vote_id, 
		review_id, remote_ip, customer_id, entity_pk_value, 
		rating_id, option_id, percent_rating, value, 
		idETLBatchRun, ins_ts
	from Landing.mag.rating_option_vote
	
	select vote_id, 
		review_id, remote_ip, customer_id, entity_pk_value, 
		rating_id, option_id, percent_rating, value, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.rating_option_vote_aud
	
	select vote_id, 
		review_id, remote_ip, customer_id, entity_pk_value, 
		rating_id, option_id, percent_rating, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.rating_option_vote_aud_hist
