use Landing
go 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_sales_flat_creditmemo
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	01-09-2017	Joseba Bayona Sola	Add mutual_amount attribute
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_creditmemo
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_creditmemo
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, 
				base_adjustment, base_adjustment_positive, base_adjustment_negative, 
				mutual_amount,
				base_to_global_rate, base_to_order_rate, order_currency_code,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, 
				base_adjustment, base_adjustment_positive, base_adjustment_negative, 
				mutual_amount,
				base_to_global_rate, base_to_order_rate, order_currency_code 
			from magento01.sales_flat_creditmemo  ' + -- ' + // '')'
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----

drop procedure mag.srcmag_lnd_get_sales_flat_creditmemo_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_creditmemo_item
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_creditmemo_item
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, order_item_id, parent_id,
				product_id, sku, name, description, 
				qty, 
				base_price, price, base_price_incl_tax, 
				base_cost, 
				base_row_total, base_discount_amount, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select cri.entity_id, cri.order_item_id, cri.parent_id,
				cri.product_id, cri.sku, cri.name, cri.description, 
				cri.qty, 
				cri.base_price, cri.price, cri.base_price_incl_tax, 
				cri.base_cost, 
				cri.base_row_total, cri.base_discount_amount
			from 
				magento01.sales_flat_creditmemo cr
			inner join
				magento01.sales_flat_creditmemo_item cri on cr.entity_id = cri.parent_id ' + -- ' + // '')'
			'where cr.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or cr.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 