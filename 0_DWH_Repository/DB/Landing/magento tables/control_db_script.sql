use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-LND' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'Magento Data' name, 'Taking Magento Source Data to Landing' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'eav_entity_type' name, 'Landing Table for eav_entity_type: EAV Tables Info' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'eav_attribute' name, 'Landing Table for eav_attribute: EAV Tables Info' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'eav_entity_attribute' name, 'Landing Table for eav_entity_attribute: EAV Tables Info' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'eav_attribute_option' name, 'Landing Table for eav_attribute_option: EAV Tables Info' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'eav_attribute_option_value' name, 'Landing Table for eav_attribute_option_value: EAV Tables Info' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity' name, 'Landing Table for customer_entity: EAV Entity: Customers' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_datetime' name, 'Landing Table for customer_entity_datetime: EAV Entity: Customers - Datetime Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_decimal' name, 'Landing Table for customer_entity_decimal: EAV Entity: Customers - Decimal Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_int' name, 'Landing Table for customer_entity_int: EAV Entity: Customers - Int Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_text' name, 'Landing Table for customer_entity_text: EAV Entity: Customers - Text Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_varchar' name, 'Landing Table for customer_entity_varchar: EAV Entity: Customers - Varchar Attr.' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity' name, 'Landing Table for customer_address_entity: EAV Entity: Customer Addresses' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_datetime' name, 'Landing Table for customer_address_entity_datetime: EAV Entity: Customer Addresses - Datetime Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_decimal' name, 'Landing Table for customer_address_entity_decimal: EAV Entity: Customer Addresses - Decimal Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_int' name, 'Landing Table for customer_address_entity_int: EAV Entity: Customer Addresses - Int Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_text' name, 'Landing Table for customer_address_entity_text: EAV Entity: Customer Addresses - Text Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_varchar' name, 'Landing Table for customer_address_entity_varchar: EAV Entity: Customer Addresses - Varchar Attr.' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity' name, 'Landing Table for catalog_category_entity: EAV Entity: Categories' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_datetime' name, 'Landing Table for catalog_category_entity_datetime: EAV Entity: Categories - Datetime Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_decimal' name, 'Landing Table for catalog_category_entity_decimal: EAV Entity: Categories - Decimal Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_int' name, 'Landing Table for catalog_category_entity_int: EAV Entity: Categories - Int Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_text' name, 'Landing Table for catalog_category_entity_text: EAV Entity: Categories - Text Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_varchar' name, 'Landing Table for catalog_category_entity_varchar: EAV Entity: Categories - Varchar Attr.' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity' name, 'Landing Table for catalog_product_entity: EAV Entity: Products' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_datetime' name, 'Landing Table for catalog_product_entity_datetime: EAV Entity: Products - Datetime Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_decimal' name, 'Landing Table for catalog_product_entity_decimal: EAV Entity: Products - Decimal Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_int' name, 'Landing Table for catalog_product_entity_int: EAV Entity: Products - Int Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_text' name, 'Landing Table for catalog_product_entity_text: EAV Entity: Products - Text Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_varchar' name, 'Landing Table for catalog_product_entity_varchar: EAV Entity: Products - Varchar Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_tier_price' name, 'Landing Table for catalog_product_entity_tier_price: EAV Entity: Products - Tier Price Attr.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_product' name, 'Landing Table for catalog_category_product: EAV Entity: Products - Category Rel.' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_website' name, 'Landing Table for catalog_product_website: EAV Entity: Products - Website Rel.' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_order' name, 'Landing Table for sales_flat_order: Flat Table: Order Headers data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_order_item' name, 'Landing Table for sales_flat_order_item: Flat Table: Order Items data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_order_payment' name, 'Landing Table for sales_flat_order_payment: Flat Table: Order Payments data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_order_address' name, 'Landing Table for sales_flat_order_address: Flat Table: Order Address data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_order_log' name, 'Landing Table for sales_order_log: Flat Table: Order Log Status data' description
			union 

			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_invoice' name, 'Landing Table for sales_flat_invoice: Flat Table: Invoice Headers data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_invoice_item' name, 'Landing Table for sales_flat_invoice_item: Flat Table: Invoice Items data' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_shipment' name, 'Landing Table for sales_flat_shipment: Flat Table: Shipment Headers data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_shipment_item' name, 'Landing Table for sales_flat_shipment_item: Flat Table: Shipment Items data' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_creditmemo' name, 'Landing Table for sales_flat_creditmemo: Flat Table: Creditmemo Headers data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'sales_flat_creditmemo_item' name, 'Landing Table for sales_flat_creditmemo_item: Flat Table: Creditmemo Items data' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'core_store' name, 'Landing Table for core_store: Magento Stores data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'core_website' name, 'Landing Table for core_website: Magento Websites data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'core_config_data' name, 'Landing Table for core_config_data: Magento Configuration data' description
			union
			
			select 'Landing' database_name, 'Magento Data' flow_name, 'edi_manufacturer' name, 'Landing Table for edi_manufacturer: Manufacturers data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'edi_supplier_account' name, 'Landing Table for edi_supplier_account: Suppliers data' description
			union 
			select 'Landing' database_name, 'Magento Data' flow_name, 'edi_packsize_pref' name, 'Landing Table for edi_packsize_pref: Product Packsizes data data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'edi_stock_item' name, 'Landing Table for edi_stock_item: Product Stock Item Parameters data' description
			union 

			select 'Landing' database_name, 'Magento Data' flow_name, 'directory_currency_rate' name, 'Landing Table for directory_currency_rate: Currency Rate data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'salesrule' name, 'Landing Table for salesrule: Coupon Codes data' description
			union 
			select 'Landing' database_name, 'Magento Data' flow_name, 'salesrule_coupon' name, 'Landing Table for salesrule_coupon: Coupon Codes data' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'po_reorder_profile' name, 'Landing Table for po_reorder_profile: Reordering Profiles data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'po_reorder_quote_payment' name, 'Landing Table for po_reorder_quote_payment: Reordering Quotes Paym. data' description
			union 
			select 'Landing' database_name, 'Magento Data' flow_name, 'admin_user' name, 'Landing Table for admin_user: Magento Users data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'log_customer' name, 'Landing Table for log_customer: Magento Customer Logging data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'po_sales_pla_item' name, 'Landing Table for po_sales_pla_item: Magento Customer PLA rows' description

			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'enterprise_customerbalance' name, 'Landing Table for enterprise_customerbalance: Magento Customer Store Credit Balance rows' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'enterprise_customerbalance_history' name, 'Landing Table for enterprise_customerbalance_history: Magento Customer Store Credit Balance History rows' description

			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'cybersourcestored_token' name, 'Landing Table for cybersourcestored_token: Magento CC Stored Token rows' description

			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'ga_entity_transaction_data' name, 'Landing Table for ga_entity_transaction_data: GA Transaction rows' description
			
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mutual' name, 'Landing Table for mutual: French Mutuelle List rows' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mutual_quotation' name, 'Landing Table for mutual_quotation: Orders done through Mutuelles rows' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 


	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'review_entity' name, 'Landing Table for review_entity: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'review_status' name, 'Landing Table for review_status: Review Data' description
			union			
			select 'Landing' database_name, 'Magento Data' flow_name, 'review' name, 'Landing Table for review: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'review_store' name, 'Landing Table for review_store: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'review_detail' name, 'Landing Table for review_detail: Review Data' description
			union

			select 'Landing' database_name, 'Magento Data' flow_name, 'rating_entity' name, 'Landing Table for rating_entity: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'rating' name, 'Landing Table for rating: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'rating_option' name, 'Landing Table for rating_option: Review Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'rating_option_vote' name, 'Landing Table for rating_option: Review Data' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 


-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'jsola' developer,  'Magento Data' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_entity_tables' name, 'SSIS PKG for reading Entity Tables data from Magento to Landing' description
			union
			select 'SRC-LND' codPackageType, 'jsola' developer,  'Magento Data' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_flat_o_i_s_cr_tables' name, 'SSIS PKG for reading Flat Tables data (Order - Invoice - Shipment - Creditmemo) from Magento to Landing' description
			union
			select 'SRC-LND' codPackageType, 'jsola' developer,  'Magento Data' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_other_tables' name, 'SSIS PKG for reading other Tables data from Magento to Landing' description
			union
			select 'SRC-LND' codPackageType, 'jcano' developer,  'Magento Data' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_extra_tables ' name, 'SSIS PKG for reading Review - Rating data from Magento to Landing' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_eav_entity_type' name, 'GET SP for reading eav_entity_type data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_eav_attribute' name, 'GET SP for reading eav_attribute data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_eav_entity_attribute' name, 'GET SP for reading eav_entity_attribute data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_eav_attribute_option' name, 'GET SP for reading eav_attribute_option data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_eav_attribute_option_value' name, 'GET SP for reading eav_attribute_option_value data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity' name, 'GET SP for reading customer_entity data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_datetime' name, 'GET SP for reading customer_entity_datetime data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_decimal' name, 'GET SP for reading customer_entity_decimal data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_int' name, 'GET SP for reading customer_entity_int data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_text' name, 'GET SP for reading customer_entity_text data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_varchar' name, 'GET SP for reading customer_entity_varchar data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity' name, 'GET SP for reading customer_address_entity data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_datetime' name, 'GET SP for reading customer_address_entity_datetime data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_decimal' name, 'GET SP for reading customer_address_entity_decimal data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_int' name, 'GET SP for reading customer_address_entity_int data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_text' name, 'GET SP for reading customer_address_entity_text data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_varchar' name, 'GET SP for reading customer_address_entity_varchar data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity' name, 'GET SP for reading catalog_category_entity data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_datetime' name, 'GET SP for reading catalog_category_entity_datetime data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_decimal' name, 'GET SP for reading catalog_category_entity_decimal data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_int' name, 'GET SP for reading catalog_category_entity_int data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_text' name, 'GET SP for reading catalog_category_entity_text data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_varchar' name, 'GET SP for reading catalog_category_entity_varchar data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity' name, 'GET SP for reading catalog_product_entity data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_datetime' name, 'GET SP for reading catalog_product_entity_datetime data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_decimal' name, 'GET SP for reading catalog_product_entity_decimal data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_int' name, 'GET SP for reading catalog_product_entity_int data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_text' name, 'GET SP for reading catalog_product_entity_text data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_varchar' name, 'GET SP for reading catalog_product_entity_varchar data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_tier_price' name, 'GET SP for reading catalog_product_entity_tier_price data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_product' name, 'GET SP for reading catalog_category_product data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_website' name, 'GET SP for reading catalog_product_website data from Magento to Landing' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 

insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_order' name, 'GET SP for reading sales_flat_order data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_order_item' name, 'GET SP for reading sales_flat_order_item data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_order_payment' name, 'GET SP for reading sales_flat_order_payment data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_order_address' name, 'GET SP for reading sales_flat_order_address data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_order_log' name, 'GET SP for reading sales_order_log data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_invoice' name, 'GET SP for reading sales_flat_invoice data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_invoice_item' name, 'GET SP for reading sales_flat_invoice_item data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_shipment' name, 'GET SP for reading sales_flat_shipment data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_shipment_item' name, 'GET SP for reading sales_flat_shipment_item data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_creditmemo' name, 'GET SP for reading sales_flat_creditmemo data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_sales_flat_creditmemo_item' name, 'GET SP for reading sales_flat_creditmemo_item data from Magento to Landing' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 

insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_core_store' name, 'GET SP for reading core_store data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_core_website' name, 'GET SP for reading core_website data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_core_config_data' name, 'GET SP for reading core_config_data data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_edi_manufacturer' name, 'GET SP for reading edi_manufacturer data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_edi_supplier_account' name, 'GET SP for reading edi_supplier_account data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_edi_packsize_pref' name, 'GET SP for reading edi_packsize_pref data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_edi_stock_item' name, 'GET SP for reading edi_stock_item data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_directory_currency_rate' name, 'GET SP for reading directory_currency_rate data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_salesrule' name, 'GET SP for reading salesrule data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_salesrule_coupon' name, 'GET SP for reading salesrule_coupon data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_po_reorder_profile' name, 'GET SP for reading po_reorder_profile data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_po_reorder_quote_payment' name, 'GET SP for reading po_reorder_quote_payment data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_admin_user' name, 'GET SP for reading admin_user data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_log_customer' name, 'GET SP for reading log_customer data from Magento to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_po_sales_pla_item' name, 'GET SP for reading po_sales_pla_item data from Magento to Landing' description

			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_enterprise_customerbalance' name, 'GET SP for reading enterprise_customerbalance data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_enterprise_customerbalance_history' name, 'GET SP for reading enterprise_customerbalance_history data from Magento to Landing' description

			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_cybersourcestored_token' name, 'GET SP for reading cybersourcestored_token data from Magento to Landing' description

			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_ga_entity_transaction_data' name, 'GET SP for reading ga_entity_transaction_data data from Magento to Landing' description

			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_mutual' name, 'GET SP for reading mutual data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_mutual_quotation' name, 'GET SP for reading mutual_quotation data from Magento to Landing' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 


insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_review_entity' name, 'GET SP for reading data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_review_status' name, 'GET SP for reading data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_review' name, 'GET SP for reading data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_review_store' name, 'GET SP for reading data from Magento to Landing' description		
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_review_detail' name, 'GET SP for reading data from Magento to Landing' description
			union

			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_rating_entity' name, 'GET SP for reading data from Magento to Landing' description						
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_rating' name, 'GET SP for reading data from Magento to Landing' description						
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_rating_option' name, 'GET SP for reading data from Magento to Landing' description			
			union
			select 'GET' codSPType, 'jcano' developer,  'srcmag_lnd_extra_tables ' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_rating_option_vote' name, 'GET SP for reading data from Magento to Landing' description			
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 

--------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'customer_entity_flat' name, 'Flat Table for customer_entity: EAV Entity Flattened: Customers' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'customer_address_entity_flat' name, 'Flat Table for customer_address_entity: EAV Entity Flattened: Customer Addresses' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_category_entity_flat' name, 'Flat Table for catalog_category_entity: EAV Entity Flattened: Categories' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'catalog_product_entity_flat' name, 'Flat Table for catalog_product_entity: EAV Entity Flattened: Products' description

			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'jsola' developer,  'Magento Data' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_entity_tables_flat' name, 'SSIS PKG for flattening Entity Tables data from Magento to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_entity_flat' name, 'GET SP for reading customer_entity data from Landing in flatten way' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_customer_address_entity_flat' name, 'GET SP for reading customer_address_entity data from Landing in flatten way' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_category_entity_flat' name, 'GET SP for reading catalog_category_entity data from Landing in flatten way' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_catalog_product_entity_flat' name, 'GET SP for reading catalog_product_entity data from Landing in flatten way' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 