------------------------------- Triggers ----------------------------------

use Landing
go


------------------------- review_entity ----------------------------

	drop trigger mag.trg_review_entity;
	go

	create trigger mag.trg_review_entity on mag.review_entity
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.review_entity_aud with (tablock) as trg 
		using inserted src 
			on (trg.entity_id = src.entity_id)
		when matched and not exists 
			(select isnull(trg.entity_code, '')
			intersect
			select isnull(src.entity_code, ''))
		
			then 
				update set
					trg.entity_code = src.entity_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (entity_id, entity_code, idETLBatchRun)
					values (src.entity_id, src.entity_code, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_rewiew_entity_aud;		
	go

	create trigger mag.trg_rewiew_entity_aud on mag.review_entity_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.review_entity_aud_hist (entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.entity_id, d.entity_code, 
				d.idETLBatchRun, 
			   case when (d.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.entity_id = i.entity_id;
	end;
	go	
	
------------------------- review_status ----------------------------

	drop trigger mag.trg_review_status;
	go

	create trigger mag.trg_review_status on mag.review_status
	after insert
	as
	begin
	
		set nocount on
	
		merge into mag.review_status_aud with (tablock) as trg
		using inserted src
			on trg.status_id = src.status_id
		when matched and not exists
			(select isnull(trg.status_code, '')
			intersect
			select isnull(src.status_code, ''))
	
			then 
				update set
					trg.status_code = src.status_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.ins_ts = getutcdate()
	
		when not matched 
		then
			insert (status_id, status_code, idETLBatchRun) 
				values (src.status_id, src.status_code, src.idETLBatchRun);
	
	end;
	go



	drop trigger mag.trg_review_status_aud;
	go

	create trigger mag.trg_review_status_aud on mag.review_status_aud
	for update, delete
	as 
	begin

		set nocount on
	
		insert into mag.review_status_aud_hist (status_id, status_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)

			select d.status_id, d.status_code, 
				d.idETLBatchRun, 
				case when (d.status_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d 
				left join
					inserted i on d.status_id = i.status_id;
		
	end;
	go		




	drop trigger mag.trg_review;
	go 

	create trigger mag.trg_review on mag.review
	after insert
	as
	begin
		set nocount on

		merge into mag.review_aud with (tablock) as trg
		using inserted src
			on (trg.review_id = src.review_id)
		when matched and not exists
			(select isnull(trg.created_at, ' '), isnull(trg.entity_id, 0), isnull(trg.status_id, 0), isnull(trg.entity_pk_value, 0)
			intersect
			select isnull(src.created_at, ' '), isnull(src.entity_id, 0), isnull(src.status_id, 0), isnull(src.entity_pk_value, 0))
			then
				update set
					trg.created_at = src.created_at, trg.entity_id = src.entity_id, trg.status_id = src.status_id, trg.entity_pk_value = src.entity_pk_value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		
		when not matched 
			then
				insert (review_id, created_at, entity_id, status_id, entity_pk_value, idETLBatchRun)
					values (src.review_id, src.created_at, src.status_id, src.entity_id, src.entity_pk_value, src.idETLBatchRun);
	end; 
	go

------------------------- review ----------------------------


	drop trigger mag.trg_review_aud;
	go 

	create trigger mag.trg_review_aud on mag.review_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.review_aud_hist (review_id, created_at, entity_id, status_id, entity_pk_value, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
					
		select d.review_id, d.created_at, d.entity_id, d.status_id, d.entity_pk_value, 
			d.idETLBatchRun, case when (d.review_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
				deleted d
			left join 
				inserted i on d.review_id = i.review_id;
	end;
	go

------------------------- review_store ----------------------------

	drop trigger mag.trg_review_store;
	go

	create trigger mag.trg_review_store on mag.review_store
	after insert 
	as 
	begin

		set nocount on
	
		merge into mag.review_store_aud with (tablock) as trg
		using inserted src
			on (trg.review_id = src.review_id and trg.store_id = src.store_id)

		when not matched then
			insert (review_id, store_id, idETLBatchRun)
				values (src.review_id, src.store_id, src.idETLBatchRun);
	end;
	go


------------------------- review_detail ----------------------------


	drop trigger mag.trg_review_detail;
	go 

	create trigger mag.trg_review_detail on mag.review_detail
	after insert
	as
	begin
		set nocount on

		merge into mag.review_detail_aud with (tablock) as trg
		using inserted src
			on (trg.detail_id = src.detail_id)
		when matched and not exists
			(select isnull(trg.review_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.title, ' '), isnull(trg.detail, ' '), isnull(trg.nickname, ' '), isnull(trg.customer_id, 0)
			intersect
			select isnull(src.review_id, 0), isnull(src.store_id, 0), 
				isnull(src.title, ' '), isnull(src.detail, ' '), isnull(src.nickname, ' '), isnull(src.customer_id, 0))
			then
				update set
					trg.review_id = src.review_id, trg.store_id = src.store_id, 
					trg.title = src.title, trg.detail = src.detail, trg.nickname = src.nickname, trg.customer_id = src.customer_id,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (detail_id, review_id, store_id, 
					title, detail, nickname, customer_id, idETLBatchRun)

					values (src.detail_id, src.review_id, src.store_id, 
						src.title, src.detail, src.nickname, src.customer_id, src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_review_detail_aud;
	go 

	create trigger mag.trg_review_detail_aud on mag.review_detail_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.review_detail_aud_hist(detail_id, review_id, store_id, 
			title, detail, nickname, customer_id,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
			select d.detail_id, d.review_id, d.store_id, 
				d.title, d.detail, d.nickname, d.customer_id, 
				d.idETLBatchRun, 
				case when (d.review_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i 
			on d.review_id = i.review_id;
	end;
	go





------------------------- rating_entity ----------------------------

	drop trigger mag.trg_rating_entity;
	go 

	create trigger mag.trg_rating_entity on mag.rating_entity
	after insert 
	as
	begin

		set nocount on

		merge into mag.rating_entity_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.entity_code, ' ')
			 intersect
			 select isnull(trg.entity_code, ' '))

		then update set
			trg.entity_code = src.entity_code,
			trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (entity_id, entity_code, idETLBatchRun)
				values (src.entity_id, src.entity_code, src.idETLBatchRun);

	end;
	go


	drop trigger mag.trg_rating_entity_aud;
	go 

	create trigger mag.trg_rating_entity_aud on mag.rating_entity_aud
	for update, delete
	as
	begin

		set nocount on

		insert into mag.rating_entity_aud_hist (entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.entity_code, 
				d.idETLBatchRun, case when (d.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.entity_id = i.entity_id
	end;
	go


------------------------- rating ----------------------------

	drop trigger mag.trg_rating;
	go 

	create trigger mag.trg_rating on mag.rating
	after insert
	as 
	begin
	
		set nocount on

		merge into mag.rating_aud with (tablock) as trg
		using inserted src
			on (trg.rating_id = src.rating_id)
		when matched and not exists
			(select isnull(trg.entity_id, 0), isnull(trg.rating_code, ' '), isnull(trg.position, 0)
			 intersect
			 select isnull(src.entity_id, 0), isnull(src.rating_code, ' '), isnull(src.position, 0))

				then 
					update set
						trg.entity_id = src.entity_id, trg.rating_code = src.rating_code, trg.position = src.position,
						trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (rating_id, entity_id, rating_code, position, idETLBatchRun)
				values (src.rating_id, src.entity_id, src.rating_code, src.position, src.idETLBatchRun);

	end;
	go



	drop trigger mag.trg_rating_aud;
	go 

	create trigger mag.trg_rating_aud on mag.rating_aud
	for delete, update
	as
	begin

		set nocount on

		insert into mag.rating_aud_hist (rating_id, entity_id, rating_code, position, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
		
			select  d.rating_id, d.entity_id, d.rating_code, d.position, 
				d.idETLBatchRun, case when (d.rating_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d 
				left join
					inserted i on  d.rating_id = i.rating_id
	end;
	go


------------------------- rating_option ----------------------------

	drop trigger mag.trg_rating_option;
	go 

	create trigger mag.trg_rating_option on mag.rating_option
	after insert
	as
	begin

		set nocount on

		merge into mag.rating_option_aud with (tablock) as trg
		using inserted src
			on (trg.option_id = src.option_id)
		when matched and not exists
			(select isnull(trg.rating_id, 0), isnull(trg.code, ' '), isnull(trg.value, 0), isnull(trg.position, 0)
			intersect
			select isnull(src.rating_id, 0), isnull(src.code, ' '), isnull(src.value, 0), isnull(src.position, 0))
			
		then 
			update set
				trg.rating_id = src.rating_id, trg.code = src.code, trg.value = src.value, trg.position = src.position,
				trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (option_id, rating_id, code, value, position, idETLBatchRun)
				values (src.option_id, src.rating_id, src.code, src.value, src.position, src.idETLBatchRun);
	end;
	go
			

	drop trigger mag.trg_rating_option_aud;
	go 

	create trigger mag.trg_rating_option_aud on mag.rating_option_aud		
	for update, delete
	as
	begin

		set nocount on 

		insert into mag.rating_option_aud_hist (option_id, rating_id, code, value, position, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)

		select d.option_id, d.rating_id, d.code, d.value, d.position, 
			d.idETLBatchRun, 
			case when (d.option_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
		from
				deleted d	
			left join
				inserted i on d.option_id = i.option_id;
	end;
	go


------------------------- rating_option_vote ----------------------------

	drop trigger mag.trg_rating_option_vote;
	go 

	create trigger mag.trg_rating_option_vote on mag.rating_option_vote
	after insert 
	as
	begin
	
		set nocount on 
		
		merge into mag.rating_option_vote_aud with (tablock) as trg
		using inserted src 
			on trg.vote_id = src.vote_id
		when matched and not exists
			(select
				isnull(trg.review_id, 0), isnull(trg.remote_ip, ' '), isnull(trg.customer_id, 0), isnull(trg.entity_pk_value, 0),
				isnull(trg.rating_id, 0), isnull(trg.option_id, 0), isnull(trg.percent_rating, 0), isnull(trg.value, 0)
			intersect
			select 
				isnull(src.review_id, 0), isnull(src.remote_ip, ' '), isnull(src.customer_id, 0), isnull(src.entity_pk_value, 0),
				isnull(src.rating_id, 0), isnull(src.option_id, 0), isnull(src.percent_rating, 0), isnull(src.value, 0)) 
			
			then 
				update set
					trg.review_id = src.review_id, trg.remote_ip = src.remote_ip, trg.customer_id = src.customer_id, trg.entity_pk_value = src.entity_pk_value, 
					trg.rating_id = src.rating_id, trg.option_id = src.option_id, trg.percent_rating = src.percent_rating, trg.value = src.value,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched then		
		insert
			(vote_id, review_id, remote_ip, customer_id, entity_pk_value, 
				rating_id, option_id, percent_rating, value, idETLBatchRun)
		 values
			(src.vote_id, src.review_id, src.remote_ip, src.customer_id, src.entity_pk_value, 
				src.rating_id, src.option_id, src.percent_rating, src.value, src.idETLBatchRun);
			 
	end;
	go
	
	drop trigger mag.trg_rating_option_vote_aud;
	go 

	create trigger mag.trg_rating_option_vote_aud on mag.rating_option_vote_aud
	after insert
	as
	begin
	
		set nocount on
		
		insert into mag.rating_option_vote_aud_hist (vote_id, review_id, remote_ip, customer_id, entity_pk_value, 
			rating_id, option_id, percent_rating, value,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
		
			select d.vote_id, d.review_id, d.remote_ip, d.customer_id, d.entity_pk_value, 
				d.rating_id, d.option_id, d.percent_rating, d.value,
				d.idETLBatchRun, case when (d.vote_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.vote_id = i.vote_id;

	end;
	go	



