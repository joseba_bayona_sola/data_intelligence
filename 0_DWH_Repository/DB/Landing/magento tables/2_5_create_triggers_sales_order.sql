
use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- sales_flat_order ----------------------------

	drop trigger mag.trg_sales_flat_order;
	go 

	create trigger mag.trg_sales_flat_order on mag.sales_flat_order
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_order_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.increment_id, ''),  isnull(trg.store_id, 0), isnull(trg.customer_id, 0), isnull(trg.created_at, ''), -- isnull(trg.updated_at, ''), 
				isnull(trg.shipping_address_id, 0), isnull(trg.billing_address_id, 0), 
				isnull(trg.state, ''), isnull(trg.status, ''), 
				isnull(trg.total_qty_ordered, 0), 
				isnull(trg.base_subtotal, 0), isnull(trg.base_subtotal_canceled, 0), isnull(trg.base_subtotal_invoiced, 0), isnull(trg.base_subtotal_refunded, 0), 
				isnull(trg.base_shipping_amount, 0), isnull(trg.base_shipping_canceled, 0), isnull(trg.base_shipping_invoiced, 0), isnull(trg.base_shipping_refunded, 0), 
				isnull(trg.base_discount_amount, 0), isnull(trg.base_discount_canceled, 0), isnull(trg.base_discount_invoiced, 0), isnull(trg.base_discount_refunded, 0), 
				isnull(trg.base_customer_balance_amount, 0), isnull(trg.base_customer_balance_invoiced, 0), isnull(trg.base_customer_balance_refunded, 0),
				isnull(trg.base_grand_total, 0), 
				isnull(trg.base_total_canceled, 0), isnull(trg.base_total_invoiced, 0), isnull(trg.base_total_invoiced_cost, 0), 
				isnull(trg.base_total_refunded, 0), isnull(trg.base_total_offline_refunded, 0), isnull(trg.base_total_online_refunded, 0), isnull(trg.bs_customer_bal_total_refunded, 0), 
				isnull(trg.base_total_due, 0),
				isnull(trg.base_to_global_rate, 0), isnull(trg.base_to_order_rate, 0), isnull(trg.order_currency_code, ''),
				isnull(trg.customer_dob, ''), isnull(trg.customer_email, ''), isnull(trg.customer_firstname, ''), isnull(trg.customer_lastname, ''), 
				isnull(trg.customer_middlename, ''), isnull(trg.customer_prefix, ''), isnull(trg.customer_suffix, ''), 
				isnull(trg.customer_taxvat, ''), isnull(trg.customer_gender, 0), isnull(trg.customer_note, ''), isnull(trg.customer_note_notify, 0), 
				isnull(trg.shipping_description, ''), 
				isnull(trg.coupon_code, ''), isnull(trg.applied_rule_ids, ''), 
				isnull(trg.affilBatch, ''), isnull(trg.affilCode, ''), isnull(trg.affilUserRef, ''), 
				isnull(trg.reminder_date, ''), isnull(trg.reminder_mobile, ''), isnull(trg.reminder_period, ''), isnull(trg.reminder_presc, ''), 
				isnull(trg.reminder_type, ''), isnull(trg.reminder_follow_sent, 0), isnull(trg.reminder_sent, 0), 
				isnull(trg.reorder_on_flag, 0), isnull(trg.reorder_profile_id, 0), isnull(trg.automatic_reorder, 0), 
				isnull(trg.postoptics_source, ''), isnull(trg.postoptics_auto_verification, ''), 
				isnull(trg.presc_verification_method, ''), isnull(trg.prescription_verification_type, ''), 
				isnull(trg.referafriend_code, ''), isnull(trg.referafriend_referer, 0), 
				isnull(trg.telesales_method_code, ''), isnull(trg.telesales_admin_username, ''), 
				isnull(trg.remote_ip, ''), isnull(trg.warehouse_approved_time, ''), isnull(trg.partbill_shippingcost_cost, 0), 
				isnull(trg.old_order_id, 0), 
				isnull(trg.mutual_amount, 0), isnull(trg.mutual_quotation_id, 0)
			intersect
			select isnull(src.increment_id, ''), isnull(src.store_id, 0), isnull(src.customer_id, 0), isnull(src.created_at, ''), -- isnull(src.updated_at, ''), 
				isnull(src.shipping_address_id, 0), isnull(src.billing_address_id, 0), 
				isnull(src.state, ''), isnull(src.status, ''), 
				isnull(src.total_qty_ordered, 0), 
				isnull(src.base_subtotal, 0), isnull(src.base_subtotal_canceled, 0), isnull(src.base_subtotal_invoiced, 0), isnull(src.base_subtotal_refunded, 0), 
				isnull(src.base_shipping_amount, 0), isnull(src.base_shipping_canceled, 0), isnull(src.base_shipping_invoiced, 0), isnull(src.base_shipping_refunded, 0), 
				isnull(src.base_discount_amount, 0), isnull(src.base_discount_canceled, 0), isnull(src.base_discount_invoiced, 0), isnull(src.base_discount_refunded, 0), 
				isnull(src.base_customer_balance_amount, 0), isnull(src.base_customer_balance_invoiced, 0), isnull(src.base_customer_balance_refunded, 0),
				isnull(src.base_grand_total, 0), 
				isnull(src.base_total_canceled, 0), isnull(src.base_total_invoiced, 0), isnull(src.base_total_invoiced_cost, 0), 
				isnull(src.base_total_refunded, 0), isnull(src.base_total_offline_refunded, 0), isnull(src.base_total_online_refunded, 0), isnull(src.bs_customer_bal_total_refunded, 0), 
				isnull(src.base_total_due, 0),
				isnull(src.base_to_global_rate, 0), isnull(src.base_to_order_rate, 0), isnull(src.order_currency_code, ''),
				isnull(src.customer_dob, ''), isnull(src.customer_email, ''), isnull(src.customer_firstname, ''), isnull(src.customer_lastname, ''), 
				isnull(src.customer_middlename, ''), isnull(src.customer_prefix, ''), isnull(src.customer_suffix, ''), 
				isnull(src.customer_taxvat, ''), isnull(src.customer_gender, 0), isnull(src.customer_note, ''), isnull(src.customer_note_notify, 0), 
				isnull(src.shipping_description, ''), 
				isnull(src.coupon_code, ''), isnull(src.applied_rule_ids, ''), 
				isnull(src.affilBatch, ''), isnull(src.affilCode, ''), isnull(src.affilUserRef, ''), 
				isnull(src.reminder_date, ''), isnull(src.reminder_mobile, ''), isnull(src.reminder_period, ''), isnull(src.reminder_presc, ''), 
				isnull(src.reminder_type, ''), isnull(src.reminder_follow_sent, 0), isnull(src.reminder_sent, 0), 
				isnull(src.reorder_on_flag, 0), isnull(src.reorder_profile_id, 0), isnull(src.automatic_reorder, 0), 
				isnull(src.postoptics_source, ''), isnull(src.postoptics_auto_verification, ''), 
				isnull(src.presc_verification_method, ''), isnull(src.prescription_verification_type, ''), 
				isnull(src.referafriend_code, ''), isnull(src.referafriend_referer, 0), 
				isnull(src.telesales_method_code, ''), isnull(src.telesales_admin_username, ''), 
				isnull(src.remote_ip, ''), isnull(src.warehouse_approved_time, ''), isnull(src.partbill_shippingcost_cost, 0), 
				isnull(src.old_order_id, 0), 
				isnull(src.mutual_amount, 0), isnull(src.mutual_quotation_id, 0))
			
			then
				update set
					trg.increment_id = src.increment_id, trg.store_id = src.store_id, trg.customer_id = src.customer_id, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.shipping_address_id = src.shipping_address_id, trg.billing_address_id = src.billing_address_id, 
					trg.state = src.state, trg.status = src.status, 
					trg.total_qty_ordered = src.total_qty_ordered, 
					trg.base_subtotal = src.base_subtotal, trg.base_subtotal_canceled = src.base_subtotal_canceled, trg.base_subtotal_invoiced = src.base_subtotal_invoiced, trg.base_subtotal_refunded = src.base_subtotal_refunded, 
					trg.base_shipping_amount = src.base_shipping_amount, trg.base_shipping_canceled = src.base_shipping_canceled, trg.base_shipping_invoiced = src.base_shipping_invoiced, trg.base_shipping_refunded = src.base_shipping_refunded, 
					trg.base_discount_amount = src.base_discount_amount, trg.base_discount_canceled = src.base_discount_canceled, trg.base_discount_invoiced = src.base_discount_invoiced, trg.base_discount_refunded = src.base_discount_refunded, 
					trg.base_customer_balance_amount = src.base_customer_balance_amount, trg.base_customer_balance_invoiced = src.base_customer_balance_invoiced, trg.base_customer_balance_refunded = src.base_customer_balance_refunded,
					trg.base_grand_total = src.base_grand_total, 
					trg.base_total_canceled = src.base_total_canceled, trg.base_total_invoiced = src.base_total_invoiced, trg.base_total_invoiced_cost = src.base_total_invoiced_cost, 
					trg.base_total_refunded = src.base_total_refunded, trg.base_total_offline_refunded = src.base_total_offline_refunded, trg.base_total_online_refunded = src.base_total_online_refunded, 
					trg.bs_customer_bal_total_refunded = src.bs_customer_bal_total_refunded, trg.base_total_due = src.base_total_due,
					trg.base_to_global_rate = src.base_to_global_rate, trg.base_to_order_rate = src.base_to_order_rate, trg.order_currency_code = src.order_currency_code,
					trg.customer_dob = src.customer_dob, trg.customer_email = src.customer_email, trg.customer_firstname = src.customer_firstname, trg.customer_lastname = src.customer_lastname, 
					trg.customer_middlename = src.customer_middlename, trg.customer_prefix = src.customer_prefix, trg.customer_suffix = src.customer_suffix, 
					trg.customer_taxvat = src.customer_taxvat, trg.customer_gender = src.customer_gender, trg.customer_note = src.customer_note, trg.customer_note_notify = src.customer_note_notify, 
					trg.shipping_description = src.shipping_description, 
					trg.coupon_code = src.coupon_code, trg.applied_rule_ids = src.applied_rule_ids, 
					trg.affilBatch = src.affilBatch, trg.affilCode = src.affilCode, trg.affilUserRef = src.affilUserRef, 
					trg.reminder_date = src.reminder_date, trg.reminder_mobile = src.reminder_mobile, trg.reminder_period = src.reminder_period, trg.reminder_presc = src.reminder_presc, 
					trg.reminder_type = src.reminder_type, trg.reminder_follow_sent = src.reminder_follow_sent, trg.reminder_sent = src.reminder_sent, 
					trg.reorder_on_flag = src.reorder_on_flag, trg.reorder_profile_id = src.reorder_profile_id, trg.automatic_reorder = src.automatic_reorder, 
					trg.postoptics_source = src.postoptics_source, trg.postoptics_auto_verification = src.postoptics_auto_verification, 
					trg.presc_verification_method = src.presc_verification_method, trg.prescription_verification_type = src.prescription_verification_type, 
					trg.referafriend_code = src.referafriend_code, trg.referafriend_referer = src.referafriend_referer, 
					trg.telesales_method_code = src.telesales_method_code, trg.telesales_admin_username = src.telesales_admin_username, 
					trg.remote_ip = src.remote_ip, trg.warehouse_approved_time = src.warehouse_approved_time, trg.partbill_shippingcost_cost = src.partbill_shippingcost_cost,
					trg.old_order_id = src.old_order_id,
					trg.mutual_amount = src.mutual_amount, trg.mutual_quotation_id = src.mutual_quotation_id,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then
				insert (entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					shipping_address_id, billing_address_id, 
					state, status, 
					total_qty_ordered, 
					base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
					base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
					base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
					base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
					base_grand_total, 
					base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
					base_to_global_rate, base_to_order_rate, order_currency_code,
					customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
					customer_taxvat, customer_gender, customer_note, customer_note_notify, 
					shipping_description, 
					coupon_code, applied_rule_ids,
					affilBatch, affilCode, affilUserRef, 
					reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
					reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
					presc_verification_method, prescription_verification_type, 
					referafriend_code, referafriend_referer, 
					telesales_method_code, telesales_admin_username, 
					remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
					old_order_id,
					mutual_amount, mutual_quotation_id,
					idETLBatchRun)
					
					values (src.entity_id, 	src.increment_id, src.store_id, src.customer_id, src.created_at, src.updated_at, 
						src.shipping_address_id, src.billing_address_id, 
						src.state, src.status, 
						src.total_qty_ordered, 
						src.base_subtotal, src.base_subtotal_canceled, src.base_subtotal_invoiced, src.base_subtotal_refunded, 
						src.base_shipping_amount, src.base_shipping_canceled, src.base_shipping_invoiced, src.base_shipping_refunded, 
						src.base_discount_amount, src.base_discount_canceled, src.base_discount_invoiced, src.base_discount_refunded, 
						src.base_customer_balance_amount, src.base_customer_balance_invoiced, src.base_customer_balance_refunded,
						src.base_grand_total, 
						src.base_total_canceled, src.base_total_invoiced, src.base_total_invoiced_cost, 
						src.base_total_refunded, src.base_total_offline_refunded, src.base_total_online_refunded, src.bs_customer_bal_total_refunded, src.base_total_due,
						src.base_to_global_rate, src.base_to_order_rate, src.order_currency_code,
						src.customer_dob, src.customer_email, src.customer_firstname, src.customer_lastname, src.customer_middlename, src.customer_prefix, src.customer_suffix, 
						src.customer_taxvat, src.customer_gender, src.customer_note, src.customer_note_notify, 
						src.shipping_description, 
						src.coupon_code, src.applied_rule_ids,
						src.affilBatch, src.affilCode, src.affilUserRef, 
						src.reminder_date, src.reminder_mobile, src.reminder_period, src.reminder_presc, src.reminder_type, src.reminder_follow_sent, src.reminder_sent, 
						src.reorder_on_flag, src.reorder_profile_id, src.automatic_reorder, src.postoptics_source, src.postoptics_auto_verification, 
						src.presc_verification_method, src.prescription_verification_type, 
						src.referafriend_code, src.referafriend_referer, 
						src.telesales_method_code, src.telesales_admin_username, 
						src.remote_ip, src.warehouse_approved_time, src.partbill_shippingcost_cost,	
						src.old_order_id, 				
						src.mutual_amount, src.mutual_quotation_id, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_order_aud;
	go

	create trigger mag.trg_sales_flat_order_aud on mag.sales_flat_order_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_order_aud_hist (entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			state, status, 
			total_qty_ordered, 
			base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
			base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
			base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
			base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
			base_grand_total, 
			base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
			base_to_global_rate, base_to_order_rate, order_currency_code,
			customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
			customer_taxvat, customer_gender, customer_note, customer_note_notify, 
			shipping_description, 
			coupon_code, applied_rule_ids,
			affilBatch, affilCode, affilUserRef, 
			reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
			reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
			presc_verification_method, prescription_verification_type, 
			referafriend_code, referafriend_referer, 
			telesales_method_code, telesales_admin_username, 
			remote_ip, warehouse_approved_time, partbill_shippingcost_cost,  
			old_order_id,
			mutual_amount, mutual_quotation_id, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.increment_id, d.store_id, d.customer_id, d.created_at, d.updated_at, 
				d.shipping_address_id, d.billing_address_id, 
				d.state, d.status, 
				d.total_qty_ordered, 
				d.base_subtotal, d.base_subtotal_canceled, d.base_subtotal_invoiced, d.base_subtotal_refunded, 
				d.base_shipping_amount, d.base_shipping_canceled, d.base_shipping_invoiced, d.base_shipping_refunded, 
				d.base_discount_amount, d.base_discount_canceled, d.base_discount_invoiced, d.base_discount_refunded, 
				d.base_customer_balance_amount, d.base_customer_balance_invoiced, d.base_customer_balance_refunded,
				d.base_grand_total, 
				d.base_total_canceled, d.base_total_invoiced, d.base_total_invoiced_cost, d.base_total_refunded, d.base_total_offline_refunded, d.base_total_online_refunded, d.bs_customer_bal_total_refunded, d.base_total_due,
				d.base_to_global_rate, d.base_to_order_rate, d.order_currency_code,
				d.customer_dob, d.customer_email, d.customer_firstname, d.customer_lastname, d.customer_middlename, d.customer_prefix, d.customer_suffix, 
				d.customer_taxvat, d.customer_gender, d.customer_note, d.customer_note_notify, 
				d.shipping_description, 
				d.coupon_code, d.applied_rule_ids,
				d.affilBatch, d.affilCode, d.affilUserRef, 
				d.reminder_date, d.reminder_mobile, d.reminder_period, d.reminder_presc, d.reminder_type, d.reminder_follow_sent, d.reminder_sent, 
				d.reorder_on_flag, d.reorder_profile_id, d.automatic_reorder, d.postoptics_source, d.postoptics_auto_verification, 
				d.presc_verification_method, d.prescription_verification_type, 
				d.referafriend_code, d.referafriend_referer, 
				d.telesales_method_code, d.telesales_admin_username, 
				d.remote_ip, d.warehouse_approved_time, d.partbill_shippingcost_cost,  
				d.old_order_id,
				d.mutual_amount, d.mutual_quotation_id,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo  -- ?? upd_ts IN BOTH
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go


------------------------- sales_flat_order_item ----------------------------

	drop trigger mag.trg_sales_flat_order_item;
	go 

	create trigger mag.trg_sales_flat_order_item on mag.sales_flat_order_item
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_order_item_aud with (tablock) as trg
		using inserted src
			on (trg.item_id = src.item_id)
		when matched and not exists
			(select 
				isnull(trg.order_id, 0), isnull(trg.store_id, 0), isnull(trg.created_at, ' '), -- isnull(trg.updated_at, ' '), 
				isnull(trg.product_id, 0), isnull(trg.sku, ' '), isnull(trg.name, ' '), isnull(trg.description, ' '), 
				isnull(trg.qty_ordered, 0), isnull(trg.qty_canceled, 0), isnull(trg.qty_invoiced, 0), isnull(trg.qty_refunded, 0), 
				isnull(trg.qty_shipped, 0), isnull(trg.qty_backordered, 0), isnull(trg.qty_returned, 0), 
				isnull(trg.weight, 0), isnull(trg.row_weight, 0), 
				isnull(trg.base_price, 0), isnull(trg.price, 0), isnull(trg.base_price_incl_tax, 0), 
				isnull(trg.base_cost, 0), 
				isnull(trg.base_row_total, 0), 
				isnull(trg.base_discount_amount, 0), isnull(trg.discount_percent, 0), 
				isnull(trg.base_amount_refunded, 0), 
				isnull(trg.mutual_amount, 0), 
				isnull(trg.product_type, ' '), isnull(trg.product_options, ' '), isnull(trg.is_virtual, 0), isnull(trg.is_lens, 0), isnull(trg.lens_group_eye, 0)
			intersect
			select 
				isnull(src.order_id, 0), isnull(src.store_id, 0), isnull(src.created_at, ' '), -- isnull(src.updated_at, ' '), 
				isnull(src.product_id, 0), isnull(src.sku, ' '), isnull(src.name, ' '), isnull(src.description, ' '), 
				isnull(src.qty_ordered, 0), isnull(src.qty_canceled, 0), isnull(src.qty_invoiced, 0), isnull(src.qty_refunded, 0), 
				isnull(src.qty_shipped, 0), isnull(src.qty_backordered, 0), isnull(src.qty_returned, 0), 
				isnull(src.weight, 0), isnull(src.row_weight, 0), 
				isnull(src.base_price, 0), isnull(src.price, 0), isnull(src.base_price_incl_tax, 0), 
				isnull(src.base_cost, 0), 
				isnull(src.base_row_total, 0), 
				isnull(src.base_discount_amount, 0), isnull(src.discount_percent, 0), 
				isnull(src.base_amount_refunded, 0), 
				isnull(src.mutual_amount, 0), 
				isnull(src.product_type, ' '), isnull(src.product_options, ' '), isnull(src.is_virtual, 0), isnull(src.is_lens, 0), isnull(src.lens_group_eye, 0))
			
			then
				update set
					trg.order_id = src.order_id, trg.store_id = src.store_id, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.product_id = src.product_id, trg.sku = src.sku, trg.name = src.name, trg.description = src.description, 
					trg.qty_ordered = src.qty_ordered, trg.qty_canceled = src.qty_canceled, trg.qty_invoiced = src.qty_invoiced, trg.qty_refunded = src.qty_refunded, 
					trg.qty_shipped = src.qty_shipped, trg.qty_backordered = src.qty_backordered, trg.qty_returned = src.qty_returned, 
					trg.weight = src.weight, trg.row_weight = src.row_weight, 
					trg.base_price = src.base_price, trg.price = src.price, trg.base_price_incl_tax = src.base_price_incl_tax, 
					trg.base_cost = src.base_cost, 
					trg.base_row_total = src.base_row_total, 
					trg.base_discount_amount = src.base_discount_amount, trg.discount_percent = src.discount_percent, 
					trg.base_amount_refunded = src.base_amount_refunded, 
					trg.mutual_amount = src.mutual_amount, 
					trg.product_type = src.product_type, trg.product_options = src.product_options, trg.is_virtual = src.is_virtual, trg.is_lens = src.is_lens, trg.lens_group_eye = src.lens_group_eye, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (item_id, 
					order_id, store_id, created_at, updated_at,
					product_id, sku, name, description, 
					qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
					weight, row_weight, 
					base_price, price, base_price_incl_tax, 
					base_cost, 
					base_row_total, 
					base_discount_amount, discount_percent, 
					base_amount_refunded,
					mutual_amount,
					product_type, product_options, is_virtual, is_lens, lens_group_eye,
					idETLBatchRun)
					
					values (src.item_id, 
						src.order_id, src.store_id, src.created_at, src.updated_at,
						src.product_id, src.sku, src.name, src.description, 
						src.qty_ordered, src.qty_canceled, src.qty_invoiced, src.qty_refunded, src.qty_shipped, src.qty_backordered, src.qty_returned, 
						src.weight, src.row_weight, 
						src.base_price, src.price, src.base_price_incl_tax, 
						src.base_cost, 
						src.base_row_total, 
						src.base_discount_amount, src.discount_percent, 
						src.base_amount_refunded,
						src.mutual_amount,
						src.product_type, src.product_options, src.is_virtual, src.is_lens, src.lens_group_eye,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_order_item_aud;
	go

	create trigger mag.trg_sales_flat_order_item_aud on mag.sales_flat_order_item_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_order_item_aud_hist (item_id, 
			order_id, store_id, created_at, updated_at,
			product_id, sku, name, description, 
			qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
			weight, row_weight, 
			base_price, price, base_price_incl_tax, 
			base_cost, 
			base_row_total, 
			base_discount_amount, discount_percent, 
			base_amount_refunded,
			mutual_amount,
			product_type, product_options, is_virtual, is_lens, lens_group_eye,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.item_id, 
				d.order_id, d.store_id, d.created_at, d.updated_at,
				d.product_id, d.sku, d.name, d.description, 
				d.qty_ordered, d.qty_canceled, d.qty_invoiced, d.qty_refunded, d.qty_shipped, d.qty_backordered, d.qty_returned, 
				d.weight, d.row_weight, 
				d.base_price, d.price, d.base_price_incl_tax, 
				d.base_cost, 
				d.base_row_total, 
				d.base_discount_amount, d.discount_percent, 
				d.base_amount_refunded,
				d.mutual_amount,
				d.product_type, d.product_options, d.is_virtual, d.is_lens, d.lens_group_eye,
				d.idETLBatchRun,
				case when (i.item_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.item_id = i.item_id;
	end;
	go

------------------------- sales_flat_order_payment ----------------------------

	drop trigger mag.trg_sales_flat_order_payment;
	go 

	create trigger mag.trg_sales_flat_order_payment on mag.sales_flat_order_payment
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_order_payment_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.parent_id, 0), 
				isnull(trg.method, ' '), 
				isnull(trg.amount_authorized, 0), isnull(trg.amount_canceled, 0), 
				isnull(trg.base_amount_ordered, 0), isnull(trg.base_amount_canceled, 0), isnull(trg.base_amount_refunded, 0), isnull(trg.base_amount_refunded_online, 0), 
				isnull(trg.cc_type, ' '), isnull(trg.cc_status, ' '), isnull(trg.cc_status_description, ' '), isnull(trg.cc_avs_status, ' '), 
				isnull(trg.cc_ss_start_year, ' '), isnull(trg.cc_ss_start_month, ' '), isnull(trg.cc_exp_year, ' '), isnull(trg.cc_exp_month, ' '), 
				isnull(trg.cc_last4, ' '), isnull(trg.cc_owner, ' '), isnull(trg.cc_ss_issue, ' '), 
				isnull(trg.additional_data, ' '), isnull(trg.cybersource_stored_id, ' ')
			intersect
			select 
				isnull(src.parent_id, 0), 
				isnull(src.method, ' '), 
				isnull(src.amount_authorized, 0), isnull(src.amount_canceled, 0), 
				isnull(src.base_amount_ordered, 0), isnull(src.base_amount_canceled, 0), isnull(src.base_amount_refunded, 0), isnull(src.base_amount_refunded_online, 0), 
				isnull(src.cc_type, ' '), isnull(src.cc_status, ' '), isnull(src.cc_status_description, ' '), isnull(src.cc_avs_status, ' '), 
				isnull(src.cc_ss_start_year, ' '), isnull(src.cc_ss_start_month, ' '), isnull(src.cc_exp_year, ' '), isnull(src.cc_exp_month, ' '), 
				isnull(src.cc_last4, ' '), isnull(src.cc_owner, ' '), isnull(src.cc_ss_issue, ' '), 
				isnull(src.additional_data, ' '), isnull(src.cybersource_stored_id, ' '))
			
			then
				update set
					trg.parent_id = src.parent_id, 
					trg.method = src.method, 
					trg.amount_authorized = src.amount_authorized, trg.amount_canceled = src.amount_canceled, 
					trg.base_amount_ordered = src.base_amount_ordered, trg.base_amount_canceled = src.base_amount_canceled, trg.base_amount_refunded = src.base_amount_refunded, trg.base_amount_refunded_online = src.base_amount_refunded_online, 
					trg.cc_type = src.cc_type, trg.cc_status = src.cc_status, trg.cc_status_description = src.cc_status_description, trg.cc_avs_status = src.cc_avs_status, 
					trg.cc_ss_start_year = src.cc_ss_start_year, trg.cc_ss_start_month = src.cc_ss_start_month, trg.cc_exp_year = src.cc_exp_year, trg.cc_exp_month = src.cc_exp_month, 
					trg.cc_last4 = src.cc_last4, trg.cc_owner = src.cc_owner, trg.cc_ss_issue = src.cc_ss_issue, 
					trg.additional_data = src.additional_data, trg.cybersource_stored_id = src.cybersource_stored_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					parent_id, 
					method, 
					amount_authorized, amount_canceled, 
					base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
					cc_type, cc_status, cc_status_description, cc_avs_status, 
					cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
					cc_last4, cc_owner, cc_ss_issue,
					additional_data, cybersource_stored_id,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.parent_id, 
						src.method, 
						src.amount_authorized, src.amount_canceled, 
						src.base_amount_ordered, src.base_amount_canceled, src.base_amount_refunded, src.base_amount_refunded_online, 
						src.cc_type, src.cc_status, src.cc_status_description, src.cc_avs_status, 
						src.cc_ss_start_year, src.cc_ss_start_month, src.cc_exp_year, src.cc_exp_month, 
						src.cc_last4, src.cc_owner, src.cc_ss_issue,
						src.additional_data, src.cybersource_stored_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_order_payment_aud;
	go

	create trigger mag.trg_sales_flat_order_payment_aud on mag.sales_flat_order_payment_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_order_payment_aud_hist (entity_id, 
			parent_id, 
			method, 
			amount_authorized, amount_canceled, 
			base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
			cc_type, cc_status, cc_status_description, cc_avs_status, 
			cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
			cc_last4, cc_owner, cc_ss_issue,
			additional_data, cybersource_stored_id,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.parent_id, 
				d.method, 
				d.amount_authorized, d.amount_canceled, 
				d.base_amount_ordered, d.base_amount_canceled, d.base_amount_refunded, d.base_amount_refunded_online, 
				d.cc_type, d.cc_status, d.cc_status_description, d.cc_avs_status, 
				d.cc_ss_start_year, d.cc_ss_start_month, d.cc_exp_year, d.cc_exp_month, 
				d.cc_last4, d.cc_owner, d.cc_ss_issue,
				d.additional_data, d.cybersource_stored_id,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

------------------------- sales_flat_order_address ----------------------------

	drop trigger mag.trg_sales_flat_order_address;
	go 

	create trigger mag.trg_sales_flat_order_address on mag.sales_flat_order_address
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_order_address_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.parent_id, 0), isnull(trg.customer_address_id, 0), isnull(trg.customer_id, 0), 
				isnull(trg.address_type, ' '), 
				isnull(trg.email, ' '), isnull(trg.company, ' '), 
				isnull(trg.firstname, ' '), isnull(trg.lastname, ' '), 
				isnull(trg.prefix, ' '), isnull(trg.middlename, ' '), isnull(trg.suffix, ' '), 
				isnull(trg.street, ' '), isnull(trg.city, ' '), isnull(trg.postcode, ' '), isnull(trg.region, ' '), 
				isnull(trg.fax, ' '), isnull(trg.telephone, ' '), 
				isnull(trg.region_id, 0), isnull(trg.country_id, ' ')
			intersect
			select 
				isnull(src.parent_id, 0), isnull(src.customer_address_id, 0), isnull(src.customer_id, 0), 
				isnull(src.address_type, ' '), 
				isnull(src.email, ' '), isnull(src.company, ' '), 
				isnull(src.firstname, ' '), isnull(src.lastname, ' '), 
				isnull(src.prefix, ' '), isnull(src.middlename, ' '), isnull(src.suffix, ' '), 
				isnull(src.street, ' '), isnull(src.city, ' '), isnull(src.postcode, ' '), isnull(src.region, ' '), 
				isnull(src.fax, ' '), isnull(src.telephone, ' '), 
				isnull(src.region_id, 0), isnull(src.country_id, ' '))
			
			then
				update set
					trg.parent_id = src.parent_id, trg.customer_address_id = src.customer_address_id, trg.customer_id = src.customer_id, 
					trg.address_type = src.address_type, 
					trg.email = src.email, trg.company = src.company, 
					trg.firstname = src.firstname, trg.lastname = src.lastname, 
					trg.prefix = src.prefix, trg.middlename = src.middlename, trg.suffix = src.suffix, 
					trg.street = src.street, trg.city = src.city, trg.postcode = src.postcode, trg.region = src.region, 
					trg.fax = src.fax, trg.telephone = src.telephone, 
					trg.region_id = src.region_id, trg.country_id = src.country_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					parent_id, customer_address_id, customer_id, 
					address_type, 
					email, company, 
					firstname, lastname, prefix, middlename, suffix, 
					street, city, postcode, region, 
					fax, telephone, 
					region_id, country_id, 
					idETLBatchRun)
					
					values (src.entity_id, 
						src.parent_id, src.customer_address_id, src.customer_id, 
						src.address_type, 
						src.email, src.company, 
						src.firstname, src.lastname, src.prefix, src.middlename, src.suffix, 
						src.street, src.city, src.postcode, src.region, 
						src.fax, src.telephone, 
						src.region_id, src.country_id, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_order_address_aud;
	go

	create trigger mag.trg_sales_flat_order_address_aud on mag.sales_flat_order_address_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_order_address_aud_hist (entity_id, 
			parent_id, customer_address_id, customer_id, 
			address_type, 
			email, company, 
			firstname, lastname, prefix, middlename, suffix, 
			street, city, postcode, region, 
			fax, telephone, 
			region_id, country_id, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.parent_id, d.customer_address_id, d.customer_id, 
				d.address_type, 
				d.email, d.company, 
				d.firstname, d.lastname, d.prefix, d.middlename, d.suffix, 
				d.street, d.city, d.postcode, d.region, 
				d.fax, d.telephone, 
				d.region_id, d.country_id, 
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

------------------------- sales_order_log ----------------------------

	drop trigger mag.trg_sales_order_log;
	go 

	create trigger mag.trg_sales_order_log on mag.sales_order_log
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_order_log_aud with (tablock) as trg
		using inserted src
			on (trg.id = src.id)
		when matched and not exists
			(select 
				isnull(trg.order_id, 0), isnull(trg.order_no, ' '), 
				isnull(trg.state, ' '), isnull(trg.status, ' '), 
				isnull(trg.user_name, ' '), isnull(trg.updated_at, ' ')
			intersect
			select 
				isnull(src.order_id, 0), isnull(src.order_no, ' '), 
				isnull(src.state, ' '), isnull(src.status, ' '), 
				isnull(src.user_name, ' '), isnull(src.updated_at, ' '))
			
			then
				update set
					trg.order_id = src.order_id, trg.order_no = src.order_no, 
					trg.state = src.state, trg.status = src.status, 
					trg.user_name = src.user_name, trg.updated_at = src.updated_at, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (id, 
					order_id, order_no, 
					state, status, 
					user_name, updated_at,
					idETLBatchRun)
					
					values (src.id, 
						src.order_id, src.order_no, 
						src.state, src.status, 
						src.user_name, src.updated_at,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_order_log_aud;
	go

	create trigger mag.trg_sales_order_log_aud on mag.sales_order_log_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_order_log_aud_hist (id, 
			order_id, order_no, 
			state, status, 
			user_name, updated_at,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.id, 
				d.order_id, d.order_no, 
				d.state, d.status, 
				d.user_name, d.updated_at,
				d.idETLBatchRun,
				case when (i.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.id = i.id;
	end;
	go