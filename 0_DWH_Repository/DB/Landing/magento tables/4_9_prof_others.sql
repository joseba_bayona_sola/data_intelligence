use Landing
go 

----------------------- core_store ----------------------------

	select *
	from Landing.mag.core_store

	select store_id, code, website_id, group_id, name, sort_order, is_active, 
		idETLBatchRun, ins_ts
	from Landing.mag.core_store

	select store_id, code, website_id, group_id, name, sort_order, is_active, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.core_store_aud

	select store_id, code, website_id, group_id, name, sort_order, is_active, 
		idETLBatchRun, ins_ts, 
		aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.core_store_aud_hist

----------------------- core_website ----------------------------

	select *
	from Landing.mag.core_website

	select website_id, code, name, sort_order, 
		is_default, is_staging, visibility, default_group_id, website_group_id,
		idETLBatchRun, ins_ts
	from Landing.mag.core_website

	select website_id, code, name, sort_order, 
		is_default, is_staging, visibility, default_group_id, website_group_id,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.core_website_aud

	select website_id, code, name, sort_order, 
		is_default, is_staging, visibility, default_group_id, website_group_id,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.core_website_aud_hist

----------------------- core_config_data ----------------------------

	select *
	from Landing.mag.core_config_data

	select config_id, scope_id, scope, path, value,
		idETLBatchRun, ins_ts
	from Landing.mag.core_config_data

	select config_id, scope_id, scope, path, value,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.core_config_data_aud

	select config_id, scope_id, scope, path, value,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.core_config_data_aud_hist



----------------------- edi_manufacturer ----------------------------

	select *
	from Landing.mag.edi_manufacturer

	select manufacturer_id, manufacturer_code, manufacturer_name,
		idETLBatchRun, ins_ts
	from Landing.mag.edi_manufacturer

	select manufacturer_id, manufacturer_code, manufacturer_name,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_manufacturer_aud

	select manufacturer_id, manufacturer_code, manufacturer_name,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.edi_manufacturer_aud_hist

----------------------- edi_supplier_account ----------------------------

	select *
	from Landing.mag.edi_supplier_account

	select account_id, manufacturer_id, 
		account_ref, account_name, account_description, 
		account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
		idETLBatchRun, ins_ts
	from Landing.mag.edi_supplier_account

	select account_id, manufacturer_id, 
		account_ref, account_name, account_description, 
		account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_supplier_account_aud

	select account_id, manufacturer_id, 
		account_ref, account_name, account_description, 
		account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.edi_supplier_account_aud_hist

----------------------- edi_packsize_pref ----------------------------

	select *
	from Landing.mag.edi_packsize_pref

	select packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
		stocked, cost, supply_days, supply_days_range, 
		enabled, strategy,
		idETLBatchRun, ins_ts
	from Landing.mag.edi_packsize_pref

	select packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
		stocked, cost, supply_days, supply_days_range, 
		enabled, strategy,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_packsize_pref_aud

	select packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
		stocked, cost, supply_days, supply_days_range, 
		enabled, strategy,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.edi_packsize_pref_aud_hist

----------------------- edi_stock_item ----------------------------

	select *
	from Landing.mag.edi_stock_item

	select top 1000 id, product_id, packsize, sku, 
		product_code, pvx_product_code, stocked, 
		BC, DI, PO, CY, AX, AD, DO, CO, 
		edi_conf1, edi_conf2,
		manufacturer_id, 
		supply_days, supply_days_range, disabled,  
		idETLBatchRun, ins_ts
	from Landing.mag.edi_stock_item

	select top 1000 id, product_id, packsize, sku, 
		product_code, pvx_product_code, stocked, 
		BC, DI, PO, CY, AX, AD, DO, CO, 
		edi_conf1, edi_conf2,
		manufacturer_id, 
		supply_days, supply_days_range, disabled,  
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_stock_item_aud

	select id, product_id, packsize, sku, 
		product_code, pvx_product_code, stocked, 
		BC, DI, PO, CY, AX, AD, DO, CO, 
		edi_conf1, edi_conf2,
		manufacturer_id, 
		supply_days, supply_days_range, disabled,  
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.edi_stock_item_aud_hist



----------------------- directory_currency_rate ----------------------------

	select *
	from Landing.mag.directory_currency_rate

	select currency_from, currency_to, rate, 
		idETLBatchRun, ins_ts
	from Landing.mag.directory_currency_rate

	select currency_from, currency_to, rate, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.directory_currency_rate_aud

	select currency_from, currency_to, rate, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.directory_currency_rate_aud_hist

----------------------- salesrule ----------------------------

	select *
	from Landing.mag.salesrule

	select rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts
	from Landing.mag.salesrule

	select rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_aud

	select rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.salesrule_aud_hist

----------------------- salesrule_coupon ----------------------------

	select *
	from Landing.mag.salesrule_coupon

	select coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts
	from Landing.mag.salesrule_coupon

	select coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_coupon_aud

	select coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.salesrule_coupon_aud_hist



----------------------- po_reorder_profile ----------------------------

	select *
	from Landing.mag.po_reorder_profile

	select id,	
		created_at, updated_at, customer_id, name, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
		idETLBatchRun, ins_ts
	from Landing.mag.po_reorder_profile

	select id,	
		created_at, updated_at, customer_id, name, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.po_reorder_profile_aud

	select id,	
		created_at, updated_at, customer_id, name, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.po_reorder_profile_aud_hist

----------------------- po_reorder_quote_payment ----------------------------

	select *
	from Landing.mag.po_reorder_quote_payment

	select payment_id, quote_id, cc_exp_month, cc_exp_year,
		idETLBatchRun, ins_ts
	from Landing.mag.po_reorder_quote_payment

	select payment_id, quote_id, cc_exp_month, cc_exp_year,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.po_reorder_quote_payment_aud

	select payment_id, quote_id, cc_exp_month, cc_exp_year,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.po_reorder_quote_payment_aud_hist



----------------------- admin_user ----------------------------

	select *
	from Landing.mag.admin_user

	select user_id, username, 
		email, firstname, lastname, lognum,
		idETLBatchRun, ins_ts
	from Landing.mag.admin_user

	select user_id, username, 
		email, firstname, lastname, lognum,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.admin_user_aud

	select user_id, username, 
		email, firstname, lastname, lognum,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.admin_user_aud_hist

----------------------- log_customer ----------------------------

	select *
	from Landing.mag.log_customer

	select log_id, visitor_id, customer_id, 
		login_at, logout_at, store_id,
		idETLBatchRun, ins_ts
	from Landing.mag.log_customer

	select log_id, visitor_id, customer_id, 
		login_at, logout_at, store_id,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.log_customer_aud

	select log_id, visitor_id, customer_id, 
		login_at, logout_at, store_id,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.log_customer_aud_hist



----------------------- po_sales_pla_item ---------------------------

	select *
	from Landing.mag.po_sales_pla_item

	select id, order_id, order_item_id, promo_key, price, product_id, 
		idETLBatchRun, ins_ts
	from Landing.mag.po_sales_pla_item

	select id, order_id, order_item_id, promo_key, price, product_id,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.po_sales_pla_item_aud

	select id, order_id, order_item_id, promo_key, price, product_id,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.po_sales_pla_item_aud_hist

----------------------- enterprise_customerbalance ---------------------------

	select *
	from Landing.mag.enterprise_customerbalance

	select balance_id, customer_id, website_id, amount, base_currency_code, 
		idETLBatchRun, ins_ts
	from Landing.mag.enterprise_customerbalance

	select balance_id, customer_id, website_id, amount, base_currency_code, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.enterprise_customerbalance_aud

	select balance_id, customer_id, website_id, amount, base_currency_code, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.enterprise_customerbalance_aud_hist


----------------------- enterprise_customerbalance_history ---------------------------

	select *
	from Landing.mag.enterprise_customerbalance_history

	select history_id, balance_id, updated_at, action, 
		balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified,
		idETLBatchRun, ins_ts
	from Landing.mag.enterprise_customerbalance_history

	select history_id, balance_id, updated_at, action, 
		balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.enterprise_customerbalance_history_aud

	select history_id, balance_id, updated_at, action, 
		balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.enterprise_customerbalance_history_aud_hist


	
----------------------- cybersourcestored_token ---------------------------

	select *
	from Landing.mag.cybersourcestored_token

	select id, ctype, clogo, created_at, updated_at,
		idETLBatchRun, ins_ts
	from Landing.mag.cybersourcestored_token

	select id, ctype, clogo, created_at, updated_at,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.cybersourcestored_token_aud

	select id, ctype, clogo, created_at, updated_at,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.cybersourcestored_token_aud_hist



----------------------- ga_entity_transaction_data ---------------------------

	select *
	from Landing.mag.ga_entity_transaction_data

	select transactionId, transaction_date, source, medium, channel,
		idETLBatchRun, ins_ts
	from Landing.mag.ga_entity_transaction_data

	select transactionId, transaction_date, source, medium, channel,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.ga_entity_transaction_data_aud

	select transactionId, transaction_date, source, medium, channel, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.ga_entity_transaction_data_aud_hist


----------------------- mutual ---------------------------

	select *
	from Landing.mag.mutual

	select mutual_id, code, name, is_online, 
		amc_number, opening_hours, api_id, priority,
		idETLBatchRun, ins_ts
	from Landing.mag.mutual

	select mutual_id, code, name, is_online, 
		amc_number, opening_hours, api_id, priority,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.mutual_aud

	select mutual_id, code, name, is_online, 
		amc_number, opening_hours, api_id, priority,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.mutual_aud_hist


----------------------- mutual_quotation ---------------------------

	select *
	from Landing.mag.mutual_quotation

	select quotation_id, 
		mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
		reference_number, amount, 
		expired_at, created_at, updated_at,
		idETLBatchRun, ins_ts
	from Landing.mag.mutual_quotation

	select quotation_id, 
		mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
		reference_number, amount, 
		expired_at, created_at, updated_at,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.mutual_quotation_aud

	select quotation_id, 
		mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
		reference_number, amount, 
		expired_at, created_at, updated_at,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.mutual_quotation_aud_hist
