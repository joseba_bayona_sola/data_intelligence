use Landing
go 

----------------------- sales_flat_shipment ----------------------------

select * 
from mag.sales_flat_shipment_aud_v
where num_records > 1
order by num_records desc, entity_id, idETLBatchRun

----------------------- sales_flat_shipment_item ----------------------------

select * 
from mag.sales_flat_shipment_item_aud_v
where num_records > 1
order by num_records desc, parent_id, entity_id, idETLBatchRun
