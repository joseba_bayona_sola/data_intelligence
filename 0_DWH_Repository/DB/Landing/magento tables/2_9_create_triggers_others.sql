use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- core_store ----------------------------

	drop trigger mag.trg_core_store;
	go 

	create trigger mag.trg_core_store on mag.core_store
	after insert
	as
	begin
		set nocount on

		merge into mag.core_store_aud with (tablock) as trg
		using inserted src
			on (trg.store_id = src.store_id)
		when matched and
			(isnull(trg.code, '') <> isnull(src.code, '')) and (isnull(trg.name, '') <> isnull(src.name, '')) and
			(isnull(trg.website_id, 0) <> isnull(src.website_id, 0)) and (isnull(trg.group_id, 0) <> isnull(src.group_id, 0)) and
			(isnull(trg.sort_order, 0) <> isnull(src.sort_order, 0)) and (isnull(trg.is_active, 0) <> isnull(src.is_active, 0))
			then
				update set
					trg.code = src.code, trg.name = src.name, 
					trg.website_id = src.website_id, trg.group_id = src.group_id, 
					trg.sort_order = src.sort_order, trg.is_active = src.is_active,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then
				insert (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun)
					values (src.store_id, src.code, src.name, src.website_id, src.group_id, src.sort_order, src.is_active, src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_core_store_aud;
	go 

	create trigger mag.trg_core_store_aud on mag.core_store_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.core_store_aud_hist (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun,
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.store_id, d.code, d.name, d.website_id, d.group_id, d.sort_order, d.is_active, d.idETLBatchRun,
				case when (i.store_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.store_id = i.store_id;
	end;
	go			 


---- 

------------------------- core_website ----------------------------

	drop trigger mag.trg_core_website;
	go 

	create trigger mag.trg_core_website on mag.core_website
	after insert
	as
	begin
		set nocount on

		merge into mag.core_website_aud with (tablock) as trg
		using inserted src
			on (trg.website_id = src.website_id)
		when matched and not exists
			(select 
				isnull(trg.code, ' '), isnull(trg.name, ' '), isnull(trg.sort_order, 0), 
				isnull(trg.is_default, 0), isnull(trg.is_staging, 0), isnull(trg.visibility, ' '), 
				isnull(trg.default_group_id, 0), isnull(trg.website_group_id, 0)
			intersect
			select 
				isnull(src.code, ' '), isnull(src.name, ' '), isnull(src.sort_order, 0), 
				isnull(src.is_default, 0), isnull(src.is_staging, 0), isnull(src.visibility, ' '), 
				isnull(src.default_group_id, 0), isnull(src.website_group_id, 0))
			
			then
				update set
					trg.code = src.code, trg.name = src.name, trg.sort_order = src.sort_order, 
					trg.is_default = src.is_default, trg.is_staging = src.is_staging, trg.visibility = src.visibility, 
					trg.default_group_id = src.default_group_id, trg.website_group_id = src.website_group_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (website_id, 
					code, name, sort_order, 
					is_default, is_staging, visibility, default_group_id, website_group_id,
					idETLBatchRun)
					
					values (src.website_id, 
						src.code, src.name, src.sort_order, 
						src.is_default, src.is_staging, src.visibility, src.default_group_id, src.website_group_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_core_website_aud;
	go

	create trigger mag.trg_core_website_aud on mag.core_website_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.core_website_aud_hist (website_id, 
			code, name, sort_order, 
			is_default, is_staging, visibility, default_group_id, website_group_id,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.website_id, 
				d.code, d.name, d.sort_order, 
				d.is_default, d.is_staging, d.visibility, d.default_group_id, d.website_group_id,
				d.idETLBatchRun,
				case when (i.website_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.website_id = i.website_id;
	end;
	go

---- 

------------------------- core_config_data ----------------------------

	drop trigger mag.trg_core_config_data;
	go 

	create trigger mag.trg_core_config_data on mag.core_config_data
	after insert
	as
	begin
		set nocount on

		merge into mag.core_config_data_aud with (tablock) as trg
		using inserted src
			on (trg.config_id = src.config_id)
		when matched and not exists
			(select 
				isnull(trg.scope_id, 0), isnull(trg.scope, ' '), isnull(trg.path, ' '), isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.scope_id, 0), isnull(src.scope, ' '), isnull(src.path, ' '), isnull(src.value, ' '))
			
			then
				update set
					trg.scope_id = src.scope_id, trg.scope = src.scope, trg.path = src.path, trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (config_id, 
					scope_id, scope, path, value,
					idETLBatchRun)
					
					values (src.config_id, 
						src.scope_id, src.scope, src.path, src.value,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_core_config_data_aud;
	go

	create trigger mag.trg_core_config_data_aud on mag.core_config_data_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.core_config_data_aud_hist (config_id, 
			scope_id, scope, path, value,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.config_id, 
				d.scope_id, d.scope, d.path, d.value,
				d.idETLBatchRun,
				case when (i.config_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.config_id = i.config_id;
	end;
	go



---- 

------------------------- edi_manufacturer ----------------------------

	drop trigger mag.trg_edi_manufacturer;
	go 

	create trigger mag.trg_edi_manufacturer on mag.edi_manufacturer
	after insert
	as
	begin
		set nocount on

		merge into mag.edi_manufacturer_aud with (tablock) as trg
		using inserted src
			on (trg.manufacturer_id = src.manufacturer_id)
		when matched and not exists
			(select 
				isnull(trg.manufacturer_code, ' '), isnull(trg.manufacturer_name, ' ') 
			intersect
			select 
				isnull(src.manufacturer_code, ' '), isnull(src.manufacturer_name, ' '))
			
			then
				update set
					trg.manufacturer_code = src.manufacturer_code, trg.manufacturer_name = src.manufacturer_name, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (manufacturer_id, 
					manufacturer_code, manufacturer_name,
					idETLBatchRun)
					
					values (src.manufacturer_id, 
						src.manufacturer_code, src.manufacturer_name,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_edi_manufacturer_aud;
	go

	create trigger mag.trg_edi_manufacturer_aud on mag.edi_manufacturer_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.edi_manufacturer_aud_hist (manufacturer_id, 
			manufacturer_code, manufacturer_name,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.manufacturer_id, 
				d.manufacturer_code, d.manufacturer_name,
				d.idETLBatchRun,
				case when (i.manufacturer_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.manufacturer_id = i.manufacturer_id;
	end;
	go

---- 

------------------------- edi_supplier_account ----------------------------

	drop trigger mag.trg_edi_supplier_account;
	go 

	create trigger mag.trg_edi_supplier_account on mag.edi_supplier_account
	after insert
	as
	begin
		set nocount on

		merge into mag.edi_supplier_account_aud with (tablock) as trg
		using inserted src
			on (trg.account_id = src.account_id)
		when matched and not exists
			(select 
				isnull(trg.manufacturer_id, 0), 
				isnull(trg.account_ref, ' '), isnull(trg.account_name, ' '), isnull(trg.account_description, ' '), 
				isnull(trg.account_edi_format, ' '), isnull(trg.account_edi_strategy, ' '), 
				isnull(trg.account_edi_cutofftime, ' '), isnull(trg.account_edi_ref, ' ') 
			intersect
			select 
				isnull(src.manufacturer_id, 0), 
				isnull(src.account_ref, ' '), isnull(src.account_name, ' '), isnull(src.account_description, ' '), 
				isnull(src.account_edi_format, ' '), isnull(src.account_edi_strategy, ' '), 
				isnull(src.account_edi_cutofftime, ' '), isnull(src.account_edi_ref, ' ')) 
			
			then
				update set
					trg.manufacturer_id = src.manufacturer_id, 
					trg.account_ref = src.account_ref, trg.account_name = src.account_name, trg.account_description = src.account_description, 
					trg.account_edi_format = src.account_edi_format, trg.account_edi_strategy = src.account_edi_strategy, 
					trg.account_edi_cutofftime = src.account_edi_cutofftime, trg.account_edi_ref = src.account_edi_ref, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (account_id, 
					manufacturer_id, 
					account_ref, account_name, account_description, 
					account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
					idETLBatchRun)
					
					values (src.account_id, 
						src.manufacturer_id, 
						src.account_ref, src.account_name, src.account_description, 
						src.account_edi_format, src.account_edi_strategy, src.account_edi_cutofftime, src.account_edi_ref, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_edi_supplier_account_aud;
	go

	create trigger mag.trg_edi_supplier_account_aud on mag.edi_supplier_account_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.edi_supplier_account_aud_hist (account_id, 
			manufacturer_id, 
			account_ref, account_name, account_description, 
			account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.account_id, 
				d.manufacturer_id, 
				d.account_ref, d.account_name, d.account_description, 
				d.account_edi_format, d.account_edi_strategy, d.account_edi_cutofftime, d.account_edi_ref, 
				d.idETLBatchRun,
				case when (i.account_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.account_id = i.account_id;
	end;
	go

---- 

------------------------- edi_packsize_pref ----------------------------

	drop trigger mag.trg_edi_packsize_pref;
	go 

	create trigger mag.trg_edi_packsize_pref on mag.edi_packsize_pref
	after insert
	as
	begin
		set nocount on

		merge into mag.edi_packsize_pref_aud with (tablock) as trg
		using inserted src
			on (trg.packsize_pref_id = src.packsize_pref_id)
		when matched and not exists
			(select 
				isnull(trg.packsize, 0), isnull(trg.product_id, 0), isnull(trg.priority, 0), isnull(trg.supplier_account_id, 0), 
				isnull(trg.stocked, ' '), isnull(trg.cost, 0), isnull(trg.supply_days, 0), isnull(trg.supply_days_range, 0), 
				isnull(trg.enabled, 0), isnull(trg.strategy, ' ')
			intersect
			select 
				isnull(src.packsize, 0), isnull(src.product_id, 0), isnull(src.priority, 0), isnull(src.supplier_account_id, 0), 
				isnull(src.stocked, ' '), isnull(src.cost, 0), isnull(src.supply_days, 0), isnull(src.supply_days_range, 0), 
				isnull(src.enabled, 0), isnull(src.strategy, ' '))
			
			then
				update set
					trg.packsize = src.packsize, trg.product_id = src.product_id, trg.priority = src.priority, trg.supplier_account_id = src.supplier_account_id, 
					trg.stocked = src.stocked, trg.cost = src.cost, trg.supply_days = src.supply_days, trg.supply_days_range = src.supply_days_range, 
					trg.enabled = src.enabled, trg.strategy = src.strategy, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (packsize_pref_id, 
					packsize, product_id, priority, supplier_account_id, 
					stocked, cost, supply_days, supply_days_range, 
					enabled, strategy,
					idETLBatchRun)
					
					values (src.packsize_pref_id, 
						src.packsize, src.product_id, src.priority, src.supplier_account_id, 
						src.stocked, src.cost, src.supply_days, src.supply_days_range, 
						src.enabled, src.strategy,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_edi_packsize_pref_aud;
	go

	create trigger mag.trg_edi_packsize_pref_aud on mag.edi_packsize_pref_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.edi_packsize_pref_aud_hist (packsize_pref_id, 
			packsize, product_id, priority, supplier_account_id, 
			stocked, cost, supply_days, supply_days_range, 
			enabled, strategy,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.packsize_pref_id, 
				d.packsize, d.product_id, d.priority, d.supplier_account_id, 
				d.stocked, d.cost, d.supply_days, d.supply_days_range, 
				d.enabled, d.strategy,
				d.idETLBatchRun,
				case when (i.packsize_pref_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.packsize_pref_id = i.packsize_pref_id;
	end;
	go

---- 

------------------------- edi_stock_item ----------------------------

	drop trigger mag.trg_edi_stock_item;
	go 

	create trigger mag.trg_edi_stock_item on mag.edi_stock_item
	after insert
	as
	begin
		set nocount on

		merge into mag.edi_stock_item_aud with (tablock) as trg
		using inserted src
			on (trg.id = src.id)
		when matched and not exists
			(select 
				isnull(trg.product_id, 0), isnull(trg.packsize, ' '), isnull(trg.sku, ' '), 
				isnull(trg.product_code, ' '), isnull(trg.pvx_product_code, ' '), isnull(trg.stocked, ' '), 
				isnull(trg.BC, ' '), isnull(trg.DI, ' '), isnull(trg.PO, ' '), isnull(trg.CY, ' '), isnull(trg.AX, ' '), isnull(trg.AD, ' '), isnull(trg.DO, ' '), isnull(trg.CO, ' '), 
				isnull(trg.edi_conf1, ' '), isnull(trg.edi_conf2, ' '), 
				isnull(trg.manufacturer_id, 0), 
				isnull(trg.supply_days, 0), isnull(trg.supply_days_range, 0), isnull(trg.disabled, 0)
			intersect
			select 
				isnull(src.product_id, 0), isnull(src.packsize, ' '), isnull(src.sku, ' '), 
				isnull(src.product_code, ' '), isnull(src.pvx_product_code, ' '), isnull(src.stocked, ' '), 
				isnull(src.BC, ' '), isnull(src.DI, ' '), isnull(src.PO, ' '), isnull(src.CY, ' '), isnull(src.AX, ' '), isnull(src.AD, ' '), isnull(src.DO, ' '), isnull(src.CO, ' '), 
				isnull(src.edi_conf1, ' '), isnull(src.edi_conf2, ' '), 
				isnull(src.manufacturer_id, 0), 
				isnull(src.supply_days, 0), isnull(src.supply_days_range, 0), isnull(src.disabled, 0))
			
			then
				update set
					trg.product_id = src.product_id, trg.packsize = src.packsize, trg.sku = src.sku, 
					trg.product_code = src.product_code, trg.pvx_product_code = src.pvx_product_code, trg.stocked = src.stocked, 
					trg.BC = src.BC, trg.DI = src.DI, trg.PO = src.PO, trg.CY = src.CY, trg.AX = src.AX, trg.AD = src.AD, trg.DO = src.DO, trg.CO = src.CO, 
					trg.edi_conf1 = src.edi_conf1, trg.edi_conf2 = src.edi_conf2, 
					trg.manufacturer_id = src.manufacturer_id, 
					trg.supply_days = src.supply_days, trg.supply_days_range = src.supply_days_range, trg.disabled = src.disabled, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (id, 
					product_id, packsize, sku, 
					product_code, pvx_product_code, stocked, 
					BC, DI, PO, CY, AX, AD, DO, CO, 
					edi_conf1, edi_conf2,
					manufacturer_id, 
					supply_days, supply_days_range, disabled,  
					idETLBatchRun)
					
					values (src.id, 
						src.product_id, src.packsize, src.sku, 
						src.product_code, src.pvx_product_code, src.stocked, 
						src.BC, src.DI, src.PO, src.CY, src.AX, src.AD, src.DO, src.CO, 
						src.edi_conf1, src.edi_conf2,
						src.manufacturer_id, 
						src.supply_days, src.supply_days_range, src.disabled,  
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_edi_stock_item_aud;
	go

	create trigger mag.trg_edi_stock_item_aud on mag.edi_stock_item_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.edi_stock_item_aud_hist (id, 
			product_id, packsize, sku, 
			product_code, pvx_product_code, stocked, 
			BC, DI, PO, CY, AX, AD, DO, CO, 
			edi_conf1, edi_conf2,
			manufacturer_id, 
			supply_days, supply_days_range, disabled,  
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.id, 
				d.product_id, d.packsize, d.sku, 
				d.product_code, d.pvx_product_code, d.stocked, 
				d.BC, d.DI, d.PO, d.CY, d.AX, d.AD, d.DO, d.CO, 
				d.edi_conf1, d.edi_conf2,
				d.manufacturer_id, 
				d.supply_days, d.supply_days_range, d.disabled,  
				d.idETLBatchRun,
				case when (i.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.id = i.id;
	end;
	go



---- 

------------------------- directory_currency_rate ----------------------------

	drop trigger mag.trg_directory_currency_rate;
	go 

	create trigger mag.trg_directory_currency_rate on mag.directory_currency_rate
	after insert
	as
	begin
		set nocount on

		merge into mag.directory_currency_rate_aud with (tablock) as trg
		using inserted src
			on (trg.currency_from = src.currency_from and trg.currency_to = src.currency_to)
		when matched and not exists
			(select 
				isnull(trg.rate, 0)
			intersect
			select 
				isnull(src.rate, 0)) 
			
			then
				update set
					trg.rate = src.rate,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (currency_from, currency_to,
					rate, 
					idETLBatchRun)
					
					values (src.currency_from, src.currency_to,
						src.rate, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_directory_currency_rate_aud;
	go

	create trigger mag.trg_directory_currency_rate_aud on mag.directory_currency_rate_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.directory_currency_rate_aud_hist (currency_from, currency_to, 
			rate, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.currency_from, d.currency_to,
				d.rate, 
				d.idETLBatchRun,
				case when (i.currency_from is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.currency_from = i.currency_from and d.currency_to = i.currency_to;
	end;
	go

---- 

------------------------- salesrule ----------------------------

	drop trigger mag.trg_salesrule;
	go 

	create trigger mag.trg_salesrule on mag.salesrule
	after insert
	as
	begin
		set nocount on

		merge into mag.salesrule_aud with (tablock) as trg
		using inserted src
			on (trg.rule_id = src.rule_id)
		when matched and not exists
			(select 
				isnull(trg.coupon_type, 0), isnull(trg.channel, ' '), isnull(trg.name, ' '), isnull(trg.description, ' '), 
				isnull(trg.from_date, ' '), isnull(trg.to_date, ' '), isnull(trg.sort_order, 0), isnull(trg.simple_action, ' '), 
				isnull(trg.discount_amount, 0), isnull(trg.discount_qty, 0), isnull(trg.discount_step, 0), 
				isnull(trg.uses_per_customer, 0), isnull(trg.uses_per_coupon, 0), 
				isnull(trg.is_active, 0), isnull(trg.is_advanced, 0), isnull(trg.is_rss, 0), isnull(trg.stop_rules_processing, 0), 
				isnull(trg.product_ids, ' '), isnull(trg.simple_free_shipping, 0), isnull(trg.apply_to_shipping, 0), 
				isnull(trg.referafriend_credit, 0), isnull(trg.postoptics_only_new_customer, 0), isnull(trg.reminder_reorder_only, 0), isnull(trg.use_auto_generation, 0), 
				isnull(trg.times_used, 0)
			intersect
			select 
				isnull(src.coupon_type, 0), isnull(src.channel, ' '), isnull(src.name, ' '), isnull(src.description, ' '), 
				isnull(src.from_date, ' '), isnull(src.to_date, ' '), isnull(src.sort_order, 0), isnull(src.simple_action, ' '), 
				isnull(src.discount_amount, 0), isnull(src.discount_qty, 0), isnull(src.discount_step, 0), 
				isnull(src.uses_per_customer, 0), isnull(src.uses_per_coupon, 0), 
				isnull(src.is_active, 0), isnull(src.is_advanced, 0), isnull(src.is_rss, 0), isnull(src.stop_rules_processing, 0), 
				isnull(src.product_ids, ' '), isnull(src.simple_free_shipping, 0), isnull(src.apply_to_shipping, 0), 
				isnull(src.referafriend_credit, 0), isnull(src.postoptics_only_new_customer, 0), isnull(src.reminder_reorder_only, 0), isnull(src.use_auto_generation, 0), 
				isnull(src.times_used, 0))
			
			then
				update set
					trg.coupon_type = src.coupon_type, trg.channel = src.channel, trg.name = src.name, trg.description = src.description, 
					trg.from_date = src.from_date, trg.to_date = src.to_date, trg.sort_order = src.sort_order, trg.simple_action = src.simple_action, 
					trg.discount_amount = src.discount_amount, trg.discount_qty = src.discount_qty, trg.discount_step = src.discount_step, 
					trg.uses_per_customer = src.uses_per_customer, trg.uses_per_coupon = src.uses_per_coupon, 
					trg.is_active = src.is_active, trg.is_advanced = src.is_advanced, trg.is_rss = src.is_rss, trg.stop_rules_processing = src.stop_rules_processing, 
					trg.product_ids = src.product_ids, trg.simple_free_shipping = src.simple_free_shipping, trg.apply_to_shipping = src.apply_to_shipping, 
					trg.referafriend_credit = src.referafriend_credit, trg.postoptics_only_new_customer = src.postoptics_only_new_customer, trg.reminder_reorder_only = src.reminder_reorder_only, trg.use_auto_generation = src.use_auto_generation, 
					trg.times_used = src.times_used, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (rule_id, 
					coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
					discount_amount, discount_qty, discount_step, 
					uses_per_customer, uses_per_coupon, 
					is_active, is_advanced, is_rss, stop_rules_processing, 
					product_ids, simple_free_shipping, apply_to_shipping, 
					referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
					times_used,
					idETLBatchRun)
					
					values (src.rule_id, 
						src.coupon_type, src.channel, src.name, src.description, src.from_date, src.to_date, src.sort_order, src.simple_action, 
						src.discount_amount, src.discount_qty, src.discount_step, 
						src.uses_per_customer, src.uses_per_coupon, 
						src.is_active, src.is_advanced, src.is_rss, src.stop_rules_processing, 
						src.product_ids, src.simple_free_shipping, src.apply_to_shipping, 
						src.referafriend_credit, src.postoptics_only_new_customer, src.reminder_reorder_only, src.use_auto_generation,
						src.times_used,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_salesrule_aud;
	go

	create trigger mag.trg_salesrule_aud on mag.salesrule_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.salesrule_aud_hist (rule_id, 
			coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
			discount_amount, discount_qty, discount_step, 
			uses_per_customer, uses_per_coupon, 
			is_active, is_advanced, is_rss, stop_rules_processing, 
			product_ids, simple_free_shipping, apply_to_shipping, 
			referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
			times_used,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.rule_id, 
				d.coupon_type, d.channel, d.name, d.description, d.from_date, d.to_date, d.sort_order, d.simple_action, 
				d.discount_amount, d.discount_qty, d.discount_step, 
				d.uses_per_customer, d.uses_per_coupon, 
				d.is_active, d.is_advanced, d.is_rss, d.stop_rules_processing, 
				d.product_ids, d.simple_free_shipping, d.apply_to_shipping, 
				d.referafriend_credit, d.postoptics_only_new_customer, d.reminder_reorder_only, d.use_auto_generation,
				d.times_used,
				d.idETLBatchRun,
				case when (i.rule_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.rule_id = i.rule_id;
	end;
	go

---- 

------------------------- salesrule_coupon ----------------------------

	drop trigger mag.trg_salesrule_coupon;
	go 

	create trigger mag.trg_salesrule_coupon on mag.salesrule_coupon
	after insert
	as
	begin
		set nocount on

		merge into mag.salesrule_coupon_aud with (tablock) as trg
		using inserted src
			on (trg.coupon_id = src.coupon_id)
		when matched and not exists
			(select 
				isnull(trg.rule_id, 0), 
				isnull(trg.type, 0), isnull(trg.code, ' '), isnull(trg.usage_limit, 0), isnull(trg.usage_per_customer, 0), 
				isnull(trg.created_at, ' '), isnull(trg.expiration_date, ' '), 
				isnull(trg.is_primary, 0), isnull(trg.referafriend_credit, 0)
			intersect
			select 
				isnull(src.rule_id, 0), 
				isnull(src.type, 0), isnull(src.code, ' '), isnull(src.usage_limit, 0), isnull(src.usage_per_customer, 0), 
				isnull(src.created_at, ' '), isnull(src.expiration_date, ' '), 
				isnull(src.is_primary, 0), isnull(src.referafriend_credit, 0))
			
			then
				update set
					trg.rule_id = src.rule_id, 
					trg.type = src.type, trg.code = src.code, trg.usage_limit = src.usage_limit, trg.usage_per_customer = src.usage_per_customer, 
					trg.created_at = src.created_at, trg.expiration_date = src.expiration_date, 
					trg.is_primary = src.is_primary, trg.referafriend_credit = src.referafriend_credit, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (coupon_id, 
					rule_id, 
					type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
					is_primary, referafriend_credit,
					idETLBatchRun)
					
					values (src.coupon_id, 
						src.rule_id, 
						src.type, src.code, src.usage_limit, src.usage_per_customer, src.created_at, src.expiration_date, 
						src.is_primary, src.referafriend_credit,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_salesrule_coupon_aud;
	go

	create trigger mag.trg_salesrule_coupon_aud on mag.salesrule_coupon_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.salesrule_coupon_aud_hist (coupon_id, 
			rule_id, 
			type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
			is_primary, referafriend_credit,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.coupon_id, 
				d.rule_id, 
				d.type, d.code, d.usage_limit, d.usage_per_customer, d.created_at, d.expiration_date, 
				d.is_primary, d.referafriend_credit,
				d.idETLBatchRun,
				case when (i.coupon_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.coupon_id = i.coupon_id;
	end;
	go



	---- 

------------------------- po_reorder_profile ----------------------------

	drop trigger mag.trg_po_reorder_profile;
	go 

	create trigger mag.trg_po_reorder_profile on mag.po_reorder_profile
	after insert
	as
	begin
		set nocount on

		merge into mag.po_reorder_profile_aud with (tablock) as trg
		using inserted src
			on (trg.id = src.id)
		when matched and not exists
			(select 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' '), isnull(trg.customer_id, 0), isnull(trg.name, ' '), 
				isnull(trg.startdate, ' '), isnull(trg.enddate, ' '), isnull(trg.interval_days, 0), isnull(trg.next_order_date, ' '), 
				isnull(trg.reorder_quote_id, 0), isnull(trg.coupon_rule, 0), isnull(trg.completed_profile, 0), 
				isnull(trg.cache_generation_date, ' '), isnull(trg.cached_reorder_quote_id, 0) 
			intersect
			select 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '), isnull(src.customer_id, 0), isnull(src.name, ' '), 
				isnull(src.startdate, ' '), isnull(src.enddate, ' '), isnull(src.interval_days, 0), isnull(src.next_order_date, ' '), 
				isnull(src.reorder_quote_id, 0), isnull(src.coupon_rule, 0), isnull(src.completed_profile, 0), 
				isnull(src.cache_generation_date, ' '), isnull(src.cached_reorder_quote_id, 0))
			
			then
				update set
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, trg.customer_id = src.customer_id, trg.name = src.name, 
					trg.startdate = src.startdate, trg.enddate = src.enddate, trg.interval_days = src.interval_days, trg.next_order_date = src.next_order_date, 
					trg.reorder_quote_id = src.reorder_quote_id, trg.coupon_rule = src.coupon_rule, trg.completed_profile = src.completed_profile, 
					trg.cache_generation_date = src.cache_generation_date, trg.cached_reorder_quote_id = src.cached_reorder_quote_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (id, 
					created_at, updated_at, customer_id, name, 
					startdate, enddate, interval_days, next_order_date, 
					reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
					idETLBatchRun)
					
					values (src.id, 
						src.created_at, src.updated_at, src.customer_id, src.name, 
						src.startdate, src.enddate, src.interval_days, src.next_order_date, 
						src.reorder_quote_id, src.coupon_rule, src.completed_profile, src.cache_generation_date, src.cached_reorder_quote_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_po_reorder_profile_aud;
	go

	create trigger mag.trg_po_reorder_profile_aud on mag.po_reorder_profile_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.po_reorder_profile_aud_hist (id, 
			created_at, updated_at, customer_id, name, 
			startdate, enddate, interval_days, next_order_date, 
			reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.id, 
				d.created_at, d.updated_at, d.customer_id, d.name, 
				d.startdate, d.enddate, d.interval_days, d.next_order_date, 
				d.reorder_quote_id, d.coupon_rule, d.completed_profile, d.cache_generation_date, d.cached_reorder_quote_id,
				d.idETLBatchRun,
				case when (i.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.id = i.id;
	end;
	go

---- 

------------------------- po_reorder_quote_payment ----------------------------

	drop trigger mag.trg_po_reorder_quote_payment;
	go 

	create trigger mag.trg_po_reorder_quote_payment on mag.po_reorder_quote_payment
	after insert
	as
	begin
		set nocount on

		merge into mag.po_reorder_quote_payment_aud with (tablock) as trg
		using inserted src
			on (trg.payment_id = src.payment_id)
		when matched and not exists
			(select 
				isnull(trg.quote_id, 0), isnull(trg.cc_exp_month, 0), isnull(trg.cc_exp_year, 0)
			intersect
			select 
				isnull(src.quote_id, 0), isnull(src.cc_exp_month, 0), isnull(src.cc_exp_year, 0))
			
			then
				update set
					trg.quote_id = src.quote_id, trg.cc_exp_month = src.cc_exp_month, trg.cc_exp_year = src.cc_exp_year, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (payment_id, 
					quote_id, cc_exp_month, cc_exp_year,
					idETLBatchRun)
					
					values (src.payment_id, 
						src.quote_id, src.cc_exp_month, src.cc_exp_year, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_po_reorder_quote_payment_aud;
	go

	create trigger mag.trg_po_reorder_quote_payment_aud on mag.po_reorder_quote_payment_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.po_reorder_quote_payment_aud_hist (payment_id, 
			quote_id, cc_exp_month, cc_exp_year,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.payment_id, 
				d.quote_id, d.cc_exp_month, d.cc_exp_year,
				d.idETLBatchRun,
				case when (i.payment_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.payment_id = i.payment_id;
	end;
	go



	---- 

------------------------- admin_user ----------------------------

	drop trigger mag.trg_admin_user;
	go 

	create trigger mag.trg_admin_user on mag.admin_user
	after insert
	as
	begin
		set nocount on

		merge into mag.admin_user_aud with (tablock) as trg
		using inserted src
			on (trg.user_id = src.user_id)
		when matched and not exists
			(select 
				isnull(trg.username, ' '), 
				isnull(trg.email, ' '), isnull(trg.firstname, ' '), isnull(trg.lastname, ' '), isnull(trg.lognum, 0)
			intersect
			select 
				isnull(src.username, ' '), 
				isnull(src.email, ' '), isnull(src.firstname, ' '), isnull(src.lastname, ' '), isnull(src.lognum, 0))			
			then
				update set
					trg.username = src.username, 
					trg.email = src.email, trg.firstname = src.firstname, trg.lastname = src.lastname, trg.lognum = src.lognum, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (user_id, 
					username, 
					email, firstname, lastname, lognum,
					idETLBatchRun)
					
					values (src.user_id, 
						src.username, 
						src.email, src.firstname, src.lastname, src.lognum,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_admin_user_aud;
	go

	create trigger mag.trg_admin_user_aud on mag.admin_user_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.admin_user_aud_hist (user_id, 
			username, 
			email, firstname, lastname, lognum,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.user_id, 
				d.username, 
				d.email, d.firstname, d.lastname, d.lognum,
				d.idETLBatchRun,
				case when (i.user_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.user_id = i.user_id;
	end;
	go

---- 

------------------------- log_customer ----------------------------

	drop trigger mag.trg_log_customer;
	go 

	create trigger mag.trg_log_customer on mag.log_customer
	after insert
	as
	begin
		set nocount on

		merge into mag.log_customer_aud with (tablock) as trg
		using inserted src
			on (trg.log_id = src.log_id)
		when matched and not exists
			(select 
				isnull(trg.visitor_id, 0), isnull(trg.customer_id, 0), 
				isnull(trg.login_at, ' '), isnull(trg.logout_at, ' '), isnull(trg.store_id, 0)
			intersect
			select 
				isnull(src.visitor_id, 0), isnull(src.customer_id, 0), 
				isnull(src.login_at, ' '), isnull(src.logout_at, ' '), isnull(src.store_id, 0))
			
			then
				update set
					trg.visitor_id = src.visitor_id, trg.customer_id = src.customer_id, 
					trg.login_at = src.login_at, trg.logout_at = src.logout_at, trg.store_id = src.store_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (log_id, 
					visitor_id, customer_id, 
					login_at, logout_at, store_id,
					idETLBatchRun)
					
					values (src.log_id, 
						src.visitor_id, src.customer_id, 
						src.login_at, src.logout_at, src.store_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_log_customer_aud;
	go

	create trigger mag.trg_log_customer_aud on mag.log_customer_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.log_customer_aud_hist (log_id, 
			visitor_id, customer_id, 
			login_at, logout_at, store_id,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.log_id, 
				d.visitor_id, d.customer_id, 
				d.login_at, d.logout_at, d.store_id,
				d.idETLBatchRun,
				case when (i.log_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.log_id = i.log_id;
	end;
	go


------------------------- po_sales_pla_item ----------------------------


	drop trigger mag.trg_po_sales_pla_item;
	go 

	create trigger mag.trg_po_sales_pla_item on mag.po_sales_pla_item
	after insert
	as
	begin
		set nocount on

		merge into mag.po_sales_pla_item_aud with (tablock) as trg
		using inserted src
			on (trg.id = src.id)
		when matched and not exists
			(select 
				isnull(trg.order_id, 0), isnull(trg.order_item_id, 0), isnull(trg.promo_key, ''), 
				isnull(trg.price, 0), isnull(trg.product_id, 0)
			intersect
			select 
				isnull(src.order_id, 0), isnull(src.order_item_id, 0), isnull(src.promo_key, ''), 
				isnull(src.price, 0), isnull(src.product_id, 0))
			
			then
				update set
					trg.order_id = src.order_id, trg.order_item_id = src.order_item_id, trg.promo_key = src.promo_key, 
					trg.price = src.price, trg.product_id = src.product_id, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (id, order_id, order_item_id, promo_key, price, product_id,
					idETLBatchRun)

					values (src.id, src.order_id, src.order_item_id, src.promo_key, src.price, src.product_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_po_sales_pla_item_aud;
	go 

	create trigger mag.trg_po_sales_pla_item_aud on mag.po_sales_pla_item_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.po_sales_pla_item_aud_hist (id, order_id, order_item_id, promo_key, price, product_id,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
			select d.id, d.order_id, d.order_item_id, d.promo_key, d.price, d.product_id,
				d.idETLBatchRun,
				case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.id = i.id;
	end;
	go


------------------------- enterprise_customerbalance ----------------------------

	drop trigger mag.trg_enterprise_customerbalance;
	go 

	create trigger mag.trg_enterprise_customerbalance on mag.enterprise_customerbalance
	after insert
	as
	begin
		set nocount on

		merge into mag.enterprise_customerbalance_aud with (tablock) as trg
		using inserted src
			on (trg.balance_id = src.balance_id)
		when matched and not exists
			(select 
				isnull(trg.customer_id, 0), isnull(trg.website_id, 0), isnull(trg.amount, 0), isnull(trg.base_currency_code, '')
			intersect
			select 
				isnull(src.customer_id, 0), isnull(src.website_id, 0), isnull(src.amount, 0), isnull(src.base_currency_code, '')) 
			
			then
				update set
					trg.customer_id = src.customer_id, trg.website_id = src.website_id, 
					trg.amount = src.amount, trg.base_currency_code = src.base_currency_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (balance_id, 
					customer_id, website_id, amount, base_currency_code, 
					idETLBatchRun)
					
					values (src.balance_id, 
						src.customer_id, src.website_id, src.amount, src.base_currency_code,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_enterprise_customerbalance_aud;
	go 

	create trigger mag.trg_enterprise_customerbalance_aud on mag.enterprise_customerbalance_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.enterprise_customerbalance_aud_hist (balance_id, 
			customer_id, website_id, amount, base_currency_code,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.balance_id, 
				d.customer_id, d.website_id, d.amount, d.base_currency_code,
				d.idETLBatchRun,
				case when (i.balance_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.balance_id = i.balance_id;
	end;
	go

------------------------- enterprise_customerbalance_history ----------------------------

	drop trigger mag.trg_enterprise_customerbalance_history;
	go 

	create trigger mag.trg_enterprise_customerbalance_history on mag.enterprise_customerbalance_history
	after insert
	as
	begin
		set nocount on

		merge into mag.enterprise_customerbalance_history_aud with (tablock) as trg
		using inserted src
			on (trg.history_id = src.history_id)
		when matched and not exists
			(select 
				isnull(trg.balance_id, 0), isnull(trg.updated_at, ' '), isnull(trg.action, 0), 
				isnull(trg.balance_amount, 0), isnull(trg.balance_delta, 0), 
				isnull(trg.additional_info, ''), isnull(trg.frontend_comment, ''), isnull(trg.is_customer_notified, 0) 
			intersect
			select 
				isnull(src.balance_id, 0), isnull(src.updated_at, ' '), isnull(src.action, 0), 
				isnull(src.balance_amount, 0), isnull(src.balance_delta, 0), 
				isnull(src.additional_info, ''), isnull(src.frontend_comment, ''), isnull(src.is_customer_notified, 0))
			
			then
				update set
					trg.balance_id = src.balance_id, trg.updated_at = src.updated_at, trg.action = src.action, 
					trg.balance_amount = src.balance_amount, trg.balance_delta = src.balance_delta, 
					trg.additional_info = src.additional_info, trg.frontend_comment = src.frontend_comment, trg.is_customer_notified = src.is_customer_notified, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (history_id, 
					balance_id, updated_at, action, 
					balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified, 
					idETLBatchRun)
					
					values (src.history_id, 
						src.balance_id, src.updated_at, src.action, 
						src.balance_amount, src.balance_delta, src.additional_info, src.frontend_comment, src.is_customer_notified, 
						src.idETLBatchRun);
	end; 
	go
	
	drop trigger mag.trg_enterprise_customerbalance_history_aud;
	go 

	create trigger mag.trg_enterprise_customerbalance_history_aud on mag.enterprise_customerbalance_history_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.enterprise_customerbalance_history_aud_hist (history_id, 
			balance_id, updated_at, action, 
			balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.history_id, 
				d.balance_id, d.updated_at, d.action, 
				d.balance_amount, d.balance_delta, d.additional_info, d.frontend_comment, d.is_customer_notified, 
				d.idETLBatchRun,
				case when (i.history_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.history_id = i.history_id;
	end;
	go



------------------------- cybersourcestored_token ----------------------------

	drop trigger mag.trg_cybersourcestored_token;
	go 

	create trigger mag.trg_cybersourcestored_token on mag.cybersourcestored_token
	after insert
	as
	begin
		set nocount on

		merge into mag.cybersourcestored_token_aud with (tablock) as trg
		using inserted src
			on (trg.id = src.id)
		when matched and not exists
			(select 
				isnull(trg.ctype, ' '), isnull(trg.clogo, ' '), isnull(trg.created_at, ' '), isnull(trg.updated_at, ' ')
			intersect
			select 
				isnull(src.ctype, ' '), isnull(src.clogo, ' '), isnull(src.created_at, ' '), isnull(src.updated_at, ' '))
			
			then
				update set
					trg.ctype = src.ctype, trg.clogo = src.clogo, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 					 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (id, ctype, clogo, created_at, updated_at, 
					idETLBatchRun)
					
					values (src.id, src.ctype, src.clogo, src.created_at, src.updated_at, 
						src.idETLBatchRun);
	end; 
	go
	
	drop trigger mag.trg_cybersourcestored_token_aud;
	go 

	create trigger mag.trg_cybersourcestored_token_aud on mag.cybersourcestored_token_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.cybersourcestored_token_aud_hist (id, ctype, clogo, created_at, updated_at, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.id, d.ctype, d.clogo, d.created_at, d.updated_at, 
				d.idETLBatchRun,
				case when (i.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.id = i.id
	end;
	go




------------------------- ga_entity_transaction_data ----------------------------

	drop trigger mag.trg_ga_entity_transaction_data;
	go 

	create trigger mag.trg_ga_entity_transaction_data on mag.ga_entity_transaction_data
	after insert
	as
	begin
		set nocount on

		merge into mag.ga_entity_transaction_data_aud with (tablock) as trg
		using inserted src
			on (trg.transactionId = src.transactionId)
		when matched and not exists
			(select 
				isnull(trg.transaction_date, 0), isnull(trg.source, ' '), isnull(trg.medium, ' '), isnull(trg.channel, ' ')
			intersect
			select 
				isnull(src.transaction_date, 0), isnull(src.source, ' '), isnull(src.medium, ' '), isnull(src.channel, ' '))
			
			then
				update set
					trg.transaction_date = src.transaction_date, trg.source = src.source, trg.medium = src.medium, trg.channel = src.channel, 					 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (transactionId, transaction_date, source, medium, channel,
					idETLBatchRun)
					
					values (src.transactionId, src.transaction_date, src.source, src.medium, src.channel,
						src.idETLBatchRun);
	end; 
	go
	

	drop trigger mag.trg_ga_entity_transaction_data_aud;
	go 

	create trigger mag.trg_ga_entity_transaction_data_aud on mag.ga_entity_transaction_data_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.ga_entity_transaction_data_aud_hist (transactionId, transaction_date, source, medium, channel,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.transactionId, d.transaction_date, d.source, d.medium, d.channel,
				d.idETLBatchRun,
				case when (i.transactionId is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.transactionId = i.transactionId
	end;
	go



------------------------- mutual ----------------------------

	drop trigger mag.trg_mutual;
	go 

	create trigger mag.trg_mutual on mag.mutual
	after insert
	as
	begin
		set nocount on

		merge into mag.mutual_aud with (tablock) as trg
		using inserted src
			on (trg.mutual_id = src.mutual_id)
		when matched and not exists
			(select 
				isnull(trg.code, ' '), isnull(trg.name, ' '), isnull(trg.is_online, 0), 
				isnull(trg.amc_number, ' '), isnull(trg.opening_hours, ' '), isnull(trg.api_id, 0), isnull(trg.priority, 0)
			intersect
			select 
				isnull(src.code, ' '), isnull(src.name, ' '), isnull(src.is_online, 0), 
				isnull(src.amc_number, ' '), isnull(src.opening_hours, ' '), isnull(src.api_id, 0), isnull(src.priority, 0))
			
			then
				update set
					trg.code = src.code, trg.name = src.name, trg.is_online = src.is_online, 
					trg.amc_number = src.amc_number, trg.opening_hours = src.opening_hours, trg.api_id = src.api_id, trg.priority = src.priority, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (mutual_id, code, name, is_online, 
					amc_number, opening_hours, api_id, priority,
					idETLBatchRun)
					
					values (src.mutual_id, src.code, src.name, src.is_online, 
						src.amc_number, src.opening_hours, src.api_id, src.priority,
						src.idETLBatchRun);
	end; 
	go
	

	drop trigger mag.trg_mutual_aud;
	go 

	create trigger mag.trg_mutual_aud on mag.mutual_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.mutual_aud_hist (mutual_id, code, name, is_online, 
			amc_number, opening_hours, api_id, priority,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.mutual_id, d.code, d.name, d.is_online, 
				d.amc_number, d.opening_hours, d.api_id, d.priority, 
				d.idETLBatchRun,
				case when (i.mutual_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.mutual_id = i.mutual_id
	end;
	go



------------------------- mutual_quotation ----------------------------

	drop trigger mag.trg_mutual_quotation;
	go 

	create trigger mag.trg_mutual_quotation on mag.mutual_quotation
	after insert
	as
	begin
		set nocount on

		merge into mag.mutual_quotation_aud with (tablock) as trg
		using inserted src
			on (trg.quotation_id = src.quotation_id)
		when matched and not exists
			(select 
				isnull(trg.mutual_id, 0), isnull(trg.mutual_customer_id, 0), 
				isnull(trg.type_id, 0), isnull(trg.is_online, 0), isnull(trg.status_id, 0), isnull(trg.error_id, 0), 
				isnull(trg.reference_number, ' '), isnull(trg.amount, ' '), 
				isnull(trg.expired_at, ' '), isnull(trg.created_at, ' ')
			intersect
			select 
				isnull(src.mutual_id, 0), isnull(src.mutual_customer_id, 0), 
				isnull(src.type_id, 0), isnull(src.is_online, 0), isnull(src.status_id, 0), isnull(src.error_id, 0), 
				isnull(src.reference_number, ' '), isnull(src.amount, ' '), 
				isnull(src.expired_at, ' '), isnull(src.created_at, ' '))
			
			then
				update set
					trg.mutual_id = src.mutual_id, trg.mutual_customer_id = src.mutual_customer_id, 
					trg.type_id = src.type_id, trg.is_online = src.is_online, trg.status_id = src.status_id, trg.error_id = src.error_id, 
					trg.reference_number = src.reference_number, trg.amount = src.amount, 
					trg.expired_at = src.expired_at, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (quotation_id, mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
					reference_number, amount, 
					expired_at, created_at, updated_at,
					idETLBatchRun)
					
					values (src.quotation_id, src.mutual_id, src.mutual_customer_id, src.type_id, src.is_online, src.status_id, src.error_id, 
						src.reference_number, src.amount, 
						src.expired_at, src.created_at, src.updated_at,
						src.idETLBatchRun);
	end; 
	go
	

	drop trigger mag.trg_mutual_quotation_aud;
	go 

	create trigger mag.trg_mutual_quotation_aud on mag.mutual_quotation_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.mutual_quotation_aud_hist (quotation_id, mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
			reference_number, amount, 
			expired_at, created_at, updated_at,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.quotation_id, d.mutual_id, d.mutual_customer_id, d.type_id, d.is_online, d.status_id, d.error_id, 
				d.reference_number, d.amount, 
				d.expired_at, d.created_at, d.updated_at,
				d.idETLBatchRun,
				case when (i.quotation_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.quotation_id = i.quotation_id
	end;
	go

