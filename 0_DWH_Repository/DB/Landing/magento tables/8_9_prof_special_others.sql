use Landing
go 

------------------------------------------------------------------------
----------------------- core_config_data ----------------------------
------------------------------------------------------------------------

	select config_id, scope_id, scope, path, value,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.core_config_data_aud
	order by scope, path, scope_id

	select scope_id, count(*)
	from Landing.mag.core_config_data_aud
	group by scope_id
	order by scope_id

	select scope, count(*)
	from Landing.mag.core_config_data_aud
	group by scope
	order by scope

	select path, count(*) num 
	from Landing.mag.core_config_data_aud
	group by path
	order by num desc, path

------------------------------------------------------------------------
----------------------- edi_manufacturer - edi_supplier_account -------
------------------------------------------------------------------------

	select manufacturer_id, manufacturer_code, manufacturer_name,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_manufacturer_aud
	order by manufacturer_code

	select m.manufacturer_id, m.manufacturer_code, m.manufacturer_name,
		a.account_id, a.account_ref, a.account_name, a.account_description, 
		a.account_edi_format, a.account_edi_strategy, a.account_edi_cutofftime, a.account_edi_ref, 
		a.idETLBatchRun, a.ins_ts, a.upd_ts
	from 
			Landing.mag.edi_supplier_account_aud a
		inner join
			Landing.mag.edi_manufacturer_aud m on a.manufacturer_id = m.manufacturer_id
	order by m.manufacturer_code, a.account_ref

------------------------------------------------------------------------
----------------------- edi_packsize_pref ------------------------------
------------------------------------------------------------------------

	select packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
		stocked, cost, supply_days, supply_days_range, 
		enabled, strategy,
		idETLBatchRun, ins_ts
	from Landing.mag.edi_packsize_pref_aud

------------------------------------------------------------------------
----------------------- edi_stock_item ------------------------------
------------------------------------------------------------------------

	select top 1000 id, product_id, packsize, sku, 
		product_code, pvx_product_code, stocked, 
		BC, DI, PO, CY, AX, AD, DO, CO, 
		edi_conf1, edi_conf2,
		manufacturer_id, 
		supply_days, supply_days_range, disabled,  
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.edi_stock_item_aud
	where sku = '1DAM90'
	order by BC, DI, PO, CY, AX, AD, DO, CO, packsize

	select sku, count(*), count(distinct packsize)
	from Landing.mag.edi_stock_item_aud
	group by sku
	order by sku

	select BC, count(*)
	from Landing.mag.edi_stock_item_aud
	group by BC
	order by BC

	select DI, count(*)
	from Landing.mag.edi_stock_item_aud
	group by DI
	order by DI

	select PO, count(*)
	from Landing.mag.edi_stock_item_aud
	group by PO
	order by PO


	select CY, count(*)
	from Landing.mag.edi_stock_item_aud
	group by CY
	order by CY

	select AX, count(*)
	from Landing.mag.edi_stock_item_aud
	group by AX
	order by AX


	select AD, count(*)
	from Landing.mag.edi_stock_item_aud
	group by AD
	order by AD

	select DO, count(*)
	from Landing.mag.edi_stock_item_aud
	group by DO
	order by DO


	select CO, count(*)
	from Landing.mag.edi_stock_item_aud
	group by CO
	order by CO


------------------------------------------------------------------------
----------------------- directory_currency_rate ------------------------------
------------------------------------------------------------------------

	select record_type, num_records, 
		currency_from, currency_to, rate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.directory_currency_rate_aud_v
	order by currency_from, currency_to, ins_ts

------------------------------------------------------------------------
----------------------- salesrule ------------------------------
------------------------------------------------------------------------

	select rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_aud
	order by channel, name

	select coupon_type, count(*)
	from Landing.mag.salesrule_aud
	group by coupon_type
	order by coupon_type

	select channel, count(*)
	from Landing.mag.salesrule_aud
	group by channel
	order by channel

	select simple_action, count(*)
	from Landing.mag.salesrule_aud
	group by simple_action
	order by simple_action

	select discount_step, count(*)
	from Landing.mag.salesrule_aud
	group by discount_step
	order by discount_step

	select uses_per_customer, count(*)
	from Landing.mag.salesrule_aud
	group by uses_per_customer
	order by uses_per_customer

	select uses_per_coupon, count(*)
	from Landing.mag.salesrule_aud
	group by uses_per_coupon
	order by uses_per_coupon

	select is_active, count(*)
	from Landing.mag.salesrule_aud
	group by is_active
	order by is_active

	select simple_free_shipping, count(*)
	from Landing.mag.salesrule_aud
	group by simple_free_shipping
	order by simple_free_shipping

	select postoptics_only_new_customer, count(*)
	from Landing.mag.salesrule_aud
	group by postoptics_only_new_customer
	order by postoptics_only_new_customer


------------------------------------------------------------------------
----------------------- salesrule_coupon ------------------------------
------------------------------------------------------------------------

	select coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_coupon_aud

	select type, count(*)
	from Landing.mag.salesrule_coupon_aud
	group by type
	order by type

	select sr.rule_id, src.coupon_id, count(src.coupon_id) over (partition by sr.rule_id) num_rep,
		sr.coupon_type, sr.channel, sr.name, sr.description, sr.from_date, sr.to_date,
		src.type, src.code, src.created_at, src.expiration_date, 
		src.usage_limit, src.usage_per_customer, src.is_primary, src.referafriend_credit,
		src.idETLBatchRun, src.ins_ts, src.upd_ts
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
	order by num_rep desc, sr.channel, src.code

------------------------------------------------------------------------
----------------------- po_reorder_profile ------------------------------
------------------------------------------------------------------------

	select top 1000 
		count(*) over (partition by customer_id) num_rep_cust,
		count(*) over (partition by reorder_quote_id) num_rep_order,
		id,	
		created_at, updated_at, customer_id, name, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.po_reorder_profile_aud
	order by num_rep_order desc, num_rep_cust desc, reorder_quote_id, startdate

	select interval_days, count(*)
	from Landing.mag.po_reorder_profile_aud
	group by interval_days
	order by interval_days

	select coupon_rule, count(*)
	from Landing.mag.po_reorder_profile_aud
	group by coupon_rule
	order by coupon_rule

	select completed_profile, count(*)
	from Landing.mag.po_reorder_profile_aud
	group by completed_profile
	order by completed_profile

------------------------------------------------------------------------
----------------------- admin_user ------------------------------
------------------------------------------------------------------------

	select user_id, username, 
		email, firstname, lastname, lognum,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.admin_user_aud
	order by username

------------------------------------------------------------------------
----------------------- log_customer ------------------------------
------------------------------------------------------------------------

	select top 10000 count(*) over (partition by customer_id) num_rep_cust,
		log_id, visitor_id, customer_id, 
		login_at, logout_at, store_id,
		idETLBatchRun, ins_ts
	from Landing.mag.log_customer_aud
	order by num_rep_cust desc, customer_id, login_at

	
	
------------------------------------------------------------------------
----------------------- enterprise_customerbalance ----------------------------
------------------------------------------------------------------------

select top 1000 balance_id, customer_id, website_id, amount, base_currency_code, 
	idETLBatchRun, ins_ts
from Landing.mag.enterprise_customerbalance_aud
where balance_id = 110479

	select count(*), count(distinct balance_id), count(distinct customer_id)--, count(distinct customer_id, website_id) 
	from Landing.mag.enterprise_customerbalance_aud

	select customer_id, count(*)
	from Landing.mag.enterprise_customerbalance_aud
	group by customer_id
	order by count(*) desc, customer_id
  
	select customer_id, website_id, count(*)
	from Landing.mag.enterprise_customerbalance_aud
	group by customer_id, website_id
	order by count(*) desc, customer_id, website_id
  
	select base_currency_code, count(*)
	from Landing.mag.enterprise_customerbalance_aud
	group by base_currency_code
	order by base_currency_code

------------------------------------------------------------------------
----------------------- enterprise_customerbalance_history ----------------------------
------------------------------------------------------------------------


select top 1000 count(*) over (partition by balance_id),
	history_id, balance_id, updated_at, action, 
	balance_amount, balance_delta, 
	additional_info, frontend_comment, is_customer_notified
from Landing.mag.enterprise_customerbalance_history_aud
-- where balance_id = 54290
order by updated_at desc

	select count(*), count(distinct history_id), count(distinct balance_id)
	from Landing.mag.enterprise_customerbalance_history_aud

	select action, count(*)
	from Landing.mag.enterprise_customerbalance_history_aud
	group by action
	order by action

	select balance_id, count(*)
	from Landing.mag.enterprise_customerbalance_history_aud
	group by balance_id
	order by count(*) desc


------------------------------------------------------------------------
----------------------- enterprise_customerbalance_history ----------------------------
------------------------------------------------------------------------


	select top 1000 transactionId, transaction_date, source, medium, channel, 
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.ga_entity_transaction_data_aud

	select transaction_date, count(*)
	from Landing.mag.ga_entity_transaction_data_aud
	group by transaction_date
	order by transaction_date

	select source, count(*)
	from Landing.mag.ga_entity_transaction_data_aud
	group by source
	order by count(*) desc

	select medium, count(*)
	from Landing.mag.ga_entity_transaction_data_aud
	where channel is not null
	group by medium
	order by medium

	select channel, count(*)
	from Landing.mag.ga_entity_transaction_data_aud
	group by channel
	order by channel

	select oh.entity_id, oh.increment_id, oh.store_id, oh.customer_id, oh.created_at, oh.created_at_date,
		oh.affilCode, 
		gat.transaction_date, gat.source, gat.medium, gat.channel, 
		count(*) over (partition by oh.created_at_date) num_rows_day,
		count(gat.transactionId) over (partition by oh.created_at_date) num_rows_gat
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, 
				affilCode, 
				CONVERT(INT, (CONVERT(VARCHAR(8), created_at, 112))) created_at_date
			from Landing.mag.sales_flat_order_aud
			where CONVERT(INT, (CONVERT(VARCHAR(8), created_at, 112))) = 20171021) oh 
		left join
			Landing.mag.ga_entity_transaction_data_aud gat on oh.increment_id = gat.transactionId
	order by oh.created_at

	select created_at_date, num_rows_day, num_rows_gat, 
		convert(decimal(12, 4), num_rows_gat * 100) / num_rows_day
	from
		(select oh.entity_id, oh.increment_id, oh.store_id, oh.customer_id, oh.created_at, oh.created_at_date,
			oh.affilCode, 
			gat.transaction_date, gat.source, gat.medium, gat.channel, 
			count(*) over (partition by oh.created_at_date) num_rows_day,
			count(gat.transactionId) over (partition by oh.created_at_date) num_rows_gat
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, 
					affilCode, 
					CONVERT(INT, (CONVERT(VARCHAR(8), created_at, 112))) created_at_date
				from Landing.mag.sales_flat_order_aud) oh 
			left join
				Landing.mag.ga_entity_transaction_data_aud gat on oh.increment_id = gat.transactionId) t
	where num_rows_gat = 0
	group by created_at_date, num_rows_day, num_rows_gat
	order by created_at_date
