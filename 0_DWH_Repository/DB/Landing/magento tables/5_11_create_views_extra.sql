
use Landing 
go

----------------------- review_entity ----------------------------

drop view mag.review_entity_aud_v 
go 

create view mag.review_entity_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records,
		entity_id, entity_code, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_id, entity_code, 
			idETLBatchRun,ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.review_entity_aud
		union
		select 'H' record_type, entity_id, entity_code, 
			idETLBatchRun,ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_entity_aud_hist) v
go


----------------------- review_status ----------------------------

drop view mag.review_status_aud_v
go 

create view mag.review_status_aud_v as
	select record_type, count(*) over (partition by status_id) num_records, 
		status_id, status_code,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		
		(select 'N' record_type, status_id, status_code, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.review_status_aud
		union
		select 'H' record_type, status_id, status_code, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_status_aud_hist) v
go


----------------------- review ----------------------------

drop view mag.review_aud_v 
go 

create view mag.review_aud_v as
	select record_type, count(*) over (partition by review_id) num_records,
		review_id, created_at, entity_id, status_id, entity_pk_value,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, review_id, created_at, entity_id, status_id, entity_pk_value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.review_aud
		union
		select 'H' record_type, review_id, created_at, entity_id, status_id, entity_pk_value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_aud_hist) v
go


----------------------- review_store ----------------------------

drop view mag.review_store_aud_v 
go 

create view mag.review_store_aud_v as
	select record_type, count(*) over (partition by review_id, store_id) num_records, 
		review_id, store_id, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from 
		(select 'N' record_type, review_id, store_id, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.review_store_aud
		union
		select 'H' record_type, review_id, store_id, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_store_aud_hist) v
go


----------------------- review_detail ----------------------------

drop view mag.review_detail_aud_v 
go 

create view mag.review_detail_aud_v as
	select record_type, count(*) over (partition by detail_id) num_records, 
		detail_id, review_id, store_id, 
		title, detail, nickname, customer_id, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, detail_id, review_id, store_id, 
			title, detail, nickname, customer_id, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.review_detail_aud
		union
		select 'H' record_type, detail_id, review_id, store_id, 
			title, detail, nickname, customer_id, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_detail_aud_hist) v
go





------------------------------------------------------------------------------

----------------------- rating_entity ----------------------------

drop view mag.rating_entity_aud_v 
go 

create view mag.rating_entity_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, 
		entity_id, entity_code, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from 
		(select 'N' record_type, entity_id, entity_code, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.rating_entity_aud
		union
		select 'H' record_type, entity_id, entity_code, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_entity_aud_hist) v
go


----------------------- rating ----------------------------

drop view mag.rating_aud_v 
go 

create view mag.rating_aud_v as
	select record_type, count(*) over (partition by rating_id) num_records, 
		rating_id, entity_id, rating_code, position,
		idETLBatchRun, ins_ts,upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from 
		(select 'N' record_type, rating_id, entity_id, rating_code, position, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.rating_aud
		union
		select 'H' record_type, rating_id, entity_id, rating_code, position, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_aud_hist) v
go


----------------------- rating_option ----------------------------

drop view mag.rating_option_aud_v 
go 

create view mag.rating_option_aud_v as
	select record_type, count(*) over (partition by option_id) num_records,
		option_id, rating_id, code, value, position, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, option_id, rating_id, code, value, position, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.rating_option_aud
		union
		select 'H' record_type, option_id, rating_id, code, value, position, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_option_aud_hist) v
go


----------------------- rating_option_value ----------------------------

drop view mag.rating_option_vote_aud_v
go 

create view mag.rating_option_vote_aud_v as
	select record_type, count(*) over (partition by vote_id) num_records, vote_id,
		review_id, remote_ip, customer_id, entity_pk_value, 
		rating_id, option_id, percent_rating, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, vote_id,
			review_id, remote_ip, customer_id, entity_pk_value, 
			rating_id, option_id, percent_rating, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.rating_option_vote_aud
		union
		select 'H' record_type, vote_id, 
			review_id, remote_ip, customer_id, entity_pk_value, 
			rating_id, option_id, percent_rating, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_option_vote_aud_hist) v
go


