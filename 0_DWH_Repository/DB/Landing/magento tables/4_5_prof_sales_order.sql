
use Landing
go 

----------------------- sales_flat_order ----------------------------

select *
from mag.sales_flat_order;

select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids,
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	old_order_id,
	mutual_amount, mutual_quotation_id,
	idETLBatchRun, ins_ts
from mag.sales_flat_order

	select min(created_at), max(created_at)
	from mag.sales_flat_order

select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids, 
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	old_order_id,
	mutual_amount, mutual_quotation_id,
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_aud
where -- idETLBatchRun = 97
	ENTITY_ID = 4822428

	select idETLBatchRun, count(*)
	from mag.sales_flat_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select idETLBatchRun, count(*)
	from mag.sales_flat_order_aud
	where upd_ts is not null
	group by idETLBatchRun
	order by idETLBatchRun

select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids, 
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	old_order_id,
	mutual_amount, mutual_quotation_id,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.sales_flat_order_aud_hist


----------------------- sales_flat_order_item ----------------------------

select *
from mag.sales_flat_invoice_item;

select item_id, order_id, store_id, created_at, updated_at,
	product_id, sku, name, description, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	weight, row_weight, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, 
	base_discount_amount, discount_percent, 
	base_amount_refunded,
	mutual_amount, 
	product_type, product_options, is_virtual, is_lens, lens_group_eye,
	idETLBatchRun, ins_ts 
from mag.sales_flat_order_item;

select item_id, order_id, store_id, created_at, updated_at,
	product_id, sku, name, description, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	weight, row_weight, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, 
	base_discount_amount, discount_percent, 
	base_amount_refunded,
	mutual_amount, 
	product_type, product_options, is_virtual, is_lens, lens_group_eye,
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_item_aud;

select item_id, order_id, store_id, created_at, updated_at,
	product_id, sku, name, description, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	weight, row_weight, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, 
	base_discount_amount, discount_percent, 
	base_amount_refunded,
	mutual_amount, 
	product_type, product_options, is_virtual, is_lens, lens_group_eye,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 
from mag.sales_flat_order_item_aud_hist;


----------------------- sales_flat_order_payment ----------------------------

select entity_id, parent_id, 
	method, 
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
	cc_type, cc_status, cc_status_description, cc_avs_status, 
	cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
	cc_last4, cc_owner, cc_ss_issue, 
	additional_data, cybersource_stored_id,
	idETLBatchRun, ins_ts
from mag.sales_flat_order_payment;

select entity_id, parent_id, 
	method, 
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
	cc_type, cc_status, cc_status_description, cc_avs_status, 
	cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
	cc_last4, cc_owner, cc_ss_issue, 
	additional_data, cybersource_stored_id,
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_payment_aud;

select entity_id, parent_id, 
	method, 
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
	cc_type, cc_status, cc_status_description, cc_avs_status, 
	cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
	cc_last4, cc_owner, cc_ss_issue, 
	additional_data, cybersource_stored_id,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 
from mag.sales_flat_order_payment_aud_hist;

----------------------- sales_flat_order_address ----------------------------

select *
from mag.sales_flat_order_address;

select entity_id, parent_id, customer_address_id, customer_id, 
	address_type, 
	email, company, 
	firstname, lastname, prefix, middlename, suffix, 
	street, city, postcode, region, 
	fax, telephone, 
	region_id, country_id, 
	idETLBatchRun, ins_ts
from mag.sales_flat_order_address;

select entity_id, parent_id, customer_address_id, customer_id, 
	address_type, 
	email, company, 
	firstname, lastname, prefix, middlename, suffix, 
	street, city, postcode, region, 
	fax, telephone, 
	region_id, country_id, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_address_aud;

select entity_id, parent_id, customer_address_id, customer_id, 
	address_type, 
	email, company, 
	firstname, lastname, prefix, middlename, suffix, 
	street, city, postcode, region, 
	fax, telephone, 
	region_id, country_id, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 
from mag.sales_flat_order_address_aud_hist;

----------------------- sales_order_log ----------------------------
select *
from mag.sales_order_log;

select id, order_id, order_no, 
	state, status, 
	user_name, updated_at, 
	idETLBatchRun, ins_ts
from mag.sales_order_log;

select id, order_id, order_no, 
	state, status, 
	user_name, updated_at, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_order_log_aud;

select id, order_id, order_no, 
	state, status, 
	user_name, updated_at, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 
from mag.sales_order_log_aud_hist;

