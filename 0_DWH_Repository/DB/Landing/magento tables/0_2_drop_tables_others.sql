use Landing
go

drop table mag.core_store;
go
drop table mag.core_store_aud;
go
drop table mag.core_store_aud_hist;
go


drop table mag.core_website;
go
drop table mag.core_website_aud;
go
drop table mag.core_website_aud_hist;
go

drop table mag.core_config_data;
go
drop table mag.core_config_data_aud;
go
drop table mag.core_config_data_aud_hist;
go

drop table mag.edi_manufacturer;
go
drop table mag.edi_manufacturer_aud;
go
drop table mag.edi_manufacturer_aud_hist;
go

drop table mag.edi_supplier_account;
go
drop table mag.edi_supplier_account_aud;
go
drop table mag.edi_supplier_account_aud_hist;
go

drop table mag.edi_packsize_pref;
go
drop table mag.edi_packsize_pref_aud;
go
drop table mag.edi_packsize_pref_aud_hist;
go

drop table mag.edi_stock_item;
go
drop table mag.edi_stock_item_aud;
go
drop table mag.edi_stock_item_aud_hist;
go

drop table mag.directory_currency_rate;
go
drop table mag.directory_currency_rate_aud;
go
drop table mag.directory_currency_rate_aud_hist;
go

drop table mag.salesrule;
go
drop table mag.salesrule_aud;
go
drop table mag.salesrule_aud_hist;
go

drop table mag.salesrule_coupon;
go
drop table mag.salesrule_coupon_aud;
go
drop table mag.salesrule_coupon_aud_hist;
go

drop table mag.po_reorder_profile;
go
drop table mag.po_reorder_profile_aud;
go
drop table mag.po_reorder_profile_aud_hist;
go

drop table mag.po_reorder_quote_payment;
go
drop table mag.po_reorder_quote_payment_aud;
go
drop table mag.po_reorder_quote_payment_aud_hist;
go

drop table mag.admin_user;
go
drop table mag.admin_user_aud;
go
drop table mag.admin_user_aud_hist;
go

drop table mag.log_customer;
go
drop table mag.log_customer_aud;
go
drop table mag.log_customer_aud_hist;
go


drop table mag.po_sales_pla_item;
go
drop table mag.po_sales_pla_item_aud;
go
drop table mag.po_sales_pla_item_aud_hist;
go


drop table mag.enterprise_customerbalance;
go
drop table mag.enterprise_customerbalance_aud;
go
drop table mag.enterprise_customerbalance_aud_hist;
go


drop table mag.enterprise_customerbalance_history;
go
drop table mag.enterprise_customerbalance_history_aud;
go
drop table mag.enterprise_customerbalance_history_aud_hist;
go


drop table mag.cybersourcestored_token;
go
drop table mag.cybersourcestored_token_aud;
go
drop table mag.cybersourcestored_token_aud_hist;
go



drop table mag.ga_entity_transaction_data;
go
drop table mag.ga_entity_transaction_data_aud;
go
drop table mag.ga_entity_transaction_data_aud_hist;
go


drop table mag.mutual;
go
drop table mag.mutual_aud;
go
drop table mag.mutual_aud_hist;
go


drop table mag.mutual_quotation;
go
drop table mag.mutual_quotation_aud;
go
drop table mag.mutual_quotation_aud_hist;
go

