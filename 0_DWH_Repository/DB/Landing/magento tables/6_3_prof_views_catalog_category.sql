use Landing
go 

----------------------- catalog_category_entity ----------------------------

select * 
from mag.catalog_category_entity_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_datetime_entity ----------------------------

select * 
from mag.catalog_category_entity_datetime_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_decimal_entity ----------------------------

select * 
from mag.catalog_category_entity_decimal_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_int_entity ----------------------------

select * 
from mag.catalog_category_entity_int_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_text_entity ----------------------------

select * 
from mag.catalog_category_entity_text_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_varchar_entity ----------------------------

select * 
from mag.catalog_category_entity_varchar_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

