use Landing
go 

------------------------------------------------------------------------
----------------------- customer_entity_flat ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.customer_entity_flat_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.customer_entity_flat_aud

	select idETLBatchRun, count(*) num_rows
	from mag.customer_entity_flat_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.customer_entity_flat_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, increment_id, email, 
			entity_type_id, attribute_set_id, 
			group_id, website_group_id, website_id, store_id, 
			is_active, disable_auto_group_change, 
			created_at, updated_at, 
			created_in, 
			prefix, firstname, middlename, lastname, suffix, 
			cus_phone, 
			dob, gender,
			default_billing, default_shipping, 
			unsubscribe_all, unsubscribe_all_date, 
			days_worn_info, 
			old_access_cust_no, old_web_cust_no, old_customer_id, 
			found_us_info, referafriend_code,
			taxvat,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_entity_flat_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- customer_address_entity_flat ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.customer_address_entity_flat_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.customer_address_entity_flat_aud

	select idETLBatchRun, count(*) num_rows
	from mag.customer_address_entity_flat_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.customer_address_entity_flat_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, increment_id, parent_id, 
			entity_type_id, attribute_set_id, 
			is_active, 
			created_at, updated_at, 
			prefix, firstname, middlename, lastname, suffix, 
			company, 
			street, city, postcode, region_id, region, country_id, 
			telephone, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_flat_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- catalog_category_entity_flat ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_flat_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_category_entity_flat_aud

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_flat_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_category_entity_flat_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id, store_id) num_rows_rep,
			entity_id, store_id, 
			parent_id, 
			entity_type_id, attribute_set_id, 
			position, level, children_count, path, 
			created_at, updated_at, 
			is_active, 
			name, url_path, url_key, display_mode, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_flat_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- catalog_product_entity_flat ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_flat_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_product_entity_flat_aud

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_flat_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_product_entity_flat_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id, store_id) num_rows_rep,
			entity_id, store_id,
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			manufacturer, 
			name, url_path, url_key, 
			status, product_type, product_lifecycle, 
			promotional_product, daysperlens, 
			tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
			equivalence, equivalent_sku, price_comp_price, glasses_colour,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_flat_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom

