use Landing
go 

------------------------------------------------------------------------
----------------------- customer_entity_flat ----------------------------
------------------------------------------------------------------------

select entity_id, increment_id, email, 
	entity_type_id, attribute_set_id, 
	group_id, website_group_id, website_id, store_id, 
	is_active, disable_auto_group_change, 
	created_at, updated_at, 
	created_in, 
	prefix, firstname, middlename, lastname, suffix, 
	cus_phone, 
	dob, gender,
	default_billing, default_shipping, 
	unsubscribe_all, unsubscribe_all_date, 
	days_worn_info, 
	old_access_cust_no, old_web_cust_no, old_customer_id, 
	found_us_info, referafriend_code, 
	idETLBatchRun, ins_ts, upd_ts
from mag.customer_entity_flat_aud

	select entity_type_id, attribute_set_id, count(*)
	from mag.customer_entity_flat_aud
	group by entity_type_id, attribute_set_id 
	order by entity_type_id, attribute_set_id

	select group_id, website_group_id, count(*)
	from mag.customer_entity_flat_aud
	group by group_id, website_group_id
	order by group_id, website_group_id

	select website_id, store_id, count(*)
	from mag.customer_entity_flat_aud
	group by website_id, store_id
	order by website_id, store_id

	select is_active, disable_auto_group_change, count(*)
	from mag.customer_entity_flat_aud
	group by is_active, disable_auto_group_change
	order by is_active, disable_auto_group_change

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.customer_entity_flat_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.customer_entity_flat_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	select unsubscribe_all, count(*)
	from mag.customer_entity_flat_aud
	-- where unsubscribe_all_date is null
	where unsubscribe_all_date is not null
	group by unsubscribe_all
	order by unsubscribe_all

	select year(unsubscribe_all_date) yyyy, month(unsubscribe_all_date) mm, count(*)
	from mag.customer_entity_flat_aud
	group by year(unsubscribe_all_date), month(unsubscribe_all_date) 
	order by year(unsubscribe_all_date), month(unsubscribe_all_date) 

	select days_worn_info, count(*)
	from mag.customer_entity_flat_aud
	group by days_worn_info
	order by days_worn_info

	select old_access_cust_no, count(*)
	from mag.customer_entity_flat_aud
	group by old_access_cust_no
	order by old_access_cust_no

	select found_us_info, count(*)
	from mag.customer_entity_flat_aud
	group by found_us_info
	order by found_us_info


	select dob, count(*)
	from mag.customer_entity_flat_aud
	group by dob
	order by dob

	select gender, count(*)
	from mag.customer_entity_flat_aud
	group by gender
	order by gender

------------------------------------------------------------------------
----------------------- customer_address_entity_flat ----------------------------
------------------------------------------------------------------------

select entity_id, increment_id, parent_id, 
	entity_type_id, attribute_set_id, 
	is_active, 
	created_at, updated_at, 
	prefix, firstname, middlename, lastname, suffix, 
	company, 
	street, city, postcode, region_id, region, country_id, 
	telephone, 
	idETLBatchRun, ins_ts, upd_ts
from mag.customer_address_entity_flat_aud

	select entity_type_id, attribute_set_id, count(*)
	from mag.customer_address_entity_flat_aud
	group by entity_type_id, attribute_set_id 
	order by entity_type_id, attribute_set_id

	select is_active, count(*)
	from mag.customer_address_entity_flat_aud
	group by is_active
	order by is_active

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.customer_address_entity_flat_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.customer_address_entity_flat_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	select region_id, count(*)
	from mag.customer_address_entity_flat_aud
	group by region_id
	order by region_id

	select region, count(*)
	from mag.customer_address_entity_flat_aud
	group by region
	order by region

	select country_id, count(*)
	from mag.customer_address_entity_flat_aud
	group by country_id
	order by country_id

	select entity_id, 
		prefix, firstname, middlename, lastname, suffix, 
		company, 
		street, city, postcode, region_id, region, country_id, 
		telephone, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_flat_aud
	where region_id is not null and region_id <> 0

	select region_id, region, country_id, count(*)
	from mag.customer_address_entity_flat_aud
	where region_id is not null and region_id <> 0
	group by region_id, region, country_id
	order by region_id, region, country_id

------------------------------------------------------------------------
----------------------- catalog_category_entity_flat ----------------------------
------------------------------------------------------------------------

select entity_id, store_id, parent_id, 
	entity_type_id, attribute_set_id, 
	position, level, children_count, path, 
	created_at, updated_at, 
	is_active, 
	name, url_path, url_key, display_mode, 
	idETLBatchRun, ins_ts, upd_ts
from mag.catalog_category_entity_flat_aud
where store_id = 0

	select entity_id, count(*)
	from mag.catalog_category_entity_flat_aud
	group by entity_id
	order by entity_id

	select store_id, count(*)
	from mag.catalog_category_entity_flat_aud
	group by store_id
	order by store_id

	select entity_type_id, attribute_set_id, count(*)
	from mag.catalog_category_entity_flat_aud
	group by entity_type_id, attribute_set_id
	order by entity_type_id, attribute_set_id

	select level, count(*)
	from mag.catalog_category_entity_flat_aud
	group by level
	order by level

	select is_active, count(*)
	from mag.catalog_category_entity_flat_aud
	group by is_active
	order by is_active

	select display_mode, count(*)
	from mag.catalog_category_entity_flat_aud
	group by display_mode
	order by display_mode


------------------------------------------------------------------------
----------------------- catalog_product_entity_flat ----------------------------
------------------------------------------------------------------------

select entity_id, store_id, 
	entity_type_id, attribute_set_id, 
	type_id, sku, required_options, has_options, 
	created_at, updated_at, 
	manufacturer, 
	name, url_path, url_key, 
	status, product_type, product_lifecycle, 
	promotional_product, daysperlens, 
	tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
	equivalence, equivalent_sku, price_comp_price, glasses_colour,
	idETLBatchRun, ins_ts, upd_ts
from mag.catalog_product_entity_flat_aud
where store_id = 0
-- where entity_id = 1083
order by entity_id

	select entity_id, count(*)
	from mag.catalog_product_entity_flat_aud
	group by entity_id
	order by entity_id

	select store_id, count(*)
	from mag.catalog_product_entity_flat_aud
	group by store_id
	order by store_id

	select entity_type_id, attribute_set_id, count(*)
	from mag.catalog_product_entity_flat_aud
	group by entity_type_id, attribute_set_id
	order by entity_type_id, attribute_set_id

	select required_options, has_options, count(*)
	from mag.catalog_product_entity_flat_aud
	group by required_options, has_options
	order by required_options, has_options

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.catalog_product_entity_flat_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.catalog_product_entity_flat_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	select status, count(*)
	from mag.catalog_product_entity_flat_aud
	group by status
	order by status

	select product_type, count(*)
	from mag.catalog_product_entity_flat_aud
	group by product_type
	order by product_type

	select product_lifecycle, count(*)
	from mag.catalog_product_entity_flat_aud
	group by product_lifecycle
	order by product_lifecycle

	select promotional_product, count(*)
	from mag.catalog_product_entity_flat_aud
	group by promotional_product
	order by promotional_product

	select daysperlens, count(*)
	from mag.catalog_product_entity_flat_aud
	group by daysperlens
	order by daysperlens

	select tax_class_id, count(*)
	from mag.catalog_product_entity_flat_aud
	group by tax_class_id
	order by tax_class_id

	select visibility, count(*)
	from mag.catalog_product_entity_flat_aud
	group by visibility
	order by visibility

	select is_lens, count(*)
	from mag.catalog_product_entity_flat_aud
	group by is_lens
	order by is_lens

	select stocked_lens, count(*)
	from mag.catalog_product_entity_flat_aud
	group by stocked_lens
	order by stocked_lens

	select telesales_only, count(*)
	from mag.catalog_product_entity_flat_aud
	group by telesales_only
	order by telesales_only

	select condition, count(*)
	from mag.catalog_product_entity_flat_aud
	group by condition
	order by condition

