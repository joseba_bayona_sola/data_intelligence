use Landing
go 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_sales_flat_invoice
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	01-09-2017	Joseba Bayona Sola	Add mutual_amount attribute
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_invoice
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_invoice
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, 
				total_qty, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, base_total_refunded, 
				mutual_amount,
				base_to_global_rate, base_to_order_rate, order_currency_code,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, 
				total_qty, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, base_total_refunded, 
				mutual_amount,
				base_to_global_rate, base_to_order_rate, order_currency_code
			from magento01.sales_flat_invoice ' + -- ' + // '')'
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


----

drop procedure mag.srcmag_lnd_get_sales_flat_invoice_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_invoice_item
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_invoice_item
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, order_item_id, parent_id, 
				product_id, sku, name, description, 
				qty, 
				base_price, price, base_price_incl_tax, 
				base_cost, 
				base_row_total, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select ii.entity_id, ii.order_item_id, ii.parent_id, 
				ii.product_id, ii.sku, ii.name, ii.description, 
				ii.qty, 
				ii.base_price, ii.price, ii.base_price_incl_tax, 
				ii.base_cost, 
				ii.base_row_total 
			from 
				magento01.sales_flat_invoice i
			inner join
				magento01.sales_flat_invoice_item ii on i.entity_id = ii.parent_id	 ' + -- ' + // '')'
			'where i.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or i.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 