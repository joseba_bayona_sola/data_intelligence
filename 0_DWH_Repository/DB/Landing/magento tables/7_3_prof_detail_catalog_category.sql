use Landing
go 

------------------------------------------------------------------------
----------------------- catalog_category_entity ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_category_entity_aud

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_category_entity_aud_hist

	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, parent_id, 
			entity_type_id, attribute_set_id, 
			position, level, children_count, path, 
			created_at, updated_at, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- catalog_category_entity_datetime ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_datetime_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_datetime_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_entity_datetime_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_datetime_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_datetime_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_datetime_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_category_entity_datetime_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_category_entity_decimal ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_decimal_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_decimal_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_entity_decimal_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_decimal_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_decimal_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_decimal_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_category_entity_decimal_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_category_entity_int ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_int_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_int_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_entity_int_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_int_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_int_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_int_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_category_entity_int_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_category_entity_text ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_text_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_text_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_entity_text_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_text_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_text_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_text_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_category_entity_text_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_category_entity_varchar ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_entity_varchar_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_varchar_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_entity_varchar_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_entity_varchar_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_category_entity_varchar_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_entity_varchar_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_category_entity_varchar_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

