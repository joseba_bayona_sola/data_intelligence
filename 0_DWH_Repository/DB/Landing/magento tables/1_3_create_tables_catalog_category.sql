use Landing
go

--------------------- Tables ----------------------------------

----------------------- catalog_category_entity ----------------------------

	create table mag.catalog_category_entity(
		entity_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity add constraint [PK_mag_catalog_category_entity]
		primary key clustered (entity_id);
	go
	alter table mag.catalog_category_entity add constraint [DF_mag_catalog_category_entity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_aud(
		entity_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_aud add constraint [PK_mag_catalog_category_entity_aud]
		primary key clustered (entity_id);
	go
	alter table mag.catalog_category_entity_aud add constraint [DF_mag_catalog_category_entity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_aud_hist(
		entity_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_aud_hist add constraint [DF_mag_catalog_category_entity_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- catalog_category_entity_datetime ----------------------------

	create table mag.catalog_category_entity_datetime(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.catalog_category_entity_datetime add constraint [PK_mag_catalog_category_entity_datetime]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_datetime add constraint [DF_mag_catalog_category_entity_datetime_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_datetime_aud(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go

	alter table mag.catalog_category_entity_datetime_aud add constraint [PK_mag_catalog_category_entity_datetime_aud]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_datetime_aud add constraint [DF_mag_catalog_category_entity_datetime_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_datetime_aud_hist(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						datetime, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.catalog_category_entity_datetime_aud_hist add constraint [DF_mag_catalog_category_entity_datetime_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	----------------------- catalog_category_entity_decimal ----------------------------

	create table mag.catalog_category_entity_decimal(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						decimal(12, 4), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_decimal add constraint [PK_mag_catalog_category_entity_decimal]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_decimal add constraint [DF_mag_catalog_category_entity_decimal_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_decimal_aud(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						decimal(12, 4), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_decimal_aud add constraint [PK_mag_catalog_category_entity_decimal_aud]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_decimal_aud add constraint [DF_mag_catalog_category_entity_decimal_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_decimal_aud_hist(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						decimal(12, 4), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_decimal_aud_hist add constraint [DF_mag_catalog_category_entity_decimal_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- catalog_category_entity_int ----------------------------

	create table mag.catalog_category_entity_int(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						int, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_int add constraint [PK_mag_catalog_category_entity_int]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_int add constraint [DF_mag_catalog_category_entity_int_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_int_aud(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						int, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_int_aud add constraint [PK_mag_catalog_category_entity_int_aud]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_int_aud add constraint [DF_mag_catalog_category_entity_int_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_int_aud_hist(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						int, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_int_aud_hist add constraint [DF_mag_catalog_category_entity_int_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- catalog_category_entity_text ----------------------------

	create table mag.catalog_category_entity_text(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(1000), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_text add constraint [PK_mag_catalog_category_entity_text]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_text add constraint [DF_mag_catalog_category_entity_text_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_text_aud(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(1000), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_text_aud add constraint [PK_mag_catalog_category_entity_text_aud]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_text_aud add constraint [DF_mag_catalog_category_entity_text_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_text_aud_hist(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(1000), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_text_aud_hist add constraint [DF_mag_catalog_category_entity_text_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- catalog_category_entity_varchar ----------------------------

	create table mag.catalog_category_entity_varchar(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_varchar add constraint [PK_mag_catalog_category_entity_varchar]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_varchar add constraint [DF_mag_catalog_category_entity_varchar_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_varchar_aud(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_varchar_aud add constraint [PK_mag_catalog_category_entity_varchar_aud]
		primary key clustered (value_id);
	go
	alter table mag.catalog_category_entity_varchar_aud add constraint [DF_mag_catalog_category_entity_varchar_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_varchar_aud_hist(
		value_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_id				int NOT NULL, 
		entity_id					int NOT NULL, 
		store_id					int NOT NULL,
		value						varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_varchar_aud_hist add constraint [DF_mag_catalog_category_entity_varchar_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

