use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_creditmemo ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_creditmemo_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_creditmemo_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_creditmemo_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_creditmemo_aud_hist

	select *
	from
		(select count(*) over(partition by entity_id) num_rep, *
		from mag.sales_flat_creditmemo_aud_hist) t
	where num_rep > 1
	order by num_rep desc, entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- sales_flat_creditmemo_item ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_creditmemo_item_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_creditmemo_item_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_creditmemo_item_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_creditmemo_item_aud_hist

	select *
	from
		(select count(*) over(partition by entity_id) num_rep, *
		from mag.sales_flat_creditmemo_item_aud_hist) t
	where num_rep > 1
	order by num_rep desc, entity_id, aud_dateFrom

