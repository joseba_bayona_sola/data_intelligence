use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- customer_entity_flat ----------------------------

	drop trigger mag.trg_customer_entity_flat;
	go 

	create trigger mag.trg_customer_entity_flat on mag.customer_entity_flat
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_flat_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.increment_id, ' '), isnull(trg.email, ' '), 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.group_id, 0), isnull(trg.website_group_id, 0), isnull(trg.website_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.is_active, 0), isnull(trg.disable_auto_group_change, 0), 
				isnull(trg.created_at, ' '), -- isnull(trg.updated_at, ' '), 
				isnull(trg.created_in, ' '), 
				isnull(trg.prefix, ' '), isnull(trg.firstname, ' '), isnull(trg.middlename, ' '), isnull(trg.lastname, ' '), isnull(trg.suffix, ' '), 
				isnull(trg.cus_phone, ' '), 
				isnull(trg.dob, ' '), isnull(trg.gender, 0), 
				isnull(trg.default_billing, 0), isnull(trg.default_shipping, 0), 
				isnull(trg.unsubscribe_all, ' '), isnull(trg.unsubscribe_all_date, ' '), 
				isnull(trg.days_worn_info, 0), 
				isnull(trg.old_access_cust_no, ' '), isnull(trg.old_web_cust_no, ' '), isnull(trg.old_customer_id, ' '), 
				isnull(trg.found_us_info, ' '), isnull(trg.referafriend_code, ' '), 
				isnull(trg.taxvat, ' ')
			intersect
			select 
				isnull(src.increment_id, ' '), isnull(src.email, ' '), 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.group_id, 0), isnull(src.website_group_id, 0), isnull(src.website_id, 0), isnull(src.store_id, 0), 
				isnull(src.is_active, 0), isnull(src.disable_auto_group_change, 0), 
				isnull(src.created_at, ' '), -- isnull(src.updated_at, ' '), 
				isnull(src.created_in, ' '), 
				isnull(src.prefix, ' '), isnull(src.firstname, ' '), isnull(src.middlename, ' '), isnull(src.lastname, ' '), isnull(src.suffix, ' '), 
				isnull(src.cus_phone, ' '), 
				isnull(trg.dob, ' '), isnull(trg.gender, 0), 
				isnull(src.default_billing, 0), isnull(src.default_shipping, 0), 
				isnull(src.unsubscribe_all, ' '), isnull(src.unsubscribe_all_date, ' '), 
				isnull(src.days_worn_info, 0), 
				isnull(src.old_access_cust_no, ' '), isnull(src.old_web_cust_no, ' '), isnull(src.old_customer_id, ' '), 
				isnull(src.found_us_info, ' '), isnull(src.referafriend_code, ' '),
				isnull(src.taxvat, ' ')) 
			
			then
				update set
					trg.increment_id = src.increment_id, trg.email = src.email, 
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.group_id = src.group_id, trg.website_group_id = src.website_group_id, trg.website_id = src.website_id, trg.store_id = src.store_id, 
					trg.is_active = src.is_active, trg.disable_auto_group_change = src.disable_auto_group_change, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.created_in = src.created_in, 
					trg.prefix = src.prefix, trg.firstname = src.firstname, trg.middlename = src.middlename, trg.lastname = src.lastname, trg.suffix = src.suffix, 
					trg.cus_phone = src.cus_phone, 
					trg.dob = src.dob, trg.gender = src.gender, 
					trg.default_billing = src.default_billing, trg.default_shipping = src.default_shipping, 
					trg.unsubscribe_all = src.unsubscribe_all, trg.unsubscribe_all_date = src.unsubscribe_all_date, 
					trg.days_worn_info = src.days_worn_info, 
					trg.old_access_cust_no = src.old_access_cust_no, trg.old_web_cust_no = src.old_web_cust_no, trg.old_customer_id = src.old_customer_id, 
					trg.found_us_info = src.found_us_info, trg.referafriend_code = src.referafriend_code, 
					trg.taxvat = src.taxvat, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					increment_id, email, 
					entity_type_id, attribute_set_id, 
					group_id, website_group_id, website_id, store_id, 
					is_active, disable_auto_group_change, 
					created_at, updated_at, 
					created_in, 
					prefix, firstname, middlename, lastname, suffix, 
					cus_phone, 
					dob, gender,
					default_billing, default_shipping, 
					unsubscribe_all, unsubscribe_all_date, 
					days_worn_info, 
					old_access_cust_no, old_web_cust_no, old_customer_id, 
					found_us_info, referafriend_code, 
					taxvat,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.increment_id, src.email, 
						src.entity_type_id, src.attribute_set_id, 
						src.group_id, src.website_group_id, src.website_id, src.store_id, 
						src.is_active, src.disable_auto_group_change, 
						src.created_at, src.updated_at, 
						src.created_in, 
						src.prefix, src.firstname, src.middlename, src.lastname, src.suffix, 
						src.cus_phone, 
						src.dob, src.gender,
						src.default_billing, src.default_shipping, 
						src.unsubscribe_all, src.unsubscribe_all_date, 
						src.days_worn_info, 
						src.old_access_cust_no, src.old_web_cust_no, src.old_customer_id, 
						src.found_us_info, src.referafriend_code, 
						src.taxvat, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_flat_aud;
	go

	create trigger mag.trg_customer_entity_flat_aud on mag.customer_entity_flat_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_flat_aud_hist (entity_id, 
			increment_id, email, 
			entity_type_id, attribute_set_id, 
			group_id, website_group_id, website_id, store_id, 
			is_active, disable_auto_group_change, 
			created_at, updated_at, 
			created_in, 
			prefix, firstname, middlename, lastname, suffix, 
			cus_phone, 
			dob, gender,
			default_billing, default_shipping, 
			unsubscribe_all, unsubscribe_all_date, 
			days_worn_info, 
			old_access_cust_no, old_web_cust_no, old_customer_id, 
			found_us_info, referafriend_code, 
			taxvat,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.increment_id, d.email, 
				d.entity_type_id, d.attribute_set_id, 
				d.group_id, d.website_group_id, d.website_id, d.store_id, 
				d.is_active, d.disable_auto_group_change, 
				d.created_at, d.updated_at, 
				d.created_in, 
				d.prefix, d.firstname, d.middlename, d.lastname, d.suffix, 
				d.cus_phone, 
				d.dob, d.gender,
				d.default_billing, d.default_shipping, 
				d.unsubscribe_all, d.unsubscribe_all_date, 
				d.days_worn_info, 
				d.old_access_cust_no, d.old_web_cust_no, d.old_customer_id, 
				d.found_us_info, d.referafriend_code, 
				d.taxvat,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

------------------------- customer_address_entity_flat ----------------------------

	drop trigger mag.trg_customer_address_entity_flat;
	go 

	create trigger mag.trg_customer_address_entity_flat on mag.customer_address_entity_flat
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_address_entity_flat_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.increment_id, ' '), isnull(trg.parent_id, 0), 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.is_active, 0), 
				isnull(trg.created_at, ' '), -- isnull(trg.updated_at, ' '), 
				isnull(trg.prefix, ' '), isnull(trg.firstname, ' '), isnull(trg.middlename, ' '), isnull(trg.lastname, ' '), isnull(trg.suffix, ' '), 
				isnull(trg.company, ' '), 
				isnull(trg.street, ' '), isnull(trg.city, ' '), isnull(trg.postcode, ' '), isnull(trg.region_id, 0), isnull(trg.region, ' '), isnull(trg.country_id, ' '), 
				isnull(trg.telephone, ' ')
			intersect
			select 
				isnull(src.increment_id, ' '), isnull(src.parent_id, 0), 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.is_active, 0), 
				isnull(src.created_at, ' '), -- isnull(src.updated_at, ' '), 
				isnull(src.prefix, ' '), isnull(src.firstname, ' '), isnull(src.middlename, ' '), isnull(src.lastname, ' '), isnull(src.suffix, ' '), 
				isnull(src.company, ' '), 
				isnull(src.street, ' '), isnull(src.city, ' '), isnull(src.postcode, ' '), isnull(src.region_id, 0), isnull(src.region, ' '), isnull(src.country_id, ' '), 
				isnull(src.telephone, ' '))			
			then
				update set
					trg.increment_id = src.increment_id, trg.parent_id = src.parent_id, 
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.is_active = src.is_active, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.prefix = src.prefix, trg.firstname = src.firstname, trg.middlename = src.middlename, trg.lastname = src.lastname, trg.suffix = src.suffix, 
					trg.company = src.company, 
					trg.street = src.street, trg.city = src.city, trg.postcode = src.postcode, trg.region_id = src.region_id, trg.region = src.region, trg.country_id = src.country_id, 
					trg.telephone = src.telephone,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					increment_id, parent_id, 
					entity_type_id, attribute_set_id, 
					is_active, 
					created_at, updated_at, 
					prefix, firstname, middlename, lastname, suffix, 
					company, 
					street, city, postcode, region_id, region, country_id, 
					telephone,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.increment_id, src.parent_id, 
						src.entity_type_id, src.attribute_set_id, 
						src.is_active, 
						src.created_at, src.updated_at, 
						src.prefix, src.firstname, src.middlename, src.lastname, src.suffix, 
						src.company, 
						src.street, src.city, src.postcode, src.region_id, src.region, src.country_id, 
						src.telephone,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_address_entity_flat_aud;
	go

	create trigger mag.trg_customer_address_entity_flat_aud on mag.customer_address_entity_flat_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_address_entity_flat_aud_hist (entity_id, 
			increment_id, parent_id, 
			entity_type_id, attribute_set_id, 
			is_active, 
			created_at, updated_at, 
			prefix, firstname, middlename, lastname, suffix, 
			company, 
			street, city, postcode, region_id, region, country_id, 
			telephone,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.increment_id, d.parent_id, 
				d.entity_type_id, d.attribute_set_id, 
				d.is_active, 
				d.created_at, d.updated_at, 
				d.prefix, d.firstname, d.middlename, d.lastname, d.suffix, 
				d.company, 
				d.street, d.city, d.postcode, d.region_id, d.region, d.country_id, 
				d.telephone,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

------------------------- catalog_category_entity_flat ----------------------------

	drop trigger mag.trg_catalog_category_entity_flat;
	go 

	create trigger mag.trg_catalog_category_entity_flat on mag.catalog_category_entity_flat
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_flat_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id and trg.store_id = src.store_id)
		when matched and not exists
			(select 
				isnull(trg.parent_id, 0), 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.position, 0), isnull(trg.level, 0), isnull(trg.children_count, 0), isnull(trg.path, ' '), 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' '), 
				isnull(trg.is_active, 0), 
				isnull(trg.name, ' '), isnull(trg.url_path, ' '), isnull(trg.url_key, ' '), isnull(trg.display_mode, ' ')
			intersect
			select 
				isnull(src.parent_id, 0), 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.position, 0), isnull(src.level, 0), isnull(src.children_count, 0), isnull(src.path, ' '), 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '), 
				isnull(src.is_active, 0), 
				isnull(src.name, ' '), isnull(src.url_path, ' '), isnull(src.url_key, ' '), isnull(src.display_mode, ' '))			
			then
				update set
					trg.parent_id = src.parent_id, 
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.position = src.position, trg.level = src.level, trg.children_count = src.children_count, trg.path = src.path, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.is_active = src.is_active, 
					trg.name = src.name, trg.url_path = src.url_path, trg.url_key = src.url_key, trg.display_mode = src.display_mode, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, store_id, 
					parent_id, 
					entity_type_id, attribute_set_id, 
					position, level, children_count, path, 
					created_at, updated_at, 
					is_active, 
					name, url_path, url_key, display_mode, 
					idETLBatchRun)
					
					values (src.entity_id, src.store_id, 
						src.parent_id, 
						src.entity_type_id, src.attribute_set_id, 
						src.position, src.level, src.children_count, src.path, 
						src.created_at, src.updated_at, 
						src.is_active, 
						src.name, src.url_path, src.url_key, src.display_mode, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_flat_aud;
	go

	create trigger mag.trg_catalog_category_entity_flat_aud on mag.catalog_category_entity_flat_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_flat_aud_hist (entity_id, store_id, 
			parent_id, 
			entity_type_id, attribute_set_id, 
			position, level, children_count, path, 
			created_at, updated_at, 
			is_active, 
			name, url_path, url_key, display_mode, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.store_id, 
				d.parent_id, 
				d.entity_type_id, d.attribute_set_id, 
				d.position, d.level, d.children_count, d.path, 
				d.created_at, d.updated_at, 
				d.is_active, 
				d.name, d.url_path, d.url_key, d.display_mode, 
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id and d.store_id = i.store_id;
	end;
	go

------------------------- catalog_product_entity_flat ----------------------------

	drop trigger mag.trg_catalog_product_entity_flat;
	go 

	create trigger mag.trg_catalog_product_entity_flat on mag.catalog_product_entity_flat
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_flat_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id and trg.store_id = src.store_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.type_id, ' '), isnull(trg.sku, ' '), isnull(trg.required_options, 0), isnull(trg.has_options, 0), 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' '), 
				isnull(trg.manufacturer, 0), 
				isnull(trg.name, ' '), isnull(trg.url_path, ' '), isnull(trg.url_key, ' '), 
				isnull(trg.status, 0), isnull(trg.product_type, ' '), isnull(trg.product_lifecycle, ' '), 
				isnull(trg.promotional_product, 0), isnull(trg.daysperlens, ' '), 
				isnull(trg.tax_class_id, 0), isnull(trg.visibility, 0), isnull(trg.is_lens, 0), isnull(trg.stocked_lens, 0), isnull(trg.telesales_only, 0), isnull(trg.condition, 0), 
				isnull(trg.equivalence, ' '), isnull(trg.equivalent_sku, ' '), isnull(trg.price_comp_price, ' '), isnull(trg.glasses_colour, 0)
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.type_id, ' '), isnull(src.sku, ' '), isnull(src.required_options, 0), isnull(src.has_options, 0), 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '), 
				isnull(src.manufacturer, 0), 
				isnull(src.name, ' '), isnull(src.url_path, ' '), isnull(src.url_key, ' '), 
				isnull(src.status, 0), isnull(src.product_type, ' '), isnull(src.product_lifecycle, ' '), 
				isnull(src.promotional_product, 0), isnull(src.daysperlens, ' '), 
				isnull(src.tax_class_id, 0), isnull(src.visibility, 0), isnull(src.is_lens, 0), isnull(src.stocked_lens, 0), isnull(src.telesales_only, 0), isnull(src.condition, 0), 
				isnull(src.equivalence, ' '), isnull(src.equivalent_sku, ' '), isnull(src.price_comp_price, ' '), isnull(src.glasses_colour, 0))			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.type_id = src.type_id, trg.sku = src.sku, trg.required_options = src.required_options, trg.has_options = src.has_options, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.manufacturer = src.manufacturer, 
					trg.name = src.name, trg.url_path = src.url_path, trg.url_key = src.url_key, 
					trg.status = src.status, trg.product_type = src.product_type, trg.product_lifecycle = src.product_lifecycle, 
					trg.promotional_product = src.promotional_product, trg.daysperlens = src.daysperlens, 
					trg.tax_class_id = src.tax_class_id, trg.visibility = src.visibility, trg.is_lens = src.is_lens, trg.stocked_lens = src.stocked_lens, trg.telesales_only = src.telesales_only, trg.condition = src.condition, 
					trg.equivalence = src.equivalence, trg.equivalent_sku = src.equivalent_sku, trg.price_comp_price = src.price_comp_price, trg.glasses_colour = src.glasses_colour,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, store_id, 
					entity_type_id, attribute_set_id, 
					type_id, sku, required_options, has_options, 
					created_at, updated_at, 
					manufacturer, 
					name, url_path, url_key, 
					status, product_type, product_lifecycle, 
					promotional_product, daysperlens, 
					tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
					equivalence, equivalent_sku, price_comp_price, glasses_colour,
					idETLBatchRun)
					
					values (src.entity_id, src.store_id, 
						src.entity_type_id, src.attribute_set_id, 
						src.type_id, src.sku, src.required_options, src.has_options, 
						src.created_at, src.updated_at, 
						src.manufacturer, 
						src.name, src.url_path, src.url_key, 
						src.status, src.product_type, src.product_lifecycle, 
						src.promotional_product, src.daysperlens, 
						src.tax_class_id, src.visibility, src.is_lens, src.stocked_lens, src.telesales_only, src.condition, 
						src.equivalence, src.equivalent_sku, src.price_comp_price, src.glasses_colour,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_flat_aud;
	go

	create trigger mag.trg_catalog_product_entity_flat_aud on mag.catalog_product_entity_flat_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_flat_aud_hist (entity_id, store_id,
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			manufacturer, 
			name, url_path, url_key, 
			status, product_type, product_lifecycle, 
			promotional_product, daysperlens, 
			tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
			equivalence, equivalent_sku, price_comp_price, glasses_colour,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.store_id, 
				d.entity_type_id, d.attribute_set_id, 
				d.type_id, d.sku, d.required_options, d.has_options, 
				d.created_at, d.updated_at, 
				d.manufacturer, 
				d.name, d.url_path, d.url_key, 
				d.status, d.product_type, d.product_lifecycle, 
				d.promotional_product, d.daysperlens, 
				d.tax_class_id, d.visibility, d.is_lens, d.stocked_lens, d.telesales_only, d.condition, 
				d.equivalence, d.equivalent_sku, d.price_comp_price, d.glasses_colour,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id and d.store_id = i.store_id;
	end;
	go

