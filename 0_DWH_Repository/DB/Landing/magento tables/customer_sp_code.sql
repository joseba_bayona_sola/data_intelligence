

declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

set @d1 = convert(datetime, '2017/02/11', 120)
set @d2 = convert(datetime, '2017/02/12', 120)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

set @sql = 'select * from openquery(MAGENTO, ' + 
	'''select entity_id, increment_id, email, entity_type_id, attribute_set_id, 
		group_id, website_group_id, website_id, store_id, is_active, disable_auto_group_change, 
		created_at, updated_at ' + 
	'from magento01.customer_entity ' + 
	'where created_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')
		or updated_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')
	'')'

print (@sql)

exec(@sql)


-------------------------------------------

declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

set @d1 = convert(datetime, '2017/02/11', 120)
set @d2 = convert(datetime, '2017/02/12', 120)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

set @sql = 'select * from openquery(MAGENTO, ' + 
	'''select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.value ' + 
	'from 
			magento01.customer_entity ce 
		inner join
			magento01.customer_entity_int cei on ce.entity_id = cei.entity_id ' + 
	'where ce.created_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')
		or ce.updated_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')
	'')'

print (@sql)

exec(@sql)


	