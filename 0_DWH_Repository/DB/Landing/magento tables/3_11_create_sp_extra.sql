use Landing
go

drop procedure mag.srcmag_lnd_get_review_entity
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - review_entity
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_review_entity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, entity_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+   'from openquery(MAGENTO,
				''select entity_id, entity_code 
				from magento01.review_entity'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.srcmag_lnd_get_review_status
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - review_status
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_review_status
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select status_id, status_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+   'from openquery(MAGENTO,
				''select status_id, status_code 
				from magento01.review_status'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.srcmag_lnd_get_review
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - review
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_review
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select review_id, created_at, entity_id, status_id, entity_pk_value, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select review_id, created_at, entity_id, status_id, entity_pk_value 
				from magento01.review'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.srcmag_lnd_get_review_store
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - review_store
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_review_store
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select review_id, store_id, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select review_id, store_id 
				from magento01.review_store'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.srcmag_lnd_get_review_detail
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - review_detail
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_review_detail
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select detail_id, review_id, store_id, 
				title, detail, nickname, customer_id, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select detail_id, review_id, store_id, 
					title, detail, nickname, customer_id 
				from magento01.review_detail'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go









drop procedure mag.srcmag_lnd_get_rating_entity
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - rating_entity
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_rating_entity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, entity_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select entity_id, entity_code 
				from magento01.rating_entity'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.srcmag_lnd_get_rating
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - rating
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_rating
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select rating_id, entity_id, rating_code, position, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select rating_id, entity_id, rating_code, position 
				from magento01.rating'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.srcmag_lnd_get_rating_option
go 

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 10-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - rating_option
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_rating_option
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select option_id, rating_id, code, value, position, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select option_id, rating_id, code, value, position 
				from magento01.rating_option'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.srcmag_lnd_get_rating_option_vote
go

-- ==========================================================================================
-- Author: Juan Pablo Cano
-- Date: 12-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - rating_option_vote
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_rating_option_vote
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin

	set nocount on 
	
	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vote_id, 
				review_id, remote_ip, customer_id, entity_pk_value, 
				rating_id, option_id, percent_rating, value, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+	'from openquery(MAGENTO,
				''select vote_id, 
					review_id, remote_ip, customer_id, entity_pk_value, 
					rating_id, option_id, percent percent_rating, value 
				from magento01.rating_option_vote'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go