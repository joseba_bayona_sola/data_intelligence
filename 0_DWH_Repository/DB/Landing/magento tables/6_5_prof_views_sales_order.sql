use Landing
go 

----------------------- sales_flat_order ----------------------------

select * 
from mag.sales_flat_order_aud_v
where num_records > 1
order by num_records desc, entity_id, idETLBatchRun

----------------------- sales_flat_order_item ----------------------------

select * 
from mag.sales_flat_order_item_aud_v
where num_records > 1
order by num_records desc, order_id, item_id, idETLBatchRun

----------------------- sales_flat_order_payment ----------------------------

select * 
from mag.sales_flat_order_payment_aud_v
where num_records > 1
order by num_records desc, parent_id, entity_id, idETLBatchRun

----------------------- sales_flat_order_address ----------------------------

select * 
from mag.sales_flat_order_address_aud_v
where num_records > 1
order by num_records desc, parent_id, entity_id, idETLBatchRun

----------------------- sales_order_log ----------------------------

select * 
from mag.sales_order_log_aud_v
where num_records > 1
order by num_records desc, order_id, id, idETLBatchRun

