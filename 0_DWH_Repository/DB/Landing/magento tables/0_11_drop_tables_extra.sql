use Landing;
go


drop table mag.review_entity;
go
drop table mag.review_entity_aud;
go
drop table mag.review_entity_aud_hist;
go


drop table mag.review_status;
go
drop table mag.review_status_aud;
go
drop table mag.review_status_aud_hist;
go


drop table mag.review;
go
drop table mag.review_aud;
go
drop table mag.review_aud_hist;
go


drop table mag.review_store;
go
drop table mag.review_store_aud;
go
drop table mag.review_store_aud_hist;
go


drop table mag.review_detail;
go
drop table mag.review_detail_aud;
go
drop table mag.review_detail_aud_hist;
go






drop table mag.rating_entity;
go
drop table mag.rating_entity_aud;
go
drop table mag.rating_entity_aud_hist;
go


drop table mag.rating;
go
drop table mag.rating_aud;
go
drop table mag.rating_aud_hist;
go


drop table mag.rating_option;
go
drop table mag.rating_option_aud;
go
drop table mag.rating_option_aud_hist;
go

drop table mag.rating_option_vote;
go
drop table mag.rating_option_vote_aud;
go
drop table mag.rating_option_vote_aud_hist;
go
