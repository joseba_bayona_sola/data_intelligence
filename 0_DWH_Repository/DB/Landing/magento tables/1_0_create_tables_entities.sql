
use Landing
go

--------------------- Tables ----------------------------------

----------------------- eav_entity_type ----------------------------

	create table mag.eav_entity_type(
		entity_type_id					int NOT NULL, 
		entity_type_code				varchar(50) NOT NULL,
		entity_model					varchar(255) NOT NULL,
		attribute_model					varchar(255),
		entity_table					varchar(255),
		value_table_prefix				varchar(255),
		entity_id_field					varchar(255),
		is_data_sharing					int NOT NULL, 
		data_sharing_key				varchar(100),
		default_attribute_set_id		int NOT NULL, 
		increment_model					varchar(255),
		increment_per_store				int NOT NULL, 
		increment_pad_length			int NOT NULL, 
		increment_pad_char				varchar(1) NOT NULL,
		additional_attribute_table		varchar(255),
		entity_attribute_collection		varchar(255), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);
	go

	alter table mag.eav_entity_type add constraint [PK_mag_eav_entity_type]
		primary key clustered (entity_type_id);
	go
	alter table mag.eav_entity_type add constraint [DF_mag_eav_entity_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	create table mag.eav_entity_type_aud(
		entity_type_id					int NOT NULL, 
		entity_type_code				varchar(50) NOT NULL,
		entity_model					varchar(255) NOT NULL,
		attribute_model					varchar(255),
		entity_table					varchar(255),
		value_table_prefix				varchar(255),
		entity_id_field					varchar(255),
		is_data_sharing					int NOT NULL, 
		data_sharing_key				varchar(100),
		default_attribute_set_id		int NOT NULL, 
		increment_model					varchar(255),
		increment_per_store				int NOT NULL, 
		increment_pad_length			int NOT NULL, 
		increment_pad_char				varchar(1) NOT NULL,
		additional_attribute_table		varchar(255),
		entity_attribute_collection		varchar(255), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		upd_ts							datetime);
	go

	alter table mag.eav_entity_type_aud add constraint [PK_mag_eav_entity_type_aud]
		primary key clustered (entity_type_id);
	go
	alter table mag.eav_entity_type_aud add constraint [DF_mag_eav_entity_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_entity_type_aud_hist(
		entity_type_id					int NOT NULL, 
		entity_type_code				varchar(50) NOT NULL,
		entity_model					varchar(255) NOT NULL,
		attribute_model					varchar(255),
		entity_table					varchar(255),
		value_table_prefix				varchar(255),
		entity_id_field					varchar(255),
		is_data_sharing					int NOT NULL, 
		data_sharing_key				varchar(100),
		default_attribute_set_id		int NOT NULL, 
		increment_model					varchar(255),
		increment_per_store				int NOT NULL, 
		increment_pad_length			int NOT NULL, 
		increment_pad_char				varchar(1) NOT NULL,
		additional_attribute_table		varchar(255),
		entity_attribute_collection		varchar(255), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);
	go

	alter table mag.eav_entity_type_aud_hist add constraint [DF_mag_eav_entity_type_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- eav_attribute ----------------------------

	create table mag.eav_attribute(
		attribute_id			int NOT NULL,
		entity_type_id			int NOT NULL,
		attribute_code			varchar(255) NOT NULL,
		attribute_model			varchar(255),
		backend_model			varchar(255),
		backend_type			varchar(8) NOT NULL,
		backend_table			varchar(255),
		frontend_model			varchar(255),
		frontend_input			varchar(50),
		frontend_label			varchar(255),
		frontend_class			varchar(255),
		source_model			varchar(255),
		is_required				int NOT NULL,
		is_user_defined			int NOT NULL,
		default_value			varchar(1000),
		is_unique				int NOT NULL,
		note					varchar(255),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);

	alter table mag.eav_attribute add constraint [PK_mag_eav_attribute]
		primary key clustered (attribute_id);
	go
	alter table mag.eav_attribute add constraint [DF_mag_eav_attribute_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_aud(
		attribute_id			int NOT NULL,
		entity_type_id			int NOT NULL,
		attribute_code			varchar(255) NOT NULL,
		attribute_model			varchar(255),
		backend_model			varchar(255),
		backend_type			varchar(8) NOT NULL,
		backend_table			varchar(255),
		frontend_model			varchar(255),
		frontend_input			varchar(50),
		frontend_label			varchar(255),
		frontend_class			varchar(255),
		source_model			varchar(255),
		is_required				int NOT NULL,
		is_user_defined			int NOT NULL,
		default_value			varchar(1000),
		is_unique				int NOT NULL,
		note					varchar(255),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);

	alter table mag.eav_attribute_aud add constraint [PK_mag_eav_attribute_aud]
		primary key clustered (attribute_id);
	go
	alter table mag.eav_attribute_aud add constraint [DF_mag_eav_attribute_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_aud_hist(
		attribute_id			int NOT NULL,
		entity_type_id			int NOT NULL,
		attribute_code			varchar(255) NOT NULL,
		attribute_model			varchar(255),
		backend_model			varchar(255),
		backend_type			varchar(8) NOT NULL,
		backend_table			varchar(255),
		frontend_model			varchar(255),
		frontend_input			varchar(50),
		frontend_label			varchar(255),
		frontend_class			varchar(255),
		source_model			varchar(255),
		is_required				int NOT NULL,
		is_user_defined			int NOT NULL,
		default_value			varchar(1000),
		is_unique				int NOT NULL,
		note					varchar(255),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);

	alter table mag.eav_attribute_aud_hist add constraint [DF_mag_eav_attribute_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- eav_entity_attribute ----------------------------

	create table mag.eav_entity_attribute(
		entity_attribute_id			int NOT NULL, 
		entity_type_id				int NOT NULL, 	
		attribute_set_id			int NOT NULL, 
		attribute_group_id			int NOT NULL, 
		attribute_id				int NOT NULL, 
		sort_order					int NOT NULL, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.eav_entity_attribute add constraint [PK_mag_eav_entity_attribute]
		primary key clustered (entity_attribute_id);
	go
	alter table mag.eav_entity_attribute add constraint [DF_mag_eav_entity_attribute_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_entity_attribute_aud(
		entity_attribute_id			int NOT NULL, 
		entity_type_id				int NOT NULL, 	
		attribute_set_id			int NOT NULL, 
		attribute_group_id			int NOT NULL, 
		attribute_id				int NOT NULL, 
		sort_order					int NOT NULL, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL,
		upd_ts						datetime);
	go

	alter table mag.eav_entity_attribute_aud add constraint [PK_mag_eav_entity_attribute_aud]
		primary key clustered (entity_attribute_id);
	go
	alter table mag.eav_entity_attribute_aud add constraint [DF_mag_eav_entity_attribute_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_entity_attribute_aud_hist(
		entity_attribute_id			int NOT NULL, 
		entity_type_id				int NOT NULL, 	
		attribute_set_id			int NOT NULL, 
		attribute_group_id			int NOT NULL, 
		attribute_id				int NOT NULL, 
		sort_order					int NOT NULL, 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL,
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.eav_entity_attribute_aud_hist add constraint [DF_mag_eav_entity_attribute_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- eav_attribute_option ----------------------------

	create table mag.eav_attribute_option(
		option_id				int NOT NULL,
		attribute_id			int NOT NULL,
		sort_order				int NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go

	alter table mag.eav_attribute_option add constraint [PK_mag_eav_attribute_option]
		primary key clustered (option_id);
	go
	alter table mag.eav_attribute_option add constraint [DF_mag_eav_attribute_option_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_option_aud(
		option_id				int NOT NULL,
		attribute_id			int NOT NULL,
		sort_order				int NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);
	go

	alter table mag.eav_attribute_option_aud add constraint [PK_mag_eav_attribute_option_aud]
		primary key clustered (option_id);
	go
	alter table mag.eav_attribute_option_aud add constraint [DF_mag_eav_attribute_option_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_option_aud_hist(
		option_id				int NOT NULL,
		attribute_id			int NOT NULL,
		sort_order				int NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go

	alter table mag.eav_attribute_option_aud_hist add constraint [DF_mag_eav_attribute_option_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- eav_attribute_option_value ----------------------------

	create table mag.eav_attribute_option_value(
		value_id				int NOT NULL, 
		option_id				int NOT NULL, 
		store_id				int NOT NULL, 
		value					varchar(255) NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go


	alter table mag.eav_attribute_option_value add constraint [PK_mag_eav_attribute_option_value]
		primary key clustered (value_id);
	go
	alter table mag.eav_attribute_option_value add constraint [DF_mag_eav_attribute_option_value_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_option_value_aud(
		value_id				int NOT NULL, 
		option_id				int NOT NULL, 
		store_id				int NOT NULL, 
		value					varchar(255) NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);
	go


	alter table mag.eav_attribute_option_value_aud add constraint [PK_mag_eav_attribute_option_value_aud]
		primary key clustered (value_id);
	go
	alter table mag.eav_attribute_option_value_aud add constraint [DF_mag_eav_attribute_option_value_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.eav_attribute_option_value_aud_hist(
		value_id				int NOT NULL, 
		option_id				int NOT NULL, 
		store_id				int NOT NULL, 
		value					varchar(255) NOT NULL,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go


	alter table mag.eav_attribute_option_value_aud_hist add constraint [DF_mag_eav_attribute_option_value_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

