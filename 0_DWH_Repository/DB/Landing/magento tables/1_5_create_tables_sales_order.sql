
use Landing
go

--------------------- Tables ----------------------------------

----------------------- sales_flat_order ----------------------------

	create table mag.sales_flat_order(
		entity_id						int NOT NULL,
		increment_id					varchar(50),
		store_id						int,
		customer_id						int, 
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							varchar(32),
		status							varchar(32),
		total_qty_ordered				decimal(12,4),
		base_subtotal					decimal(12,4),
		base_subtotal_canceled			decimal(12,4),
		base_subtotal_invoiced			decimal(12,4),
		base_subtotal_refunded			decimal(12,4),
		base_shipping_amount			decimal(12,4),
		base_shipping_canceled			decimal(12,4),
		base_shipping_invoiced			decimal(12,4),
		base_shipping_refunded			decimal(12,4),
		base_discount_amount			decimal(12,4),
		base_discount_canceled			decimal(12,4),
		base_discount_invoiced			decimal(12,4),
		base_discount_refunded			decimal(12,4),
		base_customer_balance_amount	decimal(12,4),
		base_customer_balance_invoiced	decimal(12,4),
		base_customer_balance_refunded	decimal(12,4),
		base_grand_total				decimal(12,4),
		base_total_canceled				decimal(12,4),
		base_total_invoiced				decimal(12,4),
		base_total_invoiced_cost		decimal(12,4),
		base_total_refunded				decimal(12,4),
		base_total_offline_refunded		decimal(12,4),
		base_total_online_refunded		decimal(12,4),
		bs_customer_bal_total_refunded	decimal(12,4),
		base_total_due					decimal(12,4),
		base_to_global_rate				decimal(12,4),
		base_to_order_rate				decimal(12,4),
		order_currency_code				varchar(255),
		customer_dob					datetime2,
		customer_email					varchar(255),
		customer_firstname				varchar(255),
		customer_lastname				varchar(255),
		customer_middlename				varchar(255),
		customer_prefix					varchar(255),
		customer_suffix					varchar(255),
		customer_taxvat					varchar(255),
		customer_gender					int,
		customer_note					varchar(1000),
		customer_note_notify			int,
		shipping_description			varchar(255),
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255),
		affilBatch						varchar(255),	
		affilCode						varchar(255),
		affilUserRef					varchar(255),
		reminder_date					varchar(255),
		reminder_mobile					varchar(255),
		reminder_period					varchar(255),
		reminder_presc					varchar(255),
		reminder_type					varchar(255),
		reminder_follow_sent			int,
		reminder_sent					int,
		reorder_on_flag					int,
		reorder_profile_id				int,
		automatic_reorder				int,
		postoptics_source				varchar(255),
		postoptics_auto_verification	varchar(255),
		presc_verification_method		varchar(255),
		prescription_verification_type	varchar(25),
		referafriend_code				varchar(10),
		referafriend_referer			int,
		telesales_method_code			varchar(255),
		telesales_admin_username		varchar(255),
		remote_ip						varchar(255),
		warehouse_approved_time			datetime,
		partbill_shippingcost_cost		decimal(12,4), 
		old_order_id					bigint,
		mutual_amount					decimal(12, 4),
		mutual_quotation_id				int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);
	go 

	alter table mag.sales_flat_order add constraint [PK_mag_sales_flat_order]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order add constraint [DF_mag_sales_flat_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	create table mag.sales_flat_order_aud(
		entity_id						int NOT NULL,
		increment_id					varchar(50),
		store_id						int,
		customer_id						int, 
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							varchar(32),
		status							varchar(32),
		total_qty_ordered				decimal(12,4),
		base_subtotal					decimal(12,4),
		base_subtotal_canceled			decimal(12,4),
		base_subtotal_invoiced			decimal(12,4),
		base_subtotal_refunded			decimal(12,4),
		base_shipping_amount			decimal(12,4),
		base_shipping_canceled			decimal(12,4),
		base_shipping_invoiced			decimal(12,4),
		base_shipping_refunded			decimal(12,4),
		base_discount_amount			decimal(12,4),
		base_discount_canceled			decimal(12,4),
		base_discount_invoiced			decimal(12,4),
		base_discount_refunded			decimal(12,4),
		base_customer_balance_amount	decimal(12,4),
		base_customer_balance_invoiced	decimal(12,4),
		base_customer_balance_refunded	decimal(12,4),
		base_grand_total				decimal(12,4),
		base_total_canceled				decimal(12,4),
		base_total_invoiced				decimal(12,4),
		base_total_invoiced_cost		decimal(12,4),
		base_total_refunded				decimal(12,4),
		base_total_offline_refunded		decimal(12,4),
		base_total_online_refunded		decimal(12,4),
		bs_customer_bal_total_refunded	decimal(12,4),
		base_total_due					decimal(12,4),
		base_to_global_rate				decimal(12,4),
		base_to_order_rate				decimal(12,4),
		order_currency_code				varchar(255),
		customer_dob					datetime2,
		customer_email					varchar(255),
		customer_firstname				varchar(255),
		customer_lastname				varchar(255),
		customer_middlename				varchar(255),
		customer_prefix					varchar(255),
		customer_suffix					varchar(255),
		customer_taxvat					varchar(255),
		customer_gender					int,
		customer_note					varchar(1000),
		customer_note_notify			int,
		shipping_description			varchar(255),
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255),
		affilBatch						varchar(255),	
		affilCode						varchar(255),
		affilUserRef					varchar(255),
		reminder_date					varchar(255),
		reminder_mobile					varchar(255),
		reminder_period					varchar(255),
		reminder_presc					varchar(255),
		reminder_type					varchar(255),
		reminder_follow_sent			int,
		reminder_sent					int,
		reorder_on_flag					int,
		reorder_profile_id				int,
		automatic_reorder				int,
		postoptics_source				varchar(255),
		postoptics_auto_verification	varchar(255),
		presc_verification_method		varchar(255),
		prescription_verification_type	varchar(25),
		referafriend_code				varchar(10),
		referafriend_referer			int,
		telesales_method_code			varchar(255),
		telesales_admin_username		varchar(255),
		remote_ip						varchar(255),
		warehouse_approved_time			datetime,
		partbill_shippingcost_cost		decimal(12,4), 
		old_order_id					bigint,
		mutual_amount					decimal(12, 4),
		mutual_quotation_id				int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		upd_ts							datetime);
	go 

	alter table mag.sales_flat_order_aud add constraint [PK_mag_sales_flat_order_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order_aud add constraint [DF_mag_sales_flat_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	create table mag.sales_flat_order_aud_hist(
		entity_id						int NOT NULL,
		increment_id					varchar(50),
		store_id						int,
		customer_id						int, 
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							varchar(32),
		status							varchar(32),
		total_qty_ordered				decimal(12,4),
		base_subtotal					decimal(12,4),
		base_subtotal_canceled			decimal(12,4),
		base_subtotal_invoiced			decimal(12,4),
		base_subtotal_refunded			decimal(12,4),
		base_shipping_amount			decimal(12,4),
		base_shipping_canceled			decimal(12,4),
		base_shipping_invoiced			decimal(12,4),
		base_shipping_refunded			decimal(12,4),
		base_discount_amount			decimal(12,4),
		base_discount_canceled			decimal(12,4),
		base_discount_invoiced			decimal(12,4),
		base_discount_refunded			decimal(12,4),
		base_customer_balance_amount	decimal(12,4),
		base_customer_balance_invoiced	decimal(12,4),
		base_customer_balance_refunded	decimal(12,4),
		base_grand_total				decimal(12,4),
		base_total_canceled				decimal(12,4),
		base_total_invoiced				decimal(12,4),
		base_total_invoiced_cost		decimal(12,4),
		base_total_refunded				decimal(12,4),
		base_total_offline_refunded		decimal(12,4),
		base_total_online_refunded		decimal(12,4),
		bs_customer_bal_total_refunded	decimal(12,4),
		base_total_due					decimal(12,4),
		base_to_global_rate				decimal(12,4),
		base_to_order_rate				decimal(12,4),
		order_currency_code				varchar(255),
		customer_dob					datetime2,
		customer_email					varchar(255),
		customer_firstname				varchar(255),
		customer_lastname				varchar(255),
		customer_middlename				varchar(255),
		customer_prefix					varchar(255),
		customer_suffix					varchar(255),
		customer_taxvat					varchar(255),
		customer_gender					int,
		customer_note					varchar(1000),
		customer_note_notify			int,
		shipping_description			varchar(255),
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255),
		affilBatch						varchar(255),	
		affilCode						varchar(255),
		affilUserRef					varchar(255),
		reminder_date					varchar(255),
		reminder_mobile					varchar(255),
		reminder_period					varchar(255),
		reminder_presc					varchar(255),
		reminder_type					varchar(255),
		reminder_follow_sent			int,
		reminder_sent					int,
		reorder_on_flag					int,
		reorder_profile_id				int,
		automatic_reorder				int,
		postoptics_source				varchar(255),
		postoptics_auto_verification	varchar(255),
		presc_verification_method		varchar(255),
		prescription_verification_type	varchar(25),
		referafriend_code				varchar(10),
		referafriend_referer			int,
		telesales_method_code			varchar(255),
		telesales_admin_username		varchar(255),
		remote_ip						varchar(255),
		warehouse_approved_time			datetime,
		partbill_shippingcost_cost		decimal(12,4), 
		old_order_id					bigint,
		mutual_amount					decimal(12, 4),
		mutual_quotation_id				int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);
	go 

	alter table mag.sales_flat_order_aud_hist add constraint [DF_mag_sales_flat_order_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- sales_flat_order_item ----------------------------

	create table mag.sales_flat_order_item(
		item_id					int NOT NULL,
		order_id				int NOT NULL,
		store_id				int,
		created_at				datetime,
		updated_at				datetime,
		product_id				int, 
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty_ordered				decimal(12, 4),
		qty_canceled			decimal(12, 4),
		qty_invoiced			decimal(12, 4),
		qty_refunded			decimal(12, 4),
		qty_shipped				decimal(12, 4),
		qty_backordered			decimal(12, 4),
		qty_returned			decimal(12, 4),
		weight					decimal(12, 4),
		row_weight				decimal(12, 4),
		base_price				decimal(12, 4),
		price					decimal(12, 4),
		base_price_incl_tax		decimal(12, 4),
		base_cost				decimal(12, 4),
		base_row_total			decimal(12, 4),
		base_discount_amount	decimal(12, 4),		
		discount_percent		decimal(12, 4),
		base_amount_refunded	decimal(12, 4),
		mutual_amount			decimal(12, 4),
		product_type			varchar(255),
		product_options			varchar(1000),
		is_virtual				int,
		is_lens					int, 
		lens_group_eye			varchar(5),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go 

	alter table mag.sales_flat_order_item add constraint [PK_mag_sales_flat_order_item]
		primary key clustered (item_id);
	go
	alter table mag.sales_flat_order_item add constraint [DF_mag_sales_flat_order_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_order_item_aud(
		item_id					int NOT NULL,
		order_id				int NOT NULL,
		store_id				int,
		created_at				datetime,
		updated_at				datetime,
		product_id				int, 
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty_ordered				decimal(12, 4),
		qty_canceled			decimal(12, 4),
		qty_invoiced			decimal(12, 4),
		qty_refunded			decimal(12, 4),
		qty_shipped				decimal(12, 4),
		qty_backordered			decimal(12, 4),
		qty_returned			decimal(12, 4),
		weight					decimal(12, 4),
		row_weight				decimal(12, 4),
		base_price				decimal(12, 4),
		price					decimal(12, 4),
		base_price_incl_tax		decimal(12, 4),
		base_cost				decimal(12, 4),
		base_row_total			decimal(12, 4),
		base_discount_amount	decimal(12, 4),		
		discount_percent		decimal(12, 4),
		base_amount_refunded	decimal(12, 4),
		mutual_amount			decimal(12, 4),
		product_type			varchar(255),
		product_options			varchar(1000),
		is_virtual				int,
		is_lens					int, 
		lens_group_eye			varchar(5),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		upd_ts					datetime);
	go 

	alter table mag.sales_flat_order_item_aud add constraint [PK_mag_sales_flat_order_item_aud]
		primary key clustered (item_id);
	go
	alter table mag.sales_flat_order_item_aud add constraint [DF_mag_sales_flat_order_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create index [INX_mag_sales_flat_order_item_aud_order_id] on mag.sales_flat_order_item_aud(order_id);
	go


	create table mag.sales_flat_order_item_aud_hist(
		item_id					int NOT NULL,
		order_id				int NOT NULL,
		store_id				int,
		created_at				datetime,
		updated_at				datetime,
		product_id				int, 
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty_ordered				decimal(12, 4),
		qty_canceled			decimal(12, 4),
		qty_invoiced			decimal(12, 4),
		qty_refunded			decimal(12, 4),
		qty_shipped				decimal(12, 4),
		qty_backordered			decimal(12, 4),
		qty_returned			decimal(12, 4),
		weight					decimal(12, 4),
		row_weight				decimal(12, 4),
		base_price				decimal(12, 4),
		price					decimal(12, 4),
		base_price_incl_tax		decimal(12, 4),
		base_cost				decimal(12, 4),
		base_row_total			decimal(12, 4),
		base_discount_amount	decimal(12, 4),		
		discount_percent		decimal(12, 4),
		base_amount_refunded	decimal(12, 4),
		mutual_amount			decimal(12, 4),
		product_type			varchar(255),
		product_options			varchar(1000),
		is_virtual				int,
		is_lens					int,
		lens_group_eye			varchar(5), 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go 

	alter table mag.sales_flat_order_item_aud_hist add constraint [DF_mag_sales_flat_order_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 



----------------------- sales_flat_order_payment ----------------------------

	create table mag.sales_flat_order_payment(
		entity_id						int NOT NULL,
		parent_id						int,	
		method							varchar(255), 
		amount_authorized				decimal(12, 4),
		amount_canceled					decimal(12, 4),
		base_amount_ordered				decimal(12, 4),
		base_amount_canceled			decimal(12, 4),
		base_amount_refunded			decimal(12, 4),
		base_amount_refunded_online		decimal(12, 4),
		cc_type							varchar(255),
		cc_status						varchar(255),
		cc_status_description			varchar(255),
		cc_avs_status					varchar(255),
		cc_ss_start_year				varchar(255),
		cc_ss_start_month				varchar(255),
		cc_exp_year						varchar(255),
		cc_exp_month					varchar(255),
		cc_last4						varchar(255),
		cc_owner						varchar(255),
		cc_ss_issue						varchar(255),
		additional_data					varchar(1000), 
		cybersource_stored_id			varchar(255),
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);
	go

	alter table mag.sales_flat_order_payment add constraint [PK_mag_sales_flat_order_payment]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order_payment add constraint [DF_mag_sales_flat_order_payment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_order_payment_aud(
		entity_id						int NOT NULL,
		parent_id						int,	
		method							varchar(255), 
		amount_authorized				decimal(12, 4),
		amount_canceled					decimal(12, 4),
		base_amount_ordered				decimal(12, 4),
		base_amount_canceled			decimal(12, 4),
		base_amount_refunded			decimal(12, 4),
		base_amount_refunded_online		decimal(12, 4),
		cc_type							varchar(255),
		cc_status						varchar(255),
		cc_status_description			varchar(255),
		cc_avs_status					varchar(255),
		cc_ss_start_year				varchar(255),
		cc_ss_start_month				varchar(255),
		cc_exp_year						varchar(255),
		cc_exp_month					varchar(255),
		cc_last4						varchar(255),
		cc_owner						varchar(255),
		cc_ss_issue						varchar(255),
		additional_data					varchar(1000), 
		cybersource_stored_id			varchar(255),
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		upd_ts							datetime);
	go

	alter table mag.sales_flat_order_payment_aud add constraint [PK_mag_sales_flat_order_payment_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order_payment_aud add constraint [DF_mag_sales_flat_order_payment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create index [INX_mag_sales_flat_order_payment_aud_order_id] on mag.sales_flat_order_payment_aud(parent_id);
	go

	create table mag.sales_flat_order_payment_aud_hist(
		entity_id						int NOT NULL,
		parent_id						int,	
		method							varchar(255), 
		amount_authorized				decimal(12, 4),
		amount_canceled					decimal(12, 4),
		base_amount_ordered				decimal(12, 4),
		base_amount_canceled			decimal(12, 4),
		base_amount_refunded			decimal(12, 4),
		base_amount_refunded_online		decimal(12, 4),
		cc_type							varchar(255),
		cc_status						varchar(255),
		cc_status_description			varchar(255),
		cc_avs_status					varchar(255),
		cc_ss_start_year				varchar(255),
		cc_ss_start_month				varchar(255),
		cc_exp_year						varchar(255),
		cc_exp_month					varchar(255),
		cc_last4						varchar(255),
		cc_owner						varchar(255),
		cc_ss_issue						varchar(255),
		additional_data					varchar(1000), 
		cybersource_stored_id			varchar(255),
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);
	go

	alter table mag.sales_flat_order_payment_aud_hist add constraint [DF_mag_sales_flat_order_payment_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 



----------------------- sales_flat_order_address ----------------------------

	create table mag.sales_flat_order_address(
		entity_id					int NOT NULL,
		parent_id					int, 
		customer_address_id			int,
		customer_id					int,
		address_type				varchar(255),
		email						varchar(255),
		company						varchar(255),
		firstname					varchar(255),
		lastname					varchar(255),
		prefix						varchar(255),
		middlename					varchar(255),
		suffix						varchar(255),
		street						varchar(255),
		city						varchar(255),
		postcode					varchar(255),
		region						varchar(255),
		fax							varchar(255),
		telephone					varchar(255),
		region_id					int,
		country_id					varchar(2), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.sales_flat_order_address add constraint [PK_mag_sales_flat_order_address]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order_address add constraint [DF_mag_sales_flat_order_address_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_order_address_aud(
		entity_id					int NOT NULL,
		parent_id					int, 
		customer_address_id			int,
		customer_id					int,
		address_type				varchar(255),
		email						varchar(255),
		company						varchar(255),
		firstname					varchar(255),
		lastname					varchar(255),
		prefix						varchar(255),
		middlename					varchar(255),
		suffix						varchar(255),
		street						varchar(255),
		city						varchar(255),
		postcode					varchar(255),
		region						varchar(255),
		fax							varchar(255),
		telephone					varchar(255),
		region_id					int,
		country_id					varchar(2), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go

	alter table mag.sales_flat_order_address_aud add constraint [PK_mag_sales_flat_order_address_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_order_address_aud add constraint [DF_mag_sales_flat_order_address_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_order_address_aud_hist(
		entity_id					int NOT NULL,
		parent_id					int, 
		customer_address_id			int,
		customer_id					int,
		address_type				varchar(255),
		email						varchar(255),
		company						varchar(255),
		firstname					varchar(255),
		lastname					varchar(255),
		prefix						varchar(255),
		middlename					varchar(255),
		suffix						varchar(255),
		street						varchar(255),
		city						varchar(255),
		postcode					varchar(255),
		region						varchar(255),
		fax							varchar(255),
		telephone					varchar(255),
		region_id					int,
		country_id					varchar(2), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.sales_flat_order_address_aud_hist add constraint [DF_mag_sales_flat_order_address_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- sales_order_log ----------------------------

	create table mag.sales_order_log(
		id					int NOT NULL, 
		order_id			int, 			
		order_no			varchar(50),
		state				varchar(50),
		status				varchar(50),
		user_name			varchar(100),
		updated_at			datetime, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);
	go 


	alter table mag.sales_order_log add constraint [PK_mag_sales_order_log]
		primary key clustered (id);
	go
	alter table mag.sales_order_log add constraint [DF_mag_sales_order_log_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_order_log_aud(
		id					int NOT NULL, 
		order_id			int, 			
		order_no			varchar(50),
		state				varchar(50),
		status				varchar(50),
		user_name			varchar(100),
		updated_at			datetime, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		upd_ts				datetime);
	go 


	alter table mag.sales_order_log_aud add constraint [PK_mag_sales_order_log_aud]
		primary key clustered (id);
	go
	alter table mag.sales_order_log_aud add constraint [DF_mag_sales_order_log_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_order_log_aud_hist(
		id					int NOT NULL, 
		order_id			int, 			
		order_no			varchar(50),
		state				varchar(50),
		status				varchar(50),
		user_name			varchar(100),
		updated_at			datetime, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);
	go 


	alter table mag.sales_order_log_aud_hist add constraint [DF_mag_sales_order_log_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


