
use Landing
go 

------------------------------------------------------------------------
----------------------- review_entity ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.review_entity_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.review_entity_aud

	select idETLBatchRun, count(*) num_rows
	from mag.review_entity_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.review_entity_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_entity_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom



------------------------------------------------------------------------
----------------------- review_status ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.review_status_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct status_id) num_dist_rows
	from mag.review_status_aud

	select idETLBatchRun, count(*) num_rows
	from mag.review_status_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct status_id) num_dist_rows
	from mag.review_status_aud_hist


	select *
	from
		(select count(*) over (partition by status_id) num_rows_rep,
			status_id, status_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_status_aud_hist) t
	where num_rows_rep > 1
	order by status_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- review ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.review_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct review_id) num_dist_rows
	from mag.review_aud

	select idETLBatchRun, count(*) num_rows
	from mag.review_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct review_id) num_dist_rows
	from mag.review_aud_hist


	select *
	from
		(select count(*) over (partition by review_id) num_rows_rep,
			review_id, created_at, entity_id, status_id, entity_pk_value,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_aud_hist) t
	where num_rows_rep > 1
	order by review_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- review_store ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.review_store_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select idETLBatchRun, count(*) num_rows
	from mag.review_store_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun



------------------------------------------------------------------------
----------------------- review_detail ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.review_detail_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct detail_id) num_dist_rows
	from mag.review_detail_aud

	select idETLBatchRun, count(*) num_rows
	from mag.review_detail_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct detail_id) num_dist_rows
	from mag.review_detail_aud_hist


	select *
	from
		(select count(*) over (partition by detail_id) num_rows_rep,
			detail_id, review_id, store_id, 
			title, detail, nickname, customer_id, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.review_detail_aud_hist) t
	where num_rows_rep > 1
	order by detail_id, aud_dateFrom







------------------------------------------------------------------------
----------------------- rating_entity ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.rating_entity_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.rating_entity_aud

	select idETLBatchRun, count(*) num_rows
	from mag.rating_entity_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.rating_entity_aud_hist


	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_entity_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- rating ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.rating_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct rating_id) num_dist_rows
	from mag.rating_aud

	select idETLBatchRun, count(*) num_rows
	from mag.rating_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct rating_id) num_dist_rows
	from mag.rating_aud_hist


	select *
	from
		(select count(*) over (partition by rating_id) num_rows_rep,
			rating_id, entity_id, rating_code, position,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_aud_hist) t
	where num_rows_rep > 1
	order by rating_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- rating_option ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.rating_option_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct option_id) num_dist_rows
	from mag.rating_option_aud

	select idETLBatchRun, count(*) num_rows
	from mag.rating_option_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct option_id) num_dist_rows
	from mag.rating_option_aud_hist


	select *
	from
		(select count(*) over (partition by option_id) num_rows_rep,
			option_id, rating_id, code, value, position, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_option_aud_hist) t
	where num_rows_rep > 1
	order by option_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- rating_option_vote ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.rating_option_vote_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct vote_id) num_dist_rows
	from mag.rating_option_vote_aud

	select idETLBatchRun, count(*) num_rows
	from mag.rating_option_vote_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct vote_id) num_dist_rows
	from mag.rating_option_vote_aud_hist


	select *
	from
		(select count(*) over (partition by vote_id) num_rows_rep,
			vote_id, 
			review_id, remote_ip, customer_id, entity_pk_value, 
			rating_id, option_id, percent_rating, value, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.rating_option_vote_aud_hist) t
	where num_rows_rep > 1
	order by vote_id, aud_dateFrom