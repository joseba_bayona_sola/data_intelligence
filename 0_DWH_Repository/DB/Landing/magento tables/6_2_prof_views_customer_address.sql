use Landing
go 

----------------------- customer_address_entity ----------------------------

select * 
from mag.customer_address_entity_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_datetime ----------------------------

select * 
from mag.customer_address_entity_datetime_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_decimal ----------------------------

select * 
from mag.customer_address_entity_decimal_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_int ----------------------------

select * 
from mag.customer_address_entity_int_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_text ----------------------------

select * 
from mag.customer_address_entity_text_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_varchar ----------------------------

select * 
from mag.customer_address_entity_varchar_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

