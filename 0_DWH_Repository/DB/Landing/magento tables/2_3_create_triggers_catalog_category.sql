use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- catalog_category_entity ----------------------------

	drop trigger mag.trg_catalog_category_entity;
	go 

	create trigger mag.trg_catalog_category_entity on mag.catalog_category_entity
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.parent_id, 0), 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.position, 0), isnull(trg.level, 0), isnull(trg.children_count, 0), isnull(trg.path, ' '), 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' ')

			intersect
			select 
				isnull(src.parent_id, 0), 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.position, 0), isnull(src.level, 0), isnull(src.children_count, 0), isnull(src.path, ' '), 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '))
							
			then
				update set
					trg.parent_id = src.parent_id, 
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.position = src.position, trg.level = src.level, trg.children_count = src.children_count, trg.path = src.path, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					parent_id, 
					entity_type_id, attribute_set_id, 
					position, level, children_count, path, 
					created_at, updated_at, 
					idETLBatchRun)
					
					values (src.entity_id, 
						src.parent_id, 
						src.entity_type_id, src.attribute_set_id, 
						src.position, src.level, src.children_count, src.path, 
						src.created_at, src.updated_at, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_aud;
	go

	create trigger mag.trg_catalog_category_entity_aud on mag.catalog_category_entity_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_aud_hist (entity_id, 
			parent_id, 
			entity_type_id, attribute_set_id, 
			position, level, children_count, path, 
			created_at, updated_at, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.parent_id, 
				d.entity_type_id, d.attribute_set_id, 
				d.position, d.level, d.children_count, d.path, 
				d.created_at, d.updated_at, 
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go


-----

------------------------- catalog_category_entity_datetime ----------------------------

	drop trigger mag.trg_catalog_category_entity_datetime;
	go 

	create trigger mag.trg_catalog_category_entity_datetime on mag.catalog_category_entity_datetime
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_datetime_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_datetime_aud;
	go

	create trigger mag.trg_catalog_category_entity_datetime_aud on mag.catalog_category_entity_datetime_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_datetime_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_category_entity_decimal ----------------------------

	drop trigger mag.trg_catalog_category_entity_decimal;
	go 

	create trigger mag.trg_catalog_category_entity_decimal on mag.catalog_category_entity_decimal
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_decimal_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_decimal_aud;
	go

	create trigger mag.trg_catalog_category_entity_decimal_aud on mag.catalog_category_entity_decimal_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_decimal_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_category_entity_int ----------------------------

	drop trigger mag.trg_catalog_category_entity_int;
	go 

	create trigger mag.trg_catalog_category_entity_int on mag.catalog_category_entity_int
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_int_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_int_aud;
	go

	create trigger mag.trg_catalog_category_entity_int_aud on mag.catalog_category_entity_int_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_int_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_category_entity_text ----------------------------

	drop trigger mag.trg_catalog_category_entity_text;
	go 

	create trigger mag.trg_catalog_category_entity_text on mag.catalog_category_entity_text
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_text_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_text_aud;
	go

	create trigger mag.trg_catalog_category_entity_text_aud on mag.catalog_category_entity_text_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_text_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_category_entity_varchar ----------------------------

	drop trigger mag.trg_catalog_category_entity_varchar;
	go 

	create trigger mag.trg_catalog_category_entity_varchar on mag.catalog_category_entity_varchar
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_entity_varchar_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_entity_varchar_aud;
	go

	create trigger mag.trg_catalog_category_entity_varchar_aud on mag.catalog_category_entity_varchar_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_entity_varchar_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

