use Landing
go 

----------------------- eav_entity_type ----------------------------

select * 
from mag.eav_entity_type_aud_v
where num_records > 1
order by entity_type_id, idETLBatchRun

----------------------- eav_attribute ----------------------------

select * 
from mag.eav_attribute_aud_v
where num_records > 1
order by attribute_id, idETLBatchRun

----------------------- eav_entity_attribute ----------------------------

select * 
from mag.eav_entity_attribute_aud_v
where num_records > 1
order by entity_attribute_id, idETLBatchRun

----------------------- eav_attribute_option ----------------------------

select * 
from mag.eav_attribute_option_aud_v
where num_records > 1
order by option_id, idETLBatchRun

----------------------- eav_attribute_option_value ----------------------------

select * 
from mag.eav_attribute_option_value_aud_v
where num_records > 1
order by value_id, idETLBatchRun

