use Landing
go 

----------------------- sales_flat_invoice ----------------------------

create view mag.sales_flat_invoice_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, 
		entity_id, increment_id, order_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		total_qty, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
		base_grand_total, base_total_refunded, 
		base_to_global_rate, base_to_order_rate, order_currency_code, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo 
	from
		(select 'N' record_type, entity_id, increment_id, order_id, store_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			state, 
			total_qty, 
			base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
			base_grand_total, base_total_refunded, 
			mutual_amount, 
			base_to_global_rate, base_to_order_rate, order_currency_code, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.sales_flat_invoice_aud
		union
		select 'H' record_type, entity_id, increment_id, order_id, store_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			state, 
			total_qty, 
			base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
			base_grand_total, base_total_refunded, 
			mutual_amount, 
			base_to_global_rate, base_to_order_rate, order_currency_code, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.sales_flat_invoice_aud_hist) t
go

----------------------- sales_flat_invoice_item ----------------------------

create view mag.sales_flat_invoice_item_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, 
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, 
		base_price, price, base_price_incl_tax, 
		base_cost, 
		base_row_total, 
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo 
	from
		(select 'N' record_type, entity_id, order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			base_price, price, base_price_incl_tax, 
			base_cost, 
			base_row_total, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo 
		from mag.sales_flat_invoice_item_aud
		union
		select 'H' record_type, entity_id, order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			base_price, price, base_price_incl_tax, 
			base_cost, 
			base_row_total, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo 
		from mag.sales_flat_invoice_item_aud_hist) t
go

