use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_order ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_order_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_aud_hist

	select *
	from
		(select count(*) over(partition by entity_id) num_rep, *
		from mag.sales_flat_order_aud_hist) t
	where num_rep > 1
	order by num_rep desc, entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- sales_flat_order_item ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_order_item_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct item_id) num_dist_rows
	from mag.sales_flat_order_item_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_order_item_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct item_id) num_dist_rows
	from mag.sales_flat_order_item_aud_hist

	select *
	from
		(select count(*) over(partition by item_id) num_rep, *
		from mag.sales_flat_order_item_aud_hist) t
	where num_rep > 1
	order by num_rep desc, item_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- sales_flat_order_payment ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_order_payment_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_payment_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_order_payment_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_payment_aud_hist

	select *
	from
		(select count(*) over(partition by entity_id) num_rep, *
		from mag.sales_flat_order_payment_aud_hist) t
	where num_rep > 1
	order by num_rep desc, entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- sales_flat_order_address ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_flat_order_address_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_address_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_flat_order_address_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.sales_flat_order_address_aud_hist

	select *
	from
		(select count(*) over(partition by entity_id) num_rep, *
		from mag.sales_flat_order_address_aud_hist) t
	where num_rep > 1
	order by num_rep desc, entity_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- sales_order_log ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.sales_order_log_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.sales_order_log_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.sales_order_log_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.sales_order_log_aud_hist

	select *
	from
		(select count(*) over(partition by id) num_rep, *
		from mag.sales_order_log_aud_hist) t
	where num_rep > 1
	order by num_rep desc, id, aud_dateFrom

	---- 
	select top 1000 *
	from
		(select count(*) over (partition by order_id) num_rep,
			id, order_id, order_no, 
			state, status, 
			user_name, updated_at, 
			idETLBatchRun, ins_ts
		from mag.sales_order_log) t
	where num_rep > 1
	order by num_rep desc, order_id, updated_at