use Landing
go 

----------------------- customer_entity_flat ----------------------------

select entity_id, increment_id, email, 
	entity_type_id, attribute_set_id, 
	group_id, website_group_id, website_id, store_id, 
	is_active, disable_auto_group_change, 
	created_at, updated_at, 
	created_in, 
	prefix, firstname, middlename, lastname, suffix, 
	cus_phone, 
	dob, gender,
	default_billing, default_shipping, 
	unsubscribe_all, unsubscribe_all_date, 
	days_worn_info, 
	old_access_cust_no, old_web_cust_no, old_customer_id, 
	found_us_info, referafriend_code, 
	taxvat,
	idETLBatchRun, ins_ts
from mag.customer_entity_flat

select entity_id, increment_id, email, 
	entity_type_id, attribute_set_id, 
	group_id, website_group_id, website_id, store_id, 
	is_active, disable_auto_group_change, 
	created_at, updated_at, 
	created_in, 
	prefix, firstname, middlename, lastname, suffix, 
	cus_phone, 
	dob, gender,
	default_billing, default_shipping, 
	unsubscribe_all, unsubscribe_all_date, 
	days_worn_info, 
	old_access_cust_no, old_web_cust_no, old_customer_id, 
	found_us_info, referafriend_code, 
	taxvat,
	idETLBatchRun, ins_ts, upd_ts
from mag.customer_entity_flat_aud

select top 1000 entity_id, increment_id, email, 
	entity_type_id, attribute_set_id, 
	group_id, website_group_id, website_id, store_id, 
	is_active, disable_auto_group_change, 
	created_at, updated_at, 
	created_in, 
	prefix, firstname, middlename, lastname, suffix, 
	cus_phone, 
	dob, gender,
	default_billing, default_shipping, 
	unsubscribe_all, unsubscribe_all_date, 
	days_worn_info, 
	old_access_cust_no, old_web_cust_no, old_customer_id, 
	found_us_info, referafriend_code, 
	taxvat,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.customer_entity_flat_aud_hist

----------------------- customer_address_entity_flat ----------------------------

select entity_id, increment_id, parent_id, 
	entity_type_id, attribute_set_id, 
	is_active, 
	created_at, updated_at, 
	prefix, firstname, middlename, lastname, suffix, 
	company, 
	street, city, postcode, region_id, region, country_id, 
	telephone, 
	idETLBatchRun, ins_ts
from mag.customer_address_entity_flat

select entity_id, increment_id, parent_id, 
	entity_type_id, attribute_set_id, 
	is_active, 
	created_at, updated_at, 
	prefix, firstname, middlename, lastname, suffix, 
	company, 
	street, city, postcode, region_id, region, country_id, 
	telephone, 
	idETLBatchRun, ins_ts, upd_ts
from mag.customer_address_entity_flat_aud

select entity_id, increment_id, parent_id, 
	entity_type_id, attribute_set_id, 
	is_active, 
	created_at, updated_at, 
	prefix, firstname, middlename, lastname, suffix, 
	company, 
	street, city, postcode, region_id, region, country_id, 
	telephone, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.customer_address_entity_flat_aud_hist

----------------------- catalog_category_entity_flat ----------------------------

select entity_id, store_id, parent_id, 
	entity_type_id, attribute_set_id, 
	position, level, children_count, path, 
	created_at, updated_at, 
	is_active, 
	name, url_path, url_key, display_mode, 
	idETLBatchRun, ins_ts
from mag.catalog_category_entity_flat

select entity_id, store_id, parent_id, 
	entity_type_id, attribute_set_id, 
	position, level, children_count, path, 
	created_at, updated_at, 
	is_active, 
	name, url_path, url_key, display_mode, 
	idETLBatchRun, ins_ts, upd_ts
from mag.catalog_category_entity_flat_aud

select entity_id, store_id, parent_id, 
	entity_type_id, attribute_set_id, 
	position, level, children_count, path, 
	created_at, updated_at, 
	is_active, 
	name, url_path, url_key, display_mode, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.catalog_category_entity_flat_aud_hist

----------------------- catalog_product_entity_flat ----------------------------

select entity_id, store_id, 
	entity_type_id, attribute_set_id, 
	type_id, sku, required_options, has_options, 
	created_at, updated_at, 
	manufacturer, 
	name, url_path, url_key, 
	status, product_type, product_lifecycle, 
	promotional_product, daysperlens, 
	tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
	equivalence, equivalent_sku, price_comp_price, glasses_colour,
	idETLBatchRun, ins_ts
from mag.catalog_product_entity_flat

select entity_id, store_id, 
	entity_type_id, attribute_set_id, 
	type_id, sku, required_options, has_options, 
	created_at, updated_at, 
	manufacturer, 
	name, url_path, url_key, 
	status, product_type, product_lifecycle, 
	promotional_product, daysperlens, 
	tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
	equivalence, equivalent_sku, price_comp_price, glasses_colour,
	idETLBatchRun, ins_ts, upd_ts
from mag.catalog_product_entity_flat_aud

select entity_id, store_id, 
	entity_type_id, attribute_set_id, 
	type_id, sku, required_options, has_options, 
	created_at, updated_at, 
	manufacturer, 
	name, url_path, url_key, 
	status, product_type, product_lifecycle, 
	promotional_product, daysperlens, 
	tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
	equivalence, equivalent_sku, price_comp_price, glasses_colour,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mag.catalog_product_entity_flat_aud_hist


