use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_gen_store_type
drop trigger map.trg_gen_store_type;
go 

create trigger map.trg_gen_store_type on map.gen_store_type
after insert
as
begin
	set nocount on

	merge into map.gen_store_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_name, '') = isnull(src.store_name, '')) and (isnull(trg.tld, '') = isnull(src.tld, ''))
	when matched and
		(isnull(trg.store_type, '') <> isnull(src.store_type, ''))
		then 
			update set
				trg.store_type = src.store_type, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_name, tld, store_type, idETLBatchRun)
				values (src.store_name, src.tld, src.store_type, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_gen_website_group
drop trigger map.trg_gen_website_group;
go 

create trigger map.trg_gen_website_group on map.gen_website_group
after insert
as
begin
	set nocount on

	merge into map.gen_website_group_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.store_name, '') = isnull(src.store_name, '')) and (isnull(trg.name, '') = isnull(src.name, ''))
	when matched and
		(isnull(trg.website_group, '') <> isnull(src.website_group, ''))
		then 
			update set
				trg.website_group = src.website_group, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_name, name, website_group, idETLBatchRun)
				values (src.store_name, src.name, src.website_group, src.idETLBatchRun);
end;
go 