use Landing
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Store in different Landind tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_store
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: UNION of stores in Magento (Map Tables Join) + DW_Proforma
	select store_id, store_name, website_type, website_group, website, tld, code_tld, 
		@idETLBatchRun
	from
		(select store_id, store_name, website_type, website_group, website, tld, 
			case 
				when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
				when website_type = 'White Label' then 'wl'
				else '' 
			end + TLD as code_tld
		from
			(select cs.store_id, cs.name store_name, 
				case when (st.store_type is null) then st2.store_type else st.store_type end website_type,
				case when (wg.website_group is null) then wg2.website_group else wg.website_group end website_group,
				cs.website, cs.tld
			from 
					(select store_id, name, 
						case 
							when website like '%.co.uk' then '.co.uk'
							else right(website, charindex('.', reverse(website), 1)) 
						end as tld, website
					from 
						(select store_id, name, 
							left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end) as website	
						from Landing.mag.core_store) cs) cs
				left join
					Landing.map.gen_store_type st on cs.name = st.store_name
				left join
					Landing.map.gen_store_type st2 on cs.tld = st2.tld
				left join
					Landing.map.gen_website_group wg on cs.name = wg.store_name
				left join
					Landing.map.gen_website_group wg2 on charindex(wg2.name, cs.name) > 0
			where cs.store_id <> 0) cs
		union
		select store_id * -1, store_name, website_type, website_group, website, tld, 
			case 
				when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
				when website_type = 'White Label' then 'wl'
				else '' 
			end + TLD as code_tld
		from 
			(select store_id, store_name, website_type, website_group, website, 
				case 
					when website like '%.co.uk' then '.co.uk'
					else right(website, charindex('.', reverse(website), 1)) 
				end as tld
			from 
				(select store_id, store_name, store_type website_type, website_group, 
					left(store_name, case when charindex('/', store_name, 1) = 0 then len(store_name) else charindex('/', store_name, 1) - 1 end) as website
				from Landing.migra_pr.dw_stores_full) sf) sf
		where store_id not in (20, 21)) stores

	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 