use Landing
go

select cast(s.string AS sysname)
from dbo.fnSplitString ('aa,bb,cc', ',') AS s
where isnull(s.string, '') <> '';

exec dbo.truncateTables @TableList = 'mag.core_store'

truncate table mag.core_store