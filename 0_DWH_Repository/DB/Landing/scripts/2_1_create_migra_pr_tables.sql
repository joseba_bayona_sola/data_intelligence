use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.migra_pr.dw_stores_full
create table migra_pr.dw_stores_full(
	store_id		int NOT NULL,
	store_name		varchar(100) NULL,
	website_group	varchar(100) NULL,
	store_type		varchar(100) NULL,
	visible			int NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table migra_pr.dw_stores_full add constraint [PK_migra_pr_dw_stores_full]
	primary key clustered (store_id);
go

alter table migra_pr.dw_stores_full add constraint [DF_migra_pr_dw_stores_full_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


