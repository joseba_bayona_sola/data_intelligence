use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.mag.core_store
create table mag.core_store(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go 

alter table mag.core_store add constraint [PK_mag_core_store]
	primary key clustered (store_id);
go

alter table mag.core_store add constraint [DF_mag_core_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.core_store_aud
create table mag.core_store_aud(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	upd_ts			datetime);
go 

alter table mag.core_store_aud add constraint [PK_mag_core_store_aud]
	primary key clustered (store_id);
go

alter table mag.core_store_aud add constraint [DF_mag_core_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.core_store_aud_hist
create table mag.core_store_aud_hist(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	aud_type		char(1) NOT NULL, 
	aud_dateFrom	datetime NOT NULL, 
	aud_dateTo		datetime NOT NULL);
go 

alter table mag.core_store_aud_hist add constraint [DF_mag_core_store_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------- Triggers ----------------------------------

-- Landing.mag.trg_core_store
drop trigger mag.trg_core_store;

create trigger mag.trg_core_store on mag.core_store
after insert
as
begin
	set nocount on

	merge into mag.core_store_aud with (tablock) as trg
	using inserted src
		on (trg.store_id = src.store_id)
	when matched and
		(isnull(trg.code, '') <> isnull(src.code, '')) and (isnull(trg.name, '') <> isnull(src.name, '')) and
		(isnull(trg.website_id, 0) <> isnull(src.website_id, 0)) and (isnull(trg.group_id, 0) <> isnull(src.group_id, 0)) and
		(isnull(trg.sort_order, 0) <> isnull(src.sort_order, 0)) and (isnull(trg.is_active, 0) <> isnull(src.is_active, 0))
		then
			update set
				trg.code = src.code, trg.name = src.name, 
				trg.website_id = src.website_id, trg.group_id = src.group_id, 
				trg.sort_order = src.sort_order, trg.is_active = src.is_active,
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun)
				values (src.store_id, src.code, src.name, src.website_id, src.group_id, src.sort_order, src.is_active, src.idETLBatchRun);
end; 
go

-- Landing.mag.core_store_aud_trg
drop trigger mag.trg_core_store_aud;

create trigger mag.trg_core_store_aud on mag.core_store_aud
for update, delete
as
begin
	set nocount on

	insert mag.core_store_aud_hist (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun,
		aud_type, aud_dateFrom, aud_dateTo)
		
		select d.store_id, d.code, d.name, d.website_id, d.group_id, d.sort_order, d.is_active, d.idETLBatchRun,
			case when (i.store_id is null) then 'D' else 'U' end aud_type, d.ins_ts aud_dateFrom, i.ins_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.store_id = i.store_id;
end;
go			 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_gen_store
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 18-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - core_store
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_gen_store
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Log Parameters ??

	set @sql = 'select store_id, code, website_id, group_id, name, sort_order, is_active, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, ''select * from magento01.core_store'')'
	exec(@sql)

	--select store_id, code, website_id, group_id, name, sort_order, is_active, @idETLBatchRun
	--from openquery(MAGENTO, @sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 