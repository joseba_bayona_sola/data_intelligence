
truncate table Landing.migra.match_customer

truncate table Landing.migra.match_order_header

truncate table Landing.migra.match_order_line


delete from Landing.migra.match_customer
where migra_website_name = 'VisioOptik'

delete from Landing.migra.match_order_header
where migra_website_name = 'VisioOptik'

delete from Landing.migra.match_order_line
where migra_website_name = 'VisioOptik'



------------


truncate table Landing.migra.sales_flat_order_hist

truncate table Landing.migra.sales_flat_order_hist_aud


truncate table Landing.migra.sales_flat_order_item_hist

truncate table Landing.migra.sales_flat_order_item_hist_aud


truncate table Landing.migra.sales_flat_order_address_hist

truncate table Landing.migra.sales_flat_order_address_hist_aud


------------


truncate table Landing.migra.sales_flat_order_migrate

truncate table Landing.migra.sales_flat_order_migrate_aud


truncate table Landing.migra.sales_flat_order_item_migrate

truncate table Landing.migra.sales_flat_order_item_migrate_aud


truncate table Landing.migra.sales_flat_order_address_migrate

truncate table Landing.migra.sales_flat_order_address_migrate_aud
