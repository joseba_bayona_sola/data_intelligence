use Landing
go 

--------------------- SP ----------------------------------

drop procedure migra.lnd_stg_get_hist_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-07-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from HIST Tables - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_hist_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select migra_website_name, etl_name, 
			old_customer_id, new_magento_id, 
			null proforma_customer_id, 
			new_magento_id new_customer_id, customer_email,
			@idETLBatchRun idETLBatchRun 
		from
			(select distinct migra_website_name, etl_name, 
				customer_id_hist old_customer_id, customer_id new_magento_id, customer_email		
			from Landing.aux.migra_hist_dw_hist_order_item_v 
			where customer_id is not null) c
		order by new_magento_id, migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

		select migra_website_name, etl_name, 
			order_id old_order_id, null magento_order_id, 
			null proforma_order_id, null proforma_order_no, 
			(prev_new_order_id + rank() over (order by created_at, order_id)) * -1 new_order_id, web_prefix + convert(varchar, order_id) new_order_no, 
			customer_id_hist old_customer_id, null proforma_customer_id, customer_id new_customer_id, customer_email, 
			@idETLBatchRun idETLBatchRun
		from
			(select distinct migra_website_name, etl_name, 
				case
					when (migra_website_name = 'postoptics') then 'PO'
					when (migra_website_name = 'getlenses') then 'GL'
					when (migra_website_name = 'masterlens') then 'GL'
					when (migra_website_name = 'asda') then 'AS'
					when (migra_website_name = 'VH1') then 'VH'
				end web_prefix,
				order_id, created_at,
				customer_id_hist, customer_id, customer_email
			from Landing.aux.migra_hist_dw_hist_order_item_v
			where 
				sum_row_total is not null 
				and customer_id is not null
				and (grand_total <> 0 or sum_row_total <> 0)) oh, ---- Check also orders with grand_total = 0 and sum_row_total = 0 // Neg values
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line,
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select hoi.migra_website_name, hoi.etl_name, 
			hoi.order_id old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			hoi.product_id_hist old_product_id, null proforma_product_id, hoi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_hist_dw_hist_order_item_v  hoi
			inner join
				Landing.migra.match_order_header ho on hoi.order_id = ho.old_order_id and hoi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message
	
	
	delete from Landing.migra.sales_flat_order_hist
	delete from Landing.migra.sales_flat_order_item_hist
	delete from Landing.migra.sales_flat_order_address_hist

	---- INSERT: migra.sales_flat_order_hist
	insert into Landing.migra.sales_flat_order_hist(migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_grand_total, 
		customer_email,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total, sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal
				from Landing.aux.migra_hist_dw_hist_order_item_v
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name
		where moh.etl_name = 'hist'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	---- INSERT: migra.sales_flat_order_item_hist
	insert into Landing.migra.sales_flat_order_item_hist(migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_hist_dw_hist_order_item_v hoi on mol.old_item_id = hoi.order_id and mol.num_order_line = hoi.num_order_line

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	---- INSERT: migra.sales_flat_order_address_hist
	insert into Landing.migra.sales_flat_order_address_hist(migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			ad_s.postcode, isnull(c_s.country_code, 'GB') country_id, 
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.mag.customer_entity_flat_aud c on moh.new_customer_id = c.entity_id
			left join
				Landing.mag.customer_address_entity_flat_aud ad_s on c.default_shipping = ad_s.entity_id
			left join
				Landing.map.gen_countries c_s on ad_s.country_id = c_s.country_code

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'


end;
go 





drop procedure migra.lnd_stg_get_migrate_lb150324_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-08-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (lb150324) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lb150324_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select migra_website_name, etl_name, 
			old_customer_id, new_magento_id, 
			proforma_customer_id, 
			new_customer_id, customer_email,
			@idETLBatchRun idETLBatchRun 
		from 
			(select distinct migra_website_name, etl_name,
				moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
				oh.customer_id proforma_customer_id, 
				oh.customer_id new_customer_id, c.email customer_email
			from
					(select migra_website_name, etl_name,
						order_id, 800000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'lb150324') t
		order by new_magento_id, migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, oh.customer_id proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				moi.proforma_order_id, oh.order_no, 
				moi.customer_id_hist, oh.customer_id, moi.customer_email		
			from
					(select migra_website_name, etl_name,
						order_id, 800000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v
					where sum_row_total is not null) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join -- left: orders from customers not in magento
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'lb150324') oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lb150324'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lb150324'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lb150324'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, isnull(c_s.country_code, 'GB') country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
			left join
				Landing.map.gen_countries c_s on od.shipping_country = c_s.country_name
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lb150324'


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure migra.lnd_stg_get_migrate_lh150324_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-08-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (lh150324) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lh150324_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select migra_website_name, etl_name, 
			old_customer_id, new_magento_id, 
			proforma_customer_id, 
			new_customer_id, customer_email,
			@idETLBatchRun idETLBatchRun 
		from 
			(select distinct migra_website_name, etl_name,
				moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
				oh.customer_id proforma_customer_id, 
				oh.customer_id new_customer_id, c.email customer_email
			from
					(select migra_website_name, etl_name,
						order_id, 900000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'lh150324') t
		order by new_magento_id, migra_website_name


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, oh.customer_id proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				moi.proforma_order_id, oh.order_no, 
				moi.customer_id_hist, oh.customer_id, moi.customer_email		
			from
					(select migra_website_name, etl_name,
						order_id, 900000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v
					where sum_row_total is not null) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join -- left: orders from customers not in magento
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'lh150324') oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lh150324'


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lh150324'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lh150324'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, isnull(c_s.country_code, 'GB') country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
			left join
				Landing.map.gen_countries c_s on od.shipping_country = c_s.country_name
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lh150324'


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure migra.lnd_stg_get_migrate_vd150324_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-07-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (vd150324) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_vd150324_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select migra_website_name, etl_name, 
			old_customer_id, new_magento_id, 
			proforma_customer_id, 
			new_customer_id, customer_email,
			@idETLBatchRun idETLBatchRun 
		from 
			(select distinct migra_website_name, etl_name,
				moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
				oh.customer_id proforma_customer_id, 
				oh.customer_id new_customer_id, c.email customer_email
			from
					(select migra_website_name, etl_name,
						order_id, 700000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'vd150324') t
		order by new_magento_id, migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, oh.customer_id proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				moi.proforma_order_id, oh.order_no, 
				moi.customer_id_hist, oh.customer_id, moi.customer_email		
			from
					(select migra_website_name, etl_name,
						order_id, 700000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, customer_email, 
						created_at 
					from Landing.aux.migra_migrate_order_item_v
					where sum_row_total is not null) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join -- left: orders from customers not in magento
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'vd150324') oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'vd150324'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'vd150324'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'vd150324'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, isnull(c_s.country_code, 'GB') country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
			left join
				Landing.map.gen_countries c_s on od.shipping_country = c_s.country_name
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'vd150324'


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure migra.lnd_stg_get_migrate_VisioOptik_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-07-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (VisioOptik) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_VisioOptik_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select t.migra_website_name, t.etl_name, 
			t.old_customer_id, t.new_magento_id, 
			t.proforma_customer_id, 
			t.new_customer_id, t.customer_email,
			@idETLBatchRun idETLBatchRun 
		from 
				(select distinct migra_website_name, etl_name,
					moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
					oh.customer_id proforma_customer_id, 
					oh.customer_id new_customer_id, c.email customer_email
				from
						(select migra_website_name, etl_name,
							order_id, 600000000 + order_id proforma_order_id, num_order_line,
							source, 
							store_id, 
							customer_id_hist, customer_id, customer_email, 
							created_at 
						from Landing.aux.migra_migrate_order_item_v) moi
					inner join
						Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
					inner join
						Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
				where migra_website_name = 'VisioOptik') t
			-- left join
				-- Landing.migra.match_customer mc on t.migra_website_name = mc.migra_website_name and t.new_customer_id = mc.new_customer_id and t.old_customer_id = mc.old_customer_id
		-- where mc.new_customer_id is null
		order by new_magento_id, migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, oh.customer_id proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				moi.proforma_order_id, oh.order_no, 
				moi.customer_id_hist, oh.customer_id, moi.customer_email		
			from
					(select migra_website_name, etl_name,
						order_id, 600000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, isnull(customer_email, '') customer_email,
						created_at 
					from Landing.aux.migra_migrate_order_item_v
					where sum_row_total is not null) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join -- left: orders from customers not in magento
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'VisioOptik') oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
		--left join
			--Landing.migra.match_order_header mmoh on oh.migra_website_name = mmoh.migra_website_name and oh.order_id = mmoh.old_order_id,			
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	-- where mmoh.old_order_id is null
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			moi.num_order_line old_item_id, moi.num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				(select *
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'VisioOptik')  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name,
			-- left join	
				-- Landing.migra.match_order_line molh on moi.migra_website_name = molh.migra_website_name and moi.num_order_line = molh.old_item_id and moi.num_order_line = molh.num_order_line, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		-- where molh.old_item_id is null

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lb150324'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'VisioOptik'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, isnull(c_s.country_code, 'GB') country_id,
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
			left join
				Landing.map.gen_countries c_s on od.shipping_country = c_s.country_code
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'VisioOptik'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure migra.lnd_stg_get_migrate_lensway_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-07-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (lensway) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lensway_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)
	
		select moi.migra_website_name, moi.etl_name, 
			moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
			null proforma_customer_id, 
			c.entity_id new_customer_id, moi.customer_email customer_email,
			@idETLBatchRun idETLBatchRun
		from
				(select distinct migra_website_name, etl_name,
					customer_id_hist, null customer_id, customer_email
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'lensway') moi
			inner join
				Landing.mag.customer_entity_flat_aud c on moi.customer_email = c.email


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, null proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from 
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				null proforma_order_id, 'LWUK' + CONVERT(varchar, moi.order_id) order_no, 
				moi.customer_id_hist, moi.new_customer_id customer_id, moi.customer_email
			from
				(select moi.migra_website_name, moi.etl_name,
					moi.order_id, 
					moi.store_id, 
					moi.customer_id_hist, moi.customer_id, moi.customer_email, 
					moi.created_at, 
					mc.new_customer_id 
				from 
						Landing.aux.migra_migrate_order_item_v moi
					inner join
						Landing.migra.match_customer mc on moi.customer_id_hist = mc.old_customer_id and moi.migra_website_name = mc.migra_website_name 
				where moi.migra_website_name = 'lensway' 
					and moi.sum_row_total is not null) moi) oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line: Exclude 3134 with row_total = 0
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id, -- Exclude Some Lines for Glasses?? - Transform??
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lensway'
			and not (moi.product_id = 3134 and moi.row_total = 0)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lensway'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lensway'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, isnull(c_s.country_code, 'GB') country_id,
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
			left join
				Landing.map.gen_countries c_s on od.shipping_country = c_s.country_code
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lensway'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure migra.lnd_stg_get_migrate_lenson_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-07-2017
-- Changed: 
	--	15-02-2018	Joseba Bayona Sola	Add on migra_match_order_header migra_website_name = db clause
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (lenson) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lenson_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)
	
		select moi.migra_website_name, moi.etl_name, 
			moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
			null proforma_customer_id, 
			c.entity_id new_customer_id, c.email customer_email, 
			@idETLBatchRun idETLBatchRun
		from
				(select distinct migra_website_name, etl_name,
					customer_id_hist, customer_id--, customer_email
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'lenson') moi
			inner join
				Landing.mag.customer_entity_flat_aud c on moi.customer_id = c.entity_id -- moi.customer_email = c.email
--		WHERE moi.customer_id_hist =  890639

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, null proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from 
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				null proforma_order_id, 'LSUK' + CONVERT(varchar, moi.order_id) order_no, 
				moi.customer_id_hist, moi.new_customer_id customer_id, moi.customer_email
			from
				(select moi.migra_website_name, moi.etl_name,
					moi.order_id, 
					moi.store_id, 
					moi.customer_id_hist, moi.customer_id, moi.customer_email, 
					moi.created_at, 
					mc.new_customer_id 
				from 
						Landing.aux.migra_migrate_order_item_v moi
					inner join
						Landing.migra.match_customer mc on moi.customer_id_hist = mc.old_customer_id and moi.migra_website_name = mc.migra_website_name
							and moi.customer_email = mc.customer_email
				where moi.migra_website_name = 'lenson' 
					and moi.sum_row_total is not null) moi) oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id, -- Exclude Some Lines for Glasses?? - Transform??
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lenson'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lenson'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lenson'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, 
			case when (od.shipping_country = 'United Kingdom') then 'GB' else od.shipping_country end country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lenson'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	-- 	Call from SSIS (lnd_stg_sales_fact_order_migrate - lnd_stg_get_sales_order_header_migrate)

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra.lnd_stg_get_migrate_lensonnl_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (lenson NL) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lensonnl_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)
	
		select moi.migra_website_name, moi.etl_name, 
			moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
			null proforma_customer_id, 
			c.entity_id new_customer_id, c.email customer_email, 
			@idETLBatchRun idETLBatchRun
		from
				(select distinct migra_website_name, etl_name,
					customer_id_hist, customer_id--, customer_email
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'lensonnl') moi
			inner join
				Landing.mag.customer_entity_flat_aud c on moi.customer_id = c.entity_id -- moi.customer_email = c.email

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, null proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from 
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				null proforma_order_id, 'LSUNL' + CONVERT(varchar, moi.order_id) order_no, 
				moi.customer_id_hist, moi.new_customer_id customer_id, moi.customer_email
			from
				(select moi.migra_website_name, moi.etl_name,
					moi.order_id, 
					moi.store_id, 
					moi.customer_id_hist, moi.customer_id, moi.customer_email, 
					moi.created_at, 
					mc.new_customer_id 
				from 
						Landing.aux.migra_migrate_order_item_v moi
					inner join
						Landing.migra.match_customer mc on moi.customer_id_hist = mc.old_customer_id and moi.migra_website_name = mc.migra_website_name
							-- and moi.customer_email = mc.customer_email
				where moi.migra_website_name = 'lensonnl' 
					and moi.sum_row_total is not null) moi) oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id, -- Exclude Some Lines for Glasses?? - Transform??
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lensonnl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lensonnl'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, 
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lensonnl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, 
			case when (od.shipping_country = 'United Kingdom') then 'GB' else od.shipping_country end country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lensonnl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	-- 	Call from SSIS (lnd_stg_sales_fact_order_migrate - lnd_stg_get_sales_order_header_migrate)

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure migra.lnd_stg_get_migrate_lenswaynl_prepare
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-02-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from MIGRATE Tables (Lensway NL) - Prepare Data for Loading into Order DIM - FACT
-- ==========================================================================================

create procedure migra.lnd_stg_get_migrate_lenswaynl_prepare
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)
	
		select moi.migra_website_name, moi.etl_name, 
			moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
			null proforma_customer_id, 
			c.entity_id new_customer_id, c.email customer_email, 
			@idETLBatchRun idETLBatchRun
		from
				(select distinct migra_website_name, etl_name,
					customer_id_hist, customer_id--, customer_email
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'lenswaynl') moi
			inner join
				Landing.mag.customer_entity_flat_aud c on moi.customer_id = c.entity_id -- moi.customer_email = c.email

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, null proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from 
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				null proforma_order_id, 'LWNL' + CONVERT(varchar, moi.order_id) order_no, 
				moi.customer_id_hist, moi.new_customer_id customer_id, moi.customer_email
			from
				(select moi.migra_website_name, moi.etl_name,
					moi.order_id, 
					moi.store_id, 
					moi.customer_id_hist, moi.customer_id, moi.customer_email, 
					moi.created_at, 
					mc.new_customer_id 
				from 
						Landing.aux.migra_migrate_order_item_v moi
					inner join
						Landing.migra.match_customer mc on moi.customer_id_hist = mc.old_customer_id and moi.migra_website_name = mc.migra_website_name
							-- and moi.customer_email = mc.customer_email
				where moi.migra_website_name = 'lenswaynl' 
					and moi.sum_row_total is not null) moi) oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.migra_website_name = od.db and oh.order_id = od.old_order_id,
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	order by new_order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			num_order_line old_item_id, num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id, -- Exclude Some Lines for Glasses?? - Transform??
			@idETLBatchRun idETLBatchRun
		from 
				Landing.aux.migra_migrate_order_item_v  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where moi.migra_website_name = 'lenswaynl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	delete from Landing.migra.sales_flat_order_migrate
	delete from Landing.migra.sales_flat_order_item_migrate
	delete from Landing.migra.sales_flat_order_address_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' and migra_website_name = 'lenswaynl'
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_item_migrate
	insert into migra.sales_flat_order_item_migrate (migra_website_name, etl_name, 	
		item_id, order_id, store_id, created_at, updated_at,
		product_id, sku,
		qty_ordered, base_row_total, base_discount_amount,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		idETLBatchRun)

		select mol.migra_website_name, mol.etl_name, 	
			mol.new_order_line_id item_id, mol.new_order_id order_id, hoi.store_id, hoi.created_at created_at, hoi.created_at updated_at,
			hoi.product_id, hoi.sku,
			hoi.qty_ordered qty_ordered, hoi.row_total base_row_total, hoi.row_discount base_discount_amount,
			hoi.eye, 
			hoi.base_curve, hoi.diameter, hoi.power, hoi.cylinder, hoi.axis, hoi.[add], hoi.dominant, hoi.color,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_line mol
			inner join
				Landing.aux.migra_migrate_order_item_v hoi on mol.old_item_id = hoi.num_order_line and mol.migra_website_name = hoi.migra_website_name
		where mol.etl_name = 'migrate' and mol.migra_website_name = 'lenswaynl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- INSERT: migra.sales_flat_order_address_migrate
	insert into migra.sales_flat_order_address_migrate (migra_website_name, etl_name, 	
		entity_id, parent_id, customer_id, 
		postcode, country_id,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_id parent_id, moh.new_customer_id customer_id, 
			od.shipping_postcode, 
			case when (od.shipping_country = 'United Kingdom') then 'GB' else od.shipping_country end country_id,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				Landing.migra.migrate_orderdata_v od on moh.old_order_id = od.old_order_id and moh.migra_website_name = od.db
		where moh.etl_name = 'migrate' and moh.migra_website_name = 'lenswaynl'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- 	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	-- 	Call from SSIS (lnd_stg_sales_fact_order_migrate - lnd_stg_get_sales_order_header_migrate)

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




