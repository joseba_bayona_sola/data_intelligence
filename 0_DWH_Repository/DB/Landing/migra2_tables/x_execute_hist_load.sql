
	exec Landing.migra.lnd_stg_get_hist_prepare @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_header_o_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_store_cust_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_order_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_mark_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_line_product_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_header_measures_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_header_vat_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


		merge into Landing.aux.sales_order_header_dim_order trg
		using
			(select order_id_bk, product_type_oh_bk, order_qty_time, num_rep num_diff_product_type_oh
			from
				(select order_id_bk, product_type_oh_bk, 
					order_percentage, order_flag, order_qty_time, 
					count(*) over (partition by order_id_bk) num_rep, 
					dense_rank() over (partition by order_id_bk order by order_percentage desc, product_type_oh_bk) ord_rep
				from Landing.aux.sales_order_header_oh_pt) t
			where ord_rep = 1) src 
		on trg.order_id_bk = src.order_id_bk
		when matched then
			update set
				trg.product_type_oh_bk = src.product_type_oh_bk, 
				trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh;


------------------------------------------------------------------------