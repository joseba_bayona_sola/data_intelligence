use Landing
go 


-- Landing.migra.match_customer
create table migra.match_customer(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	old_customer_id			bigint NOT NULL, 
	new_magento_id			int, 
	proforma_customer_id	int, 
	new_customer_id			int NOT NULL,
	customer_email			varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.match_customer add constraint [PK_migra_match_customer]
	primary key clustered (migra_website_name, new_customer_id, old_customer_id);
go

--alter table migra.match_customer add constraint [U_migra_match_customer_old_customer_id]
--	unique (migra_website_name, old_customer_id);
--go
alter table migra.match_customer add constraint [U_migra_match_customer_customer_email]
	unique (migra_website_name, customer_email, old_customer_id);
go


alter table migra.match_customer add constraint [DF_migra_match_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.match_order_header
create table migra.match_order_header(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	old_order_id			bigint NOT NULL, 
	magento_order_id		int, 
	proforma_order_id		numeric(10, 0) NULL,
	proforma_order_no		varchar(50) NULL,
	new_order_id			bigint NOT NULL, 
	new_order_no			varchar(50) NOT NULL,
	old_customer_id			bigint NOT NULL, 
	proforma_customer_id	int, 
	new_customer_id			int NOT NULL,
	customer_email			varchar(255) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.match_order_header add constraint [PK_migra_match_order_header]
	primary key clustered (migra_website_name, new_order_id);
go

alter table migra.match_order_header add constraint [U_migra_match_order_header_old_order_id]
	unique (migra_website_name, old_order_id);
go

alter table migra.match_order_header add constraint [DF_migra_match_order_header_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.match_order_line
create table migra.match_order_line(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	old_item_id				bigint NOT NULL, 
	num_order_line			bigint NOT NULL, 
	proforma_line_id		int NULL,
	new_order_line_id		bigint NOT NULL, 
	new_order_id			bigint NOT NULL, 
	old_product_id			int NOT NULL,  
	proforma_product_id		int NULL,
	new_product_id			int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.match_order_line add constraint [PK_migra_match_order_line]
	primary key clustered (migra_website_name, new_order_line_id);
go

alter table migra.match_order_line add constraint [U_migra_match_order_line_old_item_id]
	unique (migra_website_name, old_item_id, num_order_line);
go

alter table migra.match_order_line add constraint [DF_migra_match_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 








-- Landing.migra.sales_flat_order_hist
create table migra.sales_flat_order_hist(
	migra_website_name				varchar(20) NOT NULL,
	etl_name						varchar(10) NOT NULL,
	entity_id						int NOT NULL,
	increment_id					varchar(50),
	store_id						int,
	customer_id						int, 
	created_at						datetime,
	updated_at						datetime,
	status							varchar(32),
	total_qty_ordered				decimal(12,4),
	base_subtotal					decimal(12,4),
	base_grand_total				decimal(12,4),
	customer_email					varchar(255),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL)
go 

alter table migra.sales_flat_order_hist add constraint [PK_migra_sales_flat_order_hist] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_hist add constraint [DF_migra_sales_flat_order_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_hist_aud
create table migra.sales_flat_order_hist_aud(
	migra_website_name				varchar(20) NOT NULL,
	etl_name						varchar(10) NOT NULL,
	entity_id						int NOT NULL,
	increment_id					varchar(50),
	store_id						int,
	customer_id						int, 
	created_at						datetime,
	updated_at						datetime,
	status							varchar(32),
	total_qty_ordered				decimal(12,4),
	base_subtotal					decimal(12,4),
	base_grand_total				decimal(12,4),
	customer_email					varchar(255),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	upd_ts							datetime)
go 

alter table migra.sales_flat_order_hist_aud add constraint [PK_migra_sales_flat_order_hist_aud] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_hist_aud add constraint [DF_migra_sales_flat_order_hist_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_item_hist
create table migra.sales_flat_order_item_hist(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	item_id					int NOT NULL,
	order_id				int NOT NULL,
	store_id				int,
	created_at				datetime,
	updated_at				datetime,
	product_id				int, 
	sku						varchar(255),
	qty_ordered				decimal(12, 4),
	base_row_total			decimal(12, 4),
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.sales_flat_order_item_hist add constraint [PK_migra_sales_flat_order_item] primary key clustered (item_id);
go

alter table migra.sales_flat_order_item_hist add constraint [DF_migra_sales_flat_order_item_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_item_hist_aud
create table migra.sales_flat_order_item_hist_aud(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	item_id					int NOT NULL,
	order_id				int NOT NULL,
	store_id				int,
	created_at				datetime,
	updated_at				datetime,
	product_id				int, 
	sku						varchar(255),
	qty_ordered				decimal(12, 4),
	base_row_total			decimal(12, 4),
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime)
go 

alter table migra.sales_flat_order_item_hist_aud add constraint [PK_migra_sales_flat_order_item_hist_aud] primary key clustered (item_id);
go

alter table migra.sales_flat_order_item_hist_aud add constraint [DF_migra_sales_flat_order_item_hist_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.migra.sales_flat_order_address_hist
create table migra.sales_flat_order_address_hist(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	entity_id				int NOT NULL,
	parent_id				int,
	customer_id				int,
	postcode				varchar(255),
	country_id				varchar(2), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.sales_flat_order_address_hist add constraint [PK_migra_sales_flat_order_address_hist] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_address_hist add constraint [DF_migra_sales_flat_order_address_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_address_hist_aud
create table migra.sales_flat_order_address_hist_aud(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	entity_id				int NOT NULL,
	parent_id				int,
	customer_id				int,
	postcode				varchar(255),
	country_id				varchar(2), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime)
go 

alter table migra.sales_flat_order_address_hist_aud add constraint [PK_migra_sales_flat_order_address_hist_aud] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_address_hist_aud add constraint [DF_migra_sales_flat_order_address_hist_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 









-- Landing.migra.sales_flat_order_migrate
create table migra.sales_flat_order_migrate(
	migra_website_name				varchar(20) NOT NULL,
	etl_name						varchar(10) NOT NULL,
	entity_id						int NOT NULL,
	increment_id					varchar(50),
	store_id						int,
	customer_id						int, 
	created_at						datetime,
	updated_at						datetime,
	status							varchar(32),
	total_qty_ordered				decimal(12,4),
	base_subtotal					decimal(12,4),
	base_shipping_amount			decimal(12,4),
	base_discount_amount			decimal(12,4),
	base_grand_total				decimal(12,4),
	customer_email					varchar(255),
	shipping_description			varchar(255),
	channel							varchar(64), 
	tax_rate						decimal(12, 4), 
	order_currency					varchar(3), 
	exchangeRate					decimal(12, 4),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL)
go 

alter table migra.sales_flat_order_migrate add constraint [PK_migra_sales_flat_order_migrate] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_migrate add constraint [DF_migra_sales_flat_order_migrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_migrate_aud
create table migra.sales_flat_order_migrate_aud(
	migra_website_name				varchar(20) NOT NULL,
	etl_name						varchar(10) NOT NULL,
	entity_id						int NOT NULL,
	increment_id					varchar(50),
	store_id						int,
	customer_id						int, 
	created_at						datetime,
	updated_at						datetime,
	status							varchar(32),
	total_qty_ordered				decimal(12,4),
	base_subtotal					decimal(12,4),
	base_shipping_amount			decimal(12,4),
	base_discount_amount			decimal(12,4),
	base_grand_total				decimal(12,4),
	customer_email					varchar(255),
	shipping_description			varchar(255),
	channel							varchar(64), 
	tax_rate						decimal(12, 4), 
	order_currency					varchar(3), 
	exchangeRate					decimal(12, 4),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	upd_ts							datetime)
go 

alter table migra.sales_flat_order_migrate_aud add constraint [PK_migra_sales_flat_order_migrate_aud] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_migrate_aud add constraint [DF_migra_sales_flat_order_migrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_item_migrate
create table migra.sales_flat_order_item_migrate(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	item_id					int NOT NULL,
	order_id				int NOT NULL,
	store_id				int,
	created_at				datetime,
	updated_at				datetime,
	product_id				int, 
	sku						varchar(255),
	qty_ordered				decimal(12, 4),
	base_row_total			decimal(12, 4),
	base_discount_amount	decimal(12, 4),
	eye						varchar(10),
	base_curve				varchar(20), 
	diameter				varchar(20), 
	power					varchar(20), 
	cylinder				varchar(20), 
	axis					varchar(20), 
	[add]					varchar(20), 
	dominant				varchar(20), 
	color					varchar(20), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.sales_flat_order_item_migrate add constraint [PK_migra_sales_flat_order_item_migrate] primary key clustered (item_id);
go

alter table migra.sales_flat_order_item_migrate add constraint [DF_migra_sales_flat_order_item_migrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_item_migrate_aud
create table migra.sales_flat_order_item_migrate_aud(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	item_id					int NOT NULL,
	order_id				int NOT NULL,
	store_id				int,
	created_at				datetime,
	updated_at				datetime,
	product_id				int, 
	sku						varchar(255),
	qty_ordered				decimal(12, 4),
	base_row_total			decimal(12, 4),
	base_discount_amount	decimal(12, 4),
	eye						varchar(10),
	base_curve				varchar(20), 
	diameter				varchar(20), 
	power					varchar(20), 
	cylinder				varchar(20), 
	axis					varchar(20), 
	[add]					varchar(20), 
	dominant				varchar(20), 
	color					varchar(20), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime)
go 

alter table migra.sales_flat_order_item_migrate_aud add constraint [PK_migra_sales_flat_order_item_migrate_aud] primary key clustered (item_id);
go

alter table migra.sales_flat_order_item_migrate_aud add constraint [DF_migra_sales_flat_order_item_migrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.migra.sales_flat_order_address_migrate
create table migra.sales_flat_order_address_migrate(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	entity_id				int NOT NULL,
	parent_id				int,
	customer_id				int,
	postcode				varchar(255),
	country_id				varchar(2), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra.sales_flat_order_address_migrate add constraint [PK_migra_sales_flat_order_address_migrate] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_address_migrate add constraint [DF_migra_sales_flat_order_address_migrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra.sales_flat_order_address_migrate_aud
create table migra.sales_flat_order_address_migrate_aud(
	migra_website_name		varchar(20) NOT NULL,
	etl_name				varchar(10) NOT NULL,
	entity_id				int NOT NULL,
	parent_id				int,
	customer_id				int,
	postcode				varchar(255),
	country_id				varchar(2), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime)
go 

alter table migra.sales_flat_order_address_migrate_aud add constraint [PK_migra_sales_flat_order_address_migrate_aud] primary key clustered (entity_id);
go

alter table migra.sales_flat_order_address_migrate_aud add constraint [DF_migra_sales_flat_order_address_migrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

