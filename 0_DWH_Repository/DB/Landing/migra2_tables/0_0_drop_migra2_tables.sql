
drop table Landing.migra.match_customer
go
drop table Landing.migra.match_order_header
go
drop table Landing.migra.match_order_line
go


drop table Landing.migra.sales_flat_order_hist
go
drop table Landing.migra.sales_flat_order_hist_aud
go

drop table Landing.migra.sales_flat_order_item_hist
go
drop table Landing.migra.sales_flat_order_item_hist_aud
go

drop table Landing.migra.sales_flat_order_address_hist
go
drop table Landing.migra.sales_flat_order_address_hist_aud
go



drop table Landing.migra.sales_flat_order_migrate
go
drop table Landing.migra.sales_flat_order_migrate_aud
go

drop table Landing.migra.sales_flat_order_item_migrate
go
drop table Landing.migra.sales_flat_order_item_migrate_aud
go

drop table Landing.migra.sales_flat_order_address_migrate
go
drop table Landing.migra.sales_flat_order_address_migrate_aud
go

