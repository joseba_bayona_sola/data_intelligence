
use Landing
go 

-------------------------------------------------------------
-- Landing.aux.migra_hist_dw_hist_order_item_v  

	select top 1000 migra_website_name, etl_name,
		order_id, num_order_line, source, 
		store_id_hist, store_id, 
		customer_id_hist, customer_id, customer_email, 
		created_at, grand_total, 
		product_id_hist, product_id, sku, product_family_name, category,
		original_qty_ordered, qty_ordered, row_total, original_row_total, 
		sum_row_total, 
		diff_oh_ol
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	-- where sum_row_total is null

	select count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  


	-- OH without OL
	select top 1000 migra_website_name, etl_name, count(*)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where sum_row_total is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name



	-- OH with no matching customer
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where customer_id is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

		select customer_id_hist, count(*), count(distinct order_id)
		from Landing.aux.migra_hist_dw_hist_order_item_v  
		where customer_id is null
		group by customer_id_hist
		order by customer_id_hist


	--OH per country/store		
	select migra_website_name, country_id, count(*)
	from Landing.migra.sales_flat_order_address_hist_aud
	group by migra_website_name, country_id
	order by migra_website_name, country_id

	select s.store_name, country_id, count(*)
	from 
			Landing.migra.sales_flat_order_address_hist_aud oas
		inner join
			Landing.migra.sales_flat_order_hist_aud o on oas.parent_id = o.entity_id
		inner join
			Landing.aux.mag_gen_store_v s on o.store_id = s.store_id
	group by s.store_name, country_id
	order by s.store_name, country_id



		-- OL with no matching product
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where product_id is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

	-- OL with matching product
	select top 1000 migra_website_name, etl_name, product_id, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where product_id is not null
	group by migra_website_name, etl_name, product_id
	order by migra_website_name, etl_name, product_id

	select top 1000 migra_website_name, etl_name, category, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where product_id is not null
	group by migra_website_name, etl_name, category
	order by migra_website_name, etl_name, category




	-- OH - OL = 0 - negative values 
	select migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where grand_total = 0 and sum_row_total = 0
	-- where grand_total = 0 and sum_row_total <> 0
	-- where sum_row_total = 0 and grand_total <> 0
	-- where grand_total < 0
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

	-- OH - OL value differences
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where diff_oh_ol > 1
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

		select top 1000 *
		from Landing.aux.migra_hist_dw_hist_order_item_v  
		where diff_oh_ol > 1
			and migra_website_name = 'vh1'

	select top 1000 migra_website_name, etl_name, 
		case 
			when (diff_oh_ol) between 1 and 2 then 2
			when (diff_oh_ol) between 2 and 5 then 5
			when (diff_oh_ol) between 5 and 10 then 10
			when (diff_oh_ol) between 10 and 100 then 100
			else 1000
		end diff,
		count(*), count(distinct order_id)
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where abs(grand_total - sum_row_total) > 1
	group by migra_website_name, etl_name, 
		case 
			when (diff_oh_ol) between 1 and 2 then 2
			when (diff_oh_ol) between 2 and 5 then 5
			when (diff_oh_ol) between 5 and 10 then 10
			when (diff_oh_ol) between 10 and 100 then 100
			else 1000
		end 
	order by migra_website_name, etl_name, diff

	select top 1000 order_id, store_id, customer_id, customer_email, created_at, 
		grand_total, 
		product_id, sku, product_family_name, category, 
		original_qty_ordered, qty_ordered, row_total, original_row_total, 
		sum_row_total,
		diff_oh_ol
	from Landing.aux.migra_hist_dw_hist_order_item_v  
	where migra_website_name = 'postoptics' and grand_total <> 0 -- postoptics - getlenses - masterlens - asda - vh1
	order by diff_oh_ol desc, order_id


	-- OL qty_ordered and row_total
		-- postoptics: num packages + OL row_total OK and OH grand_total wrong? Dailies much more expensive and need row_total * qty_ordered?
		-- getlenses: num package per month + OL row_total OK and OH grand_total OK
		-- masterlens: num lenses + OL row_total OK and grand_total without ship, discount
		-- asda: num_lenses + OL row_total OK and grand_total alwayd 1/2 - Duplicated values in OL?
		-- vh1: num_lenses + OL row_total OK and grand_total OK
		select top 10000 *
		from Landing.aux.migra_hist_dw_hist_order_item_v
		where migra_website_name = 'postoptics' and product_id_hist not in (-1, -2, -3, -4)
			-- and qty_ordered = 3
		order by qty_ordered desc, product_id

		select *
		from Landing.aux.migra_hist_dw_hist_order_item_v
		where migra_website_name = 'postoptics' and product_id_hist = 1083
			-- and qty_ordered = 3
		order by qty_ordered, product_id

		select migra_website_name, etl_name, category, qty_ordered, count(*)
		from Landing.aux.migra_hist_dw_hist_order_item_v
		where migra_website_name = 'asda'
		group by migra_website_name, etl_name, category, qty_ordered
		order by migra_website_name, etl_name, category, qty_ordered

		
		
		
------------------------------------------------------------------------------



-- Landing.aux.migra_migrate_order_item_v
	select top 1000 migra_website_name, etl_name,
		order_id, num_order_line,
		source, 
		store_id, 
		customer_id_hist, customer_id, customer_email, 
		created_at, 
		migrated, orderStatusName, order_status_migrate,
		grand_total, base_shipping_amount, base_discount_amount, 
		shipping_description, channel,
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate, 
		product_id_hist, product_id, sku, productName_hist, product_family_name, category,
		eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color,
		qty_ordered, row_total, 
		sum_row_total, 
		diff_oh_ol
	from Landing.aux.migra_migrate_order_item_v  
	where migra_website_name = 'lensonnl' -- vd150324 - lb150324 - lh150324 - visioOptik - lensway - lenson - lensonnl - lenswaynl

	select migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

	select migra_website_name, etl_name, store_id, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, store_id
	order by migra_website_name, etl_name, store_id

	select migra_website_name, etl_name, orderStatusName, order_status_migrate, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, orderStatusName, order_status_migrate
	order by migra_website_name, etl_name, orderStatusName, order_status_migrate

	select migra_website_name, etl_name, migrated, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, migrated
	order by migra_website_name, etl_name, migrated	

	-- OH without OL
	select top 1000 migra_website_name, etl_name, count(*)
	from Landing.aux.migra_migrate_order_item_v  
	where sum_row_total is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

	-- OH with no matching customer
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where customer_id is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

		select customer_id_hist, count(*), count(distinct order_id)
		from Landing.aux.migra_migrate_order_item_v  
		where customer_id is null
		group by customer_id_hist
		order by customer_id_hist


		-- OL with no matching product
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where product_id is null
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

			select top 1000 *
			from Landing.aux.migra_migrate_order_item_v  
			where product_id is null

	-- OL with matching product
	select top 1000 migra_website_name, etl_name, product_id, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where product_id is not null
	group by migra_website_name, etl_name, product_id
	order by migra_website_name, etl_name, product_id

	select top 1000 migra_website_name, etl_name, category, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where product_id is not null
	group by migra_website_name, etl_name, category
	order by migra_website_name, etl_name, category

	select migra_website_name, etl_name, product_id_hist, productName_hist, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where product_id = 2327
	group by migra_website_name, etl_name, product_id_hist, productName_hist 
	order by migra_website_name, etl_name, product_id_hist, productName_hist

	select migra_website_name, etl_name, product_id_hist, productName_hist, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where product_id between 3128 and 3154
	group by migra_website_name, etl_name, product_id_hist, productName_hist 
	order by migra_website_name, etl_name, product_id_hist, productName_hist

	-- OH - OL = 0 - negative values 
	select migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where grand_total = 0 and sum_row_total = 0
	-- where grand_total = 0 and sum_row_total <> 0
	-- where sum_row_total = 0 and grand_total <> 0
	-- where grand_total < 0
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name

	-- OH - OL value differences
	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where diff_oh_ol > 1
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name


	select top 1000 order_id, num_order_line, store_id, customer_id, customer_email, created_at, 
		grand_total, base_shipping_amount, base_discount_amount, 
		product_id, sku, product_family_name, category, 
		qty_ordered, row_total, 
		sum_row_total,
		diff_oh_ol
	from Landing.aux.migra_migrate_order_item_v  
	where migra_website_name = 'lh150324' -- vd150324 - lb150324 - lh150324 - visioOptik - lensway - lenson - lensonnl - lenswaynl
		and diff_oh_ol < 1
	order by order_id, num_order_line

	select top 1000 migra_website_name, etl_name, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where base_discount_amount <> 0 --  base_shipping_amount - base_discount_amount
	group by migra_website_name, etl_name
	order by migra_website_name, etl_name


		select migra_website_name, etl_name, category, qty_ordered, count(*)
		from Landing.aux.migra_migrate_order_item_v
		where migra_website_name = 'lensonnl' -- vd150324 - lb150324 - lb150324 - visioOptik - lensway - lenson - lensonnl - lenswaynl
		group by migra_website_name, etl_name, category, qty_ordered
		order by migra_website_name, etl_name, category, qty_ordered

	------------------

	select migra_website_name, etl_name, tax_rate, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, tax_rate
	order by migra_website_name, etl_name, tax_rate

	select migra_website_name, etl_name, tax_free_amount, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, tax_free_amount
	order by migra_website_name, etl_name, tax_free_amount


	select migra_website_name, etl_name, order_currency, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, order_currency
	order by migra_website_name, etl_name, order_currency


	select migra_website_name, etl_name, store_id, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	where exchangeRate <> 1
	group by migra_website_name, etl_name, store_id
	order by migra_website_name, etl_name, store_id	
	


	select migra_website_name, etl_name, shipping_description, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, shipping_description
	order by migra_website_name, etl_name, shipping_description


	select migra_website_name, etl_name, channel, count(*), count(distinct order_id)
	from Landing.aux.migra_migrate_order_item_v  
	group by migra_website_name, etl_name, channel
	order by migra_website_name, etl_name, channel

	------------------

	select power, count(*), count(distinct order_id) -- base_curve, diameter, power, cylinder, axis, [add], dominant
	from Landing.aux.migra_migrate_order_item_v  
	group by power
	order by power
