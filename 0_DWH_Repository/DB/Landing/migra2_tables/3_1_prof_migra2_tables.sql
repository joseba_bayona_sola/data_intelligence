
-- migra.match_customer
select top 1000 
	migra_website_name, etl_name, 
	old_customer_id, new_magento_id, 
	proforma_customer_id, 
	new_customer_id, customer_email,
	idETLBatchRun, ins_ts
from Landing.migra.match_customer

-- migra.match_order_header
select top 1000 
	migra_website_name, etl_name, 
	old_order_id, magento_order_id, 
	proforma_order_id, proforma_order_no, 
	new_order_id, new_order_no, 
	old_customer_id, proforma_customer_id, new_customer_id, customer_email,
	idETLBatchRun, ins_ts
from Landing.migra.match_order_header

-- migra.match_order_line
select top 1000 
	migra_website_name, etl_name, 
	old_item_id, num_order_line,
	proforma_line_id, 
	new_order_line_id, new_order_id, 
	old_product_id, proforma_product_id, new_product_id,
	idETLBatchRun, ins_ts
from Landing.migra.match_order_line




-- migra.sales_flat_order_hist
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	status, 
	total_qty_ordered, 
	base_subtotal, base_grand_total, 
	customer_email,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_hist

-- migra.sales_flat_order_hist_aud
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	status, 
	total_qty_ordered, 
	base_subtotal, base_grand_total, 
	customer_email,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_hist_aud


-- migra.sales_flat_order_item_hist
select top 1000 
	migra_website_name, etl_name, 	
	item_id, order_id, store_id, created_at, updated_at,
	product_id, sku,
	qty_ordered, base_row_total,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_item_hist

-- migra.sales_flat_order_item_hist_aud
select top 1000 
	migra_website_name, etl_name, 	
	item_id, order_id, store_id, created_at, updated_at,
	product_id, sku,
	qty_ordered, base_row_total,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_item_hist_aud


-- migra.sales_flat_order_address_hist
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, parent_id, customer_id, 
	postcode, country_id,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_address_hist

-- migra.sales_flat_order_address_hist_aud
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, parent_id, customer_id, 
	postcode, country_id,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_address_hist_aud







-- migra.sales_flat_order_migrate
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	status, 
	total_qty_ordered, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
	customer_email,
	shipping_description, channel, 
	tax_rate, order_currency, exchangeRate,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_migrate

-- migra.sales_flat_order_migrate_aud
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	status, 
	total_qty_ordered, 
	base_subtotal, base_discount_amount, base_grand_total, 
	customer_email,
	shipping_description, channel, 
	tax_rate, order_currency, exchangeRate,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_migrate_aud


-- migra.sales_flat_order_item_migrate
select top 1000 
	migra_website_name, etl_name, 	
	item_id, order_id, store_id, created_at, updated_at,
	product_id, sku,
	qty_ordered, base_row_total, base_discount_amount,
	eye, 
	base_curve, diameter, power, cylinder, axis, [add], dominant, color,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_item_migrate

-- migra.sales_flat_order_item_migrate_aud
select top 1000 
	migra_website_name, etl_name, 	
	item_id, order_id, store_id, created_at, updated_at,
	product_id, sku,
	qty_ordered, base_row_total, base_discount_amount,
	eye, 
	base_curve, diameter, power, cylinder, axis, [add], dominant, color,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_item_migrate_aud


-- migra.sales_flat_order_address_migrate
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, parent_id, customer_id, 
	postcode, country_id,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_address_migrate

-- migra.sales_flat_order_address_migrate_aud
select top 1000 
	migra_website_name, etl_name, 	
	entity_id, parent_id, customer_id, 
	postcode, country_id,
	idETLBatchRun, ins_ts
from Landing.migra.sales_flat_order_address_migrate_aud
