
use Landing
go

drop view migra.migrate_customerdata_v
go 

create view migra.migrate_customerdata_v as
	select 'lb150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lb150324.migrate_customerdata
	union
	select 'lh150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lh150324.migrate_customerdata
	union
	select 'vd150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_vd150324.migrate_customerdata
	union
	select 'VisioOptik' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_VisioOptik.migrate_customerdata
	union
	select 'lensway' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lensway.migrate_customerdata
	union
	select 'lenson' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lenson.migrate_customerdata
	union
	select 'lenswaynl' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lenswaynl.migrate_customerdata
	union
	select 'lensonnl' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lensonnl.migrate_customerdata
go


-- Only customers that have orders
drop view migra.migrate_customerdata_f_v
go 

create view migra.migrate_customerdata_f_v as
	select 'lb150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lb150324.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lb150324.migrate_orderdata)
	union
	select 'lh150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lh150324.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lh150324.migrate_orderdata)
	union
	select 'vd150324' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_vd150324.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_vd150324.migrate_orderdata)
	union
	select 'VisioOptik' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_VisioOptik.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_VisioOptik.migrate_orderdata)
	union
	select 'lensway' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lensway.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lensway.migrate_orderdata)
	union
	select 'lenson' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lenson.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lenson.migrate_orderdata)
	union
	select 'lenswaynl' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lensway.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lenswaynl.migrate_orderdata)
	union
	select 'lensonnl' db,
		old_customer_id, 
		new_magento_id, email, created_at, 
		domainID, domainName, 
		unsubscribe_newsletter, disable_saved_card, RAF_code
	from Landing.migra_lenson.migrate_customerdata
	where old_customer_id in 
		(select old_customer_id
		from Landing.migra_lensonnl.migrate_orderdata)
go



drop view migra.migrate_orderdata_v
go 

create view migra.migrate_orderdata_v as
	select 'lb150324' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lb150324.migrate_orderdata
	union
	select 'lh150324' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lh150324.migrate_orderdata
	union
	select 'vd150324' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_vd150324.migrate_orderdata
	union
	select 'VisioOptik' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_VisioOptik.migrate_orderdata
	union
	select 'lensway' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lensway.migrate_orderdata
	union
	select 'lenson' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lenson.migrate_orderdata
	union
	select 'lenswaynl' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lenswaynl.migrate_orderdata
	union
	select 'lensonnl' db,
		old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate
	from Landing.migra_lensonnl.migrate_orderdata

go	




drop view migra.migrate_orderlinedata_v
go 

create view migra.migrate_orderlinedata_v as
	select 'lb150324' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, productID product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_lb150324.migrate_orderlinedata
	union
	select 'lh150324' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, productID product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_lh150324.migrate_orderlinedata
	union
	select 'vd150324' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, productID product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_vd150324.migrate_orderlinedata
	union
	select 'VisioOptik' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, product_id product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_VisioOptik.migrate_orderlinedata
	union
	select 'lensway' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, product_id product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_lensway.migrate_orderlinedata
	union
	select 'lenson' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, product_id product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_lenson.migrate_orderlinedata
	union
	select 'lenswaynl' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, product_id product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, row_discount
	from Landing.migra_lenswaynl.migrate_orderlinedata
	union
	select 'lensonnl' db,
		old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, product_id product_id_def, 
		productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, null row_discount
	from Landing.migra_lensonnl.migrate_orderlinedata
go

drop view migra.migrate_order_id_mapping_v
go 

create view migra.migrate_order_id_mapping_v as
	select 'lb150324' db,
		internal_order_id, user_visible_order_id, 
		has_lenses, has_solutions
	from Landing.migra_lb150324.migrate_order_id_mapping
	union
	select 'lh150324' db,
		internal_order_id, user_visible_order_id, 
		has_lenses, has_solutions
	from Landing.migra_lh150324.migrate_order_id_mapping
	union
	select 'vd150324' db,
		internal_order_id, user_visible_order_id, 
		has_lenses, has_solutions
	from Landing.migra_vd150324.migrate_order_id_mapping
go