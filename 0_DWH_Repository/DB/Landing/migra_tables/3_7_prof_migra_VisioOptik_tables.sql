
select top 1000 old_customer_id, 
	new_magento_id, email, created_at, 
	domainID, domainName, 
	unsubscribe_newsletter, disable_saved_card, RAF_code, 
	idETLBatchRun, ins_ts
from Landing.migra_VisioOptik.migrate_customerdata


select top 1000 old_order_id, 
	magento_order_id, new_increment_id, 
	store_id, website_name, domainID, domainName, 
	email, old_customer_id, magento_customer_id, 
	created_at,
	migrated, orderStatusName, 
	shipping_description, channel, source_code, 
	billing_postcode, billing_country, shipping_postcode, shipping_country, 
	base_grand_total, base_shipping_amount, base_discount_amount, 
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
	tax_rate, tax_free_amount, 
	order_currency, exchangeRate,
	idETLBatchRun, ins_ts
from Landing.migra_VisioOptik.migrate_orderdata


select top 1000 old_item_id, 
	old_order_id, magento_order_id, 
	product_id, productID, productName, packsize, eye, 
	base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
	quantity, row_total,
	idETLBatchRun, ins_ts
from Landing.migra_VisioOptik.migrate_orderlinedata


