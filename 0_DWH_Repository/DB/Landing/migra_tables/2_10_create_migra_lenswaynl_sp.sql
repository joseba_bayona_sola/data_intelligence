use Landing
go 

--------------------- SP ----------------------------------

drop procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_customerdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_customerdata)
-- ==========================================================================================

create procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_customerdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_customer_id, 
				new_magento_id, email, created_at, 
				domainID, domainName, 
				unsubscribe_newsletter, disable_saved_card, RAF_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_customer_id, 
					new_magento_id, email, created_at, 
					domainID, domainName, 
					unsubscribe_newsletter, disable_saved_card, RAF_code 
				from lensway_nl.migrate_customerdata'')'	
					
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	20-02-2018	Joseba Bayona Sola	Won't be used - migrate_orderlinedata load in miglenswaynl_lnd_merge_migrate_updates
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderdata)
-- ==========================================================================================

create procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_order_id, 
				magento_order_id, new_increment_id, 
				store_id, website_name, domainID, domainName, 
				email, old_customer_id, magento_customer_id, 
				created_at,
				migrated, null orderStatusName, 
				shipping_description, null channel, null source_code, 
				billing_postcode, billing_country, shipping_postcode, shipping_country, 
				base_grand_total, base_shipping_amount, base_discount_amount, 
				null base_grand_total_exc_vat, null base_shipping_amount_exc_vat, null base_discount_amount_exc_vat, 
				null tax_rate, null tax_free_amount, 
				order_currency, null exchangeRate,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_order_id, 
					magento_order_id, new_increment_id, 
					store_id, website_name, domainID, domainName, 
					email, old_customer_id, magento_customer_id, 
					created_at,
					migrated, 
					shipping_description, 
					billing_postcode, billing_country, shipping_postcode, shipping_country, 
					base_grand_total, base_shipping_amount, base_discount_amount, 
					order_currency
				from lensway_nl.migrate_orderdata'')'


	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderlinedata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	20-02-2018	Joseba Bayona Sola	Won't be used - migrate_orderlinedata load in miglenswaynl_lnd_merge_migrate_updates
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderlinedata)
-- ==========================================================================================

create procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderlinedata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_item_id, 
				old_order_id, magento_order_id, 
				product_id, null productID, productName, packsize, eye, 
				base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
				quantity, row_total, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_item_id, 
					old_order_id, magento_order_id, 
					product_id, productName, packsize, eye, 
					base_curve, diameter, power, cylinder, axis, `add`, dominant, color, 
					quantity, row_total
				from lensway_nl.migrate_orderlinedata'')'
	
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderdata_magento
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-02-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderdata)
-- ==========================================================================================

create procedure migra_lenswaynl.miglenswaynl_lnd_get_migrate_orderdata_magento
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_order_id, 
				magento_order_id, new_increment_id, 
				email, old_customer_id, magento_customer_id, 
				created_at, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_order_id, 
					magento_order_id, new_increment_id, 
					email, old_customer_id, magento_customer_id, 
					created_at
				from lensway_nl.migrate_orderdata'')'


	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure migra_lenswaynl.miglenswaynl_lnd_merge_migrate_updates
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-02-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Inserts Lensway NL data in OH and OL migrate tables
-- ==========================================================================================

create procedure migra_lenswaynl.miglenswaynl_lnd_merge_migrate_updates
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- insert migrate_orderdata	
	insert into Landing.migra_lenswaynl.migrate_orderdata(old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate,
		idETLBatchRun)

		select  
			oh1.order_number old_order_id, 
			isnull(oh2.magento_order_id, 0) magento_order_id, isnull(oh2.new_increment_id, '') new_increment_id, 
			22 store_id, oh1.website_name website_name, 13 domainID, oh1.website_name domainName, 
			oh1.email_address email, c.old_customer_id, c.new_magento_id magento_customer_id, 
			convert(datetime, oh1.order_date) created_at,
			0 migrated, NULL orderStatusName, 
			shipping_description, NULL channel, NULL source_code, 
			oh1.postal_code billing_postcode, oh1.country billing_country, oh1.postal_code shipping_postcode, oh1.country shipping_country, 
			convert(decimal(12, 4), oh1.base_grand_total) base_grand_total, convert(decimal(12, 4), oh1.base_shipping_amount) base_shipping_amount, 
			convert(decimal(12, 4), oh1.base_discount_amount) base_discount_amount, 
			NULL base_grand_total_exc_vat, NULL base_shipping_amount_exc_vat, NULL base_discount_amount_exc_vat, 
			NULL tax_rate, NULL tax_free_amount, 
			order_currency, NULL exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.csv.lenswaynl_oh oh1
			left join
				Landing.migra_lenswaynl.migrate_orderdata_magento oh2 on oh1.order_number = oh2.old_order_id
			inner join
				Landing.migra_lenswaynl.migrate_customerdata c on oh1.customer_id = c.old_customer_id
		order by created_at desc

	insert into Landing.migra_lenswaynl.migrate_orderdata(old_order_id, 
		magento_order_id, new_increment_id, 
		store_id, website_name, domainID, domainName, 
		email, old_customer_id, magento_customer_id, 
		created_at,
		migrated, orderStatusName, 
		shipping_description, channel, source_code, 
		billing_postcode, billing_country, shipping_postcode, shipping_country, 
		base_grand_total, base_shipping_amount, base_discount_amount, 
		base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
		tax_rate, tax_free_amount, 
		order_currency, exchangeRate,
		idETLBatchRun)

		select  
			oh1.order_number old_order_id, 
			isnull(oh2.magento_order_id, 0) magento_order_id, isnull(oh2.new_increment_id, '') new_increment_id, 
			22 store_id, oh1.website_name website_name, 13 domainID, oh1.website_name domainName, 
			oh1.email_address email, c2.old_customer_id, c2.new_magento_id magento_customer_id, 
			convert(datetime, oh1.order_date) created_at,
			0 migrated, NULL orderStatusName, 
			shipping_description, NULL channel, NULL source_code, 
			oh1.postal_code billing_postcode, oh1.country billing_country, oh1.postal_code shipping_postcode, oh1.country shipping_country, 
			convert(decimal(12, 4), oh1.base_grand_total) base_grand_total, convert(decimal(12, 4), oh1.base_shipping_amount) base_shipping_amount, 
			convert(decimal(12, 4), oh1.base_discount_amount) base_discount_amount, 
			NULL base_grand_total_exc_vat, NULL base_shipping_amount_exc_vat, NULL base_discount_amount_exc_vat, 
			NULL tax_rate, NULL tax_free_amount, 
			order_currency, NULL exchangeRate,
			@idETLBatchRun idETLBatchRun
		from 
				Landing.csv.lenswaynl_oh oh1
			left join
				Landing.migra_lenswaynl.migrate_orderdata_magento oh2 on oh1.order_number = oh2.old_order_id
			left join
				Landing.migra_lenswaynl.migrate_customerdata c on oh1.customer_id = c.old_customer_id
			inner join
				(select email, new_magento_id, min(old_customer_id) old_customer_id 
				from Landing.migra_lenswaynl.migrate_customerdata
				group by email, new_magento_id) c2 on oh1.email_address = c2.email
		where c.old_customer_id is null
		order by created_at desc

	-- insert migrate_orderlinedata	
	insert into Landing.migra_lenswaynl.migrate_orderlinedata(old_item_id, 
		old_order_id, magento_order_id, 
		product_id, productID, productName, packsize, eye, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		quantity, row_total, row_discount,
		idETLBatchRun)	

		select ol.line_id old_item_id, 
			ol.order_number old_order_id, oh.magento_order_id magento_order_id, 
			ol.product_id product_id, ol.category_code productID, ol.product_name productName, p.multiplier packsize, ol.eye eye, 
			ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.addition, ol.dominant, left(ol.color, 20), 
			ol.ordered_quantity * p.multiplier quantity, 
			convert(decimal(12, 4), row_subtotal) row_total, convert(decimal(12, 4), ol_discount) ol_discount, 
			@idETLBatchRun idETLBatchRun
		from 
				Landing.csv.lenswaynl_ol ol
			inner join
				Landing.migra_lenswaynl.migrate_orderdata oh on ol.order_number = oh.old_order_id
			inner join
				Landing.csv.lenswaynl_product_mapping p on ol.product_id = p.magento_id and ol.category_code = p.category_code

	-- Update OH base_discount_amount
	merge into Landing.migra_lenswaynl.migrate_orderdata trg
	using 
		(select old_order_id, sum(row_discount) ol_discount
		from Landing.migra_lenswaynl.migrate_orderlinedata
		group by old_order_id) src on trg.old_order_id = src.old_order_id
	when matched 
		and trg.base_discount_amount <> src.ol_discount then

		update set
			trg.base_discount_amount = src.ol_discount;

			
			
	update Landing.migra_lenswaynl.migrate_orderdata
	set base_grand_total = base_grand_total + base_discount_amount
	where base_discount_amount < 0
		
	update Landing.migra_lenswaynl.migrate_orderdata
	set base_discount_amount = 0
	where base_discount_amount < 0

	update Landing.migra_lenswaynl.migrate_orderlinedata
	set row_discount = 0
	where row_discount < 0
			
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
