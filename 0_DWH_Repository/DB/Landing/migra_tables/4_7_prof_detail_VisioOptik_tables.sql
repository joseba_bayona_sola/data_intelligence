use Landing
go 

------------------------------------------------------------------------
----------------------- migrate_customerdata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_VisioOptik.migrate_customerdata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_customer_id) num_dist_rows
	from Landing.migra_VisioOptik.migrate_customerdata


------------------------------------------------------------------------
----------------------- migrate_orderdata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_VisioOptik.migrate_orderdata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_order_id) num_dist_rows
	from Landing.migra_VisioOptik.migrate_orderdata


------------------------------------------------------------------------
----------------------- migrate_orderlinedata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_VisioOptik.migrate_orderlinedata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_item_id) num_dist_rows
	from Landing.migra_VisioOptik.migrate_orderlinedata


