use Landing
go 

------------------------------------------------------------------------
----------------------- dw_hist_order ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_hist.dw_hist_order
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct order_id) num_dist_rows
	from Landing.migra_hist.dw_hist_order

------------------------------------------------------------------------
----------------------- dw_hist_order ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_hist.dw_hist_order_item
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct order_id) num_dist_rows
	from Landing.migra_hist.dw_hist_order_item





------------------------------------------------------------------------
----------------------- dw_hist_shipment ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_hist.dw_hist_shipment
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct shipment_id) num_dist_rows
	from Landing.migra_hist.dw_hist_shipment

------------------------------------------------------------------------
----------------------- dw_hist_shipment_item ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_hist.dw_hist_shipment_item
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct shipment_id) num_dist_rows
	from Landing.migra_hist.dw_hist_shipment_item
