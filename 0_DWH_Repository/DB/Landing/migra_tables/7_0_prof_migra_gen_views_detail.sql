
--------------------------------------
-- CUSTOMER ID
--------------------------------------

	------ HIST -----------------------
	-- Customers in Hist Tables that don't exist in Magento
	select o.*
	from 
			Landing.migra_hist.dw_hist_order o
		left join
			Landing.mag.customer_entity_flat_aud c on o.customer_id = c.entity_id
	where source in ('postoptics_db', 'getlenses_db') and
		c.entity_id is null
	order by source, created_at desc

	select o.customer_id, count(*)
	from 
			Landing.migra_hist.dw_hist_order o
		left join
			Landing.mag.customer_entity_flat_aud c on o.customer_id = c.entity_id
	where source in ('postoptics_db', 'getlenses_db') and
		c.entity_id is null
	group by o.customer_id
	order by count(*) desc, o.customer_id


	------ MIGRATE -----------------------


	-- MIGRATE TABLE CUSTOMER ATTRIBUTES PROFILING
		-- MIGRATE ORIGINAL CUSTOMER PK (old_customer_id) UNIQUE BETWEEN DIFFERENT WEBSITES: Only 1 repeating
		select old_customer_id, count(*)
		from Landing.migra.migrate_customerdata_v
		group by old_customer_id
		having count(*) > 1
		order by count(*) desc, old_customer_id

		select top 1000 db,
			old_customer_id, 
			new_magento_id, email, created_at	
		from Landing.migra.migrate_customerdata_v
		where old_customer_id = 5971

		-- MIGRATE NEW MAGENTO CUSTOMER PK (new_magento_id) VALUES: Only Lensway UK and Lenson UK have values
		select db, count(*)
		from Landing.migra.migrate_customerdata_v
		where new_magento_id is not null and new_magento_id <> 0
		group by db
		order by db



	-- MAGENTO TABLE CUSTOMER ATTRIBUTES PROFILING
		select top 1000 entity_id, email, created_at, 
			old_access_cust_no, old_web_cust_no, old_customer_id
		from Landing.mag.customer_entity_flat_aud
		-- where entity_id = 89894

		-- old_access_cust_no: Identifies the Source Web: (GLUK - GLIE - VISIO - LENSWAYUK - LENSONCOUK - ...)
		select old_access_cust_no, count(*) 
		from Landing.mag.customer_entity_flat_aud
		group by old_access_cust_no
		order by count(*) desc, old_access_cust_no

		-- old_web_cust_no: ????? 28k different (25 duplicated)
		select old_web_cust_no, count(*) 
		from Landing.mag.customer_entity_flat_aud
		group by old_web_cust_no
		order by count(*) desc, old_web_cust_no

		-- old_customer_id: Previous customer_id // 854 k different (16 k duplicated)
		select old_customer_id, count(*) 
		from Landing.mag.customer_entity_flat_aud
		group by old_customer_id
		order by count(*) desc, old_customer_id

		select count(*) -- 1.765.563 // 879.419
		from Landing.mag.customer_entity_flat_aud
		where old_customer_id is not null



		-- NEW DWH CUSTOMER ATTRIBUTES PROFILING
		select top 1000 idCustomer_sk, customer_id_bk, customer_email, created_at, 
			migrate_customer_f, migrate_customer_store, migrate_customer_id
		from Warehouse.gen.dim_customer
		-- where customer_id_bk = 1918079

			select count(*) -- 1.765.563 // 870.071
			from Warehouse.gen.dim_customer
			where migrate_customer_id is not null

			select migrate_customer_store, count(*)
			from Warehouse.gen.dim_customer
			where migrate_customer_id is null
			group by migrate_customer_store
			order by migrate_customer_store


		-- MATCHING MIGRATE CUSTOMERS with MAGENTO CUSTOMERS throug OLD_CUSTOMER_ID
		select top 1000 c_m.db, c_m.old_customer_id, c_m.new_magento_id, c_m.email, c_m.created_at, 
			c.idCustomer_sk, c.customer_id_bk, c.customer_email, c.created_at, c.migrate_customer_f, c.migrate_customer_store, c.migrate_customer_id
		from 
				Landing.migra.migrate_customerdata_v c_m
			left join
				Warehouse.gen.dim_customer c on c_m.old_customer_id = c.migrate_customer_id

		select c_m.db, c.migrate_customer_f, c.migrate_customer_store, count(*)
		from 
				Landing.migra.migrate_customerdata_v c_m
			left join
				Warehouse.gen.dim_customer c on c_m.old_customer_id = c.migrate_customer_id
		group by c_m.db, c.migrate_customer_f, c.migrate_customer_store
		order by c_m.db, c.migrate_customer_f, c.migrate_customer_store





--------------------------------------
-- ORDER ID
--------------------------------------


-- PK in original order_id: Collisions?

	------ HIST -----------------------
	-- In Hist Orders NO
	select order_id, count(*)
	from Landing.migra_hist.dw_hist_order
	group by order_id
	having count(*) > 1
	order by order_id

	-- Between Hist Orders and Magento: YES (asda, masterlens are same orders)
	select top 1000 count(*) over() num_tot,
		o_m.entity_id, o_m.increment_id, o_m.status, o_m.store_id, o_m.customer_id, o_m.created_at, o_m.base_grand_total,
		o_h.order_id, o_h.source, o_h.store_id, o_h.customer_id, o_h.created_at, o_h.grand_total
	from 
			Landing.mag.sales_flat_order_aud o_m
		inner join	
			Landing.migra_hist.dw_hist_order o_h on o_m.entity_id = o_h.order_id
	where o_h.source = 'masterlens_db' -- asda_db - masterlens_db - visio_db - visiondirect // asda_db and masterlens_db already in Magento as archived
	order by o_m.entity_id
		
		select o_h.source, o_h.store_id, count(*)
		from 
				Landing.mag.sales_flat_order_aud o_m
			inner join	
				Landing.migra_hist.dw_hist_order o_h on o_m.entity_id = o_h.order_id -- and o_m.status <> 'archived'
		group by o_h.source, o_h.store_id
		order by o_h.source, o_h.store_id

	------ MIGRATE -----------------------
	-- In Migrated Orders NO
	select old_order_id, count(*)
	from Landing.migra.migrate_orderdata_v
	group by old_order_id
	having count(*) > 1
	order by old_order_id

	select db, count(*)
	from Landing.migra.migrate_orderdata_v
	where magento_order_id is not null and magento_order_id <> 0
	group by db
	order by db

	-- Between Migrated Orders and Magento: YES (Completely Different)
	select top 1000 count(*) over() num_tot,
		o_m.entity_id, o_m.increment_id, o_m.status, o_m.store_id, o_m.customer_id, o_m.created_at, o_m.base_grand_total,
		o_h.old_order_id, o_h.db, o_h.domainName, o_h.old_customer_id, o_h.created_at, o_h.base_grand_total
	from 
			Landing.mag.sales_flat_order_aud o_m
		inner join	
			Landing.migra.migrate_orderdata_v o_h on o_m.entity_id = o_h.old_order_id
	order by o_m.entity_id

		select o_h.db, count(*)
		from 
				Landing.mag.sales_flat_order_aud o_m
			inner join	
				Landing.migra.migrate_orderdata_v o_h on o_m.entity_id = o_h.old_order_id and o_m.status <> 'archived'
		group by o_h.db
		order by o_h.db


	------ HIST vs MIGRATE -----------------------
	-- Between Hist Orders and Migrated Orders: YES (Same Orders: Orders that are in both)
	select top 1000 count(*) over() num_tot,
		o_h1.order_id, o_h1.source, o_h1.store_id, o_h1.customer_id, o_h1.created_at, o_h1.grand_total,
		o_h2.old_order_id, o_h2.db, o_h2.domainName, o_h2.old_customer_id, o_h2.created_at, o_h2.base_grand_total
	from 
			Landing.migra_hist.dw_hist_order o_h1 
		inner join	
			Landing.migra.migrate_orderdata_v o_h2 on o_h1.order_id = o_h2.old_order_id
	order by o_h1.order_id

		select o_h1.source, o_h2.db, count(*)
		from 
				Landing.migra_hist.dw_hist_order o_h1 
			inner join	
				Landing.migra.migrate_orderdata_v o_h2 on o_h1.order_id = o_h2.old_order_id
		group by o_h1.source, o_h2.db
		order by o_h1.source, o_h2.db

	--------------------------------------------------------------------------------------------
	
	-- LOOKING FOR MAGENTO ORDERS (archived) related to MIGRATE ORDERS
	-- Related magento order_id: Do we have it for Migrated Companies (archived orders)

		select old_order_id, count(*)
		from Landing.mag.sales_flat_order_aud o_m
		group by old_order_id
		having count(*) > 1
		order by old_order_id

		select *
		from
			(select entity_id, increment_id, status, store_id, customer_id, created_at, base_grand_total, old_order_id, 
				count(*) over (partition by old_order_id) num_rep
			from Landing.mag.sales_flat_order_aud o_m
			where old_order_id is not null) t
		where num_rep > 1
		order by num_rep, old_order_id, entity_id

		select count(*) over () num_tot, 
			entity_id, increment_id, status, store_id, customer_id, created_at, base_grand_total, old_order_id
		from Landing.mag.sales_flat_order_aud o_m
		where o_m.old_order_id is null and status = 'archived'
		order by created_at desc

			select s.store_id, s.name, count(*)
			from 
					Landing.mag.sales_flat_order_aud o_m
				inner join
					Landing.mag.core_store_aud s on o_m.store_id = s.store_id
			-- where o_m.old_order_id is null and status = 'archived'
			where o_m.old_order_id is not null -- and status = 'archived'
			-- where status <> 'archived'
			group by s.store_id, s.name
			order by s.store_id

		-- FULL JOIN
		select top 1000 db,
			o_mi.old_order_id, o_mi.domainName, o_mi.old_customer_id, o_mi.created_at, o_mi.orderStatusName, o_mi.base_grand_total,
			o_m.entity_id, o_m.increment_id, o_m.status, o_m.store_id, o_m.customer_id, o_m.created_at, o_m.base_grand_total, o_m.old_order_id, 
			case 
				when (o_mi.old_order_id is null) then '01' 
				when (o_m.entity_id is null) then '10' 
				else '11'
			end def_order
		from 
				Landing.migra.migrate_orderdata_v o_mi
			full join
				(select entity_id, increment_id, status, store_id, customer_id, created_at, base_grand_total, old_order_id
				from Landing.mag.sales_flat_order_aud o_m
				where o_m.old_order_id is not null) o_m on o_mi.old_order_id = o_m.old_order_id

				-- NUMBERS
				select def_order, db, count(*), 
					min(created_at_mi), max(created_at_mi)
				from
					(select db,
						o_mi.old_order_id, o_mi.domainName, o_mi.old_customer_id, o_mi.created_at created_at_mi, o_mi.orderStatusName, o_mi.base_grand_total base_grand_total_mi,
						o_m.entity_id, o_m.increment_id, o_m.status, o_m.store_id, o_m.customer_id, o_m.created_at, o_m.base_grand_total,
						case 
							when (o_mi.old_order_id is null) then '01' 
							when (o_m.entity_id is null) then '10' 
							else '11'
						end def_order
					from 
							Landing.migra.migrate_orderdata_v o_mi
						full join
							(select entity_id, increment_id, status, store_id, customer_id, created_at, base_grand_total, old_order_id
							from Landing.mag.sales_flat_order_aud o_m
							where o_m.old_order_id is not null) o_m on o_mi.old_order_id = o_m.old_order_id) t
				group by def_order, db
				order by def_order, db


		-- INNER JOIN
		select top 1000 db,
			o_mi.old_order_id, o_mi.domainName, o_mi.old_customer_id, o_mi.created_at, o_mi.orderStatusName, o_mi.base_grand_total,
			o_m.entity_id, o_m.increment_id, o_m.status, o_m.store_id, o_m.customer_id, o_m.created_at, o_m.base_grand_total, o_m.old_order_id
		from 
				Landing.migra.migrate_orderdata_v o_mi
			inner join
				(select entity_id, increment_id, status, store_id, customer_id, created_at, base_grand_total, old_order_id
				from Landing.mag.sales_flat_order_aud o_m
				where o_m.old_order_id is not null) o_m on o_mi.old_order_id = o_m.old_order_id
		where db = 'VisioOptik' -- lb150324 - lh150324 - vd150324 - VisioOptik - lensway - lenson 
		order by o_m.old_order_id, entity_id












--------------------------------------
-- PRODUCT ID
--------------------------------------

	------ HIST -----------------------
	-- Hist Products that NOT EXIST in MAGENTO
	select oh.product_id, pf.product_type_name, pf.product_family_name, oh.num
	from
			(select product_id, count(*) num
			from Landing.migra_hist.dw_hist_order_item
			group by product_id) oh
		left join
			Warehouse.prod.dim_product_family_v pf on oh.product_id = pf.product_id_magento
	where pf.product_id_magento is null
	order by oh.product_id

	select oh.product_id, oh.sku, oh.num
	from	
			(select product_id, sku, count(*) num
			from Landing.migra_hist.dw_hist_order_item
			group by product_id, sku) oh
		inner join
			(select oh.product_id, pf.product_type_name, pf.product_family_name, oh.num
			from
					(select product_id, count(*) num
					from Landing.migra_hist.dw_hist_order_item
					group by product_id) oh
				left join
					Warehouse.prod.dim_product_family_v pf on oh.product_id = pf.product_id_magento
			where pf.product_id_magento is null) t on oh.product_id = t.product_id
	order by oh.product_id



	------ MIGRATE -----------------------
	-- Migrate Products that NOT EXIST in MAGENTO
	select ol.product_id_def, pf.product_type_name, pf.product_family_name, ol.num
	from
			(select product_id_def, count(*) num
			from Landing.migra.migrate_orderlinedata_v
			group by product_id_def) ol
		left join
			Warehouse.prod.dim_product_family_v pf on ol.product_id_def = pf.product_id_magento
	where pf.product_id_magento is null
	order by product_id_def

		select db, count(*)
		from Landing.migra.migrate_orderlinedata_v
		where product_id_def > 9999 or product_id_def = 0
		group by db
		order by db

		select product_id_def, productName, db, count(*)
		from Landing.migra.migrate_orderlinedata_v
		where product_id_def > 9999 or product_id_def = 0
		group by product_id_def, productName, db
		order by product_id_def, productName, db

		-- 2327: ADHOCPROD: Ad Hoc Product for Non Mapping Products
		select product_id_magento, manufacturer_name, product_type_name, category_name, magento_sku, product_family_name
		from Warehouse.prod.dim_product_family_v 
		-- where magento_sku like 'ADHOC%'
		where magento_sku like 'DUMMY%'
		order by product_type_name, category_name, magento_sku

	-- Migrate Products that EXIST in MAGENTO as GENERIC
		select ol.product_id_def, pf.product_type_name, pf.product_family_name, ol.num
		from
				(select product_id_def, count(*) num
				from Landing.migra.migrate_orderlinedata_v
				group by product_id_def) ol
			inner join
				(select product_id_magento, product_type_name, category_name, product_family_name, magento_sku
				from Warehouse.prod.dim_product_family_v
				where magento_sku like 'DUMMY%') pf on ol.product_id_def = pf.product_id_magento
		order by pf.product_type_name, pf.category_name, ol.product_id_def

		select db, count(*)
		from 
				Landing.migra.migrate_orderlinedata_v ol
			inner join
				(select product_id_magento, product_type_name, category_name, product_family_name, magento_sku
				from Warehouse.prod.dim_product_family_v
				where magento_sku like 'DUMMY%') pf on ol.product_id_def = pf.product_id_magento		
		group by db
		order by db

-----------------------------------------------------
-- Products (300k unmapped + XX Mapped to generic)
