
select top 1000 old_customer_id, 
	new_magento_id, email, created_at, 
	domainID, domainName, 
	unsubscribe_newsletter, disable_saved_card, RAF_code, 
	idETLBatchRun, ins_ts
from Landing.migra_lensonnl.migrate_customerdata
order by created_at desc

select top 1000 old_order_id, 
	magento_order_id, new_increment_id, 
	store_id, website_name, domainID, domainName, 
	email, old_customer_id, magento_customer_id, 
	created_at,
	migrated, orderStatusName, 
	shipping_description, channel, source_code, 
	billing_postcode, billing_country, shipping_postcode, shipping_country, 
	base_grand_total, base_shipping_amount, base_discount_amount, 
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
	tax_rate, tax_free_amount, 
	order_currency, exchangeRate,
	idETLBatchRun, ins_ts
from Landing.migra_lensonnl.migrate_orderdata
-- where magento_order_id = 0
where magento_customer_id = 0
order by created_at desc


select top 1000 old_item_id, 
	old_order_id, magento_order_id, 
	product_id, productID, productName, packsize, eye, 
	base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
	quantity, row_total,
	idETLBatchRun, ins_ts
from Landing.migra_lensonnl.migrate_orderlinedata
where product_id is null

-----------------------------------------------------------------

	-- CHECK 1: Migrate Customers in Magento
	select mc.old_customer_id, mc.new_magento_id, c.entity_id,
		mc.email, c.email, 
		mc.created_at, c.created_at, 
		c.old_access_cust_no, c.old_customer_id
	from 
			Landing.migra_lensonnl.migrate_customerdata mc 
		left join
			Landing.mag.customer_entity_flat_aud c on mc.new_magento_id = c.entity_id
	-- where c.entity_id is null
	order by mc.new_magento_id

	-- CHECK 2: OH Customers in Migrate/Magento Customers
	select oh.old_order_id, 
		oh.magento_order_id, 
		oh.website_name, 
		oh.old_customer_id, oh.magento_customer_id, oh.email, 
		c.entity_id, c.email,
		oh.created_at
	from 
			Landing.migra_lensonnl.migrate_orderdata oh
		left join
			(select mc.old_customer_id, mc.new_magento_id, c.entity_id, c.email
			from 
					Landing.migra_lensonnl.migrate_customerdata mc 
				left join
					Landing.mag.customer_entity_flat_aud c on mc.new_magento_id = c.entity_id) c on oh.old_customer_id = c.old_customer_id
	where c.entity_id is null
	-- where oh.magento_order_id <> 0 and oh.email <> c.email
	-- where oh.magento_order_id = 0 and oh.email <> c.email
	order by oh.created_at desc

	-- CHECK 3: OH VALUES vs OL VALUES
	select *
	from
		(select oh.old_order_id, ol.old_order_id ol_id,
			oh.magento_order_id, 
			oh.email, oh.old_customer_id, 
			oh.created_at,
			oh.base_grand_total, oh.base_shipping_amount, oh.base_discount_amount, 
			oh.base_grand_total - oh.base_shipping_amount + (oh.base_discount_amount*-1) oh_subtotal,
			ol.ol_subtotal
		from 
				Landing.migra_lensonnl.migrate_orderdata oh
			left join
				(select old_order_id, sum(row_total) ol_subtotal
				from Landing.migra_lensonnl.migrate_orderlinedata
				group by old_order_id) ol on oh.old_order_id = ol.old_order_id) oh
	-- where ol_id is null
	where oh_subtotal <> ol_subtotal
	order by created_at desc

	-- CHECK 4: Map to Magento Product ID
	select ol.old_item_id, 
		ol.old_order_id, ol.magento_order_id, 
		ol.product_id, ol.productID, pm.magento_id, ol.productName, ol.packsize, ol.eye, 
		ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.[add], ol.dominant, ol.color, 
		ol.quantity, ol.row_total
	from 
			Landing.migra_lensonnl.migrate_orderlinedata ol
		left join
			Landing.csv.lensonnl_product_mapping pm on ol.productID = pm.product_id
	where ol.product_id is null -- and ol.row_total = 0
		-- and pm.magento_id is null
	order by ol.productName
