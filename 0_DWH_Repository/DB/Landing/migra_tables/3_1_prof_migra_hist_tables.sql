
-- migra_hist.dw_hist_order
select top 1000 order_id, 
	source, store_id, customer_id, 
	created_at, grand_total, 
	idETLBatchRun, ins_ts
from Landing.migra_hist.dw_hist_order

-- migra_hist.dw_hist_order
select top 1000 order_id, 
	source, product_id, sku, 
	qty_ordered, row_total, 
	idETLBatchRun, ins_ts
from Landing.migra_hist.dw_hist_order_item



-- migra_hist.dw_hist_shipment
select shipment_id, 
	source, store_id, customer_id, 
	created_at, 
	idETLBatchRun, ins_ts
from Landing.migra_hist.dw_hist_shipment

-- migra_hist.dw_hist_shipment_item
select shipment_id, 
	source, product_id, sku, 
	qty, 
	idETLBatchRun, ins_ts
from Landing.migra_hist.dw_hist_shipment_item
