use Landing
go 


drop procedure migra.lnd_stg_get_sales_order_header_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	Add customer_oder_seq_no_web attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header HIST in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_header_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	
	exec migra.lnd_stg_get_aux_sales_order_dim_fact_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		store_id_bk, market_id_bk, customer_id_bk, 
		country_id_shipping_bk, postcode_shipping, country_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk, 
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, shipping_description_bk, telesales_admin_username_bk, price_type_name_bk, discount_f, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reorder_f, reorder_date, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, coupon_id_bk, device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web, order_source, proforma,
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p,
		countries_registered_code,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 0 weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, 
		num_lines,
		@idETLBatchRun
	from Landing.aux.sales_dim_order_header

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_sales_order_header_product_type_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header Product Type HIST in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_header_product_type_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time,
		@idETLBatchRun
	from Landing.aux.sales_order_header_oh_pt

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_sales_order_line_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line HIST in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_line_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_line_id_bk, 
		order_id_bk order_id, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, order_id_bk, order_status_name_bk, price_type_name_bk, discount_f, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, glass_package_type_bk, 
		aura_product_f,
		countries_registered_code, product_type_vat, 

		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund,
		num_erp_allocation_lines,
		@idETLBatchRun
	from Landing.aux.sales_fact_order_line

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------------



drop procedure migra.lnd_stg_get_aux_sales_order_header_o_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: O entities
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_o_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o(order_id_bk)
		select entity_id order_id_bk
		from Landing.migra.sales_flat_order_hist
		where status <> 'archived' 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: O-I-S-CR relations
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o_i_s_cr

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o_i_s_cr(order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date)

		select oh.order_id_bk, 
			oh.order_id_bk invoice_id, oh.order_id_bk shipment_id, null creditmemo_id, 
			o.increment_id order_no, o.increment_id invoice_no, o.increment_id shipment_no, null creditmemo_no, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarOrderDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeOrderDate, 
			o.created_at order_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarInvoiceDate, o.created_at invoice_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarShipmentDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
			o.created_at shipment_date, 
			null idCalendarRefundDate, null refund_date
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_hist_aud o on oh.order_id_bk = o.entity_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_store_cust_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: Store - Customer Info
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_store_cust_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_store_cust

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_store_cust(order_id_bk,
		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk)

		select o.entity_id, 
			o.store_id store_id_bk, null market_id_bk, o.customer_id customer_id_bk, oas.entity_id shipping_address_id, oas.entity_id billing_address_id, 
			oas.country_id country_id_shipping, null region_id_shipping, oas.postcode postcode_shipping, 
			oas.country_id country_id_billing_bk, null region_id_billing_bk, oas.postcode postcode_billing, 
			null customer_unsubscribe_name_bk 
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_hist_aud o on oh.order_id_bk = o.entity_id
			inner join
				Landing.migra.sales_flat_order_address_hist_aud oas on o.entity_id = oas.parent_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_dim_order_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: Order related DIM data
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_dim_order_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_order

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_order(order_id_bk,
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time)

		select o.entity_id, 
			'SHIPMENT' order_stage_name_bk, 'OK' order_status_name_bk, 'N' adjustment_order, 'Web' order_type_name_bk, o.status status_bk, 
			null payment_method_name_bk, null cc_type_name_bk, null payment_id, null payment_method_code, 
			null shipping_description_bk, null shipping_description_code, null telesales_admin_username_bk, 'Not Required' prescription_method_name_bk,
			null reminder_type_name_bk, null reminder_period_bk, null reminder_date, 0 reminder_sent, 
			'N' reorder_f, null reorder_date, null reorder_profile_id, 0 automatic_reorder,
			'H' order_source, 'H' proforma,
			null product_type_oh_bk, null order_qty_time, null aura_product_p, 

			null referafriend_code, null referafriend_referer, 
			null postoptics_auto_verification, null presc_verification_method, null warehouse_approved_time
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_hist_aud o on oh.order_id_bk = o.entity_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_dim_mark_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	23-11-2017	Joseba Bayona Sola	Set 'Hist CH' as both channel and marketing channel
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: Marketing related DIM data
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_dim_mark_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_mark

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_mark(order_id_bk, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os)

		select o.entity_id, 
			'Hist CH' channel_name_bk, 'Hist CH' marketing_channel_name_bk, null publisher_id_bk, 
			null affilCode, null ga_medium, null ga_source, null ga_channel, null telesales_f, null sms_auto_f, null mutuelles_f,
			null coupon_id_bk, null coupon_code, null applied_rule_ids,
			null device_model_name_bk, null device_browser_name_bk, null device_os_name_bk, 
			null ga_device_model, null ga_device_browser, null ga_device_os
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_hist_aud o on oh.order_id_bk = o.entity_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_measures_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: Measures Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_measures_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT STATEMENT: From XXXX
	create table #sales_order_header_measures(
		order_id_bk							bigint NOT NULL,
		local_subtotal						decimal(12, 4),
		local_shipping						decimal(12, 4),
		local_discount						decimal(12, 4),
		local_total_inc_vat					decimal(12, 4),

		local_to_global_rate				decimal(12, 4),
		order_currency_code					varchar(255),
		discount_percent					decimal(12, 4))

		insert into #sales_order_header_measures(order_id_bk, 
			local_subtotal, local_shipping, local_discount, local_total_inc_vat, 
			local_to_global_rate, order_currency_code, 
			discount_percent)

			select oh.order_id_bk,  
				o.base_subtotal local_subtotal, 
				0 local_shipping, -- case when (o.base_grand_total - o.base_subtotal > 1) then o.base_grand_total - o.base_subtotal else 0 end local_shipping, 
				0 local_discount, -- case when (o.base_subtotal - o.base_grand_total > 1) then o.base_subtotal - o.base_grand_total else 0 end local_discount,  
				o.base_grand_total local_total_inc_vat, 
				case when (s.store_name in ('postoptics.co.uk', 'getlenses.co.uk', 'masterlens.co.uk', 'asda-contactlenses.co.uk')) then 1 else 1 end local_to_global_rate, -- local_to_global_rate
				case when (s.store_name in ('postoptics.co.uk', 'getlenses.co.uk', 'masterlens.co.uk', 'asda-contactlenses.co.uk')) then 'GBP' else 'EUR' end order_currency_code, -- null order_currency_code, 
				0 discount_percent 
				-- case when (o.base_subtotal = 0) then 0 else convert(decimal(10,1), (case when (o.base_subtotal - o.base_grand_total > 1) then o.base_subtotal - o.base_grand_total else 0 end * 100 / o.base_subtotal)) end discount_percent 
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.migra.sales_flat_order_hist_aud o on oh.order_id_bk = o.entity_id
				inner join
					Landing.aux.mag_gen_store_v s on o.store_id = s.store_id


	exec migra.lnd_stg_get_aux_sales_order_line_measures_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_oh_pt_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- UPDATE AURA PERCENTAGE ON ORDER HEADER
	update Landing.aux.sales_order_header_dim_order
	set aura_product_p = 0

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_measures

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_measures(order_id_bk, 
		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, 	
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, 
		local_to_global_rate, order_currency_code, 
		discount_percent, 
		num_lines)

		select o.order_id_bk, 
			ol.price_type_name_bk, ol.discount_f,
			ol.total_qty_unit, ol.total_qty_unit_refunded, ol.total_qty_pack, 
			o.local_subtotal, o.local_shipping, o.local_discount, 0 local_store_credit_used, 
			o.local_total_inc_vat, 	
			0 local_total_refund, 0 local_store_credit_given, 0 local_bank_online_given, 
			0 local_subtotal_refund, 0 local_shipping_refund, 0 local_discount_refund, 0 local_store_credit_used_refund, 0 local_adjustment_refund, 
			o.local_total_inc_vat local_total_aft_refund_inc_vat, o.local_total_inc_vat local_total_aft_refund_inc_vat_2, 
			o.local_to_global_rate, o.order_currency_code, 
			o.discount_percent, 
			ol.num_lines
		from 
				#sales_order_header_measures o
			inner join
 				(select order_id_bk, price_type_name_bk, discount_f,  
					total_qty_unit, total_qty_unit_refunded, total_qty_pack, num_lines
				from
					(select order_id_bk, price_type_name_bk,  
						max(discount_f) over (partition by order_id_bk) discount_f,
						count(*) over (partition by order_id_bk) num_lines, 
						sum(qty_unit) over (partition by order_id_bk) total_qty_unit, 
						sum(qty_unit_refunded) over (partition by order_id_bk) total_qty_unit_refunded, 
						sum(qty_pack) over (partition by order_id_bk) total_qty_pack,
						dense_rank() over (partition by order_id_bk order by ptn.priority, order_line_id_bk) num
					from 
							Landing.aux.sales_order_line_measures ol
						inner join
							Landing.map.sales_price_type_aud ptn on ol.price_type_name_bk = ptn.price_type) t
				where num = 1) ol on o.order_id_bk = ol.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_measures

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_vat_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: VAT Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_vat_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- INSERT STATEMENT: From XXXX
	create table #sales_order_header_vat(
		order_id_bk						bigint NOT NULL,
		countries_registered_code		varchar(10) NOT NULL,
		invoice_date					datetime,

		local_total_inc_vat				decimal(12, 4))

	insert into #sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat)

		select oh_sc.order_id_bk, 
			'NON-VAT' countries_registered_code, oh.invoice_date,
			oh_m.local_total_inc_vat
		from 
				Landing.aux.sales_order_header_store_cust oh_sc
			inner join
				(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
				from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
			inner join
				Landing.aux.sales_order_header_measures oh_m on oh_sc.order_id_bk = oh_m.order_id_bk

	exec Landing.migra.lnd_stg_get_aux_sales_order_line_vat_hist  @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_vat

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee,
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee,
		vat_percent, prof_fee_percent)

		select o_v.order_id_bk, 
			o_v.countries_registered_code, o_v.invoice_date, 
			o_v.local_total_inc_vat, ol_v.local_total_exc_vat, ol_v.local_total_vat, ol_v.local_total_prof_fee,
			o_v.local_total_inc_vat local_total_aft_refund_inc_vat, ol_v.local_total_aft_refund_exc_vat, ol_v.local_total_aft_refund_vat, ol_v.local_total_aft_refund_prof_fee, 
			case when (ol_v.local_total_exc_vat = 0) then 0 else ol_v.local_total_vat * 100 / ol_v.local_total_exc_vat end vat_percent, 
			case when (o_v.local_total_inc_vat = 0) then 0 else ol_v.local_total_prof_fee * 100 / o_v.local_total_inc_vat end prof_fee_percent
		from 
				#sales_order_header_vat o_v
			inner join
				(select order_id_bk, 
					sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_prof_fee) local_total_prof_fee, 
					sum(local_total_aft_refund_exc_vat) local_total_aft_refund_exc_vat, sum(local_total_aft_refund_vat) local_total_aft_refund_vat, sum(local_total_aft_refund_prof_fee) local_total_aft_refund_prof_fee
				from Landing.aux.sales_order_line_vat
				group by order_id_bk) ol_v on o_v.order_id_bk = ol_v.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_vat

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_oh_pt_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header HIST: OH - PT relation
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_oh_pt_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_oh_pt

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_oh_pt(order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time)

		select order_id_bk, product_type_oh_bk, 
			case when (total_local_subtotal = 0) then convert(decimal(12, 4), count(*) * 100 / total_lines) -- Due to OL with sum local_subtotal = 0
				else convert(decimal(12, 4), sum(local_subtotal) * 100 / total_local_subtotal) end order_percentage, 
			1 order_flag, max(qty_time) order_qty_time
		from
			(select olp.order_line_id_bk, olp.order_id_bk, 
				olp.product_type_oh_bk, 
				olm.qty_time, olm.local_subtotal, 
				sum(olm.local_subtotal) over (partition by olp.order_id_bk) total_local_subtotal, 
				count(*) over (partition by olp.order_id_bk) total_lines
			from 
					Landing.aux.sales_order_line_product olp
				inner join
					Landing.aux.sales_order_line_measures olm on olp.order_line_id_bk = olm.order_line_id_bk) ol
		group by order_id_bk, product_type_oh_bk, total_local_subtotal, total_lines
		order by order_id_bk, product_type_oh_bk, total_local_subtotal


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line HIST: O-I-S-CR relations
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_o_i_s_cr

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_o_i_s_cr(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk)

		select oi.item_id order_line_id_bk, oh.order_id_bk,
			oh.order_id_bk invoice_id, oh.order_id_bk shipment_id, null creditmemo_id, 
			oh.order_no order_no, oh.order_no invoice_no, oh.order_no shipment_no, null creditmemo_no, 
			oi.item_id invoice_line_id, oi.item_id shipment_line_id, null creditmemo_line_id,  
			1 num_shipment_lines, null num_creditmemo_lines,
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			null idCalendarRefundDate, null refund_date, 
			1 warehouse_id_bk
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.migra.sales_flat_order_item_hist_aud oi on oh.order_id_bk = oi.order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_product_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line HIST: Product Info
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_product_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_product

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_product(order_line_id_bk, order_id_bk, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, product_type_oh_bk)

		select oi.item_id, oi.order_id, 
			oi.product_id, 
			esi.bc, esi.di, esi.po, esi.cy, esi.ax, esi.ad, esi.do, esi.co, 
			null eye, 
			oi.sku sku_magento, null sku_erp, 
			null glass_vision_type_bk, null order_line_id_vision_type, null glass_package_type_bk, null order_line_id_package_type,
			'N' aura_product_f, ppt.product_type_oh_bk
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.migra.sales_flat_order_item_hist_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_edi_stock_item esi on oi.sku = esi.product_code and oi.product_id = esi.product_id
			inner join
				Landing.aux.prod_product_product_type_oh ppt on oi.product_id = ppt.product_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_measures_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	30-08-2017	Joseba Bayona Sola	Add Yearly category + CL prefix
-- ==========================================================================================
-- Description: Reads data from Order Line HIST: Measures Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_measures_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	create table #sales_order_header_line_measures(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL,
		order_status_name_bk			varchar(20) NOT NULL, 
		price_type_name_bk				varchar(40),
		discount_f						char(1),
		qty_unit						decimal(12, 4),
		qty_pack						int,
		qty_time						int,

		local_price_unit				decimal(12, 4),
		local_price_pack				decimal(12, 4),
		local_price_pack_discount		decimal(12, 4),

		local_subtotal					decimal(12, 4),
		local_discount					decimal(12, 4),
		
		discount_percent				decimal(12, 4),
		order_allocation_rate			decimal(12, 4))

	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, 
		discount_percent, 
		order_allocation_rate) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			'OK' order_status_name_bk, 'Regular' price_type_name_bk, 'N' discount_f,
			oi.qty_ordered qty_unit, 
			case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end qty_pack, 
			oi.qty_ordered / pc.num_months qty_time,
			case when (oi.qty_ordered = 0) then oi.base_row_total / 1 else oi.base_row_total / oi.qty_ordered end local_price_unit, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end local_price_pack, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end local_price_pack_discount, 
			oi.base_row_total local_subtotal, 
			0 local_discount, 0 discount_percent, -- null local_discount, null discount_percent, 
			null order_allocation_rate
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.migra.sales_flat_order_item_hist_aud oi on ol.order_line_id_bk = oi.item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 6
						when (category_bk = 'CL - Monthlies') then 3
						when (category_bk = 'CL - Yearly') then 1
						when (category_bk = 'Sunglasses') then 1
						when (category_bk = 'Glasses') then 1
						else 1
					end size 
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 

	-- PROBLEM: Data Quality Issues on OH grand_total - OL sum local_subtotal with values that are 0 or negative				
	--merge into #sales_order_header_line_measures trg
	--using 
	--	(select ol.order_line_id_bk, 
	--		convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) allocation_rate 
	--	from 
	--			#sales_order_header_line_measures ol
	--		inner join
	--			#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk) src
	--on trg.order_line_id_bk = src.order_line_id_bk
	--when matched then
	--	update set 
	--		trg.order_allocation_rate = src.allocation_rate;

	update #sales_order_header_line_measures
	set order_allocation_rate = 0

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_measures

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund,
		local_to_global_rate, order_currency_code, 
		discount_percent,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.order_status_name_bk, ol.price_type_name_bk, ol.discount_f,
			ol.qty_unit, 0 qty_unit_refunded, ol.qty_pack, ol.qty_time, 
			ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
			ol.local_subtotal, 0 local_shipping, ol.local_discount, 0 local_store_credit_used, 
			0 local_store_credit_given, 0 local_bank_online_given, 
			0 local_subtotal_refund, 0 local_shipping_refund,
			0 local_discount_refund, 0 local_store_credit_used_refund, 
			0 local_adjustment_refund,
			o.local_to_global_rate, o.order_currency_code, 
			ol.discount_percent,
			ol.order_allocation_rate, 0 order_allocation_rate_aft_refund, 0 order_allocation_rate_refund
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk
		order by ol.order_id_bk, ol.order_line_id_bk

	-- Not applying Shipping Cost - Discount
	update Landing.aux.sales_order_line_measures
	set local_total_inc_vat = local_subtotal

	update Landing.aux.sales_order_line_measures
	set local_total_aft_refund_inc_vat = local_subtotal

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_line_measures

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_vat_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line HIST: VAT Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_vat_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_line_vat(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL, 
		countries_registered_code		varchar(10) NOT NULL,
		product_type_vat				varchar(255),
		vat_type_rate_code				char(1), 
		invoice_date					datetime,
	
		vat_rate						decimal(12, 4),
		prof_fee_rate					decimal(12, 4),

		local_total_inc_vat				decimal(12, 4),
		local_total_exc_vat				decimal(12, 4),
		local_total_vat					decimal(12, 4),
		local_total_prof_fee			decimal(12, 4))

	insert into #sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat)

		select ol.order_line_id_bk, ol.order_id_bk, 
			oh_v.countries_registered_code, oh_v.invoice_date, 
			o_m.local_total_inc_vat
		from 
				#sales_order_header_vat oh_v
			inner join
				Landing.aux.sales_order_line_o_i_s_cr ol on oh_v.order_id_bk = ol.order_id_bk
			inner join
				Landing.aux.sales_order_line_measures o_m on ol.order_line_id_bk = o_m.order_line_id_bk

	-- Set product_type_vat
	merge into #sales_order_line_vat trg
	using 
		(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
			ol_v.product_type_vat
		from 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					case when (ppt.product_type_vat = 'Colour Plano' and ol_p.power_bk <> '+00.00') then 'Contact Lenses' else ppt.product_type_vat end product_type_vat
				from 
						#sales_order_line_vat ol_v
					inner join
						Landing.aux.sales_order_line_product ol_p on ol_v.order_line_id_bk = ol_p.order_line_id_bk
					inner join
						Landing.aux.prod_product_product_type_vat ppt on ol_p.product_id_bk = ppt.product_id) ol_v	
			inner join
				Landing.map.vat_product_type_vat_aud ptv on ol_v.product_type_vat = ptv.product_type_vat) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.product_type_vat = src.product_type_vat;

	-- Set Vat Values (vat_type_rate, vat_rate) - Prof Fee Values (prof_fee_rate) based on countries_registered_code - product_type_vat
	update #sales_order_line_vat 
	set 
		vat_type_rate_code = 'Z', 
		vat_rate = 0, prof_fee_rate = 0;

	-- Calculate Prof Fee Values (local_total_prof_fee - local_total_aft_refund_prof_fee)
	update #sales_order_line_vat
	set 
		local_total_prof_fee = local_total_inc_vat * (prof_fee_rate / 100)

	-- Calculate VAT Values (local_total_vat, local_total_exc_vat  - local_total_aft_refund_vat, local_total_aft_refund_exc_vat)
	update #sales_order_line_vat
	set 
		local_total_vat = (local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate), 
		local_total_exc_vat = local_total_inc_vat - ((local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate))

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_vat

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
		vat_rate, prof_fee_rate,
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		vat_percent, prof_fee_percent)

		select order_line_id_bk, order_id_bk, 
			countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
			vat_rate, prof_fee_rate,
			local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
			local_total_inc_vat local_total_aft_refund_inc_vat, local_total_exc_vat local_total_aft_refund_exc_vat, local_total_vat local_total_aft_refund_vat, local_total_prof_fee local_total_aft_refund_prof_fee, 
			case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_exc_vat end vat_percent, 
			case when (local_total_inc_vat = 0) then 0 else local_total_prof_fee * 100 / local_total_inc_vat end prof_fee_percent
		from #sales_order_line_vat

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_line_vat

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_dim_fact_hist
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	27-09-2017	Joseba Bayona Sola	Add call to SP for Activity Other (Lapsed - Reactivate)
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Executes Aux SPs and prepares data for Order Dim - Fact Tables - HIST
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_dim_fact_hist
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	exec migra.lnd_stg_get_aux_sales_order_header_o_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_store_cust_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_order_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_mark_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_line_product_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_header_measures_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- exec migra.lnd_stg_get_aux_sales_order_line_measures_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun (Call inside lnd_stg_get_aux_sales_order_header_measures_hist)


	exec migra.lnd_stg_get_aux_sales_order_header_vat_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- exec migra.lnd_stg_get_aux_sales_order_header_oh_pt_hist @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun (Call inside lnd_stg_get_aux_sales_order_header_measures_hist)

		merge into Landing.aux.sales_order_header_dim_order trg
		using
			(select order_id_bk, product_type_oh_bk, order_qty_time, num_rep num_diff_product_type_oh
			from
				(select order_id_bk, product_type_oh_bk, 
					order_percentage, order_flag, order_qty_time, 
					count(*) over (partition by order_id_bk) num_rep, 
					dense_rank() over (partition by order_id_bk order by order_percentage desc, product_type_oh_bk) ord_rep
				from Landing.aux.sales_order_header_oh_pt) t
			where ord_rep = 1) src 
		on trg.order_id_bk = src.order_id_bk
		when matched then
			update set
				trg.product_type_oh_bk = src.product_type_oh_bk, 
				trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh;

		

	truncate table Landing.aux.sales_dim_order_header

	insert into Landing.aux.sales_dim_order_header (order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date,

		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing,
		customer_unsubscribe_name_bk,

		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os,

		countries_registered_code, 

		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent,
		num_lines, 
		idETLBatchRun)

		select oh.order_id_bk, 
			oh.invoice_id, oh.shipment_id, oh.creditmemo_id, 
			oh.order_no, oh.invoice_no, oh.shipment_no, oh.creditmemo_no, 
			oh.idCalendarOrderDate, oh.idTimeOrderDate, oh.order_date, 
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			oh.idCalendarRefundDate, oh.refund_date,

			oh_sc.store_id_bk, oh_sc.market_id_bk, oh_sc.customer_id_bk, oh_sc.shipping_address_id, oh_sc.billing_address_id,
			oh_sc.country_id_shipping_bk, oh_sc.region_id_shipping_bk, oh_sc.postcode_shipping, 
			oh_sc.country_id_billing_bk, oh_sc.region_id_billing_bk, oh_sc.postcode_billing,
			oh_sc.customer_unsubscribe_name_bk,

			oh_o.order_stage_name_bk, oh_o.order_status_name_bk, oh_o.adjustment_order, oh_o.order_type_name_bk, oh_o.status_bk, 
			oh_o.payment_method_name_bk, oh_o.cc_type_name_bk, oh_o.payment_id, oh_o.payment_method_code,
			oh_o.shipping_description_bk, oh_o.shipping_description_code, oh_o.telesales_admin_username_bk, oh_o.prescription_method_name_bk,
			oh_o.reminder_type_name_bk, oh_o.reminder_period_bk, oh_o.reminder_date, oh_o.reminder_sent, 
			oh_o.reorder_f, oh_o.reorder_date, oh_o.reorder_profile_id, oh_o.automatic_reorder,
			oh_o.order_source, oh_o.proforma,
			oh_o.product_type_oh_bk, oh_o.order_qty_time, oh_o.num_diff_product_type_oh, oh_o.aura_product_p, 

			oh_o.referafriend_code, oh_o.referafriend_referer, 
			oh_o.postoptics_auto_verification, oh_o.presc_verification_method, oh_o.warehouse_approved_time,

			oh_m.channel_name_bk, oh_m.marketing_channel_name_bk, oh_m.publisher_id_bk, 
			oh_m.affilCode, oh_m.ga_medium, oh_m.ga_source, oh_m.ga_channel, oh_m.telesales_f, oh_m.sms_auto_f, oh_m.mutuelles_f, 
			oh_m.coupon_id_bk, oh_m.coupon_code, oh_m.applied_rule_ids,
			oh_m.device_model_name_bk, oh_m.device_browser_name_bk, oh_m.device_os_name_bk, 
			oh_m.ga_device_model, oh_m.ga_device_browser, oh_m.ga_device_os,

			oh_vat.countries_registered_code, 

			oh_me.price_type_name_bk, oh_me.discount_f,
			oh_me.total_qty_unit, oh_me.total_qty_unit_refunded, oh_me.total_qty_pack, oh_me.weight, 
			oh_me.local_subtotal, oh_me.local_shipping, oh_me.local_discount, oh_me.local_store_credit_used, 
			oh_me.local_total_inc_vat, oh_vat.local_total_exc_vat, oh_vat.local_total_vat, oh_vat.local_total_prof_fee, 
			oh_me.local_total_refund, oh_me.local_store_credit_given, oh_me.local_bank_online_given, 
			oh_me.local_subtotal_refund, oh_me.local_shipping_refund, oh_me.local_discount_refund, oh_me.local_store_credit_used_refund, oh_me.local_adjustment_refund, 
			oh_me.local_total_aft_refund_inc_vat, oh_me.local_total_aft_refund_inc_vat_2, oh_vat.local_total_aft_refund_exc_vat, oh_vat.local_total_aft_refund_vat, oh_vat.local_total_aft_refund_prof_fee, 
			oh_me.local_product_cost, oh_me.local_shipping_cost, oh_me.local_total_cost, oh_me.local_margin, 
			oh_me.local_to_global_rate, oh_me.order_currency_code, 
			oh_me.discount_percent, oh_vat.vat_percent, oh_vat.prof_fee_percent,
			oh_me.num_lines, 

			@idETLBatchRun idETLBatchRun
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.aux.sales_order_header_store_cust oh_sc	on oh.order_id_bk = oh_sc.order_id_bk
		inner join
			Landing.aux.sales_order_header_dim_order oh_o on oh.order_id_bk = oh_o.order_id_bk		
		inner join
			Landing.aux.sales_order_header_dim_mark oh_m on oh.order_id_bk = oh_m.order_id_bk
		inner join
			Landing.aux.sales_order_header_measures oh_me on oh.order_id_bk = oh_me.order_id_bk
		inner join
			Landing.aux.sales_order_header_vat oh_vat on oh.order_id_bk = oh_vat.order_id_bk 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message



	truncate table Landing.aux.sales_fact_order_line

	insert into Landing.aux.sales_fact_order_line(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, 

		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, 

		countries_registered_code, product_type_vat,

		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
		num_erp_allocation_lines, 

		idETLBatchRun)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.invoice_id, ol.shipment_id, ol.creditmemo_id, 
			ol.order_no, ol.invoice_no, ol.shipment_no, ol.creditmemo_no,
			ol.invoice_line_id, ol.shipment_line_id, ol.creditmemo_line_id,  	
			ol.num_shipment_lines, ol.num_creditmemo_lines,
			ol.idCalendarInvoiceDate, ol.invoice_date, 
			ol.idCalendarShipmentDate, ol.idTimeShipmentDate, ol.shipment_date, 
			ol.idCalendarRefundDate, ol.refund_date, 
			ol.warehouse_id_bk, 

			ol_p.product_id_bk, 
			ol_p.base_curve_bk, ol_p.diameter_bk, ol_p.power_bk, ol_p.cylinder_bk, ol_p.axis_bk, ol_p.addition_bk, ol_p.dominance_bk, ol_p.colour_bk, ol_p.eye,
			ol_p.sku_magento, ol_p.sku_erp, 
			ol_p.glass_vision_type_bk, ol_p.order_line_id_vision_type, ol_p.glass_package_type_bk, ol_p.order_line_id_package_type,
			ol_p.aura_product_f, 

			ol_vat.countries_registered_code, ol_vat.product_type_vat,

			ol_me.order_status_name_bk, ol_me.price_type_name_bk, ol_me.discount_f,
			ol_me.qty_unit, ol_me.qty_unit_refunded, ol_me.qty_pack, ol_me.qty_time, ol_me.weight, 
			ol_me.local_price_unit, ol_me.local_price_pack, ol_me.local_price_pack_discount, 
			ol_me.local_subtotal, ol_me.local_shipping, ol_me.local_discount, ol_me.local_store_credit_used, 
			ol_me.local_total_inc_vat, ol_vat.local_total_exc_vat, ol_vat.local_total_vat, ol_vat.local_total_prof_fee, 
			ol_me.local_store_credit_given, ol_me.local_bank_online_given, 
			ol_me.local_subtotal_refund, ol_me.local_shipping_refund, ol_me.local_discount_refund, ol_me.local_store_credit_used_refund, ol_me.local_adjustment_refund, 
			ol_me.local_total_aft_refund_inc_vat, ol_vat.local_total_aft_refund_exc_vat, ol_vat.local_total_aft_refund_vat, ol_vat.local_total_aft_refund_prof_fee, 
			ol_me.local_product_cost, ol_me.local_shipping_cost, ol_me.local_total_cost, ol_me.local_margin, 
			ol_me.local_to_global_rate, ol_me.order_currency_code, 
			ol_me.discount_percent, ol_vat.vat_percent, ol_vat.prof_fee_percent, ol_vat.vat_rate, ol_vat.prof_fee_rate,
			ol_me.order_allocation_rate, ol_me.order_allocation_rate_aft_refund, ol_me.order_allocation_rate_refund, 
			ol_me.num_erp_allocation_lines, 

			@idETLBatchRun idETLBatchRun

		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product ol_p on ol.order_line_id_bk = ol_p.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_measures ol_me on ol.order_line_id_bk = ol_me.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_vat ol_vat on ol.order_line_id_bk = ol_vat.order_line_id_bk


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	exec mag.lnd_stg_get_aux_act_activity_sales @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_act_activity_other @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


-------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------


drop procedure migra.lnd_stg_get_sales_order_header_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	Add customer_oder_seq_no_web attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_header_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	
	exec migra.lnd_stg_get_aux_sales_order_dim_fact_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		store_id_bk, market_id_bk, customer_id_bk, 
		country_id_shipping_bk, postcode_shipping, country_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk, 
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, null shipping_description_bk, telesales_admin_username_bk, price_type_name_bk, discount_f, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reorder_f, reorder_date, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, coupon_id_bk, device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web, order_source, proforma,
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p,
		countries_registered_code,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 0 weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, 
		num_lines,
		@idETLBatchRun
	from Landing.aux.sales_dim_order_header

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_sales_order_header_product_type_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header Product Type MIGRATE in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_header_product_type_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time,
		@idETLBatchRun
	from Landing.aux.sales_order_header_oh_pt

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_sales_order_line_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line MIGRATE in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure migra.lnd_stg_get_sales_order_line_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_line_id_bk, 
		order_id_bk order_id, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, order_id_bk, order_status_name_bk, price_type_name_bk, discount_f, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, glass_package_type_bk, 
		aura_product_f,
		countries_registered_code, product_type_vat, 

		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund,
		num_erp_allocation_lines,
		@idETLBatchRun
	from Landing.aux.sales_fact_order_line

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


--------------------------------------------------


drop procedure migra.lnd_stg_get_aux_sales_order_header_o_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: O entities
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_o_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o(order_id_bk)
		select entity_id order_id_bk
		from Landing.migra.sales_flat_order_migrate
		where status <> 'archived'

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: O-I-S-CR relations
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o_i_s_cr

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o_i_s_cr(order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date)

		select oh.order_id_bk, 
			oh.order_id_bk invoice_id, oh.order_id_bk shipment_id, null creditmemo_id, 
			o.increment_id order_no, o.increment_id invoice_no, o.increment_id shipment_no, null creditmemo_no, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarOrderDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeOrderDate, 
			o.created_at order_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarInvoiceDate, o.created_at invoice_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarShipmentDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
			o.created_at shipment_date, 
			null idCalendarRefundDate, null refund_date
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_store_cust_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: Store - Customer Info
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_store_cust_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_store_cust

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_store_cust(order_id_bk,
		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk)

		select o.entity_id, 
			o.store_id store_id_bk, null market_id_bk, o.customer_id customer_id_bk, oas.entity_id shipping_address_id, oas.entity_id billing_address_id, 
			oas.country_id country_id_shipping, null region_id_shipping, oas.postcode postcode_shipping, 
			oas.country_id country_id_billing_bk, null region_id_billing_bk, oas.postcode postcode_billing, 
			null customer_unsubscribe_name_bk 
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id
			inner join
				Landing.migra.sales_flat_order_address_migrate_aud oas on o.entity_id = oas.parent_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_dim_order_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: Order related DIM data
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_dim_order_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_order

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_order(order_id_bk,
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time)

		select o.entity_id, 
			'SHIPMENT' order_stage_name_bk, 'OK' order_status_name_bk, 'N' adjustment_order, 'Web' order_type_name_bk, o.status status_bk, 
			null payment_method_name_bk, null cc_type_name_bk, null payment_id, null payment_method_code, 
			shipping_description shipping_description_bk, null shipping_description_code, null telesales_admin_username_bk, 'Not Required' prescription_method_name_bk,
			null reminder_type_name_bk, null reminder_period_bk, null reminder_date, 0 reminder_sent, 
			'N' reorder_f, null reorder_date, null reorder_profile_id, 0 automatic_reorder,
			'P' order_source, 
			case when (wpr.proforma_date is not null and wpr.proforma_date < o.created_at) then 'N' else 'Y' end proforma,
			null product_type_oh_bk, null order_qty_time, null aura_product_p, 

			null referafriend_code, null referafriend_referer, 
			null postoptics_auto_verification, null presc_verification_method, null warehouse_approved_time
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id		
			inner join
				Landing.aux.mag_gen_store_v s on o.store_id = s.store_id
			left join
				Landing.map.migra_website_proforma_aud wpr on s.website_group = wpr.website_group

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_dim_mark_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	23-11-2017	Joseba Bayona Sola	Set 'Migration CH' as channel and marketing channel with logic depending values
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: Marketing related DIM data
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_dim_mark_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_mark

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_mark(order_id_bk, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os)

		select o.entity_id, 
			-- case when (channel = '') then null else channel end channel_name_bk, 
			'Migration CH' channel_name_bk, isnull(mc.marketing_channel_name, 'Migration CH') marketing_channel_name_bk, null publisher_id_bk, 
			null affilCode, null ga_medium, null ga_source, null ga_channel, null telesales_f, null sms_auto_f, null mutuelles_f,
			null coupon_id_bk, null coupon_code, null applied_rule_ids,
			null device_model_name_bk, null device_browser_name_bk, null device_os_name_bk, 
			null ga_device_model, null ga_device_browser, null ga_device_os
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id
			left join
				Landing.map.sales_ch_migrate_channel_mch mc on o.channel = mc.channel

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_measures_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: Measures Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_measures_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- INSERT STATEMENT: From XXXX
	create table #sales_order_header_measures(
		order_id_bk							bigint NOT NULL,
		local_subtotal						decimal(12, 4),
		local_shipping						decimal(12, 4),
		local_discount						decimal(12, 4),
		local_total_inc_vat					decimal(12, 4),

		local_to_global_rate				decimal(12, 4),
		order_currency_code					varchar(255),
		discount_percent					decimal(12, 4))

		insert into #sales_order_header_measures(order_id_bk, 
			local_subtotal, local_shipping, local_discount, local_total_inc_vat, 
			local_to_global_rate, order_currency_code, 
			discount_percent)

			select oh.order_id_bk,  
				o.base_subtotal local_subtotal, 
				o.base_shipping_amount local_shipping, 
				o.base_discount_amount local_discount, 
				o.base_grand_total local_total_inc_vat, 
				isnull(o.exchangeRate, 1) local_to_global_rate, 
				case when (o.exchangeRate = 1 or o.exchangeRate is null) then 'GBP' else 'EUR' end order_currency_code,
				case when (o.base_subtotal = 0) then 0 else 
					convert(decimal(10,1), (o.base_discount_amount * 100 / o.base_subtotal)) end discount_percent 
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id
				inner join
					Landing.aux.mag_gen_store_v s on o.store_id = s.store_id

	exec migra.lnd_stg_get_aux_sales_order_line_measures_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_oh_pt_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- UPDATE AURA PERCENTAGE ON ORDER HEADER
	merge into Landing.aux.sales_order_header_dim_order trg
	using 
		(select order_id_bk, aura_product_f,
			total_subtotal, total_subtotal_esp, 
			case when (aura_product_f = 'N' or total_subtotal = 0) then 0 else convert(decimal(12, 4), (total_subtotal_esp * 100 / total_subtotal)) end aura_product_p
		from
			(select ol.order_id_bk, 
				sum(local_subtotal) over (partition by ol.order_id_bk) total_subtotal, 
				sum(local_subtotal) over (partition by ol.order_id_bk, aura_product_f) total_subtotal_esp, 
				max(aura_product_f) over (partition by ol.order_id_bk) aura_product_f,
				dense_rank() over (partition by ol.order_id_bk order by aura_product_f desc, ol.order_line_id_bk) num
			from 
					Landing.aux.sales_order_line_measures ol
				inner join
					Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk) t
		where num = 1) src
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set 
			trg.aura_product_p = src.aura_product_p;



	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_measures

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_measures(order_id_bk, 
		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, 	
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, 
		local_to_global_rate, order_currency_code, 
		discount_percent, 
		num_lines)

		select o.order_id_bk, 
			ol.price_type_name_bk, ol.discount_f,
			ol.total_qty_unit, ol.total_qty_unit_refunded, ol.total_qty_pack, 
			o.local_subtotal, o.local_shipping, o.local_discount, 0 local_store_credit_used, 
			o.local_total_inc_vat, 	
			0 local_total_refund, 0 local_store_credit_given, 0 local_bank_online_given, 
			0 local_subtotal_refund, 0 local_shipping_refund, 0 local_discount_refund, 0 local_store_credit_used_refund, 0 local_adjustment_refund, 
			o.local_total_inc_vat local_total_aft_refund_inc_vat, o.local_total_inc_vat local_total_aft_refund_inc_vat_2, 
			o.local_to_global_rate, o.order_currency_code, 
			o.discount_percent, 
			ol.num_lines
		from 
				#sales_order_header_measures o
			inner join
 				(select order_id_bk, price_type_name_bk, discount_f,  
					total_qty_unit, total_qty_unit_refunded, total_qty_pack, num_lines
				from
					(select order_id_bk, price_type_name_bk,  
						max(discount_f) over (partition by order_id_bk) discount_f,
						count(*) over (partition by order_id_bk) num_lines, 
						sum(qty_unit) over (partition by order_id_bk) total_qty_unit, 
						sum(qty_unit_refunded) over (partition by order_id_bk) total_qty_unit_refunded, 
						sum(qty_pack) over (partition by order_id_bk) total_qty_pack,
						dense_rank() over (partition by order_id_bk order by ptn.priority, order_line_id_bk) num
					from 
							Landing.aux.sales_order_line_measures ol
						inner join
							Landing.map.sales_price_type_aud ptn on ol.price_type_name_bk = ptn.price_type) t
				where num = 1) ol on o.order_id_bk = ol.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #sales_order_header_measures

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_vat_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: VAT Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_vat_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	create table #sales_order_header_vat(
		order_id_bk						bigint NOT NULL,
		countries_registered_code		varchar(10) NOT NULL,
		invoice_date					datetime,

		local_total_inc_vat				decimal(12, 4))

	insert into #sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat)

		select oh_sc.order_id_bk, 
			'NON-VAT' countries_registered_code, oh.invoice_date,
			oh_m.local_total_inc_vat
		from 
				Landing.aux.sales_order_header_store_cust oh_sc
			inner join
				(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
				from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
			inner join
				Landing.aux.sales_order_header_measures oh_m on oh_sc.order_id_bk = oh_m.order_id_bk

	exec Landing.migra.lnd_stg_get_aux_sales_order_line_vat_migrate  @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_vat

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee,
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee,
		vat_percent, prof_fee_percent)

		select o_v.order_id_bk, 
			o_v.countries_registered_code, o_v.invoice_date, 
			o_v.local_total_inc_vat, ol_v.local_total_exc_vat, ol_v.local_total_vat, ol_v.local_total_prof_fee,
			o_v.local_total_inc_vat local_total_aft_refund_inc_vat, ol_v.local_total_aft_refund_exc_vat, ol_v.local_total_aft_refund_vat, ol_v.local_total_aft_refund_prof_fee, 
			case when (ol_v.local_total_exc_vat = 0) then 0 else ol_v.local_total_vat * 100 / ol_v.local_total_exc_vat end vat_percent, 
			case when (o_v.local_total_inc_vat = 0) then 0 else ol_v.local_total_prof_fee * 100 / o_v.local_total_inc_vat end prof_fee_percent
		from 
				#sales_order_header_vat o_v
			inner join
				(select order_id_bk, 
					sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_prof_fee) local_total_prof_fee, 
					sum(local_total_aft_refund_exc_vat) local_total_aft_refund_exc_vat, sum(local_total_aft_refund_vat) local_total_aft_refund_vat, sum(local_total_aft_refund_prof_fee) local_total_aft_refund_prof_fee
				from Landing.aux.sales_order_line_vat
				group by order_id_bk) ol_v on o_v.order_id_bk = ol_v.order_id_bk	

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #sales_order_header_vat

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_header_oh_pt_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header MIGRATE: OH - PT relation
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_header_oh_pt_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_oh_pt

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_oh_pt(order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time)

		select order_id_bk, product_type_oh_bk, 
			case when (total_local_subtotal = 0) then convert(decimal(12, 4), count(*) * 100 / total_lines) -- Due to OL with sum local_subtotal = 0
				else convert(decimal(12, 4), sum(local_subtotal) * 100 / total_local_subtotal) end order_percentage, 
			1 order_flag, max(qty_time) order_qty_time
		from
			(select olp.order_line_id_bk, olp.order_id_bk, 
				olp.product_type_oh_bk, 
				olm.qty_time, olm.local_subtotal, 
				sum(olm.local_subtotal) over (partition by olp.order_id_bk) total_local_subtotal, 
				count(*) over (partition by olp.order_id_bk) total_lines
			from 
					Landing.aux.sales_order_line_product olp
				inner join
					Landing.aux.sales_order_line_measures olm on olp.order_line_id_bk = olm.order_line_id_bk) ol
		group by order_id_bk, product_type_oh_bk, total_local_subtotal, total_lines
		order by order_id_bk, product_type_oh_bk, total_local_subtotal	

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line MIGRATE: O-I-S-CR relations
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_o_i_s_cr

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_o_i_s_cr(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk)

		select oi.item_id order_line_id_bk, oh.order_id_bk,
			oh.order_id_bk invoice_id, oh.order_id_bk shipment_id, null creditmemo_id, 
			oh.order_no order_no, oh.order_no invoice_no, oh.order_no shipment_no, null creditmemo_no, 
			oi.item_id invoice_line_id, oi.item_id shipment_line_id, null creditmemo_line_id,  
			1 num_shipment_lines, null num_creditmemo_lines,
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			null idCalendarRefundDate, null refund_date, 
			1 warehouse_id_bk
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.migra.sales_flat_order_item_migrate_aud oi on oh.order_id_bk = oi.order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_product_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	20-02-2018	Joseba Bayona Sola	Add some conditions on param values
-- ==========================================================================================
-- Description: Reads data from Order Line MIGRATE: Product Info
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_product_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_product

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_product(order_line_id_bk, order_id_bk, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, product_type_oh_bk)

		select 
			oi.item_id, oi.order_id, 
			oi.product_id, 
			oi.bc, oi.di, oi.po, oi.cy, oi.ax, oi.ad, oi.do, oi.co, 
			case when (oi.eye in ('Right', 'Left')) then left(oi.eye, 1) else null end eye,
			esi.product_code sku_magento, null sku_erp, 
			null glass_vision_type_bk, null order_line_id_vision_type, null glass_package_type_bk, null order_line_id_package_type,
			case when (pr_aura.product_id is not null) then 'Y' else 'N' end aura_product_f, 
			ppt.product_type_oh_bk
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				(select item_id, order_id, 
					product_id, 
					eye, 
					case 
						when (len(base_curve) = 4) then left(base_curve, 3)
						when (base_curve like 'M%') then '8.5'
						when (base_curve like '') then null
						when (len(base_curve) > 4) then null
						else base_curve
					end bc, 
					case 
						when (len(diameter) = 5) then left(diameter, 4)
						when (diameter like '') then null
						else diameter
					end di,  
					case 
							when (power like '') then null
							when (len(power) = 1) then '+0' + power + '.00'
							when (len(power) = 2) then substring(power, 1, 1) + '0' + substring(power, 2, 1) + '.00'
							when (len(power) = 3 and charindex('-', power) = 1) then power + '.00'
							when (len(power) = 3 and charindex('+', power) = 1) then power + '.00'
							when (len(power) = 3 and charindex('-', power) = 0) then '+0' + power + '0'
							when (len(power) = 4 and charindex('.', power) = 2) then '+0' + power 
							when (len(power) = 4 and charindex('.', power) = 3) then substring(power, 1, 1) + '0' + substring(power, 2, 3) + '0'
							when (len(power) = 5 and charindex('.', power) = 3) then substring(power, 1, 1) + '0' + substring(power, 2, 5)
							when (len(power) = 5 and charindex('.', power) = 4) then power + '0'
							when (len(power) > 5) then null
							else power
						end po, 
					case 
						when (len(cylinder) = 5) then cylinder
						else null
					end cy, 
					case 
						when (axis like '') then null
						when (len(axis) = 1) then '00' + axis
						when (len(axis) = 2) then '0' + axis
						when (len(axis) > 3) then null
						else axis
					end ax, 
					case 
						when ([add] like '') then null
						when ([add] like 'L%') then 'LOW'
						when ([add] like 'M%') then 'MED'
						when ([add] like 'H%') then 'HIGH'

						when (len([add]) = 4) then left([add], 3)
						when (len([add]) = 5) then substring([add], 2, 3)
						when (len([add]) = 6) then substring([add], 2, 3)
						else [add]
					end ad, 
					case 
						when (dominant like '') then null
						when (len([add]) = 6) then substring([add], 6, 1)
						else dominant
					end do, 
					color co
				from Landing.migra.sales_flat_order_item_migrate_aud) oi on ol.order_line_id_bk = oi.item_id
			left join
				(select product_id, bc, di, po, cy, ax, ad, do, co, min(product_code) product_code
				from Landing.aux.mag_edi_stock_item
				group by product_id, bc, di, po, cy, ax, ad, do, co) esi on oi.product_id = esi.product_id
					and isnull(oi.bc, '') = isnull(esi.bc, '') and isnull(oi.di, '') = isnull(esi.di, '') and isnull(oi.po, '') = isnull(esi.po, '') and isnull(oi.cy, '') = isnull(esi.cy, '')
					and isnull(oi.ax, '') = isnull(esi.ax, '') and isnull(oi.ad, '') = isnull(esi.ad, '') and isnull(oi.do, '') = isnull(esi.do, '')
					and isnull(oi.co, '') = isnull(esi.co, '')
			left join
				Landing.map.prod_product_aura_aud pr_aura on oi.product_id = pr_aura.product_id
			inner join
				Landing.aux.prod_product_product_type_oh ppt on oi.product_id = ppt.product_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_measures_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	30-08-2017	Joseba Bayona Sola	Add Yearly category + CL prefix
	--	20-02-2018	Joseba Bayona Sola	Changes for Discount values at OL level (before only at OH)
-- ==========================================================================================
-- Description: Reads data from Order Line MIGRATE: Measures Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_measures_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_header_line_measures(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL,
		order_status_name_bk			varchar(20) NOT NULL, 
		price_type_name_bk				varchar(40),
		discount_f						char(1),
		qty_unit						decimal(12, 4),
		qty_pack						int,
		qty_time						int,

		local_price_unit				decimal(12, 4),
		local_price_pack				decimal(12, 4),
		local_price_pack_discount		decimal(12, 4),

		local_subtotal					decimal(12, 4),
		local_discount					decimal(12, 4),
		
		discount_percent				decimal(12, 4),
		order_allocation_rate			decimal(12, 4))

	-- INSERT STATEMENT: 
	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, 
		discount_percent, 
		order_allocation_rate) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			'OK' order_status_name_bk, 'Regular' price_type_name_bk, 'N' discount_f,
			oi.qty_ordered qty_unit, 
			case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end qty_pack, 
			oi.qty_ordered / pc.num_months qty_time,
			case when (oi.qty_ordered = 0) then oi.base_row_total / 1 else oi.base_row_total / oi.qty_ordered end local_price_unit, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end local_price_pack, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, pc.size))) end local_price_pack_discount, 
			oi.base_row_total local_subtotal, 
			-- 0 local_discount, 0 discount_percent, -- null local_discount, null discount_percent, 
			oi.base_discount_amount local_discount, 0 discount_percent, -- null local_discount, null discount_percent, 
			null order_allocation_rate
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.migra.sales_flat_order_item_migrate_aud oi on ol.order_line_id_bk = oi.item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 6
						when (category_bk = 'CL - Monthlies') then 3
						when (category_bk = 'CL - Yearly') then 1
						when (category_bk = 'Sunglasses') then 1
						when (category_bk = 'Glasses') then 1
						else 1
					end size 
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 

	merge into #sales_order_header_line_measures trg
	using 
		(select ol.order_line_id_bk, 
			case when (o.local_subtotal = 0) then 0 else convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) end allocation_rate
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.order_allocation_rate = src.allocation_rate;


	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_measures

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund,
		local_to_global_rate, order_currency_code, 
		discount_percent,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund)

		select 
			ol.order_line_id_bk, ol.order_id_bk, 
			ol.order_status_name_bk, ol.price_type_name_bk, 
			ol.discount_f,
			ol.qty_unit, 0 qty_unit_refunded, ol.qty_pack, ol.qty_time, 
			ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
			ol.local_subtotal, 
			
			o.local_shipping * ol.order_allocation_rate local_shipping, 
			case when (ol.local_discount is null) then o.local_discount * ol.order_allocation_rate else ol.local_discount end local_discount, -- o.local_discount * ol.order_allocation_rate local_discount, 
			
			0 local_store_credit_used, 
			0 local_store_credit_given, 0 local_bank_online_given, 
			0 local_subtotal_refund, 0 local_shipping_refund,
			0 local_discount_refund, 0 local_store_credit_used_refund, 
			0 local_adjustment_refund,
			o.local_to_global_rate, o.order_currency_code, 
			ol.discount_percent,
			ol.order_allocation_rate, 0 order_allocation_rate_aft_refund, 0 order_allocation_rate_refund
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk
		order by ol.order_id_bk, ol.order_line_id_bk

	update Landing.aux.sales_order_line_measures
	set 
		discount_f = 'Y', 
		discount_percent = convert(decimal(12, 2), local_discount * 100 / local_subtotal)
	where local_discount <> 0
		and local_subtotal <> 0


	merge into Landing.aux.sales_order_line_measures trg
	using
		(select order_line_id_bk, local_subtotal, local_shipping, local_discount, local_store_credit_used,	
			local_subtotal + local_shipping - local_discount - local_store_credit_used local_total_inc_vat
		from Landing.aux.sales_order_line_measures) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set
			trg.local_total_inc_vat = src.local_total_inc_vat;

	update Landing.aux.sales_order_line_measures
	set local_total_aft_refund_inc_vat = local_total_inc_vat

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #sales_order_header_line_measures

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure migra.lnd_stg_get_aux_sales_order_line_vat_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line MIGRATE: VAT Attributes
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_line_vat_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_line_vat(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL, 
		countries_registered_code		varchar(10) NOT NULL,
		product_type_vat				varchar(255),
		vat_type_rate_code				char(1), 
		invoice_date					datetime,
	
		vat_rate						decimal(12, 4),
		prof_fee_rate					decimal(12, 4),

		local_total_inc_vat				decimal(12, 4),
		local_total_exc_vat				decimal(12, 4),
		local_total_vat					decimal(12, 4),
		local_total_prof_fee			decimal(12, 4))

	insert into #sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat)

		select ol.order_line_id_bk, ol.order_id_bk, 
			oh_v.countries_registered_code, oh_v.invoice_date, 
			o_m.local_total_inc_vat
		from 
				#sales_order_header_vat oh_v
			inner join
				Landing.aux.sales_order_line_o_i_s_cr ol on oh_v.order_id_bk = ol.order_id_bk
			inner join
				Landing.aux.sales_order_line_measures o_m on ol.order_line_id_bk = o_m.order_line_id_bk

	-- Set product_type_vat
	merge into #sales_order_line_vat trg
	using 
		(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
			ol_v.product_type_vat
		from 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					case when (ppt.product_type_vat = 'Colour Plano' and ol_p.power_bk <> '+00.00') then 'Contact Lenses' else ppt.product_type_vat end product_type_vat
				from 
						#sales_order_line_vat ol_v
					inner join
						Landing.aux.sales_order_line_product ol_p on ol_v.order_line_id_bk = ol_p.order_line_id_bk
					inner join
						Landing.aux.prod_product_product_type_vat ppt on ol_p.product_id_bk = ppt.product_id) ol_v	
			inner join
				Landing.map.vat_product_type_vat_aud ptv on ol_v.product_type_vat = ptv.product_type_vat) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.product_type_vat = src.product_type_vat;

	-- Set Vat Values (vat_type_rate, vat_rate) - Prof Fee Values (prof_fee_rate) based on countries_registered_code - product_type_vat
	update #sales_order_line_vat 
	set 
		vat_type_rate_code = 'Z', 
		vat_rate = 0, prof_fee_rate = 0;

	-- Calculate Prof Fee Values (local_total_prof_fee - local_total_aft_refund_prof_fee)
	update #sales_order_line_vat
	set 
		local_total_prof_fee = local_total_inc_vat * (prof_fee_rate / 100)

	-- Calculate VAT Values (local_total_vat, local_total_exc_vat  - local_total_aft_refund_vat, local_total_aft_refund_exc_vat)
	update #sales_order_line_vat
	set 
		local_total_vat = (local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate), 
		local_total_exc_vat = local_total_inc_vat - ((local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate))

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_vat

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
		vat_rate, prof_fee_rate,
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		vat_percent, prof_fee_percent)

		select order_line_id_bk, order_id_bk, 
			countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
			vat_rate, prof_fee_rate,
			local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
			local_total_inc_vat local_total_aft_refund_inc_vat, local_total_exc_vat local_total_aft_refund_exc_vat, local_total_vat local_total_aft_refund_vat, local_total_prof_fee local_total_aft_refund_prof_fee, 
			case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_exc_vat end vat_percent, 
			case when (local_total_inc_vat = 0) then 0 else local_total_prof_fee * 100 / local_total_inc_vat end prof_fee_percent
		from #sales_order_line_vat

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #sales_order_line_vat

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure migra.lnd_stg_get_aux_sales_order_dim_fact_migrate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-08-2017
-- Changed: 
	--	27-09-2017	Joseba Bayona Sola	Add call to SP for Activity Other (Lapsed - Reactivate)
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Executes Aux SPs and prepares data for Order Dim - Fact Tables - MIGRATE
-- ==========================================================================================

create procedure migra.lnd_stg_get_aux_sales_order_dim_fact_migrate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	exec migra.lnd_stg_get_aux_sales_order_header_o_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_o_i_s_cr_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_store_cust_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_order_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_header_dim_mark_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_line_o_i_s_cr_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec migra.lnd_stg_get_aux_sales_order_line_product_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec migra.lnd_stg_get_aux_sales_order_header_measures_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- exec migra.lnd_stg_get_aux_sales_order_line_measures_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun (Call inside lnd_stg_get_aux_sales_order_header_measures_migrate)


	exec migra.lnd_stg_get_aux_sales_order_header_vat_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	

	-- exec migra.lnd_stg_get_aux_sales_order_header_oh_pt_migrate @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun (Call inside lnd_stg_get_aux_sales_order_header_measures_migrate)

		merge into Landing.aux.sales_order_header_dim_order trg
		using
			(select order_id_bk, product_type_oh_bk, order_qty_time, num_rep num_diff_product_type_oh
			from
				(select order_id_bk, product_type_oh_bk, 
					order_percentage, order_flag, order_qty_time, 
					count(*) over (partition by order_id_bk) num_rep, 
					dense_rank() over (partition by order_id_bk order by order_percentage desc, product_type_oh_bk) ord_rep
				from Landing.aux.sales_order_header_oh_pt) t
			where ord_rep = 1) src 
		on trg.order_id_bk = src.order_id_bk
		when matched then
			update set
				trg.product_type_oh_bk = src.product_type_oh_bk, 
				trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh;	




	 truncate table Landing.aux.sales_dim_order_header

	insert into Landing.aux.sales_dim_order_header (order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date,

		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing,
		customer_unsubscribe_name_bk,

		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os,

		countries_registered_code, 

		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent,
		num_lines, 
		idETLBatchRun)

		select oh.order_id_bk, 
			oh.invoice_id, oh.shipment_id, oh.creditmemo_id, 
			oh.order_no, oh.invoice_no, oh.shipment_no, oh.creditmemo_no, 
			oh.idCalendarOrderDate, oh.idTimeOrderDate, oh.order_date, 
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			oh.idCalendarRefundDate, oh.refund_date,

			oh_sc.store_id_bk, oh_sc.market_id_bk, oh_sc.customer_id_bk, oh_sc.shipping_address_id, oh_sc.billing_address_id,
			oh_sc.country_id_shipping_bk, oh_sc.region_id_shipping_bk, oh_sc.postcode_shipping, 
			oh_sc.country_id_billing_bk, oh_sc.region_id_billing_bk, oh_sc.postcode_billing,
			oh_sc.customer_unsubscribe_name_bk,

			oh_o.order_stage_name_bk, oh_o.order_status_name_bk, oh_o.adjustment_order, oh_o.order_type_name_bk, oh_o.status_bk, 
			oh_o.payment_method_name_bk, oh_o.cc_type_name_bk, oh_o.payment_id, oh_o.payment_method_code,
			oh_o.shipping_description_bk, oh_o.shipping_description_code, oh_o.telesales_admin_username_bk, oh_o.prescription_method_name_bk,
			oh_o.reminder_type_name_bk, oh_o.reminder_period_bk, oh_o.reminder_date, oh_o.reminder_sent, 
			oh_o.reorder_f, oh_o.reorder_date, oh_o.reorder_profile_id, oh_o.automatic_reorder,
			oh_o.order_source, oh_o.proforma, 
			oh_o.product_type_oh_bk, oh_o.order_qty_time, oh_o.num_diff_product_type_oh, oh_o.aura_product_p, 

			oh_o.referafriend_code, oh_o.referafriend_referer, 
			oh_o.postoptics_auto_verification, oh_o.presc_verification_method, oh_o.warehouse_approved_time,

			oh_m.channel_name_bk, oh_m.marketing_channel_name_bk, oh_m.publisher_id_bk, 
			oh_m.affilCode, oh_m.ga_medium, oh_m.ga_source, oh_m.ga_channel, oh_m.telesales_f, oh_m.sms_auto_f, oh_m.mutuelles_f, 
			oh_m.coupon_id_bk, oh_m.coupon_code, oh_m.applied_rule_ids,
			oh_m.device_model_name_bk, oh_m.device_browser_name_bk, oh_m.device_os_name_bk, 
			oh_m.ga_device_model, oh_m.ga_device_browser, oh_m.ga_device_os,

			oh_vat.countries_registered_code, 

			oh_me.price_type_name_bk, oh_me.discount_f,
			oh_me.total_qty_unit, oh_me.total_qty_unit_refunded, oh_me.total_qty_pack, oh_me.weight, 
			oh_me.local_subtotal, oh_me.local_shipping, oh_me.local_discount, oh_me.local_store_credit_used, 
			oh_me.local_total_inc_vat, oh_vat.local_total_exc_vat, oh_vat.local_total_vat, oh_vat.local_total_prof_fee, 
			oh_me.local_total_refund, oh_me.local_store_credit_given, oh_me.local_bank_online_given, 
			oh_me.local_subtotal_refund, oh_me.local_shipping_refund, oh_me.local_discount_refund, oh_me.local_store_credit_used_refund, oh_me.local_adjustment_refund, 
			oh_me.local_total_aft_refund_inc_vat, oh_me.local_total_aft_refund_inc_vat_2, oh_vat.local_total_aft_refund_exc_vat, oh_vat.local_total_aft_refund_vat, oh_vat.local_total_aft_refund_prof_fee, 
			oh_me.local_product_cost, oh_me.local_shipping_cost, oh_me.local_total_cost, oh_me.local_margin, 
			oh_me.local_to_global_rate, oh_me.order_currency_code, 
			oh_me.discount_percent, oh_vat.vat_percent, oh_vat.prof_fee_percent,
			oh_me.num_lines, 

			@idETLBatchRun idETLBatchRun
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.aux.sales_order_header_store_cust oh_sc	on oh.order_id_bk = oh_sc.order_id_bk
		inner join
			Landing.aux.sales_order_header_dim_order oh_o on oh.order_id_bk = oh_o.order_id_bk		
		inner join
			Landing.aux.sales_order_header_dim_mark oh_m on oh.order_id_bk = oh_m.order_id_bk
		inner join
			Landing.aux.sales_order_header_measures oh_me on oh.order_id_bk = oh_me.order_id_bk
		inner join
			Landing.aux.sales_order_header_vat oh_vat on oh.order_id_bk = oh_vat.order_id_bk 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	 truncate table Landing.aux.sales_fact_order_line

	insert into Landing.aux.sales_fact_order_line(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, 

		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, 

		countries_registered_code, product_type_vat,

		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
		num_erp_allocation_lines, 

		idETLBatchRun)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.invoice_id, ol.shipment_id, ol.creditmemo_id, 
			ol.order_no, ol.invoice_no, ol.shipment_no, ol.creditmemo_no,
			ol.invoice_line_id, ol.shipment_line_id, ol.creditmemo_line_id,  	
			ol.num_shipment_lines, ol.num_creditmemo_lines,
			ol.idCalendarInvoiceDate, ol.invoice_date, 
			ol.idCalendarShipmentDate, ol.idTimeShipmentDate, ol.shipment_date, 
			ol.idCalendarRefundDate, ol.refund_date, 
			ol.warehouse_id_bk, 

			ol_p.product_id_bk, 
			ol_p.base_curve_bk, ol_p.diameter_bk, ol_p.power_bk, ol_p.cylinder_bk, ol_p.axis_bk, ol_p.addition_bk, ol_p.dominance_bk, ol_p.colour_bk, ol_p.eye,
			ol_p.sku_magento, ol_p.sku_erp, 
			ol_p.glass_vision_type_bk, ol_p.order_line_id_vision_type, ol_p.glass_package_type_bk, ol_p.order_line_id_package_type,
			ol_p.aura_product_f, 

			ol_vat.countries_registered_code, ol_vat.product_type_vat,

			ol_me.order_status_name_bk, ol_me.price_type_name_bk, ol_me.discount_f,
			ol_me.qty_unit, ol_me.qty_unit_refunded, ol_me.qty_pack, ol_me.qty_time, ol_me.weight, 
			ol_me.local_price_unit, ol_me.local_price_pack, ol_me.local_price_pack_discount, 
			ol_me.local_subtotal, ol_me.local_shipping, ol_me.local_discount, ol_me.local_store_credit_used, 
			ol_me.local_total_inc_vat, ol_vat.local_total_exc_vat, ol_vat.local_total_vat, ol_vat.local_total_prof_fee, 
			ol_me.local_store_credit_given, ol_me.local_bank_online_given, 
			ol_me.local_subtotal_refund, ol_me.local_shipping_refund, ol_me.local_discount_refund, ol_me.local_store_credit_used_refund, ol_me.local_adjustment_refund, 
			ol_me.local_total_aft_refund_inc_vat, ol_vat.local_total_aft_refund_exc_vat, ol_vat.local_total_aft_refund_vat, ol_vat.local_total_aft_refund_prof_fee, 
			ol_me.local_product_cost, ol_me.local_shipping_cost, ol_me.local_total_cost, ol_me.local_margin, 
			ol_me.local_to_global_rate, ol_me.order_currency_code, 
			ol_me.discount_percent, ol_vat.vat_percent, ol_vat.prof_fee_percent, ol_vat.vat_rate, ol_vat.prof_fee_rate,
			ol_me.order_allocation_rate, ol_me.order_allocation_rate_aft_refund, ol_me.order_allocation_rate_refund, 
			ol_me.num_erp_allocation_lines, 

			@idETLBatchRun idETLBatchRun

		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product ol_p on ol.order_line_id_bk = ol_p.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_measures ol_me on ol.order_line_id_bk = ol_me.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_vat ol_vat on ol.order_line_id_bk = ol_vat.order_line_id_bk	

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message
	
	
	exec mag.lnd_stg_get_aux_act_activity_sales @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
			
	exec mag.lnd_stg_get_aux_act_activity_other @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

