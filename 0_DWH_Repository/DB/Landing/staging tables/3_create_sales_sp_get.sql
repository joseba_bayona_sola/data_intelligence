use Landing
go 

drop procedure mag.lnd_stg_get_sales_reminder_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Reminder Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_reminder_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_type 
	select reminder_type reminder_type_name_bk, UPPER(reminder_type) reminder_type_name, @idETLBatchRun
	from
		(select reminder_type, count(*) num
		from Landing.mag.sales_flat_order_aud
		where status <> 'archived'
		group by reminder_type) t
	where reminder_type in ('both', 'email', 'sms', 'none', 'reorder')
	order by reminder_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_reminder_period
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Reminder Period in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_reminder_period
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_period but only if they are multiples of 15 - 30
	select reminder_period_int reminder_period_bk,
		case 
			when len(reminder_period) = 1 then '000' + reminder_period
			when len(reminder_period) = 2 then '00' + reminder_period
			when len(reminder_period) = 3 then '0' + reminder_period
			else reminder_period
		end reminder_period_name, @idETLBatchRun
	from
		(select reminder_period, cast(reminder_period as int) reminder_period_int, 
			count(*) num
		from Landing.mag.sales_flat_order_aud
		where status <> 'archived'
			and reminder_period is not null
			and ((reminder_period % 30 = 0) or (reminder_period % 15 = 0))
			and reminder_period <> ''
		group by reminder_period) t
	order by reminder_period_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_order_stage
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Stage in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_stage
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From order_stage Mapping Table 
	select order_stage order_stage_name_bk, order_stage order_stage_name, description, @idETLBatchRun
	from 
		(select order_stage, max(description) description
		from Landing.map.sales_order_stage
		group by order_stage) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_order_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From order_status Mapping Table 
	select order_status order_status_name_bk, order_status order_status_name, description, @idETLBatchRun
	from
		(select order_status, max(description) description
		from Landing.map.sales_order_status
		group by order_status) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_order_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From order_type Mapping Table 
	select order_type order_type_name_bk, order_type order_type_name, description, @idETLBatchRun
	from
		(select order_type, max(description) description
		from Landing.map.sales_order_type
		group by order_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_order_status_magento
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Status Magento in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_status_magento
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From order_status_magento Mapping Table + status values from Order Table (sales_flat_order)
	select status_bk, order_status_magento_name, description, @idETLBatchRun
	from
		(select order_status_magento status_bk, order_status_magento order_status_magento_name, description
		from
			(select order_status_magento, max(description) description
			from Landing.map.sales_order_status_magento
			group by order_status_magento) t
		union
		select o.status status_bk, o.status order_status_magento_name, 'No Desc.' description
		from
				(select distinct status
				from Landing.mag.sales_flat_order
				where status not in ('archived', 'cancel_ogone', 'processing_ogone')) o
			left join
				Landing.map.sales_order_status_magento osm on o.status = osm.order_status_magento_code
		where osm.order_status_magento is null) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_payment_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Payment Method in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_payment_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From payment_method Mapping Table + payment method values from Order Payment Table (sales_flat_order_payment)
	select payment_method_name_bk, payment_method_name, description, @idETLBatchRun
	from
		(select payment_method payment_method_name_bk, payment_method payment_method_name, description
		from
			(select payment_method, max(description) description
			from Landing.map.sales_payment_method
			group by payment_method) t
		union
		select op.method payment_method_name_bk, op.method payment_method_name, 'No Desc.' description
		from
				(select distinct method
				from Landing.mag.sales_flat_order_payment_aud
				where method not in ('storecredit')) op
			left join
				Landing.map.sales_payment_method pm on op.method = pm.payment_method_code
		where pm.payment_method is null) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_cc_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from CC Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_cc_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From cc_type Mapping Table 
	select cc_type cc_type_name_bk, cc_type cc_type_name, @idETLBatchRun
	from
		(select distinct cc_type
		from Landing.map.sales_cc_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_shipping_carrier
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Shipping Carrier in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_shipping_carrier
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From shipping_method Mapping Table + shipping_description data in Order Table (sales_flat_order)
	select shipping_carrier shipping_carrier_name_bk, shipping_carrier shipping_carrier_name, @idETLBatchRun
	from
		(select distinct substring(shipping_method, 1, charindex('-', shipping_method) -1) shipping_carrier
		from Landing.map.sales_shipping_method) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_shipping_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Shipping Method in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_shipping_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From shipping_method Mapping Table + shipping_description data in Order Table (sales_flat_order)
	select shipping_method shipping_description_bk, shipping_method shipping_method_name, shipping_carrier shipping_carrier_name_bk, shipping_method description, @idETLBatchRun
	from
		(select distinct shipping_method, substring(shipping_method, 1, charindex('-', shipping_method) -1) shipping_carrier
		from Landing.map.sales_shipping_method) t


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_telesale
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Telesale in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_telesale
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX --> What about user not in admin_user table
	select o.telesales_admin_username telesales_admin_username_bk, o.telesales_admin_username telesales_username, 
		u.user_id, u.firstname telesales_first_name, u.lastname telesales_last_name, @idETLBatchRun
	from
			(select telesales_admin_username, count(*) num
			from Landing.mag.sales_flat_order_aud
			where status <> 'archived'
			group by telesales_admin_username) o
		inner join
			Landing.mag.admin_user u on o.telesales_admin_username = u.username


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_price_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Price Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_price_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select price_type price_type_name_bk, price_type price_type_name, description, @idETLBatchRun
	from
		(select price_type, max(description) description
		from Landing.map.sales_price_type
		group by price_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_prescription_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Presc. Method in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_prescription_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select prescription_method prescription_method_name_bk, prescription_method prescription_method_name, description, @idETLBatchRun
	from
		(select prescription_method, max(description) description
		from Landing.map.sales_prescription_method
		group by prescription_method) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





drop procedure mag.lnd_stg_get_sales_channel
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	No sup_channel_name anymore
	--	29-11-2017	Joseba Bayona Sola	Add channel_name so it is different from channel (bk attr)
-- ==========================================================================================
-- Description: Reads data from Channel in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_channel
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select channel channel_name_bk, channel_name, description, @idETLBatchRun
	from
		(select channel, min(channel_name) channel_name, min(description) description
		from Landing.map.sales_channel_aud
		group by channel) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_marketing_channel
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-11-2017
-- Changed: 
	--	29-11-2017	Joseba Bayona Sola	Add marketing_channel_name so it is different from marketing_channel (bk attr)
-- ==========================================================================================
-- Description: Reads data from Channel in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_marketing_channel
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From marketing_channel Mapping Table
	select marketing_channel marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel sup_marketing_channel_name, description, @idETLBatchRun
	from
		(select marketing_channel, marketing_channel_name, sup_marketing_channel, min(description) description
		from Landing.map.sales_marketing_channel_aud
		group by marketing_channel, marketing_channel_name, sup_marketing_channel) t


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_affiliate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Affiliate in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_affiliate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select publisher_id publisher_id_bk, 
		isnull(isnull(affiliate_name, affiliate_website), 'N/A') affiliate_name, affiliate_website, affiliate_network affiliate_network_name, 
		affiliate_type affiliate_type_name, affiliate_status affiliate_status_name, @idETLBatchRun
	from
		(select publisher_id, 
			min(affiliate_name) affiliate_name, min(affiliate_website) affiliate_website, min(affiliate_network) affiliate_network, 
			min(affiliate_type) affiliate_type, min(affiliate_status) affiliate_status
		from Landing.map.sales_affiliate
		group by publisher_id) t
	order by publisher_id


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_group_coupon_code
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Group Coupon Code in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_group_coupon_code
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select  isnull(channel, 'NULL') channel_bk,  isnull(channel, 'NULL') group_coupon_code_name, @idETLBatchRun
	from 
		(select distinct channel
		from Landing.mag.salesrule_aud) t
	order by isnull(channel, 'NULL')

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_coupon_code
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Coupon Code in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_coupon_code
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, isnull(sr.channel, 'NULL') channel_bk, sr.rule_id, 
		isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, sr.name coupon_code_name, 
		sr.from_date, sr.to_date, src.expiration_date, 
		sr.simple_action discount_type_name, sr.discount_amount, 
		case when (sr.postoptics_only_new_customer = 1) then 'Y' else 'N' end only_new_customer,
		case when (ref.value is not null) then 'Y' else 'N' end refer_a_friend, @idETLBatchRun
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
		left join
			(select distinct value
			from Landing.mag.core_config_data
			where path = 'referafriend/invite/voucher') ref on src.coupon_id = ref.value
	-- where sr.channel is not null and src.code is not null
	order by sr.channel, src.code

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_device_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Device Category in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_device_category
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select 'device_category_t' device_category_name_bk, 'device_category_t' device_category_name, @idETLBatchRun

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_device_brand
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Device Brand in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_device_brand
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select 'device_brand_t' device_brand_name_bk, 'device_category_t' device_category_name_bk, 'device_brand_t' device_brand_name, @idETLBatchRun

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_device_model
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Device Model in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_device_model
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select 'device_model_t' device_model_name_bk, 'device_brand_t' device_brand_name_bk, 'device_model_t' device_model_name, @idETLBatchRun

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_device_browser
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Device Browser in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_device_browser
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select 'device_browser_t' device_browser_name_bk, 'device_browser_t' device_browser_name, @idETLBatchRun

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_device_os
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Device OS in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_device_os
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From price_type Mapping Table
	select 'device_os_t' device_os_name_bk, 'device_os_t' device_os_name, @idETLBatchRun

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_rank_customer_order_seq_no
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-09-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Rank Customer Order Seq No in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_rank_customer_order_seq_no
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


		create table #customer_order_seq_no(
			customer_order_seq_no_bk	int NOT NULL, 
			rank_seq_no					char(2) NOT NULL,
			)

		declare @min_rank int, @max_rank int
		declare @rank_seq_no char(2)

		declare db_cursor cursor for
		select min_rank, max_rank, rank_seq_no
		from Landing.map.sales_rank_customer_order_seq_no

		open db_cursor 
		fetch next from db_cursor into @min_rank, @max_rank, @rank_seq_no

		while @@fetch_status = 0
		begin

			WITH gen AS (
				SELECT @min_rank AS num
				UNION ALL
				SELECT num+1 FROM gen WHERE num+1<=@max_rank
			)

			insert into #customer_order_seq_no(customer_order_seq_no_bk, rank_seq_no)
				SELECT num, @rank_seq_no 
				FROM gen
				option (maxrecursion 10000)

			fetch next from db_cursor into @min_rank, @max_rank, @rank_seq_no
		end

		close db_cursor
		deallocate db_cursor

	-- SELECT STATEMENT: From price_type Mapping Table
	select customer_order_seq_no_bk customer_order_seq_no_bk, rank_seq_no rank_seq_no, @idETLBatchRun
	from #customer_order_seq_no

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #customer_order_seq_no

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_rank_qty_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Rank Qty Time in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_rank_qty_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


		create table #qty_time(
			qty_time_bk					int NOT NULL, 
			rank_qty_time				char(2) NOT NULL,
			)

		declare @min_rank int, @max_rank int
		declare @rank_qty_time char(2)

		declare db_cursor cursor for
		select min_rank, max_rank, rank_qty_time
		from Landing.map.sales_rank_qty_time

		open db_cursor 
		fetch next from db_cursor into @min_rank, @max_rank, @rank_qty_time

		while @@fetch_status = 0
		begin

			WITH gen AS (
				SELECT @min_rank AS num
				UNION ALL
				SELECT num+1 FROM gen WHERE num+1<=@max_rank
			)

			insert into #qty_time(qty_time_bk, rank_qty_time)
				SELECT num, @rank_qty_time 
				FROM gen
				option (maxrecursion 10000)

			fetch next from db_cursor into @min_rank, @max_rank, @rank_qty_time
		end

		close db_cursor
		deallocate db_cursor

	-- SELECT STATEMENT: From price_type Mapping Table
	select qty_time_bk qty_time_bk, rank_qty_time rank_qty_time, @idETLBatchRun
	from #qty_time

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #qty_time

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_sales_rank_freq_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Rank Freq Time in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_rank_freq_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


		create table #freq_time(
			freq_time_bk				int NOT NULL, 
			rank_freq_time				char(2) NOT NULL,
			)

		declare @min_rank int, @max_rank int
		declare @rank_freq_time char(2)

		declare db_cursor cursor for
		select min_rank, 
			case when (max_rank > 30000) then 30000 else max_rank end max_rank, 
			rank_freq_time
		from Landing.map.sales_rank_freq_time

		open db_cursor 
		fetch next from db_cursor into @min_rank, @max_rank, @rank_freq_time

		while @@fetch_status = 0
		begin

			WITH gen AS (
				SELECT @min_rank AS num
				UNION ALL
				SELECT num+1 FROM gen WHERE num+1<=@max_rank
			)

			insert into #freq_time(freq_time_bk, rank_freq_time)
				SELECT num, @rank_freq_time 
				FROM gen
				option (maxrecursion 30000)

			fetch next from db_cursor into @min_rank, @max_rank, @rank_freq_time
		end

		close db_cursor
		deallocate db_cursor

	-- SELECT STATEMENT: From price_type Mapping Table
	select freq_time_bk freq_time_bk, rank_freq_time rank_freq_time, @idETLBatchRun
	from #freq_time

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #freq_time

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_rank_shipping_days
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Rank Shipping Days in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_rank_shipping_days
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


		create table #shipping_days(
			shipping_days_bk			int NOT NULL, 
			rank_shipping_days			char(2) NOT NULL,
			)

		declare @min_rank int, @max_rank int
		declare @rank_shipping_days char(2)

		declare db_cursor cursor for
		select min_rank, max_rank, 
			rank_shipping_days
		from Landing.map.sales_rank_shipping_days

		open db_cursor 
		fetch next from db_cursor into @min_rank, @max_rank, @rank_shipping_days

		while @@fetch_status = 0
		begin

			WITH gen AS (
				SELECT @min_rank AS num
				UNION ALL
				SELECT num+1 FROM gen WHERE num+1<=@max_rank
			)

			insert into #shipping_days(shipping_days_bk, rank_shipping_days)
				SELECT num, @rank_shipping_days 
				FROM gen
				option (maxrecursion 10000)

			fetch next from db_cursor into @min_rank, @max_rank, @rank_shipping_days
		end

		close db_cursor
		deallocate db_cursor

	-- SELECT STATEMENT: From price_type Mapping Table
	select shipping_days_bk shipping_days_bk, rank_shipping_days rank_shipping_days, @idETLBatchRun
	from #shipping_days

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #shipping_days

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


---------------------------------------------------------------------------------------

drop procedure mag.lnd_stg_get_sales_order_header
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	24-07-2017	Joseba Bayona Sola	Add VAT Logic attributes: countries_registered_code
	--	07-09-2017	Joseba Bayona Sola	Add customer_oder_seq_no_web attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk attribute
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_header
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	
	exec mag.lnd_stg_get_aux_sales_order_dim_fact @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		store_id_bk, market_id_bk, customer_id_bk, 
		country_id_shipping_bk, postcode_shipping, country_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk,
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, shipping_description_bk, telesales_admin_username_bk, price_type_name_bk, discount_f, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reorder_f, reorder_date, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, coupon_id_bk, device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web, order_source, proforma, 
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p,
		countries_registered_code,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 0 weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, 
		num_lines,
		@idETLBatchRun
	from Landing.aux.sales_dim_order_header

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_order_header_product_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Header Product Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_header_product_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time,
		@idETLBatchRun
	from Landing.aux.sales_order_header_oh_pt

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_sales_order_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	24-07-2017	Joseba Bayona Sola	Add VAT Logic attributes: countries_registered_code, product_type_vat
-- ==========================================================================================
-- Description: Reads data from Order Line in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_line_id_bk, 
		order_id_bk order_id, invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, order_id_bk, order_status_name_bk, price_type_name_bk, discount_f, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, glass_package_type_bk, 
		aura_product_f,
		countries_registered_code, product_type_vat, 

		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund,
		num_erp_allocation_lines,
		@idETLBatchRun
	from Landing.aux.sales_fact_order_line

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_sales_order_line_vat_fix
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Line VAT FIX in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_sales_order_line_vat_fix
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From XXXX
	select order_line_id_bk, order_id_bk order_id, order_no, order_date, 
		countries_registered_code, order_currency_code, exchange_rate,
		local_total_inc_vat * exchange_rate local_total_inc_vat_vf, local_total_exc_vat * exchange_rate local_total_exc_vat_vf, 
		local_total_vat * exchange_rate local_total_vat_vf, local_total_prof_fee * exchange_rate local_total_prof_fee_vf, 
		local_store_credit_given * exchange_rate local_store_credit_given_vf, local_bank_online_given * exchange_rate local_bank_online_given_vf, 
		local_subtotal_refund * exchange_rate local_subtotal_refund_vf, local_shipping_refund * exchange_rate local_shipping_refund_vf, local_discount_refund * exchange_rate local_discount_refund_vf, 
		local_store_credit_used_refund * exchange_rate local_store_credit_used_refund_vf, local_adjustment_refund * exchange_rate local_adjustment_refund_vf,
		@idETLBatchRun
	from
		(select ol.order_line_id_bk, oh.order_id_bk, oh.order_no, oh.order_date, 
			oh.countries_registered_code, oh.order_currency_code, 
			case when (oh.order_currency_code = 'EUR') then oh.local_to_global_rate else ex.gbp_to_eur end exchange_rate,
			ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
			ol.local_store_credit_given, ol.local_bank_online_given, 
			ol.local_subtotal_refund, ol.local_shipping_refund, ol.local_discount_refund, ol.local_store_credit_used_refund, ol.local_adjustment_refund
		from 
				Landing.aux.sales_dim_order_header oh
			inner join
				Landing.aux.sales_fact_order_line ol on oh.order_id_bk = ol.order_id_bk
			inner join
				Landing.aux.sales_exchange_rate ex on convert(date, oh.order_date) = ex.exchange_rate_day
		where oh.order_source = 'M'
			and year(oh.order_date) > 2016 
			and 
				((oh.countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and oh.order_currency_code = 'EUR') or
				(oh.countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and oh.order_currency_code <> 'EUR'))) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


----------------------------------------



drop procedure mag.lnd_stg_get_aux_sales_order_header_o
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	31-08-2017	Joseba Bayona Sola	Add to the filtering clause that orders need to have a customer ID
	--	25-09-2017	Joseba Bayona Sola	Check that store_id is different from NULL
	--	30-11-2017	Joseba Bayona Sola	Add into Orders to be processed those ones taken from GA (For Channel - Marketing Channel)
	--	08-12-2017	Joseba Bayona Sola	Exclude from process orders where subtotal = 0
	--	08-12-2017	Joseba Bayona Sola	Exclude from process orders without Glasses OL
	--	09-02-2018	Joseba Bayona Sola	Exclude archived orders that could have new invoices
-- ==========================================================================================
-- Description: Reads data from Order Header: O entities
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_o
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o(order_id_bk)
		select entity_id order_id_bk
		from Landing.mag.sales_flat_order
		where status <> 'archived' and base_subtotal <> 0 and customer_id is not null
			and store_id is not null
		union
		select i.order_id
		from 
				Landing.mag.sales_flat_invoice i
			inner join
				Landing.mag.sales_flat_order_aud o on i.order_id = o.entity_id
		where o.status <> 'archived'
		union
		select order_id
		from Landing.mag.sales_flat_shipment
		union
		select order_id
		from Landing.mag.sales_flat_creditmemo
		union
		select oh.entity_id order_id
		from
				(select transactionId, transaction_date, source, medium
				from Landing.mag.ga_entity_transaction_data_aud
				where idETLBatchRun = @idETLBatchRun) ga 
			inner join
				Landing.mag.sales_flat_order_aud oh on ga.transactionId = oh.increment_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	-- Glasses Orders without Glass OL
	delete from Landing.aux.sales_order_header_o
	where order_id_bk in 
		(select t1.order_id
		from
				(select order_id, count(order_id) num_glasses_order
				from Landing.mag.sales_flat_order_item
				where product_id = 2975
				group by order_id) t1
			left join
				(select oi.order_id, count(order_id) num_glasses_line
				from 
						Landing.mag.sales_flat_order_item oi
					inner join
						Landing.aux.prod_product_category pc on pc.category_bk = 'Glasses' and oi.product_id = pc.product_id and pc.product_id not in (2326, 2331, 3135, 3136, 3137, 3138)
					left join
						Landing.map.prod_exclude_products ep on pc.product_id = ep.product_id
				where ep.product_id is null
				group by oi.order_id) t2 on t1.order_id = t2.order_id
		where t2.order_id is null or t1.num_glasses_order <> t2.num_glasses_line)

	delete from Landing.aux.sales_order_header_o
	where order_id_bk in 
		(select entity_id
		from Landing.mag.sales_flat_order
		where base_subtotal = 0)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_aux_sales_order_header_o_i_s_cr
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	27-03-2018	Joseba Bayona Sola	Summer Time Modification
-- ==========================================================================================
-- Description: Reads data from Order Header: O-I-S-CR relations
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_o_i_s_cr
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o_i_s_cr

	-- INSERT STATEMENT: From sales_flat_order - sales_flat_invoice - sales_flat_shipment - sales_flat_creditmemo
		-- For Invoice - Shipment - Creditmemo we can have many rows per Order --> Take the row with first entity_id
	insert into Landing.aux.sales_order_header_o_i_s_cr(order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date)

	select oh.order_id_bk, 
		i.entity_id invoice_id, s.entity_id shipment_id, cr.entity_id creditmemo_id, 
		o.increment_id order_no, i.increment_id invoice_no, s.increment_id shipment_no, cr.increment_id creditmemo_no, 
		CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarOrderDate, 
		RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeOrderDate, 
		o.created_at order_date, 
		CONVERT(INT, (CONVERT(VARCHAR(8), i.created_at, 112))) idCalendarInvoiceDate, i.created_at invoice_date, 
		CONVERT(INT, (CONVERT(VARCHAR(8), s.created_at, 112))) idCalendarShipmentDate, 
		RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, s.created_at)), 2) + ':' + case when (DATEPART(MINUTE, s.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
		s.created_at shipment_date, 
		CONVERT(INT, (CONVERT(VARCHAR(8), cr.created_at, 112))) idCalendarRefundDate, cr.created_at refund_date
	from 
			Landing.aux.sales_order_header_o oh
		inner join
			(select entity_id, increment_id, -- created_at, 
				case when (o.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, o.created_at) else o.created_at end created_at
			from 
					Landing.mag.sales_flat_order_aud o
				left join	 
					Landing.map.sales_summer_time_period_aud stp on year(o.created_at) = stp.year) o on oh.order_id_bk = o.entity_id
		left join
			(select i.order_id, i.entity_id, i.increment_id, -- i.created_at, 
				case when (i.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, i.created_at) else i.created_at end created_at
			from	
				(select order_id, entity_id, increment_id, created_at
				from
					(select i.order_id, i.entity_id, i.increment_id, i.created_at, 
						rank() over (partition by i.order_id order by i.entity_id) rank_inv
					from 
							Landing.aux.sales_order_header_o oh
						inner join
							Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id) t
				where rank_inv = 1) i
			left join	 
				Landing.map.sales_summer_time_period_aud stp on year(i.created_at) = stp.year) i on oh.order_id_bk = i.order_id
		left join
			(select s.order_id, s.entity_id, s.increment_id, -- s.created_at, 
				case when (s.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, s.created_at) else s.created_at end created_at
			from
				(select order_id, entity_id, increment_id, created_at
				from
					(select s.order_id, s.entity_id, s.increment_id, s.created_at, 
						rank() over (partition by s.order_id order by s.entity_id) rank_ship
					from 
							Landing.aux.sales_order_header_o oh
						inner join
							Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id) t
				where rank_ship = 1) s
			left join	 
				Landing.map.sales_summer_time_period_aud stp on year(s.created_at) = stp.year) s on oh.order_id_bk = s.order_id
		left join
			(select r.order_id, r.entity_id, r.increment_id, -- r.created_at,
				case when (r.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, r.created_at) else r.created_at end created_at
			from 
				(select order_id, entity_id, increment_id, created_at
				from
					(select order_id, entity_id, increment_id, created_at,
						dense_rank() over (partition by order_id order by num_order) cr_rank
					from Landing.aux.mag_sales_flat_creditmemo) t
				where cr_rank = 1) r
			left join	 
				Landing.map.sales_summer_time_period_aud stp on year(r.created_at) = stp.year) cr on oh.order_id_bk = cr.order_id
	order by oh.order_id_bk desc

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_header_store_cust
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	31-08-2017	Joseba Bayona Sola	Treat Orders where country is not the one supposed to be (Sp-> ES)
	--	25-09-2017	Joseba Bayona Sola	Check that store_id is different from NULL
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Reads data from Order Header: Store - Customer Info
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_store_cust
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_store_cust

	-- TEMP TABLE: From sales_flat_order -  sales_flat_order_address tables
		-- In case wanted more info about customer - address the ids can be used (customer_id - shipping_address_id - billing_address_id)
	select o.entity_id, 
		o.store_id store_id_bk, o.customer_id customer_id_bk, o.shipping_address_id, o.billing_address_id, 
		isnull(t.country_code_OK, oas.country_id) country_id_shipping, oas.region_id region_id_shipping, oas.postcode postcode_shipping, 
		isnull(t2.country_code_OK, oab.country_id) country_id_billing, oab.region_id region_id_billing, oab.postcode postcode_billing
	into #sales_order_header_store_cust
	from 
			Landing.aux.sales_order_header_o oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		inner join
			Landing.mag.sales_flat_order_address_aud oas on o.shipping_address_id = oas.entity_id
		left join
			(select 'Sp' country_code_wrong, 'ES' country_code_OK) t on oas.country_id = t.country_code_wrong
		inner join
			Landing.mag.sales_flat_order_address_aud oab on o.billing_address_id = oab.entity_id
		left join
			(select 'Sp' country_code_wrong, 'ES' country_code_OK) t2 on oab.country_id = t2.country_code_wrong
	where o.store_id is not null
	order by o.entity_id

	-- INSERT STATEMENT: 
	insert into Landing.aux.sales_order_header_store_cust(order_id_bk,
		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing, 
		customer_unsubscribe_name_bk)
	
		select sc.entity_id, 
			sc.store_id_bk, 
			case 
				when smr1.market_id is not null then smr1.market_id 
				when smr2.market_id is not null then smr2.market_id
				when smr3.market_id is not null then smr3.market_id
			end market_id_bk,		
			sc.customer_id_bk, sc.shipping_address_id, sc.billing_address_id, 
			sc.country_id_shipping, sc.region_id_shipping, sc.postcode_shipping, 
			sc.country_id_billing, sc.region_id_billing, sc.postcode_billing, 
			case when (c.customer_unsubscribe_name_bk = 'Y' and o.idCalendarOrderDate > c.customer_unsubscribe_d) then 'Y' else 'N' end customer_unsubscribe_name_bk
		from
				#sales_order_header_store_cust sc 
			inner join
				Landing.aux.sales_order_header_o_i_s_cr o on sc.entity_id = o.order_id_bk
			left join
				Landing.map.gen_store_market_rel smr1 on sc.store_id_bk = 20 and sc.store_id_bk = smr1.store_id and sc.country_id_shipping = smr1.country		
			left join
				Landing.map.gen_store_market_rel smr2 on sc.store_id_bk <> 20 and sc.store_id_bk = smr2.store_id
			left join
				Landing.map.gen_store_market_rel smr3 on smr1.market_id is null and smr2.market_id is null and sc.store_id_bk = smr3.store_id and smr3.country is null and sc.country_id_shipping is not null
			inner join
				Landing.aux.gen_dim_customer_aud c on sc.customer_id_bk = c.customer_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_store_cust

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_header_dim_order
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	-- 06-06-2017	Joseba Bayona Sola	Add Prescription Method Logic
	-- 25-08-2017	Joseba Bayona Sola	Modify insert on #oh_status for CANCEL orders: Add logic to insert too 'closed' orders where invoice is null
	-- 31-08-2017	Joseba Bayona Sola	Modify reorder_date code checking if is a date attribute. In source is a varchar attribute
	-- 04-12-2017	Joseba Bayona Sola	Add proforma attribute
-- ==========================================================================================
-- Description: Reads data from Order Header: Order related DIM data
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_dim_order
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #oh_stage_type_status(
		order_id_bk						bigint NOT NULL, 
		order_stage_name_bk				varchar(20) NOT NULL,
		order_type_name_bk				varchar(20),
		status_bk						varchar(32))

	insert into #oh_stage_type_status (order_id_bk, order_stage_name_bk, order_type_name_bk, status_bk)

		select order_id_bk, 
			case when (shipment_id is not null) then 'SHIPMENT' else 
				case when (invoice_id is not null) then 'INVOICE' else 'ORDER' end end order_stage_name_bk,
			case 
				when (o.automatic_reorder = 1) then 'Automatic' 
				when (o.postoptics_source = 'user') then 'Web'
				when (o.postoptics_source = 'telesales') then 'Telesales'
				when o.postoptics_source is null then 'Web'
				else o.postoptics_source
			end order_type_name_bk, 
			sm.order_status_magento status_bk
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				Landing.map.sales_order_status_magento_aud sm on o.status = sm.order_status_magento_code


	create table #oh_status(
		order_id_bk						bigint NOT NULL, 
		order_status_name_bk			varchar(20) NOT NULL,
		adjustment_order				char(1))

		insert into #oh_status (order_id_bk, order_status_name_bk, adjustment_order)

			select o.entity_id order_id_bk, 'CANCEL' order_status_name_bk, 'N' adjustment_order
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			where ((o.status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0)) or (base_total_canceled is not null) or (status = 'closed')) 
				and oh.invoice_id is null
				and ((o.base_total_refunded is null or o.base_total_refunded = 0) and (o.base_customer_balance_refunded is null or o.base_customer_balance_refunded = 0))

		insert into #oh_status (order_id_bk, order_status_name_bk, adjustment_order)

			select 
				case when (o.entity_id is not null) then o.entity_id else cr.order_id end order_id_bk,
				case when (cr.min_num_order = 2) then 'OK'
					else case when (abs(base_grand_total - base_total_refunded) < 0.1 and abs(isnull(o.base_customer_balance_amount, 0) - isnull(o.base_customer_balance_refunded, 0)) < 0.1) then 'REFUND' 
						else 'PARTIAL REFUND' end end order_status_name_bk,
				case when (cr.max_num_order = 2) then 'Y' else 'N' end adjustment_order
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
				right join
					(select cr.order_id, min(cr.num_order) min_num_order, max(cr.num_order) max_num_order,
						sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total_cr
					from 
							Landing.aux.mag_sales_flat_creditmemo cr
						inner join
							Landing.aux.sales_order_header_o_i_s_cr oh on cr.order_id = oh.order_id_bk
					group by order_id) cr on o.entity_id = cr.order_id
				left join
					#oh_status t on oh.order_id_bk = t.order_id_bk			
			where t.order_id_bk is null and
				((base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or (base_total_refunded is not null and base_total_refunded <> 0)
					or cr.order_id is not null)
			order by order_id_bk

		insert into #oh_status (order_id_bk, order_status_name_bk, adjustment_order)

			select oh.order_id_bk, 'OK' order_status_name_bk, 'N' adjustment_order
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				left join
					#oh_status t on oh.order_id_bk = t.order_id_bk			
			where t.order_id_bk is null


	create table #oh_prescription_method(
		order_id_bk						bigint NOT NULL, 
		prescription_method_name_bk		varchar(50))

		insert into #oh_prescription_method (order_id_bk, prescription_method_name_bk)

			select order_id_bk, pm.prescription_method prescription_method_name_bk
			from
					(select oh.order_id_bk, 
						case
							when ((o.presc_verification_method = 'call' or o.presc_verification_method is null) and cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification <> 'Yes') then 'Call'
							when (o.presc_verification_method = 'upload' and cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification <> 'Yes') then 'Upload/Fax/Scan'
							when (cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification = 'Yes') then 'Automatic'
							else 'Not Required'
						end prescription_method_name_bk
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
						inner join
							Landing.aux.mag_core_config_data_store cds on o.store_id = cds.store_id and cds.path = 'prescription/settings/enable'
						inner join
							(select order_id, max(is_lens) is_lens
							from Landing.mag.sales_flat_order_item_aud
							group by order_id) oi on oh.order_id_bk = oi.order_id) t
				inner join
					Landing.map.sales_prescription_method pm on t.prescription_method_name_bk = pm.prescription_method

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_order

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_order(order_id_bk,
		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time)

		select osts.order_id_bk,
			osts.order_stage_name_bk, os.order_status_name_bk, os.adjustment_order, osts.order_type_name_bk, osts.status_bk, 
			pm.payment_method payment_method_name_bk, cc.cc_type cc_type_name_bk, p.entity_id payment_id, p.method payment_method_code, 
			sm.shipping_method shipping_description_bk, o.shipping_description shipping_description_code, o.telesales_admin_username telesales_admin_username_bk, opm.prescription_method_name_bk,
			o.reminder_type reminder_type_name_bk, o.reminder_period reminder_period_bk, 
			case when isdate(o.reminder_date) = 1 then convert(datetime, o.reminder_date) else null end reminder_date, -- o.reminder_date reminder_date, 
			o.reminder_sent reminder_sent, 
			case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then 'Y' else 'N' end reorder_f, 
			case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then rp.next_order_date else null end reorder_date, 
			o.reorder_profile_id reorder_profile_id, o.automatic_reorder automatic_reorder, 
			'M' order_source, 'N' proforma,
			null product_type_oh_bk, null order_qty_time, null aura_product_p, 

			o.referafriend_code, o.referafriend_referer, 
			o.postoptics_auto_verification, o.presc_verification_method, o.warehouse_approved_time
		from 
				#oh_stage_type_status osts
			inner join
				#oh_status os on osts.order_id_bk = os.order_id_bk
			inner join
				#oh_prescription_method opm on osts.order_id_bk = opm.order_id_bk
			inner join
				Landing.mag.sales_flat_order_aud o on osts.order_id_bk = o.entity_id

			inner join
				Landing.mag.sales_flat_order_payment_aud p on o.entity_id = p.parent_id
			left join
				Landing.map.sales_payment_method_aud pm on p.method = pm.payment_method_code
			left join
				Landing.map.sales_cc_type_aud cc on p.cc_type = cc.card_type_code

			left join
				Landing.map.sales_shipping_method_aud sm on o.shipping_description = sm.shipping_method_code

			left join
				Landing.mag.po_reorder_profile_aud rp on o.reorder_profile_id = rp.id	
		order by osts.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #oh_stage_type_status
	drop table #oh_status
	drop table #oh_prescription_method

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_header_dim_mark
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	Add Marketing Channel Logic (Attributes set to NULL)
	--	30-11-2017	Joseba Bayona Sola	Add Logic for set up the right Channel - Marketing Channel
	--	30-11-2017	Joseba Bayona Sola	Add Logic for Mutuelles
-- ==========================================================================================
-- Description: Reads data from Order Header: Marketing related DIM data
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_dim_mark
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	create table #oh_couponcode(
		order_id_bk						bigint NOT NULL, 
		coupon_id_bk					int,
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255), 
		channel							varchar(255))

		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) = 0 -- and len(o.applied_rule_ids) < 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
			order by o.applied_rule_ids, o.coupon_code

		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) <> 0 -- and len(o.applied_rule_ids) >= 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code
			order by o.applied_rule_ids, o.coupon_code


	create table #oh_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		ga_channel						varchar(100),
		telesales_f						char(1))

	insert into #oh_channel(order_id_bk, channel_name_bk, 
		ga_medium, ga_source, ga_channel, telesales_f)

		select order_id_bk, 
			case 
				when (ga_medium is null and telesales_f = 'N') then 'Non-GA'
				when (ga_medium is null and telesales_f = 'Y') then 'Non-GA Telesales'
				when (channel_name is not null) then channel_name
				else null
			end channel_name_bk,
			ga_medium, ga_source, null ga_channel, telesales_f
		from
			(select oh.order_id_bk, mc.channel_name,
				ga.medium ga_medium, ga.source ga_source, o.affilCode, 
				case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f 
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				left join
					Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
				left join
					Landing.map.sales_ch_ga_medium_ch mc on ga.medium = mc.ga_medium) t
		order by ga_medium, telesales_f, ga_source


	create table #oh_marketing_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		marketing_channel_name_bk		varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		affilCode						varchar(255),
		cc_channel						varchar(255),
		telesales_f						char(1), 
		sms_auto_f						char(1),
		mutuelles_f						char(1))

	insert into #oh_marketing_channel(order_id_bk, channel_name_bk,	
		ga_medium, ga_source, affilCode, cc_channel, telesales_f, sms_auto_f, mutuelles_f)

		select oh.order_id_bk, c.channel_name_bk, 
			c.ga_medium, c.ga_source, o.affilCode, 
			cc.channel cc_channel,
			case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f, 
			null sms_auto_f, 
			case when (mut.order_id_bk is not null) then 'A' else 'N' end mutuelles_f 
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				#oh_channel c on oh.order_id_bk = c.order_id_bk
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				(select *
				from #oh_couponcode
				where channel in 
					(select cc_channel
					from Landing.map.sales_ch_cc_channel_mch)) cc on oh.order_id_bk = cc.order_id_bk
			left join
				(select oh.entity_id order_id_bk, 
					oh.mutual_amount, oh.mutual_quotation_id, 
					mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount
				from 
						Landing.mag.sales_flat_order_aud oh
					inner join
						Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
					inner join 
						Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
				where mutual_amount is not null) mut on oh.order_id_bk = mut.order_id_bk
		order by c.ga_medium, o.affilCode, o.telesales_admin_username, c.ga_source, o.store_id, oh.order_date

		-- Mutuelles
		update #oh_marketing_channel
		set marketing_channel_name_bk = 'Mututelles Auto'
		where marketing_channel_name_bk is null and mutuelles_f = 'A'

		-- SMS Automatic

		-- Coupon Code
		update mc
		set 
			mc.marketing_channel_name_bk = ccmch.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_cc_channel_mch ccmch on mc.cc_channel = ccmch.cc_channel
		where mc.marketing_channel_name_bk is null

		-- Customer Service
		update #oh_marketing_channel
		set marketing_channel_name_bk = 'Customer Service'
		where marketing_channel_name_bk is null and telesales_f = 'Y'

		-- BSC: Exact affilCode Match
		update mc
		set 
			mc.marketing_channel_name_bk = ac.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_mch ac on mc.affilCode = ac.affil_code
		where mc.marketing_channel_name_bk is null

		-- BSC: Like affilCode Match - adwords-bk
		update mc
		set 
			mc.marketing_channel_name_bk = acl.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
		where mc.marketing_channel_name_bk is null

		-- BSC: Like affilCode Match - rest
		update mc
		set 
			mc.marketing_channel_name_bk = acl.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like <> 'adwords-br'
		where mc.marketing_channel_name_bk is null

		-- GA: Email
		update mc
		set 
			mc.marketing_channel_name_bk = isnull(gas.marketing_channel_name, 'Email Marketing')
		from
				#oh_marketing_channel mc
			left join
				Landing.map.sales_ch_ga_source_mch gas on mc.ga_source = gas.ga_source
		where mc.marketing_channel_name_bk is null and mc.channel_name_bk = 'Email'

		-- GA
		update #oh_marketing_channel
		set 
			marketing_channel_name_bk = case 
					when (channel_name_bk = 'Paid Search') then 'Paid Search Non Branded'
					when (channel_name_bk = 'SMS') then 'SMS Url'
					else channel_name_bk
				end
		where marketing_channel_name_bk is null and channel_name_bk is not null



	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_dim_mark

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_dim_mark(order_id_bk, 
		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os)

		select oh.order_id_bk, 
			c.channel_name_bk, mc.marketing_channel_name_bk, null publisher_id_bk, 
			mc.affilCode, c.ga_medium, c.ga_source, null ga_channel, mc.telesales_f, mc.sms_auto_f, mc.mutuelles_f,
			cc.coupon_id_bk, cc.coupon_code, cc.applied_rule_ids,
			null device_model_name_bk, null device_browser_name_bk, null device_os_name_bk, 
			null ga_device_model, null ga_device_browser, null ga_device_os
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				#oh_couponcode cc on oh.order_id_bk = cc.order_id_bk
			inner join
				#oh_channel c on oh.order_id_bk = c.order_id_bk
			inner join
				#oh_marketing_channel mc on oh.order_id_bk = mc.order_id_bk
		order by oh.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #oh_couponcode 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

drop procedure mag.lnd_stg_get_aux_sales_order_header_measures
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	26-07-2017	Joseba Bayona Sola	Change local_store_credit_used logic being 0 if value is null
	--	08-12-2017	Joseba Bayona Sola	Change discount_percent logic in case OH subtotal = 0
-- ==========================================================================================
-- Description: Reads data from Order Header: Measures Attributes
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_measures
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_header_measures(
		order_id_bk							bigint NOT NULL,
		local_subtotal						decimal(12, 4),
		local_shipping						decimal(12, 4),
		local_discount						decimal(12, 4),
		local_store_credit_used				decimal(12, 4),
		local_total_inc_vat					decimal(12, 4),

		local_total_refund					decimal(12, 4),
		local_store_credit_given			decimal(12, 4),
		local_bank_online_given				decimal(12, 4),

		local_subtotal_refund				decimal(12, 4),
		local_shipping_refund				decimal(12, 4),
		local_discount_refund				decimal(12, 4),
		local_store_credit_used_refund		decimal(12, 4),
		local_adjustment_refund				decimal(12, 4),
		local_total_aft_refund_inc_vat		decimal(12, 4),
		local_total_aft_refund_inc_vat_2	decimal(12, 4),

		local_to_global_rate				decimal(12, 4),
		order_currency_code					varchar(255),
		discount_percent					decimal(12, 4))

		insert into #sales_order_header_measures(order_id_bk, 
			local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
			local_total_refund, local_store_credit_given, local_bank_online_given, 
			local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
			local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, 
			local_to_global_rate, order_currency_code, 
			discount_percent)

			select oh.order_id_bk, 
				o.base_subtotal local_subtotal, o.base_shipping_amount local_shipping, 
				case when (o.base_discount_amount < 0) then o.base_discount_amount * -1 else o.base_discount_amount end local_discount, isnull(o.base_customer_balance_amount, 0) local_store_credit_used, 
				o.base_grand_total local_total_inc_vat, 

				isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0) local_total_refund, 
				isnull(o.bs_customer_bal_total_refunded, 0) local_store_credit_given, 
				(isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0)) - isnull(o.bs_customer_bal_total_refunded, 0) local_bank_online_given, 

				isnull(cr.sum_base_subtotal_amount, 0) local_subtotal_refund, isnull(cr.sum_base_shipping_amount, 0) local_shipping_refund, isnull(cr.sum_base_discount_amount, 0) local_discount_refund, 
				isnull(cr.sum_base_customer_balance_amount, 0) local_store_credit_used_refund, isnull(cr.sum_base_adjustment, 0) local_adjustment_refund, 
				o.base_grand_total - ((isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0)) - isnull(o.bs_customer_bal_total_refunded, 0)) local_total_aft_refund_inc_vat, 
				null local_total_aft_refund_inc_vat_2, 

				o.base_to_global_rate local_to_global_rate, o.order_currency_code order_currency_code, 
				case when (o.base_subtotal = 0) then 0 
					else convert(decimal(10,1), (case when (o.base_discount_amount < 0) then o.base_discount_amount * -1 else o.base_discount_amount end * 100 / o.base_subtotal)) 
				end	discount_percent
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.aux.sales_order_header_dim_order odo on oh.order_id_bk = odo.order_id_bk
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
				left join
					(select cr.order_id, min(cr.num_order) min_num_order, max(cr.num_order) max_num_order,
						sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total_cr, 
						sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal_amount, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount,
						sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount,
						sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, 
						sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative 
					from 
							Landing.aux.mag_sales_flat_creditmemo cr
						inner join
							Landing.aux.sales_order_header_o_i_s_cr oh on cr.order_id = oh.order_id_bk
					group by order_id) cr on oh.order_id_bk = cr.order_id
			order by oh.order_id_bk

	merge into #sales_order_header_measures trg
	using 
		(select order_id_bk,
			(local_subtotal - local_subtotal_refund) + (local_shipping - local_shipping_refund) - (local_discount - local_discount_refund)  
				- (local_store_credit_used - local_store_credit_used_refund) - local_adjustment_refund 
				+ (local_store_credit_given - local_store_credit_used_refund) local_total_aft_refund_inc_vat_2
		from #sales_order_header_measures) src
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.local_total_aft_refund_inc_vat_2 = src.local_total_aft_refund_inc_vat_2;


	exec mag.lnd_stg_get_aux_sales_order_line_measures @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_header_oh_pt @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- UPDATE AURA PERCENTAGE ON ORDER HEADER
	merge into Landing.aux.sales_order_header_dim_order trg
	using 
		(select order_id_bk, aura_product_f,
			total_subtotal, total_subtotal_esp, 
			case when (aura_product_f = 'N') then 0 else convert(decimal(12, 4), (total_subtotal_esp * 100 / total_subtotal)) end aura_product_p
		from
			(select ol.order_id_bk, 
				sum(local_subtotal) over (partition by ol.order_id_bk) total_subtotal, 
				sum(local_subtotal) over (partition by ol.order_id_bk, aura_product_f) total_subtotal_esp, 
				max(aura_product_f) over (partition by ol.order_id_bk) aura_product_f,
				dense_rank() over (partition by ol.order_id_bk order by aura_product_f desc, ol.order_line_id_bk) num
			from 
					Landing.aux.sales_order_line_measures ol
				inner join
					Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk) t
		where num = 1) src
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set 
			trg.aura_product_p = src.aura_product_p;

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_measures

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_measures(order_id_bk, 
		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, 	
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, 
		local_to_global_rate, order_currency_code, 
		discount_percent, 
		num_lines)

		select o.order_id_bk, 
			ol.price_type_name_bk, ol.discount_f,
			ol.total_qty_unit, ol.total_qty_unit_refunded, ol.total_qty_pack, 
			o.local_subtotal, o.local_shipping, o.local_discount, o.local_store_credit_used, 
			o.local_total_inc_vat, 	
			o.local_total_refund, o.local_store_credit_given, o.local_bank_online_given, 
			o.local_subtotal_refund, o.local_shipping_refund, o.local_discount_refund, o.local_store_credit_used_refund, o.local_adjustment_refund, 
			o.local_total_aft_refund_inc_vat, o.local_total_aft_refund_inc_vat_2, 
			o.local_to_global_rate, o.order_currency_code, 
			o.discount_percent, 
			ol.num_lines
		from 
				#sales_order_header_measures o
			inner join
 				(select order_id_bk, price_type_name_bk, discount_f,  
					total_qty_unit, total_qty_unit_refunded, total_qty_pack, num_lines
				from
					(select order_id_bk, price_type_name_bk,  
						max(discount_f) over (partition by order_id_bk) discount_f,
						count(*) over (partition by order_id_bk) num_lines, 
						sum(qty_unit) over (partition by order_id_bk) total_qty_unit, 
						sum(qty_unit_refunded) over (partition by order_id_bk) total_qty_unit_refunded, 
						sum(qty_pack) over (partition by order_id_bk) total_qty_pack,
						dense_rank() over (partition by order_id_bk order by ptn.priority, order_line_id_bk) num
					from 
							Landing.aux.sales_order_line_measures ol
						inner join
							Landing.map.sales_price_type_aud ptn on ol.price_type_name_bk = ptn.price_type) t
				where num = 1) ol on o.order_id_bk = ol.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_measures

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_aux_sales_order_header_vat
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2017
-- Changed: 
	--	22-09-2017	Joseba Bayona Sola	Add VAT Logic for Old Stores (PO, GLUK, GLIE, GLIT, ASDA, EYEPLAN)
-- ==========================================================================================
-- Description: Reads data from Order Header: VAT Attributes
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_vat
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_header_vat(
		order_id_bk						bigint NOT NULL,
		countries_registered_code		varchar(10) NOT NULL,
		invoice_date					datetime,

		local_total_inc_vat				decimal(12, 4),
		local_total_aft_refund_inc_vat	decimal(12, 4))

	-- Old Store Orders: VAT Logic based on store_id
	insert into #sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_aft_refund_inc_vat)

		select oh_sc.order_id_bk,
			crs.countries_registered_code countries_registered_code, oh.invoice_date,
			oh_m.local_total_inc_vat, oh_m.local_total_aft_refund_inc_vat
		from 
				Landing.aux.sales_order_header_store_cust oh_sc
			inner join
				(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
				from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
			inner join
				Landing.mag.core_store_aud s on oh_sc.store_id_bk = s.store_id
			inner join
				Landing.map.vat_countries_registered_store_aud crs on s.store_id = crs.store_id
			inner join
				Landing.aux.sales_order_header_measures oh_m on oh_sc.order_id_bk = oh_m.order_id_bk

	-- Current Store Orders: VAT Logic based on Shipping Country
	insert into #sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_aft_refund_inc_vat)

		select oh_sc.order_id_bk, 
			case when (vr.countries_registered_code is not null) then vr.countries_registered_code else vr2.countries_registered_code end countries_registered_code, oh.invoice_date,
			oh_m.local_total_inc_vat, oh_m.local_total_aft_refund_inc_vat
		from 
				Landing.aux.sales_order_header_store_cust oh_sc
			inner join
				(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
				from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
			inner join
				Landing.mag.core_store_aud s on oh_sc.store_id_bk = s.store_id
			inner join
				Landing.map.gen_countries_aud c on oh_sc.country_id_shipping_bk = c.country_code
			inner join
				Landing.aux.sales_order_header_measures oh_m on oh_sc.order_id_bk = oh_m.order_id_bk
			left join
				(select cr.countries_registered_code, 
					cr.country_type, cr.country_code,
					vr.vat_time_period_start, vr.vat_time_period_finish
				from 
						Landing.map.vat_countries_registered_aud cr
					inner join
						(select distinct countries_registered_code, vat_time_period_start, vat_time_period_finish
						from Landing.map.vat_vat_rate_aud) vr on cr.countries_registered_code = vr.countries_registered_code
					left join 
						(select distinct countries_registered_code
						from Landing.map.vat_countries_registered_store_aud) crs on cr.countries_registered_code = crs.countries_registered_code
				where crs.countries_registered_code is null
					and cr.country_type in ('UK', 'EU') and cr.country_code <> 'ZZ') vr 
							on c.country_type = vr.country_type and oh_sc.country_id_shipping_bk = vr.country_code and oh.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
			left join
				(select cr.countries_registered_code, 
					cr.country_type, cr.country_code,
					vr.vat_time_period_start, vr.vat_time_period_finish
				from 
						Landing.map.vat_countries_registered_aud cr
					inner join
						(select distinct countries_registered_code, vat_time_period_start, vat_time_period_finish
						from Landing.map.vat_vat_rate_aud) vr on cr.countries_registered_code = vr.countries_registered_code
				where cr.country_code = 'ZZ') vr2 
							on c.country_type = vr2.country_type and oh.invoice_date is not null and vr.countries_registered_code is null
			left join
				Landing.map.vat_countries_registered_store_aud crs on s.store_id = crs.store_id
		where crs.store_id is null
		order by c.country_type, oh_sc.country_id_shipping_bk, oh_sc.order_id_bk

	-- Orders where not logic could be set up: NON-VAT
	insert into #sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_aft_refund_inc_vat)

		select oh_sc.order_id_bk, 
			'NON-VAT' countries_registered_code, oh.invoice_date,
			oh_m.local_total_inc_vat, oh_m.local_total_aft_refund_inc_vat
		from 
				Landing.aux.sales_order_header_store_cust oh_sc
			inner join
				(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
				from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
			inner join
				Landing.aux.sales_order_header_measures oh_m on oh_sc.order_id_bk = oh_m.order_id_bk
			left join
				#sales_order_header_vat ov on oh_sc.order_id_bk = ov.order_id_bk
		where ov.order_id_bk is null

	exec Landing.mag.lnd_stg_get_aux_sales_order_line_vat  @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_vat

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_vat(order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee,
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee,
		vat_percent, prof_fee_percent)

		select o_v.order_id_bk, 
			o_v.countries_registered_code, o_v.invoice_date, 
			o_v.local_total_inc_vat, ol_v.local_total_exc_vat, ol_v.local_total_vat, ol_v.local_total_prof_fee,
			o_v.local_total_aft_refund_inc_vat, ol_v.local_total_aft_refund_exc_vat, ol_v.local_total_aft_refund_vat, ol_v.local_total_aft_refund_prof_fee, 
			case when (ol_v.local_total_exc_vat = 0) then 0 else ol_v.local_total_vat * 100 / ol_v.local_total_exc_vat end vat_percent, 
			case when (o_v.local_total_inc_vat = 0) then 0 else ol_v.local_total_prof_fee * 100 / o_v.local_total_inc_vat end prof_fee_percent
		from 
				#sales_order_header_vat o_v
			inner join
				(select order_id_bk, 
					sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_prof_fee) local_total_prof_fee, 
					sum(local_total_aft_refund_exc_vat) local_total_aft_refund_exc_vat, sum(local_total_aft_refund_vat) local_total_aft_refund_vat, sum(local_total_aft_refund_prof_fee) local_total_aft_refund_prof_fee
				from Landing.aux.sales_order_line_vat
				group by order_id_bk) ol_v on o_v.order_id_bk = ol_v.order_id_bk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_vat
	
	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_header_oh_pt
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	25-09-2017	Joseba Bayona Sola	Exclude OH where SUM of base_subtotal in OL = 0
-- ==========================================================================================
-- Description: Reads data from Order Header: OH - PT relation
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_header_oh_pt
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_oh_pt

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_oh_pt(order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time)

		select order_id_bk, product_type_oh_bk, 
			convert(decimal(12, 4), sum(local_subtotal) * 100 / total_local_subtotal) order_percentage, 1 order_flag, max(qty_time) order_qty_time
		from
			(select olp.order_line_id_bk, olp.order_id_bk, 
				olp.product_type_oh_bk, 
				olm.qty_time, olm.local_subtotal, 
				sum(olm.local_subtotal) over (partition by olp.order_id_bk) total_local_subtotal
			from 
					Landing.aux.sales_order_line_product olp
				inner join
					Landing.aux.sales_order_line_measures olm on olp.order_line_id_bk = olm.order_line_id_bk) ol
		where total_local_subtotal <> 0
		group by order_id_bk, product_type_oh_bk, total_local_subtotal
		order by order_id_bk, product_type_oh_bk, total_local_subtotal

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






drop procedure mag.lnd_stg_get_aux_sales_order_line_o_i_s_cr
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	27-03-2018	Joseba Bayona Sola	Summer Time Modification
-- ==========================================================================================
-- Description: Reads data from Order Line: O-I-S-CR relations
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_line_o_i_s_cr
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_o_i_s_cr

	-- INSERT STATEMENT: From sales_flat_order_item - sales_flat_invoice_item - sales_flat_shipment_item - sales_flat_creditmemo_item
		-- For Invoice: We suppose that the right invoice is the one taken for Header
		-- For Shipment - Creditmemo we can have many rows per Order Item --> Take the row with first entity_id + Count Lines

	insert into Landing.aux.sales_order_line_o_i_s_cr(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk)

		select 
 			oi.item_id order_line_id_bk, oh.order_id_bk, 
			ii.invoice_id, si.shipment_id, cri.entity_id creditmemo_id,
			oh.order_no, ii.invoice_no, si.shipment_no, cri.increment_id creditmemo_no,
			ii.invoice_line_id, si.shipment_line_id, cri.item_id creditmemo_line_id,
			si.num_shipment_lines, cri.num_rep_rows num_creditmemo_lines,
			ii.idCalendarInvoiceDate, ii.invoice_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), si.shipment_date, 112))) idCalendarShipmentDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, si.shipment_date)), 2) + ':' + case when (DATEPART(MINUTE, si.shipment_date) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
			si.shipment_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), cri.created_at, 112))) idCalendarRefundDate, cri.created_at refund_date, 
			1 warehouse_id_bk
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_item_aud oi on oh.order_id_bk = oi.order_id
			left join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
			left join
				(select oh.order_id_bk, oh.invoice_id, oh.invoice_no, 
					ii.entity_id invoice_line_id, ii.order_item_id,
					oh.idCalendarInvoiceDate, oh.invoice_date
				from 
						Landing.aux.sales_order_header_o_i_s_cr oh
					inner join
						Landing.mag.sales_flat_invoice_aud i on oh.invoice_id = i.entity_id
					inner join
						Landing.mag.sales_flat_invoice_item_aud ii on i.entity_id = ii.parent_id) ii on oh.order_id_bk = ii.order_id_bk and oi.item_id = ii.order_item_id
			left join		
				(select s.order_id_bk, s.shipment_id, s.shipment_no, 
					s.shipment_line_id, s.order_item_id, 
					s.num_shipment_lines,
					-- s.shipment_date, 
					case when (s.shipment_date between stp.from_date and stp.to_date) then dateadd(hour, 1, s.shipment_date) else s.shipment_date end shipment_date
				from
						(select order_id_bk, shipment_id, shipment_no, 
							shipment_line_id, order_item_id, 
							num_shipment_lines,
							shipment_date
						from
							(select oh.order_id_bk, 
								s.entity_id shipment_id, s.increment_id shipment_no, 
								si.entity_id shipment_line_id, si.order_item_id,
								s.created_at shipment_date,
								count(*) over (partition by si.order_item_id) num_shipment_lines,
								rank() over (partition by si.order_item_id order by s.entity_id) rank_s
							from 
									Landing.aux.sales_order_header_o_i_s_cr oh
								inner join
									Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id
								inner join
									Landing.mag.sales_flat_shipment_item_aud si on s.entity_id = si.parent_id) s --where oh.order_id_bk = 5275010
						where rank_s = 1) s 
					left join	 
						Landing.map.sales_summer_time_period_aud stp on year(s.shipment_date) = stp.year) si on oh.order_id_bk = si.order_id_bk and oi.item_id = si.order_item_id
			left join
				(select order_id, order_item_id, entity_id, increment_id, item_id, num_rep_rows, -- created_at,
					case when (cri.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, cri.created_at) else cri.created_at end created_at
				from 
						Landing.aux.mag_sales_flat_creditmemo_item cri
					left join
						Landing.map.sales_summer_time_period_aud stp on year(cri.created_at) = stp.year) cri on oh.order_id_bk = cri.order_id and oi.item_id = cri.order_item_id
		where ep.product_id is null
		order by oh.order_id_bk, oi.item_id 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_line_product
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	06-06-2017	Joseba Bayona Sola	Add Eye Logic
-- ==========================================================================================
-- Description: Reads data from Order Line: Product Info
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_line_product
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	create table #glasses_conv(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL, 
		product_id_bk					int NOT NULL, 

		glass_vision_type_bk			varchar(50),
		order_line_id_vision_type		bigint, 
		glass_package_type_bk			varchar(50),
		order_line_id_package_type		bigint)

	insert into #glasses_conv(order_line_id_bk, order_id_bk, product_id_bk, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type)

		select gl.item_id order_line_id_bk, gl.order_id order_id_bk, 
			gl.product_id, 
			vi.product_name glass_vision_type_bk, vi.item_id order_line_id_vision_type, pk.product_name glass_package_type_bk, pk.item_id order_line_id_package_type
		from
				(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
					rank() over (partition by oi.order_id order by item_id) rank_glass_row
				from 
						Landing.aux.sales_order_line_o_i_s_cr ol
					inner join	
						Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
					inner join
						Landing.aux.prod_product_category pc on oi.product_id = pc.product_id and pc.category_bk = 'Glasses' and pc.product_id not in (2326, 2331, 3135, 3136, 3137, 3138)) gl
			left join
				(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, ep.product_name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
					rank() over (partition by oi.order_id order by item_id) rank_vision_type_row
				from 
						(select distinct order_id_bk
						from Landing.aux.sales_order_line_o_i_s_cr) ol
					inner join
						Landing.mag.sales_flat_order_item_aud oi on ol.order_id_bk = oi.order_id
					inner join
						Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'vision type') vi on gl.order_id = vi.order_id and gl.rank_glass_row = vi.rank_vision_type_row
			left join
				(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, ep.product_name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
					rank() over (partition by oi.order_id order by item_id) rank_package_type_row
				from 
						(select distinct order_id_bk
						from Landing.aux.sales_order_line_o_i_s_cr) ol
					inner join
						Landing.mag.sales_flat_order_item_aud oi on ol.order_id_bk = oi.order_id
					inner join
						Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'package type') pk on gl.order_id = pk.order_id and gl.rank_glass_row = pk.rank_package_type_row
		order by gl.order_id, gl.item_id


	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_product

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_line_product(order_line_id_bk, order_id_bk, 
		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, product_type_oh_bk)
	
		select oi.item_id, oi.order_id, 
			-- oi.product_id, 
			case when (pr_90.product_id is not null) then pr_90.product_id else oi.product_id end product_id,
			esi.bc, esi.di, esi.po, esi.cy, esi.ax, esi.ad, esi.do, esi.co, 
			case when (oi.lens_group_eye in ('Right', 'Left')) then left(oi.lens_group_eye, 1) else null end eye,
			oi.sku sku_magento, null sku_erp, 
			gl.glass_vision_type_bk, gl.order_line_id_vision_type, gl.glass_package_type_bk, gl.order_line_id_package_type,
			case when (pr_aura.product_id is not null) then 'Y' else 'N' end aura_product_f, ppt.product_type_oh_bk
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_edi_stock_item esi on oi.sku = esi.product_code and oi.product_id = esi.product_id
			left join
				#glasses_conv gl on ol.order_line_id_bk = gl.order_line_id_bk
			left join
				Landing.map.prod_product_aura_aud pr_aura on oi.product_id = pr_aura.product_id
			left join
				Landing.map.prod_product_30_90_aud pr_90 on oi.product_id = pr_90.product_id_new
			inner join
				Landing.aux.prod_product_product_type_oh ppt on oi.product_id = ppt.product_id
		order by oi.item_id, oi.order_id		

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #glasses_conv 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_sales_order_line_measures
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 09-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	25-07-2017	Joseba Bayona Sola	Modify code totake care of Refunded Values at OL level (using order_allocation_rate_refund)
	--	30-08-2017	Joseba Bayona Sola	Add Yearly category + CL prefix
	--  25-09-2017	Joseba Bayona Sola	Take care of Store ID - Product combinations where we have more than 1 Tier Pricing row
-- ==========================================================================================
-- Description: Reads data from Order Line: Measures Attributes
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_line_measures
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_header_line_measures(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL,
		order_status_name_bk			varchar(20) NOT NULL, 
		price_type_name_bk				varchar(40),
		discount_f						char(1),
		qty_unit						decimal(12, 4),
		qty_unit_refunded				decimal(12, 4),
		qty_pack						int,
		qty_time						int,

		local_price_unit				decimal(12, 4),
		local_price_pack				decimal(12, 4),
		local_price_pack_discount		decimal(12, 4),

		local_subtotal					decimal(12, 4),
		local_discount					decimal(12, 4),
		local_subtotal_refund			decimal(12, 4),
		local_discount_refund			decimal(12, 4),

		discount_percent				decimal(12, 4),
		order_allocation_rate			decimal(12, 4), 
		order_allocation_rate_aft_refund decimal(12, 4),
		order_allocation_rate_refund	decimal(12, 4))
	

	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, local_subtotal_refund, local_discount_refund, 
		discount_percent, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			case when (odo.order_status_name_bk in ('OK', 'CANCEL')) then odo.order_status_name_bk else 
				case when (oi.qty_ordered = oi.qty_refunded) then 'REFUND'
					 when (oi.qty_ordered > oi.qty_refunded and oi.qty_refunded > 0) then 'PARTIAL REFUND'
					 else 'OK' end end order_status_name_bk,
			--case when (oi.base_discount_amount <> 0) then
			--	case 
			--		when (pla.order_item_id is not null) then 'PLA & Discounted'
			--		when (pp.value is not null) then 'Tier Pricing & Discounted'
			--		else 'Discounted'
			--	end
			--	else case 
			--		when (pla.order_item_id is not null) then 'PLA'
			--		when (pp.value is not null) then 'Tier Pricing'
			--		else 'Regular'
			--	end
			--end price_type_name_bk,
			case 
				when (pla.order_item_id is not null) then 'PLA'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end price_type_name_bk, 
			case when (oi.base_discount_amount <> 0) then 'Y' else 'N' end discount_f,
			oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, 
			case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end qty_pack, -- broken orders
			oi.qty_ordered / pc.num_months qty_time,
			oi.base_price local_price_unit, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack, 
			(oi.base_row_total - oi.base_discount_amount) / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack_discount, 

			oi.base_row_total local_subtotal, oi.base_discount_amount local_discount, 
			oi.base_amount_refunded local_refund, isnull(oicr.sum_base_discount_amount, 0) local_discount_refund, 
			oi.discount_percent discount_percent,
			null order_allocation_rate, null order_allocation_rate_aft_refund, null order_allocation_rate_refund
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr on ol.order_line_id_bk = oicr.order_item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
			left join
				Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
			left join
				(select store_id, product_id, min(value) value, min(qty) qty
				from Landing.aux.mag_catalog_product_price_aud
				where price_type = 'Tier Pricing'
				group by store_id, product_id) pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
		where olp.order_line_id_vision_type is null 
		order by ol.order_id_bk, ol.order_line_id_bk

	-- Insert Glasses Products (Special Treatment for Package Type Lines)
	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, local_subtotal_refund, local_discount_refund, 
		discount_percent, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			case when (odo.order_status_name_bk in ('OK', 'CANCEL')) then odo.order_status_name_bk else 
				case when (oi.qty_ordered = oi.qty_refunded) then 'REFUND'
					 when (oi.qty_ordered > oi.qty_refunded and oi.qty_refunded > 0) then 'PARTIAL REFUND'
					 else 'OK' end end order_status_name_bk,
			--case when (oi.base_discount_amount <> 0) then
			--	case 
			--		when (pla.order_item_id is not null) then 'PLA & Discounted'
			--		when (pp.value is not null) then 'Tier Pricing & Discounted'
			--		else 'Discounted'
			--	end
			--	else case 
			--		when (pla.order_item_id is not null) then 'PLA'
			--		when (pp.value is not null) then 'Tier Pricing'
			--		else 'Regular'
			--	end
			--end price_type_name_bk,
			case 
				when (pla.order_item_id is not null) then 'PLA'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end price_type_name_bk, 
			case when (oi.base_discount_amount <> 0) then 'Y' else 'N' end discount_f,
			oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) qty_pack, 
			oi.qty_ordered / pc.num_months qty_time,
			oi.base_price local_price_unit, 
			oi.base_row_total / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack, 
			(oi.base_row_total - oi.base_discount_amount) / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack_discount, 

			oi.base_row_total + oi2.base_row_total local_subtotal, oi.base_discount_amount + oi2.base_discount_amount local_discount, 
			oi.base_amount_refunded + oi2.base_amount_refunded local_refund, isnull(oicr.sum_base_discount_amount, 0) + isnull(oicr2.sum_base_discount_amount, 0) local_discount_refund, 
			oi.discount_percent discount_percent,
			null order_allocation_rate, null order_allocation_rate_aft_refund, null order_allocation_rate_refund
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			inner join
				Landing.mag.sales_flat_order_item_aud oi2 on olp.order_line_id_package_type = oi2.item_id
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr on ol.order_line_id_bk = oicr.order_item_id
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr2 on olp.order_line_id_package_type = oicr2.order_item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
			left join
				Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
			left join
				(select store_id, product_id, value, qty
				from Landing.aux.mag_catalog_product_price_aud
				where price_type = 'Tier Pricing') pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
		where olp.order_line_id_package_type is not null 
		order by ol.order_id_bk, ol.order_line_id_bk

		
	merge into #sales_order_header_line_measures trg
	using 
		(select ol.order_line_id_bk, 
			convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) allocation_rate, 
			case when (abs(o.local_subtotal - o.local_subtotal_refund) < 0.1) then convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) -- OH FULL REFUND
				else case when (abs(ol.local_subtotal - ol.local_subtotal_refund) < 0.1) then 0  -- OL ITEM IS NOT REFUNDED
						else convert(decimal(12, 4), (ol.local_subtotal - ol.local_subtotal_refund) / (o.local_subtotal - o.local_subtotal_refund)) end -- OL PARTIAL REFUND
			end order_allocation_rate_aft_refund, 
			case when (ol.local_subtotal_refund <> 0 and o.local_subtotal_refund <> 0) then 
				convert(decimal(12, 4), (ol.local_subtotal_refund / o.local_subtotal_refund)) else 0 end order_allocation_rate_refund -- Modified
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.order_allocation_rate = src.allocation_rate, 
			trg.order_allocation_rate_aft_refund = src.order_allocation_rate_aft_refund,
			trg.order_allocation_rate_refund = src.order_allocation_rate_refund;

	-- SPECIAL SCENARIO FOR THOSE ORDERS WHERE NOT REFUND ITEMS BUT ADJUSTMENT DONE
	merge into #sales_order_header_line_measures trg
	using 
		(select ol.order_line_id_bk, 
			convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) order_allocation_rate_refund 
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk
			inner join
				Landing.aux.sales_order_header_dim_order oh_o on ol.order_id_bk = oh_o.order_id_bk
		where oh_o.order_status_name_bk = 'OK' and adjustment_order = 'Y') src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.order_allocation_rate_refund = src.order_allocation_rate_refund;

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_measures

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund,
		local_to_global_rate, order_currency_code, 
		discount_percent,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.order_status_name_bk, ol.price_type_name_bk, ol.discount_f,
			ol.qty_unit, ol.qty_unit_refunded, ol.qty_pack, ol.qty_time, 
			ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
			ol.local_subtotal, o.local_shipping * ol.order_allocation_rate local_shipping, ol.local_discount, o.local_store_credit_used * ol.order_allocation_rate local_store_credit_used, 
			-- null local_store_credit_given, null local_bank_online_given, 
			o.local_store_credit_given * ol.order_allocation_rate_refund local_store_credit_given, o.local_bank_online_given * ol.order_allocation_rate_refund local_bank_online_given, 
			ol.local_subtotal_refund, o.local_shipping_refund * ol.order_allocation_rate_refund local_shipping_refund,
			ol.local_discount_refund, o.local_store_credit_used_refund * ol.order_allocation_rate_refund local_store_credit_used_refund, 
			o.local_adjustment_refund * ol.order_allocation_rate_refund local_adjustment_refund,
			o.local_to_global_rate, o.order_currency_code, 
			ol.discount_percent,
			ol.order_allocation_rate, ol.order_allocation_rate_aft_refund, ol.order_allocation_rate_refund
		from 
				#sales_order_header_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk
		order by ol.order_id_bk, ol.order_line_id_bk

	merge into Landing.aux.sales_order_line_measures trg
	using
		(select order_line_id_bk, local_subtotal, local_shipping, local_discount, local_store_credit_used,	
			local_subtotal + local_shipping - local_discount - local_store_credit_used local_total_inc_vat
		from Landing.aux.sales_order_line_measures) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set
			trg.local_total_inc_vat = src.local_total_inc_vat;

	merge into Landing.aux.sales_order_line_measures trg
	using
		(select order_line_id_bk, 
			local_subtotal, local_subtotal_refund, local_shipping, local_shipping_refund, 
			local_discount, local_discount_refund, local_store_credit_used,	local_store_credit_used_refund, local_adjustment_refund, local_store_credit_given,
			(local_subtotal - local_subtotal_refund) + (local_shipping - local_shipping_refund) - (local_discount - local_discount_refund)  
				- (local_store_credit_used - local_store_credit_used_refund) - local_adjustment_refund 
				+ (local_store_credit_given - local_store_credit_used_refund) local_total_aft_refund_inc_vat
		from Landing.aux.sales_order_line_measures) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set
			trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat;

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_header_line_measures

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mag.lnd_stg_get_aux_sales_order_line_vat
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2017
-- Changed: 
	--	22-09-2017	Joseba Bayona Sola	Add VAT Logic for Old Stores (PO, GLUK, GLIE, GLIT, ASDA, EYEPLAN)
-- ==========================================================================================
-- Description: Reads data from Order Line: VAT Attributes
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_line_vat
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #sales_order_line_vat(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL, 
		countries_registered_code		varchar(10) NOT NULL,
		product_type_vat				varchar(255),
		vat_type_rate_code				char(1), 
		invoice_date					datetime,
	
		vat_rate						decimal(12, 4),
		prof_fee_rate					decimal(12, 4),

		local_total_inc_vat				decimal(12, 4),
		local_total_exc_vat				decimal(12, 4),
		local_total_vat					decimal(12, 4),
		local_total_prof_fee			decimal(12, 4),

		local_total_aft_refund_inc_vat	decimal(12, 4),
		local_total_aft_refund_exc_vat	decimal(12, 4),
		local_total_aft_refund_vat		decimal(12, 4),
		local_total_aft_refund_prof_fee	decimal(12, 4), 
		
		order_allocation_rate			decimal(12, 4))


	insert into #sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_aft_refund_inc_vat, 
		order_allocation_rate)

		select ol.order_line_id_bk, ol.order_id_bk, 
			oh_v.countries_registered_code, oh_v.invoice_date, 
			o_m.local_total_inc_vat, o_m.local_total_aft_refund_inc_vat, 
			o_m.order_allocation_rate
		from 
				#sales_order_header_vat oh_v
			inner join
				Landing.aux.sales_order_line_o_i_s_cr ol on oh_v.order_id_bk = ol.order_id_bk
			inner join
				Landing.aux.sales_order_line_measures o_m on ol.order_line_id_bk = o_m.order_line_id_bk

	-- Set product_type_vat
	merge into #sales_order_line_vat trg
	using 
		(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
			ol_v.product_type_vat
		from 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					case when (ppt.product_type_vat = 'Colour Plano' and ol_p.power_bk <> '+00.00') then 'Contact Lenses' else ppt.product_type_vat end product_type_vat
				from 
						#sales_order_line_vat ol_v
					inner join
						Landing.aux.sales_order_line_product ol_p on ol_v.order_line_id_bk = ol_p.order_line_id_bk
					inner join
						Landing.aux.prod_product_product_type_vat ppt on ol_p.product_id_bk = ppt.product_id) ol_v	
			inner join
				Landing.map.vat_product_type_vat_aud ptv on ol_v.product_type_vat = ptv.product_type_vat) src
	on trg.order_line_id_bk = src.order_line_id_bk
	when matched then
		update set 
			trg.product_type_vat = src.product_type_vat;

	-- (Fix Prof Fee): Set Vat Values -  (vat_type_rate, vat_rate) - Prof Fee Values (prof_fee_rate) based on countries_registered_code - product_type_vat
			merge into #sales_order_line_vat trg
			using 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					ol_v.countries_registered_code, ol_v.product_type_vat, ol_v.invoice_date,
					vr.vat_type_rate_code, 
					vr.vat_rate, pff.prof_fee_fixed_value
				from 
						(select order_line_id_bk, order_id_bk, 
							ol.countries_registered_code, 
							ol.countries_registered_code countries_registered_code_pf,
							product_type_vat, invoice_date, 
							local_total_inc_vat
						from 
								#sales_order_line_vat ol
							inner join
								(select distinct countries_registered_code
								from Landing.map.vat_prof_fee_fixed_value) pff on ol.countries_registered_code = pff.countries_registered_code) ol_v
					inner join
						Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
							and ol_v.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
					inner join
						Landing.map.vat_prof_fee_fixed_value_aud pff on ol_v.countries_registered_code_pf = pff.countries_registered_code 
							and ol_v.local_total_inc_vat between pff.order_value_min and pff.order_value_max) src
			on trg.order_line_id_bk = src.order_line_id_bk
			when matched then 
				update set
					trg.vat_type_rate_code = src.vat_type_rate_code, 
					trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_fixed_value;
						
	-- (Prof Fee Rate): Set Vat Values -  (vat_type_rate, vat_rate) - Prof Fee Values (prof_fee_rate) based on countries_registered_code - product_type_vat
			merge into #sales_order_line_vat trg
			using 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					ol_v.countries_registered_code, ol_v.product_type_vat, ol_v.invoice_date,
					vr.vat_type_rate_code, 
					vr.vat_rate, pfr.prof_fee_rate
				from 
						(select order_line_id_bk, order_id_bk, 
							ol.countries_registered_code, 
							ol.countries_registered_code countries_registered_code_pf,
							product_type_vat, invoice_date
						from 
								#sales_order_line_vat ol
							left join
								(select distinct countries_registered_code
								from Landing.map.vat_prof_fee_fixed_value) pff on ol.countries_registered_code = pff.countries_registered_code
						where pff.countries_registered_code is null) ol_v
					inner join
						Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
							and ol_v.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
					inner join
						Landing.map.vat_prof_fee_rate_aud pfr on ol_v.countries_registered_code_pf = pfr.countries_registered_code and ol_v.product_type_vat = pfr.product_type_vat 
							and ol_v.invoice_date between pfr.vat_time_period_start and pfr.vat_time_period_finish) src
			on trg.order_line_id_bk = src.order_line_id_bk
			when matched then 
				update set
					trg.vat_type_rate_code = src.vat_type_rate_code, 
					trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate;

	-- (Fix Prof Fee): Calculate Prof Fee Values (local_total_prof_fee - local_total_aft_refund_prof_fee)
			update #sales_order_line_vat
			set 
				local_total_prof_fee = prof_fee_rate * order_allocation_rate, 
				local_total_aft_refund_prof_fee = prof_fee_rate * order_allocation_rate
			where countries_registered_code in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)

			update #sales_order_line_vat
			set 
				prof_fee_rate = 0
			where countries_registered_code in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)

	-- (Prof Fee Rate): Calculate Prof Fee Values (local_total_prof_fee - local_total_aft_refund_prof_fee)
			update #sales_order_line_vat
			set 
				local_total_prof_fee = local_total_inc_vat * (prof_fee_rate / 100), 
				local_total_aft_refund_prof_fee = local_total_aft_refund_inc_vat * (prof_fee_rate / 100)
			where countries_registered_code not in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)

	-- Calculate VAT Values (local_total_vat, local_total_exc_vat  - local_total_aft_refund_vat, local_total_aft_refund_exc_vat)
	update #sales_order_line_vat
	set 
		local_total_vat = (local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate), 
		local_total_exc_vat = local_total_inc_vat - ((local_total_inc_vat - local_total_prof_fee) * vat_rate / (100 + vat_rate)), 

		local_total_aft_refund_vat = (local_total_aft_refund_inc_vat - local_total_aft_refund_prof_fee) * vat_rate / (100 + vat_rate), 
		local_total_aft_refund_exc_vat = local_total_aft_refund_inc_vat - ((local_total_aft_refund_inc_vat - local_total_aft_refund_prof_fee) * vat_rate / (100 + vat_rate))

	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_line_vat

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
		vat_rate, prof_fee_rate,
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		vat_percent, prof_fee_percent)

		select order_line_id_bk, order_id_bk, 
			countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
			vat_rate, prof_fee_rate,
			local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
			local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
			case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_exc_vat end vat_percent, 
			case when (local_total_inc_vat = 0) then 0 else local_total_prof_fee * 100 / local_total_inc_vat end prof_fee_percent
		from #sales_order_line_vat

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	drop table #sales_order_line_vat
	
	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_aux_sales_order_dim_fact
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-05-2017
-- Changed: 
	--	02-06-2017	Joseba Bayona Sola	Add SP calls to prepare Activity Fact Data (Sales - Other)
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	24-07-2017	Joseba Bayona Sola	Add VAT Logic: SP Call + INS countries_registered_code, product_type_vat
	--	23-11-2017	Joseba Bayona Sola	Add Marketing Channel Logic
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add market_id_bk, customer_unsubscribe_name_bk attributes
-- ==========================================================================================
-- Description: Executes Aux SPs and prepares data for Order Dim - Fact Tables
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_sales_order_dim_fact
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	exec mag.lnd_stg_get_aux_sales_order_header_o @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_header_o_i_s_cr @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_header_store_cust @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_header_dim_order @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_header_dim_mark @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec mag.lnd_stg_get_aux_sales_order_line_o_i_s_cr @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_sales_order_line_product @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec mag.lnd_stg_get_aux_sales_order_header_measures @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- exec mag.lnd_stg_get_aux_sales_order_line_measures @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun (Call inside lnd_stg_get_aux_sales_order_header_measures)


	exec mag.lnd_stg_get_aux_sales_order_header_vat @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	

	exec mag.lnd_stg_get_aux_sales_order_header_oh_pt @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun
	
		merge into Landing.aux.sales_order_header_dim_order trg
		using
			(select order_id_bk, product_type_oh_bk, order_qty_time, num_rep num_diff_product_type_oh
			from
				(select order_id_bk, product_type_oh_bk, 
					order_percentage, order_flag, order_qty_time, 
					count(*) over (partition by order_id_bk) num_rep, 
					dense_rank() over (partition by order_id_bk order by order_percentage desc, product_type_oh_bk) ord_rep
				from Landing.aux.sales_order_header_oh_pt) t
			where ord_rep = 1) src 
		on trg.order_id_bk = src.order_id_bk
		when matched then
			update set
				trg.product_type_oh_bk = src.product_type_oh_bk, 
				trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh;




	truncate table Landing.aux.sales_dim_order_header

	insert into Landing.aux.sales_dim_order_header (order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date,

		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing,
		customer_unsubscribe_name_bk,

		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma,
		product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os,

		countries_registered_code, 

		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent,
		num_lines, 
		idETLBatchRun)

		select oh.order_id_bk, 
			oh.invoice_id, oh.shipment_id, oh.creditmemo_id, 
			oh.order_no, oh.invoice_no, oh.shipment_no, oh.creditmemo_no, 
			oh.idCalendarOrderDate, oh.idTimeOrderDate, oh.order_date, 
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			oh.idCalendarRefundDate, oh.refund_date,

			oh_sc.store_id_bk, oh_sc.market_id_bk, oh_sc.customer_id_bk, oh_sc.shipping_address_id, oh_sc.billing_address_id,
			oh_sc.country_id_shipping_bk, oh_sc.region_id_shipping_bk, oh_sc.postcode_shipping, 
			oh_sc.country_id_billing_bk, oh_sc.region_id_billing_bk, oh_sc.postcode_billing,
			oh_sc.customer_unsubscribe_name_bk,

			oh_o.order_stage_name_bk, oh_o.order_status_name_bk, oh_o.adjustment_order, oh_o.order_type_name_bk, oh_o.status_bk, 
			oh_o.payment_method_name_bk, oh_o.cc_type_name_bk, oh_o.payment_id, oh_o.payment_method_code,
			oh_o.shipping_description_bk, oh_o.shipping_description_code, oh_o.telesales_admin_username_bk, oh_o.prescription_method_name_bk,
			oh_o.reminder_type_name_bk, oh_o.reminder_period_bk, oh_o.reminder_date, oh_o.reminder_sent, 
			oh_o.reorder_f, oh_o.reorder_date, oh_o.reorder_profile_id, oh_o.automatic_reorder,
			oh_o.order_source, oh_o.proforma, 
			oh_o.product_type_oh_bk, oh_o.order_qty_time, oh_o.num_diff_product_type_oh, oh_o.aura_product_p, 

			oh_o.referafriend_code, oh_o.referafriend_referer, 
			oh_o.postoptics_auto_verification, oh_o.presc_verification_method, oh_o.warehouse_approved_time,

			oh_m.channel_name_bk, oh_m.marketing_channel_name_bk, oh_m.publisher_id_bk, 
			oh_m.affilCode, oh_m.ga_medium, oh_m.ga_source, oh_m.ga_channel, oh_m.telesales_f, oh_m.sms_auto_f, oh_m.mutuelles_f, 
			oh_m.coupon_id_bk, oh_m.coupon_code, oh_m.applied_rule_ids,
			oh_m.device_model_name_bk, oh_m.device_browser_name_bk, oh_m.device_os_name_bk, 
			oh_m.ga_device_model, oh_m.ga_device_browser, oh_m.ga_device_os,

			oh_vat.countries_registered_code, 

			oh_me.price_type_name_bk, oh_me.discount_f,
			oh_me.total_qty_unit, oh_me.total_qty_unit_refunded, oh_me.total_qty_pack, oh_me.weight, 
			oh_me.local_subtotal, oh_me.local_shipping, oh_me.local_discount, oh_me.local_store_credit_used, 
			oh_me.local_total_inc_vat, oh_vat.local_total_exc_vat, oh_vat.local_total_vat, oh_vat.local_total_prof_fee, 
			oh_me.local_total_refund, oh_me.local_store_credit_given, oh_me.local_bank_online_given, 
			oh_me.local_subtotal_refund, oh_me.local_shipping_refund, oh_me.local_discount_refund, oh_me.local_store_credit_used_refund, oh_me.local_adjustment_refund, 
			oh_me.local_total_aft_refund_inc_vat, oh_me.local_total_aft_refund_inc_vat_2, oh_vat.local_total_aft_refund_exc_vat, oh_vat.local_total_aft_refund_vat, oh_vat.local_total_aft_refund_prof_fee, 
			oh_me.local_product_cost, oh_me.local_shipping_cost, oh_me.local_total_cost, oh_me.local_margin, 
			oh_me.local_to_global_rate, oh_me.order_currency_code, 
			oh_me.discount_percent, oh_vat.vat_percent, oh_vat.prof_fee_percent,
			oh_me.num_lines, 

			@idETLBatchRun idETLBatchRun
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.aux.sales_order_header_store_cust oh_sc	on oh.order_id_bk = oh_sc.order_id_bk
		inner join
			Landing.aux.sales_order_header_dim_order oh_o on oh.order_id_bk = oh_o.order_id_bk		
		inner join
			Landing.aux.sales_order_header_dim_mark oh_m on oh.order_id_bk = oh_m.order_id_bk
		inner join
			Landing.aux.sales_order_header_measures oh_me on oh.order_id_bk = oh_me.order_id_bk
		inner join
			Landing.aux.sales_order_header_vat oh_vat on oh.order_id_bk = oh_vat.order_id_bk 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	
	truncate table Landing.aux.sales_fact_order_line

	insert into Landing.aux.sales_fact_order_line(order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, 

		product_id_bk, 
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, 

		countries_registered_code, product_type_vat,

		order_status_name_bk, price_type_name_bk, discount_f,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
		num_erp_allocation_lines, 

		idETLBatchRun)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.invoice_id, ol.shipment_id, ol.creditmemo_id, 
			ol.order_no, ol.invoice_no, ol.shipment_no, ol.creditmemo_no,
			ol.invoice_line_id, ol.shipment_line_id, ol.creditmemo_line_id,  	
			ol.num_shipment_lines, ol.num_creditmemo_lines,
			ol.idCalendarInvoiceDate, ol.invoice_date, 
			ol.idCalendarShipmentDate, ol.idTimeShipmentDate, ol.shipment_date, 
			ol.idCalendarRefundDate, ol.refund_date, 
			ol.warehouse_id_bk, 

			ol_p.product_id_bk, 
			ol_p.base_curve_bk, ol_p.diameter_bk, ol_p.power_bk, ol_p.cylinder_bk, ol_p.axis_bk, ol_p.addition_bk, ol_p.dominance_bk, ol_p.colour_bk, ol_p.eye,
			ol_p.sku_magento, ol_p.sku_erp, 
			ol_p.glass_vision_type_bk, ol_p.order_line_id_vision_type, ol_p.glass_package_type_bk, ol_p.order_line_id_package_type,
			ol_p.aura_product_f, 

			ol_vat.countries_registered_code, ol_vat.product_type_vat,

			ol_me.order_status_name_bk, ol_me.price_type_name_bk, ol_me.discount_f,
			ol_me.qty_unit, ol_me.qty_unit_refunded, ol_me.qty_pack, ol_me.qty_time, ol_me.weight, 
			ol_me.local_price_unit, ol_me.local_price_pack, ol_me.local_price_pack_discount, 
			ol_me.local_subtotal, ol_me.local_shipping, ol_me.local_discount, ol_me.local_store_credit_used, 
			ol_me.local_total_inc_vat, ol_vat.local_total_exc_vat, ol_vat.local_total_vat, ol_vat.local_total_prof_fee, 
			ol_me.local_store_credit_given, ol_me.local_bank_online_given, 
			ol_me.local_subtotal_refund, ol_me.local_shipping_refund, ol_me.local_discount_refund, ol_me.local_store_credit_used_refund, ol_me.local_adjustment_refund, 
			ol_me.local_total_aft_refund_inc_vat, ol_vat.local_total_aft_refund_exc_vat, ol_vat.local_total_aft_refund_vat, ol_vat.local_total_aft_refund_prof_fee, 
			ol_me.local_product_cost, ol_me.local_shipping_cost, ol_me.local_total_cost, ol_me.local_margin, 
			ol_me.local_to_global_rate, ol_me.order_currency_code, 
			ol_me.discount_percent, ol_vat.vat_percent, ol_vat.prof_fee_percent, ol_vat.vat_rate, ol_vat.prof_fee_rate,
			ol_me.order_allocation_rate, ol_me.order_allocation_rate_aft_refund, ol_me.order_allocation_rate_refund, 
			ol_me.num_erp_allocation_lines, 

			@idETLBatchRun idETLBatchRun

		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product ol_p on ol.order_line_id_bk = ol_p.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_measures ol_me on ol.order_line_id_bk = ol_me.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_vat ol_vat on ol.order_line_id_bk = ol_vat.order_line_id_bk


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	
	exec mag.lnd_stg_get_aux_act_activity_sales @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec mag.lnd_stg_get_aux_act_activity_other @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

