

			select entity_id, increment_id, created_at, updated_at, 
				coupon_code, applied_rule_ids, charindex(',', applied_rule_ids), 
				base_discount_amount
			from Landing.mag.sales_flat_order_aud
			where charindex(',', applied_rule_ids) <> 0
			order by created_at

			select *
			from Warehouse.sales.dim_order_header_v
			where order_id_bk = 5515350

			select o.order_id_bk--, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) = 0 -- and len(o.applied_rule_ids) < 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
			order by o.applied_rule_ids, o.coupon_code



			select o.order_id_bk--, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from Landing.mag.sales_flat_order_aud o 
					where entity_id = 5515350 and
						(o.coupon_code is not null or o.applied_rule_ids is not null)
						-- and charindex(',', applied_rule_ids) = 0 
						and len(o.applied_rule_ids) < 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
			order by o.applied_rule_ids, o.coupon_code


		select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids
		from
				(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
				from Landing.mag.sales_flat_order_aud o 
				where entity_id = 5515350 and
					(o.coupon_code is not null or o.applied_rule_ids is not null)
					and charindex(',', applied_rule_ids) <> 0 
					--and len(o.applied_rule_ids) >= 5
					) o
			left join
				(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
					isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code
				from 
						Landing.mag.salesrule_coupon_aud src
					right join
						Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code
		order by o.applied_rule_ids, o.coupon_code