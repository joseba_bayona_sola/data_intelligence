use Landing
go 


drop procedure mag.lnd_stg_get_gen_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-03-2017
-- Changed: 
	--	09-02-2018	Joseba Bayona Sola	Add hour_name attribute
-- ==========================================================================================
-- Description: Reads data from Time in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Companies from store_company table
	select time_name time_name_bk, time_name, hour_name, hour, minute, day_part, @idETLBatchRun
	from Landing.map.gen_time
	order by time_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_gen_company
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Company in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_company
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Companies from store_company table
	select company_name company_name_bk, company_name, @idETLBatchRun
	from
		(select distinct company_name
		from Landing.map.gen_store_company) t
	order by company_name


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop view aux.mag_gen_store_v 
go

create view aux.mag_gen_store_v as
	select store_id, store_name, website_type, website_group, website, tld, code_tld, acquired, company_name
	from
		(select cs.store_id, cs.store_name, cs.website_type, cs.website_group, cs.website, cs.tld, 
			case 
				when cs.website_type = 'Core UK' and cs.tld <> '.co.uk' then 'gbp'
				when cs.website_type = 'White Label' then 'wl'
				else '' 
			end + cs.tld as code_tld, 'N' acquired, comp.company_name
		from
				(select cs.store_id, cs.name store_name, 
					case when (st.store_type is null) then st2.store_type else st.store_type end website_type,
					case when (wg.website_group is null) then wg2.website_group else wg.website_group end website_group,
					cs.website, cs.tld
				from 
						(select store_id, name, 
							case 
								when website like '%.co.uk' then '.co.uk'
								else right(website, charindex('.', reverse(website), 1)) 
							end as tld, website
						from 
							(select store_id, name, 
								left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end) as website	
							from Landing.mag.core_store) cs) cs
					left join
						Landing.map.gen_store_type_aud st on cs.name = st.store_name
					left join
						Landing.map.gen_store_type_aud st2 on cs.tld = st2.tld
					left join
						Landing.map.gen_website_group_aud wg on cs.name = wg.store_name
					left join
						Landing.map.gen_website_group_aud wg2 on charindex(wg2.name, cs.name) > 0
				where cs.store_id <> 0) cs
			left join
				Landing.map.gen_store_company_aud comp on cs.store_name = comp.store_name -- and comp.company_name <> 'Contact Lens Group BV'
		union
		select sf.store_id * -1, 
			case when (sf.store_id in (20, 21)) then sf.store_name + ' (A)' else sf.store_name end store_name, 
			sf.website_type, 
			case when (sf.store_id in (20, 21)) then sf.website_group + ' (A)' else sf.website_group end website_group, 
			case when (sf.store_id in (20, 21)) then sf.website + ' (A)' else sf.website end website,  
			sf.tld, 
			case 
				when sf.website_type = 'Core UK' and sf.tld <> '.co.uk' then 'gbp'
				when sf.website_type = 'White Label' then 'wl'
				else '' 
			end + sf.tld as code_tld, 'Y' acquired, comp.company_name
		from 
			(select store_id, store_name, website_type, website_group, website, 
				case 
					when website like '%.co.uk' then '.co.uk'
					else right(website, charindex('.', reverse(website), 1)) 
				end as tld
			from 
					(select store_id, store_name, store_type website_type, website_group, 
						left(store_name, case when charindex('/', store_name, 1) = 0 then len(store_name) else charindex('/', store_name, 1) - 1 end) as website
					from Landing.migra_pr.dw_stores_full) sf) sf
				left join
					Landing.map.gen_store_company_aud comp 
						on case when (sf.store_id in (20, 21)) then sf.store_name + ' (A)' else sf.store_name end = comp.store_name -- and comp.company_name <> 'Vision Direct BV'
		union
		select ms.store_id * -1 store_id, ms.store_name, ms.website_type, ms.website_group, ms.website, ms.tld, ms.tld code_tld, 
			'Y' acquired, comp.company_name
		from
			(select rank() over (order by store_name) store_id,
				store_name, store_type website_type, website_group, store_name website, 
				case 
					when store_name like '%.co.uk' then '.co.uk'
					else right(store_name, charindex('.', reverse(store_name), 1)) 
				end as tld
			from Landing.map.gen_masterlens_store_aud) ms
		inner join
			Landing.map.gen_store_company_aud comp on ms.store_name = comp.store_name
		union	
		select store_id, ms.store_name, ms.website_type, ms.website_group, ms.website, ms.tld, ms.tld code_tld, 
			'Y' acquired, comp.company_name
		from
			(select store_id,
				store_name, store_type website_type, website_group, store_name website, 
				case 
					when store_name like '%.co.uk' then '.co.uk'
					else right(store_name, charindex('.', reverse(store_name), 1)) 
				end as tld
			from Landing.map.gen_migrate_store_aud) ms
		inner join
			Landing.map.gen_store_company_aud comp on ms.store_name = comp.store_name) stores
go

drop procedure mag.lnd_stg_get_gen_store
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2017
-- Changed: 
	--	31-07-2017	Joseba Bayona Sola	Replace Store Query with a view in order to reuse code
	--	09-01-2018	Joseba Bayona Sola	Modified view to allocate correctly company_name
-- ==========================================================================================
-- Description: Reads data from Store in different Landind tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_store
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: UNION of stores in Magento (Map Tables Join) + DW_Proforma
	select store_id, store_name, website_type, website_group, website, tld, code_tld, acquired, company_name, @idETLBatchRun
	from Landing.aux.mag_gen_store_v
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.lnd_stg_get_gen_country
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Country in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_country
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Data from gen_countries mapping table // Add missing countries from customer_address_entity_flat - sales_flat_order_address
	select country_code country_id_bk, country_code, country_name, 
		country_zone, country_type, country_continent, country_state, @idETLBatchRun
	from Landing.map.gen_countries

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

drop procedure mag.lnd_stg_get_gen_region
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2017
-- Changed: 
	--	23-08-2017	Joseba Bayona Sola	Filter and only return those records where region_name strig is not null
-- ==========================================================================================
-- Description: Reads data from Region in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_region
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select region_id region_id_bk, region region_name, country_id country_id_bk, @idETLBatchRun
	from
		(select country_id, region_id, region, 
			count(*) num, max(count(*)) over (partition by region_id) max_rep,
			rank() over (partition by region_id order by region) rank_rep
		from
			(select entity_id, country_id, region_id, region
			from Landing.mag.customer_address_entity_flat
			where (region_id is not null and region_id <> 0)
				and country_id in ('US', 'CA', 'DE', 'AT', 'CH', 'ES', 'FR', 'RO', 'FI', 'LV', 'IT')
			union
			select entity_id, country_id, region_id, region
			from Landing.mag.sales_flat_order_address
			where (region_id is not null and region_id <> 0)
				and country_id in ('US', 'CA', 'DE', 'AT', 'CH', 'ES', 'FR', 'RO', 'FI', 'LV', 'IT')) t
		group by country_id, region_id, region) t
	-- where num = max_rep
	where rank_rep = 1
		and region is not null
	order by region_id, region, country_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_gen_market
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-09-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Market in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_market
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Data from gen_countries mapping table // Add missing countries from customer_address_entity_flat - sales_flat_order_address
	select market_id market_id, market_name market_name, @idETLBatchRun
	from
		(select market_id, max(market_name) market_name
		from Landing.map.gen_market
		group by market_id) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_gen_customer_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_customer_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Data from gen_customer_type mapping table 
	select customer_type customer_type_name_bk, customer_type customer_type_name, description, @idETLBatchRun
	from
		(select customer_type, max(description) description
		from Landing.map.gen_customer_type
		group by customer_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_gen_customer_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_customer_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Data from gen_customer_status mapping table 
	select customer_status customer_status_name_bk, customer_status customer_status_name, description, @idETLBatchRun
	from
		(select customer_status, max(description) description
		from Landing.map.gen_customer_status
		group by customer_status) t


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_gen_customer_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer Unsubscribe in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_customer_unsubscribe
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Data from gen_customer_status mapping table 
	select customer_unsubscribe customer_unsubscribe_name_bk, customer_unsubscribe customer_unsubscribe_name, description, @idETLBatchRun
	from
		(select customer_unsubscribe, max(description) description
		from Landing.map.gen_customer_unsubscribe
		group by customer_unsubscribe) t


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.lnd_stg_get_gen_customer
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	04-09-2017	Joseba Bayona Sola	Add market_id_bk attribute
	--	22-01-2018	Joseba Bayona Sola	Modification on migrate_customer_id to check if can convert from varchar to bigint
	--	08-03-2018	Joseba Bayona Sola	Modify Market Logic
	--	12-03-2018	Joseba Bayona Sola	Add customer_unsubscribe_name_bk attribute
	--	13-03-2018	Joseba Bayona Sola	Change FINAL SELECT logic creating aux tables for dim_customer
-- ==========================================================================================
-- Description: Reads data from Customer in a Landing tables and inserts data in Staging table
	-- Customer Base to Update: Customer Entity Table Records + DotMailer Cust (NO YET) + Text Mess Cust (NO YET) + Orders N-U with their Cust (NO YET) 
	-- Need All Customers data from Customer Entity --> Aud table
-- ==========================================================================================

create procedure mag.lnd_stg_get_gen_customer
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #customer_data(
		customer_id_bk								int NOT NULL, 
		customer_email								varchar(255),
		created_at									datetime, 
		updated_at									datetime, 
		store_id_bk									int NOT NULL, 
		market_id_bk								int, 
		customer_type_name_bk						varchar(50) NOT NULL, 
		customer_status_name_bk						varchar(50),
		prefix										varchar(255),
		first_name									varchar(255),
		last_name									varchar(255),
		dob											datetime2,
		gender										char(1),
		language									char(2),
		phone_number								varchar(255),
		street										varchar(1000),
		city										varchar(255),
		postcode									varchar(255),
		region										varchar(255),
		region_id_bk								int, 
		country_id_bk								char(2), 
		postcode_s									varchar(255),
		country_id_s_bk								char(2), 
		referafriend_code							varchar(255),
		days_worn_info								varchar(255),
		migrate_customer_f							char(1) NOT NULL,
		migrate_customer_store						varchar(255),
		migrate_customer_id							bigint)

	-- Creates TEMP Table - #customer_data_entity
	-- Customer related important data from customer_entity_flat
	-- Transformations for Gender - Language - Days Worn - Migrate Customer
	select c.entity_id customer_id_bk, 
		c.email customer_email, c.created_at, c.updated_at, c.store_id store_id_bk, 
		'RETAIL' customer_type_name_bk, NULL customer_status_name_bk,
		c.prefix, c.firstname first_name, c.lastname last_name, 
		c.dob, 
		case 
			when (opt_gender.value = 'Male') then 'M'
			when (opt_gender.value = 'Female') then 'F'
			else NULL 
		end gender, 
		UPPER(LEFT(ccds.value, 2)) language,
		c.cus_phone phone_number, 
		c.default_billing, c.default_shipping, 
		c.referafriend_code, opt_days_worn.value days_worn_info, 
		ISNULL(mcs.migrate_customer_f, 'N') migrate_customer_f, mcs.migrate_customer_store, 
		case 
			when (old_customer_id is not null and old_customer_id <> '' and old_customer_id NOT LIKE '%[^0-9]%') then cast(old_customer_id as bigint)
			when ((old_customer_id is not null and old_customer_id LIKE '%[^0-9]%' and CHARINDEX(old_access_cust_no, old_customer_id, 1) = 1) and
				isnumeric(SUBSTRING(old_customer_id, len(old_access_cust_no) + 1, len(old_customer_id) - len(old_access_cust_no))) = 1)
			 then 
				cast(SUBSTRING(old_customer_id, len(old_access_cust_no) + 1, len(old_customer_id) - len(old_access_cust_no)) as bigint)
			else null
		end migrate_customer_id
	into 
		#customer_data_entity 
	from 
			Landing.mag.customer_entity_flat c
		left join
			Landing.mag.eav_attribute_option_value opt_gender on c.gender = opt_gender.option_id and opt_gender.value in ('Male', 'Female')
		left join
			Landing.mag.eav_attribute_option_value opt_days_worn on c.days_worn_info = opt_days_worn.option_id 
		left join
			Landing.aux.mag_core_config_data_store ccds on c.store_id = ccds.store_id and ccds.path = 'general/locale/code' 
		left join
			Landing.map.gen_migrate_customer_store mcs on c.old_access_cust_no = mcs.migrate_store_code

	-- Creates TEMP Table - #customer_data
	-- Previous Customer Data + Billing - Shipping Address Data
	insert into #customer_data(customer_id_bk, customer_email,
		created_at, updated_at,
		store_id_bk, customer_type_name_bk, customer_status_name_bk,
		prefix, first_name, last_name, 
		dob, gender, language, 
		phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
		postcode_s, country_id_s_bk, 
		referafriend_code, days_worn_info, 
		migrate_customer_f, migrate_customer_store, migrate_customer_id) 

		select c.customer_id_bk, c.customer_email,
			c.created_at, c.updated_at,
			c.store_id_bk, c.customer_type_name_bk, c.customer_status_name_bk,
			c.prefix, c.first_name, c.last_name, 
			c.dob, c.gender, c.language, 
			c.phone_number, ad_b.street, ad_b.city, ad_b.postcode, ad_b.region, ad_b.region_id region_id_bk, c_b.country_code country_id_bk,
			ad_s.postcode, c_s.country_code country_id_s_bk,
			c.referafriend_code, c.days_worn_info, 
			c.migrate_customer_f, c.migrate_customer_store, c.migrate_customer_id

		from 
			#customer_data_entity  c
		left join
			Landing.mag.customer_address_entity_flat_aud ad_b on c.default_billing = ad_b.entity_id
		left join
			Landing.mag.customer_address_entity_flat_aud ad_s on c.default_shipping = ad_s.entity_id
		left join
			Landing.map.gen_countries c_b on ad_b.country_id = c_b.country_code
		left join
			Landing.map.gen_countries c_s on ad_s.country_id = c_s.country_code

	-- Update market_id_bk
	merge #customer_data trg
	using 
		(select cd.customer_id_bk, cd.store_id_bk, cd.country_id_s_bk, 
			case 
				when smr1.market_id is not null then smr1.market_id 
				when smr2.market_id is not null then smr2.market_id
				when smr3.market_id is not null then smr3.market_id
				else 1
			end market_id_bk
		from 
				#customer_data cd
			left join
				Landing.map.gen_store_market_rel smr1 on cd.store_id_bk = 20 and cd.store_id_bk = smr1.store_id and cd.country_id_s_bk = smr1.country		
			left join
				Landing.map.gen_store_market_rel smr2 on cd.store_id_bk <> 20 and cd.store_id_bk = smr2.store_id 
			left join
				Landing.map.gen_store_market_rel smr3 on smr1.market_id is null and smr2.market_id is null and cd.store_id_bk = smr3.store_id and smr3.country is null and cd.country_id_s_bk is not null) src
	on trg.customer_id_bk = src.customer_id_bk
	when matched then
		update set trg.market_id_bk = src.market_id_bk;

	--merge #customer_data trg
	--using 
	--	(select cd.customer_id_bk, cd.store_id_bk, cd.country_id_s_bk, 
	--		isnull(smr1.market_id, isnull(smr2.market_id, 1)) market_id_bk
	--	from 
	--			#customer_data cd
	--		left join
	--			Landing.map.gen_store_market_rel smr1 on cd.country_id_s_bk = smr1.country		
	--		left join
	--			Landing.map.gen_store_market_rel smr2 on cd.store_id_bk = smr2.store_id) src
	--on trg.customer_id_bk = src.customer_id_bk
	--when matched then
	--	update set trg.market_id_bk = src.market_id_bk;

	-- Customer - Unsubscribe Magento: SP inserting data into Auxiliary Table
	exec mag.lnd_stg_get_aux_gen_customer_unsubscribe_magento @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Customer - Unsubscribe DotMailer: SP inserting data into Auxiliary Table
	exec mag.lnd_stg_get_aux_gen_customer_unsubscribe_dotmailer @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Customer - Unsubscribe Text Message: SP inserting data into Auxiliary Table
	exec mag.lnd_stg_get_aux_gen_customer_unsubscribe_textmessage @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Customer - Reminder: SP inserting data into Auxiliary Table
	exec mag.lnd_stg_get_aux_gen_customer_reminder @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Customer - Reorder: SP inserting data into Auxiliary Table
	exec mag.lnd_stg_get_aux_gen_customer_reorder @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- INSERT STATEMENT: XXXXX
	truncate table Landing.aux.gen_dim_customer

	insert into Landing.aux.gen_dim_customer(customer_id_bk, customer_email,
		created_at, updated_at,
		store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
		prefix, first_name, last_name, 
		dob, gender, language, 
		phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
		postcode_s, country_id_s_bk, 
		customer_unsubscribe_name_bk, customer_unsubscribe_d,
		unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
		unsubscribe_text_message_f, unsubscribe_text_message_d, 
		reminder_f, reminder_type_name_bk, reminder_period_bk, 
		reorder_f, 
		referafriend_code, days_worn_info, 
		migrate_customer_f, migrate_customer_store, migrate_customer_id, 
		idETLBatchRun)

		select c.customer_id_bk, c.customer_email,
			c.created_at, c.updated_at,
			c.store_id_bk, c.market_id_bk, c.customer_type_name_bk, c.customer_status_name_bk,
			c.prefix, c.first_name, c.last_name, 
			c.dob, c.gender, c.language, 
			c.phone_number, c.street, c.city, c.postcode, c.region, c.region_id_bk, c.country_id_bk, 
			c.postcode_s, c.country_id_s_bk, 
			case when (cum.unsubscribe_mark_email_magento_f = 'Y' or cud.unsubscribe_mark_email_dotmailer_f = 'Y') then 'Y' else 'N' end customer_unsubscribe_name_bk, 
			case when (isnull(cum.unsubscribe_mark_email_magento_d, cud.unsubscribe_mark_email_dotmailer_d) <= isnull(cud.unsubscribe_mark_email_dotmailer_d, cum.unsubscribe_mark_email_magento_d)) 
				then cum.unsubscribe_mark_email_magento_d else cud.unsubscribe_mark_email_dotmailer_d end customer_unsubscribe_d,

			cum.unsubscribe_mark_email_magento_f, cum.unsubscribe_mark_email_magento_d, cud.unsubscribe_mark_email_dotmailer_f, cud.unsubscribe_mark_email_dotmailer_d, 
			cutm.unsubscribe_text_message_f, cutm.unsubscribe_text_message_d, 
			cur.reminder_f, cur.reminder_type_name_bk, cur.reminder_period_bk, 
			curo.reorder_f, 
			c.referafriend_code, c.days_worn_info, 
			c.migrate_customer_f, c.migrate_customer_store, c.migrate_customer_id, 
			@idETLBatchRun
		from 
				#customer_data c
			inner join
				Landing.aux.gen_customer_unsubscribe_magento cum on c.customer_id_bk = cum.customer_id_bk
			inner join
				Landing.aux.gen_customer_unsubscribe_dotmailer cud on c.customer_id_bk = cud.customer_id_bk
			inner join
				Landing.aux.gen_customer_unsubscribe_textmessage cutm on c.customer_id_bk = cutm.customer_id_bk
			inner join
				Landing.aux.gen_customer_reminder cur on c.customer_id_bk = cur.customer_id_bk
			inner join
				Landing.aux.gen_customer_reorder curo on c.customer_id_bk = curo.customer_id_bk
	
	-- SELECT STATEMENT: XXXXX
	select customer_id_bk, customer_email,
		created_at, updated_at,
		store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
		prefix, first_name, last_name, 
		dob, gender, language, 
		phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
		postcode_s, country_id_s_bk, 
		customer_unsubscribe_name_bk,
		unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
		unsubscribe_text_message_f, unsubscribe_text_message_d, 
		reminder_f, reminder_type_name_bk, reminder_period_bk, 
		reorder_f, 
		referafriend_code, days_worn_info, 
		migrate_customer_f, migrate_customer_store, migrate_customer_id, 
		idETLBatchRun
	from Landing.aux.gen_dim_customer


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_magento
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Relates Customers with their Unsubscribe Magento Choice
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_magento
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.gen_customer_unsubscribe_magento

	-- INSERT STATEMENT: XXXX
	insert into Landing.aux.gen_customer_unsubscribe_magento (customer_id_bk, unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d)
		select entity_id customer_id_bk, 
			case  
				when unsubscribe_all in ('1', 'Yes') then 'Y' 
				else 'N' 
			end unsubscribe_mark_email_magento_f, 
			case 
				when unsubscribe_all in ('1', 'Yes') then convert(int, (CONVERT(VARCHAR(8), isnull(unsubscribe_all_date, updated_at), 112))) 
				else NULL
			end unsubscribe_mark_email_magento_d
		from Landing.mag.customer_entity_flat
		order by customer_id_bk desc


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_dotmailer
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Relates Customers with their Unsubscribe Dotmailer Choice
	-- Currently we have NO data from DotMailer: Records will be set to unsubscribe = 'No'
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_dotmailer
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.gen_customer_unsubscribe_dotmailer

	-- INSERT STATEMENT: XXXX
	insert into Landing.aux.gen_customer_unsubscribe_dotmailer (customer_id_bk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d)
		select entity_id customer_id_bk, 'N' unsubscribe_mark_email_dotmailer_f, NULL unsubscribe_mark_email_dotmailer_d
		from Landing.mag.customer_entity_flat


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_textmessage
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Relates Customers with their Unsubscribe Text Message Choice
	-- Currently we have NO data from Text Message: Records will be set to unsubscribe = 'No'
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_gen_customer_unsubscribe_textmessage
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.gen_customer_unsubscribe_textmessage

	-- INSERT STATEMENT: XXXX
	insert into Landing.aux.gen_customer_unsubscribe_textmessage (customer_id_bk, unsubscribe_text_message_f, unsubscribe_text_message_d)
		select entity_id customer_id_bk, 'N' unsubscribe_text_message_f, NULL unsubscribe_text_message_d
		from Landing.mag.customer_entity_flat


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_aux_gen_customer_reminder
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Relates Customers with their Reminder Choice
	-- Take the last order for the Customer (Need to query aud order table) + Filter NOT cancelled / refunded in status
	-- Take from the last order reminder_type, reminder_period 
	-- Last Order: Count and Rank Orders and Take Last One
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_gen_customer_reminder
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.gen_customer_reminder

	-- INSERT STATEMENT: XXXX
	insert into Landing.aux.gen_customer_reminder (customer_id_bk, reminder_f, reminder_type_name_bk, reminder_period_bk)
		select entity_id customer_id_bk, 
			case 
				when (reminder_type in ('both', 'email', 'sms', 'reorder')) then 'Y'
				else 'N'
			end reminder_f,
			reminder_type reminder_type_name_bk, reminder_period reminder_period_bk
		from
			(select c.entity_id, 
				o.entity_id order_id, o.created_at, o.reminder_type, o.reminder_period, o.reminder_date, 
				count(*) over (partition by c.entity_id) num_rows_cust, 
				rank() over (partition by c.entity_id order by o.created_at, o.entity_id) rank_rows_cust
			from 
					Landing.mag.customer_entity_flat c
				left join 
					(select *
					from Landing.mag.sales_flat_order_aud 
					where status not in ('cancel', 'close')) o on c.entity_id = o.customer_id) t
		where num_rows_cust = rank_rows_cust


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_aux_gen_customer_reorder
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Relates Customers with their Reorder Choice
	-- Take All Customers that have a Reordering Profile based on the Reordering Profile Dates
	-- Join with Customer Base
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_gen_customer_reorder
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.gen_customer_reorder

	-- INSERT STATEMENT: XXXX
	insert into Landing.aux.gen_customer_reorder (customer_id_bk, reorder_f)
		select c.entity_id customer_id_bk, 
			case when (rp.customer_id is not null) then 'Y' else 'N' end reorder_f
		from 
				Landing.mag.customer_entity_flat c
			left join
				(select customer_id, count(*) num
				from Landing.mag.po_reorder_profile_aud
				where enddate > startdate and enddate > getutcdate()
				group by customer_id) rp on c.entity_id = rp.customer_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

