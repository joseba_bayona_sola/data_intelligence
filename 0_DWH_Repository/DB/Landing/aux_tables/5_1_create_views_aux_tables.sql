
use Landing
go 

----------------------- mag_catalog_product_price ----------------------------

create view aux.mag_catalog_product_price_aud_v as
	select record_type, count(*) over (partition by store_id, product_id, price_type, qty) num_records, 
		store_id, store_name, product_id, product_name, product_lifecycle, 
		value, qty, min_qty_prod, num_packs_tier_price, 
		total_price, pack_price, disc_percent, max_pack_price, 
		promo_key, price_type, pla_type,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from 
		(select 'N' record_type, store_id, store_name, product_id, product_name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, disc_percent, max_pack_price, 
			promo_key, price_type, pla_type,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from aux.mag_catalog_product_price_aud
		union
		select 'H' record_type, store_id, store_name, product_id, product_name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, disc_percent, max_pack_price, 
			promo_key, price_type, pla_type,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from aux.mag_catalog_product_price_aud_hist) t 
go 




drop view aux.act_customer_cr_reg_v
go 

create view aux.act_customer_cr_reg_v as
	select customer_id_bk, 
		case 
			when (store_id_first_all is null) then 1
			when (store_id_first_all is not null and store_id_last_non_vd is null) then 2
			when (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) then 3
			when (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) then 4
			else 5
		end cust_type,
		email, crreg.store_id, created_at, 
		old_access_cust_no, store_id_oacn, 
		num_dist_websites, 
		store_id_first_all, first_all_date, store_id_first_vd, first_vd_date, 
		store_id_first_non_vd, first_non_vd_date, store_id_last_non_vd, last_non_vd_date, non_vd_customer_status_at_migration_date,
		store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date, migration_date, 
		store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date, migration_date_oacn, 
		store_id_bk_cr, s1.store_name store_name_cr, create_date, 
		store_id_bk_reg, s2.store_name store_name_reg, register_date
	from 
			Landing.aux.act_customer_cr_reg crreg
		inner join
			Landing.aux.mag_gen_store_v s1 on crreg.store_id_bk_cr = s1.store_id
		inner join
			Landing.aux.mag_gen_store_v s2 on crreg.store_id_bk_reg = s2.store_id
go