use Landing
go 

----------------------- mag_catalog_product_price ----------------------------

select * 
from aux.mag_catalog_product_price_aud_v
where num_records > 1
order by store_id, product_id, price_type, qty, pla_type, idETLBatchRun

select * 
from aux.mag_catalog_product_price_aud_v
where idETLBatchRun <> 1
order by store_id, product_id, price_type, qty, pla_type, idETLBatchRun

