
select top 1000 count(*)
from Landing.aux.act_customer_cr_reg

select top 1000 count(*)
from Warehouse.gen.dim_customer

select top 1000 count(*)
from Warehouse.act.fact_customer_signature

select top 1000 idETLBatchRun_ins, count(*)
from Landing.aux.act_customer_cr_reg
group by idETLBatchRun_ins
order by idETLBatchRun_ins desc

---------------------------------------------

select top 1000 *
from Landing.aux.act_customer_cr_reg
order by customer_id_bk desc

select top 1000 *
from Landing.aux.act_customer_cr_reg_v
order by customer_id_bk desc

select count(*)
from Landing.aux.act_customer_cr_reg_v

select num_dist_websites, count(*)
from Landing.aux.act_customer_cr_reg_v
group by num_dist_websites
order by num_dist_websites

select cust_type, count(*)
from Landing.aux.act_customer_cr_reg_v
group by cust_type
order by cust_type

----------------------------------------------------------

select top 1000 s.store_name, store_name_cr, count(*), sum(count(*)) over ()
from 
		Landing.aux.act_customer_cr_reg_v crreg
	left join
		Landing.aux.mag_gen_store_v s on crreg.store_id_first_all = s.store_id
where s.store_name is not null and s.store_name <> store_name_cr
	-- and store_name_cr not in ('lensway.co.uk', 'lenson.co.uk', 'lensway.nl', 'lenson.nl', 'yourlenses.nl')
group by s.store_name, store_name_cr
-- order by s.store_name, store_name_cr
order by store_name_cr, s.store_name
-- order by count(*) desc

---------------------------------------- CREATE STORE (Types 1, 2, 4: When set the store_id check depending on create_date) 
	-- Change masterlens migration_date on act_website_migrate_date_aud

	-- modify store_id depending on create date putting GetLenses

	select s.website_group, year(create_date), count(*)
	from 
			Landing.aux.act_customer_cr_reg_v crreg
		inner join
			Landing.aux.mag_gen_store_v s on crreg.store_id_bk_cr = s.store_id
	where s.website_group = 'Getlenses' and create_date > '2014-12-01'
	-- where s.website_group = 'Visiondirect' and create_date < '2014-05-01'
		and store_id_first_all is not null
	group by s.website_group, year(create_date)
	order by s.website_group, year(create_date)

	select crreg.*
	from 
			Landing.aux.act_customer_cr_reg_v crreg
		inner join
			Landing.aux.mag_gen_store_v s on crreg.store_id_bk_cr = s.store_id
	where s.website_group = 'Visiondirect' and create_date < '2014-05-01'
		and year(create_date) = 2009
		and store_id_first_all is not null
	order by crreg.store_id_first_all

---------------------------------------- REGISTER DATE (Resolve With Right create store id)

select store_name_reg, year(register_date), count(*)
from Landing.aux.act_customer_cr_reg_v
group by store_name_reg, year(register_date)
order by store_name_reg, year(register_date)

select top 1000 
	count(*) over (), count(*) over (partition by cust_type),
	*
from Landing.aux.act_customer_cr_reg_v
where store_name_reg = 'visiondirect.co.uk' and year(register_date) < 2014
	and cust_type = 3

select cust_type, count(*)
from Landing.aux.act_customer_cr_reg_v
where store_name_reg = 'visiondirect.co.uk' and year(register_date) < 2014
group by cust_type
order by cust_type

---------------------------------------- MIGR CUSTOMERS -------------------------

select cust_type, store_name_cr, count(*)
from Landing.aux.act_customer_cr_reg_v
where store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
group by cust_type, store_name_cr
order by cust_type, store_name_cr

-- Type 1: No problem!

-- Type 2: Ones with status = 'ACTIVE' are the "WRONG" ones (Should not being MARKED as MIGR)

select *
from Landing.aux.act_customer_cr_reg_v
where cust_type = 2
	and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
	-- and (old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) 
	-- and customer_status_at_oacn_date is null 
	and customer_status_at_oacn_date = 'ACTIVE'
order by store_name_cr, first_vd_date

	select store_name_cr, count(*)
	from Landing.aux.act_customer_cr_reg_v
	where cust_type = 2
		and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
		and customer_status_at_oacn_date = 'ACTIVE'
	group by store_name_cr
	order by store_name_cr

-- Type 3: 

	-- Should not be MARKED as MIGR (Last Purchase in Diff Migr Store /// LAPSED in Migr Store)
	select *
	from Landing.aux.act_customer_cr_reg_v
	where cust_type = 3
		and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
		-- and (old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) 
		-- and store_id_last_non_vd <> store_id_bk_cr
		and store_id_last_non_vd = store_id_bk_cr and non_vd_customer_status_at_migration_date = 'LAPSED'
	order by store_name_cr, last_non_vd_date

		select store_name_cr, count(*)
		from Landing.aux.act_customer_cr_reg_v
		where cust_type = 3
			and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
			-- and (old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) 
			-- and store_id_last_non_vd <> store_id_bk_cr
			and store_id_last_non_vd = store_id_bk_cr and non_vd_customer_status_at_migration_date = 'LAPSED'
		group by store_name_cr
		order by store_name_cr


	-- Should be MARKED as MIGR (Last Purchase in Migr and ACTIVE)
	select *
	from Landing.aux.act_customer_cr_reg_v
	where cust_type = 3
		and store_name_cr not in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
		and store_id_last_non_vd in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))
		and non_vd_customer_status_at_migration_date = 'ACTIVE'
	order by store_id_last_non_vd, last_non_vd_date

		select store_id_last_non_vd, count(*)
		from Landing.aux.act_customer_cr_reg_v
		where cust_type = 3
			and store_name_cr not in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
			and store_id_last_non_vd in (select store_id
										from Landing.map.act_website_old_access_cust_no_aud
										where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))
			and non_vd_customer_status_at_migration_date = 'ACTIVE'
		group by store_id_last_non_vd
		order by store_id_last_non_vd

-- Type 4: 

	-- Should not be MARKED as MIGR (Last Purchase in Diff Migr Store /// ACTIVE IN VD /// LAPSED in Migr Store)
	select *
	from Landing.aux.act_customer_cr_reg_v
	where cust_type = 4
		and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
		-- and (old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) 
		-- and store_id_last_non_vd <> store_id_bk_cr
		-- and store_id_last_non_vd = store_id_bk_cr and customer_status_at_migration_date = 'ACTIVE'
		and store_id_last_non_vd = store_id_bk_cr and non_vd_customer_status_at_migration_date = 'LAPSED'
	order by store_id_last_non_vd, last_non_vd_date

		select store_name_cr, count(*)
		from Landing.aux.act_customer_cr_reg_v
		where cust_type = 4
			and store_name_cr in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
			-- and (old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) 
			-- and store_id_last_non_vd <> store_id_bk_cr
			-- and store_id_last_non_vd = store_id_bk_cr and customer_status_at_migration_date = 'ACTIVE'
			and store_id_last_non_vd = store_id_bk_cr and customer_status_at_migration_date = 'LAPSED' and non_vd_customer_status_at_migration_date = 'LAPSED'
		group by store_name_cr
		order by store_name_cr

	-- Should be MARKED as MIGR (Last Purchase in Migr and ACTIVE and in VD as LAPSED)
	select *
	from Landing.aux.act_customer_cr_reg_v
	where cust_type = 4
		and store_name_cr not in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
		and store_id_last_non_vd in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))
		and non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED'
	order by store_id_last_non_vd, last_non_vd_date

		select store_id_last_non_vd, count(*)
		from Landing.aux.act_customer_cr_reg_v
		where cust_type = 4
			and store_name_cr not in ('lensway.co.uk', 'lenson.co.uk', 'lenson.nl', 'lensway.nl', 'yourlenses.nl')
			and store_id_last_non_vd in (select store_id
										from Landing.map.act_website_old_access_cust_no_aud
										where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))
			and non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED'
		group by store_id_last_non_vd
		order by store_id_last_non_vd
