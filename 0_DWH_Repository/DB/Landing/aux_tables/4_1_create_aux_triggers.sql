use Landing
go 

	drop trigger aux.trg_mag_catalog_product_price;
	go 

	create trigger aux.trg_mag_catalog_product_price on aux.mag_catalog_product_price
	after insert
	as
	begin
		set nocount on

		merge into aux.mag_catalog_product_price_aud with (tablock) as trg
		using inserted src
			on (trg.store_id = src.store_id and trg.product_id = src.product_id and trg.price_type = src.price_type and trg.qty = src.qty and trg.pla_type = src.pla_type)
		when matched and not exists
			(select 
				isnull(trg.product_lifecycle, ' '), 
				isnull(trg.value, 0), isnull(trg.min_qty_prod, 0), isnull(trg.num_packs_tier_price, 0), 
				isnull(trg.total_price, 0), isnull(trg.pack_price, 0), isnull(trg.disc_percent, 0), isnull(trg.max_pack_price, 0), 
				isnull(trg.promo_key, ' ')
			intersect
			select 
				isnull(src.product_lifecycle, ' '), 
				isnull(src.value, 0), isnull(src.min_qty_prod, 0), isnull(src.num_packs_tier_price, 0), 
				isnull(src.total_price, 0), isnull(src.pack_price, 0), isnull(src.disc_percent, 0), isnull(src.max_pack_price, 0), 
				isnull(src.promo_key, ' ')) 
			
			then
				update set
					trg.product_lifecycle = src.product_lifecycle, 
					trg.value = src.value, trg.min_qty_prod = src.min_qty_prod, trg.num_packs_tier_price = src.num_packs_tier_price, 
					trg.total_price = src.total_price, trg.pack_price = src.pack_price, trg.disc_percent = src.disc_percent, trg.max_pack_price = src.max_pack_price, 
					trg.promo_key = src.promo_key, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (store_id, store_name, product_id, product_name, product_lifecycle, 
					value, qty, min_qty_prod, num_packs_tier_price, 
					total_price, pack_price, disc_percent, max_pack_price, 
					promo_key, price_type, pla_type,
					idETLBatchRun)
					
					values (src.store_id, src.store_name, src.product_id, src.product_name, src.product_lifecycle, 
						src.value, src.qty, src.min_qty_prod, src.num_packs_tier_price, 
						src.total_price, src.pack_price, src.disc_percent, src.max_pack_price, 
						src.promo_key, src.price_type, src.pla_type,
						src.idETLBatchRun);
	end; 
	go


	drop trigger aux.trg_mag_catalog_product_price_aud;
	go

	create trigger aux.trg_mag_catalog_product_price_aud on aux.mag_catalog_product_price_aud
	for update, delete
	as
	begin
		set nocount on

		insert aux.mag_catalog_product_price_aud_hist (store_id, store_name, product_id, product_name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, disc_percent, max_pack_price, 
			promo_key, price_type, pla_type,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.store_id, d.store_name, d.product_id, d.product_name, d.product_lifecycle, 
				d.value, d.qty, d.min_qty_prod, d.num_packs_tier_price, 
				d.total_price, d.pack_price, d.disc_percent, d.max_pack_price, 
				d.promo_key, d.price_type, d.pla_type,
				d.idETLBatchRun,
				case when (i.store_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.store_id = i.store_id and d.product_id = i.product_id and d.price_type = i.price_type and d.qty = i.qty and d.pla_type = i.pla_type;
	end;
	go


	---- 


	drop trigger aux.trg_sales_dim_order_header;
	go 

	create trigger aux.trg_sales_dim_order_header on aux.sales_dim_order_header
	after insert
	as
	begin
		set nocount on

		merge into aux.sales_dim_order_header_aud with (tablock) as trg
		using inserted src
			on (trg.order_id_bk = src.order_id_bk)
		when matched and not exists
			(select 
				isnull(trg.invoice_id, 0), isnull(trg.shipment_id, 0), isnull(trg.creditmemo_id, 0), 
				isnull(trg.order_no, ' '), isnull(trg.invoice_no, ' '), isnull(trg.shipment_no, ' '), isnull(trg.creditmemo_no, ' '), 
				isnull(trg.idCalendarOrderDate, 0), isnull(trg.idTimeOrderDate, ' '), isnull(trg.order_date, ' '), 
				isnull(trg.idCalendarInvoiceDate, 0), isnull(trg.invoice_date, ' '), 
				isnull(trg.idCalendarShipmentDate, 0), isnull(trg.idTimeShipmentDate, ' '), isnull(trg.shipment_date, ' '), 
				isnull(trg.idCalendarRefundDate, 0), isnull(trg.refund_date, ' '), 

				isnull(trg.store_id_bk, 0), isnull(trg.market_id_bk, 0), isnull(trg.customer_id_bk, 0), isnull(trg.shipping_address_id, 0), isnull(trg.billing_address_id, 0), 
				isnull(trg.country_id_shipping_bk, ' '), isnull(trg.region_id_shipping_bk, 0), isnull(trg.postcode_shipping, ' '), 
				isnull(trg.country_id_billing_bk, ' '), isnull(trg.region_id_billing_bk, 0), isnull(trg.postcode_billing, ' '), 
				isnull(trg.customer_unsubscribe_name_bk, ''), 

				isnull(trg.order_stage_name_bk, ' '), isnull(trg.order_status_name_bk, ' '), isnull(trg.adjustment_order, ' '), isnull(trg.order_type_name_bk, ' '), isnull(trg.status_bk, ' '), 
				isnull(trg.payment_method_name_bk, ' '), isnull(trg.cc_type_name_bk, ' '), isnull(trg.payment_id, 0), isnull(trg.payment_method_code, ' '), 
				isnull(trg.shipping_description_bk, ' '), isnull(trg.shipping_description_code, ' '), isnull(trg.telesales_admin_username_bk, ' '), isnull(trg.prescription_method_name_bk, ' '), 
				isnull(trg.reminder_type_name_bk, ' '), isnull(trg.reminder_period_bk, 0), isnull(trg.reminder_date, ' '), isnull(trg.reminder_sent, 0), 
				isnull(trg.reorder_f, ' '), isnull(trg.reorder_date, ' '), isnull(trg.reorder_profile_id, 0), isnull(trg.automatic_reorder, 0), 
				isnull(trg.order_source, ' '), isnull(trg.proforma, ' '), 
				isnull(trg.product_type_oh_bk, ' '), isnull(trg.order_qty_time, 0), isnull(trg.num_diff_product_type_oh, 0), isnull(trg.aura_product_p, 0), 
				isnull(trg.referafriend_code, ' '), isnull(trg.referafriend_referer, 0), 
				isnull(trg.postoptics_auto_verification, ' '), isnull(trg.presc_verification_method, ' '), isnull(trg.warehouse_approved_time, ' '), 

				isnull(trg.channel_name_bk, ' '), isnull(trg.marketing_channel_name_bk, ' '), isnull(trg.publisher_id_bk, 0), 
				isnull(trg.affilCode, ' '), isnull(trg.ga_medium, ' '), isnull(trg.ga_source, ' '), isnull(trg.ga_channel, ' '), 
				isnull(trg.telesales_f, ' '), isnull(trg.sms_auto_f, ' '), isnull(trg.mutuelles_f, ' '), 
				isnull(trg.coupon_id_bk, 0), isnull(trg.coupon_code, ' '), isnull(trg.applied_rule_ids, ' '), 
				isnull(trg.device_model_name_bk, ' '), isnull(trg.device_browser_name_bk, ' '), isnull(trg.device_os_name_bk, ' '), 
				isnull(trg.ga_device_model, ' '), isnull(trg.ga_device_browser, ' '), isnull(trg.ga_device_os, ' '), 

				isnull(trg.countries_registered_code, ' '), 

				isnull(trg.price_type_name_bk, ' '), isnull(trg.discount_f, ' '), 
				isnull(trg.total_qty_unit, 0), isnull(trg.total_qty_unit_refunded, 0), isnull(trg.total_qty_pack, 0), isnull(trg.weight, 0), 
				isnull(trg.local_subtotal, 0), isnull(trg.local_shipping, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
				isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), isnull(trg.local_total_vat, 0), isnull(trg.local_total_prof_fee, 0), 
				isnull(trg.local_total_refund, 0), isnull(trg.local_store_credit_given, 0), isnull(trg.local_bank_online_given, 0), 
				isnull(trg.local_subtotal_refund, 0), isnull(trg.local_shipping_refund, 0), isnull(trg.local_discount_refund, 0), isnull(trg.local_store_credit_used_refund, 0), isnull(trg.local_adjustment_refund, 0), 
				isnull(trg.local_total_aft_refund_inc_vat, 0), isnull(trg.local_total_aft_refund_inc_vat_2, 0), 
				isnull(trg.local_total_aft_refund_exc_vat, 0), isnull(trg.local_total_aft_refund_vat, 0), isnull(trg.local_total_aft_refund_prof_fee, 0), 
				isnull(trg.local_product_cost, 0), isnull(trg.local_shipping_cost, 0), isnull(trg.local_total_cost, 0), isnull(trg.local_margin, 0), 
				isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, ' '), 
				isnull(trg.discount_percent, 0), isnull(trg.vat_percent, 0), isnull(trg.prof_fee_percent, 0), 
				isnull(trg.num_lines, 0)
			intersect
			select 
				isnull(src.invoice_id, 0), isnull(src.shipment_id, 0), isnull(src.creditmemo_id, 0), 
				isnull(src.order_no, ' '), isnull(src.invoice_no, ' '), isnull(src.shipment_no, ' '), isnull(src.creditmemo_no, ' '), 
				isnull(src.idCalendarOrderDate, 0), isnull(src.idTimeOrderDate, ' '), isnull(src.order_date, ' '), 
				isnull(src.idCalendarInvoiceDate, 0), isnull(src.invoice_date, ' '), 
				isnull(src.idCalendarShipmentDate, 0), isnull(src.idTimeShipmentDate, ' '), isnull(src.shipment_date, ' '), 
				isnull(src.idCalendarRefundDate, 0), isnull(src.refund_date, ' '), 

				isnull(src.store_id_bk, 0), isnull(src.market_id_bk, 0), isnull(src.customer_id_bk, 0), isnull(src.shipping_address_id, 0), isnull(src.billing_address_id, 0), 
				isnull(src.country_id_shipping_bk, ' '), isnull(src.region_id_shipping_bk, 0), isnull(src.postcode_shipping, ' '), 
				isnull(src.country_id_billing_bk, ' '), isnull(src.region_id_billing_bk, 0), isnull(src.postcode_billing, ' '), 
				isnull(src.customer_unsubscribe_name_bk, ''), 

				isnull(src.order_stage_name_bk, ' '), isnull(src.order_status_name_bk, ' '), isnull(src.adjustment_order, ' '), isnull(src.order_type_name_bk, ' '), isnull(src.status_bk, ' '), 
				isnull(src.payment_method_name_bk, ' '), isnull(src.cc_type_name_bk, ' '), isnull(src.payment_id, 0), isnull(src.payment_method_code, ' '), 
				isnull(src.shipping_description_bk, ' '), isnull(src.shipping_description_code, ' '), isnull(src.telesales_admin_username_bk, ' '), isnull(src.prescription_method_name_bk, ' '), 
				isnull(src.reminder_type_name_bk, ' '), isnull(src.reminder_period_bk, 0), isnull(src.reminder_date, ' '), isnull(src.reminder_sent, 0), 
				isnull(src.reorder_f, ' '), isnull(src.reorder_date, ' '), isnull(src.reorder_profile_id, 0), isnull(src.automatic_reorder, 0), 
				isnull(src.order_source, ' '), isnull(src.proforma, ' '), 
				isnull(src.product_type_oh_bk, ' '), isnull(src.order_qty_time, 0), isnull(src.num_diff_product_type_oh, 0), isnull(src.aura_product_p, 0), 
				isnull(src.referafriend_code, ' '), isnull(src.referafriend_referer, 0), 
				isnull(src.postoptics_auto_verification, ' '), isnull(src.presc_verification_method, ' '), isnull(src.warehouse_approved_time, ' '), 

				isnull(src.channel_name_bk, ' '), isnull(src.marketing_channel_name_bk, ' '), isnull(src.publisher_id_bk, 0), 
				isnull(src.affilCode, ' '), isnull(src.ga_medium, ' '), isnull(src.ga_source, ' '), isnull(src.ga_channel, ' '), 
				isnull(src.telesales_f, ' '), isnull(src.sms_auto_f, ' '), isnull(src.mutuelles_f, ' '), 
				isnull(src.coupon_id_bk, 0), isnull(src.coupon_code, ' '), isnull(src.applied_rule_ids, ' '), 
				isnull(src.device_model_name_bk, ' '), isnull(src.device_browser_name_bk, ' '), isnull(src.device_os_name_bk, ' '), 
				isnull(src.ga_device_model, ' '), isnull(src.ga_device_browser, ' '), isnull(src.ga_device_os, ' '), 

				isnull(src.countries_registered_code, ' '), 

				isnull(src.price_type_name_bk, ' '), isnull(src.discount_f, ' '), 
				isnull(src.total_qty_unit, 0), isnull(src.total_qty_unit_refunded, 0), isnull(src.total_qty_pack, 0), isnull(src.weight, 0), 
				isnull(src.local_subtotal, 0), isnull(src.local_shipping, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
				isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), isnull(src.local_total_vat, 0), isnull(src.local_total_prof_fee, 0), 
				isnull(src.local_total_refund, 0), isnull(src.local_store_credit_given, 0), isnull(src.local_bank_online_given, 0), 
				isnull(src.local_subtotal_refund, 0), isnull(src.local_shipping_refund, 0), isnull(src.local_discount_refund, 0), isnull(src.local_store_credit_used_refund, 0), isnull(src.local_adjustment_refund, 0), 
				isnull(src.local_total_aft_refund_inc_vat, 0), isnull(trg.local_total_aft_refund_inc_vat_2, 0), 
				isnull(src.local_total_aft_refund_exc_vat, 0), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, 0), 
				isnull(src.local_product_cost, 0), isnull(src.local_shipping_cost, 0), isnull(src.local_total_cost, 0), isnull(src.local_margin, 0), 
				isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ' '), 
				isnull(src.discount_percent, 0), isnull(src.vat_percent, 0), isnull(src.prof_fee_percent, 0), 
				isnull(src.num_lines, 0)) 
			
			then
				update set
					trg.invoice_id = src.invoice_id, trg.shipment_id = src.shipment_id, trg.creditmemo_id = src.creditmemo_id, 
					trg.order_no = src.order_no, trg.invoice_no = src.invoice_no, trg.shipment_no = src.shipment_no, trg.creditmemo_no = src.creditmemo_no, 
					trg.idCalendarOrderDate = src.idCalendarOrderDate, trg.idTimeOrderDate = src.idTimeOrderDate, trg.order_date = src.order_date, 
					trg.idCalendarInvoiceDate = src.idCalendarInvoiceDate, trg.invoice_date = src.invoice_date, 
					trg.idCalendarShipmentDate = src.idCalendarShipmentDate, trg.idTimeShipmentDate = src.idTimeShipmentDate, trg.shipment_date = src.shipment_date, 
					trg.idCalendarRefundDate = src.idCalendarRefundDate, trg.refund_date = src.refund_date, 

					trg.store_id_bk = src.store_id_bk, trg.market_id_bk = src.market_id_bk, trg.customer_id_bk = src.customer_id_bk, trg.shipping_address_id = src.shipping_address_id, trg.billing_address_id = src.billing_address_id, 
					trg.country_id_shipping_bk = src.country_id_shipping_bk, trg.region_id_shipping_bk = src.region_id_shipping_bk, trg.postcode_shipping = src.postcode_shipping, 
					trg.country_id_billing_bk = src.country_id_billing_bk, trg.region_id_billing_bk = src.region_id_billing_bk, trg.postcode_billing = src.postcode_billing, 
					trg.customer_unsubscribe_name_bk = src.customer_unsubscribe_name_bk, 

					trg.order_stage_name_bk = src.order_stage_name_bk, trg.order_status_name_bk = src.order_status_name_bk, trg.adjustment_order = src.adjustment_order, 
					trg.order_type_name_bk = src.order_type_name_bk, trg.status_bk = src.status_bk, 
					trg.payment_method_name_bk = src.payment_method_name_bk, trg.cc_type_name_bk = src.cc_type_name_bk, trg.payment_id = src.payment_id, trg.payment_method_code = src.payment_method_code, 
					trg.shipping_description_bk = src.shipping_description_bk, trg.shipping_description_code = src.shipping_description_code, 
					trg.telesales_admin_username_bk = src.telesales_admin_username_bk, trg.prescription_method_name_bk = src.prescription_method_name_bk, 
					trg.reminder_type_name_bk = src.reminder_type_name_bk, trg.reminder_period_bk = src.reminder_period_bk, trg.reminder_date = src.reminder_date, trg.reminder_sent = src.reminder_sent, 
					trg.reorder_f = src.reorder_f, trg.reorder_date = src.reorder_date, trg.reorder_profile_id = src.reorder_profile_id, trg.automatic_reorder = src.automatic_reorder, 
					trg.order_source = src.order_source, trg.proforma = src.proforma, 
					trg.product_type_oh_bk = src.product_type_oh_bk, trg.order_qty_time = src.order_qty_time, trg.num_diff_product_type_oh = src.num_diff_product_type_oh, trg.aura_product_p = src.aura_product_p, 

					trg.referafriend_code = src.referafriend_code, trg.referafriend_referer = src.referafriend_referer, 
					trg.postoptics_auto_verification = src.postoptics_auto_verification, trg.presc_verification_method = src.presc_verification_method, trg.warehouse_approved_time = src.warehouse_approved_time, 

					trg.channel_name_bk = src.channel_name_bk, trg.marketing_channel_name_bk = src.marketing_channel_name_bk, trg.publisher_id_bk = src.publisher_id_bk, 
					trg.affilCode = src.affilCode, trg.ga_medium = src.ga_medium, trg.ga_source = src.ga_source, trg.ga_channel = src.ga_channel, 
					trg.telesales_f = src.telesales_f, trg.sms_auto_f = src.sms_auto_f, trg.mutuelles_f = src.mutuelles_f, 
					trg.coupon_id_bk = src.coupon_id_bk, trg.coupon_code = src.coupon_code, trg.applied_rule_ids = src.applied_rule_ids, 
					trg.device_model_name_bk = src.device_model_name_bk, trg.device_browser_name_bk = src.device_browser_name_bk, trg.device_os_name_bk = src.device_os_name_bk, 
					trg.ga_device_model = src.ga_device_model, trg.ga_device_browser = src.ga_device_browser, trg.ga_device_os = src.ga_device_os, 

					trg.countries_registered_code = src.countries_registered_code, 

					trg.price_type_name_bk = src.price_type_name_bk, trg.discount_f= src.discount_f, 
					trg.total_qty_unit = src.total_qty_unit, trg.total_qty_unit_refunded = src.total_qty_unit_refunded, trg.total_qty_pack = src.total_qty_pack, trg.weight = src.weight, 
					trg.local_subtotal = src.local_subtotal, trg.local_shipping = src.local_shipping, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
					trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee, 
					trg.local_total_refund = src.local_total_refund, trg.local_store_credit_given = src.local_store_credit_given, trg.local_bank_online_given = src.local_bank_online_given, 
					trg.local_subtotal_refund = src.local_subtotal_refund, trg.local_shipping_refund = src.local_shipping_refund, trg.local_discount_refund = src.local_discount_refund, 
					trg.local_store_credit_used_refund = src.local_store_credit_used_refund, trg.local_adjustment_refund = src.local_adjustment_refund, 
					trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat, trg.local_total_aft_refund_inc_vat_2 = src.local_total_aft_refund_inc_vat_2, 
					trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
					trg.local_product_cost = src.local_product_cost, trg.local_shipping_cost = src.local_shipping_cost, trg.local_total_cost = src.local_total_cost, trg.local_margin = src.local_margin, 
					trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
					trg.discount_percent = src.discount_percent, trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent, 
					trg.num_lines = src.num_lines, 

					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (order_id_bk, 
					invoice_id, shipment_id, creditmemo_id, 
					order_no, invoice_no, shipment_no, creditmemo_no, 
					idCalendarOrderDate, idTimeOrderDate, order_date, 
					idCalendarInvoiceDate, invoice_date, 
					idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
					idCalendarRefundDate, refund_date,

					store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
					country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
					country_id_billing_bk, region_id_billing_bk, postcode_billing,
					customer_unsubscribe_name_bk,

					order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
					payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
					shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
					reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
					reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
					order_source, proforma, 
					product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

					referafriend_code, referafriend_referer, 
					postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

					channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
					affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
					coupon_id_bk, coupon_code, applied_rule_ids,
					device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
					ga_device_model, ga_device_browser, ga_device_os,

					countries_registered_code,

					price_type_name_bk, discount_f,
					total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
					local_subtotal, local_shipping, local_discount, local_store_credit_used, 
					local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
					local_total_refund, local_store_credit_given, local_bank_online_given, 
					local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
					local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
					local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
					local_to_global_rate, order_currency_code, 
					discount_percent, vat_percent, prof_fee_percent,
					num_lines,

					idETLBatchRun)
					
					values (src.order_id_bk, 
						src.invoice_id, src.shipment_id, src.creditmemo_id, 
						src.order_no, src.invoice_no, src.shipment_no, src.creditmemo_no, 
						src.idCalendarOrderDate, src.idTimeOrderDate, src.order_date, 
						src.idCalendarInvoiceDate, src.invoice_date, 
						src.idCalendarShipmentDate, src.idTimeShipmentDate, src.shipment_date, 
						src.idCalendarRefundDate, src.refund_date,

						src.store_id_bk, src.market_id_bk, src.customer_id_bk, src.shipping_address_id, src.billing_address_id,
						src.country_id_shipping_bk, src.region_id_shipping_bk, src.postcode_shipping, 
						src.country_id_billing_bk, src.region_id_billing_bk, src.postcode_billing,
						src.customer_unsubscribe_name_bk,

						src.order_stage_name_bk, src.order_status_name_bk, src.adjustment_order, src.order_type_name_bk, src.status_bk, 
						src.payment_method_name_bk, src.cc_type_name_bk, src.payment_id, src.payment_method_code,
						src.shipping_description_bk, src.shipping_description_code, src.telesales_admin_username_bk, src.prescription_method_name_bk,
						src.reminder_type_name_bk, src.reminder_period_bk, src.reminder_date, src.reminder_sent, 
						src.reorder_f, src.reorder_date, src.reorder_profile_id, src.automatic_reorder,
						src.order_source, src.proforma, 
						src.product_type_oh_bk, src.order_qty_time, src.num_diff_product_type_oh, src.aura_product_p, 

						src.referafriend_code, src.referafriend_referer, 
						src.postoptics_auto_verification, src.presc_verification_method, src.warehouse_approved_time,

						src.channel_name_bk, src.marketing_channel_name_bk, src.publisher_id_bk, 
						src.affilCode, src.ga_medium, src.ga_source, src.ga_channel, src.telesales_f, src.sms_auto_f, src.mutuelles_f,
						src.coupon_id_bk, src.coupon_code, src.applied_rule_ids,
						src.device_model_name_bk, src.device_browser_name_bk, src.device_os_name_bk, 
						src.ga_device_model, src.ga_device_browser, src.ga_device_os,

						src.countries_registered_code,

						src.price_type_name_bk, src.discount_f,
						src.total_qty_unit, src.total_qty_unit_refunded, src.total_qty_pack, src.weight, 
						src.local_subtotal, src.local_shipping, src.local_discount, src.local_store_credit_used, 
						src.local_total_inc_vat, src.local_total_exc_vat, src.local_total_vat, src.local_total_prof_fee, 
						src.local_total_refund, src.local_store_credit_given, src.local_bank_online_given, 
						src.local_subtotal_refund, src.local_shipping_refund, src.local_discount_refund, src.local_store_credit_used_refund, src.local_adjustment_refund, 
						src.local_total_aft_refund_inc_vat, src.local_total_aft_refund_inc_vat_2, src.local_total_aft_refund_exc_vat, src.local_total_aft_refund_vat, src.local_total_aft_refund_prof_fee, 
						src.local_product_cost, src.local_shipping_cost, src.local_total_cost, src.local_margin, 
						src.local_to_global_rate, src.order_currency_code, 
						src.discount_percent, src.vat_percent, src.prof_fee_percent,
						src.num_lines,

						src.idETLBatchRun);
	end; 
	go



	---- 


	drop trigger aux.trg_sales_fact_order_line;
	go 

	create trigger aux.trg_sales_fact_order_line on aux.sales_fact_order_line
	after insert
	as
	begin
		set nocount on

		merge into aux.sales_fact_order_line_aud with (tablock) as trg
		using inserted src
			on (trg.order_line_id_bk = src.order_line_id_bk)
		when matched and not exists
			(select 
				isnull(trg.order_id_bk, 0), 
				isnull(trg.invoice_id, 0), isnull(trg.shipment_id, 0), isnull(trg.creditmemo_id, 0), 
				isnull(trg.order_no, ' '), isnull(trg.invoice_no, ' '), isnull(trg.shipment_no, ' '), isnull(trg.creditmemo_no, ' '), 
				isnull(trg.invoice_line_id, 0), isnull(trg.shipment_line_id, 0), isnull(trg.creditmemo_line_id, 0), 
				isnull(trg.num_shipment_lines, 0), isnull(trg.num_creditmemo_lines, 0), 
				isnull(trg.idCalendarInvoiceDate, 0), isnull(trg.invoice_date, ' '), 
				isnull(trg.idCalendarShipmentDate, 0), isnull(trg.idTimeShipmentDate, ' '), isnull(trg.shipment_date, ' '), 
				isnull(trg.idCalendarRefundDate, 0), isnull(trg.refund_date, ' '), 
				isnull(trg.warehouse_id_bk, 0), 

				isnull(trg.product_id_bk, 0), 
				isnull(trg.base_curve_bk, ' '), isnull(trg.diameter_bk, ' '), isnull(trg.power_bk, ' '), isnull(trg.cylinder_bk, ' '), isnull(trg.axis_bk, ' '), 
				isnull(trg.addition_bk, ' '), isnull(trg.dominance_bk, ' '), isnull(trg.colour_bk, ' '), isnull(trg.eye, ' '), 
				isnull(trg.sku_magento, ' '), isnull(trg.sku_erp, ' '), 
				isnull(trg.glass_vision_type_bk, ' '), isnull(trg.order_line_id_vision_type, 0), isnull(trg.glass_package_type_bk, ' '), isnull(trg.order_line_id_package_type, 0), 
				isnull(trg.aura_product_f, ' '), 

				isnull(trg.countries_registered_code, ' '), isnull(trg.product_type_vat, ' '),  

				isnull(trg.order_status_name_bk, ' '), isnull(trg.price_type_name_bk, ' '), isnull(trg.discount_f, ' '), 
				isnull(trg.qty_unit, 0), isnull(trg.qty_unit_refunded, 0), isnull(trg.qty_pack, 0), isnull(trg.qty_time, 0), isnull(trg.weight, 0), 
				isnull(trg.local_price_unit, 0), isnull(trg.local_price_pack, 0), isnull(trg.local_price_pack_discount, 0), 
				isnull(trg.local_subtotal, 0), isnull(trg.local_shipping, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
				isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), isnull(trg.local_total_vat, 0), isnull(trg.local_total_prof_fee, 0), 
				isnull(trg.local_store_credit_given, 0), isnull(trg.local_bank_online_given, 0), 
				isnull(trg.local_subtotal_refund, 0), isnull(trg.local_shipping_refund, 0), isnull(trg.local_discount_refund, 0), isnull(trg.local_store_credit_used_refund, 0), isnull(trg.local_adjustment_refund, 0), 
				isnull(trg.local_total_aft_refund_inc_vat, 0), isnull(trg.local_total_aft_refund_exc_vat, 0), isnull(trg.local_total_aft_refund_vat, 0), isnull(trg.local_total_aft_refund_prof_fee, 0), 
				isnull(trg.local_product_cost, 0), isnull(trg.local_shipping_cost, 0), isnull(trg.local_total_cost, 0), isnull(trg.local_margin, 0), 
				isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, ' '), 
				isnull(trg.discount_percent, 0), isnull(trg.vat_percent, 0), isnull(trg.prof_fee_percent, 0), isnull(trg.vat_rate, 0), isnull(trg.prof_fee_rate, 0), 
				isnull(trg.order_allocation_rate, 0), isnull(trg.order_allocation_rate_aft_refund, 0), isnull(trg.order_allocation_rate_refund, 0), 
				isnull(trg.num_erp_allocation_lines, 0)

			intersect
			select 
				isnull(src.order_id_bk, 0), 
				isnull(src.invoice_id, 0), isnull(src.shipment_id, 0), isnull(src.creditmemo_id, 0), 
				isnull(src.order_no, ' '), isnull(src.invoice_no, ' '), isnull(src.shipment_no, ' '), isnull(src.creditmemo_no, ' '), 
				isnull(src.invoice_line_id, 0), isnull(src.shipment_line_id, 0), isnull(src.creditmemo_line_id, 0), 
				isnull(src.num_shipment_lines, 0), isnull(src.num_creditmemo_lines, 0), 
				isnull(src.idCalendarInvoiceDate, 0), isnull(src.invoice_date, ' '), 
				isnull(src.idCalendarShipmentDate, 0), isnull(src.idTimeShipmentDate, ' '), isnull(src.shipment_date, ' '), 
				isnull(src.idCalendarRefundDate, 0), isnull(src.refund_date, ' '), 
				isnull(src.warehouse_id_bk, 0), 

				isnull(src.product_id_bk, 0), 
				isnull(src.base_curve_bk, ' '), isnull(src.diameter_bk, ' '), isnull(src.power_bk, ' '), isnull(src.cylinder_bk, ' '), isnull(src.axis_bk, ' '), 
				isnull(src.addition_bk, ' '), isnull(src.dominance_bk, ' '), isnull(src.colour_bk, ' '), isnull(src.eye, ' '), 
				isnull(src.sku_magento, ' '), isnull(src.sku_erp, ' '), 
				isnull(src.glass_vision_type_bk, ' '), isnull(src.order_line_id_vision_type, 0), isnull(src.glass_package_type_bk, ' '), isnull(src.order_line_id_package_type, 0), 
				isnull(src.aura_product_f, ' '), 

				isnull(src.countries_registered_code, ' '), isnull(src.product_type_vat, ' '),  

				isnull(src.order_status_name_bk, ' '), isnull(src.price_type_name_bk, ' '), isnull(src.discount_f, ' '), 
				isnull(src.qty_unit, 0), isnull(src.qty_unit_refunded, 0), isnull(src.qty_pack, 0), isnull(src.qty_time, 0), isnull(src.weight, 0), 
				isnull(src.local_price_unit, 0), isnull(src.local_price_pack, 0), isnull(src.local_price_pack_discount, 0), 
				isnull(src.local_subtotal, 0), isnull(src.local_shipping, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
				isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), isnull(src.local_total_vat, 0), isnull(src.local_total_prof_fee, 0), 
				isnull(src.local_store_credit_given, 0), isnull(src.local_bank_online_given, 0), 
				isnull(src.local_subtotal_refund, 0), isnull(src.local_shipping_refund, 0), isnull(src.local_discount_refund, 0), isnull(src.local_store_credit_used_refund, 0), isnull(src.local_adjustment_refund, 0), 
				isnull(src.local_total_aft_refund_inc_vat, 0), isnull(src.local_total_aft_refund_exc_vat, 0), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, 0), 
				isnull(src.local_product_cost, 0), isnull(src.local_shipping_cost, 0), isnull(src.local_total_cost, 0), isnull(src.local_margin, 0), 
				isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ' '), 
				isnull(src.discount_percent, 0), isnull(src.vat_percent, 0), isnull(src.prof_fee_percent, 0), isnull(src.vat_rate, 0), isnull(src.prof_fee_rate, 0), 
				isnull(src.order_allocation_rate, 0), isnull(src.order_allocation_rate_aft_refund, 0), isnull(src.order_allocation_rate_refund, 0), 
				isnull(src.num_erp_allocation_lines, 0)) 			

			then
				update set
					trg.order_id_bk = src.order_id_bk, 
					trg.invoice_id = src.invoice_id, trg.shipment_id = src.shipment_id, trg.creditmemo_id = src.creditmemo_id, 
					trg.order_no = src.order_no, trg.invoice_no = src.invoice_no, trg.shipment_no = src.shipment_no, trg.creditmemo_no = src.creditmemo_no, 
					trg.invoice_line_id = src.invoice_line_id, trg.shipment_line_id = src.shipment_line_id, trg.creditmemo_line_id = src.creditmemo_line_id, 
					trg.num_shipment_lines = src.num_shipment_lines, trg.num_creditmemo_lines = src.num_creditmemo_lines, 
					trg.idCalendarInvoiceDate = src.idCalendarInvoiceDate, trg.invoice_date = src.invoice_date, 
					trg.idCalendarShipmentDate = src.idCalendarShipmentDate, trg.idTimeShipmentDate = src.idTimeShipmentDate, trg.shipment_date = src.shipment_date, 
					trg.idCalendarRefundDate = src.idCalendarRefundDate, trg.refund_date = src.refund_date, 
					trg.warehouse_id_bk = src.warehouse_id_bk, 

					trg.product_id_bk = src.product_id_bk, 
					trg.base_curve_bk = src.base_curve_bk, trg.diameter_bk = src.diameter_bk, trg.power_bk = src.power_bk, trg.cylinder_bk = src.cylinder_bk, trg.axis_bk = src.axis_bk, 
					trg.addition_bk = src.addition_bk, trg.dominance_bk = src.dominance_bk, trg.colour_bk = src.colour_bk, trg.eye = src.eye, 
					trg.sku_magento = src.sku_magento, trg.sku_erp = src.sku_erp, 
					trg.glass_vision_type_bk = src.glass_vision_type_bk, trg.order_line_id_vision_type = src.order_line_id_vision_type, 
					trg.glass_package_type_bk = src.glass_package_type_bk, trg.order_line_id_package_type = src.order_line_id_package_type, 
					trg.aura_product_f = src.aura_product_f, 

					trg.countries_registered_code = src.countries_registered_code, trg.product_type_vat = src.product_type_vat, 

					trg.order_status_name_bk = src.order_status_name_bk, trg.price_type_name_bk = src.price_type_name_bk, trg.discount_f = src.discount_f, 
					trg.qty_unit = src.qty_unit, trg.qty_unit_refunded = src.qty_unit_refunded, trg.qty_pack = src.qty_pack, trg.qty_time = src.qty_time, trg.weight = src.weight, 
					trg.local_price_unit = src.local_price_unit, trg.local_price_pack = src.local_price_pack, trg.local_price_pack_discount = src.local_price_pack_discount, 
					trg.local_subtotal = src.local_subtotal, trg.local_shipping = src.local_shipping, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
					trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee, 
					trg.local_store_credit_given = src.local_store_credit_given, trg.local_bank_online_given = src.local_bank_online_given, 
					trg.local_subtotal_refund = src.local_subtotal_refund, trg.local_shipping_refund = src.local_shipping_refund, trg.local_discount_refund = src.local_discount_refund, 
					trg.local_store_credit_used_refund = src.local_store_credit_used_refund, trg.local_adjustment_refund = src.local_adjustment_refund, 
					trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat, trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, 
					trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
					trg.local_product_cost = src.local_product_cost, trg.local_shipping_cost = src.local_shipping_cost, trg.local_total_cost = src.local_total_cost, trg.local_margin = src.local_margin, 
					trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
					trg.discount_percent = src.discount_percent, trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent, trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate, 
					trg.order_allocation_rate = src.order_allocation_rate, trg.order_allocation_rate_aft_refund = src.order_allocation_rate_aft_refund, trg.order_allocation_rate_refund = src.order_allocation_rate_refund, 
					trg.num_erp_allocation_lines = src.num_erp_allocation_lines, 

					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (order_line_id_bk, order_id_bk, 
					invoice_id, shipment_id, creditmemo_id, 
					order_no, invoice_no, shipment_no, creditmemo_no,
					invoice_line_id, shipment_line_id, creditmemo_line_id,  	
					num_shipment_lines, num_creditmemo_lines,
					idCalendarInvoiceDate, invoice_date, 
					idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
					idCalendarRefundDate, refund_date, 
					warehouse_id_bk, 

					product_id_bk, 
					base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
					sku_magento, sku_erp, 
					glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
					aura_product_f, 

					countries_registered_code, product_type_vat, 

					order_status_name_bk, price_type_name_bk, discount_f,
					qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
					local_price_unit, local_price_pack, local_price_pack_discount, 
					local_subtotal, local_shipping, local_discount, local_store_credit_used, 
					local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
					local_store_credit_given, local_bank_online_given, 
					local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
					local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
					local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
					local_to_global_rate, order_currency_code, 
					discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
					order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
					num_erp_allocation_lines, 
					idETLBatchRun)
					
					values (src.order_line_id_bk, src.order_id_bk, 
						src.invoice_id, src.shipment_id, src.creditmemo_id, 
						src.order_no, src.invoice_no, src.shipment_no, src.creditmemo_no,
						src.invoice_line_id, src.shipment_line_id, src.creditmemo_line_id,  	
						src.num_shipment_lines, src.num_creditmemo_lines,
						src.idCalendarInvoiceDate, src.invoice_date, 
						src.idCalendarShipmentDate, src.idTimeShipmentDate, src.shipment_date, 
						src.idCalendarRefundDate, src.refund_date, 
						src.warehouse_id_bk, 

						src.product_id_bk, 
						src.base_curve_bk, src.diameter_bk, src.power_bk, src.cylinder_bk, src.axis_bk, src.addition_bk, src.dominance_bk, src.colour_bk, src.eye,
						src.sku_magento, src.sku_erp, 
						src.glass_vision_type_bk, src.order_line_id_vision_type, src.glass_package_type_bk, src.order_line_id_package_type,
						src.aura_product_f, 

						src.countries_registered_code, src.product_type_vat, 

						src.order_status_name_bk, src.price_type_name_bk, src.discount_f,
						src.qty_unit, src.qty_unit_refunded, src.qty_pack, src.qty_time, src.weight, 
						src.local_price_unit, src.local_price_pack, src.local_price_pack_discount, 
						src.local_subtotal, src.local_shipping, src.local_discount, src.local_store_credit_used, 
						src.local_total_inc_vat, src.local_total_exc_vat, src.local_total_vat, src.local_total_prof_fee, 
						src.local_store_credit_given, src.local_bank_online_given, 
						src.local_subtotal_refund, src.local_shipping_refund, src.local_discount_refund, src.local_store_credit_used_refund, src.local_adjustment_refund, 
						src.local_total_aft_refund_inc_vat, src.local_total_aft_refund_exc_vat, src.local_total_aft_refund_vat, src.local_total_aft_refund_prof_fee, 
						src.local_product_cost, src.local_shipping_cost, src.local_total_cost, src.local_margin, 
						src.local_to_global_rate, src.order_currency_code, 
						src.discount_percent, src.vat_percent, src.prof_fee_percent, src.vat_rate, src.prof_fee_rate,
						src.order_allocation_rate, src.order_allocation_rate_aft_refund, src.order_allocation_rate_refund, 
						src.num_erp_allocation_lines, 

						src.idETLBatchRun);
	end; 
	go




	drop trigger aux.trg_act_customer_registration;
	go 

	create trigger aux.trg_act_customer_registration on aux.act_customer_registration
	after insert
	as
	begin
		set nocount on

		merge into aux.act_customer_registration_aud with (tablock) as trg
		using inserted src
			on (trg.customer_id_bk = src.customer_id_bk)
		when matched and not exists
			(select 
				isnull(trg.store_id_bk, 0), 
				isnull(trg.idCalendarRegisterDate, 0), isnull(trg.register_date, ' ')
			intersect
			select 
				isnull(src.store_id_bk, 0), 
				isnull(src.idCalendarRegisterDate, 0), isnull(src.register_date, ' ')) 
			
			then
				update set
					trg.store_id_bk = src.store_id_bk, 
					trg.idCalendarRegisterDate = src.idCalendarRegisterDate, trg.register_date = src.register_date, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (customer_id_bk, store_id_bk, idCalendarRegisterDate, register_date, 
					idETLBatchRun)
					
					values (src.customer_id_bk, src.store_id_bk, src.idCalendarRegisterDate, src.register_date,  
						src.idETLBatchRun);
	end; 
	go
	
	
	

	drop trigger aux.trg_gen_dim_customer;
	go 

	create trigger aux.trg_gen_dim_customer on aux.gen_dim_customer
	after insert
	as
	begin
		set nocount on

		merge into aux.gen_dim_customer_aud with (tablock) as trg
		using inserted src
			on (trg.customer_id_bk = src.customer_id_bk)
		when matched and not exists
			(select 
				isnull(trg.customer_email, ' '), 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' '), 
				isnull(trg.store_id_bk, 0), isnull(trg.market_id_bk, 0), isnull(trg.customer_type_name_bk, ' '), isnull(trg.customer_status_name_bk, ' '), 
				isnull(trg.prefix, ' '), isnull(trg.first_name, ' '), isnull(trg.last_name, ' '), 
				isnull(trg.dob, ' '), isnull(trg.gender, ' '), isnull(trg.language, ' '), 
				isnull(trg.phone_number, ' '), isnull(trg.street, ' '), isnull(trg.city, ' '), isnull(trg.postcode, ' '), 
				isnull(trg.region, ' '), isnull(trg.region_id_bk, 0), isnull(trg.country_id_bk, ' '), 
				isnull(trg.postcode_s, ' '), isnull(trg.country_id_s_bk, ' '), 
				isnull(trg.customer_unsubscribe_name_bk, ' '), isnull(trg.customer_unsubscribe_d, 0), 
				isnull(trg.unsubscribe_mark_email_magento_f, ' '), isnull(trg.unsubscribe_mark_email_magento_d, 0), 
				isnull(trg.unsubscribe_mark_email_dotmailer_f, ' '), isnull(trg.unsubscribe_mark_email_dotmailer_d, 0),
				isnull(trg.unsubscribe_text_message_f, ' '), isnull(trg.unsubscribe_text_message_d, 0),  
				isnull(trg.reminder_f, ' '), isnull(trg.reminder_type_name_bk, ' '), isnull(trg.reminder_period_bk, 0), 
				isnull(trg.reorder_f, ' '), 
				isnull(trg.referafriend_code, ' '), isnull(trg.days_worn_info, ' '), 
				isnull(trg.migrate_customer_f, ' '), isnull(trg.migrate_customer_store, ' '), isnull(trg.migrate_customer_id, ' ') 
			intersect
			select 
				isnull(src.customer_email, ' '), 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '), 
				isnull(src.store_id_bk, 0), isnull(src.market_id_bk, 0), isnull(src.customer_type_name_bk, ' '), isnull(src.customer_status_name_bk, ' '), 
				isnull(src.prefix, ' '), isnull(src.first_name, ' '), isnull(src.last_name, ' '), 
				isnull(src.dob, ' '), isnull(src.gender, ' '), isnull(src.language, ' '), 
				isnull(src.phone_number, ' '), isnull(src.street, ' '), isnull(src.city, ' '), isnull(src.postcode, ' '), 
				isnull(src.region, ' '), isnull(src.region_id_bk, 0), isnull(src.country_id_bk, ' '), 
				isnull(src.postcode_s, ' '), isnull(src.country_id_s_bk, ' '), 
				isnull(src.customer_unsubscribe_name_bk, ' '), isnull(src.customer_unsubscribe_d, 0), 
				isnull(src.unsubscribe_mark_email_magento_f, ' '), isnull(src.unsubscribe_mark_email_magento_d, 0), 
				isnull(src.unsubscribe_mark_email_dotmailer_f, ' '), isnull(src.unsubscribe_mark_email_dotmailer_d, 0),
				isnull(src.unsubscribe_text_message_f, ' '), isnull(src.unsubscribe_text_message_d, 0),  
				isnull(src.reminder_f, ' '), isnull(src.reminder_type_name_bk, ' '), isnull(src.reminder_period_bk, 0), 
				isnull(src.reorder_f, ' '), 
				isnull(src.referafriend_code, ' '), isnull(src.days_worn_info, ' '), 
				isnull(src.migrate_customer_f, ' '), isnull(src.migrate_customer_store, ' '), isnull(src.migrate_customer_id, ' '))
		then
			update set
				trg.customer_email = src.customer_email, 
				trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
				trg.store_id_bk = src.store_id_bk, trg.market_id_bk = src.market_id_bk, trg.customer_type_name_bk = src.customer_type_name_bk, trg.customer_status_name_bk = src.customer_status_name_bk, 
				trg.prefix = src.prefix, trg.first_name = src.first_name, trg.last_name = src.last_name, 
				trg.dob = src.dob, trg.gender = src.gender, trg.language = src.language, 
				trg.phone_number = src.phone_number, trg.street = src.street, trg.city = src.city, trg.postcode = src.postcode, 
				trg.region = src.region, trg.region_id_bk = src.region_id_bk, trg.country_id_bk = src.country_id_bk, 
				trg.postcode_s = src.postcode_s, trg.country_id_s_bk = src.country_id_s_bk, 
				trg.customer_unsubscribe_name_bk = src.customer_unsubscribe_name_bk, trg.customer_unsubscribe_d = src.customer_unsubscribe_d, 
				trg.unsubscribe_mark_email_magento_f = src.unsubscribe_mark_email_magento_f, trg.unsubscribe_mark_email_magento_d = src.unsubscribe_mark_email_magento_d, 
				trg.unsubscribe_mark_email_dotmailer_f = src.unsubscribe_mark_email_dotmailer_f, trg.unsubscribe_mark_email_dotmailer_d = src.unsubscribe_mark_email_dotmailer_d, 
				trg.unsubscribe_text_message_f = src.unsubscribe_text_message_f, trg.unsubscribe_text_message_d = src.unsubscribe_text_message_d, 
				trg.reminder_f = src.reminder_f, trg.reminder_type_name_bk = src.reminder_type_name_bk, trg.reminder_period_bk = src.reminder_period_bk, 
				trg.reorder_f = src.reorder_f, 
				trg.referafriend_code = src.referafriend_code, trg.days_worn_info = src.days_worn_info, 
				trg.migrate_customer_f = src.migrate_customer_f, trg.migrate_customer_store = src.migrate_customer_store, trg.migrate_customer_id = src.migrate_customer_id, 

				trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then 
				insert (customer_id_bk, customer_email,
					created_at, updated_at,
					store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
					prefix, first_name, last_name, 
					dob, gender, language, 
					phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
					postcode_s, country_id_s_bk, 
					customer_unsubscribe_name_bk, customer_unsubscribe_d,
					unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
					unsubscribe_text_message_f, unsubscribe_text_message_d, 
					reminder_f, reminder_type_name_bk, reminder_period_bk, 
					reorder_f, 
					referafriend_code, days_worn_info, 
					migrate_customer_f, migrate_customer_store, migrate_customer_id, 
					idETLBatchRun)

				values (src.customer_id_bk, src.customer_email,
					src.created_at, src.updated_at,
					src.store_id_bk, src.market_id_bk, src.customer_type_name_bk, src.customer_status_name_bk,
					src.prefix, src.first_name, src.last_name, 
					src.dob, src.gender, src.language, 
					src.phone_number, src.street, src.city, src.postcode, src.region, src.region_id_bk, src.country_id_bk, 
					src.postcode_s, src.country_id_s_bk, 
					src.customer_unsubscribe_name_bk, src.customer_unsubscribe_d,
					src.unsubscribe_mark_email_magento_f, src.unsubscribe_mark_email_magento_d, src.unsubscribe_mark_email_dotmailer_f, src.unsubscribe_mark_email_dotmailer_d, 
					src.unsubscribe_text_message_f, src.unsubscribe_text_message_d, 
					src.reminder_f, src.reminder_type_name_bk, src.reminder_period_bk, 
					src.reorder_f, 
					src.referafriend_code, src.days_worn_info, 
					src.migrate_customer_f, src.migrate_customer_store, src.migrate_customer_id, 
					src.idETLBatchRun);
	end;
	go	