use Landing
go

-- prod_product_category
select product_id, 
	category_bk, cl_type_bk, cl_feature_bk
from Landing.aux.prod_product_category

-- prod_product_product_type_oh
select product_id, product_type_oh_bk
from Landing.aux.prod_product_product_type_oh

-- prod_product_product_type_vat
select product_id, product_type_vat
from Landing.aux.prod_product_product_type_vat



-- mag_core_config_data_store
select store_id, scope_id, path, value
from Landing.aux.mag_core_config_data_store



-- mag_sales_flat_creditmemo
select order_id, num_order, 
	entity_id, increment_id, created_at,
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative 
from Landing.aux.mag_sales_flat_creditmemo

-- mag_sales_flat_creditmemo_item
select order_id, order_item_id, 
	entity_id, increment_id, created_at, item_id,
	num_rep_rows, 
	sum_qty, sum_base_row_total, sum_base_discount_amount
from Landing.aux.mag_sales_flat_creditmemo_item


-- mag_edi_stock_item
select product_id, product_code, 
	bc, di, po, cy, ax, ad, do, co
from Landing.aux.mag_edi_stock_item


-- mag_catalog_product_price
select store_id, store_name, product_id, product_name, product_lifecycle, 
	value, qty, min_qty_prod, num_packs_tier_price, 
	total_price, pack_price, disc_percent, max_pack_price, 
	promo_key, price_type, pla_type,
	idETLBatchRun, ins_ts
from Landing.aux.mag_catalog_product_price
order by product_id, store_id, price_type, qty

select store_id, store_name, product_id, product_name, product_lifecycle, 
	value, qty, min_qty_prod, num_packs_tier_price, 
	total_price, pack_price, disc_percent, max_pack_price, 
	promo_key, price_type, pla_type,
	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.mag_catalog_product_price_aud
order by product_id, store_id, price_type, qty

select store_id, store_name, product_id, product_name, product_lifecycle, 
	value, qty, min_qty_prod, num_packs_tier_price, 
	total_price, pack_price, disc_percent, max_pack_price, 
	promo_key, price_type, pla_type,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from Landing.aux.mag_catalog_product_price_aud_hist



-- gen_customer_unsubscribe_magento
select customer_id_bk, 
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d
from Landing.aux.gen_customer_unsubscribe_magento

-- gen_customer_unsubscribe_dotmailer
select customer_id_bk, 
	unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d
from Landing.aux.gen_customer_unsubscribe_dotmailer

-- gen_customer_unsubscribe_textmessage
select customer_id_bk, 
	unsubscribe_text_message_f, unsubscribe_text_message_d
from Landing.aux.gen_customer_unsubscribe_textmessage

-- gen_customer_reminder
select customer_id_bk, 
	reminder_f, reminder_type_name_bk, reminder_period_bk
from Landing.aux.gen_customer_reminder

-- gen_customer_reorder
select customer_id_bk, 
	reorder_f
from Landing.aux.gen_customer_reorder



-- sales_order_header_o
select order_id_bk
from Landing.aux.sales_order_header_o

-- sales_order_header_o_i_s_cr
select order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate, idTimeOrderDate, order_date, 
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date 
from Landing.aux.sales_order_header_o_i_s_cr

-- sales_order_header_store_cust
select order_id_bk, 
	store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
	country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
	country_id_billing_bk, region_id_billing_bk, postcode_billing, 
	customer_unsubscribe_name_bk
from Landing.aux.sales_order_header_store_cust

-- sales_order_header_dim_order
select order_id_bk,
	order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
	payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
	shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
	reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
	reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
	order_source, proforma,
	product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

	referafriend_code, referafriend_referer, 
	postoptics_auto_verification, presc_verification_method, warehouse_approved_time
from Landing.aux.sales_order_header_dim_order

-- sales_order_header_dim_mark
select order_id_bk, 
	channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
	affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f, 
	coupon_id_bk, coupon_code, applied_rule_ids,
	device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
	ga_device_model, ga_device_browser, ga_device_os	
from Landing.aux.sales_order_header_dim_mark

-- sales_order_header_measures
select order_id_bk, 
	price_type_name_bk, discount_f,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines
from Landing.aux.sales_order_header_measures

-- sales_order_header_vat
select order_id_bk, 
	countries_registered_code, invoice_date, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	vat_percent, prof_fee_percent
from Landing.aux.sales_order_header_vat


-- sales_order_header_oh_pt
select order_id_bk, product_type_oh_bk, 
	order_percentage, order_flag, order_qty_time
from Landing.aux.sales_order_header_oh_pt


-- sales_order_line_o_i_s_cr
select order_line_id_bk, order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no,
	invoice_line_id, shipment_line_id, creditmemo_line_id,  	
	num_shipment_lines, num_creditmemo_lines,
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date, 
	warehouse_id_bk
from Landing.aux.sales_order_line_o_i_s_cr

-- sales_order_line_product
select order_line_id_bk, order_id_bk, 
	product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
	sku_magento, sku_erp, 
	glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
	aura_product_f	
from Landing.aux.sales_order_line_product

-- sales_order_line_measures
select order_line_id_bk, order_id_bk, 
	order_status_name_bk, price_type_name_bk, discount_f,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines
from Landing.aux.sales_order_line_measures


-- sales_order_line_vat
select order_line_id_bk, order_id_bk, 
	countries_registered_code, product_type_vat, vat_type_rate_code, invoice_date, 
	vat_rate, prof_fee_rate,
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	vat_percent, prof_fee_percent
from Landing.aux.sales_order_line_vat



-- sales_dim_order_header
select order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate, idTimeOrderDate, order_date, 
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date,

	store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
	country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
	country_id_billing_bk, region_id_billing_bk, postcode_billing,
	customer_unsubscribe_name_bk,

	order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
	payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
	shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
	reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
	reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
	order_source, proforma,
	product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

	referafriend_code, referafriend_referer, 
	postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

	channel_name_bk, marketing_channel_name_bk, publisher_id_bk, affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
	coupon_id_bk, coupon_code, applied_rule_ids,
	device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
	ga_device_model, ga_device_browser, ga_device_os,

	countries_registered_code,

	price_type_name_bk, discount_f,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines, 

	idETLBatchRun, ins_ts

from Landing.aux.sales_dim_order_header

-- sales_dim_order_header_aud
select order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate, idTimeOrderDate, order_date, 
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date,

	store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
	country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
	country_id_billing_bk, region_id_billing_bk, postcode_billing,
	customer_unsubscribe_name_bk,

	order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
	payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
	shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
	reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
	reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
	order_source, proforma,
	product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p, 

	referafriend_code, referafriend_referer, 
	postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

	channel_name_bk, marketing_channel_name_bk, publisher_id_bk, affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
	coupon_id_bk, coupon_code, applied_rule_ids,
	device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
	ga_device_model, ga_device_browser, ga_device_os,

	countries_registered_code,

	price_type_name_bk, discount_f,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines, 

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.sales_dim_order_header_aud




-- sales_fact_order_line
select order_line_id_bk, order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no,
	invoice_line_id, shipment_line_id, creditmemo_line_id,  	
	num_shipment_lines, num_creditmemo_lines,
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date, 
	warehouse_id_bk, 

	product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
	sku_magento, sku_erp, 
	glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
	aura_product_f, 

	countries_registered_code, product_type_vat,

	order_status_name_bk, price_type_name_bk, discount_f,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines, 

	idETLBatchRun, ins_ts
from Landing.aux.sales_fact_order_line

-- sales_fact_order_line_aud
select order_line_id_bk, order_id_bk, 
	invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no,
	invoice_line_id, shipment_line_id, creditmemo_line_id,  	
	num_shipment_lines, num_creditmemo_lines,
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date, 
	warehouse_id_bk, 

	product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
	sku_magento, sku_erp, 
	glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
	aura_product_f, 

	countries_registered_code, product_type_vat,

	order_status_name_bk, price_type_name_bk, discount_f,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines, 

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.sales_fact_order_line_aud



-- act_fact_activity_sales
select order_id_bk, 
	idCalendarActivityDate, activity_date, prev_activity_date, next_activity_date, 
	idCalendarPrevActivityDate, idCalendarNextActivityDate, 
	activity_type_name_bk, store_id_bk, customer_id_bk, 
	channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
	customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, 

	local_subtotal, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, 
	local_to_global_rate, order_currency_code
from Landing.aux.act_fact_activity_sales


-- act_fact_activity_other
select activity_id_bk, 
	idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
	activity_type_name_bk, store_id_bk, customer_id_bk, 
	activity_seq_no 

from Landing.aux.act_fact_activity_other


-- act_customer_registration
select customer_id_bk, store_id_bk, idCalendarRegisterDate, register_date, 
		idETLBatchRun, ins_ts
from Landing.aux.act_customer_registration

select customer_id_bk, store_id_bk, idCalendarRegisterDate, register_date, 
		idETLBatchRun, ins_ts, upd_ts
from Landing.aux.act_customer_registration_aud


-- act_customer_cr_reg
select customer_id_bk, 
	email, store_id, created_at, 
	old_access_cust_no, store_id_oacn, website_group_oacn,
	migrate_db, migrate_old_customer_id, migrate_created_at, 
	num_dist_websites, 
	store_id_first_all, first_all_date, store_id_first_vd, first_vd_date, store_id_first_non_vd, first_non_vd_date, 
	store_id_last_non_vd, last_non_vd_date, 
	non_vd_customer_status_at_migration_date,
	store_id_last_vd_before_migration, last_vd_before_migration_date, 
	customer_status_at_migration_date, 
	migration_date, 
	store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date,
	migration_date_oacn, 
	store_id_bk_cr, create_date, 
	store_id_bk_reg, register_date, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Landing.aux.act_customer_cr_reg



select exchange_rate_day, eur_to_gbp, gbp_to_eur, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts 
from Landing.aux.sales_exchange_rate



select customer_id_bk, customer_email,
	created_at, updated_at,
	store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
	prefix, first_name, last_name, 
	dob, gender, language, 
	phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
	postcode_s, country_id_s_bk, 
	customer_unsubscribe_name_bk, customer_unsubscribe_d,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
	unsubscribe_text_message_f, unsubscribe_text_message_d, 
	reminder_f, reminder_type_name_bk, reminder_period_bk, 
	reorder_f, 
	referafriend_code, days_worn_info, 
	migrate_customer_f, migrate_customer_store, migrate_customer_id, 
	idETLBatchRun, ins_ts
from Landing.aux.gen_dim_customer


select top 1000 customer_id_bk, customer_email,
	created_at, updated_at,
	store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
	prefix, first_name, last_name, 
	dob, gender, language, 
	phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
	postcode_s, country_id_s_bk, 
	customer_unsubscribe_name_bk, customer_unsubscribe_d,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
	unsubscribe_text_message_f, unsubscribe_text_message_d, 
	reminder_f, reminder_type_name_bk, reminder_period_bk, 
	reorder_f, 
	referafriend_code, days_worn_info, 
	migrate_customer_f, migrate_customer_store, migrate_customer_id, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.gen_dim_customer_aud