use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.aux.prod_product_category
create table aux.prod_product_category(
	product_id				int NOT NULL,
	category_bk				varchar(50) NOT NULL,
	cl_type_bk				varchar(50), 
	cl_feature_bk			varchar(50));
go

 alter table Landing.aux.prod_product_category add constraint [PK_aux_prod_product_category]
	primary key clustered (product_id);
go


-- Landing.aux.prod_product_product_type_oh
create table aux.prod_product_product_type_oh(
	product_id				int NOT NULL,
	product_type_oh_bk		varchar(50) NOT NULL);
go

 alter table Landing.aux.prod_product_product_type_oh add constraint [PK_prod_product_product_type_oh]
	primary key clustered (product_id);
go


-- Landing.aux.prod_product_product_type_vat
create table aux.prod_product_product_type_vat(
	product_id				int NOT NULL,
	product_type_vat		varchar(255) NOT NULL);
go

 alter table Landing.aux.prod_product_product_type_vat add constraint [PK_prod_product_product_type_vat]
	primary key clustered (product_id);
go




-- Landing.aux.mag_core_config_data_store
create table aux.mag_core_config_data_store(
	store_id			int NOT NULL, 
	scope_id			int NOT NULL,
	path				varchar(255) NOT NULL,
	value				varchar(1000))
go

 alter table Landing.aux.mag_core_config_data_store add constraint [PK_aux_mag_core_config_data_store]
	primary key clustered (store_id, path);
go


-- Landing.aux.mag_sales_flat_creditmemo
create table aux.mag_sales_flat_creditmemo(
	order_id							int NOT NULL,
	num_order							int NOT NULL,  
	entity_id							int NOT NULL,
	increment_id						varchar(50),
	created_at							datetime,
	num_rep_rows						int, 
	sum_base_subtotal					decimal(12,4), 
	sum_base_shipping_amount			decimal(12,4), 
	sum_base_discount_amount			decimal(12,4), 
	sum_base_customer_balance_amount	decimal(12,4), 
	sum_base_grand_total				decimal(12,4), 
	sum_base_adjustment					decimal(12,4), 
	sum_base_adjustment_positive		decimal(12,4), 
	sum_base_adjustment_negative		decimal(12,4)
	)
go

 alter table Landing.aux.mag_sales_flat_creditmemo add constraint [PK_aux_mag_sales_flat_creditmemo]
	primary key clustered (order_id, num_order);
go



-- Landing.aux.mag_sales_flat_creditmemo_item
create table aux.mag_sales_flat_creditmemo_item(
	order_id				int NOT NULL, 
	order_item_id			int NOT NULL, 
	entity_id				int NOT NULL,
	increment_id			varchar(50),
	item_id					int NOT NULL,
	created_at				datetime,
	num_rep_rows			int, 
	sum_qty					decimal(12,4), 
	sum_base_row_total		decimal(12,4), 
	sum_base_discount_amount decimal(12,4));
	
go

 alter table Landing.aux.mag_sales_flat_creditmemo_item add constraint [PK_aux_mag_sales_flat_creditmemo_item]
	primary key clustered (order_id, order_item_id);
go



-- Landing.aux.mag_edi_stock_item
create table aux.mag_edi_stock_item(
	product_id				int NOT NULL,
	product_code			varchar(277) NOT NULL,
	BC						char(3),
	DI						char(4),
	PO						char(6),
	CY						char(5),
	AX						char(3),
	AD						char(4),
	DO						char(1),
	CO						varchar(100));
	
go

 alter table Landing.aux.mag_edi_stock_item add constraint [PK_aux_mag_edi_stock_item]
	primary key clustered (product_id, product_code);
go


-- Landing.aux.mag_catalog_product_price
create table aux.mag_catalog_product_price(
	store_id				int NOT NULL, 
	store_name				varchar(255) NOT NULL, 
	product_id				int NOT NULL, 
	product_name			varchar(255) NOT NULL, 
	product_lifecycle		varchar(255),  
	value					decimal(12, 4),
	qty						int NOT NULL, 
	min_qty_prod			int,
	num_packs_tier_price	int, 
	total_price				decimal(12, 4),
	pack_price				decimal(12, 4),
	disc_percent			decimal(12, 4),
	max_pack_price			decimal(12, 4),
	promo_key				varchar(40), 
	price_type				varchar(20) NOT NULL, 
	pla_type				char(3) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go
alter table Landing.aux.mag_catalog_product_price add constraint [PK_aux_mag_catalog_product_price]
	primary key clustered (store_id, product_id, qty, price_type, pla_type);
go
alter table Landing.aux.mag_catalog_product_price add constraint [DF_aux_mag_catalog_product_price_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

create table aux.mag_catalog_product_price_aud(
	store_id				int NOT NULL, 
	store_name				varchar(255) NOT NULL, 
	product_id				int NOT NULL, 
	product_name			varchar(255) NOT NULL, 
	product_lifecycle		varchar(255),  
	value					decimal(12, 4),
	qty						int NOT NULL, 
	min_qty_prod			int,
	num_packs_tier_price	int, 
	total_price				decimal(12, 4),
	pack_price				decimal(12, 4),
	disc_percent			decimal(12, 4),
	max_pack_price			decimal(12, 4),
	promo_key				varchar(40), 
	price_type				varchar(20) NOT NULL, 
	pla_type				char(3) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime	
	);
	
go
alter table Landing.aux.mag_catalog_product_price_aud add constraint [PK_aux_mag_catalog_product_price_aud]
	primary key clustered (store_id, product_id, qty, price_type, pla_type);
go
alter table Landing.aux.mag_catalog_product_price_aud add constraint [DF_aux_mag_catalog_product_price_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

create table aux.mag_catalog_product_price_aud_hist(
	store_id				int NOT NULL, 
	store_name				varchar(255) NOT NULL, 
	product_id				int NOT NULL, 
	product_name			varchar(255) NOT NULL, 
	product_lifecycle		varchar(255),  
	value					decimal(12, 4),
	qty						int NOT NULL, 
	min_qty_prod			int,
	num_packs_tier_price	int, 
	total_price				decimal(12, 4),
	pack_price				decimal(12, 4),
	disc_percent			decimal(12, 4),
	max_pack_price			decimal(12, 4),
	promo_key				varchar(40), 
	price_type				varchar(20) NOT NULL, 
	pla_type				char(3) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	aud_type				char(1) NOT NULL, 
	aud_dateFrom			datetime NOT NULL, 
	aud_dateTo				datetime NOT NULL);
	
go
alter table Landing.aux.mag_catalog_product_price_aud_hist add constraint [DF_aux_mag_catalog_product_price_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.aux.gen_customer_unsubscribe_magento
create table aux.gen_customer_unsubscribe_magento(
	customer_id_bk								int NOT NULL, 	
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d			int);
go

 alter table Landing.aux.gen_customer_unsubscribe_magento add constraint [PK_aux_gen_customer_unsubscribe_magento]
	primary key clustered (customer_id_bk);
go


-- Landing.aux.gen_customer_unsubscribe_dotmailer
create table aux.gen_customer_unsubscribe_dotmailer(
	customer_id_bk								int NOT NULL, 	
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d			int,);
go

 alter table Landing.aux.gen_customer_unsubscribe_dotmailer add constraint [PK_aux_gen_customer_unsubscribe_dotmailer]
	primary key clustered (customer_id_bk);
go


-- Landing.aux.gen_customer_unsubscribe_textmessage
create table aux.gen_customer_unsubscribe_textmessage(
	customer_id_bk								int NOT NULL, 	
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d					int);
go

 alter table Landing.aux.gen_customer_unsubscribe_textmessage add constraint [PK_aux_gen_customer_unsubscribe_textmessage]
	primary key clustered (customer_id_bk);
go


-- Landing.aux.gen_customer_reminder
create table aux.gen_customer_reminder(
	customer_id_bk								int NOT NULL, 	
	reminder_f									char(1) NOT NULL,
	reminder_type_name_bk						varchar(50),
	reminder_period_bk							int);
go

 alter table Landing.aux.gen_customer_reminder add constraint [PK_aux_gen_customer_reminder]
	primary key clustered (customer_id_bk);
go


-- Landing.aux.gen_customer_reorder
create table aux.gen_customer_reorder(
	customer_id_bk								int NOT NULL, 	
	reorder_f									char(1) NOT NULL);
go

 alter table Landing.aux.gen_customer_reorder add constraint [PK_aux_gen_customer_reorder]
	primary key clustered (customer_id_bk);
go




-- Landing.aux.sales_order_header_o
create table aux.sales_order_header_o(
	order_id_bk						bigint NOT NULL);
go

 alter table Landing.aux.sales_order_header_o add constraint [PK_aux_sales_order_header_o]
	primary key clustered (order_id_bk);
go

-- Landing.aux.sales_order_header_o_i_s_cr
create table aux.sales_order_header_o_i_s_cr(
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	idCalendarOrderDate				int NOT NULL, 
	idTimeOrderDate					varchar(10) NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime);
go

 alter table Landing.aux.sales_order_header_o_i_s_cr add constraint [PK_aux_sales_order_header_o_i_s_cr]
	primary key clustered (order_id_bk);
go


-- Landing.aux.sales_order_header_store_cust
create table aux.sales_order_header_store_cust(
	order_id_bk						bigint NOT NULL,
	store_id_bk						int NOT NULL,
	market_id_bk					int,
	customer_id_bk					int NOT NULL,
	shipping_address_id				int,
	billing_address_id				int,
	country_id_shipping_bk			char(2), 
	region_id_shipping_bk			int,
	postcode_shipping				varchar(255), 
	country_id_billing_bk			char(2),
	postcode_billing				varchar(255), 
	region_id_billing_bk			int, 
	customer_unsubscribe_name_bk	varchar(10));
go

 alter table Landing.aux.sales_order_header_store_cust add constraint [PK_aux_sales_order_header_store_cust]
	primary key clustered (order_id_bk);
go


-- Landing.aux.sales_order_header_dim_order
create table aux.sales_order_header_dim_order(
	order_id_bk						bigint NOT NULL, 
	order_stage_name_bk				varchar(20) NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	adjustment_order				char(1),
	order_type_name_bk				varchar(20),
	status_bk						varchar(32),
	payment_method_name_bk			varchar(20),
	cc_type_name_bk					varchar(50),
	payment_id						int,
	payment_method_code				varchar(255),
	shipping_description_bk			varchar(255),
	shipping_description_code		varchar(255),
	telesales_admin_username_bk		varchar(40), 
	prescription_method_name_bk		varchar(50),
	reminder_type_name_bk			varchar(50),
	reminder_period_bk				int, 
	reminder_date					datetime,
	reminder_sent					int,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,
	reorder_profile_id				int,
	automatic_reorder				int,
	order_source					char(1),
	proforma						char(1),
	aura_product_p					decimal(12, 4),
	product_type_oh_bk				varchar(50),
	order_qty_time					int,
	num_diff_product_type_oh		int,
	referafriend_code				varchar(10),
	referafriend_referer			int,
	postoptics_auto_verification	varchar(255),
	presc_verification_method		varchar(255),
	warehouse_approved_time			datetime
	);
go

 alter table Landing.aux.sales_order_header_dim_order add constraint [PK_aux_sales_order_header_dim_order]
	primary key clustered (order_id_bk);
go


-- Landing.aux.sales_order_header_dim_mark
create table aux.sales_order_header_dim_mark(
	order_id_bk						bigint NOT NULL, 
	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	publisher_id_bk					int, 
	affilCode						varchar(255),
	ga_medium						varchar(100),
	ga_source						varchar(100),
	ga_channel						varchar(100),
	telesales_f						char(1),
	sms_auto_f						char(1),
	mutuelles_f						char(1),
	coupon_id_bk					int,
	coupon_code						varchar(255),
	applied_rule_ids				varchar(255),
	device_model_name_bk			varchar(255), 
	device_browser_name_bk			varchar(50),  
	device_os_name_bk				varchar(50), 
	ga_device_model					varchar(255),
	ga_device_browser				varchar(50),
	ga_device_os					varchar(50));
go

 alter table Landing.aux.sales_order_header_dim_mark add constraint [PK_aux_sales_order_header_dim_mark]
	primary key clustered (order_id_bk);
go


-- Landing.aux.sales_order_header_measures
create table aux.sales_order_header_measures(
	order_id_bk						bigint NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_inc_vat_2 decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int);
go

 alter table Landing.aux.sales_order_header_measures add constraint [PK_aux_sales_order_header_measures]
	primary key clustered (order_id_bk);
go

-- Landing.aux.sales_order_header_vat
create table aux.sales_order_header_vat(
	order_id_bk						bigint NOT NULL,
	countries_registered_code		varchar(10) NOT NULL,
	invoice_date					datetime,

	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4));
go

 alter table Landing.aux.sales_order_header_vat add constraint [PK_aux_sales_order_header_vat]
	primary key clustered (order_id_bk);
go


-- Landing.aux.sales_order_header_oh_pt
create table aux.sales_order_header_oh_pt(
	order_id_bk						bigint NOT NULL, 
	product_type_oh_bk				varchar(50) NOT NULL, 
	order_percentage				decimal(12, 2), 
	order_flag						bit, 
	order_qty_time					int);
go

 alter table Landing.aux.sales_order_header_oh_pt add constraint [PK_aux_sales_order_header__oh_pt]
	primary key clustered (order_id_bk, product_type_oh_bk);
go





-- Landing.aux.sales_order_line_o_i_s_cr
create table aux.sales_order_line_o_i_s_cr(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	invoice_line_id					int,
	shipment_line_id				int,
	creditmemo_line_id				int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	
	num_shipment_lines				int,
	num_creditmemo_lines			int,

	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime, 
	
	warehouse_id_bk					bigint NOT NULL);
go

 alter table Landing.aux.sales_order_line_o_i_s_cr add constraint [PK_aux_sales_order_line_o_i_s_cr]
	primary key clustered (order_line_id_bk);
go


-- Landing.aux.sales_order_line_product
create table aux.sales_order_line_product(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	product_id_bk					int NOT NULL, 
	base_curve_bk					char(3),
	diameter_bk						char(4),
	power_bk						char(6),
	cylinder_bk						char(5),
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1), 
	colour_bk						varchar(50), 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	glass_vision_type_bk			varchar(50),
	order_line_id_vision_type		bigint, 
	glass_package_type_bk			varchar(50),
	order_line_id_package_type		bigint, 

	aura_product_f					char(1), 
	product_type_oh_bk				varchar(50))
go

 alter table Landing.aux.sales_order_line_product add constraint [PK_aux_sales_order_line_product]
	primary key clustered (order_line_id_bk);
go


-- Landing.aux.sales_order_line_measures
create table aux.sales_order_line_measures(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	order_status_name_bk			varchar(20) NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int);
go

 alter table Landing.aux.sales_order_line_measures add constraint [PK_aux_sales_order_line_measures]
	primary key clustered (order_line_id_bk);
go


-- Landing.aux.sales_order_line_vat
create table aux.sales_order_line_vat(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	countries_registered_code		varchar(10) NOT NULL,
	product_type_vat				varchar(255),
	vat_type_rate_code				char(1), 
	invoice_date					datetime,

	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4));
go

 alter table Landing.aux.sales_order_line_vat add constraint [PK_aux_sales_order_line_vat]
	primary key clustered (order_line_id_bk);
go

-----------------------------------------------------------------

-- Landing.aux.sales_dim_order_header
create table aux.sales_dim_order_header(
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	idCalendarOrderDate				int NOT NULL, 
	idTimeOrderDate					varchar(10) NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime,

	store_id_bk						int NOT NULL,
	market_id_bk					int,
	customer_id_bk					int NOT NULL,
	shipping_address_id				int,
	billing_address_id				int,
	country_id_shipping_bk			char(2), 
	region_id_shipping_bk			int,
	postcode_shipping				varchar(255), 
	country_id_billing_bk			char(2),
	postcode_billing				varchar(255), 
	region_id_billing_bk			int,
	customer_unsubscribe_name_bk	varchar(10),

	order_stage_name_bk				varchar(20) NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	adjustment_order				char(1),
	order_type_name_bk				varchar(20),
	status_bk						varchar(32),
	payment_method_name_bk			varchar(20),
	cc_type_name_bk					varchar(50),
	payment_id						int,
	payment_method_code				varchar(255),
	shipping_description_bk			varchar(255),
	shipping_description_code		varchar(255),
	telesales_admin_username_bk		varchar(40), 
	prescription_method_name_bk		varchar(50),
	reminder_type_name_bk			varchar(50),
	reminder_period_bk				int, 
	reminder_date					datetime,
	reminder_sent					int,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,
	reorder_profile_id				int,
	automatic_reorder				int,
	order_source					char(1),
	proforma						char(1),
	aura_product_p					decimal(12, 4),
	product_type_oh_bk				varchar(50),
	order_qty_time					int,
	num_diff_product_type_oh		int,
	referafriend_code				varchar(10),
	referafriend_referer			int,
	postoptics_auto_verification	varchar(255),
	presc_verification_method		varchar(255),
	warehouse_approved_time			datetime,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	publisher_id_bk					int, 
	affilCode						varchar(255),
	ga_medium						varchar(100),
	ga_source						varchar(100),
	ga_channel						varchar(100), 
	telesales_f						char(1), 
	sms_auto_f						char(1),
	mutuelles_f						char(1),
	coupon_id_bk					int,
	coupon_code						varchar(255),
	applied_rule_ids				varchar(255),
	device_model_name_bk			varchar(255), 
	device_browser_name_bk			varchar(50),  
	device_os_name_bk				varchar(50), 
	ga_device_model					varchar(255),
	ga_device_browser				varchar(50),
	ga_device_os					varchar(50),

	countries_registered_code		varchar(10), 

	price_type_name_bk				varchar(40),
	discount_f						char(1),
	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_inc_vat_2 decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int, 


	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Landing.aux.sales_dim_order_header add constraint [PK_aux_sales_dim_order_header]
	primary key clustered (order_id_bk);
go

alter table Landing.aux.sales_dim_order_header add constraint [DF_aux_sales_dim_order_header_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



create table aux.sales_dim_order_header_aud(
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	idCalendarOrderDate				int NOT NULL, 
	idTimeOrderDate					varchar(10) NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime,

	store_id_bk						int NOT NULL,
	market_id_bk					int,
	customer_id_bk					int NOT NULL,
	shipping_address_id				int,
	billing_address_id				int,
	country_id_shipping_bk			char(2), 
	region_id_shipping_bk			int,
	postcode_shipping				varchar(255), 
	country_id_billing_bk			char(2),
	postcode_billing				varchar(255), 
	region_id_billing_bk			int,
	customer_unsubscribe_name_bk	varchar(10),

	order_stage_name_bk				varchar(20) NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	adjustment_order				char(1),
	order_type_name_bk				varchar(20),
	status_bk						varchar(32),
	payment_method_name_bk			varchar(20),
	cc_type_name_bk					varchar(50),
	payment_id						int,
	payment_method_code				varchar(255),
	shipping_description_bk			varchar(255),
	shipping_description_code		varchar(255),
	telesales_admin_username_bk		varchar(40), 
	prescription_method_name_bk		varchar(50),
	reminder_type_name_bk			varchar(50),
	reminder_period_bk				int, 
	reminder_date					datetime,
	reminder_sent					int,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,
	reorder_profile_id				int,
	automatic_reorder				int,
	order_source					char(1),
	proforma						char(1),
	aura_product_p					decimal(12, 4),
	product_type_oh_bk				varchar(50),
	order_qty_time					int,
	num_diff_product_type_oh		int,
	referafriend_code				varchar(10),
	referafriend_referer			int,
	postoptics_auto_verification	varchar(255),
	presc_verification_method		varchar(255),
	warehouse_approved_time			datetime,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	publisher_id_bk					int, 
	affilCode						varchar(255),
	ga_medium						varchar(100),
	ga_source						varchar(100),
	ga_channel						varchar(100), 
	telesales_f						char(1), 
	sms_auto_f						char(1),
	mutuelles_f						char(1),
	coupon_id_bk					int,
	coupon_code						varchar(255),
	applied_rule_ids				varchar(255),
	device_model_name_bk			varchar(255), 
	device_browser_name_bk			varchar(50),  
	device_os_name_bk				varchar(50), 
	ga_device_model					varchar(255),
	ga_device_browser				varchar(50),
	ga_device_os					varchar(50),

	countries_registered_code		varchar(10), 

	price_type_name_bk				varchar(40),
	discount_f						char(1),
	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_inc_vat_2 decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int, 


	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	upd_ts							datetime);
go 

alter table Landing.aux.sales_dim_order_header_aud add constraint [PK_aux_sales_dim_order_header_aud]
	primary key clustered (order_id_bk);
go

alter table Landing.aux.sales_dim_order_header_aud add constraint [DF_aux_sales_dim_order_header_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create index [aux_sales_dim_order_header_aud_customer_id_bk] on Landing.aux.sales_dim_order_header_aud(customer_id_bk)

-- Landing.aux.sales_fact_order_line
create table aux.sales_fact_order_line(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	invoice_line_id					int,
	shipment_line_id				int,
	creditmemo_line_id				int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	
	num_shipment_lines				int,
	num_creditmemo_lines			int,

	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime, 
	
	warehouse_id_bk					bigint NOT NULL, 

	product_id_bk					int NOT NULL, 
	base_curve_bk					char(3),
	diameter_bk						char(4),
	power_bk						char(6),
	cylinder_bk						char(5),
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1), 
	colour_bk						varchar(50), 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	glass_vision_type_bk			varchar(50),
	order_line_id_vision_type		bigint, 
	glass_package_type_bk			varchar(50),
	order_line_id_package_type		bigint, 

	aura_product_f					char(1), 

	countries_registered_code		varchar(10),
	product_type_vat				varchar(255),

	order_status_name_bk			varchar(20) NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int, 
	
	
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go

alter table Landing.aux.sales_fact_order_line add constraint [PK_aux_sales_fact_order_line]
	primary key clustered (order_line_id_bk);
go

alter table Landing.aux.sales_fact_order_line add constraint [DF_aux_sales_fact_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table aux.sales_fact_order_line_aud(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	invoice_line_id					int,
	shipment_line_id				int,
	creditmemo_line_id				int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	
	num_shipment_lines				int,
	num_creditmemo_lines			int,

	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime, 
	
	warehouse_id_bk					bigint NOT NULL, 

	product_id_bk					int NOT NULL, 
	base_curve_bk					char(3),
	diameter_bk						char(4),
	power_bk						char(6),
	cylinder_bk						char(5),
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1), 
	colour_bk						varchar(50), 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	glass_vision_type_bk			varchar(50),
	order_line_id_vision_type		bigint, 
	glass_package_type_bk			varchar(50),
	order_line_id_package_type		bigint, 

	aura_product_f					char(1), 

	countries_registered_code		varchar(10),
	product_type_vat				varchar(255),

	order_status_name_bk			varchar(20) NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int, 
	
	
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime)
go

alter table Landing.aux.sales_fact_order_line_aud add constraint [PK_aux_sales_fact_order_line_aud]
	primary key clustered (order_line_id_bk);
go

alter table Landing.aux.sales_fact_order_line_aud add constraint [DF_aux_sales_fact_order_line_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

create index [aux_sales_fact_order_line_aud_order_id_bk] on Landing.aux.sales_fact_order_line_aud(order_id_bk)



-- Landing.aux.act_fact_activity_sales
create table aux.act_fact_activity_sales(
	order_id_bk						bigint NOT NULL, 

	idCalendarActivityDate			int NOT NULL, 
	activity_date					datetime NOT NULL,
	prev_activity_date				datetime,
	next_activity_date				datetime,
	idCalendarPrevActivityDate		int, 
	idCalendarNextActivityDate		int, 

	activity_type_name_bk			varchar(50) NOT NULL,
	store_id_bk						int NOT NULL,
	customer_id_bk					int NOT NULL,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	product_type_oh_bk				varchar(50),  
	order_qty_time					int,  
	num_diff_product_type_oh		int,

	customer_status_name_bk			varchar(50), 
	customer_order_seq_no			int, 
	customer_order_seq_no_web		int, 

	local_subtotal					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),

	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255));
go

alter table Landing.aux.act_fact_activity_sales add constraint [PK_act_fact_activity_sales]
	primary key clustered (order_id_bk);
go

--alter table Landing.aux.act_fact_activity_sales add constraint [UNIQ_act_fact_activity_sales_cust_act_type_order_seq_no]
--	unique (customer_id_bk, activity_type_name_bk, customer_order_seq_no);
--go


-- Landing.aux.act_fact_activity_other
create table aux.act_fact_activity_other(
	activity_id_bk					bigint NOT NULL, 

	idCalendarActivityDate			int NOT NULL, 
	activity_date					datetime NOT NULL,
	idCalendarPrevActivityDate		int, 

	activity_type_name_bk			varchar(50) NOT NULL,
	store_id_bk						int NOT NULL,
	customer_id_bk					int NOT NULL,
	
	activity_seq_no					int);
go

alter table Landing.aux.act_fact_activity_other add constraint [PK_act_fact_activity_other]
	primary key clustered (activity_id_bk, activity_type_name_bk, idCalendarActivityDate);
go

alter table Landing.aux.act_fact_activity_other add constraint [UNIQ_act_fact_activity_other_cust_act_type_act_act_date]
	unique (customer_id_bk, activity_type_name_bk, activity_date);
go

alter table Landing.aux.act_fact_activity_other add constraint [UNIQ_act_fact_activity_other_cust_act_type_act_seq_no]
	unique (customer_id_bk, activity_type_name_bk, activity_seq_no);
go





-- Landing.aux.act_customer_registration
create table aux.act_customer_registration(
	customer_id_bk			int NOT NULL,
	store_id_bk				int NOT NULL, 
	idCalendarRegisterDate	int NOT NULL,
	register_date			datetime NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go
alter table Landing.aux.act_customer_registration add constraint [PK_aux_act_customer_registration]
	primary key clustered (customer_id_bk);
go
alter table Landing.aux.act_customer_registration add constraint [DF_aux_act_customer_registration_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.aux.act_customer_registration_aud
create table aux.act_customer_registration_aud(
	customer_id_bk			int NOT NULL,
	store_id_bk				int NOT NULL, 
	idCalendarRegisterDate	int NOT NULL,
	register_date			datetime NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	upd_ts					datetime);
go
alter table Landing.aux.act_customer_registration_aud add constraint [PK_aux_act_customer_registration_aud]
	primary key clustered (customer_id_bk);
go
alter table Landing.aux.act_customer_registration_aud add constraint [DF_aux_act_customer_registration_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

 

create table aux.act_customer_cr_reg(
	customer_id_bk						int NOT NULL,
	email								varchar(255),
	store_id							int, 
	created_at							datetime NOT NULL,

	old_access_cust_no					varchar(255),
	store_id_oacn						int,
	website_group_oacn					varchar(255),

	migrate_db							varchar(25),
	migrate_old_customer_id				bigint, 
	migrate_created_at					datetime,
				
	num_dist_websites					int,

	store_id_first_all					int, 
	first_all_date						datetime,

	store_id_first_vd					int,
	first_vd_date						datetime,

	store_id_first_non_vd				int,
	first_non_vd_date					datetime,

	store_id_last_non_vd				int,
	last_non_vd_date					datetime,

	non_vd_customer_status_at_migration_date	varchar(50),

	store_id_last_vd_before_migration	int,
	last_vd_before_migration_date		datetime,
	customer_status_at_migration_date	varchar(50),

	migration_date						datetime,

	store_id_last_vd_before_oacn		int,
	last_vd_before_oacn_date			datetime,
	customer_status_at_oacn_date		varchar(50),

	migration_date_oacn					datetime,

	store_id_bk_cr						int NOT NULL,
	create_date							datetime NOT NULL,	

	store_id_bk_reg						int NOT NULL,
	register_date						datetime NOT NULL,
	
	
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint,
	upd_ts								datetime)
go

alter table Landing.aux.act_customer_cr_reg add constraint [PK_aux_act_customer_cr_reg]
	primary key clustered (customer_id_bk);
go

alter table Landing.aux.act_customer_cr_reg add constraint [DF_aux_act_customer_cr_reg_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Landing.aux.sales_exchange_rate(
	exchange_rate_day		date NOT NULL,
	eur_to_gbp				decimal(12, 4) NOT NULL, 
	gbp_to_eur				decimal(12, 4) NOT NULL, 

	idETLBatchRun_ins		bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	idETLBatchRun_upd		bigint, 
	upd_ts					datetime);
go 

alter table Landing.aux.sales_exchange_rate add constraint [PK_aux_sales_exchange_rate]
	primary key clustered (exchange_rate_day);
go

alter table Landing.aux.sales_exchange_rate add constraint [DF_aux_sales_exchange_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.aux.gen.dim_customer
create table Landing.aux.gen_dim_customer(
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime, 
	updated_at									datetime, 
	store_id_bk									int NOT NULL, 
	market_id_bk								int, 
	customer_type_name_bk						varchar(50) NOT NULL, 
	customer_status_name_bk						varchar(50),
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	region_id_bk								int, 
	country_id_bk								char(2), 
	postcode_s									varchar(255),
	country_id_s_bk								char(2), 
	customer_unsubscribe_name_bk				varchar(10),
	customer_unsubscribe_d						int,
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d			int,		
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d			int,		
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d					int,
	reminder_f									char(1) NOT NULL,
	reminder_type_name_bk						varchar(50),
	reminder_period_bk							int,
	reorder_f									char(1) NOT NULL,
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	migrate_customer_f							char(1) NOT NULL,
	migrate_customer_store						varchar(255),
	migrate_customer_id							bigint,

	idETLBatchRun								bigint NOT NULL, 
	ins_ts										datetime NOT NULL);
go 

alter table Landing.aux.gen_dim_customer add constraint [PK_aux_gen_dim_customer]
	primary key clustered (customer_id_bk);
go

alter table Landing.aux.gen_dim_customer add constraint [DF_aux_gen_dim_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


-- Landing.aux.gen.dim_customer_aud
create table Landing.aux.gen_dim_customer_aud(
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime, 
	updated_at									datetime, 
	store_id_bk									int NOT NULL, 
	market_id_bk								int, 
	customer_type_name_bk						varchar(50) NOT NULL, 
	customer_status_name_bk						varchar(50),
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	region_id_bk								int, 
	country_id_bk								char(2), 
	postcode_s									varchar(255),
	country_id_s_bk								char(2), 
	customer_unsubscribe_name_bk				varchar(10),
	customer_unsubscribe_d						int,
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d			int,		
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d			int,		
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d					int,
	reminder_f									char(1) NOT NULL,
	reminder_type_name_bk						varchar(50),
	reminder_period_bk							int,
	reorder_f									char(1) NOT NULL,
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	migrate_customer_f							char(1) NOT NULL,
	migrate_customer_store						varchar(255),
	migrate_customer_id							bigint,

	idETLBatchRun								bigint NOT NULL, 
	ins_ts										datetime NOT NULL, 
	upd_ts										datetime);
go 

alter table Landing.aux.gen_dim_customer_aud add constraint [PK_aux_gen_dim_customer_aud]
	primary key clustered (customer_id_bk);
go

alter table Landing.aux.gen_dim_customer_aud add constraint [DF_aux_gen_dim_customer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
