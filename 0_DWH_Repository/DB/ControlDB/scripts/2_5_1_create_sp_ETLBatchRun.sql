use ControlDB
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Start record for the ETL Batch Run to the logging Table - Return Unique Record Number
-- ==========================================================================================

create procedure logging.logETLBatchRun_start_sp
	@codETLBatchType varchar(20), @codFlowType varchar(20), 
	@package_name varchar(100),
	@description varchar(255),
	@dateFrom datetime, @dateTo datetime, 
	@idETLBatchRun	bigint OUTPUT
as

begin
	set nocount on;

	insert into ControlDB.logging.t_ETLBatchRun (idETLBatchType, idFlowType, idPackage, description, dateFrom, dateTo, 
		startTime, runStatus)

		select bt.idETLBatchType, ft.idFlowType, p.idPackage, t.description, t.dateFrom, t.dateTo, 
			getutcdate() startTime, 'RUNNING' runStatus
		from
				(select @codETLBatchType codETLBatchType, @codFlowType codFlowType, @package_name package_name, @description description, @dateFrom dateFrom, @dateTo dateTo) t
			inner join
				ControlDB.config.cat_ETLBatchType bt on t.codETLBatchType = bt.codETLBatchType
			inner join
				ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
			inner join
				ControlDB.config.t_package p on t.package_name = p.package_name;

	set @idETLBatchRun = SCOPE_IDENTITY();

	return @idETLBatchRun;
end 

go 


-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Finish record for the ETL Batch Run to the logging Table 
-- ==========================================================================================

create procedure logging.logETLBatchRun_stop_sp
	@idETLBatchRun	bigint,
	@runStatus varchar(50)--, @message varchar(500)
as

begin
	set nocount on;

	update ControlDB.logging.t_ETLBatchRun
	set
		finishTime = getutcdate(), 
		runStatus = @runStatus--, message = @message
	where idETLBatchRun = @idETLBatchRun;
end 	 

go 