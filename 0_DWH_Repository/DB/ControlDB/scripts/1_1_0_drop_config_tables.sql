
use ControlDB
go 

drop table ControlDB.config.t_SP_ssrs
go 

drop table ControlDB.config.t_SP
go 

drop table ControlDB.config.t_Package
go 

drop table ControlDB.config.t_Table
go 

drop table ControlDB.config.t_Flow
go 

drop table ControlDB.config.t_BusinessArea
go 

drop table ControlDB.config.t_Developer
go 

drop table ControlDB.config.t_Database
go 

-- Catalog Tables

drop table ControlDB.config.cat_DatabaseType;
go 

drop table ControlDB.config.cat_DWHTableType;
go 

drop table ControlDB.config.cat_FlowType;
go 

drop table ControlDB.config.cat_PackageType;
go 

drop table ControlDB.config.cat_SPType;
go 

drop table ControlDB.config.cat_ETLBatchType;
go 

