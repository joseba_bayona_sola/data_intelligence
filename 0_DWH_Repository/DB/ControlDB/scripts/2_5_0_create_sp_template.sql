
use ControlDB
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Start record for the ETL Batch Run to the logging Table - Return Unique Record Number
-- ==========================================================================================

create procedure logging.logETLBatchRun_start_sp

as

begin
	set nocount on;

end 

go 