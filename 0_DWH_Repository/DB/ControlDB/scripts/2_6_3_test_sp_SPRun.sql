USE ControlDB
GO

DECLARE	@return_value int, @idSPRun bigint

	EXEC @return_value = logging.logSPRun_start_sp
			@sp_name = 'lnd_stg_get_gen_store', 
			@idETLBatchRun = 1, @idPackageRun = 1,  
			@idSPRun = @idSPRun OUTPUT

	SELECT	@idSPRun as '@idSPRun'
	SELECT	'Return Value' = @return_value

GO

	EXEC logging.logSPRun_message_sp 
		@idSPRun = 1,
		@rowAmountSelect = 50, @message = '50 rows read '

	go

	EXEC logging.logSPRun_stop_sp 
		@idSPRun = 1,
		@runStatus = 'COMPLETE'

	go
