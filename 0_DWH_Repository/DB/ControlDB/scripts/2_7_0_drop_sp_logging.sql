
use ControlDB
go 

drop procedure logging.logETLBatchRun_start_sp
go
drop procedure logging.logETLBatchRun_stop_sp
go

drop procedure logging.logPackageRun_start_sp
go
drop procedure logging.logPackageRun_stop_sp
go
drop procedure logging.logPackageRun_message_sp
go

drop procedure logging.logSPRun_start_sp
go
drop procedure logging.logSPRun_stop_sp
go
drop procedure logging.logSPRun_message_sp
go


