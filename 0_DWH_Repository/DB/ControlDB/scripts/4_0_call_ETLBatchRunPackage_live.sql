
use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Live'

	select @dateFromV = convert(varchar, '2017-11-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-12-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - 2017-11 Fix Mutuelle', @package_name_call = 'VisionDirectDataWarehouseLoad' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Live'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - LAPSED Fix Testing', @package_name_call = 'VisionDirectDataWarehouseLoadNoSRC' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Live'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Test Env Name Change', @package_name_call = 'VisionDirectDataWarehouseLoadCustomers' 

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @migration_type varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMigration.dtsx', @environment_name = 'Live'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Migrate - Lensway NL 2008, 09, 10, 11', @package_name_call = 'VisionDirectDataWarehouseLoadMigration' -- VisionDirectDataWarehouseLoad

	select @migration_type = 'migrate'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" /Par "\"migration_type\"";"\"' + @migration_type + '\""'

		

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMendix.dtsx', @environment_name = 'Live'

	select @dateFromV = convert(varchar, '2017-11-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-12-01 00:00:00', 120) 

	select @description = 'VD Mendix Full Load - WH Allocation', @package_name_call = 'LoadAll_src_lnd_mendix_one' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

