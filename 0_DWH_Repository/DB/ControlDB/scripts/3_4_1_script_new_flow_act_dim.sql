use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'act' codBusinessArea, 
				'Activity Dim' name, 'Data about Customer Activity - DIM Tables' description
			union
			select 'WH-DWH' codFlowType, 'FACT' codDWHTableType, 'act' codBusinessArea, 
				'Activity Fact' name, 'Data about Customer Activity - FACT Tables' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Activity Dim' flow_name, 'dim_activity_group' name, 'Staging Table for Activity Group: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Dim' flow_name, 'dim_activity_group' name, 'Warehouse Table for Activity Group: Activity Group Dimension' description
			union
			select 'Staging' database_name, 'Activity Dim' flow_name, 'dim_activity_type' name, 'Staging Table for Activity Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Dim' flow_name, 'dim_activity_type' name, 'Warehouse Table for Activity Type: Activity Type Dimension' description

			union
			select 'Staging' database_name, 'Activity Fact' flow_name, 'fact_activity_sales' name, 'Staging Table for Activity Sales: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_activity_sales' name, 'Warehouse Table for Activity Sales: Activity Sales Fact' description
			union
			select 'Staging' database_name, 'Activity Fact' flow_name, 'fact_activity_other' name, 'Staging Table for Activity Other: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_activity_other' name, 'Warehouse Table for Activity Other: Activity Other Fact' description
			
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature' name, 'Warehouse Table for Customer Signature: Info about Customer' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'dim_customer_signature_v' name, 'Warehouse Table for Customer Signature: Dim Table for Joins' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'dim_customer_signature_scd' name, 'Warehouse Table for Customer Signature	SCD: Customer Signature Auditing' description
		
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_other' name, 'Warehouse Aux Table for Customer Signature: Info from Activity Other' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales' name, 'Warehouse Aux Table for Customer Signature: Info from Activity Sales' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales_main' name, 'Warehouse Aux Table for Customer Signature: Info from Activity Sales Main' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_orders' name, 'Warehouse Aux Table for Customer Signature: Info from Activity Orders' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales_main_product' name, 'Warehouse Aux Table for Customer Signature: Activity Sales Main - Product' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales_main_freq' name, 'Warehouse Aux Table for Customer Signature: Activity Sales Main - Frequency' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales_main_ch' name, 'Warehouse Aux Table for Customer Signature: Activity Sales Main - Channel' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_sales_main_pr' name, 'Warehouse Aux Table for Customer Signature: Activity Sales Main - Price Type' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_signature_aux_customers' name, 'Warehouse Aux Table for Customer Signature: Customers to be updated' description

			union
			select 'Staging' database_name, 'Activity Fact' flow_name, 'dim_customer_cr_reg' name, 'Staging Table for Customer CREATE - REGISTER: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'dim_customer_cr_reg' name, 'Warehouse Table for Customer CREATE - REGISTER: Customer Info' description
			
			union
			select 'Staging' database_name, 'Activity Dim' flow_name, 'dim_rank_cr_time_mm' name, 'Staging Table for Rank Create Time MM: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Dim' flow_name, 'dim_rank_cr_time_mm' name, 'Warehouse Table for Rank Create Time MM: Rank Create Time MM Dimension' description
			
			union
			select 'Staging' database_name, 'Activity Fact' flow_name, 'fact_customer_product_signature' name, 'Staging Table for Customer Product Signature: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Activity Fact' flow_name, 'fact_customer_product_signature' name, 'Warehouse Table for Customer Product Signature: Customer Product Signature Fact' description

			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Activity Dim' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_act_dim_activity' name, 'SSIS PKG for reading Activity DIM data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Activity Dim' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_act_dim_activity' name, 'SSIS PKG for reading Activity DIM data from Staging to Warehouse - Update Dimension' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'Activity Fact' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_act_fact_activity' name, 'SSIS PKG for reading Activity FACT data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Activity Fact' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_act_fact_activity' name, 'SSIS PKG for reading Activity FACT data from Staging to Warehouse - Update Dimension' description
			union
			select 'DWH-DWH' codPackageType, 'jsola' developer,  'Activity Fact' flow_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_act_fact_customer_signature' name, 'SSIS PKG for reading Activity Tables Data and prepare Customer Signature data from Warehouse to Warehouse' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_dim_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_activity_group' name, 'GET SP for reading Activity Group data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_activity_group' name, 'GET SP for reading Activity Group data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_activity_group' name, 'MERGE SP for upading Activity Group Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_dim_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_activity_type' name, 'GET SP for reading Activity Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_activity_type' name, 'GET SP for reading Activity Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_activity_type' name, 'MERGE SP for upading Activity Type Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_activity_sales' name, 'GET SP for reading Activity Sales data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_activity_sales' name, 'GET SP for reading Activity Sales data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_activity_sales' name, 'MERGE SP for upading Activity Sales Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_activity_other' name, 'GET SP for reading Activity Other data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_activity_other' name, 'GET SP for reading Activity Other data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_activity_other' name, 'MERGE SP for upading Activity Other Dimension data' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_act_order_header_from_activity_sales' name, 'MERGE SP for upading Sales Order Header from Activity Sales' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_act_activity_sales' name, 'GET SP for for preparing Sales Activity Fact data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_act_activity_other' name, 'GET SP for for preparing Other Activity Fact data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_act_customer_registration' name, 'GET SP for for preparing Customer Register Activity data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_act_activity_sales_all_cust' name, 'GET SP for for preparing Sales - Other Activity Fact data for All Customers' description

			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_act_customer_signature' name, 'MERGE SP for upading Customer Signature data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature' name, 'GET SP for upading Customer Signature data' description

			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_ins_act_customer_signature_v' name, 'INS SP for upading Customer Signature DIM table' description

			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_other' name, 'GET SP for taking Customer Signature data - Other' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_sales' name, 'GET SP for taking Customer Signature data - Sales' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_sales_main' name, 'GET SP for taking Customer Signature data - Sales Main' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_orders' name, 'GET SP for taking Customer Signature data - Orders' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_sales_main_product' name, 'GET SP for taking Customer Signature data - Orders' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_sales_main_freq' name, 'GET SP for taking Customer Signature data - Orders' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_get_act_customer_signature_aux_sales_main_ch_pr' name, 'GET SP for taking Customer Signature data - Orders' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_customer_cr_reg' name, 'GET SP for for preparing CREATE - REGISTER data for All Customers' description
			union
			select 'GET' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_customer_cr_reg' name, 'GET SP for reading CREATE - REGISTER data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_act_fact_customer_signature' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_customer_cr_reg' name, 'MERGE SP for upading CREATE - REGISTER data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_act_customer_cr_reg' name, 'GET SP for for preparing CREATE - REGISTER data for All Customers - AUX Table' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_dim_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_rank_cr_time_mm' name, 'GET SP for reading Rank CR Time in MM data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_rank_cr_time_mm' name, 'GET SP for reading Rank CR Time in MM data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_dim_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_rank_cr_time_mm' name, 'MERGE SP for upading Rank CR Time in MM Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_act_fact_activity' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_act_customer_product_signature' name, 'GET SP for reading Customer Product Signature data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_act_customer_product_signature' name, 'GET SP for reading Customer Product Signature data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_act_fact_activity' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_act_customer_product_signature' name, 'MERGE SP for upading Customer Product Signature Dimension data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 