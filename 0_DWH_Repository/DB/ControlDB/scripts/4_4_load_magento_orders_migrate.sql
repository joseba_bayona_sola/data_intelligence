	delete from Landing.migra.sales_flat_order_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into Landing.migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' 
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name
		where moh.migra_website_name = 'vd150324' and year(created_at) in (2013, 2014) -- 


-------------------------------------------------------------------------------

	delete from Landing.migra.sales_flat_order_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into Landing.migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select oh.migra_website_name, oh.etl_name, 	
			oh.entity_id, oh.increment_id, oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, 
			oh.status, 
			oh.total_qty_ordered, 
			oh.base_subtotal, oh.base_shipping_amount, oh.base_discount_amount, oh.base_grand_total, 
			oh.customer_email,
			oh.shipping_description, oh.channel, 
			oh.tax_rate, oh.order_currency, oh.exchangeRate, 
			1 idETLBatchRun
		from 
				Landing.migra.sales_flat_order_migrate_aud oh
			--inner join
			--	(select customer_id, num_orders
			--	from
			--		(select top 100 customer_id, count(*) num_orders
			--		from Landing.migra.sales_flat_order_migrate_aud
			--		where migra_website_name = 'lenswaynl'
			--		group by customer_id
			--		order by num_orders desc) t) c on oh.customer_id = c.customer_id
		where oh.migra_website_name = 'lenswaynl'	
			and year(oh.created_at) in (2006, 2007, 2008, 2009, 2010, 2011)
