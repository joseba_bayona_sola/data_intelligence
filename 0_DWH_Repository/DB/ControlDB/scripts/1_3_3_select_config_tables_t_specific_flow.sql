
use ControlDB
go 

-- t_Database
select idDatabase, idDatabaseType, database_name, description, ins_ts
from ControlDB.config.t_Database 
order by idDatabase; 

-- t_Developer
select idDeveloper, developer, name, ins_ts
from ControlDB.config.t_Developer
order by idDeveloper;

-- t_BusinessArea 
select idBusinessArea, businessArea_name, codBusinessArea, description, ins_ts
from ControlDB.config.t_BusinessArea 
order by idBusinessArea;


-- t_Flow
select idFlow, idFlowType, idDWHTableType, idBusinessArea, flow_name, description
from ControlDB.config.t_Flow
order by idFlow; 

-- t_Table
select idTable, idDatabase, idFlow, table_name, description
from ControlDB.config.t_Table 
order by idTable;

-- t_Package 
select idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, package_name, description
from ControlDB.config.t_Package
order by idPackage;
	
-- t_SP
select idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, sp_name, description
from ControlDB.config.t_SP 
order by idSP;

-- 
	-- t_Database_v
	select idDatabase, codDatabaseType, database_name, description, ins_ts
	from ControlDB.config.t_Database_v
	order by idDatabase; 

	-- t_Flow_v
	select idFlow, codFlowType, codDWHTableType, codBusinessArea,
		flow_name, description, ins_ts
	from ControlDB.config.t_Flow_v
	order by idFlow;

	-- t_Table_v
	select idTable, database_name, codBusinessArea, flow_name, 
		table_name, description, ins_ts
	from ControlDB.config.t_Table_v 
	order by flow_name, database_name, table_name, idTable;

	-- config.t_Package_v
	select idPackage, codPackageType, developer, flow_name, db_from, db_to,
		package_name, description, ins_ts
	from ControlDB.config.t_Package_v
	order by flow_name, codPackageType, package_name, idPackage

	-- t_SP_v
	select idSP, codSPType, developer, package_name, db_from, db_to,
		sp_name, description, ins_ts
	from ControlDB.config.t_SP_v
	order by package_name, db_from, db_to, sp_name, idSP