use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'sales' codBusinessArea, 
				'Sales Order' name, 'Data about Sales related Order Dimensions' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Sales Order' flow_name, 'dim_order_stage' name, 'Staging Table for Order Stage: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_order_stage' name, 'Warehouse Table for Order Stage: Order Stage Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_order_status' name, 'Staging Table for Order Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_order_status' name, 'Warehouse Table for Order Status: Order Status Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_order_type' name, 'Staging Table for Order Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_order_type' name, 'Warehouse Table for Order Type: Order Type Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_order_status_magento' name, 'Staging Table for Order Status Magento: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_order_status_magento' name, 'Warehouse Table for Order Status Magento: Order Status Magento Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_payment_method' name, 'Staging Table for Payment Method: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_payment_method' name, 'Warehouse Table for Payment Method: Payment Method Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_cc_type' name, 'Staging Table for CC Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_cc_type' name, 'Warehouse Table for CC Type: CC Type Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_shipping_carrier' name, 'Staging Table for Shipping Carrier: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_shipping_carrier' name, 'Warehouse Table for Shipping Carrier: Shipping Carrier Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_shipping_method' name, 'Staging Table for Shipping Method: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_shipping_method' name, 'Warehouse Table for Shipping Method: Shipping Method Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_telesale' name, 'Staging Table for Telesale: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_telesale' name, 'Warehouse Table for Telesale: Telesale Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_price_type' name, 'Staging Table for Price Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_price_type' name, 'Warehouse Table for Price Type: Price Type Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_prescription_method' name, 'Staging Table for Presc. Method: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_prescription_method' name, 'Warehouse Table for Presc. Method: Presc. Method Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_rank_customer_order_seq_no' name, 'Staging Table for Rank Customer Order Seq No: Rank Dimension' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_rank_customer_order_seq_no' name, 'Warehouse Table for Rank Customer Order Seq No: Rank Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_rank_qty_time' name, 'Staging Table for Rank Qty Time: Rank Dimension' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_rank_qty_time' name, 'Warehouse Table for Rank Qty Time: Rank Dimension' description
			union
			select 'Staging' database_name, 'Sales Order' flow_name, 'dim_rank_freq_time' name, 'Staging Table for Rank Freq Time: Rank Dimension' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_rank_freq_time' name, 'Warehouse Table for Rank Freq Time: Rank Dimension' description
			union
			select 'Warehouse' database_name, 'Sales Order' flow_name, 'dim_rank_shipping_days' name, 'Warehouse Table for Rank Shipping Days: Rank Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Sales Order' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_sales_dim_order' name, 'SSIS PKG for reading Store data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Sales Order' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_sales_dim_order' name, 'SSIS PKG for reading Store data from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_stage' name, 'GET SP for reading Order Stage data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_stage' name, 'GET SP for reading Order Stage data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_stage' name, 'MERGE SP for upading Order Stage Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_status' name, 'GET SP for reading Order Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_status' name, 'GET SP for reading Order Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_status' name, 'MERGE SP for upading Order Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_type' name, 'GET SP for reading Order Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_type' name, 'GET SP for reading Order Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_type' name, 'MERGE SP for upading Order Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_status_magento' name, 'GET SP for reading Order Status Magento data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_status_magento' name, 'GET SP for reading Order Status Magento data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_status_magento' name, 'MERGE SP for upading Order Status Magento Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_payment_method' name, 'GET SP for reading Payment Method data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_payment_method' name, 'GET SP for reading Payment Method data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_payment_method' name, 'MERGE SP for upading Payment Method Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_cc_type' name, 'GET SP for reading CC Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_cc_type' name, 'GET SP for reading CC Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_cc_type' name, 'MERGE SP for upading CC Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_shipping_carrier' name, 'GET SP for reading Shipping Carrier data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_shipping_carrier' name, 'GET SP for reading Shipping Carrier data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_shipping_carrier' name, 'MERGE SP for upading Shipping Carrier Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_shipping_method' name, 'GET SP for reading Shipping Method data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_shipping_method' name, 'GET SP for reading Shipping Method data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_shipping_method' name, 'MERGE SP for upading Shipping Method Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_telesale' name, 'GET SP for reading Telesale data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_telesale' name, 'GET SP for reading Telesale data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_telesale' name, 'MERGE SP for upading Telesale Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_price_type' name, 'GET SP for reading Price Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_price_type' name, 'GET SP for reading Price Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_price_type' name, 'MERGE SP for upading Price Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_prescription_method' name, 'GET SP for reading Presc. Method data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_prescription_method' name, 'GET SP for reading Presc. Method data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_prescription_method' name, 'MERGE SP for upading Presc. Method Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_rank_customer_order_seq_no' name, 'GET SP for reading Rank Cust Order Seq NO data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_rank_customer_order_seq_no' name, 'GET SP for reading Rank Cust Order Seq NO data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_rank_customer_order_seq_no' name, 'MERGE SP for upading Rank Cust Order Seq NO Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_rank_qty_time' name, 'GET SP for reading Rank Qty Time data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_rank_qty_time' name, 'GET SP for reading Rank Qty Time data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_rank_qty_time' name, 'MERGE SP for upading Rank Qty Time Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_rank_freq_time' name, 'GET SP for reading Rank Freq Time data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_rank_freq_time' name, 'GET SP for reading Rank Freq Time data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_rank_freq_time' name, 'MERGE SP for upading Rank Freq Time Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_rank_shipping_days' name, 'GET SP for reading Rank Shipping Days data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_rank_shipping_days' name, 'GET SP for reading Rank Shipping Days data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_rank_shipping_days' name, 'MERGE SP for upading Rank Shipping Days Dimension data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 