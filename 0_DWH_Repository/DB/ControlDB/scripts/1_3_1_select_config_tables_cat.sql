
-- cat_DatabaseType
select idDatabaseType, codDatabaseType, description, ins_ts
from ControlDB.config.cat_DatabaseType
order by idDatabaseType 

-- cat_DWHTableType
select idDWHTableType, codDWHTableType, description, ins_ts
from ControlDB.config.cat_DWHTableType
order by idDWHTableType; 

-- cat_FlowType
select idFlowType, codFlowType, description, ins_ts
from ControlDB.config.cat_FlowType
order by idFlowType;

-- cat_PackageType
select idPackageType, codPackageType, description, ins_ts
from ControlDB.config.cat_PackageType
order by idPackageType;

-- cat_SPType
select idSPType, codSPType, description, ins_ts
from ControlDB.config.cat_SPType
order by idSPType; 

-- cat_ETLBatchType
select idETLBatchType, codETLBatchType, description, ins_ts
from ControlDB.config.cat_ETLBatchType
order by idETLBatchType;
