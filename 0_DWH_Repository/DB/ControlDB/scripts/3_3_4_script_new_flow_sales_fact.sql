use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'FACT' codDWHTableType, 'sales' codBusinessArea, 
				'Sales Fact' name, 'Data about Order Header - Order Line for Sales' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Sales Fact' flow_name, 'dim_order_header' name, 'Staging Table for Order Header: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Fact' flow_name, 'dim_order_header' name, 'Warehouse Table for Order Header: Order Header Dimension having also Fact Data' description
			union
			select 'Staging' database_name, 'Sales Fact' flow_name, 'dim_order_header_product_type' name, 'Staging Table for Order Header: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Fact' flow_name, 'dim_order_header_product_type' name, 'Warehouse Table for Order Header: Order Header Dimension having also Fact Data' description
			union
			select 'Staging' database_name, 'Sales Fact' flow_name, 'fact_order_line' name, 'Staging Table for Order Line: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Fact' flow_name, 'fact_order_line' name, 'Warehouse Table for Order Line: Order Line Fact Table' description

			-- union
			-- select 'Warehouse' database_name, 'Sales Fact' flow_name, 'dim_exchange_rate' name, 'Warehouse Table for Exchange Rate values' description
			union
			select 'Staging' database_name, 'Sales Fact' flow_name, 'fact_order_line_vat_fix' name, 'Staging Table for Order Line VAT Fix: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Fact' flow_name, 'fact_order_line_vat_fix' name, 'Warehouse Table for Order Line VAT Fix: Order Line Fact Table' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Sales Fact' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_sales_fact_order' name, 'SSIS PKG for reading Order Fact data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Sales Fact' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_sales_fact_order' name, 'SSIS PKG for reading Order Fact data from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header' name, 'GET SP for reading Order Header data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_header' name, 'GET SP for reading Order Header data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_header' name, 'MERGE SP for upading Order Header Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header_product_type' name, 'GET SP for reading Order Header Product Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_header_product_type' name, 'GET SP for reading Order Header Product Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_header_product_type' name, 'MERGE SP for upading Order Header Product Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_line' name, 'GET SP for reading Order Line data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_line' name, 'GET SP for reading Order Line data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_line' name, 'MERGE SP for upading Order Line Fact data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o' name, 'GET SP for reading Order Header data: O entities' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o_i_s_cr' name, 'GET SP for reading Order Header data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_store_cust' name, 'GET SP for reading Order Header data: Store - Customer Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_order' name, 'GET SP for reading Order Header data: Order related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_mark' name, 'GET SP for reading Order Header data: Marketing related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_measures' name, 'GET SP for reading Order Header data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_vat' name, 'GET SP for reading Order Header data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_oh_pt' name, 'GET SP for reading Order Header data: OH - PT relation' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_o_i_s_cr' name, 'GET SP for reading Order Line data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_product' name, 'GET SP for reading Order Line data: Product Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_measures' name, 'GET SP for reading Order Line data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_vat' name, 'GET SP for reading Order Line data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_dim_fact' name, 'GET SP for reading preparing Order Dim - Fact Aux Tables using different Auxiliar Tables' description
			
			-- union
			-- select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				-- 'stg_dwh_merge_sales_exchange_rate' name, 'MERGE SP for updating exchange rate values' description

			union
			select 'MERGE' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'lnd_lnd_merge_aux_sales_exchange_rate' name, 'mERGE SP for setting the exchange rate values' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_line_vat_fix' name, 'GET SP for reading Order Line VAT Fix data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_order_line_vat_fix' name, 'GET SP for reading Order Line VAT Fix data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_fact_order' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_order_line_vat_fix' name, 'MERGE SP for upading Order Line VAT Fix Fact data' description			
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 