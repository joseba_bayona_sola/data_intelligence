USE ControlDB
GO

DECLARE	@return_value int, @idETLBatchRun bigint

	EXEC @return_value = logging.logETLBatchRun_start_sp
			@codETLBatchType = 'FULL', @codFlowType = 'SRC-DWH', @package_name = 'srcmag_lnd_gen_store', @description = 'Test 1',
			@dateFrom = '2017-01-12', @dateTo = '2017-01-13',
			@idETLBatchRun = @idETLBatchRun OUTPUT

	SELECT	@idETLBatchRun as '@idETLBatchRun'
	SELECT	'Return Value' = @return_value

GO

-- 

	EXEC logging.logETLBatchRun_stop_sp 
		@idETLBatchRun = 1,
		@runStatus = 'COMPLETE', @message = 'ETL Batch Execution OK'

	go
