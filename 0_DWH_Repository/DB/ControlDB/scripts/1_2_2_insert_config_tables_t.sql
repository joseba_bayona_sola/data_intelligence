use ControlDB
go 

-- t_Database
	insert into ControlDB.config.t_Database (idDatabase, idDatabaseType, database_name, description) 
		select t.idDatabase, dt.idDatabaseType, t.name, t.description
		from
				(select 1 idDatabase, 'SQL-Server' codDatabaseType, 'ControlDB' name, 'ETL Configuration and Logging Database' description
				union
				select 2 idDatabase, 'SQL-Server' codDatabaseType, 'Landing' name, 'Landing Database: Store the data as it comes from different Sources' description
				union
				select 3 idDatabase, 'SQL-Server' codDatabaseType, 'Staging' name, 'Staging Database: Store the data transformed and in a similar way to Warehouse' description
				union
				select 4 idDatabase, 'SQL-Server' codDatabaseType, 'Warehouse' name, 'Warehouse Database: Store the data in a Dimesional way: Ready to be used for Analyse Purposes' description
				union
				select 5 idDatabase, 'SQL-Server' codDatabaseType, 'Adhoc' name, 'Adhoc Database: Views to Warehouse so users can analyze data in an adhoc way' description
				) t
			inner join
				ControlDB.config.cat_DatabaseType dt on t.codDatabaseType = dt.codDatabaseType;
	go 

	insert into ControlDB.config.t_Database (idDatabase, idDatabaseType, database_name, description) 
		select t.idDatabase, dt.idDatabaseType, t.name, t.description
		from
				(select 6 idDatabase, 'MySQL' codDatabaseType, 'Magento01' name, 'SRC Database: Stores all Sales data from Magento Website' description
				union
				select 7 idDatabase, 'PostgreSQL' codDatabaseType, 'mendix_db' name, 'SRC Database: Stores all ERP data from Mendix ERP' description
				) t
			inner join
				ControlDB.config.cat_DatabaseType dt on t.codDatabaseType = dt.codDatabaseType;
	go 


	insert into ControlDB.config.t_Database (idDatabase, idDatabaseType, database_name, description) 
		select t.idDatabase, dt.idDatabaseType, t.name, t.description
		from
				(select 8 idDatabase, 'SQL-Server' codDatabaseType, 'DW_GetLenses' name, 'Current DWH: Sales orders done in VD companies' description
				union
				select 9 idDatabase, 'SQL-Server' codDatabaseType, 'DW_Proforma' name, 'Current DWH: Sales orders done in acquired companies' description
				) t
			inner join
				ControlDB.config.cat_DatabaseType dt on t.codDatabaseType = dt.codDatabaseType;
	go 

	insert into ControlDB.config.t_Database (idDatabase, idDatabaseType, database_name, description) 
		select t.idDatabase, dt.idDatabaseType, t.name, t.description
		from
				(select 10 idDatabase, 'ExcelFile' codDatabaseType, 'Excel' name, 'Excel Files' description) t
			inner join
				ControlDB.config.cat_DatabaseType dt on t.codDatabaseType = dt.codDatabaseType;
	go 



-- t_Developer
	insert into ControlDB.config.t_Developer (idDeveloper, developer, name) values
		(1, 'jsola', 'Joseba Bayona Sola')
	insert into ControlDB.config.t_Developer (idDeveloper, developer, name) values
		(2, 'iirujo', 'Isabel Irujo Cia')
	insert into ControlDB.config.t_Developer (idDeveloper, developer, name) values
		(3, 'jcano', 'Juan Pablo Cano')
	go 


-- t_BusinessArea 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(1, 'General', 'gen', 'Dimension Data that is common to many Business Areas')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(2, 'Sales', 'sales', 'Data about Orders (Headers - Lines) made through Magento Website: Orders - Invoices - Shipments - Creditmemos')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(3, 'Products', 'prod', 'Data about Products information stored in Magento and Mendix ERP')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(4, 'Stock', 'stock', 'Data about Warehouses and Product Stock in Mendix ERP')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(5, 'Marketing', 'mark', 'Data related with Marketing activities')
	go 

	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(6, 'Mapping', 'map', 'Data related with Mapping files that will be used during Landing - Stage transformations')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(7, 'Migration', 'migra', 'Data related with DW_GetLenses - DW_Proforma')
	go 

	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(8, 'Customer Service', 'cuserv', 'Data related with Customer Service activities')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(9, 'Customer Activity + Signature', 'act', 'Data related with Customer Activity and Signature Info')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(10, 'Product Activity + Signature', 'prdact', 'Data related with Product Activity and Signature Info')
	go 
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(11, 'Order Activity + Signature', 'ordact', 'Data related with Order Activity and Signature Info')
	go 
	
	insert into ControlDB.config.t_BusinessArea (idBusinessArea, businessArea_name, codBusinessArea, description) values
		(12, 'Statistics', 'stat', 'Data related with statistics taken for different things to measure')
	go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRun' name, 'SSIS PKG for starting the ETL Batch Run and calling rest of packages' description
			) t
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		left join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		left join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		left join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
go 