
use ControlDB
go 

-- t_Database_v
create view config.t_Database_v as
	select d.idDatabase, d.idDatabaseType, dt.codDatabaseType, d.database_name, d.description, d.ins_ts
	from 
			ControlDB.config.t_Database d
		inner join
			ControlDB.config.cat_DatabaseType dt on d.idDatabaseType = dt.idDatabaseType; 
go 

-- t_Flow_v
create view config.t_Flow_v as
	select f.idFlow, f.idFlowType, ft.codFlowType, f.idDWHTableType, dtt.codDWHTableType, f.idBusinessArea, ba.codBusinessArea,
		f.flow_name, f.description, f.ins_ts
	from 
			ControlDB.config.t_Flow f
		inner join
			ControlDB.config.cat_FlowType ft on f.idFlowType = ft.idFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on f.idDWHTableType = dtt.idDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on f.idBusinessArea = ba.idBusinessArea
go			

-- t_Table_v
create view config.t_Table_v as
	select t.idTable, t.idDatabase, d.database_name, f.idBusinessArea, f.codBusinessArea, t.idFlow, f.flow_name, 
		t.table_name, t.description, t.ins_ts
	from 
			ControlDB.config.t_Table t
		inner join
			ControlDB.config.t_Database d on t.idDatabase = d.idDatabase
		inner join
			ControlDB.config.t_Flow_v f on t.idFlow = f.idFlow
go

-- t_Package_v 
create view config.t_Package_v as
	select p.idPackage, p.idPackageType, pt.codPackageType, p.idDeveloper, d.developer, p.idFlow, f.flow_name, p.idDatabaseFrom, db_f.database_name db_from, p.idDatabaseTo, db_t.database_name db_to,
		p.package_name, p.description, p.ins_ts
	from 
			ControlDB.config.t_Package p
		inner join
			ControlDB.config.cat_PackageType pt on p.idPackageType = pt.idPackageType
		inner join
			ControlDB.config.t_Developer d on p.idDeveloper = d.idDeveloper
		left join
			ControlDB.config.t_Flow f on p.idFlow = f.idFlow
		left join
			ControlDB.config.t_Database db_f on p.idDatabaseFrom = db_f.idDatabase
		left join
			ControlDB.config.t_Database db_t on p.idDatabaseTo = db_t.idDatabase
go
	
-- t_SP_v
create view config.t_SP_v as
	select sp.idSP, sp.idSPType, st.codSPType, sp.idDeveloper, d.developer, sp.idPackage, p.package_name, sp.idDatabaseFrom, db_f.database_name db_from, sp.idDatabaseTo, db_t.database_name db_to,
		sp.sp_name, sp.description, sp.ins_ts
	from 
			ControlDB.config.t_SP sp
		inner join
			ControlDB.config.cat_SPType st on sp.idSPType = st.idSPType
		inner join
			ControlDB.config.t_Developer d on sp.idDeveloper = d.idDeveloper
		inner join
			ControlDB.config.t_Package p on sp.idPackage = p.idPackage
		inner join
			ControlDB.config.t_Database db_f on sp.idDatabaseFrom = db_f.idDatabase
		inner join
			ControlDB.config.t_Database db_t on sp.idDatabaseTo = db_t.idDatabase
go 


-- t_SP_ssrs_v
create view config.t_SP_ssrs_v as
	select sp.idSP_ssrs, sp.idSPType, st.codSPType, sp.idDeveloper, d.developer, 
		sp.sp_name, sp.description, sp.ins_ts
	from 
			ControlDB.config.t_SP_ssrs sp
		inner join
			ControlDB.config.cat_SPType st on sp.idSPType = st.idSPType
		inner join
			ControlDB.config.t_Developer d on sp.idDeveloper = d.idDeveloper
go 

