USE ControlDB
GO

DECLARE	@return_value int, @idPackageRun bigint, @idExec uniqueidentifier

	SET @idExec = newid()

	EXEC @return_value = logging.logPackageRun_start_sp
			@package_name = 'lnd_stg_gen_store', 
			@idETLBatchRun = 1,  
			@idExecution = @idExec,
			@idPackageRun = @idPackageRun OUTPUT

	SELECT	@idPackageRun as '@idPackageRun'
	SELECT	'Return Value' = @return_value

GO

-- 

	EXEC logging.logPackageRun_message_sp 
		@idPackageRun = 1,
		@messageType = 'SELECT', @rowAmount = 50, @message = '50 rows read '

	go

	EXEC logging.logPackageRun_stop_sp 
		@idPackageRun = 1,
		@runStatus = 'COMPLETE'

	go
