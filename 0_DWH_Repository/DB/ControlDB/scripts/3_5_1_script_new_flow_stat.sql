use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'WH-DWH' codFlowType, 'FACT' codDWHTableType, 'stat' codBusinessArea, 
				'Statistics Fact' name, 'Data about General STAT Tables' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 


-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Warehouse' database_name, 'Statistics Fact' flow_name, 'fact_customer_status' name, 'Warehouse Table for Statistics: Customers depending of Customer status' description
			union
			select 'Warehouse' database_name, 'Statistics Fact' flow_name, 'fact_magento_unsubscribe' name, 'Warehouse Table for Statistics: Customers depending of Magento Unsubscribe' description
			union
			select 'Warehouse' database_name, 'Statistics Fact' flow_name, 'fact_dotmailer_unsubscribe' name, 'Warehouse Table for Statistics: Customers depending of Dotmailer Unsubscribe' description
			union
			select 'Warehouse' database_name, 'Statistics Fact' flow_name, 'fact_sms_unsubscribe' name, 'Warehouse Table for Statistics: Customers depending of SMS Unsubscribe' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'DWH-DWH' codPackageType, 'jsola' developer,  'Statistics Fact' flow_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_stat_fact_tables' name, 'SSIS PKG for inserting data into stats tables' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_stat_fact_tables' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_stat_customer_status' name, 'MERGE SP for updating Customer Status stat fact table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_stat_fact_tables' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_stat_magento_unsubscribe' name, 'MERGE SP for updating Magento Unsubscribe stat fact table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_stat_fact_tables' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_stat_dotmailer_unsubscribe' name, 'MERGE SP for updating Dotmailer Unsubscribe stat fact table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'dwh_dwh_stat_fact_tables' package_name, 'Warehouse' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'dwh_dwh_merge_stat_sms_unsubscribe' name, 'MERGE SP for updating SMS Unsubscribe stat fact table' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 
