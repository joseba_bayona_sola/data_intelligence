use ControlDB
go 

-- cat_DatabaseType
create table ControlDB.config.cat_DatabaseType (
	idDatabaseType			int NOT NULL, 
	codDatabaseType			varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_DatabaseType_ins_ts] DEFAULT (getutcdate()));
go 
	
alter table ControlDB.config.cat_DatabaseType add constraint PK_cat_DatabaseType
	primary key clustered (idDatabaseType);	 
go 

-- cat_DWHTableType
create table ControlDB.config.cat_DWHTableType (
	idDWHTableType			int NOT NULL, 
	codDWHTableType			varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_DWHTableType_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.cat_DWHTableType add constraint PK_cat_DWHTableType
	primary key clustered (idDWHTableType);	 
go 

-- cat_FlowType
create table ControlDB.config.cat_FlowType (
	idFlowType				int NOT NULL, 
	codFlowType				varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_FlowType_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.cat_FlowType add constraint PK_cat_FlowType
	primary key clustered (idFlowType);	 
go 

-- cat_PackageType
create table ControlDB.config.cat_PackageType (
	idPackageType			int NOT NULL, 
	codPackageType			varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_PackageType_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.cat_PackageType add constraint PK_cat_PackageType
	primary key clustered (idPackageType);	 
go 

-- cat_SPType
create table ControlDB.config.cat_SPType (
	idSPType				int NOT NULL, 
	codSPType				varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_SPType_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.cat_SPType add constraint PK_cat_SPType
	primary key clustered (idSPType);	 
go 

-- cat_ETLBatchType
create table ControlDB.config.cat_ETLBatchType (
	idETLBatchType			int NOT NULL, 
	codETLBatchType			varchar(20) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_cat_ETLBatchType_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.cat_ETLBatchType add constraint PK_cat_ETLBatchType
	primary key clustered (idETLBatchType);	 
go 


----------------------------------------------------------------

-- t_Database
create table ControlDB.config.t_Database (
	idDatabase				int NOT NULL, 
	idDatabaseType			int NOT NULL, 
	database_name			varchar(50) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_Database_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_Database add constraint PK_Database
	primary key clustered (idDatabase);	 
go 

alter table ControlDB.config.t_Database add constraint UNIQ_Database_database_name
	unique (database_name)
go

alter table ControlDB.config.t_Database add constraint FK_Database_DatabaseType
	foreign key (idDatabaseType) references ControlDB.config.cat_DatabaseType (idDatabaseType);	 
go 

-- t_Developer
create table ControlDB.config.t_Developer (
	idDeveloper				int NOT NULL, 
	developer				varchar(50) NOT NULL,
	name					varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_Developer_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_Developer add constraint PK_Developer
	primary key clustered (idDeveloper);	 
go 

alter table ControlDB.config.t_Developer add constraint UNIQ_Developer_developer
	unique (developer)
go

-- t_BusinessArea
create table ControlDB.config.t_BusinessArea (
	idBusinessArea			int NOT NULL, 
	businessArea_name		varchar(100) NOT NULL, 
	codBusinessArea			varchar(6) NOT NULL,
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_BusinessArea_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_BusinessArea add constraint PK_BusinessArea
	primary key clustered (idBusinessArea);	 
go 

alter table ControlDB.config.t_BusinessArea add constraint UNIQ_BusinessArea_codBusinessArea
	unique (codBusinessArea)
go


-- t_Flow
create table ControlDB.config.t_Flow (
	idFlow					int NOT NULL,
	idFlowType				int NOT NULL, 
	idDWHTableType			int NOT NULL,
	idBusinessArea			int NOT NULL,
	flow_name				varchar(100) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_Flow_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_Flow add constraint PK_Flow
	primary key clustered (idFlow);	 
go 

alter table ControlDB.config.t_Flow add constraint UNIQ_Flow_flow_name
	unique (flow_name)
go


alter table ControlDB.config.t_Flow add constraint FK_Flow_FlowType
	foreign key (idFlowType) references ControlDB.config.cat_FlowType (idFlowType);	 
go 
alter table ControlDB.config.t_Flow add constraint FK_Flow_DWHTableType
	foreign key (idDWHTableType) references ControlDB.config.cat_DWHTableType (idDWHTableType);	 
go 
alter table ControlDB.config.t_Flow add constraint FK_Flow_BusinessArea
	foreign key (idBusinessArea) references ControlDB.config.t_BusinessArea (idBusinessArea);	 
go 

-- t_Table
create table ControlDB.config.t_Table (
	idTable					int NOT NULL, 
	idDatabase				int NOT NULL,
	idFlow					int NOT NULL,
	table_name				varchar(100) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_Table_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_Table add constraint PK_Table
	primary key clustered (idTable);	 
go 

alter table ControlDB.config.t_Table add constraint UNIQ_Table_table_name
	unique (idDatabase, table_name)
go


alter table ControlDB.config.t_Table add constraint FK_Table_Database
	foreign key (idDatabase) references ControlDB.config.t_Database (idDatabase);	 
go 
alter table ControlDB.config.t_Table add constraint FK_Table_Flow
	foreign key (idFlow) references ControlDB.config.t_Flow (idFlow);	 
go 

-- t_Package 
create table ControlDB.config.t_Package (
	idPackage				int NOT NULL,
	idPackageType			int NOT NULL,
	idDeveloper				int NOT NULL,
	idFlow					int,
	idDatabaseFrom			int,
	idDatabaseTo			int,
	package_name			varchar(100) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_Package_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.config.t_Package add constraint PK_Package
	primary key clustered (idPackage);	 
go 

alter table ControlDB.config.t_Package add constraint UNIQ_Package_package_name
	unique (package_name)
go


alter table ControlDB.config.t_Package add constraint FK_Package_PackageType
	foreign key (idPackageType) references ControlDB.config.cat_PackageType (idPackageType);	 
go 
alter table ControlDB.config.t_Package add constraint FK_Package_Developer
	foreign key (idDeveloper) references ControlDB.config.t_Developer (idDeveloper);	 
go 
alter table ControlDB.config.t_Package add constraint FK_Package_Flow
	foreign key (idFlow) references ControlDB.config.t_Flow (idFlow);	 
go 
alter table ControlDB.config.t_Package add constraint FK_Package_DatabaseFrom
	foreign key (idDatabaseFrom) references ControlDB.config.t_Database (idDatabase);	 
go 
alter table ControlDB.config.t_Package add constraint FK_Package_DatabaseTo
	foreign key (idDatabaseTo) references ControlDB.config.t_Database (idDatabase);	 
go 


-- t_SP
create table ControlDB.config.t_SP (
	idSP					int NOT NULL,
	idSPType				int NOT NULL,
	idDeveloper				int NOT NULL,
	idPackage				int NOT NULL,
	idDatabaseFrom			int NOT NULL,
	idDatabaseTo			int NOT NULL,
	sp_name					varchar(100) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SP_ins_ts] DEFAULT (getutcdate()));
go 


alter table ControlDB.config.t_SP add constraint PK_SP
	primary key clustered (idSP);	 
go 

alter table ControlDB.config.t_SP add constraint UNIQ_SP_sp_name
	unique (sp_name)
go


alter table ControlDB.config.t_SP add constraint FK_SP_SPType
	foreign key (idSPType) references ControlDB.config.cat_SPType (idSPType);	 
go 
alter table ControlDB.config.t_SP add constraint FK_SP_Developer
	foreign key (idDeveloper) references ControlDB.config.t_Developer (idDeveloper);	 
go 
alter table ControlDB.config.t_SP add constraint FK_SP_Package
	foreign key (idPackage) references ControlDB.config.t_Package (idPackage);	 
go 
alter table ControlDB.config.t_SP add constraint FK_SP_DatabaseFrom
	foreign key (idDatabaseFrom) references ControlDB.config.t_Database (idDatabase);	 
go 
alter table ControlDB.config.t_SP add constraint FK_SP_DatabaseTo
	foreign key (idDatabaseTo) references ControlDB.config.t_Database (idDatabase);	 
go 
	
	
	
-- t_SP_ssrs
create table ControlDB.config.t_SP_ssrs (
	idSP_ssrs				int NOT NULL,
	idSPType				int NOT NULL,
	idDeveloper				int NOT NULL,
	sp_name					varchar(100) NOT NULL, 
	description				varchar(255) NOT NULL, 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SP_ssrs_ins_ts] DEFAULT (getutcdate()));
go 


alter table ControlDB.config.t_SP_ssrs add constraint PK_SP_ssrs
	primary key clustered (idSP_ssrs);	 
go 

alter table ControlDB.config.t_SP_ssrs add constraint UNIQ_SP_ssrs_sp_name
	unique (sp_name)
go


alter table ControlDB.config.t_SP_ssrs add constraint FK_SP_ssrs_SPType
	foreign key (idSPType) references ControlDB.config.cat_SPType (idSPType);	 
go 
alter table ControlDB.config.t_SP_ssrs add constraint FK_SP_ssrs_Developer
	foreign key (idDeveloper) references ControlDB.config.t_Developer (idDeveloper);	 
go 


	