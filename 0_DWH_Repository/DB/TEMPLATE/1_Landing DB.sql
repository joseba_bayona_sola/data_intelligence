
use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.mag.temp_xxxx
create table mag.temp_xxxx(
	xxxx_1			int NOT NULL, 
	xxxx_2			varchar(100), 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go 

alter table mag.temp_xxxx add constraint [PK_mag_temp_xxxx]
	primary key clustered (xxxx_1);
go

alter table mag.temp_xxxx add constraint [DF_mag_temp_xxxx_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.temp_xxxx_aud
create table mag.temp_xxxx_aud(
	xxxx_1			int NOT NULL, 
	xxxx_2			varchar(100), 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	upd_ts			datetime);
go 

alter table mag.temp_xxxx_aud add constraint [PK_mag_temp_xxxx_aud]
	primary key clustered (xxxx_1);
go

alter table mag.temp_xxxx_aud add constraint [DF_mag_temp_xxxx_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.temp_xxxx_aud_hist
create table mag.temp_xxxx_aud_hist(
	xxxx_1			int NOT NULL, 
	xxxx_2			varchar(100), 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	aud_type		char(1) NOT NULL, 
	aud_dateFrom	datetime NOT NULL, 
	aud_dateTo		datetime NOT NULL);
go 

alter table mag.temp_xxxx_aud_hist add constraint [DF_temp_xxxx_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------- Triggers ----------------------------------

-- Landing.mag.trg_temp_xxxx

create trigger mag.trg_temp_xxxx on mag.temp_xxxx
after insert
as
begin
	set nocount on

	merge into mag.temp_xxxx_aud with (tablock) as trg
	using inserted src
		on (trg.xxxx_1 = src.xxxx_1)
	when matched and
		(isnull(trg.xxxx_2, '') <> isnull(src.xxxx_2, '')) 
		then
			update set
				trg.xxxx_2 = src.xxxx_2, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (xxxx_1, xxxx_2, idETLBatchRun)
				values (src.xxxx_1, src.xxxx_2, src.idETLBatchRun);
end; 
go


create trigger mag.trg_temp_xxxx_aud on mag.temp_xxxx_aud
for update, delete
as
begin
	set nocount on

	insert mag.temp_xxxx_aud_hist (xxxx_1, xxxx_2, idETLBatchRun,
		aud_type, aud_dateFrom, aud_dateTo)
		
		select d.xxxx_1, d.xxxx_2, d.idETLBatchRun,
			case when (i.xxxx_1 is null) then 'D' else 'U' end aud_type, d.ins_ts aud_dateFrom, i.ins_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.xxxx_1 = i.xxxx_1;
end;



-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 18-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento - TEMPLATE
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_temp_xxxx
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Log Parameters ??

	set @sql = 'select rank() over(order by store_id) as xxxx_1, ''xxxx'' as xxxx_2, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, ''select * from magento01.core_store'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


-----------------------------

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from TEMPLATE in Landing and places in Staging
-- ==========================================================================================

create procedure mag.lnd_stg_get_temp_xxxx
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 

	select xxxx_1, xxxx_2, @idETLBatchRun
	from Landing.mag.temp_xxxx
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 