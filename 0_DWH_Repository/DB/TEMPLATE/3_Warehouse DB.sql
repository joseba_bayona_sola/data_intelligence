use Warehouse
go

create table Warehouse.gen.dim_temp_xxxx(
	xxxx_1_sk			int NOT NULL IDENTITY(1, 1), 
	xxxx_1_bk			int NOT NULL, 
	xxxx_2				varchar(100) NULL, 
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	idETLBatchRun_upd	bigint, 
	upd_ts				datetime);
go 

alter table Warehouse.gen.dim_temp_xxxx add constraint [PK_gen_dim_temp_xxxx]
	primary key clustered (xxxx_1_sk);
go

alter table Warehouse.gen.dim_temp_xxxx add constraint [DF_gen_dim_temp_xxxx_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_temp_xxxx add constraint [UNIQ_gen_dim_temp_xxxx_bk]
	unique (xxxx_1_bk);
go


create table Warehouse.gen.dim_temp_xxxx_wrk(
	xxxx_1_bk			int NOT NULL, 
	xxxx_2				varchar(100) NULL,  
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Warehouse.gen.dim_temp_xxxx_wrk add constraint [DF_gen_dim_temp_xxxx_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from TEMPLATE from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_temp_xxxx
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_temp_xxxx with (tablock) as trg
	using Warehouse.gen.dim_temp_xxxx_wrk src
		on (trg.xxxx_1_bk = src.xxxx_1_bk)
	when matched and not exists 
		(select isnull(trg.xxxx_2, '')
		intersect
		select isnull(src.xxxx_2, ''))
		then 
			update set
				trg.xxxx_2 = src.xxxx_2, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (xxxx_1_bk, xxxx_2, idETLBatchRun_ins)
				values (src.xxxx_1_bk, src.xxxx_2, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 