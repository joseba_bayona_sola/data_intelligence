
--------------------------------------------
-- TABLES PROFILING
--------------------------------------------

	-- Landing
	select xxxx_1, xxxx_2, idETLBatchRun, ins_ts
	from Landing.mag.temp_xxxx

	select xxxx_1, xxxx_2, idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.temp_xxxx_aud

	select xxxx_1, xxxx_2, idETLBatchRun, ins_ts, 
		aud_type, aud_dateFrom, aud_dateTo
	from Landing.mag.temp_xxxx_aud_hist


	-- Staging
	select xxxx_1_bk, xxxx_2, idETLBatchRun, ins_ts
	from Staging.gen.dim_temp_xxxx


	-- Warehouse
	select xxxx_1_sk, 
		xxxx_1_bk, xxxx_2, 
		idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts 
	from Warehouse.gen.dim_temp_xxxx

	select 
		xxxx_1_bk, xxxx_2, 
		idETLBatchRun, ins_ts
	from Warehouse.gen.dim_temp_xxxx_wrk


-------------------------------------------------------

--------------------------------------------
-- LOGGING TABLES PROFILING
--------------------------------------------

	-- t_PackageRun_v
	select idPackageRun, idPackage, flow_name, package_name, 
		idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where package_name in ('temp_srcmag_lnd_temp_xxxx', 'temp_lnd_stg_temp_xxxx', 'temp_stg_dwh_temp_xxxx')
	order by idETLBatchRun, startTime

	-- t_PackageRunMessage_v
	select idPackageRunMessage, idPackageRun, flow_name, package_name, idExecution, startTime, finishTime, runStatus, 
		messageTime, messageType, rowAmount, message, 
		ins_ts
	from ControlDB.logging.t_PackageRunMessage_v
	where idPackageRun in 
		(select idPackageRun
		from ControlDB.logging.t_PackageRun_v
		where package_name in ('temp_srcmag_lnd_temp_xxxx', 'temp_lnd_stg_temp_xxxx', 'temp_stg_dwh_temp_xxxx'))
	order by startTime

	-- t_SPRun_v
	select idSPRun, idSP, package_name, sp_name,
		idETLBatchRun, idPackageRun, idExecution, pkg_startTime, pkg_finishTime, pkg_runStatus, 
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_SPRun_v
	where idPackageRun in 
		(select idPackageRun
		from ControlDB.logging.t_PackageRun_v
		where package_name in ('temp_srcmag_lnd_temp_xxxx', 'temp_lnd_stg_temp_xxxx', 'temp_stg_dwh_temp_xxxx'))
	order by idETLBatchRun, startTime

	-- t_SPRunMessage_v
	select idSPRunMessage, idSPRun, package_name, sp_name, startTime, finishTime, runStatus, 
		messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
		ins_ts	
	from ControlDB.logging.t_SPRunMessage_v
	where idSPRun in 
		(select idSPRun
		from ControlDB.logging.t_SPRun_v
		where idPackageRun in 
			(select idPackageRun
			from ControlDB.logging.t_PackageRun_v
			where package_name in ('temp_srcmag_lnd_temp_xxxx', 'temp_lnd_stg_temp_xxxx', 'temp_stg_dwh_temp_xxxx')))
	order by startTime

