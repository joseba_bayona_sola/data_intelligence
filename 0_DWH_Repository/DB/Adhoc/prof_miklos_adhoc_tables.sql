/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[costofsales]
order by allocation_date desc

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[coststats]
order by parentmagentoid

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[coststats03]
order by parentmagentoid

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[coststats09]
order by parentmagentoid

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[coststats10]
order by parentmagentoid

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[eurorates]
order by rate_date desc

SELECT TOP 1000 count(*) over (), *
  FROM [Adhoc].[dbo].[test_costs]
order by allocation_date desc
