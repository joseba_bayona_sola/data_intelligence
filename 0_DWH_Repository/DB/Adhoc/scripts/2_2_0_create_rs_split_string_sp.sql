use Adhoc
go 

drop function dbo.fnSplitString;
go 

create function dbo.fnSplitString(@string nvarchar(max), @delimiter char(1) = ',') returns @t table(string nvarchar(max))
as
begin

	declare @pos int;
	declare @piece varchar(500);

	if right(rtrim(@string),1) <> @delimiter set @string = @string  + @delimiter

	set @pos =  patindex('%' + @delimiter + '%' , @string)

	while @pos <> 0 
	begin
		set @piece = left(@string, @pos - 1)
 
		-- You have a piece of data, so insert it, print it, do whatever you want to with it.
		insert	@t select	@piece

		set @string = stuff(@string, 1, @pos,'')
		set @pos =  patindex('%' + @delimiter + '%' , @string)
	end

	return 
end;
go
