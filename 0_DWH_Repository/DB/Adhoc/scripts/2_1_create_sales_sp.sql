use Adhoc
go



drop procedure sales.adhoc_insert_aux_oh_everclear_elite
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Inserts Everclear Elite OH data + Conversion, Repetition info
-- ==========================================================================================

create procedure sales.adhoc_insert_aux_oh_everclear_elite
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- TRUNCATE STATEMENT
	delete from Adhoc.sales.aux_oh_everclear_elite

	select order_id_bk, order_no, order_date, 
		customer_id, line_status_name, customer_order_seq_no, 
		product_id_magento 
	into #everclear_elite_sales_an
	from Warehouse.sales.fact_order_line_v
	where product_id_magento in (3168)
		and website_group = 'VisionDirect'

	-- INSERT STATEMENT 
	insert into Adhoc.sales.aux_oh_everclear_elite (order_id_bk, customer_id, conversion_f, repetition_f)

		select t1.order_id_bk, t1.customer_id, t1.conversion_f, t2.repetition_f
		from
				(select order_id_bk, customer_id, max(conversion) conversion_f
				from
					(select *, 
						case when (product_id_magento in (1084, 2301, 2298, 2334, 2417)) then 'Y' else 'N' end conversion
					from
						(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
							ol.order_date order_date_prev, 
							ol.product_id_magento, ol.product_family_name, 
							max(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) last_order_per_cust
						from
								(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
								from #everclear_elite_sales_an) ee 
							left join
								Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date > ol.order_date) t
					where order_date_prev = last_order_per_cust or last_order_per_cust is null) t 
				group by order_id_bk, customer_id) t1
			inner join
				(select order_id_bk, customer_id, max(repetition) repetition_f
				from
					(select *, 
						case when (product_id_magento in (3168)) then 'Y' else 'N' end repetition
					from
						(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
							ol.order_date order_date_next, 
							ol.product_id_magento, ol.product_family_name, 
							min(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) first_order_per_cust
						from
								(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
								from #everclear_elite_sales_an) ee 
							left join
								Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date < ol.order_date) t
					where order_date_next = first_order_per_cust or first_order_per_cust is null) t 
				group by order_id_bk, customer_id) t2 on t1.order_id_bk = t2.order_id_bk
		order by t1.conversion_f, t2.repetition_f
	
	set @rowAmountInsert = @@ROWCOUNT
	set @message = 'Insert # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert)

	drop table #everclear_elite_sales_an

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountInsert = @rowAmountInsert, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go