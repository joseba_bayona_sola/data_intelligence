use Adhoc
go

drop procedure rs.dwh_rs_param_gen_calendar_month
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Calendar Month
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_calendar_month
 	@from_date date, @to_date date
as
begin
	set nocount on

	DECLARE @last_day_of_month as varchar(10)

	create table #calendar_month(
		month_from_date				date, 
		month_to_date				date, 
		calendar_month_name			varchar(50))

	while @from_date < @to_date 
		begin
			set @last_day_of_month = day(dateadd(day, -1, dateadd(month, 1, format(@from_date, '01-MMMM-yy'))))

			insert into #calendar_month(month_from_date, month_to_date, calendar_month_name) values
				(format(@from_date, '01-MMMM-yyyy'), format(@from_date, @last_day_of_month + '-MMMM-yyyy'), format(@from_date, 'MMMM-yyyy'))

			set @from_date = dateadd(month, 1, @from_date)
		end

	select month_from_date, month_to_date, calendar_month_name
	from #calendar_month

	drop table #calendar_month
end;
go 


drop procedure rs.dwh_rs_param_gen_calendar_week_day
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Calendar Week Day
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_calendar_week_day
as
begin
	set nocount on

	select distinct calendar_date_week, calendar_date_week_name
	from Warehouse.gen.dim_calendar
	order by calendar_date_week

end;
go 


drop procedure rs.dwh_rs_param_gen_time_day_part
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Time Day Part
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_time_day_part
as
begin
	set nocount on

	select distinct day_part
	from Warehouse.gen.dim_time

end;
go 



drop procedure rs.dwh_rs_param_gen_company
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Company
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_company
as
begin
	set nocount on

	select distinct idCompany_sk, company_name
	from Warehouse.gen.dim_company
	where idCompany_sk <> -1
	order by company_name

end;
go 


drop procedure rs.dwh_rs_param_gen_acquired
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Acquired
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_acquired
as
begin
	set nocount on

	select distinct acquired
	from Warehouse.gen.dim_store_v

end;
go 


drop procedure rs.dwh_rs_param_gen_website_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Website Group
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_website_group
as
begin
	set nocount on

	select distinct website_group
	from Warehouse.gen.dim_store_v
	where website_group <> 'N/A'
	order by website_group

end;
go 



drop procedure rs.dwh_rs_param_gen_tld
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: TLD
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_tld
as
begin
	set nocount on

	select distinct tld
	from Warehouse.gen.dim_store_v
	where tld not in ('N/A', '', '.de', '.eu', '.se', '.us')
	order by tld

end;
go 



drop procedure rs.dwh_rs_param_gen_website
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Website
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_website
	@acquired	char(1),
	@tld		varchar(max)
as
begin
	set nocount on

	select cast(s.string AS sysname) tld
	into #tld
	from Adhoc.dbo.fnSplitString (@tld,',') AS s

	select distinct s.website
	from 
			Warehouse.gen.dim_store_v s
		inner join
			#tld t on s.tld = t.tld and s.acquired = @acquired
	order by s.website

	drop table #tld
end;
go 



drop procedure rs.dwh_rs_param_gen_store
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Store
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_store
	@tld	varchar(max)
as
begin
	set nocount on

	select cast(s.string AS sysname) tld
	into #tld
	from Adhoc.dbo.fnSplitString (@tld,',') AS s

	select s.idStore_sk, s.store_name
	from 
			Warehouse.gen.dim_store_v s
		inner join
			#tld t on s.tld = t.tld
	order by s.store_name

	drop table #tld

end;
go 



drop procedure rs.dwh_rs_param_gen_market
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Market
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_market
as
begin
	set nocount on

	select idMarket_sk, market_name
	from Warehouse.gen.dim_market
	-- where market_name <> 'N/A'
	order by market_name

end;
go 



drop procedure rs.dwh_rs_param_gen_country
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Country
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_country
as
begin
	set nocount on

	select idCountry_sk, country_name
	from Warehouse.gen.dim_country
	where country_name <> 'XX'
	order by country_name

end;
go 



drop procedure rs.dwh_rs_param_gen_currency
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Currency
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_currency
	@tld	varchar(max)
as
begin
	set nocount on

	DECLARE @tld_co_uk varchar(10)

	select cast(s.string AS sysname) tld
	into #tld
	from Adhoc.dbo.fnSplitString (@tld,',') AS s

	select @tld_co_uk = tld
	from #tld
	where tld = '.co.uk'

	select 1 id, 'Global' caption, '�' as symbol
	union
	select 2 id, 'Local' caption, 
		case when (@tld_co_uk is not null) then '�' else '�' end as symbol
end;
go 


------------------------
------------------------


drop procedure rs.dwh_rs_param_gen_customer_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Customer Type
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_customer_type
as
begin
	set nocount on

	select idCustomerType_sk, customer_type_name
	from Warehouse.gen.dim_customer_type
	order by customer_type_name

end;
go 


drop procedure rs.dwh_rs_param_gen_customer_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Customer Status
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_customer_status
as
begin
	set nocount on

	select idCustomerStatus_sk, customer_status_name
	from Warehouse.gen.dim_customer_status
	order by customer_status_name

end;
go 


drop procedure rs.dwh_rs_param_gen_customer_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Country
-- ==========================================================================================

create procedure rs.dwh_rs_param_gen_customer_unsubscribe
as
begin
	set nocount on

	select idCustomerUnsubscribe_sk, customer_unsubscribe_name
	from Warehouse.gen.dim_customer_unsubscribe
	where customer_unsubscribe_name <> 'N/A'
	order by customer_unsubscribe_name

end;
go 



