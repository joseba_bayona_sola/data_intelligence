
use Adhoc
go


drop procedure rs.dwh_rs_data_finance_oh
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get DATA data: Orders
-- ==========================================================================================

create procedure rs.dwh_rs_data_finance_oh
	@from_date				date,
	@to_date				date,
	@website				varchar(max), 
	@market					varchar(max), 
	@country				varchar(max), 
	@website_group			varchar(max), 
	@marketing_channel		varchar(max), 
	@price_type				varchar(max), 
	@product_family			varchar(max), 
	@rank_qty_time			varchar(max) 

as
begin
	set nocount on

	DECLARE @idSPRun_ssrs bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)

	exec ControlDB.logging.logSPRun_ssrs_start_sp @sp_name = @sp_name, @idSPRun_ssrs = @idSPRun_ssrs output
	
	-- Parameters Preparation
	select s.idStore_sk
	into #website
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website
			from Adhoc.dbo.fnSplitString (@website, ',') AS s) t on s.website = t.website

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'website', @parameter_value = @website
	
	select cast(s.string AS int) idMarket_sk
	into #market
	from Adhoc.dbo.fnSplitString (@market, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'market', @parameter_value = @market

	select cast(s.string AS int) idCountry_sk
	into #country
	from Adhoc.dbo.fnSplitString (@country, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'country', @parameter_value = @country

	select s.idStore_sk
	into #website_group
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website_group
			from Adhoc.dbo.fnSplitString (@website_group, ',') AS s) t on s.website_group = t.website_group

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'website_group', @parameter_value = @website_group

	select cast(s.string AS int) idMarketingChannel_sk
	into #marketing_channel
	from Adhoc.dbo.fnSplitString (@marketing_channel, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'marketing_channel', @parameter_value = @marketing_channel

	select cast(s.string AS int) idPriceType_sk
	into #price_type
	from Adhoc.dbo.fnSplitString (@price_type, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'price_type', @parameter_value = @price_type

	select cast(s.string AS int) idProductFamily_sk
	into #product_family
	from Adhoc.dbo.fnSplitString (@product_family, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'product_family', @parameter_value = @product_family

	select rqt.qty_time_bk qty_time
	into #rank_qty_time
	from 
			Warehouse.sales.dim_rank_qty_time rqt
		inner join
			(select cast(s.string AS sysname) rank_qty_time
			from Adhoc.dbo.fnSplitString (@rank_qty_time, ',') AS s) t on rqt.rank_qty_time = t.rank_qty_time

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'rank_qty_time', @parameter_value = @rank_qty_time


	-- Customer Filtering
	select cs.idCustomer_sk_fk
	into #customers
	from 
			Warehouse.act.fact_customer_signature cs
		inner join
			#website_group wg on cs.idStoreCreate_sk_fk = wg.idStore_sk
		-- inner join #marketing_channel
		-- inner join #price_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT CUST - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- OH Filtering
	select oh.idOrderHeader_sk, oh.order_id_bk  -- oh.order_id_bk 
	into #oh
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			#customers c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			#website w on oh.idStore_sk_fk = w.idStore_sk
		inner join
			#market m on oh.idMarket_sk_fk = m.idMarket_sk
		inner join
			#country co on oh.idCountryShipping_sk_fk = co.idCountry_sk
	where oh.invoice_date between @from_date and @to_date

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT OH - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- OL Filtering
	select ol.idOrderLine_sk -- order_line_id_bk 
	into #ol
	from 
			Warehouse.sales.fact_order_line ol
		inner join
			#oh oh on ol.order_id = oh.order_id_bk
		inner join
			#product_family pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
		inner join
			#rank_qty_time rqt on ol.qty_time = rqt.qty_time

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT OL - Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- DATA
	select order_id_bk, order_no, invoice_date, rank_shipping_days, 
		website, market_name, order_status_name,
		customer_id, customer_email, customer_order_seq_no, 
		product_family_name, qty_unit, qty_pack, qty_time, 
		local_total_inc_vat
	from 
			Warehouse.sales.fact_order_line_v olv
		inner join
			#ol	ol on olv.idOrderLine_sk = ol.idOrderLine_sk
	order by order_no, invoice_date, product_family_name

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT DATA - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message
	
	drop table #ol
	drop table #oh
	drop table #customers

	drop table #website 
	drop table #market 
	drop table #country 
	drop table #website_group 
	drop table #marketing_channel 
	drop table #price_type 
	drop table #product_family 
	drop table #rank_qty_time

	exec ControlDB.logging.logSPRun_ssrs_stop_sp @idSPRun_ssrs = @idSPRun_ssrs, @runStatus = 'COMPLETE'

end;
go 





use Adhoc
go


drop procedure rs.dwh_rs_data_finance_retention
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get DATA data: Retention Report 
-- ==========================================================================================

create procedure rs.dwh_rs_data_finance_retention
	@from_date				date,
	@to_date				date,
	@website				varchar(max), 
	@market					varchar(max), 
	@country				varchar(max), 
	@website_group			varchar(max), 
	@marketing_channel		varchar(max), 
	@price_type				varchar(max), 
	@product_family			varchar(max), 
	@rank_qty_time			varchar(max) 

as
begin
	set nocount on

	DECLARE @idSPRun_ssrs bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)

	exec ControlDB.logging.logSPRun_ssrs_start_sp @sp_name = @sp_name, @idSPRun_ssrs = @idSPRun_ssrs output


	-- Parameters Preparation
	select s.idStore_sk
	into #website
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website
			from Adhoc.dbo.fnSplitString (@website, ',') AS s) t on s.website = t.website

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'website', @parameter_value = @website
	
	select cast(s.string AS int) idMarket_sk
	into #market
	from Adhoc.dbo.fnSplitString (@market, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'market', @parameter_value = @market

	select cast(s.string AS int) idCountry_sk
	into #country
	from Adhoc.dbo.fnSplitString (@country, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'country', @parameter_value = @country

	select s.idStore_sk
	into #website_group
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website_group
			from Adhoc.dbo.fnSplitString (@website_group, ',') AS s) t on s.website_group = t.website_group

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'website_group', @parameter_value = @website_group

	select cast(s.string AS int) idMarketingChannel_sk
	into #marketing_channel
	from Adhoc.dbo.fnSplitString (@marketing_channel, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'marketing_channel', @parameter_value = @marketing_channel

	select cast(s.string AS int) idPriceType_sk
	into #price_type
	from Adhoc.dbo.fnSplitString (@price_type, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'price_type', @parameter_value = @price_type

	select cast(s.string AS int) idProductFamily_sk
	into #product_family
	from Adhoc.dbo.fnSplitString (@product_family, ',') AS s

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'product_family', @parameter_value = @product_family

	select rqt.qty_time_bk qty_time
	into #rank_qty_time
	from 
			Warehouse.sales.dim_rank_qty_time rqt
		inner join
			(select cast(s.string AS sysname) rank_qty_time
			from Adhoc.dbo.fnSplitString (@rank_qty_time, ',') AS s) t on rqt.rank_qty_time = t.rank_qty_time

		exec ControlDB.logging.logSPRun_ssrs_parameter_sp @idSPRun_ssrs = @idSPRun_ssrs, @parameter = 'rank_qty_time', @parameter_value = @rank_qty_time


	-- Customer Filtering
	select cs.idCustomer_sk_fk
	into #customers
	from 
			Warehouse.act.fact_customer_signature cs
		inner join
			#website_group wg on cs.idStoreCreate_sk_fk = wg.idStore_sk
		inner join 
			#marketing_channel mc on cs.idMarketingChannelFirst_sk_fk = mc.idMarketingChannel_sk
		inner join
			#price_type pt on cs.idPriceTypeFirst_sk_fk = mc.idPriceType_sk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT CUST - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- OH Filtering
	select oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no
	into #oh
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			#customers c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			#website w on oh.idStore_sk_fk = w.idStore_sk
		inner join
			#market m on oh.idMarket_sk_fk = m.idMarket_sk
		inner join
			#country co on oh.idCountryShipping_sk_fk = co.idCountry_sk
		inner join
			Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	where oh.invoice_date between @from_date and @to_date
		and os.order_status_name in ('OK', 'PARTIAL REFUND')

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT OH - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- OL Filtering -- RETENTION table
	select oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no, 
		sum(ol.local_total_inc_vat) local_total_inc_vat, sum(ol.local_total_exc_vat) local_total_exc_vat, 
		sum(ol.local_total_inc_vat * ol.local_to_global_rate) global_total_inc_vat, sum(ol.local_total_exc_vat * ol.local_to_global_rate) global_total_exc_vat
	into #retention
	from 
			Warehouse.sales.fact_order_line ol
		inner join
			#oh oh on ol.idOrderHeader_sk_fk = oh.idOrderHeader_sk
		inner join
			#product_family pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
		inner join
			#rank_qty_time rqt on ol.qty_time = rqt.qty_time
	group by oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT RET_OL - Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message


	-- DATA
	select period, customer_order_seq_no, month_diff, 
		
		count(distinct idCustomer_sk_fk) customer_count, sum(num_cust) next_order_customer_count, 
		sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_exc_vat) local_total_exc_vat, sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_exc_vat,
		sum(next_local_total_inc_vat) next_local_total_inc_vat, sum(next_local_total_exc_vat) next_local_total_exc_vat, 
		sum(next_global_total_inc_vat) next_global_total_inc_vat, sum(next_global_total_exc_vat) next_global_total_exc_vat
	from
		(select idCustomer_sk_fk, customer_order_seq_no, customer_order_seq_no_own, 
			period, period_next, datediff(month, period, period_next) as month_diff, 

			case when (period_next is not null) then 1 else NULL end num_cust,
			local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,
			next_local_total_inc_vat, next_local_total_exc_vat, next_global_total_inc_vat, next_global_total_exc_vat
		from
			(select idCustomer_sk_fk, 
				invoice_date, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				next_invoice_date, cast(cast(year(next_invoice_date) as varchar) + '-' + cast(month(next_invoice_date) as varchar) + '-01' as date) period_next, 
				customer_order_seq_no, customer_order_seq_no_own, 

				local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,
				next_local_total_inc_vat, next_local_total_exc_vat, next_global_total_inc_vat, next_global_total_exc_vat
			from
				(select idCustomer_sk_fk, 
					invoice_date, yyyy, mm, 
					customer_order_seq_no, customer_order_seq_no_own,
					local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,

					lead(invoice_date) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_invoice_date, 
					lead(local_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_inc_vat, 
					lead(local_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_exc_vat, 
					lead(global_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_inc_vat, 
					lead(global_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_exc_vat

				from
					(select idOrderHeader_sk, idCustomer_sk_fk, invoice_date, year(invoice_date) yyyy, month(invoice_date) mm, 
						customer_order_seq_no, 
						rank() over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) customer_order_seq_no_own,
						local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat
					from #retention) t) t) t) t
	group by period, customer_order_seq_no, month_diff
	order by customer_order_seq_no, period, month_diff


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT DATA - # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_ssrs_message_sp @idSPRun_ssrs = @idSPRun_ssrs, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #retention
	drop table #oh
	drop table #customers

	drop table #website 
	drop table #market 
	drop table #country 
	drop table #website_group 
	drop table #marketing_channel 
	drop table #price_type 
	drop table #product_family 
	drop table #rank_qty_time

	exec ControlDB.logging.logSPRun_ssrs_stop_sp @idSPRun_ssrs = @idSPRun_ssrs, @runStatus = 'COMPLETE'

end;
go 

