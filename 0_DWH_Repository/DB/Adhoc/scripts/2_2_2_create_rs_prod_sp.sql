use Adhoc
go



drop procedure rs.dwh_rs_param_prod_product_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Product Type
-- ==========================================================================================

create procedure rs.dwh_rs_param_prod_product_type
as
begin
	set nocount on

	select idProductType_sk, product_type_name
	from Warehouse.prod.dim_product_type
	order by product_type_name

end;
go 



drop procedure rs.dwh_rs_param_prod_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Category
-- ==========================================================================================

create procedure rs.dwh_rs_param_prod_category
	@product_type	varchar(max)
as
begin
	set nocount on

	select cast(s.string AS int) idProductType_sk
	into #product_type
	from Adhoc.dbo.fnSplitString (@product_type,',') AS s

	select c.idCategory_sk, c.category_name
	from 
			Warehouse.prod.dim_category c
		inner join
			#product_type pt on c.idProductType_sk_fk = pt.idProductType_sk	
	order by c.category_name

end;
go 



drop procedure rs.dwh_rs_param_prod_product_family_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Product Family Group
-- ==========================================================================================

create procedure rs.dwh_rs_param_prod_product_family_group
as
begin
	set nocount on

	select idProductFamilyGroup_sk, product_family_group_name
	from warehouse.prod.dim_product_family_group
	order by product_family_group_name

end;
go 



drop procedure rs.dwh_rs_param_prod_product_family
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Product Family
-- ==========================================================================================

create procedure rs.dwh_rs_param_prod_product_family
	@category	varchar(max)
as
begin
	set nocount on

	select cast(s.string AS int) idCategory_sk
	into #category
	from Adhoc.dbo.fnSplitString (@category,',') AS s

	select pf.idProductFamily_sk, pf.product_id_magento, pf.product_family_name
	from 
			Warehouse.prod.dim_product_family_v pf
		inner join
			#category c on pf.idCategory_sk = c.idCategory_sk
	order by pf.product_family_name

end;
go 

