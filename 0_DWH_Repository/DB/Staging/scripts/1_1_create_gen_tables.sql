use Staging
go 


create table Staging.gen.dim_time(
	time_name_bk		varchar(10) NOT NULL, 
	time_name			varchar(10) NOT NULL,
	hour_name			varchar(10) NOT NULL,
	hour				char(2) NOT NULL,
	minute				char(2) NOT NULL,
	day_part			varchar(20) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.gen.dim_time add constraint [PK_gen_dim_time]
	primary key clustered (time_name_bk);
go

alter table Staging.gen.dim_time add constraint [DF_gen_dim_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.gen.dim_company(
	company_name_bk		varchar(100) NOT NULL, 
	company_name		varchar(100) NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.gen.dim_company add constraint [PK_gen_dim_company]
	primary key clustered (company_name_bk);
go

alter table Staging.gen.dim_company add constraint [DF_gen_dim_company_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.gen.dim_store(
	store_id_bk			int NOT NULL, 
	store_name			varchar(100) NULL, 
	website_type		varchar(100) NULL, 
	website_group		varchar(100) NULL, 
	website				varchar(100) NULL, 
	tld					varchar(100) NULL, 
	code_tld			varchar(100) NULL, 
	acquired			char(1) NOT NULL, 
	company_name_bk		varchar(100),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Staging.gen.dim_store add constraint [PK_gen_dim_store]
	primary key clustered (store_id_bk);
go

alter table Staging.gen.dim_store add constraint [DF_gen_dim_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.gen.dim_country(
	country_id_bk			char(2) NOT NULL, 
	country_code			char(2) NOT NULL, 
	country_name			varchar(50) NOT NULL,
	country_zone			varchar(10) NOT NULL,
	country_type			varchar(10) NOT NULL,
	country_continent		varchar(20) NOT NULL,
	country_state			varchar(10) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go

alter table Staging.gen.dim_country add constraint [PK_gen_dim_country]
	primary key clustered (country_id_bk);
go

alter table Staging.gen.dim_country add constraint [DF_gen_dim_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go  


create table Staging.gen.dim_region(
	region_id_bk		int NOT NULL, 
	region_name			varchar(255) NOT NULL, 
	country_id_bk		char(2), 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.gen.dim_region add constraint [PK_gen_dim_region]
	primary key clustered (region_id_bk);
go

alter table Staging.gen.dim_region add constraint [DF_gen_dim_region_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.gen.dim_market(
	market_id_bk		int NOT NULL, 
	market_name			varchar(100) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.gen.dim_market add constraint [PK_gen_dim_market]
	primary key clustered (market_id_bk);
go

alter table Staging.gen.dim_market add constraint [DF_gen_dim_market_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.gen.dim_customer_type(
	customer_type_name_bk	varchar(50) NOT NULL,
	customer_type_name		varchar(50) NOT NULL, 
	description				varchar(255) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.gen.dim_customer_type add constraint [PK_gen_dim_customer_type]
	primary key clustered (customer_type_name_bk);
go

alter table Staging.gen.dim_customer_type add constraint [DF_gen_dim_customer_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.gen.dim_customer_status(
	customer_status_name_bk		varchar(50) NOT NULL,
	customer_status_name		varchar(50) NOT NULL, 
	description					varchar(255) NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.gen.dim_customer_status add constraint [PK_gen_dim_customer_status]
	primary key clustered (customer_status_name_bk);
go

alter table Staging.gen.dim_customer_status add constraint [DF_gen_dim_customer_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.gen.dim_customer_unsubscribe(
	customer_unsubscribe_name_bk	varchar(10) NOT NULL,
	customer_unsubscribe_name		varchar(10) NOT NULL, 
	description						varchar(255) NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.gen.dim_customer_unsubscribe add constraint [PK_gen_dim_customer_unsubscribe]
	primary key clustered (customer_unsubscribe_name_bk);
go

alter table Staging.gen.dim_customer_unsubscribe add constraint [DF_gen_dim_customer_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.gen.dim_customer(
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime, 
	updated_at									datetime, 
	store_id_bk									int NOT NULL, 
	market_id_bk								int, 
	customer_type_name_bk						varchar(50) NOT NULL, 
	customer_status_name_bk						varchar(50),
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	region_id_bk								int, 
	country_id_bk								char(2), 
	postcode_s									varchar(255),
	country_id_s_bk								char(2), 
	customer_unsubscribe_name_bk				varchar(10),
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d			int,		
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d			int,		
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d					int,
	reminder_f									char(1) NOT NULL,
	reminder_type_name_bk						varchar(50),
	reminder_period_bk							int,
	reorder_f									char(1) NOT NULL,
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	migrate_customer_f							char(1) NOT NULL,
	migrate_customer_store						varchar(255),
	migrate_customer_id							bigint,
	idETLBatchRun								bigint NOT NULL, 
	ins_ts										datetime NOT NULL);
go

alter table Staging.gen.dim_customer add constraint [PK_gen_dim_customer]
	primary key clustered (customer_id_bk);
go

alter table Staging.gen.dim_customer add constraint [DF_gen_dim_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 