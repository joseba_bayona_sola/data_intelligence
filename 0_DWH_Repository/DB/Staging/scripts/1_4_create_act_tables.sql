use Staging
go 

create table Staging.act.dim_activity_group(
	activity_group_name_bk		varchar(50) NOT NULL,
	activity_group_name			varchar(50) NOT NULL,  
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.act.dim_activity_group add constraint [PK_act_dim_activity_group]
	primary key clustered (activity_group_name_bk);
go

alter table Staging.act.dim_activity_group add constraint [DF_act_dim_activity_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.act.dim_activity_type(
	activity_type_name_bk		varchar(50) NOT NULL,
	activity_type_name			varchar(50) NOT NULL,  
	activity_group_name_bk		varchar(50) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.act.dim_activity_type add constraint [PK_act_dim_activity_type]
	primary key clustered (activity_type_name_bk);
go

alter table Staging.act.dim_activity_type add constraint [DF_act_dim_activity_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.act.fact_activity_sales(
	order_id_bk						bigint NOT NULL,
	
	idCalendarActivityDate			int NOT NULL, 
	activity_date					datetime NOT NULL,
	idCalendarPrevActivityDate		int, 
	idCalendarNextActivityDate		int, 

	activity_type_name_bk			varchar(50) NOT NULL,
	store_id_bk						int NOT NULL,
	customer_id_bk					int NOT NULL,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	product_type_oh_bk				varchar(50),  
	order_qty_time					int,  
	num_diff_product_type_oh		int,

	customer_status_name_bk			varchar(50), 
	customer_order_seq_no			int, 
	customer_order_seq_no_web		int, 

	local_subtotal					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),

	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.act.fact_activity_sales add constraint [PK_act_fact_activity_sales]
	primary key clustered (order_id_bk);
go

alter table Staging.act.fact_activity_sales add constraint [DF_act_fact_activity_sales_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Staging.act.fact_activity_sales add constraint [UNIQ_act_fact_activity_sales_cust_act_type_order_seq_no]
	unique (customer_id_bk, activity_type_name_bk, customer_order_seq_no);
go

create table Staging.act.fact_activity_other(
	activity_id_bk					bigint NOT NULL,
	
	idCalendarActivityDate			int NOT NULL, 
	activity_date					datetime NOT NULL,
	idCalendarPrevActivityDate		int, 

	activity_type_name_bk			varchar(50) NOT NULL,
	store_id_bk						int NOT NULL,
	customer_id_bk					int NOT NULL,

	activity_seq_no					int NOT NULL, 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.act.fact_activity_other add constraint [PK_act_fact_activity_other]
	primary key clustered (activity_id_bk, activity_type_name_bk, idCalendarActivityDate);
go

alter table Staging.act.fact_activity_other add constraint [DF_act_fact_activity_other_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Staging.act.fact_activity_other add constraint [UNIQ_act_fact_activity_other_cust_act_type_act_seq_no]
	unique (customer_id_bk, activity_type_name_bk, activity_seq_no);
go



create table Staging.act.dim_customer_cr_reg(
	customer_id_bk				int NOT NULL, 
	store_id_bk_cr				int NOT NULL, 
	idCalendarCreateDate		int NOT NULL, 
	store_id_bk_reg				int NOT NULL, 
	idCalendarRegisterDate		int NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.act.dim_customer_cr_reg add constraint [PK_act_dim_customer_cr_reg]
	primary key clustered (customer_id_bk);
go

alter table Staging.act.dim_customer_cr_reg add constraint [DF_act_dim_customer_cr_reg_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.act.dim_rank_cr_time_mm(
	cr_time_mm_bk				int NOT NULL, 
	cr_time_mm					char(2) NOT NULL,
	cr_time_name				varchar(10) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.act.dim_rank_cr_time_mm add constraint [PK_act_dim_rank_cr_time_mm]
	primary key clustered (cr_time_mm_bk);
go

alter table Staging.act.dim_rank_cr_time_mm add constraint [DF_act_dim_rank_cr_time_mm_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.act.fact_customer_product_signature(
	customer_id_bk				int NOT NULL, 
	product_id_bk				int NOT NULL, 
	idCalendarFirstOrderDate	int NOT NULL, 
	idCalendarLastOrderDate		int NOT NULL, 
	num_tot_orders				int NOT NULL, 
	subtotal_tot_orders			decimal(12, 4) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.act.fact_customer_product_signature add constraint [PK_act_fact_customer_product_signature]
	primary key clustered (customer_id_bk, product_id_bk);
go

alter table Staging.act.fact_customer_product_signature add constraint [DF_act_fact_customer_product_signature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

