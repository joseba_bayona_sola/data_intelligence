use Staging
go 



select activity_group_name_bk, activity_group_name, 
	idETLBatchRun, ins_ts
from Staging.act.dim_activity_group

select activity_type_name_bk, activity_type_name, 
	activity_group_name_bk, 
	idETLBatchRun, ins_ts
from Staging.act.dim_activity_type



select 
	order_id_bk, 
	idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
	activity_type_name_bk, store_id_bk, customer_id_bk, 
	channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
	customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, 

	local_subtotal, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, 
	local_to_global_rate, order_currency_code,
	
	idETLBatchRun, ins_ts
from Staging.act.fact_activity_sales


select 
	activity_id_bk, 
	idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
	activity_type_name_bk, store_id_bk, customer_id_bk, 
	activity_seq_no, 

	idETLBatchRun, ins_ts
from Staging.act.fact_activity_other



select customer_id_bk, 
	store_id_bk_cr, idCalendarCreateDate, 
	store_id_bk_reg, idCalendarRegisterDate,
	idETLBatchRun, ins_ts
from Staging.act.dim_customer_cr_reg


select cr_time_mm_bk, 
	cr_time_mm, cr_time_name,
	idETLBatchRun, ins_ts
from Staging.act.dim_rank_cr_time_mm



select customer_id_bk, product_id_bk, 
	idCalendarFirstOrderDate, idCalendarLastOrderDate, 
	num_tot_orders, subtotal_tot_orders, 
	idETLBatchRun, ins_ts
from Staging.act.fact_customer_product_signature

