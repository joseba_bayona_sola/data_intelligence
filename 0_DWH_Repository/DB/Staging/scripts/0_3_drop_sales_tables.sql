use Staging
go 

drop table Staging.sales.dim_order_header
go

drop table Staging.sales.dim_order_header_product_type
go

drop table Staging.sales.fact_order_line
go



drop table Staging.sales.dim_reminder_type
go

drop table Staging.sales.dim_reminder_period
go


drop table Staging.sales.dim_order_stage
go
drop table Staging.sales.dim_order_status
go
drop table Staging.sales.dim_order_type
go
drop table Staging.sales.dim_order_status_magento
go

drop table Staging.sales.dim_payment_method
go
drop table Staging.sales.dim_cc_type
go

drop table Staging.sales.dim_shipping_carrier
go
drop table Staging.sales.dim_shipping_method
go

drop table Staging.sales.dim_telesale
go

drop table Staging.sales.dim_price_type
go


drop table Staging.sales.dim_prescription_method
go



drop table Staging.sales.dim_channel
go

drop table Staging.sales.dim_marketing_channel
go

drop table Staging.sales.dim_affiliate
go

drop table Staging.sales.dim_group_coupon_code
go

drop table Staging.sales.dim_coupon_code
go


drop table Staging.sales.dim_device_category
go

drop table Staging.sales.dim_device_brand
go

drop table Staging.sales.dim_device_model
go

drop table Staging.sales.dim_device_browser
go

drop table Staging.sales.dim_device_os
go


drop table Staging.sales.dim_rank_customer_order_seq_no
go

drop table Staging.sales.dim_rank_qty_time
go

drop table Staging.sales.dim_rank_freq_time
go

drop table Staging.sales.dim_rank_shipping_days
go


drop table Staging.sales.fact_order_line_vat_fix
go



