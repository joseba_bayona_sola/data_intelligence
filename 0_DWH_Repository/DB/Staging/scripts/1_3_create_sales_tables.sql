use Staging
go 

create table Staging.sales.dim_reminder_type(
	reminder_type_name_bk	varchar(50) NOT NULL,
	reminder_type_name		varchar(50) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.sales.dim_reminder_type add constraint [PK_sales_dim_reminder_type]
	primary key clustered (reminder_type_name_bk);
go

alter table Staging.sales.dim_reminder_type add constraint [DF_sales_dim_reminder_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_reminder_period(
	reminder_period_bk		int NOT NULL,
	reminder_period_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.sales.dim_reminder_period add constraint [PK_sales_dim_reminder_period]
	primary key clustered (reminder_period_bk);
go

alter table Staging.sales.dim_reminder_period add constraint [DF_sales_dim_reminder_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.sales.dim_order_stage(
	order_stage_name_bk			varchar(20) NOT NULL,
	order_stage_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_order_stage add constraint [PK_sales_dim_order_stage]
	primary key clustered (order_stage_name_bk);
go

alter table Staging.sales.dim_order_stage add constraint [DF_sales_dim_order_stage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_order_status(
	order_status_name_bk		varchar(20) NOT NULL,
	order_status_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_order_status add constraint [PK_sales_dim_order_status]
	primary key clustered (order_status_name_bk);
go

alter table Staging.sales.dim_order_status add constraint [DF_sales_dim_order_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_order_type(
	order_type_name_bk			varchar(20) NOT NULL,
	order_type_name				varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_order_type add constraint [PK_sales_dim_order_type]
	primary key clustered (order_type_name_bk);
go

alter table Staging.sales.dim_order_type add constraint [DF_sales_dim_order_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_order_status_magento(
	status_bk					varchar(32) NOT NULL,
	order_status_magento_name	varchar(32) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_order_status_magento add constraint [PK_sales_dim_order_status_magento]
	primary key clustered (status_bk);
go

alter table Staging.sales.dim_order_status_magento add constraint [DF_sales_dim_order_status_magento_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.sales.dim_payment_method(
	payment_method_name_bk		varchar(255) NOT NULL,
	payment_method_name			varchar(255) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_payment_method add constraint [PK_sales_dim_payment_method]
	primary key clustered (payment_method_name_bk);
go

alter table Staging.sales.dim_payment_method add constraint [DF_sales_dim_payment_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_cc_type(
	cc_type_name_bk				varchar(50) NOT NULL,
	cc_type_name				varchar(50) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_cc_type add constraint [PK_sales_dim_cc_type]
	primary key clustered (cc_type_name_bk);
go

alter table Staging.sales.dim_cc_type add constraint [DF_sales_dim_cc_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_shipping_carrier(
	shipping_carrier_name_bk	varchar(50) NOT NULL,
	shipping_carrier_name		varchar(50) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_shipping_carrier add constraint [PK_sales_dim_shipping_carrier]
	primary key clustered (shipping_carrier_name_bk);
go

alter table Staging.sales.dim_shipping_carrier add constraint [DF_sales_dim_shipping_carrier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_shipping_method(
	shipping_description_bk		varchar(255) NOT NULL,
	shipping_method_name		varchar(255) NOT NULL,
	shipping_carrier_name_bk	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_shipping_method add constraint [PK_sales_dim_shipping_method]
	primary key clustered (shipping_description_bk);
go

alter table Staging.sales.dim_shipping_method add constraint [DF_sales_dim_shipping_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_telesale(
	telesales_admin_username_bk			varchar(40) NOT NULL,
	telesales_username					varchar(40) NOT NULL, 
	user_id								int NOT NULL, 
	telesales_first_name				varchar(32), 
	telesales_last_name					varchar(32),
	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go

alter table Staging.sales.dim_telesale add constraint [PK_sales_dim_telesale]
	primary key clustered (telesales_admin_username_bk);
go

alter table Staging.sales.dim_telesale add constraint [DF_sales_dim_telesale_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_price_type(
	price_type_name_bk			varchar(40) NOT NULL,
	price_type_name				varchar(40) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_price_type add constraint [PK_sales_dim_price_type]
	primary key clustered (price_type_name_bk);
go

alter table Staging.sales.dim_price_type add constraint [DF_sales_dim_price_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_prescription_method(
	prescription_method_name_bk		varchar(50) NOT NULL,
	prescription_method_name		varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.sales.dim_prescription_method add constraint [PK_sales_dim_prescription_method]
	primary key clustered (prescription_method_name_bk);
go

alter table Staging.sales.dim_prescription_method add constraint [DF_sales_dim_prescription_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.sales.dim_channel(
	channel_name_bk				varchar(50) NOT NULL,
	channel_name				varchar(50) NOT NULL,
	description					varchar(255),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_channel add constraint [PK_sales_dim_channel]
	primary key clustered (channel_name_bk);
go

alter table Staging.sales.dim_channel add constraint [DF_sales_dim_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_marketing_channel(
	marketing_channel_name_bk	varchar(50) NOT NULL,
	marketing_channel_name		varchar(50) NOT NULL,
	sup_marketing_channel_name	varchar(50),
	description					varchar(255),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_marketing_channel add constraint [PK_sales_dim_marketing_channel]
	primary key clustered (marketing_channel_name_bk);
go

alter table Staging.sales.dim_marketing_channel add constraint [DF_sales_dim_marketing_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_affiliate(
	publisher_id_bk				int NOT NULL, 
	affiliate_name				varchar(255) NOT NULL, 
	affiliate_website			varchar(255), 
	affiliate_network_name		varchar(50), 
	affiliate_type_name			varchar(50), 
	affiliate_status_name		varchar(20),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_affiliate add constraint [PK_sales_dim_affiliate]
	primary key clustered (publisher_id_bk);
go

alter table Staging.sales.dim_affiliate add constraint [DF_sales_dim_affiliate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_group_coupon_code(
	channel_bk					varchar(255) NOT NULL,
	group_coupon_code_name		varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_group_coupon_code add constraint [PK_sales_dim_group_coupon_code]
	primary key clustered (channel_bk);
go

alter table Staging.sales.dim_group_coupon_code add constraint [DF_sales_dim_group_coupon_code_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_coupon_code(
	coupon_id_bk				int NOT NULL,
	channel_bk					varchar(255),
	rule_id						int,
	coupon_code					varchar(255) NOT NULL,
	coupon_code_name			varchar(255),
	from_date					date, 
	to_date						date,
	expiration_date				datetime,
	discount_type_name			varchar(32), 
	discount_amount				decimal(12, 4),
	only_new_customer			char(1),
	refer_a_friend				char(1),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_coupon_code add constraint [PK_sales_dim_coupon_code]
	primary key clustered (coupon_id_bk);
go

alter table Staging.sales.dim_coupon_code add constraint [DF_sales_dim_coupon_code_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.sales.dim_device_category(
	device_category_name_bk		varchar(20) NOT NULL,
	device_category_name		varchar(20),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_device_category add constraint [PK_sales_dim_device_category]
	primary key clustered (device_category_name_bk);
go

alter table Staging.sales.dim_device_category add constraint [DF_sales_dim_device_category_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_device_brand(
	device_brand_name_bk		varchar(50) NOT NULL,
	device_category_name_bk		varchar(20) NOT NULL,
	device_brand_name			varchar(50),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_device_brand add constraint [PK_sales_dim_device_brand]
	primary key clustered (device_brand_name_bk);
go

alter table Staging.sales.dim_device_brand add constraint [DF_sales_dim_device_brand_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_device_model(
	device_model_name_bk		varchar(255) NOT NULL, 
	device_brand_name_bk		varchar(50) NOT NULL,
	device_model_name			varchar(255), 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_device_model add constraint [PK_sales_dim_device_model]
	primary key clustered (device_model_name_bk);
go

alter table Staging.sales.dim_device_model add constraint [DF_sales_dim_device_model_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_device_browser(
	device_browser_name_bk		varchar(50) NOT NULL, 
	device_browser_name			varchar(50),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_device_browser add constraint [PK_sales_dim_device_browser]
	primary key clustered (device_browser_name_bk);
go

alter table Staging.sales.dim_device_browser add constraint [DF_sales_dim_device_browser_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.sales.dim_device_os(
	device_os_name_bk			varchar(50) NOT NULL, 
	device_os_name				varchar(50),
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_device_os add constraint [PK_sales_dim_device_os]
	primary key clustered (device_os_name_bk);
go

alter table Staging.sales.dim_device_os add constraint [DF_sales_dim_device_os_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_rank_customer_order_seq_no(
	customer_order_seq_no_bk	int NOT NULL, 
	rank_seq_no					char(2) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_rank_customer_order_seq_no add constraint [PK_sales_dim_rank_customer_order_seq_no]
	primary key clustered (customer_order_seq_no_bk);
go

alter table Staging.sales.dim_rank_customer_order_seq_no add constraint [DF_sales_dim_rank_customer_order_seq_no_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_rank_qty_time(
	qty_time_bk					int NOT NULL, 
	rank_qty_time				char(2) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_rank_qty_time add constraint [PK_sales_dim_rank_qty_time]
	primary key clustered (qty_time_bk);
go

alter table Staging.sales.dim_rank_qty_time add constraint [DF_sales_dim_rank_qty_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_rank_freq_time(
	freq_time_bk				int NOT NULL, 
	rank_freq_time				char(2) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_rank_freq_time add constraint [PK_sales_dim_rank_freq_time]
	primary key clustered (freq_time_bk);
go

alter table Staging.sales.dim_rank_freq_time add constraint [DF_sales_dim_rank_freq_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_rank_shipping_days(
	shipping_days_bk			int NOT NULL, 
	rank_shipping_days			char(2) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.sales.dim_rank_shipping_days add constraint [PK_sales_dim_rank_shipping_days]
	primary key clustered (shipping_days_bk);
go

alter table Staging.sales.dim_rank_shipping_days add constraint [DF_sales_dim_rank_shipping_days_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


----------------------------------------------------------

create table Staging.sales.dim_order_header(
	order_id_bk						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarOrderDate				int NOT NULL, 
	idTimeOrderDate					varchar(10) NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime,

	store_id_bk						int NOT NULL,
	market_id_bk					int, 
	customer_id_bk					int NOT NULL,
	country_id_shipping_bk			char(2), 
	postcode_shipping				varchar(255), 
	country_id_billing_bk			char(2),
	postcode_billing				varchar(255),
	customer_unsubscribe_name_bk	varchar(10),

	order_stage_name_bk				varchar(20) NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	adjustment_order				char(1),
	order_type_name_bk				varchar(20),
	status_bk						varchar(32),
	payment_method_name_bk			varchar(20),
	cc_type_name_bk					varchar(50),
	shipping_description_bk			varchar(255),
	telesales_admin_username_bk		varchar(40), 
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	prescription_method_name_bk		varchar(50),
	reminder_type_name_bk			varchar(50),
	reminder_period_bk				int, 
	reminder_date					datetime,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	publisher_id_bk					int, 
	coupon_id_bk					int,
	device_model_name_bk			varchar(255), 
	device_browser_name_bk			varchar(50),  
	device_os_name_bk				varchar(50), 

	customer_status_name_bk			varchar(50), 
	customer_order_seq_no			int, 
	customer_order_seq_no_web		int, 
	order_source					char(1),
	proforma						char(1),

	product_type_oh_bk				varchar(50),  
	order_qty_time					int,  
	num_diff_product_type_oh		int,
	aura_product_p					decimal(12, 4),

	countries_registered_code		varchar(10), 

	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int,

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.sales.dim_order_header add constraint [PK_sales_dim_order_header]
	primary key clustered (order_id_bk);
go

alter table Staging.sales.dim_order_header add constraint [DF_sales_dim_order_header_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.dim_order_header_product_type(
	order_id_bk						bigint NOT NULL,
	product_type_oh_bk				varchar(50) NOT NULL, 
	order_percentage				decimal(12, 2), 
	order_flag						bit, 
	order_qty_time					int,  
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.sales.dim_order_header_product_type add constraint [PK_sales_dim_order_header_product_type]
	primary key clustered (order_id_bk, product_type_oh_bk);
go

alter table Staging.sales.dim_order_header_product_type add constraint [DF_sales_dim_order_header_product_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.fact_order_line(
	order_line_id_bk				bigint NOT NULL,
	order_id						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime,

	warehouse_id_bk					bigint NOT NULL,
	order_id_bk						bigint NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),

	product_id_bk					int NOT NULL, 
	base_curve_bk					char(3),
	diameter_bk						char(4),
	power_bk						char(6),
	cylinder_bk						char(5),
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1), 
	colour_bk						varchar(50), 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	glass_vision_type_bk			varchar(50),
	glass_package_type_bk			varchar(50),

	aura_product_f					char(1),

	countries_registered_code		varchar(10), 
	product_type_vat				varchar(255),

	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int,

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.sales.fact_order_line add constraint [PK_sales_fact_order_line]
	primary key clustered (order_line_id_bk);
go

alter table Staging.sales.fact_order_line add constraint [DF_sales_fact_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.sales.fact_order_line_vat_fix(
	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),
	exchange_rate						decimal(12, 4),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Staging.sales.fact_order_line_vat_fix add constraint [PK_sales_fact_order_line_vat_fix]
	primary key clustered (order_line_id_bk);
go

alter table Staging.sales.fact_order_line_vat_fix add constraint [DF_sales_fact_order_line_vat_fix_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
