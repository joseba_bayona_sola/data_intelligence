use Staging
go 

drop procedure act.stg_dwh_get_act_activity_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Group in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_activity_group
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select activity_group_name_bk, activity_group_name, @idETLBatchRun
	from Staging.act.dim_activity_group
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure act.stg_dwh_get_act_activity_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Type in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_activity_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select activity_type_name_bk, activity_type_name, 
		activity_group_name_bk, 
		@idETLBatchRun
	from Staging.act.dim_activity_type
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






drop procedure act.stg_dwh_get_act_activity_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	07-09-2017	Joseba Bayona Sola	Add customer_oder_seq_no_web attribute
	--	16-10-2017	Joseba Bayona Sola	Add idCalendarNextActivityDate attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk attribute
-- ==========================================================================================
-- Description: Reads data from Activity Sales in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_activity_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select order_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
		customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, 

		local_subtotal, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, 
		local_to_global_rate, order_currency_code,
		
		@idETLBatchRun
	from Staging.act.fact_activity_sales
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure act.stg_dwh_get_act_activity_other
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Other in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_activity_other
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select activity_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		activity_seq_no, 

		@idETLBatchRun
	from Staging.act.fact_activity_other
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure act.stg_dwh_get_act_customer_cr_reg
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer CR - REG in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_customer_cr_reg
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select customer_id_bk, 
		store_id_bk_cr, idCalendarCreateDate, 
		store_id_bk_reg, idCalendarRegisterDate,
		@idETLBatchRun
	from Staging.act.dim_customer_cr_reg
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure act.stg_dwh_get_act_rank_cr_time_mm
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer CR - REG in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_rank_cr_time_mm
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select cr_time_mm_bk, 
		cr_time_mm, cr_time_name,
		@idETLBatchRun
	from Staging.act.dim_rank_cr_time_mm
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





drop procedure act.stg_dwh_get_act_customer_product_signature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 21-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Customer Product Signature in Staging Table 
-- ==========================================================================================

create procedure act.stg_dwh_get_act_customer_product_signature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select customer_id_bk, product_id_bk, 
		idCalendarFirstOrderDate, idCalendarLastOrderDate, 
		num_tot_orders, subtotal_tot_orders, 
		@idETLBatchRun
	from Staging.act.fact_customer_product_signature

	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

