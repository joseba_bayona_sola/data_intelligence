use Staging
go 


drop table Staging.prod.dim_product_type
go

drop table Staging.prod.dim_category
go


drop table Staging.prod.dim_cl_type
go

drop table Staging.prod.dim_cl_feature
go


drop table Staging.prod.dim_manufacturer
go



drop table Staging.prod.dim_product_lifecycle
go

drop table Staging.prod.dim_product_visibility
go


drop table Staging.prod.dim_glass_vision_type
go

drop table Staging.prod.dim_glass_package_type
go



drop table Staging.prod.dim_param_BC
go

drop table Staging.prod.dim_param_DI
go

drop table Staging.prod.dim_param_PO
go


drop table Staging.prod.dim_param_CY
go

drop table Staging.prod.dim_param_AX
go


drop table Staging.prod.dim_param_AD
go

drop table Staging.prod.dim_param_DO
go


drop table Staging.prod.dim_param_COL
go



drop table Staging.prod.dim_product_family
go



drop table Staging.prod.dim_product_type_oh
go


drop table Staging.prod.dim_product_family_group
go

