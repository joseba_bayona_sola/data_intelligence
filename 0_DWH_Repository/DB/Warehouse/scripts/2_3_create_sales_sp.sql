use Warehouse
go 


drop procedure sales.stg_dwh_merge_sales_reminder_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Reminder Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_reminder_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_reminder_type with (tablock) as trg
	using Warehouse.sales.dim_reminder_type_wrk src
		on (trg.reminder_type_name_bk = src.reminder_type_name_bk)
	when matched and not exists 
		(select isnull(trg.reminder_type_name, '')
		intersect
		select isnull(src.reminder_type_name, ''))
		then 
			update set
				trg.reminder_type_name = src.reminder_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (reminder_type_name_bk, reminder_type_name, idETLBatchRun_ins)
				values (src.reminder_type_name_bk, src.reminder_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_reminder_period
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Reminder Period from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_reminder_period
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_reminder_period with (tablock) as trg
	using Warehouse.sales.dim_reminder_period_wrk src
		on (trg.reminder_period_bk = src.reminder_period_bk)
	when matched and not exists 
		(select isnull(trg.reminder_period_name, '')
		intersect
		select isnull(src.reminder_period_name, ''))
		then 
			update set
				trg.reminder_period_name = src.reminder_period_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (reminder_period_bk, reminder_period_name, idETLBatchRun_ins)
				values (src.reminder_period_bk, src.reminder_period_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_order_stage
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Stage from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_stage
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_stage with (tablock) as trg
	using Warehouse.sales.dim_order_stage_wrk src
		on (trg.order_stage_name_bk = src.order_stage_name_bk)
	when matched and not exists 
		(select isnull(trg.order_stage_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_stage_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_stage_name = src.order_stage_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_stage_name_bk, order_stage_name, description, idETLBatchRun_ins)
				values (src.order_stage_name_bk, src.order_stage_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_order_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_status with (tablock) as trg
	using Warehouse.sales.dim_order_status_wrk src
		on (trg.order_status_name_bk = src.order_status_name_bk)
	when matched and not exists 
		(select isnull(trg.order_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_status_name = src.order_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_status_name_bk, order_status_name, description, idETLBatchRun_ins)
				values (src.order_status_name_bk, src.order_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_order_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_type with (tablock) as trg
	using Warehouse.sales.dim_order_type_wrk src
		on (trg.order_type_name_bk = src.order_type_name_bk)
	when matched and not exists 
		(select isnull(trg.order_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_type_name = src.order_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_type_name_bk, order_type_name, description, idETLBatchRun_ins)
				values (src.order_type_name_bk, src.order_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_order_status_magento
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Status Magento from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_status_magento
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_status_magento with (tablock) as trg
	using Warehouse.sales.dim_order_status_magento_wrk src
		on (trg.status_bk = src.status_bk)
	when matched and not exists 
		(select isnull(trg.order_status_magento_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_status_magento_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_status_magento_name = src.order_status_magento_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (status_bk, order_status_magento_name, description, idETLBatchRun_ins)
				values (src.status_bk, src.order_status_magento_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_payment_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Payment Method from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_payment_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_payment_method with (tablock) as trg
	using Warehouse.sales.dim_payment_method_wrk src
		on (trg.payment_method_name_bk = src.payment_method_name_bk)
	when matched and not exists 
		(select isnull(trg.payment_method_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.payment_method_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.payment_method_name = src.payment_method_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (payment_method_name_bk, payment_method_name, description, idETLBatchRun_ins)
				values (src.payment_method_name_bk, src.payment_method_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_cc_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from CC Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_cc_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_cc_type with (tablock) as trg
	using Warehouse.sales.dim_cc_type_wrk src
		on (trg.cc_type_name_bk = src.cc_type_name_bk)
	when matched and not exists 
		(select isnull(trg.cc_type_name, '')
		intersect
		select isnull(src.cc_type_name, ''))
		then 
			update set
				trg.cc_type_name = src.cc_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (cc_type_name_bk, cc_type_name, idETLBatchRun_ins)
				values (src.cc_type_name_bk, src.cc_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_shipping_carrier
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Shipping Carrier from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_shipping_carrier
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_shipping_carrier with (tablock) as trg
	using Warehouse.sales.dim_shipping_carrier_wrk src
		on (trg.shipping_carrier_name_bk = src.shipping_carrier_name_bk)
	when matched and not exists 
		(select isnull(trg.shipping_carrier_name, '')
		intersect
		select isnull(src.shipping_carrier_name, ''))
		then 
			update set
				trg.shipping_carrier_name = src.shipping_carrier_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (shipping_carrier_name_bk, shipping_carrier_name, idETLBatchRun_ins)
				values (src.shipping_carrier_name_bk, src.shipping_carrier_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_shipping_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Shipping Method from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_shipping_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_shipping_method with (tablock) as trg
	using Warehouse.sales.dim_shipping_method_wrk src
		on (trg.shipping_description_bk = src.shipping_description_bk)
	when matched and not exists 
		(select isnull(trg.shipping_method_name, ''), isnull(trg.idShippingCarrier_sk_fk, 0), isnull(trg.description, '')
		intersect
		select isnull(src.shipping_method_name, ''), isnull(src.idShippingCarrier_sk_fk, 0), isnull(src.description, ''))
		then 
			update set
				trg.shipping_method_name = src.shipping_method_name, trg.idShippingCarrier_sk_fk = src.idShippingCarrier_sk_fk, 
				trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (shipping_description_bk, shipping_method_name, idShippingCarrier_sk_fk, description, idETLBatchRun_ins)
				values (src.shipping_description_bk, src.shipping_method_name, src.idShippingCarrier_sk_fk, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_telesale
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Telesale from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_telesale
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_telesale with (tablock) as trg
	using Warehouse.sales.dim_telesale_wrk src
		on (trg.telesales_admin_username_bk = src.telesales_admin_username_bk)
	when matched and not exists 
		(select isnull(trg.telesales_username, ''), isnull(trg.user_id, 0), isnull(trg.telesales_first_name, ''), isnull(trg.telesales_last_name, '')
		intersect
		select isnull(src.telesales_username, ''), isnull(src.user_id, 0), isnull(src.telesales_first_name, ''), isnull(src.telesales_last_name, ''))
		then 
			update set
				trg.telesales_username = src.telesales_username, trg.user_id = src.user_id, 
				trg.telesales_first_name = src.telesales_first_name, trg.telesales_last_name = src.telesales_last_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (telesales_admin_username_bk, telesales_username, user_id, telesales_first_name, telesales_last_name, idETLBatchRun_ins)
				values (src.telesales_admin_username_bk, src.telesales_username, src.user_id, src.telesales_last_name, src.telesales_last_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_price_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Price Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_price_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_price_type with (tablock) as trg
	using Warehouse.sales.dim_price_type_wrk src
		on (trg.price_type_name_bk = src.price_type_name_bk)
	when matched and not exists 
		(select isnull(trg.price_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.price_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.price_type_name = src.price_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (price_type_name_bk, price_type_name, description, idETLBatchRun_ins)
				values (src.price_type_name_bk, src.price_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_prescription_method
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Prescription Method from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_prescription_method
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_prescription_method with (tablock) as trg
	using Warehouse.sales.dim_prescription_method_wrk src
		on (trg.prescription_method_name_bk = src.prescription_method_name_bk)
	when matched and not exists 
		(select isnull(trg.prescription_method_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.prescription_method_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.prescription_method_name = src.prescription_method_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (prescription_method_name_bk, prescription_method_name, description, idETLBatchRun_ins)
				values (src.prescription_method_name_bk, src.prescription_method_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure sales.stg_dwh_merge_sales_channel
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	No sup_channel_name anymore
-- ==========================================================================================
-- Description: Merges data from Channel from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_channel
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_channel with (tablock) as trg
	using Warehouse.sales.dim_channel_wrk src
		on (trg.channel_name_bk = src.channel_name_bk)
	when matched and not exists 
		(select isnull(trg.channel_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.channel_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.channel_name = src.channel_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (channel_name_bk, channel_name, description, idETLBatchRun_ins)
				values (src.channel_name_bk, src.channel_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_marketing_channel
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-11-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Marketing Channel from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_marketing_channel
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_marketing_channel with (tablock) as trg
	using Warehouse.sales.dim_marketing_channel_wrk src
		on (trg.marketing_channel_name_bk = src.marketing_channel_name_bk)
	when matched and not exists 
		(select isnull(trg.marketing_channel_name, ''), isnull(trg.sup_marketing_channel_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.marketing_channel_name, ''), isnull(src.sup_marketing_channel_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.marketing_channel_name = src.marketing_channel_name, trg.sup_marketing_channel_name = src.sup_marketing_channel_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel_name, description, idETLBatchRun_ins)
				values (src.marketing_channel_name_bk, src.marketing_channel_name, src.sup_marketing_channel_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_affiliate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Affiliate from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_affiliate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_affiliate with (tablock) as trg
	using Warehouse.sales.dim_affiliate_wrk src
		on (trg.publisher_id_bk = src.publisher_id_bk)
	when matched and not exists 
		(select isnull(trg.affiliate_name, ''), isnull(trg.affiliate_website, ''), isnull(trg.affiliate_network_name, ''), 
			isnull(trg.affiliate_type_name, ''), isnull(trg.affiliate_status_name, '')
		intersect
		select isnull(src.affiliate_name, ''), isnull(src.affiliate_website, ''), isnull(src.affiliate_network_name, '')
			, isnull(src.affiliate_type_name, ''), isnull(src.affiliate_status_name, ''))
		then 
			update set
				trg.affiliate_name = src.affiliate_name, trg.affiliate_website = src.affiliate_website, trg.affiliate_network_name = src.affiliate_network_name, 
				trg.affiliate_type_name = src.affiliate_type_name, trg.affiliate_status_name = src.affiliate_status_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (publisher_id_bk, affiliate_name, affiliate_website, affiliate_network_name, affiliate_type_name, affiliate_status_name, idETLBatchRun_ins)
				values (src.publisher_id_bk, src.affiliate_name, src.affiliate_website, src.affiliate_network_name, src.affiliate_type_name, src.affiliate_status_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_group_coupon_code
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Group Coupon Code from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_group_coupon_code
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_group_coupon_code with (tablock) as trg
	using Warehouse.sales.dim_group_coupon_code_wrk src
		on (trg.channel_bk = src.channel_bk)
	when matched and not exists 
		(select isnull(trg.group_coupon_code_name, '')
		intersect
		select isnull(src.group_coupon_code_name, ''))
		then 
			update set
				trg.group_coupon_code_name = src.group_coupon_code_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (channel_bk, group_coupon_code_name, idETLBatchRun_ins)
				values (src.channel_bk, src.group_coupon_code_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_coupon_code
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Coupon Code from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_coupon_code
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_coupon_code with (tablock) as trg
	using Warehouse.sales.dim_coupon_code_wrk src
		on (trg.coupon_id_bk = src.coupon_id_bk)
	when matched and not exists 
		(select isnull(trg.idGroupCouponCode_sk_fk, 0), isnull(trg.rule_id, 0), isnull(trg.coupon_code, ''), isnull(trg.coupon_code_name, ''), 
			isnull(trg.from_date, ''), isnull(trg.to_date, ''), isnull(trg.expiration_date, ''), 
			isnull(trg.discount_type_name, ''), isnull(trg.discount_amount, ''), isnull(trg.only_new_customer, ''), isnull(trg.refer_a_friend, '')
		intersect
		select isnull(src.idGroupCouponCode_sk_fk, 0), isnull(src.rule_id, 0), isnull(src.coupon_code, ''), isnull(src.coupon_code_name, ''), 
			isnull(src.from_date, ''), isnull(src.to_date, ''), isnull(src.expiration_date, ''), 
			isnull(src.discount_type_name, ''), isnull(src.discount_amount, ''), isnull(src.only_new_customer, ''), isnull(src.refer_a_friend, ''))
		then 
			update set
				trg.idGroupCouponCode_sk_fk = src.idGroupCouponCode_sk_fk, trg.rule_id = src.rule_id, trg.coupon_code = src.coupon_code, trg.coupon_code_name = src.coupon_code_name, 
				trg.from_date = src.from_date, trg.to_date = src.to_date, trg.expiration_date = src.expiration_date, 
				trg.discount_type_name = src.discount_type_name, trg.discount_amount = src.discount_amount, trg.only_new_customer = src.only_new_customer, trg.refer_a_friend = src.refer_a_friend, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (coupon_id_bk, idGroupCouponCode_sk_fk, rule_id, coupon_code, coupon_code_name, 
				from_date, to_date, expiration_date, discount_type_name, discount_amount, only_new_customer, refer_a_friend, idETLBatchRun_ins)
				
				values (src.coupon_id_bk, src.idGroupCouponCode_sk_fk, rule_id, src.coupon_code, src.coupon_code_name, 
					src.from_date, src.to_date, src.expiration_date, src.discount_type_name, src.discount_amount, src.only_new_customer, src.refer_a_friend, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_device_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Device Category from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_device_category
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_device_category with (tablock) as trg
	using Warehouse.sales.dim_device_category_wrk src
		on (trg.device_category_name_bk = src.device_category_name_bk)
	when matched and not exists 
		(select isnull(trg.device_category_name, '')
		intersect
		select isnull(src.device_category_name, ''))
		then 
			update set
				trg.device_category_name = src.device_category_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (device_category_name_bk, device_category_name, idETLBatchRun_ins)
				values (src.device_category_name_bk, src.device_category_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_device_brand
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Device Brand from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_device_brand
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_device_brand with (tablock) as trg
	using Warehouse.sales.dim_device_brand_wrk src
		on (trg.device_brand_name_bk = src.device_brand_name_bk)
	when matched and not exists 
		(select isnull(trg.idDeviceCategory_sk_fk, 0), isnull(trg.device_brand_name, '')
		intersect
		select isnull(src.idDeviceCategory_sk_fk, 0), isnull(src.device_brand_name, ''))
		then 
			update set
				trg.idDeviceCategory_sk_fk = src.idDeviceCategory_sk_fk, trg.device_brand_name = src.device_brand_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (device_brand_name_bk, idDeviceCategory_sk_fk, device_brand_name, idETLBatchRun_ins)
				values (src.device_brand_name_bk, src.idDeviceCategory_sk_fk, src.device_brand_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_device_model
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Device Model from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_device_model
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_device_model with (tablock) as trg
	using Warehouse.sales.dim_device_model_wrk src
		on (trg.device_model_name_bk = src.device_model_name_bk)
	when matched and not exists 
		(select isnull(trg.idDeviceBrand_sk_fk, 0), isnull(trg.device_model_name, '')
		intersect
		select isnull(src.idDeviceBrand_sk_fk, 0), isnull(src.device_model_name, ''))
		then 
			update set
				trg.idDeviceBrand_sk_fk = src.idDeviceBrand_sk_fk, trg.device_model_name = src.device_model_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (device_model_name_bk, idDeviceBrand_sk_fk, device_model_name, idETLBatchRun_ins)
				values (src.device_model_name_bk, src.idDeviceBrand_sk_fk, src.device_model_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_device_browser
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Device Browser from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_device_browser
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_device_browser with (tablock) as trg
	using Warehouse.sales.dim_device_browser_wrk src
		on (trg.device_browser_name_bk = src.device_browser_name_bk)
	when matched and not exists 
		(select isnull(trg.device_browser_name, '')
		intersect
		select isnull(src.device_browser_name, ''))
		then 
			update set
				trg.device_browser_name = src.device_browser_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (device_browser_name_bk, device_browser_name, idETLBatchRun_ins)
				values (src.device_browser_name_bk, src.device_browser_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure sales.stg_dwh_merge_sales_device_os
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Device OS from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_device_os
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_device_os with (tablock) as trg
	using Warehouse.sales.dim_device_os_wrk src
		on (trg.device_os_name_bk = src.device_os_name_bk)
	when matched and not exists 
		(select isnull(trg.device_os_name, '')
		intersect
		select isnull(src.device_os_name, ''))
		then 
			update set
				trg.device_os_name = src.device_os_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (device_os_name_bk, device_os_name, idETLBatchRun_ins)
				values (src.device_os_name_bk, src.device_os_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_rank_customer_order_seq_no
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-09-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Rank Customer Order Seq No from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_rank_customer_order_seq_no
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_rank_customer_order_seq_no with (tablock) as trg
	using Warehouse.sales.dim_rank_customer_order_seq_no_wrk src
		on (trg.customer_order_seq_no_bk = src.customer_order_seq_no_bk)
	when matched and not exists 
		(select isnull(trg.rank_seq_no, '')
		intersect
		select isnull(src.rank_seq_no, ''))
		then 
			update set
				trg.rank_seq_no = src.rank_seq_no, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idCustomer_order_seq_no_sk,
				customer_order_seq_no_bk, rank_seq_no, idETLBatchRun_ins)
				values (src.customer_order_seq_no_bk,
					src.customer_order_seq_no_bk, src.rank_seq_no, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_rank_qty_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Rank Qty Time from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_rank_qty_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_rank_qty_time with (tablock) as trg
	using Warehouse.sales.dim_rank_qty_time_wrk src
		on (trg.qty_time_bk = src.qty_time_bk)
	when matched and not exists 
		(select isnull(trg.rank_qty_time, '')
		intersect
		select isnull(src.rank_qty_time, ''))
		then 
			update set
				trg.rank_qty_time = src.rank_qty_time, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idQty_time_sk,
				qty_time_bk, rank_qty_time, idETLBatchRun_ins)
				values (src.qty_time_bk,
					src.qty_time_bk, src.rank_qty_time, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure sales.stg_dwh_merge_sales_rank_freq_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Rank Freq Time from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_rank_freq_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_rank_freq_time with (tablock) as trg
	using Warehouse.sales.dim_rank_freq_time_wrk src
		on (trg.freq_time_bk = src.freq_time_bk)
	when matched and not exists 
		(select isnull(trg.rank_freq_time, '')
		intersect
		select isnull(src.rank_freq_time, ''))
		then 
			update set
				trg.rank_freq_time = src.rank_freq_time, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idFreq_time_sk,
				freq_time_bk, rank_freq_time, idETLBatchRun_ins)
				values (src.freq_time_bk,
					src.freq_time_bk, src.rank_freq_time, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure sales.stg_dwh_merge_sales_rank_shipping_days
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Rank Shipping Days from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_rank_shipping_days
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_rank_shipping_days with (tablock) as trg
	using Warehouse.sales.dim_rank_shipping_days_wrk src
		on (trg.shipping_days_bk = src.shipping_days_bk)
	when matched and not exists 
		(select isnull(trg.rank_shipping_days, '')
		intersect
		select isnull(src.rank_shipping_days, ''))
		then 
			update set
				trg.rank_shipping_days = src.rank_shipping_days, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idShipping_days_sk,
				shipping_days_bk, rank_shipping_days, idETLBatchRun_ins)
				values (src.shipping_days_bk,
					src.shipping_days_bk, src.rank_shipping_days, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





------------------------------------------------------------------------------------

drop procedure sales.stg_dwh_merge_sales_order_header
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	24-07-2017	Joseba Bayona Sola	Add VAT Logic attributes: countries_registered_code
	--	07-09-2017	Joseba Bayona Sola	Add customer_oder_seq_no_web attribute
	--	23-11-2017	Joseba Bayona Sola	Add idMarketingChannel_sk_fk attribute
	--	04-12-2017	Joseba Bayona Sola	Add proforma attribute
	--	13-03-2018	Joseba Bayona Sola	Add idMarket_sk_fk, idCustomerUnsubscribe_sk_fk attributes
-- ==========================================================================================
-- Description: Merges data from Order Header DIM from Working table into Final DWH Dim Table
	-- idCustomerStatusLifecycle_sk_fk and customer_order_seq_no updated later from Activity
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_header
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_header with (tablock) as trg
	using Warehouse.sales.dim_order_header_wrk src
		on (trg.order_id_bk = src.order_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.invoice_id, 0), isnull(trg.shipment_id, 0), isnull(trg.creditmemo_id, 0), 
			isnull(trg.order_no, ''), isnull(trg.invoice_no, ''), isnull(trg.shipment_no, ''), isnull(trg.creditmemo_no, ''), 
			isnull(trg.idCalendarOrderDate_sk_fk, 0), isnull(trg.idTimeOrderDate_sk_fk, 0), isnull(trg.order_date, ''), 
			isnull(trg.idCalendarInvoiceDate_sk_fk, 0), isnull(trg.invoice_date, ''), 
			isnull(trg.idCalendarShipmentDate_sk_fk, 0), isnull(trg.idTimeShipmentDate_sk_fk, 0), isnull(trg.shipment_date, ''), 
			isnull(trg.idCalendarRefundDate_sk_fk, 0), isnull(trg.refund_date, ''), 
			isnull(trg.idStore_sk_fk, 0), isnull(trg.idMarket_sk_fk, 0), isnull(trg.idCustomer_sk_fk, 0), isnull(trg.idCustomerSCD_sk_fk, 0), 
			isnull(trg.idCountryShipping_sk_fk, 0), isnull(trg.postcode_shipping, ''), isnull(trg.idCountryBilling_sk_fk, 0), isnull(trg.postcode_billing, ''), 
			isnull(trg.idCustomerUnsubscribe_sk_fk, 0), 
			isnull(trg.idOrderStage_sk_fk, 0), isnull(trg.idOrderStatus_sk_fk, 0), isnull(trg.adjustment_order, ''), isnull(trg.idOrderType_sk_fk, 0), isnull(trg.idOrderStatusMagento_sk_fk, 0), 
			isnull(trg.idPaymentMethod_sk_fk, 0), isnull(trg.idCCType_sk_fk, 0), isnull(trg.idShippingMethod_sk_fk, 0), isnull(trg.idTelesale_sk_fk, 0), 
			isnull(trg.idPriceType_sk_fk, 0), isnull(trg.discount_f, ''), isnull(trg.idPrescriptionMethod_sk_fk, 0), 
			isnull(trg.idReminderType_sk_fk, 0), isnull(trg.idReminderPeriod_sk_fk, 0), isnull(trg.reminder_date, ''), isnull(trg.reorder_f, ''), isnull(trg.reorder_date, ''), 
			isnull(trg.idChannel_sk_fk, 0), isnull(trg.idMarketingChannel_sk_fk, 0), isnull(trg.idAffiliate_sk_fk, 0), isnull(trg.idCouponCode_sk_fk, 0), 
			isnull(trg.idDeviceModel_sk_fk, 0), isnull(trg.idDeviceBrowser_sk_fk, 0), isnull(trg.idDeviceOS_sk_fk, 0), 
			--isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), isnull(trg.customer_order_seq_no, 0), isnull(trg.customer_order_seq_no_web, 0), 
			isnull(trg.order_source, ''), isnull(trg.proforma, ''), 
			isnull(trg.idProductTypeOH_sk_fk, 0), isnull(trg.order_qty_time, 0), isnull(trg.num_diff_product_type_oh, 0), isnull(trg.aura_product_p, 0), 
			isnull(trg.countries_registered_code, ''), 
			isnull(trg.total_qty_unit, 0), isnull(trg.total_qty_unit_refunded, 0), isnull(trg.total_qty_pack, 0), isnull(trg.weight, 0), 
			isnull(trg.local_subtotal, 0), isnull(trg.local_shipping, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
			isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), isnull(trg.local_total_vat, 0), isnull(trg.local_total_prof_fee, 0), 
			isnull(trg.local_total_refund, 0), isnull(trg.local_store_credit_given, 0), isnull(trg.local_bank_online_given, 0), 
			isnull(trg.local_subtotal_refund, 0), isnull(trg.local_shipping_refund, 0), isnull(trg.local_discount_refund, 0), isnull(trg.local_store_credit_used_refund, 0), isnull(trg.local_adjustment_refund, 0), 
			isnull(trg.local_total_aft_refund_inc_vat, 0), isnull(trg.local_total_aft_refund_exc_vat, 0), isnull(trg.local_total_aft_refund_vat, 0), isnull(trg.local_total_aft_refund_prof_fee, 0), 
			isnull(trg.local_product_cost, 0), isnull(trg.local_shipping_cost, 0), isnull(trg.local_total_cost, 0), isnull(trg.local_margin, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, ''), 
			isnull(trg.discount_percent, 0), isnull(trg.vat_percent, 0), isnull(trg.prof_fee_percent, 0), 
			isnull(trg.num_lines, 0)

		intersect
		select 
			isnull(src.invoice_id, 0), isnull(src.shipment_id, 0), isnull(src.creditmemo_id, 0), 
			isnull(src.order_no, ''), isnull(src.invoice_no, ''), isnull(src.shipment_no, ''), isnull(src.creditmemo_no, ''), 
			isnull(src.idCalendarOrderDate_sk_fk, 0), isnull(src.idTimeOrderDate_sk_fk, 0), isnull(src.order_date, ''), 
			isnull(src.idCalendarInvoiceDate_sk_fk, 0), isnull(src.invoice_date, ''), 
			isnull(src.idCalendarShipmentDate_sk_fk, 0), isnull(src.idTimeShipmentDate_sk_fk, 0), isnull(src.shipment_date, ''), 
			isnull(src.idCalendarRefundDate_sk_fk, 0), isnull(src.refund_date, ''), 
			isnull(src.idStore_sk_fk, 0), isnull(src.idMarket_sk_fk, 0), isnull(src.idCustomer_sk_fk, 0), isnull(src.idCustomerSCD_sk_fk, 0), 
			isnull(src.idCountryShipping_sk_fk, 0), isnull(src.postcode_shipping, ''), isnull(src.idCountryBilling_sk_fk, 0), isnull(src.postcode_billing, ''), 
			isnull(src.idCustomerUnsubscribe_sk_fk, 0), 
			isnull(src.idOrderStage_sk_fk, 0), isnull(src.idOrderStatus_sk_fk, 0), isnull(src.adjustment_order, ''), isnull(src.idOrderType_sk_fk, 0), isnull(src.idOrderStatusMagento_sk_fk, 0), 
			isnull(src.idPaymentMethod_sk_fk, 0), isnull(src.idCCType_sk_fk, 0), isnull(src.idShippingMethod_sk_fk, 0), isnull(src.idTelesale_sk_fk, 0), 
			isnull(src.idPriceType_sk_fk, 0), isnull(src.discount_f, ''), isnull(src.idPrescriptionMethod_sk_fk, 0), 
			isnull(src.idReminderType_sk_fk, 0), isnull(src.idReminderPeriod_sk_fk, 0), isnull(src.reminder_date, ''), isnull(src.reorder_f, ''), isnull(src.reorder_date, ''), 
			isnull(src.idChannel_sk_fk, 0), isnull(src.idMarketingChannel_sk_fk, 0), isnull(src.idAffiliate_sk_fk, 0), isnull(src.idCouponCode_sk_fk, 0), 
			isnull(src.idDeviceModel_sk_fk, 0), isnull(src.idDeviceBrowser_sk_fk, 0), isnull(src.idDeviceOS_sk_fk, 0), 
			-- isnull(src.idCustomerStatusLifecycle_sk_fk, 0), isnull(src.customer_order_seq_no, 0), isnull(src.customer_order_seq_no_web, 0), 
			isnull(src.order_source, ''), isnull(src.proforma, ''), 
			isnull(src.idProductTypeOH_sk_fk, 0), isnull(src.order_qty_time, 0), isnull(src.num_diff_product_type_oh, 0), isnull(src.aura_product_p, 0), 
			isnull(src.countries_registered_code, ''), 
			isnull(src.total_qty_unit, 0), isnull(src.total_qty_unit_refunded, 0), isnull(src.total_qty_pack, 0), isnull(src.weight, 0), 
			isnull(src.local_subtotal, 0), isnull(src.local_shipping, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
			isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), isnull(src.local_total_vat, 0), isnull(src.local_total_prof_fee, 0), 
			isnull(src.local_total_refund, 0), isnull(src.local_store_credit_given, 0), isnull(src.local_bank_online_given, 0), 
			isnull(src.local_subtotal_refund, 0), isnull(src.local_shipping_refund, 0), isnull(src.local_discount_refund, 0), isnull(src.local_store_credit_used_refund, 0), isnull(src.local_adjustment_refund, 0), 
			isnull(src.local_total_aft_refund_inc_vat, 0), isnull(src.local_total_aft_refund_exc_vat, 0), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, 0), 
			isnull(src.local_product_cost, 0), isnull(src.local_shipping_cost, 0), isnull(src.local_total_cost, 0), isnull(src.local_margin, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ''), 
			isnull(src.discount_percent, 0), isnull(src.vat_percent, 0), isnull(src.prof_fee_percent, 0),
			isnull(src.num_lines, 0))
		then 
			update set
				trg.invoice_id = src.invoice_id, trg.shipment_id = src.shipment_id, trg.creditmemo_id = src.creditmemo_id, 
				trg.order_no = src.order_no, trg.invoice_no = src.invoice_no, trg.shipment_no = src.shipment_no, trg.creditmemo_no = src.creditmemo_no, 
				trg.idCalendarOrderDate_sk_fk = src.idCalendarOrderDate_sk_fk, trg.idTimeOrderDate_sk_fk = src.idTimeOrderDate_sk_fk, trg.order_date = src.order_date, 
				trg.idCalendarInvoiceDate_sk_fk = src.idCalendarInvoiceDate_sk_fk, trg.invoice_date = src.invoice_date, 
				trg.idCalendarShipmentDate_sk_fk = src.idCalendarShipmentDate_sk_fk, trg.idTimeShipmentDate_sk_fk = src.idTimeShipmentDate_sk_fk, trg.shipment_date = src.shipment_date, 
				trg.idCalendarRefundDate_sk_fk = src.idCalendarRefundDate_sk_fk, trg.refund_date = src.refund_date, 
				trg.idStore_sk_fk = src.idStore_sk_fk, trg.idMarket_sk_fk = src.idMarket_sk_fk, trg.idCustomer_sk_fk = src.idCustomer_sk_fk, trg.idCustomerSCD_sk_fk = src.idCustomerSCD_sk_fk, 
				trg.idCountryShipping_sk_fk = src.idCountryShipping_sk_fk, trg.postcode_shipping = src.postcode_shipping, 
				trg.idCountryBilling_sk_fk = src.idCountryBilling_sk_fk, trg.postcode_billing = src.postcode_billing, 
				trg.idCustomerUnsubscribe_sk_fk = src.idCustomerUnsubscribe_sk_fk, 
				trg.idOrderStage_sk_fk = src.idOrderStage_sk_fk, trg.idOrderStatus_sk_fk = src.idOrderStatus_sk_fk, trg.adjustment_order = src.adjustment_order, 
				trg.idOrderType_sk_fk = src.idOrderType_sk_fk, trg.idOrderStatusMagento_sk_fk = src.idOrderStatusMagento_sk_fk, 
				trg.idPaymentMethod_sk_fk = src.idPaymentMethod_sk_fk, trg.idCCType_sk_fk = src.idCCType_sk_fk, trg.idShippingMethod_sk_fk = src.idShippingMethod_sk_fk, 
				trg.idTelesale_sk_fk = src.idTelesale_sk_fk, trg.idPriceType_sk_fk = src.idPriceType_sk_fk, trg.discount_f = src.discount_f, trg.idPrescriptionMethod_sk_fk = src.idPrescriptionMethod_sk_fk, 
				trg.idReminderType_sk_fk = src.idReminderType_sk_fk, trg.idReminderPeriod_sk_fk = src.idReminderPeriod_sk_fk, trg.reminder_date = src.reminder_date, 
				trg.reorder_f = src.reorder_f, trg.reorder_date = src.reorder_date, 
				trg.idChannel_sk_fk = src.idChannel_sk_fk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk_fk, trg.idAffiliate_sk_fk = src.idAffiliate_sk_fk, trg.idCouponCode_sk_fk = src.idCouponCode_sk_fk, 
				trg.idDeviceModel_sk_fk = src.idDeviceModel_sk_fk, trg.idDeviceBrowser_sk_fk = src.idDeviceBrowser_sk_fk, trg.idDeviceOS_sk_fk = src.idDeviceOS_sk_fk, 
				-- trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web, 
				trg.order_source = src.order_source, trg.proforma = src.proforma, 
				trg.idProductTypeOH_sk_fk = src.idProductTypeOH_sk_fk, trg.order_qty_time = src.order_qty_time, trg.num_diff_product_type_oh = src.num_diff_product_type_oh, trg.aura_product_p = src.aura_product_p, 
				trg.countries_registered_code = src.countries_registered_code, 
				trg.total_qty_unit = src.total_qty_unit, trg.total_qty_unit_refunded = src.total_qty_unit_refunded, trg.total_qty_pack = src.total_qty_pack, trg.weight = src.weight, 
				trg.local_subtotal = src.local_subtotal, trg.local_shipping = src.local_shipping, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee, 
				trg.local_total_refund = src.local_total_refund, trg.local_store_credit_given = src.local_store_credit_given, trg.local_bank_online_given = src.local_bank_online_given, 
				trg.local_subtotal_refund = src.local_subtotal_refund, trg.local_shipping_refund = src.local_shipping_refund, trg.local_discount_refund = src.local_discount_refund, 
				trg.local_store_credit_used_refund = src.local_store_credit_used_refund, trg.local_adjustment_refund = src.local_adjustment_refund, 
				trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat, trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, 
				trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
				trg.local_product_cost = src.local_product_cost, trg.local_shipping_cost = src.local_shipping_cost, trg.local_total_cost = src.local_total_cost, trg.local_margin = src.local_margin, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
				trg.discount_percent = src.discount_percent, trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent, 
				trg.num_lines = src.num_lines, 

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_id_bk, invoice_id, shipment_id, creditmemo_id, 
				order_no, invoice_no, shipment_no, creditmemo_no, 
				idCalendarOrderDate_sk_fk, idTimeOrderDate_sk_fk, order_date, 
				idCalendarInvoiceDate_sk_fk, invoice_date, 
				idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
				idCalendarRefundDate_sk_fk, refund_date, 
				idStore_sk_fk, idMarket_sk_fk, idCustomer_sk_fk, idCustomerSCD_sk_fk, 
				idCountryShipping_sk_fk, postcode_shipping, idCountryBilling_sk_fk, postcode_billing,
				idCustomerUnsubscribe_sk_fk,
				idOrderStage_sk_fk, idOrderStatus_sk_fk, adjustment_order, idOrderType_sk_fk, idOrderStatusMagento_sk_fk, 
				idPaymentMethod_sk_fk, idCCType_sk_fk, idShippingMethod_sk_fk, idTelesale_sk_fk, idPriceType_sk_fk, discount_f, idPrescriptionMethod_sk_fk,
				idReminderType_sk_fk, idReminderPeriod_sk_fk, reminder_date, reorder_f, reorder_date, 
				idChannel_sk_fk, idMarketingChannel_sk_fk, idAffiliate_sk_fk, idCouponCode_sk_fk, idDeviceModel_sk_fk, idDeviceBrowser_sk_fk, idDeviceOS_sk_fk,
				idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, order_source, proforma, 
				idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh, aura_product_p,
				countries_registered_code,
				total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
				local_subtotal, local_shipping, local_discount, local_store_credit_used, 
				local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
				local_total_refund, local_store_credit_given, local_bank_online_given, 
				local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
				local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
				local_to_global_rate, order_currency_code, 
				discount_percent, vat_percent, prof_fee_percent, 
				num_lines,
				idETLBatchRun_ins)
				
				values (src.order_id_bk, src.invoice_id, src.shipment_id, src.creditmemo_id, 
					src.order_no, src.invoice_no, src.shipment_no, src.creditmemo_no, 
					src.idCalendarOrderDate_sk_fk, src.idTimeOrderDate_sk_fk, src.order_date, 
					src.idCalendarInvoiceDate_sk_fk, src.invoice_date, 
					src.idCalendarShipmentDate_sk_fk, src.idTimeShipmentDate_sk_fk, src.shipment_date, 
					src.idCalendarRefundDate_sk_fk, src.refund_date, 
					src.idStore_sk_fk, src.idMarket_sk_fk, src.idCustomer_sk_fk, src.idCustomerSCD_sk_fk, 
					src.idCountryShipping_sk_fk, src.postcode_shipping, src.idCountryBilling_sk_fk, src.postcode_billing,
					src.idCustomerUnsubscribe_sk_fk,
					src.idOrderStage_sk_fk, src.idOrderStatus_sk_fk, src.adjustment_order, src.idOrderType_sk_fk, src.idOrderStatusMagento_sk_fk, 
					src.idPaymentMethod_sk_fk, src.idCCType_sk_fk, src.idShippingMethod_sk_fk, src.idTelesale_sk_fk, src.idPriceType_sk_fk, src.discount_f, src.idPrescriptionMethod_sk_fk,
					src.idReminderType_sk_fk, src.idReminderPeriod_sk_fk, src.reminder_date, src.reorder_f, src.reorder_date, 
					src.idChannel_sk_fk, src.idMarketingChannel_sk_fk, src.idAffiliate_sk_fk, src.idCouponCode_sk_fk, src.idDeviceModel_sk_fk, src.idDeviceBrowser_sk_fk, src.idDeviceOS_sk_fk,
					src.idCustomerStatusLifecycle_sk_fk, src.customer_order_seq_no, src.customer_order_seq_no_web, src.order_source, src.proforma, 
					src.idProductTypeOH_sk_fk, src.order_qty_time, src.num_diff_product_type_oh, src.aura_product_p,
					src.countries_registered_code,
					src.total_qty_unit, src.total_qty_unit_refunded, src.total_qty_pack, src.weight, 
					src.local_subtotal, src.local_shipping, src.local_discount, src.local_store_credit_used, 
					src.local_total_inc_vat, src.local_total_exc_vat, src.local_total_vat, src.local_total_prof_fee, 
					src.local_total_refund, src.local_store_credit_given, src.local_bank_online_given, 
					src.local_subtotal_refund, src.local_shipping_refund, src.local_discount_refund, src.local_store_credit_used_refund, src.local_adjustment_refund, 
					src.local_total_aft_refund_inc_vat, src.local_total_aft_refund_exc_vat, src.local_total_aft_refund_vat, src.local_total_aft_refund_prof_fee, 
					src.local_product_cost, src.local_shipping_cost, src.local_total_cost, src.local_margin, 
					src.local_to_global_rate, src.order_currency_code, 
					src.discount_percent, src.vat_percent, src.prof_fee_percent, 
					src.num_lines,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_order_header_product_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	25-08-2017	Joseba Bayona Sola	Replace MERGE statement with DELETE - INSERT statements (Avoid more than 100 in order_percentage per order when product changes product type oh)
-- ==========================================================================================
-- Description: Merges data from Order Header Product Type AUX from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_header_product_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	--merge into Warehouse.sales.dim_order_header_product_type with (tablock) as trg
	--using Warehouse.sales.dim_order_header_product_type_wrk src
	--	on (trg.idOrderHeader_sk_fk = src.idOrderHeader_sk_fk and trg.idProductTypeOH_sk_fk = src.idProductTypeOH_sk_fk)
	--when matched and not exists 
	--	(select isnull(trg.order_percentage, 0), isnull(trg.order_flag, 0), isnull(trg.order_qty_time, 0)
	--	intersect
	--	select isnull(src.order_percentage, 0), isnull(src.order_flag, 0), isnull(src.order_qty_time, 0))
	--	then 
	--		update set
	--			trg.order_percentage = src.order_percentage, trg.order_flag = src.order_flag, trg.order_qty_time = src.order_qty_time, 
	--			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	--when not matched
	--	then 
	--		insert (idOrderHeader_sk_fk, idProductTypeOH_sk_fk, 
	--				order_percentage, order_flag, order_qty_time, 
	--				idETLBatchRun_ins)
	--			values (src.idOrderHeader_sk_fk, src.idProductTypeOH_sk_fk, 
	--				src.order_percentage, src.order_flag, src.order_qty_time, 
	--				@idETLBatchRun)
			
	--OUTPUT $action INTO #MergeResults;			

	--SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	--FROM    
	--		(SELECT mergeAction, 1 rows
	--		 FROM #MergeResults) c 
	--	 PIVOT
	--		(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	


	-- DELETE STATEMENT
	delete from Warehouse.sales.dim_order_header_product_type
	where idOrderHeader_sk_fk in 
		(select distinct idOrderHeader_sk_fk
		from Warehouse.sales.dim_order_header_product_type_wrk)

	set @rowAmountDelete = @@ROWCOUNT

	-- INSERT STATEMENT 
	insert into Warehouse.sales.dim_order_header_product_type(idOrderHeader_sk_fk, idProductTypeOH_sk_fk, 
		order_percentage, order_flag, order_qty_time, 
		idETLBatchRun_ins)

		select idOrderHeader_sk_fk, idProductTypeOH_sk_fk, 
			order_percentage, order_flag, order_qty_time, 
			@idETLBatchRun
		from Warehouse.sales.dim_order_header_product_type_wrk

	set @rowAmountInsert = @@ROWCOUNT

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure sales.stg_dwh_merge_sales_order_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	24-07-2017	Joseba Bayona Sola	Add VAT Logic attributes: countries_registered_code, product_type_vat
-- ==========================================================================================
-- Description: Merges data from Order Line FACT from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.fact_order_line with (tablock) as trg
	using Warehouse.sales.fact_order_line_wrk src
		on (trg.order_line_id_bk = src.order_line_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.order_id, 0), isnull(trg.invoice_id, 0), isnull(trg.shipment_id, 0), isnull(trg.creditmemo_id, 0), 
			isnull(trg.order_no, ''), isnull(trg.invoice_no, ''), isnull(trg.shipment_no, ''), isnull(trg.creditmemo_no, ''), 
			isnull(trg.idCalendarInvoiceDate_sk_fk, 0), isnull(trg.invoice_date, ''),  
			isnull(trg.idCalendarShipmentDate_sk_fk, 0), isnull(trg.idTimeShipmentDate_sk_fk, 0), isnull(trg.shipment_date, ''), 
			isnull(trg.idCalendarRefundDate_sk_fk, 0), isnull(trg.refund_date, ''),
			isnull(trg.idWarehouse_sk_fk, 0), isnull(trg.idOrderHeader_sk_fk, 0), isnull(trg.idOrderStatus_line_sk_fk, 0), isnull(trg.idPriceType_sk_fk, 0), isnull(trg.discount_f, ''), 
			isnull(trg.idProductFamily_sk_fk, 0), 
			isnull(trg.idBC_sk_fk, ''), isnull(trg.idDI_sk_fk, ''), isnull(trg.idPO_sk_fk, ''), 
			isnull(trg.idCY_sk_fk, ''), isnull(trg.idAX_sk_fk, ''), 
			isnull(trg.idAD_sk_fk, ''), isnull(trg.idDO_sk_fk, ''), isnull(trg.idCOL_sk_fk, 0), isnull(trg.eye, ''), 
			isnull(trg.sku_magento, ''), isnull(trg.sku_erp, ''), 
			isnull(trg.idGlassVisionType_sk_fk, 0), isnull(trg.idGlassPackageType_sk_fk, 0), 
			isnull(trg.aura_product_f, ''), 
			isnull(trg.countries_registered_code, ''), isnull(trg.product_type_vat, ''), 
			isnull(trg.qty_unit, 0), isnull(trg.qty_unit_refunded, 0), isnull(trg.qty_pack, 0), isnull(trg.qty_time, 0), isnull(trg.weight, 0), 
			isnull(trg.local_price_unit, 0), isnull(trg.local_price_pack, 0), isnull(trg.local_price_pack_discount, 0), 
			isnull(trg.local_subtotal, 0), isnull(trg.local_shipping, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
			isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), isnull(trg.local_total_vat, 0), isnull(trg.local_total_prof_fee, 0), 
			isnull(trg.local_store_credit_given, 0), isnull(trg.local_bank_online_given, 0), 
			isnull(trg.local_subtotal_refund, 0), isnull(trg.local_shipping_refund, 0), isnull(trg.local_discount_refund, 0), isnull(trg.local_store_credit_used_refund, 0), isnull(trg.local_adjustment_refund, 0), 
			isnull(trg.local_total_aft_refund_inc_vat, 0), isnull(trg.local_total_aft_refund_exc_vat, 0), isnull(trg.local_total_aft_refund_vat, 0), isnull(trg.local_total_aft_refund_prof_fee, 0), 
			isnull(trg.local_product_cost, 0), isnull(trg.local_shipping_cost, 0), isnull(trg.local_total_cost, 0), isnull(trg.local_margin, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, ''), 
			isnull(trg.discount_percent, 0), isnull(trg.vat_percent, 0), isnull(trg.prof_fee_percent, 0), 
			isnull(trg.vat_rate, 0), isnull(trg.prof_fee_rate, 0),
			isnull(trg.order_allocation_rate, 0), isnull(trg.order_allocation_rate_aft_refund, 0), isnull(trg.order_allocation_rate_refund, 0), 
			isnull(trg.num_erp_allocation_lines, 0)
		intersect
		select 
			isnull(src.order_id, 0), isnull(src.invoice_id, 0), isnull(src.shipment_id, 0), isnull(src.creditmemo_id, 0), 
			isnull(src.order_no, ''), isnull(src.invoice_no, ''), isnull(src.shipment_no, ''), isnull(src.creditmemo_no, ''), 
			isnull(src.idCalendarInvoiceDate_sk_fk, 0), isnull(src.invoice_date, ''),  
			isnull(src.idCalendarShipmentDate_sk_fk, 0), isnull(src.idTimeShipmentDate_sk_fk, 0), isnull(src.shipment_date, ''), 
			isnull(src.idCalendarRefundDate_sk_fk, 0), isnull(src.refund_date, ''),
			isnull(src.idWarehouse_sk_fk, 0), isnull(src.idOrderHeader_sk_fk, 0), isnull(src.idOrderStatus_line_sk_fk, 0), isnull(src.idPriceType_sk_fk, 0), isnull(src.discount_f, ''), 
			isnull(src.idProductFamily_sk_fk, 0), 
			isnull(src.idBC_sk_fk, ''), isnull(src.idDI_sk_fk, ''), isnull(src.idPO_sk_fk, ''), 
			isnull(src.idCY_sk_fk, ''), isnull(src.idAX_sk_fk, ''), 
			isnull(src.idAD_sk_fk, ''), isnull(src.idDO_sk_fk, ''), isnull(src.idCOL_sk_fk, 0), isnull(src.eye, ''), 
			isnull(src.sku_magento, ''), isnull(src.sku_erp, ''), 
			isnull(src.idGlassVisionType_sk_fk, 0), isnull(src.idGlassPackageType_sk_fk, 0), 
			isnull(src.aura_product_f, ''), 
			isnull(src.countries_registered_code, ''), isnull(src.product_type_vat, ''), 
			isnull(src.qty_unit, 0), isnull(src.qty_unit_refunded, 0), isnull(src.qty_pack, 0), isnull(src.qty_time, 0), isnull(src.weight, 0), 
			isnull(src.local_price_unit, 0), isnull(src.local_price_pack, 0), isnull(src.local_price_pack_discount, 0), 
			isnull(src.local_subtotal, 0), isnull(src.local_shipping, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
			isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), isnull(src.local_total_vat, 0), isnull(src.local_total_prof_fee, 0), 
			isnull(src.local_store_credit_given, 0), isnull(src.local_bank_online_given, 0), 
			isnull(src.local_subtotal_refund, 0), isnull(src.local_shipping_refund, 0), isnull(src.local_discount_refund, 0), isnull(src.local_store_credit_used_refund, 0), isnull(src.local_adjustment_refund, 0), 
			isnull(src.local_total_aft_refund_inc_vat, 0), isnull(src.local_total_aft_refund_exc_vat, 0), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, 0), 
			isnull(src.local_product_cost, 0), isnull(src.local_shipping_cost, 0), isnull(src.local_total_cost, 0), isnull(src.local_margin, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ''), 
			isnull(src.discount_percent, 0), isnull(src.vat_percent, 0), isnull(src.prof_fee_percent, 0), 
			isnull(src.vat_rate, 0), isnull(src.prof_fee_rate, 0), 
			isnull(src.order_allocation_rate, 0), isnull(src.order_allocation_rate_aft_refund, 0), isnull(src.order_allocation_rate_refund, 0), 
			isnull(src.num_erp_allocation_lines, 0))
		then 
			update set
				trg.order_id = src.order_id, trg.invoice_id = src.invoice_id, trg.shipment_id = src.shipment_id, trg.creditmemo_id = src.creditmemo_id, 
				trg.order_no = src.order_no, trg.invoice_no = src.invoice_no, trg.shipment_no = src.shipment_no, trg.creditmemo_no = src.creditmemo_no, 
				trg.idCalendarInvoiceDate_sk_fk = src.idCalendarInvoiceDate_sk_fk, trg.invoice_date = src.invoice_date, 
				trg.idCalendarShipmentDate_sk_fk = src.idCalendarShipmentDate_sk_fk, trg.idTimeShipmentDate_sk_fk = src.idTimeShipmentDate_sk_fk, trg.shipment_date = src.shipment_date, 
				trg.idCalendarRefundDate_sk_fk = src.idCalendarRefundDate_sk_fk, trg.refund_date = src.refund_date, 
				trg.idWarehouse_sk_fk = src.idWarehouse_sk_fk, trg.idOrderHeader_sk_fk = src.idOrderHeader_sk_fk, trg.idOrderStatus_line_sk_fk = src.idOrderStatus_line_sk_fk, 
				trg.idPriceType_sk_fk = src.idPriceType_sk_fk, trg.discount_f = src.discount_f, 
				trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk, 
				trg.idBC_sk_fk = src.idBC_sk_fk, trg.idDI_sk_fk = src.idDI_sk_fk, trg.idPO_sk_fk = src.idPO_sk_fk, 
				trg.idCY_sk_fk = src.idCY_sk_fk, trg.idAX_sk_fk = src.idAX_sk_fk, 
				trg.idAD_sk_fk = src.idAD_sk_fk, trg.idDO_sk_fk = src.idDO_sk_fk, trg.idCOL_sk_fk = src.idCOL_sk_fk, trg.eye = src.eye, 
				trg.sku_magento = src.sku_magento, trg.sku_erp = src.sku_erp, 
				trg.idGlassVisionType_sk_fk = src.idGlassVisionType_sk_fk, trg.idGlassPackageType_sk_fk = src.idGlassPackageType_sk_fk, 
				trg.aura_product_f = src.aura_product_f, 
				trg.countries_registered_code = src.countries_registered_code, trg.product_type_vat = src.product_type_vat, 
				trg.qty_unit = src.qty_unit, trg.qty_unit_refunded = src.qty_unit_refunded, trg.qty_pack = src.qty_pack, trg.qty_time = src.qty_time, trg.weight = src.weight, 
				trg.local_price_unit = src.local_price_unit, trg.local_price_pack = src.local_price_pack, trg.local_price_pack_discount = src.local_price_pack_discount, 
				trg.local_subtotal = src.local_subtotal, trg.local_shipping = src.local_shipping, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee, 
				trg.local_store_credit_given = src.local_store_credit_given, trg.local_bank_online_given = src.local_bank_online_given, 
				trg.local_subtotal_refund = src.local_subtotal_refund, trg.local_shipping_refund = src.local_shipping_refund, trg.local_discount_refund = src.local_discount_refund, 
				trg.local_store_credit_used_refund = src.local_store_credit_used_refund, trg.local_adjustment_refund = src.local_adjustment_refund, 
				trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat, trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, 
				trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
				trg.local_product_cost = src.local_product_cost, trg.local_shipping_cost = src.local_shipping_cost, trg.local_total_cost = src.local_total_cost, trg.local_margin = src.local_margin, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
				trg.discount_percent = src.discount_percent, trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent, 
				trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate, 
				trg.order_allocation_rate = src.order_allocation_rate, trg.order_allocation_rate_aft_refund = src.order_allocation_rate_aft_refund, 
				trg.order_allocation_rate_refund = src.order_allocation_rate_refund, 
				trg.num_erp_allocation_lines = src.num_erp_allocation_lines,

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_line_id_bk, 
				order_id, invoice_id, shipment_id, creditmemo_id, 
				order_no, invoice_no, shipment_no, creditmemo_no, 
				idCalendarInvoiceDate_sk_fk, invoice_date, 
				idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
				idCalendarRefundDate_sk_fk, refund_date, 
				idWarehouse_sk_fk, idOrderHeader_sk_fk, idOrderStatus_line_sk_fk, idPriceType_sk_fk, discount_f,
				idProductFamily_sk_fk,
				idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, eye,
				sku_magento, sku_erp, 
				idGlassVisionType_sk_fk, idGlassPackageType_sk_fk, 
				aura_product_f,
				countries_registered_code, product_type_vat,
				qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
				local_price_unit, local_price_pack, local_price_pack_discount, 
				local_subtotal, local_shipping, local_discount, local_store_credit_used, 
				local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
				local_store_credit_given, local_bank_online_given, 
				local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
				local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
				local_to_global_rate, order_currency_code, 
				discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate, 
				order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund,
				num_erp_allocation_lines,
				idETLBatchRun_ins)

				values (src.order_line_id_bk, 
					src.order_id, src.invoice_id, src.shipment_id, src.creditmemo_id, 
					src.order_no, src.invoice_no, src.shipment_no, src.creditmemo_no, 
					src.idCalendarInvoiceDate_sk_fk, src.invoice_date, 
					src.idCalendarShipmentDate_sk_fk, src.idTimeShipmentDate_sk_fk, src.shipment_date, 
					src.idCalendarRefundDate_sk_fk, src.refund_date, 
					src.idWarehouse_sk_fk, src.idOrderHeader_sk_fk, src.idOrderStatus_line_sk_fk, src.idPriceType_sk_fk, src.discount_f,
					src.idProductFamily_sk_fk,
					src.idBC_sk_fk, src.idDI_sk_fk, src.idPO_sk_fk, src.idCY_sk_fk, src.idAX_sk_fk, src.idAD_sk_fk, src.idDO_sk_fk, src.idCOL_sk_fk, src.eye,
					src.sku_magento, src.sku_erp, 
					src.idGlassVisionType_sk_fk, src.idGlassPackageType_sk_fk, 
					src.aura_product_f,
					src.countries_registered_code, src.product_type_vat,
					src.qty_unit, src.qty_unit_refunded, src.qty_pack, src.qty_time, src.weight, 
					src.local_price_unit, src.local_price_pack, src.local_price_pack_discount, 
					src.local_subtotal, src.local_shipping, src.local_discount, src.local_store_credit_used, 
					src.local_total_inc_vat, src.local_total_exc_vat, src.local_total_vat, src.local_total_prof_fee, 
					src.local_store_credit_given, src.local_bank_online_given, 
					src.local_subtotal_refund, src.local_shipping_refund, src.local_discount_refund, src.local_store_credit_used_refund, src.local_adjustment_refund, 
					src.local_total_aft_refund_inc_vat, src.local_total_aft_refund_exc_vat, src.local_total_aft_refund_vat, src.local_total_aft_refund_prof_fee, 
					src.local_product_cost, src.local_shipping_cost, src.local_total_cost, src.local_margin, 
					src.local_to_global_rate, src.order_currency_code, 
					src.discount_percent, src.vat_percent, src.prof_fee_percent, src.vat_rate, src.prof_fee_rate, 
					src.order_allocation_rate, src.order_allocation_rate_aft_refund, src.order_allocation_rate_refund,
					src.num_erp_allocation_lines,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




--drop procedure sales.stg_dwh_merge_sales_exchange_rate
--go

---- ==========================================================================================
---- Author: Joseba Bayona Sola
---- Date: 29-12-2017
---- Changed: 
--	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
---- ==========================================================================================
---- Description: Merges data for Exchange Rate table from OH values (EUR TO GBP)
---- ==========================================================================================

--create procedure sales.stg_dwh_merge_sales_exchange_rate
-- 	@idETLBatchRun bigint, @idPackageRun bigint
--as
--begin
--	set nocount on

--	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
--	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

--	create table #MergeResults (MergeAction char(6) NOT NULL);

--	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

--	-- MERGE STATEMENT 
--	merge into Warehouse.sales.dim_exchange_rate with (tablock) as trg
--	using  
--		(select exchange_rate_day, local_to_global_rate eur_to_gbp, convert(decimal(12, 4), 1 / local_to_global_rate) gbp_to_eur
--		from
--			(select distinct exchange_rate_day, local_to_global_rate
--			from
--				(select convert(date, order_date) exchange_rate_day, local_to_global_rate, 
--					count(*) over (partition by convert(date, order_date)) num_rep, 
--					rank() over (partition by convert(date, order_date) order by count(*)) ord_rep
--				from Warehouse.sales.dim_order_header_v
--				where website_group = 'VisionDirect'
--					and order_currency_code = 'EUR'
--					and order_status_name = 'OK'
--				group by convert(date, order_date), local_to_global_rate) t
--			where num_rep = ord_rep) t) src
--		on (trg.exchange_rate_day = src.exchange_rate_day)
--	when matched and not exists 
--		(select isnull(trg.eur_to_gbp, 0), isnull(trg.gbp_to_eur, 0)
--		intersect
--		select isnull(src.eur_to_gbp, 0), isnull(src.gbp_to_eur, 0))
--		then 
--			update set
--				trg.eur_to_gbp = src.eur_to_gbp, trg.gbp_to_eur = src.gbp_to_eur, 
--				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
--	when not matched
--		then 
--			insert (exchange_rate_day, eur_to_gbp, gbp_to_eur, idETLBatchRun_ins)
--				values (src.exchange_rate_day, src.eur_to_gbp, src.gbp_to_eur, @idETLBatchRun)
			
--	OUTPUT $action INTO #MergeResults;			

--	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
--	FROM    
--			(SELECT mergeAction, 1 rows
--			 FROM #MergeResults) c 
--		 PIVOT
--			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

--	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
--		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
--		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

--	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
--		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
--		@message = @message

--	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

--end;
--go 



drop procedure sales.stg_dwh_merge_sales_order_line_vat_fix
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for OL rows with VAT Fix needed
-- ==========================================================================================

create procedure sales.stg_dwh_merge_sales_order_line_vat_fix
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.fact_order_line_vat_fix with (tablock) as trg
	using Warehouse.sales.fact_order_line_vat_fix_wrk src
		on (trg.idOrderLine_sk_fk = src.idOrderLine_sk_fk)
	when matched and not exists 
		(select 
			isnull(trg.idOrderHeader_sk_fk, 0), 
			isnull(trg.order_line_id_bk, 0), isnull(trg.order_id, 0), isnull(trg.order_no, ''), isnull(trg.order_date, ''), 
			isnull(trg.countries_registered_code, ''), isnull(trg.order_currency_code, ''), isnull(trg.exchange_rate, 0), 
			isnull(trg.local_total_inc_vat_vf, 0), isnull(trg.local_total_exc_vat_vf, 0), isnull(trg.local_total_vat_vf, 0), isnull(trg.local_total_prof_fee_vf, 0), 
			isnull(trg.local_store_credit_given_vf, 0), isnull(trg.local_bank_online_given_vf, 0), 
			isnull(trg.local_subtotal_refund_vf, 0), isnull(trg.local_shipping_refund_vf, 0), isnull(trg.local_discount_refund_vf, 0), isnull(trg.local_store_credit_used_refund_vf, 0), isnull(trg.local_adjustment_refund_vf, 0)
		intersect
		select 
			isnull(src.idOrderHeader_sk_fk, 0), 
			isnull(src.order_line_id_bk, 0), isnull(src.order_id, 0), isnull(src.order_no, ''), isnull(src.order_date, ''), 
			isnull(src.countries_registered_code, ''), isnull(src.order_currency_code, ''), isnull(src.exchange_rate, 0), 
			isnull(src.local_total_inc_vat_vf, 0), isnull(src.local_total_exc_vat_vf, 0), isnull(src.local_total_vat_vf, 0), isnull(src.local_total_prof_fee_vf, 0), 
			isnull(src.local_store_credit_given_vf, 0), isnull(src.local_bank_online_given_vf, 0), 
			isnull(src.local_subtotal_refund_vf, 0), isnull(src.local_shipping_refund_vf, 0), isnull(src.local_discount_refund_vf, 0), isnull(src.local_store_credit_used_refund_vf, 0), isnull(src.local_adjustment_refund_vf, 0))
		
		then 
			update set
				trg.idOrderHeader_sk_fk = src.idOrderHeader_sk_fk, 
				trg.order_line_id_bk = src.order_line_id_bk, trg.order_id = src.order_id, trg.order_no = src.order_no, trg.order_date = src.order_date, 
				trg.countries_registered_code = src.countries_registered_code, trg.order_currency_code = src.order_currency_code, trg.exchange_rate = src.exchange_rate, 
				trg.local_total_inc_vat_vf = src.local_total_inc_vat_vf, trg.local_total_exc_vat_vf = src.local_total_exc_vat_vf, 
				trg.local_total_vat_vf = src.local_total_vat_vf, trg.local_total_prof_fee_vf = src.local_total_prof_fee_vf, 
				trg.local_store_credit_given_vf = src.local_store_credit_given_vf, trg.local_bank_online_given_vf = src.local_bank_online_given_vf, 
				trg.local_subtotal_refund_vf = src.local_subtotal_refund_vf, trg.local_shipping_refund_vf = src.local_shipping_refund_vf, trg.local_discount_refund_vf = src.local_discount_refund_vf, 
				trg.local_store_credit_used_refund_vf = src.local_store_credit_used_refund_vf, trg.local_adjustment_refund_vf = src.local_adjustment_refund_vf, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (idOrderLine_sk_fk, idOrderHeader_sk_fk, 
				order_line_id_bk, order_id, order_no, order_date, 
				countries_registered_code, order_currency_code, exchange_rate,
				local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
				local_store_credit_given_vf, local_bank_online_given_vf, 
				local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf,
				idETLBatchRun_ins)

			values (src.idOrderLine_sk_fk, src.idOrderHeader_sk_fk, 
				src.order_line_id_bk, src.order_id, src.order_no, src.order_date, 
				src.countries_registered_code, src.order_currency_code, src.exchange_rate,
				src.local_total_inc_vat_vf, src.local_total_exc_vat_vf, src.local_total_vat_vf, src.local_total_prof_fee_vf, 
				src.local_store_credit_given_vf, src.local_bank_online_given_vf, 
				src.local_subtotal_refund_vf, src.local_shipping_refund_vf, src.local_discount_refund_vf, src.local_store_credit_used_refund_vf, src.local_adjustment_refund_vf, 
				@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 