use Warehouse
go

drop view stat.fact_customer_status_v
go 

create view stat.fact_customer_status_v as
	select cs.customer_status_name, fcs.website_register, fcs.website_group_create, crt.cr_time_name, fcs.stat_date, 
		sum(fcs.num_customers) num_customers
	from 
			Warehouse.stat.fact_customer_status fcs
		inner join
			Warehouse.gen.dim_customer_status cs on fcs.idCustomerStatus_sk_fk = cs.idCustomerStatus_sk
		inner join
			Warehouse.act.dim_rank_cr_time_mm crt on fcs.idCR_time_mm_sk_fk = crt.idCR_time_mm_sk
	group by cs.customer_status_name, fcs.website_register, fcs.website_group_create, crt.cr_time_name, fcs.stat_date
go



drop view stat.fact_magento_unsubscribe_v
go 

create view stat.fact_magento_unsubscribe_v as
	select fmu.magento_unsubscribe_f, fmu.website_register, fmu.website_group_create, crt.cr_time_name, fmu.stat_date, 
		sum(fmu.num_customers) num_customers
	from 
			Warehouse.stat.fact_magento_unsubscribe fmu
		inner join
			Warehouse.act.dim_rank_cr_time_mm crt on fmu.idCR_time_mm_sk_fk = crt.idCR_time_mm_sk
	group by fmu.magento_unsubscribe_f, fmu.website_register, fmu.website_group_create, crt.cr_time_name, fmu.stat_date
go




drop view stat.dotmailer_unsubscribe_v
go 

create view stat.dotmailer_unsubscribe_v as
	select fdu.dotmailer_unsubscribe_f, fdu.website_register, fdu.website_group_create, crt.cr_time_name, fdu.stat_date, 
		sum(fdu.num_customers) num_customers
	from 
			Warehouse.stat.fact_dotmailer_unsubscribe fdu
		inner join
			Warehouse.act.dim_rank_cr_time_mm crt on fdu.idCR_time_mm_sk_fk = crt.idCR_time_mm_sk
	group by fdu.dotmailer_unsubscribe_f, fdu.website_register, fdu.website_group_create, crt.cr_time_name, fdu.stat_date
go




drop view stat.fact_sms_unsubscribe_v
go 

create view stat.fact_sms_unsubscribe_v as
	select fsu.sms_unsubscribe_f, fsu.website_register, fsu.website_group_create, crt.cr_time_name, fsu.stat_date, 
		sum(fsu.num_customers) num_customers
	from 
			Warehouse.stat.fact_sms_unsubscribe fsu
		inner join
			Warehouse.act.dim_rank_cr_time_mm crt on fsu.idCR_time_mm_sk_fk = crt.idCR_time_mm_sk
	group by fsu.sms_unsubscribe_f, fsu.website_register, fsu.website_group_create, crt.cr_time_name, fsu.stat_date
go

