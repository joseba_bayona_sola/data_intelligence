
use Warehouse
go

select idCustomerStatus_sk_fk, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	num_customers, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stat.fact_customer_status


select magento_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	num_customers, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stat.fact_magento_unsubscribe


select dotmailer_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	num_customers, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stat.fact_dotmailer_unsubscribe


select sms_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	num_customers, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stat.fact_sms_unsubscribe

