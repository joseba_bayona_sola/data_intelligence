use Warehouse
go

drop table Warehouse.stat.fact_customer_status
go

drop table Warehouse.stat.fact_magento_unsubscribe
go

drop table Warehouse.stat.fact_dotmailer_unsubscribe
go

drop table Warehouse.stat.fact_sms_unsubscribe
go

