
use Warehouse
go

drop view tableau.fact_customer_signature_v
go 

create view tableau.fact_customer_signature_v as
	select f.customer_id, f.customer_email, 
		f.acquired_create, f.website_group_create, f.website_create, f.store_create, f.create_date,
		f.website_register, f.store_register, f.register_date, 
		f.market_name, 
		d.country_code_ship,
		f.customer_status_name, f.status_update_date,
		f.rank_num_tot_orders, f.num_tot_orders, f.subtotal_tot_orders, 
		f.rank_num_tot_cancel, f.num_tot_cancel, f.subtotal_tot_cancel, 
		f.rank_num_tot_refund, f.num_tot_refund, f.subtotal_tot_refund, 
		f.num_tot_lapsed, f.num_tot_reactivate,
		f.num_tot_discount_orders, f.discount_tot_value, f.num_tot_store_credit_orders, f.store_credit_tot_value, 
		f.main_type_oh_name, f.num_diff_product_type_oh,
		-- f.main_order_qty_time, f.avg_order_qty_time, f.stdev_order_qty_time, 
		-- f.avg_order_freq_time, f.stdev_order_freq_time, 
		f.rank_main_order_qty_time, f.main_order_qty_time, f.rank_avg_order_qty_time, f.avg_order_qty_time, f.stdev_order_qty_time, 
		f.rank_avg_order_freq_time, f.avg_order_freq_time, f.stdev_order_freq_time, 
		f.main_channel_name, f.main_marketing_channel_name, f.main_price_type_name
	from 
			Warehouse.act.fact_customer_signature_v f
		inner join
			Warehouse.act.dim_customer_signature_v d on f.idCustomer_sk_fk = d.idCustomer_sk_fk
go


drop view tableau.fact_activity_sales_v
go 

create view tableau.fact_activity_sales_v as
	select acs.activity_id_bk, acs.activity_date_c, acs.activity_type_name, 
		-- acs.acquired, 
		acs.tld, acs.website_group, acs.website, acs.store_name, 
		acs.market_name, 
		acs.customer_id, cs.website_group_create, cs.country_code_ship, 
		cs.rank_num_tot_orders, cs.num_tot_orders, cs.subtotal_tot_orders,
		acs.rank_order_qty_time, acs.order_qty_time, acs.rank_next_order_freq_time, acs.next_order_freq_time,
		-- cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.main_order_qty_time, cs.avg_order_freq_time
		cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.rank_main_order_qty_time, cs.main_order_qty_time, cs.rank_avg_order_freq_time, cs.avg_order_freq_time
	from
			(select order_id_bk activity_id_bk, activity_date_c, customer_status_name activity_type_name, 
				acquired, tld, website_group, website, store_name, 
				market_name, 
				customer_id, 
				rank_order_qty_time, order_qty_time, rank_next_order_freq_time, next_order_freq_time
			from Warehouse.act.fact_activity_sales_v
			where activity_type_name = 'ORDER' and customer_status_name = 'NEW'
			union
			select activity_id_bk, activity_date_c, activity_type_name, 
				acquired, tld, website_group, website, store_name, 
				market_name, 
				customer_id, 
				null rank_order_qty_time, null order_qty_time, null rank_next_order_freq_time, null next_order_freq_time
			from Warehouse.act.fact_activity_other_v
			where activity_type_name in ('REACTIVATE', 'LAPSED')
			--union
			--select activity_id_bk, activity_date_c, activity_type_name, 
			--	acquired, tld, website_group, website, store_name, 
			--	market_name, 
			--	customer_id
			--from Warehouse.act.fact_activity_other_v
			--where activity_type_name in ('REGISTER')
			--	and customer_id not in 
			--		(select distinct customer_id 
			--		from Warehouse.act.fact_activity_sales_v)
			) acs
		inner join
			Warehouse.act.dim_customer_signature_v cs on acs.customer_id = cs.customer_id
go
	
select top 1000 *
from Warehouse.act.dim_customer_signature_v