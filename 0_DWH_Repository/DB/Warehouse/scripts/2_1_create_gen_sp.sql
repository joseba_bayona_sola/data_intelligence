use Warehouse
go 


drop procedure gen.stg_dwh_insert_gen_calendar
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Loads Calendar Data into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_insert_gen_calendar
 	@startDate date, @numberOfYears int
as
begin
	set nocount on

	set datefirst 1;
	set dateformat mdy;
	set language US_ENGLISH;

	DECLARE @cutoffDate date = DATEADD(YEAR, @numberOfYears, @startDate);

	-- CREATE TEMP TABLE
	create table #dim(
		[date]       DATE PRIMARY KEY, 
		[day]        AS DATEPART(DAY,      [date]),
		[month]      AS DATEPART(MONTH,    [date]),
		FirstOfMonth AS CONVERT(DATE, DATEADD(MONTH, DATEDIFF(MONTH, 0, [date]), 0)),
		[MonthName]  AS DATENAME(MONTH,    [date]),
		[week]       AS DATEPART(WEEK,     [date]),
		[ISOweek]    AS DATEPART(ISO_WEEK, [date]),
		[DayOfWeek]  AS DATEPART(WEEKDAY,  [date]),
		[quarter]    AS DATEPART(QUARTER,  [date]),
		[year]       AS DATEPART(YEAR,     [date]),
		FirstOfYear  AS CONVERT(DATE, DATEADD(YEAR,  DATEDIFF(YEAR,  0, [date]), 0)),
		Style112     AS CONVERT(CHAR(8),   [date], 112),
		Style101     AS CONVERT(CHAR(10),  [date], 101),
		Style103     AS CONVERT(CHAR(10),  [date], 103));

	-- INSERT TEMP TABLE
	INSERT #dim([date]) 
		SELECT d
		FROM
			(SELECT d = DATEADD(DAY, rn - 1, @startDate)
			 FROM 
				(SELECT TOP (DATEDIFF(DAY, @startDate, @cutoffDate)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
				FROM 
						sys.all_objects AS s1
					CROSS JOIN	
						sys.all_objects AS s2
				ORDER BY s1.[object_id]) x) y;

	-- INSERT STATEMENT 
	insert into Warehouse.gen.dim_calendar(idCalendar_sk, 
		calendar_date, calendar_date_num, calendar_date_name, 
		calendar_date_week, calendar_date_week_name, is_weekend, 
		week_year_num, week_year_name, week_month_num, week_month_name, 
		month_num, month_name, month_year_name, 
		quarter_num, quarter_name, quarter_year_name, 
		year_num)

		select CONVERT(INT, Style112) idCalendar_sk, 
			[date] calendar_date, CONVERT(TINYINT, [day]) calendar_date_num, Style103 calendar_date_name, 
			CONVERT(TINYINT, [DayOfWeek]) calendar_date_week, CONVERT(VARCHAR(10), DATENAME(WEEKDAY, [date])) calendar_date_week_name, 
			CONVERT(BIT, CASE WHEN [DayOfWeek] IN (6,7) THEN 1 ELSE 0 END) is_weekend, 
			CONVERT(TINYINT, [week]) week_year_num, convert(varchar, [year]) + ' - WK: ' + convert(varchar, [week]) week_year_name, 
			CONVERT(TINYINT, DENSE_RANK() OVER (PARTITION BY [year], [month] ORDER BY [week])) week_month_num, 
			LEFT([MonthName], 3) + ' - WK: ' +  CONVERT(VARCHAR, DENSE_RANK() OVER (PARTITION BY [year], [month] ORDER BY [week])) week_month_name, 
			CONVERT(TINYINT, [month]) month_num, CONVERT(VARCHAR(10), [MonthName]) month_name, convert(varchar, [year]) + ' - ' + LEFT([MonthName], 3) month_year_name, 
			CONVERT(TINYINT, [quarter]) quarter_num, 'QT - ' +  CONVERT(VARCHAR, [quarter]) quarter_name, convert(varchar, [year]) + ' - ' + 'QT - ' +  CONVERT(VARCHAR, [quarter]) quarter_year_name, 
			[year] year_num
		from #dim
		order by idCalendar_sk

end;
go 



drop procedure gen.stg_dwh_merge_gen_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-03-2017
-- Changed: 
	--	09-02-2018	Joseba Bayona Sola	Add hour_name attribute
-- ==========================================================================================
-- Description: Merges data from Time from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_time
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_time with (tablock) as trg
	using Warehouse.gen.dim_time_wrk src
		on (trg.time_name_bk = src.time_name_bk)
	when matched and not exists 
		(select isnull(trg.time_name, ''), isnull(trg.hour_name, ''), 
			isnull(trg.hour, ''), isnull(trg.minute, ''), isnull(trg.day_part, '') 
		intersect
		select isnull(src.time_name, ''), isnull(src.hour_name, ''), 
			isnull(src.hour, ''), isnull(src.minute, ''), isnull(src.day_part, ''))
		then 
			update set
				trg.time_name = src.time_name, trg.hour_name = src.hour_name, 
				trg.hour = src.hour, trg.minute = src.minute, trg.day_part = src.day_part, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (time_name_bk, time_name, hour_name, hour, minute, day_part, idETLBatchRun_ins)
				values (src.time_name_bk, src.time_name, src.hour_name, src.hour, src.minute, src.day_part, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure gen.stg_dwh_merge_gen_company
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Company from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_company
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_company with (tablock) as trg
	using Warehouse.gen.dim_company_wrk src
		on (trg.company_name_bk = src.company_name_bk)
	when matched and not exists 
		(select isnull(trg.company_name, '') 
		intersect
		select isnull(src.company_name, ''))
		then 
			update set
				trg.company_name = src.company_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (company_name_bk, company_name, idETLBatchRun_ins)
				values (src.company_name_bk, src.company_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 






drop procedure gen.stg_dwh_merge_gen_store
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 22-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Store from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_store
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_store with (tablock) as trg
	using Warehouse.gen.dim_store_wrk src
		on (trg.store_id_bk = src.store_id_bk)
	when matched and not exists 
		(select isnull(trg.store_name, ''), 
			isnull(trg.website_type, ''), isnull(trg.website_group, ''), isnull(trg.website, ''), 
			isnull(trg.tld, ''), isnull(trg.code_tld, ''), isnull(trg.acquired, ''), 
			isnull(trg.idCompany_sk_fk, 0) 
		intersect
		select isnull(src.store_name, ''), 
			isnull(src.website_type, ''), isnull(src.website_group, ''), isnull(src.website, ''), 
			isnull(src.tld, ''), isnull(src.code_tld, ''), isnull(src.acquired, ''), 
			isnull(src.idCompany_sk_fk, 0))
		then 
			update set
				trg.store_name = src.store_name, 
				trg.website_type = src.website_type, trg.website_group = src.website_group, trg.website = src.website, 
				trg.tld = src.tld, trg.code_tld = src.code_tld, trg.acquired = src.acquired, 
				trg.idCompany_sk_fk = src.idCompany_sk_fk, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (store_id_bk, store_name, website_type, website_group, website, tld, code_tld, acquired, idCompany_sk_fk, idETLBatchRun_ins)
				values (src.store_id_bk, src.store_name, src.website_type, src.website_group, src.website, src.tld, src.code_tld, src.acquired, src.idCompany_sk_fk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure gen.stg_dwh_merge_gen_country
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Country from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_country
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_country with (tablock) as trg
	using Warehouse.gen.dim_country_wrk src
		on (trg.country_id_bk = src.country_id_bk)
	when matched and not exists 
		(select isnull(trg.country_code, ''), isnull(trg.country_name, ''), 
			isnull(trg.country_zone, ''), isnull(trg.country_type, ''), isnull(trg.country_continent, ''), 
			isnull(trg.country_state, '') 
		intersect
		select isnull(src.country_code, ''), isnull(src.country_name, ''), 
			isnull(src.country_zone, ''), isnull(src.country_type, ''), isnull(src.country_continent, ''), 
			isnull(src.country_state, ''))
		then 
			update set
				trg.country_code = src.country_code, trg.country_name = src.country_name, 
				trg.country_zone = src.country_zone, trg.country_type = src.country_type, trg.country_continent = src.country_continent, 
				trg.country_state = src.country_state, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (country_id_bk, country_code, country_name, country_zone, country_type, country_continent, country_state, idETLBatchRun_ins)
				values (src.country_id_bk, src.country_code, src.country_name, src.country_zone, src.country_type, src.country_continent, src.country_state, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure gen.stg_dwh_merge_gen_region
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Region from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_region
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_region with (tablock) as trg
	using Warehouse.gen.dim_region_wrk src
		on (trg.region_id_bk = src.region_id_bk)
	when matched and not exists 
		(select isnull(trg.region_name, ''), isnull(trg.idCountry_sk_fk, 0)
		intersect
		select isnull(src.region_name, ''), isnull(src.idCountry_sk_fk, 0))
		then 
			update set
				trg.region_name = src.region_name, trg.idCountry_sk_fk = src.idCountry_sk_fk, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (region_id_bk, region_name, idCountry_sk_fk, idETLBatchRun_ins)
				values (src.region_id_bk, src.region_name, src.idCountry_sk_fk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure gen.stg_dwh_merge_gen_market
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-09-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Market from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_market
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_market with (tablock) as trg
	using Warehouse.gen.dim_market_wrk src
		on (trg.market_id_bk = src.market_id_bk)
	when matched and not exists 
		(select isnull(trg.market_name, '')
		intersect
		select isnull(src.market_name, ''))
		then 
			update set
				trg.market_name = src.market_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (market_id_bk, market_name, idETLBatchRun_ins)
				values (src.market_id_bk, src.market_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure gen.stg_dwh_merge_gen_customer_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Customer Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_customer_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_customer_type with (tablock) as trg
	using Warehouse.gen.dim_customer_type_wrk src
		on (trg.customer_type_name_bk = src.customer_type_name_bk)
	when matched and not exists 
		(select isnull(trg.customer_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.customer_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.customer_type_name = src.customer_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (customer_type_name_bk, customer_type_name, description, idETLBatchRun_ins)
				values (src.customer_type_name_bk, src.customer_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure gen.stg_dwh_merge_gen_customer_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Customer Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_customer_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_customer_status with (tablock) as trg
	using Warehouse.gen.dim_customer_status_wrk src
		on (trg.customer_status_name_bk = src.customer_status_name_bk)
	when matched and not exists 
		(select isnull(trg.customer_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.customer_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.customer_status_name = src.customer_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (customer_status_name_bk, customer_status_name, description, idETLBatchRun_ins)
				values (src.customer_status_name_bk, src.customer_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure gen.stg_dwh_merge_gen_customer_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Customer Unsubscribe from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_customer_unsubscribe
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_customer_unsubscribe with (tablock) as trg
	using Warehouse.gen.dim_customer_unsubscribe_wrk src
		on (trg.customer_unsubscribe_name_bk = src.customer_unsubscribe_name_bk)
	when matched and not exists 
		(select isnull(trg.customer_unsubscribe_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.customer_unsubscribe_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.customer_unsubscribe_name = src.customer_unsubscribe_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (customer_unsubscribe_name_bk, customer_unsubscribe_name, description, idETLBatchRun_ins)
				values (src.customer_unsubscribe_name_bk, src.customer_unsubscribe_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure gen.stg_dwh_merge_gen_customer
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	04-09-2017	Joseba Bayona Sola	Add idMarket_sk_fk attribute
-- ==========================================================================================
-- Description: Merges data from Customer from Working table into Final DWH Dim Table
	-- idCustomerStatus_sk_fk: Not included in Merge Filter + Update: Will come as NULL from Source and will be calculated later
	-- updated_at: Not included in Merge Filter: Will make unnecessary updades
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_customer
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_customer with (tablock) as trg
	using Warehouse.gen.dim_customer_wrk src
		on (trg.customer_id_bk = src.customer_id_bk)
	
	when matched and not exists 
		(select isnull(trg.customer_email, ''), isnull(trg.created_at, ''), 
			isnull(trg.idStore_sk_fk, 0), isnull(trg.idMarket_sk_fk, 0), 
			isnull(trg.idCustomerType_sk_fk, 0), 
			isnull(trg.prefix, ''), isnull(trg.first_name, ''), isnull(trg.last_name, ''), 
			isnull(trg.migrate_customer_f, ''), isnull(trg.migrate_customer_store, ''), isnull(trg.migrate_customer_id, 0)
		intersect
		select isnull(src.customer_email, ''), isnull(src.created_at, ''), 
			isnull(src.idStore_sk_fk, 0), isnull(src.idMarket_sk_fk, 0), 
			isnull(src.idCustomerType_sk_fk, 0), 
			isnull(src.prefix, ''), isnull(src.first_name, ''), isnull(src.last_name, ''), 
			isnull(src.migrate_customer_f, ''), isnull(src.migrate_customer_store, ''), isnull(src.migrate_customer_id, 0))
		then 
			update set
				trg.customer_email = src.customer_email, 
				trg.created_at = src.created_at, trg.updated_at = src.updated_at,  
				trg.idStore_sk_fk = src.idStore_sk_fk, trg.idMarket_sk_fk = src.idMarket_sk_fk, 
				trg.idCustomerType_sk_fk = src.idCustomerType_sk_fk, 
				trg.prefix = src.prefix, trg.first_name = src.first_name, trg.last_name = src.last_name, 
				trg.migrate_customer_f = src.migrate_customer_f, trg.migrate_customer_store = src.migrate_customer_store, trg.migrate_customer_id = src.migrate_customer_id, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	
	when not matched
		then 
			insert (customer_id_bk, customer_email,
				created_at, updated_at,
				idStore_sk_fk, idMarket_sk_fk, 
				idCustomerType_sk_fk,
				prefix, first_name, last_name, 
				migrate_customer_f, migrate_customer_store, migrate_customer_id, 
				idETLBatchRun_ins)
				
				values (src.customer_id_bk, src.customer_email,
					src.created_at, src.updated_at,
					src.idStore_sk_fk, src.idMarket_sk_fk, 
					src.idCustomerType_sk_fk,
					src.prefix, src.first_name, src.last_name, 
					src.migrate_customer_f, src.migrate_customer_store, src.migrate_customer_id, 
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure gen.stg_dwh_merge_gen_customer_SCD
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-04-2017
-- Changed: 
	--	04-09-2017	Joseba Bayona Sola	Add idMarket_sk_fk attribute
	--	12-03-2018	Joseba Bayona Sola	Add idCustomerUnsubscribe_sk_fk attribute
-- ==========================================================================================
-- Description: Merges data from Customer SCD from Working table into Final DWH Dim Table
	-- idCustomerStatus_sk_fk: Not included in Merge Filter + Update: Will come as NULL from Source and will be calculated later
	-- updated_at: Not included in Merge Filter: Will make unnecessary updades
-- Table with Trigger: 
	-- The trigger is launched after update, taking deleted records	(updated ones) and inserting them with isCurrent 'N' and idCalendarTo_sk_fk = getutcdate	 
-- ==========================================================================================

create procedure gen.stg_dwh_merge_gen_customer_SCD
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.gen.dim_customer_SCD with (tablock) as trg
	using 
		(select dim.idCustomer_sk, wrk.*
		from 
				Warehouse.gen.dim_customer_wrk wrk
			inner join
				(select idCustomer_sk, customer_id_bk customer_id_bk_dim
				from Warehouse.gen.dim_customer) dim on wrk.customer_id_bk = dim.customer_id_bk_dim) src

		on (trg.customer_id_bk = src.customer_id_bk) AND trg.isCurrent = 'Y'
	
	when matched and not exists 
		(select isnull(trg.customer_email, ''), isnull(trg.created_at, ''), 
			isnull(trg.idStore_sk_fk, 0), isnull(trg.idMarket_sk_fk, 0), 
			isnull(trg.idCustomerType_sk_fk, 0), 
			isnull(trg.prefix, ''), isnull(trg.first_name, ''), isnull(trg.last_name, ''), 
			isnull(trg.dob, ''), isnull(trg.gender, ''), isnull(trg.language, ''), 
			isnull(trg.phone_number, ''), isnull(trg.street, ''), isnull(trg.city, ''), isnull(trg.postcode, ''), isnull(trg.region, ''), isnull(trg.idRegion_sk_fk, 0), isnull(trg.idCountry_sk_fk, 0), 
			isnull(trg.postcode_s, ''), isnull(trg.idCountry_s_sk_fk, 0), 
			isnull(trg.idCustomerUnsubscribe_sk_fk, 0), 
			isnull(trg.unsubscribe_mark_email_magento_f, ''), isnull(trg.unsubscribe_mark_email_magento_d_sk_fk, 0), isnull(trg.unsubscribe_mark_email_dotmailer_f, ''), isnull(trg.unsubscribe_mark_email_dotmailer_d_sk_fk, 0), 
			isnull(trg.unsubscribe_text_message_f, ''), isnull(trg.unsubscribe_text_message_d_sk_fk, 0), 
			isnull(trg.reminder_f, ''), isnull(trg.idReminderType_sk_fk, 0), isnull(trg.idReminderPeriod_sk_fk, 0), 
			isnull(trg.reorder_f, ''), 
			isnull(trg.referafriend_code, ''), isnull(trg.days_worn_info, '')

		intersect
		select isnull(src.customer_email, ''), isnull(src.created_at, ''), 
			isnull(src.idStore_sk_fk, 0), isnull(src.idMarket_sk_fk, 0), 
			isnull(src.idCustomerType_sk_fk, 0), 
			isnull(src.prefix, ''), isnull(src.first_name, ''), isnull(src.last_name, ''), 
			isnull(src.dob, ''), isnull(src.gender, ''), isnull(src.language, ''), 
			isnull(src.phone_number, ''), isnull(src.street, ''), isnull(src.city, ''), isnull(src.postcode, ''), isnull(src.region, ''), isnull(src.idRegion_sk_fk, 0), isnull(src.idCountry_sk_fk, 0), 
			isnull(src.postcode_s, ''), isnull(src.idCountry_s_sk_fk, 0), 
			isnull(src.idCustomerUnsubscribe_sk_fk, 0), 
			isnull(src.unsubscribe_mark_email_magento_f, ''), isnull(src.unsubscribe_mark_email_magento_d_sk_fk, 0), isnull(src.unsubscribe_mark_email_dotmailer_f, ''), isnull(src.unsubscribe_mark_email_dotmailer_d_sk_fk, 0), 
			isnull(src.unsubscribe_text_message_f, ''), isnull(src.unsubscribe_text_message_d_sk_fk, 0), 
			isnull(src.reminder_f, ''), isnull(src.idReminderType_sk_fk, 0), isnull(src.idReminderPeriod_sk_fk, 0), 
			isnull(src.reorder_f, ''), 
			isnull(src.referafriend_code, ''), isnull(src.days_worn_info, ''))
		then 
			update set
				trg.customer_email = src.customer_email, 
				trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
				trg.idStore_sk_fk = src.idStore_sk_fk, trg.idMarket_sk_fk = src.idMarket_sk_fk, 
				trg.idCustomerType_sk_fk = src.idCustomerType_sk_fk, 
				trg.prefix = src.prefix, trg.first_name = src.first_name, trg.last_name = src.last_name, 
				trg.dob = src.dob, trg.gender = src.gender, trg.language = src.language, 
				trg.phone_number = src.phone_number, trg.street = src.street, trg.city = src.city, trg.postcode = src.postcode, trg.region = src.region, 
				trg.idRegion_sk_fk = src.idRegion_sk_fk, trg.idCountry_sk_fk = src.idCountry_sk_fk, 
				trg.postcode_s = src.postcode_s, trg.idCountry_s_sk_fk = src.idCountry_s_sk_fk, 
				trg.idCustomerUnsubscribe_sk_fk = src.idCustomerUnsubscribe_sk_fk, 
				trg.unsubscribe_mark_email_magento_f = src.unsubscribe_mark_email_magento_f, trg.unsubscribe_mark_email_magento_d_sk_fk = src.unsubscribe_mark_email_magento_d_sk_fk, 
				trg.unsubscribe_mark_email_dotmailer_f = src.unsubscribe_mark_email_dotmailer_f, trg.unsubscribe_mark_email_dotmailer_d_sk_fk = src.unsubscribe_mark_email_dotmailer_d_sk_fk, 
				trg.unsubscribe_text_message_f = src.unsubscribe_text_message_f, trg.unsubscribe_text_message_d_sk_fk = src.unsubscribe_text_message_d_sk_fk, 
				trg.reminder_f = src.reminder_f, trg.idReminderType_sk_fk = src.idReminderType_sk_fk, trg.idReminderPeriod_sk_fk = src.idReminderPeriod_sk_fk, 
				trg.reorder_f = src.reorder_f, 
				trg.referafriend_code = src.referafriend_code, trg.days_worn_info = src.days_worn_info, 
				trg.idCalendarFrom_sk_fk = convert(int, (CONVERT(VARCHAR(8), GETUTCDATE(), 112))), trg.idCalendarTo_sk_fk = NULL,
				trg.isCurrent = 'Y',
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	
	when not matched
		then 
			insert (idCustomer_sk_fk,
				customer_id_bk, customer_email,
				created_at, updated_at, 
				idStore_sk_fk, idMarket_sk_fk,
				idCustomerType_sk_fk,
				prefix, first_name, last_name, 
				dob, gender, language, 
				phone_number, street, city, postcode, region, idRegion_sk_fk, idCountry_sk_fk, 
				postcode_s, idCountry_s_sk_fk, 
				idCustomerUnsubscribe_sk_fk,
				unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk, 
				unsubscribe_text_message_f, unsubscribe_text_message_d_sk_fk, 
				reminder_f, idReminderType_sk_fk, idReminderPeriod_sk_fk, 
				reorder_f, 
				referafriend_code, days_worn_info, 
				idCalendarFrom_sk_fk, idCalendarTo_sk_fk, isCurrent, 
				idETLBatchRun_ins)
				
				values (src.idCustomer_sk,
					src.customer_id_bk, src.customer_email,
					src.created_at, src.updated_at,
					src.idStore_sk_fk, src.idMarket_sk_fk, 
					src.idCustomerType_sk_fk,
					src.prefix, src.first_name, src.last_name, 
					src.dob, src.gender, src.language, 
					src.phone_number, src.street, src.city, src.postcode, src.region, src.idRegion_sk_fk, src.idCountry_sk_fk, 
					src.postcode_s, src.idCountry_s_sk_fk, 
					src.idCustomerUnsubscribe_sk_fk,
					src.unsubscribe_mark_email_magento_f, src.unsubscribe_mark_email_magento_d_sk_fk, src.unsubscribe_mark_email_dotmailer_f, src.unsubscribe_mark_email_dotmailer_d_sk_fk, 
					src.unsubscribe_text_message_f, src.unsubscribe_text_message_d_sk_fk, 
					src.reminder_f, src.idReminderType_sk_fk, src.idReminderPeriod_sk_fk, 
					src.reorder_f, 
					src.referafriend_code, src.days_worn_info, 
					convert(int, (CONVERT(VARCHAR(8), GETUTCDATE(), 112))), NULL, 'Y',
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
