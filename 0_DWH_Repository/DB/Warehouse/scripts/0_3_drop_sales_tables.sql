use Warehouse
go 


drop table Warehouse.sales.fact_order_line
go
drop table Warehouse.sales.fact_order_line_wrk
go

drop table Warehouse.sales.dim_order_header_product_type
go
drop table Warehouse.sales.dim_order_header_product_type_wrk
go

drop table Warehouse.sales.dim_order_header
go
drop table Warehouse.sales.dim_order_header_wrk
go

-----------------------------------------------------------



drop table Warehouse.sales.dim_reminder_type
go
drop table Warehouse.sales.dim_reminder_type_wrk
go

drop table Warehouse.sales.dim_reminder_period
go
drop table Warehouse.sales.dim_reminder_period_wrk
go



drop table Warehouse.sales.dim_order_stage
go
drop table Warehouse.sales.dim_order_stage_wrk
go

drop table Warehouse.sales.dim_order_status
go
drop table Warehouse.sales.dim_order_status_wrk
go

drop table Warehouse.sales.dim_order_type
go
drop table Warehouse.sales.dim_order_type_wrk
go

drop table Warehouse.sales.dim_order_status_magento
go
drop table Warehouse.sales.dim_order_status_magento_wrk
go



drop table Warehouse.sales.dim_payment_method
go
drop table Warehouse.sales.dim_payment_method_wrk
go

drop table Warehouse.sales.dim_cc_type
go
drop table Warehouse.sales.dim_cc_type_wrk
go



drop table Warehouse.sales.dim_shipping_method
go
drop table Warehouse.sales.dim_shipping_method_wrk
go

drop table Warehouse.sales.dim_shipping_carrier
go
drop table Warehouse.sales.dim_shipping_carrier_wrk
go



drop table Warehouse.sales.dim_telesale
go
drop table Warehouse.sales.dim_telesale_wrk
go

drop table Warehouse.sales.dim_price_type
go
drop table Warehouse.sales.dim_price_type_wrk
go


drop table Warehouse.sales.dim_prescription_method
go
drop table Warehouse.sales.dim_prescription_method_wrk
go




drop table Warehouse.sales.dim_channel
go
drop table Warehouse.sales.dim_channel_wrk
go

drop table Warehouse.sales.dim_marketing_channel
go
drop table Warehouse.sales.dim_marketing_channel_wrk
go


drop table Warehouse.sales.dim_affiliate
go
drop table Warehouse.sales.dim_affiliate_wrk
go

drop table Warehouse.sales.dim_coupon_code
go
drop table Warehouse.sales.dim_coupon_code_wrk
go

drop table Warehouse.sales.dim_group_coupon_code
go
drop table Warehouse.sales.dim_group_coupon_code_wrk
go



drop table Warehouse.sales.dim_device_model
go
drop table Warehouse.sales.dim_device_model_wrk
go

drop table Warehouse.sales.dim_device_brand
go
drop table Warehouse.sales.dim_device_brand_wrk
go

drop table Warehouse.sales.dim_device_category
go
drop table Warehouse.sales.dim_device_category_wrk
go


drop table Warehouse.sales.dim_device_browser
go
drop table Warehouse.sales.dim_device_browser_wrk
go

drop table Warehouse.sales.dim_device_os
go
drop table Warehouse.sales.dim_device_os_wrk
go



------------------------------------------------------


drop table Warehouse.sales.dim_rank_customer_order_seq_no
go
drop table Warehouse.sales.dim_rank_customer_order_seq_no_wrk
go

drop table Warehouse.sales.dim_rank_qty_time
go
drop table Warehouse.sales.dim_rank_qty_time_wrk
go

drop table Warehouse.sales.dim_rank_freq_time
go
drop table Warehouse.sales.dim_rank_freq_time_wrk
go

drop table Warehouse.sales.dim_rank_shipping_days
go
drop table Warehouse.sales.dim_rank_shipping_days_wrk
go

--------------------------------------------------------

--drop table Warehouse.sales.dim_exchange_rate
--go

drop table Warehouse.sales.fact_order_line_vat_fix
go
drop table Warehouse.sales.fact_order_line_vat_fix_wrk
go
