
use Warehouse
go

	drop trigger act.trg_fact_customer_signature_SCD;
	go

	create trigger act.trg_fact_customer_signature_SCD on act.fact_customer_signature 
	for update
	as
	begin
		set nocount on

		insert act.fact_customer_signature_scd (idCustomer_sk_fk, 
			idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
			idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 

			idCustomerStatusLifecycle_sk_fk,  

			idCalendarFrom_sk_fk, idCalendarTo_sk_fk,
			idETLBatchRun_ins, ins_ts)

			select d.idCustomer_sk_fk, 
				d.idStoreCreate_sk_fk, d.idCalendarCreateDate_sk_fk, 
				d.idStoreRegister_sk_fk, d.idCalendarRegisterDate_sk_fk, 

				d.idCustomerStatusLifecycle_sk_fk, 
				convert(int, (CONVERT(VARCHAR(8), isnull(d.upd_ts, d.ins_ts), 112))), 
				convert(int, (CONVERT(VARCHAR(8), i.upd_ts, 112))),

				i.idETLBatchRun_upd, i.upd_ts
			from
					deleted d
				inner join
					inserted i on d.idCustomer_sk_fk = i.idCustomer_sk_fk
			where (d.idStoreCreate_sk_fk <> i.idStoreCreate_sk_fk) or (d.idCalendarCreateDate_sk_fk <> i.idCalendarCreateDate_sk_fk) or		
				(d.idStoreRegister_sk_fk <> i.idStoreRegister_sk_fk) or (d.idCalendarRegisterDate_sk_fk <> i.idCalendarRegisterDate_sk_fk) or
				(d.idCustomerStatusLifecycle_sk_fk <> i.idCustomerStatusLifecycle_sk_fk);
	end;
	go
