use Warehouse
go 

create table Warehouse.gen.dim_calendar(
	idCalendar_sk				int NOT NULL, 
	calendar_date				date NOT NULL, 
	calendar_date_num			tinyint,
	calendar_date_name			char(10),
	calendar_date_week			tinyint, 
	calendar_date_week_name		varchar(10),
	is_weekend					bit, 
	week_year_num				tinyint,
	week_year_name				varchar(20),
	week_month_num				tinyint,
	week_month_name				varchar(20),
	month_num					tinyint,
	month_name					varchar(10),
	month_year_name				varchar(10),
	quarter_num					tinyint,
	quarter_name				varchar(10),
	quarter_year_name			varchar(20),
	year_num					int);
go

alter table Warehouse.gen.dim_calendar add constraint [PK_gen_dim_calendar]
	primary key clustered (idCalendar_sk);
go

--------------------------------------------------------

create table Warehouse.gen.dim_time(
	idTime_sk					int NOT NULL IDENTITY(1, 1), 
	time_name_bk				varchar(10) NOT NULL,
	time_name					varchar(10) NOT NULL,
	hour_name					varchar(10) NOT NULL,
	hour						char(2) NOT NULL,
	minute						char(2) NOT NULL,
	day_part					varchar(20) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.gen.dim_time add constraint [PK_gen_dim_time]
	primary key clustered (idTime_sk);
go

alter table Warehouse.gen.dim_time add constraint [DF_gen_dim_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_time add constraint [UNIQ_gen_dim_time_time_name_bk]
	unique (time_name_bk);
go


create table Warehouse.gen.dim_time_wrk(
	time_name_bk				varchar(10) NOT NULL,
	time_name					varchar(10) NOT NULL,
	hour_name					varchar(10) NOT NULL,
	hour						char(2) NOT NULL,
	minute						char(2) NOT NULL,
	day_part					varchar(20) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.gen.dim_time_wrk add constraint [DF_gen_dim_time_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------


create table Warehouse.gen.dim_company(
	idCompany_sk		int NOT NULL IDENTITY(1, 1), 
	company_name_bk		varchar(100) NOT NULL, 
	company_name		varchar(100) NULL,
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	idETLBatchRun_upd	bigint, 
	upd_ts				datetime);
go

alter table Warehouse.gen.dim_company add constraint [PK_gen_dim_company]
	primary key clustered (idCompany_sk);
go

alter table Warehouse.gen.dim_company add constraint [DF_gen_dim_company_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_company add constraint [UNIQ_gen_dim_company_company_name_bk]
	unique (company_name_bk);
go

alter table Warehouse.gen.dim_company add constraint [UNIQ_gen_dim_company_company_name]
	unique (company_name);
go


create table Warehouse.gen.dim_company_wrk(
	company_name_bk		varchar(100) NOT NULL, 
	company_name		varchar(100) NULL,
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Warehouse.gen.dim_company_wrk add constraint [DF_gen_dim_company_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.gen.dim_store(
	idStore_sk			int NOT NULL IDENTITY(1, 1), 
	store_id_bk			int NOT NULL, 
	store_name			varchar(100) NULL, 
	website_type		varchar(100) NULL, 
	website_group		varchar(100) NULL, 
	website				varchar(100) NULL, 
	tld					varchar(100) NULL, 
	code_tld			varchar(100) NULL, 
	acquired			char(1) NOT NULL, 
	idCompany_sk_fk		int,
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	idETLBatchRun_upd	bigint, 
	upd_ts				datetime);
go 

alter table Warehouse.gen.dim_store add constraint [PK_gen_dim_store]
	primary key clustered (idStore_sk);
go

alter table Warehouse.gen.dim_store add constraint [DF_gen_dim_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_store add constraint [UNIQ_gen_dim_store_store_id_bk]
	unique (store_id_bk);
go

alter table Warehouse.gen.dim_store add constraint [UNIQ_gen_dim_store_store_name_acquired]
	unique (store_name, acquired);
go

alter table Warehouse.gen.dim_store add constraint [FK_gen_dim_store_dim_company]
	foreign key (idCompany_sk_fk) references Warehouse.gen.dim_company (idCompany_sk);
go


create table Warehouse.gen.dim_store_wrk(
	store_id_bk			int NOT NULL, 
	store_name			varchar(100) NULL, 
	website_type		varchar(100) NULL, 
	website_group		varchar(100) NULL, 
	website				varchar(100) NULL, 
	tld					varchar(100) NULL, 
	code_tld			varchar(100) NULL, 
	acquired			char(1) NOT NULL, 
	idCompany_sk_fk		int,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Warehouse.gen.dim_store_wrk add constraint [DF_gen_dim_store_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_store_wrk add constraint [DF_gen_dim_store_wrk_idCompany_sk_fk] DEFAULT -1 for idCompany_sk_fk;
go

--------------------------------------------------------

create table Warehouse.gen.dim_country(
	idCountry_sk			int NOT NULL IDENTITY(1, 1), 
	country_id_bk			char(2) NOT NULL, 
	country_code			char(2) NOT NULL, 
	country_name			varchar(50) NOT NULL,
	country_zone			varchar(10) NOT NULL,
	country_type			varchar(10) NOT NULL,
	country_continent		varchar(20) NOT NULL,
	country_state			varchar(10) NOT NULL,
	idETLBatchRun_ins		bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	idETLBatchRun_upd		bigint, 
	upd_ts					datetime);
go 

alter table Warehouse.gen.dim_country add constraint [PK_gen_dim_country]
	primary key clustered (idCountry_sk);
go

alter table Warehouse.gen.dim_country add constraint [DF_gen_dim_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_country add constraint [UNIQ_gen_dim_country_country_id_bk]
	unique (country_id_bk);
go


create table Warehouse.gen.dim_country_wrk(
	country_id_bk			char(2) NOT NULL, 
	country_code			char(2) NOT NULL, 
	country_name			varchar(50) NOT NULL,
	country_zone			varchar(10) NOT NULL,
	country_type			varchar(10) NOT NULL,
	country_continent		varchar(20) NOT NULL,
	country_state			varchar(10) NOT NULL,
	idETLBatchRun_ins		bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go 

alter table Warehouse.gen.dim_country_wrk add constraint [DF_gen_dim_country_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.gen.dim_region(
	idRegion_sk			int NOT NULL IDENTITY(1, 1), 
	region_id_bk		int NOT NULL, 
	region_name			varchar(255) NOT NULL, 
	idCountry_sk_fk		int, 
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	idETLBatchRun_upd	bigint, 
	upd_ts				datetime);
go 

alter table Warehouse.gen.dim_region add constraint [PK_gen_dim_region]
	primary key clustered (idRegion_sk);
go

alter table Warehouse.gen.dim_region add constraint [DF_gen_dim_region_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_region add constraint [UNIQ_gen_dim_region_region_id_bk]
	unique (region_id_bk);
go

alter table Warehouse.gen.dim_region add constraint [FK_gen_dim_region_dim_country]
	foreign key (idCountry_sk_fk) references Warehouse.gen.dim_country (idCountry_sk);
go

create table Warehouse.gen.dim_region_wrk(
	region_id_bk		int NOT NULL, 
	region_name			varchar(255) NOT NULL, 
	idCountry_sk_fk		int, 
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Warehouse.gen.dim_region_wrk add constraint [DF_gen_dim_region_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.gen.dim_market(
	idMarket_sk			int NOT NULL IDENTITY(1, 1), 
	market_id_bk		int NOT NULL, 
	market_name			varchar(100) NULL, 
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL, 
	idETLBatchRun_upd	bigint, 
	upd_ts				datetime);
go 

alter table Warehouse.gen.dim_market add constraint [PK_gen_dim_market]
	primary key clustered (idMarket_sk);
go

alter table Warehouse.gen.dim_market add constraint [DF_gen_dim_market_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_market add constraint [UNIQ_gen_dim_market_market_id_bk]
	unique (market_id_bk);
go


create table Warehouse.gen.dim_market_wrk(
	market_id_bk		int NOT NULL, 
	market_name			varchar(100) NULL, 
	idETLBatchRun_ins	bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Warehouse.gen.dim_market_wrk add constraint [DF_gen_dim_market_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.gen.dim_customer_type(
	idCustomerType_sk		int NOT NULL IDENTITY(1, 1), 
	customer_type_name_bk	varchar(50) NOT NULL,
	customer_type_name		varchar(50) NOT NULL, 
	description				varchar(255) NOT NULL, 
	idETLBatchRun_ins		bigint NOT NULL, 
	ins_ts					datetime NOT NULL, 
	idETLBatchRun_upd		bigint, 
	upd_ts					datetime);
go 

alter table Warehouse.gen.dim_customer_type add constraint [PK_gen_dim_customer_type]
	primary key clustered (idCustomerType_sk);
go

alter table Warehouse.gen.dim_customer_type add constraint [DF_gen_dim_customer_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_customer_type add constraint [UNIQ_gen_dim_customer_type_customer_type_name_bk]
	unique (customer_type_name_bk);
go



create table Warehouse.gen.dim_customer_type_wrk(
	customer_type_name_bk	varchar(50) NOT NULL,
	customer_type_name		varchar(50) NOT NULL, 
	description				varchar(255) NOT NULL, 
	idETLBatchRun_ins		bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go 

alter table Warehouse.gen.dim_customer_type_wrk add constraint [DF_gen_dim_customer_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.gen.dim_customer_status(
	idCustomerStatus_sk			int NOT NULL IDENTITY(1, 1), 
	customer_status_name_bk		varchar(50) NOT NULL,
	customer_status_name		varchar(50) NOT NULL, 
	description					varchar(255) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.gen.dim_customer_status add constraint [PK_gen_dim_customer_status]
	primary key clustered (idCustomerStatus_sk);
go

alter table Warehouse.gen.dim_customer_status add constraint [DF_gen_dim_customer_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_customer_status add constraint [UNIQ_gen_dim_customer_status_customer_status_name_bk]
	unique (customer_status_name_bk);
go



create table Warehouse.gen.dim_customer_status_wrk(
	customer_status_name_bk		varchar(50) NOT NULL,
	customer_status_name		varchar(50) NOT NULL, 
	description					varchar(255) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.gen.dim_customer_status_wrk add constraint [DF_gen_dim_customer_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.gen.dim_customer_unsubscribe(
	idCustomerUnsubscribe_sk		int NOT NULL IDENTITY(1, 1), 
	customer_unsubscribe_name_bk	varchar(10) NOT NULL,
	customer_unsubscribe_name		varchar(10) NOT NULL, 
	description						varchar(255) NOT NULL, 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.gen.dim_customer_unsubscribe add constraint [PK_gen_dim_customer_unsubscribe]
	primary key clustered (idCustomerUnsubscribe_sk);
go

alter table Warehouse.gen.dim_customer_unsubscribe add constraint [DF_gen_dim_customer_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_customer_unsubscribe add constraint [UNIQ_gen_dim_customer_unsubscribe_customer_unsubscribe_name_bk]
	unique (customer_unsubscribe_name_bk);
go



create table Warehouse.gen.dim_customer_unsubscribe_wrk(
	customer_unsubscribe_name_bk	varchar(10) NOT NULL,
	customer_unsubscribe_name		varchar(10) NOT NULL, 
	description						varchar(255) NOT NULL, 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.gen.dim_customer_unsubscribe_wrk add constraint [DF_gen_dim_customer_unsubscribe_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.gen.dim_customer(
	idCustomer_sk				int NOT NULL IDENTITY(1, 1), 
	customer_id_bk				int NOT NULL, 
	customer_email				varchar(255),
	created_at					datetime,
	updated_at					datetime,
	idStore_sk_fk				int NOT NULL,
	idMarket_sk_fk				int,  
	idCustomerType_sk_fk		int NOT NULL,
	idCustomerStatus_sk_fk		int, 
	prefix						varchar(255),
	first_name					varchar(255),
	last_name					varchar(255),
	migrate_customer_f			char(1) NOT NULL,
	migrate_customer_store		varchar(255),
	migrate_customer_id			bigint,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.gen.dim_customer add constraint [PK_gen_dim_customer]
	primary key clustered (idCustomer_sk);
go

alter table Warehouse.gen.dim_customer add constraint [DF_gen_dim_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_customer add constraint [UNIQ_gen_dim_customer_customer_id_bk]
	unique (customer_id_bk);
go


alter table Warehouse.gen.dim_customer add constraint [FK_gen_dim_customer_dim_store]
	foreign key (idStore_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go

alter table Warehouse.gen.dim_customer add constraint [FK_gen_dim_customer_dim_market]
	foreign key (idMarket_sk_fk) references Warehouse.gen.dim_market (idMarket_sk);
go

alter table Warehouse.gen.dim_customer add constraint [FK_gen_dim_customer_dim_customer_type]
	foreign key (idCustomerType_sk_fk) references Warehouse.gen.dim_customer_type (idCustomerType_sk);
go

alter table Warehouse.gen.dim_customer add constraint [FK_gen_dim_customer_dim_customer_status]
	foreign key (idCustomerStatus_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go


create table Warehouse.gen.dim_customer_SCD(
	idCustomerSCD_sk							int NOT NULL IDENTITY(1, 1), 
	idCustomer_sk_fk							int NOT NULL, 
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime,
	updated_at									datetime,
	idStore_sk_fk								int NOT NULL,
	idMarket_sk_fk								int, 
	idCustomerType_sk_fk						int NOT NULL,
	idCustomerStatus_sk_fk						int, 
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	idRegion_sk_fk								int, 
	idCountry_sk_fk								int, 
	postcode_s									varchar(255),
	idCountry_s_sk_fk							int, 
	idCustomerUnsubscribe_sk_fk					int, 
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d_sk_fk		int,		
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d_sk_fk	int,		
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d_sk_fk			int,		
	reminder_f									char(1) NOT NULL,
	idReminderType_sk_fk						int NOT NULL,
	idReminderPeriod_sk_fk						int,
	reorder_f									char(1) NOT NULL,
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	idCalendarFrom_sk_fk						int NOT NULL,
	idCalendarTo_sk_fk							int,
	isCurrent									char(1),
	idETLBatchRun_ins							bigint NOT NULL, 
	ins_ts										datetime NOT NULL, 
	idETLBatchRun_upd							bigint, 
	upd_ts										datetime);
go 

alter table Warehouse.gen.dim_customer_SCD add constraint [PK_gen_dim_customer_SCD]
	primary key clustered (idCustomerSCD_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [DF_gen_dim_customer_SCD_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_store]
	foreign key (idStore_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_market]
	foreign key (idMarket_sk_fk) references Warehouse.gen.dim_market (idMarket_sk);
go


alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_customer_type]
	foreign key (idCustomerType_sk_fk) references Warehouse.gen.dim_customer_type (idCustomerType_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_customer_status]
	foreign key (idCustomerStatus_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_region]
	foreign key (idRegion_sk_fk) references Warehouse.gen.dim_region (idRegion_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_country]
	foreign key (idCountry_sk_fk) references Warehouse.gen.dim_country (idCountry_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_country_s]
	foreign key (idCountry_s_sk_fk) references Warehouse.gen.dim_country (idCountry_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_customer_unsubscribe]
	foreign key (idCustomerUnsubscribe_sk_fk) references Warehouse.gen.dim_customer_unsubscribe (idCustomerUnsubscribe_sk);

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_reminder_type]
	foreign key (idReminderType_sk_fk) references Warehouse.sales.dim_reminder_type (idReminderType_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_reminder_period]
	foreign key (idReminderPeriod_sk_fk) references Warehouse.sales.dim_reminder_period (idReminderPeriod_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_calendar_uns_magento]
	foreign key (unsubscribe_mark_email_magento_d_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_calendar_uns_dotmailer]
	foreign key (unsubscribe_mark_email_dotmailer_d_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_calendar_uns_textmessage]
	foreign key (unsubscribe_text_message_d_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_calendar_from]
	foreign key (idCalendarFrom_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.gen.dim_customer_SCD add constraint [FK_gen_dim_customer_SCD_dim_calendar_to]
	foreign key (idCalendarTo_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

create nonclustered index INX_gen_dim_customer_SCD_idCustomer_sk_fk on Warehouse.gen.dim_customer_SCD (idCustomer_sk_fk)


create table Warehouse.gen.dim_customer_wrk(
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime,
	updated_at									datetime,
	idStore_sk_fk								int NOT NULL,
	idMarket_sk_fk								int, 
	idCustomerType_sk_fk						int NOT NULL,
	idCustomerStatus_sk_fk						int, 
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	idRegion_sk_fk								int, 
	idCountry_sk_fk								int, 
	postcode_s									varchar(255),
	idCountry_s_sk_fk							int, 
	idCustomerUnsubscribe_sk_fk					int, 
	unsubscribe_mark_email_magento_f			char(1) NOT NULL, 
	unsubscribe_mark_email_magento_d_sk_fk		int,		
	unsubscribe_mark_email_dotmailer_f			char(1) NOT NULL, 
	unsubscribe_mark_email_dotmailer_d_sk_fk	int,		
	unsubscribe_text_message_f					char(1) NOT NULL, 
	unsubscribe_text_message_d_sk_fk			int,		
	reminder_f									char(1) NOT NULL,
	idReminderType_sk_fk						int,
	idReminderPeriod_sk_fk						int,
	reorder_f									char(1) NOT NULL,
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	migrate_customer_f							char(1) NOT NULL,
	migrate_customer_store						varchar(255),
	migrate_customer_id							bigint,
	idETLBatchRun_ins							bigint NOT NULL, 
	ins_ts										datetime NOT NULL);
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idStore_sk_fk] DEFAULT -1 for idStore_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idCountry_sk_fk] DEFAULT -1 for idCountry_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idCountry_s_sk_fk] DEFAULT -1 for idCountry_s_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_CustomerUnsubscribe_sk_fk] DEFAULT -1 for idCustomerUnsubscribe_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idReminderType_sk_fk] DEFAULT -1 for idReminderType_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idReminderPeriod_sk_fk] DEFAULT -1 for idReminderPeriod_sk_fk;
go 

alter table Warehouse.gen.dim_customer_wrk add constraint [DF_gen_dim_customer_wrk_idMarket_sk_fk] DEFAULT -1 for idMarket_sk_fk;
go 


