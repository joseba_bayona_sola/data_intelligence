use Warehouse
go 


drop procedure dbo.dwh_insert_na_records
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-04-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel DIM
-- ==========================================================================================
-- Description: Loads N/A records (with -1 sk) in needed DIM tables
-- ==========================================================================================

create procedure dbo.dwh_insert_na_records as

begin
	-- Warehouse.gen.dim_company
	set identity_insert Warehouse.gen.dim_company on

	insert into Warehouse.gen.dim_company(idCompany_sk,
		company_name_bk, company_name, 
		idETLBatchRun_ins) 

		select t.idCompany_sk, t.company_name_bk, t.company_name, t.idETLBatchRun_ins
		from 
				(select -1 idCompany_sk, 'N/A' company_name_bk, 'N/A' company_name, 1 idETLBatchRun_ins) t
			left join
				Warehouse.gen.dim_company c on t.idCompany_sk = c.idCompany_sk
		where c.idCompany_sk is null

	set identity_insert Warehouse.gen.dim_company off


	-- Warehouse.gen.dim_store
	set identity_insert Warehouse.gen.dim_store on

	insert into Warehouse.gen.dim_store(idStore_sk,
		store_id_bk, store_name, website_type, website_group, website, tld, code_tld, acquired,
		idCompany_sk_fk,
		idETLBatchRun_ins)

		select t.idStore_sk,
			t.store_id_bk, t.store_name, t.website_type, t.website_group, t.website, t.tld, t.code_tld, t.acquired,
			t.idCompany_sk_fk,
			t.idETLBatchRun_ins
		from
				(select -1 idStore_sk,
					0 store_id_bk, 'N/A' store_name, 'N/A' website_type, 'N/A' website_group, 'N/A' website, 'N/A' tld, 'N/A' code_tld, 'N' acquired,
					-1 idCompany_sk_fk,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.gen.dim_store s on t.idStore_sk = s.idStore_sk
		where s.idStore_sk is null

	set identity_insert Warehouse.gen.dim_store off


	-- Warehouse.gen.dim_country
	set identity_insert Warehouse.gen.dim_country on

	insert into Warehouse.gen.dim_country(idCountry_sk, country_id_bk, 
		country_code, country_name, country_zone, country_type, country_continent, country_state,
		idETLBatchRun_ins)

		select t.idCountry_sk, t.country_id_bk, 
			t.country_code, t.country_name, t.country_zone, t.country_type, t.country_continent, t.country_state,
			t.idETLBatchRun_ins
		from
				(select -1 idCountry_sk, 'XX' country_id_bk, 
					'XX' country_code, 'N/A' country_name, 'N/A' country_zone, 'N/A' country_type, 'N/A' country_continent, 'N/A' country_state,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.gen.dim_country c on t.idCountry_sk = c.idCountry_sk
		where c.idCountry_sk is null

	set identity_insert Warehouse.gen.dim_country off

	-- Warehouse.gen.dim_region
	-- set identity_insert Warehouse.gen.dim_region on
	-- set identity_insert Warehouse.gen.dim_region off



	-- Warehouse.prod.dim_manufacturer
	set identity_insert Warehouse.prod.dim_manufacturer on

	insert into Warehouse.prod.dim_manufacturer(idManufacturer_sk, 
		manufacturer_bk, manufacturer_name,
		idETLBatchRun_ins)

		select t.idManufacturer_sk, 
			t.manufacturer_bk, t.manufacturer_name,
			t.idETLBatchRun_ins
		from
				(select -1 idManufacturer_sk, 
					-1 manufacturer_bk, 'N/A' manufacturer_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.prod.dim_manufacturer m on t.idManufacturer_sk = m.idManufacturer_sk
		where m.idManufacturer_sk is null

	set identity_insert Warehouse.prod.dim_manufacturer off


	-- Warehouse.sales.dim_reminder_type
	set identity_insert Warehouse.sales.dim_reminder_type on

	insert into Warehouse.sales.dim_reminder_type(idReminderType_sk, 
		reminder_type_name_bk, reminder_type_name,
		idETLBatchRun_ins)

		select t.idReminderType_sk, 
			t.reminder_type_name_bk, t.reminder_type_name,
			t.idETLBatchRun_ins
		from
				(select -1 idReminderType_sk, 
					'N/A' reminder_type_name_bk, 'N/A' reminder_type_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_reminder_type rt on t.idReminderType_sk = rt.idReminderType_sk
		where rt.idReminderType_sk is null

	set identity_insert Warehouse.sales.dim_reminder_type off

	-- Warehouse.sales.dim_reminder_period
	set identity_insert Warehouse.sales.dim_reminder_period on

	insert into Warehouse.sales.dim_reminder_period(idReminderPeriod_sk, 
		reminder_period_bk, reminder_period_name,
		idETLBatchRun_ins)

		select t.idReminderPeriod_sk, 
			t.reminder_period_bk, t.reminder_period_name,
			t.idETLBatchRun_ins
		from
				(select -1 idReminderPeriod_sk, 
					-1 reminder_period_bk, 'N/A' reminder_period_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_reminder_period rp on t.idReminderPeriod_sk = rp.idReminderPeriod_sk
		where rp.idReminderPeriod_sk is null

	set identity_insert Warehouse.sales.dim_reminder_period off

	--------------------------------------------------------------

	-- Warehouse.sales.dim_order_type
	set identity_insert Warehouse.sales.dim_order_type on

	insert into Warehouse.sales.dim_order_type(idOrderType_sk, 
		order_type_name_bk, order_type_name, description,
		idETLBatchRun_ins)
		
		select t.idOrderType_sk, 
			t.order_type_name_bk, t.order_type_name, t.description,
			t.idETLBatchRun_ins
		from 
				(select -1 idOrderType_sk, 
					'N/A' order_type_name_bk, 'N/A' order_type_name, 'Non Applicable' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_order_type ot on t.idOrderType_sk = ot.idOrderType_sk
		where ot.idOrderType_sk is null

	set identity_insert Warehouse.sales.dim_order_type off


	-- Warehouse.sales.dim_order_status_magento
	set identity_insert Warehouse.sales.dim_order_status_magento on

	insert into Warehouse.sales.dim_order_status_magento(idOrderStatusMagento_sk, 
		status_bk, order_status_magento_name, description,
		idETLBatchRun_ins)
		
		select t.idOrderStatusMagento_sk, 
			t.status_bk, t.order_status_magento_name, t.description,
			t.idETLBatchRun_ins
		from 
				(select -1 idOrderStatusMagento_sk, 
					'N/A' status_bk, 'N/A' order_status_magento_name, 'Non Applicable' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_order_status_magento osm on t.idOrderStatusMagento_sk = osm.idOrderStatusMagento_sk
		where osm.idOrderStatusMagento_sk is null

	set identity_insert Warehouse.sales.dim_order_status_magento off


	-- Warehouse.sales.dim_payment_method 
	set identity_insert Warehouse.sales.dim_payment_method on

	insert into Warehouse.sales.dim_payment_method(idPaymentMethod_sk, 
		payment_method_name_bk, payment_method_name, description,
		idETLBatchRun_ins)
		
		select t.idPaymentMethod_sk, 
			t.payment_method_name_bk, t.payment_method_name, t.description,
			t.idETLBatchRun_ins
		from 
				(select -1 idPaymentMethod_sk, 
					'N/A' payment_method_name_bk, 'N/A' payment_method_name, 'Non Applicable' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_payment_method pm on t.idPaymentMethod_sk = pm.idPaymentMethod_sk
		where pm.idPaymentMethod_sk is null

	set identity_insert Warehouse.sales.dim_payment_method off


	-- Warehouse.sales.dim_cc_type
	set identity_insert Warehouse.sales.dim_cc_type on

	insert into Warehouse.sales.dim_cc_type(idCCType_sk, 
		cc_type_name_bk, cc_type_name, 
		idETLBatchRun_ins)
		
		select t.idCCType_sk, 
			t.cc_type_name_bk, t.cc_type_name, 
			t.idETLBatchRun_ins
		from 
				(select -1 idCCType_sk, 
					'N/A' cc_type_name_bk, 'N/A' cc_type_name, 
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_cc_type cct on t.idCCType_sk = cct.idCCType_sk
		where cct.idCCType_sk is null

	set identity_insert Warehouse.sales.dim_cc_type off


	-- Warehouse.sales.dim_shipping_carrier
	set identity_insert Warehouse.sales.dim_shipping_carrier on

	insert into Warehouse.sales.dim_shipping_carrier(idShippingCarrier_sk, 
		shipping_carrier_name_bk, shipping_carrier_name,
		idETLBatchRun_ins)
		
		select t.idShippingCarrier_sk, 
			t.shipping_carrier_name_bk, t.shipping_carrier_name,
			t.idETLBatchRun_ins
		from
				(select -1 idShippingCarrier_sk, 
					'N/A' shipping_carrier_name_bk, 'N/A' shipping_carrier_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_shipping_carrier sc on t.idShippingCarrier_sk = sc.idShippingCarrier_sk
		where sc.idShippingCarrier_sk is null

	set identity_insert Warehouse.sales.dim_shipping_carrier off


	-- Warehouse.sales.dim_shipping_method
	set identity_insert Warehouse.sales.dim_shipping_method on

	insert into Warehouse.sales.dim_shipping_method(idShippingMethod_sk, 
		shipping_description_bk, shipping_method_name, idShippingCarrier_sk_fk, description,
		idETLBatchRun_ins)
		
		select t.idShippingMethod_sk, 
			t.shipping_description_bk, t.shipping_method_name, t.idShippingCarrier_sk_fk, t.description,
			t.idETLBatchRun_ins
		from
				(select -1 idShippingMethod_sk, 
					'N/A' shipping_description_bk, 'N/A' shipping_method_name, -1 idShippingCarrier_sk_fk, 'Non Applicable' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_shipping_method sm on t.idShippingMethod_sk = sm.idShippingMethod_sk
		where sm.idShippingMethod_sk is null

	set identity_insert Warehouse.sales.dim_shipping_method off


	-- Warehouse.sales.dim_channel
	set identity_insert Warehouse.sales.dim_channel on

	insert into Warehouse.sales.dim_channel(idChannel_sk, 
		channel_name_bk, channel_name, description,
		idETLBatchRun_ins)

		select t.idChannel_sk, 
			t.channel_name_bk, t.channel_name, t.description,
			t.idETLBatchRun_ins
		from
				(select -1 idChannel_sk, 
					'Unknown' channel_name_bk, 'Unknown' channel_name, 'Not possible to identify a channel' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_channel sm on t.idChannel_sk = sm.idChannel_sk
		where sm.idChannel_sk is null

	set identity_insert Warehouse.sales.dim_channel off


	-- Warehouse.sales.dim_marketing_channel
	set identity_insert Warehouse.sales.dim_marketing_channel on

	insert into Warehouse.sales.dim_marketing_channel(idMarketingChannel_sk, 
		marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel_name, description,
		idETLBatchRun_ins)

		select t.idMarketingChannel_sk, 
			t.marketing_channel_name_bk, t.sup_marketing_channel_name, t.marketing_channel_name, t.description,
			t.idETLBatchRun_ins
		from
				(select -1 idMarketingChannel_sk, 
					'Unknown' marketing_channel_name_bk, 'Unknown' marketing_channel_name, 'Unknown' sup_marketing_channel_name, 'Not possible to identify a channel' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_marketing_channel sm on t.idMarketingChannel_sk = sm.idMarketingChannel_sk
		where sm.idMarketingChannel_sk is null

	set identity_insert Warehouse.sales.dim_marketing_channel off

	-- Warehouse.sales.dim_device_category
	set identity_insert Warehouse.sales.dim_device_category on

	insert into Warehouse.sales.dim_device_category(idDeviceCategory_sk, 
		device_category_name_bk, device_category_name,
		idETLBatchRun_ins)
		
		select t.idDeviceCategory_sk, 
			t.device_category_name_bk, t.device_category_name,
			t.idETLBatchRun_ins
		from
				(select -1 idDeviceCategory_sk, 
					'N/A' device_category_name_bk, 'N/A' device_category_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_device_category sm on t.idDeviceCategory_sk = sm.idDeviceCategory_sk
		where sm.idDeviceCategory_sk is null

	set identity_insert Warehouse.sales.dim_device_category off

	-- Warehouse.sales.dim_device_brand
	set identity_insert Warehouse.sales.dim_device_brand on

	insert into Warehouse.sales.dim_device_brand(idDeviceBrand_sk, 
		device_brand_name_bk, idDeviceCategory_sk_fk, device_brand_name,
		idETLBatchRun_ins)
		
		select t.idDeviceBrand_sk, 
			t.device_brand_name_bk, t.idDeviceCategory_sk_fk, t.device_brand_name,
			t.idETLBatchRun_ins
		from
				(select -1 idDeviceBrand_sk, 
					'N/A' device_brand_name_bk, -1 idDeviceCategory_sk_fk, 'N/A' device_brand_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_device_brand sm on t.idDeviceBrand_sk = sm.idDeviceBrand_sk
		where sm.idDeviceBrand_sk is null

	set identity_insert Warehouse.sales.dim_device_brand off

	-- Warehouse.sales.dim_device_model
	set identity_insert Warehouse.sales.dim_device_model on

	insert into Warehouse.sales.dim_device_model(idDeviceModel_sk, 
		device_model_name_bk, idDeviceBrand_sk_fk, device_model_name,
		idETLBatchRun_ins)
		
		select t.idDeviceModel_sk, 
			t.device_model_name_bk, t.idDeviceBrand_sk_fk, t.device_model_name,
			t.idETLBatchRun_ins
		from
				(select -1 idDeviceModel_sk, 
					'N/A' device_model_name_bk, -1 idDeviceBrand_sk_fk, 'Non Applicable' device_model_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_device_model sm on t.idDeviceModel_sk = sm.idDeviceModel_sk
		where sm.idDeviceModel_sk is null

	set identity_insert Warehouse.sales.dim_device_model off


	-- Warehouse.sales.dim_device_browser
	set identity_insert Warehouse.sales.dim_device_browser on

	insert into Warehouse.sales.dim_device_browser(idDeviceBrowser_sk, 
		device_browser_name_bk, device_browser_name,
		idETLBatchRun_ins)
		
		select t.idDeviceBrowser_sk, 
			t.device_browser_name_bk, t.device_browser_name,
			t.idETLBatchRun_ins
		from
				(select -1 idDeviceBrowser_sk, 
					'N/A' device_browser_name_bk, 'N/A' device_browser_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_device_browser sm on t.idDeviceBrowser_sk = sm.idDeviceBrowser_sk
		where sm.idDeviceBrowser_sk is null

	set identity_insert Warehouse.sales.dim_device_browser off

	-- Warehouse.sales.dim_device_os
	set identity_insert Warehouse.sales.dim_device_os on

	insert into Warehouse.sales.dim_device_os(idDeviceOS_sk, 
		device_os_name_bk, device_os_name,
		idETLBatchRun_ins)
		
		select t.idDeviceOS_sk, 
			t.device_os_name_bk, t.device_os_name,
			t.idETLBatchRun_ins
		from
				(select -1 idDeviceOS_sk, 
					'N/A' device_os_name_bk, 'N/A' device_os_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.sales.dim_device_os sm on t.idDeviceOS_sk = sm.idDeviceOS_sk
		where sm.idDeviceOS_sk is null

	set identity_insert Warehouse.sales.dim_device_os off


	-- Warehouse.sales.dim_market
	set identity_insert Warehouse.gen.dim_market on

	insert into Warehouse.gen.dim_market(idMarket_sk, 
		market_id_bk, market_name,
		idETLBatchRun_ins)
		
		select t.idMarket_sk, 
			t.market_id_bk, t.market_name,
			t.idETLBatchRun_ins
		from
				(select -1 idMarket_sk, 
					'-1' market_id_bk, 'N/A' market_name,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.gen.dim_market sm on t.idMarket_sk = sm.idMarket_sk
		where sm.idMarket_sk is null

	set identity_insert Warehouse.gen.dim_market off



	-- Warehouse.gen.dim_customer_unsubscribe
	set identity_insert Warehouse.gen.dim_customer_unsubscribe on

	insert into Warehouse.gen.dim_customer_unsubscribe(idCustomerUnsubscribe_sk, 
		customer_unsubscribe_name_bk, customer_unsubscribe_name, description,
		idETLBatchRun_ins)
		
		select t.idCustomerUnsubscribe_sk, 
			t.customer_unsubscribe_name_bk, t.customer_unsubscribe_name, t.description,
			t.idETLBatchRun_ins
		from
				(select -1 idCustomerUnsubscribe_sk, 
					'N/A' customer_unsubscribe_name_bk, 'N/A' customer_unsubscribe_name, 'N/A' description,
					1 idETLBatchRun_ins) t
			left join
				Warehouse.gen.dim_customer_unsubscribe sm on t.idCustomerUnsubscribe_sk = sm.idCustomerUnsubscribe_sk
		where sm.idCustomerUnsubscribe_sk is null

	set identity_insert Warehouse.gen.dim_customer_unsubscribe off
end;
go 