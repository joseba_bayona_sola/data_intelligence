use Warehouse
go 

--------------------------------------------------------

create table Warehouse.sales.dim_reminder_type(
	idReminderType_sk			int NOT NULL IDENTITY(1, 1), 
	reminder_type_name_bk		varchar(50) NOT NULL,
	reminder_type_name			varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_reminder_type add constraint [PK_sales_dim_reminder_type]
	primary key clustered (idReminderType_sk);
go

alter table Warehouse.sales.dim_reminder_type add constraint [DF_sales_dim_reminder_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_reminder_type add constraint [UNIQ_sales_dim_reminder_type_reminder_type_name_bk]
	unique (reminder_type_name_bk);
go



create table Warehouse.sales.dim_reminder_type_wrk(
	reminder_type_name_bk		varchar(50) NOT NULL,
	reminder_type_name			varchar(50) NOT NULL,  
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_reminder_type_wrk add constraint [DF_sales_dim_reminder_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_reminder_period(
	idReminderPeriod_sk			int NOT NULL IDENTITY(1, 1), 
	reminder_period_bk			int NOT NULL,
	reminder_period_name		varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_reminder_period add constraint [PK_sales_dim_reminder_period]
	primary key clustered (idReminderPeriod_sk);
go

alter table Warehouse.sales.dim_reminder_period add constraint [DF_sales_dim_reminder_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_reminder_period add constraint [UNIQ_sales_dim_reminder_period_reminder_period_bk]
	unique (reminder_period_bk);
go



create table Warehouse.sales.dim_reminder_period_wrk(
	reminder_period_bk			int NOT NULL,
	reminder_period_name		varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_reminder_period_wrk add constraint [DF_sales_dim_reminder_period_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_order_stage(
	idOrderStage_sk				int NOT NULL IDENTITY(1, 1), 
	order_stage_name_bk			varchar(20) NOT NULL,
	order_stage_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_order_stage add constraint [PK_sales_dim_order_stage]
	primary key clustered (idOrderStage_sk);
go

alter table Warehouse.sales.dim_order_stage add constraint [DF_sales_dim_order_stage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_stage add constraint [UNIQ_sales_dim_order_stage_order_stage_name_bk]
	unique (order_stage_name_bk);
go



create table Warehouse.sales.dim_order_stage_wrk(
	order_stage_name_bk			varchar(20) NOT NULL,
	order_stage_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_stage_wrk add constraint [DF_sales_dim_order_stage_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_order_status(
	idOrderStatus_sk			int NOT NULL IDENTITY(1, 1), 
	order_status_name_bk		varchar(20) NOT NULL,
	order_status_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_order_status add constraint [PK_sales_dim_order_status]
	primary key clustered (idOrderStatus_sk);
go

alter table Warehouse.sales.dim_order_status add constraint [DF_sales_dim_order_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_status add constraint [UNIQ_sales_dim_order_status_order_status_name_bk]
	unique (order_status_name_bk);
go



create table Warehouse.sales.dim_order_status_wrk(
	order_status_name_bk		varchar(20) NOT NULL,
	order_status_name			varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_status_wrk add constraint [DF_sales_dim_order_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_order_type(
	idOrderType_sk				int NOT NULL IDENTITY(1, 1), 
	order_type_name_bk			varchar(20) NOT NULL,
	order_type_name				varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_order_type add constraint [PK_sales_dim_order_type]
	primary key clustered (idOrderType_sk);
go

alter table Warehouse.sales.dim_order_type add constraint [DF_sales_dim_order_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_type add constraint [UNIQ_sales_dim_order_type_order_type_name_bk]
	unique (order_type_name_bk);
go



create table Warehouse.sales.dim_order_type_wrk(
	order_type_name_bk			varchar(20) NOT NULL,
	order_type_name				varchar(20) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_type_wrk add constraint [DF_sales_dim_order_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_order_status_magento(
	idOrderStatusMagento_sk		int NOT NULL IDENTITY(1, 1), 
	status_bk					varchar(32) NOT NULL,
	order_status_magento_name	varchar(32) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_order_status_magento add constraint [PK_sales_dim_order_status_magento]
	primary key clustered (idOrderStatusMagento_sk);
go

alter table Warehouse.sales.dim_order_status_magento add constraint [DF_sales_dim_order_status_magento_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_status_magento add constraint [UNIQ_sales_dim_order_status_magento_order_status_bk]
	unique (status_bk);
go



create table Warehouse.sales.dim_order_status_magento_wrk(
	status_bk					varchar(32) NOT NULL,
	order_status_magento_name	varchar(32) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_status_magento_wrk add constraint [DF_sales_dim_order_status_magento_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_payment_method(
	idPaymentMethod_sk			int NOT NULL IDENTITY(1, 1), 
	payment_method_name_bk		varchar(255) NOT NULL,
	payment_method_name			varchar(255) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_payment_method add constraint [PK_sales_dim_payment_method]
	primary key clustered (idPaymentMethod_sk);
go

alter table Warehouse.sales.dim_payment_method add constraint [DF_sales_dim_payment_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_payment_method add constraint [UNIQ_sales_dim_payment_method_payment_method_name_bk]
	unique (payment_method_name_bk);
go



create table Warehouse.sales.dim_payment_method_wrk(
	payment_method_name_bk		varchar(255) NOT NULL,
	payment_method_name			varchar(255) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_payment_method_wrk add constraint [DF_sales_dim_payment_method_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_cc_type(
	idCCType_sk					int NOT NULL IDENTITY(1, 1), 
	cc_type_name_bk				varchar(50) NOT NULL,
	cc_type_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_cc_type add constraint [PK_sales_dim_cc_type]
	primary key clustered (idCCType_sk);
go

alter table Warehouse.sales.dim_cc_type add constraint [DF_sales_dim_cc_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_cc_type add constraint [UNIQ_sales_dim_cc_type_cc_type_name_bk]
	unique (cc_type_name_bk);
go



create table Warehouse.sales.dim_cc_type_wrk(
	cc_type_name_bk				varchar(50) NOT NULL,
	cc_type_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_cc_type_wrk add constraint [DF_sales_dim_cc_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_shipping_carrier(
	idShippingCarrier_sk		int NOT NULL IDENTITY(1, 1), 
	shipping_carrier_name_bk	varchar(50) NOT NULL,
	shipping_carrier_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_shipping_carrier add constraint [PK_sales_dim_shipping_carrier]
	primary key clustered (idShippingCarrier_sk);
go

alter table Warehouse.sales.dim_shipping_carrier add constraint [DF_sales_dim_shipping_carrier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_shipping_carrier add constraint [UNIQ_sales_dim_shipping_carrier_shipping_carrier_name_bk]
	unique (shipping_carrier_name_bk);
go



create table Warehouse.sales.dim_shipping_carrier_wrk(
	shipping_carrier_name_bk	varchar(50) NOT NULL,
	shipping_carrier_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_shipping_carrier_wrk add constraint [DF_sales_dim_shipping_carrier_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_shipping_method(
	idShippingMethod_sk			int NOT NULL IDENTITY(1, 1), 
	shipping_description_bk		varchar(255) NOT NULL,
	shipping_method_name		varchar(255) NOT NULL,
	idShippingCarrier_sk_fk		int NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_shipping_method add constraint [PK_sales_dim_shipping_method]
	primary key clustered (idShippingMethod_sk);
go

alter table Warehouse.sales.dim_shipping_method add constraint [DF_sales_dim_shipping_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_shipping_method add constraint [UNIQ_sales_dim_shipping_method_shipping_description_bk]
	unique (shipping_description_bk);
go

alter table Warehouse.sales.dim_shipping_method add constraint [FK_sales_dim_shipping_method_dim_shipping_carrier]
	foreign key (idShippingCarrier_sk_fk) references Warehouse.sales.dim_shipping_carrier (idShippingCarrier_sk);
go


create table Warehouse.sales.dim_shipping_method_wrk(
	shipping_description_bk		varchar(255) NOT NULL,
	shipping_method_name		varchar(255) NOT NULL,
	idShippingCarrier_sk_fk		int NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_shipping_method_wrk add constraint [DF_sales_dim_shipping_method_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_telesale(
	idTelesale_sk						int NOT NULL IDENTITY(1, 1), 
	telesales_admin_username_bk			varchar(40) NOT NULL,
	telesales_username					varchar(40) NOT NULL, 
	user_id								int NOT NULL, 
	telesales_first_name				varchar(32), 
	telesales_last_name					varchar(32),
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.sales.dim_telesale add constraint [PK_sales_dim_telesale]
	primary key clustered (idTelesale_sk);
go

alter table Warehouse.sales.dim_telesale add constraint [DF_sales_dim_telesale_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_telesale add constraint [UNIQ_sales_dim_telesale_telesales_admin_username_name_bk]
	unique (telesales_admin_username_bk);
go



create table Warehouse.sales.dim_telesale_wrk(
	telesales_admin_username_bk			varchar(40) NOT NULL,
	telesales_username					varchar(40) NOT NULL, 
	user_id								int NOT NULL, 
	telesales_first_name				varchar(32), 
	telesales_last_name					varchar(32),
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.sales.dim_telesale_wrk add constraint [DF_sales_dim_telesale_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_price_type(
	idPriceType_sk				int NOT NULL IDENTITY(1, 1), 
	price_type_name_bk			varchar(40) NOT NULL,
	price_type_name				varchar(40) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_price_type add constraint [PK_sales_dim_price_type]
	primary key clustered (idPriceType_sk);
go

alter table Warehouse.sales.dim_price_type add constraint [DF_sales_dim_price_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_price_type add constraint [UNIQ_sales_dim_price_type_price_type_name_bk]
	unique (price_type_name_bk);
go



create table Warehouse.sales.dim_price_type_wrk(
	price_type_name_bk			varchar(40) NOT NULL,
	price_type_name				varchar(40) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_price_type_wrk add constraint [DF_sales_dim_price_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.sales.dim_prescription_method(
	idPrescriptionMethod_sk			int NOT NULL IDENTITY(1, 1), 
	prescription_method_name_bk		varchar(50) NOT NULL,
	prescription_method_name		varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.sales.dim_prescription_method add constraint [PK_sales_dim_prescription_method]
	primary key clustered (idPrescriptionMethod_sk);
go

alter table Warehouse.sales.dim_prescription_method add constraint [DF_sales_dim_prescription_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_prescription_method add constraint [UNIQ_sales_dim_prescription_method_prescription_method_name_bk]
	unique (prescription_method_name_bk);
go



create table Warehouse.sales.dim_prescription_method_wrk(
	prescription_method_name_bk		varchar(50) NOT NULL,
	prescription_method_name		varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.sales.dim_prescription_method_wrk add constraint [DF_sales_dim_prescription_method_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 






--------------------------------------------------------

create table Warehouse.sales.dim_channel(
	idChannel_sk				int NOT NULL IDENTITY(1, 1), 
	channel_name_bk				varchar(50) NOT NULL,
	channel_name				varchar(50) NOT NULL,
	description					varchar(255),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_channel add constraint [PK_sales_dim_channel]
	primary key clustered (idChannel_sk);
go

alter table Warehouse.sales.dim_channel add constraint [DF_sales_dim_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_channel add constraint [UNIQ_sales_dim_channel_channel_name_bk]
	unique (channel_name_bk);
go



create table Warehouse.sales.dim_channel_wrk(
	channel_name_bk				varchar(50) NOT NULL,
	channel_name				varchar(50) NOT NULL,
	description					varchar(255),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_channel_wrk add constraint [DF_sales_dim_channel_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_marketing_channel(
	idMarketingChannel_sk		int NOT NULL IDENTITY(1, 1), 
	marketing_channel_name_bk	varchar(50) NOT NULL,
	marketing_channel_name		varchar(50) NOT NULL,
	sup_marketing_channel_name	varchar(50) NOT NULL,
	description					varchar(255),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_marketing_channel add constraint [PK_sales_dim_marketing_channel]
	primary key clustered (idMarketingChannel_sk);
go

alter table Warehouse.sales.dim_marketing_channel add constraint [DF_sales_dim_marketing_channel_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_marketing_channel add constraint [UNIQ_sales_dim_marketing_channel_marketing_channel_name_bk]
	unique (marketing_channel_name_bk);
go



create table Warehouse.sales.dim_marketing_channel_wrk(
	marketing_channel_name_bk	varchar(50) NOT NULL,
	marketing_channel_name		varchar(50) NOT NULL,
	sup_marketing_channel_name	varchar(50) NOT NULL,
	description					varchar(255),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_marketing_channel_wrk add constraint [DF_sales_dim_marketing_channel_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_affiliate(
	idAffiliate_sk				int NOT NULL IDENTITY(1, 1), 
	publisher_id_bk				int NOT NULL, 
	affiliate_name				varchar(255) NOT NULL, 
	affiliate_website			varchar(255), 
	affiliate_network_name		varchar(50), 
	affiliate_type_name			varchar(50), 
	affiliate_status_name		varchar(20),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_affiliate add constraint [PK_sales_dim_affiliate]
	primary key clustered (idAffiliate_sk);
go

alter table Warehouse.sales.dim_affiliate add constraint [DF_sales_dim_affiliate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_affiliate add constraint [UNIQ_sales_dim_affiliate_publisher_id_bk]
	unique (publisher_id_bk);
go



create table Warehouse.sales.dim_affiliate_wrk(
	publisher_id_bk				int NOT NULL, 
	affiliate_name				varchar(255) NOT NULL, 
	affiliate_website			varchar(255), 
	affiliate_network_name		varchar(50), 
	affiliate_type_name			varchar(50), 
	affiliate_status_name		varchar(20),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_affiliate_wrk add constraint [DF_sales_dim_affiliate_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_group_coupon_code(
	idGroupCouponCode_sk		int NOT NULL IDENTITY(1, 1), 
	channel_bk					varchar(255) NOT NULL,
	group_coupon_code_name		varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_group_coupon_code add constraint [PK_sales_dim_group_coupon_code]
	primary key clustered (idGroupCouponCode_sk);
go

alter table Warehouse.sales.dim_group_coupon_code add constraint [DF_sales_dim_group_coupon_code_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_group_coupon_code add constraint [UNIQ_sales_dim_group_coupon_channel_bk]
	unique (channel_bk);
go



create table Warehouse.sales.dim_group_coupon_code_wrk(
	channel_bk					varchar(255) NOT NULL,
	group_coupon_code_name		varchar(255) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_group_coupon_code_wrk add constraint [DF_sales_dim_group_coupon_code_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_coupon_code(
	idCouponCode_sk				int NOT NULL IDENTITY(1, 1), 
	coupon_id_bk				int NOT NULL,
	idGroupCouponCode_sk_fk		int, 
	rule_id						int,
	coupon_code					varchar(255) NOT NULL,
	coupon_code_name			varchar(255),
	from_date					date, 
	to_date						date,
	expiration_date				datetime,
	discount_type_name			varchar(32), 
	discount_amount				decimal(12, 4),
	only_new_customer			char(1),
	refer_a_friend				char(1),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_coupon_code add constraint [PK_sales_dim_coupon_code]
	primary key clustered (idCouponCode_sk);
go

alter table Warehouse.sales.dim_coupon_code add constraint [DF_sales_dim_coupon_code_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_coupon_code add constraint [UNIQ_sales_dim_coupon_code_coupon_id_bk]
	unique (coupon_id_bk);
go

alter table Warehouse.sales.dim_coupon_code add constraint [FK_sales_dim_coupon_code_dim_group_coupon_code]
	foreign key (idGroupCouponCode_sk_fk) references Warehouse.sales.dim_group_coupon_code (idGroupCouponCode_sk);
go

create table Warehouse.sales.dim_coupon_code_wrk(
	coupon_id_bk				int NOT NULL,
	idGroupCouponCode_sk_fk		int, 
	rule_id						int,
	coupon_code					varchar(255) NOT NULL,
	coupon_code_name			varchar(255),
	from_date					date, 
	to_date						date,
	expiration_date				datetime,
	discount_type_name			varchar(32), 
	discount_amount				decimal(12, 4),
	only_new_customer			char(1),
	refer_a_friend				char(1),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_coupon_code_wrk add constraint [DF_sales_dim_coupon_code_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




--------------------------------------------------------

create table Warehouse.sales.dim_device_category(
	idDeviceCategory_sk			int NOT NULL IDENTITY(1, 1), 
	device_category_name_bk		varchar(20) NOT NULL,
	device_category_name		varchar(20),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_device_category add constraint [PK_sales_dim_device_category]
	primary key clustered (idDeviceCategory_sk);
go

alter table Warehouse.sales.dim_device_category add constraint [DF_sales_dim_device_category_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_device_category add constraint [UNIQ_sales_dim_device_category_device_category_name_bk]
	unique (device_category_name_bk);
go



create table Warehouse.sales.dim_device_category_wrk(
	device_category_name_bk		varchar(20) NOT NULL,
	device_category_name		varchar(20),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_device_category_wrk add constraint [DF_sales_dim_device_category_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_device_brand(
	idDeviceBrand_sk			int NOT NULL IDENTITY(1, 1), 
	device_brand_name_bk		varchar(50) NOT NULL,
	idDeviceCategory_sk_fk		int NOT NULL,
	device_brand_name			varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_device_brand add constraint [PK_sales_dim_device_brand]
	primary key clustered (idDeviceBrand_sk);
go

alter table Warehouse.sales.dim_device_brand add constraint [DF_sales_dim_device_brand_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_device_brand add constraint [UNIQ_sales_dim_device_brand_device_brand_name_bk]
	unique (device_brand_name_bk);
go

alter table Warehouse.sales.dim_device_brand add constraint [FK_sales_dim_device_brand_dim_device_category]
	foreign key (idDeviceCategory_sk_fk) references Warehouse.sales.dim_device_category (idDeviceCategory_sk);
go



create table Warehouse.sales.dim_device_brand_wrk(
	device_brand_name_bk		varchar(50) NOT NULL,
	idDeviceCategory_sk_fk		int NOT NULL,
	device_brand_name			varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_device_brand_wrk add constraint [DF_sales_dim_device_brand_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_device_model(
	idDeviceModel_sk			int NOT NULL IDENTITY(1, 1), 
	device_model_name_bk		varchar(255) NOT NULL, 
	idDeviceBrand_sk_fk			int NOT NULL, 
	device_model_name			varchar(255), 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_device_model add constraint [PK_sales_dim_device_model]
	primary key clustered (idDeviceModel_sk);
go

alter table Warehouse.sales.dim_device_model add constraint [DF_sales_dim_device_model_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_device_model add constraint [UNIQ_sales_dim_device_model_device_model_name_bk]
	unique (device_model_name_bk);
go

alter table Warehouse.sales.dim_device_model add constraint [FK_sales_dim_device_model_dim_device_brand]
	foreign key (idDeviceBrand_sk_fk) references Warehouse.sales.dim_device_brand (idDeviceBrand_sk);
go


create table Warehouse.sales.dim_device_model_wrk(
	device_model_name_bk		varchar(255) NOT NULL, 
	idDeviceBrand_sk_fk			int NOT NULL, 
	device_model_name			varchar(255), 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_device_model_wrk add constraint [DF_sales_dim_device_model_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_device_browser(
	idDeviceBrowser_sk			int NOT NULL IDENTITY(1, 1), 
	device_browser_name_bk		varchar(50) NOT NULL, 
	device_browser_name			varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_device_browser add constraint [PK_sales_dim_device_browser]
	primary key clustered (idDeviceBrowser_sk);
go

alter table Warehouse.sales.dim_device_browser add constraint [DF_sales_dim_device_browser_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_device_browser add constraint [UNIQ_sales_dim_device_browser_device_browser_name_bk]
	unique (device_browser_name_bk);
go



create table Warehouse.sales.dim_device_browser_wrk(
	device_browser_name_bk		varchar(50) NOT NULL, 
	device_browser_name			varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_device_browser_wrk add constraint [DF_sales_dim_device_browser_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.sales.dim_device_os(
	idDeviceOS_sk				int NOT NULL IDENTITY(1, 1), 
	device_os_name_bk			varchar(50) NOT NULL, 
	device_os_name				varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_device_os add constraint [PK_sales_dim_device_os]
	primary key clustered (idDeviceOS_sk);
go

alter table Warehouse.sales.dim_device_os add constraint [DF_sales_dim_device_os_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_device_os add constraint [UNIQ_sales_dim_device_os_device_os_name_bk]
	unique (device_os_name_bk);
go



create table Warehouse.sales.dim_device_os_wrk(
	device_os_name_bk			varchar(50) NOT NULL, 
	device_os_name				varchar(50),
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_device_os_wrk add constraint [DF_sales_dim_device_os_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




--------------------------------------------------------

create table Warehouse.sales.dim_rank_customer_order_seq_no(
	idCustomer_order_seq_no_sk	int NOT NULL, 
	customer_order_seq_no_bk	int NOT NULL, 
	rank_seq_no					char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_rank_customer_order_seq_no add constraint [PK_sales_dim_rank_customer_order_seq_no]
	primary key clustered (idCustomer_order_seq_no_sk);
go

alter table Warehouse.sales.dim_rank_customer_order_seq_no add constraint [DF_sales_dim_rank_customer_order_seq_no_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_rank_customer_order_seq_no add constraint [UNIQ_sales_dim_rank_customer_order_seq_no_customer_order_seq_no_bk]
	unique (customer_order_seq_no_bk);
go



create table Warehouse.sales.dim_rank_customer_order_seq_no_wrk(
	customer_order_seq_no_bk	int NOT NULL, 
	rank_seq_no					char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_rank_customer_order_seq_no_wrk add constraint [DF_sales_dim_rank_customer_order_seq_no_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.sales.dim_rank_qty_time(
	idQty_time_sk				int NOT NULL, 
	qty_time_bk					int NOT NULL, 
	rank_qty_time				char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_rank_qty_time add constraint [PK_sales_dim_rank_qty_time]
	primary key clustered (idQty_time_sk);
go

alter table Warehouse.sales.dim_rank_qty_time add constraint [DF_sales_dim_rank_qty_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_rank_qty_time add constraint [UNIQ_sales_dim_rank_qty_time_qty_time_bk]
	unique (qty_time_bk);
go



create table Warehouse.sales.dim_rank_qty_time_wrk(
	qty_time_bk					int NOT NULL, 
	rank_qty_time				char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_rank_qty_time_wrk add constraint [DF_sales_dim_rank_qty_time_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





create table Warehouse.sales.dim_rank_freq_time(
	idFreq_time_sk				int NOT NULL, 
	freq_time_bk					int NOT NULL, 
	rank_freq_time				char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_rank_freq_time add constraint [PK_sales_dim_rank_freq_time]
	primary key clustered (idFreq_time_sk);
go

alter table Warehouse.sales.dim_rank_freq_time add constraint [DF_sales_dim_rank_freq_time_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_rank_freq_time add constraint [UNIQ_sales_dim_rank_freq_time_freq_time_bk]
	unique (freq_time_bk);
go



create table Warehouse.sales.dim_rank_freq_time_wrk(
	freq_time_bk				int NOT NULL, 
	rank_freq_time				char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_rank_freq_time_wrk add constraint [DF_sales_dim_rank_freq_time_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.sales.dim_rank_shipping_days(
	idShipping_days_sk			int NOT NULL, 
	shipping_days_bk			int NOT NULL, 
	rank_shipping_days			char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.sales.dim_rank_shipping_days add constraint [PK_sales_dim_rank_shipping_days]
	primary key clustered (idShipping_days_sk);
go

alter table Warehouse.sales.dim_rank_shipping_days add constraint [DF_sales_dim_rank_shipping_days_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_rank_shipping_days add constraint [UNIQ_sales_dim_rank_shipping_days_shipping_days_bk]
	unique (shipping_days_bk);
go



create table Warehouse.sales.dim_rank_shipping_days_wrk(
	shipping_days_bk			int NOT NULL, 
	rank_shipping_days			char(2) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.sales.dim_rank_shipping_days_wrk add constraint [DF_sales_dim_rank_shipping_days_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.sales.dim_order_header(
	idOrderHeader_sk				int NOT NULL IDENTITY(1, 1), 
	order_id_bk						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarOrderDate_sk_fk		int NOT NULL, 
	idTimeOrderDate_sk_fk			int NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate_sk_fk		int, 
	invoice_date					datetime,
	idCalendarShipmentDate_sk_fk	int, 
	idTimeShipmentDate_sk_fk		int,
	shipment_date					datetime,
	idCalendarRefundDate_sk_fk		int, 
	refund_date						datetime,

	idStore_sk_fk					int NOT NULL,
	idMarket_sk_fk					int,
	idCustomer_sk_fk				int NOT NULL,
	idCustomerSCD_sk_fk				int NOT NULL,
	idCountryShipping_sk_fk			int NOT NULL,
	postcode_shipping				varchar(255), 
	idCountryBilling_sk_fk			int NOT NULL,
	postcode_billing				varchar(255),
	idCustomerUnsubscribe_sk_fk		int,

	idOrderStage_sk_fk				int NOT NULL, 
	idOrderStatus_sk_fk				int NOT NULL, 
	adjustment_order				char(1),
	idOrderType_sk_fk				int NOT NULL, 
	idOrderStatusMagento_sk_fk		int NOT NULL, 
	idPaymentMethod_sk_fk			int NOT NULL, 
	idCCType_sk_fk					int NOT NULL, 
	idShippingMethod_sk_fk			int NOT NULL, 
	idTelesale_sk_fk				int, 
	idPriceType_sk_fk				int NOT NULL, 
	discount_f						char(1),
	idPrescriptionMethod_sk_fk		int NOT NULL, 
	idReminderType_sk_fk			int NOT NULL, 
	idReminderPeriod_sk_fk			int NOT NULL, 
	reminder_date					datetime,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,

	idChannel_sk_fk					int NOT NULL, 
	idMarketingChannel_sk_fk		int NOT NULL, 
	idAffiliate_sk_fk				int, 
	idCouponCode_sk_fk				int, 
	idDeviceModel_sk_fk				int NOT NULL,
	idDeviceBrowser_sk_fk			int NOT NULL,
	idDeviceOS_sk_fk				int NOT NULL,

	idCustomerStatusLifecycle_sk_fk	int,
	customer_order_seq_no			int, 
	customer_order_seq_no_web		int, 
	order_source					char(1),
	proforma						char(1),
	
	idProductTypeOH_sk_fk			int,
	order_qty_time					int,  
	num_diff_product_type_oh		int,
	aura_product_p					decimal(12, 4),

	countries_registered_code		varchar(10),

	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int,

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.sales.dim_order_header add constraint [PK_sales_dim_order_header]
	primary key clustered (idOrderHeader_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [DF_sales_dim_order_header_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_header add constraint [UNIQ_sales_dim_order_header_order_id_bk]
	unique (order_id_bk);
go

create nonclustered index [INX_sales_dim_order_header_order_date] on Warehouse.sales.dim_order_header (order_date asc)

create nonclustered index [INX_sales_dim_order_header_invoice_date] on Warehouse.sales.dim_order_header (invoice_date asc)

create nonclustered index [INX_sales_dim_order_header_shipment_date] on Warehouse.sales.dim_order_header (shipment_date asc)

create nonclustered index [INX_sales_dim_order_header_idStore_sk_fk] on Warehouse.sales.dim_order_header (idStore_sk_fk)

create nonclustered index INX_sales_dim_order_header_idCustomer_sk_fk on Warehouse.sales.dim_order_header (idCustomer_sk_fk)


alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_calendar_order_date]
	foreign key (idCalendarOrderDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_calendar_invoice_date]
	foreign key (idCalendarInvoiceDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_calendar_shipment_date]
	foreign key (idCalendarShipmentDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_calendar_refund_date]
	foreign key (idCalendarRefundDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_time_order_date]
	foreign key (idTimeOrderDate_sk_fk) references Warehouse.gen.dim_time (idTime_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_time_shipment_date]
	foreign key (idTimeShipmentDate_sk_fk) references Warehouse.gen.dim_time (idTime_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_store]
	foreign key (idStore_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_market]
	foreign key (idMarket_sk_fk) references Warehouse.gen.dim_market (idMarket_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_customer_scd]
	foreign key (idCustomerSCD_sk_fk) references Warehouse.gen.dim_customer_scd (idCustomerSCD_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_country_ship]
	foreign key (idCountryShipping_sk_fk) references Warehouse.gen.dim_country (idCountry_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_country_bill]
	foreign key (idCountryBilling_sk_fk) references Warehouse.gen.dim_country (idCountry_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_customer_unsubscribe]
	foreign key (idCustomerUnsubscribe_sk_fk) references Warehouse.gen.dim_customer_unsubscribe (idCustomerUnsubscribe_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_order_stage]
	foreign key (idOrderStage_sk_fk) references Warehouse.sales.dim_order_stage (idOrderStage_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_order_status]
	foreign key (idOrderStatus_sk_fk) references Warehouse.sales.dim_order_status (idOrderStatus_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_order_type]
	foreign key (idOrderType_sk_fk) references Warehouse.sales.dim_order_type (idOrderType_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_order_status_magento]
	foreign key (idOrderStatusMagento_sk_fk) references Warehouse.sales.dim_order_status_magento (idOrderStatusMagento_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_payment_method]
	foreign key (idPaymentMethod_sk_fk) references Warehouse.sales.dim_payment_method (idPaymentMethod_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_cc_type]
	foreign key (idCCType_sk_fk) references Warehouse.sales.dim_cc_type (idCCType_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_shipping_method]
	foreign key (idShippingMethod_sk_fk) references Warehouse.sales.dim_shipping_method (idShippingMethod_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_telesale]
	foreign key (idTelesale_sk_fk) references Warehouse.sales.dim_telesale (idTelesale_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_price_type]
	foreign key (idPriceType_sk_fk) references Warehouse.sales.dim_price_type (idPriceType_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_prescription_method]
	foreign key (idPrescriptionMethod_sk_fk) references Warehouse.sales.dim_prescription_method (idPrescriptionMethod_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_reminder_type]
	foreign key (idReminderType_sk_fk) references Warehouse.sales.dim_reminder_type (idReminderType_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_reminder_period]
	foreign key (idReminderPeriod_sk_fk) references Warehouse.sales.dim_reminder_period (idReminderPeriod_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_channel]
	foreign key (idChannel_sk_fk) references Warehouse.sales.dim_channel (idChannel_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_marketing_channel]
	foreign key (idMarketingChannel_sk_fk) references Warehouse.sales.dim_marketing_channel (idMarketingChannel_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_affiliate]
	foreign key (idAffiliate_sk_fk) references Warehouse.sales.dim_affiliate (idAffiliate_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_coupon_code]
	foreign key (idCouponCode_sk_fk) references Warehouse.sales.dim_coupon_code (idCouponCode_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_device_model]
	foreign key (idDeviceModel_sk_fk) references Warehouse.sales.dim_device_model (idDeviceModel_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_device_browser]
	foreign key (idDeviceBrowser_sk_fk) references Warehouse.sales.dim_device_browser (idDeviceBrowser_sk);
go
alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_device_os]
	foreign key (idDeviceOS_sk_fk) references Warehouse.sales.dim_device_os (idDeviceOS_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_customer_status]
	foreign key (idCustomerStatusLifecycle_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_product_type_oh]
	foreign key (idProductTypeOH_sk_fk) references Warehouse.prod.dim_product_type_oh (idProductTypeOH_sk);
go

alter table Warehouse.sales.dim_order_header add constraint [FK_sales_dim_order_header_dim_rank_customer_order_seq_no]
	foreign key (customer_order_seq_no) references Warehouse.sales.dim_rank_customer_order_seq_no (idCustomer_order_seq_no_sk);
go






create table Warehouse.sales.dim_order_header_wrk(
	order_id_bk						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarOrderDate_sk_fk		int NOT NULL, 
	idTimeOrderDate_sk_fk			int NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate_sk_fk		int, 
	invoice_date					datetime,
	idCalendarShipmentDate_sk_fk	int, 
	idTimeShipmentDate_sk_fk		int,
	shipment_date					datetime,
	idCalendarRefundDate_sk_fk		int, 
	refund_date						datetime,

	idStore_sk_fk					int NOT NULL,
	idMarket_sk_fk					int,
	idCustomer_sk_fk				int NOT NULL,
	idCustomerSCD_sk_fk				int NOT NULL,
	idCountryShipping_sk_fk			int,
	postcode_shipping				varchar(255), 
	idCountryBilling_sk_fk			int,
	postcode_billing				varchar(255),
	idCustomerUnsubscribe_sk_fk		int,

	idOrderStage_sk_fk				int NOT NULL, 
	idOrderStatus_sk_fk				int NOT NULL, 
	idOrderType_sk_fk				int NOT NULL, 
	idOrderStatusMagento_sk_fk		int, 
	idPaymentMethod_sk_fk			int, 
	idCCType_sk_fk					int, 
	idShippingMethod_sk_fk			int, 
	idTelesale_sk_fk				int, 
	idPriceType_sk_fk				int NOT NULL, 
	discount_f						char(1),
	idPrescriptionMethod_sk_fk		int NOT NULL, 
	idReminderType_sk_fk			int, 
	idReminderPeriod_sk_fk			int, 
	reminder_date					datetime,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,

	idChannel_sk_fk					int, 
	idMarketingChannel_sk_fk		int, 
	idAffiliate_sk_fk				int, 
	idCouponCode_sk_fk				int, 
	idDeviceModel_sk_fk				int,
	idDeviceBrowser_sk_fk			int,
	idDeviceOS_sk_fk				int,

	idCustomerStatusLifecycle_sk_fk	int,
	customer_order_seq_no			int, 
	customer_order_seq_no_web		int, 
	order_source					char(1),
	proforma						char(1),
	adjustment_order				char(1),

	idProductTypeOH_sk_fk			int,
	order_qty_time					int,  
	num_diff_product_type_oh		int,
	aura_product_p					decimal(12, 4),

	countries_registered_code		varchar(10),

	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int,

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [PK_sales_dim_order_header_wrk]
	primary key clustered (order_id_bk);
go

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idMarket_sk_fk] DEFAULT -1 for idMarket_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idCountryShipping_sk_fk] DEFAULT -1 for idCountryShipping_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idCountryBilling_sk_fk] DEFAULT -1 for idCountryBilling_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idCustomerUnsubscribe_sk_fk] DEFAULT -1 for idCustomerUnsubscribe_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idOrderType_sk_fk] DEFAULT -1 for idOrderType_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idOrderStatusMagento_sk_fk] DEFAULT -1 for idOrderStatusMagento_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idPaymentMethod_sk_fk] DEFAULT -1 for idPaymentMethod_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idCCType_sk_fk] DEFAULT -1 for idCCType_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idShippingMethod_sk_fk] DEFAULT -1 for idShippingMethod_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idChannel_sk_fk] DEFAULT -1 for idChannel_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idMarketingChannel_sk_fk] DEFAULT -1 for idMarketingChannel_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idReminderType_sk_fk] DEFAULT -1 for idReminderType_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idReminderPeriod_sk_fk] DEFAULT -1 for idReminderPeriod_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idDeviceModel_sk_fk] DEFAULT -1 for idDeviceModel_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idDeviceBrowser_sk_fk] DEFAULT -1 for idDeviceBrowser_sk_fk;
go 

alter table Warehouse.sales.dim_order_header_wrk add constraint [DF_sales_dim_order_header_wrk_idDeviceOS_sk_fk] DEFAULT -1 for idDeviceOS_sk_fk;
go 

				
	

--------------------------------------------------------

create table Warehouse.sales.dim_order_header_product_type(
	idOrderHeader_sk_fk				int NOT NULL, 
	idProductTypeOH_sk_fk			int NOT NULL, 
	order_percentage				decimal(12, 2), 
	order_flag						bit, 
	order_qty_time					int,  
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.sales.dim_order_header_product_type add constraint [PK_sales_dim_order_header_product_type]
	primary key clustered (idOrderHeader_sk_fk, idProductTypeOH_sk_fk);
go

alter table Warehouse.sales.dim_order_header_product_type add constraint [DF_sales_dim_order_header_product_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.dim_order_header_product_type add constraint [FK_sales_dim_order_header_product_type_dim_order_header]
	foreign key (idOrderHeader_sk_fk) references Warehouse.sales.dim_order_header (idOrderHeader_sk);
go
alter table Warehouse.sales.dim_order_header_product_type add constraint [FK_sales_dim_order_header_product_type_dim_product_type_oh]
	foreign key (idProductTypeOH_sk_fk) references Warehouse.prod.dim_product_type_oh (idProductTypeOH_sk);
go

create table Warehouse.sales.dim_order_header_product_type_wrk(
	idOrderHeader_sk_fk				int NOT NULL, 
	idProductTypeOH_sk_fk			int NOT NULL, 
	order_percentage				decimal(12, 2), 
	order_flag						bit, 
	order_qty_time					int, 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.sales.dim_order_header_product_type_wrk add constraint [PK_sales_dim_order_header_product_type_wrk]
	primary key clustered (idOrderHeader_sk_fk, idProductTypeOH_sk_fk);
go

alter table Warehouse.sales.dim_order_header_product_type_wrk add constraint [DF_sales_dim_order_header_product_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.sales.fact_order_line(
	idOrderLine_sk					int NOT NULL IDENTITY(1, 1), 
	order_line_id_bk				bigint NOT NULL,
	order_id						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarInvoiceDate_sk_fk		int, 
	invoice_date					datetime,
	idCalendarShipmentDate_sk_fk	int, 
	idTimeShipmentDate_sk_fk		int,
	shipment_date					datetime,
	idCalendarRefundDate_sk_fk		int, 
	refund_date						datetime,

	idWarehouse_sk_fk				int,-- NOT NULL,
	idOrderHeader_sk_fk				int NOT NULL,
	idOrderStatus_line_sk_fk		int NOT NULL,
	idPriceType_sk_fk				int NOT NULL,
	discount_f						char(1),

	idProductFamily_sk_fk			int NOT NULL,
	idBC_sk_fk						char(3), 
	idDI_sk_fk						char(4), 
	idPO_sk_fk						char(6), 
	idCY_sk_fk						char(5), 
	idAX_sk_fk						char(3), 
	idAD_sk_fk						char(4), 
	idDO_sk_fk						char(1), 
	idCOL_sk_fk						int, 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	idGlassVisionType_sk_fk			int, 
	idGlassPackageType_sk_fk		int,

	aura_product_f					char(1),

	countries_registered_code		varchar(10), 
	product_type_vat				varchar(255),

	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int,

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.sales.fact_order_line add constraint [PK_sales_fact_order_line]
	primary key clustered (idOrderLine_sk);
go

alter table Warehouse.sales.fact_order_line add constraint [DF_sales_fact_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.fact_order_line add constraint [UNIQ_sales_fact_order_line_order_line_id_bk]
	unique (order_line_id_bk);
go

create nonclustered index [INX_sales_dim_order_header_idOrderHeader_sk_fk] on Warehouse.sales.fact_order_line (idOrderHeader_sk_fk)

create nonclustered index [INX_sales_dim_order_header_idProductFamily_sk_fk] on Warehouse.sales.fact_order_line (idProductFamily_sk_fk)


alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_calendar_invoice_date]
	foreign key (idCalendarInvoiceDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_calendar_shipment_date]
	foreign key (idCalendarShipmentDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_calendar_refund_date]
	foreign key (idCalendarRefundDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_time_shipment_date]
	foreign key (idTimeShipmentDate_sk_fk) references Warehouse.gen.dim_time (idTime_sk);
go

-- alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_warehouse]
-- 	foreign key (idWarehouse_sk_fk) references Warehouse.sales.dim_price_type (idWarehouse_sk);
-- go

alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_order_header]
	foreign key (idOrderHeader_sk_fk) references Warehouse.sales.dim_order_header (idOrderHeader_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_order_status]
	foreign key (idOrderStatus_line_sk_fk) references Warehouse.sales.dim_order_status (idOrderStatus_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_price_type]
	foreign key (idPriceType_sk_fk) references Warehouse.sales.dim_price_type (idPriceType_sk);
go


alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_product_family]
	foreign key (idProductFamily_sk_fk) references Warehouse.prod.dim_product_family (idProductFamily_sk);
go

alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_bc]
	foreign key (idBC_sk_fk) references Warehouse.prod.dim_param_bc (idBC_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_di]
	foreign key (idDI_sk_fk) references Warehouse.prod.dim_param_di (idDI_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_po]
	foreign key (idPO_sk_fk) references Warehouse.prod.dim_param_po (idPO_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_cy]
	foreign key (idCY_sk_fk) references Warehouse.prod.dim_param_cy (idCY_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_ax]
	foreign key (idAX_sk_fk) references Warehouse.prod.dim_param_ax (idAX_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_ad]
	foreign key (idAD_sk_fk) references Warehouse.prod.dim_param_ad (idAD_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_do]
	foreign key (idDO_sk_fk) references Warehouse.prod.dim_param_do (idDO_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_param_col]
	foreign key (idCOL_sk_fk) references Warehouse.prod.dim_param_col (idCOL_sk);
go

alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_glass_vision_type]
	foreign key (idGlassVisionType_sk_fk) references Warehouse.prod.dim_glass_vision_type (idGlassVisionType_sk);
go
alter table Warehouse.sales.fact_order_line add constraint [FK_sales_fact_order_line_dim_glass_package_type]
	foreign key (idGlassPackageType_sk_fk) references Warehouse.prod.dim_glass_package_type (idGlassPackageType_sk);
go

create nonclustered index INX_sales_fact_order_line_order_id on Warehouse.sales.fact_order_line (order_id)


create table Warehouse.sales.fact_order_line_wrk(
	order_line_id_bk				bigint NOT NULL,
	order_id						bigint NOT NULL,
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 

	idCalendarInvoiceDate_sk_fk		int, 
	invoice_date					datetime,
	idCalendarShipmentDate_sk_fk	int, 
	idTimeShipmentDate_sk_fk		int,
	shipment_date					datetime,
	idCalendarRefundDate_sk_fk		int, 
	refund_date						datetime,

	idWarehouse_sk_fk				int, --NOT NULL,
	idOrderHeader_sk_fk				int NOT NULL,
	idOrderStatus_line_sk_fk		int NOT NULL,
	idPriceType_sk_fk				int NOT NULL,
	discount_f						char(1),

	idProductFamily_sk_fk			int NOT NULL,
	idBC_sk_fk						char(3), 
	idDI_sk_fk						char(4), 
	idPO_sk_fk						char(6), 
	idCY_sk_fk						char(5), 
	idAX_sk_fk						char(3), 
	idAD_sk_fk						char(4), 
	idDO_sk_fk						char(1), 
	idCOL_sk_fk						int, 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	idGlassVisionType_sk_fk			int, 
	idGlassPackageType_sk_fk		int,

	aura_product_f					char(1),

	countries_registered_code		varchar(10), 
	product_type_vat				varchar(255),

	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int,

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.sales.fact_order_line_wrk add constraint [PK_sales_fact_order_line_wrk]
	primary key clustered (order_line_id_bk);
go

alter table Warehouse.sales.fact_order_line_wrk add constraint [DF_sales_fact_order_line_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





--create table Warehouse.sales.dim_exchange_rate(
--	exchange_rate_day		date NOT NULL,
--	eur_to_gbp				decimal(12, 4) NOT NULL, 
--	gbp_to_eur				decimal(12, 4) NOT NULL, 

--	idETLBatchRun_ins		bigint NOT NULL, 
--	ins_ts					datetime NOT NULL, 
--	idETLBatchRun_upd		bigint, 
--	upd_ts					datetime);
--go 

--alter table Warehouse.sales.dim_exchange_rate add constraint [PK_sales_dim_exchange_rate]
--	primary key clustered (exchange_rate_day);
--go

--alter table Warehouse.sales.dim_exchange_rate add constraint [DF_sales_dim_exchange_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
--go 


create table Warehouse.sales.fact_order_line_vat_fix(
	idOrderLine_sk_fk					int NOT NULL, 
	idOrderHeader_sk_fk					int NOT NULL,

	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),
	exchange_rate						decimal(12, 4),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [PK_sales_fact_order_line_vat_fix]
	primary key clustered (idOrderLine_sk_fk);
go

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [DF_sales_fact_order_line_vat_fix_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [UNIQ_sales_fact_order_line_vat_fix_order_line_id_bk]
	unique (order_line_id_bk);
go

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [FK_sales_fact_order_line_vat_fix_fact_order_line]
	foreign key (idOrderLine_sk_fk) references Warehouse.sales.fact_order_line (idOrderLine_sk);
go



create table Warehouse.sales.fact_order_line_vat_fix_wrk(
	idOrderLine_sk_fk					int NOT NULL, 
	idOrderHeader_sk_fk					int NOT NULL,

	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),
	exchange_rate						decimal(12, 4),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 


alter table Warehouse.sales.fact_order_line_vat_fix_wrk add constraint [PK_sales_fact_order_line_vat_fix_wrk]
	primary key clustered (idOrderLine_sk_fk);
go

alter table Warehouse.sales.fact_order_line_vat_fix_wrk add constraint [DF_sales_fact_order_line_vat_fix_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
