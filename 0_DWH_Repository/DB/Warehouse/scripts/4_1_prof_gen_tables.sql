use Warehouse
go 


select idCalendar_sk, 
	calendar_date, calendar_date_num, calendar_date_name, 
	calendar_date_week, calendar_date_week_name, is_weekend, 
	week_year_num, week_year_name, week_month_num, week_month_name, 
	month_num, month_name, month_year_name, 
	quarter_num, quarter_name, quarter_year_name, 
	year_num
from Warehouse.gen.dim_calendar
order by idCalendar_sk


select idTime_sk, 
	time_name_bk, time_name, hour_name, hour, minute, day_part, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_time
order by time_name_bk

select time_name_bk, time_name, hour_name, hour, minute, day_part, 
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_time_wrk
order by time_name_bk




select idCompany_sk,
	company_name_bk, company_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_company

select company_name_bk, company_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_company_wrk


select idStore_sk,
	store_id_bk, store_name, website_type, website_group, website, tld, code_tld, acquired,
	idCompany_sk_fk,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_store

select store_id_bk, store_name, website_type, website_group, website, tld, code_tld, acquired,
	idCompany_sk_fk,
	idETLBatchRun, ins_ts
from Warehouse.gen.dim_store_wrk




select idCountry_sk, country_id_bk, 
	country_code, country_name, country_zone, country_type, country_continent, country_state,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_country

select country_id_bk, 
	country_code, country_name, country_zone, country_type, country_continent, country_state,
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_country_wrk


select idRegion_sk, region_id_bk, region_name, idCountry_sk_fk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_region

select region_id_bk, region_name, idCountry_sk_fk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_region_wrk



select idMarket_sk, market_id_bk, market_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_market

select market_id_bk, market_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_market_wrk




select idCustomerType_sk, customer_type_name_bk, customer_type_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_customer_type

select customer_type_name_bk, customer_type_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_customer_type_wrk


select idCustomerStatus_sk, customer_status_name_bk, customer_status_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_customer_status

select customer_status_name_bk, customer_status_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_customer_status_wrk


select idCustomerUnsubscribe_sk, customer_unsubscribe_name_bk, customer_unsubscribe_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_customer_unsubscribe

select customer_unsubscribe_name_bk, customer_unsubscribe_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_customer_unsubscribe_wrk


select top 1000 idCustomer_sk, 
	customer_id_bk, customer_email,
	created_at, updated_at,
	idStore_sk_fk, idMarket_sk_fk,
	idCustomerType_sk_fk, idCustomerStatus_sk_fk,
	prefix, first_name, last_name, 
	migrate_customer_f, migrate_customer_store, migrate_customer_id, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_customer

select top 1000 idCustomerSCD_sk, 
	idCustomer_sk_fk, customer_id_bk, customer_email,
	created_at, updated_at,
	idStore_sk_fk, idMarket_sk_fk,
	idCustomerType_sk_fk, idCustomerStatus_sk_fk,
	prefix, first_name, last_name, 
	dob, gender, language, 
	phone_number, street, city, postcode, region, idRegion_sk_fk, idCountry_sk_fk, 
	postcode_s, idCountry_s_sk_fk, 
	idCustomerUnsubscribe_sk_fk,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk, 
	unsubscribe_text_message_f, unsubscribe_text_message_d_sk_fk, 
	reminder_f, idReminderType_sk_fk, idReminderPeriod_sk_fk, 
	reorder_f, 
	referafriend_code, days_worn_info, 
	idCalendarFrom_sk_fk, idCalendarTo_sk_fk, isCurrent, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_customer_SCD

select customer_id_bk, customer_email,
	created_at, updated_at,
	idStore_sk_fk, idMarket_sk_fk,
	idCustomerType_sk_fk, idCustomerStatus_sk_fk,
	prefix, first_name, last_name, 
	dob, gender, language, 
	phone_number, street, city, postcode, region, idRegion_sk_fk, idCountry_sk_fk, 
	postcode_s, idCountry_s_sk_fk, 
	idCustomerUnsubscribe_sk_fk,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk, 
	unsubscribe_text_message_f, unsubscribe_text_message_d_sk_fk, 
	reminder_f, idReminderType_sk_fk, idReminderPeriod_sk_fk, 
	reorder_f, 
	referafriend_code, days_worn_info, 
	migrate_customer_f, migrate_customer_store, migrate_customer_id, 
	idETLBatchRun_ins, ins_ts
from Warehouse.gen.dim_customer_wrk
