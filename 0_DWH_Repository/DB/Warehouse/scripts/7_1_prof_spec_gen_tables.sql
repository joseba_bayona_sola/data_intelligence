
select idETLBatchRun_ins, count(*)
from Warehouse.gen.dim_customer
group by idETLBatchRun_ins
order by idETLBatchRun_ins

select idETLBatchRun_upd, count(*)
from Warehouse.gen.dim_customer
where idETLBatchRun_upd is not null
group by idETLBatchRun_upd
order by idETLBatchRun_upd

select *
from Warehouse.gen.dim_customer
where idETLBatchRun_upd is not null


select *
from
	(select count(*) over (partition by email) num_rows, *
	from Landing.mag.customer_entity_flat_aud) t
where num_rows > 1
order by num_rows desc, email, entity_id

select *
from
	(select count(*) over (partition by customer_email) num_rows, *
	from Warehouse.gen.dim_customer_v) t
where num_rows > 1
order by num_rows desc, customer_email, customer_id_bk


------------------------------------------------------

select *
from Warehouse.gen.dim_customer_SCD
where idCustomer_sk_fk = 6629

select idETLBatchRun_ins, count(*)
from Warehouse.gen.dim_customer_SCD
group by idETLBatchRun_ins
order by idETLBatchRun_ins

select idETLBatchRun_upd, count(*)
from Warehouse.gen.dim_customer_SCD
where idETLBatchRun_upd is not null
group by idETLBatchRun_upd
order by idETLBatchRun_upd

select *
from Warehouse.gen.dim_customer_SCD
where idETLBatchRun_upd is not null
order by customer_id_bk

select customer_id_bk, count(*)
from Warehouse.gen.dim_customer_SCD
where isCurrent = 'Y'
group by customer_id_bk
order by count(*) desc


select *
from
	(select count(*) over (partition by customer_id_bk) num_rows, *
	from Warehouse.gen.dim_customer_SCD) t
where num_rows > 1
order by num_rows desc, customer_id_bk, isCurrent, ins_ts
