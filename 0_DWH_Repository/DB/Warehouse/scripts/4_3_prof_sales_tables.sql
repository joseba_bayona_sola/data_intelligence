use Warehouse
go 


select idReminderType_sk, 
	reminder_type_name_bk, reminder_type_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_reminder_type

select reminder_type_name_bk, reminder_type_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_reminder_type_wrk


select idReminderPeriod_sk, 
	reminder_period_bk, reminder_period_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_reminder_period

select reminder_period_bk, reminder_period_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_reminder_period_wrk



select idOrderStage_sk, 
	order_stage_name_bk, order_stage_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_stage

select order_stage_name_bk, order_stage_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_stage_wrk


select idOrderStatus_sk, 
	order_status_name_bk, order_status_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_status

select order_status_name_bk, order_status_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_status_wrk


select idOrderType_sk, 
	order_type_name_bk, order_type_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_type

select order_type_name_bk, order_type_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_type_wrk


select idOrderStatusMagento_sk, 
	status_bk, order_status_magento_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_status_magento

select status_bk, order_status_magento_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_status_magento_wrk




select idPaymentMethod_sk, 
	payment_method_name_bk, payment_method_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_payment_method

select payment_method_name_bk, payment_method_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_payment_method_wrk


select idCCType_sk, 
	cc_type_name_bk, cc_type_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_cc_type

select cc_type_name_bk, cc_type_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_cc_type_wrk


select idShippingCarrier_sk, 
	shipping_carrier_name_bk, shipping_carrier_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_shipping_carrier

select shipping_carrier_name_bk, shipping_carrier_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_shipping_carrier_wrk


select idShippingMethod_sk, 
	shipping_description_bk, shipping_method_name, idShippingCarrier_sk_fk, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_shipping_method

select shipping_description_bk, shipping_method_name, idShippingCarrier_sk_fk, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_shipping_method_wrk



select idTelesale_sk, 
	telesales_admin_username_bk, telesales_username, 
	user_id, telesales_first_name, telesales_last_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_telesale

select telesales_admin_username_bk, telesales_username, 
	user_id, telesales_first_name, telesales_last_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_telesale_wrk

		select 
			t1.telesales_admin_username_bk, t2.telesales_admin_username_bk, 
			t1.telesales_username, t2.telesales_username, 
			t1.user_id, t2.user_id, t1.telesales_first_name, t2.telesales_first_name, t1.telesales_last_name, t2.telesales_last_name
		from 
				Warehouse.sales.dim_telesale t1 
			inner join
				Warehouse.sales.dim_telesale_wrk t2 on t1.telesales_admin_username_bk = t2.telesales_admin_username_bk
		where t1.telesales_username <> t2.telesales_username or t1.user_id <> t2.user_id or 
			t1.telesales_first_name <> t2.telesales_first_name or t1.telesales_last_name <> t2.telesales_last_name 


select idPriceType_sk, 
	price_type_name_bk, price_type_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_price_type

select price_type_name_bk, price_type_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_price_type_wrk


select idPrescriptionMethod_sk, 
	prescription_method_name_bk, prescription_method_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_prescription_method

select prescription_method_name_bk, prescription_method_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_prescription_method_wrk






select idChannel_sk, 
	channel_name_bk, channel_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_channel

select channel_name_bk, channel_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_channel_wrk


select idMarketingChannel_sk, 
	marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel_name, description,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_marketing_channel

select marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel_name, description,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_marketing_channel_wrk



select idAffiliate_sk, 
	publisher_id_bk, affiliate_name, affiliate_website, affiliate_network_name, affiliate_type_name, affiliate_status_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_affiliate

select publisher_id_bk, affiliate_name, affiliate_website, affiliate_network_name, affiliate_type_name, affiliate_status_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_affiliate_wrk



select idGroupCouponCode_sk, 
	channel_bk, group_coupon_code_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_group_coupon_code

select channel_bk, group_coupon_code_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_group_coupon_code_wrk



select idCouponCode_sk, 
	coupon_id_bk, idGroupCouponCode_sk_fk, rule_id, coupon_code, coupon_code_name, 
	from_date, to_date, expiration_date, discount_type_name, discount_amount, only_new_customer, refer_a_friend,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_coupon_code

select coupon_id_bk, idGroupCouponCode_sk_fk, rule_id, coupon_code, coupon_code_name, 
	from_date, to_date, expiration_date, discount_type_name, discount_amount, only_new_customer, refer_a_friend,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_coupon_code_wrk



select idDeviceCategory_sk, 
	device_category_name_bk, device_category_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_device_category

select device_category_name_bk, device_category_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_device_category_wrk



select idDeviceBrand_sk, 
	device_brand_name_bk, idDeviceCategory_sk_fk, device_brand_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_device_brand

select device_brand_name_bk, idDeviceCategory_sk_fk, device_brand_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_device_brand_wrk



select idDeviceModel_sk, 
	device_model_name_bk, idDeviceBrand_sk_fk, device_model_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_device_model

select device_model_name_bk, idDeviceBrand_sk_fk, device_model_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_device_model_wrk



select idDeviceBrowser_sk, 
	device_browser_name_bk, device_browser_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_device_browser

select device_browser_name_bk, device_browser_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_device_browser_wrk



select idDeviceOS_sk, 
	device_os_name_bk, device_os_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_device_os

select device_os_name_bk, device_os_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_device_os_wrk



select idCustomer_order_seq_no_sk,
	customer_order_seq_no_bk, rank_seq_no,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_rank_customer_order_seq_no

select customer_order_seq_no_bk, rank_seq_no,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_rank_customer_order_seq_no_wrk



select idQty_time_sk,
	qty_time_bk, rank_qty_time,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_rank_qty_time

select qty_time_bk, rank_qty_time,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_rank_qty_time_wrk



select idFreq_time_sk,
	freq_time_bk, rank_freq_time,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_rank_freq_time

select freq_time_bk, rank_freq_time,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_rank_freq_time_wrk


select idShipping_days_sk,
	shipping_days_bk, rank_shipping_days,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_rank_shipping_days

select shipping_days_bk, rank_shipping_days,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_rank_shipping_days_wrk

-----------------------------------------------------------------------

select top 1000 idOrderHeader_sk, 
	order_id_bk, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate_sk_fk, idTimeOrderDate_sk_fk, order_date, 
	idCalendarInvoiceDate_sk_fk, invoice_date, 
	idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
	idCalendarRefundDate_sk_fk, refund_date, 
	idStore_sk_fk, idMarket_sk_fk, idCustomer_sk_fk, idCustomerSCD_sk_fk, 
	idCountryShipping_sk_fk, postcode_shipping, idCountryBilling_sk_fk, postcode_billing,
	idCustomerUnsubscribe_sk_fk,
	idOrderStage_sk_fk, idOrderStatus_sk_fk, adjustment_order, idOrderType_sk_fk, idOrderStatusMagento_sk_fk, 
	idPaymentMethod_sk_fk, idCCType_sk_fk, idShippingMethod_sk_fk, idTelesale_sk_fk, idPriceType_sk_fk, discount_f, idPrescriptionMethod_sk_fk,
	idReminderType_sk_fk, idReminderPeriod_sk_fk, reminder_date, reorder_f, reorder_date, 
	idChannel_sk_fk, idMarketingChannel_sk_fk, idAffiliate_sk_fk, idCouponCode_sk_fk, idDeviceModel_sk_fk, idDeviceBrowser_sk_fk, idDeviceOS_sk_fk,
	idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, order_source, proforma,
	idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh, aura_product_p,
	countries_registered_code,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines, 

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_header

select 	order_id_bk, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate_sk_fk, idTimeOrderDate_sk_fk, order_date, 
	idCalendarInvoiceDate_sk_fk, invoice_date, 
	idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
	idCalendarRefundDate_sk_fk, refund_date, 
	idStore_sk_fk, idMarket_sk_fk, idCustomer_sk_fk, idCustomerSCD_sk_fk, 
	idCountryShipping_sk_fk, postcode_shipping, idCountryBilling_sk_fk, postcode_billing,
	idCustomerUnsubscribe_sk_fk,
	idOrderStage_sk_fk, idOrderStatus_sk_fk, adjustment_order, idOrderType_sk_fk, idOrderStatusMagento_sk_fk, 
	idPaymentMethod_sk_fk, idCCType_sk_fk, idShippingMethod_sk_fk, idTelesale_sk_fk, idPriceType_sk_fk, discount_f, idPrescriptionMethod_sk_fk,
	idReminderType_sk_fk, idReminderPeriod_sk_fk, reminder_date, reorder_f, reorder_date, 
	idChannel_sk_fk, idMarketingChannel_sk_fk, idAffiliate_sk_fk, idCouponCode_sk_fk, idDeviceModel_sk_fk, idDeviceBrowser_sk_fk, idDeviceOS_sk_fk,
	idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, order_source, proforma,
	idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh, aura_product_p,
	countries_registered_code,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines, 

	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_header_wrk



select idOrderHeader_sk_fk, idProductTypeOH_sk_fk, 
	order_percentage, order_flag, order_qty_time,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.dim_order_header_product_type

select idOrderHeader_sk_fk, idProductTypeOH_sk_fk, 
	order_percentage, order_flag, order_qty_time,
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.dim_order_header_product_type_wrk



select idOrderLine_sk, 
	order_line_id_bk, 
	order_id, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarInvoiceDate_sk_fk, invoice_date, 
	idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
	idCalendarRefundDate_sk_fk, refund_date, 
	idWarehouse_sk_fk, idOrderHeader_sk_fk, idOrderStatus_line_sk_fk, idPriceType_sk_fk, discount_f, 
	idProductFamily_sk_fk,
	idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, eye,
	sku_magento, sku_erp, 
	idGlassVisionType_sk_fk, idGlassPackageType_sk_fk, 
	aura_product_f,
	countries_registered_code, product_type_vat,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines,

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.fact_order_line

select order_line_id_bk, 
	order_id, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarInvoiceDate_sk_fk, invoice_date, 
	idCalendarShipmentDate_sk_fk, idTimeShipmentDate_sk_fk, shipment_date, 
	idCalendarRefundDate_sk_fk, refund_date, 
	idWarehouse_sk_fk, idOrderHeader_sk_fk, idOrderStatus_line_sk_fk, idPriceType_sk_fk, discount_f, 
	idProductFamily_sk_fk,
	idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, eye,
	sku_magento, sku_erp, 
	idGlassVisionType_sk_fk, idGlassPackageType_sk_fk, 
	aura_product_f,
	countries_registered_code, product_type_vat,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines,

	idETLBatchRun_ins, ins_ts
from Warehouse.sales.fact_order_line_wrk



--select exchange_rate_day, eur_to_gbp, gbp_to_eur, 
--	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
--from Warehouse.sales.dim_exchange_rate


select idOrderLine_sk_fk, idOrderHeader_sk_fk, 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, exchange_rate,
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.fact_order_line_vat_fix

select idOrderLine_sk_fk, idOrderHeader_sk_fk, 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, exchange_rate,
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.fact_order_line_vat_fix_wrk