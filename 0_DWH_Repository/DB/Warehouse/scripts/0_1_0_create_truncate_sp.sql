use Warehouse
go 

drop function dbo.fnSplitString;

create function dbo.fnSplitString(@string nvarchar(max), @delimiter char(1) = ',') returns @t table(string nvarchar(max))
as
begin

	declare @pos int;
	declare @piece varchar(500);

	if right(rtrim(@string),1) <> @delimiter set @string = @string  + @delimiter

	set @pos =  patindex('%' + @delimiter + '%' , @string)

	while @pos <> 0 
	begin
		set @piece = left(@string, @pos - 1)
 
		-- You have a piece of data, so insert it, print it, do whatever you want to with it.
		insert	@t select	@piece

		set @string = stuff(@string, 1, @pos,'')
		set @pos =  patindex('%' + @delimiter + '%' , @string)
	end

	return 
end;
go

drop procedure dbo.truncateTables;

create procedure dbo.truncateTables
    @TableList varchar(MAX)
as

begin

    DECLARE @TableName sysname;
    DECLARE @TablesParsed TABLE (TableName sysname NOT NULL);
    DECLARE @sql nvarchar (1000);

	if isnull(@tableList,'') = '' RETURN;

    insert into @TablesParsed (TableName) 
		select cast(s.string AS sysname)
		from dbo.fnSplitString (@tableList,',') AS s
		where isnull(s.string, '') <> '';

    while exists (select * from @TablesParsed) 
    begin
		select top 1 @TableName = TableName FROM @TablesParsed;

        set @sql = concat('TRUNCATE TABLE ', @TableName);
        
        exec (@sql);

        delete from @TablesParsed where TableName = @TableName;
    end;

end;
go 
