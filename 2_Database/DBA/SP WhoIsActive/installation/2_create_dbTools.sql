--Vision Direct
--https://lenses.atlassian.net/wiki/spaces/TECH/pages/1484455942/sp+WhoIsActive

--Step 1.
--Create the [DBAtools] database.

USE [master]
GO

CREATE DATABASE [DBAtools]
CONTAINMENT = NONE
ON PRIMARY ( NAME = N'DBAtools', FILENAME = N'D:\databases\DBAtools.mdf', SIZE = 1048576KB, FILEGROWTH = 1048576KB)
LOG ON ( NAME = N'DBAtools_log', FILENAME = N'D:\databases\DBAtools_log.ldf', SIZE = 1024KB, FILEGROWTH = 1024KB)
GO

ALTER DATABASE [DBAtools] MODIFY FILE ( NAME = N'DBAtools', MAXSIZE = 262144000KB )
ALTER DATABASE [DBAtools] SET COMPATIBILITY_LEVEL = 120
ALTER DATABASE [DBAtools] SET ANSI_NULL_DEFAULT OFF 
ALTER DATABASE [DBAtools] SET ANSI_NULLS OFF 
ALTER DATABASE [DBAtools] SET ANSI_PADDING OFF 
ALTER DATABASE [DBAtools] SET ANSI_WARNINGS OFF 
ALTER DATABASE [DBAtools] SET ARITHABORT OFF 
ALTER DATABASE [DBAtools] SET AUTO_CLOSE OFF 
ALTER DATABASE [DBAtools] SET AUTO_SHRINK OFF 
ALTER DATABASE [DBAtools] SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF)
ALTER DATABASE [DBAtools] SET AUTO_UPDATE_STATISTICS ON 
ALTER DATABASE [DBAtools] SET CURSOR_CLOSE_ON_COMMIT OFF 
ALTER DATABASE [DBAtools] SET CURSOR_DEFAULT  GLOBAL 
ALTER DATABASE [DBAtools] SET CONCAT_NULL_YIELDS_NULL OFF 
ALTER DATABASE [DBAtools] SET NUMERIC_ROUNDABORT OFF 
ALTER DATABASE [DBAtools] SET QUOTED_IDENTIFIER OFF 
ALTER DATABASE [DBAtools] SET RECURSIVE_TRIGGERS OFF 
ALTER DATABASE [DBAtools] SET  DISABLE_BROKER 
ALTER DATABASE [DBAtools] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
ALTER DATABASE [DBAtools] SET DATE_CORRELATION_OPTIMIZATION OFF 
ALTER DATABASE [DBAtools] SET PARAMETERIZATION SIMPLE 
ALTER DATABASE [DBAtools] SET READ_COMMITTED_SNAPSHOT OFF 
ALTER DATABASE [DBAtools] SET  READ_WRITE 
ALTER DATABASE [DBAtools] SET RECOVERY SIMPLE 
ALTER DATABASE [DBAtools] SET  MULTI_USER 
ALTER DATABASE [DBAtools] SET PAGE_VERIFY CHECKSUM  
ALTER DATABASE [DBAtools] SET TARGET_RECOVERY_TIME = 0 SECONDS 
ALTER DATABASE [DBAtools] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DBAtools]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [DBAtools] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO

--Step 1 done.



--Step 2.
--Create the [WhoIsActive] table.

USE [master]
GO

DECLARE @s VARCHAR(MAX)

EXEC sp_WhoIsActive 
@get_plans = 2, 
@get_task_info = 2, 
@format_output = 0, 
@output_column_list = '
[dd hh:mm:ss.mss] 
[start_time] 
[percent_complete] 
[collection_time] 
[status] 
[session_id] 
[request_id] 
[login_name] 
[host_name] 
[database_name] 
[program_name] 
[wait_info] 
[blocking_session_id] 
[CPU] 
[tempdb_allocations] 
[tempdb_current] 
[reads] 
[writes] 
[physical_reads] 
[used_memory] 
[open_tran_count]
[sql_text] 
[query_plan] 
',
@return_schema = 1, 
@schema = @s OUTPUT

SET @s = REPLACE(@s, '<table_name>', 'DBAtools.dbo.WhoIsActive')

EXEC(@s) 
GO

USE [DBAtools]
GO

CREATE UNIQUE CLUSTERED INDEX [CI-20190808-100011] ON [dbo].[WhoIsActive] ([collection_time] ASC, [session_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, MAXDOP = 4)
GO

--Step 2 done.



--Step 3.
--Test configuration by running the sp 10 times with a 5s delay. 

EXEC sp_WhoIsActive 
@get_plans = 2, 
@get_task_info = 2, 
@format_output = 0, 
@output_column_list = '
[dd hh:mm:ss.mss] 
[start_time] 
[percent_complete] 
[collection_time] 
[status] 
[session_id] 
[request_id] 
[login_name] 
[host_name] 
[database_name] 
[program_name] 
[wait_info] 
[blocking_session_id] 
[CPU] 
[tempdb_allocations] 
[tempdb_current] 
[reads] 
[writes] 
[physical_reads] 
[used_memory] 
[open_tran_count]
[sql_text] 
[query_plan] 
',
@destination_table = 'DBAtools.dbo.WhoIsActive'

WAITFOR DELAY '00:00:05' 
GO 10

--Step 3 done.



--Step 4.

--Query the collected data.
SELECT TOP (50) 
--*
[start_time]
--,[percent_complete]
,[collection_time]
,DATEDIFF(MINUTE,start_time,collection_time) AS 'exec_time[mm]'
,[status]
,[session_id]
--,[request_id]
,[login_name]
,[host_name]
,[database_name]
--,[program_name]
,[wait_info]
,[blocking_session_id]
,[CPU]
,[tempdb_allocations]
,[tempdb_current]
,[reads]
,[writes]
,[physical_reads]
,[used_memory]
,[open_tran_count]
,[sql_text]
--,[query_plan]
FROM [DBAtools].[dbo].[WhoIsActive] WITH (NOLOCK)
WHERE 
1=1
--AND DATEDIFF(MINUTE,start_time,collection_time) > 10
ORDER BY collection_time DESC
;

--Step 4 done.



--Step 5.
--Create a SQL Agent Job with this T-SQL code to populate the table, on a schedule.

EXEC sp_WhoIsActive 
@get_plans = 2, 
@get_task_info = 2, 
@format_output = 0, 
@output_column_list = '
[dd hh:mm:ss.mss] 
[start_time] 
[percent_complete] 
[collection_time] 
[status] 
[session_id] 
[request_id] 
[login_name] 
[host_name] 
[database_name] 
[program_name] 
[wait_info] 
[blocking_session_id] 
[CPU] 
[tempdb_allocations] 
[tempdb_current] 
[reads] 
[writes] 
[physical_reads] 
[used_memory] 
[open_tran_count]
[sql_text] 
[query_plan] 
',
@destination_table = 'DBAtools.dbo.WhoIsActive'

--Step 5 done.



--Step 6.
--Create an extra step in the Job to cleanup old collected data.
--The database max size was set to 250GB on the dev server. It collected about 5GB of data over 24 hours.
--So expected to have enough free space for at least 40 days of collection.

DELETE
FROM [DBAtools].[dbo].[WhoIsActive] 
WHERE 
1=1
AND DATEDIFF(DAY,collection_time,GETDATE()) > 40
;

--Step 6 done.



