
USE [master]
GO

DECLARE @s VARCHAR(MAX)

EXEC sp_WhoIsActive @get_plans = 2, @get_task_info = 2, @format_output = 0, 
	@output_column_list = '
		[dd hh:mm:ss.mss] 
		[start_time] 
		[percent_complete] 
		[collection_time] 
		[status] 
		[session_id] 
		[request_id] 
		[login_name] 
		[host_name] 
		[database_name] 
		[program_name] 
		[wait_info] 
		[blocking_session_id] 
		[CPU] 
		[tempdb_allocations] 
		[tempdb_current] 
		[reads] 
		[writes] 
		[physical_reads] 
		[used_memory] 
		[open_tran_count]
		[sql_text] 
		[query_plan]',
	@return_schema = 1, 
	@schema = @s OUTPUT

SET @s = REPLACE(@s, '<table_name>', 'DBAtools.dbo.WhoIsActive')

EXEC(@s) 
GO

USE [DBAtools]
GO

CREATE UNIQUE CLUSTERED INDEX [CI-20190808-100011] ON [dbo].[WhoIsActive] ([collection_time] ASC, [session_id] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = ON, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, MAXDOP = 4)
GO

-----------------------------------------------

USE [DBAtools]
GO

create view dbo.WhoIsActive_v as

	select duration_min, num_rep, ord_rep,
		case when (num_rep = ord_rep) then 'Y' else 'N' end latest_f,
		start_time, start_time_date, percent_complete, collection_time, status, 
		session_id, request_id, login_name, host_name, 
		sql_text, 
		database_name, program_name, 
		wait_info, blocking_session_id, 
		cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	from
		(select 
			datediff(minute, start_time, collection_time) duration_min,
			count(*) over (partition by session_id, start_time, sql_text) num_rep, 
			dense_rank() over (partition by session_id, start_time, sql_text order by collection_time) ord_rep, 
			start_time, convert(date, start_time) start_time_date, percent_complete, collection_time, status, 
			session_id, request_id, login_name, host_name, 
			sql_text, 
			database_name, program_name, 
			wait_info, blocking_session_id, 
			cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
			-- , query_plan
		from DBAtools.dbo.WhoIsActive) wia
go