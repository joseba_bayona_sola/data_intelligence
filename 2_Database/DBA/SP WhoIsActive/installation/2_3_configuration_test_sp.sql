
USE [DBAtools]
GO

EXEC sp_WhoIsActive 
	@get_plans = 2, @get_task_info = 2, @format_output = 0, 
	@output_column_list = '
		[dd hh:mm:ss.mss] 
		[start_time] 
		[percent_complete] 
		[collection_time] 
		[status] 
		[session_id] 
		[request_id] 
		[login_name] 
		[host_name] 
		[database_name] 
		[program_name] 
		[wait_info] 
		[blocking_session_id] 
		[CPU] 
		[tempdb_allocations] 
		[tempdb_current] 
		[reads] 
		[writes] 
		[physical_reads] 
		[used_memory] 
		[open_tran_count]
		[sql_text] 
		[query_plan]',
	@destination_table = 'DBAtools.dbo.WhoIsActive'

WAITFOR DELAY '00:00:05' 
GO 10
