

select top 10000 
	duration_min, num_rep, ord_rep, latest_f,
	start_time, start_time_date, percent_complete, collection_time, status, 
	session_id, request_id, login_name, host_name, 
	sql_text, 
	database_name, program_name, 
	wait_info, blocking_session_id, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	-- , query_plan
from DBAtools.dbo.WhoIsActive_v
where latest_f = 'Y' 
	-- and duration_min > 5
order by collection_time desc

select top 10000 
	duration_min, start_time, start_time_date, status, 
	session_id, login_name,
	sql_text, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	-- , query_plan
from DBAtools.dbo.WhoIsActive_v
where latest_f = 'Y' 
	and duration_min > 5
	and login_name = 'tableau_user' and sql_text like '%sales_ds_v%'
	-- and login_name = 'tableau_user' and sql_text like '%sales_erp_ds_v%'
order by collection_time desc


	select top 1000 count(*)
	from DBAtools.dbo.WhoIsActive_v

	select top 1000 latest_f, count(*)
	from DBAtools.dbo.WhoIsActive_v
	group by latest_f
	order by latest_f

	select top 1000 login_name, host_name, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by login_name, host_name
	order by login_name, host_name

	select top 1000 session_id, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by session_id
	order by session_id

	select top 1000 sql_text, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by sql_text
	order by count(*) desc, sql_text

	select top 1000 database_name, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by database_name
	order by database_name

	select top 1000 program_name, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by program_name
	order by program_name

	select wait_info, count(*)
	from DBAtools.dbo.WhoIsActive_v
	where latest_f = 'Y' 
		and duration_min > 5
	group by wait_info
	order by wait_info

------------------------------------------------------

select top 10000 
	duration_min, num_rep, ord_rep, latest_f,
	start_time, start_time_date, percent_complete, collection_time, status, 
	session_id, request_id, login_name, host_name, 
	sql_text, 
	database_name, program_name, 
	wait_info, blocking_session_id, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	-- , query_plan
from DBAtools.dbo.WhoIsActive_v
where -- latest_f = 'Y' and 
	sql_text like '%alloc_order_header_erp_dim_order%'
order by collection_time desc
