
-- set statistics io, time on;


select convert(bigint, 577363049) * 8 / 1024 / 1024

select convert(bigint, 582742) * 8 / 1024 / 1024


select top 10
	datediff(minute, start_time, collection_time),
	start_time, percent_complete, collection_time, status, 
	session_id, request_id, login_name, host_name, 
	sql_text, 
	database_name, program_name, 
	wait_info, blocking_session_id, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	-- , query_plan
from DBAtools.dbo.WhoIsActive
-- where abs(datediff(minute, collection_time, start_time)) > 10
order by collection_time desc

	select top 1000 count(*), min(convert(date, collection_time)), max(convert(date, collection_time))
	from DBAtools.dbo.WhoIsActive

	select top 1000 login_name, host_name, count(*)
	from DBAtools.dbo.WhoIsActive
	group by login_name, host_name
	order by login_name, host_name

	select top 1000 session_id, count(*)
	from DBAtools.dbo.WhoIsActive
	group by session_id
	order by session_id

	select top 1000 sql_text, count(*)
	from DBAtools.dbo.WhoIsActive
	group by sql_text
	order by count(*) desc, sql_text

	select top 1000 database_name, count(*)
	from DBAtools.dbo.WhoIsActive
	group by database_name
	order by database_name

	select top 1000 program_name, count(*)
	from DBAtools.dbo.WhoIsActive
	group by program_name
	order by program_name

	select wait_info, count(*)
	from DBAtools.dbo.WhoIsActive
	group by wait_info
	order by wait_info


------------------------------------------------------

select top 10 start_time, percent_complete, collection_time, status, 
	session_id, request_id, login_name, host_name, 
	sql_text, 
	database_name, program_name, 
	wait_info, blocking_session_id, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	, query_plan
from DBAtools.dbo.WhoIsActive
-- where program_name = 'Tableau 2019.2'
where sql_text like '%alloc_order_header_erp_dim_order%'
order by collection_time desc

------------------------------------------------------


select top 10000 
	datediff(minute, start_time, collection_time) duration_min,
	count(*) over (partition by session_id, start_time) num_rep, 
	dense_rank() over (partition by session_id, start_time order by collection_time) ord_rep, 
	start_time, percent_complete, collection_time, status, 
	session_id, request_id, login_name, host_name, 
	sql_text, 
	database_name, program_name, 
	wait_info, blocking_session_id, 
	cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	-- , query_plan
from DBAtools.dbo.WhoIsActive
where 
	-- (session_id = 56 and start_time = '2019-08-13 12:03:55.990') 
	-- or (session_id = 59 and start_time = '2019-08-13 09:43:59.700')
	(session_id = 56 and start_time = '2019-08-13 03:03:44.873')
	or (session_id = 83 and start_time = '2019-08-13 14:05:08.313')
order by collection_time desc

------------------------------------------------------

drop table #t

select t1.session_id, t2.collection_time
into #t
from 
		DBAtools.dbo.WhoIsActive  t1
	inner join
		DBAtools.dbo.WhoIsActive_v t2 on t1.session_id = t2.session_id and t1.collection_time = t2.collection_time
where -- (DATEDIFF(DAY,t1.collection_time,GETDATE()) > 5 and t2.latest_f = 'N') or 
	t2.latest_f = 'N' or
	(DATEDIFF(DAY,t1.collection_time,GETDATE()) > 30 and t2.latest_f = 'Y')

select count(*)
from 
		DBAtools.dbo.WhoIsActive  t1
	inner join
		#t t2 on t1.session_id = t2.session_id and t1.collection_time = t2.collection_time


delete from t1
from 
		DBAtools.dbo.WhoIsActive  t1
	inner join
		#t t2 on t1.session_id = t2.session_id and t1.collection_time = t2.collection_time
