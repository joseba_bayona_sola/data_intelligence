use DBAtools
go

drop view dbo.WhoIsActive_v 
go 

create view dbo.WhoIsActive_v as

	select duration_min, num_rep, ord_rep,
		case when (num_rep = ord_rep) then 'Y' else 'N' end latest_f,
		start_time, start_time_date, percent_complete, collection_time, status, 
		session_id, request_id, login_name, host_name, 
		sql_text, 
		database_name, program_name, 
		wait_info, blocking_session_id, 
		cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
	from
		(select 
			datediff(minute, start_time, collection_time) duration_min,
			count(*) over (partition by session_id, start_time, sql_text) num_rep, 
			dense_rank() over (partition by session_id, start_time, sql_text order by collection_time) ord_rep, 
			start_time, convert(date, start_time) start_time_date, percent_complete, collection_time, status, 
			session_id, request_id, login_name, host_name, 
			sql_text, 
			database_name, program_name, 
			wait_info, blocking_session_id, 
			cpu, tempdb_allocations, tempdb_current, reads, writes, physical_reads, used_memory, open_tran_count
			-- , query_plan
		from DBAtools.dbo.WhoIsActive) wia
go