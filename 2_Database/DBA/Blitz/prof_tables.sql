
-- BlitzWho

select top 100 *
from DBAtools.dbo.BlitzWho

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzWho

-- BlitzCache

select top 100 *
from DBAtools.dbo.BlitzCache

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzCache

-- BlitzCacheResults

select top 100 *
from DBAtools.dbo.BlitzCacheResults

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzCacheResults

-- BlitzFirst

select top 100 ID, ServerName, CheckDate, 
	CheckID, Priority, FindingsGroup, Finding, URL, Details, 
	HowToStopIt, QueryPlan, QueryText, StartTime, LoginName, NTUserName, OriginalLoginName, ProgramName, HostName, 
	DatabaseID, DatabaseName, OpenTransactionCount, DetailsInt, QueryHash, JoinKey
from DBAtools.dbo.BlitzFirst
where CheckID = 12

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst

	select top 1000 ServerName, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst
	group by ServerName
	order by ServerName

	select top 1000 CheckID, Priority, FindingsGroup, Finding, URL, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst
	group by CheckID, Priority, FindingsGroup, Finding, URL
	order by CheckID, Priority, FindingsGroup, Finding, URL

-- BlitzFirst_FileStats

select top 1000 ID, ServerName, CheckDate, 
	DatabaseID, FileID, DatabaseName, FileLogicalName, TypeDesc, SizeOnDiskMB, 
	io_stall_read_ms, num_of_reads, bytes_read, 
	io_stall_write_ms, num_of_writes, bytes_written, 
	PhysicalName
from DBAtools.dbo.BlitzFirst_FileStats
where DatabaseName = 'Landing' and TypeDesc = 'ROWS'
order by CheckDate

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_FileStats

	select top 1000 ServerName, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_FileStats
	group by ServerName
	order by ServerName

	select top 1000 DatabaseID, FileID, DatabaseName, FileLogicalName, TypeDesc, PhysicalName, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_FileStats
	group by DatabaseID, FileID, DatabaseName, FileLogicalName, TypeDesc, PhysicalName
	order by DatabaseID, FileID, DatabaseName, FileLogicalName, TypeDesc, PhysicalName

-- BlitzFirst_PerfmonStats

select top 100 ID, ServerName, CheckDate, 
	object_name, counter_name, instance_name, cntr_value, cntr_type, value_delta, value_per_second
from DBAtools.dbo.BlitzFirst_PerfmonStats
order by CheckDate

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats

	select top 1000 ServerName, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats
	group by ServerName
	order by ServerName

	select top 1000 object_name, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats
	group by object_name
	order by object_name

	select top 1000 counter_name, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats
	group by counter_name
	order by counter_name

	select top 1000 object_name, counter_name, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats
	group by object_name, counter_name
	order by object_name, counter_name

	select top 1000 convert(date, CheckDate), count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_PerfmonStats
	group by convert(date, CheckDate)
	order by convert(date, CheckDate)

-- BlitzFirst_WaitStats

select top 100 ID, ServerName, CheckDate, 
	wait_type, wait_time_ms, signal_wait_time_ms, waiting_tasks_count
from DBAtools.dbo.BlitzFirst_WaitStats
order by wait_type, CheckDate

	select top 1000 count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_WaitStats

	select top 1000 ServerName, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_WaitStats
	group by ServerName
	order by ServerName

	select top 1000 wait_type, count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_WaitStats
	group by wait_type
	order by wait_type

	select top 1000 convert(date, CheckDate), count(*), min(CheckDate), max(CheckDate)
	from DBAtools.dbo.BlitzFirst_WaitStats
	group by convert(date, CheckDate)
	order by convert(date, CheckDate)

-- BlitzFirst_WaitStats_Categories

select WaitType, WaitCategory, Ignorable
from DBAtools.dbo.BlitzFirst_WaitStats_Categories
order by WaitCategory, WaitType, Ignorable

	select top 1000 count(*)
	from DBAtools.dbo.BlitzFirst_WaitStats_Categories

	select top 1000 WaitCategory, count(*), count(distinct WaitType)
	from DBAtools.dbo.BlitzFirst_WaitStats_Categories
	group by WaitCategory
	order by WaitCategory