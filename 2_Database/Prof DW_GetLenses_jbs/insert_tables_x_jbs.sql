Use DW_GetLenses_jbs
go 

-- Order Headers
insert into DW_GetLenses_jbs.dbo.order_headers
	select *
	from DW_GetLenses.dbo.order_headers
	where document_date > getutcdate() - 30;

-- Order Lines
insert into DW_GetLenses_jbs.dbo.order_lines
	select *
	from DW_GetLenses.dbo.order_lines
	where document_date > getutcdate() - 30;


-- Invoice Headers
insert into DW_GetLenses_jbs.dbo.invoice_headers
	select *
	from DW_GetLenses.dbo.invoice_headers
	where document_date > getutcdate() - 30;

-- Invoice Lines
insert into DW_GetLenses_jbs.dbo.invoice_lines
	select *
	from DW_GetLenses.dbo.invoice_lines
	where document_date > getutcdate() - 30;


-- Shipment Headers
insert into DW_GetLenses_jbs.dbo.shipment_headers
	select *
	from DW_GetLenses.dbo.shipment_headers
	where document_date > getutcdate() - 30;

-- Shipment Lines
insert into DW_GetLenses_jbs.dbo.shipment_lines
	select *
	from DW_GetLenses.dbo.shipment_lines
	where document_date > getutcdate() - 30;


