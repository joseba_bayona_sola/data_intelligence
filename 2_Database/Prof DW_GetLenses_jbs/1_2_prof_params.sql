
select -- top 100 *
	op.qty, count(*)
from 
		DW_Sales_Actual.dbo.dim_order_parameters op
	inner join
		DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
	inner join
		DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
where 
	-- c.category = 'Dailies'
	-- c.category = 'Two Weeklies'
	-- c.category = 'Monthlies'
	-- c.category = 'Torics for Astigmatism'
	-- c.category = 'Multifocals'
	-- c.category = 'Colours'
	-- p.product_type = 'Solutions & Eye Care'
	p.product_type = 'Sunglasses'
group by op.qty
order by op.qty

------------------------------------------------------------

select 
	case when (t1.product_type is not null) then t1.product_type else t2.product_type end product_type, 
	num_not_null, num_null
from
		(select p.product_type, count(*) num_not_null
		from 
				DW_Sales_Actual.dbo.dim_order_parameters op
			inner join
				DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
			inner join
				DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
		where 
			-- op.lens_base_curve <> ''
			-- op.lens_diameter <> ''
			-- op.lens_power <> ''
			op.lens_cylinder <> ''
			-- op.lens_axis <> ''
			-- op.lens_addition <> ''
			-- op.lens_dominance <> ''
			-- op.lens_colour <> ''
		group by p.product_type) t1
	full join
		(select p.product_type, count(*) num_null
		from 
				DW_Sales_Actual.dbo.dim_order_parameters op
			inner join
				DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
			inner join
				DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
		where 
			-- op.lens_base_curve = ''
			-- op.lens_diameter = ''
			-- op.lens_power = ''
			op.lens_cylinder = ''
			-- op.lens_axis = ''
			-- op.lens_addition = ''
			-- op.lens_dominance = ''
			-- op.lens_colour = ''
		group by p.product_type) t2 on t1.product_type = t2.product_type
order by product_type

----------------------------------------------------------------------

select 
	case when (t1.product_type is not null) then t1.product_type else t2.product_type end product_type, 
	case when (t1.category is not null) then t1.category else t2.category end category, 
	num_not_null, num_null
from
		(select p.product_type, c.category,
			count(*) num_not_null
		from 
				DW_Sales_Actual.dbo.dim_order_parameters op
			inner join
				DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
			inner join
				DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
		where 
			-- op.lens_base_curve <> ''
			-- op.lens_diameter <> ''
			-- op.lens_power <> ''
			-- op.lens_cylinder <> ''
			-- op.lens_axis <> ''
			op.lens_addition <> ''
			-- op.lens_dominance <> ''
			-- op.lens_colour <> ''
		group by p.product_type, c.category) t1
	full join
		(select p.product_type, c.category,
			count(*) num_null
		from 
				DW_Sales_Actual.dbo.dim_order_parameters op
			inner join
				DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
			inner join
				DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
		where 
			-- op.lens_base_curve = ''
			-- op.lens_diameter = ''
			-- op.lens_power = ''
			-- op.lens_cylinder = ''
			-- op.lens_axis = ''
			op.lens_addition = ''
			-- op.lens_dominance = ''
			-- op.lens_colour = ''
		group by p.product_type, c.category) t2 on t1.product_type = t2.product_type and t1.category = t2.category 
order by product_type, category

----------------------------------------------------------------------

	select p.product_type, c.category, op.product_id, op.name, count(*)
	from 
			DW_Sales_Actual.dbo.dim_order_parameters op
		inner join
			DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
		inner join
			DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
	where 
		-- op.lens_base_curve <> ''
		-- op.lens_diameter <> ''
		-- op.lens_power <> ''
		-- op.lens_cylinder <> ''
		-- op.lens_axis <> ''
		op.lens_addition <> ''
		-- op.lens_dominance <> ''
		-- op.lens_colour <> ''
	group by p.product_type, c.category, op.product_id, op.name
	order by p.product_type, c.category, op.product_id, op.name

