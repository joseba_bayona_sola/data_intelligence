
select top 100 order_id, order_no, document_id, document_type, 
	customer_id, 
	order_lifecycle
from DW_GetLenses.dbo.order_headers

select top 100 order_id, order_no, document_id, document_type, 
	customer_id, 
	order_lifecycle
from DW_Proforma.dbo.order_headers

----------------------------------------

	select top 100 *
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle
		from DW_Proforma.dbo.order_headers) oh

	select top 100 count(*) over (), customer_id, order_lifecycle, count(*)
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle
		from DW_Proforma.dbo.order_headers) oh
		group by customer_id, order_lifecycle
		order by customer_id, order_lifecycle

	---------------------------------------------------------------------------------------------

	-- Regular Customers with Not New Order Record -- 52042 (4, 1646010) !!!
	select top 100 count(*) over () num, min(oh_r.customer_id) over () min_cust_id, max(oh_r.customer_id) over () max_cust_id, 
		oh_r.customer_id, oh_r.order_lifecycle, oh_r.num_rows
	from
			(select *
			from
				(select customer_id, order_lifecycle, count(*) num_rows
				from
					(select order_id, order_no, document_id, document_type, 
						customer_id, 
						order_lifecycle
					from DW_GetLenses.dbo.order_headers
					union
					select order_id, order_no, document_id, document_type, 
						customer_id, 
						order_lifecycle
					from DW_Proforma.dbo.order_headers) oh
					group by customer_id, order_lifecycle) t
			where order_lifecycle = 'Regular') oh_r
		left join
			(select *
			from
				(select customer_id, order_lifecycle, count(*) num_rows
				from
					(select order_id, order_no, document_id, document_type, 
						customer_id, 
						order_lifecycle
					from DW_GetLenses.dbo.order_headers
					union
					select order_id, order_no, document_id, document_type, 
						customer_id, 
						order_lifecycle
					from DW_Proforma.dbo.order_headers) oh
					group by customer_id, order_lifecycle) t
			where order_lifecycle = 'New') oh_n on oh_r.customer_id = oh_n.customer_id
	where oh_n.customer_id is null
	order by oh_r.customer_id, oh_r.order_lifecycle

	---------------------------------------------------------------------------------------------

	-- Customers with more than one 'New' record

	select top 1000 customer_id, order_lifecycle, num_rows
	from
		(select customer_id, order_lifecycle, count(*) num_rows
		from
			(select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle
			from DW_Proforma.dbo.order_headers) oh
			group by customer_id, order_lifecycle) oh
	where order_lifecycle = 'New'
	order by customer_id, order_lifecycle

	select order_lifecycle, num_rows, count(*) -- OK:925.830 - Wrong: (2: 34886, 3:22577, 4:508, 5:71, 6:2, 7:2, 9:3) Total: 58.049 !!!
	from
		(select customer_id, order_lifecycle, count(*) num_rows
		from
			(select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle
			from DW_Proforma.dbo.order_headers) oh
			group by customer_id, order_lifecycle) oh
	where order_lifecycle = 'New'
	group by order_lifecycle, num_rows
	order by order_lifecycle, num_rows

		-- 4:508, 5:71, 6:2, 7:2, 9:3 Cases !!!
		select t.num_rows,
			count(*) over (partition by oh.customer_id, oh.document_type) num_rep_doc_type,
			oh.*
		from 
				(select customer_id, order_lifecycle, num_rows
				from
					(select customer_id, order_lifecycle, count(*) num_rows
					from
						(select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_GetLenses.dbo.order_headers
						union
						select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_Proforma.dbo.order_headers) oh
						group by customer_id, order_lifecycle) oh
				where order_lifecycle = 'New' and num_rows > 3) t
			inner join 
				(select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_Proforma.dbo.order_headers) oh on t.customer_id = oh.customer_id and t.order_lifecycle = oh.order_lifecycle
		order by t.num_rows, oh.customer_id, oh.order_id, oh.order_date, oh.document_date;

		-- 2: 34886, 3:22577 Cases
		select top 1000 t.num_rows,
			count(*) over () num_tot,
			count(*) over (partition by oh.customer_id, oh.order_id) num_rep_orders,
			count(*) over (partition by oh.customer_id, oh.document_type) num_rep_doc_type,
			oh.*
		from 
				(select customer_id, order_lifecycle, num_rows
				from
					(select customer_id, order_lifecycle, count(*) num_rows
					from
						(select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_GetLenses.dbo.order_headers
						union
						select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_Proforma.dbo.order_headers) oh
						group by customer_id, order_lifecycle) oh
				where order_lifecycle = 'New' and num_rows in (2, 3)) t
			inner join 
				(select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_Proforma.dbo.order_headers) oh on t.customer_id = oh.customer_id and t.order_lifecycle = oh.order_lifecycle
		order by t.num_rows, oh.customer_id, oh.order_id, oh.order_date, oh.document_date;

		select t.num_rows, oh.document_type, count(*) num_tot -- (2: 34952 - 34820 (27194, 7626) !!!, 3: 44925 - 22806 (8769, 14037) !!!
		from 
				(select customer_id, order_lifecycle, num_rows
				from
					(select customer_id, order_lifecycle, count(*) num_rows
					from
						(select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_GetLenses.dbo.order_headers
						union
						select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle
						from DW_Proforma.dbo.order_headers) oh
						group by customer_id, order_lifecycle) oh
				where order_lifecycle = 'New' and num_rows in (2, 3)) t
			inner join 
				(select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle
				from DW_Proforma.dbo.order_headers) oh on t.customer_id = oh.customer_id and t.order_lifecycle = oh.order_lifecycle
		group by t.num_rows, oh.document_type
		order by t.num_rows, oh.document_type;
		
			select top 1000												-- (2: 132 !!!, 3: 45118)
				count(*) over () num_tot,
				count(*) over (partition by num_rows) num_tot_num_rows,
				*
			from
				(select t.num_rows,
					count(*) over () num_tot,
					count(*) over (partition by oh.customer_id, oh.order_id) num_rep_orders,
					count(*) over (partition by oh.customer_id, oh.document_type) num_rep_doc_type,
					oh.*
				from 
						(select customer_id, order_lifecycle, num_rows
						from
							(select customer_id, order_lifecycle, count(*) num_rows
							from
								(select order_id, order_no, document_id, document_type, 
									customer_id, 
									order_lifecycle
								from DW_GetLenses.dbo.order_headers
								union
								select order_id, order_no, document_id, document_type, 
									customer_id, 
									order_lifecycle
								from DW_Proforma.dbo.order_headers) oh
								group by customer_id, order_lifecycle) oh
						where order_lifecycle = 'New' and num_rows in (2, 3)) t
					inner join 
						(select order_id, order_no, document_id, document_type, 
							order_date, document_date,
							customer_id, 
							order_lifecycle
						from DW_GetLenses.dbo.order_headers
						union
						select order_id, order_no, document_id, document_type, 
							order_date, document_date,
							customer_id, 
							order_lifecycle
						from DW_Proforma.dbo.order_headers) oh on t.customer_id = oh.customer_id and t.order_lifecycle = oh.order_lifecycle) t
			where num_rep_doc_type > 1
			order by num_rows, customer_id, order_id, order_date, document_date;
	