
-- Order Headers with NO Order Lines
select top 100 oh.document_id, oh.document_type, oh.document_date, 
	count(ol.line_id) num
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	left join 
		DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where ol.line_id is null
group by oh.document_id, oh.document_type, oh.document_date

-- Order Lines with NO Order Headers
select top 100 ol.line_id, ol.document_id, ol.document_type, count(oh.order_id) num
from 
		DW_GetLenses_jbs.dbo.order_lines ol 
	left join 
		DW_GetLenses_jbs.dbo.order_headers oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
where oh.document_id is null
group by ol.line_id, ol.document_id, ol.document_type

---------------------------------------------------------------------------------

-- Order Headers, Lines relation depending on number of Lines per Order
select top 100 oh.document_id, oh.document_type, count(ol.line_id) num
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	inner join 
		DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
group by oh.document_id, oh.document_type
order by num desc;

select *
from DW_GetLenses_jbs.dbo.order_headers 
where 
	(document_id = 4452505 and document_type = 'ORDER') or (document_id = 1242787 and document_type = 'CREDITMEMO') 
	--or (document_id = 742681 and document_type = 'ORDER') or (document_id = 1202127 and document_type = 'CREDITMEMO') 
order by order_no

select *
from DW_GetLenses_jbs.dbo.order_lines
where 
	(document_id = 4452505 and document_type = 'ORDER') or (document_id = 1242787 and document_type = 'CREDITMEMO') 
	--or (document_id = 742681 and document_type = 'ORDER') or (document_id = 1202127 and document_type = 'CREDITMEMO') 
order by order_no, product_id



-- Order Headers, Lines relation depending on number of Orders per Line
select top 100 ol.line_id, ol.document_id, ol.document_type, count(oh.order_id) num
from 
		DW_GetLenses_jbs.dbo.order_lines ol 
	inner join 
		DW_GetLenses_jbs.dbo.order_headers oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
group by ol.line_id, ol.document_id, ol.document_type
order by num desc;
