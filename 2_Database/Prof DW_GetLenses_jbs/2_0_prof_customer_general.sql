
select top 100 *
from DW_GetLenses.dbo.customers

select top 100 *
from DW_Proforma.dbo.customers


	select count(*) -- 1.429.993
	from DW_GetLenses.dbo.customers

	select count(*) -- 1018
	from DW_Proforma.dbo.customers

	select count(*) -- 1.431.011 !!!
	from
		(select customer_id
		from DW_GetLenses.dbo.customers
		union
		select customer_id
		from DW_Proforma.dbo.customers) c

	select count(*) -- 0
	from
		(select customer_id
		from DW_GetLenses.dbo.customers
		intersect
		select customer_id
		from DW_Proforma.dbo.customers) c

	

	------------------------------------------

	select YEAR(first_order_date), count(*)
	from DW_GetLenses.dbo.customers
	group by YEAR(first_order_date)
	order by YEAR(first_order_date)

	select YEAR(first_order_date), MONTH(first_order_date), count(*)
	from DW_GetLenses.dbo.customers
	group by YEAR(first_order_date), MONTH(first_order_date)
	order by YEAR(first_order_date), MONTH(first_order_date)

	select num_of_orders, count(*)
	from DW_GetLenses.dbo.customers
	group by num_of_orders
	order by num_of_orders
