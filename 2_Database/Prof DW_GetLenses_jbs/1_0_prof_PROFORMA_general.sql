

select top 1000 *
from DW_Proforma.dbo.order_headers

select store_name, count(*)
from DW_Proforma.dbo.order_headers
group by store_name
order by store_name

select convert(date, document_date), count(*)
from DW_Proforma.dbo.order_headers
group by convert(date, document_date)
order by convert(date, document_date) desc

	select year(convert(date, document_date)), count(*)
	from DW_Proforma.dbo.order_headers
	group by year(convert(date, document_date))
	order by year(convert(date, document_date)) desc

	select 
		year(convert(date, document_date)), month(convert(date, document_date)),
		count(*)
	from DW_Proforma.dbo.order_headers
	group by year(convert(date, document_date)), month(convert(date, document_date))
	order by year(convert(date, document_date)) desc, month(convert(date, document_date))

--------------------------------------------------------------------------------------------

select document_type, count(*)
from DW_Proforma.dbo.order_headers
group by document_type
order by document_type

--------------------------------------------------------------------------------------------

select customer_id, count(*)
from DW_Proforma.dbo.order_headers
group by customer_id
order by customer_id
