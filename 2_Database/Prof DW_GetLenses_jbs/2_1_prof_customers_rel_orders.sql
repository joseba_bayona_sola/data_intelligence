
select top 100 *
from DW_GetLenses.dbo.order_headers

select top 100 *
from DW_Proforma.dbo.order_headers

	select count(*) -- 3.077.786
	from DW_GetLenses.dbo.order_headers

	select count(*) -- 1.523.839
	from DW_Proforma.dbo.order_headers


	select count(distinct customer_id) -- 819.146
	from DW_GetLenses.dbo.order_headers

	select count(distinct customer_id) -- 376.055
	from DW_Proforma.dbo.order_headers

	select count(distinct customer_id) -- 1.038.181 !!! (Less than Total Cust)
	from
		(select customer_id
		from DW_GetLenses.dbo.order_headers
		union
		select customer_id
		from DW_Proforma.dbo.order_headers) t

	select count(distinct customer_id) -- 157.020 !!!
	from
		(select customer_id
		from DW_GetLenses.dbo.order_headers
		intersect
		select customer_id
		from DW_Proforma.dbo.order_headers) t

	---------------------------------------------------

	select oh.customer_id -- 0 !!!
	from 
			(select customer_id
			from
				(select customer_id
				from DW_GetLenses.dbo.order_headers
				union
				select customer_id
				from DW_Proforma.dbo.order_headers) t) oh
		left join 
			(select customer_id
			from
				(select customer_id
				from DW_GetLenses.dbo.customers
				union
				select customer_id
				from DW_Proforma.dbo.customers) c) c on oh.customer_id = c.customer_id
	where c.customer_id is null
	order by c.customer_id 