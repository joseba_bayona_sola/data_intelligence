
use DW_GetLenses_jbs
go 

-- Order Headers
select top 100 *
from DW_GetLenses_jbs.dbo.order_headers;

-- Order Lines
select top 100 *
from DW_GetLenses_jbs.dbo.order_lines;


-- Invoice Headers
select top 100 *
from DW_GetLenses_jbs.dbo.invoice_headers;

-- Invoice Lines
select top 100 *
from DW_GetLenses_jbs.dbo.invoice_lines;


-- Shipment Headers
select top 100 *
from DW_GetLenses_jbs.dbo.shipment_headers;

-- Shipment Lines
select top 100 *
from DW_GetLenses_jbs.dbo.shipment_lines;

