
-- Different # rows per Business Channel records
select business_channel, count(*) num
from DW_GetLenses_jbs.dbo.order_headers
group by business_channel
order by num desc, business_channel;

-- Different # rows per Lifecycle - Business Channel records
select order_lifecycle, business_channel, count(*) num
from DW_GetLenses_jbs.dbo.order_headers
group by order_lifecycle, business_channel
order by order_lifecycle, num desc, business_channel;

