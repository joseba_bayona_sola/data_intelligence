
-- PK document_id, document_type-- NO
select top 1000 count(*) over() num_total, *
from 
	(select top 100 count(*) over (partition by document_id, document_type) num, *
	from DW_GetLenses_jbs.dbo.order_lines ol) t
where num > 1
	--and store_name = 'visiondirect.co.uk'
order by num, document_id desc, document_type

-- PK document_id, document_type, line_id --> YES
select top 100 document_id, document_type, line_id, count(*) num
from DW_GetLenses_jbs.dbo.order_lines 
group by document_id, document_type, line_id
order by num desc

-- PK document_id, document_type, product_id --> NO
select top 100 document_id, document_type, product_id, count(*) num
from DW_GetLenses_jbs.dbo.order_lines 
group by document_id, document_type, product_id
order by num desc

select *
from DW_GetLenses_jbs.dbo.order_headers
where 
	(document_id = 4468794) or 
	(document_id = 4518816) 
order by document_id, document_type

select *
from DW_GetLenses_jbs.dbo.order_lines 
where 
	(document_id = 4468794) or 
	(document_id = 4518816) 
order by document_id, document_type, line_id

-- Line_id PK NO
select top 100 line_id, count(*) num
from DW_GetLenses_jbs.dbo.order_lines 
group by line_id
order by num desc

select *
from DW_GetLenses_jbs.dbo.order_lines 
where line_id in (8892715, 8996699, 8962278, 8896459)
order by line_id