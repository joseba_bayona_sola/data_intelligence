
	-- Number of REPEATED O. Headers per # of repetition
	select num_rep, count(*)
	from
		(select order_id, document_type, 
			count(*) over () num_tot,
			count(*) over (partition by order_id) num_rep
		from DW_GetLenses_jbs.dbo.order_headers) t
	group by num_rep
	order by num_rep;

	select document_type, count(*)
	from
		(select order_id, document_type, 
			count(*) over () num_tot,
			count(*) over (partition by order_id) num_rep
		from DW_GetLenses_jbs.dbo.order_headers) t
	where num_rep = 1
	group by document_type
	order by document_type;

	select document_type, count(*)
	from
		(select order_id, document_type, 
			count(*) over () num_tot,
			count(*) over (partition by order_id) num_rep
		from DW_GetLenses_jbs.dbo.order_headers) t
	where num_rep = 2
	group by document_type
	order by document_type;

	select document_type, count(*)
	from
		(select order_id, document_type, 
			count(*) over () num_tot,
			count(*) over (partition by order_id) num_rep
		from DW_GetLenses_jbs.dbo.order_headers) t
	where num_rep = 3
	group by document_type
	order by document_type;

