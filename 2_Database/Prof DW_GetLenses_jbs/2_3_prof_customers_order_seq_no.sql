
select top 100 order_id, order_no, document_id, document_type, 
	customer_id, 
	order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.order_headers

select top 100 order_id, order_no, document_id, document_type, 
	customer_id, 
	order_lifecycle, customer_order_seq_no
from DW_Proforma.dbo.order_headers

----------------------------------------

	select top 100 *
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh

	select top 100 count(*) over (), customer_id, customer_order_seq_no, count(*)
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh
	group by customer_id, customer_order_seq_no
	order by customer_id, customer_order_seq_no


	-- Customers - cust_ord_seq: 
		-- ALL: OH Records 4.601.625 / All Cust - Seq Combination Records: 4.256.061 / Diff: 375k
		-- NOT NULL: OH Records 4.479.236 / All Cust - Seq Combination Records: 4.176.331 / Diff: 302.905
	select top 100 count(*) over () num_rows, sum(count(*)) over () sum_rows,
		customer_id, customer_order_seq_no, count(*) -- 79.730
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh
	--where customer_order_seq_no is not null
	group by customer_id, customer_order_seq_no
	order by customer_id, customer_order_seq_no

	---------------------------------------------------------------------------------------------
	-- NULL
	-- Repeated
	-- Correct Order of Seq --> Missing Correct Order Seq
	-- Total Numbers per Customer depending on reliability

	---------------------------------------------------------------------------------------------

	-- Customers with cust_ord_seq equal to null -- 79.730 cust, -- 122.389 order rows
	select top 100 count(*) over () num_rows, sum(count(*)) over () sum_rows,
		customer_id, customer_order_seq_no, count(*) -- 79.730
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh
	where customer_order_seq_no is null
	group by customer_id, customer_order_seq_no
	order by customer_id, customer_order_seq_no

	select db, document_type, count(*), 
		min(order_date), max(order_date)
	from
		(select 'GL' db, order_id, order_no, document_id, document_type, 
			order_date, document_date,
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select 'P' db, order_id, order_no, document_id, document_type, 
			order_date, document_date,
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh
	where customer_order_seq_no is null
	group by db, document_type
	order by db, document_type;




	-- Customers with repeated cust_ord_seq -- 188.287 cust, -- 491.192 order rows
	select top 1000 count(*) over () num_rows, sum(num) over () sum_rows,
		customer_id, customer_order_seq_no, num, 
		count(*) over (partition by customer_order_seq_no, num) num_rep_comb
	from 
		(select 
			customer_id, customer_order_seq_no, count(*) num
		from
			(select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_Proforma.dbo.order_headers) oh
		where customer_order_seq_no is not null
		group by customer_id, customer_order_seq_no) t
	where t.num > 1
	order by customer_id, customer_order_seq_no

		-- Distribution of rows depending on customer_order_seq_no and num of repeated times
		select top 1000 customer_order_seq_no, num, count(*)
		from 
			(select 
				customer_id, customer_order_seq_no, count(*) num
			from
				(select order_id, order_no, document_id, document_type, 
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_Proforma.dbo.order_headers) oh
			where customer_order_seq_no is not null
			group by customer_id, customer_order_seq_no) t
		where t.num > 1
		group by customer_order_seq_no, num
		having count(*) > 99
		order by customer_order_seq_no, num

		-- Customers with repeated customer_order_seq_no and rest of cases
		select top 1000 --c.num,
			--count(*) over (partition by oh.customer_id, oh.document_type) num_rep_doc_type,
			count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) num_rep,
			oh.*
		from 
				(select 
					distinct customer_id--, 
					-- customer_order_seq_no, num, 
					-- count(*) over (partition by customer_order_seq_no, num) num_rep_comb
				from 
					(select 
						customer_id, customer_order_seq_no, count(*) num
					from
						(select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle, customer_order_seq_no
						from DW_GetLenses.dbo.order_headers
						union
						select order_id, order_no, document_id, document_type, 
							customer_id, 
							order_lifecycle, customer_order_seq_no
						from DW_Proforma.dbo.order_headers) oh
					where customer_order_seq_no is not null
					group by customer_id, customer_order_seq_no) t
				where t.num > 1) c
			inner join 
				(select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					order_date, document_date,
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_Proforma.dbo.order_headers) oh on c.customer_id = oh.customer_id --and c.customer_order_seq_no = oh.customer_order_seq_no
		order by --c.num,	
			oh.customer_id, oh.customer_order_seq_no, oh.order_id, oh.order_date, oh.document_date;

		select document_type, count(*)
		from 
			(select --c.num,
				--count(*) over (partition by oh.customer_id, oh.document_type) num_rep_doc_type,
				count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) num_rep,
				oh.*
			from 
					(select 
						distinct customer_id--, 
						-- customer_order_seq_no, num, 
						-- count(*) over (partition by customer_order_seq_no, num) num_rep_comb
					from 
						(select 
							customer_id, customer_order_seq_no, count(*) num
						from
							(select order_id, order_no, document_id, document_type, 
								customer_id, 
								order_lifecycle, customer_order_seq_no
							from DW_GetLenses.dbo.order_headers
							union
							select order_id, order_no, document_id, document_type, 
								customer_id, 
								order_lifecycle, customer_order_seq_no
							from DW_Proforma.dbo.order_headers) oh
						where customer_order_seq_no is not null
						group by customer_id, customer_order_seq_no) t
					where t.num > 1) c
				inner join 
					(select order_id, order_no, document_id, document_type, 
						order_date, document_date,
						customer_id, 
						order_lifecycle, customer_order_seq_no
					from DW_GetLenses.dbo.order_headers
					union
					select order_id, order_no, document_id, document_type, 
						order_date, document_date,
						customer_id, 
						order_lifecycle, customer_order_seq_no
					from DW_Proforma.dbo.order_headers) oh on c.customer_id = oh.customer_id --and c.customer_order_seq_no = oh.customer_order_seq_no
					) t
			where num_rep > 1
			group by document_type
			order by document_type;

	--------------------------------------------------------------------------

	-- Correct Order of Seq --> Missing Correct Order Seq -- GENERAL NUMBERS (1.038.181 total cust)
	select top 1000 count(*) over (),
		customer_id, 
		min(customer_order_seq_no) min_seq, max(customer_order_seq_no) max_seq, count(distinct customer_order_seq_no) num_ord, 
		max(customer_order_seq_no) - min(customer_order_seq_no) + 1 diff_seq
	from 
		(select order_id, order_no, document_id, document_type, 
			order_date, document_date,
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			order_date, document_date,
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh 
	group by customer_id
	order by customer_id

	select min_seq, count(*) -- 971.395 start with ONE / 66.786 NO
	from 		
		(select 
			customer_id, 
			min(customer_order_seq_no) min_seq, max(customer_order_seq_no) max_seq, count(distinct customer_order_seq_no) num_ord, 
			max(customer_order_seq_no) - min(customer_order_seq_no) + 1 diff_seq
		from 
			(select order_id, order_no, document_id, document_type, 
				order_date, document_date,
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				order_date, document_date,
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_Proforma.dbo.order_headers) oh 
		group by customer_id) t
	group by min_seq
	order by min_seq

	select num_ord, diff_seq, count(*) -- Equal 1.016.966, Diff = 6266
	from 		
		(select 
			customer_id, 
			min(customer_order_seq_no) min_seq, max(customer_order_seq_no) max_seq, count(distinct customer_order_seq_no) num_ord, 
			max(customer_order_seq_no) - min(customer_order_seq_no) + 1 diff_seq
		from 
			(select order_id, order_no, document_id, document_type, 
				order_date, document_date,
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				order_date, document_date,
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_Proforma.dbo.order_headers) oh 
		group by customer_id) t
	where num_ord <> diff_seq
	group by num_ord, diff_seq
	order by num_ord, diff_seq

	--------------------------------------------------------------------------

	-- Correct Order of Seq --> Missing Correct Order Seq
	select top 100 customer_id, customer_order_seq_no, count(*) 
	from
		(select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		union
		select order_id, order_no, document_id, document_type, 
			customer_id, 
			order_lifecycle, customer_order_seq_no
		from DW_Proforma.dbo.order_headers) oh
	where customer_order_seq_no is not null
	group by customer_id, customer_order_seq_no
	order by customer_id, customer_order_seq_no

	select customer_id, customer_order_seq_no, 
		isnull(lead(customer_order_seq_no) over (partition by customer_id order by customer_order_seq_no), customer_order_seq_no) next_customer_order_seq_no,
		customer_order_seq_no - 
			isnull(lead(customer_order_seq_no) over (partition by customer_id order by customer_order_seq_no), customer_order_seq_no) diff,
		num
	from
		(select top 100 customer_id, customer_order_seq_no, count(*) num
		from
			(select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			union
			select order_id, order_no, document_id, document_type, 
				customer_id, 
				order_lifecycle, customer_order_seq_no
			from DW_Proforma.dbo.order_headers) oh
		where customer_order_seq_no is not null
		group by customer_id, customer_order_seq_no) t
	order by customer_id, customer_order_seq_no

	select diff, count(*)
	from
		(select customer_id, customer_order_seq_no, 
			isnull(lead(customer_order_seq_no) over (partition by customer_id order by customer_order_seq_no), customer_order_seq_no) next_customer_order_seq_no,
			customer_order_seq_no - 
				isnull(lead(customer_order_seq_no) over (partition by customer_id order by customer_order_seq_no), customer_order_seq_no) diff,
			num
		from
			(select customer_id, customer_order_seq_no, count(*) num
			from
				(select order_id, order_no, document_id, document_type, 
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_GetLenses.dbo.order_headers
				union
				select order_id, order_no, document_id, document_type, 
					customer_id, 
					order_lifecycle, customer_order_seq_no
				from DW_Proforma.dbo.order_headers) oh
			where customer_order_seq_no is not null
			group by customer_id, customer_order_seq_no) t) t
	where num <> 1
	group by diff
	order by diff
