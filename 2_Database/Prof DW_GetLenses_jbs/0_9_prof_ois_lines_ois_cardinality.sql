
select 
	dense_rank() over (order by order_id) num_order,
	dense_rank() over (partition by order_id order by document_date) num_row_dt,	
	rank() over (partition by order_id, document_type order by line_id) num_row_l,	
	line_id, document_id, document_type, document_date,
	order_id, order_no, order_date, 
	store_name, 
	product_type, category_id, product_id, sku, name
from DW_GetLenses_jbs.dbo.order_lines
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, line_id

select 
	dense_rank() over (order by order_id) num_order,
	dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
	rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
	line_id, il.document_id, il.document_type, il.document_date,
	order_id, order_no, order_date, 
	il.store_name, 
	product_type, category_id, product_id, sku, name
from 
		DW_GetLenses_jbs.dbo.invoice_lines il
	inner join 
		DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, line_id

select 
	dense_rank() over (order by order_id) num_order,
	dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
	rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
	line_id, sl.document_id, sl.document_type, sl.document_date,
	order_id, order_no, order_date, 
	sl.store_name, 
	product_type, category_id, product_id, sku, name
from 
		DW_GetLenses_jbs.dbo.shipment_lines sl
	inner join
		DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, line_id

-----------------------------------------------------------------------------
-- FULL JOIN
select 
	ol.order_id, il.order_id, sl.order_id, 
	ol.order_no, il.order_no, sl.order_no, 
	ol.document_id, ol.document_type, ol.document_date,
	il.document_id, il.document_type, il.document_date,
	sl.document_id, sl.document_type, sl.document_date,

	il.invoice_id, il.invoice_no, sl.shipment_id, sl.shipment_no,
	ol.order_date, il.invoice_date, sl.shipment_date,
	ol.store_name, il.store_name, sl.store_name, 

	ol.product_type, il.product_type, sl.product_type, 
	ol.product_id, il.product_id, sl.product_id, 
	ol.sku, il.sku, sl.sku, 
	ol.name, il.name, sl.name
from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l,	
			line_id, document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name, 
			product_type, category_id, product_id, sku, name
		from DW_GetLenses_jbs.dbo.order_lines
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) ol
	full join
		(select 
			dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
			rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
			line_id, il.document_id, il.document_type, il.document_date,
			ih.order_id, ih.order_no, ih.order_date, ih.invoice_id, ih.invoice_no, ih.invoice_date,
			il.store_name, 
			product_type, category_id, product_id, sku, name
		from 
				DW_GetLenses_jbs.dbo.invoice_lines il
			inner join 
				DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) il on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
	full join
		(select 
			dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
			rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
			line_id, sl.document_id, sl.document_type, sl.document_date,
			sh.order_id, sh.order_no, sh.order_date, sh.shipment_id, sh.shipment_no, sh.shipment_date,
			sl.store_name, 
			product_type, category_id, product_id, sku, name
		from 
				DW_GetLenses_jbs.dbo.shipment_lines sl
			inner join
				DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) sl on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
order by ol.order_id, ol.line_id

-----------------------------------------------------------------

-- Order x Invoice Relationship in Lines - Missing Records

select 
	--count(distinct ol.order_id) over () tot_oh, count(distinct il.order_id) over () tot_ih, 
	count(ol.order_id) over () tot_ol, count(il.order_id) over () tot_il, 
	dense_rank() over (order by ol.order_id, ol.document_date, ol.line_id) rank_ol, dense_rank() over (order by il.order_id, il.document_date, il.line_id) rank_il,
	ol.order_id, il.order_id, ol.order_no, il.order_no, ol.num_row_dt, ol.num_row_l,
	ol.document_id, ol.document_type, ol.document_date,
	il.document_id, il.document_type, il.document_date,

	il.invoice_id, il.invoice_no, il.num_row_dt, il.num_row_l
from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l,	
			line_id, document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name, 
			product_type, category_id, product_id, sku, name
		from DW_GetLenses_jbs.dbo.order_lines
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) ol
	full join
		(select 
			dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
			rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
			line_id, il.document_id, il.document_type, il.document_date,
			ih.order_id, ih.order_no, ih.order_date, ih.invoice_id, ih.invoice_no, ih.invoice_date,
			il.store_name, 
			product_type, category_id, product_id, sku, name
		from 
				DW_GetLenses_jbs.dbo.invoice_lines il
			inner join 
				DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) il on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
where 
	ol.order_id is null or il.order_id is null
order by ol.order_id, ol.document_date, ol.line_id, il.order_id, il.document_date, il.line_id

	select ol.document_type, il.document_type, count(*)
	from 
			(select 
				dense_rank() over (partition by order_id order by document_date) num_row_dt,	
				rank() over (partition by order_id, document_type order by line_id) num_row_l, *	
			from DW_GetLenses_jbs.dbo.order_lines) ol
		full join
			(select 
				dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
				rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
				ih.order_id, ih.order_no, ih.order_date, il.*
			from 
					DW_GetLenses_jbs.dbo.invoice_lines il
				inner join 
					DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type) il 
						on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
	where 
		ol.order_id is null or il.order_id is null
	group by ol.document_type, il.document_type 
	order by ol.document_type, il.document_type

-- Order x Shipment Relationship in Lines - Missing Records
select 
	--count(distinct ol.order_id) over () tot_oh, count(distinct il.order_id) over () tot_ih, 
	count(ol.order_id) over () tot_ol, count(sl.order_id) over () tot_sl, 
	dense_rank() over (order by ol.order_id, ol.document_date, ol.line_id) rank_ol, dense_rank() over (order by sl.order_id, sl.document_date, sl.line_id) rank_sl,
	ol.order_id, sl.order_id, ol.order_no, sl.order_no, ol.num_row_dt, ol.num_row_l,
	ol.document_id, ol.document_type, ol.document_date,
	sl.document_id, sl.document_type, sl.document_date,

	sl.shipment_id, sl.shipment_no, sl.num_row_dt, sl.num_row_l
from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l,	
			line_id, document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name, 
			product_type, category_id, product_id, sku, name
		from DW_GetLenses_jbs.dbo.order_lines
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) ol
	full join
		(select 
			dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
			rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
			line_id, sl.document_id, sl.document_type, sl.document_date,
			sh.order_id, sh.order_no, sh.order_date, sh.shipment_id, sh.shipment_no, sh.shipment_date,
			sl.store_name, 
			product_type, category_id, product_id, sku, name
		from 
				DW_GetLenses_jbs.dbo.shipment_lines sl
			inner join
				DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) sl 
		) sl on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
where 
	ol.order_id is null or sl.order_id is null
order by ol.order_id, ol.document_date, ol.line_id, sl.order_id, sl.document_date, sl.line_id

	select ol.document_type, sl.document_type, count(*)	
	from 
			(select 
				dense_rank() over (partition by order_id order by document_date) num_row_dt,	
				rank() over (partition by order_id, document_type order by line_id) num_row_l,	*
			from DW_GetLenses_jbs.dbo.order_lines) ol
		full join
			(select 
				dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
				rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
				sh.order_id, sh.order_no, sh.order_date, sl.*
			from 
					DW_GetLenses_jbs.dbo.shipment_lines sl
				inner join
					DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type) sl 
						on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
	where ol.order_id is null or sl.order_id is null
	group by ol.document_type, sl.document_type
	order by ol.document_type, sl.document_type

-----------------------------------------------------------------
-- Matching records in Order - Invoice - Shipment but different Document Type

select ol.document_type, il.document_type, sl.document_type, count(*)
from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l, *	
		from DW_GetLenses_jbs.dbo.order_lines) ol
	inner join
		(select 
			dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
			rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
			ih.order_id, ih.order_no, ih.order_date, il.*
		from 
				DW_GetLenses_jbs.dbo.invoice_lines il
			inner join 
				DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type) il 
					on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
	inner join
		(select 
			dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
			rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
			sh.order_id, sh.order_no, sh.order_date, sl.*
		from 
				DW_GetLenses_jbs.dbo.shipment_lines sl
			inner join
				DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type) sl 
					on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
group by ol.document_type, il.document_type, sl.document_type 
order by ol.document_type, il.document_type, sl.document_type

select top 1000 
	count(*) over () num_tot,
	ol.order_id, il.order_id, sl.order_id, ol.order_no, il.order_no, sl.order_no, 
	ol.document_id, ol.document_type, ol.document_date,
	il.document_id, il.document_type, il.document_date,
	sl.document_id, sl.document_type, sl.document_date,

	il.invoice_id, il.invoice_no, sl.shipment_id, sl.shipment_no,
	ol.order_date, il.invoice_date, sl.shipment_date,
	ol.store_name, il.store_name, sl.store_name, 
	ol.product_id, il.product_id, sl.product_id, 
	ol.sku, il.sku, sl.sku, ol.name, il.name, sl.name
from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l, *	
		from DW_GetLenses_jbs.dbo.order_lines) ol
	inner join
		(select 
			dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
			rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
			ih.order_id, ih.order_no, ih.order_date, il.*
		from 
				DW_GetLenses_jbs.dbo.invoice_lines il
			inner join 
				DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type) il 
					on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
	inner join
		(select 
			dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
			rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
			sh.order_id, sh.order_no, sh.order_date, sl.*
		from 
				DW_GetLenses_jbs.dbo.shipment_lines sl
			inner join
				DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type) sl 
					on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
where 
	(ol.document_type = 'CREDITMEMO' and il.document_type = 'CREDITMEMO' and sl.document_type  = 'SHIPMENT') or
	(ol.document_type = 'CREDITMEMO' and il.document_type = 'INVOICE' and sl.document_type  = 'SHIPMENT') or
	(ol.document_type = 'ORDER' and il.document_type = 'INVOICE' and sl.document_type  = 'CREDITMEMO') 

-----------------------------------------------------------------

select top 1000 
	count(*) over () num_tot,
	ol.order_id, il.order_id, sl.order_id, ol.order_no, il.order_no, sl.order_no, 
	ol.document_id, ol.document_type, ol.document_date,
	il.document_id, il.document_type, il.document_date,
	sl.document_id, sl.document_type, sl.document_date,

	il.invoice_id, il.invoice_no, sl.shipment_id, sl.shipment_no,
	ol.order_date, il.invoice_date, sl.shipment_date,
	ol.store_name, il.store_name, sl.store_name, 
	ol.product_id, il.product_id, sl.product_id, 
	ol.sku, il.sku, sl.sku, ol.name, il.name, sl.name, 
	ol.unit_weight, il.unit_weight, sl.unit_weight, ol.line_weight, il.line_weight, sl.line_weight, 	
	ol.price_type, il.price_type, sl.price_type, 

	ol.qty, il.qty, sl.qty, 

	ol.local_prof_fee, il.local_prof_fee, sl.local_prof_fee, 
	ol.local_price_inc_vat, il.local_price_inc_vat, sl.local_price_inc_vat, ol.local_line_subtotal_inc_vat, il.local_line_subtotal_inc_vat, sl.local_line_subtotal_inc_vat, 
	ol.local_shipping_inc_vat, il.local_shipping_inc_vat, sl.local_shipping_inc_vat, ol.local_discount_inc_vat, il.local_discount_inc_vat, sl.local_discount_inc_vat, 
	ol.local_store_credit_inc_vat, il.local_store_credit_inc_vat, sl.local_store_credit_inc_vat, ol.local_adjustment_inc_vat, il.local_adjustment_inc_vat, sl.local_adjustment_inc_vat,  
	ol.local_line_total_inc_vat, il.local_line_total_inc_vat, sl.local_line_total_inc_vat, 
	ol.local_total_cost, il.local_total_cost, sl.local_total_cost, 
	ol.local_margin_amount, il.local_margin_amount, sl.local_margin_amount, 
	ol.local_to_global_rate, il.local_to_global_rate, sl.local_to_global_rate

from 
		(select 
			dense_rank() over (partition by order_id order by document_date) num_row_dt,	
			rank() over (partition by order_id, document_type order by line_id) num_row_l, *	
		from DW_GetLenses_jbs.dbo.order_lines) ol
	inner join
		(select 
			dense_rank() over (partition by order_id order by il.document_date) num_row_dt,	
			rank() over (partition by order_id, il.document_type order by line_id) num_row_l,	
			ih.order_id, ih.order_no, ih.order_date, il.*
		from 
				DW_GetLenses_jbs.dbo.invoice_lines il
			inner join 
				DW_GetLenses_jbs.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type) il 
					on ol.order_id = il.order_id and ol.num_row_dt = il.num_row_dt and ol.num_row_l = il.num_row_l
	inner join
		(select 
			dense_rank() over (partition by order_id order by sl.document_date) num_row_dt,	
			rank() over (partition by order_id, sl.document_type order by line_id) num_row_l,	
			sh.order_id, sh.order_no, sh.order_date, sl.*
		from 
				DW_GetLenses_jbs.dbo.shipment_lines sl
			inner join
				DW_GetLenses_jbs.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type) sl 
					on ol.order_id = sl.order_id and ol.num_row_dt = sl.num_row_dt and ol.num_row_l = sl.num_row_l
where 
	((ol.document_type = 'CREDITMEMO' and il.document_type = 'CREDITMEMO' and sl.document_type  = 'CREDITMEMO') or
	(ol.document_type = 'ORDER' and il.document_type = 'INVOICE' and sl.document_type  = 'SHIPMENT')) and
	not 
		((ol.product_id = il.product_id) and (ol.sku = il.sku) and
		((ol.unit_weight = il.unit_weight) or (ol.unit_weight is null or il.unit_weight is null)) --and 
		-- and (ol.line_weight = il.line_weight) -- 58
		-- and (ol.price_type = il.price_type) -- 84
		-- and (ol.qty = il.qty and ol.qty = sl.qty) -- 1199
		-- and (ol.local_price_inc_vat = il.local_price_inc_vat and ol.local_price_inc_vat = sl.local_price_inc_vat) --  1316
		-- and (ol.local_line_subtotal_inc_vat = il.local_line_subtotal_inc_vat and ol.local_line_subtotal_inc_vat = sl.local_line_subtotal_inc_vat) -- 1348
		-- and (ol.local_line_total_inc_vat = il.local_line_total_inc_vat and ol.local_line_total_inc_vat = sl.local_line_total_inc_vat) -- 1364
		-- and (ol.local_total_cost = il.local_total_cost and ol.local_total_cost = sl.local_total_cost) -- 12034
		-- and (ol.local_margin_amount = il.local_margin_amount and ol.local_margin_amount = sl.local_margin_amount) -- 12050
		-- and (ol.local_to_global_rate = il.local_to_global_rate and ol.local_to_global_rate = sl.local_to_global_rate) -- 0
		)
order by ol.order_id, ol.line_id