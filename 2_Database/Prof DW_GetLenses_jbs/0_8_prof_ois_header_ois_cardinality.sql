
select 
	dense_rank() over (order by order_id) num_order,
	rank() over (partition by order_id order by document_date) num_row,	
	document_id, document_type, document_date,
	order_id, order_no, order_date, 
	store_name,
	customer_id, customer_email
from DW_GetLenses_jbs.dbo.order_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date

select 
	dense_rank() over (order by order_id) num_order,
	rank() over (partition by order_id order by document_date) num_row,
	document_id, document_type, document_date,
	order_id, order_no, order_date, 
	invoice_id, invoice_no, invoice_date, 
	store_name,
	customer_id, customer_email
from DW_GetLenses_jbs.dbo.invoice_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date

select 
	dense_rank() over (order by order_id) num_order,
	rank() over (partition by order_id order by document_date) num_row,
	document_id, document_type, document_date,
	order_id, order_no, order_date, 
	invoice_id, invoice_no, invoice_date, 
	shipment_id, shipment_no, shipment_date,
	store_name,
	customer_id, customer_email
from DW_GetLenses_jbs.dbo.shipment_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date

-----------------------------------------------------------------------------
-- FULL JOIN
select 
	oh.order_id, ih.order_id, sh.order_id, oh.order_no, ih.order_no, sh.order_no, 
	oh.document_id, oh.document_type, oh.document_date,
	ih.document_id, ih.document_type, ih.document_date,
	sh.document_id, sh.document_type, sh.document_date,

	ih.invoice_id, ih.invoice_no, sh.shipment_id, sh.shipment_no,
	oh.order_date, ih.invoice_date, sh.shipment_date,
	oh.store_name, ih.store_name, sh.store_name, 
	oh.customer_id, ih.customer_id, sh.customer_id, oh.customer_email, ih.customer_email, sh.customer_email
from 
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.order_headers
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) oh
	full join
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			invoice_id, invoice_no, invoice_date, 
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.invoice_headers
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
	full join
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			invoice_id, invoice_no, invoice_date, 
			shipment_id, shipment_no, shipment_date,
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.shipment_headers
		where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
order by oh.order_id, oh.document_date


-----------------------------------------------------------------

-- Order x Invoice Relationship - Missing Records (3016)
select 
	count(oh.order_id) over () tot_oh, count(ih.order_id) over () tot_ih, 
	dense_rank() over (order by oh.order_id, oh.document_date) rank_oh, dense_rank() over (order by ih.order_id, ih.document_date) rank_ih,
	oh.order_id, ih.order_id, oh.order_no, ih.order_no, oh.num_row, 
	oh.document_id, oh.document_type, oh.document_date,
	ih.document_id, ih.document_type, ih.document_date,

	ih.invoice_id, ih.invoice_no, ih.num_row
from 
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.order_headers
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) oh
	full join
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			invoice_id, invoice_no, invoice_date, 
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.invoice_headers
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
where 
	oh.document_id is null or ih.document_id is null
order by oh.order_id, ih.order_id

	select 
		oh.document_type, ih.document_type, count(*)
	from 
			(select 
				rank() over (partition by order_id order by document_date) num_row, *
			from DW_GetLenses_jbs.dbo.order_headers) oh
		full join
			(select 
				rank() over (partition by order_id order by document_date) num_row, *
			from DW_GetLenses_jbs.dbo.invoice_headers) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
	where 
		oh.document_id is null or ih.document_id is null
	group by oh.document_type, ih.document_type
	order by oh.document_type, ih.document_type

-- Order x Shipment Relationship - Missing Records
select 
	count(oh.order_id) over () tot_oh, count(sh.order_id) over () tot_sh, 
	dense_rank() over (order by oh.order_id, oh.document_date) rank_oh, dense_rank() over (order by sh.order_id, sh.document_date) rank_sh,
	oh.order_id, sh.order_id, oh.order_no, sh.order_no, oh.num_row, 
	oh.document_id, oh.document_type, oh.document_date,
	sh.document_id, sh.document_type, sh.document_date,

	sh.shipment_id, sh.shipment_no, sh.num_row
from 
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.order_headers
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) oh
	full join
		(select 
			rank() over (partition by order_id order by document_date) num_row,
			document_id, document_type, document_date,
			order_id, order_no, order_date, 
			invoice_id, invoice_no, invoice_date, 
			shipment_id, shipment_no, shipment_date,
			store_name,
			customer_id, customer_email
		from DW_GetLenses_jbs.dbo.shipment_headers
		--where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
		) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
where 
	oh.document_id is null or sh.shipment_id is null
order by oh.order_id, sh.order_id

	select 
		oh.document_type, sh.document_type, count(*)
	from 
			(select 
				rank() over (partition by order_id order by document_date) num_row, *
			from DW_GetLenses_jbs.dbo.order_headers) oh
		full join
			(select 
				rank() over (partition by order_id order by document_date) num_row, *
			from DW_GetLenses_jbs.dbo.shipment_headers) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
	where 
		oh.document_id is null or sh.document_id is null
	group by oh.document_type, sh.document_type
	order by oh.document_type, sh.document_type

-----------------------------------------------------------------
-- Matching records in Order - Invoice - Shipment but different Document Type

select top 1000
	oh.document_type, ih.document_type, sh.document_type, count(*) 
from 
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.order_headers) oh
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.invoice_headers) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.shipment_headers) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
group by oh.document_type, ih.document_type, sh.document_type
order by oh.document_type, ih.document_type, sh.document_type

select top 1000
	count(*) over () num_tot,
	oh.order_id, ih.order_id, sh.order_id, oh.order_no, ih.order_no, sh.order_no, 
	oh.document_id, oh.document_type, oh.document_date,
	ih.document_id, ih.document_type, ih.document_date,
	sh.document_id, sh.document_type, sh.document_date,

	ih.invoice_id, ih.invoice_no, sh.shipment_id, sh.shipment_no,
	oh.order_date, ih.invoice_date, sh.shipment_date,
	oh.store_name, ih.store_name, sh.store_name, 
	oh.customer_id, ih.customer_id, sh.customer_id, oh.customer_email, ih.customer_email, sh.customer_email
from 
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.order_headers) oh
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.invoice_headers) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.shipment_headers) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
where 
	(oh.document_type = 'CREDITMEMO' and ih.document_type = 'CREDITMEMO' and sh.document_type  = 'SHIPMENT') or
	(oh.document_type = 'CREDITMEMO' and ih.document_type = 'INVOICE' and sh.document_type  = 'CREDITMEMO') or
	(oh.document_type = 'CREDITMEMO' and ih.document_type = 'INVOICE' and sh.document_type  = 'SHIPMENT') or
	(oh.document_type = 'ORDER' and ih.document_type = 'INVOICE' and sh.document_type  = 'CREDITMEMO') 
order by oh.order_id, oh.document_date

-----------------------------------------------------------------

select top 1000
	count(*) over () num_tot,
	oh.order_id, ih.order_id, sh.order_id, oh.order_no, ih.order_no, sh.order_no, 
	oh.document_id, oh.document_type, oh.document_date,
	ih.document_id, ih.document_type, ih.document_date,
	sh.document_id, sh.document_type, sh.document_date,

	ih.invoice_id, ih.invoice_no, sh.shipment_id, sh.shipment_no,
	oh.order_date, ih.invoice_date, sh.shipment_date,
	oh.store_name, ih.store_name, sh.store_name, 
	oh.customer_id, ih.customer_id, sh.customer_id, oh.customer_email, ih.customer_email, sh.customer_email, 
	oh.order_type, ih.order_type, sh.order_type, oh.order_lifecycle, ih.order_lifecycle, sh.order_lifecycle, 
	oh.business_source, ih.business_source, sh.business_source, oh.business_channel, ih.business_channel, sh.business_channel, 
	oh.coupon_code, ih.coupon_code, sh.coupon_code, oh.status, ih.status, sh.status, 
	oh.payment_method, ih.payment_method, sh.payment_method, oh.cc_type, ih.cc_type, sh.cc_type, 
	oh.shipping_carrier, ih.shipping_carrier, sh.shipping_carrier, oh.shipping_method, ih.shipping_method, sh.shipping_method, 

	oh.total_qty, ih.total_qty, sh.total_qty, 
	oh.local_prof_fee, ih.local_prof_fee, sh.local_prof_fee, 
	oh.local_subtotal_inc_vat, ih.local_subtotal_inc_vat, sh.local_subtotal_inc_vat, 
	oh.local_shipping_inc_vat, ih.local_shipping_inc_vat, sh.local_shipping_inc_vat, oh.local_discount_inc_vat, ih.local_discount_inc_vat, sh.local_discount_inc_vat, 
	oh.local_store_credit_inc_vat, ih.local_store_credit_inc_vat, sh.local_store_credit_inc_vat, oh.local_adjustment_inc_vat, ih.local_adjustment_inc_vat, sh.local_adjustment_inc_vat, 
	oh.local_total_inc_vat, ih.local_total_inc_vat, sh.local_total_inc_vat, 
	oh.local_total_cost, ih.local_total_cost, sh.local_total_cost, 
	oh.local_margin_amount, ih.local_margin_amount, sh.local_margin_amount, 
	oh.local_to_global_rate, ih.local_to_global_rate, sh.local_to_global_rate

from 
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.order_headers) oh
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.invoice_headers) ih on oh.order_id = ih.order_id and oh.num_row = ih.num_row
	inner join
		(select 
			rank() over (partition by order_id order by document_date) num_row, *
		from DW_GetLenses_jbs.dbo.shipment_headers) sh on oh.order_id = sh.order_id and oh.num_row = sh.num_row
where 
	((oh.document_type = 'CREDITMEMO' and ih.document_type = 'CREDITMEMO' and sh.document_type  = 'CREDITMEMO') or 
	(oh.document_type = 'ORDER' and ih.document_type = 'INVOICE' and sh.document_type  = 'SHIPMENT')) and 
	--not 
		((oh.customer_id = ih.customer_id) and (oh.customer_email = ih.customer_email) and
		(oh.order_type = ih.order_type) and (oh.order_lifecycle = ih.order_lifecycle) and 	
		((oh.business_source = ih.business_source) or (oh.business_source is NULL and ih.business_source is NULL)) and 
		((oh.business_channel = ih.business_channel) or (oh.business_channel is NULL and ih.business_channel is NULL)) and
		((oh.coupon_code = ih.coupon_code) or (oh.coupon_code is NULL and ih.coupon_code is NULL)) and
		(oh.status = ih.status) and (oh.payment_method = ih.payment_method) and 
		((oh.cc_type = ih.cc_type) or (oh.cc_type is NULL and ih.cc_type is NULL)) and
		(oh.shipping_carrier = ih.shipping_carrier) and (oh.shipping_method = ih.shipping_method) --and
		--(oh.total_qty = ih.total_qty and oh.total_qty = sh.total_qty) and -- 1068
		--(oh.local_subtotal_inc_vat = ih.local_subtotal_inc_vat and oh.local_subtotal_inc_vat = sh.local_subtotal_inc_vat) and -- 29224
		--(oh.local_total_inc_vat = ih.local_total_inc_vat and oh.local_total_inc_vat = sh.local_total_inc_vat) and -- 146
		--(oh.local_margin_amount = ih.local_margin_amount and oh.local_margin_amount = sh.local_margin_amount) and -- 29198
		--(oh.local_to_global_rate = ih.local_to_global_rate and oh.local_to_global_rate = sh.local_to_global_rate) -- 0
		)
 
order by oh.order_id, oh.document_date
