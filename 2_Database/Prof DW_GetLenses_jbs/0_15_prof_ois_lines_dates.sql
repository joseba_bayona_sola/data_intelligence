
-- ORDER: DOCUMENT DATES

select top 1000 *
from DW_GetLenses_jbs.dbo.order_lines
where convert(date, document_date) <> convert(date, order_date)
order by line_id

	select top 1000 convert(date, document_date) doc_date, convert(date, order_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.order_lines
	where convert(date, document_date) = convert(date, order_date)
	group by convert(date, document_date), convert(date, order_date)
	order by doc_date, ord_date

	select top 1000 convert(date, document_date) doc_date, convert(date, order_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.order_lines
	where convert(date, document_date) <> convert(date, order_date)
	group by convert(date, document_date), convert(date, order_date)
	order by doc_date, ord_date

	-- 

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.order_lines
	where convert(date, document_date) = convert(date, order_date)
	group by document_type
	order by document_type;

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.order_lines
	where convert(date, document_date) <> convert(date, order_date)
	group by document_type
	order by document_type;

-----------------------------------------------------------------------------------------------------

-- INVOICE: DOCUMENT DATES

select top 1000 *
from DW_GetLenses_jbs.dbo.invoice_lines
where (convert(date, document_date) <> convert(date, invoice_date)) or invoice_date is null
order by line_id

	select top 1000 convert(date, document_date) doc_date, convert(date, invoice_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.invoice_lines
	where convert(date, document_date) = convert(date, invoice_date)
	group by convert(date, document_date), convert(date, invoice_date)
	order by doc_date, ord_date

	select top 1000 convert(date, document_date) doc_date, convert(date, invoice_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.invoice_lines
	where (convert(date, document_date) <> convert(date, invoice_date)) or invoice_date is null
	group by convert(date, document_date), convert(date, invoice_date)
	order by doc_date, ord_date

	-- 

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.invoice_lines
	where convert(date, document_date) = convert(date, invoice_date)
	group by document_type
	order by document_type;

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.invoice_lines
	where (convert(date, document_date) <> convert(date, invoice_date)) or invoice_date is null
	group by document_type
	order by document_type;

-----------------------------------------------------------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.shipment_lines
where (convert(date, document_date) <> convert(date, shipment_date)) or shipment_date is null
order by line_id

	select top 1000 convert(date, document_date) doc_date, convert(date, shipment_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.shipment_lines
	where convert(date, document_date) = convert(date, shipment_date)
	group by convert(date, document_date), convert(date, shipment_date)
	order by doc_date, ord_date

	select top 1000 convert(date, document_date) doc_date, convert(date, shipment_date) ord_date, count(*)
	from DW_GetLenses_jbs.dbo.shipment_lines
	where convert(date, document_date) <> convert(date, shipment_date)
	group by convert(date, document_date), convert(date, shipment_date)
	order by doc_date, ord_date

	-- 

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.shipment_lines
	where convert(date, document_date) = convert(date, shipment_date)
	group by document_type
	order by document_type;

	select top 1000 document_type, count(*)
	from DW_GetLenses_jbs.dbo.shipment_lines
	where convert(date, document_date) <> convert(date, shipment_date)
	group by document_type
	order by document_type;