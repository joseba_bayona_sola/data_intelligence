
-- Different # rows per Lifecycle records
select order_lifecycle, count(*)
from DW_GetLenses_jbs.dbo.order_headers
group by order_lifecycle
order by order_lifecycle;

-- Different # rows per Lifecycle records - Cust Seq No
select order_lifecycle, customer_order_seq_no, count(*)
from DW_GetLenses_jbs.dbo.order_headers
group by order_lifecycle, customer_order_seq_no
order by order_lifecycle, customer_order_seq_no;

-- Different # rows per Lifecycle records - Doc Type
select order_lifecycle, document_type, count(*)
from DW_GetLenses_jbs.dbo.order_headers
group by order_lifecycle, document_type
order by order_lifecycle, document_type;

---------------------------------------------------------------

select top 1000 order_id, order_no, store_name, document_type,
	order_date, document_date,
	customer_id, 
	order_lifecycle, customer_order_seq_no,
	count(*) over (partition by customer_id) tot_cust,
	count(*) over (partition by customer_id, order_id) tot_cust_order
from DW_GetLenses_jbs.dbo.order_headers
where order_lifecycle = 'New'
order by tot_cust desc, customer_id, order_id, document_type

-- For New Lifecycle records, # records per customer
select customer_id, count(*) num
from DW_GetLenses_jbs.dbo.order_headers
where order_lifecycle = 'New'
group by customer_id
order by num desc

	-- Records per customers with NEW repeated records
	select 
		dense_rank() over (order by customer_id) num_cust,
		dense_rank() over (partition by customer_id order by order_id) num_cust_order,
		document_type, order_id, order_no, store_name, 
		order_date, document_date,
		customer_id, customer_email,
		order_lifecycle, customer_order_seq_no,

		total_qty, local_total_inc_vat, local_total_cost
	from DW_GetLenses_jbs.dbo.order_headers
	where --order_lifecycle = 'New' and 
		customer_id in (1627433, 1633432, 1630738, 1631877)
	order by customer_id, order_id, document_date

	select 
		dense_rank() over (order by fo.customer_id) num_cust,
		dense_rank() over (partition by fo.customer_id order by fo.order_no) num_cust_order,
		fo.uniqueID, 
		fo.document_type, fo.order_no, store_name, 
		fo.document_date,
		customer_id, order_lifecycle, customer_order_seq_no,

		product_id, sku,

		qty, local_line_subtotal_inc_vat, local_subtotal_cost, 
		document_count, product_count, line_count
	from 
			DW_GetLenses_jbs.dbo.Fact_Orders fo
		inner join 
			DW_GetLenses_jbs.dbo.Dim_Order_Headers oh on fo.uniqueID = oh.uniqueID
	where --order_lifecycle = 'New' and 
		customer_id in (1627433, 1633432, 1630738, 1631877)
	order by customer_id, order_no, document_date

---------------------------------------------------------------
-- Profiling customer_id values for New Orders
select min(customer_id), max(customer_id)
from DW_GetLenses_jbs.dbo.order_headers
where order_lifecycle = 'New'

select r, r* 100000, count(*)
from 
	(select round(customer_id/100000, 2) r
	from DW_GetLenses_jbs.dbo.order_headers
	where order_lifecycle = 'New') t
group by r
order by r;

---------------------------------------------------------------

select top 1000 count(*) over () num_tot,
	order_id, document_type 
from
	(select order_id, document_type, 
		count(*) over () num_tot,
		count(*) over (partition by order_id) num_rep
	from DW_GetLenses_jbs.dbo.order_headers) t
where num_rep = 1

-- Customers with NEW orders when Orders only have one records
select top 1000 count(*) over () num_tot,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, 
	oh.order_lifecycle, oh.customer_order_seq_no,
	count(*) over (partition by oh.customer_id) tot_cust,
	count(*) over (partition by oh.customer_id, oh.order_id) tot_cust_order
from 
		(select count(*) over () num_tot,
			order_id, document_type 
		from
			(select order_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		where num_rep = 1) t
	inner join 
		DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
where order_lifecycle = 'New'
order by tot_cust desc, customer_id, order_id, document_type

-- Customers with NEW orders when Orders only have one records --> COMPARE WITH REST OF THE ORDERS
select 
	t.order_id, t.order_no, t.store_name, t.document_type,
	t.order_date, t.document_date,
	t.customer_id, t.customer_email,
	t.order_lifecycle, t.customer_order_seq_no,
	dense_rank() over (order by t.customer_id) num_cust,
	count(*) over (partition by oh.order_id) num_rep_order,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from 
	(select top 100000 count(*) over () num_tot,
		oh.order_id, oh.order_no, oh.store_name, oh.document_type,
		oh.order_date, oh.document_date,
		oh.customer_id, oh.customer_email,
		oh.order_lifecycle, oh.customer_order_seq_no,
		count(*) over (partition by oh.customer_id) tot_cust,
		count(*) over (partition by oh.customer_id, oh.order_id) tot_cust_order
	from 
			(select count(*) over () num_tot,
				order_id, document_type 
			from
				(select order_id, document_type, 
					count(*) over () num_tot,
					count(*) over (partition by order_id) num_rep
				from DW_GetLenses_jbs.dbo.order_headers) t
			where num_rep = 1) t
		inner join 
			DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
	where order_lifecycle = 'New' and oh.document_type = 'ORDER'
	order by tot_cust desc, customer_id, order_id, document_type) t 
left join 
	DW_GetLenses.dbo.order_headers oh on t.customer_id = oh.customer_id and t.order_id <> oh.order_id
where t.tot_cust = 1
	and oh.order_id is not null
	and oh.order_date < t.order_date
order by num_rep_order, t.customer_id, oh.order_id, oh.document_date
