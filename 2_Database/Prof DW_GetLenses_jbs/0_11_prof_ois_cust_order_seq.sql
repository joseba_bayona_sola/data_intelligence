

-- Different # rows per Cust Seq No
select customer_order_seq_no, count(*)
from DW_GetLenses_jbs.dbo.order_headers
group by customer_order_seq_no
order by customer_order_seq_no;

select top 1000 order_id, order_no, store_name, document_type,
	order_date, document_date,
	customer_id, 
	order_lifecycle, customer_order_seq_no,
	count(*) over (partition by customer_id, customer_order_seq_no) tot_cust_rep,
	count(*) over (partition by customer_id, order_id) tot_cust_order
from DW_GetLenses_jbs.dbo.order_headers
order by tot_cust_rep desc, customer_id, order_id, document_type

--------------------------------------------------------------

-- For New Lifecycle records, # records per customer
select customer_id, count(distinct customer_order_seq_no) num
from DW_GetLenses_jbs.dbo.order_headers
group by customer_id
having count(distinct customer_order_seq_no) > 3
order by num desc

-- Orders of Customers with MORE DIFFERENT CUST_ORD_SEQ
select top 1000 
	t.num,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no,
	count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) tot_cust_rep,
	count(*) over (partition by oh.customer_id, oh.order_id) tot_cust_order
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	inner join
		(select customer_id, count(distinct customer_order_seq_no) num
		from DW_GetLenses_jbs.dbo.order_headers
		group by customer_id
		having count(distinct customer_order_seq_no) > 3) t on oh.customer_id = t.customer_id
order by t.num desc, oh.customer_id, oh.order_id, oh.document_date

-------------------------------------------------------------------------------------------------
-- Customers with the CUST_ORDER_SEQ_NO when Orders only have one records
select top 1000 count(*) over () num_tot,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no,
	count(*) over (partition by oh.customer_id) tot_cust, 
	count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) tot_cust_rep 
from 
		(select count(*) over () num_tot,
			order_id, document_type 
		from
			(select order_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		where num_rep = 1) t
	inner join 
		DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
where oh.document_type = 'ORDER'
order by tot_cust_rep desc, tot_cust desc, customer_id, order_id, document_date

-- Customers with the CUST_ORDER_SEQ_NO when Orders only have one records --> COMPARE WITH REST OF THE ORDERS (REPEATED CUST_ORDER_SEQ_NO)
select 
	t.order_id, t.order_no, t.store_name, t.document_type,
	t.order_date, t.document_date,
	t.customer_id, t.customer_email,
	t.order_lifecycle, t.customer_order_seq_no,
	dense_rank() over (order by t.customer_id) num_cust,
	count(*) over (partition by oh.order_id) num_rep_order,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from 
		(select top 1000 count(*) over () num_tot,
			oh.order_id, oh.order_no, oh.store_name, oh.document_type,
			oh.order_date, oh.document_date,
			oh.customer_id, oh.customer_email,
			oh.order_lifecycle, oh.customer_order_seq_no,
			count(*) over (partition by oh.customer_id) tot_cust, 
			count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) tot_cust_rep 
		from 
				(select count(*) over () num_tot,
					order_id, document_type 
				from
					(select order_id, document_type, 
						count(*) over () num_tot,
						count(*) over (partition by order_id) num_rep
					from DW_GetLenses_jbs.dbo.order_headers) t
				where num_rep = 1) t
			inner join 
				DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
		where oh.document_type = 'ORDER'
		order by tot_cust_rep desc, tot_cust desc, customer_id, order_id, document_date) t
	left join 
		DW_GetLenses.dbo.order_headers oh on t.customer_id = oh.customer_id --and t.order_id <> oh.order_id
where t.tot_cust_rep > 1
order by num_rep_order, t.customer_id, oh.order_id, oh.document_date, t.order_id

-- Customers with the CUST_ORDER_SEQ_NO when Orders only have one records --> COMPARE WITH REST OF THE ORDERS (NO REPEAT)
select 
	t.order_id, t.order_no, t.store_name, t.document_type,
	t.order_date, t.document_date,
	t.customer_id, t.customer_email,
	t.order_lifecycle, t.customer_order_seq_no,
	dense_rank() over (order by t.customer_id) num_cust,
	count(*) over (partition by oh.order_id) num_rep_order,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from 
		(select top 100 count(*) over () num_tot,
			oh.order_id, oh.order_no, oh.store_name, oh.document_type,
			oh.order_date, oh.document_date,
			oh.customer_id, oh.customer_email,
			oh.order_lifecycle, oh.customer_order_seq_no,
			count(*) over (partition by oh.customer_id) tot_cust, 
			count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) tot_cust_rep 
		from 
				(select count(*) over () num_tot,
					order_id, document_type 
				from
					(select order_id, document_type, 
						count(*) over () num_tot,
						count(*) over (partition by order_id) num_rep
					from DW_GetLenses_jbs.dbo.order_headers) t
				where num_rep = 1) t
			inner join 
				DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
		where oh.document_type = 'ORDER'
		order by tot_cust_rep desc, tot_cust desc, customer_id, order_id, document_date) t
	left join 
		DW_GetLenses.dbo.order_headers oh on t.customer_id = oh.customer_id --and t.order_id <> oh.order_id
where t.tot_cust_rep = 1
order by num_rep_order, t.customer_id, oh.order_id, oh.document_date, t.order_id

select 
	dense_rank() over (order by t.customer_id) num_cust,
	count(*) over (partition by oh.order_id) num_rep_order,
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from
		(select distinct top 100 customer_id 
		from 
			(select count(*) over () num_tot,
				oh.customer_id,
				count(*) over (partition by oh.customer_id) tot_cust, 
				count(*) over (partition by oh.customer_id, oh.customer_order_seq_no) tot_cust_rep 
			from 
					(select count(*) over () num_tot,
						order_id, document_type 
					from
						(select order_id, document_type, 
							count(*) over () num_tot,
							count(*) over (partition by order_id) num_rep
						from DW_GetLenses_jbs.dbo.order_headers) t
					where num_rep = 1) t
				inner join 
					DW_GetLenses_jbs.dbo.order_headers oh on t.order_id = oh.order_id
			where oh.document_type = 'ORDER') t
		where t.tot_cust_rep = 1) t
	left join 
		DW_GetLenses.dbo.order_headers oh on t.customer_id = oh.customer_id --and t.order_id <> oh.order_id
order by t.customer_id, oh.order_id, oh.document_date

select 
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from DW_GetLenses.dbo.order_headers oh
where customer_id = 110
order by order_id;

select 
	oh.order_id, oh.order_no, oh.store_name, oh.document_type,
	oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email,
	oh.order_lifecycle, oh.customer_order_seq_no
from DW_Proforma.dbo.order_headers oh
where customer_id = 110;
