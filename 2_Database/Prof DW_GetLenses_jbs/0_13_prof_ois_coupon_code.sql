
-- Different # rows per Business Channel records
select coupon_code, count(*) num
from DW_GetLenses_jbs.dbo.order_headers
group by coupon_code
order by num desc, coupon_code;

-- Different # rows per Lifecycle - Business Channel records
select order_lifecycle, coupon_code, count(*) num
from DW_GetLenses_jbs.dbo.order_headers
group by order_lifecycle, coupon_code
order by order_lifecycle, num desc, coupon_code;
