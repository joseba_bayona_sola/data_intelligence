
-- Document Type in Headers, Lines

select top 100 document_type, count(*)
from 
	DW_GetLenses_jbs.dbo.order_headers
	-- DW_GetLenses_jbs.dbo.invoice_headers
	-- DW_GetLenses_jbs.dbo.shipment_headers
group by document_type
order by document_type

select top 100 document_type, count(*)
from 
	DW_GetLenses_jbs.dbo.order_lines
	-- DW_GetLenses_jbs.dbo.invoice_lines
	-- DW_GetLenses_jbs.dbo.shipment_lines
group by document_type
order by document_type

-- Status in Headers, Lines

select top 100 status, count(*)
from 
	DW_GetLenses_jbs.dbo.order_headers
	-- DW_GetLenses_jbs.dbo.invoice_headers
	-- DW_GetLenses_jbs.dbo.shipment_headers
group by status
order by status


-------------------------------------------------------------------

-- Cardinality in Order Header

select top 100 document_id, count(*) num 
from 
	DW_GetLenses_jbs.dbo.order_headers
group by document_id
having count(*) > 1
order by num desc

select top 100 order_id, count(*) num 
from 
	DW_GetLenses_jbs.dbo.order_headers
group by order_id
having count(*) > 1
order by num desc

select top 100 order_no, count(*) num 
from 
	DW_GetLenses_jbs.dbo.order_headers
group by order_no
having count(*) > 1
order by num desc



select *
from DW_GetLenses_jbs.dbo.order_headers
where document_id = 4501634 or order_no = '8002274175'
order by order_no, order_date, document_date

select *
from DW_GetLenses_jbs.dbo.order_lines
where order_no = '5001311518' or order_no = '8002274175'
order by order_no, order_date, document_date;


select *
from DW_GetLenses_jbs.dbo.invoice_headers
where order_no = '5001311518' or order_no = '8002274175'
order by order_no, order_date, document_date

select *
from DW_GetLenses_jbs.dbo.shipment_headers
where order_no = '5001311518' or order_no = '8002274175'
order by order_no, order_date, document_date

