use DW_GetLenses_jbs
go 

-- Dim_Order_Headers
select * 
into DW_GetLenses_jbs.dbo.Dim_Order_Headers
from DW_Sales_Actual.dbo.Dim_Order_Headers
where document_date > getutcdate() - 30;

-- Fact_Orders
select * 
into DW_GetLenses_jbs.dbo.Fact_Orders
from DW_Sales_Actual.dbo.Fact_Orders
where document_date > getutcdate() - 30;


-- Dim_Invoice_Headers
select * 
into DW_GetLenses_jbs.dbo.Dim_Invoice_Headers
from DW_Sales_Actual.dbo.Dim_Invoice_Headers
where document_date > getutcdate() - 30;

-- Fact_Invoice
select * 
into DW_GetLenses_jbs.dbo.Fact_Invoices
from DW_Sales_Actual.dbo.Fact_Invoices
where document_date > getutcdate() - 30;


-- Dim_Shipment_Headers
select * 
into DW_GetLenses_jbs.dbo.Dim_Shipment_Headers
from DW_Sales_Actual.dbo.Dim_Shipment_Headers
where document_date > getutcdate() - 30;

-- Fact_Shipments
select * 
into DW_GetLenses_jbs.dbo.Fact_Shipments
from DW_Sales_Actual.dbo.Fact_Shipments
where document_date > getutcdate() - 30;




