
-- Sample Queries to Header - Lines
select *
from DW_GetLenses_jbs.dbo.order_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date

-- local_subtotal_inc_vat = local_subtotal_vat + local_subtotal_exc_vat 
-- local_shipping_inc_vat = local_shipping_vat + local_shipping_exc_vat
-- local_discount_inc_vat = local_discount_vat + local_discount_exc_vat
-- local_store_credit_inc_vat = local_store_credit_vat + local_store_credit_exc_vat (NULL?, NULL?)
-- local_adjustment_inc_vat = local_adjustment_vat + local_adjustment_exc_vat (NULL?, NULL?)

-- local_total_inc_vat = local_total_vat + local_total_exc_vat
-- local_total_inc_vat = local_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat (local_store_credit_inc_vat?,  local_adjustment_inc_vat?)

-- local_total_cost = local_subtotal_cost + local_shipping_cost 

-- local_margin_amount = local_total_inc_vat - local_total_cost - local_total_vat
-- local_margin_amount = local_total_exc_vat - local_total_cost
-- local_margin_percent = local_margin_amount/local_total_exc_vat

--
-- global_prof_fee = local_prof_fee * local_to_global_rate
-- global_total_inc_vat = local_total_inc_vat * local_to_global_rate

-- Important Attributes in Header
select document_id, document_type, 
	order_id, order_no, order_date, document_date,
	customer_id, customer_email, 
	total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, -- local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, -- local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, -- local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, -- local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, -- local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, -- local_total_vat, local_total_exc_vat, 
	
	--local_subtotal_cost, local_shipping_cost, 
	--local_total_cost, 
	local_margin_amount, local_margin_percent, 
	
	local_to_global_rate, order_currency_code, 

	-- global_prof_fee, 
	-- global_subtotal_inc_vat, -- global_subtotal_vat, global_subtotal_exc_vat, 
	-- global_shipping_inc_vat, -- global_shipping_vat, global_shipping_exc_vat, 
	-- global_discount_inc_vat, -- global_discount_vat, global_discount_exc_vat, 
	-- global_store_credit_inc_vat, -- global_store_credit_vat, global_store_credit_exc_vat, 
	-- global_adjustment_inc_vat, -- global_adjustment_vat, global_adjustment_exc_vat, 
	-- global_total_inc_vat, -- global_total_vat, global_total_exc_vat, 
	
	-- global_subtotal_cost, global_shipping_cost, 
	-- global_total_cost, 
	-- global_margin_amount, global_margin_percent, 
	
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses_jbs.dbo.order_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382)
order by order_id, document_date

-- Comprobation of important Atribute Calculation in Header
select document_id, document_type, 
	order_id, order_no, order_date, document_date,
	store_name,
	customer_id, customer_email, 
	--local_prof_fee,
	local_total_inc_vat, local_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + ISNULL(local_store_credit_inc_vat, 0) + ISNULL(local_adjustment_inc_vat, 0) local_total_inc_vat_2,
	local_total_cost, local_subtotal_cost + local_shipping_cost local_total_cost_2,
	local_margin_amount, local_total_inc_vat - local_total_cost - local_total_vat local_margin_amount_2, local_total_exc_vat - local_total_cost local_margin_amount_3, 
	local_margin_percent, local_margin_amount/case when local_total_exc_vat = 0 then NULL else local_total_exc_vat end local_margin_percent_2,
	global_total_inc_vat, local_total_inc_vat * local_to_global_rate global_total_inc_vat_2
	--,case when local_total_exc_vat = 0 then 1 else local_total_exc_vat end local_total_exc_vat
from DW_GetLenses_jbs.dbo.order_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382)
order by order_id, document_date

------
-- Comprobation of important Atribute Calculation in Header for ALL records in one month --> Chech for discrepancies
select *
from 
	(select document_id, document_type, 
		order_id, order_no, order_date, document_date,
		store_name,
		customer_id, customer_email, 
		--local_prof_fee,
		local_store_credit_inc_vat,
		local_total_inc_vat, local_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + ISNULL(local_store_credit_inc_vat, 0) + ISNULL(local_adjustment_inc_vat, 0) local_total_inc_vat_2,
		local_total_cost, local_subtotal_cost + local_shipping_cost local_total_cost_2,
		local_margin_amount, local_total_exc_vat - local_total_cost local_margin_amount_2, local_total_inc_vat - local_total_cost - local_total_vat local_margin_amount_3, 
		local_margin_percent, local_margin_amount/case when local_total_exc_vat = 0 then NULL else local_total_exc_vat end local_margin_percent_2,
		global_total_inc_vat, local_total_inc_vat * local_to_global_rate global_total_inc_vat_2
	from DW_GetLenses_jbs.dbo.order_headers) oh 
where 
	abs(local_total_inc_vat - local_total_inc_vat_2) > 1 or abs(local_total_cost - local_total_cost_2) > 1 or 
	abs(local_margin_amount - local_margin_amount_2) > 1 or abs(local_margin_percent - local_margin_percent_2) > 1 or 
	abs(global_total_inc_vat - global_total_inc_vat_2) > 1
order by order_id, document_date