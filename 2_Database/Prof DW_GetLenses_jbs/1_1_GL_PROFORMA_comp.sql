

select top 100 count(*) over (), -- 3.077.786
	order_id, order_no, document_id, document_type, store_name,
	customer_id
from DW_GetLenses.dbo.order_headers

select top 100 count(*) over (), -- 1.523.839
	order_id, order_no, document_id, document_type, store_name,
	customer_id
from DW_Proforma.dbo.order_headers


---------------------------------------------------

-- Order ID
	select top 1000 order_id -- 0
	from 
		(select order_id
		from DW_GetLenses.dbo.order_headers
		intersect
		select order_id
		from DW_Proforma.dbo.order_headers) oh

-- Order No
	select top 1000 order_no -- 0
	from 
		(select order_no
		from DW_GetLenses.dbo.order_headers
		intersect
		select order_no
		from DW_Proforma.dbo.order_headers) oh

