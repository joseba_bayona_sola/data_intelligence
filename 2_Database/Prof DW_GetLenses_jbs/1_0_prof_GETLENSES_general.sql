
select top 1000 *
from DW_GetLenses.dbo.order_headers

select store_name, count(*)
from DW_GetLenses.dbo.order_headers
group by store_name
order by store_name

select convert(date, document_date), count(*)
from DW_GetLenses.dbo.order_headers
group by convert(date, document_date)
order by convert(date, document_date) desc

	select year(convert(date, document_date)), count(*)
	from DW_GetLenses.dbo.order_headers
	group by year(convert(date, document_date))
	order by year(convert(date, document_date)) desc

	select 
		year(convert(date, document_date)), month(convert(date, document_date)),
		count(*)
	from DW_GetLenses.dbo.order_headers
	where store_name in ('visiondirect.co.uk', 'visiondirect.ie')
	group by year(convert(date, document_date)), month(convert(date, document_date))
	order by year(convert(date, document_date)) desc, month(convert(date, document_date))

--------------------------------------------------------------------------------------------

select document_type, count(*)
from DW_GetLenses.dbo.order_headers
group by document_type
order by document_type