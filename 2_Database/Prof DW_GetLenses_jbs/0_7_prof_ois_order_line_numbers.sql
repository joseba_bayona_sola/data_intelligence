
-- Sample Queries to Header - Lines
select *
from DW_GetLenses_jbs.dbo.order_headers
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date

select *
from DW_GetLenses_jbs.dbo.order_lines
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date, line_id

-- local_line_subtotal_inc_vat =  local_price_inc_vat * qty 

-- local_line_subtotal_inc_vat = local_line_subtotal_vat + local_line_subtotal_exc_vat
-- local_shipping_inc_vat = local_shipping_vat + local_shipping_exc_vat
-- local_discount_inc_vat = local_discount_vat + local_discount_exc_vat
-- local_store_credit_inc_vat = local_store_credit_vat + local_store_credit_exc_vat
-- local_adjustment_inc_vat = local_adjustment_vat + local_adjustment_exc_vat

-- local_line_total_inc_vat = local_line_total_vat + local_line_total_exc_vat
-- local_line_total_inc_vat = local_line_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + local_store_credit_inc_vat + local_adjustment_inc_vat

-- local_total_cost = local_subtotal_cost + local_shipping_cost

-- local_margin_amount = local_line_total_exc_vat - local_total_cost
-- local_margin_percent = local_margin_amount/local_line_total_exc_vat

-- global_line_total_inc_vat = local_line_total_inc_vat * local_to_global_rate

-- Important Attributes in Lines
select document_id, document_type, 
	order_id, order_no, order_date, document_date,
	product_type, product_id, sku, name, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, -- local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, -- local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, -- local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, -- local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, -- local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, -- local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, -- local_line_total_vat, local_line_total_exc_vat, 

	-- local_subtotal_cost, local_shipping_cost, 
	local_total_cost, 
	local_margin_amount, local_margin_percent, 
	
	local_to_global_rate, order_currency_code, 

	-- global_prof_fee, 
	-- global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	-- global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	-- global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	-- global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	-- global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	-- global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	-- global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	-- global_subtotal_cost, global_shipping_cost, global_total_cost, 
	-- global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses_jbs.dbo.order_lines
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date, line_id

-- Comprobation of important Atribute Calculation in Lines
select document_id, document_type, line_id,
	order_id, order_no, order_date, document_date,
	product_type, product_id, sku, name, 

	local_line_subtotal_inc_vat, local_price_inc_vat * qty local_line_subtotal_inc_vat_2, 
	local_line_total_inc_vat, local_line_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + ISNULL(local_store_credit_inc_vat, 0) + ISNULL(local_adjustment_inc_vat, 0) local_line_total_inc_vat_2, 
	local_total_cost, local_subtotal_cost + local_shipping_cost local_total_cost_2, 
	local_margin_amount, local_line_total_exc_vat - local_total_cost local_margin_amount_2,
	local_margin_percent, local_margin_amount/case when local_line_total_exc_vat = 0 then NULL else local_line_total_exc_vat end local_margin_percent_2,
	global_line_total_inc_vat, local_line_total_inc_vat * local_to_global_rate global_line_total_inc_vat_2
from DW_GetLenses_jbs.dbo.order_lines
where order_id in (4426397, 4452505, 4458350, 4441741, 4471382, 4426122)
order by order_id, document_date, line_id

------
-- Comprobation of important Atribute Calculation in Lines for ALL records in one month --> Chech for discrepancies
select *
from 
	(select document_id, document_type, line_id,
		order_id, order_no, order_date, document_date,
		product_type, product_id, sku, name, 

		local_line_subtotal_inc_vat, local_price_inc_vat * qty local_line_subtotal_inc_vat_2, 
		local_store_credit_inc_vat,
		local_line_total_inc_vat, local_line_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + ISNULL(local_store_credit_inc_vat, 0) + ISNULL(local_adjustment_inc_vat, 0) local_line_total_inc_vat_2, 
		local_total_cost, local_subtotal_cost + local_shipping_cost local_total_cost_2, 
		local_margin_amount, local_line_total_exc_vat - local_total_cost local_margin_amount_2,
		local_margin_percent, local_margin_amount/case when local_line_total_exc_vat = 0 then NULL else local_line_total_exc_vat end local_margin_percent_2,
		global_line_total_inc_vat, local_line_total_inc_vat * local_to_global_rate global_line_total_inc_vat_2
	from DW_GetLenses_jbs.dbo.order_lines) t
where 
	abs(local_line_subtotal_inc_vat - local_line_subtotal_inc_vat_2) > 1 or 
	abs(local_line_total_inc_vat - local_line_total_inc_vat_2) > 1 or abs(local_total_cost - local_total_cost_2) > 1 or 
	abs(local_margin_amount - local_margin_amount_2) > 1 or abs(local_margin_percent - local_margin_percent_2) > 1 or 
	abs(global_line_total_inc_vat - global_line_total_inc_vat_2) > 1
order by order_id, document_date, line_id

--------------------------------------------------------------------------------

-- local_subtotal_inc_vat = SUM(local_line_subtotal_inc)
-- local_shipping_inc_vat
-- local_discount_inc_vat
-- local_store_credit_inc_vat
-- local_adjustment_inc_vat
-- local_total_inc_vat

-- Important Atributes Header - Lines combine
select oh.document_id, oh.document_type, ol.line_id,
	oh.order_id, oh.order_no, oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email, 
	oh.total_qty, ol.qty, 
	oh.local_prof_fee, ol.local_prof_fee local_prof_fee_ol, 
	oh.local_subtotal_inc_vat, ol.local_line_total_inc_vat,   
	oh.local_shipping_inc_vat, ol.local_shipping_inc_vat local_shipping_inc_vat_ol, 
	oh.local_discount_inc_vat, ol.local_discount_inc_vat local_discount_inc_vat_ol, 
	oh.local_store_credit_inc_vat, ol.local_store_credit_inc_vat local_store_credit_inc_vat_ol, 
	oh.local_adjustment_inc_vat, ol.local_adjustment_inc_vat local_adjustment_inc_vat_ol,  
	oh.local_total_inc_vat, ol.local_line_total_inc_vat,
	oh.local_total_cost, ol.local_total_cost local_total_cost_ol, 
	oh.local_to_global_rate, oh.order_currency_code
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	inner join 
		DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.order_id in (4426397, 4452505, 4458350, 4441741, 4471382)
order by oh.order_id, oh.document_date, ol.line_id

-- Comprobation of important Atribute Calculation in Lines respect to Header
select oh.document_id, oh.document_type, count(ol.line_id) line_id_count,
	oh.order_id, oh.order_no, oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email, 
	oh.total_qty, sum(ol.qty) qty_sum, 
	oh.local_prof_fee, sum(ol.local_prof_fee) local_prof_fee_sum, 
	oh.local_subtotal_inc_vat, sum(ol.local_line_subtotal_inc_vat) local_line_subtotal_inc_vat_sum,   
	oh.local_shipping_inc_vat, sum(ol.local_shipping_inc_vat) local_shipping_inc_vat_sum, 
	oh.local_discount_inc_vat, sum(ol.local_discount_inc_vat) local_discount_inc_vat_sum, 
	oh.local_store_credit_inc_vat, sum(ol.local_store_credit_inc_vat) local_store_credit_inc_vat_sum, 
	oh.local_adjustment_inc_vat, sum(ol.local_adjustment_inc_vat) local_adjustment_inc_vat_sum,  
	oh.local_total_inc_vat, sum(ol.local_line_total_inc_vat) local_line_total_inc_vat_sum, 
	oh.local_total_cost, sum(ol.local_total_cost) local_total_cost_sum, 
	oh.local_to_global_rate, oh.order_currency_code
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	inner join 
		DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.order_id in (4426397, 4452505, 4458350, 4441741, 4471382)
group by oh.document_id, oh.document_type, 
	oh.order_id, oh.order_no, oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email, 
	oh.total_qty, 
	oh.local_prof_fee, 
	oh.local_subtotal_inc_vat, 
	oh.local_shipping_inc_vat, 
	oh.local_discount_inc_vat,  
	oh.local_store_credit_inc_vat, 
	oh.local_adjustment_inc_vat,   
	oh.local_total_inc_vat, 
	oh.local_total_cost, 
	oh.local_to_global_rate, oh.order_currency_code
order by oh.order_id, oh.document_date

--
-- Comprobation of important Atribute Calculation in Lines respect to Header for ALL records in one month --> Chech for discrepancies
select *
from 
	(select oh.document_id, oh.document_type, count(ol.line_id) line_id_count,
		oh.order_id, oh.order_no, oh.order_date, oh.document_date,
		oh.customer_id, oh.customer_email, 
		oh.total_qty, sum(ol.qty) qty_sum, 
		oh.local_prof_fee, sum(ol.local_prof_fee) local_prof_fee_sum, 
		oh.local_subtotal_inc_vat, sum(ol.local_line_subtotal_inc_vat) local_line_subtotal_inc_vat_sum,   
		oh.local_shipping_inc_vat, sum(ol.local_shipping_inc_vat) local_shipping_inc_vat_sum, 
		oh.local_discount_inc_vat, sum(ol.local_discount_inc_vat) local_discount_inc_vat_sum, 
		oh.local_store_credit_inc_vat, sum(ol.local_store_credit_inc_vat) local_store_credit_inc_vat_sum, 
		oh.local_adjustment_inc_vat, sum(ol.local_adjustment_inc_vat) local_adjustment_inc_vat_sum,  
		oh.local_total_inc_vat, sum(ol.local_line_total_inc_vat) local_line_total_inc_vat_sum, 
		oh.local_total_cost, sum(ol.local_total_cost) local_total_cost_sum, 
		oh.local_to_global_rate, oh.order_currency_code
	from 
			DW_GetLenses_jbs.dbo.order_headers oh
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
	group by oh.document_id, oh.document_type, 
		oh.order_id, oh.order_no, oh.order_date, oh.document_date,
		oh.customer_id, oh.customer_email, 
		oh.total_qty, 
		oh.local_prof_fee, 
		oh.local_subtotal_inc_vat, 
		oh.local_shipping_inc_vat, 
		oh.local_discount_inc_vat,  
		oh.local_store_credit_inc_vat, 
		oh.local_adjustment_inc_vat,   
		oh.local_total_inc_vat, 
		oh.local_total_cost, 
		oh.local_to_global_rate, oh.order_currency_code) t 
where 
	abs(total_qty - qty_sum) > 1 or 
	abs(local_prof_fee - local_prof_fee_sum) > 1 or 
	abs(local_subtotal_inc_vat - local_line_subtotal_inc_vat_sum) > 1 or 
	abs(local_shipping_inc_vat - local_shipping_inc_vat_sum) > 1 or abs(local_discount_inc_vat - local_discount_inc_vat_sum) > 1 or
	abs(local_store_credit_inc_vat - local_store_credit_inc_vat_sum) > 1 or  abs(local_adjustment_inc_vat - local_adjustment_inc_vat_sum) > 1 or
	abs(local_total_inc_vat - local_line_total_inc_vat_sum) > 1 or 
	abs(local_total_cost - local_total_cost_sum) > 1 
order by order_id, document_date

--------------------------------------
-- Sample Query
select oh.document_id, oh.document_type, ol.line_id,
	oh.order_id, oh.order_no, oh.order_date, oh.document_date,
	oh.customer_id, oh.customer_email, 
	ol.product_type, ol.product_id, ol.sku, ol.name, 
	oh.total_qty, ol.qty, 
	oh.local_prof_fee, ol.local_prof_fee local_prof_fee_ol, 
	oh.local_subtotal_inc_vat, ol.local_line_total_inc_vat,   
	oh.local_shipping_inc_vat, ol.local_shipping_inc_vat local_shipping_inc_vat_ol, 
	oh.local_discount_inc_vat, ol.local_discount_inc_vat local_discount_inc_vat_ol, 
	oh.local_store_credit_inc_vat, ol.local_store_credit_inc_vat local_store_credit_inc_vat_ol, 
	oh.local_adjustment_inc_vat, ol.local_adjustment_inc_vat local_adjustment_inc_vat_ol,  
	oh.local_total_inc_vat, ol.local_line_total_inc_vat,
	oh.local_total_cost, ol.local_total_cost local_total_cost_ol, 
	oh.local_to_global_rate, oh.order_currency_code
from 
		DW_GetLenses_jbs.dbo.order_headers oh
	inner join 
		DW_GetLenses_jbs.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.order_id in (4506767, 4521997, 4522111)
order by oh.order_id, oh.document_date, ol.line_id