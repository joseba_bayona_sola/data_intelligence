
-- Detail Data per Line
	select top 1000 ol.line_id, 
		ol.product_type, ol.product_id, ol.sku, ol.name, 
		ol.local_prof_fee,  
		ol.qty, ol.local_price_inc_vat, 
		ol.local_line_subtotal_inc_vat, 
		ol.local_subtotal_cost, 
		ol.order_currency_code
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
	order by ol.product_id, ol.order_currency_code, ol.qty, ol.local_price_inc_vat 

-- # rows per product
	select top 1000 ol.product_type, ol.product_id, ol.name, count(*) num, sum(qty), sum(qty)/30
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
	group by ol.product_type, ol.product_id, ol.name
	order by num desc
	--order by ol.product_type, ol.product_id, ol.name;

-------------------------------------------------------------------------------------------------

	-- Product 1083: #rows per Order Currency - QTY
	select top 1000 
		ol.product_type, ol.product_id, ol.name, 
		ol.order_currency_code, 
		ol.qty, 
		count(*)
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
		and ol.product_id = 1083
	group by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.qty
	order by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.qty

	-- Product 1083: #rows per Order Currency - LOCAL_PRICE_INC_VAT
	select top 1000 
		ol.product_type, ol.product_id, ol.name, 
		ol.order_currency_code, 
		ol.local_price_inc_vat, 
		count(*)
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
		and ol.product_id = 1083
	group by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.local_price_inc_vat
	order by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.local_price_inc_vat

	-- Product 1083: #rows per Order Currency - LOCAL_SUBTOTAL_COST
	select top 1000 
		ol.product_type, ol.product_id, ol.name, 
		ol.order_currency_code, 
		ol.local_subtotal_cost, 
		count(*)
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
		and ol.product_id = 1083
	group by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.local_subtotal_cost
	order by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.local_subtotal_cost


	select top 1000 -- ol.sku, ol.line_id, 
		ol.product_type, ol.product_id, ol.name, -- ol.sku, 
		-- ol.local_line_subtotal_inc_vat, 
		-- ol.local_subtotal_cost, 
		ol.order_currency_code, 
		ol.qty, ol.local_price_inc_vat,
		--ol.local_prof_fee,  
		count(*)
	from 
			(select order_id, document_id, document_type, 
				count(*) over () num_tot,
				count(*) over (partition by order_id) num_rep
			from DW_GetLenses_jbs.dbo.order_headers) t
		inner join 
			DW_GetLenses_jbs.dbo.order_lines ol on t.document_id = ol.document_id and t.document_type = ol.document_type
	where t.num_rep = 1
		and t.document_type = 'ORDER'
		and ol.product_id = 1083
	group by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.qty, ol.local_price_inc_vat--, ol.local_prof_fee
	order by ol.product_type, ol.product_id, ol.name, ol.order_currency_code, ol.qty, ol.local_price_inc_vat--, ol.local_prof_fee 


-----------------------------------------------------------------------------------

select *
from DW_GetLenses.dbo.products
where product_id =  1083