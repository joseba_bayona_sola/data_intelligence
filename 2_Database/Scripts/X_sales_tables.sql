
-- Sales Flat Order Payment
select top 1000 
	entity_id, parent_id, 
	shipping_captured, 

	amount_refunded, amount_canceled, shipping_amount, amount_paid, 
	amount_authorized, shipping_refunded, amount_ordered, 

	base_amount_refunded, base_amount_canceled, base_shipping_amount, base_amount_paid_online, 
	base_amount_authorized, base_shipping_refunded, base_amount_refunded_online, base_amount_ordered,  

	method, 

	quote_payment_id, last_trans_id,
	ideal_issuer_title, ideal_issuer_id, ideal_transaction_checked,  
	protection_eligibility, [ paybox_question_number], po_number, account_status, 
	anet_trans_method, flo2cash_account_id, paybox_request_number, address_status, 

	cc_ss_start_year, cc_ss_start_month, cc_exp_month, cc_exp_year, 
	cc_last4, 
	cc_status, cc_status_description, cc_cid_status, cc_type, cc_approval, 
	cc_debug_request_body, cc_debug_response_body, cc_debug_response_serialized, 
	cc_secure_verify, cc_ss_issue, cc_avs_status, cc_number_enc, cc_trans_id, 

	echeck_type,  echeck_account_type,  echeck_account_name, echeck_bank_name, echeck_routing_number,  

	cybersource_token, 
	cybersource_cc_stored, cybersource_stored_id, cybersource_stored_use,
	cybersource_last_authorize, cybersource_last_authorize_id, 
	cybersource_last_capture, cybersource_last_capture_id, 
	cybersource_last_refund, cybersource_last_refund_id, 
	cybersource_last_void, cybersource_last_void_id,  

	internetkassa_issuer_pm, internetkassa_issuer_brand, internetkassa_issuer_ideal
from DW_GetLenses.dbo.sales_flat_order_payment

	select method, count(*)
	from DW_GetLenses.dbo.sales_flat_order_payment
	group by method
	order by method;


-- Sales Flat Shipment Track
select top 1000 
	entity_id, parent_id,
	order_id,
	track_number,   	
	description, title, carrier_code,
	weight, qty,
	created_at, updated_at
from DW_GetLenses.dbo.dw_sales_flat_shipment_track

	select carrier_code, count(*)
	from DW_GetLenses.dbo.dw_sales_flat_shipment_track
	group by carrier_code
	order by carrier_code;
