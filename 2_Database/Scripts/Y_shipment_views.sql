
-- dbo.Dim_Shipping_Carrier
	SELECT shipping_carrier 
	from dw_getlenses.dbo.order_headers oh 
	where exists 
		(select * from DW_GetLenses.dbo.stores where stores.store_name =oh.store_name ) 
	union
	SELECT shipping_carrier 
	from dw_proforma.dbo.order_headers oh 
	where exists 
		(select * from DW_Proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr') 
		and oh.document_date >='01-feb-2014'

-- dbo.Dim_Shipping_Carrier_New
	select * 
	from Dim_Shipping_Carrier rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_20 pc where pc.LinkField=isnull(rs.shipping_carrier,''))



-- dbo.Dim_Shipping_Methods
	select shipping_method 
	from dw_getlenses.dbo.order_headers oh 
	where exists 
		(select * from DW_GetLenses.dbo.stores where stores.store_name =oh.store_name ) 
	union
	select shipping_method 
	from dw_proforma.dbo.order_headers oh 
	where exists 
		(select * from DW_Proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr') 
		and oh.document_date >='01-feb-2014'

-- dbo.Dim_Shipping_Methods_New
	select * 
	from Dim_Shipping_Methods rs
	where not exists
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_19 pc where pc.LinkField=isnull(rs.shipping_method,''));


-- dbo.Dim_Shipment_Parameters 
	select distinct
		p.product_id, p.name, 
		cast((l.sku+'-LQ' + isnull(CAST(cast(abs(l.qty) as integer) AS varchar),'0')) as varchar(255)) as sku, l.sku as product_code,
		isnull(l.lens_base_curve,'') as lens_base_curve, isnull(l.lens_diameter,'') as lens_diameter,
		isnull(l.lens_power,'') as lens_power, isnull(l.lens_cylinder,'') as lens_cylinder,
		isnull(l.lens_axis,'') as lens_axis,  isnull(l.lens_addition,'') as lens_addition,
		isnull(l.lens_dominance,'') as lens_dominance, isnull(l.lens_colour,'') as lens_colour,
		isnull(cast(abs(l.qty) as integer),0) as qty
	from 
			(select * 
			from dw_getlenses.dbo.shipment_lines sl 
			where exists
				(select * from DW_GetLenses.dbo.stores where stores.store_name =sl.store_name ) 
			union all
			select * 
			from dw_proforma.dbo.shipment_lines sl 
			where exists
				(select * from DW_Proforma.dbo.stores where stores.store_name =sl.store_name and stores.store_name <>'visiooptik.fr') 
				and sl.document_date >='01-feb-2014') l
		INNER join 
			dbo.Dim_products p ON l.product_id = p.product_id

-- dbo.Dim_Shipment_Parameters_New 
select * 
from Dim_Shipment_Parameters rs
where not exists 
	(Select * from [Phocas_Sales_Actual_Live].dbo.entity_27 pc where pc.LinkField=rs.sku)


-- dbo.Dim_Shipment_Qty_Price
	SELECT 
		'S' + LEFT(document_type, 2) + CAST(document_id AS varchar) + '-' + CAST(l.product_id AS varchar) + '-' + isnull(CAST(abs(qty) AS varchar),'0.0000') AS UniqueIDQP, 
		l.product_id, p.[name], 
		cast(l.product_id as varchar(10))+ '-' + (CASE order_currency_code WHEN 'GBP' THEN '1' WHEN 'EUR' THEN '2' ELSE '3' END) + 
			case price_type when 'Discounted' then '1' when 'PLA' then '2' when 'PLA & Discounted' then '3' when 'Regular' then '4' else '5' end +
			isnull(convert(varchar(8),cast(abs(sum(local_line_subtotal_inc_vat+local_discount_inc_vat)/sum(qty))* pack_qty as decimal(10,2)),1),'0') +' '+p.[name] as name_packprice,
	
		cast(l.product_id as varchar(10)) + '-' + isnull(cast(floor(abs(qty)) AS varchar),0)+' '+p.[name] as name_qty,
		cast(l.product_id as varchar(10)) + '-' + 
			case price_type when 'Discounted' then '1' when 'PLA' then '2' when 'PLA & Discounted' then '3' when 'Regular' then '4' else '5' end +' '+p.[name] as name_pricetype,
		qty=isnull(abs(qty),0), isnull(pack_qty,0) as pack_qty, 
		pack_price= (CASE order_currency_code WHEN 'GBP' THEN '�' WHEN 'EUR' THEN '�' ELSE order_currency_code END) + 
			isnull(convert(varchar(8),cast(abs(sum(local_line_subtotal_inc_vat+local_discount_inc_vat)/sum(qty)) *pack_qty as decimal(10,2)),1),'0'),
		lines=isnull(sum(CASE WHEN qty < 0 THEN -1 WHEN qty = 0 THEN 0 WHEN qty > 0 THEN 1 END),0), 
		isnull(sum(qty),0) AS total_qty , l.price_type
	
	FROM       
			(select document_type, document_id, product_id,
				order_currency_code,
				price_type, 
				local_line_subtotal_inc_vat, local_discount_inc_vat, 
				qty=case when qty=0 then null else qty end  
			from dw_getlenses.dbo.shipment_lines sl 
			where exists 
				(select * from DW_GetLenses.dbo.stores where stores.store_name =sl.store_name ) 
			union all
			select document_type, document_id, product_id,
				order_currency_code,
				price_type, 
				local_line_subtotal_inc_vat, local_discount_inc_vat,
				qty=case when qty=0 then null else qty end  
			from dw_proforma.dbo.shipment_lines sl 
			where exists 
				(select * from DW_Proforma.dbo.stores where stores.store_name =sl.store_name and stores.store_name <>'visiooptik.fr')
				and sl.document_date >='01-feb-2014')l
		INNER JOIN 
			Dim_products p ON l.product_id = p.product_id
	GROUP BY 'S' + LEFT(document_type, 2) + CAST(document_id AS varchar), l.product_id, abs(qty),p.[name], pack_qty, l.order_currency_code,l.price_type


-- dbo.Dim_Shipment_Qty_Price_New
select * 
from Dim_Shipment_Qty_Price rs
where not exists 
	(Select * from [Phocas_Sales_Actual_Live].dbo.entity_23 pc where pc.LinkField=rs.UniqueIDQP) 



-- dbo.Dim_Shipment_Headers
	SELECT 
		'S'+LEFT(s.document_type, 1) + CAST(CAST(s.document_id  AS INT) AS VARCHAR(255))AS UniqueID, 
		document_no= s.shipment_no, s.document_type,
		s.order_no, 
		s.order_type, s.order_lifecycle, 
		s.business_source, s.business_channel, s.coupon_code,
		ISNULL(s.payment_method,'Unknown') as payment_method, s.shipping_method,
		s.length_of_time_to_invoice, 
		length_of_time_invoice_to_shipment=
			CASE
				WHEN length_of_time_invoice_to_this_shipment >= 0 THEN RIGHT('000'+CAST(length_of_time_invoice_to_this_shipment AS VARCHAR(3)),3)
				ELSE 'N/A'
			END,
		s.shipping_carrier, 
		s.reminder_date, s.reminder_mobile, s.reminder_period, s.reminder_presc, s.reminder_type, 
		s.reorder_on_flag, s.reorder_profile_id, reorder_date=LEFT(CONVERT(VARCHAR(20),s.reorder_date, 120),10), s.reorder_interval, 
		s.presc_verification_method, 
		s.referafriend_code, s.referafriend_referer,
		CASE DATEPART(dw, s.document_date) 
		  WHEN 1 THEN '7: Sunday' WHEN 2 THEN '1: Monday' WHEN 3 THEN '2: Tuesday' WHEN 4 THEN '3: Wednesday'
		  WHEN 5 THEN '4: Thursday' WHEN 6 THEN '5: Friday' WHEN 7 THEN '6: Saturday'
		END AS document_day,
		CAST(s.document_date AS TIME) AS document_time, DATEPART(HOUR, s.document_date) AS document_hour, CAST(s.document_date AS DATE) AS document_date,
		ISNULL(LEFT(CONVERT(VARCHAR(30), customer_first_order_date,120),7),'2001-01') AS customer_first_order_month, 
		customer_order_seq_no=RIGHT('0000'+CAST(ISNULL(s.customer_order_seq_no,0) AS VARCHAR(4)),4),
		s.customer_business_channel, 
		s.telesales_admin_username   AS 'telesales_agent',
		CASE
			WHEN s.telesales_admin_username IS NULL THEN 'Non-Telesales Order' 
			WHEN (a.first_name + ' ' + a.last_name IS NULL) THEN 'Deleted User'
			ELSE a.first_name + ' ' + a.last_name
		END AS 'agent_name',
		COALESCE(
			CASE WHEN s.cc_type = '' THEN NULL ELSE  s.cc_type END, 
			CASE WHEN p.cc_type = '' THEN NULL ELSE  p.cc_type END, 'Other Payment Methods') AS 'cc_type',

		s.billing_street1 + ' ' + s.billing_city + ' ' + s.billing_region AS 'billing_address', s.billing_postcode, s.billing_country_id, 
		c.country_name, 
		s.shiping_street1 + ' ' + s.shipping_city + ' '  + s.shipping_region AS 'shipping_address', s.shipping_postcode, s.shipping_country_id, 
		c.country_name AS 'Delivery Country',
		cast(s.order_date as date) as order_date, cast(s.invoice_date as date) as invoice_date, case when s_no=1 then cast(s.shipment_date as date) else null end as shipment_date,
		format(s.length_of_time_to_invoice,'000') as days_to_invoice, 
		case when s_no=1 then format(datediff(day,s.order_date,s.shipment_date),'000') else null end as days_to_shipment,
		case when s_no=1 then format(datediff(day,s.invoice_date,s.shipment_date),'000') else null end as days_invoice_to_shipment

	FROM  
			(select sh.*, 
				row_number() over (partition by order_id order by shipment_date desc) s_no 
			from dw_getlenses.dbo.shipment_headers sh 
			where exists
				(select * from dw_getlenses.dbo.stores where stores.store_name=sh.store_name)
			union all
			select sh.*,
				row_number() over (partition by order_id order by shipment_date desc) s_no 
			from dw_proforma.dbo.shipment_headers sh 
			where exists
				(select * from dw_proforma.dbo.stores where stores.store_name=sh.store_name and stores.store_name <>'visiooptik.fr') 
				and sh.document_date >='01-feb-2014') s 
		LEFT JOIN 
			dw_getlenses.dbo.countries c ON billing_country_id = c.country_code
		LEFT JOIN 
			dw_getlenses.dbo.admin_user a ON telesales_admin_username = a.[user_name]
		LEFT JOIN 
			Dim_Sales_Flat_Order_Payment p ON s.order_id = p.parent_id

-- dbo.Dim_Shipment_Headers_New
select * 
from Dim_Shipment_Headers rs
where not exists 
	(Select * from [Phocas_Sales_Actual_Live].dbo.entity_11 pc where pc.LinkField=rs.UniqueID)

---------------------------------------------------------------------------------------------------

-- dbo.Fact_Shipments

	SELECT 
		h.store_name, cast(l.product_id as numeric(10,0)) as product_id,  
		cast(h.customer_id as int) as 'customer_id', 
		'S' + LEFT(h.document_type, 1) + cast(CAST(h.document_id as int) AS varchar(255)) AS UniqueID
		h.shipping_country_id, 
		l.qty, 
		l.global_prof_fee, 
        l.global_line_subtotal_inc_vat, l.global_line_subtotal_vat, l.global_line_subtotal_exc_vat, 
        l.global_shipping_inc_vat, l.global_shipping_vat, l.global_shipping_exc_vat, 
		l.global_discount_inc_vat, l.global_discount_vat, l.global_discount_exc_vat, 
		l.global_store_credit_inc_vat, l.global_store_credit_vat, l.global_store_credit_exc_vat, 
		l.global_adjustment_inc_vat, l.global_adjustment_vat, l.global_adjustment_exc_vat, 
		l.global_line_total_inc_vat, l.global_line_total_vat, l.global_line_total_exc_vat, 
		l.global_subtotal_cost, l.global_shipping_cost, l.global_total_cost, 
		l.global_margin_amount, 
		l.local_prof_fee, 
		l.local_line_subtotal_inc_vat, l.local_line_subtotal_vat, l.local_line_subtotal_exc_vat, 
		l.local_shipping_inc_vat, l.local_shipping_vat, l.local_shipping_exc_vat, 
		l.local_discount_inc_vat, l.local_discount_vat, l.local_discount_exc_vat, 
		l.local_store_credit_inc_vat, l.local_store_credit_vat, l.local_store_credit_exc_vat, 
		l.local_adjustment_inc_vat, l.local_adjustment_vat, l.local_adjustment_exc_vat, 
		l.local_line_total_inc_vat, l.local_line_total_vat, l.local_line_total_exc_vat, 
		l.local_subtotal_cost, l.local_shipping_cost, l.local_total_cost, 
		l.local_margin_amount, 
        l.local_to_global_rate, 
		cast(h.document_date as date) as document_date, '' AS invoice_no, h.document_type, h.order_no, 
		h.shipping_carrier, 
		h.reminder_date, h.reminder_mobile, 
		h.reminder_period,
        h.reminder_presc, h.reminder_type, 
		h.reorder_on_flag, h.reorder_profile_id, h.reorder_date, h.reorder_interval, 
		h.presc_verification_method, 
		h.referafriend_code, h.referafriend_referer, 
		l.lens_eye, l.lens_base_curve, l.lens_diameter, l.lens_power, l.lens_cylinder, l.lens_axis, l.lens_addition, l.lens_dominance, l.lens_colour, 
        l.lens_days, l.sku, 
		l.global_line_total_exc_vat AS global_line_total_exc_vat_for_localtoglobal, l.local_line_total_exc_vat AS local_line_total_exc_vat_for_localtoglobal, 
		l.line_weight,
		(cast(1 as decimal(12,4)) / ls.row_count)AS document_count, (cast(1 as decimal(12,4)) / ps.row_count) AS product_count,
		'O' + LEFT(l.document_type, 2) + Cast(l.document_id AS varchar) + '-' + cast(l.product_id as varchar) + '-' + isnull(CAST(abs(l.qty) AS varchar),'0.0000') AS UniqueIDQP,
		CASE WHEN l.qty > 0 THEN 1 WHEN l.qty = 0 THEN 0 WHEN l.qty < 0 THEN -1 END AS line_count,
		length_of_time_to_invoice = 0, length_of_time_invoice_to_shipment = 0,
		cast((sku+'-LQ' + isnull(CAST(cast(abs(l.qty) as integer) AS varchar),'0')) as varchar(255)) as sku_for_parameters,
		CASE datepart(dw, h.document_date) 
			WHEN 1 THEN '7: Sunday' WHEN 2 THEN '1: Monday' WHEN 3 THEN '2: Tuesday' WHEN 4 THEN '3: Wednesday'
			WHEN 5 THEN '4: Thursday' WHEN 6 THEN '5: Friday' WHEN 7 THEN '6: Saturday'
		END as document_day,
		cast(h.document_date as time) as document_time, datepart(hour, h.document_date) as document_hour
FROM        
		dw_getlenses.dbo.shipment_headers h 
	INNER JOIN 
		dw_getlenses.dbo.shipment_lines l ON h.document_type = l.document_type AND h.document_id = l.document_id
	LEFT JOIN 
		dw_getlenses.dbo.dw_shipment_line_sum ls ON ls.document_id = l.document_id AND ls.document_type = l.document_type
	LEFT JOIN 
		dw_getlenses.dbo.dw_shipment_line_product_sum ps ON ps.document_id =  l.document_id AND ps.document_type = l.document_type AND ps.product_id =  l.product_id
								  
where h.document_date <
	(select cast(current_date_time as date) 
	from dw_sales_actual.dbo.current_date_time) and 
	exists (select * from DW_GetLenses.dbo.stores where stores.store_name =l.store_name ) 
union all
		h.store_name, cast(l.product_id as numeric(10,0)) as product_id,  
		cast(h.customer_id as int) as 'customer_id', 
		'S' + LEFT(h.document_type, 1) + cast(CAST(h.document_id as int) AS varchar(255)) AS UniqueID
		h.shipping_country_id, 
		l.qty, 
		l.global_prof_fee, 
        l.global_line_subtotal_inc_vat, l.global_line_subtotal_vat, l.global_line_subtotal_exc_vat, 
        l.global_shipping_inc_vat, l.global_shipping_vat, l.global_shipping_exc_vat, 
		l.global_discount_inc_vat, l.global_discount_vat, l.global_discount_exc_vat, 
		l.global_store_credit_inc_vat, l.global_store_credit_vat, l.global_store_credit_exc_vat, 
		l.global_adjustment_inc_vat, l.global_adjustment_vat, l.global_adjustment_exc_vat, 
		l.global_line_total_inc_vat, l.global_line_total_vat, l.global_line_total_exc_vat, 
		l.global_subtotal_cost, l.global_shipping_cost, l.global_total_cost, 
		l.global_margin_amount, 
		l.local_prof_fee, 
		l.local_line_subtotal_inc_vat, l.local_line_subtotal_vat, l.local_line_subtotal_exc_vat, 
		l.local_shipping_inc_vat, l.local_shipping_vat, l.local_shipping_exc_vat, 
		l.local_discount_inc_vat, l.local_discount_vat, l.local_discount_exc_vat, 
		l.local_store_credit_inc_vat, l.local_store_credit_vat, l.local_store_credit_exc_vat, 
		l.local_adjustment_inc_vat, l.local_adjustment_vat, l.local_adjustment_exc_vat, 
		l.local_line_total_inc_vat, l.local_line_total_vat, l.local_line_total_exc_vat, 
		l.local_subtotal_cost, l.local_shipping_cost, l.local_total_cost, 
		l.local_margin_amount, 
        l.local_to_global_rate, 
		cast(h.document_date as date) as document_date, '' AS invoice_no, h.document_type, h.order_no, 
		h.shipping_carrier, 
		h.reminder_date, h.reminder_mobile, 
		h.reminder_period,
        h.reminder_presc, h.reminder_type, 
		h.reorder_on_flag, h.reorder_profile_id, h.reorder_date, h.reorder_interval, 
		h.presc_verification_method, 
		h.referafriend_code, h.referafriend_referer, 
		l.lens_eye, l.lens_base_curve, l.lens_diameter, l.lens_power, l.lens_cylinder, l.lens_axis, l.lens_addition, l.lens_dominance, l.lens_colour, 
        l.lens_days, l.sku, 
		l.global_line_total_exc_vat AS global_line_total_exc_vat_for_localtoglobal, l.local_line_total_exc_vat AS local_line_total_exc_vat_for_localtoglobal, 
		l.line_weight,
		(cast(1 as decimal(12,4)) / ls.row_count)AS document_count, (cast(1 as decimal(12,4)) / ps.row_count) AS product_count,
		'O' + LEFT(l.document_type, 2) + Cast(l.document_id AS varchar) + '-' + cast(l.product_id as varchar) + '-' + isnull(CAST(abs(l.qty) AS varchar),'0.0000') AS UniqueIDQP,
		CASE WHEN l.qty > 0 THEN 1 WHEN l.qty = 0 THEN 0 WHEN l.qty < 0 THEN -1 END AS line_count,
		length_of_time_to_invoice = 0, length_of_time_invoice_to_shipment = 0,
		cast((sku+'-LQ' + isnull(CAST(cast(abs(l.qty) as integer) AS varchar),'0')) as varchar(255)) as sku_for_parameters,
		CASE datepart(dw, h.document_date) 
			WHEN 1 THEN '7: Sunday' WHEN 2 THEN '1: Monday' WHEN 3 THEN '2: Tuesday' WHEN 4 THEN '3: Wednesday'
			WHEN 5 THEN '4: Thursday' WHEN 6 THEN '5: Friday' WHEN 7 THEN '6: Saturday'
		END as document_day,
		cast(h.document_date as time) as document_time, datepart(hour, h.document_date) as document_hour
FROM				
		dw_proforma.dbo.shipment_headers h 
	INNER JOIN 
		dw_proforma.dbo.shipment_lines l ON h.document_type = l.document_type AND h.document_id = l.document_id and h.store_name =l.store_name 
	LEFT JOIN 
		dw_proforma.dbo.dw_shipment_line_sum ls ON ls.document_id = l.document_id AND ls.document_type = l.document_type
	LEFT JOIN 
		dw_proforma.dbo.dw_shipment_line_product_sum ps ON ps.document_id =  l.document_id AND ps.document_type = l.document_type AND ps.product_id =  l.product_id
where h.document_date>='01-feb-2014' and 
	h.document_date < (select cast(current_date_time as date) from dw_sales_actual.dbo.current_date_time) and 
	exists (select * from DW_Proforma.dbo.stores where stores.store_name =h.store_name and stores.store_name <>'visiooptik.fr') 

-- dbo.Fact_Shipments_Un
	select * 
	from Fact_shipments 
	where document_date>(select max(maxdate) from [Phocas_Sales_Actual_Live].dbo.streams where id in (5,6))

