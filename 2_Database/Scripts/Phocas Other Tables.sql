
-- Batches
select top 5 *
from Phocas_Sales_Actual_Live.dbo.Batches

-- Branch
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Branch

-- CategoryType
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.CategoryType

-- Functions
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Functions

-- Images
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Images

-- Options
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Options

-- QuantaCategoryHeader
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.QuantaCategoryHeader

-- Scalar
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Scalar

-- Streams
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Streams

-- Summary Commands
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.SummaryCommands

-- TempBulkTable
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.TempBulkTable

-- UserSession
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.UserSession

--------------------------------------------------------------------------------------

-- Period
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Period

-- Period Type
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodType

-- Period 0
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_0

-- Period 1
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_1

-- Period Day
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Day

-- Period Month
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Month

-- Period Year
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Year


--------------------------------------------------------------------------------------

-- Quanta
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.quanta

-- QuantaB
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.quantaB

