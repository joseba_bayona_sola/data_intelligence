
select top 1000 UniqueID, 
	doh.order_no, 
	document_no, document_type,
	document_date, document_day, document_time, document_hour,
	order_date, 
	invoice_date, days_to_invoice, 
	shipment_date, days_to_shipment, days_invoice_to_shipment
		
	order_type, order_lifecycle, 
	business_source, business_channel, customer_business_channel, coupon_code,
	telesales_agent, agent_name,
	payment_method, shipping_method, shipping_carrier, 
	customer_first_order_month, customer_order_seq_no,

	length_of_time_to_invoice, length_of_time_invoice_to_shipment,
	
	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer,

	cc_type, 
	billing_address, billing_postcode, billing_country_id, country_name, 
	shipping_address, shipping_postcode, shipping_country_id, [Delivery Country], 

	presc_verification_method

from 
		DW_Sales_Actual.dbo.Dim_Order_Headers doh
	inner join 
		(select order_no
		from DW_GetLenses.dbo.order_headers
		where customer_id = 867400) oh on doh.order_no = oh.order_no
order by order_date desc;

--where order_no = '17000271903';

select top 1000 
	store_name, customer_id, product_id,
	UniqueID, UniqueIDQP, 
	order_no, invoice_no, 
	
	shipping_country_id, 

	document_type, document_date, document_day, document_time, document_hour, 
	
	shipping_carrier, 
	length_of_time_to_invoice, length_of_time_invoice_to_shipment, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer, 
	presc_verification_method, 

	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	line_weight, 
	sku, sku_for_parameters, 

	qty, 
	local_prof_fee, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, 

	global_prof_fee, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount,

	local_to_global_rate, 
	global_line_total_exc_vat_for_localtoglobal, local_line_total_exc_vat_for_localtoglobal, 

	document_count, product_count, line_count

from DW_Sales_Actual.dbo.Fact_Orders
--where order_no = '17000271903';
where customer_id = 867400;

-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------

