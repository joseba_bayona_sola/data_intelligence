
-- Wearer
select top 1000 
	id, name, 
	dob, 
	customer_id
from DW_GetLenses.dbo.dw_wearer

-- Wearer Order Item
select top 1000 
	id, 
	order_id, item_id, quote_item_id, wearer_prescription_id, 
	prescription_flag, 
	item_data, comment
from DW_GetLenses.dbo.dw_wearer_order_item

	select prescription_flag, count(*)
	from DW_GetLenses.dbo.dw_wearer_order_item
	group by prescription_flag
	order by prescription_flag;

-- Wearer Prescription
select top 1000 
	id, 
	wearer_id, method_id, 
	date_last_prescription, date_start_prescription, 
	optician_name, optician_address1, optician_address2, 
	optician_city, optician_state, optician_country, optician_postcode, optician_phone, 
	optician_lookup_id, 
	reminder_sent

from DW_GetLenses.dbo.dw_wearer_prescription

