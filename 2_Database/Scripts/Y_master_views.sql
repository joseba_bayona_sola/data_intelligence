
-- dbo.Dim_Stores_All 
	select *,
		case 
			when store_type='Core UK' and tld<>'.co.uk' then 'gbp'
			when store_type='White Label' then 'wl'
			else '' end +TLD as code_tld

	from 
		(select *,
			case 
				when website like '%.co.uk' then '.co.uk'
				else right(website,charindex('.',reverse(website),1)) 
			end as TLD 
		from
			(select distinct store_name, website_group, store_type,
				left(store_name,case when charindex('/',store_name,1)=0 then len(store_name) else charindex('/',store_name,1)-1 end) as website,
				store_id
			from
				(select * from dw_getlenses.dbo.dw_stores_full where visible=1
				union
				select * from dw_proforma.dbo.dw_stores_full where visible=1) rs) rs) rs

-- dbo.Dim_Stores_All_New
	select * 
	from Dim_Stores_All rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_1 pc where pc.LinkField=rs.store_name)



-- dbo.Dim_Categories
	SELECT [sort] = min(right('000'+cast(category_id as varchar(3)),3)), category
	FROM dw_getlenses.dbo.categories
	group by category

-- dbo.Dim_Categories_New
	select * 
	from Dim_Categories rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_7 pc where pc.linkfield=rs.sort)


--dbo.Dim_All_Categories
	select * 
	from dw_getlenses.dbo.categories

-- dbo.Dim_All_Categories_New
	select * 
	from dw_getlenses.dbo.categories rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_9 pc where pc.LinkField=rs.modality)



-- dbo.Dim_Products
	select Products.*, 
		categories.category as category, categories.feature as feature, categories.type as LensType, categories.modality as modality, 
		Dim_Categories.sort as category_sort
	from 
			dw_getlenses.dbo.products
		left outer join 
			dw_getlenses.dbo.categories on products.category_id = categories.category_id
		inner join 
			Dim_Categories on categories.category = Dim_Categories.category
	where products.product_type not like 'glasses%'
		and store_name='Default'

-- [dbo].[Dim_Products_New] as 
	select * 
	from Dim_Products rs
	where not exists
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_5 pc where pc.LinkField=rs.product_id)


-- dbo.Dim_Countries
	SELECT 
		country_name, country_code, 
		country_type, country_zone, country_continent, country_state  
	FROM [DW_GetLenses].[dbo].[countries]
	union
	SELECT 'Yugoslavia','YU','EU','EUR','Europe','State'
	union
	SELECT 'East Timor','TP','EU','EUR','Europe','Territory'
	order by country_type, country_zone, country_continent, country_name

-- dbo.Dim_Countries_New
	select * 
	from Dim_Countries rs 
	where not exists 
		(select * from [Phocas_Sales_Actual_Live].[dbo].[Entity_31] pc where pc.linkfield=rs.country_code)


-- dbo.Dim_Business_Channels
	select business_channel 
	from dw_getlenses.dbo.order_headers oh 
	where exists 
		(select * from dw_getlenses.dbo.stores where stores.store_name =oh.store_name)
	union
	select business_channel 
	from DW_Proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr')
		and oh.document_date >='01-feb-2014'

-- dbo.Dim_Business_Channels_New
	select * 
	from Dim_Business_Channels rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_15 pc where pc.LinkField=isnull(rs.business_channel,''))


--dbo.Dim_Coupon_Codes
	select 
		coupon_code = h.coupon_code,
		description = isnull(c.description,''),
		start_date = cast(isnull(c.start_date, DATEFROMPARTS(2010,01,01)) as date), end_date = cast(isnull(c.end_date, DATEFROMPARTS(2099,12,31)) as date),
		channel = isnull(c.channel,''), 
		new_customer = isnull(CASE c.new_customer WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END, 'No')
	from 
			(select coupon_code 
			from dw_getlenses.dbo.order_headers h 
			where exists 
				(select * from DW_GetLenses.dbo.stores where stores.store_name=h.store_name) 
			union 
			select coupon_code 
			from dw_proforma.dbo.order_headers h 
			where exists
				(select * from DW_Proforma.dbo.stores where stores.store_name=h.store_name and stores.store_name <>'visiooptik.fr')
				and h.document_date >='01-feb-2014') h 
		left outer join 
			dw_getlenses.dbo.coupon_channel c on h.coupon_code = c.coupon_code
	group by h.coupon_code, c.description, c.start_date, c.end_date, c.channel, c.new_customer
	having h.coupon_code is not null

-- dbo.Dim_Coupon_Codes_new 
	select * 
	from Dim_Coupon_Codes rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_16 pc where pc.linkfield=rs.coupon_code)



-- dbo.Dim_Payment_Methods
	SELECT ISNULL(payment_method,'Unknown') as payment_method 
	FROM dw_getlenses.dbo.order_headers oh 
	where exists 
		(select * from DW_GetLenses.dbo.stores where stores.store_name =oh.store_name ) 
	union
	SELECT ISNULL(payment_method,'Unknown') 
	FROM dw_proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr')
		and oh.document_date>='01-feb-2014' 

-- dbo.Dim_Payment_Methods_New
	SELECT * 
	from Dim_Payment_Methods rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_17 pc where pc.LinkField=rs.payment_method)


-- dbo.Dim_Card_Type
	SELECT 
		COALESCE(CASE WHEN i.cc_type = '' THEN null ELSE  i.cc_type END, CASE WHEN p.cc_type = '' THEN NULL ELSE  p.cc_type END, 'Other Payment Methods') AS 'cc_type'
	FROM 
		dw_getlenses.dbo.invoice_headers i
	left join 
		[dbo].[Dim_Sales_Flat_Order_Payment] p ON i.order_id = p.parent_id
	where exists 
		(select * from dw_getlenses.dbo.stores where stores.store_name =i.store_name)
	union
	SELECT 
		COALESCE(CASE WHEN i.cc_type = '' THEN null ELSE  i.cc_type END, 'Other Payment Methods') AS 'cc_type'
	FROM dw_proforma.dbo.invoice_headers i
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =i.store_name and stores.store_name <>'visiooptik.fr')
		and i.document_date >='01-feb-2014'

-- dbo.Dim_Card_Type_New
	select * 
	from Dim_Card_Type rs 
	where not exists 
		(select * from [Phocas_Sales_Actual_Live].[dbo].[Entity_18] pc where pc.linkfield=rs.cc_type)


-- dbo.Dim_Telesales_Agents
	SELECT DISTINCT 
		o.telesales_admin_username AS 'telesales_admin_username', 
		CASE 
			when o.telesales_admin_username is null then 'Non-Telesales Order'
			WHEN (a.first_name + ' ' + a.last_name is null) then 'Deleted User'
			ELSE a.first_name + ' ' + a.last_name
		END as 'agent_name'
	from 
			dw_getlenses.dbo.order_headers o
		inner join 
			DW_GetLenses.dbo.stores stores on stores.store_name=o.store_name 
		left join 
			dw_getlenses.dbo.admin_user a ON o.telesales_admin_username = a.[user_name]

-- dbo.Dim_Telesales_Agents_New
	SELECT * 
	from Dim_Telesales_Agents rs
	where not exists
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_22 pc where pc.LinkField=isnull(rs.telesales_admin_username,''))



------------------------------------------------------------------------------------------------------

-- dbo.Dim_Customer_Order_Seq_No as
	select customer_order_seq_no='0000'
	union
	select format(isnull(customer_order_seq_no,0),'0000') as customer_order_seq_no 
	from dw_getlenses.dbo.order_headers oh 
	where exists 
		(select * from DW_GetLenses.dbo.stores where stores.store_name =oh.store_name)
	union
	select format(isnull(customer_order_seq_no,0),'0000') as customer_order_seq_no
	from dw_proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr')
		and oh.document_date>='01-feb-2014'

-- dbo.Dim_Customer_Order_Seq_No_New as
	select * 
	from Dim_Customer_Order_Seq_No rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_14 pc where pc.LinkField=rs.customer_order_seq_no)


-- dbo.Dim_Customer_Channels_New
	select * 
	from Dim_Business_Channels rs
	where not exists 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_29 pc where pc.LinkField=isnull(rs.business_channel,''))



-- dbo.Dim_Days_to_Desptach
	select distinct [days_to_shipment] 
	from Dim_shipment_headers
	UNION 
	select 'N/A'

-- dbo.Dim_Days_to_Desptach_New
	select * 
	from Dim_Days_to_Desptach rs
	where not exists
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_21 pc where pc.LinkField=isnull(rs.days_to_shipment,''))


-- dbo.Dim_First_Order_Month 
	select distinct customer_first_order_month
	from Dim_order_headers

-- dbo.Dim_First_Order_Month_New
select * 
from Dim_First_Order_Month rs
where not exists
	(Select * from [Phocas_Sales_Actual_Live].dbo.entity_30 pc where pc.LinkField=rs.customer_first_order_month)




-- dbo.Dim_Sales_Flat_Order_Payment
SELECT
	[entity_id], [parent_id],
	[cc_cid_status], [cc_type],
	[ideal_issuer_id], [po_number], [echeck_account_name],
	[cc_avs_status], [cc_number_enc], [cc_trans_id]
  FROM dw_getlenses.[dbo].[sales_flat_order_payment]
  where cc_type IS NOT NULL