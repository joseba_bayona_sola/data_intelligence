
select top 100 order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.Order_Headers

select top 100 order_lifecycle, customer_order_seq_no, count(*)
from DW_GetLenses.dbo.Order_Headers
where order_lifecycle = 'New'
group by order_lifecycle, customer_order_seq_no
order by order_lifecycle, customer_order_seq_no;

select top 100 order_lifecycle, customer_order_seq_no, count(*)
from DW_GetLenses.dbo.Order_Headers
where customer_order_seq_no = 1
group by order_lifecycle, customer_order_seq_no
order by order_lifecycle, customer_order_seq_no;

-----------------------------------------------------------

select customer_order_seq_no, count(*)
from DW_GetLenses.dbo.Order_Headers
where order_lifecycle = 'New'
	and order_date > getutcdate() - 30
group by customer_order_seq_no
order by customer_order_seq_no

select top 100 customer_id, count(*) num
from DW_GetLenses.dbo.Order_Headers
where order_lifecycle = 'New'
	and order_date > getutcdate() - 30
group by customer_id
order by num desc

select order_id, order_no, 
	store_name, 

	order_type, order_lifecycle, 
	auto_verification, presc_verification_method, 

	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, approved_time, 

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef,
	telesales_method_code, telesales_admin_username, 

	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel, 
	customer_order_seq_no,  
	remote_ip, 

	status, coupon_code, 
	weight, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent,

	referafriend_code, referafriend_referer, 

	shipping_carrier, shipping_method, 

	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, 

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 
	
	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shiping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 
	
	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 
	
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 
	
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 
	
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.Order_Headers
where customer_id in (1619047, 1627433, 447280)
	and order_no = 5001303149
order by customer_id, order_date

select *
from DW_GetLenses.dbo.Order_Lines
where order_no = '5001303149'

-----------------------------------------------------------

select top 100 customer_id, count(*) num
from DW_GetLenses.dbo.Order_Headers
where order_lifecycle = 'New'
group by customer_id
order by num desc