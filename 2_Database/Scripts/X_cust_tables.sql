
-- Customers
--	(business_channel)
--	(store_name, website_id, website_group)
select top 1000 *
from DW_GetLenses.dbo.dw_customers;

select top 1000 *
from DW_GetLenses.dbo.stage_customers;

select top 50 
	customer_id, 
	email, password_hash, 
	prefix, firstname, middlename, lastname, suffix, 
	gender, dob, language, cus_phone, alternate_email, 
	card_expiry_date, default_billing, default_shipping, taxvat,  
	parent_customer_id, is_parent_customer, 

	business_channel, store_name, website_id, website_group, created_in, found_us_info,   
	days_worn, eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer, 
	unsubscribe_all, unsubscribe_all_date, 
	facebook_id, facebook_permissions, 
	referafriend_code, postoptics_send_post, 

	first_order_date, last_logged_in_date, last_order_date, num_of_orders, 

	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, 
	segment_sport, segment_professional, segment_lifestage, segment_vanity, 

	segment, segment_2, segment_3, segment_4, 

	old_access_cust_no, old_web_cust_no, old_customer_id, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 

	created_at, updated_at, 

	count(*) over () num_tot
from DW_GetLenses.dbo.customers;

	select customer_id, count(*) num
	from DW_GetLenses.dbo.customers
	group by customer_id
	order by num desc; 

	select parent_customer_id, is_parent_customer, count(*)
	from DW_GetLenses.dbo.customers
	group by parent_customer_id, is_parent_customer
	order by parent_customer_id, is_parent_customer; 

	select business_channel, count(*)
	from DW_GetLenses.dbo.customers
	group by business_channel
	order by business_channel; 

	select store_name, website_id, website_group, count(*)
	from DW_GetLenses.dbo.customers
	group by store_name, website_id, website_group
	order by store_name, website_id, website_group; 

	select created_in, count(*)
	from DW_GetLenses.dbo.customers
	group by created_in
	order by created_in; 

	select found_us_info, count(*)
	from DW_GetLenses.dbo.customers
	group by found_us_info
	order by found_us_info; 

	select days_worn, count(*)
	from DW_GetLenses.dbo.customers
	group by days_worn
	order by days_worn; 

	select eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer, count(*)
	from DW_GetLenses.dbo.customers
	group by eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer
	order by eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer; 

	select segment_lifecycle, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_lifecycle
	order by segment_lifecycle; 

	select segment_geog, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_geog
	order by segment_geog; 

	select segment_usage,  segment_purch_behaviour, segment_eysight, 
		segment_sport, segment_professional, segment_lifestage, segment_vanity, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_usage,  segment_purch_behaviour, segment_eysight, 
		segment_sport, segment_professional, segment_lifestage, segment_vanity
	order by segment_usage,  segment_purch_behaviour, segment_eysight, 
		segment_sport, segment_professional, segment_lifestage, segment_vanity; 

	select top 1000 segment, count(*)
	from DW_GetLenses.dbo.customers
	group by segment
	order by segment; 

	select top 1000 segment_2, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_2
	order by segment_2; 

	select top 1000 segment_3, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_3
	order by segment_3; 

	select segment_4, count(*)
	from DW_GetLenses.dbo.customers
	group by segment_4
	order by segment_4; 
	
-- Customers Address
--	(customer_id)
--	(entity_type_id, attribute_set_id)

select top 1000 *
from DW_GetLenses.dbo.dw_customer_addresses;

select top 1000 *
from DW_GetLenses.dbo.stage_customer_addresses;

select top 1000 
	address_id, 
	entity_type_id, attribute_set_id, 
	address_no, 
	customer_id, 
	prefix, firstname, middlename, lastname, suffix, 
	company, 
	street, city, country_id, region, region_id, postcode, telephone, fax, 
	cybersource_version, 
	order_address_id, quote_address_id, 
	dw_att, dw_type, dw_synced, dw_synced_at, dw_proc,

	created_at, updated_at, is_active,  

	count(*) over () num_tot	
from DW_GetLenses.dbo.customer_addresses;

	select entity_type_id, count(*)
	from DW_GetLenses.dbo.customer_addresses
	group by entity_type_id
	order by entity_type_id;

	select attribute_set_id, count(*)
	from DW_GetLenses.dbo.customer_addresses
	group by attribute_set_id
	order by attribute_set_id;

	select cybersource_version, count(*)
	from DW_GetLenses.dbo.customer_addresses
	group by cybersource_version
	order by cybersource_version;

	select order_address_id, quote_address_id, count(*)
	from DW_GetLenses.dbo.customer_addresses
	group by order_address_id, quote_address_id
	order by order_address_id, quote_address_id;

	select dw_att, dw_type, dw_synced, dw_synced_at, dw_proc, count(*)
	from DW_GetLenses.dbo.customer_addresses
	group by dw_att, dw_type, dw_synced, dw_synced_at, dw_proc
	order by dw_att, dw_type, dw_synced, dw_synced_at, dw_proc;

-- Log Customers
select top 1000 *
from DW_GetLenses.dbo.dw_log_customer;

select top 1000 *
from DW_GetLenses.dbo.stage_log_customer;

select top 1000 log_id, 
	visitor_id, customer_id, 
	login_at, logout_at, 
	store_id
from DW_GetLenses.dbo.log_customer;

	select count(*), min(login_at), max(login_at)
	from DW_GetLenses.dbo.log_customer;

-- Binned Customers
select top 1000 entity_id, website_group_id
from DW_GetLenses.dbo.dw_binned_customers;

-- Bounces
select top 1000 
	email, bounceDate
from DW_GetLenses.dbo.bounces;

-- Unsubscribes
select email, 
	unsubscribeDate
from DW_GetLenses.dbo.unsubscribes;