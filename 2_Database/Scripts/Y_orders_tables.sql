

-- Order Types
select order_type
from DW_Sales_Actual.dbo.Dim_Order_Types
order by order_type;

select order_type
from DW_Sales_Actual.dbo.Dim_Order_Types_new

-- Order LifeCycles
select order_lifecycle
from DW_Sales_Actual.dbo.Dim_Order_Lifecycles
order by order_lifecycle

select order_lifecycle
from DW_Sales_Actual.dbo.Dim_Order_Lifecycles_new

-- Order Parameters
select TOP 100 product_id, name, 
	sku, product_code, 
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
	--,count(*) over () num
from DW_Sales_Actual.dbo.Dim_Order_Parameters
--where product_id = 1083
--where lens_cylinder <> ''
--where lens_addition <> ''
--where lens_dominance <> ''
--where lens_colour <> ''
order by product_id, product_code

	select TOP 100 c.product_type, c.modality, c.type, c.feature,
		op.product_id, op.name, 
		op.sku, op.product_code, 
		op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour, 
		op.qty
		--,count(*) over () num
	from 
			DW_Sales_Actual.dbo.Dim_Order_Parameters op
		inner join
			DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
		inner join 
			DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
	--where op.product_id = 1083
	--where lens_cylinder <> ''
	--where lens_axis <> ''

	--where lens_addition <> ''
	--where lens_dominance <> ''
	
	--where lens_colour <> ''
	where c.product_type = 'Glasses'
	order by op.product_id, op.product_code

	select TOP 100 c.product_type, c.modality, c.type, c.feature, count(*)
	from 
			DW_Sales_Actual.dbo.Dim_Order_Parameters op
		inner join
			DW_Sales_Actual.dbo.Dim_Products p on op.product_id = p.product_id
		inner join 
			DW_Sales_Actual.dbo.Dim_All_Categories c on p.category_id = c.category_id
	--where lens_cylinder <> '' --> Spherical?, {Promo} Other?, Other? 
	--where lens_axis <> '' --> Spherical?, {Promo} Other?, Other? 

	--where lens_addition <> '' --> Toric?, {Promo} Other?, Other?
	--where lens_dominance <> '' --> Toric?, {Promo} Other?
	
	--where lens_colour <> '' --> Solutions & Eye Care?, Other AND feature not correctly
	group by c.product_type, c.modality, c.type, c.feature
	order by c.product_type, c.modality, c.type, c.feature



	select product_id, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by product_id
	order by product_id

	select lens_base_curve, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_base_curve
	order by lens_base_curve

	select lens_diameter, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_diameter
	order by lens_diameter

	select replace(lens_power, Char(13), '') , count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_power
	order by lens_power
	
	select qty, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by qty
	order by qty

	select lens_cylinder, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_cylinder
	order by lens_cylinder

	select lens_axis, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_axis
	order by lens_axis

	select lens_addition, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_addition
	order by lens_addition

	select lens_dominance, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_dominance
	order by lens_dominance

	select lens_colour, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_colour
	order by lens_colour


-- Order Qty Price 
select TOP 100 UniqueIDQP, 
	product_id, name, 
	name_qty, name_packprice, name_pricetype, 
	qty, pack_price, price_type, 
	lines, 
	pack_qty, total_qty
	--,count(*) over () num
from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
where product_id = 1083
order by product_id, qty, price_type, pack_price

	select product_id, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
	group by product_id
	order by product_id;

	select qty, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
	group by qty
	order by qty;

	select price_type, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
	group by price_type
	order by price_type;

	select lines, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
	group by lines
	order by lines;

	select pack_qty, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
	group by pack_qty
	order by pack_qty;



-- Order Header
select top 1000 UniqueID, 
	order_no, 
	document_no, document_type,
	document_date, document_day, document_time, document_hour,
	order_date, 
	invoice_date, days_to_invoice, 
	shipment_date, days_to_shipment, days_invoice_to_shipment
		
	order_type, order_lifecycle, 
	business_source, business_channel, customer_business_channel, coupon_code,
	telesales_agent, agent_name,
	payment_method, shipping_method, shipping_carrier, 
	customer_first_order_month, customer_order_seq_no,

	length_of_time_to_invoice, length_of_time_invoice_to_shipment,
	
	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer,

	cc_type, 
	billing_address, billing_postcode, billing_country_id, country_name, 
	shipping_address, shipping_postcode, shipping_country_id, [Delivery Country], 

	presc_verification_method

from DW_Sales_Actual.dbo.Dim_Order_Headers


---------------------------------------------------------------------------------------------------

-- Order Facts
select top 1000 
	store_name, customer_id, product_id,
	UniqueID, UniqueIDQP, 
	order_no, invoice_no, 
	
	shipping_country_id, 

	document_type, document_date, document_day, document_time, document_hour, 
	
	shipping_carrier, 
	length_of_time_to_invoice, length_of_time_invoice_to_shipment, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer, 
	presc_verification_method, 

	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	line_weight, 
	sku, sku_for_parameters, 

	qty, 
	local_prof_fee, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, 

	global_prof_fee, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount,

	local_to_global_rate, 
	global_line_total_exc_vat_for_localtoglobal, local_line_total_exc_vat_for_localtoglobal, 

	document_count, product_count, line_count

from DW_Sales_Actual.dbo.Fact_Orders
