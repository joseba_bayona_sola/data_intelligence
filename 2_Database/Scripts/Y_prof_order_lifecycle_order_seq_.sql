
select top 1000 
	store_name, customer_id, product_id,
	fo.UniqueID, UniqueIDQP, 
	fo.order_no, invoice_no, 
	
	oh.order_lifecycle, oh.customer_order_seq_no,

	fo.shipping_country_id, 

	fo.document_type, fo.document_date, fo.document_day, fo.document_time, fo.document_hour, 
	
	fo.shipping_carrier, 
	fo.length_of_time_to_invoice, fo.length_of_time_invoice_to_shipment, 

	fo.reorder_on_flag, fo.reorder_profile_id, fo.reorder_date, fo.reorder_interval, 
	fo.reminder_type, fo.reminder_date, fo.reminder_period, fo.reminder_presc, fo.reminder_mobile, 
	fo.referafriend_code, fo.referafriend_referer, 
	fo.presc_verification_method, 

	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	line_weight, 
	sku, sku_for_parameters, 

	qty, 
	local_prof_fee, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, 

	global_prof_fee, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount,

	local_to_global_rate, 
	global_line_total_exc_vat_for_localtoglobal, local_line_total_exc_vat_for_localtoglobal, 

	document_count, product_count, line_count

from 
		DW_Sales_Actual.dbo.Fact_Orders fo
	inner join 	
		DW_Sales_Actual.dbo.Dim_Order_Headers oh on fo.UniqueID = oh.UniqueID
where customer_id in (1619047, 1627433)
--where fo.uniqueID = 'OCA4415917'
order by order_date

select *
from DW_Sales_Actual.dbo.Fact_Orders
where order_no = '5001303149'
