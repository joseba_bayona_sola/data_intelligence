
-- Customers 
select count(*), 
	min(created_at), max(created_at)
from DW_GetLenses.dbo.customers;

select count(*), 
	min(created_at), max(created_at)
from DW_Proforma.dbo.customers;

-- Orders
select count(*), 
	min(order_date), max(order_date)
from DW_GetLenses.dbo.order_headers;

select count(*), 
	min(order_date), max(order_date)
from DW_Proforma.dbo.order_headers;


-- Invoices
select count(*), 
	min(order_date), max(order_date)
from DW_GetLenses.dbo.invoice_headers;

select count(*), 
	min(order_date), max(order_date)
from DW_Proforma.dbo.invoice_headers;


-- Shipments
select count(*), 
	min(order_date), max(order_date)
from DW_GetLenses.dbo.shipment_headers;

select count(*), 
	min(order_date), max(order_date)
from DW_Proforma.dbo.shipment_headers;



