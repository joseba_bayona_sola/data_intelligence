

select top 100000 
	order_id, order_no, 
	store_name, 

	order_type, order_lifecycle, customer_order_seq_no,  
	auto_verification, presc_verification_method, 

	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, approved_time, 

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef,
	telesales_method_code, telesales_admin_username, 

	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel, 
	remote_ip, 

	status, coupon_code, 
	weight, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent,

	referafriend_code, referafriend_referer, 

	shipping_carrier, shipping_method, 

	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, 

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 
	
	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shiping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 
	
	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 
	
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 
	
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 
	
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from order_headers
--where order_date > GETUTCDATE() - 7
--where order_id = 4495297
where customer_id = 867400
order by order_date desc;

select top 1000 
	line_id, 
	ol.order_id, order_no, store_name, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from 
		DW_GetLenses.dbo.order_lines ol
	inner join 
		(select order_id
		from DW_GetLenses.dbo.order_headers
		where customer_id = 867400) oh on ol.order_id = oh.order_id
--where order_id = 4495297;
order by order_date desc, line_id;

-------------------------------------------------------------------------------------

select top 1000 
	invoice_id, invoice_no, 
	store_name, 

	order_type, order_lifecycle, 
	auto_verification, presc_verification_method, 

	document_id, document_type, document_date, document_updated_at, document_no, 
	invoice_date, updated_at, approved_time, 
	length_of_time_to_invoice

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef, 
	telesales_method_code, telesales_admin_username, 

	ih.order_id, order_no, order_date, 

	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel
	customer_order_seq_no, 
	remote_ip, 

	status, coupon_code, 
	weight,

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent, 

	referafriend_code, referafriend_referer, 
	
	shipping_carrier, shipping_method, 
	 
	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, 

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 

	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 

	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from 
		DW_GetLenses.dbo.invoice_headers ih
	inner join 
		(select order_id
		from DW_GetLenses.dbo.order_headers
		where customer_id = 867400) oh on ih.order_id = oh.order_id
--where order_id = 4495297;
order by order_date desc;

select top 1000 
	line_id, 
	il.invoice_id, store_name, invoice_no, 

	document_id, document_type, document_date, document_updated_at, 
	invoice_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join 
		(select invoice_id
		from 
			DW_GetLenses.dbo.invoice_headers ih
		inner join 
			(select order_id
			from DW_GetLenses.dbo.order_headers
			where customer_id = 867400) oh on ih.order_id = oh.order_id) ih on il.invoice_id = ih.invoice_id
--where invoice_id = 3918138;
order by invoice_date desc, line_id;

-------------------------------------------------------------------------------------

select top 1000 
	shipment_id, shipment_no,  
	store_name, 

	order_type, order_lifecycle,
	auto_verification, presc_verification_method,

	document_id, document_type, document_date, document_updated_at, 
	shipment_date, updated_at, approved_time, 
	length_of_time_to_invoice, length_of_time_invoice_to_this_shipment, 

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef,
	telesales_method_code, telesales_admin_username, 

	invoice_id, invoice_no, invoice_date,
	sh.order_id, order_no, order_date, 
	shipment_no_of_order, 
			
	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel, 
	customer_order_seq_no,  
	remote_ip, 
	
	status, coupon_code, 
	weight,  

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent,

	referafriend_code, referafriend_referer, 

	shipping_carrier, shipping_method, 

	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5,

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 

	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shiping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 

	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from 
		DW_GetLenses.dbo.shipment_headers sh
	inner join 
		(select order_id
		from DW_GetLenses.dbo.order_headers
		where customer_id = 867400) oh on sh.order_id = oh.order_id
--where order_id = 4495297;
order by shipment_date desc;

select top 1000 line_id, 
	sl.shipment_id, store_name, shipment_no, 

	document_id, document_type, document_date, document_updated_at, 
	shipment_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, qty_backordered, 

	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from 
		DW_GetLenses.dbo.shipment_lines sl
	inner join 
		(select sh.shipment_id
		from 
				DW_GetLenses.dbo.shipment_headers sh
			inner join 
				(select order_id
				from DW_GetLenses.dbo.order_headers
				where customer_id = 867400) oh on sh.order_id = oh.order_id) sh on sl.shipment_id = sh.shipment_id
order by shipment_date, line_id;