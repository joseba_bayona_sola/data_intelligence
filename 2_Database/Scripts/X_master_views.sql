
-- dbo.V_Stores
	select store_name 
	from [dbo].[stores]
	union
	select code 
	from dw_proforma.dbo.v_stores

-- dbo.V_Stores_All
	select *,
		case 
			when store_type='Core UK' and tld<>'.co.uk' then 'gbp'
			when store_type='White Label' then 'wl'
			else '' 
		end + TLD as code_tld
	from 
		(select *,
			case 
				when website like '%.co.uk' then '.co.uk'
				else right(website,charindex('.',reverse(website),1)) 
			end as TLD 
		from
			(select distinct
				store_name, website_group, store_type,
				left(store_name,case when charindex('/',store_name,1)=0 then len(store_name) else charindex('/',store_name,1)-1 end) as website,
				store_id
			from
				(select * from dw_stores_full where visible=1
				union
				select * from dw_proforma.dbo.dw_stores_full where visible=1) rs) rs) rs


-- dbo.V_Categories
	SELECT 
		[sort] = min(right('000'+cast(category_id as varchar(3)),3)), category
	FROM categories
	group by category
 
-- dbo.V_All_Categories
	select * 
	from categories

-- dbo.V_Products
	select 
		Products.*, 
		categories.category as category, categories.feature as feature, categories.type as LensType, categories.modality as modality,  
		V_Categories.sort as category_sort
	from 
			products
		left outer join 
			categories  on products.category_id = categories.category_id
		inner join 
			V_Categories on categories.category = V_Categories.category
	where 
		products.product_type not like 'glasses%' and 
		store_name='Default'



-- dbo.V_Countries
	SELECT *  
	FROM [countries]
	union
	SELECT 'Yugoslavia','YU','EU','EUR','Europe','State'
	union
	SELECT 'East Timor','TP','EU','EUR','Europe','Territory'


-- dbo.V_Business_Channels 
	select business_channel 
	from order_headers oh 
	where exists 
		(select * from stores where stores.store_name =oh.store_name)
	union
	select business_channel 
	from DW_Proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr') and 
		oh.document_date >='01-feb-2014'

-- dbo.V_Business_Sources
	select business_source 
	from order_headers
	union
	select business_source 
	from DW_Proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr') and 
		oh.document_date >='01-feb-2014'

-- dbo.V_Coupon_Codes
	select 
		coupon_code = h.coupon_code,
		description = isnull(c.description,''),
		start_date = cast(isnull(c.start_date, DATEFROMPARTS(2010,01,01)) as date),
		end_date = cast(isnull(c.end_date, DATEFROMPARTS(2099,12,31)) as date),
		channel = isnull(c.channel,''),
		new_customer = isnull(CASE c.new_customer WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END, 'No')
	from 
			(select coupon_code 
			from order_headers h 
			where exists 
				(select * from stores where stores.store_name=h.store_name) 
			union 
			select coupon_code 
			from dw_proforma.dbo.order_headers h 
			where exists
				(select * from DW_Proforma.dbo.stores where stores.store_name=h.store_name and stores.store_name <>'visiooptik.fr') and 
				h.document_date >='01-feb-2014') h 
		left outer join 
			coupon_channel c on h.coupon_code = c.coupon_code
	group by h.coupon_code, c.description,	c.start_date, c.end_date, c.channel, c.new_customer
	having h.coupon_code is not null

----------------------------------------------------------------

-- dbo.V_Price_Types
	select 'PLA & Discounted' as price_type
	union
	select 'Discounted' as price_type
	union
	select 'PLA' as price_type
	union
	select 'Regular' as price_type

-- dbo.V_Pack_Prices 
	SELECT distinct pack_price 
	from V_Order_Qty_Price

-- dbo.V_Payment_Methods
	SELECT ISNULL(payment_method,'Unknown') as payment_method 
	FROM order_headers oh 
	where exists 
		(select * from stores where stores.store_name =oh.store_name ) 
	union
	SELECT ISNULL(payment_method,'Unknown') 
	FROM dw_proforma.dbo.order_headers oh 
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =oh.store_name and stores.store_name <>'visiooptik.fr')
		and oh.document_date>='01-feb-2014' 

-- dbo.V_Card_Type
	SELECT 
		COALESCE(
			CASE WHEN i.cc_type = '' THEN null ELSE i.cc_type END, 
			CASE WHEN p.cc_type = '' THEN NULL ELSE  p.cc_type END, 
			'Other Payment Methods') AS 'cc_type'
	FROM 
			invoice_headers i
		left join 
			[dbo].[V_Sales_Flat_Order_Payment] p ON i.order_id = p.parent_id
	where exists 
		(select * from stores where stores.store_name =i.store_name)
	union
	SELECT 
		COALESCE(
			CASE WHEN i.cc_type = '' THEN null ELSE i.cc_type END, 
			'Other Payment Methods') AS 'cc_type'
	FROM dw_proforma.dbo.invoice_headers i
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =i.store_name and stores.store_name <>'visiooptik.fr') and 
		i.document_date >='01-feb-2014';
