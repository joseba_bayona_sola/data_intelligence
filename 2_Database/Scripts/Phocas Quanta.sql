
------------------------ QUANTA 
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.quanta

	select top 1000 count(*) -- 37.668.938
	from Phocas_Sales_Actual_Live.dbo.quanta

	-- BatchID
	select top 1000 batchID, count(*) -- 107.382
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by batchID
	order by batchID

	-- StreamsID
	select top 1000 StreamsID, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by StreamsID
	order by StreamsID

	-- moment
	select top 1000 moment, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by moment
	order by moment desc


	-- period_Day
	select top 1000 period_Day, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by period_Day
	order by period_Day desc

	-- period_Month
	select top 1000 period_Month, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by period_Month
	order by period_Month desc

	-- period_Year
	select top 1000 period_Year, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by period_Year
	order by period_Year desc


	-- period_0
	select top 1000 period_0, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by period_0
	order by period_0 desc

	-- period_1
	select top 1000 period_1, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quanta
	group by period_1
	order by period_1 desc

---------------------------------------------------------------------------

------------------------ QUANTA B
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.quantab

	select top 1000 count(*) -- 107.382
	from Phocas_Sales_Actual_Live.dbo.quantab

	-- BatchID
	select top 1000 batchID, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quantab
	group by batchID
	order by batchID

	-- StreamsID
	select top 1000 StreamsID, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quantab
	group by StreamsID
	order by StreamsID

	-- moment
	select top 1000 moment, count(*) 
	from Phocas_Sales_Actual_Live.dbo.quantab
	group by moment
	order by moment

