
-- Branch 
select id, name, 
	parentBranchID, parentPillarID, 
	TrackingGUID,
	format,
	imageID, SortOrder, HideCode, ExternalLink
from Phocas_Sales_Actual_Live.dbo.Branch
order by id;

	select parentBranchID, parentPillarID, count(*)
	from Phocas_Sales_Actual_Live.dbo.Branch
	group by parentBranchID, parentPillarID
	order by parentBranchID, parentPillarID

-- Category Type
select id, name, 
	branchID, 
	TrackingGUID,
	format, FormatStr, textStyle,
	DisplayColumn, DisplayRow, DisplayWidth, DisplayHeight, 
	SelectOrder, SortOrder, HideCode, ExternalLink
from Phocas_Sales_Actual_Live.dbo.CategoryType
order by BranchID, id;

	select BranchID, count(*)
	from Phocas_Sales_Actual_Live.dbo.CategoryType
	group by BranchID
	order by BranchID;

-- Scalar
select id, name, 
	trackingGUID, 
	ShowDecimals, 
	SortOrder, IsVirtual, fn
from Phocas_Sales_Actual_Live.dbo.Scalar
order by id;

-- Functions 
select id, name, 
	source, 
	formatStr, 
	isPercentage, RoundingDigits, ScalingDigits
from Phocas_Sales_Actual_Live.dbo.Functions
order by id;

	select source, count(*)
	from Phocas_Sales_Actual_Live.dbo.Functions
	group by source
	order by source;

-- Streams
select id, name, 
	trackingGUID, 
	isBudget, isBalance, 
	SortOrder, 
	transactionCount, minDate, maxDate
from Phocas_Sales_Actual_Live.dbo.Streams
order by id;


---------------------------------------------------------------

-- Summary Commands
select top 1000 id, command
from Phocas_Sales_Actual_Live.dbo.SummaryCommands
order by id

---------------------------------------------------------------

-- Options
select top 1000 id, value
from Phocas_Sales_Actual_Live.dbo.Options
order by id

---------------------------------------------------------------

-- Batches
select top 1000 id, 
	loadTime, convert(date, loadTime) loadTime_date,
	project
from Phocas_Sales_Actual_Live.dbo.Batches
order by id
