

select domain, store_type
from DW_Proforma.dbo.DomainStoreTypes

select orderStatusName, descriptions, 
	dw_status, dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment
from DW_Proforma.dbo.mapping_order_status
order by dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment, orderstatusname

select old_shipping_carrier, new_shipping_carrier, new_shipping_method
from DW_Proforma.dbo.mapping_shipping_carrier_method
order by new_shipping_carrier, new_shipping_method, old_shipping_carrier

select top 1000 product_id, name, sku, 
	is_lens, lens_days, category_id, product_type,
	product_params, ad, co, status
from DW_Proforma.dbo.vwProductSKUs
where product_id = 1083
order by product_params

select product_id, colour, map_colour
from DW_Proforma.dbo.vwProductColours


CREATE view [dbo].[vwProductSKUs] as
	select distinct
		cast(prod.product_id as int) as product_id, prod.name, cast(isnull(edi.[product_code],prod.sku) as varchar(255)) as sku, 
		case when prod.product_type like '%lenses%' then 1 else 0 end is_lens, prod.daysperlens as lens_days, prod.category_id, prod.product_type,
		cast(upper(
			case when isnull(edi.ad,'')<>'' then 'AD'+
				case when isnumeric(edi.ad)=0 then left(edi.ad,1) else cast(cast(edi.ad as decimal(6,2)) as varchar(20)) end
				else '' end +
			case when isnull(edi.ax,'')<>'' then 'AX'+edi.ax else '' end +
			case when isnull(edi.bc,'')<>'' then 'BC'+edi.bc else '' end +
			case when isnull(edi.co,'')<>'' then 'CO'+edi.co else '' end +
			case when isnull(edi.cy,'')<>'' then 'CY'+edi.cy else '' end +
			case when isnull(edi.di,'')<>'' then 'DI' +
				case when isnumeric(edi.di)=0 then edi.di else cast(cast(edi.di as decimal(6,1)) as varchar(20)) end
				else '' end +
			case when isnull(edi.do,'')<>'' then 'DO'+edi.do else '' end +
			case when isnull(edi.po,'')<>'' then 'PO'+cast(cast(edi.po as decimal(6,2)) as varchar(20)) else '' end) as varchar(255)) as [product_params],
			cast(isnull(edi.AD,'') as nvarchar(20)) as AD, cast(isnull(edi.CO,'') as nvarchar(20)) as CO,
			prod.status
	from 
			dw_getlenses.dbo.products prod
		left outer join 
			dbo.vwEdiStockItem edi on edi.product_id=prod.product_id
	where prod.store_name='default'


CREATE view [dbo].[vwProductColours] as
	select distinct product_id,cast(co as nvarchar(20)) as colour,cast(co as nvarchar(20)) as map_colour 
	from [dbo].[vwEdiStockItem] 
	where isnull(co,'')<>''
	union
	select 2312 as product_id,'Hazel' as colour,cast('DARK HAZEL' as nvarchar(20)) as map_colour 
	union
	select 1122 as product_id,'Aqua' as colour,cast('Caribbean Aqua' as nvarchar(20)) as map_colour 


CREATE view [dbo].[vwEdiStockItem] as
	select product_id,
		replace(replace(pvx_product_code,'-PS'+dw_getlenses.dbo.getParamValue('PS',pvx_product_code,'-'),''),'-DIA','-DI') as product_code,
		dw_getlenses.dbo.getParamValue('AD',pvx_product_code,'-') as AD,
		dw_getlenses.dbo.getParamValue('AX',pvx_product_code,'-') as AX,
		dw_getlenses.dbo.getParamValue('BC',pvx_product_code,'-') as BC,
		dw_getlenses.dbo.getParamValue('CY',pvx_product_code,'-') as CY,
		case 
			when dw_getlenses.dbo.getParamValue('DIA',pvx_product_code,'-')='' then dw_getlenses.dbo.getParamValue('DI',pvx_product_code,'-') 
			else dw_getlenses.dbo.getParamValue('DIA',pvx_product_code,'-') end	as DI,
		dw_getlenses.dbo.getParamValue('DO',pvx_product_code,'-') as DO,
		dw_getlenses.dbo.getParamValue('PO',pvx_product_code,'-') as PO,
		dw_getlenses.dbo.getParamValue('CO',pvx_product_code,'-') as CO
	from dw_getlenses.dbo.edi_stock_item
	union
	select *
	from edi_stock_item_ex
