
-- Stores
select store_name, website_group, store_type
from DW_GetLenses.dbo.dw_stores;

select store_name, website_group, store_type
from DW_GetLenses.dbo.stage_stores;

select store_name, website_group, store_type
from DW_GetLenses.dbo.stores
order by website_group, store_type, store_name;

-- Stores Full
select store_id, store_name, website_group, store_type, visible
from DW_GetLenses.dbo.dw_stores_full
order by website_group, store_type, store_name;

select store_id, store_name, website_group, store_type, visible
from DW_GetLenses.dbo.stage_dw_stores_full;

-----------------------------------------------------------------------------------
-- edi_manufacturer
select manufacturer_id, 
	manufacturer_code, manufacturer_name
from DW_GetLenses.dbo.dw_edi_manufacturer;

select manufacturer_id, 
	manufacturer_code, manufacturer_name
from DW_GetLenses.dbo.stage_edi_manufacturer;

select manufacturer_id, 
	manufacturer_code, manufacturer_name
from DW_GetLenses.dbo.edi_manufacturer
order by manufacturer_id;

-- edi_supplier_account (manufacturer_id)
select account_id, 
	manufacturer_id, 
	account_ref, account_name, account_description, 
	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
from DW_GetLenses.dbo.dw_edi_supplier_account;

select account_id, 
	manufacturer_id, 
	account_ref, account_name, account_description, 
	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
from DW_GetLenses.dbo.stage_edi_supplier_account;

select account_id, 
	manufacturer_id, 
	account_ref, account_name, account_description, 
	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
from DW_GetLenses.dbo.edi_supplier_account
order by manufacturer_id, account_ref;

	select 	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, count(*)
	from DW_GetLenses.dbo.edi_supplier_account
	group by account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
	order by account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref;


-- Categories
select category_id, 
	promotional_product, 
	modality, type, feature, category, product_type
from DW_GetLenses.dbo.dw_categories;

select category_id, 
	promotional_product, 
	modality, type, feature, category, product_type
from DW_GetLenses.dbo.stage_categories;

select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product, 
	count(*) over () tot, 
	count(*) over (partition by category) tot_category, 
	count(*) over (partition by product_type) tot_product_type, 
	count(*) over (partition by modality) tot_modality, 
	count(*) over (partition by type) tot_type, 
	count(*) over (partition by feature) tot_feature, 
	count(*) over (partition by promotional_product) tot_prom
from DW_GetLenses.dbo.categories
order by product_type, category, modality, type, feature;

	select product_type, count(*)	
	from DW_GetLenses.dbo.categories
	group by product_type
	order by product_type;

	select category, count(*)	
	from DW_GetLenses.dbo.categories
	group by category
	order by category;

	select modality, count(*)	
	from DW_GetLenses.dbo.categories
	group by modality
	order by modality;

	select type, count(*)	
	from DW_GetLenses.dbo.categories
	group by type
	order by type;

	select feature, count(*)	
	from DW_GetLenses.dbo.categories
	group by feature
	order by feature;

	select promotional_product, count(*)	
	from DW_GetLenses.dbo.categories
	group by promotional_product
	order by promotional_product;


-- Products
--	(category_id, manufacturer) 
select *
from DW_GetLenses.dbo.dw_products;

select *
from DW_GetLenses.dbo.stage_products;

select *
from DW_GetLenses.dbo.products;

select 
	product_id, store_name,
	sku, equivalent_sku, replacement_sku, autoreorder_alter_sku, 
	status, 
	category_id, product_type, manufacturer, 
	
	product_lifecycle, product_lifecycle_text, 
	brand, telesales_only, stocked_lens, tax_class_id, 

	name, description, short_description, category_list_description, 
	meta_title, meta_keyword, meta_description, 
	image, small_image, thumbnail, url_key, url_path, visibility, 
	image_label, small_image_label, thumbnail_label, 

	average_cost, weight, 
	daysperlens, alt_daysperlens, equivalence, availability, condition,
	upsell_text, price_comp_price, rrp_price, 
	promotional_product, promotion_rule, promotion_rule2, promotional_text, 
	google_base_price, google_feed_title_prefix, google_feed_title_suffix, google_shopping_gtin
	
	glasses_colour, glasses_material, glasses_size, glasses_sex, glasses_style, glasses_shape, glasses_lens_width, glasses_bridge_width, 

	pack_qty, packtext, alt_pack_qty, base_pack_qty, base_no_packs, 
	local_base_unit_price_inc_vat, local_base_value_inc_vat, local_base_pack_price_inc_vat, 
	local_base_unit_price_exc_vat, local_base_value_exc_vat, local_base_pack_price_exc_vat, 
	local_base_prof_fee, local_base_product_cost, local_base_shipping_cost, local_base_total_cost, local_base_margin_amount, local_base_margin_percent, 
	global_base_unit_price_inc_vat, global_base_value_inc_vat, global_base_pack_price_inc_vat, 
	global_base_unit_price_exc_vat, global_base_value_exc_vat, global_base_pack_price_exc_vat, 
	global_base_prof_fee, global_base_product_cost, global_base_shipping_cost, global_base_total_cost, global_base_margin_amount, global_base_margin_percent, 

	multi_pack_qty, multi_no_packs, 
	local_multi_unit_price_inc_vat, local_multi_value_inc_vat, local_multi_pack_price_inc_vat, 
	local_multi_unit_price_exc_vat, local_multi_value_exc_vat, local_multi_pack_price_exc_vat, 
	local_multi_prof_fee, local_multi_product_cost, local_multi_shipping_cost, local_multi_total_cost, local_multi_margin_amount, local_multi_margin_percent, 
	local_multi_unit_price_inc_vat, global_multi_value_inc_vat, global_multi_pack_price_inc_vat, 
	global_multi_unit_price_exc_vat, global_multi_value_exc_vat, global_multi_pack_price_exc_vat, 
	global_multi_prof_fee, global_multi_product_cost, global_multi_shipping_cost, global_multi_total_cost, global_multi_margin_amount, global_multi_margin_percent, 

	local_to_global_rate, store_currency_code, prof_fee_percent, vat_percent_before_prof_fee, vat_percent, 

	created_at, updated_at,
	 
	count(*) over () num_tot			 
from DW_GetLenses.dbo.products
where product_id in (1352, 2253);

	select product_id, count(*) num
	from DW_GetLenses.dbo.products
	group by product_id
	order by num desc;

	select product_id, sku, count(*) num
	from DW_GetLenses.dbo.products
	group by product_id, sku
	order by num desc;

	select product_id, store_name, count(*) num
	from DW_GetLenses.dbo.products
	group by product_id, store_name
	order by num desc, product_id, store_name;


-- Product Prices
select *
from DW_GetLenses.dbo.dw_product_prices;

select *
from DW_GetLenses.dbo.stage_product_prices;

select price_id, 
	store_name, product_id, 
	qty, pack_size, 

	local_unit_price_inc_vat, local_qty_price_inc_vat, local_pack_price_inc_vat, 
	local_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, store_currency_code, 

	global_unit_price_inc_vat, global_qty_price_inc_vat, global_pack_price_inc_vat, 
	global_prof_fee, 
	global_product_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, 
	vat_percent_before_prof_fee, vat_percent
from DW_GetLenses.dbo.product_prices
where product_id in (1352, 2253)
order by product_id, store_name;

	select product_id, count(*) num
	from DW_GetLenses.dbo.product_prices
	group by product_id
	order by num desc;

-- Product Despatch Times
--	(product_id)
select product_id, name, 
	availability, 
	info
from DW_GetLenses.dbo.product_despatch_times
order by product_id

	select availability, count(*)
	from DW_GetLenses.dbo.product_despatch_times
	group by availability
	order by availability;

-----------------------------------------------------------------------------------

-- edi_packsize_pref (supplier_account_id, product_id)
select *
from DW_GetLenses.dbo.dw_edi_packsize_pref;

select *
from DW_GetLenses.dbo.stage_edi_packsize_pref;

select packsize_pref_id, 
	packsize, product_id, 
	priority, 
	supplier_account_id, 
	stocked, 
	cost, 
	supply_days, supply_days_range, 
	enabled, 
	strategy
from DW_GetLenses.dbo.edi_packsize_pref;

	select packsize, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by packsize
	order by packsize;

	select priority, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by priority
	order by priority;

	select supplier_account_id, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by supplier_account_id
	order by supplier_account_id;

	select stocked, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by stocked
	order by stocked;

	select supply_days, supply_days_range, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by supply_days, supply_days_range
	order by supply_days, supply_days_range;

	select enabled, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by enabled
	order by enabled;

	select strategy, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by strategy
	order by strategy;

-- edi_supplier_stock_item (manufacturer_id, product_id)
select top 1000 id, 
	product_id, packsize, sku, 
	product_code, pvx_product_code, 
	stocked, 
	BC, PO, CY, DO, CO, DI, AD, AX, 
	edi_conf1, edi_conf2, 
	manufacturer_id, 
	supply_days, supply_days_range, 
	disabled
from DW_GetLenses.dbo.edi_stock_item;

	select product_id, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by product_id
	order by product_id;

	select packsize, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by packsize
	order by packsize;

	select sku, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by sku
	order by sku;

	select stocked, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by stocked
	order by stocked;

	select supply_days, supply_days_range, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by supply_days, supply_days_range
	order by supply_days, supply_days_range;

	select disabled, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by disabled
	order by disabled;

	select BC, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by BC
	order by BC;

	select PO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by PO
	order by PO;

	select CY, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by CY
	order by CY;

	select DO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by DO
	order by DO;

	select CO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by CO
	order by CO;

	select DI, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by DI
	order by DI;

	select AD, AX, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by AD, AX
	order by AD, AX;

-----------------------------------------------------------------------------------

-- Calendar
select *
from DW_GetLenses.dbo.dw_calendar

	select year_key, count(*)
	from DW_GetLenses.dbo.dw_calendar
	group by year_key
	order by year_key;

-- Continents
select country, continent
from DW_GetLenses.dbo.continents
order by continent, country;

-- Countries
select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_GetLenses.dbo.dw_countries;

select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_GetLenses.dbo.stage_countries;

select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_GetLenses.dbo.countries
order by country_type, country_zone, country_continent;

-- New Countries
select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_GetLenses.dbo.new_countries
order by country_type, country_zone, country_continent;

-- Currency Rates
select 
	currency_from, currency_to, 
	date, 
	rate, 
	updated_at
from DW_GetLenses.dbo.currency_rates
order by currency_from, currency_to, date desc;

	select currency_from, currency_to, count(*),
		min(date), max(date)
	from DW_GetLenses.dbo.currency_rates
	group by currency_from, currency_to
	order by currency_from, currency_to;

-----------------------------------------------------------------------------------

-- Business Source
select source_code, channel
from DW_GetLenses.dbo.dw_business_source;

select source_code, channel
from DW_GetLenses.dbo.stage_business_source;

select source_code, channel, 
	count(*) over () tot, 
	count(*) over (partition by channel) tot_channel
from DW_GetLenses.dbo.business_source
order by channel, source_code;

-- Coupon Channel
select coupon_code, 
	start_date, end_date, description,
	source_code, 
	channel, new_customer
from DW_GetLenses.dbo.dw_coupon_channel;

select coupon_code, 
	start_date, end_date, description,
	source_code, 
	channel, new_customer
from DW_GetLenses.dbo.stage_coupon_channel;

select coupon_code, 
	start_date, end_date, description,
	source_code, 
	channel, new_customer
from DW_GetLenses.dbo.coupon_channel
order by channel, new_customer, coupon_code;

	select channel, count(*)
	from DW_GetLenses.dbo.coupon_channel
	group by channel
	order by channel

	select new_customer, count(*)
	from DW_GetLenses.dbo.coupon_channel
	group by new_customer
	order by new_customer

-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------


-- Admin User
select user_id, 
	first_name, last_name, user_name, email
from DW_GetLenses.dbo.admin_user
order by user_name;


-------------------------------------------------------------------



-- Brand Order
select top 1000 domain, status, cost
from DW_GetLenses.dbo.eBrandOrder
order by domain

	select status, count(*)
	from DW_GetLenses.dbo.eBrandOrder
	group by status
	order by status;

-- Max ROrder Order
select reorder_profile_id, max_order_iod
from DW_GetLenses.dbo.max_rorder_order



-- Supplier Export
select *
from DW_GetLenses.dbo.Supplier_Export
--Annual Revenue (converted) Currency, Created Date, Account ID, Account Name, Type, Billing State/Province, Billing Street, Billing Address Line 1, Billing Address Line 2, Billing Address Line 3, Billing City, Billing Zip/Postal Code, Billing Country, Phone, Fax, Website, Accounts Payable Control, Accounts Receivable Control, Bank Account Name, Bank Account Number, Bank Account Reference, Bank BIC, Bank City, Bank Country, Bank Fax, Bank IBAN Number, Bank Name, Bank Sort Code, Bank State/Province, Bank Street, Bank SWIFT Number, Bank Zip/Postal Code, Base Date 1, Days Offset 1, Description 1, Payment Priority, Reporting Code, Tax Status, VAT Registration Number, Annual Revenue Currency



-- Zid
select id
from DW_GetLenses.dbo.zid



-- Cyber Source Stored
select top 1000 *
from DW_GetLenses.dbo.cybersourcestored

--store_name, invoice_id, invoice_no, document_type, document_date, document_updated_at, document_id, document_no, invoice_date, updated_at, order_id, order_no, order_date, customer_id, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, customer_taxvat, status, coupon_code, weight, customer_gender, customer_dob, shipping_carrier, shipping_method, approved_time, length_of_time_to_invoice, payment_method, local_payment_authorized, local_payment_canceled, cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, billing_email, billing_company, billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone, billing_fax, shipping_email, shipping_company, shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone, shipping_fax, has_lens, total_qty, local_prof_fee, local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, local_total_inc_vat, local_total_vat, local_total_exc_vat, local_subtotal_cost, local_shipping_cost, local_total_cost, local_margin_amount, local_margin_percent, local_to_global_rate, order_currency_code, global_prof_fee, global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, global_total_inc_vat, global_total_vat, global_total_exc_vat, global_subtotal_cost, global_shipping_cost, global_total_cost, global_margin_amount, global_margin_percent, prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent, customer_note, customer_note_notify, remote_ip, business_source, business_channel, affilBatch, affilCode, affilUserRef, auto_verification, order_type, order_lifecycle, customer_order_seq_no, reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_sent, reminder_follow_sent, telesales_method_code, telesales_admin_username, reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, reorder_cc_exp_month, reorder_cc_exp_year, automatic_reorder, presc_verification_method, referafriend_code, referafriend_referer, customer_first_order_date, customer_business_channel

-- Cyber Source Stored Token
select top 1000 *
from DW_GetLenses.dbo.cybersourcestored_token

--id, created_at, updated_at, subscription_id, customer_id, store_id, address_id, order_address_id, addr_version, active, ctype, clogo, cname, cnumbermasked, cexpiry, cc_request_id


-- EMV Gender Prefix
select title, gender
from DW_GetLenses.dbo.EMV_gender_prefix
order by gender, title

-- EMV Sync List
select top 1000 *
from DW_GetLenses.dbo.EMV_sync_list

--CLIENT_ID, WEBSITE_ID, STORE_ID, STORE_URL, FIRSTNAME, LASTNAME, EMAIL_ORIGINE, EMAIL, EMVCELLPHONE, DATEJOIN, DATEUNJOIN, TITLE, DATEOFBIRTH, SEED, CLIENTURN, SOURCE, CODE, SEGMENT, EMVADMIN1, EMVADMIN2, EMVADMIN3, EMVADMIN4, EMVADMIN5, PRODUCT_ID, CURRENCY, COUNTRY, EMAIL_ORIGIN, GENDER, LANGUAGE, ORDER_REMINDER_ID, ORDER_REMINDER_SOURCE, ORDER_REMINDER_DATE, ORDER_REMINDER_NO, ORDER_REMINDER_PREF, ORDER_REMINDER_PRODUCT, ORDER_REMINDER_REORDER_URL, PRESCRIPTION_EXPIRY_DATE, PRESCRIPTION_WEARER, PRESCRIPTION_OPTICIAN, UNSUBSCRIBE, BOUNCE, REGISTRATION_DATE, FIRST_ORDER_DATE, LAST_LOGGED_IN_DATE, LAST_ORDER_DATE, LAST_ORDER_ID, LAST_ORDER_SOURCE, NUM_OF_ORDERS, CARD_EXPIRY_DATE, DAILIES_STORE_ID1, DAILIES_STORE_ID2, DAILIES_SKU1, DAILIES_SKU2, DAILIES_NAME1, DAILIES_NAME2, DAILIES_URL1, DAILIES_URL2, DAILIES_LAST_ORDER_DATE, MONTHLIES_STORE_ID1, MONTHLIES_STORE_ID2, MONTHLIES_SKU1, MONTHLIES_SKU2, MONTHLIES_NAME1, MONTHLIES_NAME2, MONTHLIES_URL1, MONTHLIES_URL2, MONTHLIES_CATEGORY1, MONTHLIES_CATEGORY2, MONTHLIES_LAST_ORDER_DATE, COLOURS_STORE_ID1, COLOURS_STORE_ID2, COLOURS_SKU1, COLOURS_SKU2, COLOURS_NAME1, COLOURS_NAME2, COLOURS_URL1, COLOURS_URL2, COLOURS_LAST_ORDER_DATE, SOLUTIONS_STORE_ID1, SOLUTIONS_STORE_ID2, SOLUTIONS_SKU1, SOLUTIONS_SKU2, SOLUTIONS_CATEGORY1, SOLUTIONS_CATEGORY2, SOLUTIONS_NAME1, SOLUTIONS_NAME2, SOLUTIONS_URL1, SOLUTIONS_URL2, SOLUTIONS_LAST_ORDER_DATE, OTHER_STORE_ID1, OTHER_STORE_ID2, OTHER_SKU1, OTHER_SKU2, OTHER_CATEGORY1, OTHER_CATEGORY2, OTHER_NAME1, OTHER_NAME2, OTHER_URL1, OTHER_URL2, OTHER_LAST_ORDER_DATE, SEGMENT_LIFECYCLE, SEGMENT_USAGE, SEGMENT_GEOG, SEGMENT_PURCH_BEHAVIOUR, SEGMENT_EYSIGHT, SEGMENT_SPORT, SEGMENT_PROFESSIONAL, SEGMENT_LIFESTAGE, SEGMENT_VANITY, SEGMENT_2, SEGMENT_3, SEGMENT_4, LAST_PRODUCT_LAST_ORDER_DATE, LAST_PRODUCT_NAME1, LAST_PRODUCT_NAME2, LAST_PRODUCT_SKU1, LAST_PRODUCT_SKU2, LAST_PRODUCT_URL1, LAST_PRODUCT_URL2, LAST_PRODUCT_TYPE, LENS_LAST_ORDER_DATE, LENS_NAME1, LENS_NAME2, LENS_SKU1, LENS_SKU2, LENS_URL1, LENS_URL2, WEBSITE_UNSUBSCRIBE, check_sum, W_UNSUB_DATE

-- EMV Sync List Prev
select top 1000 *
from DW_GetLenses.dbo.EMV_sync_list_prev

--CLIENT_ID, WEBSITE_ID, STORE_ID, STORE_URL, FIRSTNAME, LASTNAME, EMAIL_ORIGINE, EMAIL, EMVCELLPHONE, DATEJOIN, DATEUNJOIN, TITLE, DATEOFBIRTH, SEED, CLIENTURN, SOURCE, CODE, SEGMENT, EMVADMIN1, EMVADMIN2, EMVADMIN3, EMVADMIN4, EMVADMIN5, PRODUCT_ID, CURRENCY, COUNTRY, EMAIL_ORIGIN, GENDER, LANGUAGE, ORDER_REMINDER_ID, ORDER_REMINDER_SOURCE, ORDER_REMINDER_DATE, ORDER_REMINDER_NO, ORDER_REMINDER_PREF, ORDER_REMINDER_PRODUCT, ORDER_REMINDER_REORDER_URL, PRESCRIPTION_EXPIRY_DATE, PRESCRIPTION_WEARER, PRESCRIPTION_OPTICIAN, UNSUBSCRIBE, BOUNCE, REGISTRATION_DATE, FIRST_ORDER_DATE, LAST_LOGGED_IN_DATE, LAST_ORDER_DATE, LAST_ORDER_ID, LAST_ORDER_SOURCE, NUM_OF_ORDERS, CARD_EXPIRY_DATE, DAILIES_STORE_ID1, DAILIES_STORE_ID2, DAILIES_SKU1, DAILIES_SKU2, DAILIES_NAME1, DAILIES_NAME2, DAILIES_URL1, DAILIES_URL2, DAILIES_LAST_ORDER_DATE, MONTHLIES_STORE_ID1, MONTHLIES_STORE_ID2, MONTHLIES_SKU1, MONTHLIES_SKU2, MONTHLIES_NAME1, MONTHLIES_NAME2, MONTHLIES_URL1, MONTHLIES_URL2, MONTHLIES_CATEGORY1, MONTHLIES_CATEGORY2, MONTHLIES_LAST_ORDER_DATE, COLOURS_STORE_ID1, COLOURS_STORE_ID2, COLOURS_SKU1, COLOURS_SKU2, COLOURS_NAME1, COLOURS_NAME2, COLOURS_URL1, COLOURS_URL2, COLOURS_LAST_ORDER_DATE, SOLUTIONS_STORE_ID1, SOLUTIONS_STORE_ID2, SOLUTIONS_SKU1, SOLUTIONS_SKU2, SOLUTIONS_CATEGORY1, SOLUTIONS_CATEGORY2, SOLUTIONS_NAME1, SOLUTIONS_NAME2, SOLUTIONS_URL1, SOLUTIONS_URL2, SOLUTIONS_LAST_ORDER_DATE, OTHER_STORE_ID1, OTHER_STORE_ID2, OTHER_SKU1, OTHER_SKU2, OTHER_CATEGORY1, OTHER_CATEGORY2, OTHER_NAME1, OTHER_NAME2, OTHER_URL1, OTHER_URL2, OTHER_LAST_ORDER_DATE, SEGMENT_LIFECYCLE, SEGMENT_USAGE, SEGMENT_GEOG, SEGMENT_PURCH_BEHAVIOUR, SEGMENT_EYSIGHT, SEGMENT_SPORT, SEGMENT_PROFESSIONAL, SEGMENT_LIFESTAGE, SEGMENT_VANITY, SEGMENT_2, SEGMENT_3, SEGMENT_4, LAST_PRODUCT_LAST_ORDER_DATE, LAST_PRODUCT_NAME1, LAST_PRODUCT_NAME2, LAST_PRODUCT_SKU1, LAST_PRODUCT_SKU2, LAST_PRODUCT_URL1, LAST_PRODUCT_URL2, LAST_PRODUCT_TYPE, LENS_LAST_ORDER_DATE, LENS_NAME1, LENS_NAME2, LENS_SKU1, LENS_SKU2, LENS_URL1, LENS_URL2, WEBSITE_UNSUBSCRIBE, check_sum, W_UNSUB_DATE
