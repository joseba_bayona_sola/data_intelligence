
-- dw_hist_order
select top 100 *
from DW_GetLenses.dbo.dw_hist_order

select top 100 source,
	store_id, order_id, customer_id, 
	created_at, 
	grand_total
from DW_GetLenses.dbo.dw_hist_order
where customer_id = 110
order by order_id

	select top 100 source, count(*)
	from DW_GetLenses.dbo.dw_hist_order
	group by source
	order by source;

-- dw_hist_order_item
select top 100 *
from DW_GetLenses.dbo.dw_hist_order_item

select top 100 hoi.source, 
	hoi.order_id, 
	hoi.product_id, hoi.sku, 
	hoi.qty_ordered, hoi.row_total
from 
		DW_GetLenses.dbo.dw_hist_order_item hoi
	inner join	
		DW_GetLenses.dbo.dw_hist_order ho on hoi.order_id = ho.order_id and ho.customer_id = 110
order by hoi.order_id


-- dw_hist_shipment
select top 100 *
from DW_GetLenses.dbo.dw_hist_shipment

select top 100 source,
	store_id, shipment_id, customer_id, 
	created_at
from DW_GetLenses.dbo.dw_hist_shipment
where customer_id = 110
order by shipment_id

	select top 100 source, count(*)
	from DW_GetLenses.dbo.dw_hist_shipment
	group by source
	order by source;

-- dw_hist_shipment_item
select top 100 *
from DW_GetLenses.dbo.dw_hist_shipment_item

select top 100 hsi.source, 
	hsi.shipment_id, 
	hsi.product_id, hsi.sku, 
	hsi.qty
from 
		DW_GetLenses.dbo.dw_hist_shipment_item hsi
	inner join
		DW_GetLenses.dbo.dw_hist_shipment hs on hsi.shipment_id = hs.shipment_id and hs.customer_id = 110
order by hsi.shipment_id
