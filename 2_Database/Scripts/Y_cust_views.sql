
-- [dbo].[Dim_Customers] 
	SELECT [customer_id]
		  ,[email]
		  ,[store_name]
		  ,[created_at]
		  ,[updated_at]
		  ,[website_group]
		  ,[created_in]
		  ,[prefix]
		  ,[firstname]
		  ,[middlename]
		  ,[lastname]
		  ,[suffix]
		  ,[taxvat]
		  ,[postoptics_send_post]
		  ,[facebook_id]
		  ,[facebook_permissions]
		  ,[gender]
		  ,[dob]
		  ,[unsubscribe_all]
		  ,[days_worn]
		  ,[parent_customer_id]
		  ,[is_parent_customer]
		  ,[eyeplan_credit_limit]
		  ,[eyeplan_approved_flag]
		  ,[eyeplan_can_ref_new_customer]
		  ,[cus_phone]
		  ,[found_us_info]
		  ,[password_hash]
		  ,[referafriend_code]
		  ,[alternate_email]
		  ,[emvadmin1]
		  ,[emvadmin2]
		  ,[emvadmin3]
		  ,[emvadmin4]
		  ,[emvadmin5]
		  ,[language]
		  ,[first_order_date]
		  ,[last_logged_in_date]
		  ,[last_order_date]
		  ,[num_of_orders]
		  ,[card_expiry_date]
		  ,[segment_lifecycle]
		  ,[segment_usage]
		  ,[segment_geog]
		  ,[segment_purch_behaviour]
		  ,[segment_eysight]
		  ,[segment_sport]
		  ,[segment_professional]
		  ,[segment_lifestage]
		  ,[segment_vanity]
		  ,[segment]
		  ,[segment_2]
		  ,[segment_3]
		  ,[segment_4]
		  ,[default_billing]
		  ,[default_shipping]
		  ,ISNULL([business_channel],'Unknown') AS [business_channel]
		  , lastname + ', ' + LTRIM(prefix + ' ') + LTRIM(firstname + ' ')   AS CustomerName,
		  ISNULL(LEFT(CONVERT(VARCHAR(30), first_order_date,120),7),'2001-01') AS first_order_month
	FROM dw_getlenses.dbo.customers c
	where exists 
		(select * from DW_GetLenses.dbo.stores where stores.store_name =c.store_name ) 
	union all
	SELECT [customer_id]
		  ,[email]
		  ,[store_name]
		  ,[created_at]
		  ,[updated_at]
		  ,[website_group]
		  ,[created_in]
		  ,[prefix]
		  ,[firstname]
		  ,[middlename]
		  ,[lastname]
		  ,[suffix]
		  ,[taxvat]
		  ,[postoptics_send_post]
		  ,[facebook_id]
		  ,[facebook_permissions]
		  ,[gender]
		  ,[dob]
		  ,[unsubscribe_all]
		  ,[days_worn]
		  ,[parent_customer_id]
		  ,[is_parent_customer]
		  ,[eyeplan_credit_limit]
		  ,[eyeplan_approved_flag]
		  ,[eyeplan_can_ref_new_customer]
		  ,[cus_phone]
		  ,[found_us_info]
		  ,[password_hash]
		  ,[referafriend_code]
		  ,[alternate_email]
		  ,[emvadmin1]
		  ,[emvadmin2]
		  ,[emvadmin3]
		  ,[emvadmin4]
		  ,[emvadmin5]
		  ,[language]
		  ,[first_order_date]
		  ,[last_logged_in_date]
		  ,[last_order_date]
		  ,[num_of_orders]
		  ,[card_expiry_date]
		  ,[segment_lifecycle]
		  ,[segment_usage]
		  ,[segment_geog]
		  ,[segment_purch_behaviour]
		  ,[segment_eysight]
		  ,[segment_sport]
		  ,[segment_professional]
		  ,[segment_lifestage]
		  ,[segment_vanity]
		  ,[segment]
		  ,[segment_2]
		  ,[segment_3]
		  ,[segment_4]
		  ,[default_billing]
		  ,[default_shipping]
		  ,ISNULL([business_channel],'Unknown') AS [business_channel]
		  , lastname + ', ' + LTRIM(prefix + ' ') + LTRIM(firstname + ' ')   AS CustomerName,
		  ISNULL(LEFT(CONVERT(VARCHAR(30), first_order_date,120),7),'2001-01') AS first_order_month
	FROM 
		dw_proforma.dbo.customers c
	where exists 
		(select * from dw_proforma.dbo.stores where stores.store_name =c.store_name) 

--[dbo].[Dim_Customers_New] AS 
	SELECT TOP 1000 * 
	FROM Dim_customers rs 
	where NOT EXISTS 
		(Select * from [Phocas_Sales_Actual_Live].dbo.entity_28 pc where pc.linkfield=rs.customer_id)

