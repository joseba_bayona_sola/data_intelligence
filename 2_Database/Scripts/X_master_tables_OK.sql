
-- Stores
select store_name, website_group, store_type
from DW_GetLenses.dbo.stores
order by website_group, store_type, store_name;

-- Stores Full
select store_id, store_name, website_group, store_type, visible
from DW_GetLenses.dbo.dw_stores_full
order by website_group, store_type, store_name;


-- edi_manufacturer
select manufacturer_id, 
	manufacturer_code, manufacturer_name
from DW_GetLenses.dbo.edi_manufacturer
order by manufacturer_name;

-- edi_supplier_account (manufacturer_id)
select account_id, 
	manufacturer_id, 
	account_ref, account_name, account_description, 
	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
from DW_GetLenses.dbo.edi_supplier_account
order by manufacturer_id, account_ref;

	select s.account_id, 
		s.manufacturer_id, m.manufacturer_name,
		s.account_ref, s.account_name, s.account_description, 
		s.account_edi_format, s.account_edi_strategy, s.account_edi_cutofftime, s.account_edi_ref
	from 
			DW_GetLenses.dbo.edi_supplier_account s
		LEFT JOIN 
			DW_GetLenses.dbo.edi_manufacturer m on s.manufacturer_id = m.manufacturer_id
	order by s.manufacturer_id, s.account_ref;

	select 	account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, count(*)
	from DW_GetLenses.dbo.edi_supplier_account
	group by account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref
	order by account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref;

-- Categories
select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product, 
	count(*) over () tot, 
	count(*) over (partition by category) tot_category, 
	count(*) over (partition by product_type) tot_product_type, 
	count(*) over (partition by modality) tot_modality, 
	count(*) over (partition by type) tot_type, 
	count(*) over (partition by feature) tot_feature, 
	count(*) over (partition by promotional_product) tot_prom
from DW_GetLenses.dbo.categories
order by product_type, category, modality, type, feature;

select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product
from DW_GetLenses.dbo.categories
order by category_id, product_type, category, modality, type, feature;

	select product_type, count(*) num	
	from DW_GetLenses.dbo.categories
	group by product_type
	order by product_type;

	select category, count(*) num	
	from DW_GetLenses.dbo.categories
	group by category
	order by category;

	select modality, count(*) num	
	from DW_GetLenses.dbo.categories
	group by modality
	order by modality;

	select type, count(*) num	
	from DW_GetLenses.dbo.categories
	group by type
	order by type;

	select feature, count(*) num	
	from DW_GetLenses.dbo.categories
	group by feature
	order by feature;

	select promotional_product, count(*) num	
	from DW_GetLenses.dbo.categories
	group by promotional_product
	order by promotional_product;

-- Products
--	(category_id, manufacturer) 
select 
	product_id, store_name,
	sku, equivalent_sku, replacement_sku, autoreorder_alter_sku, 
	status, 
	category_id, product_type, manufacturer, 
	
	product_lifecycle, product_lifecycle_text, 
	brand, telesales_only, stocked_lens, tax_class_id, 

	name, description, short_description, category_list_description, 
	meta_title, meta_keyword, meta_description, 
	image, small_image, thumbnail, url_key, url_path, visibility, 
	image_label, small_image_label, thumbnail_label, 

	average_cost, weight, 
	daysperlens, alt_daysperlens, equivalence, availability, condition,
	upsell_text, price_comp_price, rrp_price, 
	promotional_product, promotion_rule, promotion_rule2, promotional_text, 
	google_base_price, google_feed_title_prefix, google_feed_title_suffix, google_shopping_gtin
	
	glasses_colour, glasses_material, glasses_size, glasses_sex, glasses_style, glasses_shape, glasses_lens_width, glasses_bridge_width, 

	pack_qty, packtext, alt_pack_qty, 
	base_pack_qty, base_no_packs, 
	local_base_unit_price_inc_vat, local_base_unit_price_exc_vat, 
	local_base_value_inc_vat, local_base_value_exc_vat, 
	local_base_pack_price_inc_vat, local_base_pack_price_exc_vat, 	
	local_base_prof_fee, 
	local_base_product_cost, local_base_shipping_cost, local_base_total_cost, 
	local_base_margin_amount, local_base_margin_percent, 

	global_base_unit_price_inc_vat, global_base_unit_price_exc_vat, 
	global_base_value_inc_vat, global_base_value_exc_vat, 
	global_base_pack_price_inc_vat, global_base_pack_price_exc_vat, 	
	global_base_prof_fee, global_base_product_cost, global_base_shipping_cost, global_base_total_cost, 
	global_base_margin_amount, global_base_margin_percent, 

	multi_pack_qty, multi_no_packs, 
	local_multi_unit_price_inc_vat, local_multi_unit_price_exc_vat, 
	local_multi_value_inc_vat, local_multi_value_exc_vat, 
	local_multi_pack_price_inc_vat, local_multi_pack_price_exc_vat, 	
	local_multi_prof_fee, local_multi_product_cost, local_multi_shipping_cost, local_multi_total_cost, 
	local_multi_margin_amount, local_multi_margin_percent, 
	
	global_multi_unit_price_inc_vat, global_multi_unit_price_exc_vat, 
	global_multi_value_inc_vat, global_multi_value_exc_vat, 
	global_multi_pack_price_inc_vat, global_multi_pack_price_exc_vat, 
	global_multi_prof_fee, global_multi_product_cost, global_multi_shipping_cost, global_multi_total_cost, 
	global_multi_margin_amount, global_multi_margin_percent, 

	local_to_global_rate, store_currency_code, 
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, 

	created_at, updated_at,
	 
	count(*) over () num_tot			 
from DW_GetLenses.dbo.products
where product_id in (1083, 1352, 2253, 2975, 2998)
	and store_name in ('Default', 'visiondirect.co.uk')
order by product_id, store_name;

select 
	product_id, store_name,
	sku,  
	status, 
	category_id, product_type, manufacturer, 
	
	product_lifecycle,  
	telesales_only, stocked_lens, tax_class_id, 

	name, 

	average_cost, weight, 
	daysperlens, equivalence, availability, condition,
	promotional_product, 
	
	pack_qty, alt_pack_qty, base_pack_qty, base_no_packs
from DW_GetLenses.dbo.products
where store_name = 'default'
	and product_type = 'Glasses'
ORDER BY manufacturer, product_type, category_id, product_id, store_name;

	select product_id, count(*) num
	from DW_GetLenses.dbo.products
	group by product_id
	order by num desc;

	select product_id, sku, name, manufacturer, product_type,
		count(*) num, 
		count(*) over (partition by product_id) num2
	from DW_GetLenses.dbo.products
	group by product_id, sku, name, manufacturer, product_type
	order by manufacturer, product_type, product_id;

	select product_id, store_name, count(*) num
	from DW_GetLenses.dbo.products
	group by product_id, store_name
	order by num desc, product_id, store_name;

	select store_name, count(*) num
	from DW_GetLenses.dbo.products
	group by store_name
	order by store_name;


	select status, count(*) num
	from DW_GetLenses.dbo.products
	group by status
	order by status;

	select category_id, count(*) num
	from DW_GetLenses.dbo.products
	group by category_id
	order by category_id;

	select product_type, count(*) num
	from DW_GetLenses.dbo.products
	group by product_type
	order by product_type;

	select manufacturer, count(*) num
	from DW_GetLenses.dbo.products
	group by manufacturer
	order by manufacturer;

	select product_lifecycle, count(*) num
	from DW_GetLenses.dbo.products
	group by product_lifecycle
	order by product_lifecycle;

	select telesales_only, count(*) num
	from DW_GetLenses.dbo.products
	group by telesales_only
	order by telesales_only;

	select stocked_lens, count(*) num
	from DW_GetLenses.dbo.products
	group by stocked_lens
	order by stocked_lens;

	select tax_class_id, count(*) num
	from DW_GetLenses.dbo.products
	group by tax_class_id
	order by tax_class_id;

	select daysperlens, count(*) num
	from DW_GetLenses.dbo.products
	group by daysperlens
	order by daysperlens;

	select equivalence, count(*) num
	from DW_GetLenses.dbo.products
	group by equivalence
	order by equivalence;

	select availability, count(*) num
	from DW_GetLenses.dbo.products
	group by availability
	order by availability;

	select condition, count(*) num
	from DW_GetLenses.dbo.products
	group by condition
	order by condition;

	select promotional_product, count(*) num
	from DW_GetLenses.dbo.products
	group by promotional_product
	order by promotional_product;

	select pack_qty, multi_pack_qty, count(*) num
	from DW_GetLenses.dbo.products
	group by pack_qty, multi_pack_qty
	order by pack_qty, multi_pack_qty;

	select base_no_packs, multi_no_packs, count(*) num
	from DW_GetLenses.dbo.products
	group by base_no_packs, multi_no_packs
	order by base_no_packs, multi_no_packs;

	select base_pack_qty, count(*) num
	from DW_GetLenses.dbo.products
	group by base_pack_qty
	order by base_pack_qty;




-- Product Prices
select price_id, 
	store_name, product_id, 
	qty, pack_size, 

	local_unit_price_inc_vat, local_qty_price_inc_vat, local_pack_price_inc_vat, 
	local_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, store_currency_code, 

	global_unit_price_inc_vat, global_qty_price_inc_vat, global_pack_price_inc_vat, 
	global_prof_fee, 
	global_product_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, 
	vat_percent_before_prof_fee, vat_percent
from DW_GetLenses.dbo.product_prices
where product_id in (1352, 2253)
order by product_id, store_name;

	select product_id, count(*) num
	from DW_GetLenses.dbo.product_prices
	group by product_id
	order by num desc;

-- Product Despatch Times
--	(product_id)
select product_id, name, 
	availability, hours, 
	info
from DW_GetLenses.dbo.product_despatch_times
order by product_id

	select hours, availability, count(*)
	from DW_GetLenses.dbo.product_despatch_times
	group by hours, availability
	order by hours, availability;

-- edi_packsize_pref (supplier_account_id, product_id)
select packsize_pref_id, 
	packsize, product_id, 
	priority, 
	supplier_account_id, 
	stocked, 
	cost, 
	supply_days, supply_days_range, 
	enabled, 
	strategy
from DW_GetLenses.dbo.edi_packsize_pref;

	select packsize, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by packsize
	order by packsize;

	select priority, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by priority
	order by priority;

	select supplier_account_id, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by supplier_account_id
	order by supplier_account_id;

	select stocked, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by stocked
	order by stocked;

	select supply_days, supply_days_range, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by supply_days, supply_days_range
	order by supply_days, supply_days_range;

	select enabled, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by enabled
	order by enabled;

	select strategy, count(*)
	from DW_GetLenses.dbo.edi_packsize_pref
	group by strategy
	order by strategy;

-- edi_supplier_stock_item (manufacturer_id, product_id)
select top 1000 id, 
	product_id, sku, packsize, 
	product_code, pvx_product_code, 
	manufacturer_id,
	stocked, 
	BC, PO, CY, DO, CO, DI, AD, AX, 
		 
	supply_days, supply_days_range, 
	disabled, 
	edi_conf1, edi_conf2
from DW_GetLenses.dbo.edi_stock_item;

	select product_id, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by product_id
	order by product_id;

	select packsize, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by packsize
	order by packsize;

	select sku, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by sku
	order by sku;

	select stocked, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by stocked
	order by stocked;

	select supply_days, supply_days_range, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by supply_days, supply_days_range
	order by supply_days, supply_days_range;

	select disabled, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by disabled
	order by disabled;

	select BC, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by BC
	order by BC;

	select PO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by PO
	order by PO;

	select CY, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by CY
	order by CY;

	select DO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by DO
	order by DO;

	select CO, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by CO
	order by CO;

	select DI, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by DI
	order by DI;

	select AD, AX, count(*)
	from DW_GetLenses.dbo.edi_stock_item
	group by AD, AX
	order by AD, AX;

-- Calendar
select *
from DW_GetLenses.dbo.dw_calendar

	select year_key, count(*)
	from DW_GetLenses.dbo.dw_calendar
	group by year_key
	order by year_key;

-- Continents
select country, continent
from DW_GetLenses.dbo.continents
order by continent, country;

-- Countries
select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_GetLenses.dbo.countries
order by country_type, country_zone, country_continent, country_name;

-- Currency Rates
select 
	currency_from, currency_to, 
	date, 
	rate, 
	updated_at
from DW_GetLenses.dbo.currency_rates
order by currency_from, currency_to, date desc;

	select currency_from, currency_to, count(*),
		min(date), max(date)
	from DW_GetLenses.dbo.currency_rates
	group by currency_from, currency_to
	order by currency_from, currency_to;



-- Business Source
select source_code, channel 
	--,count(*) over () tot, 
	--count(*) over (partition by channel) tot_channel
from DW_GetLenses.dbo.business_source
order by channel, source_code;

	select channel, count(*)
	from DW_GetLenses.dbo.business_source
	group by channel
	order by channel;

-- Coupon Channel
select coupon_code, 
	start_date, end_date, --description,
	source_code, 
	channel, new_customer
from DW_GetLenses.dbo.coupon_channel
order by channel, new_customer, coupon_code;

	select channel, count(*)
	from DW_GetLenses.dbo.coupon_channel
	group by channel
	order by channel;

-- Admin User
select user_id, 
	first_name, last_name, user_name, email
from DW_GetLenses.dbo.admin_user
order by user_name;