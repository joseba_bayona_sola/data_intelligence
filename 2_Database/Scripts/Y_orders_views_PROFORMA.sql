
-- dbo.Dim_Order_Headers - PROFORMA
	SELECT 'O' + LEFT(o.document_type, 2) + CAST(CAST(o.document_id AS INT) AS VARCHAR(255)) AS UniqueID, 
	document_no= o.order_no, o.document_type,
	o.order_no, 
	o.order_type, o.order_lifecycle, 
	o.business_source, o.business_channel, o.coupon_code,
	ISNULL(o.payment_method,'Unknown') as payment_method, o.shipping_method,
	length_of_time_to_invoice = 0, length_of_time_invoice_to_shipment = 'N/A',
	o.shipping_carrier, 
	o.reminder_date, o.reminder_mobile, o.reminder_period, o.reminder_presc, o.reminder_type,
	o.reorder_on_flag, o.reorder_profile_id, reorder_date=LEFT(CONVERT(VARCHAR(20), o.reorder_date, 120),10), o.reorder_interval, 
	o.presc_verification_method, 
	o.referafriend_code, o.referafriend_referer,
	CASE DATEPART(dw, o.document_date) 
		WHEN 1 THEN '7: Sunday' WHEN 2 THEN '1: Monday' WHEN 3 THEN '2: Tuesday' WHEN 4 THEN '3: Wednesday'
		WHEN 5 THEN '4: Thursday' WHEN 6 THEN '5: Friday' WHEN 7 THEN '6: Saturday'
	END AS document_day,
	CAST(o.document_date AS TIME) AS document_time, DATEPART(HOUR, o.document_date) AS document_hour, CAST(o.document_date AS DATE) AS document_date,
	ISNULL(LEFT(CONVERT(VARCHAR(30), o.customer_first_order_date,120),7),'2001-01') AS customer_first_order_month, 
	customer_order_seq_no=RIGHT('0000'+CAST(ISNULL(o.customer_order_seq_no,0) AS VARCHAR(4)),4),
	o.customer_business_channel, 
	o.telesales_admin_username AS 'telesales_agent',
	
	CASE
		WHEN o.telesales_admin_username IS NULL THEN 'Non-Telesales Order'
		WHEN (a.first_name + ' ' + a.last_name IS NULL) THEN 'Deleted User'
		ELSE a.first_name + ' ' + a.last_name
	END AS 'agent_name',

	COALESCE(
		CASE WHEN o.cc_type = '' THEN NULL ELSE  o.cc_type END, 
		CASE WHEN p.cc_type = '' THEN NULL ELSE  p.cc_type END, 'Other Payment Methods') AS 'cc_type', 

	o.billing_street1 + ' ' + o.billing_city + ' ' + o.billing_region AS 'billing_address', o.billing_postcode, o.billing_country_id, 
	c.country_name, 
	o.shiping_street1 + ' ' + o.shipping_city + ' '  + o.shipping_region AS 'shipping_address', o.shipping_postcode, o.shipping_country_id, 
	c.country_name AS 'Delivery Country',
	cast(o.order_date as date) as order_date, cast(ih.invoice_date as date) as invoice_date, cast(sh.shipment_date as date) as shipment_date,
	format(datediff(day,o.order_date,ih.invoice_date),'000') as days_to_invoice,
	isnull(format(datediff(day,o.order_date,sh.shipment_date),'000'),'N/A') as days_to_shipment,
	isnull(format(datediff(day,ih.invoice_date,sh.shipment_date),'000'),'N/A') as days_invoice_to_shipment

	FROM   
			(select h.* 
			from dw_getlenses.dbo.order_headers h 
			where exists
				(select * from dw_getlenses.dbo.stores where stores.store_name=h.store_name 
					and stores.website_group<>'ASDA' and stores.store_type <>'White Label') 
			union all
			select h.* from dw_proforma.dbo.order_headers h 
			where exists 
				(select * from dw_proforma.dbo.stores where stores.store_name=h.store_name 
					and stores.website_group<>'ASDA' and stores.store_type <>'White Label')) o
		LEFT JOIN 
			dw_getlenses.dbo.countries c ON billing_country_id = c.country_code
		LEFT JOIN 
			dw_getlenses.dbo.admin_user a ON telesales_admin_username = a.[user_name]
		LEFT JOIN 
			Dim_Sales_Flat_Order_Payment p ON o.order_id = p.parent_id
		left join 
			(select order_id, min(invoice_date) as invoice_date 
			from 
				(select order_id, invoice_date 
				from dw_getlenses.dbo.invoice_headers h 
				where exists 
					(select * from dw_getlenses.dbo.stores where stores.store_name=h.store_name 
						and stores.website_group<>'ASDA' and stores.store_type <>'White Label')
				union all
				select order_id, invoice_date 
				from dw_proforma.dbo.invoice_headers h 
				where exists 
					(select * from dw_proforma.dbo.stores where stores.store_name=h.store_name 
						and stores.website_group<>'ASDA' and stores.store_type <>'White Label')) rs
			group by order_id) ih on ih.order_id=o.order_id
		left join 
			(select order_id, max(shipment_date) as shipment_date, max(document_date) as document_date
			from 
				(select order_id, shipment_date, document_date 
				from dw_getlenses.dbo.shipment_headers h 
				where exists
					(select * from dw_getlenses.dbo.stores where stores.store_name=h.store_name 
						and stores.website_group<>'ASDA' and stores.store_type <>'White Label') 
				union all
				select order_id, shipment_date, document_date 
				from dw_proforma.dbo.shipment_headers h 
				where exists
					(select * from dw_proforma.dbo.stores where stores.store_name=h.store_name 
						and stores.website_group<>'ASDA' and stores.store_type <>'White Label')) rs
			group by order_id) sh on sh.order_id=o.order_id


---------------------------------------------------------------------------------------------------
-- dbo.Fact_Orders

	SELECT h.store_name, cast(l.product_id as numeric(10,0)) as product_id,  
		cast(h.customer_id as int) as 'customer_id', 
		'O' + LEFT(h.document_type, 2) + cast(CAST(h.document_id as int) AS varchar(255)) AS UniqueID, 
		h.shipping_country_id, 
		l.qty, 
		l.global_prof_fee, 
        l.global_line_subtotal_inc_vat, l.global_line_subtotal_vat, l.global_line_subtotal_exc_vat, 
        l.global_shipping_inc_vat, l.global_shipping_vat, l.global_shipping_exc_vat, 
		l.global_discount_inc_vat, l.global_discount_vat, l.global_discount_exc_vat, 
		l.global_store_credit_inc_vat, l.global_store_credit_vat, l.global_store_credit_exc_vat, 
		l.global_adjustment_inc_vat, l.global_adjustment_vat, l.global_adjustment_exc_vat, 
		l.global_line_total_inc_vat, l.global_line_total_vat, l.global_line_total_exc_vat, 
		l.global_subtotal_cost, l.global_shipping_cost, l.global_total_cost, 
        l.global_margin_amount, 
		l.local_prof_fee, 
		l.local_line_subtotal_inc_vat, l.local_line_subtotal_vat, l.local_line_subtotal_exc_vat, 
		l.local_shipping_inc_vat, l.local_shipping_vat, l.local_shipping_exc_vat, 
        l.local_discount_inc_vat, l.local_discount_vat, l.local_discount_exc_vat, 
		l.local_store_credit_inc_vat, l.local_store_credit_vat, l.local_store_credit_exc_vat, 
		l.local_adjustment_inc_vat, l.local_adjustment_vat, l.local_adjustment_exc_vat, 
		l.local_line_total_inc_vat, l.local_line_total_vat, l.local_line_total_exc_vat, 
        l.local_subtotal_cost, l.local_shipping_cost, l.local_total_cost, 
		l.local_margin_amount, 
        l.local_to_global_rate, 
		cast(h.document_date as date) as document_date, '' AS invoice_no, h.document_type, h.order_no, 
        h.shipping_carrier, 
		h.reminder_date, h.reminder_mobile, 
		CASE h.reminder_period WHEN '' then NULL ELSE h.reminder_period END AS reminder_period, 
        h.reminder_presc, h.reminder_type, 
		h.reorder_on_flag, h.reorder_profile_id, h.reorder_date, h.reorder_interval, 
		h.presc_verification_method, 
		h.referafriend_code, h.referafriend_referer, 
		l.lens_eye, l.lens_base_curve, l.lens_diameter, l.lens_power, l.lens_cylinder, l.lens_axis, l.lens_addition, l.lens_dominance, l.lens_colour, 
        l.lens_days, l.sku, 
		l.global_line_total_exc_vat AS global_line_total_exc_vat_for_localtoglobal, l.local_line_total_exc_vat AS local_line_total_exc_vat_for_localtoglobal, 
		l.line_weight, 
		(cast(1 as decimal(12,4)) / ls.row_count)AS document_count, (cast(1 as decimal(12,4)) / ps.row_count) AS product_count,
		'O' + LEFT(l.document_type, 2) + Cast(l.document_id AS varchar) + '-' + cast(l.product_id as varchar) + '-' + isnull(CAST(abs(l.qty) AS varchar),'0.0000') AS UniqueIDQP,
		CASE WHEN l.qty > 0 THEN 1 WHEN l.qty = 0 THEN 0 WHEN l.qty < 0 THEN -1 END AS line_count,
		length_of_time_to_invoice = 0, length_of_time_invoice_to_shipment = 0,
		cast((sku+'-LQ' + isnull(CAST(cast(abs(l.qty) as integer) AS varchar),'0')) as varchar(255)) as sku_for_parameters,
		CASE datepart(dw, h.document_date) 
			WHEN 1 THEN '7: Sunday' WHEN 2 THEN '1: Monday' WHEN 3 THEN '2: Tuesday' WHEN 4 THEN '3: Wednesday'
			WHEN 5 THEN '4: Thursday' WHEN 6 THEN '5: Friday' WHEN 7 THEN '6: Saturday'
		END as document_day,
		cast(h.document_date as time) as document_time, datepart(hour, h.document_date) as document_hour
		   
	FROM        
			dw_getlenses.dbo.order_headers h 
		INNER JOIN 
			dw_getlenses.dbo.order_lines l ON h.document_type = l.document_type AND h.document_id =  l.document_id
		LEFT JOIN 
			dw_getlenses.dbo.dw_order_line_sum ls ON ls.document_id = l.document_id AND ls.document_type =  l.document_type
		LEFT JOIN 
			dw_getlenses.dbo.dw_order_line_product_sum ps ON ps.document_id = l.document_id AND ps.document_type =  l.document_type AND ps.product_id = l.product_id
					  
	where h.document_date <
		(select cast(current_date_time as date) 
		from dw_sales_proforma.dbo.current_date_time) and 
		exists (select * from DW_GetLenses.dbo.stores where stores.store_name =l.store_name 
			and stores.website_group<>'ASDA' and stores.store_type <>'White Label') 
	union all

	SELECT h.store_name, cast(l.product_id as numeric(10,0)) as product_id, cast(h.customer_id as int) as 'customer_id', 'O' + LEFT(h.document_type, 2) 
                      + cast(CAST(h.document_id as int) AS varchar(255)) AS UniqueID, h.shipping_country_id, l.qty, l.global_prof_fee, 
                      l.global_line_subtotal_inc_vat, l.global_line_subtotal_vat, l.global_line_subtotal_exc_vat, 
                      l.global_shipping_inc_vat, l.global_shipping_vat, l.global_shipping_exc_vat, l.global_discount_inc_vat, 
                      l.global_discount_vat, l.global_discount_exc_vat, l.global_store_credit_inc_vat, l.global_store_credit_vat, 
                      l.global_store_credit_exc_vat, l.global_adjustment_inc_vat, l.global_adjustment_vat, 
                      l.global_adjustment_exc_vat, l.global_line_total_inc_vat, l.global_line_total_vat, 
                      l.global_line_total_exc_vat, l.global_subtotal_cost, l.global_shipping_cost, l.global_total_cost, 
                      l.global_margin_amount, l.local_prof_fee, l.local_line_subtotal_inc_vat, l.local_line_subtotal_vat, 
                      l.local_line_subtotal_exc_vat, l.local_shipping_inc_vat, l.local_shipping_vat, l.local_shipping_exc_vat, 
                      l.local_discount_inc_vat, l.local_discount_vat, l.local_discount_exc_vat, l.local_store_credit_inc_vat, 
                      l.local_store_credit_vat, l.local_store_credit_exc_vat, l.local_adjustment_inc_vat, l.local_adjustment_vat, 
                      l.local_adjustment_exc_vat, l.local_line_total_inc_vat, l.local_line_total_vat, l.local_line_total_exc_vat, 
                      l.local_subtotal_cost, l.local_shipping_cost, l.local_total_cost, l.local_margin_amount, 
                      l.local_to_global_rate, cast(h.document_date as date) as document_date, '' AS invoice_no, h.document_type, h.order_no, 
                      h.shipping_carrier, h.reminder_date, h.reminder_mobile, 
					  
					  
					  
						CASE h.reminder_period 
							WHEN '' then NULL
							ELSE h.reminder_period
							END AS reminder_period, 
                      h.reminder_presc, h.reminder_type, h.reorder_on_flag, h.reorder_profile_id, 
                      h.reorder_date, h.reorder_interval, h.presc_verification_method, h.referafriend_code, 
                      h.referafriend_referer, l.lens_eye, l.lens_base_curve, l.lens_diameter, l.lens_power, 
                      l.lens_cylinder, l.lens_axis, l.lens_addition, l.lens_dominance, l.lens_colour, 
                      l.lens_days, l.sku, l.global_line_total_exc_vat AS global_line_total_exc_vat_for_localtoglobal, 
                      l.local_line_total_exc_vat AS local_line_total_exc_vat_for_localtoglobal, l.line_weight,
					  (cast(1 as decimal(12,4)) / ls.row_count)AS document_count, 
					  (cast(1 as decimal(12,4)) / ps.row_count) AS product_count,
					  'O' + LEFT(l.document_type, 2) + Cast(l.document_id AS varchar) + '-' + cast(l.product_id as varchar) + '-' + isnull(CAST(abs(l.qty) AS varchar),'0.0000') AS UniqueIDQP,
					  CASE 
						WHEN l.qty > 0 THEN 1
						WHEN l.qty = 0 THEN 0
						WHEN l.qty < 0 THEN -1
					  END AS line_count,
					  length_of_time_to_invoice = 0,
					  length_of_time_invoice_to_shipment = 0,
					  cast((sku+'-LQ' + isnull(CAST(cast(abs(l.qty) as integer) AS varchar),'0')) as varchar(255)) as sku_for_parameters,
					  CASE datepart(dw, h.document_date) 
						WHEN 1 THEN '7: Sunday'
						WHEN 2 THEN '1: Monday'
						WHEN 3 THEN '2: Tuesday'
						WHEN 4 THEN '3: Wednesday'
						WHEN 5 THEN '4: Thursday'
						WHEN 6 THEN '5: Friday'
						WHEN 7 THEN '6: Saturday'
					  END as document_day,
					  cast(h.document_date as time) as document_time,
					  datepart(hour, h.document_date) as document_hour
	FROM				
			dw_proforma.dbo.order_headers h 
		INNER JOIN 
			dw_proforma.dbo.order_lines l ON h.document_type = l.document_type AND h.document_id =  l.document_id and h.store_name=l.store_name
		LEFT JOIN 
			dw_proforma.dbo.dw_order_line_sum ls ON ls.document_id = l.document_id AND ls.document_type =  l.document_type
		LEFT JOIN 
			dw_proforma.dbo.dw_order_line_product_sum ps ON ps.document_id = l.document_id AND ps.document_type =  l.document_type AND ps.product_id = l.product_id
	where h.document_date < 
		(select cast(current_date_time as date) 
		from dw_sales_proforma.dbo.current_date_time) and 
		exists (select * from DW_Proforma.dbo.stores where stores.store_name =h.store_name 
			and stores.website_group<>'ASDA' and stores.store_type <>'White Label')  
					