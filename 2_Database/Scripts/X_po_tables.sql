
-- PO Sales Pla Item
select top 1000 id, 
	order_id, order_item_id, 
	promo_key, 
	product_id
	price
from DW_GetLenses.dbo.po_sales_pla_item

	select promo_key, count(*)
	from DW_GetLenses.dbo.po_sales_pla_item
	group by promo_key
	order by promo_key;

	select product_id, count(*)
	from DW_GetLenses.dbo.po_sales_pla_item
	group by product_id
	order by product_id;

-- PO Warehouse Stock Level
select top 100 slevel_item_id, 
	available_value, allocated_value, 
	location_id, 
	updated_at
from DW_GetLenses.dbo.po_warehouse_stock_level
where available_value = 1;

	select location_id, count(*)
	from DW_GetLenses.dbo.po_warehouse_stock_level
	group by location_id
	order by location_id;

	select top 1000 available_value, count(*)
	from DW_GetLenses.dbo.po_warehouse_stock_level
	group by available_value
	order by available_value;

	select top 1000 allocated_value, count(*)
	from DW_GetLenses.dbo.po_warehouse_stock_level
	group by allocated_value
	order by allocated_value;
