
-- Order Headers
select top 1000 
	order_id, order_no, 
	store_name, 

	order_type, order_lifecycle, 
	auto_verification, presc_verification_method, 

	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, approved_time, 

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef,
	telesales_method_code, telesales_admin_username, 

	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel, 
	customer_order_seq_no,  
	remote_ip, 

	status, coupon_code, 
	weight, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent,

	referafriend_code, referafriend_referer, 

	shipping_carrier, shipping_method, 

	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, 

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 
	
	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shiping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 
	
	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 
	
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 
	
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 
	
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.order_headers;

	select store_name, count(*)
	from DW_GetLenses.dbo.order_headers
	group by store_name
	order by store_name;

	select order_type, count(*)
	from DW_GetLenses.dbo.order_headers
	group by order_type
	order by order_type;

	select order_lifecycle, count(*)
	from DW_GetLenses.dbo.order_headers
	group by order_lifecycle
	order by order_lifecycle;

	select auto_verification, count(*)
	from DW_GetLenses.dbo.order_headers
	group by auto_verification
	order by auto_verification;

	select presc_verification_method, count(*)
	from DW_GetLenses.dbo.order_headers
	group by presc_verification_method
	order by presc_verification_method;

	select document_type, count(*)
	from DW_GetLenses.dbo.order_headers
	group by document_type
	order by document_type;

	select business_source, affilCode, count(*)
	from DW_GetLenses.dbo.order_headers
	group by business_source, affilCode
	order by business_source, affilCode;

	select business_channel, count(*)
	from DW_GetLenses.dbo.order_headers
	group by business_channel
	order by business_channel;

	select customer_business_channel, count(*)
	from DW_GetLenses.dbo.order_headers
	group by customer_business_channel
	order by customer_business_channel;

	select affilCode, count(*)
	from DW_GetLenses.dbo.order_headers
	group by affilCode
	order by affilCode;

	select telesales_method_code, count(*)
	from DW_GetLenses.dbo.order_headers
	group by telesales_method_code
	order by telesales_method_code;

	select status, count(*)
	from DW_GetLenses.dbo.order_headers
	group by status
	order by status;

	select reorder_on_flag, automatic_reorder, count(*)
	from DW_GetLenses.dbo.order_headers
	group by reorder_on_flag, automatic_reorder
	order by reorder_on_flag, automatic_reorder;

	select reminder_type, count(*)
	from DW_GetLenses.dbo.order_headers
	group by reminder_type
	order by reminder_type;

-- Order Lines
select top 1000 
	line_id, 
	order_id, order_no, store_name, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from DW_GetLenses.dbo.order_lines;

	select document_type, count(*)
	from DW_GetLenses.dbo.order_lines
	group by document_type
	order by document_type;

	select product_type, count(*)
	from DW_GetLenses.dbo.order_lines
	group by product_type
	order by product_type;

	select price_type, count(*)
	from DW_GetLenses.dbo.order_lines
	group by price_type
	order by price_type;

	select is_virtual, count(*)
	from DW_GetLenses.dbo.order_lines
	group by is_virtual
	order by is_virtual;

	select free_shipping, count(*)
	from DW_GetLenses.dbo.order_lines
	group by free_shipping
	order by free_shipping;

	select no_discount, count(*)
	from DW_GetLenses.dbo.order_lines
	group by no_discount
	order by no_discount;

-- Order Lines Cancel
select top 1000 line_id, 
	order_id, store_name, order_no, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.order_lines_cancel;

-- Order Lines Credit Memo
select top 1000 line_id, 
	order_id, store_name, order_no, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.order_lines_creditmemo;

-- Order Lines Order
select top 1000 line_id, 
	order_id, store_name, order_no, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.order_lines_order;

-- Order Status History
select top 1000 status_id,
	order_id, order_no,
	status, status_time, 
	admin_username
from DW_GetLenses.dbo.order_status_history;

	select count(*), min(status_time), max(status_time)
	from DW_GetLenses.dbo.order_status_history;

	select status, count(*)
	from DW_GetLenses.dbo.order_status_history
	group by status
	order by status;

-------------------------------------------------------------------------------------------------------

-- Order Hist
select top 1000 order_id, 
	source, store_id, customer_id, 
	created_at, 
	grand_total
from DW_GetLenses.dbo.dw_hist_order;

	select source, count(*)
	from DW_GetLenses.dbo.dw_hist_order
	group by source
	order by source;

	select store_id, count(*)
	from DW_GetLenses.dbo.dw_hist_order
	group by store_id
	order by store_id;

-- Order Hist Item
select top 1000 order_id, 
	source, 
	product_id, sku, 
	qty_ordered, row_total
from DW_GetLenses.dbo.dw_hist_order_item;

	select product_id, count(*)
	from DW_GetLenses.dbo.dw_hist_order_item
	group by product_id
	order by product_id;

	select product_id, sku, count(*)
	from DW_GetLenses.dbo.dw_hist_order_item
	group by product_id, sku
	order by product_id, sku;

-------------------------------------------------------------------------------------------------------

-- Order Line Sum
select top 1000 
	document_id, 
	document_type, 
	row_count
from DW_GetLenses.dbo.dw_order_line_sum
where document_id = 1576397;

	select document_type, count(*)
	from DW_GetLenses.dbo.dw_order_line_sum
	group by document_type
	order by document_type;

-- Order Line Product Sum
select top 1000 
	document_id, 
	document_type, 
	product_id, row_count
from DW_GetLenses.dbo.dw_order_line_product_sum
where document_id = 1576397;

	select document_type, count(*)
	from DW_GetLenses.dbo.dw_order_line_product_sum
	group by document_type
	order by document_type;

	select product_id, count(*)
	from DW_GetLenses.dbo.dw_order_line_product_sum
	group by product_id
	order by product_id;

	select top 100 document_id, count(*) num
	from DW_GetLenses.dbo.dw_order_line_product_sum
	group by document_id
	order by num desc;



-------------------------------------------------------------------------------------------------------
