
-- Stores
select store_id, 
	store_name, website, website_group, store_type, 
	tld, code_tld
--select *
from DW_Sales_Actual.dbo.Dim_Stores_All
order by website_group, store_name;

select store_id, 
	store_name, website, website_group, store_type, 
	tld, code_tld
from DW_Sales_Actual.dbo.Dim_Stores_All_New
order by store_type, website_group, store_name;

-- Categories
select sort, category
from DW_Sales_Actual.dbo.Dim_Categories
order by category

select sort, category
from DW_Sales_Actual.dbo.Dim_Categories_new
order by category

-- All Categories
select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product
from DW_Sales_Actual.dbo.Dim_All_Categories
order by category_id, category, modality, type, feature;

select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product
from DW_Sales_Actual.dbo.Dim_All_Categories_new
order by product_type, category, modality, type, feature;

-- Products
select 
	category_sort, product_type, manufacturer, category, LensType, modality, feature,  
	product_id, store_name, name, 
	sku, equivalent_sku, replacement_sku, autoreorder_alter_sku, 
	status, 
	
	
	product_lifecycle,  
	telesales_only, stocked_lens, 

	average_cost, weight, 
	daysperlens, alt_daysperlens, --equivalence, availability, condition,
	promotional_product, google_shopping_gtin,
	
	pack_qty, alt_pack_qty, base_pack_qty, base_no_packs, 
	
	created_at, updated_at 
from DW_Sales_Actual.dbo.Dim_Products
where product_type in ('{Promo} Contact Lenses - Dailies', '{Promo} Other') -- Contact Lenses - Dailies, Contact Lenses - Monthlies & Other, Solutions & Eye Care, Other, Sunglasses, {Promo} Contact Lenses - Dailies, {Promo} Other
--where product_id = 2975
order by category_sort, manufacturer, product_type, lensType, modality, feature, product_id;

select *
from DW_Sales_Actual.dbo.Dim_Products_new

	select distinct manufacturer
	from DW_Sales_Actual.dbo.Dim_Products
	order by manufacturer

	select distinct product_type
	from DW_Sales_Actual.dbo.Dim_Products
	order by product_type

-- Countries
select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_Sales_Actual.dbo.Dim_Countries
order by country_type, country_zone, country_continent;

select country_name, country_code, 
	country_type, country_zone, country_continent, country_state
from DW_Sales_Actual.dbo.Dim_Countries_new 
order by country_type, country_zone, country_continent;

-- Business Channels
select business_channel
from DW_Sales_Actual.dbo.Dim_Business_Channels
order by business_channel;

select business_channel
from DW_Sales_Actual.dbo.Dim_Business_Channels_new

-- Coupon Codes
select coupon_code, 
	start_date, end_date, --description,
	channel, new_customer
from DW_Sales_Actual.dbo.Dim_Coupon_Codes
order by channel, new_customer, coupon_code;

	select channel, count(*)
	from DW_Sales_Actual.dbo.Dim_Coupon_Codes
	group by channel
	order by channel

select coupon_code, 
	start_date, end_date, description,
	channel, new_customer
from DW_Sales_Actual.dbo.Dim_Coupon_Codes_new
order by channel, new_customer, coupon_code;

-- Payment Methods
select payment_method
from DW_Sales_Actual.dbo.Dim_Payment_Methods
order by payment_method;

select payment_method
from DW_Sales_Actual.dbo.Dim_Payment_Methods_new

-- Card Type
select cc_type
from DW_Sales_Actual.dbo.Dim_Card_Type
order by cc_type;

select cc_type
from DW_Sales_Actual.dbo.Dim_Card_Type_new

-- Telesale Agents
select telesales_admin_username, agent_name
from DW_Sales_Actual.dbo.Dim_Telesales_Agents
order by telesales_admin_username;

select telesales_admin_username, agent_name
from DW_Sales_Actual.dbo.Dim_Telesales_Agents_new


------------------------------------------------------------------------------------------------------


-- Customer Order Seq
select top 1000 customer_order_seq_no
from DW_Sales_Actual.dbo.Dim_Customer_Order_Seq_No
order by customer_order_seq_no;

-- Customer Channels New
select business_channel
from DW_Sales_Actual.dbo.Dim_Customer_Channels_New
order by business_channel;

-- Days to Desptach
select *
from DW_Sales_Actual.dbo.Dim_Days_to_Desptach
order by days_to_shipment

-- First Order Month
select *
from DW_Sales_Actual.dbo.Dim_First_Order_Month
order by customer_first_order_month

-- Sales Flat Order Payment
select top 1000 *
from DW_Sales_Actual.dbo.Dim_Sales_Flat_Order_Payment


