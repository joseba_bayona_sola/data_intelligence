
-- Stores
select store_name, website_group, store_type
from DW_Proforma.dbo.stores
order by website_group, store_type, store_name;

-- Stores Full
select store_id, store_name, website_group, store_type, visible
from DW_Proforma.dbo.dw_stores_full
order by website_group, store_type, store_name;