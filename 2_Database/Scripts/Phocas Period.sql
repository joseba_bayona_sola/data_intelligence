
-- PeriodType
select id, name, 
	depth, moving, periodqty
from Phocas_Sales_Actual_Live.dbo.PeriodType
order by id

-- Period
select id, periodTypeID, name, 
	startAt, EndAt, 
	CustomYear
from Phocas_Sales_Actual_Live.dbo.Period
order by periodTypeID, id

	select periodTypeID, count(*)
	from Phocas_Sales_Actual_Live.dbo.Period
	group by periodTypeID
	order by periodTypeID;

	select periodTypeID, CustomYear, count(*)
	from Phocas_Sales_Actual_Live.dbo.Period
	group by periodTypeID, CustomYear
	order by periodTypeID, CustomYear;


-- PeriodIndex_Day
select moment, periodIndex
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Day

-- PeriodIndex_Month
select moment, periodIndex
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Month

-- PeriodIndex_Year
select moment, periodIndex
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_Year



-- PeriodIndex_0 (Financial Year)
select moment, PeriodIndex
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_0

-- PeriodIndex_1 (Week)
select moment, PeriodIndex
from Phocas_Sales_Actual_Live.dbo.PeriodIndex_1