
-- Core Config Data

select top 1000 
	config_id, store_id, 
	path, value
from DW_GetLenses.dbo.dw_core_config_data;

	select store_id, count(*)
	from DW_GetLenses.dbo.dw_core_config_data
	group by store_id
	order by store_id;

select config_id, scope_id, scope, 
	path, value
from DW_GetLenses.dbo.stage_core_config_data;

select config_id, store_id, 
	path, value
from DW_GetLenses.dbo.stage_dw_core_config_data;

select 
	config_id, scope_id, scope, 
	path, value
from DW_GetLenses.dbo.core_config_data;
	
	select scope_id, scope, count(*)
	from DW_GetLenses.dbo.core_config_data
	group by scope_id, scope
	order by scope_id, scope;

-- DW Rebuild Build List
select table_name, build_from
from DW_GetLenses.dbo.dw_rebuild_build_list
order by table_name, build_from;

-- DW Rebuild Tables
select table_name, 
	table_type, enabled, 
	create_from, 
	update_type, update_id, 
	live, synced
from DW_GetLenses.dbo.dw_rebuild_tables
order by table_name;

-- DW Sync Status
select *
from DW_GetLenses.dbo.dw_sync_status

select event_type, event_time, event_message
from DW_GetLenses.dbo.dw_sync_status
where event_type in ('start_time', 'vw_products', 'order_headers', 'order_lines')
order by event_type, event_time desc;

	select event_type, 
		count(*), min(event_time), max(event_time)
	from DW_GetLenses.dbo.dw_sync_status
	group by event_type
	order by event_type;
