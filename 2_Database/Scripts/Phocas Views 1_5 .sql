USE [Phocas_Sales_Actual_Live]
GO

CREATE VIEW [dbo].[Summary_Day_1_5] AS 
	SELECT StreamsID , Branch_1, Branch_5, PeriodIndex , 
		Scalar_1, Scalar_2, Scalar_3, Scalar_4, Scalar_5, Scalar_6, Scalar_7, Scalar_8, Scalar_9, 
		Scalar_10, Scalar_11, Scalar_12, Scalar_13, Scalar_14, Scalar_15, Scalar_16, Scalar_17, Scalar_18, Scalar_19, 
		Scalar_20, Scalar_21, Scalar_22, Scalar_23, Scalar_24, Scalar_25, Scalar_26, Scalar_27, Scalar_28, Scalar_29, Scalar_30, 
		RecordCount 
	FROM Transaction_Day_1_5 /* WITH (NOEXPAND)*/ 
	UNION ALL 
	SELECT StreamsID , Branch_1, Branch_5, pm.PeriodIndex AS PeriodIndex , 
		SUM(Scalar_1) AS Scalar_1, SUM(Scalar_2) AS Scalar_2, SUM(Scalar_3) AS Scalar_3, SUM(Scalar_4) AS Scalar_4, SUM(Scalar_5) AS Scalar_5, 
		SUM(Scalar_6) AS Scalar_6, SUM(Scalar_7) AS Scalar_7, SUM(Scalar_8) AS Scalar_8, SUM(Scalar_9) AS Scalar_9, 
		SUM(Scalar_10) AS Scalar_10, SUM(Scalar_11) AS Scalar_11, SUM(Scalar_12) AS Scalar_12, SUM(Scalar_13) AS Scalar_13, SUM(Scalar_14) AS Scalar_14, 
		SUM(Scalar_15) AS Scalar_15, SUM(Scalar_16) AS Scalar_16, SUM(Scalar_17) AS Scalar_17, SUM(Scalar_18) AS Scalar_18, SUM(Scalar_19) AS Scalar_19, 
		SUM(Scalar_20) AS Scalar_20, SUM(Scalar_21) AS Scalar_21, SUM(Scalar_22) AS Scalar_22, SUM(Scalar_23) AS Scalar_23, SUM(Scalar_24) AS Scalar_24, 
		SUM(Scalar_25) AS Scalar_25, SUM(Scalar_26) AS Scalar_26, SUM(Scalar_27) AS Scalar_27, SUM(Scalar_28) AS Scalar_28, SUM(Scalar_29) AS Scalar_29, SUM(Scalar_30) AS Scalar_30, 
		COUNT_BIG(*) AS RecordCount 
	FROM 
			dbo.QuantaB 
		INNER JOIN 
			PeriodIndex_Day pm on (pm.Moment = QuantaB.Moment)GROUP BY StreamsID, pm.PeriodIndex , Branch_1, Branch_5
GO


CREATE VIEW [dbo].[Summary_Day_1_5_10] AS 
	SELECT StreamsID , Branch_1, Branch_5, Entity_5.Grp_10 AS Branch_10, PeriodIndex , 
		SUM(Scalar_1) AS Scalar_1, SUM(Scalar_2) AS Scalar_2, SUM(Scalar_3) AS Scalar_3, SUM(Scalar_4) AS Scalar_4, SUM(Scalar_5) AS Scalar_5, 
		SUM(Scalar_6) AS Scalar_6, SUM(Scalar_7) AS Scalar_7, SUM(Scalar_8) AS Scalar_8, SUM(Scalar_9) AS Scalar_9, 
		SUM(Scalar_10) AS Scalar_10, SUM(Scalar_11) AS Scalar_11, SUM(Scalar_12) AS Scalar_12, SUM(Scalar_13) AS Scalar_13, SUM(Scalar_14) AS Scalar_14, 
		SUM(Scalar_15) AS Scalar_15, SUM(Scalar_16) AS Scalar_16, SUM(Scalar_17) AS Scalar_17, SUM(Scalar_18) AS Scalar_18, SUM(Scalar_19) AS Scalar_19, 
		SUM(Scalar_20) AS Scalar_20, SUM(Scalar_21) AS Scalar_21, SUM(Scalar_22) AS Scalar_22, SUM(Scalar_23) AS Scalar_23, SUM(Scalar_24) AS Scalar_24, 
		SUM(Scalar_25) AS Scalar_25, SUM(Scalar_26) AS Scalar_26, SUM(Scalar_27) AS Scalar_27, SUM(Scalar_28) AS Scalar_28, SUM(Scalar_29) AS Scalar_29, SUM(Scalar_30) AS Scalar_30, 
		SUM(RecordCount) AS RecordCount 
	FROM 
			Transaction_Day_1_5 /* WITH (NOEXPAND)*/ 
		INNER JOIN 
			dbo.Entity_1 ON (Entity_1.ID = Transaction_Day_1_5.Branch_1) 
		INNER JOIN 
			dbo.Entity_5 ON (Entity_5.ID = Transaction_Day_1_5.Branch_5) 
	GROUP BY PeriodIndex, StreamsID , Branch_1, Branch_5, Entity_5.Grp_10 
	UNION ALL 
	SELECT StreamsID , Branch_1, Branch_5, Branch_10, pm.PeriodIndex AS PeriodIndex , 
		SUM(Scalar_1) AS Scalar_1, SUM(Scalar_2) AS Scalar_2, SUM(Scalar_3) AS Scalar_3, SUM(Scalar_4) AS Scalar_4, SUM(Scalar_5) AS Scalar_5, 
		SUM(Scalar_6) AS Scalar_6, SUM(Scalar_7) AS Scalar_7, SUM(Scalar_8) AS Scalar_8, SUM(Scalar_9) AS Scalar_9, 
		SUM(Scalar_10) AS Scalar_10, SUM(Scalar_11) AS Scalar_11, SUM(Scalar_12) AS Scalar_12, SUM(Scalar_13) AS Scalar_13, SUM(Scalar_14) AS Scalar_14, 
		SUM(Scalar_15) AS Scalar_15, SUM(Scalar_16) AS Scalar_16, SUM(Scalar_17) AS Scalar_17, SUM(Scalar_18) AS Scalar_18, SUM(Scalar_19) AS Scalar_19, 
		SUM(Scalar_20) AS Scalar_20, SUM(Scalar_21) AS Scalar_21, SUM(Scalar_22) AS Scalar_22, SUM(Scalar_23) AS Scalar_23, SUM(Scalar_24) AS Scalar_24, 
		SUM(Scalar_25) AS Scalar_25, SUM(Scalar_26) AS Scalar_26, SUM(Scalar_27) AS Scalar_27, SUM(Scalar_28) AS Scalar_28, SUM(Scalar_29) AS Scalar_29, SUM(Scalar_30) AS Scalar_30, 
		COUNT_BIG(*) AS RecordCount 
	FROM 
			dbo.QuantaB 
		INNER JOIN 
			PeriodIndex_Day pm on (pm.Moment = QuantaB.Moment)
	GROUP BY StreamsID, pm.PeriodIndex , Branch_1, Branch_5, Branch_10
GO

CREATE VIEW [dbo].[Summary_Day_1_5_15] AS 
	SELECT StreamsID , Branch_1, Branch_5, Entity_11.Grp_15 AS Branch_15, PeriodIndex , 
		SUM(Scalar_1) AS Scalar_1, SUM(Scalar_2) AS Scalar_2, SUM(Scalar_3) AS Scalar_3, SUM(Scalar_4) AS Scalar_4, SUM(Scalar_5) AS Scalar_5, 
		SUM(Scalar_6) AS Scalar_6, SUM(Scalar_7) AS Scalar_7, SUM(Scalar_8) AS Scalar_8, SUM(Scalar_9) AS Scalar_9, 
		SUM(Scalar_10) AS Scalar_10, SUM(Scalar_11) AS Scalar_11, SUM(Scalar_12) AS Scalar_12, SUM(Scalar_13) AS Scalar_13, SUM(Scalar_14) AS Scalar_14, 
		SUM(Scalar_15) AS Scalar_15, SUM(Scalar_16) AS Scalar_16, SUM(Scalar_17) AS Scalar_17, SUM(Scalar_18) AS Scalar_18, SUM(Scalar_19) AS Scalar_19, 
		SUM(Scalar_20) AS Scalar_20, SUM(Scalar_21) AS Scalar_21, SUM(Scalar_22) AS Scalar_22, SUM(Scalar_23) AS Scalar_23, SUM(Scalar_24) AS Scalar_24, 
		SUM(Scalar_25) AS Scalar_25, SUM(Scalar_26) AS Scalar_26, SUM(Scalar_27) AS Scalar_27, SUM(Scalar_28) AS Scalar_28, SUM(Scalar_29) AS Scalar_29, SUM(Scalar_30) AS Scalar_30, 
		SUM(RecordCount) AS RecordCount 
	FROM 
			Transaction_Day_1_5_11 /* WITH (NOEXPAND)*/ 
		INNER JOIN 
			dbo.Entity_1 ON (Entity_1.ID = Transaction_Day_1_5_11.Branch_1) 
		INNER JOIN 
			dbo.Entity_5 ON (Entity_5.ID = Transaction_Day_1_5_11.Branch_5) 
		INNER JOIN 
			dbo.Entity_11 ON (Entity_11.ID = Transaction_Day_1_5_11.Branch_11) 
	GROUP BY PeriodIndex, StreamsID , Branch_1, Branch_5, Entity_11.Grp_15 
	UNION ALL 
	SELECT StreamsID , Branch_1, Branch_5, Branch_15, pm.PeriodIndex AS PeriodIndex , 
		SUM(Scalar_1) AS Scalar_1, SUM(Scalar_2) AS Scalar_2, SUM(Scalar_3) AS Scalar_3, SUM(Scalar_4) AS Scalar_4, SUM(Scalar_5) AS Scalar_5, 
		SUM(Scalar_6) AS Scalar_6, SUM(Scalar_7) AS Scalar_7, SUM(Scalar_8) AS Scalar_8, SUM(Scalar_9) AS Scalar_9, 
		SUM(Scalar_10) AS Scalar_10, SUM(Scalar_11) AS Scalar_11, SUM(Scalar_12) AS Scalar_12, SUM(Scalar_13) AS Scalar_13, SUM(Scalar_14) AS Scalar_14, 
		SUM(Scalar_15) AS Scalar_15, SUM(Scalar_16) AS Scalar_16, SUM(Scalar_17) AS Scalar_17, SUM(Scalar_18) AS Scalar_18, SUM(Scalar_19) AS Scalar_19, 
		SUM(Scalar_20) AS Scalar_20, SUM(Scalar_21) AS Scalar_21, SUM(Scalar_22) AS Scalar_22, SUM(Scalar_23) AS Scalar_23, SUM(Scalar_24) AS Scalar_24, 
		SUM(Scalar_25) AS Scalar_25, SUM(Scalar_26) AS Scalar_26, SUM(Scalar_27) AS Scalar_27, SUM(Scalar_28) AS Scalar_28, SUM(Scalar_29) AS Scalar_29, SUM(Scalar_30) AS Scalar_30, 
		COUNT_BIG(*) AS RecordCount 
	FROM 
			dbo.QuantaB 
		INNER JOIN 
			PeriodIndex_Day pm on (pm.Moment = QuantaB.Moment)
	GROUP BY StreamsID, pm.PeriodIndex , Branch_1, Branch_5, Branch_15
GO
