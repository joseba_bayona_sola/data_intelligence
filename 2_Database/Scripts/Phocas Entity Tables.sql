
-- Entity 1: Stores
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_1;

-- Entity 2: ??? (Stores)
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_2;

-- Entity 3: ??? (Stores)
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_3;

-- Entity 4: ??? (Stores)
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_4;

-- Entity 5: Products
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_5;

-- Entity 6: ??? (Manufacturer)
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_6;

-- Entity 7: Categories
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_7;

-- Entity 8: (Categories) - Product Type
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_8;

-- Entity 9: All Categories
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_9;

-- Entity 10: Lens Modality
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_10;


-- Entity 11: Headers: O-I-S
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_11;

-- Entity 12: Order Types
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_12;

-- Entity 13: Order Lifecycle
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_13;

-- Entity 14: Customer Order Seq
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_14;

-- Entity 15: Business Channel
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_15;

-- Entity 16: Coupon Codes
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_16;

-- Entity 17: Payment Methods
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_17;

-- Entity 18: Cart Type
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_18;

-- Entity 19: Shipping Methods
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_19;


-- Entity 20: Shipping Carrier
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_20;

-- Entity 21: Days to Despatch
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_21;

-- Entity 22: Telesale Agents
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_22;

-- Entity 23: Qty Prices: O-I-S
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_23;

-- Entity 24: Qty Prices ???
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_24;

-- Entity 25: Qty Prices ???
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_25;

-- Entity 26: Qty Prices ???
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_26;

-- Entity 27: Parameters: O-I-S
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_27;

-- Entity 28: Customers
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_28;

-- Entity 29: Customer Channels
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_29;



-- Entity 30: First Order Month
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_30;

-- Entity 31: Countries
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_31;

-- Entity 32: Country Type
select top 1000 *
from Phocas_Sales_Actual_Live.dbo.Entity_32;


