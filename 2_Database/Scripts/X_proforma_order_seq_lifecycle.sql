

-- ORDER LIFECYCLE
select top 1000 *
from DW_Proforma.dbo.order_lifecycle

	select count(*) -- 2.323.433
	from DW_Proforma.dbo.order_lifecycle

select top 1000 customer_id, 
	store_id, source, 
	order_id, order_no, created_at, 
	status, 
	
	order_rank_all, 
	order_rank_cancel, order_rank_cancel_only, 
	order_rank, 
	prev_status,
	order_lifecycle_all, order_lifecycle_cancel, 
	order_lifecycle	
from DW_Proforma.dbo.order_lifecycle
where customer_id in (28, 1308902)
order by customer_id, created_at, order_rank

	select customer_id, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_lifecycle
	group by customer_id
	order by count(*) desc

	select store_id, source, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_lifecycle
	group by store_id, source 
	order by store_id, source

		select store_id, count(*) -- 2.324.112
		from DW_Proforma.dbo.order_lifecycle
		group by store_id
		order by store_id

		select source, count(*) -- 2.324.112
		from DW_Proforma.dbo.order_lifecycle
		group by source
		order by source

	select status, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_lifecycle
	group by status
	order by status


	select order_lifecycle, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_lifecycle
	group by order_lifecycle
	order by order_lifecycle




-- ORDER SEQ
select top 1000 *
from DW_Proforma.dbo.order_seq

	select count(*) -- 2.324.112
	from DW_Proforma.dbo.order_seq

select top 1000 customer_id, 
	store_id, source, 
	document_id, document_type, order_id, created_at,

	order_rank, 
	next_document_type, prev_document_type, 
	full_credit, prev_full_credit, 

	order_seq_plus, order_seq_minus, 
	order_seq, 

	document_seq 
from DW_Proforma.dbo.order_seq
where customer_id in (28, 1308902)
order by customer_id, created_at

	select customer_id, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_seq
	group by customer_id
	order by count(*) desc

	select store_id, source, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_seq
	group by store_id, source 
	order by store_id, source

		select store_id, count(*) -- 2.324.112
		from DW_Proforma.dbo.order_seq
		group by store_id
		order by store_id

		select source, count(*) -- 2.324.112
		from DW_Proforma.dbo.order_seq
		group by source
		order by source

	select document_type, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_seq
	group by document_type
	order by document_type

	select next_document_type, prev_document_type, count(*) -- 2.324.112
	from DW_Proforma.dbo.order_seq
	group by next_document_type, prev_document_type
	order by next_document_type, prev_document_type


