EXEC sp_updatestats

select *
from sysindexes
order by name

-- Tables 
SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
	--t2.TableName, 
	t2.Records
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES
		-- WHERE TABLE_SCHEMA = 'dbo'
		) t1
	FULL JOIN 
		(SELECT  t.name TableName, i.rows Records
		from sysobjects t, sysindexes i
		where t.xtype = 'U' and i.id = t.id and i.indid in (0,1)
			-- AND t.uid = 1
			) t2 ON t1.TABLE_NAME = t2.TableName
WHERE t1.TABLE_TYPE = 'BASE TABLE'
	and table_schema = 'mend' and table_name not like '%_aud%'
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, t2.TableName, t2.Records
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = 'audit') t1
	FULL JOIN 
		(SELECT  t.name TableName, i.rows Records
		from sysobjects t, sysindexes i
		where t.xtype = 'U' and i.id = t.id and i.indid in (0,1)
			AND t.uid = 5) t2 ON t1.TABLE_NAME = t2.TableName
WHERE t1.TABLE_TYPE = 'BASE TABLE'
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

-- Views
SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES) t1
WHERE t1.TABLE_TYPE = 'VIEW'
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

-- Procedures
SELECT SPECIFIC_CATALOG, SPECIFIC_SCHEMA, SPECIFIC_NAME, ROUTINE_TYPE 
FROM information_schema.routines 
WHERE routine_type = 'FUNCTION' -- PROCEDURE, FUNCTION
order by SPECIFIC_SCHEMA, SPECIFIC_NAME

-- Triggers
SELECT  s.name AS table_schema, o.name AS trigger_name, OBJECT_NAME(parent_obj) AS table_name, 
    OBJECTPROPERTY( id, 'ExecIsUpdateTrigger') AS isupdate, OBJECTPROPERTY( id, 'ExecIsDeleteTrigger') AS isdelete,
	OBJECTPROPERTY( id, 'ExecIsInsertTrigger') AS isinsert, OBJECTPROPERTY( id, 'ExecIsAfterTrigger') AS isafter,
	OBJECTPROPERTY( id, 'ExecIsInsteadOfTrigger') AS isinsteadof, OBJECTPROPERTY(id, 'ExecIsTriggerDisabled') AS [disabled] 
FROM 
	sysobjects o
INNER JOIN 
	sys.tables t ON o.parent_obj = t.object_id 
INNER JOIN 
	sys.schemas s ON t.schema_id = s.schema_id 
WHERE o.type = 'TR'
ORDER BY table_schema, trigger_name;

SELECT  s.name AS table_schema, o.name AS trigger_name, OBJECT_NAME(parent_obj) AS table_name, 
    OBJECTPROPERTY( id, 'ExecIsUpdateTrigger') AS isupdate, OBJECTPROPERTY( id, 'ExecIsDeleteTrigger') AS isdelete,
	OBJECTPROPERTY( id, 'ExecIsInsertTrigger') AS isinsert, OBJECTPROPERTY( id, 'ExecIsAfterTrigger') AS isafter,
	OBJECTPROPERTY( id, 'ExecIsInsteadOfTrigger') AS isinsteadof, OBJECTPROPERTY(id, 'ExecIsTriggerDisabled') AS [disabled] 
FROM 
	sysobjects o
INNER JOIN 
	sys.views v ON o.parent_obj = v.object_id 
INNER JOIN 
	sys.schemas s ON v.schema_id = s.schema_id 
WHERE o.type = 'TR'
ORDER BY table_schema, trigger_name;



USE TDW
GO 

SELECT OBJECT_NAME(constid) fk_name, 
	OBJECT_NAME(fkeyid) table_name, 
	OBJECT_NAME(rkeyid) ref_table_name
FROM sys.sysforeignkeys
ORDER BY table_name, ref_table_name


-- COLUMNS 

SELECT TABLE_CATALOG, TABLE_SCHEMA, 
	TABLE_NAME, COLUMN_NAME, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, ORDINAL_POSITION
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE -- COLUMN_NAME LIKE 'EMV_sync_list'
	TABLE_NAME = 'EMV_sync_list'
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION;

SELECT TABLE_CATALOG, TABLE_SCHEMA, 
	TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE COLUMN_NAME LIKE 'mk_CustomerMarket'
	--AND TABLE_SCHEMA = 'dbo'
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION;


-- COLUMNS 

SELECT --TABLE_CATALOG, TABLE_SCHEMA, 
	TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME LIKE 'fact_customer_signature_v'
	AND TABLE_SCHEMA = 'tableau'
ORDER BY ORDINAL_POSITION;


SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION, DATA_TYPE
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE DATA_TYPE = 'date' -- datetime, date
	AND COLUMN_NAME not in ('ins_ts', 'upd_ts', 'aud_dateFrom', 'aud_dateTo')
ORDER BY TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION















	
	
	


































































