
select job_id, name, description, enabled, 
	date_created, date_modified 
from msdb.dbo.sysjobs
order by name

-- Phocas Build: 2E79CD08-4C4C-4F0B-9E73-EC69D081F137
-- Phocas Build (SALES PROFORMA): 8637200A-E7AF-4DAA-BE93-67CEED1B2693

-- Feeds_AffiliateWindow: DA4E294B-8289-4414-BE56-CD21E8D738E5
-- Feeds_Affilinet: 60C783B9-85F3-4F55-A84C-D4BF1EDE1F03
-- Feeds_TradeTrackerFR: 1F18AF7E-D8B2-4559-A5BC-A3F02AFFD4FA
-- Feeds_ZanoxES: CF74672D-D7B6-4D5B-8228-D48BA3ECB59C
-- Feeds_ZanoxIT: 43D1A97C-CABA-45D7-A1B1-0352BDFAFABF
-- Feeds_ZanoxNL: E8ADBDE2-1CA1-48F9-890F-39455D0A52FE	
	
	

select instance_id, job_id, 
	run_date,
	step_id, step_name, message, run_duration
from msdb.dbo.sysjobhistory
where job_id = '2E79CD08-4C4C-4F0B-9E73-EC69D081F137'
order by run_date desc, step_id

--

select jh.instance_id, jh.job_id, j.name, j.description,
	jh.run_date,
	jh.step_id, jh.step_name, jh.message, jh.run_duration
from 
		msdb.dbo.sysjobhistory jh
	inner join
		msdb.dbo.sysjobs j on jh.job_id = j.job_id
where run_date = '20170413'
order by j.name, jh.step_id

-- 

select top 1000 *
from master.dbo.sysprocesses
order by login_time desc
