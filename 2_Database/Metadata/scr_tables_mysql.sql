
select 
  table_catalog, table_schema, table_name, table_rows
from INFORMATION_SCHEMA.TABLES
where table_schema = 'magento01'
  and table_type = 'BASE TABLE' -- BASE TABLE, VIEW
  and table_name like 'wearer%'
order by table_name

select table_schema, count(*)
from INFORMATION_SCHEMA.TABLES
group by table_schema
order by table_schema