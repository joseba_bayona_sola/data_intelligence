
SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
	charindex('_', t1.TABLE_NAME, 1), 
	substring(t1.TABLE_NAME, 0, (charindex('_', t1.TABLE_NAME, 1)))
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES) t1
WHERE t1.TABLE_TYPE = 'VIEW'
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES) t1
WHERE t1.TABLE_TYPE = 'VIEW'
	--and t1.TABLE_NAME like 'Budgets%' -- 1
	--and t1.TABLE_NAME like 'Summary_Day_%' -- 15.908 - 961 - 391
	--and t1.TABLE_NAME like 'Summary_Month%' -- 15.908
	--and t1.TABLE_NAME like 'Summary_Year%' -- 15.908
	--and t1.TABLE_NAME like 'Summary_%' and t1.TABLE_NAME not like 'Summary_Day%' and t1.TABLE_NAME not like 'Summary_Month%' and t1.TABLE_NAME not like 'Summary_Year%' -- 31.816
	and t1.TABLE_NAME like 'Transaction_Day%' -- 565
	--and t1.TABLE_NAME like 'Transaction_Month%' -- 565
	--and t1.TABLE_NAME like 'Transaction_Year%' -- 565
	--and t1.TABLE_NAME like 'Transaction_%' and t1.TABLE_NAME not like 'Transaction_Day%' and t1.TABLE_NAME not like 'Transaction_Month%' and t1.TABLE_NAME not like 'Transaction_Year%' -- 1130
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;


SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
	substring(t1.TABLE_NAME, len('Summary_Day_')+1, len(t1.TABLE_NAME) - len('Summary_Day_')) numbers 
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES) t1
WHERE t1.TABLE_TYPE = 'VIEW'
	and t1.TABLE_NAME like 'Summary_Day_%' -- 15.908 - 961 - 391
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

select *, 
	substring(numbers, 0, charindex('_', numbers, 1)) first_num, 
	substring(numbers, charindex('_', numbers, 1)+1, len(numbers) - charindex('_', numbers, 1)) rest_numbers
from 
	(SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
		substring(t1.TABLE_NAME, len('Summary_Day_')+1, len(t1.TABLE_NAME) - len('Summary_Day_')) numbers 
	from
			(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
			FROM INFORMATION_SCHEMA.TABLES) t1
	WHERE t1.TABLE_TYPE = 'VIEW'
		and t1.TABLE_NAME like 'Summary_Day_%') t -- 15.908 - 961 - 391
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME;

select *, 
	substring(rest_numbers, 0, charindex('_', rest_numbers, 1)) second_num, 
	substring(rest_numbers, charindex('_', rest_numbers, 1)+1, len(rest_numbers) - charindex('_', rest_numbers, 1)) third_num
from 
	(select *, 
		substring(numbers, 0, charindex('_', numbers, 1)) first_num, 
		substring(numbers, charindex('_', numbers, 1)+1, len(numbers) - charindex('_', numbers, 1)) rest_numbers
	from 
		(SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
			substring(t1.TABLE_NAME, len('Summary_Day_')+1, len(t1.TABLE_NAME) - len('Summary_Day_')) numbers 
		from
				(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
				FROM INFORMATION_SCHEMA.TABLES) t1
		WHERE t1.TABLE_TYPE = 'VIEW'
			and t1.TABLE_NAME like 'Summary_Day_%') t) t
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME;

	----------------------------- GOOD --------------------------------
	select first_num, 
		--second_num, 
		--third_num, 
		count(*)
	from 
		(select *, 
			substring(rest_numbers, 0, charindex('_', rest_numbers, 1)) second_num, 
			substring(rest_numbers, charindex('_', rest_numbers, 1)+1, len(rest_numbers) - charindex('_', rest_numbers, 1)) third_num
		from 
			(select *, 
				substring(numbers, 0, charindex('_', numbers, 1)) first_num, 
				substring(numbers, charindex('_', numbers, 1)+1, len(numbers) - charindex('_', numbers, 1)) rest_numbers
			from 
				(SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME, 
					substring(t1.TABLE_NAME, len('Transaction_Day_')+1, len(t1.TABLE_NAME) - len('Transaction_Day_')) numbers 
				from
						(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
						FROM INFORMATION_SCHEMA.TABLES) t1
				WHERE t1.TABLE_TYPE = 'VIEW'
					and t1.TABLE_NAME like 'Transaction_Day_%'
					) t) t) t
	group by first_num
		--, second_num
		--, third_num
	order by first_num
		--, second_num
		--, third_num;
