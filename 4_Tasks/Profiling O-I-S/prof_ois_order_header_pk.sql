
-- PK document_id -- NO (145100)
select top 1000 count(*) over() num_total, *
from 
	(select count(*) over (partition by document_id) num, *
	from DW_GetLenses.dbo.order_headers oh) t
where num > 1
	and store_name = 'visiondirect.co.uk'
order by num, document_id desc

	select store_name, count(*), min(document_date), max(document_date) 
	from 
		(select count(*) over (partition by document_id) num, *
		from DW_GetLenses.dbo.order_headers oh) t
	where num > 1
	group by store_name
	order by store_name

-- PK document_id, document_type -- YES
select top 1000 count(*) over() num_total, *
from 
	(select count(*) over (partition by document_id, document_type) num, *
	from DW_GetLenses.dbo.order_headers oh) t
where num > 1
	--and store_name = 'visiondirect.co.uk'
order by num, document_id desc, document_type


-- PK order_id -- NO (279218)
select top 1000 count(*) over() num_total, *
from 
	(select count(*) over (partition by order_id) num, *
	from DW_GetLenses.dbo.order_headers oh) t
where num > 1
order by num, order_id

-- PK order_id, document_type -- NO (2884)
select top 1000 count(*) over() num_total, *
from 
	(select count(*) over (partition by order_id, document_type) num, *
	from DW_GetLenses.dbo.order_headers oh) t
where num > 1
order by num, order_id, document_type

	select store_name, count(*), min(document_date), max(document_date) 
	from 
		(select count(*) over (partition by order_id, document_type) num, *
		from DW_GetLenses.dbo.order_headers oh) t
	where num > 1
	group by store_name
	order by store_name

-- PK order_no -- NO (279376)
select top 1000 count(*) over() num_total, *
from 
	(select count(*) over (partition by order_no) num, *
	from DW_GetLenses.dbo.order_headers oh) t
where num > 1
order by num, order_no

-------------------------------------------------------------
-- Records in Order Headers
select *
from DW_GetLenses.dbo.order_headers
where customer_id = 739032
order by order_id


-- Records in Order Lines
select *
from DW_GetLenses.dbo.order_lines
where order_id = 4516482

-------------------------------------------------------------

-- Document Id with different order_no
select top 1000 document_id, count(distinct order_no) num 
from DW_GetLenses.dbo.order_headers
group by document_id
having count(distinct order_no) > 1
order by num desc;

	select oh.*
	from 
			(select top 1000 document_id, count(distinct order_no) num 
			from DW_GetLenses.dbo.order_headers
			group by document_id
			having count(distinct order_no) > 1) t
		inner join 
			DW_GetLenses.dbo.order_headers oh on t.document_id = oh.document_id
	order by oh.document_id, oh.order_no;

	select oh.store_name, count(*), min(order_date), max(order_date)
	from 
			(select top 1000 document_id, count(distinct order_no) num 
			from DW_GetLenses.dbo.order_headers
			group by document_id
			having count(distinct order_no) > 1) t
		inner join 
			DW_GetLenses.dbo.order_headers oh on t.document_id = oh.document_id
	group by oh.store_name
	order by oh.store_name;

-- Order no with different document Id
select top 1000 order_no, count(distinct document_id) num 
from DW_GetLenses.dbo.order_headers
group by order_no
having count(distinct document_id) > 1
order by num desc;

	select oh.*
	from 
			(select top 1000 order_no, count(distinct document_id) num 
			from DW_GetLenses.dbo.order_headers
			group by order_no
			having count(distinct document_id) > 1) t
		inner join 
			DW_GetLenses.dbo.order_headers oh on t.order_no = oh.order_no
	order by t.num desc, oh.order_no, oh.document_id;
