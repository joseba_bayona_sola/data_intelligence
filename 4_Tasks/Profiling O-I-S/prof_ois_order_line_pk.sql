
-- PK document_id, document_type, line -- ???
select top 1000 count(*) over() num_total, *
from 
	(select top 100 count(*) over (partition by document_id, document_type) num, *
	from DW_GetLenses.dbo.order_lines ol) t
where num > 1
	--and store_name = 'visiondirect.co.uk'
order by num, document_id desc, document_type

-- PK document_id, document_type, line_id --> YES
select top 100 document_id, document_type, line_id, count(*) num
from DW_GetLenses.dbo.order_lines 
group by document_id, document_type, line_id
order by num desc

-- PK document_id, document_type, product_id --> NO
select top 100 document_id, document_type, product_id, count(*) num
from DW_GetLenses.dbo.order_lines 
group by document_id, document_type, product_id
order by num desc

select *
from DW_GetLenses.dbo.order_headers
where 
	(document_id = 872521) or 
	(document_id = 431108) 
order by document_id, document_type

select *
from DW_GetLenses.dbo.order_lines 
where 
	(document_id = 872521 and document_type = 'ORDER' and product_id = 1083) or 
	(document_id = 431108 and document_type = 'ORDER' and product_id = 1305) 
order by document_id, document_type, line_id

-- Line_id PK NO
select top 100 line_id, count(*) num
from DW_GetLenses.dbo.order_lines 
group by line_id
order by num desc

select *
from DW_GetLenses.dbo.order_lines 
where line_id in (1210220, 1206390, 1250403, 1199706)
order by line_id