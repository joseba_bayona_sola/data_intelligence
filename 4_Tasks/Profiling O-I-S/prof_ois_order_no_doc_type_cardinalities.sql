
-- Order with ONLY ONE document_type record per order_no
select document_type, num, count(*)
from 
	(select order_no, document_type, count(*) num, 
		count(*) over (partition by order_no) num_order_no 
	from 
		DW_GetLenses.dbo.order_headers
	group by order_no, document_type
	having count(*) = 1) t
where num_order_no = 1
group by document_type, num 
order by document_type, num  

select TOP 1000 oh.*
from 
		(select order_no, document_type, count(*) num, 
			count(*) over (partition by order_no) num_order_no 
		from 
			DW_GetLenses.dbo.order_headers
		group by order_no, document_type
		having count(*) = 1) t
	inner join
		DW_GetLenses.dbo.order_headers oh on t.order_no = oh.order_no and t.document_type = oh.document_type and t.num_order_no = 1
--where t.document_type in ('CANCEL', 'CREDITMEMO')
order by oh.order_no, oh.document_date

-- Order with diferent document_type records per order_no
select document_type, num, count(*)
from 
	(select order_no, document_type, count(*) num, 
		count(*) over (partition by order_no) num_order_no 
	from 
		DW_GetLenses.dbo.order_headers
	group by order_no, document_type
	having count(*) = 1) t
where num_order_no > 1
group by document_type, num 
order by document_type, num  

select TOP 1000 oh.*
from 
		(select order_no, document_type, count(*) num, 
			count(*) over (partition by order_no) num_order_no 
		from 
			DW_GetLenses.dbo.order_headers
		group by order_no, document_type
		having count(*) = 1) t
	inner join
		DW_GetLenses.dbo.order_headers oh on t.order_no = oh.order_no and t.document_type = oh.document_type and t.num_order_no > 1
--where t.document_type = 'ORDER' -- ORDER, CANCEL, CREDITMEMO
order by oh.order_no, oh.document_date

-- Order with diferent document_type records (AND REPEATED SOME OF THEM) per order_no
select document_type, num, count(*)
from 
	(select order_no, document_type, count(*) num 
	from 
		DW_GetLenses.dbo.order_headers
	group by order_no, document_type
	having count(*) > 1) t
group by document_type, num 
order by document_type, num  

select oh.*
from 
		(select order_no, document_type, count(*) num 
		from 
			DW_GetLenses.dbo.order_headers
		group by order_no, document_type
		having count(*) > 1) t
	inner join
		DW_GetLenses.dbo.order_headers oh on t.order_no = oh.order_no and t.document_type = oh.document_type
order by oh.order_no, oh.document_date

