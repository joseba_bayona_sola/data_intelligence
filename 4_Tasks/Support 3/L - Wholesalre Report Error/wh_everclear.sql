select oli.product_family_name, oli.packsize, SUM(oli.local_total_unit_cost * oli.qty_stock) / SUM(oli.qty_stock) weighted_avg
from Warehouse.alloc.fact_order_line_erp_issue_v oli
	right join Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
where 
	wholesale_customer_name = 'Lensway Group AB' 
	and invoice_date_c between '2019-07-01' and '2019-07-31' 
	and oli.product_family_name in ('everclear ELITE','everclear ELITE (5 pack)')
group by oli.product_family_name, oli.packsize;


select whoil.invoice_no, whoil.order_no_erp, whoil.wholesale_customer_name, 
	 oli.batchstockissueid_bk, oli.orderid_bk, oli.warehouse_name, oli.product_family_name, oli.packsize, oli.sku, oli.qty_stock, oli.local_total_unit_cost, oli.order_currency_code,
	 oli.wh_shipment_type_name, oli.receipt_number, oli.purchase_order_number
from Warehouse.alloc.fact_order_line_erp_issue_v oli
	right join Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
where 
	wholesale_customer_name = 'Lensway Group AB' 
	and invoice_date_c between '2019-07-01' and '2019-07-31' 
	and oli.product_family_name in ('everclear ELITE','everclear ELITE (5 pack)')
order by whoil.invoice_no, whoil.order_no_erp, oli.product_family_name, oli.wh_shipment_type_name, oli.receipt_number, oli.purchase_order_number, oli.sku
