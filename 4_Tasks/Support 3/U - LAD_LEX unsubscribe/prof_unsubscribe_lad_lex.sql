
drop table #lad_lex_unsubscribe

select idCustomer_sk_fk, customer_id_bk, 
	customer_email, first_name, last_name, phone_number, 
	street, city, region, postcode_s, country_code_s, 
	customer_unsubscribe_name, customer_unsubscribe_date, customer_unsubscribe_dotmailer_name, customer_unsubscribe_dotmailer_date,
	old_access_cust_no
into #lad_lex_unsubscribe
from Warehouse.gen.dim_customer_scd_v
where old_access_cust_no in ('LAD', 'LEX')

	select customer_id_bk, customer_email, customer_unsubscribe_name, customer_unsubscribe_date, old_access_cust_no
	from #lad_lex_unsubscribe
	where customer_unsubscribe_name = 'Y'
	order by customer_unsubscribe_date

	select customer_unsubscribe_name, count(*)
	from #lad_lex_unsubscribe
	group by customer_unsubscribe_name
	order by customer_unsubscribe_name

	select old_access_cust_no, customer_unsubscribe_name, count(*)
	from #lad_lex_unsubscribe
	group by old_access_cust_no, customer_unsubscribe_name
	order by old_access_cust_no, customer_unsubscribe_name

	select old_access_cust_no, customer_unsubscribe_name, customer_unsubscribe_dotmailer_name, count(*)
	from #lad_lex_unsubscribe
	group by old_access_cust_no, customer_unsubscribe_name, customer_unsubscribe_dotmailer_name
	order by old_access_cust_no, customer_unsubscribe_name, customer_unsubscribe_dotmailer_name


------------------------------------------------------------

	select t1.customer_id_bk, t1.customer_email, 
		t1.customer_unsubscribe_name, t1.customer_unsubscribe_date, t2.unsubscribe_all_date, t2.unsubscribe_all,
		t1.customer_unsubscribe_dotmailer_name,
		t1.old_access_cust_no, t3.customer_status_name
	select t1.customer_id_bk, t1.customer_email
	from 
			#lad_lex_unsubscribe t1
		inner join
			Landing.mag.customer_entity_flat_aud t2 on t1.customer_id_bk = t2.entity_id
		inner join
			Warehouse.act.dim_customer_signature_v t3 on t1.customer_id_bk = t3.customer_id
	where t1.customer_unsubscribe_name = 'Y' 
		-- and t2.unsubscribe_all = 'Yes'
		-- and t2.unsubscribe_all <> 'Yes' and t1.customer_unsubscribe_dotmailer_name = 'N' -- Set to No
		and t2.unsubscribe_all <> 'Yes' and t1.customer_unsubscribe_dotmailer_name = 'Y' -- Set to Yes
	order by t1.old_access_cust_no, unsubscribe_all_date
