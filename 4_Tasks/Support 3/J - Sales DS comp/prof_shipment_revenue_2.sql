
drop table #shipment_rev_1

select order_line_id_bk, order_id_bk, shipment_id, order_no, shipment_no, 
	order_date, invoice_date, shipment_date, website, payment_method_name,
	local_total_exc_vat, global_total_exc_vat, 
	idETLBatchRun_ins, idETLBatchRun_upd
into #shipment_rev_1
from Warehouse.sales.fact_order_line_v
where shipment_date between '2019-08-19' and '2019-08-20'

	select top 1000 *
	from #shipment_rev_1

	select website, count(distinct order_id_bk), sum(local_total_exc_vat), sum(global_total_exc_vat)
	from #shipment_rev_1
	group by website
	order by website

	select *, sum(global_total_exc_vat) over (partition by website)
	from #shipment_rev_1
	where 
		-- website = 'visiondirect.nl' and 
			(idETLBatchRun_ins = 3622 or idETLBatchRun_upd = 3622)
	order by idETLBatchRun_ins, idETLBatchRun_upd

--------------------------


	select top 1000 s.entity_id, s.increment_id, s.created_at, s.updated_at, s.ins_ts, s.upd_ts, s.created_at_dw, s.despatched_at_dw
	from 
			Landing.mag.sales_flat_shipment_aud s
		inner join
			(select distinct shipment_id
			from #shipment_rev_1
			where 
				-- website = 'visiondirect.nl' and 
					(idETLBatchRun_ins = 3622 or idETLBatchRun_upd = 3622)) sr on s.entity_id = sr.shipment_id 
	order by s.increment_id

	-- 

	select top 1000 s.id, s.shipmentNumber, s.orderIncrementID, s.status, s.createdDate, s.changedDate, s.dateReadyToShipUTC, s.dateShippedUTC, s.ins_ts, s.upd_ts
	from 
			Landing.mend.ship_customershipment_aud s
		inner join
			(select distinct shipment_no
			from #shipment_rev_1
			where 
				-- website = 'visiondirect.nl' and 
					(idETLBatchRun_ins = 3618 or idETLBatchRun_upd = 3618)) sr on s.shipmentNumber = sr.shipment_no 
	order by s.shipmentNumber


