
select order_no, convert(int, order_id) order_id, 
	convert(decimal(12, 4), local_subtotal_cost) local_subtotal_cost, convert(decimal(12, 4), global_subtotal_cost) global_subtotal_cost
from DW_GetLenses_jbs.dbo.sales_ds_cost_data

select order_no, convert(int, order_id) order_id, 
	convert(decimal(12, 4), local_subtotal_cost) local_subtotal_cost, convert(decimal(12, 4), global_subtotal_cost) global_subtotal_cost
from DW_GetLenses_jbs.dbo.sales_erp_ds_cost_data
where local_subtotal_cost <> ''

select t1.order_no, t1.order_id, 
	t1.local_subtotal_cost, t2.local_subtotal_cost, abs(t1.local_subtotal_cost - t2.local_subtotal_cost) diff_local,
	t1.global_subtotal_cost, t2.global_subtotal_cost, abs(t1.global_subtotal_cost - t2.global_subtotal_cost) diff_global
from
		(select order_no, convert(int, order_id) order_id, 
			convert(decimal(12, 4), local_subtotal_cost) local_subtotal_cost, convert(decimal(12, 4), global_subtotal_cost) global_subtotal_cost
		from DW_GetLenses_jbs.dbo.sales_ds_cost_data) t1
	left join
		(select order_no, convert(int, order_id) order_id, 
			convert(decimal(12, 4), local_subtotal_cost) local_subtotal_cost, convert(decimal(12, 4), global_subtotal_cost) global_subtotal_cost
		from DW_GetLenses_jbs.dbo.sales_erp_ds_cost_data
		where local_subtotal_cost <> '') t2 on t1.order_id = t2.order_id
-- where t2.order_id is null
-- order by t1.order_no
-- order by diff_local desc
order by diff_global desc


--------------------------------------------------
--------------------------------------------------


-- SALES DS

select order_id_bk, order_no, order_date, invoice_date, 
	website, 
	local_total_cost_discount, global_total_cost_discount, 
	order_currency_code, local_to_global_rate
from Warehouse.sales.dim_order_header_v
where order_id_bk = 9913728

select order_id_bk, idOrderLine_sk, order_line_id_bk, order_no, order_date, invoice_date, 
	website, 
	local_total_cost_discount, global_total_cost_discount, 
	order_currency_code, local_to_global_rate
from Warehouse.sales.fact_order_line_v
where order_id_bk = 9909983

select top 1000 idOrderLine_sk_fk, idOrderLineERP_sk_fk, 
	qty_unit_mag, qty_unit_erp, qty_percentage, 
	local_total_cost_mag, local_total_cost_erp, 
	local_total_cost_discount_mag, local_total_cost_discount_erp, 
	local_to_global_rate, order_currency_code
from Warehouse.alloc.fact_order_line_mag_erp
where idOrderLine_sk_fk in (22680867, 22680868)

--------------------------------------

-- SALES DS ERP

select top 1000 transaction_type, batchstockissueid_bk,
	order_id_bk, order_line_id_bk, order_no,  order_date, invoice_date, 
	website, 
	local_total_cost, global_total_cost, 
	order_currency_code, local_to_global_rate
from Warehouse.alloc.fact_order_line_erp_issue_all_v
where order_id_bk = 9911685
