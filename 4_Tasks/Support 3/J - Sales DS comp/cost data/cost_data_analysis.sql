select orderid_bk, 
	order_no_erp, order_id_bk, 

	order_date_sync, 
	country_id_shipping_bk, warehouseid_pref_bk, 

	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_to_global_rate, order_currency_code
from Landing.aux.alloc_dim_order_header_erp



--------------------

-- SOLUTION: In Issue have 3 currencies LOCAL (Warehouse), GLOBAL (GBP), ORDER (Magento Order)
	-- Agree on Date to be used for converting values

-- In Issue we are setting as LOCAL the CURRENCY from the Warehouse, GLOBAL is GBP, rate from stock register date
select oli.batchstockissueid_bk, oli.orderlinestockallocationtransactionid_bk, 
	oli.issue_id, 
	oli.orderlineid_bk, oh.orderid_bk, oh.order_no_erp,

	oli.qty_stock, 
	oli.local_total_unit_cost, oli.local_total_cost, 
	oli.local_total_unit_cost_discount, oli.local_total_cost_discount, 
	oli.local_to_global_rate, oli.order_currency_code, 
	oli.date_exchange
from 
		Landing.aux.alloc_fact_order_line_erp_issue oli
	inner join
		Landing.aux.alloc_fact_order_line_erp ol on oli.orderlineid_bk = ol.orderlineid_bk
	inner join
		Landing.aux.alloc_dim_order_header_erp oh on ol.orderid_bk = oh.orderid_bk
where oh.order_no_erp = '17000589493'

		select oh.order_no_erp, sum(oli.local_total_cost) local_total_cost, sum(oli.local_total_cost_discount) local_total_cost_discount, oli.local_to_global_rate, oli.order_currency_code 
		into #t1
		from 
				Landing.aux.alloc_fact_order_line_erp_issue oli
			inner join
				Landing.aux.alloc_fact_order_line_erp ol on oli.orderlineid_bk = ol.orderlineid_bk
			inner join
				Landing.aux.alloc_dim_order_header_erp oh on ol.orderid_bk = oh.orderid_bk
		group by oh.order_no_erp, oli.local_to_global_rate, oli.order_currency_code
		order by oh.order_no_erp

-- In OL we are setting as LOCAL the CURRENCY from the Order, GLOBAL is GBP, rate from Order
select olme.order_line_id_bk, ol.order_no,
	olme.local_total_cost_mag, olme.local_total_cost_erp, 
	olme.local_total_cost_discount_mag, olme.local_total_cost_discount_erp, 
	olme.local_to_global_rate, olme.order_currency_code
from 
		Landing.aux.alloc_fact_order_line_mag_erp olme
	inner join
		Warehouse.sales.fact_order_line ol on olme.order_line_id_bk = ol.order_line_id_bk
where ol.order_no = '17000589493'
		
		select ol.order_no, sum(olme.local_total_cost_mag) local_total_cost_mag, sum(olme.local_total_cost_discount_mag) local_total_cost_discount_mag, olme.local_to_global_rate, olme.order_currency_code
		into #t2
		from 
				Landing.aux.alloc_fact_order_line_mag_erp olme
			inner join
				Warehouse.sales.fact_order_line ol on olme.order_line_id_bk = ol.order_line_id_bk
		where olme.local_total_cost_mag is not null
		group by ol.order_no, olme.local_to_global_rate, olme.order_currency_code
		order by ol.order_no

----------------------------------------------------------------		

	select left(order_no_erp, 2), order_currency_code, count(*)
	from Landing.aux.alloc_dim_order_header_erp
	group by left(order_no_erp, 2), order_currency_code
	order by left(order_no_erp, 2), order_currency_code

	select left(order_no_erp, 2), order_currency_code, count(*)
	from #t1
	group by left(order_no_erp, 2), order_currency_code
	order by left(order_no_erp, 2), order_currency_code

	select left(order_no, 2), order_currency_code, count(*)
	from #t2
	group by left(order_no, 2), order_currency_code
	order by left(order_no, 2), order_currency_code

	--- 

select t1.order_no_erp, t1.local_total_cost, t2.local_total_cost, t1.local_total_cost_discount, t2.local_total_cost_discount, t1.order_currency_code, t2.order_currency_code
from
		(select order_no_erp, order_currency_code, sum(local_total_cost) local_total_cost, sum(local_total_cost_discount) local_total_cost_discount
		from #t1
		group by order_no_erp, order_currency_code) t1
	inner join
		(select order_no, order_currency_code, sum(local_total_cost_mag) local_total_cost, sum(local_total_cost_discount_mag) local_total_cost_discount
		from #t2
		group by order_no, order_currency_code) t2 on t1.order_no_erp = t2.order_no
where abs(t1.local_total_cost - t2.local_total_cost) > 0.1
order by t1.order_no_erp
