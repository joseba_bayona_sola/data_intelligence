
	-- 12886093
	select top 1000 count(*)
	from Warehouse.alloc.fact_order_line_erp_issue

	-- 12872477
	select top 1000 count(*)
	from Warehouse.alloc.fact_order_line_erp_issue_v

drop table DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v

-- 20 min: OK
select *
into DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
from Warehouse.alloc.fact_order_line_erp_issue_lj_v

select top 1000 *
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v

	-- 13052285
	select top 1000 count(*)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v

	-- Check 1.1: Diff in numbers because not all ADJ in Live
	select transaction_type, count(*)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	group by transaction_type
	order by transaction_type

	-- Check 1.2: qty_percentage: NULL value (182422), 0 value (623), 1.5 value (1)
	select qty_percentage, count(*)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	group by qty_percentage
	order by qty_percentage

		select top 1000 *
		from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
		-- where qty_percentage is null and idOrderLineERPIssue_sk <> -1 -- All from OH, OL ERP still not issued
		-- where qty_percentage = 0 and qty_stock <> 0 -- All from OH, OL ERP with Issue rows but not issued qty on it
		where qty_percentage > 1
		order by orderid_bk desc

	-- Check 1.3: Check sum(qty_percentage) <> 1
	select count(*) over (), order_no_erp, orderid_bk, order_date_sync, idOrderLineERP_sk, sum(qty_percentage)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	where transaction_type <> 'A'
		and order_type_erp_f = 'R'
	group by order_no_erp, orderid_bk, order_date_sync, idOrderLineERP_sk
	-- having sum(qty_percentage) <> 1 -- 177649
	having abs(1 - sum(qty_percentage)) > 0.00000100 -- 8043 / 4880 - ACTION: Look further on consequences
	order by sum(qty_percentage)

	-- Check 1.4: Cancellations

	select top 1000 cancelled_f, count(*)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	group by cancelled_f
	order by cancelled_f

	select *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	where cancelled_f = 'Y' 

	select top 1000 *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	where orderid_bk = 48695170973383819

-----------------------------------------------------------

drop table DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v

-- 1 hour: Divide by zero error encountered
select *
into DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v
from Warehouse.alloc.fact_order_line_erp_issue_all_v

	select top 1000 *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v

-----------------------------------------------------------

drop table DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2

	-- 5 min: Arithmetic overflow error converting numeric to data type numeric
	-- 9 min: no qty_percentage related attributes - qty_percentage from Warehouse.alloc.fact_order_line_erp_issue
	select 
		olit.transaction_type, olit.idOrderLineERPIssue_sk, olit.idOrderLine_sk,
	
		olitv.orderlineid_bk, case when (olit.transaction_type = 'O') then olitv.orderlineid_bk else null end orderlineid_bk_c, 
		olitv.orderid_bk, case when (olit.transaction_type = 'O') then olitv.orderid_bk else null end orderid_bk_c, 
		olitv.batchstockissueid_bk, case when (olit.transaction_type = 'O') then olitv.batchstockissueid_bk else null end batchstockissueid_bk_c, 

		-- ol.invoice_id, ol.shipment_id, ol.creditmemo_id,
	
 		olit.invoice_date, olit.shipment_date, 
		olitv.order_date_sync, olitv.promised_shipment_date, olitv.promised_delivery_date, 
		olitv.cage_shipment_date, olitv.snap_complete_date,
		olitv.allocation_date, 
		case when (olit.transaction_type = 'A') then olitv.invoice_posted_date else olitv.issue_date end issue_date,

		olitv.order_type_erp_f, olitv.order_status_erp_name, olitv.shipment_status_erp_name, olitv.allocation_status_name, olitv.allocation_strategy_name,

		olitv.manufacturer_name_I, olitv.brand_name_I, 
		olitv.product_type_name_I, olitv.category_name_I, olitv.product_family_group_name_I, 
		olitv.product_id_magento_I, olitv.product_family_name_I, 
		olitv.SKU, olitv.stock_item_description,

		olitv.warehouse_name_pref, olitv.warehouse_name,

		olitv.bom_f, olitv.inventory_level_success_f,
		olitv.pack_size_optimum_dist, olitv.pack_size_optimum_pack_size, olitv.pack_size_optimum_f,
		olitv.shortage_PO_source, olitv.shortage_PO_type, olitv.shortage_PO_number, 

		olitv.issue_id, olitv.allocation_type_name, olitv.allocation_record_type_name, olitv.cancelled_f, 
		olitv.batch_id, olitv.batch_stock_register_date,
		
		olitv.supplier_name, olitv.wh_shipment_type_name, olitv.receipt_number, 
		olitv.purchase_order_number, olitv.po_source_name, olitv.po_type_name, olitv.created_by, olitv.created_date_po, olitv.confirmed_date_po, olitv.submitted_date_po,

		olit.qty_unit qty_unit_web, olit.qty_pack qty_pack_web, 
		olitv.qty_unit qty_unit_erp, olitv.qty_stock qty_pack_erp, 

		isnull(olitv.qty_percentage, 1) qty_percentage,

		olit.qty_unit * isnull(olitv.qty_percentage, 1) qty_unit_web_m, olit.qty_pack * isnull(olitv.qty_percentage, 1) qty_pack_web_m, 
		case when (olit.transaction_type = 'A') then 0 else olitv.qty_unit end qty_unit_erp_m, -- C for refunds ??
		case when (olit.transaction_type = 'A') then 0 else olitv.qty_stock end qty_pack_erp_m, -- C for refunds ?? 
		olit.weight * isnull(olitv.qty_percentage, 1) weight, 

		olit.local_subtotal * isnull(olitv.qty_percentage, 1) local_subtotal, olit.local_shipping * isnull(olitv.qty_percentage, 1) local_shipping, 
			olit.local_discount * isnull(olitv.qty_percentage, 1) local_discount, olit.local_store_credit * isnull(olitv.qty_percentage, 1) local_store_credit, 
		olit.local_total_inc_vat * isnull(olitv.qty_percentage, 1) local_total_inc_vat, olit.local_total_exc_vat * isnull(olitv.qty_percentage, 1) local_total_exc_vat, 
			olit.local_total_vat * isnull(olitv.qty_percentage, 1) local_total_vat, olit.local_total_prof_fee * isnull(olitv.qty_percentage, 1) local_total_prof_fee, 

		olit.global_subtotal * isnull(olitv.qty_percentage, 1) global_subtotal, olit.global_shipping * isnull(olitv.qty_percentage, 1) global_shipping, 
			olit.global_discount * isnull(olitv.qty_percentage, 1) global_discount, olit.global_store_credit * isnull(olitv.qty_percentage, 1) global_store_credit, 
		olit.global_total_inc_vat * isnull(olitv.qty_percentage, 1) global_total_inc_vat, olit.global_total_exc_vat * isnull(olitv.qty_percentage, 1) global_total_exc_vat, 
			olit.global_total_vat * isnull(olitv.qty_percentage, 1) global_total_vat, olit.global_total_prof_fee * isnull(olitv.qty_percentage, 1) global_total_prof_fee, 

		olit.local_total_inc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_inc_vat_vf, olit.local_total_exc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_exc_vat_vf, 
			olit.local_total_vat_vf * isnull(olitv.qty_percentage, 1) local_total_vat_vf, olit.local_total_prof_fee_vf * isnull(olitv.qty_percentage, 1) local_total_prof_fee_vf,

		olit.local_payment_online * isnull(olitv.qty_percentage, 1) local_payment_online, olit.local_payment_offline * isnull(olitv.qty_percentage, 1) local_payment_offline, 
			olit.local_reimbursement * isnull(olitv.qty_percentage, 1) local_reimbursement,
		olit.global_payment_online * isnull(olitv.qty_percentage, 1) global_payment_online, olit.global_payment_offline * isnull(olitv.qty_percentage, 1) global_payment_offline, 
			olit.global_reimbursement * isnull(olitv.qty_percentage, 1) global_reimbursement,

		case when (olit.transaction_type = 'C') then 0 else olitv.local_total_cost_discount end local_product_cost, 
		0 local_shipping_cost, 0 local_freight_cost, 
		case when (olit.transaction_type = 'C') then 0 else olitv.local_total_cost_discount end local_total_cost, 

		case when (olit.transaction_type = 'C') then 0 else olitv.global_total_cost_discount end global_product_cost, 
		0 global_shipping_cost, 0 global_freight_cost, 
		case when (olit.transaction_type = 'C') then 0 else olitv.global_total_cost_discount end global_total_cost
	into DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	from 
			Warehouse.alloc.fact_order_line_erp_issue_all olit
		left join
			Warehouse.alloc.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk 
			-- DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk
				and case when (olit.transaction_type in ('O', 'C')) then 'O' else olit.transaction_type end = olitv.transaction_type

select top 1000 *
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2

-- select 2618630 - 2660592
select top 1000 count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2

select qty_percentage, count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
group by qty_percentage
order by qty_percentage

select order_type_erp_f, count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
group by order_type_erp_f
order by order_type_erp_f

	-- Check 2.1: Why NULL values on order_type_erp_f
	select *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	where order_type_erp_f is null -- and idOrderLineERPIssue_sk <> - 1 -- All from OH, OH ERP with still not issues
	order by invoice_date desc

select transaction_type, count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
group by transaction_type
order by transaction_type

	-- Check 2.2: Values depending on trasanction type
	select top 1000 *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	-- where transaction_type = 'O'
	-- where transaction_type = 'C'
	where transaction_type = 'A'
	order by invoice_date desc

	-- Check 2.3: Sales NULL values
	select top 1000 count(*) over (), *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	where local_subtotal is null and idOrderLine_sk <> -1 -- Need to put 0 values for I, W orders - ACTION

	-- Check 2.4: Cost NULL values
	select top 1000 count(*) over (), *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	where local_product_cost is null and idOrderLineERPIssue_sk <> -1 -- Need to put 0 values for I, W orders - ACTION

	-- Check 2.5: Cancellations
	select top 1000 cancelled_f, count(*)
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	group by cancelled_f
	order by cancelled_f

	select *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	where cancelled_f = 'Y' 

	select top 1000 *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v2
	where orderid_bk = 48695171056825315

-----------------------------------------------------------
	
	drop table DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3

	select 
		olit.transaction_type, olit.idOrderLineERPIssue_sk, olit.idOrderLine_sk,
	
		olitv.orderlineid_bk, case when (olit.transaction_type = 'O') then olitv.orderlineid_bk else null end orderlineid_bk_c, 
		olitv.orderid_bk, case when (olit.transaction_type = 'O') then olitv.orderid_bk else null end orderid_bk_c, 
		olitv.batchstockissueid_bk, case when (olit.transaction_type = 'O') then olitv.batchstockissueid_bk else null end batchstockissueid_bk_c, 

		olitv.order_type_erp_f, 

		isnull(olitv.qty_percentage, 1) qty_percentage,

		olit.qty_unit * isnull(olitv.qty_percentage, 1) qty_unit_web_m, olit.qty_pack * isnull(olitv.qty_percentage, 1) qty_pack_web_m, -- OK
		case when (olit.transaction_type = 'A') then 0 else olitv.qty_unit end qty_unit_erp_m, -- C for refunds ??
		case when (olit.transaction_type = 'A') then 0 else olitv.qty_stock end qty_pack_erp_m, -- C for refunds ?? 
		olit.weight * isnull(olitv.qty_percentage, 1) weight, -- OK

		-- isnull(olit.local_subtotal, olitv.local_subtotal) * isnull(olitv.qty_percentage, 1) local_subtotal, -- WRONG
		olit.local_subtotal * isnull(olitv.qty_percentage, 1) local_subtotal,
		olit.local_shipping * isnull(olitv.qty_percentage, 1) local_shipping, 
			olit.local_discount * isnull(olitv.qty_percentage, 1) local_discount, 
			olit.local_store_credit * isnull(olitv.qty_percentage, 1) local_store_credit, 
		-- isnull(olit.local_total_inc_vat, olitv.local_subtotal) * isnull(olitv.qty_percentage, 1) local_total_inc_vat, -- WRONG
		olit.local_total_inc_vat * isnull(olitv.qty_percentage, 1) local_total_inc_vat, 
		olit.local_total_exc_vat * isnull(olitv.qty_percentage, 1) local_total_exc_vat, 
			olit.local_total_vat * isnull(olitv.qty_percentage, 1) local_total_vat, olit.local_total_prof_fee * isnull(olitv.qty_percentage, 1) local_total_prof_fee, 

		olit.global_subtotal * isnull(olitv.qty_percentage, 1) global_subtotal, olit.global_shipping * isnull(olitv.qty_percentage, 1) global_shipping, 
			olit.global_discount * isnull(olitv.qty_percentage, 1) global_discount, olit.global_store_credit * isnull(olitv.qty_percentage, 1) global_store_credit, 
		olit.global_total_inc_vat * isnull(olitv.qty_percentage, 1) global_total_inc_vat, olit.global_total_exc_vat * isnull(olitv.qty_percentage, 1) global_total_exc_vat, 
			olit.global_total_vat * isnull(olitv.qty_percentage, 1) global_total_vat, olit.global_total_prof_fee * isnull(olitv.qty_percentage, 1) global_total_prof_fee, 

		olit.local_total_inc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_inc_vat_vf, olit.local_total_exc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_exc_vat_vf, -- OK
			olit.local_total_vat_vf * isnull(olitv.qty_percentage, 1) local_total_vat_vf, olit.local_total_prof_fee_vf * isnull(olitv.qty_percentage, 1) local_total_prof_fee_vf, -- OK

		olit.local_payment_online * isnull(olitv.qty_percentage, 1) local_payment_online, olit.local_payment_offline * isnull(olitv.qty_percentage, 1) local_payment_offline, -- OK
			olit.local_reimbursement * isnull(olitv.qty_percentage, 1) local_reimbursement, -- OK
		olit.global_payment_online * isnull(olitv.qty_percentage, 1) global_payment_online, olit.global_payment_offline * isnull(olitv.qty_percentage, 1) global_payment_offline, -- OK 
			olit.global_reimbursement * isnull(olitv.qty_percentage, 1) global_reimbursement -- OK
	into DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3
	from 
			Warehouse.alloc.fact_order_line_erp_issue_all olit
		left join
			DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk
				and case when (olit.transaction_type in ('O', 'C')) then 'O' else olit.transaction_type end = olitv.transaction_type
	-- where olitv.order_type_erp_f is null or olitv.order_type_erp_f = 'R'
	-- where olitv.order_type_erp_f = 'I'
	where olitv.order_type_erp_f = 'W'


select top 1000 *
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3
where order_type_erp_f is null

-- 13947367
select top 1000 count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3

select qty_percentage, count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3
group by qty_percentage
order by qty_percentage

select order_type_erp_f, count(*)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_all_v3
group by order_type_erp_f
order by order_type_erp_f

-----------------------------------------------------------

select transaction_type, orderlineid_bk, orderid_bk, batchstockissueid_bk, 
	order_no_erp, order_date_sync, order_status_erp_name, 
	local_subtotal, qty_percentage, 
	local_subtotal * isnull(qty_percentage, 1)
from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
where order_type_erp_f = 'W'
	-- and (local_subtotal > 100000000 or local_subtotal < 0)
order by local_subtotal

	select local_subtotal, count(*) num
	into #erp_subtotal_analysis
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	where order_type_erp_f = 'W'
	group by local_subtotal
	order by local_subtotal

	select local_subtotal, 
		-- convert(decimal(12, 4), local_subtotal), 
		convert(decimal(28, 8), local_subtotal), 
		num
	from #erp_subtotal_analysis
	where local_subtotal < 1000000000
	order by local_subtotal desc
	 
	-- WS00000547
	select *
	from DW_GetLenses_jbs.dbo.fact_order_line_erp_issue_lj_v
	where order_type_erp_f = 'W'
		and local_subtotal > 100000000
