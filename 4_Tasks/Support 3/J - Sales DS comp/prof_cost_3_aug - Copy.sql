
drop table #oh

select *
into #oh
from Warehouse.sales.dim_order_header
where order_date between '2019-08-01' and '2019-08-05'

	select order_id_bk, order_no, shipment_no, order_date, local_total_inc_vat, local_total_exc_vat, local_total_cost
	from #oh
	where local_total_cost = 0 and shipment_no is not null
	-- where local_total_cost is null and shipment_no is not null
	order by order_date


	select ol.idOrderHeader_sk_fk, ol.idOrderLine_sk, oh.order_id_bk, ol.order_line_id_bk, ol.order_no, ol.shipment_no, oh.order_date, oh.shipment_date,
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_cost
	from 
			#oh oh
		inner join
			Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
	where oh.local_total_cost = 0 and oh.shipment_no is not null
		-- and oh.idOrderHeader_sk = 10818096
	order by oh.order_date

	select ol.idOrderHeader_sk_fk, ol.idOrderLine_sk, ol2.idOrderLineERP_sk_fk, oh.order_id_bk, ol.order_line_id_bk, ol.order_no, ol.shipment_no, oh.order_date, 
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_cost, ol2.local_total_cost_mag
	from 
			#oh oh
		inner join
			Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
		left join
			Warehouse.alloc.fact_order_line_mag_erp ol2 on ol.idOrderLine_sk = ol2.idOrderLine_sk_fk
	where oh.local_total_cost = 0 and oh.shipment_no is not null
		-- and oh.idOrderHeader_sk = 10818096
	order by oh.order_date

----------------------------------------------

-- Missing Records: From 2019-07-21 20:36:42.000
-- ETL Run Time: 2019-07-23 10:25:34.580

select top 1000 *
from Warehouse.alloc.dim_order_header_erp_v
where order_id_bk = 9644765

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_v
where order_id_bk = 9644765

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where issue_id = 10695127

----------------------------------------------------------------

select top 1000 *, datediff(hour, createddate, ins_ts)
from Landing.mend.wh_batchstockissue_aud
where issueid = 10695127


	select top 100000 *, datediff(hour, createddate, ins_ts) diff_time
	from Landing.mend.wh_batchstockissue_aud
	where ins_ts > '2019-07-01'
	order by diff_time desc


select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_issue_v
where issueid = 10695127

