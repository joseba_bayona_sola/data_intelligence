
select top 10 count(*) over (), sum(ohw.local_total_exc_vat * ohw.local_to_global_rate) over (), 
	ohw.order_id_bk, ohw.order_no, ohw.shipment_date, 
	oh.idETLBatchRun_ins, oh.ins_ts, oh.idETLBatchRun_upd, oh.upd_ts
from 
	Warehouse.sales.dim_order_header_wrk ohw
inner join	
	Warehouse.sales.dim_order_header oh on ohw.order_id_bk = oh.order_id_bk
where ohw.shipment_date between '2019-08-14' and '2019-08-15' and oh.idETLBatchRun_ins <> 3598
-- where shipment_date between '2019-08-15' and '2019-08-16'
order by ohw.shipment_date desc