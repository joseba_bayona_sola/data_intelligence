
use Landing
go

-- DATA-1171

select top 1000 order_line_id_bk, order_id_bk, order_no, invoice_date, 
	product_id_bk, sku_magento, sku_erp, productid_erp_bk,
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk
from Staging.sales.fact_order_line
where productid_erp_bk is null
order by sku_magento

	drop table #ol_null_sku_erp

	select order_line_id_bk, order_id, order_no, invoice_date, 
		idProductFamily_sk_fk, sku_magento, sku_erp, idProduct_sk_fk,
		idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk
	into #ol_null_sku_erp
	from Warehouse.sales.fact_order_line
	-- where order_line_id_bk = 22153245
	where invoice_date > '2016-01-01' and idProduct_sk_fk is null and order_line_id_bk > 0

	drop table DW_Getlenses_jbs.dbo.ol_null_sku_erp

	select t1.order_line_id_bk, t1.order_id, t1.order_no, t1.invoice_date, 
		pf.product_id_magento, pf.product_family_name, t1.sku_magento, t1.sku_erp, t1.idProduct_sk_fk,
		t1.idBC_sk_fk, t1.idDI_sk_fk, t1.idPO_sk_fk, t1.idCY_sk_fk, t1.idAX_sk_fk, t1.idAD_sk_fk, t1.idDO_sk_fk, t1.idCOL_sk_fk
	into DW_Getlenses_jbs.dbo.ol_null_sku_erp 
	from 
		#ol_null_sku_erp t1
	inner join
		Warehouse.prod.dim_product_family_v pf on t1.idProductFamily_sk_fk = pf.idProductFamily_sk

		select top 1000 *
		from DW_Getlenses_jbs.dbo.ol_null_sku_erp
		-- where product_id_magento <> 2950
		where product_id_magento = 2311
		order by product_id_magento

		select top 1000 product_id_magento, product_family_name, count(*)
		from DW_Getlenses_jbs.dbo.ol_null_sku_erp
		where product_id_magento <> 2950
		group by product_id_magento, product_family_name
		order by product_id_magento, product_family_name

		select t1.product_id_magento, t1.product_family_name, t1.sku_magento, t2.product_code, 
			t2.bc, t2.di, t2.po, t2.cy, t2.ax, t2.ad, t2.do, t2.co,
			t1.num
		from
				(select product_id_magento, product_family_name, sku_magento, count(*) num
				from DW_Getlenses_jbs.dbo.ol_null_sku_erp
				where product_id_magento <> 2950
				group by product_id_magento, product_family_name, sku_magento) t1
			left join
				Landing.aux.mag_edi_stock_item t2 on t1.sku_magento = t2.product_code
		-- order by product_id_magento, product_family_name, sku_magento
		order by num desc, product_id_magento, product_family_name, sku_magento

		select *
		from Warehouse.prod.dim_param_col


select top 1000 count(*) over (), *
from Landing.aux.mag_edi_stock_item
where (product_id = 2311 and co = 'BLACK') or (product_id = 2338 and co = 'GEMSTONE GREEN')

select top 1000 *
from Landing.aux.prod_dim_product_v
-- where product_id_bk = 2311
where product_id_bk = 2338
order by colour_bk

select top 1000 *
from Warehouse.prod.dim_product_family

select top 1000 count(*) over (), *
from Landing.aux.mag_edi_stock_item

select *
from dbo.fnSplitString('1DAM90-BC8.5-DI14.2-PO-6.50', '-')

select *
from dbo.fnSplitString('BT1D30A-AX150-BC8.4-CY-0.75-DI14.5-PO-5.75', '-')

select *
from dbo.fnSplitString('FRIL02-BC8.6-DI13.8-PO-0.50', '-')
