use DW_GetLenses_jbs
go

--table storing the parsed parameter values from the edi product code
--drop table DW_GetLenses_jbs.dbo.mag_edi_stock_item
--go

--create table DW_GetLenses_jbs.dbo.mag_edi_stock_item(
--	product_id				int NOT NULL,
--	product_code			varchar(277) NOT NULL,
--	BC						char(3),
--	DI						char(4),
--	PO						char(6),
--	CY						char(5),
--	AX						char(3),
--	AD						char(4),
--	DO						char(1),
--	CO						varchar(100));
	
--go

-- alter table DW_GetLenses_jbs.dbo.mag_edi_stock_item add constraint [PK_aux_mag_edi_stock_item]
--	primary key clustered (product_id, product_code);
--go

drop procedure dbo.sp_checksku
go

--IN: product_id + product_code (sku with parameter-value pairs)
create procedure dbo.sp_checksku @in_pid int, @in_sku nvarchar(200)
as
begin

	declare @sku nvarchar(200) = SUBSTRING(@in_sku, charindex('-',  @in_sku, 1), len(@in_sku))

	--table with the list of parameters to parse
	declare @params table (p nvarchar(2))
	insert into @params values ('BC'),('DI'),('PO'),('CY'),('DO'),('AD'),('AX'),('CO')

	declare @p nvarchar(2)
	declare @p_value nvarchar(100)

	declare @BC_value nvarchar(3)
	declare @DI_value nvarchar(4)
	declare @PO_value nvarchar(6)
	declare @CY_value nvarchar(5)
	declare @DO_value nvarchar(1)
	declare @AD_value nvarchar(4)
	declare @AX_value nvarchar(3)
	declare @CO_value nvarchar(100)

	declare @sql nvarchar(500)

	declare param_cur cursor for 
		select p from @params

	open param_cur

		fetch next from param_cur into @p

		while @@FETCH_STATUS = 0 
		begin
			
			--position of the parameter on the sku string
			--COLLATE for case sensitive charindex
			declare @position int = charindex(@p, @sku COLLATE SQL_Latin1_General_CP1_CS_AS, 0)
			
			--substring of the sku from the parameter
			declare @sku_subtr nvarchar(200) = substring(@sku, @position + 2 , len(@sku))

			--limit of the substring that is the value of the parameter
			declare @param_limit int = charindex('-', substring(@sku, @position + 2 , len(@sku)), 2) - 1
		
			if @position = 0 --the sku doesnt have the parameter
			begin
				if @p = 'BC' set @BC_value = 0
				if @p = 'DI' set @DI_value = 0
				if @p = 'PO' set @PO_value = 0
				if @p = 'CY' set @CY_value = 0
				if @p = 'DO' set @DO_value = 0
				if @p = 'AD' set @AD_value = 0
				if @p = 'CO' set @CO_value = 0
				if @p = 'AX' set @AX_value = 0
			end	
			else 
				begin
					if @param_limit = '-1' --the parameter is at the end
					begin
						if @p = 'BC' set @BC_value = @sku_subtr
						if @p = 'DI' set @DI_value = @sku_subtr
						if @p = 'PO' set @PO_value = @sku_subtr
						if @p = 'CY' set @CY_value = @sku_subtr
						if @p = 'DO' set @DO_value = @sku_subtr
						if @p = 'AD' set @AD_value = @sku_subtr
						if @p = 'AX' set @AX_value = @sku_subtr
						if @p = 'CO' set @CO_value = @sku_subtr
					end
					else
					begin
						if @p = 'BC' set @BC_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'DI' set @DI_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'PO' set @PO_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'CY' set @CY_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'DO' set @DO_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'AD' set @AD_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'AX' set @AX_value = substring(@sku, @position + 2 , @param_limit)
						if @p = 'CO' set @CO_value = substring(@sku, @position + 2 , @param_limit)
					end
				end 

			fetch next from param_cur into @p
		end

		--dyn sql handling data type conversion, string quotes
		set @sql = 'insert into DW_GetLenses_jbs.dbo.mag_edi_stock_item (product_id, product_code, BC, DI, PO, CY, DO, AD, AX, CO) values 
				(' + convert(nvarchar(50), @in_pid) + ' ,' + char(39) + @in_sku + char(39) + ', ' + @BC_value + ', ' + @DI_value + ', ' + @PO_value + ', ' + @CY_value + ', ' 
						  + char(39) + @DO_value + char(39) + ', ' + char(39) + case when @AD_value = 'Medi' then 'MED' else @AD_value end + char(39) 
						  + ', ' + @AX_value + ', ' + char(39) + @CO_value + char(39) + ')'

		exec (@sql)

	close param_cur
	deallocate param_cur
end

-----------------------------
--cursor for main table, using the id and code to identify rows and process them

begin

	set nocount on
	
	declare @p_id int, @p_code varchar(277)

	--empty table before the sp runs
	truncate table DW_GetLenses_jbs.dbo.mag_edi_stock_item 

	declare main_cur cursor for 
		select product_id, product_code from Landing.aux.mag_edi_stock_item

	open main_cur

	fetch next from main_cur into @p_id, @p_code;

	while @@FETCH_STATUS = 0 
	begin
		
		execute dbo.sp_checksku @p_id, @p_code

		fetch next from main_cur into @p_id, @p_code;
	end

	close main_cur
	deallocate main_cur

end

------------------------

--checks

--19734 diffs
drop table #temp_diff_sku

select * into #temp_diff_sku from (
select product_id, product_code,
	BC,
	DI,
	PO,
	CY,
	AX,
	AD,
	DO,
	CO
from Landing.aux.mag_edi_stock_item 
except
select product_id, product_code, 
	convert(decimal(10,2), case when BC LIKE '0' then null else BC end) BC,
	convert(decimal(10,2), case when DI LIKE '0' then null else DI end) DI,
	convert(decimal(10,2), case when PO LIKE '0' then null else PO end) PO,
	convert(decimal(10,2), case when CY LIKE '0' then null else CY end) CY,
	convert(decimal(10,2), case when AX LIKE '0' then null else AX end) AX,
	case when AD LIKE '0' then null else AD end AD,
	case when DO LIKE '0' then null else DO end DO,
	case when CO LIKE '0' then null else CO end CO
from DW_GetLenses_jbs.dbo.mag_edi_stock_item) v

drop table DW_GetLenses_jbs.dbo.final_skus_diff_mag_edi

select * into DW_GetLenses_jbs.dbo.final_skus_diff_mag_edi from (
select product_id, product_code,
	BC,
	DI,
	PO,
	CY,
	AX,
	AD,
	DO,
	CO,
	'EDI' flag
from Landing.aux.mag_edi_stock_item where product_code in (select product_code from #temp_diff_sku)
union
select product_id, product_code,
	convert(decimal(10,2), BC) BC,
	convert(decimal(10,2), DI) DI,
	convert(decimal(10,2), PO) PO,
	convert(decimal(10,2), CY) CY,
	convert(decimal(10,2), AX) AX,
	case when AD LIKE '0' then null else AD end AD,
	case when DO LIKE '0' then null else DO end DO,
	case when CO LIKE '0' then null else CO end CO,
	'TEST' flag
from DW_GetLenses_jbs.dbo.mag_edi_stock_item where product_code in (select product_code from #temp_diff_sku)
) v

select * from DW_GetLenses_jbs.dbo.final_skus_diff_mag_edi 
where product_id not in (1182,1284,2243,2244,2245,2246,2247)
order by product_id, product_code, flag

--checking for products with no parameters
select * from
(select 
	product_id, 
	product_code, 
	count(1) over (partition by product_id, product_code order by product_id, product_code) rep
 from DW_GetLenses_jbs.dbo.final_skus_diff_mag_edi ) v
where rep <> 2
