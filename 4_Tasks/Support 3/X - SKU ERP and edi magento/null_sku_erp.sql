

select top 1000 order_line_id_bk, order_id, order_no, invoice_date, 
	product_id_bk, sku_magento,
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye
	
from Staging.sales.fact_order_line
where productid_erp_bk is null
order by product_id_bk, sku_magento