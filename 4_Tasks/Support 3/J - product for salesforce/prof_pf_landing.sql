use DW_GetLenses_jbs
go


	create table dbo.catalog_product_entity_flat_aud(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		type_id						varchar(32) NOT NULL, 
		sku							varchar(64),
		required_options			int NOT NULL,
		has_options					int NOT NULL, 
		created_at					datetime, 
		updated_at					datetime, 
		manufacturer				int, 
		name						varchar(255),
		url_path					varchar(255),
		url_key						varchar(255),
		status						int, 
		product_type				varchar(1000),
		product_lifecycle			varchar(1000),
		promotional_product			int, 
		daysperlens					varchar(255),
		tax_class_id				int, 
		visibility					int, 
		is_lens						int, 
		stocked_lens				int, 
		telesales_only				int, 
		condition					int, 
		equivalence					varchar(255),
		equivalent_sku				varchar(255),
		price_comp_price			varchar(255),
		glasses_colour				int,
		google_shopping_gtin 		varchar(255),	
		brand						int,	
		packsize					varchar(255),
		rrp_price					DECIMAL(26,2),
		clens_extended_wear			INT,
		lens_family					INT,
		packtext					varchar(255),
		short_description			varchar(1000),
		meta_description			varchar(255),
		meta_keyword				varchar(1000),
		meta_title					varchar(255),
		product_lifecycle_text		varchar(1000),
		product_lifecycle_title		varchar(1000),
		unavailable_for_country		varchar(255));
	go 


	alter table dbo.catalog_product_entity_flat_aud add constraint [PK_dbo_catalog_product_entity_flat_aud]
		primary key clustered (entity_id, store_id);
	go

----------------------------------------------------------

	create table #catalog_product_entity_flat_varchar(
			entity_id					int NOT NULL,
			store_id					int NOT NULL,
			name						varchar(255),
			url_path					varchar(255),
			url_key						varchar(255),
			daysperlens					varchar(255),
			equivalence					varchar(255),
			equivalent_sku				varchar(255),
			price_comp_price			varchar(255), 
			google_shopping_gtin		varchar(255), 
			packsize					varchar(255),
			packtext					varchar(255),
			meta_description			varchar(255),
			meta_title					varchar(255),
			unavailable_for_country		varchar(255));


	create table #catalog_product_entity_flat_int(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,
			manufacturer				int, 		
			status						int, 	
			promotional_product			int, 
			tax_class_id				int, 
			visibility					int, 
			is_lens						int, 
			stocked_lens				int, 
			telesales_only				int, 
			condition					int, 
			glasses_colour				int, 
			brand						int,
			clens_extended_wear			INT,
			lens_family					INT);

	create table #catalog_product_entity_flat_decimal(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,
			rrp_price					DECIMAL(26,2));


	create table #catalog_product_entity_flat_text(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,
			product_type				varchar(1000),
			product_lifecycle			varchar(1000),
			short_description			varchar(1000),
			meta_keyword				varchar(1000),
			product_lifecycle_text		varchar(1000),
			product_lifecycle_title		varchar(1000));

	insert into #catalog_product_entity_flat_varchar(entity_id, store_id, name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price, google_shopping_gtin, packsize,packtext,meta_description,meta_title,unavailable_for_country)

		select entity_id, store_id, name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price, google_shopping_gtin, packsize,packtext,meta_description,meta_title,unavailable_for_country
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'varchar') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_varchar) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price, google_shopping_gtin, packsize,packtext,meta_description,meta_title,unavailable_for_country)) pvt

	insert into  #catalog_product_entity_flat_int(entity_id, store_id, 
		manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, 
		stocked_lens, telesales_only, condition, glasses_colour, brand,
		clens_extended_wear,lens_family)

		select entity_id, store_id, 
			manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, stocked_lens, 
			telesales_only, condition, glasses_colour, brand,
			clens_extended_wear,lens_family
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'int') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_int) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, stocked_lens, 
					telesales_only, condition, glasses_colour, brand,
					clens_extended_wear,lens_family)) pvt

	insert into  #catalog_product_entity_flat_decimal(entity_id, store_id, 
		rrp_price)

		select entity_id, store_id, 
			rrp_price
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'decimal') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_decimal) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (rrp_price)) pvt


	insert into #catalog_product_entity_flat_text(entity_id, store_id, product_type, product_lifecycle,short_description,meta_keyword,product_lifecycle_text,product_lifecycle_title)

		select entity_id, store_id, product_type, product_lifecycle,short_description,meta_keyword,product_lifecycle_text,product_lifecycle_title
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'text') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_text) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (product_type, product_lifecycle,short_description,meta_keyword,product_lifecycle_text,product_lifecycle_title)) pvt

	insert into dbo.catalog_product_entity_flat_aud (entity_id, store_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			manufacturer, 
			name, url_path, url_key, 
			status, product_type, product_lifecycle, 
			promotional_product, daysperlens, 
			tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
			equivalence, equivalent_sku, price_comp_price, glasses_colour, 
			google_shopping_gtin, brand, packsize, 
			packtext, meta_description, meta_title, unavailable_for_country,
			rrp_price, clens_extended_wear, lens_family,
			short_description, meta_keyword, product_lifecycle_text, product_lifecycle_title)

		select p.entity_id, pvit.store_id, 
			p.entity_type_id, p.attribute_set_id, 
			p.type_id, p.sku, p.required_options, p.has_options, 
			p.created_at, p.updated_at, 
			pvit.manufacturer, 
			pvit.name, pvit.url_path, pvit.url_key, 
			pvit.status, pvit.product_type, pvit.product_lifecycle, 
			pvit.promotional_product, pvit.daysperlens, 
			pvit.tax_class_id, pvit.visibility, pvit.is_lens, pvit.stocked_lens, pvit.telesales_only, pvit.condition, 
			pvit.equivalence, pvit.equivalent_sku, pvit.price_comp_price, pvit.glasses_colour, 
			pvit.google_shopping_gtin, pvit.brand, pvit.packsize, 
			pvit.packtext,pvit.meta_description,pvit.meta_title,pvit.unavailable_for_country,
			pvit.rrp_price,pvit.clens_extended_wear,pvit.lens_family,
			pvit.short_description,pvit.meta_keyword,pvit.product_lifecycle_text,pvit.product_lifecycle_title
		from 
				Landing.mag.catalog_product_entity p
			left join
				(select
					case when (pvi.entity_id is null) then pt.entity_id else pvi.entity_id end entity_id, 
					case when (pvi.store_id is null) then pt.store_id else pvi.store_id end store_id, 
					pvi.name, pvi.url_path, pvi.url_key, pvi.daysperlens, pvi.equivalence, pvi.equivalent_sku, pvi.price_comp_price,
					pvi.manufacturer, pvi.status, pvi.promotional_product, pvi.tax_class_id, pvi.visibility, pvi.is_lens, pvi.stocked_lens, pvi.telesales_only, pvi.condition, pvi.glasses_colour,
					pvi.google_shopping_gtin, pvi.brand, pvi.packsize,
					pvi.packtext,pvi.meta_description,pvi.meta_title,pvi.unavailable_for_country,
					pt.rrp_price,pvi.clens_extended_wear,pvi.lens_family,
					pt.product_type, pt.product_lifecycle,pt.short_description,pt.meta_keyword,pt.product_lifecycle_text,pt.product_lifecycle_title
				from
						(select 
							case when (pv.entity_id is null) then pi.entity_id else pv.entity_id end entity_id, 
							case when (pv.store_id is null) then pi.store_id else pv.store_id end store_id, 
							pv.name, pv.url_path, pv.url_key, pv.daysperlens, pv.equivalence, pv.equivalent_sku, pv.price_comp_price,
							pi.manufacturer, pi.status, pi.promotional_product, pi.tax_class_id, pi.visibility, pi.is_lens, pi.stocked_lens, pi.telesales_only, pi.condition, pi.glasses_colour, 
							pv.google_shopping_gtin, pi.brand, pv.packsize,pv.packtext,pv.meta_description,pv.meta_title,pv.unavailable_for_country,
							pi.clens_extended_wear,pi.lens_family
						from 
								#catalog_product_entity_flat_varchar pv
							full join
								#catalog_product_entity_flat_int pi on pv.entity_id = pi.entity_id and pv.store_id = pi.store_id) pvi
					full join
						(select
							case when (pd.entity_id is null) then pt.entity_id else pd.entity_id end entity_id, 
							case when (pd.store_id is null) then pt.store_id else pd.store_id end store_id, 
							pd.rrp_price, 
							pt.product_type, pt.product_lifecycle,pt.short_description,pt.meta_keyword,pt.product_lifecycle_text,pt.product_lifecycle_title
						from 
								#catalog_product_entity_flat_decimal pd
							full join
								#catalog_product_entity_flat_text pt on pd.entity_id = pt.entity_id and pd.store_id = pt.store_id) pt 
									on pvi.entity_id = pt.entity_id and pvi.store_id = pt.store_id) pvit on p.entity_id = pvit.entity_id

----------------------------------------------------------------------------
----------------------------------------------------------------------------

select *
from Landing.mag.catalog_product_entity_flat_aud

select *
from dbo.catalog_product_entity_flat_aud

-- packtext, meta_description, meta_title, unavailable_for_country,
-- rrp_price, clens_extended_wear, lens_family,
-- short_description, meta_keyword, product_lifecycle_text, product_lifecycle_title

---

select pef.entity_id, s.store_name, 
	pef.brand, pf.brand_name, pef.manufacturer, pf.manufacturer_name, pef.name, pef.short_description, pef.sku, 
	pef.status, pef.visibility,
	pef.created_at, pef.updated_at, 
	pef.meta_title, pef.meta_description, pef.meta_keyword, pef.url_key,
	pef.product_lifecycle, pef.product_lifecycle_text, pef.product_lifecycle_title, 
	pef.packsize, pef.packtext, 
	pef.daysperlens, pef.clens_extended_wear, pef.lens_family, 
	pef.rrp_price, pef.google_shopping_gtin,
	pef.unavailable_for_country

from 
		dbo.catalog_product_entity_flat_aud pef
	inner join -- left
		Warehouse.prod.dim_product_family_v pf on pef.entity_id = pf.product_id_magento
	inner join
		(select 0 store_id, 'default' store_name
		union
		select store_id, store_name
		from Landing.aux.mag_gen_store_v
		where store_name like 'visiondirect%'
			and acquired = 'N'
			and tld in ('.co.uk', '.ie', '.nl', '.es', '.it', '.fr', '.be')) s on pef.store_id = s.store_id
-- where pf.product_id_bk is null -- 289 records (glasses, bundle)
order by pef.entity_id, pef.store_id

-- 

create view dbo.product_family_v as
	select pef.entity_id, s.store_name, 
		coalesce(pef.brand, pef0.brand) brand, pf.brand_name,
		coalesce(pef.manufacturer, pef0.manufacturer) manufacturer, pf.manufacturer_name,
		coalesce(pef.name, pef0.name) name, coalesce(pef.short_description, pef0.short_description) short_description, coalesce(pef.sku, pef0.sku) sku, 
		coalesce(pef.status, pef0.status) status, coalesce(pef.visibility, pef0.visibility) visibility, 
		coalesce(pef.created_at, pef0.created_at) created_at, coalesce(pef.updated_at, pef0.updated_at) updated_at, 
		coalesce(pef.meta_title, pef0.meta_title) meta_title, coalesce(pef.meta_description, pef0.meta_description) meta_description, coalesce(pef.meta_keyword, pef0.meta_keyword) meta_keyword, coalesce(pef.url_key, pef0.url_key) url_key, 
		coalesce(pef.product_lifecycle, pef0.product_lifecycle) product_lifecycle, coalesce(pef.product_lifecycle_text, pef0.product_lifecycle_text) product_lifecycle_text, coalesce(pef.product_lifecycle_title, pef0.product_lifecycle_title) product_lifecycle_title, 
		coalesce(pef.packsize, pef0.packsize) packsize, coalesce(pef.packtext, pef0.packtext) packtext, 
		coalesce(pef.daysperlens, pef0.daysperlens) daysperlens, coalesce(pef.clens_extended_wear, pef0.clens_extended_wear) clens_extended_wear, coalesce(pef.lens_family, pef0.lens_family) lens_family, 
		coalesce(pef.rrp_price, pef0.rrp_price) rrp_price, coalesce(pef.google_shopping_gtin, pef0.google_shopping_gtin) google_shopping_gtin, 
		coalesce(pef.unavailable_for_country, pef0.unavailable_for_country) unavailable_for_country	

	from 
			dbo.catalog_product_entity_flat_aud pef
		inner join -- left
			Warehouse.prod.dim_product_family_v pf on pef.entity_id = pf.product_id_magento
		inner join
			(select store_id, store_name
			from Landing.aux.mag_gen_store_v
			where store_name like 'visiondirect%'
				and acquired = 'N'
				and tld in ('.co.uk', '.ie', '.nl', '.es', '.it', '.fr', '.be')) s on pef.store_id = s.store_id
		inner join
			(select pef.entity_id, s.store_name, 
				pef.brand, pef.manufacturer, pef.name, pef.short_description, pef.sku, 
				pef.status, pef.visibility,
				pef.created_at, pef.updated_at, 
				pef.meta_title, pef.meta_description, pef.meta_keyword, pef.url_key,
				pef.product_lifecycle, pef.product_lifecycle_text, pef.product_lifecycle_title, 
				pef.packsize, pef.packtext, 
				pef.daysperlens, pef.clens_extended_wear, pef.lens_family, 
				pef.rrp_price, pef.google_shopping_gtin,
				pef.unavailable_for_country

			from 
					dbo.catalog_product_entity_flat_aud pef
				inner join
					(select 0 store_id, 'default' store_name) s on pef.store_id = s.store_id) pef0 on pef.entity_id = pef0.entity_id
go


select distinct entity_id
from DW_GetLenses_jbs.dbo.product_family_v
order by entity_id
