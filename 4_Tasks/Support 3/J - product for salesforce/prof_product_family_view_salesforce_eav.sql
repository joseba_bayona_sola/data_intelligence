
--entity_id - Y
--brand - Y
--manufacturer - Y
--sku - Y
--name - Y
	--rrp_price - N
--url_key - Y
--visibility - Y
--google_shopping_gtin - Y
--status - Y
--created_at - Y
--updated_at - Y
--product_lifecycle - Y
	--substitution_type - N
--daysperlens - Y
	--clens_extended_wear - N
	--lens_family - N
--packsize - Y
	--packtext - N
	--short_description - N
	--meta_description - N
	--meta_keyword - N
	--meta_title - N
	--product_lifecycle_text - N
	--product_lifecycle_title - N
	--unavailable_for_country - N

select entity_id, store_id, 
	entity_type_id, attribute_set_id, 
	type_id, sku, required_options, has_options, 
	created_at, updated_at, 
	manufacturer, 
	name, url_path, url_key, 
	status, product_type, product_lifecycle, 
	promotional_product, daysperlens, 
	tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, 
	equivalence, equivalent_sku, price_comp_price, glasses_colour,
	google_shopping_gtin,
	brand, packsize,
	idETLBatchRun, ins_ts
from Landing.mag.catalog_product_entity_flat

select entity_id, 
	brand, manufacturer, 
	sku, name, url_key, visibility, google_shopping_gtin, status, 
	created_at, updated_at, 
	product_lifecycle, 
	daysperlens, 
	packsize

	--rrp_price, product_lifecycle_text, product_lifecycle_title, substitution_type, clens_extended_wear, lens_family, 
	--packtext, 
	--short_description, meta_description, meta_keyword, meta_title, 
	--unavailable_for_country
from Landing.mag.catalog_product_entity_flat


	select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		left join
			Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
	where ent.entity_type_code = 'catalog_product' 
	-- order by atr.attribute_code
		and atr.attribute_code in (
			'rrp_price', 'product_lifecycle_text', 'product_lifecycle_title', 'substitution_type', 'clens_extended_wear', 'lens_family', 
			'packtext', 
			'short_description', 'meta_description', 'meta_keyword', 'meta_title', 
			'unavailable_for_country')

