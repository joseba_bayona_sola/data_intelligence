
select top 1000 category_id, product_id, store_id, 
	position, is_parent, visibility
from Landing.mag.catalog_category_product_index

	select count(*)
	from Landing.mag.catalog_category_product_index

	select store_id, count(*)
	from Landing.mag.catalog_category_product_index
	group by store_id
	order by store_id

	select product_id, count(*)
	from Landing.mag.catalog_category_product_index
	group by product_id
	order by product_id

	select product_id, store_id, count(*)
	from Landing.mag.catalog_category_product_index
	group by product_id, store_id
	order by product_id, store_id

select entity_id, store_id, parent_id, name
from Landing.mag.catalog_category_entity_flat
where store_id = 0
order by entity_id, store_id

select *
from Landing.sf_mig.product_family_v

select ccpi.category_id, ccpi.product_id, ccpi.store_id, 
	coalesce(c.name, c0.name) category_name, 
	ccpi.is_parent, ccpi.position, ccpi.visibility
from 
		Landing.mag.catalog_category_product_index ccpi
	left join
		Landing.mag.catalog_category_entity_flat c on ccpi.category_id = c.entity_id and ccpi.store_id = c.store_id
	inner join
		(select entity_id, store_id, parent_id, name from Landing.mag.catalog_category_entity_flat where store_id = 0) c0 on ccpi.category_id = c0.entity_id
where ccpi.product_id = 3194
	-- and c.entity_id is null
order by ccpi.category_id, ccpi.product_id, ccpi.store_id

select *
from Landing.mag.catalog_category_product
where product_id = 3194

-------------------------------------------------------

select pf.entity_id, pf.store_id, pf.store_name, pf.name, 
	c.category_id, c.category_name, c.is_parent, c.position, c.visibility
from 
		Landing.sf_mig.product_family_v pf
	left join
		(select ccpi.category_id, ccpi.product_id, ccpi.store_id, 
			coalesce(c.name, c0.name) category_name, 
			ccpi.is_parent, ccpi.position, ccpi.visibility
		from 
				Landing.mag.catalog_category_product_index ccpi
			left join
				Landing.mag.catalog_category_entity_flat c on ccpi.category_id = c.entity_id and ccpi.store_id = c.store_id
			inner join
				(select entity_id, store_id, parent_id, name from Landing.mag.catalog_category_entity_flat where store_id = 0) c0 on ccpi.category_id = c0.entity_id) c 
					on pf.entity_id = c.product_id and pf.store_id = c.store_id
-- where c.category_id is null
-- where pf.entity_id = 1083 and pf.store_id = 20
order by pf.entity_id, pf.store_id, c.category_id
