
		select ol.order_line_id_bk, olmv.orderlineid orderlineid_bk, olmv.orderlinemagentoid orderlinemagentoid_bk -- isnull(t.orderlineid_bk, 0)
		into #alloc_order_line_mag_erp_olm
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_orderlinemagento_v olmv on o.orderid_bk = olmv.orderid
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olmv.magentoItemID = ol.order_line_id_bk
		where olmv.magentoItemID <> 6485561
		
		select count(*) over (partition by order_line_id_bk) num_rep, *
		from #alloc_order_line_mag_erp_olm
		order by num_rep desc, order_line_id_bk

		-- olm Magento
		select ol.*
		from
				(select distinct order_line_id_bk
				from
					(select count(*) over (partition by order_line_id_bk) num_rep, *
					from #alloc_order_line_mag_erp_olm) olm
				where olm.num_rep > 1) olm
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
		order by order_no, order_line_id_bk

		-- olm ERP
		select olm2.*
		from
				(select *
				from
					(select count(*) over (partition by order_line_id_bk) num_rep, *
					from #alloc_order_line_mag_erp_olm) olm
				where olm.num_rep > 1) olm1
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm2 on olm1.orderlinemagentoid_bk = olm2.orderlinemagentoid
		order by olm2.incrementid, olm2.magentoItemID

		select distinct olm2.orderid, olm2.magentoOrderID_int, olm2.incrementID, olm2.orderStatus, olm2.createdDate
		from
				(select *
				from
					(select count(*) over (partition by order_line_id_bk) num_rep, *
					from #alloc_order_line_mag_erp_olm) olm
				where olm.num_rep > 1) olm1
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm2 on olm1.orderlinemagentoid_bk = olm2.orderlinemagentoid
		order by olm2.incrementid

		-- olm Issue
		select olm2.orderid, oli.orderlineid_bk, olm2.magentoOrderID_int, olm2.magentoItemID, olm2.incrementID, olm2.orderStatus, 
			oli.issue_id, oli.issue_date, oli.qty_stock, oli.local_total_unit_cost
		from
				(select *
				from
					(select count(*) over (partition by order_line_id_bk) num_rep, *
					from #alloc_order_line_mag_erp_olm) olm
				where olm.num_rep > 1) olm1
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm2 on olm1.orderlinemagentoid_bk = olm2.orderlinemagentoid
			inner join		
				Landing.aux.alloc_fact_order_line_erp_issue oli on olm1.orderlineid_bk = oli.orderlineid_bk
		order by olm2.incrementid, olm2.magentoItemID, olm2.orderid

		select top 1000 incrementID, count(*)
		from Landing.mend.gen_order_order_v
		group by incrementID
		having count(*) > 1
		order by incrementID

-------------------------------------------------------------
-------------------------------------------------------------

		select dense_rank() over (partition by ol.order_line_id_bk order by olmv.orderlineid) ord_rep,
			ol.order_line_id_bk, olmv.orderlineid orderlineid_bk, olmv.orderlinemagentoid orderlinemagentoid_bk -- isnull(t.orderlineid_bk, 0)
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_orderlinemagento_v olmv on o.orderid_bk = olmv.orderid
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olmv.magentoItemID = ol.order_line_id_bk
		where olmv.magentoItemID <> 6485561
			and o.orderid_bk in (48695171042418968, 48695171042418969)

			select olm.order_line_id_bk, ol.orderlineid_bk, olmv.orderid,
				case when (ol.num_olm > 1) then 'Y' else 'N' end deduplicate_f, 
				olmv.quantityOrdered, olmv.quantityAllocated 
			from 
					Landing.aux.alloc_order_line_mag_erp_olm olm
				inner join
					(select orderlineid_bk, count(*) num_olm
					from Landing.aux.alloc_order_line_mag_erp_olm
					group by orderlineid_bk) ol on olm.orderlineid_bk = ol.orderlineid_bk
				inner join
					Landing.mend.gen_order_orderlinemagento_v olmv on olm.orderlinemagentoid_bk = olmv.orderlinemagentoid
			where olmv.orderid in (48695171042418968, 48695171042418969)

-------------------------------------------------------------
-------------------------------------------------------------

	insert into Landing.aux.alloc_order_line_mag_erp_olm (order_line_id_bk, orderlineid_bk, orderlinemagentoid_bk)

		select order_line_id_bk, orderlineid_bk, orderlinemagentoid_bk
		from
			(select dense_rank() over (partition by ol.order_line_id_bk order by olmv.orderlineid) ord_rep,
				ol.order_line_id_bk, olmv.orderlineid orderlineid_bk, olmv.orderlinemagentoid orderlinemagentoid_bk -- isnull(t.orderlineid_bk, 0)
			from 
					Landing.aux.alloc_order_header_erp_o o
				inner join
					Landing.mend.gen_order_orderlinemagento_v olmv on o.orderid_bk = olmv.orderid
				inner join
					Landing.aux.sales_fact_order_line_aud ol on olmv.magentoItemID = ol.order_line_id_bk
			where olmv.magentoItemID <> 6485561) olm
		where ord_rep = 1


	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.alloc_order_line_mag_erp_dates (order_line_id_bk, 
		idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
		idCalendarExpectedShipmentDate, expected_shipment_date, 
		idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate)

		select olm.order_line_id_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate
		from 
				Landing.aux.alloc_order_line_mag_erp_olm olm
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp2.year) slolm on olm.orderlinemagentoid_bk = slolm.orderlinemagentoid 