
select top 1000 *
from 
	Staging.rec.dim_receipt r
left join
	Warehouse.rec.dim_wh_shipment s on r.shipment_id_bk = s.shipment_id_bk
where s.idWHShipment_sk is null

---------------------------

	select whs.id receiptid, w.warehouseid, s.supplier_id, -- wsib_whs.warehousestockitembatchid,
		whs.shipmentID shipment_ID, whs.receiptID receipt_ID, whs.receiptNumber, whs.orderNumber, whs.invoiceRef, whs.supplieradviceref,
		w.code, w.name warehouse_name, s.supplierID, s.supplierName, s.currencyCode,
		whs.status, whs.shipmentType, 
		whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
		whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
		whs.lock,
		whs.auditComment,
		whs.createdDate
	from 
			Landing.mend.whship_receipt_aud whs
		inner join
			Landing.mend.whship_receipt_warehouse_aud whsw on whs.id = whsw.receiptid
		inner join
			Landing.mend.gen_wh_warehouse_v w on whsw.warehouseid = w.warehouseid
		inner join
			Landing.mend.whship_receipt_supplier_aud whss on whs.id = whss.receiptid
		inner join
			Landing.mend.gen_supp_supplier_v s on whss.supplierid = s.supplier_id
	where whs.id in (20829148276773823, 20829148276773825, 20829148276773821)


	select shipment_ID shipment_id_bk, 
		warehouseid warehouseid_bk, supplier_id supplier_id_bk, shipmentType wh_shipment_type_bk, 
		case when (whs.num_rep > 1) 
			then whs.shipment_ID + '-'  + convert(varchar, whs.ord_rep) 
			else whs.shipment_ID
		end shipment_number
	from
		(select shipment_ID, warehouseid, supplier_id, shipmentType, 
			count(*) over (partition by shipment_ID) num_rep, 
			rank() over (partition by shipment_ID order by warehouseid, supplier_id) ord_rep
		from
			(select shipment_ID, warehouseid, supplier_id, max(shipmentType) shipmentType
			from 
				(select whs.id receiptid, w.warehouseid, s.supplier_id, -- wsib_whs.warehousestockitembatchid,
					whs.shipmentID shipment_ID, whs.receiptID receipt_ID, whs.receiptNumber, whs.orderNumber, whs.invoiceRef, whs.supplieradviceref,
					w.code, w.name warehouse_name, s.supplierID, s.supplierName, s.currencyCode,
					whs.status, whs.shipmentType, 
					whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
					whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
					whs.lock,
					whs.auditComment,
					whs.createdDate
				from 
						Landing.mend.whship_receipt_aud whs
					inner join
						Landing.mend.whship_receipt_warehouse_aud whsw on whs.id = whsw.receiptid
					inner join
						Landing.mend.gen_wh_warehouse_v w on whsw.warehouseid = w.warehouseid
					inner join
						Landing.mend.whship_receipt_supplier_aud whss on whs.id = whss.receiptid
					inner join
						Landing.mend.gen_supp_supplier_v s on whss.supplierid = s.supplier_id
				where whs.id in (20829148276773823, 20829148276773825, 20829148276773821)) t
			where shipment_ID is not null and warehouseid is not null and supplier_id is not null
			group by shipment_ID, warehouseid, supplier_id) whs) whs
