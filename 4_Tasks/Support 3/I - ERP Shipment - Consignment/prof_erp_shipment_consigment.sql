﻿
select id, shipmentID, shipmentNumber, orderIncrementID, status, createddate, labelRenderer
from customershipments$customershipment
limit 1000

	select count(*)
	from customershipments$customershipment

	select status, count(*)
	from customershipments$customershipment	
	group by status
	order by status

-- 

select id, order_number, identifier, warehouse_id, consignment_number
from customershipments$consignment
limit 1000	

	select count(*)
	from customershipments$consignment

select customershipments$consignmentid, customershipments$customershipmentid
from customershipments$scurriconsignment_shipment
limit 1000	

	select count(*)
	from customershipments$scurriconsignment_shipment

-- 

select s.id, s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.status, s.createddate, s.labelRenderer,
	co.id, co.order_number, co.identifier, co.warehouse_id, co.consignment_number
from 
		customershipments$customershipment s
	left join	
		customershipments$scurriconsignment_shipment cos on s.id = cos.customershipments$customershipmentid
	left join
		customershipments$consignment co on cos.customershipments$consignmentid = co.id	
where s.status = 'Complete' and s.labelRenderer = 'Scurri'
	and co.id is null		
order by s.id desc
limit 1000

	select extract(year from s.createddate), extract(month from s.createddate), count(*)
	from 
			customershipments$customershipment s
		left join	
			customershipments$scurriconsignment_shipment cos on s.id = cos.customershipments$customershipmentid
		left join
			customershipments$consignment co on cos.customershipments$consignmentid = co.id	
	where s.status = 'Complete' and s.labelRenderer = 'Scurri'
		and co.id is null			
	group by extract(year from s.createddate), extract(month from s.createddate)
	order by extract(year from s.createddate), extract(month from s.createddate)
			