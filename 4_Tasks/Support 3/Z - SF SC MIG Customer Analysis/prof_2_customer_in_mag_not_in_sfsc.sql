

		-- View with all constraints: duplicates + in customer_entity_all + is_active + gdpr + email format
		select -- top 1000 count(*) over (), 
			c.entity_id, c.email, c.created_at, c.updated_at
		into #magento_customer
		from 
				Landing.mag.customer_entity_flat_aud c
			inner join
				Landing.mag.customer_entity_all c_all on c.entity_id = c_all.entity_id
			left join
				Landing.aux.gdpr_customer_list gdpr on c.entity_id = gdpr.customer_id
		where c.is_active = 1 and gdpr.customer_id is null
			and c.email not like '%@@%'
			-- and c.email LIKE '%^[\+A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$%' 

	-- 2.1: Customers in Landing: Magento not in SF SC Customer (Accounting for different constraints)
	select count(*) over (), c_mag.entity_id, c_mag.email, c_mag.created_at, c_mag.updated_at
	from 
			#magento_customer c_mag
		left join
			Landing.sf_sc.dm_customer_aud c_sfsc on c_mag.entity_id = convert(int, c_sfsc.SFCC_Customer_Id__pc)
	where c_sfsc.Id is null
		-- and c_mag.email NOT LIKE '%@%.%' 
		and c_mag.email like '%_@__%.__%' -- AND patindex('%[^a-z,0-9,@,.,_,\-]%', c_mag.email) = 0
	order by c_mag.created_at, c_mag.entity_id

	-- 2.2: Customers in SF MIG views not in (2) (entity_sfcc_exported is null) 

	select entity_id, email, created_at, updated_at
	into #sf_mig_customer
	from Landing.sf_mig.customer_v c_mig
	where entity_sfcc_exported is null
		and c_mig.email not like '%@@%'
		and c_mig.email like '%_@__%.__%' 
	order by created_at

	-- 2.3: Check between them

	select c1.entity_id, c2.entity_id, c1.email, c2.email, c1.created_at, c2.created_at, c1.updated_at, c2.updated_at, 
		cd.entity_id
	from
			(select c_mag.entity_id, c_mag.email, c_mag.created_at, c_mag.updated_at
			from 
					#magento_customer c_mag
				left join
					Landing.sf_sc.dm_customer_aud c_sfsc on c_mag.entity_id = convert(int, c_sfsc.SFCC_Customer_Id__pc)
			where c_sfsc.Id is null
				and c_mag.email like '%_@__%.__%') c1
		full join
			#sf_mig_customer c2 on c1.entity_id = c2.entity_id
		left join
			Landing.mag.customer_entity_duplicates cd on isnull(c1.entity_id, c2.entity_id) = cd.entity_id
	-- where c1.entity_id is null or c2.entity_id is null
	order by isnull(c1.created_at, c2.created_at), isnull(c1.entity_id, c2.entity_id)
