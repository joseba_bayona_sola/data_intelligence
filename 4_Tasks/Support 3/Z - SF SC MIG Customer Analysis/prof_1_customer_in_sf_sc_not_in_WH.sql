
-- 1: Customers in SF SC Customer not updated in Warehouse: DIM

	select top 1000 count(*) over (), 
		c_sfsc.Id, c_sfsc.PersonContactId, c_sfsc.SFCC_Customer_Id__pc, c_sfsc.PersonEmail, c_sfsc.ins_ts,
		c_wh.idCustomer_sk, c_wh.customer_id_bk, c_wh.customer_email, c_wh.created_at, c_wh.updated_at
	from 
			Landing.sf_sc.dm_customer_aud c_sfsc
		inner join
			Warehouse.gen.dim_customer c_wh on convert(int, c_sfsc.SFCC_Customer_Id__pc) = c_wh.customer_id_bk 
	where c_wh.Id is null

	select top 1000 count(*) over (), 
		c_sfsc.Id, c_sfsc.PersonContactId, c_sfsc.SFCC_Customer_Id__pc, c_sfsc.PersonEmail, c_sfsc.ins_ts,
		c_wh.idCustomer_sk, c_wh.customer_id_bk, c_wh.customer_email, c_wh.created_at, c_wh.updated_at
	from 
			Landing.sf_sc.dm_customer_aud c_sfsc
		left join
			Warehouse.gen.dim_customer c_wh on convert(int, c_sfsc.SFCC_Customer_Id__pc) = c_wh.customer_id_bk 
	where c_wh.customer_id_bk is null
	order by c_sfsc.ins_ts

		select c_wh.idCustomer_sk, c_wh.customer_id_bk, c_wh.customer_email, c_wh.created_at, c_wh.updated_at, 
			c_wh.Id, c_wh.PersonContactId, c_wh.EON_Opt_in__c_Email
		from Warehouse.gen.dim_customer c_wh
		where customer_email in ('claire.omalley@kerry.com', 'fredgal23@yahoo.fr', 'johndelphin61@gmail.com', 'babla123445@gmail.com')
		order by customer_email