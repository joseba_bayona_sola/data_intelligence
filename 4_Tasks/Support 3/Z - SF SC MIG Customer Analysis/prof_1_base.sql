
-- Source of Data

	-- (1.1) Landing: Magento
	select top 1000 count(*) over (), entity_id, email, created_at, updated_at
	from Landing.mag.customer_entity_flat_aud

	-- (1.2) Warehouse: DIM
	select top 1000 count(*) over (), idCustomer_sk, customer_id_bk, customer_email, created_at, updated_at, 
		Id, PersonContactId, EON_Opt_in__c_Email
	from Warehouse.gen.dim_customer

	-- (2) Landing: SF SC Customer
	select top 1000 count(*) over (), Id, PersonContactId, SFCC_Customer_Id__pc, PersonEmail
	from Landing.sf_sc.dm_customer_aud

	-- (3) Landing: SF SC Preferences
	select top 1000 EON_Preference_Record_Id__c, EON_Account__Record_Id__c, EON_Method__c, EON_Frequency__c, EON_Opt_in__c, EON_Legacy_Preference_Id__c, 
		CreatedById, CreatedDate
	from Landing.sf_sc.dm_preferences_aud

	-- (4) Landing: Dotmailer Unsub Customers
	select top 1000 email, store_id, timestamp, ins_ts
	from Landing.mag.dotmailer_suppressed_contact

-- Checks to be done: 

	-- 1: Customers in (2) not updated in (1.2)

	-- 2.1: Customers in (1.1) not in (2) (Accounting for different constraints)
	-- 2.2: Customers in SF MIG views not in (2) (entity_sfcc_exported is null) 
		-- View with all constraints: duplicates + in customer_entity_all + is_active + gdpr + email format

	-- 3. Customers in (3) not updated in (1.2)

	-- 4. Customers in (4) not in (3) (Don't have all information on (3))

-- Tasks to be done: 

	-- 1: Planning and Viability of changing structure on mag_rt tables

	-- 2: Email: Situate Problem + Solution and Advantages + Actions

	-- 3: Development of new views
	
	
	update trg
	set
		trg.EON_Account__Record_Id__c = src.Id, trg.PersonContactId = src.PersonContactId, 
		trg.upd_ts = getutcdate()
	from
			Landing.mag.customer_entity_duplicates trg
		inner join
			Landing.sf_sc.dm_customer_aud src on trg.entity_id = convert(int, src.SFCC_Customer_Id__pc)
	where isnull(trg.EON_Account__Record_Id__c, '') <> src.Id or isnull(trg.PersonContactId, '') <> src.PersonContactId
	