
drop table #lad_lex_karen

select idCustomer_sk_fk, customer_id_bk, 
	customer_email, first_name, last_name, phone_number, 
	street, city, region, postcode_s, country_code_s, 
	customer_unsubscribe_name, customer_unsubscribe_dotmailer_name, 
	old_access_cust_no
into #lad_lex_karen
from Warehouse.gen.dim_customer_scd_v
where old_access_cust_no in ('LAD', 'LEX')
	and postcode_s like '28%' and country_code_s = 'ES'

select t1.*, t2.WEBSITE_UNSUBSCRIBE,
	t2.NUM_OF_ORDERS, t2.LENS_SKU1, t2.LENS_SKU2, t2.LENS_LAST_ORDER_DATE
select customer_id_bk customer_id, customer_email, t2.SEGMENT_GEOG
from 
		#lad_lex_karen t1
	inner join
		Warehouse.dm.EMV_sync_list t2 on t1.customer_id_bk = t2.CLIENT_ID
	inner join
		Landing.mag.customer_entity_flat_aud c on t1.customer_id_bk = c.entity_id
where t2.WEBSITE_UNSUBSCRIBE = 1
	and t2.LENS_SKU1 is not null
	and t2.LENS_LAST_ORDER_DATE > '2019-01-01'
order by t2.SEGMENT_GEOG, t2.LENS_LAST_ORDER_DATE desc


------------------------------------------
------------------------------------------

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where customer_id = 1899147

select top 1000 *
from Warehouse.dm.fact_customer_signature_dm
where client_id = 3013703

select top 1000 *
from Warehouse.dm.EMV_sync_list
where client_id = 3013703

select count(*)
from #lad_lex_karen

select customer_unsubscribe_name, count(*)
from #lad_lex_karen
group by customer_unsubscribe_name
order by customer_unsubscribe_name

