
select top 1000 *
from Landing.aux.mag_catalog_product_price_aud
-- where -- store_id = 23
	-- and 
where	price_type = 'Tier Pricing'
	and store_id > 20
	and product_lifecycle <> 'discontinued'
order by store_id, product_id, qty, price_type


select top 1000 *
from Landing.aux.mag_catalog_product_price
where store_id = 23
order by store_id, product_id, qty, price_type

select *
from Landing.mag.catalog_product_entity_tier_price
where website_id = 23 and entity_id in (1083, 1128)
order by entity_id, qty, value

select *
from Landing.mag.catalog_product_entity_tier_price_aud
where website_id = 23 and entity_id in (1083, 1128)
order by entity_id, qty, value

---------

select top 1000 *
from Landing.aux.mag_catalog_product_price
where store_id = 23 and product_id in (1083, 1128)
order by qty, value

select top 1000 *
from Landing.aux.mag_catalog_product_price_aud
where store_id = 23 and product_id in (1083, 1128)
order by qty, value

---------

		select store_id, store_name, product_id, name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, 
			(max_pack_price - pack_price) * 100 / case when (max_pack_price = 0) then 1 else max_pack_price end disc_percent, max_pack_price,
			isnull(promo_key, '') promo_key, 

			case when (promo_key <> '') then 'PLA' else
				case when (num_packs_tier_price > 1) then 'Tier Pricing' else 'Regular' end end price_type, 
			
			case when (len(left(promo_key, 3)) = 0) then 'REG' else left(promo_key, 3) end pla_type
		from
			(select store_id, store_name, product_id, name, product_lifecycle, 
				value, qty, min_qty_prod, 
				qty / min_qty_prod num_packs_tier_price, 
				total_price, total_price / (qty / min_qty_prod) pack_price, 
				promo_key, 
				max(total_price / (qty / min_qty_prod)) over (partition by product_id, store_id) max_pack_price
			from
				(select tp.website_id store_id, s.name store_name, tp.entity_id product_id, p.name, p.product_lifecycle, 
					tp.value, tp.qty, 
					min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
					tp.value * tp.qty total_price,
					tp.promo_key
				from 
						Landing.mag.catalog_product_entity_tier_price_aud tp
					inner join
						(select entity_id, sku, name, product_lifecycle
						from Landing.mag.catalog_product_entity_flat_aud 
						where store_id = 0) p on tp.entity_id = p.entity_id 
					inner join
						Landing.mag.core_store_aud s on tp.website_id = s.store_id
				where tp.customer_group_id = 0 and tp.all_groups = 1
					and tp.website_id = 23 and tp.entity_id = 1083
				) t) t
		where store_id not in (0, 29) 
		order by product_id, store_id, qty, promo_key

---------
---------
---------

select top 1000 *
from Landing.aux.mag_catalog_product_price
where store_id > 20
	and price_type = 'Tier Pricing'
	and product_lifecycle <> 'discontinued'
order by store_id, product_id, value

select top 1000 t1.*
from 
		Landing.aux.mag_catalog_product_price t1
	inner join
		(select distinct store_id, product_id
		from Landing.aux.mag_catalog_product_price
		where store_id > 20
			and price_type = 'Tier Pricing'
			and product_lifecycle <> 'discontinued') t2 on t1.store_id = t2.store_id and t1.product_id = t2.product_id
order by t1.store_id, t1.product_id, price_type, value

