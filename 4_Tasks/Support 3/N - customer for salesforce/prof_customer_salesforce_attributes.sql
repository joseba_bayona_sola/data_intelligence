
select top 1000 *
from Landing.mag.customer_entity_flat

select top 1000 entity_id, created_in, created_at, email, store_id, 
	prefix, firstname, lastname, dob,
	unsubscribe_all, 
	default_billing, default_shipping, cus_phone 
	-- check_prescription, tc_accepted_date, shipping_address, billing_address
from Landing.mag.customer_entity_flat

	select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		left join
			Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
	where ent.entity_type_code = 'customer' 
	-- order by atr.attribute_code
		and atr.attribute_code in (
			'check_prescription', 'tc_accepted_date', 'shipping_address', 'billing_address')

