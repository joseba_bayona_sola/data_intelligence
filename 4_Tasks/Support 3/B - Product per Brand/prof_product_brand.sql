
select top 1000 *
from Landing.map.mag_eav_attributes
where entity_type_code = 'catalog_product'

select attribute_id, entity_type_id, attribute_code, backend_type
from Landing.mag.eav_attribute
where entity_type_id = 4 and attribute_code like 'brand%'

select entity_type_id, entity_type_code
from Landing.mag.eav_entity_type_aud 

select *
from Landing.mag.catalog_product_entity_flat

select top 1000 *
from Landing.mag.eav_attribute_option_value
where option_id = 230
order by option_id

---------------------------------------------------------

		select entity_id, store_id, brand
		from 
				(select atr.entity_type_id, atr.entity_type_code, atr.attribute_id, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
						where ent.entity_type_code = 'catalog_product' and atr.backend_type = 'int' and atr.attribute_code = 'brand' and atr.attribute_id = 849) atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_int) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (brand)) pvt
		order by brand, entity_id