

select top 1000 *
from Warehouse.sales.dim_order_header_v

select oh.*
into #temp_oh_promo_29
from 
	warehouse.sales.dim_order_header_v oh
inner join 
	(select c.customer_id_bk
	from 
		warehouse.gen.dim_customer_v c
	inner join 
		DW_GetLenses_jbs.dbo.uk_eye_drop_promo_daily_29 t on c.customer_email = t.Email) c on oh.customer_id = c.customer_id_bk
where oh.order_date >= '2019-03-29';

select coupon_code, order_date_c, count(1) 
from #temp_oh_promo_29 
group by coupon_code, order_date_c
order by coupon_code, order_date_c

select *
from #temp_oh_promo_29 
where coupon_code is null
order by order_date

select ol.*
from 
		#temp_oh_promo_29 oh
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk
where oh.coupon_code = 'FREEEVERCLEAREYE2019'
	and ol.local_discount <> 0
order by oh.order_id_bk, ol.order_line_id_bk

select ol.*
from 
		#temp_oh_promo_29 oh
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk
where oh.coupon_code is null
	-- and ol.product_id_magento = 2328
order by oh.order_id_bk, ol.order_line_id_bk



select *
from #temp_oh_promo_29 
where coupon_code = 'FREEEVERCLEAREYE2019'
order by order_date



