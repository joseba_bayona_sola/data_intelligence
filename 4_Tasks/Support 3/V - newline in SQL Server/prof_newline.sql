

USE [master]
GO




/****** Object:  LinkedServer [MAGENTO]    Script Date: 07/09/2018 09:08:28 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'MAGENTO_UNI', @srvproduct=N'MySQL_replUni', @provider=N'MSDASQL', @datasrc=N'MySQL_replUni'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'MAGENTO_UNI',@useself=N'False',@locallogin=NULL,@rmtuser=N'root',@rmtpassword='M4g3nt0123'

GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'rpc', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'rpc out', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_UNI', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


		select value_id, 
			entity_type_id, attribute_id, entity_id, value, value2, dbo.GetColumnValue(value2, '|||', 1) as Street1, dbo.GetColumnValue(value2, '|', 2) as Street2, dbo.GetColumnValue(value2, '|', 3) as Street3, 
				
				'test
				test', 
				dbo.GetColumnValue('test
				test', char(13), 1) as test1, 
				dbo.GetColumnValue('test
				test', ' ', 1) as test1_2, 

				'123 Cheam Road

URGENT - contains prescription optical products - please make every effort to deliver this package. Please leave in the porch if we are out.',
				dbo.GetColumnValue('123 Cheam Road

URGENT - contains prescription optical products - please make every effort to deliver this package. Please leave in the porch if we are out.', char(13), 1) as test2, 
				dbo.GetColumnValue('123 Cheam Road

URGENT - contains prescription optical products - please make every effort to deliver this package. Please leave in the porch if we are out.', ' ', 1) as test2_2

			-- ,CONVERT(VARBINARY(MAX), value)

			from openquery(MAGENTO_UNI, 
			'select cei.value_id, cei.entity_type_id, cei.attribute_id, cei.entity_id, cei.value, replace(value, ''\n'', ''�'') value2 
			from magento01.customer_address_entity_text cei 
			where cei.entity_id = 73981')


---------------------------------------------

select cast(s.string AS sysname)
from dbo.fnSplitString('aa,,aa,,aa', ',,') s

select cast(s.string AS sysname)
from dbo.fnSplitString('aa|aa|aa', '|') s

select cast(s.string AS sysname)
from dbo.fnSplitString('123 Cheam Road||URGENT - contains prescription optical products - please make every effort to deliver this package. Please leave in the porch if we are out.', '|') s

select dense_rank() over (), cast(s.string AS sysname)
from dbo.fnSplitString2('17 Mill Grove, Cheadle|*;|*;If Out Leave In Porch', '|*;') s

select cast(s.string AS sysname)
from dbo.fnSplitString2('26b Burland Road', '|*;') s

		select top 1000 entity_id, value, dbo.GetColumnValue(value, '|*;', 1) street1, dbo.GetColumnValue(value, '|*;', 2) street2, dbo.GetColumnValue(value, '|*;', 3) deliverynote 
		from Landing.mag_rt.customer_address_entity_text2
		where entity_id = 138620