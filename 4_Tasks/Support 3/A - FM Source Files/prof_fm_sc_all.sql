

		select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, year + month + day fm_date_str, qty_packs qty_packs_gr
		from DW_GetLenses_jbs.dbo.fm_gross_requirements

		select year + month + day fm_date_str, count(*)
		from DW_GetLenses_jbs.dbo.fm_gross_requirements
		group by year + month + day
		order by fm_date_str

		select convert(date, '2018110', 120) fm_date

--------------------------------------------------------------

drop table #fm_gr_sf
drop table #fm_o_soh
drop table #fm_sh_ss
drop table #fm_gr_sf_o_soh

-- gross_requirements - sales_forecast
select  
	case when (gr.sku is not null) then gr.sku else sf.sku end sku, 
	case when (gr.warehouse_name is not null) then gr.warehouse_name else sf.warehouse_name end warehouse_name, 
	case when (gr.fm_date is not null) then gr.fm_date else sf.fm_date end fm_date, 
	gr.qty_packs_gr, sf.qty_packs_sf
into #fm_gr_sf
from
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_gr
		from DW_GetLenses_jbs.dbo.fm_gross_requirements) gr
	full join
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_sf
		from DW_GetLenses_jbs.dbo.fm_sales_forecast) sf on gr.warehouse_name = sf.warehouse_name and gr.sku = sf.sku and gr.fm_date = sf.fm_date

-- orders - stock_on_hand
select 
	case when (o.sku is not null) then o.sku else soh.sku end sku, 
	case when (o.warehouse_name is not null) then o.warehouse_name else soh.warehouse_name end warehouse_name, 
	case when (o.fm_date is not null) then o.fm_date else soh.fm_date end fm_date, 
	o.qty_packs_o, soh.qty_packs_soh
into #fm_o_soh
from
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_o
		from DW_GetLenses_jbs.dbo.fm_orders) o
	full join
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_soh
		from DW_GetLenses_jbs.dbo.fm_stock_on_hand) soh on o.warehouse_name = soh.warehouse_name and o.sku = soh.sku and o.fm_date = soh.fm_date

-- shortages - safety_stock
select 
	case when (sh.sku is not null) then sh.sku else ss.sku end sku, 
	case when (sh.warehouse_name is not null) then sh.warehouse_name else ss.warehouse_name end warehouse_name, 
	case when (sh.fm_date is not null) then sh.fm_date else ss.fm_date end fm_date, 
	sh.qty_packs_sh, ss.qty_packs_ss
into #fm_sh_ss
from
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_sh
		from DW_GetLenses_jbs.dbo.fm_shortages) sh
	full join
		(select sku, 
			case 
				when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
				when (warehouse = 'GIR_RETAIL') then 'Girona'
				when (warehouse = 'YORK_RETAIL') then 'York'
			end warehouse_name, 
			year, month, day, convert(date, year + '-' + month + '-' + day, 120) fm_date, qty_packs qty_packs_ss
		from DW_GetLenses_jbs.dbo.fm_sales_safety_stock) ss on sh.warehouse_name = ss.warehouse_name and sh.sku = ss.sku and sh.fm_date = ss.fm_date
		

-------------------------------------------------------------------------
-------------------------------------------------------------------------

select top 1000 *, count(*) over ()
from #fm_gr_sf

select top 1000 *, count(*) over ()
from #fm_o_soh

select top 1000 *, count(*) over ()
from #fm_sh_ss


select 
	case when (gr_sf.sku is not null) then gr_sf.sku else o_soh.sku end sku, 
	case when (gr_sf.warehouse_name is not null) then gr_sf.warehouse_name else o_soh.warehouse_name end warehouse_name, 
	case when (gr_sf.fm_date is not null) then gr_sf.fm_date else o_soh.fm_date end fm_date, 
	gr_sf.qty_packs_gr, gr_sf.qty_packs_sf, o_soh.qty_packs_o, o_soh.qty_packs_soh
into #fm_gr_sf_o_soh
from
		#fm_gr_sf gr_sf
	full join
		#fm_o_soh o_soh on gr_sf.warehouse_name = o_soh.warehouse_name and gr_sf.sku = o_soh.sku and gr_sf.fm_date = o_soh.fm_date


select 
	case when (gr_sf_o_soh.sku is not null) then gr_sf_o_soh.sku else sh_ss.sku end sku, 
	case when (gr_sf_o_soh.warehouse_name is not null) then gr_sf_o_soh.warehouse_name else sh_ss.warehouse_name end warehouse_name, 
	case when (gr_sf_o_soh.fm_date is not null) then gr_sf_o_soh.fm_date else sh_ss.fm_date end fm_date, 
	gr_sf_o_soh.qty_packs_gr, gr_sf_o_soh.qty_packs_sf, gr_sf_o_soh.qty_packs_o, gr_sf_o_soh.qty_packs_soh, sh_ss.qty_packs_sh, sh_ss.qty_packs_ss
into DW_GetLenses_jbs.dbo.fm_all_2
from
		#fm_gr_sf_o_soh gr_sf_o_soh
	full join
		#fm_sh_ss sh_ss on gr_sf_o_soh.warehouse_name = sh_ss.warehouse_name and gr_sf_o_soh.sku = sh_ss.sku and gr_sf_o_soh.fm_date = sh_ss.fm_date


-------------------------------------------------------------------------
-------------------------------------------------------------------------

select *
from DW_GetLenses_jbs.dbo.fm_all
where sku in ('01083B2D4CF0000000000301', 'W00040000000000000000011')
order by sku, warehouse_name, fm_date

select *
from DW_GetLenses_jbs.dbo.fm_all
where sku in ('02317B3D4CZ0000000000301') and warehouse_name = 'Amsterdam'
order by sku, warehouse_name, fm_date


select top 1000 *
from DW_GetLenses_jbs.dbo.fm_all
where qty_packs_gr is not null and qty_packs_sf is not null and qty_packs_o is not null and qty_packs_soh is not null and qty_packs_sh is not null and qty_packs_ss is not null
order by sku, warehouse_name, fm_date

------------------------------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.fm_all_2
where qty_packs_gr is not null and qty_packs_sf is not null and qty_packs_o is not null and qty_packs_soh is not null and qty_packs_sh is not null and qty_packs_ss is not null
order by sku, warehouse_name, fm_date

select *
from DW_GetLenses_jbs.dbo.fm_all_2
where sku like '01084%'
order by sku, warehouse_name, fm_date

	select top 1000 warehouse_name, count(*)
	from DW_GetLenses_jbs.dbo.fm_all_2
	group by warehouse_name
	order by warehouse_name

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.fm_all_2
	group by sku
	order by sku

	select fm_date, count(*)
	from DW_GetLenses_jbs.dbo.fm_all_2
	group by fm_date
	order by fm_date
