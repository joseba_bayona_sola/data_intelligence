
use Landing
go

select top 1000 *
from fm.fm_sales_forecast


select top 1000 warehouse, count(*)
from fm.fm_sales_forecast
group by warehouse
order by warehouse

select top 1000 year, month, day, count(*)
from fm.fm_sales_forecast
group by year, month, day
order by year, month, day

select sku, count(*)
from fm.fm_sales_forecast
group by sku
order by sku

----------------------------------

select top 1000 year, month, day, 
	convert(int, convert(varchar(8), convert(date, convert(varchar(4), year) + '/' + convert(varchar(2), month) + '/' + convert(varchar(2), day), 111), 112)) fm_date,
	count(*)
from fm.fm_stock_on_hand
group by year, month, day
order by year, month, day

select qty_packs, len(qty_packs),
	-- convert(decimal(28, 8), qty_packs),
	count(*)
-- from fm.fm_sales_forecast -- 31082:0 // 545 MB --> NEED
from fm.fm_gross_requirements -- 25558708:0 // 1.9G --> NEED ??
-- from fm.fm_orders -- 71:0  // 28 MB --> NEED
-- from fm.fm_stock_on_hand -- 5 - 214858 - 7420499:0 // 2.7G --> NEED
-- from fm.fm_shortages -- 5 - 228067 - 48994945:0 // 2.7G --> formula
-- from fm.fm_safety_stock -- 5234543:0 // 2.7G --> NEED
-- from fm.fm_min_stock -- 114293:0 // 2.5G --> NEED
-- from fm.fm_max_stock -- :0
-- from fm.fm_suppressions -- 105974 - 4102 - 49389433:0 // 2.7G  --> formula
group by qty_packs
order by qty_packs

-------------------------------------------------------------

select top 1000 warehouse, count(*)
from fm.fm_stock_on_hand
group by warehouse
order by warehouse

select top 1000 year, month, day, count(*)
from fm.fm_stock_on_hand
group by year, month, day
order by year, month, day

select sku, count(*)
from fm.fm_stock_on_hand
group by sku
order by sku

select qty_packs, len(qty_packs),
	convert(decimal(28, 8), qty_packs),
	count(*)
from fm.fm_stock_on_hand
where qty_packs <> ''
group by qty_packs
order by len(qty_packs) desc

select top 1000 *
from fm.fm_stock_on_hand
where qty_packs = ''

select top 1000 SKU, warehouse, year, month, day,
	qty_packs,
	idETLBatchRun, ins_ts
from fm.fm_gross_requirements
where sku = '01084B2D4AS0000000000301'	and warehouse = 'YORK_RETAIL'
order by year, month, day

select top 1000 SKU, warehouse, year, month, day,
	qty_packs,
	idETLBatchRun, ins_ts
from fm.fm_stock_on_hand
where sku = '01084B2D4AS0000000000301'	and warehouse = 'YORK_RETAIL'
order by year, month, day

select top 1000 SKU, warehouse, year, month, day,
	qty_packs,
	idETLBatchRun, ins_ts
from fm.fm_safety_stock
where sku = '01084B2D4AS0000000000301'	and warehouse = 'YORK_RETAIL'
order by year, month, day

select top 1000 SKU, warehouse, year, month, day,
	qty_packs,
	idETLBatchRun, ins_ts
from fm.fm_shortages
where sku = '01084B2D4AS0000000000301'	and warehouse = 'YORK_RETAIL'
order by year, month, day

select top 1000 SKU, warehouse, year, month, day,
	qty_packs,
	idETLBatchRun, ins_ts
from fm.fm_min_stock
where sku = '01084B2D4AS0000000000301'	and warehouse = 'YORK_RETAIL'
order by year, month, day
