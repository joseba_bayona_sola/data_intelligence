
select top 1000 *
from Warehouse.fm.dim_wh_stock_item_static_data_v
where product_id_magento = 1083 and packsize = 30
	and warehouse_name = 'York'
	and sku = '01083B2D4CV0000000000301'

	select top 1000 warehouse_name, product_id_magento, product_family_name, packsize, 
		sku, stock_item_description, 
		planner, abc, stockable, stocked, final
	from Warehouse.fm.dim_wh_stock_item_static_data_v
	where 
		(product_id_magento = 1083 and packsize = 30 and warehouse_name = 'York' and sku in ('01083B2D4CV0000000000301', '01083B2D4CH0000000000301')) or
		(product_id_magento = 2317 and packsize = 30 and warehouse_name = 'York' and sku in ('02317B3D4CH0000000000301')) or -- qty_sales_forecast
		(product_id_magento = 1089 and packsize = 6 and warehouse_name = 'York' and sku in ('01089B3D7CDC10000B200061')) or -- qty_shortages
		(product_id_magento = 1085 and packsize = 90 and warehouse_name = 'York' and sku in ('01085B2D4BZ0000000000901')) -- qty_suppresions
	order by warehouse_name, SKU

---------------------------

select top 1000 *
from Warehouse.fm.fact_planned_requirements_v
where product_id_magento = 1083 and packsize = 30
	and warehouse_name = 'York'
	and sku = '01083B2D4CV0000000000301'

	select top 1000 warehouse_name, product_id_magento, product_family_name, packsize, 
		sku, stock_item_description, 
		supplier_type_name, supplier_name, fm_reference, release_date, due_date, 
		planner, quantity, unit_cost -- overdue_status, 
	from Warehouse.fm.fact_planned_requirements_v
	where 
		-- (product_id_magento = 1083 and packsize = 30 and warehouse_name = 'York' and sku in ('01083B2D4CV0000000000301', '01083B2D4CH0000000000301')) -- or
		(product_id_magento = 2317 and packsize = 30 and warehouse_name = 'York' and sku in ('02317B3D4CH0000000000301'))-- or -- qty_sales_forecast
		-- (product_id_magento = 1089 and packsize = 6 and warehouse_name = 'York' and sku in ('01089B3D7CDC10000B200061')) -- or -- qty_shortages
		-- (product_id_magento = 1085 and packsize = 90 and warehouse_name = 'York' and sku in ('01085B2D4BZ0000000000901')) -- qty_suppresions
	order by warehouse_name, SKU, release_date
