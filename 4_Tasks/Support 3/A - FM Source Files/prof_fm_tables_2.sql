
select top 1000 SKU, warehouse,
	planner, abc, stockable, stocked, final, supplier
from Landing.fm.fm_stock_static_data
-- where sku = '01083B2D4AT0000000000301'
where warehouse not in ('AMSTERDAM_WHOLESALE', 'YORK_WHOLESALE')
	and stocked is null
order by sku, warehouse

	select top 1000 SKU, warehouse, count(*)
	from Landing.fm.fm_stock_static_data
	group by sku, warehouse
	order by count(*) desc, sku, warehouse

	select top 1000 warehouse, 
		charindex('RETAIL', warehouse, 1), 
		charindex('WHOLESALE', warehouse, 1),
		substring(warehouse, 1, charindex('_', warehouse, 1)-1),
		substring(warehouse, charindex('_', warehouse, 1)+1, len(warehouse)),
		substring(warehouse, charindex('_', warehouse, 1)+1, 1),
		count(*)
	from Landing.fm.fm_stock_static_data
	group by warehouse
	order by warehouse

	select top 1000 planner, count(*)
	from Landing.fm.fm_stock_static_data
	where warehouse not in ('AMSTERDAM_WHOLESALE', 'YORK_WHOLESALE')
	group by planner
	order by planner

	select top 1000 abc, count(*)
	from Landing.fm.fm_stock_static_data
	where warehouse not in ('AMSTERDAM_WHOLESALE', 'YORK_WHOLESALE')
	group by abc
	order by abc

	select top 1000 stockable, count(*)
	from Landing.fm.fm_stock_static_data
	where warehouse not in ('AMSTERDAM_WHOLESALE', 'YORK_WHOLESALE')
	group by stockable
	order by stockable

	select top 1000 stocked, count(*)
	from Landing.fm.fm_stock_static_data
	group by stocked
	order by stocked

	select top 1000 final, count(*)
	from Landing.fm.fm_stock_static_data
	group by final
	order by final

	select top 1000 abc, stockable, stocked, final, count(*)
	from Landing.fm.fm_stock_static_data
	group by abc, stockable, stocked, final
	order by abc, stockable, stocked, final


	select top 1000 supplier, count(*)
	from Landing.fm.fm_stock_static_data
	group by supplier
	order by supplier


select top 1000 count(*) over () num_tot,
	count(*) over (partition by fm_reference, destination_warehouse, sku, supplier) num_rep, 
	count(*) over (partition by fm_reference, destination_warehouse, sku) num_rep1, 
	count(*) over (partition by destination_warehouse, sku, supplier) num_rep2, 
	destination_warehouse, SKU, release_date, due_date,
	fm_reference, product_family, pack_size, source_warehouse, supplier, quantity, overdue_status, planner, unit_cost
from Landing.fm.fm_planned_requirements
-- where release_date is null or due_date is null
-- where unit_cost is null -- and supplier <> ''
-- where supplier is null or supplier = ''
order by num_rep1 desc, destination_warehouse, sku, fm_reference, supplier

	select fm_reference, count(*)
	from Landing.fm.fm_planned_requirements
	group by fm_reference
	order by fm_reference

	select top 1000 fm_reference, destination_warehouse, sku, supplier, count(*)
	from Landing.fm.fm_planned_requirements
	group by fm_reference, destination_warehouse, sku, supplier
	order by count(*) desc, fm_reference, destination_warehouse, sku, supplier

	select top 1000 destination_warehouse, sku, supplier, count(*)
	from Landing.fm.fm_planned_requirements
	group by destination_warehouse, sku, supplier
	order by count(*) desc, destination_warehouse, sku, supplier

	select top 1000 destination_warehouse, count(*)
	from Landing.fm.fm_planned_requirements
	group by destination_warehouse
	order by destination_warehouse

	select top 1000 source_warehouse, count(*)
	from Landing.fm.fm_planned_requirements
	group by source_warehouse
	order by source_warehouse

	select top 1000 product_family, count(*)
	from Landing.fm.fm_planned_requirements
	group by product_family
	order by product_family

	select top 1000 supplier, count(*)
	from Landing.fm.fm_planned_requirements
	group by supplier
	order by supplier

	select top 1000 overdue_status, count(*)
	from Landing.fm.fm_planned_requirements
	group by overdue_status
	order by overdue_status

	select top 1000 planner, count(*)
	from Landing.fm.fm_planned_requirements
	group by planner
	order by planner

SET DATEFORMAT dmy

	select top 1000 release_date, CONVERT(DATE, release_date), 
		convert(varchar(8), convert(date, release_date, 103), 112) idCalendarReleaseDate,
		count(*)
	from Landing.fm.fm_planned_requirements
	group by release_date
	order by release_date


	select top 1000 release_date, CONVERT(DATE, release_date), 
		CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112), 
		CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112)),
		isdate(release_date),
		count(*)
	from Landing.fm.fm_planned_requirements
	group by release_date
	order by CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112))

	select top 1000 due_date, CONVERT(DATE, due_date), 
		CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112), 
		CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112)),
		isdate(due_date),
		count(*)
	from Landing.fm.fm_planned_requirements
	group by due_date
	order by CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112))
