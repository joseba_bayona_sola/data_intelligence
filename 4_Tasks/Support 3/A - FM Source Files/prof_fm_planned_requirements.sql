
select *
from DW_GetLenses_jbs.dbo.fm_planned_requirements


select destination_warehouse, source_warehouse, supplier, 
	fm_reference, product_family, pack_size, sku, 
	quantity, unit_cost, 
	release_date, due_date, overdue_status, planner
from DW_GetLenses_jbs.dbo.fm_planned_requirements
-- where fm_reference = '1084-AMSTERDAM_RETAIL-30PASS120190719'
where fm_reference = '1084-GIR_RETAIL-90PASS320190218' and sku = '01084B2D4AT0000000000901'

select fm_reference, count(*)
from DW_GetLenses_jbs.dbo.fm_planned_requirements
group by fm_reference
order by fm_reference

select destination_warehouse, sku, fm_reference, count(*)
from DW_GetLenses_jbs.dbo.fm_planned_requirements
group by destination_warehouse, sku, fm_reference
order by count(*) desc, destination_warehouse, sku, fm_reference
