
select top 1000 *
from Warehouse.fm.fact_fm_data_v
where 
	-- product_id_magento = 1083 and packsize = 30 and warehouse_name = 'York' and sku in ('01083B2D4CV0000000000301', '01083B2D4CH0000000000301')
	-- qty_sales_forecast <> 0 and product_id_magento = 2317
	-- qty_shortages <> 0 and product_id_magento = 1089
	qty_suppressions <> 0 and product_id_magento = 1085

	select top 1000 product_id_magento, product_family_name, count(*), count(distinct sku)
	from Warehouse.fm.fact_fm_data_v
	-- where qty_sales_forecast <> 0
	-- where qty_shortages <> 0
	where qty_suppressions <> 0
	group by product_id_magento, product_family_name
	order by product_id_magento, product_family_name

	select top 1000 warehouse_name, product_id_magento, product_family_name, count(*), count(distinct sku)
	from Warehouse.fm.fact_fm_data_v
	group by warehouse_name, product_id_magento, product_family_name
	order by warehouse_name, product_id_magento, product_family_name

	select top 1000 count(*), count(distinct sku)
	from Warehouse.fm.fact_fm_data_v
	where product_id_magento = 1083 and packsize = 30
		and warehouse_name = 'York'

	select product_id_magento, product_family_name, packsize, 
		sku, stock_item_description, count(*), 
		avg(qty_sales_forecast) qty_sales_forecast, avg(qty_gross_req) qty_gross_req, avg(qty_orders) qty_orders, avg(qty_stock_on_hand) qty_stock_on_hand, avg(qty_shortages) qty_shortages, 
		avg(qty_safety_stock) qty_safety_stock, avg(qty_min_stock) qty_min_stock, avg(qty_max_stock) qty_max_stock, avg(qty_suppressions) qty_suppressions
	from Warehouse.fm.fact_fm_data_v
	where 
		-- product_id_magento = 1083 and packsize = 30 and warehouse_name = 'York' -- and sku in ('01083B2D4CV0000000000301', '01083B2D4CH0000000000301')
		product_id_magento = 1089 and packsize = 6 and warehouse_name = 'York' -- and sku in ('01089B3D7CDC10000B200061')
		-- product_id_magento = 2317 and packsize = 30 and warehouse_name = 'York' -- and sku in ('01089B3D7CDC10000B200061')
		-- product_id_magento = 1085 and packsize = 90 and warehouse_name = 'York' -- and sku in ('01089B3D7CDC10000B200061')
	group by product_id_magento, product_family_name, packsize, 
		sku, stock_item_description
	order by product_id_magento, product_family_name, packsize, 
		sku, stock_item_description
	-- order by qty_shortages desc

select warehouse_name, product_id_magento, product_family_name, packsize, 
	sku, stock_item_description, fm_date,
	qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
	qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
from Warehouse.fm.fact_fm_data_v
where 
	(
	-- (product_id_magento = 1083 and packsize = 30 and warehouse_name = 'York' and sku in ('01083B2D4CV0000000000301', '01083B2D4CH0000000000301')) or
	(product_id_magento = 2317 and packsize = 30 and warehouse_name = 'York' and sku in ('02317B3D4CH0000000000301')) -- or -- qty_sales_forecast
	-- (product_id_magento = 1089 and packsize = 6 and warehouse_name = 'York' and sku in ('01089B3D7CDC10000B200061')) or -- qty_shortages
	-- (product_id_magento = 1085 and packsize = 90 and warehouse_name = 'York' and sku in ('01085B2D4BZ0000000000901')) -- qty_suppresions
	-- (product_id_magento = 1085 and packsize = 30 and warehouse_name = 'York' and sku in ('01085B2D4BZ0000000000301')) -- qty_suppresions
	)
	-- and qty_sales_forecast <> 0
	-- and qty_orders <> 0
	-- and qty_shortages <> 0
	-- and qty_suppressions <> 0
order by warehouse_name, SKU, fm_date
