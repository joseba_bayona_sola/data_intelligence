
select top 1000 sku, warehouse, year, month, day, qty_packs
from DW_GetLenses_jbs.dbo.fm_orders
-- where sku in ('01083B2D4CF0000000000301', 'W00040000000000000000011')
order by sku, warehouse, year, month, day

	select top 1000 count(*)
	from DW_GetLenses_jbs.dbo.fm_orders

	select top 1000 warehouse, count(*)
	from DW_GetLenses_jbs.dbo.fm_orders
	group by warehouse
	order by warehouse

	select top 1000 year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.fm_orders
	group by year, month, day
	order by year, month, day

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.fm_orders
	group by sku
	order by sku

	select sku, warehouse, count(*)
	from DW_GetLenses_jbs.dbo.fm_orders
	group by sku, warehouse
	order by sku, warehouse

----------------------------------------------------------------

select top 1000 sku, 
	case 
		when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
		when (warehouse = 'GIR_RETAIL') then 'Girona'
		when (warehouse = 'YORK_RETAIL') then 'York'
	end warehouse_name, 
	year, month, day, qty_packs
from DW_GetLenses_jbs.dbo.fm_orders
order by sku, warehouse_name, year, month, day

select sku, warehouse_name, count(*) 
from
	(select sku, 
		case 
			when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
			when (warehouse = 'GIR_RETAIL') then 'Girona'
			when (warehouse = 'YORK_RETAIL') then 'York'
		end warehouse_name, 
		year, month, day, qty_packs
	from DW_GetLenses_jbs.dbo.fm_orders) t
group by sku, warehouse_name
order by sku, warehouse_name


select top 1000 wsi.warehousestockitemid_bk, 
	case when (t.sku is not null) then t.sku else wsi.sku end sku,
	case when (t.warehouse_name is not null) then t.warehouse_name else wsi.warehouse_name end warehouse_name,
	wsi.product_id_magento, wsi.product_family_name, wsi.stock_item_description,
	t.num, count(*) over ()
from
		(select sku, warehouse_name, count(*) num
		from
			(select sku, 
				case 
					when (warehouse = 'AMSTERDAM_RETAIL') then 'Amsterdam'
					when (warehouse = 'GIR_RETAIL') then 'Girona'
					when (warehouse = 'YORK_RETAIL') then 'York'
				end warehouse_name, 
				year, month, day, qty_packs
			from DW_GetLenses_jbs.dbo.fm_orders) t
		group by sku, warehouse_name) t
	full join
		Warehouse.stock.dim_wh_stock_item_v wsi on t.sku = wsi.sku and t.warehouse_name = wsi.warehouse_name
-- where wsi.sku is null
-- where t.sku is null
where wsi.sku is not null and t.sku is not null
order by sku, warehouse_name

