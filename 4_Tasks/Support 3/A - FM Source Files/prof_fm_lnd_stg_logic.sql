
		select top 1000 count(*) over (),
			si.stockitemid, si.sku, count(*) over (partition by si.sku) num_rep
		from 
				Landing.mend.gen_prod_stockitem_v si
			inner join
				Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		where si.stockitemid is not null
		order by num_rep desc, sku

		select count(*) over (),
			stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
		from 
			(select si.stockitemid, si.sku, si.stockitemdescription, 
				isnull(wsi.num_records, 0) num_records,
				count(*) over (partition by si.sku) num_rep, 
				rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
			from 
					Landing.mend.gen_prod_stockitem_v si
				left join
					(select stockitemid, count(*) num_records
					from Landing.mend.gen_wh_warehousestockitem_v
					where wh_stock_item_batch_f = 'Y'
					group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
		where num_rep = ord_rep
		order by num_rep desc

--------------------------------

select warehouseid_bk, stockitemid_bk,
	supplierid_bk, sale_type,
	planner, abc, stockable, stocked, final	
from Staging.fm.dim_wh_stock_item_static_data

select top 1000 count(*) over (), *
from Landing.fm.fm_stock_static_data

select top 1000 count(*) over (),
	w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, 
	null supplierid_bk, substring(warehouse, charindex('_', warehouse, 1)+1, 1) sale_type,
	planner, abc, stockable, stocked, final
from 
		Landing.fm.fm_stock_static_data ssd
	inner join
		Landing.mend.gen_wh_warehouse_v w on substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1) = w.shortName
	inner join
		(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
		from 
			(select si.stockitemid, si.sku, si.stockitemdescription, 
				isnull(wsi.num_records, 0) num_records,
				count(*) over (partition by si.sku) num_rep, 
				rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
			from 
					Landing.mend.gen_prod_stockitem_v si
				left join
					(select stockitemid, count(*) num_records
					from Landing.mend.gen_wh_warehousestockitem_v
					where wh_stock_item_batch_f = 'Y'
					group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
		where num_rep = ord_rep) si on ssd.sku = si.sku
where substring(warehouse, charindex('_', warehouse, 1)+1, 1) = 'R'


select top 1000 count(*) over (),
	w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, 
	null supplierid_bk, substring(warehouse, charindex('_', warehouse, 1)+1, 1) sale_type,
	planner, abc, stockable, stocked, final
from 
		Landing.fm.fm_stock_static_data ssd
	inner join
		Landing.mend.gen_wh_warehouse_v w on substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1) = w.shortName
	inner join
		-- Landing.mend.gen_prod_stockitem_v si on ssd.sku = si.sku
		(select si.stockitemid, si.sku
		from 
				Landing.mend.gen_prod_stockitem_v si
			inner join
				Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		where si.stockitemid is not null) si on ssd.sku = si.sku
where substring(warehouse, charindex('_', warehouse, 1)+1, 1) = 'R'

select top 1000 count(*) over (),
	w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, ssd.sku,
	null supplierid_bk, substring(warehouse, charindex('_', warehouse, 1)+1, 1) sale_type,
	planner, abc, stockable, stocked, final
from 
		Landing.fm.fm_stock_static_data ssd
	inner join
		Landing.mend.gen_wh_warehouse_v w on substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1) = w.shortName
	left join
		Landing.mend.gen_prod_stockitem_v si on ssd.sku = si.sku
where si.stockitemid is null

select top 1000 count(*) 
from 
		Landing.fm.fm_stock_static_data ssd
	inner join
		Landing.mend.gen_wh_warehouse_v w on substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1) = w.shortName
	inner join
		-- Landing.mend.gen_prod_stockitem_v si on ssd.sku = si.sku
		(select si.stockitemid, si.sku
		from 
				Landing.mend.gen_prod_stockitem_v si
			inner join
				Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		where si.stockitemid is not null) si on ssd.sku = si.sku


----------------------------------------------

select warehouseid_bk, stockitemid_bk, idCalendarFMDate,
	sale_type, qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, 
	qty_shortages, qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
from Staging.fm.fact_fm_data

----------------------------------------------

select warehouseid_bk, stockitemid_bk, supplierid_bk, fm_reference,
	warehousesourceid_bk, idCalendarReleaseDate, idCalendarDueDate, planner, overdue_status,
	packsize, quantity, unit_cost
from Staging.fm.fact_planned_requirements

select top 1000 count(*) over (), *
from Landing.fm.fm_planned_requirements

SET DATEFORMAT dmy

select top 1000 count(*) over (), count(*) over (partition by w.warehouseid, si.stockitemid, s.supplier_id, fm_reference) num_rep,
	w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, s.supplier_id supplierid_bk, fm_reference,
	null warehousesourceid_bk, 
	CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112)) idCalendarReleaseDate, 
	CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112)) idCalendarDueDate, 
	planner, overdue_status,
	pack_size packsize, quantity, unit_cost
from 
		Landing.fm.fm_planned_requirements pr
	inner join
		Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
	inner join
		(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
		from 
			(select si.stockitemid, si.sku, si.stockitemdescription, 
				isnull(wsi.num_records, 0) num_records,
				count(*) over (partition by si.sku) num_rep, 
				rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
			from 
					Landing.mend.gen_prod_stockitem_v si
				left join
					(select stockitemid, count(*) num_records
					from Landing.mend.gen_wh_warehousestockitem_v
					where wh_stock_item_batch_f = 'Y'
					group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
		where num_rep = ord_rep) si on pr.sku = si.sku
	inner join
		Landing.mend.gen_supp_supplier_v s on pr.supplier = s.supplierName
order by num_rep desc



select top 1000 count(*) over (), count(*) over (partition by w.warehouseid, si.stockitemid, supplier, fm_reference) num_rep,
	w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, -- s.supplier_id supplierid_bk, 
	fm_reference,
	null warehousesourceid_bk, 
	CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112)) idCalendarReleaseDate, 
	CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112)) idCalendarDueDate, 
	planner, overdue_status,
	pack_size packsize, quantity, unit_cost
from 
		Landing.fm.fm_planned_requirements pr
	inner join
		Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
	inner join
		-- Landing.mend.gen_prod_stockitem_v si on ssd.sku = si.sku
		(select si.stockitemid, si.sku
		from 
				Landing.mend.gen_prod_stockitem_v si
			inner join
				Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		where si.stockitemid is not null) si on pr.sku = si.sku
	-- left join
		-- Landing.mend.gen_supp_supplier_v s on pr.supplier = s.supplierName
--where si.stockitemid is null
order by num_rep desc

select count(*)
from 
		Landing.fm.fm_planned_requirements pr
	inner join
		Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
	inner join
		(select si.stockitemid, si.sku
		from 
				Landing.mend.gen_prod_stockitem_v si
			inner join
				Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		where si.stockitemid is not null) si on pr.sku = si.sku
