
select *
from Landing.mag.salesrule_coupon

select convert(date, created_at), count(*)
from Landing.mag.salesrule_coupon
-- where year(created_at) = 2019
group by convert(date, created_at)
order by count(*) desc, convert(date, created_at) desc

--------------------------

select top 1000 count(*) over (), *
from Landing.mag.enterprise_customerbalance_aud
order by balance_id desc

select top 1000 count(*) over (), *
from Landing.mag.enterprise_customerbalance_history_aud
order by history_id desc

select top 1000 convert(date, updated_at), count(*) 
from Landing.mag.enterprise_customerbalance_history_aud
where year(updated_at) = 2019
group by convert(date, updated_at)
order by count(*) desc, convert(date, updated_at) desc
