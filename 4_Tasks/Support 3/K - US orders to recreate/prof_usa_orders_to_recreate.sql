
select *
from DW_GetLenses_jbs.dbo.usa_orders_to_recreate

	select distinct shipment, name, customer
	from DW_GetLenses_jbs.dbo.usa_orders_to_recreate

drop table #gen_ship_customershipment_v

select us.Shipment, us.Name, us.Customer, 
	sh.customershipmentid, sh.orderid, sh.magentoOrderID_int, sh.incrementID, sh.orderStatus, sh.orderIncrementID, sh.shipmentNumber
into #gen_ship_customershipment_v
from 
		(select distinct shipment, name, customer
		from DW_GetLenses_jbs.dbo.usa_orders_to_recreate) us
	left join
		Landing.mend.gen_ship_customershipment_v sh on us.shipment = sh.shipmentNumber

select Shipment, Name, Customer, 
	customershipmentid, orderid, magentoOrderID_int, incrementID, orderStatus, orderIncrementID, shipmentNumber, 
	oh.order_date, oh.shipment_date, oh.country_code_ship, oh.region_name_ship, oh.postcode_shipping
from 
		#gen_ship_customershipment_v sh
	inner join
		Warehouse.sales.dim_order_header_v oh on sh.magentoOrderID_int = oh.order_id_bk
-- where customershipmentid is null -- 0
-- where oh.country_code_ship <> 'US'
order by oh.order_id_bk

select oh.order_id_bk, oh.order_no, Shipment shipment_no
from 
		#gen_ship_customershipment_v sh
	inner join
		Warehouse.sales.dim_order_header_v oh on sh.magentoOrderID_int = oh.order_id_bk
-- where customershipmentid is null -- 0
-- where oh.country_code_ship <> 'US'
order by oh.order_id_bk

	select count(distinct oh.order_id_bk)
	from 
			#gen_ship_customershipment_v sh
		inner join
			Warehouse.sales.dim_order_header_v oh on sh.magentoOrderID_int = oh.order_id_bk


-------------------------------------------------------------

select oh.order_id_bk, oh.order_no, oh.shipment_no, oh.customer_id, 
	oh.order_status_magento_name, oh.order_status_name, oh.customer_order_seq_no_gen,
	oh.global_store_credit_given, oh.local_bank_online_given, oh.global_total_exc_vat, 
	sum(oh.global_total_exc_vat) over (), 
	sum(oh.global_product_cost) over (), sum(oh.global_product_cost_discount) over ()
from 
		#gen_ship_customershipment_v sh
	inner join
		Warehouse.sales.dim_order_header_v oh on sh.magentoOrderID_int = oh.order_id_bk
-- where oh.customer_id = 2843688
order by oh.order_status_name, oh.order_status_magento_name, oh.order_id_bk

	select oh1.order_id_bk, oh1.order_no, oh1.shipment_no, oh1.customer_id, 
		oh1.order_status_magento_name, oh1.order_status_name, --oh1.customer_order_seq_no_gen,
		oh1.global_store_credit_given, oh1.local_bank_online_given, oh1.global_total_exc_vat, 

		oh2.order_id_bk order_id_bk_new, oh2.order_no order_no_new, oh2.shipment_no shipment_no_new, oh2.global_total_exc_vat global_total_exc_vat_new, 

		oh3.order_no order_no_succ
		--sum(oh1.global_total_exc_vat) over (), 
		--sum(oh2.global_total_exc_vat) over ()
	from
			(select oh.order_id_bk, oh.order_no, oh.shipment_no, oh.customer_id, 
				oh.order_status_magento_name, oh.order_status_name, oh.customer_order_seq_no_gen,
				oh.global_store_credit_given, oh.local_bank_online_given, oh.global_total_exc_vat
			from 
					#gen_ship_customershipment_v sh
				inner join
					Warehouse.sales.dim_order_header_v oh on sh.magentoOrderID_int = oh.order_id_bk
			-- where oh.customer_id = 2843688
			) oh1
		left join
			Warehouse.sales.dim_order_header_v oh2 on oh1.customer_id = oh2.customer_id and oh1.customer_order_seq_no_gen + 1 = oh2.customer_order_seq_no_gen 
		left join
			#new_orders oh3 on oh1.customer_id = oh3.customer_id
	order by oh1.order_status_name, oh1.order_status_magento_name, oh1.order_id_bk

---------------------------------------

select t.order_no, oh.order_id_bk, oh.customer_id, oh.customer_email
into #new_orders
from 
		DW_GetLenses_jbs.dbo.usa_orders_to_recreate_success t
	inner join
		Warehouse.sales.dim_order_header_v oh on t.order_no = oh.order_no

	select order_id_bk, order_no, customer_email
	from #new_orders
