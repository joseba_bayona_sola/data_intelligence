	
drop table #mag_edi_stock_item

create table #mag_edi_stock_item(
	product_id				int NOT NULL,
	product_code			varchar(300) NOT NULL,
	BC						char(3),
	DI						char(4),
	PO						char(6),
	CY						char(5),
	AX						char(3),
	AD						char(4),
	DO						char(1),
	CO						varchar(100));
go
	
	insert into #mag_edi_stock_item(product_id, product_code, 
		bc, di, po, cy, ax, ad, do, co)

	select 	
		product_id, product_code, 
		max(bc) bc, max(di) di, max(po) po, max(cy) cy, max(ax) ax, max(ad) ad, max(do) do, max(co) co
	-- into #mag_edi_stock_item
	from
		(select distinct
			product_id, product_code, 
			left(bc, 3) bc, 
			left(di, 4) di, 
			case 
				when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
				when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
				when (len(po) = 1) then '+0' + po + '.00'
				when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
				when (len(po) = 3 and charindex('-', po) = 1) then po + '.00'
				when (len(po) = 3 and charindex('-', po) = 0) then '+0' + po + '0'
				when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
				when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
				when (len(po) = 12) then '+0' + substring(po, 1, 4)
				when (len(po) = 13) then substring(po, 1, 1) + '0' + + substring(po, 2, 4)
				when (len(po) = 14) then substring(po, 1, 6)
				when (len(po) = 6 and charindex('.', po) = 4 and substring(po, 1, 1) not in ('-', '+')) then substring(po, 2, 1) + '0' + substring(po, 3, 5)
				else po
			end po,
			-- replace(cy, ' ', '') cy, 
			case
				when (len(cy) = 6 and charindex('-', cy) = 2) then substring(cy, 2, 6)
				else cy
			end cy,
			case
				when (len(ax) = 1) then '00' + ax
				when (len(ax) = 2) then '0' + ax
				else ax end ax, 
			case 
				when ad in ('Medium', 'MF', 'MID') then 'MED'
				else upper(ad) end ad, 
			do, 
			upper(co) co
		from 
			(select product_id, sku, product_code, 
				case when (bc = '') then NULL else bc end bc, case when (di = '') then NULL else di end di, case when (po = '') then NULL else replace(po, char(13), '') end po, 
				case when (cy = '') then NULL else cy end cy, case when (ax = '') then NULL else ax end ax, 
				case when (ad = '') then NULL else ad end ad, case when (do = '') then NULL else do end do, case when (co = '') then NULL else co end co 
			from
				(select distinct product_id, sku, product_code, 
					bc, di, po, cy, ax, ad, do, co
				from Landing.mag.edi_stock_item_aud) esi) esi) t
	group by product_id, product_code

	select top 1000 *, len(cy) a
	from #mag_edi_stock_item
	order by a desc

	select po, count(*), len(po)
	from Landing.mag.edi_stock_item_aud
	where product_id = 3206
	group by po
	order by po

	select po, count(*)
	from #mag_edi_stock_item
	-- where product_id = 3206
	group by po
	order by po

-- cylinder

select ' -1.25', len(' -1.25'), replace(' -1.25', ' ', ''), len(replace(' -1.25', ' ', ''))


 select distinct product_id, sku, product_code, 
	bc, di, po, cy, ax, ad, do, co, 
	len(cy) a, 
	replace(cy, ' ', ''), 
	ltrim(cy), 
	charindex('-', cy), 
	case
		when (len(cy) = 6 and charindex('-', cy) = 2) then substring(cy, 2, 6)
		else cy
	end
from Landing.mag.edi_stock_item_aud
where product_id in (3206, 1083, 1317)
order by a desc

select *, 
	substring(po, 2, 1) + '0' + substring(po, 3, 5)
from Landing.mag.edi_stock_item_aud
-- where len(cy) = 6 and charindex('-', cy) = 2
where len(po) = 6 and charindex('.', po) = 4 and substring(po, 1, 1) not in ('-', '+')

select *
from Landing.mag.edi_stock_item_aud
where product_code = 'A1DMA-AX10-BC8.5-CY-2.25-DI14.5-PO-0.50'



---------------------------------------------------------------------------------

	select 	
		product_id, product_code, 
		max(bc) bc, max(di) di, max(po) po, max(cy) cy, max(ax) ax, max(ad) ad, max(do) do, max(co) co, 
		len(max(cy)) a
	from
		(select distinct
			product_id, product_code, 
			left(bc, 3) bc, 
			left(di, 4) di, 
			case 
				when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
				when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
				when (len(po) = 1) then '+0' + po + '.00'
				when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
				when (len(po) = 3 and charindex('-', po) = 1) then po + '.00'
				when (len(po) = 3 and charindex('-', po) = 0) then '+0' + po + '0'
				when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
				when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
				when (len(po) = 12) then '+0' + substring(po, 1, 4)
				when (len(po) = 13) then substring(po, 1, 1) + '0' + + substring(po, 2, 4)
				when (len(po) = 14) then substring(po, 1, 6)
				else po
			end po,
			cy, 
			case
				when (len(ax) = 1) then '00' + ax
				when (len(ax) = 2) then '0' + ax
				else ax end ax, 
			case 
				when ad in ('Medium', 'MF', 'MID') then 'MED'
				else upper(ad) end ad, 
			do, 
			upper(co) co
		from 
			(select product_id, sku, product_code, 
				case when (bc = '') then NULL else bc end bc, case when (di = '') then NULL else di end di, case when (po = '') then NULL else replace(po, char(13), '') end po, 
				case when (cy = '') then NULL else cy end cy, case when (ax = '') then NULL else ax end ax, 
				case when (ad = '') then NULL else ad end ad, case when (do = '') then NULL else do end do, case when (co = '') then NULL else co end co 
			from
				(select distinct product_id, sku, product_code, 
					bc, di, po, cy, ax, ad, do, co
				from Landing.mag.edi_stock_item_aud
				where product_id = 1317) esi) esi) t
	group by product_id, product_code
	order by a desc