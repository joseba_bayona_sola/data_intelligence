
select top 1000 batchstockissueid_bk, order_no_erp, order_id_bk, order_date_sync, cage_shipment_date, snap_complete_date, issue_date, datediff(minute, snap_complete_date, issue_date),
	issue_id, cancelled_f, 
	warehouse_name, product_id_magento, product_family_name, packsize, sku, stock_item_description
from Warehouse.alloc.fact_order_line_erp_issue_v
order by batchstockissueid_bk desc

select diff, count(*), min(convert(date, issue_date)), max(convert(date, issue_date))
from
	(select snap_complete_date, issue_date, datediff(minute, snap_complete_date, issue_date) diff
	from Warehouse.alloc.fact_order_line_erp_issue_v) t
group by diff
order by count(*) desc, diff
