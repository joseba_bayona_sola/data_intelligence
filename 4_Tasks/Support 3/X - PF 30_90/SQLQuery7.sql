

	insert into Landing.mag.order_sync_product_replacement (product_id, replacement_product_id, use_for_display, idETLBatchRun) 
		select product_id, replacement_product_id, use_for_display, 1 idETLBatchRun
		from openquery(MAGENTO, 
			'select product_id, replacement_product_id, use_for_display
			from magento01.order_sync_product_replacement')

	select ospr.product_id, pf3090.product_id_new, pf1.name, pf11.product_family_name,
		ospr.replacement_product_id, pf3090.product_id, pf2.name, pf22.product_family_name,
		ospr.use_for_display
	from 
			Landing.mag.order_sync_product_replacement ospr
		left join
			Landing.mag.catalog_product_entity_flat_aud pf1 on ospr.product_id = pf1.entity_id and pf1.store_id = 0
		left join
			Warehouse.prod.dim_product_family_v pf11 on pf1.entity_id = pf11.product_id_magento
		left join
			Landing.mag.catalog_product_entity_flat_aud pf2 on ospr.replacement_product_id = pf2.entity_id and pf2.store_id = 0	
		left join
			Warehouse.prod.dim_product_family_v pf22 on pf2.entity_id = pf22.product_id_magento
		left join
			Landing.map.prod_product_30_90 pf3090 on pf1.entity_id = pf3090.product_id_new
	order by ospr.product_id