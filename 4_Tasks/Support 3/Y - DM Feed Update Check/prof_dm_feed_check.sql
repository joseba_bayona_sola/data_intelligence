
select top 1000 *
from Warehouse.gen.dim_customer
where customer_email = 'emma.haskell@visiondirect.co.uk'

select top 1000 *
from Warehouse.sales.dim_order_header_v
where customer_id = 922653
order by order_date

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where customer_id = 922653


select top 1000 *
from Warehouse.dm.fact_customer_signature_dm
where client_id = 922653

select top 1000 *
from Warehouse.dm.EMV_sync_list
where client_id = 922653

-----------------------------------------------

select top 1000 customer_email
from Warehouse.dm.Dotmailer_Unsubscribed_Customers
where customer_email = 'emma.haskell@visiondirect.co.uk'

select top 1000 *
from Warehouse.dm.Dotmailer_Supressed_Data
where email = 'emma.haskell@visiondirect.co.uk'

	select email, store_id, status, dateRemoved, timestamp,
		idETLBatchRun, ins_ts
	from Landing.mag.dotmailer_suppressed_contact_aud
	where email = 'emma.haskell@visiondirect.co.uk'

-----------------------------------------------

drop table #fact_customer_signature_dm

select client_id, website_id, store_id, email, segment_lifecycle, last_order_date, num_of_orders
into #fact_customer_signature_dm
from Warehouse.dm.fact_customer_signature_dm

	select website_id, store_id, count(*)
	from #fact_customer_signature_dm
	group by website_id, store_id
	order by website_id, store_id

select count(*) over (), csdm.client_id, csdm.email, 
	csdm.segment_lifecycle, csdm.last_order_date, csdm.num_of_orders,
	csdm.store_id, dsc.store_id, dsc.timestamp, 
	dsc2.store_id, dsc2.timestamp
from 
		#fact_customer_signature_dm csdm
	inner join
		Landing.mag.dotmailer_suppressed_contact_aud dsc on csdm.email = dsc.email and csdm.store_id <> dsc.store_id
	left join
		Landing.mag.dotmailer_suppressed_contact_aud dsc2 on csdm.email = dsc2.email and csdm.store_id = dsc2.store_id
where 
	((csdm.store_id = 20 and dsc.store_id not in (8, 9, 20, 21)) or
	(csdm.store_id = 21 and dsc.store_id not in (8, 9, 20, 21)) or
	(csdm.store_id not in (20, 21)))
	-- and csdm.email = 'emma.haskell@visiondirect.co.uk'
	and dsc2.store_id is null
order by csdm.client_id, csdm.email, csdm.store_id, dsc.store_id

	select distinct csdm.client_id, csdm.email, 
		csdm.segment_lifecycle, csdm.last_order_date, csdm.num_of_orders,
		csdm.store_id
	from 
			#fact_customer_signature_dm csdm
		inner join
			Landing.mag.dotmailer_suppressed_contact_aud dsc on csdm.email = dsc.email and csdm.store_id <> dsc.store_id
		left join
			Landing.mag.dotmailer_suppressed_contact_aud dsc2 on csdm.email = dsc2.email and csdm.store_id = dsc2.store_id
	where 
		((csdm.store_id = 20 and dsc.store_id not in (8, 9, 20, 21)) or
		(csdm.store_id = 21 and dsc.store_id not in (8, 9, 20, 21)) or
		(csdm.store_id not in (20, 21)))
		-- and csdm.email = 'emma.haskell@visiondirect.co.uk'
		and dsc2.store_id is null
	order by csdm.segment_lifecycle, csdm.last_order_date, csdm.client_id

	select csdm.store_id, dsc.store_id, count(*)
	from 
			#fact_customer_signature_dm csdm
		inner join
			Landing.mag.dotmailer_suppressed_contact_aud dsc on csdm.email = dsc.email and csdm.store_id <> dsc.store_id
	where 
		(csdm.store_id = 20 and dsc.store_id not in (8, 9, 20, 21)) or
		(csdm.store_id = 21 and dsc.store_id not in (8, 9, 20, 21)) or
		(csdm.store_id not in (20, 21))
	group by csdm.store_id, dsc.store_id
	order by csdm.store_id, dsc.store_id
