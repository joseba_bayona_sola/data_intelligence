
-- Data at OL level
-- Questions
	-- Goal: What is this data wanted for
	-- Order Date vs Invoice Data
	-- Order Status: Include Cancel // Refunds
	-- Revenue: Local / Global - Inc VAT / Exc VAT
	-- Stores: All ?? Only UK?

-- order_id_bk 
-- order_date
-- customer_order_seq_no
-- category_name 
-- product_family_name
-- num_items 
-- line_revenue
-- order_revenue

-- reseller_yn 

select top 1000 count(*) over (), convert(int, Clienturn) customer_id
from DW_GetLenses_jbs.dbo.dotmailer_user_ids
order by convert(int, Clienturn)

select top 1000 dmu.customer_id, c.idCustomer_sk, c.customer_id_bk, c.customer_email
from 
		(select convert(int, Clienturn) customer_id
		from DW_GetLenses_jbs.dbo.dotmailer_user_ids) dmu
	left join
		Warehouse.gen.dim_customer c on dmu.customer_id = c.customer_id_bk
where c.customer_id_bk is null
order by dmu.customer_id

------------------------------------------------------------

select count(*)
from 
		(select convert(int, Clienturn) customer_id
		from DW_GetLenses_jbs.dbo.dotmailer_user_ids) dmu
	inner join
		Warehouse.gen.dim_customer c on dmu.customer_id = c.customer_id_bk
	inner join
		Warehouse.sales.dim_order_header oh on c.idCustomer_sk = oh.idCustomer_sk_fk
	inner join
		Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	inner join
		Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
where oh.order_date between '2016-01-01' and '2019-06-01'
	and order_source = 'M' and s.store_id_bk in (20)



-- 

select top 1000 c.idCustomer_sk, c.customer_id_bk, c.customer_email, 
 	oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date, -- oh.invoice_date, 
	os.order_status_name,
	oh.customer_order_seq_no_gen, oh.local_total_inc_vat
from 
		Warehouse.sales.dim_order_header oh
	inner join
		(select idCustomer_sk, customer_id_bk, customer_email
		from Warehouse.gen.dim_customer
		where customer_id_bk in (21, 33, 41, 42, 48)) c on oh.idCustomer_sk_fk = c.idCustomer_sk
	inner join
		Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	inner join
		Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
where oh.order_date > '2016-01-01' and order_source = 'M' and s.store_id_bk in (20)
order by c.customer_id_bk, oh.order_date	

select top 1000 c.idCustomer_sk, c.customer_id_bk, c.customer_email, 
	oh.order_id_bk, oh.order_no, oh.order_date, oh.invoice_date, os.order_status_name,
	oh.customer_order_seq_no_gen, 
	pf.category_name, pf.product_id_magento, pf.product_family_name
from 
		Warehouse.sales.dim_order_header oh
	inner join
		(select idCustomer_sk, customer_id_bk, customer_email
		from Warehouse.gen.dim_customer
		where customer_id_bk in (21, 33, 41, 42, 48)) c on oh.idCustomer_sk_fk = c.idCustomer_sk
	inner join
		Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	inner join
		Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
	inner join
		Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
where oh.order_date > '2016-01-01'
order by c.customer_id_bk, oh.order_date	

-- 

----------------------------------------------------------------
----------------------------------------------------------------

select c.idCustomer_sk, c.customer_id_bk, c.customer_email, 
 	oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date, -- oh.invoice_date, 
	os.order_status_name,
	oh.customer_order_seq_no_gen, oh.local_total_inc_vat
into #temp_oh_c
from 
		(select convert(int, Clienturn) customer_id
		from DW_GetLenses_jbs.dbo.dotmailer_user_ids) dmu
	inner join
		Warehouse.gen.dim_customer c on dmu.customer_id = c.customer_id_bk
	inner join
		Warehouse.sales.dim_order_header oh on c.idCustomer_sk = oh.idCustomer_sk_fk
	inner join
		Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	inner join
		Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
where oh.order_date between '2016-01-01' and '2019-06-01'
	and order_source = 'M' and s.store_id_bk in (20)

select -- oh.order_id_bk order_id, 
	oh.order_no, oh.order_date, oh.order_status_name,
	oh.customer_id_bk customer_id, oh.customer_order_seq_no_gen, 
	pf.category_name, pf.product_id_magento, pf.product_family_name, 
	ol.qty_unit, ol.local_total_inc_vat ol_revenue, oh.local_total_inc_vat oh_revenue
from 
		#temp_oh_c oh
	inner join
		Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
	inner join
		Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk