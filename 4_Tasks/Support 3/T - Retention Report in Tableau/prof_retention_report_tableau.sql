
-- Think about how C - A records affect

-- Retention Report on Sales DS
	-- Using rank_freq_time
	-- Using Month Diff between invoice_dates
	-- Using Month Diff between invoice_dates + Specific dimension filtering

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where customer_id = 1507450

select top 1000 *
from Warehouse.sales.dim_order_header_v
where customer_id = 1507450
order by order_date

select top 1000 *
from Warehouse.act.fact_activity_sales_v
where customer_id = 1507450
order by activity_date

select top 1000 *
from Landing.map.sales_rank_freq_time

select top 1000 *
from Warehouse.sales.dim_rank_freq_time
