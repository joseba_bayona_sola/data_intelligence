
		select order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel
		from
			(select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel, 
				count(*) over (partition by o.order_id_bk) num_rep, 
				dense_rank() over (partition by o.order_id_bk order by cc.coupon_id_bk) ord_rep
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) <> 0 -- and len(o.applied_rule_ids) >= 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code) t
		where num_rep = ord_rep
		order by applied_rule_ids, coupon_code
