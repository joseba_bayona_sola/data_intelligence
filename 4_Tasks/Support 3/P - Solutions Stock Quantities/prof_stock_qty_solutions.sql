
select top 1000 warehouse_name, product_id_magento, category_name, product_family_name, 
	-- qty_available, qty_outstanding_allocation, qty_on_hold
	sum(qty_available + qty_outstanding_allocation + qty_on_hold)
from Warehouse.stock.dim_wh_stock_item_batch_v
where (qty_available + qty_outstanding_allocation + qty_on_hold) <> 0
	and category_name like 'SL%' or category_name like 'EC%'
group by warehouse_name, product_id_magento, category_name, product_family_name
order by product_id_magento, category_name, product_family_name, warehouse_name
