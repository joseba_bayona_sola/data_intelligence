
select top 1000 count(*) over (partition by stock_item_description) num_rep, *
from Warehouse.prod.dim_stock_item_v
order by num_rep desc, sku

select top 1000 count(*) over (partition by stock_item_description, warehouse_name) num_rep, *
from Warehouse.stock.dim_wh_stock_item_v
order by num_rep desc, sku

select top 1000 *
from Warehouse.stock.dim_wh_stock_item
