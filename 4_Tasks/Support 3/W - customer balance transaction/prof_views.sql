
select top 1000 *
from Warehouse.act.dim_customer_balance_v
where customer_email like 'michael.kraftman%'
order by balance_id_bk

select top 1000 *
from Warehouse.act.dim_customer_balance_hist_v
where customer_email like 'michael.kraftman%'
order by balance_id_bk, balance_date

select top 1000 *
from Warehouse.act.fact_customer_balance_transaction_v
where customer_email like 'michael.kraftman%'
order by balance_id_bk, transaction_date
