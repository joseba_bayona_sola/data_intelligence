
select top 1000 *
from Landing.mag.enterprise_customerbalance_aud
order by balance_id desc

select ecbh.history_id, ecb.balance_id, ecb.customer_id, ecb.website_id, ecb.amount, 
	ecbh.updated_at, ecbh.action, ecbh.balance_delta, ecbh.additional_info, ecbh.frontend_comment,
	year(updated_at), month(updated_at), sum(balance_delta) over (partition by year(updated_at), month(updated_at))
from 
		Landing.mag.enterprise_customerbalance_aud ecb
	inner join
		Landing.mag.enterprise_customerbalance_history_aud ecbh on ecb.balance_id = ecbh.balance_id
where ecb.customer_id in (1665, 1398274, 2460857, 2496589)
-- where action = 2 and balance_delta < 0
-- where action = 1
order by ecb.customer_id, ecbh.updated_at desc
-- order by ecbh.updated_at desc

select top 1000 *
from Landing.mag.enterprise_customerbalance_history_aud
where balance_id = 172907
order by action, updated_at

-- 1: Updated (By admin: . NULL)(Positive, Negative)
-- 2: Created (By admin:) (Positive, Negative)
-- 3: Used (#Order) (Minus value)
-- 4: Refunded (#Order) (Positive Value(
-- 5: Reverted (#Order) (Positive value)
-- 6: Expired (Minus)
select top 1000 action, count(*)
from Landing.mag.enterprise_customerbalance_history_aud
group by action
order by action

select top 1000 action, case when (balance_delta > 0) then 'PLUS' else 'MINUS' end flag, count(*)
from Landing.mag.enterprise_customerbalance_history_aud
group by action, case when (balance_delta > 0) then 'PLUS' else 'MINUS' end
order by action, flag

select top 1000 is_customer_notified, count(*)
from Landing.mag.enterprise_customerbalance_history_aud
group by is_customer_notified
order by is_customer_notified

select top 1000 *
from Landing.mag.enterprise_customerbalance_history_aud
where action = 1 and balance_delta < 0
order by updated_at desc

---------------------------------------

	select top 1000 history_id, balance_id, updated_at, action, balance_delta, additional_info, frontend_comment,
		-- charindex('By admin:', additional_info, 1), charindex('.', additional_info, 1), len('By admin:'), 
		-- substring(additional_info, charindex('By admin:', additional_info, 1) + len('By admin:'), charindex('.', additional_info, 1) - (charindex('By admin:', additional_info, 1) + len('By admin:'))) -- 1

		charindex('#', additional_info, 1), charindex(',', additional_info, 1),
		substring(additional_info, charindex('#', additional_info, 1) + 1, len(additional_info))
	from Landing.mag.enterprise_customerbalance_history_aud
	where action = 5
	order by updated_at desc

---------------------------------------

	select 1 customer_id, 'aa' customer_name, 'bb' store_name, 
		'aa' order_no, 'aa' customer_service_agent, 'aa' action_name,
		GETUTCDATE() transaction_date, 
		1 transaction_amount, 
		'aa', additional_info, 'aa', frontend_comment, 'aa', is_customer_notified 


	drop table #cbt

	select ecbh.history_id, 
		ecb.website_id, w.website_name,
		ecb.customer_id, c.customer_email, c.first_name, c.last_name,
		case when (ecbh.action in (1, 2)) then substring(additional_info, charindex('By admin:', additional_info, 1) + len('By admin:'), charindex('.', additional_info, 1) - (charindex('By admin:', additional_info, 1) + len('By admin:'))) 
			else null end customer_service_agent,
		case 
			when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, len(additional_info)) 
			when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) = 0) then substring(additional_info, charindex(' ', additional_info, 1) + 1, len(additional_info)) 
			when (ecbh.action in (4) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, charindex(',', additional_info, 1) - charindex('#', additional_info, 1) -1) 
			else null 
		end order_no,
		case 
			when (ecbh.action = 1) then 'Updated'
			when (ecbh.action = 2) then 'Created'
			when (ecbh.action = 3) then 'Used'
			when (ecbh.action = 4) then 'Refunded'
			when (ecbh.action = 5) then 'Reverted'
			when (ecbh.action = 6) then 'Expired'
		end action_name,
		ecbh.updated_at transaction_date, 
		ecbh.balance_delta transaction_amount, 
		ecbh.additional_info, ecbh.frontend_comment, ecbh.is_customer_notified
		, ecbh.balance_id, dense_rank() over (partition by ecbh.balance_id order by ecbh.updated_at, ecbh.history_id) ord_rep
	into #cbt
	from 
			Landing.mag.enterprise_customerbalance_aud ecb
		inner join
			Landing.mag.enterprise_customerbalance_history_aud ecbh on ecb.balance_id = ecbh.balance_id
		left join
			(select website_id, -- max(name) name, charindex('/', max(name), 1),
				case when (charindex('/', max(name), 1) = 0) then max(name) else substring(max(name), 1, charindex('/', max(name), 1)-1) end website_name
			from Landing.mag.core_store
			group by website_id) w on ecb.website_id = w.website_id
		left join
			Landing.aux.gen_dim_customer_aud c on ecb.customer_id = c.customer_id_bk
	-- where ecbh.updated_at between '2019-11-01' and '2020-03-01'
	-- where ecbh.updated_at > '2018-01-01' 
		-- and ecbh.action in (4)
		-- and website_id in (26, 28)
	order by ecb.customer_id, ecbh.updated_at desc

	select *
	from #cbt
	where action_name = 'Refunded' -- Updated - Created - Used - Refunded - Reverted - Expired
		-- and customer_service_agent is null
	-- where customer_id = 1449525

	select *
	from #cbt
	order by customer_id, transaction_date
	-- where customer_email is null

	select customer_service_agent, count(*), sum(transaction_amount)
	from #cbt
	group by customer_service_agent
	order by customer_service_agent

	select action_name, count(*), sum(transaction_amount)
	from #cbt
	group by action_name
	order by action_name

	select order_no, count(*)
	from #cbt
	group by order_no
	order by order_no

	select website_id, website_name, count(*)
	from #cbt
	group by website_id, website_name
	order by website_id, website_name

	select ord_rep, count(*)
	from #cbt
	group by ord_rep
	order by ord_rep

		select action_name, count(*), sum(transaction_amount)
		from #cbt
		where ord_rep = 1
		group by action_name
		order by action_name

		select *
		from #cbt
		where ord_rep = 1 and action_name <> 'Created'
		order by action_name, customer_id, transaction_date

-----------------------------------------------------------------------
select top 1000 entity_id, increment_id, customer_id
into #oh
from Landing.mag.sales_flat_order_aud
where increment_id in 
	('17000740303', '8006528367', '8006567317', '21000613109', '21000609842', '8006544088', '8006524980', '26000218203', '5002243310', '8006536224', 
	'8006544088', '8006567317', '8006542559', '21000613109', '8006543008', '17000662616', '17000662616', '17000662934', '17000662934', '17000654792', 
	'17000654792', '17000739795', '8006578998', '17000739672', '24000255198', '8006554424', '24000254769', '8006536202', '8006547272', '8006542129', 
	'26000228537', '26000228537', '5002235016', '5002235016', '5002236163', '5002236163', '24000254927', '5002243476', '5002241978')

select ol.order_id_bk, ol.order_line_id_bk, ol.order_no, ol.order_date, ol.invoice_date,
	ol.store_name, ol.customer_id, 
	ol.order_status_name, ol.product_id_magento, ol.product_family_name, 
	ol.local_subtotal, ol.local_store_credit_used, ol.local_total_inc_vat, ol.local_store_credit_given, ol.local_bank_online_given
from 
		#oh oh 
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.entity_id = ol.order_id_bk
-- order by ol.store_name, ol.order_date, ol.product_id_magento
order by ol.customer_id, ol.order_date, ol.product_id_magento

select history_id, 
	website_id, website_name,
	cbt.customer_id, customer_email, first_name, last_name,
	customer_service_agent, order_no,
	action_name,
	transaction_date, 
	transaction_amount, 
	additional_info, frontend_comment, is_customer_notified
from 
		(select distinct customer_id from #oh) oh 
	inner join
		#cbt cbt on oh.customer_id = cbt.customer_id
order by cbt.customer_id, transaction_date