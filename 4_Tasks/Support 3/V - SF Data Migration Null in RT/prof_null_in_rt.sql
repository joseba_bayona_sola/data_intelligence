
select top 1000 entity_id, email, created_at, updated_at
from Landing.mag_rt.customer_entity_flat
where firstname is null
order by updated_at, created_at

drop table #customer_entity_flat_null

select top 1000 entity_id, email, created_at, updated_at
into #customer_entity_flat_null
from Landing.mag_rt.customer_entity_flat
where firstname is null
order by updated_at, created_at

select t1.*
from #customer_entity_flat_null t1

select t1.*, t2.*
from 
		#customer_entity_flat_null t1
	left join
		Landing.mag_rt.customer_entity_varchar t2 on t1.entity_id = t2.entity_id
		-- Landing.mag_rt.customer_entity_int t2 on t1.entity_id = t2.entity_id
		-- Landing.mag_rt.customer_entity_datetime t2 on t1.entity_id = t2.entity_id

select t1.*, t2.*
from 
		#customer_entity_flat_null t1
	left join
		Landing.mag_rt.customer_entity_flat_aud t2 on t1.entity_id = t2.entity_id

select top 1000 *
from Landing.mag_rt.customer_entity_varchar
where entity_id = 604751

select top 1000 *
from Landing.mag_rt.customer_entity_flat
where entity_id = 604751

-------------------------------------------------

select t1.*, t2.*
from 
		#customer_entity_flat_null t1
	left join
		Landing.mag_rt.customer_entity t2 on t1.entity_id = t2.entity_id


-----------------------------------------------------------
-----------------------------------------------------------