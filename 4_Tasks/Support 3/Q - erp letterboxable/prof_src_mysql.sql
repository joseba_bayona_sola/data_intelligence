
select entity_id, increment_id, created_at, shipping_description
from sales_flat_order
order by entity_id desc
limit 1000

select packsize_pref_id, product_id, packsize, priority, strategy, comment, enabled, letterboxable
from po_warehouse_pvx_packsize_pref
order by product_id, packsize
limit 1000

  select product_id, packsize, count(*)
  from po_warehouse_pvx_packsize_pref
  group by product_id, packsize
  order by count(*) desc, product_id, packsize

  select strategy, count(*)
  from po_warehouse_pvx_packsize_pref
  group by strategy
  order by strategy

  select comment, count(*)
  from po_warehouse_pvx_packsize_pref
  group by comment
  order by comment
  
  select enabled, count(*)
  from po_warehouse_pvx_packsize_pref
  group by enabled
  order by enabled
  
  select letterboxable, count(*)
  from po_warehouse_pvx_packsize_pref
  group by letterboxable
  order by letterboxable
  
-- --------------------------------------

select *
from po_warehouse_pvx_order
order by pvx_order_id desc
limit 1000

select *
from po_warehouse_pvx_order_item
order by pvx_order_item_id desc
limit 1000
