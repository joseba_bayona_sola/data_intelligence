
-- Query: 2 queries: per PFPS - per OH

	select *
	from Landing.map.sales_shipping_method
	order by shipping_method

	select top 1000 order_id_bk, order_no, order_date, invoice_date, website, country_code_ship, shipping_method_name, global_total_inc_vat
	from Warehouse.sales.dim_order_header_v

	-- erp: order_date_sync - prod_allocation (product, packsize, quantity, volume, weight) -- 

	select packsize_pref_id, product_id, packsize, letterboxable
	into #po_warehouse_pvx_packsize_pref
	from openquery(MAGENTO,
		'select packsize_pref_id, product_id, packsize, letterboxable
		from magento01.po_warehouse_pvx_packsize_pref')

	select *
	from #po_warehouse_pvx_packsize_pref

	select count(*) over (partition by wpps.product_id, wpps.packsize) num_rep,
		wpps.packsize_pref_id, pf.category_name, wpps.product_id, pf.product_family_name, pf.product_lifecycle_name, wpps.packsize, wpps.letterboxable, 
		pfps.weight, pfps.height * pfps.width * pfps.depth volume
		, pfps.height, pfps.width, pfps.depth
	from
			#po_warehouse_pvx_packsize_pref wpps
		inner join
			Warehouse.prod.dim_product_family_v pf on wpps.product_id = pf.product_id_magento
		left join
			(select packsizeid, magentoProductID_int, magentoProductID, name, size, weight, height, width, depth
			from Landing.mend.gen_prod_productfamilypacksize_v) pfps on wpps.product_id = pfps.magentoProductID_int and wpps.packsize = pfps.size
	-- order by num_rep desc, wpps.product_id, wpps.packsize
	-- order by weight, letterboxable, product_id, packsize
	order by volume, letterboxable, product_id, packsize

	select count(*) over (partition by wpps.product_id, wpps.packsize) num_rep,
		wpps.packsize_pref_id, pf.category_name, pfps.magentoProductID_int product_id, pf.product_family_name, pf.product_lifecycle_name, pfps.size packsize, wpps.letterboxable, 
		pfps.weight, pfps.height * pfps.width * pfps.depth volume
		, pfps.height, pfps.width, pfps.depth
	from
			#po_warehouse_pvx_packsize_pref wpps
		right join
			(select packsizeid, magentoProductID_int, magentoProductID, name, size, weight, height, width, depth
			from Landing.mend.gen_prod_productfamilypacksize_v) pfps on wpps.product_id = pfps.magentoProductID_int and wpps.packsize = pfps.size
		inner join
			Warehouse.prod.dim_product_family_v pf on pfps.magentoProductID_int = pf.product_id_magento
	where wpps.packsize_pref_id is null and pfps.packsizeid is not null
	-- order by num_rep desc, wpps.product_id, wpps.packsize
	-- order by weight, letterboxable, product_id, packsize
	order by volume, letterboxable, product_id, packsize

	select *
	from Warehouse.prod.dim_product_family_v

-- Modelling
 
	-- Landing po_warehouse_pvx_packsize_pref

	-- Landing Measures per package type

	select *
	from Landing.mend.gen_prod_productfamilypacksize_v

	select *
	from Warehouse.prod.dim_product_family_pack_size