--Flat file table

select COUNT(distinct(order_id)), COUNT(distinct(customer_id)), COUNT(1) from DW_getlenses_jbs.dbo.missing_rem_date

select top 1000 order_id, customer_id, hits
from DW_getlenses_jbs.dbo.missing_rem_date


drop table #temp_orders
drop table #temp_orders_oh

drop table #temp_export_missing_info

--order info of the customers provided --7685
select distinct r.customer_id rem_customer_id, r.order_id rem_order_id, v.order_no, v.customer_id, v.reminder_date, v.customer_order_seq_no, v.order_date
into #temp_orders
from 
		Warehouse.sales.fact_order_line_v v
	right join 
		DW_getlenses_jbs.dbo.missing_rem_date r on v.customer_id = r.customer_id

--order info of the customers provided --7685
select r.customer_id rem_customer_id, r.order_id rem_order_id, 
	v.order_id_bk, v.order_no, v.customer_id, v.reminder_date, v.customer_order_seq_no, v.order_date, v.order_status_name
into #temp_orders_oh
from 
		Warehouse.sales.dim_order_header_v v
	right join 
		DW_getlenses_jbs.dbo.missing_rem_date r on v.customer_id = r.customer_id

	select *
	from #temp_orders_oh
	order by customer_id, order_date

--using lag to get just get the data of the order previous to the one provided
select v.customer_id, v.order_no, v.order_date, v.prev_order_no, v.prev_order_date, v.prev_reminder_date
-- into #temp_export_missing_info
from
	(select 
		o.customer_id, o.order_no, o.rem_order_id, o.customer_order_seq_no, o.order_date, o.reminder_date, 
		lag(o.order_no, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_order_no,
		lag(o.order_date, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_order_date,
		lag(o.reminder_date, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_reminder_date
	from #temp_orders o
	where o.customer_id = 15617) v
where order_no = rem_order_id



--using lag to get just get the data of the order previous to the one provided
select v.customer_id, v.order_id_bk, v.order_no, v.order_date, v.prev_order_no, v.prev_order_date, v.prev_reminder_date, datediff(dd, v.order_date, v.prev_reminder_date) diff_dd
-- into #temp_export_missing_info
from
	(select 
		o.customer_id, o.order_id_bk, o.order_no, o.rem_order_id, o.customer_order_seq_no, o.order_date, o.reminder_date, 
		lag(o.order_no, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_order_no,
		lag(o.order_date, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_order_date,
		lag(o.reminder_date, 1) over (partition by o.customer_id order by o.customer_order_seq_no) prev_reminder_date
	from #temp_orders_oh o
	-- where o.customer_id = 15617
	) v
where order_no = rem_order_id
order by diff_dd