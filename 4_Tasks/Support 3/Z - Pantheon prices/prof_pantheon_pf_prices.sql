
select *
from Landing.mag.catalog_product_entity_tier_price
where entity_id = 1083
order by website_id

	select website_id, count(*)
	from Landing.mag.catalog_product_entity_tier_price
	group by website_id
	order by website_id

select *
from Landing.aux.mag_catalog_product_price
-- where product_id = 1083
order by product_id, store_id

	select store_id, store_name, count(*)
	from Landing.aux.mag_catalog_product_price
	group by store_id, store_name
	order by store_id, store_name

select cpp.product_id, pc.magento_sku sku, cpp.product_name, -- cpp.store_id, 
	cpp.store_name, cpp.product_lifecycle, pc.category_bk product_type, cpp.qty packsize, -- cpp.value price_lens, 
	cpp.pack_price--, cpp.price_type
	--, min_store_id
-- into #pf_price_uk
from 
		Landing.aux.mag_catalog_product_price cpp
	left join
		Landing.aux.mag_prod_product_family_v pc on cpp.product_id = pc.product_id_bk
	right join
		(select entity_id, min(store_id) min_store_id
		from Landing.sf_mig.product_family_v
		where store_id = 20
		group by entity_id) pf on cpp.product_id = pf.entity_id
where store_id >=20
-- 	and product_id = 1083
	and store_name = 'visiondirect.co.uk' and price_type = 'Regular' -- visiondirect.ie - visiondirect.nl - visiondirect.es - visiondirect.it - visiondirect.be/nl - visiondirect.fr
order by min_store_id desc, product_id, store_id

select pf2.*
from 
		#pf_price_uk pf1
	right join
		(select entity_id, name
		from Landing.sf_mig.product_family_v
		where store_id = 20) pf2 on pf1.product_id = pf2.entity_id
where pf1.product_id is null
order by pf2.entity_id