

select top 1000 order_id_bk, order_no, order_date, channel_name, marketing_channel_name
from Warehouse.sales.dim_order_header_v
where order_id_bk in (9544226, 9544610, 9544288)
order by order_id_bk


select top 1000 order_id_bk, order_no, order_date, affilCode, ga_medium, ga_source, channel_name_bk, marketing_channel_name_bk
from Landing.aux.sales_dim_order_header_aud
where order_id_bk in (9544226, 9544610, 9544288)
order by order_id_bk

select top 1000 oh.order_id_bk, oh.order_no, oh.order_date, oh.affilCode, oh.ga_medium, oh.ga_source, oh.channel_name_bk, oh.marketing_channel_name_bk, 
	ga.transaction_date, ga.medium, ga.source, ga.ins_ts
from 
		Landing.aux.sales_dim_order_header_aud oh
	left join	
		Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
where oh.order_id_bk in (9544226, 9544610, 9544288)

select top 1000 oh.order_id_bk, oh.order_no, oh.order_date, oh.affilCode, oh.ga_medium, oh.ga_source, oh.channel_name_bk, oh.marketing_channel_name_bk, 
	ga.transaction_date, ga.medium, ga.source, ga.ins_ts, 
	mc.channel_name
from 
		Landing.aux.sales_dim_order_header_aud oh
	left join	
		Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
	left join
		Landing.map.sales_ch_ga_medium_ch mc on ga.medium = mc.ga_medium
where oh.order_id_bk in (9544226, 9544610, 9544288)


select top 1000 transaction_date, count(*)
from Landing.mag.ga_entity_transaction_data_aud
group by transaction_date
order by transaction_date desc

---------------------------------------

select oh.order_id_bk, oh.order_no, oh.order_date, oh.affilCode, oh.ga_medium, oh.ga_source, oh.channel_name_bk, oh.marketing_channel_name_bk, 
	ga.transaction_date, ga.medium, ga.source, ga.ins_ts, 
	mc.channel_name
into #oh_channel
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join	
		Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
	left join
		Landing.map.sales_ch_ga_medium_ch mc on ga.medium = mc.ga_medium
where oh.channel_name_bk <> mc.channel_name

	select order_id_bk, order_no, order_date, affilCode, ga_medium, ga_source, channel_name_bk, marketing_channel_name_bk, 
		transaction_date, medium, source, ins_ts, 
		channel_name
	from #oh_channel
	order by order_date

		select convert(date, order_date), count(*)
		from #oh_channel
		group by convert(date, order_date)
		order by convert(date, order_date)

		select channel_name_bk, channel_name, count(*)
		from #oh_channel
		group by channel_name_bk, channel_name
		order by channel_name_bk, channel_name

		select channel_name_bk, marketing_channel_name_bk, channel_name, count(*)
		from #oh_channel
		group by channel_name_bk, marketing_channel_name_bk, channel_name
		order by channel_name_bk, marketing_channel_name_bk, channel_name

