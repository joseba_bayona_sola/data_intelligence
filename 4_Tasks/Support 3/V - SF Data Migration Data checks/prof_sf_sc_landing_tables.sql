

select top 1000 count(*) over (), *
from Landing.sf_sc.dm_customer

select top 1000 *
from Landing.sf_sc.dm_customer_aud
order by ins_ts desc

-- 2653560
select top 1000 count(*) over (), *
from Landing.sf_sc.dm_customer_aud
order by ins_ts desc

select convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), datepart(second, ins_ts), count(*)
from Landing.sf_sc.dm_customer_aud
group by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), datepart(second, ins_ts)
order by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), datepart(second, ins_ts)


select convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*)
from Landing.sf_sc.dm_customer_aud
group by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
order by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)


select top 1000 count(*) over (), c1.*, c2.ins_ts, c2.upd_ts
from 
		Landing.sf_sc.dm_customer c1
	inner join
		Landing.sf_sc.dm_customer_aud c2 on c1.id = c2.id
-- where c1.ins_ts <> c2.ins_ts
where c1.ins_ts = c2.ins_ts
order by id

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- dotmailer customers in RT views
	-- Not in View: Guest customers in DM not in Magento
select email, max(ins_ts) updated_at
into #dotmailer_c_rt
from Landing.mag.dotmailer_suppressed_contact
group by email

select c1.*, c2.entity_id, c2.PersonContactId, c2.Id, c2.unsubscribe_all, c2.EON_Opt_in__c
from
		#dotmailer_c_rt c1
	left join	 
		Landing.sf_mig.customer_rt_v c2 on c1.email = c2.email
-- where c2.email is null
-- where c2.EON_Opt_in__c <> 0
order by c2.updated_at

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

select *
from Landing.sf_mig.customer_rt_v
where firstname is null
order by updated_at desc

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

select duration, count(*), sum(count(*)) over (), convert(decimal(12, 4), count(*)) * 100 / sum(count(*)) over ()
from ControlDB.logging.t_ETLBatchRunRT_v
group by duration
order by duration

select year(startTime), month(startTime), day(startTime), count(*)
from ControlDB.logging.t_ETLBatchRunRT_v
-- where duration = 0
-- where duration in (1, 2)
where duration > 2
group by year(startTime), month(startTime), day(startTime)
order by year(startTime), month(startTime), day(startTime)

select idETLBatchRun, codETLBatchType, codFlowType, idPackage, flow_name, package_name,
	description, dateFrom, dateTo, 
	startTime, finishTime, duration, runStatus, -- message,
	ins_ts
from ControlDB.logging.t_ETLBatchRunRT_v
where duration > 3
order by idETLBatchRun

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- 188.692
select count(*) over (), *
from Landing.sf_mig.customer_v
where entity_sfcc_exported is null
order by updated_at

select *
from Landing.sf_mig.customer_rt_v
where entity_sfcc_exported is null
order by updated_at



-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

select *
from Landing.sf_mig.customer_rt_v
where entity_id = 3013884

select *
from Landing.sf_mig.customer_address_rt_v
where entity_id = 3013884

select *
from Landing.sf_mig.customer_preferences_rt_v
where entity_id = 3013884

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

(@P0 nvarchar(4000),@P1 nvarchar(4000),@P2 nvarchar(4000),@P3 nvarchar(4000),@P4 bigint)insert into sf_sc."dm_customer" (
		Id,
        PersonContactId,
       SFCC_Customer_Id__pc,
        PersonEmail,
        idETLBatchRun)
        
        VALUES
        (
        @P0,
        @P1,
        @P2,       
        @P3,
        @P4
        )                                        

select * 
from 
	sf_mig.customer_address_rt_v 
LEFT join 
	sf_sc.dm_customer on sf_mig.customer_address_rt_v.entity_id = sf_sc.dm_customer.SFCC_Customer_Id__pc 
where sf_sc.dm_customer.SFCC_Customer_Id__pc is not null

select * 
from 
	sf_mig.customer_preferences_rt_v 
LEFT join 
	sf_sc.dm_customer on sf_mig.customer_preferences_rt_v.entity_id = sf_sc.dm_customer.SFCC_Customer_Id__pc 
where sf_sc.dm_customer.SFCC_Customer_Id__pc is not null and sf_mig.customer_preferences_rt_v.EON_Opt_in__c = 1