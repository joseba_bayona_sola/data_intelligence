

drop table #customer_not_in_sf_sc

select *
into #customer_not_in_sf_sc
from Landing.sf_mig.customer_v
where entity_sfcc_exported is null
order by updated_at

select *
into DW_GetLenses_jbs.dbo.customer_not_in_sf_sc
from #customer_not_in_sf_sc

select *
into DW_GetLenses_jbs.dbo.customer_not_in_sf_sc
from #customer_not_in_sf_sc

	select count(*)
	from DW_GetLenses_jbs.dbo.customer_not_in_sf_sc
	where email like '%@@%'

	select entity_id, email, store_id, created_at, updated_at, created_in, 
		firstname, lastname,
		EON_Migrated_From_Site__c, 
		dob,
		EON_Last_Order_Date__c, SFCC_Last_Login_Time__c
	-- select *
	select entity_id, email, EON_Last_Order_Date__c, SFCC_Last_Login_Time__c
	-- from DW_GetLenses_jbs.dbo.customer_not_in_sf_sc
	from #customer_not_in_sf_sc
	-- where email like '%@%@%' -- 111272
	-- where email like '%@%@%' and (SFCC_Last_Login_Time__c > dateadd(year, -3, getutcdate()) or EON_Last_Order_Date__c > dateadd(year, -3, getutcdate())) -- 3
	where email not like '%@@%' -- 77340
		-- and lastname = '' -- 203
		-- and dob is not null -- 61
		and email LIKE '%^[\+A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$%' 
	-- where email not like '%@%@%' and (SFCC_Last_Login_Time__c > dateadd(year, -1, getutcdate()) or EON_Last_Order_Date__c > dateadd(year, -1, getutcdate())) -- 40065 // 22566
	-- where email not like '%@%@%' and (created_at > GETUTCDATE() -2 or updated_at > GETUTCDATE() -2)
	-- where not (SFCC_Last_Login_Time__c > dateadd(year, -3, getutcdate())) and (not (EON_Last_Order_Date__c > dateadd(year, -3, getutcdate())) or EON_Last_Order_Date__c is null)-- 148544
	order by SFCC_Last_Login_Time__c desc
	-- order by updated_at desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.customer_not_in_sf_sc
	where email like '%holgado%'
	
	select store_id, count(*)
	from #customer_not_in_sf_sc
	where email not like '%@%@%' 
	group by store_id
	order by store_id

	select EON_Migrated_From_Site__c, count(*)
	from #customer_not_in_sf_sc
	where email like '%@%@%' 
	group by EON_Migrated_From_Site__c
	order by EON_Migrated_From_Site__c

	select top 1000 *
	from #customer_not_in_sf_sc
	where entity_id in (2913799, 3126173)
		and email LIKE '%^[\+A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$%' 

------------------------------------------------------------------------

select max(convert(int, SFCC_Customer_Id__pc)), count(*)
from Landing.sf_sc.dm_customer_aud
where ins_ts < convert(datetime, '2020-02-19 11:30:00')

select top 1000 count(*) over (), *
from Landing.sf_sc.dm_customer_aud
where ins_ts > convert(datetime, '2020-02-19 11:30:00')
	and convert(int, SFCC_Customer_Id__pc) > 3110802
order by ins_ts
