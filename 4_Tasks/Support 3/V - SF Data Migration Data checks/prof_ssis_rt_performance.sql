	-- t_PackageRunRT_v
	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRunRT_v
	where idETLBatchRun = 5923

	-- t_PackageRunMessageRT_v
	select idPackageRunMessage, prm.idPackageRun, prm.flow_name, prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunRTMessage_v prm
		inner join
			ControlDB.logging.t_PackageRunRT_v pr on prm.idPackageRun = pr.idPackageRun
	where pr.idETLBatchRun = 5446
		--and prm.package_name = 'lnd_stg_sales_fact_order' --  // stg_dwh_sales_fact_order
	order by prm.package_name, prm.message

	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where sp.idETLBatchRun = 5446
		--and spm.package_name = 'lnd_stg_sales_fact_order' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime

	select top 1000 *
	from Landing.mag_rt.customer_entity_flat_aud
	where idETLBatchRun = 5446

	select top 1000 *
	from Landing.mag_rt.customer_address_entity_flat_aud
	where idETLBatchRun = 5446
