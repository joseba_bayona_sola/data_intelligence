select top 1000 -- count(*) over (), 
	customer_id, customer_email, 
	num_tot_orders_gen, num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund
from Warehouse.act.fact_customer_signature_v
where num_tot_orders > 0 and num_tot_cancel > 0 and num_tot_refund > 0
	-- and customer_id = 197358

select top 1000 order_id_bk, website, order_no, order_date, invoice_date, customer_id, customer_email, 
	order_status_name, order_status_magento_name, customer_status_name,
	customer_order_seq_no, customer_order_seq_no_gen, order_currency_code,
	global_subtotal, sum(global_subtotal) over (partition by customer_id, order_status_name), 
	global_total_inc_vat, sum(global_total_inc_vat) over (partition by customer_id, order_status_name)
from Warehouse.sales.dim_order_header_v
where customer_id = 197358
order by order_date

	-- Logic: Global Total Inc VAT on OK + REFUND