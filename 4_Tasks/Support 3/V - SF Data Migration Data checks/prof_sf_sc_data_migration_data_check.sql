
drop table #customer

select top 1000 idCustomer_sk, customer_id_bk, customer_email
into #customer
from Warehouse.gen.dim_customer
where customer_email in ('kyleeg325@gmail.com', 'kermarrec.solenn@gmail.com', 'harrymorgan666@hotmail.co.uk', 'joaquin@demolicionestecnicas.es', 
	'allisonglynnebingham@yahoo.com', 'gannonammons18@gmail.com', 'brookeloriallen@outlook.com', 'connollyjulia09@gmail.com', 'janpoppysmith@outlook.com')


	select *
	from #customer

	select c.customer_id_bk, c.customer_email, oh.order_id_bk, oh.order_no, oh.order_date, oh.website, oh.order_status_name, oh.order_status_magento_name, 
		oh.customer_order_seq_no, oh.customer_order_seq_no_gen, oh.customer_order_seq_no_web,
		oh.local_subtotal, oh.local_shipping, oh.local_discount, oh.local_store_credit_used, oh.local_total_inc_vat, 
		oh.global_subtotal, oh.global_shipping, oh.global_discount, oh.global_store_credit_used, oh.global_total_inc_vat, 
		oh.local_total_refund, oh.local_store_credit_given, oh.local_bank_online_given,
		oh.global_total_refund, oh.global_store_credit_given, oh.global_bank_online_given, 
		oh.local_to_global_rate, oh.order_currency_code
	from 
			#customer c
		inner join
			Warehouse.sales.dim_order_header_v oh on c.customer_id_bk = oh.customer_id
	order by c.customer_id_bk, oh.order_date

	select c.customer_id_bk, c.customer_email, 
		cs.website_group_create, cs.website_create,
		cs.create_date, cs.register_date, cs.last_order_date, cs.last_login_date,
		cs.num_tot_orders, cs.num_tot_refund, cs.subtotal_tot_orders, cs.subtotal_tot_refund
	from 
			#customer c
		inner join
			Warehouse.act.fact_customer_signature_v cs on c.customer_id_bk = cs.customer_id
	order by c.customer_id_bk


	select top 1000 c.customer_id_bk, c.customer_email, 
		c2.EON_Number_of_Orders__pc, c2.EON_Number_of_Refunds__c, c2.EON_Total_Order_Value__pc, c2.EON_Total_Refund_Value__c	
	from 
			#customer c
		inner join	
			Landing.sf_mig.customer_v c2 on c.customer_id_bk = c2.entity_id
