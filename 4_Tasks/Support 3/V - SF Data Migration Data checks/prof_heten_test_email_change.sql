
select top 1000 * -- entity_id, email, created_at, updated_at
from Landing.mag.customer_entity_flat_aud
where email in ('uat3vd@gmail.com', 'julie.leslie4@outlook.com', 'gedunn12@gmail.com')
order by email

select top 1000 * -- entity_id, email, created_at, updated_at
from Landing.mag_rt.customer_entity_flat_aud
where email in ('uat3vd@gmail.com', 'uat333vd@gmail.com', 'julie.leslie4@outlook.com', 'gedunn12@gmail.com')
order by email

select top 1000 *
from Landing.sf_mig.customer_rt_v
where email in ('uat3vd@gmail.com', 'uat333vd@gmail.com', 'julie.leslie4@outlook.com')

select top 1000 *
from Landing.sf_mig.customer3_rt_v
where email in ('uat3vd@gmail.com', 'uat333vd@gmail.com', 'julie.leslie4@outlook.com')

select top 1000 *
from Landing.sf_sc.dm_customer_aud
-- where PersonEmail in ('uat3vd@gmail.com', 'uat333vd@gmail.com')
where convert(int, SFCC_Customer_id__pc) in (2977103, 3179184)

------------------------------------------------------------------------

						select c1.entity_id, c1.email, 
							c1.store_id, 
							c1.is_active, 
							c1.created_at, c1.updated_at, 
							c1.created_in, 
							c1.prefix, c1.firstname, c1.middlename, c1.lastname, c1.suffix, 
							c1.cus_phone, 
							c1.dob, c1.gender,
							c1.default_billing, c1.default_shipping, 
							c1.unsubscribe_all, c1.unsubscribe_all_date, 
							c1.taxvat, c1.password_hash, c1.check_prescription, c1.tc_accepted_date, c1.tc_version
						from 
								Landing.mag.customer_entity_flat_aud c1
							inner join
								Landing.mag.customer_entity_all c_all on c1.entity_id = c_all.entity_id
							inner join
								Landing.mag_rt.customer_entity_flat_aud c2 on c1.email = c2.email and c1.entity_id <> c2.entity_id
						-- where c1.email in ('uat3vd@gmail.com', 'uat333vd@gmail.com')
						union
						select entity_id, email, 
							store_id, 
							is_active, 
							created_at, updated_at, 
							created_in, 
							prefix, firstname, middlename, lastname, suffix, 
							cus_phone, 
							dob, gender,
							default_billing, default_shipping, 
							unsubscribe_all, unsubscribe_all_date, 
							taxvat, password_hash, check_prescription, tc_accepted_date, tc_version
						from Landing.mag_rt.customer_entity_flat_aud
						where email in ('uat3vd@gmail.com', 'uat333vd@gmail.com')
						union
						select c.entity_id, c.email, 
							c.store_id, 
							c1.is_active, 
							c.created_at, case when (c.updated_at > dms.updated_at) then c.updated_at else dms.updated_at end updated_at, 
							c.created_in, 
							c.prefix, c.firstname, c1.middlename, c.lastname, c1.suffix, 
							c.cus_phone, 
							c.dob, c1.gender,
							c.default_billing, c.default_shipping, 
							c.unsubscribe_all, c1.unsubscribe_all_date, 
							c1.taxvat, c1.password_hash, c.check_prescription, c.tc_accepted_date, c.tc_version
		
							from
									--(select email, max(ins_ts) updated_at
									--from Landing.mag.dotmailer_suppressed_contact
									--group by email) dms 
									(select email, max(ins_ts) updated_at
									from
										(select email, ins_ts
										from Landing.mag.dotmailer_suppressed_contact
										union
										select sc_cust.PersonEmail email, isnull(sc_pref.upd_ts, sc_pref.ins_ts) ins_ts
										from 
												Landing.sf_sc.dm_preferences_aud sc_pref
											inner join
												Landing.sf_sc.dm_customer_aud sc_cust on sc_pref.EON_Account__Record_Id__c = sc_cust.Id 
										where sc_pref.EON_Method__c = 'Email' and isnull(sc_pref.upd_ts, sc_pref.ins_ts) > getutcdate() -1) t
									group by email) dms
								inner join
									Landing.sf_mig.customer_v c on dms.email = c.email
								inner join
									Landing.mag.customer_entity_flat_aud c1 on c.entity_id = c1.entity_id
								left join
									Landing.mag_rt.customer_entity_flat_aud c_rt on c.entity_id = c_rt.entity_id
							where c_rt.entity_id is null	
								and c.email in ('uat3vd@gmail.com', 'uat333vd@gmail.com')
