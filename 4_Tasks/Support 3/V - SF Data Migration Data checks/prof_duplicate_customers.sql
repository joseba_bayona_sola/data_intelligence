
select top 1000 cd.entity_id, cd.email, c.email, c.website_group_id, c.website_id, c.store_id, 
	cd.created_at, cd.num_rep, cd.migration_f, cd.last_order_date, 
	cd.last_order_date, 
	sc_cust.Id, sc_cust.PersonContactId,
	null delete_f,
	cd.ins_ts
from 
		Landing.mag.customer_entity_duplicates cd
	inner join
		Landing.mag.customer_entity_flat_aud c on cd.entity_id = c.entity_id
	left join
		Landing.sf_sc.dm_customer_aud sc_cust on cd.entity_id = convert(int, sc_cust.SFCC_Customer_Id__pc)
-- where cd.email in ('michaelgeyskens@live.be', 'michaelgeyskens@msn.com')
where cd.email in ('corinnabridges@gmail.com', 'jamiemartin569@gmail.com', 'jbressend1981@gmail.com', 'nickjohn02@blueyonder.co.uk') order by cd.email
-- where cd.email <> rtrim(ltrim(c.email)) order by cd.email
-- order by cd.ins_ts desc, cd.email


select top 1000 *
from Landing.mag.customer_entity_flat_aud
where entity_id in (2756494, 3118313)

select top 1000 *
from Landing.sf_sc.dm_customer_aud
where SFCC_Customer_Id__pc in (2756494, 3118313)

--------------------------------------------------------------

	select entity_id, email, email_dupl, created_at, 
		website_group_id, website_id, store_id,		
		num_rep, last_order_date,
		EON_Account__Record_Id__c, PersonContactId,
		migration_f, migration_f_final, delete_f,
		idETLBatchRun, ins_ts
	from Landing.mag.customer_entity_duplicates
	where email <> email_dupl
	order by created_at desc;

	select count(*), count(distinct email), count(distinct email_dupl), count(distinct EON_Account__Record_Id__c)
	from Landing.mag.customer_entity_duplicates
	where email not like '%@@%'

	-- uniqueness of migration_f = 'y' per email
	select count(*), count(distinct email), count(distinct email_dupl), count(distinct EON_Account__Record_Id__c)
	from Landing.mag.customer_entity_duplicates
	where migration_f_final = 'Y'
		and email not like '%@@%'

	-- uniqueness of migration_f = 'y' per email
	select *
	from Landing.mag.customer_entity_duplicates
	-- where migration_f = 'Y' and EON_Account__Record_Id__c is null and email not like '%@@%'
	where migration_f <> 'Y' and EON_Account__Record_Id__c is not null and email not like '%@@%'
	order by email

	-- customers that per email were migrated more than once to SF SC
	select *
	from
		(select count(EON_Account__Record_Id__c) over (partition by email) num_rep1, *
		from Landing.mag.customer_entity_duplicates) c
	where num_rep1 > 1

	-- customers that are not any more duplicated by email (email has changed)
	select c1.*
	from 
			Landing.mag.customer_entity_duplicates c1
		left join	
			(select c.entity_id, c.email, c.created_at, c.num_rep
			from
				(select entity_id, email, created_at, 
					count(*) over (partition by email) num_rep
				from
					(select entity_id, rtrim(ltrim(email)) email, created_at
					from Landing.mag.customer_entity_all) c) c
			where num_rep > 1) c2 on c1.entity_id = c2.entity_id
	where c2.entity_id is null

--------------------------------------------------------------

select cd.*
from 
		Landing.mag.customer_entity_duplicates cd
	left join
		Landing.mag.customer_entity_all c on cd.entity_id = c.entity_id
where c.entity_id is null

--------------------------------------------------------------

	-- Beth Feed
	select top 1000 entity_id, email, email_dupl, created_at, 
		website_group_id, website_id, store_id
	from Landing.mag.customer_entity_duplicates
	order by email_dupl, created_at