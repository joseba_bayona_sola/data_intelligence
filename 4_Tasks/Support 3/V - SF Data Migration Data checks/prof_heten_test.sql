
-- 'uat1vd@gmail.com', 'uat7vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com', 'jibs1284@gmail.com'

-- SF SC tables

select top 1000 *
from Landing.sf_sc.dm_customer_aud
where PersonEmail in ('uat1vd@gmail.com', 'uat7vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com', 'jibs1284@gmail.com')
order by PersonEmail

select top 1000 *
from Landing.sf_sc.dm_customer_aud
where PersonEmail in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com')
order by PersonEmail

select top 1000 c.PersonEmail, 
	p.*
from 
		Landing.sf_sc.dm_customer_aud c
	inner join
		Landing.sf_sc.dm_preferences_aud p on c.Id = p.EON_Account__Record_Id__c
where PersonEmail in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by c.PersonEmail

select top 1000 c.customer_email, c.unsubscribe_all, c.EON_Opt_in__c_Email, c.ins_ts, c.upd_ts,
	p.*
from 
		Warehouse.gen.dim_customer c
	inner join
		Landing.sf_sc.dm_preferences_aud p on c.Id = p.EON_Account__Record_Id__c
where customer_email in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by c.customer_email

	select *
	from Warehouse.gen.dim_customer_wrk
	where customer_email in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
	order by customer_email

select top 1000 *
from Warehouse.dm.EMV_sync_list
where email in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by email

	select top 1000 *
	from Warehouse.dm.fact_customer_signature_dm
	where email in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
	order by email

	select top 1000 *
	from Warehouse.dm.Dotmailer_supressed_data
	where email in ('uat1vd@gmail.com', 'uat2vd@gmail.com', 'uat3vd@gmail.com', 'uat4vd@gmail.com', 'uat5vd@gmail.com', 'uat6vd@gmail.com', 'uat71vd@gmail.com', 'uat8vd@gmail.com', 'uat9vd@gmail.com', 'uat10vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
	order by email


-- SF MIG views

select top 1000 *
from Landing.sf_mig.customer_rt_v
where email in ('uat1vd@gmail.com', 'uat7vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by email

select top 1000 *
from Landing.sf_mig.customer_address_rt_v
where email in ('uat1vd@gmail.com', 'uat7vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by email

select top 1000 *
from Landing.sf_mig.customer_rt_v
where email in ('uat1vd@gmail.com', 'uat7vd@gmail.com', 'uat12vd@gmail.com', 'uat20vd@gmail.com')
order by email