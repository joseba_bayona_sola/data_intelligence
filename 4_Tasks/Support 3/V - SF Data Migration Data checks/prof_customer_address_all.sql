
		-- 4151978 - 8.30 min
		select entity_id, increment_id, parent_id, 
			entity_type_id, attribute_set_id, is_active, 
			created_at, updated_at
		into DW_GetLenses_jbs.dbo.customer_address_entity_all
		from openquery(MAGENTO, 
			'select entity_id, increment_id, parent_id, 
				entity_type_id, attribute_set_id, is_active, 
				created_at, updated_at  
			from magento01.customer_address_entity')

		select top 1000 *
		from DW_GetLenses_jbs.dbo.customer_address_entity_all

		-- 4151997 - 12 min
		select entity_id, parent_id, 
			created_at, updated_at
		into DW_GetLenses_jbs.dbo.customer_address_entity_all2
		from openquery(MAGENTO, 
			'select entity_id, parent_id, 
				created_at, updated_at  
			from magento01.customer_address_entity')


-------------------------------------------------------------
	
	select top 1000 count(*) over (), cae.*
	from 
			Landing.mag.customer_address_entity_aud cae
		left join
			DW_GetLenses_jbs.dbo.customer_address_entity_all caea on cae.entity_id = caea.entity_id
	where caea.entity_id is null
	order by cae.updated_at desc
