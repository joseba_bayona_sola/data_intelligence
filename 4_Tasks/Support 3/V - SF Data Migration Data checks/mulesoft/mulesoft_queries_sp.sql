
use Landing
go


-- DMaddressUpdates
	select * 
	from sf_mig.customer_address_rt_v 
	where entity_sfcc_exported is not null  
		and updated_at > :source and updated_at < :target 
	order by updated_at desc

	-- SP CALL
	declare @dd1 datetime, @dd2 datetime
	set @dd1 = getutcdate() -1
	set @dd2 = getutcdate() 
	exec sf_mig.lnd_mule_ds_get_updated_customer_address 1, 1, @dd1, @dd2

-- DMCustomerUpdates
	select * 
	from sf_mig.customer_rt_v 
	where entity_sfcc_exported is not null 
		and updated_at > :source and updated_at < :target  
	order by updated_at desc
	
	-- SP CALL
	declare @dd1 datetime, @dd2 datetime
	set @dd1 = getutcdate() -1
	set @dd2 = getutcdate() 
	exec sf_mig.lnd_mule_ds_get_updated_customer 1, 1, @dd1, @dd2


-- DMpreferenceUpdates
	select * 
	from sf_mig.customer_preferences_rt_v 
	where entity_sfcc_exported is not null   
		and updated_at  > :source and updated_at < :target 
	order by updated_at desc
	
	-- SP CALL
	declare @dd1 datetime, @dd2 datetime
	set @dd1 = getutcdate() -1
	set @dd2 = getutcdate() 
	exec sf_mig.lnd_mule_ds_get_updated_customer_preferences 1, 1, @dd1, @dd2


-- syncAddresses

	select * 
	from 
			sf_mig.customer_address_rt_v 
		LEFT join 
			sf_sc.dm_customer on sf_mig.customer_address_rt_v.entity_id = sf_sc.dm_customer.SFCC_Customer_Id__pc 
	where sf_sc.dm_customer.SFCC_Customer_Id__pc is not null order by sf_mig.customer_address_rt_v.entity_sfcc_exported desc

	-- SP CALL
	exec sf_mig.lnd_mule_ds_get_new_customer_address 1, 1		


-- SyncCustomers
	delete from sf_sc.dm_customer

	-- SP CALL
	exec sf_mig.lnd_mule_ds_delete_customer 1, 1

	select * 
	from sf_mig.customer_rt_v 
	where entity_sfcc_exported is null

	-- SP CALL
	exec sf_mig.lnd_mule_ds_get_new_customer 1, 1

	insert into sf_sc."dm_customer" (Id, PersonContactId, SFCC_Customer_Id__pc, PersonEmail, idETLBatchRun) VALUES
		(:Id, :PersonContactId, :EON_Legacy_Customer_Id__c, :PersonEmail, :idETLBatchRun)
	
	-- SP CALL
	exec sf_mig.lnd_mule_ds_insert_customer 1, 1, 'a', 'a', '10000000000', 'a'

-- syncPreferences

	select * 
	from 
			sf_mig.customer_preferences_rt_v 
		LEFT join 
			sf_sc.dm_customer on sf_mig.customer_preferences_rt_v.entity_id = sf_sc.dm_customer.SFCC_Customer_Id__pc 
	where sf_sc.dm_customer.SFCC_Customer_Id__pc is not null and sf_mig.customer_preferences_rt_v.EON_Opt_in__c = 1

	-- SP CALL
	exec sf_mig.lnd_mule_ds_get_new_customer_preferences 1, 1



-- vd-sync-preferences-sctowhFlow


	delete from  sf_sc.dm_preferences

	-- SP CALL
	exec sf_mig.lnd_mule_ds_delete_customer_preferences 1, 1

	insert into  sf_sc."dm_preferences" (EON_Preference_Record_Id__c, EON_Account__Record_Id__c, EON_Method__c, EON_Frequency__c, EON_Opt_in__c, EON_Legacy_Preference_Id__c,
		CreatedById, CreatedDate, idETLBatchRun)
		VALUES(:recordid, :eon_account__c, :eon_method__c, :EON_Frequency__c, :eon_opt_in__c, :EON_Legacy_Preference_Id__c, 
			:CreatedById, :CreatedDate, :idetlbatchrun)

	-- SP CALL
	declare @dd datetime
	set @dd = convert(datetime, '2020-02-25')
	exec sf_mig.lnd_mule_ds_insert_customer_preferences 1, 1, 'a', 'a', 'a', 'a', 1, 'a', 'a', @dd



-- guestToCustomer

	select * 
	from Landing.sf_mig.customer_rt_v 
	where email in ($(vars.inClause))

	-- SP CALL
	exec sf_mig.lnd_mule_ds_get_customer_by_email 1, 1, 'billyandboo@gmail.com'

