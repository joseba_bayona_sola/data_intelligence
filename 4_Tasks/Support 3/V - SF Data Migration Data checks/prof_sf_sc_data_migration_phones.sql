
drop table #customer_null_address
drop table #customer_null_address_oh

select *
into #customer_null_address
from Landing.mag.customer_entity_flat_aud
where default_billing is null and default_shipping is null

select entity_id, email, created_at, updated_at, cus_phone, default_billing, default_shipping
from #customer_null_address

		select entity_id customer_id_bk, email, created_at, updated_at, cus_phone, 
			case 
				when (reminder_type in ('both', 'email', 'sms', 'reorder')) then 'Y'
				else 'N'
			end reminder_f,
			reminder_type reminder_type_name_bk, reminder_period reminder_period_bk, 
			reminder_mobile reminder_phone, country_id reminder_phone_country_id
		into #customer_null_address_oh
		from
			(select c.entity_id, c.email, c.created_at, c.updated_at, c.cus_phone, 
				o.entity_id order_id, -- o.created_at, 
				o.reminder_type, o.reminder_period, o.reminder_date, o.reminder_mobile,
				o.country_id,
				count(*) over (partition by c.entity_id) num_rows_cust, 
				rank() over (partition by c.entity_id order by o.created_at, o.entity_id) rank_rows_cust
			from 
					#customer_null_address c
				left join 
					(select o.*, oas.country_id
					from 
							Landing.mag.sales_flat_order_aud o
						left join
							Landing.mag.sales_flat_order_address_aud oas on o.shipping_address_id = oas.entity_id -- billing_address_id
					where status not in ('canceled', 'close', 'archived')) o on c.entity_id = o.customer_id) t
		where num_rows_cust = rank_rows_cust

select customer_id_bk, email, created_at, updated_at, cus_phone, 
	reminder_f,
	reminder_type_name_bk, reminder_period_bk, 
	reminder_phone, reminder_phone_country_id
from #customer_null_address_oh
where reminder_phone is not null and reminder_phone <> ''
order by customer_id_bk desc
