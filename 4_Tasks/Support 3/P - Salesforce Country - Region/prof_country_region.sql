
select country_code, country_name
from Warehouse.gen.dim_country
order by country_code, country_name

select c.country_code, c.country_name, r.region_id_bk region_id, r.region_name
from 
		Warehouse.gen.dim_region r
	inner join
		Warehouse.gen.dim_country c on r.idCountry_sk_fk = c.idCountry_sk
order by c.country_code, c.country_name, r.region_name

-----------------------------------------

select t.country_id country_code, c.country_name, t.default_name region_name
from
		(select country_id, default_name
		from openquery(MAGENTO, 'select distinct country_id, default_name from magento01.directory_country_region')) t
	left join
		Warehouse.gen.dim_country c on t.country_id = c.country_code
-- where c.country_name is null
order by country_id, default_name
Finland