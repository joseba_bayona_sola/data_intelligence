
drop table #oh_stats

select order_id_bk, order_no, order_date, order_source, local_total_inc_vat * local_to_global_rate global_total_inc_vat, num_lines
into #oh_stats
from Warehouse.sales.dim_order_header
where order_date between '2019-01-01' and '2020-01-01'
	and order_source = 'M'

select top 1000 *, convert(date, order_date), datepart(hour, order_date)
from #oh_stats
order by order_date

	-- stats per day
		-- MAX: 2019-11-29	12616 // AVG: 5798.31
	select convert(date, order_date), count(*) orders_per_day, 
		count(*) over () num_days, sum(count(*)) over () tot_orders, 
		sum(count(*)) over () / convert(decimal(12, 4), count(*) over ()) avg_orders_per_day
	from #oh_stats
	group by convert(date, order_date)
	order by count(*) desc
	-- order by convert(date, order_date) desc

	-- stats per hour
		-- -- MAX: 2019-11-29 9	1190 // AVG: 241.62
	select top 1000 convert(date, order_date), datepart(hour, order_date), count(*) orders_per_hour, 
		count(*) over () num_hours, sum(count(*)) over () tot_orders,
		sum(count(*)) over () / convert(decimal(12, 4), count(*) over ()) avg_orders_per_hour
	from #oh_stats
	group by convert(date, order_date), datepart(hour, order_date)
	order by count(*) desc
	-- order by convert(date, order_date), datepart(hour, order_date)


	select top 1000 avg(global_total_inc_vat), max(global_total_inc_vat), avg(convert(decimal(12, 4), num_lines)), max(convert(decimal(12, 4), num_lines))
	from #oh_stats

	select top 1000 *
	from #oh_stats
	order by global_total_inc_vat desc

	select top 1000 *
	from #oh_stats
	order by num_lines desc