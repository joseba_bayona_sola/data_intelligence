
select top 1000 *
from Landing.map.prod_product_family_group

select top 1000 *
from Landing.map.prod_product_family_group_pf
where product_id = 1233

select product_id_magento, category_name, cl_type_name, cl_feature_name, product_family_group_name,
	product_family_name
from Warehouse.prod.dim_product_family_v
-- where product_family_name like '%expression%'

	select category_name, cl_type_name, cl_feature_name, product_family_group_name, count(*)
	from Warehouse.prod.dim_product_family_v
	group by category_name, cl_type_name, cl_feature_name, product_family_group_name
	order by category_name, cl_type_name, cl_feature_name, product_family_group_name

------------------------------------------------

drop table #pf_segments

select product_family_name, product_family_group_name_old, product_family_group_name_new
into #pf_segments
from
	(select 'Biofinity XR toric' product_family_name, null product_family_group_name_old, 'Monthly SiHy Toric' product_family_group_name_new
	union
	select 'everclear COMFORT', null, 'Daily HY Sph' -- everclear COMFORT, everclear COMFORT (5 Pack) / COMFORT & COMFORT 5pk
	union
	select 'everclear COMFORT (5 Pack)', null, 'Daily HY Sph'
	union
	select 'Live Daily Disposable', null, 'Daily SiHy sph'
	union
	select 'Biofinity Energys', 'Monthly HY Sph', 'Monthly SiHy Sph' -- Biofinity Energys / Biofinity Energy
	union
	select 'Soflens Multifocal', 'Monthly SiHy Sph', 'monthly Hy Sph'
	union
	select 'Avaira Toric', 'others', 'Monthly SiHy Toric'
	union
	select 'Avaira Vitality', 'others', 'Monthly SIHy Sph'
	union
	select 'Bausch & Lomb ULTRA for Presbyopia', 'others', 'Monthly SiHY Multi' -- Bausch & Lomb ULTRA for Presbyopia / Ultra for presbyopia
	union
	select 'Biofinity XR', 'others', 'Monthly SiHy Sph'
	union
	select 'Biomedics Toric', 'others', 'Daily Hy Toric'
	union
	select 'Expressions Colors', 'others', 'monthly Colour' -- Expressions Colors / Expressions colours
	union
	select 'Frequency Xcel Toric', 'others', 'Monthly hy Toric'
	union
	select 'FreshLook Colors', 'others', 'Monthly colours' -- FreshLook Colors / Freshlook colours
	union
	select 'Freshlook illuminate', 'others', 'Daily colour'
	union
	select 'Proclear multifocal XR', 'others', 'Monthly Hy Multi'
	union
	select 'Purevision toric', 'others', 'Monthly SiHy Toric') pf

-- PF match
select pf.product_id_magento, pfs.product_family_name
from
		(select distinct product_family_name
		from #pf_segments) pfs
	left join
		Warehouse.prod.dim_product_family_v pf on pfs.product_family_name = pf.product_family_name

select s.product_family_group, pfs.product_family_group_name_old
from
		(select distinct product_family_group_name_old
		from #pf_segments
		where product_family_group_name_old is not null) pfs
	left join
		Landing.map.prod_product_family_group s on pfs.product_family_group_name_old = s.product_family_group

select s.product_family_group, pfs.product_family_group_name_new
from
		(select distinct product_family_group_name_new
		from #pf_segments) pfs
	left join
		Landing.map.prod_product_family_group s on pfs.product_family_group_name_new = s.product_family_group

------------------------------------------------------

drop table #segments

select product_family_group_wrong, product_family_group
into #segments
from
	(select 'Daily colour' product_family_group_wrong, 'Daily Col' product_family_group
	union
	select 'Daily Hy Toric', 'Daily HY Tor'
	union
	select 'Daily SiHy sph', 'Daily SIHI Sph'
	union
	select 'monthly Colour', 'Monthly Col'
	union
	select 'Monthly colours', 'Monthly Col'
	union
	select 'Monthly hy Toric', 'Monthly HY Tor' 
	union
	select 'Monthly SiHY Multi', 'Monthly SIHI Multi'
	union
	select 'Monthly SiHy Sph', 'Monthly SIHI Sph'
	union
	select 'Monthly SiHy Toric', 'Monthly SIHI Tor') t


select pf.product_id_magento, pfs.product_family_name, pf.product_family_group_name,
	pfs.product_family_group_name_old, -- pfs.product_family_group_name_new, s.product_family_group,
	isnull(s.product_family_group, pfs.product_family_group_name_new) product_family_group
from 
		#pf_segments pfs
	inner join
		Warehouse.prod.dim_product_family_v pf on pfs.product_family_name = pf.product_family_name 
	left join
		#segments s on pfs.product_family_group_name_new = s.product_family_group_wrong
	inner join
		Landing.map.prod_product_family_group s2 on isnull(s.product_family_group, pfs.product_family_group_name_new) = s2.product_family_group
-- where pf.product_family_group_name <> isnull(s.product_family_group, pfs.product_family_group_name_new) 
-- order by pf.product_id_magento
order by pfs.product_family_name

-- Monthly HY Multi
-- Monthly Hy Multi

select *
from Landing.aux.mag_prod_product_family_v
where product_id_bk = 1233

select *
from Staging.prod.dim_product_family
where product_id_bk = 1233