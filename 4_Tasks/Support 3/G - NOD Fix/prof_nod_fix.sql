
select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk
into #nod_fix
from Warehouse.nod.fact_customer_signature_nod
where idCustomerSignatureNOD_sk not in 
	(select idCustomerSignatureNOD_sk_fk
	from Warehouse.nod.fact_customer_signature_nod_score)

------------------------------------------------

	-- Mean Values Temp Table
	select 
			avg(case when (stdev_order_freq_time > 20) then 20 else stdev_order_freq_time end) as stdev_order_freq_time,
			avg(case when (create_time_mm > 0) then log10(create_time_mm) else 0 end) as create_time_mm_LB10,
			avg(case when (customer_status_name = 'NEW') then 1 else 0 end) as customer_status_name_NEW,
			avg(case when (customer_status_name = 'REGULAR') then 1 else 0 end) as customer_status_name_REGULAR, 
			avg(case when rank_num_tot_orders='++' then 1 
				when rank_num_tot_orders='01' then 2 
				when rank_num_tot_orders='02' then 3
				when rank_num_tot_orders='03' then 4
				when rank_num_tot_orders='05' then 5
				when rank_num_tot_orders='08' then 6
				when rank_num_tot_orders='13' then 7
				else 0 
			end) as rank_num_tot_orders,
			avg(case when (num_tot_orders > 0.975 and num_tot_orders <= 2.92) then 1 else 0 end) as num_tot_orders__0Pt975_2Pt92,
			avg(case when (rank_avg_order_qty_time = '01') then 1 else 0 end) as rank_avg_order_qty_time_01, 
			avg(case when (avg_order_qty_time > 48) then 48 else avg_order_qty_time end) as avg_order_qty_time,
			avg(case when (avg_order_qty_time > 0) then case when (avg_order_qty_time > 48) then log10(48) else log10(avg_order_qty_time) end else 0 end) as avg_order_qty_time_LB10,
			avg(case when (rank_avg_order_freq_time = '01') then 1 else 0 end) as rank_avg_order_freq_time_01, 
			avg(case when (rank_avg_order_freq_time = '03') then 1 else 0 end) as rank_avg_order_freq_time_03, 
			avg(case when (rank_avg_order_freq_time = '04') then 1 else 0 end) as rank_avg_order_freq_time_04, 
			avg(avg_order_freq_time) avg_order_freq_time, 
			avg(case when (all_history_product_count > 25) then 25 else all_history_product_count end) as all_history_product_count,
			avg(case when (spend_CLDaily > 0) then case when spend_CLDaily <= 3000 then log10(spend_CLDaily) else log10(3000) end else 0 end) as spend_CLDaily_LB10,
			avg(case when (orders_CLDaily > 30) then 30 else orders_CLDaily end) as orders_CLDaily,
			avg(order_est_consumption_time) order_est_consumption_time, 
			avg(case when (consumption_index_all_orders > 12) then 12 else consumption_index_all_orders end) as consumption_index_all_orders,
			avg(consumption_index_lenses_only) consumption_index_lenses_only, 
			avg(consumption_index_all_orders_N) consumption_index_all_orders_N, avg(consumption_index_lenses_only_N) consumption_index_lenses_only_N, 
			avg(est_nmtno_all_orders) est_nmtno_all_orders, avg(est_nmtno_lenses_only) est_nmtno_lenses_only
	into #fact_customer_signature_nod_score_prep_means
	from Warehouse.nod.fact_customer_signature_nod_v

	-- Important attributes for scoring
	select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
		rank_stdev_order_freq_time, stdev_order_freq_time stdev_order_freq_time_orig,
			case when (stdev_order_freq_time > 20) then 20 else stdev_order_freq_time end as stdev_order_freq_time,
		create_time_mm, 
			case when (create_time_mm > 0) then log10(create_time_mm) else 0 end as create_time_mm_LB10,
		customer_status_name, 
			case when (customer_status_name = 'NEW') then 1 else 0 end as customer_status_name_NEW,
			case when (customer_status_name = 'REGULAR') then 1 else 0 end as customer_status_name_REGULAR, 
		rank_num_tot_orders rank_num_tot_orders_orig, 
			case when rank_num_tot_orders='++' then 1 
				when rank_num_tot_orders='01' then 2 
				when rank_num_tot_orders='02' then 3
				when rank_num_tot_orders='03' then 4
				when rank_num_tot_orders='05' then 5
				when rank_num_tot_orders='08' then 6
				when rank_num_tot_orders='13' then 7
				else 0 
			end as rank_num_tot_orders,
		num_tot_orders, 
			case when (num_tot_orders > 0.975 and num_tot_orders <= 2.92) then 1 else 0 end as num_tot_orders__0Pt975_2Pt92,
		rank_avg_order_qty_time, 
		
			case when (rank_avg_order_qty_time = '01') then 1 else 0 end as rank_avg_order_qty_time_01, 
		avg_order_qty_time avg_order_qty_time_orig, 
			case when (avg_order_qty_time > 48) then 48 else avg_order_qty_time end as avg_order_qty_time,
			case when (avg_order_qty_time > 0) then case when (avg_order_qty_time > 48) then log10(48) else log10(avg_order_qty_time) end else 0 end as avg_order_qty_time_LB10,
		rank_avg_order_freq_time, 
			case when (rank_avg_order_freq_time = '01') then 1 else 0 end as rank_avg_order_freq_time_01, 
			case when (rank_avg_order_freq_time = '03') then 1 else 0 end as rank_avg_order_freq_time_03, 
			case when (rank_avg_order_freq_time = '04') then 1 else 0 end as rank_avg_order_freq_time_04, 
		avg_order_freq_time, 
		all_history_product_count all_history_product_count_orig, 
			case when (all_history_product_count > 25) then 25 else all_history_product_count end as all_history_product_count,
		spend_CLDaily, 
			case when (spend_CLDaily > 0) then case when spend_CLDaily <= 3000 then log10(spend_CLDaily) else log10(3000) end else 0 end as spend_CLDaily_LB10,
			case when (orders_CLDaily > 30) then 30 else orders_CLDaily end as orders_CLDaily,
		order_est_consumption_time, 
		consumption_index_all_orders consumption_index_all_orders_orig, 
					case when (consumption_index_all_orders > 12) then 12 else consumption_index_all_orders end as consumption_index_all_orders,
		consumption_index_lenses_only, 
		consumption_index_all_orders_N, consumption_index_lenses_only_N, 
		est_nmtno_all_orders, est_nmtno_lenses_only
	into #fact_customer_signature_nod_score_prep
	from Warehouse.nod.fact_customer_signature_nod_v
	where idCustomerSignatureNOD_sk in (select idCustomerSignatureNOD_sk from #nod_fix)



	-- Replace NULL values with Mean Values
	update trg
	set 
		trg.stdev_order_freq_time = isnull(trg.stdev_order_freq_time, src.stdev_order_freq_time),
		trg.create_time_mm_LB10 = isnull(trg.create_time_mm_LB10, src.create_time_mm_LB10),
		trg.customer_status_name_NEW = isnull(trg.customer_status_name_NEW, src.customer_status_name_NEW),
		trg.customer_status_name_REGULAR = isnull(trg.customer_status_name_REGULAR, src.customer_status_name_REGULAR),
		trg.rank_num_tot_orders = isnull(trg.rank_num_tot_orders, src.rank_num_tot_orders),
		trg.num_tot_orders__0Pt975_2Pt92 = isnull(trg.num_tot_orders__0Pt975_2Pt92, src.num_tot_orders__0Pt975_2Pt92),
		trg.rank_avg_order_qty_time_01 = isnull(trg.rank_avg_order_qty_time_01, src.rank_avg_order_qty_time_01),
		trg.avg_order_qty_time = isnull(trg.avg_order_qty_time, src.avg_order_qty_time),
		trg.avg_order_qty_time_LB10 = isnull(trg.avg_order_qty_time_LB10, src.avg_order_qty_time_LB10),
		trg.rank_avg_order_freq_time_01 = isnull(trg.rank_avg_order_freq_time_01, src.rank_avg_order_freq_time_01),
		trg.rank_avg_order_freq_time_03 = isnull(trg.rank_avg_order_freq_time_03, src.rank_avg_order_freq_time_03),
		trg.rank_avg_order_freq_time_04 = isnull(trg.rank_avg_order_freq_time_04, src.rank_avg_order_freq_time_04),
		trg.avg_order_freq_time = isnull(trg.avg_order_freq_time, src.avg_order_freq_time),
		trg.all_history_product_count = isnull(trg.all_history_product_count, src.all_history_product_count),
		trg.spend_CLDaily_LB10 = isnull(trg.spend_CLDaily_LB10, src.spend_CLDaily_LB10),
		trg.orders_CLDaily = isnull(trg.orders_CLDaily, src.orders_CLDaily),
		trg.order_est_consumption_time = isnull(trg.order_est_consumption_time, src.order_est_consumption_time),
		trg.consumption_index_all_orders = isnull(trg.consumption_index_all_orders, src.consumption_index_all_orders),
		trg.consumption_index_lenses_only = isnull(trg.consumption_index_lenses_only, src.consumption_index_lenses_only),
		trg.consumption_index_all_orders_N = isnull(trg.consumption_index_all_orders_N, src.consumption_index_all_orders_N),
		trg.consumption_index_lenses_only_N = isnull(trg.consumption_index_lenses_only_N, src.consumption_index_lenses_only_N),
		trg.est_nmtno_all_orders = isnull(trg.est_nmtno_all_orders, src.est_nmtno_all_orders),
		trg.est_nmtno_lenses_only = isnull(trg.est_nmtno_lenses_only, src.est_nmtno_lenses_only)
	from
		#fact_customer_signature_nod_score_prep trg, 
			#fact_customer_signature_nod_score_prep_means src


	-- Model 1
		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			rank_stdev_order_freq_time, num_tot_orders,
			create_time_mm_LB10, (create_time_mm_LB10 - 1.74300247533994)/0.201608317096454 create_time_mm_LB10_z,
			rank_avg_order_qty_time_01,  (rank_avg_order_qty_time_01 - 0.324355228052344)/0.468148305838813 rank_avg_order_qty_time_01_z,
			avg_order_qty_time, (avg_order_qty_time - 2.90439588362343)/2.28933046441053 avg_order_qty_time_z,
			rank_avg_order_freq_time_01, (rank_avg_order_freq_time_01 - 0.156650997331978)/0.363482950345861 rank_avg_order_freq_time_01_z,
			rank_avg_order_freq_time_03, (rank_avg_order_freq_time_03 - 0.272455850590776)/0.44523729953222 rank_avg_order_freq_time_03_z,
			avg_order_freq_time, (avg_order_freq_time - 99.2806504891373)/64.1409591792995 avg_order_freq_time_z, 
			consumption_index_all_orders, (consumption_index_all_orders - 1.32445838902486)/0.985752022086512 consumption_index_all_orders_z, 
			consumption_index_all_orders_N, (consumption_index_all_orders_N - 10.401917415149)/10.144923893941 consumption_index_all_orders_N_z,
			est_nmtno_lenses_only, (est_nmtno_lenses_only - 3.7999754202418)/3.04091835540693 est_nmtno_lenses_only_z
		into #fact_customer_signature_nod_score_prep_m1
		from #fact_customer_signature_nod_score_prep
		
		update #fact_customer_signature_nod_score_prep_m1
		set 
			create_time_mm_LB10_z = case when (create_time_mm_LB10_z < -3) then -3 when (create_time_mm_LB10_z > 3) then 3 else create_time_mm_LB10_z end, 
			rank_avg_order_qty_time_01_z = case when (rank_avg_order_qty_time_01_z < -3) then -3 when (rank_avg_order_qty_time_01_z > 3) then 3 else rank_avg_order_qty_time_01_z end, 
			avg_order_qty_time_z = case when (avg_order_qty_time_z < -3) then -3 when (avg_order_qty_time_z > 3) then 3 else avg_order_qty_time_z end, 
			rank_avg_order_freq_time_01_z = case when (rank_avg_order_freq_time_01_z < -3) then -3 when (rank_avg_order_freq_time_01_z > 3) then 3 else rank_avg_order_freq_time_01_z end, 
			rank_avg_order_freq_time_03_z = case when (rank_avg_order_freq_time_03_z < -3) then -3 when (rank_avg_order_freq_time_03_z > 3) then 3 else rank_avg_order_freq_time_03_z end, 
			avg_order_freq_time_z = case when (avg_order_freq_time_z < -3) then -3 when (avg_order_freq_time_z > 3) then 3 else avg_order_freq_time_z end, 
			consumption_index_all_orders_z = case when (consumption_index_all_orders_z < -3) then -3 when (consumption_index_all_orders_z > 3) then 3 else consumption_index_all_orders_z end, 
			consumption_index_all_orders_N_z = case when (consumption_index_all_orders_N_z < -3) then -3 when (consumption_index_all_orders_N_z > 3) then 3 else consumption_index_all_orders_N_z end, 
			est_nmtno_lenses_only_z = case when (est_nmtno_lenses_only_z < -3) then -3 when (est_nmtno_lenses_only_z > 3) then 3 else est_nmtno_lenses_only_z end

		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			exp(1.412441 + (create_time_mm_LB10_z * 0.055099) 
				+ (rank_avg_order_qty_time_01_z * -0.030561) + (avg_order_qty_time_z * 0.064499) 
				+ (rank_avg_order_freq_time_01_z * -0.041444) + (rank_avg_order_freq_time_03_z * 0.015333) + (avg_order_freq_time_z * 0.200598)  
				+ (consumption_index_all_orders_z * 0.040130) + (consumption_index_all_orders_N_z *-0.086669) + (est_nmtno_lenses_only_z * 0.139277) 
				) - 1 prediction_next_order_date_month
		into #fact_customer_signature_nod_score_m1
		from #fact_customer_signature_nod_score_prep_m1
		where rank_stdev_order_freq_time in ('00','01') and num_tot_orders > 3

	-- Model 2
		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			rank_stdev_order_freq_time, num_tot_orders,
			customer_status_name_REGULAR, (customer_status_name_REGULAR - 0.820066466213008)/0.384138162166286 customer_status_name_REGULAR_z,
			avg_order_freq_time, (avg_order_freq_time - 242.602278841589)/144.049002400607 avg_order_freq_time_z,
			rank_avg_order_freq_time_04, (rank_avg_order_freq_time_04 - 0.120620351321412)/0.325690711390103 rank_avg_order_freq_time_04_z,
			all_history_product_count, (all_history_product_count - 11.8770691565121)/6.83209709337425 all_history_product_count_z,
			spend_CLDaily_LB10, (spend_CLDaily_LB10 - 1.31163903768732)/1.29057781428574 spend_CLDaily_LB10_z,
			order_est_consumption_time, (order_est_consumption_time - 4.55758822598512)/3.97450238999145 order_est_consumption_time_z,
			consumption_index_lenses_only, (consumption_index_lenses_only - 2.86686740323016)/3.01359032246973 consumption_index_lenses_only_z,
			consumption_index_lenses_only_N, (consumption_index_lenses_only_N - 6.22604825627422)/5.2608812988849 consumption_index_lenses_only_N_z,
			est_nmtno_all_orders, (est_nmtno_all_orders - 9.53017476087989)/13.4947675889012 est_nmtno_all_orders_z			
		into #fact_customer_signature_nod_score_prep_m2
		from #fact_customer_signature_nod_score_prep

		update #fact_customer_signature_nod_score_prep_m2
		set 
			customer_status_name_REGULAR_z = case when (customer_status_name_REGULAR_z < -3) then -3 when (customer_status_name_REGULAR_z > 3) then 3 else customer_status_name_REGULAR_z end, 
			avg_order_freq_time_z = case when (avg_order_freq_time_z < -3) then -3 when (avg_order_freq_time_z > 3) then 3 else avg_order_freq_time_z end, 
			rank_avg_order_freq_time_04_z = case when (rank_avg_order_freq_time_04_z < -3) then -3 when (rank_avg_order_freq_time_04_z > 3) then 3 else rank_avg_order_freq_time_04_z end, 
			all_history_product_count_z = case when (all_history_product_count_z < -3) then -3 when (all_history_product_count_z > 3) then 3 else all_history_product_count_z end, 
			spend_CLDaily_LB10_z = case when (spend_CLDaily_LB10_z < -3) then -3 when (spend_CLDaily_LB10_z > 3) then 3 else spend_CLDaily_LB10_z end, 
			order_est_consumption_time_z = case when (order_est_consumption_time_z < -3) then -3 when (order_est_consumption_time_z > 3) then 3 else order_est_consumption_time_z end, 
			consumption_index_lenses_only_z = case when (consumption_index_lenses_only_z < -3) then -3 when (consumption_index_lenses_only_z > 3) then 3 else consumption_index_lenses_only_z end, 
			consumption_index_lenses_only_N_z = case when (consumption_index_lenses_only_N_z < -3) then -3 when (consumption_index_lenses_only_N_z > 3) then 3 else consumption_index_lenses_only_N_z end, 
			est_nmtno_all_orders_z = case when (est_nmtno_all_orders_z < -3) then -3 when (est_nmtno_all_orders_z > 3) then 3 else est_nmtno_all_orders_z end

		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			exp(1.929354 + (customer_status_name_REGULAR_z * -0.099935) 
				+ (avg_order_freq_time_z * 0.138277) + (rank_avg_order_freq_time_04_z * -0.009888) 
				+ (all_history_product_count_z * -0.018939) + (spend_CLDaily_LB10_z * -0.011061) 
				+ (order_est_consumption_time_z * 0.287166) + (consumption_index_lenses_only_z * 0.11109) + (consumption_index_lenses_only_N_z * -0.070875) 
				+ (est_nmtno_all_orders_z * -0.061164) 
				) - 1 prediction_next_order_date_month
		into #fact_customer_signature_nod_score_m2
		from #fact_customer_signature_nod_score_prep_m2
		where rank_stdev_order_freq_time not in ('00','01') and num_tot_orders > 3

	-- Model 3
		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			rank_stdev_order_freq_time, num_tot_orders,
			create_time_mm_LB10, (create_time_mm_LB10 - 1.51062279137679)/0.197119458645437 create_time_mm_LB10_z,
			customer_status_name_REGULAR, (customer_status_name_REGULAR - 0.2568875)/0.436919556185669 customer_status_name_REGULAR_z,
			avg_order_qty_time_LB10, (avg_order_qty_time_LB10 - 0.438425380055767)/0.345341490183518 avg_order_qty_time_LB10_z,
			avg_order_freq_time, (avg_order_freq_time - 118.450725)/284.674347132866 avg_order_freq_time_z,
			spend_CLDaily_LB10, (spend_CLDaily_LB10 - 0.802903804656752)/0.886566377365845 spend_CLDaily_LB10_z,
			order_est_consumption_time, (order_est_consumption_time - 3.8187625)/3.96793866828983 order_est_consumption_time_z,
			consumption_index_lenses_only, (consumption_index_lenses_only - 3.74159498213808)/4.2605881978683 consumption_index_lenses_only_z,
			est_nmtno_all_orders, (est_nmtno_all_orders - 11.3809203086565)/22.6821122188993 est_nmtno_all_orders_z,
			est_nmtno_lenses_only, (est_nmtno_lenses_only - 10.3690773808797)/20.5663391644962 est_nmtno_lenses_only_z
		into #fact_customer_signature_nod_score_prep_m3
		from #fact_customer_signature_nod_score_prep

		update #fact_customer_signature_nod_score_prep_m3
		set 
			create_time_mm_LB10_z = case when (create_time_mm_LB10_z < -3) then -3 when (create_time_mm_LB10_z > 3) then 3 else create_time_mm_LB10_z end, 
			customer_status_name_REGULAR_z = case when (customer_status_name_REGULAR_z < -3) then -3 when (customer_status_name_REGULAR_z > 3) then 3 else customer_status_name_REGULAR_z end, 			
			avg_order_qty_time_LB10_z = case when (avg_order_qty_time_LB10_z < -3) then -3 when (avg_order_qty_time_LB10_z > 3) then 3 else avg_order_qty_time_LB10_z end, 
			avg_order_freq_time_z = case when (avg_order_freq_time_z < -3) then -3 when (avg_order_freq_time_z > 3) then 3 else avg_order_freq_time_z end, 
			spend_CLDaily_LB10_z = case when (spend_CLDaily_LB10_z < -3) then -3 when (spend_CLDaily_LB10_z > 3) then 3 else spend_CLDaily_LB10_z end, 
			order_est_consumption_time_z = case when (order_est_consumption_time_z < -3) then -3 when (order_est_consumption_time_z > 3) then 3 else order_est_consumption_time_z end, 
			consumption_index_lenses_only_z = case when (consumption_index_lenses_only_z < -3) then -3 when (consumption_index_lenses_only_z > 3) then 3 else consumption_index_lenses_only_z end, 
			est_nmtno_all_orders_z = case when (est_nmtno_all_orders_z < -3) then -3 when (est_nmtno_all_orders_z > 3) then 3 else est_nmtno_all_orders_z end,
			est_nmtno_lenses_only_z = case when (est_nmtno_lenses_only_z < -3) then -3 when (est_nmtno_lenses_only_z > 3) then 3 else est_nmtno_lenses_only_z end

		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			exp(2.015350 + (create_time_mm_LB10_z * 0.150561) + (customer_status_name_REGULAR_z * -0.119672) 
				+ (avg_order_qty_time_LB10_z * 0.042120) + (avg_order_freq_time_z * -0.006086) 
				+ (spend_CLDaily_LB10_z * -0.010927) 
				+ (order_est_consumption_time_z * 0.163802) + (consumption_index_lenses_only_z * 0.116999) 
				+ (est_nmtno_all_orders_z * -0.072343) + (est_nmtno_lenses_only_z * 0.041846) 
				) - 1 prediction_next_order_date_month
		into #fact_customer_signature_nod_score_m3
		from #fact_customer_signature_nod_score_prep_m3
		where num_tot_orders <= 3


	-- Model 5
		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			rank_stdev_order_freq_time, num_tot_orders,
			
			create_time_mm_LB10, (create_time_mm_LB10 - 1.61843564335897)/0.254685351547377 create_time_mm_LB10_z,
			customer_status_name_NEW, (customer_status_name_NEW - 0.45815)/0.498248613290602 customer_status_name_NEW_z,
			rank_num_tot_orders, (rank_num_tot_orders - 3.190113)/1.700979 rank_num_tot_orders_z,
			num_tot_orders__0Pt975_2Pt92, (num_tot_orders__0Pt975_2Pt92 - 0.6080125)/0.488196967470342 num_tot_orders__0Pt975_2Pt92_z,
			consumption_index_all_orders_N, (consumption_index_all_orders_N - 5.64014712892512)/5.03907918149691 consumption_index_all_orders_N_z,
			est_nmtno_all_orders, (est_nmtno_all_orders - 10.1913304072111)/17.7525708020706 est_nmtno_all_orders_z

		into #fact_customer_signature_nod_score_prep_m5
		from #fact_customer_signature_nod_score_prep

		update #fact_customer_signature_nod_score_prep_m5
		set 
			create_time_mm_LB10_z = case when (create_time_mm_LB10_z < -3) then -3 when (create_time_mm_LB10_z > 3) then 3 else create_time_mm_LB10_z end, 
			customer_status_name_NEW_z = case when (customer_status_name_NEW_z < -3) then -3 when (customer_status_name_NEW_z > 3) then 3 else customer_status_name_NEW_z end, 			
			rank_num_tot_orders_z = case when (rank_num_tot_orders_z < -3) then -3 when (rank_num_tot_orders_z > 3) then 3 else rank_num_tot_orders_z end, 
			num_tot_orders__0Pt975_2Pt92_z = case when (num_tot_orders__0Pt975_2Pt92_z < -3) then -3 when (num_tot_orders__0Pt975_2Pt92_z > 3) then 3 else num_tot_orders__0Pt975_2Pt92_z end, 
			consumption_index_all_orders_N_z = case when (consumption_index_all_orders_N_z < -3) then -3 when (consumption_index_all_orders_N_z > 3) then 3 else consumption_index_all_orders_N_z end, 
			est_nmtno_all_orders_z = case when (est_nmtno_all_orders_z < -3) then -3 when (est_nmtno_all_orders_z > 3) then 3 else est_nmtno_all_orders_z end 

		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			1 / (1 + exp(-(0.116859 + (create_time_mm_LB10_z * -0.692795) + (customer_status_name_NEW_z * -0.932611) 
				+ (rank_num_tot_orders_z * 0.130753) + (num_tot_orders__0Pt975_2Pt92_z * -0.258725) 
				+ (consumption_index_all_orders_N_z * 0.783715) + (est_nmtno_all_orders * -0.416232)))) prediction_renewal
		into #fact_customer_signature_nod_score_m5
		from #fact_customer_signature_nod_score_prep_m5



	-- INSERT STATEMENT
	insert into Warehouse.nod.fact_customer_signature_nod_score (
		idCustomerSignatureNOD_sk_fk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
		idOrderHeaderNext_sk_fk, 
		model_no_renewal, model_no_next_order_date, 
		magento_reminder_date, 
		prediction_renewal, prediction_next_order_date_month, prediction_next_order_date, 
		real_renewal, real_next_order_date, 
		idETLBatchRun_ins)

		select m5.idCustomerSignatureNOD_sk idCustomerSignatureNOD_sk_fk, m5.idCustomer_sk_fk, m5.idOrderHeader_sk_fk, m5.order_date, 
			null idOrderHeaderNext_sk_fk, 
			5 model_no_renewal, m.model_no_next_order_date, 
			convert(date, oh.reminder_date) magento_reminder_date, 
			m5.prediction_renewal, m.prediction_next_order_date_month, dateadd(dd, convert(int, m.prediction_next_order_date_month * 30), m5.order_date) prediction_next_order_date, 
			null real_renewal, null real_next_order_date, 
			1 idETLBatchRun_ins
		from 
				#fact_customer_signature_nod_score_m5 m5
			left join
				(select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, prediction_next_order_date_month, 1 model_no_next_order_date
				from #fact_customer_signature_nod_score_m1
				union
				select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, prediction_next_order_date_month, 2 model_no_next_order_date
				from #fact_customer_signature_nod_score_m2
				union
				select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, prediction_next_order_date_month, 3 model_no_next_order_date
				from #fact_customer_signature_nod_score_m3) m on m5.idCustomerSignatureNOD_sk = m.idCustomerSignatureNOD_sk
			inner join
				Warehouse.sales.dim_order_header oh on m5.idOrderHeader_sk_fk = oh.idOrderHeader_sk


		drop table #fact_customer_signature_nod_score_prep_means
		drop table #fact_customer_signature_nod_score_prep

		drop table #fact_customer_signature_nod_score_prep_m1
		drop table #fact_customer_signature_nod_score_m1

		drop table #fact_customer_signature_nod_score_prep_m2
		drop table #fact_customer_signature_nod_score_m2

		drop table #fact_customer_signature_nod_score_prep_m3
		drop table #fact_customer_signature_nod_score_m3

		drop table #fact_customer_signature_nod_score_prep_m5
		drop table #fact_customer_signature_nod_score_m5

-----------------------------------------------------------------