
select top 1000 *
from Landing.mend.comp_currency_aud

select top 1000 *
from Landing.mend.comp_spotrate_aud
where id in (126663739519847721, 126663739519847717)


select top 1000 currencyid, spotrateid, currencycode, effectivedate, nexteffectivedate, effectivedate_d, nexteffectivedate_d, historicalrate
from Landing.mend.gen_comp_currency_v
-- where nexteffectivedate_d = CAST(GETDATE() AS DATE)
where currencycode = 'EUR'
order by currencycode, effectivedate desc, nexteffectivedate desc

select top 1000 *
from Landing.mend.gen_comp_currency_exchange_v
where nexteffectivedate = CAST(GETDATE() AS DATE) and unique_f = 'Y'
-- where currencycode_from = 'GBP' and currencycode_to = 'EUR'
	-- and effectivedate = nexteffectivedate
order by currencycode_from, currencycode_to, effectivedate desc, nexteffectivedate desc

	select top 1000 count(*)
	from Landing.mend.gen_comp_currency_exchange_v

	select nexteffectivedate, count(*)
	from Landing.mend.gen_comp_currency_exchange_v
	where currencycode_from = 'GBP' and currencycode_to = 'EUR'
	group by nexteffectivedate
	order by nexteffectivedate desc

------------------------------------------------------------------

		select EUR gbp_to_eur_rate, USD gbp_to_usd_rate, SEK gbp_to_sek_rate
		from
				(select currencycode_to, convert(decimal(12, 4), exchange_rate) exchange_rate
				from Landing.mend.gen_comp_currency_exchange_v
				where currencycode_from = 'GBP' and currencycode_to in ('EUR', 'USD', 'SEK') and nexteffectivedate = CAST(GETDATE() AS DATE)) ce
			pivot
				(avg(exchange_rate)
				for currencycode_to in (EUR, USD, SEK)) pvt

		select USD eur_to_usd_rate, SEK eur_to_sek_rate
		from
				(select currencycode_to, convert(decimal(12, 4), exchange_rate) exchange_rate
				from Landing.mend.gen_comp_currency_exchange_v
				where currencycode_from = 'EUR' and currencycode_to in ('USD', 'SEK') and nexteffectivedate = CAST(GETDATE() AS DATE)) ce
			pivot
				(avg(exchange_rate)
				for currencycode_to in (USD, SEK)) pvt

------------------------------------------------------------------

select *, 
	count(*) over (partition by currencycode_from, currencycode_to, nexteffectivedate) num_rep, 
	dense_rank() over (partition by currencycode_from, currencycode_to, nexteffectivedate order by effectivedate, exchange_rate) ord_rep,
	case when (count(*) over (partition by currencycode_from, currencycode_to, nexteffectivedate) 
		= dense_rank() over (partition by currencycode_from, currencycode_to, nexteffectivedate order by effectivedate, exchange_rate)) then 'Y' else 'N' end unique_f
	-- row_number() over (order by (select null))
from Landing.mend.gen_comp_currency_exchange_v
where currencycode_from = 'GBP' and currencycode_to = 'EUR'
	and unique_f = 'Y'
order by currencycode_from, currencycode_to, effectivedate desc, nexteffectivedate desc

	select currencycode_from, currencycode_to, nexteffectivedate, count(*)
	from Landing.mend.gen_comp_currency_exchange_v
	where currencycode_from = 'GBP' and currencycode_to = 'EUR'
	group by currencycode_from, currencycode_to, nexteffectivedate
	order by currencycode_from, currencycode_to, nexteffectivedate desc
