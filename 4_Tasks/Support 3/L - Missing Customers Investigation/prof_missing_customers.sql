
select *
from Warehouse.gen.dim_customer
where customer_id_bk in (2882625, 2881548, 2881547, 2881546)

select top 1000 *
from Landing.aux.gen_dim_customer_aud
where customer_id_bk = 2881548

select top 1000 *
from Landing.mag.customer_entity_flat_aud
where entity_id in 	(2882625, 2881548, 2881547, 2881546)

select top 1000 *
from Landing.mag.customer_entity_aud
where entity_id in 	(2882625, 2881548, 2881547, 2881546)

select top 1000 count(*)
from Landing.mag.customer_entity_aud

	drop table #customer

	select entity_id, email, created_at, updated_at
	into #customer
	from openquery(MAGENTO, 
			'select entity_id, email, created_at, updated_at
			from magento01.customer_entity
			order by entity_id desc
			limit 10000') 

	select c1.*
	from 
			#customer c1
		left join
			Landing.mag.customer_entity_aud c2 on c1.entity_id = c2.entity_id	
	where c2.entity_id is null 
		-- and c1.entity_id = 2881548
	order by c1.entity_id desc

select top 1000 *
from Landing.mag.customer_entity
order by entity_id desc
