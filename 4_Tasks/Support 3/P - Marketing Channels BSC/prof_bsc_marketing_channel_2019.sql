
drop table #orders_bsc

select order_id_bk, order_no, order_date, 
	ga_medium, ga_source, 
	affilCode, coupon_id_bk, coupon_code, 
	channel_name_bk, marketing_channel_name_bk
into #orders_bsc
from Landing.aux.sales_dim_order_header_aud
where order_date between '2019-01-01' and '2019-10-01'

select year(order_date), month(order_date), count(*) 
from #orders_bsc
group by year(order_date), month(order_date)
order by year(order_date), month(order_date)

select year(order_date) yyyy, month(order_date) mm, affilCode, count(*) num_oh, -- sum(count(*)) over (partition by year(order_date), month(order_date)), 
	 convert(decimal(12, 4), count(*)) * 100 / sum(count(*)) over (partition by year(order_date), month(order_date)) perc_oh
from #orders_bsc
group by year(order_date), month(order_date), affilCode
order by affilCode, year(order_date), month(order_date)

----------------------------

select top 1000 oh1.*, oh2.customer_order_seq_no_gen
from 
		#orders_bsc oh1
	inner join
		Warehouse.sales.dim_order_header_v oh2 on oh1.order_id_bk = oh2.order_id_bk


select year(oh2.invoice_date) yyyy, month(oh2.invoice_date) mm, affilCode, count(*) num_oh, -- sum(count(*)) over (partition by year(order_date), month(order_date)), 
	 convert(decimal(12, 4), count(*)) * 100 / sum(count(*)) over (partition by year(oh2.invoice_date), month(oh2.invoice_date)) perc_oh
from 
		#orders_bsc oh1
	inner join
		Warehouse.sales.dim_order_header_v oh2 on oh1.order_id_bk = oh2.order_id_bk
where oh2.invoice_date is not null
	and oh2.customer_order_seq_no_gen = 1
group by year(oh2.invoice_date), month(oh2.invoice_date), affilCode
order by affilCode, year(oh2.invoice_date), month(oh2.invoice_date)