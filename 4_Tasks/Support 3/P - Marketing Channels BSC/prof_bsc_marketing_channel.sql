
drop table #orders_bsc

select order_id_bk, order_no, order_date, 
	ga_medium, ga_source, 
	affilCode, coupon_id_bk, coupon_code, 
	channel_name_bk, marketing_channel_name_bk
into #orders_bsc
from Landing.aux.sales_dim_order_header_aud
where order_date between '2019-09-01' and '2019-10-01'

select *
from #orders_bsc

select affilCode, count(*)
from #orders_bsc
group by affilCode
order by affilCode

select affilCode, marketing_channel_name_bk, count(*)
from #orders_bsc
where affilCode is not null
group by affilCode, marketing_channel_name_bk
order by affilCode, marketing_channel_name_bk

select *
from Landing.map.sales_ch_affil_code_mch

select *
from Landing.map.sales_ch_affil_code_like_mch

	select oh.affilCode, oh.marketing_channel_name_bk marketing_channel_name_tableau, oh.num num_rows, -- ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name, 
		coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) marketing_channel_name_logic, 
		case when (coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) <> oh.marketing_channel_name_bk) then 'N' else 'Y' end match_f
	from
			(select affilCode, marketing_channel_name_bk, count(*) num
			from #orders_bsc
			where affilCode is not null
			group by affilCode, marketing_channel_name_bk) oh
		left join
			Landing.map.sales_ch_affil_code_mch ac on oh.affilCode = ac.affil_code
		left join
			Landing.map.sales_ch_affil_code_like_mch acl on oh.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
		left join
			Landing.map.sales_ch_affil_code_like_mch acl2 on oh.affilCode like acl2.affil_code_like + '%' and acl2.affil_code_like <> 'adwords-br'
	-- where coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) <> oh.marketing_channel_name_bk
		-- and oh.marketing_channel_name_bk not in ('Refer a Friend', 'Social Media', 'Super Affiliates')
	order by affilCode, marketing_channel_name_bk

	select oh.affilCode, oh.marketing_channel_name_bk marketing_channel_name_tableau, oh.num num_rows, oh.coupon_code,
		-- ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name, 
		coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) marketing_channel_name_logic, 
		case when (coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) <> oh.marketing_channel_name_bk) then 'N' else 'Y' end match_f
	from
			(select affilCode, marketing_channel_name_bk, coupon_code, count(*) num
			from #orders_bsc
			where affilCode is not null
			group by affilCode, marketing_channel_name_bk, coupon_code) oh
		left join
			Landing.map.sales_ch_affil_code_mch ac on oh.affilCode = ac.affil_code
		left join
			Landing.map.sales_ch_affil_code_like_mch acl on oh.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
		left join
			Landing.map.sales_ch_affil_code_like_mch acl2 on oh.affilCode like acl2.affil_code_like + '%' and acl2.affil_code_like <> 'adwords-br'
	where coalesce(ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name) <> oh.marketing_channel_name_bk
		-- and oh.marketing_channel_name_bk not in ('Refer a Friend', 'Social Media', 'Super Affiliates')
	order by affilCode, marketing_channel_name_bk