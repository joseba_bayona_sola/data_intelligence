
select top 1000 order_line_id_bk, invoice_date, 
	product_family_name, pack_size,
	qty_pack, qty_unit,
	local_price_pack, local_price_unit, 
	global_subtotal, global_shipping, global_discount, global_store_credit_used, 
	global_total_exc_vat, local_to_global_rate
from Warehouse.sales.fact_order_line_v
where order_line_id_bk in (19212749, 19421759)		

-- new local_price_pack: 21 (convert to global depending on market with exchange rate)
-- calculate new global_price_unit (depending on pack_size)
-- calculate new global_subtotal (depending on qty_unit)
-- calculate new global_subtotal_exc_vat (dependin on vat_percent)
-- calculate new global_total_exc_vat (with shipping, discount, store credit all in exc vat)

-- Questions: 
	-- Measures just in Local / Also in Global
	-- Which Measures: Subtotal vs Total Exc VAT (Sh, Disc, SC + VAT calc) // Margin ??
	-- Refunds ?? 