
drop table #oh

select idOrderHeader_sk, order_id_bk, website, order_no, invoice_no, order_date, invoice_date, order_status_magento_name, payment_method_name
into #oh
from Warehouse.sales.dim_order_header_v
where (order_date > '2019-09-23' and order_date < '2019-09-24') or 
	(invoice_date > '2019-09-23' and invoice_date < '2019-09-24')
order by order_date

select oh.order_id_bk, oh.website, 
	oh.order_no, oh.invoice_no, oh.order_date, oh.invoice_date, ohe.order_date_sync, 
	datediff(minute, oh.order_date, ohe.order_date_sync) diff_od,
	datediff(minute, oh.invoice_date, ohe.order_date_sync) diff_id,
	oh.order_status_magento_name, oh.payment_method_name 
from 
		#oh oh
	left join
		Warehouse.alloc.dim_order_header_erp ohe on oh.order_id_bk = ohe.order_id_bk
order by oh.order_date
-- order by diff_id desc
