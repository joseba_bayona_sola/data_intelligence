
select -- count(*) over (), old_customer_id, 
	new_magento_id customer_id, email, 
	domainName acquired_website, old_customer_lifecycle, vd_customer_lifecycle, 
	-- cs.customer_id, cs.customer_email, 
	cs.website_group_create, cs.website_create, cs.customer_status_name current_customer_status_name, cs.num_tot_orders -- num_tot_orders is not the gen one: no taking refunds
from 
		Landing.migra_ladlex.migrate_customerdata_aud c
	inner join
		Warehouse.act.dim_customer_signature_v cs on c.new_magento_id = cs.customer_id
order by domainName, website_create, num_tot_orders
