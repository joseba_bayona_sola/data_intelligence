" SELECT   " +
        
        " coalesce(si.SKU, '')                              AS col01 , " +
        " coalesce(si.StockItemDescription,'')             AS col02 , " +                
        " coalesce(p.SKU, '')                             AS col03 , " +
        " coalesce(p.Description,'')                     AS col04 , " +                
        " coalesce(pf.MagentoProductID,'')                  AS col05 , " +
        " coalesce(pf.ManufacturerProductFamily,'')     AS col06 , " +
        " coalesce(pc.Name,'')                               AS col07 , " +
        " ''                                                 AS col08 , " +
        " coalesce(p.ProductType,'')                      AS col09 , " +
        " ''                                                AS col10 , " +
        " 'All_Products'                                  AS c0l11   " +
        
        " FROM  Product.Barcode AS bc " +
        " LEFT OUTER JOIN bc/Product.Barcode_StockItem/Product.StockItem AS si  " +
       " LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize AS ps " +
       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product AS p "   +
       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +
       " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily AS pf " +
       " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory AS pc " +
       " WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')";