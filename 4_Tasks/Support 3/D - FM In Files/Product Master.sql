//              " SELECT DISTINCT " +
               " SELECT  " +

                       " coalesce(si.SKU,'')                          AS col01 , " +
                       " coalesce(si.StockItemDescription,'')         AS col02 , " +
                       " ''                                           AS col03 , " +
                       " ''                                           AS col04 , " +
                       " ''                                           AS col05 , " +
                       " ''                                           AS col06 , " +
                       " ''                                           AS col07 , " +
                       " 'Lenses'                                     AS col08 , " +
                       " coalesce(si.PackSize,0)                      AS col09 , " +     // PackSize.size - Alex Y. new changes
                       " 'Packs'                                      AS col10 , " +
                       " coalesce(ps.PacksPerCarton,1)                AS col11 , " +     // Akex Y.  ps.PacksPerCarton
                       " 'Cartons'                                    AS col12 , " +
                       " ''                                           AS col13 , " +     // Calculated field
                       " 'Pallets'                                    AS col14 , " +
                       " ''                                           AS col15 , " +
//                      " coalesce(si.PackSize,0)                      AS col16 , " +     // PackSize.size same as col09


                       " coalesce(cl.PO, '')                          AS col17 , " +
                       " coalesce(cl.DI, '')                          AS col18 , " +
                       " coalesce(cl.BC, '')                          AS col19 , " +
                       " coalesce(cl.AX, '')                          AS col20 , " +
                       " coalesce(cl.AD, '')                          AS col21 , " +
                       " coalesce(cl._DO,'')                          AS col22 , " +
                       " coalesce(cl.CO, '')                          AS col23 , " +
                       " coalesce(cl.CY, '')                          AS col24 , " +

//                        "  ''                                           AS col17 , " +
//                        "  ''                                           AS col18 , " +
//                        "  ''                                           AS col19 , " +
//                        "  ''                                           AS col20 , " +
//                        "  ''                                           AS col21 , " +
//                        "  ''                                           AS col22 , " +
//                        "  ''                                           AS col23 , " +
//                        "  ''                                           AS col24 , " +


                       " coalesce(bc.UPCCode,'')                      AS col25 , " +
                       " coalesce(pf.Status,'')                       AS col26 , " +
                       " ps.PurchasingPreferenceOrder                 AS col27 , " +
                       " coalesce(pf.Manufacturer, '')                AS col28 , " +
                       " ''                                           AS col29 , " +
                       " ''                                           AS col30 , " +
                       " ''                                           AS col31 , " +
                       " ''                                           AS col32 , " +
                       " ''                                           AS col33 , " +

//                      " coalesce(ps.PacksPerCarton,0)                AS PacksPerCarton , " +
                       " coalesce(ps.PacksPerPallet,0)                AS PacksPerPallet " +


                        " FROM  Product.Barcode AS bc " +
                        " LEFT OUTER JOIN bc/Product.Barcode_StockItem/Product.StockItem AS si  " +

" LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize AS ps " +
                       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product AS p "   +
                       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +  // remed to include solutions
                       " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily AS pf " +
                       " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory AS pc " +
                       " WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')";