
drop table #ie_orders

select order_id_bk, order_no, order_date, 
	ga_medium, ga_source, 
	affilCode, coupon_id_bk, coupon_code, 
	channel_name_bk, marketing_channel_name_bk
into #ie_orders
from Landing.aux.sales_dim_order_header_aud
where store_id_bk = 21
	and order_date > '2019-01-01'

select count(*)
from #ie_orders
	
select affilCode, count(*)
from #ie_orders
group by affilCode
order by affilCode

select affilCode, marketing_channel_name_bk, count(*)
from #ie_orders
group by affilCode, marketing_channel_name_bk
order by affilCode, marketing_channel_name_bk

------------------------------------

select *
from Landing.map.sales_ch_affil_code_mch

select *
from Landing.map.sales_ch_affil_code_like_mch

select oh.affilCode, ac.marketing_channel_name, acl.marketing_channel_name, acl2.marketing_channel_name, 
	oh.num
from 
		(select affilCode, count(*) num
		from #ie_orders
		group by affilCode) oh 
	left join
		Landing.map.sales_ch_affil_code_mch ac on oh.affilCode = ac.affil_code
	left join
		Landing.map.sales_ch_affil_code_like_mch acl on oh.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
	left join
		Landing.map.sales_ch_affil_code_like_mch acl2 on oh.affilCode like acl2.affil_code_like + '%' and acl2.affil_code_like <> 'adwords-br'
order by oh.affilCode


select *
from Landing.map.sales_ch_affil_code_mch
order by affil_code, marketing_channel_name

select *
from Landing.map.sales_ch_affil_code_like_mch
