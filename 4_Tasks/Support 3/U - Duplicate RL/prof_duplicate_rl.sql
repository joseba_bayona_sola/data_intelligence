
select top 1000 *
from Warehouse.po.dim_purchase_order
where purchase_order_number = 'P00074953'

select top 1000 *
from Warehouse.po.fact_purchase_order_line
where idPurchaseOrder_sk_fk = 68653

select top 1000 *
from Warehouse.rec.dim_receipt
where receipt_number = 'TP00074953-0'

select top 1000 *
from Warehouse.rec.fact_receipt_line
where idReceipt_sk_fk = 119686

select *
from Warehouse.prod.dim_product_family_price_v
-- where idProductFamilyPrice_sk = 8570
where supplier_name = 'Winston USD' and product_id_magento = '2403'

select *
from Warehouse.prod.dim_product_family_price
where unit_price in (12.510234, 12.569285)

select *
from Warehouse.prod.dim_product_family_price_v
where gbp_unit_price in (12.510234, 12.569285)


	select top 1000 *
	from Warehouse.prod.dim_product_family_price
	where idProductFamilyPrice_sk = 8570

	select top 1000 *
	from Warehouse.prod.dim_product_family_price_v
	where idProductFamilyPrice_sk = 8570

	select count(*)
	from Warehouse.prod.dim_product_family_price

	select count(*)
	from Warehouse.prod.dim_product_family_price_v

--------------------------------------------------------

	select recl.idproductfamilyprice_sk_fk,
		recl.receiptlineid_bk, pol.purchaseorderlineid_bk, irl.invoicelineid_bk,
		recl.warehouse_name, recl.supplier_type_name, recl.supplier_name, -- pfp.supplier_type_name supplier_type_name_price, pfp.supplier_name supplier_name_price, 
		recl.wh_shipment_type_name, recl.receipt_status_name, recl.shipment_number, recl.receipt_number, recl.supplier_advice_ref, 
		recl.created_date, recl.arrived_date, recl.confirmed_date, recl.stock_registered_date, recl.due_date, 
		case 
			when recl.receipt_status_name in ('Stock_Registered') then recl.stock_registered_date
			when recl.receipt_status_name in ('Arrived') then recl.arrived_date
			else recl.due_date
		end receipt_date,

		-- pfp.price_type, pfp.price_status, pfp.effective_date, pfp.expiry_date, pfp.unit_price, pfp.currency_code AS Unit_price_Currency_code, pfp.gbp_unit_price, pfp.eur_unit_price, pfp.usd_unit_price,
		-- pfp.gbp_to_eur_rate, pfp.gbp_to_usd_rate, pfp.gbp_to_sek_rate, pfp.eur_to_usd_rate, pfp.eur_to_sek_rate, pfp.york_pass, pfp.ams_pass, pfp.gir_pass,


		pol.po_source_name, pol.po_type_name, pol.po_status_name, 
		pol.purchase_order_number, pol.supplier_reference, pol.pass_num,
		pol.created_by, pol.assigned_to, 
		pol.created_date created_date_po, pol.approved_date approved_date_po, pol.confirmed_date confirmed_date_po, 
		pol.submitted_date submitted_date_po, pol.completed_date completed_date_po, pol.due_date due_date_po, -- pol.received_date, 
		
		it_spo.purchase_order_number_t, 
		it_tpo.order_no_erp order_no_iso_tpo, it_spo.order_no_erp order_no_iso_spo, it_tpo.wholesale_customer_name tpo_customer_name, it_spo.wholesale_customer_name spo_customer_name,
		irl.invrec_status_name, irl.created_by invoice_rec_by, irl.invoice_ref, irl.net_suite_no, 
		irl.invoice_date, irl.approved_date, irl.posted_date, irl.payment_due_date, 

		-- isnull(recl.manufacturer_name, pfp.manufacturer_name) manufacturer_name, isnull(recl.product_type_name, pfp.product_type_name) product_type_name, 
		-- isnull(recl.category_name, pfp.category_name) category_name, isnull(recl.product_family_group_name, pfp.product_family_group_name) product_family_group_name,
		-- isnull(recl.product_id_magento, pfp.product_id_magento) product_id_magento, isnull(recl.product_family_name, pfp.product_family_name) product_family_name, isnull(recl.packsize, pfp.size) packsize,
		recl.product_description, recl.stock_item_description, recl.SKU,

		ssd.abc, ssd.stockable, ssd.stocked, ssd.final,

		recl.receipt_line_sync_status_name, 
		-- count(*) over (partition by pol.purchaseorderlineid_bk) num_pol_rep, 
		dense_rank() over (partition by pol.purchaseorderlineid_bk order by recl.receiptlineid_bk, irl.invoicelineid_bk) ord_pol_rep,
		-- irl.num_rl_rep,
		 
		isnull(pol.qty_ordered, 0) qty_ordered,
		-- recl.qty_ordered, 
		isnull(case when (irl.invoicelineid_bk is not null) then irl.quantity else recl.qty_received end, 0) qty_received, 
		isnull(case when (irl.invoicelineid_bk is not null) then irl.quantity else recl.qty_accepted end, 0) qty_accepted, 
		case 
			when recl.receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then recl.qty_ordered
			when recl.receipt_status_name in ('Received') then recl.qty_received
			else 0 -- Stock_Registered
		end as qty_open, 0 qty_planned,

		isnull(recl.purchase_invoice_unit_cost, recl.purchase_unit_cost) purchase_unit_cost, -- recl.purchase_total_cost, recl.purchase_total_cost_accepted, 
		isnull(recl.local_invoice_unit_cost, recl.local_unit_cost) local_unit_cost, -- recl.local_total_cost, recl.local_total_cost_accepted, 
		isnull(recl.global_invoice_unit_cost, recl.global_unit_cost) global_unit_cost, -- recl.global_total_cost, recl.global_total_cost_accepted, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then isnull (recl.purchase_invoice_unit_cost, recl.purchase_unit_cost) - (isnull (recl.purchase_invoice_unit_cost, recl.purchase_unit_cost) * recl.discount_amount) 
			else isnull (recl.purchase_invoice_unit_cost, recl.purchase_unit_cost)
		end purchase_unit_cost_discount, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then isnull (recl.local_invoice_unit_cost, recl.local_unit_cost) - (isnull (recl.local_invoice_unit_cost, recl.local_unit_cost) * recl.discount_amount) 
			else isnull (recl.local_invoice_unit_cost, recl.local_unit_cost)
		end local_unit_cost_discount, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then isnull (recl.global_invoice_unit_cost, recl.global_unit_cost) - (isnull (recl.global_invoice_unit_cost, recl.global_unit_cost) * recl.discount_amount) 
			else isnull (recl.global_invoice_unit_cost, recl.global_unit_cost)
		end global_unit_cost_discount, 

		irl.adjusted_unit_cost purchase_adjusted_unit_cost, irl.local_adjusted_unit_cost, irl.global_adjusted_unit_cost, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then irl.adjusted_unit_cost - (irl.adjusted_unit_cost * recl.discount_amount)
			else irl.adjusted_unit_cost
		end purchase_adjusted_unit_cost_discount, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then irl.local_adjusted_unit_cost - (irl.local_adjusted_unit_cost * recl.discount_amount)
			else irl.local_adjusted_unit_cost
		end local_adjusted_unit_cost_discount, 
		case when (recl.discount_amount is not null and recl.stock_registered_date between recl.discount_date_from and recl.discount_date_to) 
			then irl.global_adjusted_unit_cost - (irl.global_adjusted_unit_cost * recl.discount_amount)
			else irl.global_adjusted_unit_cost
		end global_adjusted_unit_cost_discount, 

		irl.adjustment_applied, irl.local_adjustment_applied, irl.global_adjustment_applied, 

		recl.local_to_wh_rate, recl.local_to_global_rate, 
		recl.currency_code, recl.currency_code_wh
from 
		Warehouse.rec.fact_receipt_line_v recl
	left join
		Warehouse.po.fact_purchase_order_line_v pol on recl.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
	full join
		Warehouse.prod.dim_product_family_price_v pfp ON pfp.idProductFamilyPrice_sk = recl.idproductfamilyprice_sk_fk
	left join
		Warehouse.po.dim_intersite_transfer_v it_tpo on pol.idPurchaseOrder_sk = it_tpo.idPurchaseOrderT_sk_fk
	left join
		Warehouse.po.dim_intersite_transfer_v it_spo on pol.idPurchaseOrder_sk = it_spo.idPurchaseOrderS_sk_fk
	left join
		(select count(*) over (partition by idReceiptLine_sk_fk) num_rl_rep, invoicelineid_bk, idReceiptLine_sk_fk, 
			invrec_status_name, created_by, invoice_ref, net_suite_no, 
			invoice_date, approved_date, posted_date, payment_due_date, 
	
			quantity, 

			adjusted_unit_cost, adjusted_total_cost, adjustment_applied/quantity adjustment_applied,
			local_adjusted_unit_cost, local_adjusted_total_cost, local_adjustment_applied/quantity local_adjustment_applied,
			global_adjusted_unit_cost, global_adjusted_total_cost, global_adjustment_applied/quantity global_adjustment_applied
		from Warehouse.invrec.fact_invoice_reconciliation_line_v) irl on recl.idReceiptLine_sk = irl.idReceiptLine_sk_fk
	left join
		Warehouse.fm.dim_wh_stock_item_static_data_v ssd on recl.idWarehouse_sk = ssd.idWarehouse_sk and recl.idStockItem_sk = ssd.idStockItem_sk
where recl.idReceipt_sk = 119686
	-- pol.purchaseorderlineid_bk = 4503599636527858
	-- and recl.sku = '02403B3D3BQ0000060000301' 