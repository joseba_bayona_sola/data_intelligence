	
	
	select top 1000 entity_id, email, website_id, store_id, created_in
	from Landing.mag.customer_entity_flat_aud
	where entity_id = 2096401

drop table #oh_customer

select oh.entity_id, oh.store_id, s.website_id, oh.created_at, oh.customer_id, oh2.payment_method_name_bk
into #oh_customer
from
	(select entity_id, store_id, created_at, customer_id
	from
		(select entity_id, store_id, created_at, customer_id, 
			count(*) over (partition by customer_id) num_rep, 
			dense_rank() over (partition by customer_id order by entity_id) ord_rep
		from Landing.mag.sales_flat_order_aud
		where status <> 'archived') oh 
	where num_rep = ord_rep) oh
left join
	Landing.mag.core_store s on oh.store_id = s.store_id
left join
	Landing.aux.sales_dim_order_header_aud oh2 on oh.entity_id = oh2.order_id_bk

	select count(*), count(distinct customer_id)
	from #oh_customer

	select website_id, store_id, count(*), count(distinct customer_id)
	from #oh_customer
	group by website_id, store_id
	order by website_id, store_id

	select top 1000	count(*) over (),
		c.entity_id, c.email, 
		c.website_id, c.store_id, s1.name, c.created_in, 
		oh.entity_id, oh.website_id, oh.store_id, s2.name, oh.created_at, oh.payment_method_name_bk
	
	select c.entity_id customer_id, c.email, c.store_id store_id_customer, oh.entity_id order_id, oh.store_id store_id_last_order, oh.created_at last_order_date, oh.payment_method_name_bk
	from 
			Landing.mag.customer_entity_flat_aud c
		left join
			#oh_customer oh on c.entity_id = oh.customer_id
		inner join
			Landing.mag.core_store s1 on c.store_id = s1.store_id
		left join
			Landing.mag.core_store s2 on oh.store_id = s2.store_id
	-- where oh.customer_id is not null and c.store_id <> oh.store_id
	-- where c.website_id = 28 and c.store_id = 29
	-- where c.website_id = 0 and oh.store_id = 23
	-- where c.website_id = 22 and oh.website_id = 28

	where oh.customer_id is not null and c.store_id <> oh.store_id and oh.store_id >= 20 -- and oh.store_id not in (22, 28)
	-- order by c.store_id, s1.name, oh.store_id, s2.name 
	order by oh.created_at desc

	-- website
	select c.website_id website_id_cust, w.name website_name_cust, oh.website_id website_id_oh, w2.name store_name_oh, count(*) num -- c.website_id, c.created_in, 
	from 
			Landing.mag.customer_entity_flat_aud c
		left join
			#oh_customer oh on c.entity_id = oh.customer_id
		inner join
			(select website_id, min(left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end)) name
			from Landing.mag.core_store
			group by website_id) w on c.website_id = w.website_id
		left join
			(select website_id, min(left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end)) name
			from Landing.mag.core_store
			group by website_id) w2 on oh.website_id = w2.website_id
	where oh.customer_id is not null and c.website_id = oh.website_id -- and oh.store_id < 20
	-- where oh.customer_id is null -- and c.website_id <> oh.website_id
	group by c.website_id, w.name, oh.website_id, w2.name -- c.website_id, c.created_in, 
	order by c.website_id, w.name, oh.website_id, w2.name -- c.website_id, c.created_in, 

	-- store
	select c.store_id store_id_cust, s1.name store_name_cust, oh.store_id store_id_oh, s2.name store_name_oh, count(*) num -- c.website_id, c.created_in, 
	from 
			Landing.mag.customer_entity_flat_aud c
		left join
			#oh_customer oh on c.entity_id = oh.customer_id
		inner join
			Landing.mag.core_store s1 on c.store_id = s1.store_id
		left join
			Landing.mag.core_store s2 on oh.store_id = s2.store_id
	where oh.customer_id is not null and c.store_id <> oh.store_id and oh.store_id >= 20 -->=
	-- where oh.customer_id is null -- and c.store_id <> oh.store_id
	group by c.store_id, s1.name, oh.store_id, s2.name -- c.website_id, c.created_in, 
	order by c.store_id, s1.name, oh.store_id, s2.name -- c.website_id, c.created_in, 

----------------------------------------------------

		select c.store_id, s.name, c.num
		from
				(select store_id, count(*) num
				from Landing.mag.customer_entity_flat_aud
				group by store_id) c
			inner join
				Landing.mag.core_store s on c.store_id = s.store_id
		order by c.store_id

		select oh.store_id, s.store_name, oh.num
		from
				(select store_id_bk store_id, count(*) num
				from Landing.aux.sales_dim_order_header_aud
				group by store_id_bk ) oh
			inner join
				Warehouse.gen.dim_store s on oh.store_id = s.store_id_bk
		order by oh.store_id
