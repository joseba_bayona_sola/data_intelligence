
-- Customer belonging to website

	-- Magento source table
	select top 1000 entity_id, email, website_id, store_id, created_in
	from Landing.mag.customer_entity_flat_aud
	where entity_id = 2096401

		select w.website_id, w.name, c.num
		from
				(select website_id, count(*) num
				from Landing.mag.customer_entity_flat_aud
				group by website_id) c
			inner join
				(select website_id, min(left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end)) name
				from Landing.mag.core_store
				group by website_id) w on c.website_id = w.website_id
		order by c.website_id

		select c.store_id, s.name, c.num
		from
				(select store_id, count(*) num
				from Landing.mag.customer_entity_flat_aud
				group by store_id) c
			inner join
				Landing.mag.core_store s on c.store_id = s.store_id
		order by c.store_id

			select c.store_id, s.name, w.website_id, w.name, c.num
			from
					(select website_id, store_id, count(*) num
					from Landing.mag.customer_entity_flat_aud
					group by website_id, store_id) c
				inner join
					(select website_id, min(left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end)) name
					from Landing.mag.core_store
					group by website_id) w on c.website_id = w.website_id
				inner join
					Landing.mag.core_store s on c.store_id = s.store_id
			order by c.store_id, c.website_id

		-- 

		select created_in, count(*)
		from Landing.mag.customer_entity_flat_aud
		group by created_in
		order by created_in

			select c.store_id, s.name, w.website_id, w.name, c.created_in, c.num
			from
					(select created_in, website_id, store_id, count(*) num
					from Landing.mag.customer_entity_flat_aud
					group by created_in, website_id, store_id) c
				inner join
					(select website_id, min(left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end)) name
					from Landing.mag.core_store
					group by website_id) w on c.website_id = w.website_id
				inner join
					Landing.mag.core_store s on c.store_id = s.store_id
			order by c.store_id, c.website_id, c.created_in

	-- Warehouse.gen.dim_customer
		-- idStore_sk_fk: customer_entity_flat_aud.store_id

	select top 1000 idCustomer_sk, customer_id_bk, customer_email, idStore_sk_fk, store_id_bk, store_name
	from Warehouse.gen.dim_customer_v
		
		select store_id_bk, store_name, count(*)
		from Warehouse.gen.dim_customer_v
		group by store_id_bk, store_name
		order by store_id_bk, store_name



	-- Warehouse.act.fact_customer_signature
		-- idStoreCreate_sk_fk (First Website): Big logic based on orders, old_access_cust_no, migrations
		-- idStoreRegister_sk_fk: Based on customer_entity_flat_aud.store_id + map.act_website_register_mapping_aud

	select top 1000 idCustomer_sk_fk, idStoreCreate_sk_fk, idStoreRegister_sk_fk, idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk
	from Warehouse.act.fact_customer_signature

		-- Source for SP for fact_customer_signature
		select top 1000 customer_id_bk, idStoreCreate_sk_fk, idStoreRegister_sk_fk
		from Warehouse.act.dim_customer_cr_reg
		
			select wrm.store_id, wrm.store_id_register, s1.name, s2.name
			from 
					Landing.map.act_website_register_mapping_aud wrm
				inner join
					Landing.mag.core_store s1 on wrm.store_id = s1.store_id
				inner join
					Landing.mag.core_store s2 on wrm.store_id_register = s2.store_id
			order by wrm.store_id, store_id_register

			select *
			from Landing.map.act_website_migrate_date_aud

		-- Used in Customer Single View DS
		select top 1000 idCustomer_sk_fk, customer_id, customer_email, 
			website_create, store_create, website_register, store_register, store_first, store_last
		from Warehouse.act.fact_customer_signature_v

		-- Used in Sales DS
		select top 1000 idCustomer_sk_fk, customer_id, customer_email, 
			website_create, website_register
		from Warehouse.act.dim_customer_signature_v

	-- SF MIG view
		-- store_id: customer_entity_flat_aud.store_id
		-- created_in: customer_entity_flat_aud.created_in
		-- EON_Preferred_Site__c: Based on customer_entity_flat_aud.store_id + Landing.map.gen_store_sf_site_map_aud
		-- EON_Preferred_Locale__c: Based on customer_entity_flat_aud.store_id + Landing.map.gen_store_sf_site_map_aud
		-- EON_Migrated_From_Site__c: act.fact_customer_signature.idStoreCreate_sk_fk - website_create

	select top 1000 entity_id, email, store_id, created_in, EON_Preferred_Site__c, EON_Preferred_Locale__c, EON_Migrated_From_Site__c
	from Landing.sf_mig.customer_v

		select *
		from Landing.map.gen_store_sf_site_map_aud
		order by store_id_bk, store_name

---------------------------------------------------------------------

	-- 2096401: ramon.martens@gmail.com

	select -- top 1000 count(*) over (), 
		oh.order_id_bk, oh.order_no, oh.order_date, oh.invoice_date, 
		-- oh.idStore_sk_fk, 
		s.store_id_bk, s.store_name,
		-- oh.idCustomer_sk_fk, 
		c.customer_id_bk, c.customer_email, c.store_id_bk store_id_bk_cust, c.store_name store_name_cust
	into #oh_customers_ie
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			Warehouse.gen.dim_store s on oh.idStore_sk_fk = s.idStore_sk
		inner join	
			Warehouse.gen.dim_customer_v c on oh.idCustomer_sk_fk = c.idCustomer_sk
	where oh.order_date > '2018-01-01' -- 2020-01-01
		and c.store_id_bk = 21
		and s.store_id_bk <> 21
	order by c.customer_id_bk, oh.order_date

		select *
		from #oh_customers_ie
		order by customer_id_bk, order_date

		select count(*), count(distinct customer_id_bk)
		from #oh_customers_ie

		select year(order_date), count(*), count(distinct customer_id_bk)
		from #oh_customers_ie
		group by year(order_date)
		order by year(order_date)

		select store_id_bk_cust, store_id_bk, count(*)
		from #oh_customers_ie
		group by store_id_bk_cust, store_id_bk
		order by store_id_bk_cust, store_id_bk

	select top 1000 *
	from Warehouse.sales.dim_order_header_v
	where customer_id = 2096401
	order by order_date
