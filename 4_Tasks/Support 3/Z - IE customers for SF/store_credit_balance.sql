
select top 1000 count(*) over (), *
from Landing.mag.enterprise_customerbalance_aud
where amount <> 0
	--and amount < 0
	and website_id = 39
order by balance_id desc

	select count(*), count(distinct customer_id)
	from Landing.mag.enterprise_customerbalance_aud
	where amount <> 0

	select ecb.website_id, cs.website_name, cs.store_id, ssm.sf_site,
		isnull(c1.value, c2.value) currency, ecb.num_rows, ecb.num_customers
	from
			(select website_id, count(*) num_rows, count(distinct customer_id) num_customers
			from Landing.mag.enterprise_customerbalance_aud
			where amount <> 0
			group by website_id) ecb
		left join
			(select website_id, min(store_id) store_id, min(name) website_name
			from Landing.mag.core_store
			group by website_id) cs on ecb.website_id = cs.website_id
		left join
			Landing.map.gen_store_sf_site_map_aud ssm on cs.store_id = ssm.store_id_bk
		left join
			(select scope_id, value
			from Landing.mag.core_config_data
			where path like '%currency/options/base%') c1 on ecb.website_id = c1.scope_id, 
			(select scope_id, value from Landing.mag.core_config_data where path like '%currency/options/base%' and scope_id = 0) c2 
	order by ecb.website_id

select *
from Landing.mag.core_store

select top 1000 count(*) over (), count(*) over (partition by cb.customer_id) num_rep,
	cb.balance_id, 
	cb.customer_id, c.email, c.store_id, ssm.sf_site,
	cb.website_id, isnull(c1.value, c2.value) currency, cb.amount,
	sum(cb.amount) over (partition by cb.customer_id, isnull(c1.value, c2.value)) total_balance, 
	dense_rank() over (partition by cb.customer_id, isnull(c1.value, c2.value) order by cb.amount desc, cb.website_id) total_balance_website_id
from 
		Landing.mag.enterprise_customerbalance_aud cb
	inner join
		Landing.mag.customer_entity_flat_aud c on cb.customer_id = c.entity_id
	left join
		(select website_id, min(store_id) store_id, min(name) website_name
		from Landing.mag.core_store
		group by website_id) cs on cb.website_id = cs.website_id
	left join
		Landing.map.gen_store_sf_site_map_aud ssm on cs.store_id = ssm.store_id_bk
	left join
		(select scope_id, value
		from Landing.mag.core_config_data
		where path like '%currency/options/base%') c1 on cb.website_id = c1.scope_id, 
		(select scope_id, value from Landing.mag.core_config_data where path like '%currency/options/base%' and scope_id = 0) c2 
where amount <> 0
	-- and amount < 0
order by num_rep desc, cb.customer_id, cb.balance_id desc


select count(*) over (partition by cb.customer_id) num_rep, *
into #cb_balance_migration
from
	(select 
		cb.balance_id, 
		cb.customer_id, c.email, c.store_id, ssm.sf_site,
		cb.website_id, isnull(c1.value, c2.value) currency, cb.amount,
		sum(cb.amount) over (partition by cb.customer_id, isnull(c1.value, c2.value)) total_balance, 
		dense_rank() over (partition by cb.customer_id, isnull(c1.value, c2.value) order by cb.amount desc, cb.website_id) total_balance_website_id
	from 
			Landing.mag.enterprise_customerbalance_aud cb
		inner join
			Landing.mag.customer_entity_flat_aud c on cb.customer_id = c.entity_id
		left join
			(select website_id, min(store_id) store_id, min(name) website_name
			from Landing.mag.core_store
			group by website_id) cs on cb.website_id = cs.website_id
		left join
			Landing.map.gen_store_sf_site_map_aud ssm on cs.store_id = ssm.store_id_bk
		left join
			(select scope_id, value
			from Landing.mag.core_config_data
			where path like '%currency/options/base%') c1 on cb.website_id = c1.scope_id, 
			(select scope_id, value from Landing.mag.core_config_data where path like '%currency/options/base%' and scope_id = 0) c2 
	where amount <> 0) cb
where cb.total_balance <> 0 and cb.total_balance_website_id = 1
	-- and cb.total_balance < 0
order by num_rep desc, customer_id

	select *
	from #cb_balance_migration
	-- where total_balance < 0
	order by num_rep desc, customer_id

	select currency, store_id, count(*)
	from #cb_balance_migration
	group by currency, store_id
	order by currency, store_id


	select currency, sf_site, count(*)
	from #cb_balance_migration
	group by currency, sf_site
	order by currency, sf_site