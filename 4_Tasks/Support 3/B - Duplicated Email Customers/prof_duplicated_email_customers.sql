
select customer_id_bk, customer_email, created_at, store_name, first_name, last_name
from
	(select count(*) over (partition by customer_email) num_rep, *
	from Warehouse.gen.dim_customer_v) c
where num_rep > 1 
order by customer_email, customer_id_bk