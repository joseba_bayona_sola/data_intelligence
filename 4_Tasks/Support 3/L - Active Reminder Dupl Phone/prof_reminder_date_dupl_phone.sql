drop table #invalid_dates

select distinct reminder_date
into #invalid_dates
from Landing.mag.sales_flat_order_aud
where ISDATE(reminder_date) = 0 and reminder_date is not null

-- 

drop table #rem_cust

select customer_id
into #rem_cust
from
		(select 
			customer_id, COUNT(reminder_mobile) over (partition by customer_id order by customer_id) no_rows, 
			reminder_mobile,
			reminder_date, reminder_presc
		from Landing.mag.sales_flat_order_aud
		where reminder_date not in  (select reminder_date from #invalid_dates)) v
where no_rows > 1 and 
	convert(date, reminder_date, 120) > GETDATE()	 
group by customer_id, no_rows 
having COUNT(distinct(replace(reminder_mobile,' ',''))) > 1

-- 

drop table #rem_orders

select 
	s.customer_id, entity_id, increment_id,
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent
into #rem_orders
from 
		Landing.mag.sales_flat_order_aud s
	inner join 
		#rem_cust c on s.customer_id = c.customer_id
where reminder_date not in (select reminder_date from #invalid_dates)

select customer_id, entity_id, increment_id, reminder_date, reminder_mobile, reminder_type, 
	dense_rank() over (order by customer_id) num_customer, 
	dense_rank() over (partition by customer_id order by entity_id) num_order_by_customer
from #rem_orders 
where CONVERT(date, reminder_date, 120) > GETDATE()
order by customer_id, entity_id
