
select *
from ControlDB.logging.t_etlDashboard_v
where idETLBatchRun = 3688
	and pkg_name = 'lnd_stg_stock_hist_tables'

	select
		b.idETLBatchRun, b.description Batch_Description, b.runStatus Batch_Status, b.startTime Batch_Start, b.finishTime Batch_End, b.duration Batch_Duration,
		
		p.package_name PKG_Name, p.flow_name PKG_Flow_Name, p.runStatus PKG_Status, p.startTime PKG_Start, p.finishTime PKG_End, 
		datediff(minute, p.startTime, p.finishTime) PKG_Duration
	from 
			ControlDB.logging.t_ETLBatchRun_v b
		inner join 
			ControlDB.logging.t_PackageRun_v p on b.idETLBatchRun = p.idETLBatchRun
	where b.startTime > getutcdate() - 365
		and b.idETLBatchRun = 3688

	select
		b.idETLBatchRun, b.description Batch_Description, b.runStatus Batch_Status, b.startTime Batch_Start, b.finishTime Batch_End, b.duration Batch_Duration,
		
		p.package_name PKG_Name, p.flow_name PKG_Flow_Name, p.runStatus PKG_Status, p.startTime PKG_Start, p.finishTime PKG_End, 
		datediff(minute, p.startTime, p.finishTime) PKG_Duration,

		p_sp.sp_name, p_sp.startTime SP_Start, p_sp.finishTime SP_End, p_sp.runStatus SP_Status, p_sp.duration SP_Duration
		
	from 
			ControlDB.logging.t_ETLBatchRun_v b
		inner join 
			ControlDB.logging.t_PackageRun_v p on b.idETLBatchRun = p.idETLBatchRun
		inner join 
			ControlDB.logging.t_SPRun_v p_sp on p.idPackageRun = p_sp.idPackageRun

	where b.startTime > getutcdate() - 365
		and b.idETLBatchRun = 3688
		and p_sp.sp_name = 'lnd_stg_get_gen_wholesale_customer' -- lnd_stg_get_gen_wholesale_customer // lnd_stg_get_aux_stock_wh_stock_item_batch_hist_info

	select
		b.idETLBatchRun, b.description Batch_Description, b.runStatus Batch_Status, b.startTime Batch_Start, b.finishTime Batch_End, b.duration Batch_Duration,
		
		p.package_name PKG_Name, p.flow_name PKG_Flow_Name, p.runStatus PKG_Status, p.startTime PKG_Start, p.finishTime PKG_End, 
		datediff(minute, p.startTime, p.finishTime) PKG_Duration,

		p_sp.sp_name, p_sp.startTime SP_Start, p_sp.finishTime SP_End, p_sp.runStatus SP_Status, p_sp.duration SP_Duration,
		
		l.executionid, l.event SSIS_Event, l.message SP_Message
	from 
			ControlDB.logging.t_ETLBatchRun_v b
		inner join 
			ControlDB.logging.t_PackageRun_v p on b.idETLBatchRun = p.idETLBatchRun
		inner join 
			ControlDB.logging.t_SPRun_v p_sp on p.idPackageRun = p_sp.idPackageRun
		inner join 
			ControlDB.dbo.sysssislog l on p_sp.idExecution = l.executionid
	where b.startTime > getutcdate() - 365
		and b.idETLBatchRun = 3688
		and p_sp.sp_name = 'lnd_stg_get_gen_wholesale_customer' -- lnd_stg_get_gen_wholesale_customer // lnd_stg_get_aux_stock_wh_stock_item_batch_hist_info


