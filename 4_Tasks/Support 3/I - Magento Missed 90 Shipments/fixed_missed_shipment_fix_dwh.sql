
use Landing
go

select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		total_qty, 
		total_weight, 
		shipping_description, 
		despatched_at
into #sales_flat_shipment
from openquery(MAGENTO, 
	'select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		total_qty, 
		total_weight, 
		shipping_description, 
		despatched_at
	from magento01.sales_flat_shipment 
	where created_at > ''2019-06-01''')

select sh1.entity_id, sh1.order_id, sh1.created_at, sh1.despatched_at, sh1.despatched_at shipment_date, 
	CONVERT(INT, (CONVERT(VARCHAR(8), sh1.despatched_at, 112))) idCalendarShipmentDate, 
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, sh1.despatched_at)), 2) + ':' + case when (DATEPART(MINUTE, sh1.despatched_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate
into #sales_flat_shipment_upd
from 
		#sales_flat_shipment sh1
	inner join
		Landing.mag.sales_flat_shipment_aud sh2 on sh1.entity_id = sh2.entity_id
where sh2.despatched_at_dw is null and sh1.despatched_at is not null



-- Update Landing.mag.sales_flat_shipment_aud

	-- Disable Trigger
	disable trigger mag.trg_sales_flat_shipment_aud on mag.sales_flat_shipment_aud
	go

	update trg
	set trg.despatched_at = src.despatched_at, trg.despatched_at_dw = src.despatched_at
	from 
		Landing.mag.sales_flat_shipment_aud trg
	inner join
		#sales_flat_shipment_upd src on trg.entity_id = src.entity_id

	-- Enable Trigger
	enable trigger mag.trg_sales_flat_shipment_aud on mag.sales_flat_shipment_aud
	go

-- Update Landing.aux.sales_dim_order_header_aud

	update trg
	set trg.shipment_date = src.shipment_date, trg.idCalendarShipmentDate = src.idCalendarShipmentDate, trg.idTimeShipmentDate = src.idTimeShipmentDate
	from 
		Landing.aux.sales_dim_order_header_aud trg
	inner join
		#sales_flat_shipment_upd src on trg.order_id_bk = src.order_id

-- Update Landing.aux.sales_fact_order_line_aud

	update trg
	set trg.shipment_date = src.shipment_date, trg.idCalendarShipmentDate = src.idCalendarShipmentDate, trg.idTimeShipmentDate = src.idTimeShipmentDate
	from 
		Landing.aux.sales_fact_order_line_aud trg
	inner join
		#sales_flat_shipment_upd src on trg.order_id_bk = src.order_id

-- Update Warehouse.sales.dim_order_header (join with Warehouse.gen.dim_time)
	update trg
	set trg.shipment_date = src.shipment_date, trg.idCalendarShipmentDate_sk_fk = src.idCalendarShipmentDate, trg.idTimeShipmentDate_sk_fk = t.idTime_sk
	from 
		Warehouse.sales.dim_order_header trg
	inner join
		#sales_flat_shipment_upd src on trg.order_id_bk = src.order_id
	inner join
		Warehouse.gen.dim_time t on src.idTimeShipmentDate = t.time_name_bk

-- Update Warehouse.sales.fact_order_line
	update trg
	set trg.shipment_date = src.shipment_date, trg.idCalendarShipmentDate_sk_fk = src.idCalendarShipmentDate, trg.idTimeShipmentDate_sk_fk = t.idTime_sk
	from 
		Warehouse.sales.fact_order_line trg
	inner join
		#sales_flat_shipment_upd src on trg.order_id = src.order_id
	inner join
		Warehouse.gen.dim_time t on src.idTimeShipmentDate = t.time_name_bk

drop table #sales_flat_shipment
drop table #sales_flat_shipment_upd
