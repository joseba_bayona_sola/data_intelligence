
select order_id_bk, order_no, order_date, invoice_date, shipment_date, website, order_status_name, order_status_magento_name, payment_method_name, cc_type_name
into #missing_shipments
from Warehouse.sales.dim_order_header_v
where order_date > '2020-06-01'
	and shipment_date is null
order by order_id_bk

	select top 1000 *
	from #missing_shipments
	where order_status_name = 'OK'
	order by order_id_bk

	select convert(date, order_date), count(*)
	from #missing_shipments
	where order_status_name = 'OK'
	group by convert(date, order_date)
	order by convert(date, order_date)

	select website, count(*)
	from #missing_shipments
	where order_status_name = 'OK'
	group by website
	order by website

	select order_status_name, count(*)
	from #missing_shipments
	group by order_status_name
	order by order_status_name

	select order_status_magento_name, count(*)
	from #missing_shipments
	where order_status_name = 'OK'
	group by order_status_magento_name
	order by order_status_magento_name


-----------------------------------------

select order_id_bk, order_no, order_date, invoice_date, shipment_date, website, order_status_name, order_status_magento_name, payment_method_name, cc_type_name
from Warehouse.sales.dim_order_header_v
where order_id_bk = 11784090
order by order_id_bk

select top 1000 *
from Landing.mend.order_order_aud
where incrementID in ('17000796732', '8007027999')

select top 1000 *
from Landing.mend.ship_customershipment_aud
where orderIncrementID in ('17000796732', '8007027999')


