
drop table #magento_ship

select entity_id, increment_id, order_id, despatched_at,
	created_at, updated_at
from Landing.mag.sales_flat_shipment
order by entity_id desc

select top 1000 count(*) over (),
	entity_id, increment_id, order_id, despatched_at,
	created_at, despatched_at_dw,
	created_at, updated_at
from Landing.mag.sales_flat_shipment_aud
where created_at > '2020-06-01' and despatched_at is null -- 26672
-- where created_at > '2019-05-01' and created_at < '2019-06-12' and despatched_at is null -- 28
order by entity_id desc 

select 
	entity_id, increment_id, order_id, despatched_at,
	created_at_dw, despatched_at_dw,
	created_at, updated_at
into #magento_ship
from Landing.mag.sales_flat_shipment_aud
where created_at > '2019-12-01' and despatched_at is null 
order by entity_id desc 


--- 

select top 1000 id, shipmentNumber, orderIncrementID, dateReadyToShipUTC, dateShippedUTC
from Landing.mend.ship_customershipment

-- select ms.entity_id, cs.id, ms.increment_id, ms.order_id, ms.created_at_dw, cs.dateReadyToShipUTC, ms.despatched_at_dw, cs.dateShippedUTC
select ms.order_id, oh.increment_id order_no, oh.created_at order_date, oh.status, 
	ms.increment_id shipment_no, ms.created_at_dw, cs.dateReadyToShipUTC, ms.despatched_at_dw, cs.dateShippedUTC
from 
		#magento_ship ms
	left join
		Landing.mend.ship_customershipment_aud cs on ms.increment_id = cs.shipmentNumber
	inner join
		Landing.mag.sales_flat_order_aud oh on ms.order_id = oh.entity_id
-- where cs.id is null
where cs.id is not null and cs.dateShippedUTC is not null
order by ms.entity_id 

-- 

select top 1000 idOrderHeader_sk, order_id_bk, shipment_id, order_no, shipment_no, order_date, shipment_date
from Warehouse.sales.dim_order_header

select top 1000 idOrderLine_sk, order_line_id_bk, order_id, idOrderHeader_sk_fk, shipment_id, order_no, shipment_no, shipment_date
from Warehouse.sales.fact_order_line

select ms.entity_id, oh.shipment_id, ms.increment_id, oh.shipment_no, ms.order_id, oh.order_id_bk, oh.idOrderHeader_sk, 
	ms.created_at_dw, ms.despatched_at_dw, 
	oh.idCalendarShipmentDate_sk_fk, oh.idTimeShipmentDate_sk_fk, oh.shipment_date, 
	oh.order_no, oh.order_date
from 
		#magento_ship ms
	left join
		Warehouse.sales.dim_order_header oh on ms.order_id = oh.order_id_bk
-- where ms.entity_id <> oh.shipment_id or ms.increment_id <> oh.shipment_no
order by ms.entity_id

select ms.entity_id, ol.shipment_id, ms.increment_id, ol.shipment_no, ms.order_id, ol.order_id, oh.idOrderHeader_sk, 
	ms.created_at_dw, ms.despatched_at_dw, 
	ol.idCalendarShipmentDate_sk_fk, ol.idTimeShipmentDate_sk_fk, ol.shipment_date, 
	oh.order_no, oh.order_date
from 
		#magento_ship ms
	left join
		Warehouse.sales.dim_order_header oh on ms.order_id = oh.order_id_bk
	left join
		Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
-- where ms.entity_id <> ol.shipment_id 
order by ms.entity_id

--

select ms.entity_id, oh.shipment_id, ms.increment_id, oh.shipment_no, ms.order_id, oh.order_id_bk, 
	ms.created_at_dw, ms.despatched_at_dw, 
	oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
	oh.order_no, oh.order_date
from 
		#magento_ship ms
	left join
		Landing.aux.sales_dim_order_header_aud oh on ms.order_id = oh.order_id_bk
-- where ms.entity_id <> oh.shipment_id or ms.increment_id <> oh.shipment_no
order by ms.entity_id

select ms.entity_id, ol.shipment_id, ms.increment_id, ol.shipment_no, ms.order_id, ol.order_id_bk, 
	ms.created_at_dw, ms.despatched_at_dw, 
	ol.idCalendarShipmentDate, ol.idTimeShipmentDate, ol.shipment_date
from 
		#magento_ship ms
	left join
		Landing.aux.sales_fact_order_line_aud ol on ms.order_id = ol.order_id_bk
-- where ms.entity_id <> ol.shipment_id 
order by ms.entity_id

