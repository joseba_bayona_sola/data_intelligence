
-- 3014220
select top 1000 count(*) over (),
	idCustomer_sk_fk, customer_id, customer_email, customer_status_name, last_order_date, num_tot_orders, num_tot_emails_sent
from Warehouse.act.fact_customer_signature_v

-- 3014220
select top 1000 count(*) over (), 
	idCustomer_sk_fk, client_id, email
from Warehouse.dm.fact_customer_signature_dm

-- 1757398
select top 1000 count(*) over (),
	SF_CUSTOMER_ID, SF_SITE_ID, CLIENT_ID, EMAIL
from Warehouse.dm.SFMC_feed_v

	select 3014220 - 1757398

	select 561821 + 955 + 602560 + 303523

-- Excluded
	-- RNB Customers -- 561821
	select top 1000 count(*) over (), 
		idCustomer_sk_fk, client_id, email
	from Warehouse.dm.fact_customer_signature_dm csdm
	where segment_lifecycle = 'RNB' and (csdm.segment_geog not in ('LAD', 'LEX') or csdm.segment_geog is null)

	-- GDPR Customers - 955
	select customer_id from Warehouse.dm.Dotmailer_GDPR_Customers

	-- DM Unsubscribes -- 602560
	select top 1000 count(*) over (), customer_email
	from
		(select customer_email
		from Warehouse.dm.Dotmailer_Unsubscribed_Customers
		union
		select distinct email
		from Warehouse.dm.Dotmailer_Supressed_Data
		union 
		select customer_email
		from Warehouse.dm.sf_mc_exclude_customers) t

	-- Not Engaged Customers -- 303523
	select top 1000 count(*) over (), customer_email
	from Warehouse.dm.sf_mc_exclude_customers

----------------------------------------------------------------
----------------------------------------------------------------

drop table #sfmc_feed

select SF_CUSTOMER_ID, SF_SITE_ID, CLIENT_ID, EMAIL
into #sfmc_feed
from Warehouse.dm.SFMC_feed_v

select top 1000 count(*) over (),
	cs.idCustomer_sk_fk, cs.customer_id, cs.customer_email, cs.customer_unsubscribe_name_overall, cs.customer_status_name, 
	cs.last_order_date, cs.num_tot_orders, cs.num_tot_emails_sent
from 
		#sfmc_feed mcf
	inner join
		Warehouse.act.fact_customer_signature_v cs on mcf.CLIENT_ID = cs.customer_id
where cs.num_tot_emails_sent = 0 -- 909906

	select cs.customer_unsubscribe_name_overall, cs.customer_status_name, count(*), convert(decimal(12, 2), count(*)) * 100 / sum(count(*)) over ()
	from 
			#sfmc_feed mcf
		inner join
			Warehouse.act.fact_customer_signature_v cs on mcf.CLIENT_ID = cs.customer_id
	where cs.num_tot_emails_sent = 0 -- 909906
	group by cs.customer_unsubscribe_name_overall, cs.customer_status_name
	order by cs.customer_unsubscribe_name_overall, cs.customer_status_name

