

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v
where warehousestockitembatchid_bk = 43910096370179269

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist
where idWHStockItemBatch_sk_fk = 2920898
order by idCalendarStockDate_sk_fk, idInvoiceReconciliationLine_sk_fk

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
where warehousestockitembatchid_bk = 43910096370179269
order by stock_date, idInvoiceReconciliationLine_sk_fk

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_full_v
where warehousestockitembatchid_bk = 43910096370179269
order by stock_date, idInvoiceReconciliationLine_sk_fk

--------------------------------------------------------------------

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v
where warehousestockitembatchid_bk = 43910096370179269

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_full
where warehousestockitembatchid_bk = 43910096370179269

select *
from Landing.aux.mend_stock_batchstockmovement_v
where warehousestockitembatchid_bk = 43910096370179269

select top 1000 *
from Landing.mend.gen_wh_batchstockmovement_v
where warehousestockitembatchid = 43910096370179269

select top 1000 *
from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
where warehousestockitembatchid = 43910096370179269

--------------------------------------------------------------------
	-- Full Stock Report Calculation

		select distinct wsibh.warehousestockitembatchid_bk
		into #t0
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
			inner join
				Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products p on wsibh.magentoProductID_int = p.product_id_magento
		where 
			wsibh.warehousestockitembatchid_bk = 43910096370179269
			and wsibh.update_type not in ('T')

		select wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.batch_stock_register_date, 
			c.calendar_date,
			wsibh.qty_received, 
			wsibh.num_rep, wsibh.ord_rep
		into #t1
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
			inner join
				#t0 wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Landing.aux.dim_calendar
				where calendar_date <= getutcdate()) c on convert(date, wsibh.batch_stock_register_date) <= c.calendar_date -- we use batch_stock_register_date // -2 days cut off (restore DB at 9 pm)
		where wsibh.update_type not in ('T')

		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.invoicelineid_bk is not null) then i.invoicelineid_bk else wsibm.invoicelineid_bk end invoicelineid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2b
		from
				(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep <> 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk and oli.invoicelineid_bk = wsib.invoicelineid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep <> 1 and ord_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date


select *
from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud
where warehousestockitembatchid_bk = 43910096370179269
order by issue_date

	select count(*) over (), *
	from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud
	where qty_stock < 0
	order by warehousestockitembatchid_bk, invoicelineid_bk

select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_issue_v 
where warehousestockitembatchid = 43910096370179269
order by issue_date

------------------------------------------------------------

	-- alloc_fact_order_line_erp_issue_invrec_aud Calculation

	select ord_rep, warehousestockitembatchid_bk, invoicelineid_bk, batch_id,
		qty_received
	-- into #wsib_invrec
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
	where num_rep > 1 and warehousestockitembatchid_bk = 43910096370179269
	order by warehousestockitembatchid_bk, invoicelineid_bk

	select wsibi.batchstockissueid batchstockissueid_bk, 
		wsibi.issueid issue_id, wsibi.issue_date, case when (wsibi.cancelled = 0) then 'N' else 'Y' end cancelled_f, 
		wsibi.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsibi.issuedquantity_issue*-1 qty_stock
	into #wsib_issue
	from 
			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
		inner join
			(select distinct warehousestockitembatchid_bk
			from #wsib_invrec) wsib_ir on wsibi.warehousestockitembatchid = wsib_ir.warehousestockitembatchid_bk
	where wsibi.cancelled = 0


		create table #alloc_fact_order_line_erp_issue_invrec_aud(
			batchstockissueid_bk						bigint NOT NULL, 

			issue_id									bigint, 

			issue_date									datetime,

			cancelled_f									char(1) NOT NULL, 

			warehousestockitembatchid_bk				bigint NOT NULL, 
			invoicelineid_bk							bigint NOT NULL,

			qty_stock									decimal(28, 8)) 


		declare @warehousestockitembatchid_bk bigint, @invoicelineid_bk bigint

		declare db_cursor cursor for
			select warehousestockitembatchid_bk, invoicelineid_bk
			from #wsib_invrec
			order by warehousestockitembatchid_bk, invoicelineid_bk

		open db_cursor
		fetch next from db_cursor into @warehousestockitembatchid_bk, @invoicelineid_bk

		while @@FETCH_STATUS = 0
		begin

			insert into #alloc_fact_order_line_erp_issue_invrec_aud (batchstockissueid_bk, 
				issue_id, issue_date, cancelled_f, 
				warehousestockitembatchid_bk, invoicelineid_bk, 
				qty_stock)

				select batchstockissueid_bk, 
					issue_id, issue_date, cancelled_f, 
					warehousestockitembatchid_bk, invoicelineid_bk, 
					case when (flag_sign  = 'P') then (qty_stock * -1) else qty_hist + (qty_stock * -1) end qty_stock
				from
					(select batchstockissueid_bk,
						issue_id, issue_date, cancelled_f,
						warehousestockitembatchid_bk, invoicelineid_bk, 
						qty_stock, qty_hist,
						flag_sign,
						dense_rank() over (partition by warehousestockitembatchid_bk, invoicelineid_bk, flag_sign order by issue_date, batchstockissueid_bk) flag_sign_num
					from
						(select wsib_ir.ord_rep, wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_ir.batch_id, wsib_ir.qty_received, 
							wsib_i.batchstockissueid_bk, 
							wsib_i.issue_id, wsib_i.issue_date, wsib_i.cancelled_f, 
							wsib_i.qty_stock, 

							wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_i.batchstockissueid_bk, wsib_ir.invoicelineid_bk) qty_hist, 
							case when (wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_i.batchstockissueid_bk, wsib_ir.invoicelineid_bk)) < 0 then 'N' else 'P' end flag_sign 
						from 
								#wsib_invrec wsib_ir
							inner join
								(select t1.batchstockissueid_bk, t1.issue_id, t1.issue_date, t1.cancelled_f, t1.warehousestockitembatchid_bk, 
									t1.qty_stock + isnull(t2.qty_stock, 0) qty_stock
								from 
										#wsib_issue t1
									left join
										(select batchstockissueid_bk, sum(qty_stock) qty_stock
										from #alloc_fact_order_line_erp_issue_invrec_aud
										group by batchstockissueid_bk) t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
								where t1.qty_stock + isnull(t2.qty_stock, 0) <> 0) wsib_i on wsib_ir.warehousestockitembatchid_bk = wsib_i.warehousestockitembatchid_bk
						where wsib_ir.warehousestockitembatchid_bk = @warehousestockitembatchid_bk and invoicelineid_bk = @invoicelineid_bk) t) t
				where flag_sign = 'P' or (flag_sign = 'N' and flag_sign_num = 1 and qty_hist + (qty_stock * -1) <> 0)

			fetch next from db_cursor into @warehousestockitembatchid_bk, @invoicelineid_bk
		end 

		close db_cursor
		deallocate db_cursor

		select *, sum(qty_stock) over (), sum(qty_stock) over (partition by invoicelineid_bk)
		from #alloc_fact_order_line_erp_issue_invrec_aud
		order by warehousestockitembatchid_bk, invoicelineid_bk, issue_date

----------------------------------------------

	delete from #alloc_fact_order_line_erp_issue_invrec_aud

				select batchstockissueid_bk, 
					issue_id, issue_date, cancelled_f, 
					warehousestockitembatchid_bk, invoicelineid_bk, 
					case when (flag_sign  = 'P') then (qty_stock * -1) else qty_hist + (qty_stock * -1) end qty_stock
				from
					(select batchstockissueid_bk,
						issue_id, issue_date, cancelled_f,
						warehousestockitembatchid_bk, invoicelineid_bk, 
						qty_stock, qty_hist,
						flag_sign,
						dense_rank() over (partition by warehousestockitembatchid_bk, invoicelineid_bk, flag_sign order by batchstockissueid_bk, issue_date) flag_sign_num
					from
						(select wsib_ir.ord_rep, wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_ir.batch_id, wsib_ir.qty_received, 
							wsib_i.batchstockissueid_bk, 
							wsib_i.issue_id, wsib_i.issue_date, wsib_i.cancelled_f, 
							wsib_i.qty_stock, 

							wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_i.batchstockissueid_bk, wsib_ir.invoicelineid_bk) qty_hist, 
							case when (wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_i.batchstockissueid_bk, wsib_ir.invoicelineid_bk)) < 0 then 'N' else 'P' end flag_sign 
						from 
								#wsib_invrec wsib_ir
							inner join
								(select t1.batchstockissueid_bk, t1.issue_id, t1.issue_date, t1.cancelled_f, t1.warehousestockitembatchid_bk, 
									t1.qty_stock + isnull(t2.qty_stock, 0) qty_stock
								from 
										#wsib_issue t1
									left join
										(select batchstockissueid_bk, sum(qty_stock) qty_stock
										from #alloc_fact_order_line_erp_issue_invrec_aud
										group by batchstockissueid_bk) t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
								where t1.qty_stock + isnull(t2.qty_stock, 0) <> 0) wsib_i on wsib_ir.warehousestockitembatchid_bk = wsib_i.warehousestockitembatchid_bk
						where wsib_ir.warehousestockitembatchid_bk = 43910096370179269 and invoicelineid_bk = 181832834956137144) t
						-- order by batchstockissueid_bk 
						) t
				where flag_sign = 'P' or (flag_sign = 'N' and flag_sign_num = 1 and qty_hist + (qty_stock * -1) <> 0)
				order by batchstockissueid_bk