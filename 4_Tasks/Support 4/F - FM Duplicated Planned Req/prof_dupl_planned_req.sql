	select count(*) over (partition by w.warehouseid, si.stockitemid, s.supplier_id, fm_reference) num_rep,
		w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, s.supplier_id supplierid_bk, fm_reference,
		null warehousesourceid_bk, 
		release_date, due_date,
		-- CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112)) idCalendarReleaseDate, 
		-- CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112)) idCalendarDueDate, 
		planner, overdue_status,
		pack_size packsize, quantity, unit_cost
	from 
			Landing.fm.fm_planned_requirements pr
		inner join
			Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
		inner join
			(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
			from 
				(select si.stockitemid, si.sku, si.stockitemdescription, 
					isnull(wsi.num_records, 0) num_records,
					count(*) over (partition by si.sku) num_rep, 
					rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
				from 
						Landing.mend.gen_prod_stockitem_v si
					left join
						(select stockitemid, count(*) num_records
						from Landing.mend.gen_wh_warehousestockitem_v
						where wh_stock_item_batch_f = 'Y'
						group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
			where num_rep = ord_rep) si on pr.sku = si.sku
		inner join
			-- Landing.mend.gen_supp_supplier_v s on pr.supplier = s.supplierName
			(select s.supplier_id, s.supplierID, s.supplierName
			from
				(select supplier_id, supplierID, supplierName, 
					count(*) over (partition by supplierName) num_rep, 
					dense_rank() over (partition by supplierName order by supplier_id) ord_rep
				from Landing.mend.gen_supp_supplier_v) s
			where num_rep = ord_rep) s on pr.supplier = s.supplierName
	order by num_rep desc, fm_reference


	select release_date, count(*)
	from Landing.fm.fm_planned_requirements
	group by release_date
	order by release_date