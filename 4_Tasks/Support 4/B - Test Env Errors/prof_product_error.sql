
-- mag_prod_product_family_v
	
	select p.product_id product_id_bk, pfm.productfamilyid,
		p.manufacturer_bk, p.brand_id_bk, cp.category_bk, cp.cl_type_bk, cp.cl_feature_bk, p.product_lifecycle_bk, p.visibility_id_bk, 
		pfg.product_family_group product_family_group_bk, ptoh.product_type_oh_bk,
		p.magento_sku, null product_family_code,
		p.product_family_name, -- isnull(pfm.manufacturerProductFamily, p.product_family_name) product_family_name_erp,
		pfm.manufacturerProductFamily product_family_name_erp,
		case
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then substring(p.product_family_name, 1, charindex(' In ', p.product_family_name) - 1)
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) = 0) then p.product_family_name
			else null
		end glass_sunglass_name,
		case
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then 
				substring(p.product_family_name, charindex(' In ', p.product_family_name) + 4, len(p.product_family_name) - charindex(' In ', p.product_family_name))
			else null
		end glass_sunglass_colour,
		p.status, pfm.status status_erp, p.promotional_product, p.telesales_product, 
		ep.erp_product, p.pack_size, 
		p.url_path, p.url_key, p.google_shopping_gtin
	from 
			(select p1.entity_id product_id, 
				p1.manufacturer manufacturer_bk, p1.brand brand_id_bk,
				case 
					when (isnull(p2.product_lifecycle, p1.product_lifecycle) is null or isnull(p2.product_lifecycle, p1.product_lifecycle) = '') then 'REGULAR'
					else UPPER(isnull(p2.product_lifecycle, p1.product_lifecycle)) 
				end product_lifecycle_bk,
				isnull(p2.visibility, p1.visibility) visibility_id_bk,
				p1.sku magento_sku, 
				p1.name product_family_name, 
				isnull(p2.status, p1.status) status,
				case when (p1.promotional_product is NULL or p1.promotional_product = '') then 0 else p1.promotional_product end promotional_product, 
				case when (p1.telesales_only is NULL or p1.telesales_only = '') then 0 else p1.telesales_only end telesales_product,
				convert(int, isnull(case when (p1.packsize = '') then NULL else p1.packsize end, 1)) pack_size, 
				p1.url_path, p1.url_key, p1.google_shopping_gtin

			from 
					Landing.mag.catalog_product_entity_flat p1
				left join
					Landing.mag.catalog_product_entity_flat p2 on p1.entity_id = p2.entity_id and p2.store_id = 20 
			where p1.store_id = 0) p
		left join
			Landing.map.prod_exclude_products_aud ep on p.product_id = ep.product_id 
		inner join
			Landing.aux.prod_product_category cp on p.product_id = cp.product_id
		left join
			Landing.map.prod_product_family_group_pf_aud pfg on p.product_id = pfg.product_id
		left join 
			Landing.aux.prod_product_product_type_oh ptoh on p.product_id = ptoh.product_id
		left join
			Landing.mend.gen_prod_productfamily_v pfm on p.product_id = pfm.magentoProductID_int
	where ep.product_id is null or (ep.product_id is not null and ep.erp_product is not null)
	union
	select pf.product_id_bk, pf.productfamilyid,
		pf.manufacturer_id_bk manufacturer_bk, null brand_id_bk, category_bk, null cl_type_bk, null cl_feature_bk, product_lifecycle_bk, visibility_id_bk, null product_family_group_bk, null product_type_oh_bk,
		pf.magentoProductID magento_sku, null product_family_code,
		pf.name product_family_name, 
		pfm.manufacturerProductFamily product_family_name_erp,
		null glass_sunglass_name, null glass_sunglass_colour,
		0 status, pfm.status status_erp, 0 promotional_product, 0 telesales_product, 
		null erp_product, 1 pack_size, 
		null url_path, null url_key, null google_shopping_gtin
	from 
			Landing.map.prod_product_family_erp_aud pf
		inner join
			Landing.mend.gen_prod_productfamily_v pfm on pf.productfamilyid = pfm.productfamilyid

		select p.*, pfm.*
		from
				(select p1.entity_id product_id, 
					p1.name product_family_name
				from 
						Landing.mag.catalog_product_entity_flat p1
					left join
						Landing.mag.catalog_product_entity_flat p2 on p1.entity_id = p2.entity_id and p2.store_id = 20 
				where p1.store_id = 0) p
			right join
				Landing.mend.gen_prod_productfamily_v pfm on p.product_id = pfm.magentoProductID_int
			left join
				Landing.map.prod_product_family_erp_aud pf on pfm.productfamilyid = pf.productfamilyid
		where p.product_id is null and pf.productfamilyid is null

		select *
		from Landing.map.prod_product_family_erp_aud

-- Landing.mend.gen_prod_productfamily_v
	select pf.id productfamilyid, 
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pf.id end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name, pf.manufacturerProductFamily, pf.status
	from Landing.mend.prod_productfamily_aud pf

	select *
	from Landing.mend.prod_productfamily_aud pf
	where id in (52917295621609729, 52917295621609630, 52917295621609629, 52917295621604146)
	order by id desc

-- idProductFamilyPackSize_sk

	select pfps.packsizeid, pfps.productfamilyid,
		pf.product_id_bk, convert(int, pfps.size) size, pfps.description product_family_packsize_name, 
		pfps.allocationPreferenceOrder allocation_preference_order, pfps.purchasingPreferenceOrder purchasing_preference_order, 
		pfps.packspercarton packs_per_carton, pfps.packsperpallet packs_per_pallet,
		pfps.width, pfps.height, pfps.depth, pfps.weight
	from 
			Landing.mend.gen_prod_productfamilypacksize_v pfps
		left join
			Landing.aux.mag_prod_product_family_v pf on pfps.productfamilyid = pf.productfamilyid
	where pfps.packsizeid is not null
		and pf.productfamilyid is null
	order by pfps.productfamilyid desc

-- idProduct_sk

	select count(*) over (),
		p.productid product_id_erp_bk, pf.product_id_bk,
		bc base_curve_bk, 
		case 
			when (len(di) = 2) then di + '.0'
			else di 
		end diameter_bk, 
		case 
			when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
			when (len(po) = 3) then po + '.00'
			when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
			when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
			when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
			when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
			else po 
		end power_bk, 
		cy cylinder_bk, 
		case 
			when (len(ax) = 1) then '00' + ax
			when (len(ax) = 2) then '0' + ax
			else ax 
		end axis_bk, 
		case 
			when ad in ('Medium', 'MF', 'MID') then 'MED'
			else upper(ad) 
		end addition_bk, 
		_do dominance_bk, 
		upper (case
			when co = 'BrilliantB' then 'BrilliantBlue' 
			when co = 'Gemstone G' then 'Gemstone Green'
			when co = 'GemstoneGr' then 'GemstoneGreen'
			when co = 'Pacific Bl' then 'Pacific Blue'
			when co = 'Sapphire B' then 'Sapphire Blue'
			when co = 'SterlingGr' then 'SterlingGrey'
			when co = 'TrueSapphi' then 'TrueSapphire'
			else co		
		end) colour_bk,
		p.SKU_product parameter, p.description product_description, 
		p.isBOM, 
		case when (p.productid is not null and p2.id is null) then 'Y' else 'N' end delete_f
	from 
			Landing.mend.gen_prod_product_v p
		left join
			Landing.aux.mag_prod_product_family_v pf on p.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.prod_product p2 on p.productid = p2.id
	where p.productid is not null
		and pf.productfamilyid is null
	order by p.productid desc

-- idStockItem_sk

	select top 1000 count(*) over (), si.stockitemid stockitemid_bk, 
		si.productid productid_erp_bk, si.packsizeid packsizeid_bk, si.packSize packsize, si.lifecycle stock_item_lifecycle_bk,
		si.SKU, si.stockitemdescription stock_item_description, 
		bc.upccode upc_code, si.manufacturerCodeNumber manufacturer_code,
		case when (si.stockitemid is not null and si2.id is null) then 'Y' else 'N' end delete_f
	from 
			Landing.mend.gen_prod_stockitem_v si
		left join
			Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.prod_stockitem si2 on si.stockitemid = si2.id
		left join
			(select stockitemid, barcodeid, upccode, num_bc_rep
			from
				(select bcsi.stockitemid, bcsi.barcodeid, 
					bc.upccode, bc.iscurrent, 
					count(*) over (partition by bcsi.stockitemid) num_bc_rep, 
					dense_rank() over (partition by bcsi.stockitemid order by bcsi.barcodeid desc) ord_bc
				from
						Landing.mend.prod_barcode_stockitem bcsi 
					left join
						Landing.mend.prod_barcode bc on bcsi.barcodeid = bc.id
				where bc.iscurrent = 1) bc
			where ord_bc = 1) bc on si.stockitemid = bc.stockitemid
	where si.stockitemid is not null
		and pf.productfamilyid is null
	order by si.stockitemid desc