
	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where runStatus = 'ERROR'
		and ins_ts > '2020-04-01'
		and package_name = 'stg_dwh_alloc_tables'
	order by ins_ts

	select flow_name, package_name, count(*)
	from ControlDB.logging.t_PackageRun_v
	where runStatus = 'ERROR'
		and ins_ts > '2020-04-01'
	group by flow_name, package_name
	order by flow_name, package_name