
-- There was an error with OLE_DEST_dim_receipt_wrk.Inputs[OLE DB Destination Input].Columns[idWHShipment_sk] on OLE_DEST_dim_receipt_wrk.Inputs[OLE DB Destination Input]. 
-- The column status returned was: "The value violated the integrity constraints for the column.".  

select top 1000 count(*) over (), *
from 
		Staging.rec.dim_receipt r
	left join
		Warehouse.rec.dim_wh_shipment s on r.shipment_id_bk = s.shipment_id_bk
where s.shipment_id_bk is null

select top 1000 count(*) over (), *
from Warehouse.rec.dim_receipt_wrk

select top 1000 count(*) over (), *
from 
	Staging.rec.fact_receipt_line rl
left join
	Warehouse.rec.dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null

select top 1000 count(*) over (), *
from Warehouse.rec.fact_receipt_line_wrk

select rl.*
from 
		Landing.aux.rec_fact_receipt_line rl
	left join
		Landing.aux.rec_dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null

delete r
from
		Landing.aux.rec_dim_receipt r
	left join
		Warehouse.rec.dim_wh_shipment s on r.shipment_id_bk = s.shipment_id_bk
where s.shipment_id_bk is null

delete rl
from 
		Landing.aux.rec_fact_receipt_line rl
	left join
		Landing.aux.rec_dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null