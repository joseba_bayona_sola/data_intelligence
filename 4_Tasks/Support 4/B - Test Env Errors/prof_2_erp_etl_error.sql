
-- stg_dwh_alloc_tables

-- There was an error with OLE_DEST_fact_order_line_erp_wrk.Inputs[OLE DB Destination Input].Columns[idProduct_sk] on OLE_DEST_fact_order_line_erp_wrk.Inputs[OLE DB Destination Input]. 
-- The column status returned was: "The value violated the integrity constraints for the column.".  

-- stg_dwh_stock_tables

-- There was an error with OLE_DEST_dim_intransit_stock_item_batch_wrk.Inputs[OLE DB Destination Input].Columns[idStockItem_sk] on OLE_DEST_dim_intransit_stock_item_batch_wrk.Inputs[OLE DB Destination Input]. 
-- The column status returned was: "The value violated the integrity constraints for the column.".  

-- stg_dwh_frcons_tables

-- There was an error with OLE_DEST_freight_consignment_line_wrk.Inputs[OLE DB Destination Input].Columns[idProductFamilyPackSize_sk] on OLE_DEST_freight_consignment_line_wrk.Inputs[OLE DB Destination Input]. 
-- The column status returned was: "The value violated the integrity constraints for the column.".  

-- stg_dwh_rec_tables
-- SSIS Error Code DTS_E_PROCESSINPUTFAILED.  The ProcessInput method on component "OLE_DEST_dim_receipt_line_wrk" (131) failed with error code 0xC0209029 while processing input "OLE DB Destination Input" (144). 
-- The identified component returned an error from the ProcessInput method. The error is specific to the component, but the error is fatal and will cause the Data Flow task to stop running.  
-- There may be error messages posted before this with more information about the failure.  

select top 1000 count(*) over (), *
from 
		Staging.rec.dim_receipt r
	left join
		Warehouse.rec.dim_wh_shipment s on r.shipment_id_bk = s.shipment_id_bk
where s.shipment_id_bk is null

select top 1000 count(*) over (), *
from Warehouse.rec.dim_receipt_wrk

select top 1000 count(*) over (), *
from 
	Staging.rec.fact_receipt_line rl
left join
	Warehouse.rec.dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null

select top 1000 count(*) over (), *
from Warehouse.rec.fact_receipt_line_wrk

select rl.*
from 
		Landing.aux.rec_fact_receipt_line rl
	left join
		Landing.aux.rec_dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null

delete rl
from 
		Landing.aux.rec_fact_receipt_line rl
	left join
		Landing.aux.rec_dim_receipt r on rl.receiptid_bk = r.receiptid_bk
where r.receiptid_bk is null


-- There was an error with OLE_DEST_dim_wholesale_order_header_erp_wrk.Inputs[OLE DB Destination Input].Columns[idOrderHeaderERP_sk] on OLE_DEST_dim_wholesale_order_header_erp_wrk.Inputs[OLE DB Destination Input]. 
-- The column status returned was: "The value violated the integrity constraints for the column.".  

select t1.*
from 
	Staging.alloc.dim_wholesale_order_header_erp t1
left join
	Warehouse.alloc.dim_order_header_erp t2 on t1.orderid_bk = t2.orderid_bk
where t2.orderid_bk is null

select top 1000 *
from Landing.aux.alloc_dim_order_header_erp_aud
where orderid_bk = 48695171068102101

delete from Landing.aux.alloc_dim_order_header_erp_aud
where orderid_bk = 48695171068102101

select *
from Staging.alloc.dim_order_header_erp
order by orderid_bk
