
-------------------------------------------------------------
-------------------------------------------------------------

	select count(*) over (partition by PersonEmail) num_rep, 
		dense_rank() over (partition by PersonEmail order by c.ID desc, date_created desc, EON_Sign_Up_Form_Source__c) ord_rep,
		date_created, num_of_orders,
		c.customer_id_bk, c.Id,
		1 SCCActive__c,
		PersonEmail, FirstName, LastName, 
		EON_Preferred_Locale__c, EON_Preferred_Site__c, 
		1 EON_Newsletter_Signed_Up__c, EON_Sign_Up_Form_Source__c, EON_Preferred_Lenses__c
	
	from
			(select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders,
				Email PersonEmail, FirstName, 'Newsletter' LastName, 
				'en_GB' EON_Preferred_Locale__c, 'VD-UK' EON_Preferred_Site__c, 'Fresh Relevance - UK' EON_Sign_Up_Form_Source__c, 
				case 
					when (Su_Lens_Type = 'coloured_lenses') then 'Coloured Lenses'
					when (Su_Lens_Type = 'daily_disposable') then 'Daily disposable'
					when (Su_Lens_Type = 'daily_torics_for_astigmatism') then 'Daily Torics for Astigmatism'
					when (Su_Lens_Type = 'monthly') then 'Monthly'
					when (Su_Lens_Type = 'multifocal') then 'Multifocal'
					when (Su_Lens_Type = 'torics_for_astigmatism') then 'Torics for Astigmatism'
					when (Su_Lens_Type = 'two_weekly_disposable') then '2 Weekly disposable'
				end EON_Preferred_Lenses__c 
			from Landing.csv.guest_fresh_relevance
			union
			select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders, 
				Email, FirstName, 'Newsletter' LastName, 
				'en_GB' EON_Preferred_Locale__c, 'VD-UK' EON_Preferred_Site__c, 'Glasses Direct' EON_Sign_Up_Form_Source__c, GD_Lens_Type_Su EON_Preferred_Lenses__c
			from Landing.csv.guest_glasses_direct
			union
			select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders, 
				Email, FirstName, LastName, 
				'fr_FR' EON_Preferred_Locale__c, 'VD-FR' EON_Preferred_Site__c, 'MGEN' EON_Sign_Up_Form_Source__c, null EON_Preferred_Lenses__c 
			from Landing.csv.guest_mgen
			union
			select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders,
				Email, FirstName, LastName, 
				'fr_FR' EON_Preferred_Locale__c, 'VD-FR' EON_Preferred_Site__c, 'MGEFI' EON_Sign_Up_Form_Source__c, null EON_Preferred_Lenses__c 
			from Landing.csv.guest_mgefi) g
		left join
			Landing.aux.gen_dim_customer_aud c on g.PersonEmail = c.customer_email 
	-- where c.customer_id_bk is not null and g.num_of_orders <> '' -- Order placed customers: 12311
	-- where c.customer_id_bk is not null and g.num_of_orders = '' -- RNB Customers: 1990
	-- where c.customer_id_bk is null and g.num_of_orders <> '' -- GDPR??: 52
	-- where c.customer_id_bk is null -- Real Guests: 11413
	order by num_rep desc, PersonEmail

	-- 

	select count(*) over (partition by PersonEmail) num_rep, 
		customer_id_bk, Id,
		1 SCCActive__c,
		PersonEmail, FirstName, LastName, 
		EON_Preferred_Locale__c, EON_Preferred_Site__c, 
		1 EON_Newsletter_Signed_Up__c, EON_Sign_Up_Form_Source__c, EON_Preferred_Lenses__c
	
	from
		(select count(*) over (partition by PersonEmail) num_rep, 
			dense_rank() over (partition by PersonEmail order by c.ID desc, date_created desc, EON_Sign_Up_Form_Source__c) ord_rep,
			date_created, num_of_orders,
			c.customer_id_bk, c.Id,
			1 SCCActive__c,
			PersonEmail, FirstName, LastName, 
			EON_Preferred_Locale__c, EON_Preferred_Site__c, 
			1 EON_Newsletter_Signed_Up__c, EON_Sign_Up_Form_Source__c, EON_Preferred_Lenses__c
		
		from
				(select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders,
					Email PersonEmail, FirstName, 'Newsletter' LastName, 
					'en_GB' EON_Preferred_Locale__c, 'VD-UK' EON_Preferred_Site__c, 'Fresh Relevance - UK' EON_Sign_Up_Form_Source__c, 
					case 
						when (Su_Lens_Type = 'coloured_lenses') then 'Coloured Lenses'
						when (Su_Lens_Type = 'daily_disposable') then 'Daily disposable'
						when (Su_Lens_Type = 'daily_torics_for_astigmatism') then 'Daily Torics for Astigmatism'
						when (Su_Lens_Type = 'monthly') then 'Monthly'
						when (Su_Lens_Type = 'multifocal') then 'Multifocal'
						when (Su_Lens_Type = 'torics_for_astigmatism') then 'Torics for Astigmatism'
						when (Su_Lens_Type = 'two_weekly_disposable') then '2 Weekly disposable'
					end EON_Preferred_Lenses__c 
				from Landing.csv.guest_fresh_relevance
				union
				select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders, 
					Email, FirstName, 'Newsletter' LastName, 
					'en_GB' EON_Preferred_Locale__c, 'VD-UK' EON_Preferred_Site__c, 'Glasses Direct' EON_Sign_Up_Form_Source__c, GD_Lens_Type_Su EON_Preferred_Lenses__c
				from Landing.csv.guest_glasses_direct
				union
				select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders, 
					Email, FirstName, LastName, 
					'fr_FR' EON_Preferred_Locale__c, 'VD-FR' EON_Preferred_Site__c, 'MGEN' EON_Sign_Up_Form_Source__c, null EON_Preferred_Lenses__c 
				from Landing.csv.guest_mgen
				union
				select convert(datetime, DateCreated, 103) date_created, Num_Of_Orders num_of_orders,
					Email, FirstName, LastName, 
					'fr_FR' EON_Preferred_Locale__c, 'VD-FR' EON_Preferred_Site__c, 'MGEFI' EON_Sign_Up_Form_Source__c, null EON_Preferred_Lenses__c 
				from Landing.csv.guest_mgefi) g
			left join
				Landing.aux.gen_dim_customer_aud c on g.PersonEmail = c.customer_email) g 
	where g.ord_rep = 1
	--	and g.customer_id_bk is null and g.customer_id_bk is null and g.num_of_orders <> '' -- GDPR??: 52f_orders <> '' -- GDPR??: 52
	order by num_rep desc

-------------------------------------------------------------
-------------------------------------------------------------

select Id, EON_Newsletter_Signed_Up__c, EON_Sign_Up_Form_Source__c, isnull(EON_Preferred_Lenses__c, '') EON_Preferred_Lenses__c
from Landing.sf_mig.guest_v2_v
where Id is not null

select SCCActive__c, '0121i000000kJCdAAM' RecordTypeId, -- 0123O0000003TEsQAM
	FirstName, case when (LastName = '') then 'Newsletter' else LastName end LastName, PersonEmail, 
	EON_Newsletter_Signed_Up__c, EON_Sign_Up_Form_Source__c, isnull(EON_Preferred_Lenses__c, '') EON_Preferred_Lenses__c,
	EON_Preferred_Locale__c, EON_Preferred_Site__c
from Landing.sf_mig.guest_v2_v
where Id is null
