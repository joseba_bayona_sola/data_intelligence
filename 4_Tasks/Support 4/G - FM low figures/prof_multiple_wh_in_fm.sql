
select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_gross_requirements
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_sales_forecast
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_orders
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*)--, sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_stock_on_hand
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_safety_stock
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*)--, sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_shortages
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_min_stock
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_max_stock
group by warehouse
order by warehouse

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*)--, sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_suppressions
group by warehouse
order by warehouse


