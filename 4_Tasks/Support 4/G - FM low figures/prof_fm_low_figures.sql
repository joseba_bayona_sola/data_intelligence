
select top 1000 *
from Landing.fm.fm_gross_requirements

select top 1000 warehouse, 
	substring(warehouse, 1, charindex('_', warehouse, 1)-1),
	substring(substring(warehouse, 1, charindex('_', warehouse, 1)-1), 1, 3),
	count(*), sum(convert(decimal(12, 2), qty_packs))
from Landing.fm.fm_gross_requirements
group by warehouse
order by warehouse


	-- t_PackageRunMessage_v
	select idPackageRunMessage, prm.idPackageRun, prm.flow_name, prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunMessage_v prm
		inner join
			ControlDB.logging.t_PackageRun_v pr on prm.idPackageRun = pr.idPackageRun
	where -- pr.idETLBatchRun = 35496
		prm.package_name = 'fm_lnd_files_all' --  // stg_dwh_sales_fact_order
		and message like 'EX_SRC_fm_Gross_Requirements%'
	order by prm.package_name, messageTime desc

	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, datediff(minute, spm.startTime, spm.finishTime) duration,
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where -- sp.idETLBatchRun = 35496 -- 3683
		spm.sp_name = 'stg_dwh_get_fm_fm_data' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime desc, spm.messageTime


	select *
	from Landing.mend.gen_wh_warehouse_v

	select *
	from Landing.mend.wh_warehouse_aud_v
	order by id, ins_ts

select top 1000 *
from Staging.fm.fact_fm_data

