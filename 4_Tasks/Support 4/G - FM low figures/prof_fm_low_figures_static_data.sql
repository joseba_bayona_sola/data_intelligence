
	select top 1000 count(*) over (),
		count(*) over (partition by w.warehouseid, si.stockitemid) num_rep,
		ssd.*,
		w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, 
		null supplierid_bk, substring(warehouse, charindex('_', warehouse, 1)+1, 1) sale_type,
		planner, abc, stockable, stocked, final
	from 
			Landing.fm.fm_stock_static_data ssd
		inner join
			Landing.mend.gen_wh_warehouse_v w on substring(substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1), 1, 3) = w.shortName
				-- on substring(ssd.warehouse, 1, charindex('_', ssd.warehouse, 1)-1) = w.shortName
		inner join
			(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
			from 
				(select si.stockitemid, si.sku, si.stockitemdescription, 
					isnull(wsi.num_records, 0) num_records,
					count(*) over (partition by si.sku) num_rep, 
					rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
				from 
						Landing.mend.gen_prod_stockitem_v si
					left join
						(select stockitemid, count(*) num_records
						from Landing.mend.gen_wh_warehousestockitem_v
						where wh_stock_item_batch_f = 'Y'
						group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
			where num_rep = ord_rep) si on ssd.sku = si.sku
	where substring(warehouse, charindex('_', warehouse, 1)+1, 1) = 'R'
		-- and w.warehouseid = 46161896180547586 and si.stockitemid = 32088147345062649
		and warehouse not in ('AMS_RETAIL', 'YOR_RETAIL')
	order by num_rep desc

select top 1000 *
from Landing.fm.fm_stock_static_data		
where sku = '01083B2D4CT0000000000301'

select top 1000 warehouse, count(*)
from Landing.fm.fm_stock_static_data
group by warehouse
order by warehouse

select top 1000 count(*) over (), *
from Landing.fm.fm_stock_static_data		
-- where warehouse = 'AMS_RETAIL' and abc is not null
where warehouse = 'AMSTERDAM_RETAIL' and abc is null

	-- t_PackageRunMessage_v
	select idPackageRunMessage, prm.idPackageRun, prm.flow_name, prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunMessage_v prm
		inner join
			ControlDB.logging.t_PackageRun_v pr on prm.idPackageRun = pr.idPackageRun
	where -- pr.idETLBatchRun = 35496
		prm.package_name = 'fm_lnd_files_all' --  // stg_dwh_sales_fact_order
		and message like 'EX_SRC_fm_stock_static_data%'
	order by prm.package_name, messageTime desc