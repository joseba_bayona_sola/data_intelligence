
select distinct email
from DW_GetLenses_jbs.dbo.fr_failed_records

select fr.email, c.entity_id, c.email, c.created_at, c.updated_at, c.store_id, c.created_in, c.firstname, c.lastname
from
		(select distinct email
		from DW_GetLenses_jbs.dbo.fr_failed_records) fr
	left join
		Landing.mag.customer_entity_flat_aud c on fr.email = c.email
order by c.entity_id

select fr.email, c.entity_id, c.email, c.created_at, c.updated_at, c.store_id, c.created_in, c.firstname, c.lastname
from
		(select distinct email
		from DW_GetLenses_jbs.dbo.fr_failed_records) fr
	left join
		Landing.mag_rt.customer_entity_flat_aud c on fr.email = c.email
order by c.entity_id

	select c.store_id, count(*)
	from
			(select distinct email
			from DW_GetLenses_jbs.dbo.fr_failed_records) fr
		left join
			Landing.mag.customer_entity_flat_aud c on fr.email = c.email
	group by c.store_id
	order by c.store_id
