
select *
from DW_GetLenses_jbs.dbo.glossybox_redeemed_codes

select t1.coupon_code, t1.num_coupon_code, 
	t1.idCustomer_sk_fk, c.customer_id_bk, c.customer_email, c.first_name, c.last_name,	
	t1.upd_ts coupon_given_date, t2.used_date, 
	dense_rank() over (order by t2.used_date, t1.upd_ts, t1.coupon_code) num_coupon_code
from 
		Warehouse.dm.glossybox_coupon t1
	left join
		DW_GetLenses_jbs.dbo.glossybox_redeemed_codes t2 on t1.coupon_code = t2.coupon_code
	inner join
		Warehouse.gen.dim_customer c on t1.idCustomer_sk_fk = c.idCustomer_sk
where t1.idCustomer_sk_fk is not null
order by t1.num_coupon_code

select t1.coupon_code, dense_rank() over (order by t1.coupon_code) num_coupon_code
from 
	Landing.csv.glossybox_coupon_new t1
left join
	Warehouse.dm.glossybox_coupon t2 on t1.coupon_code = t2.coupon_code
where t2.coupon_code is null
order by t1.coupon_code


-------------------------------------------------

select t1.coupon_code, t1.num_coupon_code, 
	t1.customer_id_bk, t1.customer_email, t1.first_name, t1.last_name,	
	t1.coupon_given_date, t1.used_date, t2.coupon_code coupon_code_new
from
		(select t1.coupon_code, t1.num_coupon_code, 
			t1.idCustomer_sk_fk, c.customer_id_bk, c.customer_email, c.first_name, c.last_name,	
			t1.upd_ts coupon_given_date, t2.used_date, 
			dense_rank() over (order by t2.used_date, t1.upd_ts, t1.coupon_code) num_coupon_code_null
		from 
				Warehouse.dm.glossybox_coupon t1
			left join
				DW_GetLenses_jbs.dbo.glossybox_redeemed_codes t2 on t1.coupon_code = t2.coupon_code
			inner join
				Warehouse.gen.dim_customer c on t1.idCustomer_sk_fk = c.idCustomer_sk
		where t1.idCustomer_sk_fk is not null) t1
	left join
		(select t1.coupon_code, dense_rank() over (order by t1.coupon_code) num_coupon_code
		from 
				Landing.csv.glossybox_coupon_new t1
			left join
				Warehouse.dm.glossybox_coupon t2 on t1.coupon_code = t2.coupon_code
		where t2.coupon_code is null) t2 on t1.num_coupon_code_null = t2.num_coupon_code and t1.used_date is null
order by t1.num_coupon_code

order by t1.coupon_code