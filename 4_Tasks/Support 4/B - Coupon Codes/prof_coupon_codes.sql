
select *
from Warehouse.sales.dim_coupon_code_v
where coupon_code_name in ('20Keyworkers NL', 'Keyworkers NL', '20Keyworkers', 'Keyworkers', 'UNIDAYS 2020', 'GROUPON 2020', 'Rakuten TV 2020', 'Affiliate Wowcher ')
order by coupon_id_bk desc

select *
from Warehouse.sales.dim_coupon_code
where coupon_code_name in ('Rakuten TV 2020')
order by coupon_id_bk desc

select *
from Warehouse.sales.dim_coupon_code_v
-- where coupon_code in ('96XUP1UABRHD')
order by coupon_id_bk desc

	select count(*), count(distinct coupon_id_bk), count(distinct coupon_code), count(distinct coupon_code_name)
	from Warehouse.sales.dim_coupon_code_v

	select count(*) over (partition by coupon_code) num_rep, *
	from Warehouse.sales.dim_coupon_code_v
	order by num_rep desc

------------------------------


select top 1000 entity_id, increment_id, created_at, coupon_code, applied_rule_ids, customer_id
from Landing.mag.sales_flat_order
where coupon_code is not null
order by entity_id desc

select top 1000 order_id_bk, order_no, order_date, coupon_id_bk, coupon_code, applied_rule_ids
from Landing.aux.sales_dim_order_header
where coupon_id_bk is not null
order by order_id_bk desc

----------------------------------

select *
from Landing.mag.salesrule_aud
order by rule_id desc

select *
from Landing.mag.salesrule_coupon_aud
order by coupon_id desc

select distinct value
from Landing.mag.core_config_data
where path = 'referafriend/invite/voucher'

	select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, isnull(sr.channel, 'NULL') channel_bk, sr.rule_id, 
		isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, sr.name coupon_code_name, 
		sr.from_date, sr.to_date, src.expiration_date, 
		sr.simple_action discount_type_name, sr.discount_amount, 
		case when (sr.postoptics_only_new_customer = 1) then 'Y' else 'N' end only_new_customer,
		case when (ref.value is not null) then 'Y' else 'N' end refer_a_friend
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
		left join
			(select distinct value
			from Landing.mag.core_config_data
			where path = 'referafriend/invite/voucher') ref on src.coupon_id = ref.value
	-- where src.rule_id is null
	where ref.value is not null
	order by sr.rule_id desc, src.coupon_id desc

----------------------------------

						select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
						from 
								Landing.aux.sales_order_header_o_i_s_cr oh
							inner join
								Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
						where (o.coupon_code is not null or o.applied_rule_ids is not null)
							and charindex(',', applied_rule_ids) = 0 -- and len(o.applied_rule_ids) < 5

						select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
							isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
							sr.channel
						from 
								Landing.mag.salesrule_coupon_aud src
							right join
								Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
						where sr.rule_id = 3024

						----

						select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
						from 
								Landing.aux.sales_order_header_o_i_s_cr oh
							inner join
								Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
						where (o.coupon_code is not null or o.applied_rule_ids is not null)
							and charindex(',', applied_rule_ids) <> 0

----------------------------------
