
			merge into #sales_order_line_vat trg
			using 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					ol_v.countries_registered_code, ol_v.product_type_vat, ol_v.invoice_date,
					vr.vat_type_rate_code, 
					vr.vat_rate, pfr.prof_fee_rate
				from 
						(select order_line_id_bk, order_id_bk, 
							ol.countries_registered_code, 
							ol.countries_registered_code countries_registered_code_pf,
							product_type_vat, dateadd(dd, 30, invoice_date) invoice_date, invoice_date invoice_date_old
						from 
								#sales_order_line_vat ol
							left join
								(select distinct countries_registered_code
								from Landing.map.vat_prof_fee_fixed_value_aud) pff on ol.countries_registered_code = pff.countries_registered_code
						where pff.countries_registered_code is null -- and ol.countries_registered_code in ('EFTA', 'NON-EU')
						) ol_v
					inner join
						Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
							and ol_v.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
					inner join
						Landing.map.vat_prof_fee_rate_aud pfr on ol_v.countries_registered_code_pf = pfr.countries_registered_code and ol_v.product_type_vat = pfr.product_type_vat 
							and ol_v.invoice_date between pfr.vat_time_period_start and pfr.vat_time_period_finish) src
			on trg.order_line_id_bk = src.order_line_id_bk
			when matched then 
				update set
					trg.vat_type_rate_code = src.vat_type_rate_code, 
					trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate;

select dateadd(dd, 30, invoice_date), vr.*, pfr.*, ol_v.*
from 
	Landing.aux.sales_order_line_vat ol_v
inner join
	Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
		and dateadd(dd, 30, ol_v.invoice_date) between vr.vat_time_period_start and vr.vat_time_period_finish
inner join
	Landing.map.vat_prof_fee_rate_aud pfr on ol_v.countries_registered_code = pfr.countries_registered_code and ol_v.product_type_vat = pfr.product_type_vat 
		and dateadd(dd, 30, ol_v.invoice_date) between pfr.vat_time_period_start and pfr.vat_time_period_finish
-- where ol_v.prof_fee_rate is null
where ol_v.countries_registered_code in ('NON-EU', 'EFTA')
order by invoice_date

------------------------------------------------


select top 1000 count(*) over (), *
from Landing.aux.sales_fact_order_line
where prof_fee_rate is null

select countries_registered_code, prof_fee_rate, count(*) 
from Landing.aux.sales_fact_order_line
group by countries_registered_code, prof_fee_rate
order by countries_registered_code, prof_fee_rate

