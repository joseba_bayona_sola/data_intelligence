
select *
from DW_GetLenses_jbs.dbo.adm_wearers

	select distinct consumer_email email
	from DW_GetLenses_jbs.dbo.adm_wearers

	select c.idCustomer_sk, c.customer_id_bk, c.customer_email
	from
			Warehouse.gen.dim_customer c
		inner join
			(select distinct consumer_email email
			from DW_GetLenses_jbs.dbo.adm_wearers) t on c.customer_email = t.email

	select c.idCustomer_sk_fk, c.customer_id_bk, c.customer_email, 
		case when (isnull(c.customer_unsubscribe_name, 'N') = 'Y' or isnull(c.customer_unsubscribe_dotmailer_name, 'N') = 'Y' or isnull(c.customer_unsubscribe_sf_mc_name, 'N') = 'Y') then 'Y' else 'N' end final_uns,
		coalesce(c.customer_unsubscribe_date, c.customer_unsubscribe_dotmailer_date, c.customer_unsubscribe_sf_mc_date) uns_date,
		-- c.Id, c.PersonContactId, c.EON_Opt_in__c_Email,
		c.unsubscribe_all, c.unsubscribe_all_date, 
		c.customer_unsubscribe_name, c.customer_unsubscribe_date, c.customer_unsubscribe_dotmailer_name, c.customer_unsubscribe_dotmailer_date, 
		c.customer_unsubscribe_sf_mc_name, c.customer_unsubscribe_sf_mc_date
		
	from
			Warehouse.gen.dim_customer_scd_v c
		inner join
			(select distinct consumer_email email
			from DW_GetLenses_jbs.dbo.adm_wearers) t on c.customer_email = t.email
	order by final_uns

	select csv.customer_id, csv.customer_email,
		csv.customer_unsubscribe_name, csv.customer_unsubscribe_date
	from
			Warehouse.tableau.customer_single_view_ds csv
		inner join
			Warehouse.gen.dim_customer c on csv.customer_id = c.customer_id_bk
		inner join
			(select distinct consumer_email email
			from DW_GetLenses_jbs.dbo.adm_wearers) t on c.customer_email = t.email

	select csv.entity_id, csv.email,
		csv.EON_Opt_in__c
	from
			Landing.sf_mig.customer3_v csv
		inner join
			Warehouse.gen.dim_customer c on csv.entity_id = c.customer_id_bk
		inner join
			(select distinct consumer_email email
			from DW_GetLenses_jbs.dbo.adm_wearers) t on c.customer_email = t.email
	order by csv.EON_Opt_in__c, csv.email

	select c.customer_id_bk, c.customer_email, c.Id,
		csv.EON_Opt_in__c
	from
			Warehouse.gen.dim_customer c 
		inner join
			(select distinct consumer_email email
			from DW_GetLenses_jbs.dbo.adm_wearers) t on c.customer_email = t.email
		left join
			Landing.sf_sc.EON_preferences__c_aud csv on c.Id = csv.EON_Account__c
	order by csv.EON_Opt_in__c, c.customer_email



-------------------------------------------------------------

select top 1000 *
from Warehouse.gen.dim_customer
where customer_email = 'kateisscottish@gmail.com'

select top 1000 idCustomer_sk_fk, customer_id_bk, customer_email, 
	Id, PersonContactId, EON_Opt_in__c_Email,
	unsubscribe_all, unsubscribe_all_date, 
	customer_unsubscribe_name, customer_unsubscribe_date, customer_unsubscribe_dotmailer_name, customer_unsubscribe_dotmailer_date, 
	customer_unsubscribe_sf_mc_name, customer_unsubscribe_sf_mc_date
from Warehouse.gen.dim_customer_scd_v
where idCustomer_sk_fk = 154710

select top 1000 *
from Warehouse.tableau.customer_single_view_ds
where customer_id = 1904393