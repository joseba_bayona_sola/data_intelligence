
select entity_id, order_id, increment_id, customer_id, created_at, updated_at, despatched_at_dw
from
	(select entity_id, order_id, increment_id, customer_id, created_at, updated_at, dateadd(hour, 1, despatched_at_dw) despatched_at_dw
	from Landing.mag.sales_flat_shipment_aud
	where entity_id > 10560000) s
where despatched_at_dw <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, despatched_at_dw)) + ' ' + '01:30')
order by entity_id desc

select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where order_id_bk = 12068087

select top 1000 id, shipmentNumber, orderIncrementID, dateReadyToShipUTC, dateShippedUTC, createdDate, changedDate
from Landing.mend.ship_customershipment
where dateShippedUTC is not null
	and shipmentNumber = '8007192347-1'
order by id desc

-------------------------------------------------------------

drop table #shipments

select id, shipmentNumber, orderIncrementID, dateReadyToShipUTC, dateShippedUTC, createdDate, changedDate
into #shipments
from Landing.mend.ship_customershipment_aud
where createdDate > '2019-02-14'

select count(*) 
from #shipments

select top 1000 count(*) over (),
	id, shipmentNumber, orderIncrementID, dateReadyToShipUTC, dateShippedUTC, dateShippedUTC_real, createdDate, changedDate
from
	(select s.id, s.shipmentNumber, s.orderIncrementID, s.dateReadyToShipUTC, s.dateShippedUTC, 
		case when (s.dateShippedUTC between stp.from_date and stp.to_date) then dateadd(hour, 1, s.dateShippedUTC) else s.dateShippedUTC end dateShippedUTC_real,
		s.createdDate, s.changedDate
	from 
		#shipments s
	left join
		Landing.map.sales_summer_time_period_aud stp on year(s.dateShippedUTC) = stp.year
	where s.dateShippedUTC is not null) s
where dateShippedUTC_real <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, dateShippedUTC_real)) + ' ' + '01:30')
	and dateShippedUTC_real > '2020-07-01'
order by id desc

	select datepart(hour, dateShippedUTC_real), datepart(minute, dateShippedUTC_real), count(*)
	from
		(select s.id, s.shipmentNumber, s.orderIncrementID, s.dateReadyToShipUTC, s.dateShippedUTC, 
			case when (s.dateShippedUTC between stp.from_date and stp.to_date) then dateadd(hour, 1, s.dateShippedUTC) else s.dateShippedUTC end dateShippedUTC_real,
			s.createdDate, s.changedDate
		from 
			#shipments s
		left join
			Landing.map.sales_summer_time_period_aud stp on year(s.dateShippedUTC) = stp.year
		where s.dateShippedUTC is not null) s
	where dateShippedUTC_real <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, dateShippedUTC_real)) + ' ' + '01:30')
		and dateShippedUTC_real > '2020-07-01'
	group by datepart(hour, dateShippedUTC_real), datepart(minute, dateShippedUTC_real)
	order by datepart(hour, dateShippedUTC_real), datepart(minute, dateShippedUTC_real)
