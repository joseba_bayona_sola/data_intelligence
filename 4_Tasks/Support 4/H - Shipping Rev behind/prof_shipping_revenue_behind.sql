
select order_id_bk, order_no, shipment_no, 
	order_date, invoice_date, shipment_date, 
	order_status_magento_name, order_status_name
into #oh_august
from Warehouse.sales.dim_order_header_v
where order_date > '2020-08-01'

	select *
	from #oh_august
	where invoice_date is not null and shipment_date is null
	
	select oh.order_id_bk, oh.order_no, oh.shipment_no, 
		oh.order_date, oh.invoice_date, oh.shipment_date, 
		oh.order_status_magento_name, oh.order_status_name, 
		sh.increment_id, sh.shipping_description, sh.created_at_dw, sh.despatched_at_dw, 
		sh2.status, sh2.dateReadyToShipUTC, sh2.dateShippedUTC
	from 
			#oh_august oh
		left join
			Landing.mag.sales_flat_shipment_aud sh on oh.order_id_bk = sh.order_id
		left join
			Landing.mend.ship_customershipment_aud sh2 on oh.shipment_no = sh2.shipmentNumber
	where oh.invoice_date is not null and oh.shipment_date is null
		-- and shipment_no is not null
		-- and sh.despatched_at_dw is not null
		-- and sh2.dateShippedUTC is not null
	order by order_id_bk

	select convert(date, invoice_date), order_status_name, count(*)
	from 
			#oh_august oh
		left join
			Landing.mag.sales_flat_shipment_aud sh on oh.order_id_bk = sh.order_id
		left join
			Landing.mend.ship_customershipment_aud sh2 on oh.shipment_no = sh2.shipmentNumber
	where oh.invoice_date is not null and oh.shipment_date is null
	group by convert(date, invoice_date), order_status_name
	order by order_status_name, convert(date, invoice_date)
