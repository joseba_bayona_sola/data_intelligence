
--select idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
--into DW_GetLenses_jbs.dbo.stock_report_20200131_1
--from Warehouse.stock.dim_wh_stock_item_batch_hist
--where idCalendarStockDate_sk_fk = 20200131

--select idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
--into DW_GetLenses_jbs.dbo.stock_report_20200131_2
--from Warehouse.stock.dim_wh_stock_item_batch_hist_full
--where idCalendarStockDate_sk_fk = 20200131

select idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
into DW_GetLenses_jbs.dbo.stock_report_20200531_1
from Warehouse.stock.dim_wh_stock_item_batch_hist
where idCalendarStockDate_sk_fk = 20200531

select idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
into DW_GetLenses_jbs.dbo.stock_report_20200531_2
from Warehouse.stock.dim_wh_stock_item_batch_hist_full
where idCalendarStockDate_sk_fk = 20200531

	select count(*), count(distinct idWHStockItemBatch_sk_fk), sum(qty_on_hand)
	from DW_GetLenses_jbs.dbo.stock_report_20200531_1

	select count(*), count(distinct idWHStockItemBatch_sk_fk), sum(qty_on_hand)
	from DW_GetLenses_jbs.dbo.stock_report_20200531_2

	select top 10000 count(*) over (), 
		t1.idWHStockItemBatch_sk_fk, t2.idWHStockItemBatch_sk_fk, t1.idInvoiceReconciliationLine_sk_fk, t2.idInvoiceReconciliationLine_sk_fk, t1.qty_on_hand, t2.qty_on_hand, 
		sum(t1.qty_on_hand) over (), sum(t2.qty_on_hand) over (), sum(t1.qty_on_hand - t2.qty_on_hand) over (),
		wsib.warehousestockitembatchid_bk, wsib.batch_id, wsib.qty_available
	from 
			DW_GetLenses_jbs.dbo.stock_report_20200531_1 t1
		full join
			DW_GetLenses_jbs.dbo.stock_report_20200531_2 t2 on t1.idWHStockItemBatch_sk_fk = t2.idWHStockItemBatch_sk_fk and t1.idInvoiceReconciliationLine_sk_fk = t2.idInvoiceReconciliationLine_sk_fk
		inner join
			Warehouse.stock.dim_wh_stock_item_batch wsib on isnull(t1.idWHStockItemBatch_sk_fk, t2.idWHStockItemBatch_sk_fk) = wsib.idWHStockItemBatch_sk
	-- where t1.idWHStockItemBatch_sk_fk is null order by t2.qty_on_hand -- 142
	-- where t2.idWHStockItemBatch_sk_fk is null order by t1.qty_on_hand -- 3496 -- RERUN
	-- where t1.idWHStockItemBatch_sk_fk is not null and t1.idWHStockItemBatch_sk_fk is not null and t1.qty_on_hand = t2.qty_on_hand -- 318973
	where t1.idWHStockItemBatch_sk_fk is not null and t1.idWHStockItemBatch_sk_fk is not null and t1.qty_on_hand <> t2.qty_on_hand order by (t1.qty_on_hand - t2.qty_on_hand) -- 3311 -- RERUN

select idWHStockItemBatch_sk, idInvoiceReconciliationLine_sk_fk, warehousestockitembatchid_bk, stock_date, warehouse_name, product_id_magento, sku, batch_id, qty_on_hand
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
where warehousestockitembatchid_bk in (43910096370179269, 43910096369930813, 43910096370014199)
	-- idWHStockItemBatch_sk in (2754985, 2920898, 2669812)
	and stock_date = '2020-05-31'
order by warehousestockitembatchid_bk, stock_date

select top 100 idWHStockItemBatch_sk, warehousestockitembatchid_bk, stock_date, warehouse_name, product_id_magento, sku, batch_id, qty_on_hand
from Warehouse.stock.dim_wh_stock_item_batch_hist_full_v
where warehousestockitembatchid_bk in (43910096370179269, 43910096369930813, 43910096370014199)
	-- idWHStockItemBatch_sk in (2754985, 2920898, 2669812)
	and stock_date = '2020-05-31'
order by warehousestockitembatchid_bk, stock_date


select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
where warehousestockitembatchid_bk in (43910096370179269)