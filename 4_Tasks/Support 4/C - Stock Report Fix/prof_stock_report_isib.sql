
select idIntransitStockItemBatch_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
into DW_GetLenses_jbs.dbo.stock_report_isib_20200531_1
from Warehouse.stock.dim_intransit_stock_item_batch_hist
where idCalendarStockDate_sk_fk = 20200531

select idIntransitStockItemBatch_sk_fk, idCalendarStockDate_sk_fk, qty_on_hand
into DW_GetLenses_jbs.dbo.stock_report_isib_20200531_2
from Warehouse.stock.dim_intransit_stock_item_batch_hist_full
where idCalendarStockDate_sk_fk = 20200531

	select count(*), count(distinct idIntransitStockItemBatch_sk_fk), sum(qty_on_hand)
	from DW_GetLenses_jbs.dbo.stock_report_isib_20200531_1

	select count(*), count(distinct idIntransitStockItemBatch_sk_fk), sum(qty_on_hand)
	from DW_GetLenses_jbs.dbo.stock_report_isib_20200531_2

	select top 10000 count(*) over (), 
		t1.idIntransitStockItemBatch_sk_fk, t2.idIntransitStockItemBatch_sk_fk, t1.qty_on_hand, t2.qty_on_hand, 
		sum(t1.qty_on_hand) over (), sum(t2.qty_on_hand) over (), sum(t1.qty_on_hand - t2.qty_on_hand) over (), 
		isib.intransitstockitembatchid_bk, isib.qty_registered
	from 
			DW_GetLenses_jbs.dbo.stock_report_isib_20200531_1 t1
		full join
			DW_GetLenses_jbs.dbo.stock_report_isib_20200531_2 t2 on t1.idIntransitStockItemBatch_sk_fk = t2.idIntransitStockItemBatch_sk_fk
		inner join
			Warehouse.stock.dim_intransit_stock_item_batch_v isib on isnull(t1.idIntransitStockItemBatch_sk_fk, t2.idIntransitStockItemBatch_sk_fk) = isib.idIntransitStockItemBatch_sk
	-- where t1.idIntransitStockItemBatch_sk_fk is null order by t2.qty_on_hand -- 0
	where t2.idIntransitStockItemBatch_sk_fk is null order by t1.qty_on_hand -- 155
	-- where t1.idIntransitStockItemBatch_sk_fk is not null and t1.idIntransitStockItemBatch_sk_fk is not null and t1.qty_on_hand = t2.qty_on_hand -- 0
	-- where t1.idIntransitStockItemBatch_sk_fk is not null and t1.idIntransitStockItemBatch_sk_fk is not null and t1.qty_on_hand <> t2.qty_on_hand order by (t1.qty_on_hand - t2.qty_on_hand) -- 0


select idIntransitStockItemBatch_sk, intransitstockitembatchid_bk, stock_date, warehouse_name_from, product_id_magento, sku, qty_on_hand
from Warehouse.stock.dim_intransit_stock_item_batch_hist_v
where intransitstockitembatchid_bk in (96827391988963773)
	-- idIntransitStockItemBatch_sk_fk in (438969)
	-- and stock_date = '2020-05-31'
order by intransitstockitembatchid_bk, stock_date

select idIntransitStockItemBatch_sk, intransitstockitembatchid_bk, stock_date, warehouse_name_from, product_id_magento, sku, qty_on_hand
from Warehouse.stock.dim_intransit_stock_item_batch_hist_full_v
where intransitstockitembatchid_bk in (96827391988963773)
	-- idIntransitStockItemBatch_sk_fk in (438969)
	-- and stock_date = '2020-05-31'
order by intransitstockitembatchid_bk, stock_date