
	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, datediff(minute, spm.startTime, spm.finishTime) duration,
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_stock_wh_stock_item' 
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime desc, spm.messageTime

	select *
	from Landing.aux.stock_warehousestockitem

		select distinct warehousestockitemid
		from Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud
		union
		select distinct warehousestockitemid
		from
				(select distinct warehousestockitembatchid
				from Landing.mend.wh_batchstockmovement_warehousestockitembatch) bsm_wsib
			inner join
				Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsib_wsi on bsm_wsib.warehousestockitembatchid = wsib_wsi.warehousestockitembatchid

	select top 1000 wsi.warehousestockitemid warehousestockitemid_bk,
		wsi.warehouseid_bk, wsi.stockitemid_bk, wsi.stock_method_type_bk, 
		wsi.wh_stock_item_description, 

		isnull(wsib.qty_received, 0) qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, isnull(wsib.qty_issued_stock, 0) qty_issued_stock, 
		wsi.qty_on_hold, isnull(wsib.qty_registered, 0) qty_registered, isnull(wsib.qty_disposed, 0) qty_disposed, wsi.qty_due_in, wsi.qty_forward_demand,
		wsi.stocked, 
		case when (wsi2.id is null) then 'Y' else 'N' end delete_f,
		1
	select count(*)
	from
			(select warehousestockitemid, 
				warehouseid warehouseid_bk, stockitemid stockitemid_bk, stockingmethod stock_method_type_bk, 
				id_string wh_stock_item_description, 

				availableqty qty_available, outstandingallocation qty_outstanding_allocation, allocatedstockqty qty_allocated_stock, 
				onhold qty_on_hold, dueinqty qty_due_in, forwarddemand qty_forward_demand,
				stocked stocked
			from 
					Landing.mend.gen_wh_warehousestockitem_v wsi
				inner join	
					Landing.aux.mag_prod_product_family_v pf on wsi.productfamilyid = pf.productfamilyid) wsi
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsi.warehousestockitemid = awsi.warehousestockitemid
		left join
			(select warehousestockitemid_bk, 
				sum(qty_received) qty_received, sum(qty_issued_stock) qty_issued_stock, 
				sum(qty_registered_sm) qty_registered, sum(qty_disposed_sm) qty_disposed
			from Landing.aux.mend_stock_warehousestockitembatch_v
			where delete_f = 'N'
			group by warehousestockitemid_bk) wsib on wsi.warehousestockitemid = wsib.warehousestockitemid_bk
		left join
			Landing.mend.wh_warehousestockitem wsi2 on wsi.warehousestockitemid = wsi2.id

-----------------------------------------

-- WSI with not WSIB: Parameters that have never been asked
select count(*), count(distinct id)
from Landing.mend.wh_warehousestockitem_aud

select count(*)
from Landing.mend.gen_wh_warehousestockitem_v

select top 1000 count(*) over (), t1.*
from 
		Landing.mend.gen_wh_warehousestockitem_v t1
	left join
		(select distinct warehousestockitemid
		from Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud) t2 on t1.warehousestockitemid = t2.warehousestockitemid
where t2.warehousestockitemid is null

-----------------------------------------

select top 1000 *
from Landing.aux.mend_stock_warehousestockitembatch_v

	select top 1000 count(*)
	from Landing.aux.mend_stock_warehousestockitembatch_v
