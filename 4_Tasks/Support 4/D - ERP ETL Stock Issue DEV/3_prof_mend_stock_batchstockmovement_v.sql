select top 1000 *
from Landing.aux.mend_stock_batchstockmovement_v

	select top 1000 count(*)
	from Landing.aux.mend_stock_batchstockmovement_v



	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		case 
			when (stock_adjustment_type_bk in ('Manual_Positive')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_2')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement <> wsib.receivedquantity) then qty_movement 
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement = wsib.receivedquantity 
				and wsib.quantityaccepted is not null and wsib.quantityaccepted = wsib.receivedquantity) then qty_movement
			else 0 
		end qty_registered, 
		case when (stock_adjustment_type_bk in ('Negative', 'Manual_Negative')) then qty_movement else 0 end qty_disposed, 
		-- case when (bsm.batch_movement_date between stp.from_date and stp.to_date) then dateadd(hour, 1, bsm.batch_movement_date) else bsm.batch_movement_date end batch_movement_date,
		batch_movement_date, batch_movement_create_date, 	
		movement_reason, movement_comment, operator,
		case when (wsib2.id is not null) then 'N' else 'Y' end delete_f
	from 
			(select batchstockmovementid batchstockmovementid_bk, 
				batchstockmovement_id batch_stock_move_id, 
				case 
					when (moveid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (moveid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else moveid
				end	move_id, 
				case 
					when (movelineid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (movelineid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else movelineid
				end	move_line_id, 	

				warehousestockitembatchid warehousestockitembatchid_bk, 
				case when (stockadjustment is null) then batchstockmovementtype else stockadjustment end stock_adjustment_type_bk, 
				case when (movetype is null) then 'MANUAL' else movetype end stock_movement_type_bk, 
				smp.stock_movement_period stock_movement_period_bk, 
	
				quantity qty_movement, 
				case 
					when (comment like '%Patch%62%') then warehousestockitembatch_createdate 
					else coalesce(batchstockmovement_createdate, warehousestockitembatch_createdate) -- current stock report logic (without stockmovement_createdate)
					-- else coalesce(batchstockmovement_createdate, stockmovement_createdate, warehousestockitembatch_createdate) 
				end batch_movement_date, 
				isnull(batchstockmovement_createdate, stockmovement_createdate) batch_movement_create_date, 
				
				case
					when (operator = 'SYSTEM') then 'Stock Sync'
					when (batchstockmovementtype in ('Manual_Negative', 'Manual_Positive')) then 'Manual Stock Movement'	
					when (reasonid = '01') then 'Damages'
					when (reasonid = '02') then 'Stock Check'
					when (reasonid = '03') then 'Sales Sample'
					when (reasonid = '99') then 'Warehouse Adjust Only'
					when (reasonid = 'AU') then 'Audit'
					else 'Unknown'
				end movement_reason,
				comment movement_comment, 
				operator 					
			from 
					Landing.mend.gen_wh_batchstockmovement_v bsm
				inner join
					Landing.map.erp_stock_movement_period_aud smp on isnull(bsm.batchstockmovement_createdate, bsm.stockmovement_createdate) between smp.period_start and smp.period_finish
			) bsm
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.map.sales_summer_time_period_aud stp on year(bsm.batch_movement_date) = stp.year
		left join
			Landing.mend.wh_warehousestockitembatch_full wsib2 on bsm.warehousestockitembatchid_bk = wsib2.id