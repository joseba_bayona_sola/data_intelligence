
-- 12633
select count(*) over (), 
	CreatedAt, Shipment_number, substring(shipment_number, 1, charindex('-', Shipment_number)-1) order_no,
	OrderStatus, Status, Warehouse, ShippingMethod, ScurriShippingMethod, ShippingServiceChanged, ShippingCountry, ExpectedShippingDate
from DW_GetLenses_jbs.dbo.snap_expected_fine

-- 4420
select count(*) over (), 
	CreatedAt, Shipment_number, substring(shipment_number, 1, charindex('-', Shipment_number)-1) order_no,
	OrderStatus, Status, Warehouse, ShippingMethod, ScurriShippingMethod, ShippingServiceChanged, ShippingCountry, ExpectedShippingDate
from DW_GetLenses_jbs.dbo.snap_expected_not_fine

select order_id_bk, invoice_id, shipment_id, order_no, order_date, shipment_date, store_name, 
	customer_id, customer_email, order_status_name
into #snap_fail_orders
from Warehouse.sales.dim_order_header_v
where order_date > '2020-08-13'
order by order_id_bk

select o.*, t.shipment_number, t.createdAt, t.expectedShippingDate
from
		(select *
		from #snap_fail_orders
		where invoice_id is not null and 
			shipment_id is null and order_status_name = 'OK'
			) o
	inner join
		DW_GetLenses_jbs.dbo.snap_expected_not_fine t on o.order_no = substring(t.shipment_number, 1, charindex('-', t.Shipment_number)-1) -- 4227
		-- DW_GetLenses_jbs.dbo.snap_expected_fine t on o.order_no = substring(t.shipment_number, 1, charindex('-', t.Shipment_number)-1) -- 9110
-- where t.Shipment_number is null
order by o.order_id_bk
