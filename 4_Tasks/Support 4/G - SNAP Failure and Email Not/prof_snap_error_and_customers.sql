
drop table #snap_fail_orders
go
drop table #snap_customers
go
drop table #snap_customers_feed
go



select order_id_bk, invoice_id, shipment_id, order_no, order_date, shipment_date, store_name, 
	customer_id, customer_email, order_status_name
into #snap_fail_orders
from Warehouse.sales.dim_order_header_v
-- where order_date > '2020-08-13' and order_date < '2020-08-18'
-- where order_date > '2020-08-18' and order_date < '2020-08-19'
where order_date > '2020-08-19' and order_date < '2020-08-20'
order by order_id_bk

	select top 1000 *
	from #snap_fail_orders
	order by order_id_bk

	select top 1000 convert(date, order_date), count(*), count(invoice_id), count(shipment_id)
	from #snap_fail_orders
	group by convert(date, order_date)
	order by convert(date, order_date)

	select top 1000 convert(date, order_date), order_status_name, count(*), count(shipment_id)
	from #snap_fail_orders
	group by convert(date, order_date), order_status_name
	order by convert(date, order_date), order_status_name

	select top 1000 *
	from #snap_fail_orders
	where invoice_id is not null and 
		shipment_id is null and order_status_name = 'OK'
	order by order_id_bk

		select top 1000 count(*), count(distinct customer_id)
		from #snap_fail_orders
		where invoice_id is not null and 
			shipment_id is null and order_status_name = 'OK'

	select c.customer_id, c2.customer_email, c2.personContactId, 
		c2.customer_unsubscribe_name, c2.customer_unsubscribe_dotmailer_name, c2.customer_unsubscribe_sf_mc_name
	into #snap_customers
	from
		(select distinct customer_id
		from #snap_fail_orders
		where invoice_id is not null and 
			shipment_id is null and order_status_name = 'OK') c
	inner join
		Warehouse.gen.dim_customer_scd_v c2 on c.customer_id = c2.customer_id_bk

	select c1.customer_id, c1.personContactId, c1.customer_email, c2.firstname, c2.lastname, c2.email_origin website,
		c2.last_order_date,
		c1.customer_unsubscribe_name, c1.customer_unsubscribe_dotmailer_name, c1.customer_unsubscribe_sf_mc_name, f.SF_CUSTOMER_ID
	into #snap_customers_feed
	from 
			#snap_customers c1
		inner join
			Warehouse.dm.fact_customer_signature_dm c2 on c1.customer_id = c2.client_id
		left join
			Warehouse.dm.SFMC_feed_v f on c1.customer_id = f.client_id
	
	select customer_id, personContactId SF_CUSTOMER_ID, customer_email, firstname, lastname, website, 
		case when (SF_CUSTOMER_ID is null) then 'N' else 'Y' end customer_in_feed
	from #snap_customers_feed
	-- where SF_CUSTOMER_ID is null 
	-- where SF_CUSTOMER_ID is null and customer_unsubscribe_dotmailer_name = 'N'
	-- where SF_CUSTOMER_ID is not null and customer_unsubscribe_dotmailer_name = 'Y'
	order by website, customer_id
