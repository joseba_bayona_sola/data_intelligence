			
-- create new odbc datasource using PostgreSQL ODBC unicode

-- create new linked server: MENDIX_DB_RESTORE_UNI
			
			select id, 
				status, reference, creditnoteno, netsuiteno, 
				processeddate, comment,
				createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
			from openquery(MENDIX_DB_RESTORE_UNI,'
				select id, 
					status, reference, creditnoteno, netsuiteno, 
					processeddate, comment,
					createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
				from public."invoicereconciliation$creditnote"')
			order by id desc


drop table DW_GetLenses_jbs.dbo.invrec_creditnote

create table DW_GetLenses_jbs.dbo.invrec_creditnote (
	id								bigint NOT NULL,
	status							varchar(9),
	reference						varchar(200),
	creditnoteno					bigint,
	netsuiteno						varchar(200),
	processeddate					datetime2,
	comment							varchar(500),
	createddate						datetime2,
	totalamount						decimal(28,8),
	creditdate						datetime2,
	vatcreditamount					decimal(28,8),
	goodscreditamount				decimal(28,8),
	carriagecreditamount			decimal(28,8),
	misccreditamount				decimal(28,8));
go 


select id, 
	status, reference, creditnoteno, netsuiteno, 
	processeddate, comment,
	createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
from DW_GetLenses_jbs.dbo.invrec_creditnote
order by id desc

		insert into DW_GetLenses_jbs.dbo.invrec_creditnote (id, 
			status, reference, creditnoteno, netsuiteno, 
			processeddate, comment,
			createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount)

			select id, 
				status, reference, creditnoteno, netsuiteno, 
				processeddate, comment,
				createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
			from openquery(MENDIX_DB_RESTORE,'
				select id, 
					status, reference, creditnoteno, netsuiteno, 
					processeddate, comment,
					createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
				from public."invoicereconciliation$creditnote"');

--------------------------------------------------------------


			select id, 
				incrementID, magentoOrderID, orderStatus, 
				orderLinesMagento, orderLinesDeduped,
				_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
				allocatedDate, promisedShippingDate, promisedDeliveryDate,
				labelRenderer,  
				couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
				createdDate, changedDate
			from openquery(MENDIX_DB_RESTORE_UNI,
				'select id, incrementID, magentoOrderID, orderStatus, 
					orderLinesMagento, orderLinesDeduped, 
					_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
					allocatedDate, promisedShippingDate, promisedDeliveryDate,        
					labelRenderer,
					couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
					createdDate, changedDate         
				from public."orderprocessing$order"   
				where id = 48695171068693885
				')

