
select top 1000 order_id_bk, order_no, order_date, payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code
from Landing.aux.sales_dim_order_header
order by order_id_bk desc

	select top 1000 payment_method_code, payment_method_name_bk, cc_type_name_bk, count(*)
	from Landing.aux.sales_dim_order_header
	group by payment_method_code, payment_method_name_bk, cc_type_name_bk
	order by payment_method_code, payment_method_name_bk, cc_type_name_bk

	select cc_type_name_bk, count(*)
	from Landing.aux.sales_dim_order_header_aud
	group by cc_type_name_bk
	order by cc_type_name_bk

select top 1000 order_id_bk, order_no, order_date, payment_method_name, cc_type_name
from Warehouse.sales.dim_order_header_v
order by order_id_bk desc

select top 1000 entity_id, parent_id, method, amount_authorized, cc_type, cc_exp_year, cc_exp_month, cc_last4, cc_owner
from Landing.mag.sales_flat_order_payment
order by entity_id desc
	
	select top 1000 count(*) over (partition by parent_id) num_rep, 
		entity_id, parent_id, method, amount_authorized, cc_type, cc_exp_year, cc_exp_month, cc_last4, cc_owner
	from Landing.mag.sales_flat_order_payment_aud
	order by num_rep desc

	select top 1000 method, cc_type, count(*)
	from Landing.mag.sales_flat_order_payment_aud
	group by method, cc_type
	order by method, cc_type

select *
from Warehouse.sales.dim_cc_type

select *
from Landing.map.sales_cc_type_aud

select cc_type, count(*), min(ins_ts), max(ins_ts)
from Landing.mag.sales_flat_order_payment_aud
group by cc_type
order by cc_type


----------------------------------------
select *
from sales_flat_order_payment
where parent_id = 11716557
  
  select count(*), count(distinct parent_id)
  from sales_flat_order_payment
  
select *
from sales_payment_transaction
where order_id = 11716557
limit 1000

  select count(*), count(distinct order_id)
  from sales_payment_transaction

----------------------------------------
----------------------------------------

select top 1000 order_id_bk, order_no, order_date, payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code
from Landing.aux.sales_dim_order_header_aud
where order_id_bk = 11790576
order by order_id_bk desc

select top 1000 order_id_bk, order_no, order_date, payment_method_name, cc_type_name
from Warehouse.sales.dim_order_header_v
where order_id_bk = 11790576
order by order_id_bk desc

select top 1000 *
from Warehouse.sales.dim_order_header_wrk
where order_id_bk = 11790576
order by order_id_bk desc

select top 1000 *
from Warehouse.sales.dim_cc_type

select top 1000 *
from Landing.map.sales_cc_type_aud