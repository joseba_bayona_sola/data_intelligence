select top 1000 
	store_name, customer_id, product_id,
	UniqueID, UniqueIDQP, 
	order_no, invoice_no, 
	
	shipping_country_id, 

	document_type, document_date, document_day, document_time, document_hour, 
	
	shipping_carrier, 
	length_of_time_to_invoice, length_of_time_invoice_to_shipment, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer, 
	presc_verification_method, 

	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	line_weight, 
	sku, sku_for_parameters, 

	qty, 
	local_prof_fee, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, 

	global_prof_fee, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount,

	local_to_global_rate, 
	global_line_total_exc_vat_for_localtoglobal, local_line_total_exc_vat_for_localtoglobal, 

	document_count, product_count, line_count

from DW_Sales_Actual.dbo.Fact_Orders
where product_id = 2450 and store_name = 'visiondirect.co.uk'
	and document_date = '2016-10-11'
order by order_no desc

select document_type,
	count(*), min(document_time), max(document_time),
	sum(qty), sum(global_line_total_exc_vat), 
	sum(document_count), sum(product_count), sum(line_count)
from DW_Sales_Actual.dbo.Fact_Orders
where product_id = 2450 and store_name = 'visiondirect.co.uk'
	and document_date = '2016-10-11'
group by document_type
order by document_type;