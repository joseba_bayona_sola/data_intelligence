
select -- website_type, 
	store_name, 
	year(shipment_date), month(shipment_date),
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee,
	avg(local_to_global_rate) exchange_rate
from Warehouse.sales.fact_order_line_ship_v
where shipment_date between '2017-01-01' and '2017-10-01'
	and store_name = 'visiondirect.fr'
group by -- website_type, 
	year(shipment_date), month(shipment_date), store_name
order by year(shipment_date), month(shipment_date), store_name


select -- website_type, 
	store_name, 
	year(shipment_date_c), month(shipment_date_c),
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.tableau.fact_ol_ship_gen_v
where shipment_date_c between '2017-01-01' and '2017-10-01'
	and store_name = 'visiondirect.fr'
group by -- website_type, 
	year(shipment_date_c), month(shipment_date_c), store_name
order by year(shipment_date_c), month(shipment_date_c), store_name




		select *
		from Warehouse.sales.fact_order_line_ship_v
		where shipment_date between '2017-03-01' and '2017-04-01'
			and store_name = 'visiondirect.fr'
			and convert(date, shipment_date) <> shipment_date_c 
				and month(shipment_date) <> month(shipment_date_c)

		select *
		from Warehouse.sales.fact_order_line_v
		where order_id_bk = 5119387

------------------------------------------------------------------------------


select -- website_type, 
	store_name, 
	year(invoice_date), month(invoice_date),
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date_c between '2017-01-01' and '2017-10-01'
	and store_name = 'visiondirect.fr'
group by -- website_type, 
	year(invoice_date), month(invoice_date), store_name
order by year(invoice_date), month(invoice_date), store_name

select -- website_type, 
	store_name, 
	year(invoice_date_c), month(invoice_date_c),
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.tableau.fact_ol_inv_gen_v
where invoice_date_c between '2017-01-01' and '2017-10-01'
	and store_name = 'visiondirect.fr'
group by -- website_type, 
	year(invoice_date_c), month(invoice_date_c), store_name
order by year(invoice_date_c), month(invoice_date_c), store_name