
select min(shipment_date), max(shipment_date)
from vw_shipment_lines_shipment
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'


select 
  sum(local_line_total_exc_vat), sum(local_line_total_vat), sum(local_line_total_inc_vat), sum(local_prof_fee), 
  sum(global_line_total_exc_vat), sum(global_line_total_vat), sum(global_line_total_inc_vat), sum(global_prof_fee)
from vw_shipment_lines_shipment
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'

select 
  sum(local_line_total_exc_vat), sum(local_line_total_vat), sum(local_line_total_inc_vat), sum(local_prof_fee), 
  sum(global_line_total_exc_vat), sum(global_line_total_vat), sum(global_line_total_inc_vat), sum(global_prof_fee)
from vw_shipment_lines_creditmemo
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'

--------------------------------------------------

select product_id, 
  sum(local_line_total_exc_vat), sum(local_line_total_vat), sum(local_line_total_inc_vat), sum(local_prof_fee)
from vw_shipment_lines_shipment
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'
group by product_id
order by product_id

select line_id, shipment_id, store_name, shipment_no, shipment_date, 
  product_id, 
  local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, local_prof_fee
  -- global_line_total_exc_vat, global_line_total_vat, global_line_total_inc_vat, global_prof_fee, 
  prof_fee_percent, vat_percent_before_prof_fee
from vw_shipment_lines_shipment
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'
  and product_id = 1090
order by store_name, shipment_date

select *
from dw_shipment_lines
where shipment_id = 4986439

select local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, local_prof_fee
from shipment_lines_vat
where item_id in (8822221, 8822222)

--------------------------------------------------

select line_id, shipment_id, store_name, shipment_no, shipment_date, 
  product_id, 
  local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, local_prof_fee, 
  global_line_total_exc_vat, global_line_total_vat, global_line_total_inc_vat, global_prof_fee, 
  prof_fee_percent, vat_percent_before_prof_fee
from vw_shipment_lines_shipment
where store_name = 'visiondirect.co.uk'
  and shipment_date between '2017-08-01' and '2017-09-01'
  and shipment_id = 4991060