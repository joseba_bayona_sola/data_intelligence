select ptv.product_type_vat, category_name, 
	product_id_magento, product_family_name, pfr.prof_fee_rate
from 
		Warehouse.prod.dim_product_family_v pf
	inner join
		Landing.aux.prod_product_product_type_vat ptv on pf.product_id_magento = ptv.product_id
	inner join	
		Landing.map.vat_prof_fee_rate pfr on ptv.product_type_vat = pfr.product_type_vat and pfr.countries_registered_code = 'UK-GB'
order by ptv.product_type_vat, category_name, product_id_magento
