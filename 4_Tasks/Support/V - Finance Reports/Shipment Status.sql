
select store_name, 
	order_id_bk, order_no, 
	invoice_no, invoice_date_c, 
	order_status_magento_name, 
	global_total_exc_vat - global_total_prof_fee global_total_exc_vat_after_prof_fee, 
	local_total_exc_vat - local_total_prof_fee local_total_exc_vat_after_prof_fee, 
	shipment_date
from Warehouse.sales.fact_order_line_inv_v
where invoice_date_c between '2017-06-01' and '2017-08-31'
	and (shipment_date > '2017-09-01' or shipment_date is null)
	and order_status_magento_name <> 'closed'
order by order_id_bk

select top 1000 count(*), 
	sum(global_total_exc_vat - global_total_prof_fee) global_total_exc_vat_after_prof_fee, 
	sum(local_total_exc_vat - local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date_c between '2017-06-01' and '2017-08-31'
	and (shipment_date > '2017-09-01' or shipment_date is null)
	and order_status_magento_name <> 'closed'

