
select order_line_id_bk, order_line_id_bk_c, order_id_bk, order_id_bk_c, order_no, 
	shipment_id, shipment_no, shipment_date, shipment_date_c, 
	-- website_type, 
	store_name, 
	product_id_magento, 
	local_total_exc_vat, local_total_vat, local_total_inc_vat, local_total_prof_fee,
	global_total_exc_vat, global_total_vat, global_total_inc_vat, global_total_prof_fee, 
	local_to_global_rate
from Warehouse.sales.fact_order_line_ship_vn
where shipment_date between '2017-08-01' and '2017-09-01'
	-- and order_line_id_bk_c is null
order by shipment_date


select -- website_type, 
	store_name, 
	sum(global_total_exc_vat) global_total_exc_vat, sum(global_total_vat) global_total_vat, sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_prof_fee) global_total_prof_fee, 
	sum(global_total_exc_vat) - sum(global_total_prof_fee) global_total_exc_vat_after_prof_fee, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee,
	avg(local_to_global_rate) exchange_rate
from Warehouse.sales.fact_order_line_ship_vn
where shipment_date between '2017-09-01' and '2017-10-01'
group by -- website_type, 
	store_name
order by global_total_exc_vat desc, -- website_type, 
	store_name


------------------------------------------------

select product_id_magento, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee,
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee,
	sum(global_total_exc_vat) global_total_exc_vat, sum(global_total_vat) global_total_vat, sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_prof_fee) global_total_prof_fee, 
	sum(global_total_exc_vat) - sum(global_total_prof_fee) global_total_exc_vat_after_prof_fee, 
	avg(local_to_global_rate) exchange_rate
from Warehouse.sales.fact_order_line_ship_v
where shipment_date between '2017-08-01' and '2017-09-01'
	and store_name = 'visiondirect.co.uk'
group by product_id_magento
order by product_id_magento


select order_line_id_bk, order_line_id_bk_c, order_id_bk, order_id_bk_c, order_no, 
	shipment_id, shipment_no, shipment_date, shipment_date_c, 
	website_type, store_name, 
	product_id_magento, 
	local_total_exc_vat, local_total_vat, local_total_inc_vat, local_total_prof_fee,
	global_total_exc_vat, global_total_vat, global_total_inc_vat, global_total_prof_fee, 
	local_to_global_rate
from Warehouse.sales.fact_order_line_ship_v
where shipment_date between '2017-08-01' and '2017-09-01'
	and product_id_magento = 1090 -- 1086
	and store_name = 'visiondirect.co.uk'
order by store_name, shipment_date

select *
from Warehouse.sales.fact_order_line_ship_v
where shipment_date between '2017-08-01' and '2017-09-01'
	-- and product_id_magento = 1090 -- 1086
	and store_name = 'visiondirect.co.uk'
	and shipment_id = 4986439
order by store_name, shipment_date
