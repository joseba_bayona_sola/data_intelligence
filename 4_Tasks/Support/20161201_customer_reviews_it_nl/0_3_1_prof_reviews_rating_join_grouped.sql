select re.entity_pk_value product_code, p.product_name, 
  -- re.created_at_mod review_date, 
  count(distinct re.customer_id), count(distinct re.review_id)
from 
    (select r.review_id, rd.store_id, st.name, r.created_at, DATE_FORMAT(r.created_at, '%d/%m/%Y') created_at_mod,
      r.entity_id, re.entity_code, r.status_id, rs.status_code, r.entity_pk_value, 
      rd.detail_id, rd.customer_id, rd.nickname, rd.title, rd.detail 
    from 
        magento01.review r
      inner join 
        magento01.review_entity re on r.entity_id = re.entity_id
      inner join 
        magento01.review_status rs on r.status_id = rs.status_id
      inner join 
        magento01.review_store rst on r.review_id = rst.review_id
      inner join 
        magento01.review_detail rd on r.review_id = rd.review_id and rst.store_id = rd.store_id
      inner join 
        magento01.core_store st ON rd.store_id = st.store_id) re      
  inner join
    (select rov.vote_id, 
      rov.review_id, rov.remote_ip, rov.remote_ip_long, rov.customer_id, rov.entity_pk_value, 
      rov.rating_id, re.entity_code, r.rating_code, rov.option_id, rov.percent, rov.value
    from 
        magento01.rating_option_vote rov
      inner join 
        magento01.rating r on rov.rating_id = r.rating_id
      inner join 
        magento01.rating_entity re on r.entity_id = re.entity_id) ra on re.review_id = ra.review_id
  inner join        
    (select pe.entity_id, pe.sku, pev.value product_name
    from 
        magento01.catalog_product_entity pe
      inner join 
        magento01.catalog_product_entity_varchar pev on pe.entity_id = pev.entity_id
      inner join 
        magento01.eav_attribute atr on pev.entity_type_id = atr.entity_type_id and pev.attribute_id = atr.attribute_id    
    where pev.store_id = 0
      and atr.attribute_code = 'name'
      and pev.value in ('Dailies Aquacomfort Plus', 'Frequency Xcel Toric', 'Biofinity Toric', 'Focus Dailies All Day Comfort', 'Biofinity', 
    	  'Acuvue Oasys', 'Air Optix for Astigmatism', 'Air Optix Aqua', 'Dailies Total 1', '1 Day Acuvue Moist for Astigmatism')) p on re.entity_pk_value = p.entity_id        
  inner join
    (select entity_id, email
    from magento01.customer_entity) c on re.customer_id = c.entity_id  
where re.name in ('visiondirect.it')
  -- and re.created_at > curdate() - interval 5 day
  and re.created_at between curdate() - interval 5 day and curdate()
  -- and re.created_at between curdate() - interval 1 day and curdate()
  -- and re.created_at > curdate()
group by re.entity_pk_value, p.product_name-- , re.created_at_mod 
order by re.entity_pk_value;

select re.entity_pk_value product_code, p.product_name, 
  -- re.created_at_mod review_date, 
  count(distinct re.customer_id), count(distinct re.review_id)
from 
    (select r.review_id, rd.store_id, st.name, r.created_at, DATE_FORMAT(r.created_at, '%d/%m/%Y') created_at_mod,
      r.entity_id, re.entity_code, r.status_id, rs.status_code, r.entity_pk_value, 
      rd.detail_id, rd.customer_id, rd.nickname, rd.title, rd.detail 
    from 
        magento01.review r
      inner join 
        magento01.review_entity re on r.entity_id = re.entity_id
      inner join 
        magento01.review_status rs on r.status_id = rs.status_id
      inner join 
        magento01.review_store rst on r.review_id = rst.review_id
      inner join 
        magento01.review_detail rd on r.review_id = rd.review_id and rst.store_id = rd.store_id
      inner join 
        magento01.core_store st ON rd.store_id = st.store_id) re      
  inner join
    (select rov.vote_id, 
      rov.review_id, rov.remote_ip, rov.remote_ip_long, rov.customer_id, rov.entity_pk_value, 
      rov.rating_id, re.entity_code, r.rating_code, rov.option_id, rov.percent, rov.value
    from 
        magento01.rating_option_vote rov
      inner join 
        magento01.rating r on rov.rating_id = r.rating_id
      inner join 
        magento01.rating_entity re on r.entity_id = re.entity_id) ra on re.review_id = ra.review_id
  inner join        
    (select pe.entity_id, pe.sku, pev.value product_name
    from 
        magento01.catalog_product_entity pe
      inner join 
        magento01.catalog_product_entity_varchar pev on pe.entity_id = pev.entity_id
      inner join 
        magento01.eav_attribute atr on pev.entity_type_id = atr.entity_type_id and pev.attribute_id = atr.attribute_id    
    where pev.store_id = 0
      and atr.attribute_code = 'name'
      and pev.value in ('Biofinity', 'Biofinity Toric', 'Dailies Aquacomfort Plus', 'Air Optix Aqua', '1 Day Acuvue Moist', 
    	  'Softens Daily Disposable',  'Focus Dailies All Day Comfort', 'Acuvue Oasys', 'Air Optix for Astigmatism', 'Acuvue Moist for Astigmatism')) p on re.entity_pk_value = p.entity_id        
  inner join
    (select entity_id, email
    from magento01.customer_entity) c on re.customer_id = c.entity_id  
where re.name in ('visiondirect.nl')
  -- and re.created_at > curdate() - interval 5 day
  and re.created_at between curdate() - interval 5 day and curdate()
  -- and re.created_at between curdate() - interval 1 day and curdate()
  -- and re.created_at > curdate()
group by re.entity_pk_value, p.product_name-- , re.created_at_mod 
order by re.entity_pk_value;