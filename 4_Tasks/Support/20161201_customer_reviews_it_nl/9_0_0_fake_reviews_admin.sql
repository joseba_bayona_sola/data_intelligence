
select *
from 
    (select r.review_id, rd.store_id, st.name, r.created_at, DATE_FORMAT(r.created_at, '%d/%m/%Y') created_at_mod,
      r.entity_id, re.entity_code, r.status_id, rs.status_code, r.entity_pk_value, 
      rd.detail_id, rd.customer_id, rd.nickname, rd.title, rd.detail 
    from 
        magento01.review r
      inner join 
        magento01.review_entity re on r.entity_id = re.entity_id
      inner join 
        magento01.review_status rs on r.status_id = rs.status_id
      inner join 
        magento01.review_store rst on r.review_id = rst.review_id
      inner join 
        magento01.review_detail rd on r.review_id = rd.review_id and rst.store_id = rd.store_id
      inner join 
        magento01.core_store st ON rd.store_id = st.store_id) re      
-- where store_id = 29       
-- where entity_pk_value = 1092
where store_id = 0
order by created_at
