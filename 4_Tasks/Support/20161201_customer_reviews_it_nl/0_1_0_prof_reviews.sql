
-- review_entity
select entity_id, entity_code
from magento01.review_entity
limit 100;

-- review_status
select status_id, status_code
from magento01.review_status
limit 100;

-- -------------------------------------------

-- review
select review_id, created_at, 
  entity_id, status_id, entity_pk_value
from magento01.review
order by created_at desc
limit 100;

  select entity_id, count(*)
  from magento01.review
  group by entity_id
  order by entity_id
  limit 100;

  select status_id, count(*)
  from magento01.review
  group by status_id
  order by status_id
  limit 100;

-- review_store
select review_id, store_id
from magento01.review_store
limit 100;

-- review_detail
select detail_id, review_id, store_id, title, detail, nickname, customer_id
from magento01.review_detail
limit 100;

-- -------------------------------------------

-- review_entity_summary
select primary_id, 
  entity_type, store_id, entity_pk_value,  
  reviews_count, rating_summary
from magento01.review_entity_summary
order by entity_pk_value, store_id
limit 100;


