
-- Cardinalities 
  select review_id, count(*)
  from magento01.rating_option_vote
  group by review_id
  order by count(*) desc, review_id;
 
select vote_id, 
  review_id, remote_ip, remote_ip_long, customer_id, entity_pk_value, 
  rating_id, option_id, percent, value
from magento01.rating_option_vote
where review_id in (4669, 5066, 4943, 4911)
order by review_id, rating_id
limit 100;

-- Join 
select rov.vote_id, 
  rov.review_id, rov.remote_ip, rov.remote_ip_long, rov.customer_id, rov.entity_pk_value, 
  rov.rating_id, re.entity_code, r.rating_code, rov.option_id, rov.percent, rov.value
from 
    magento01.rating_option_vote rov
  inner join 
    magento01.rating r on rov.rating_id = r.rating_id
  inner join 
    magento01.rating_entity re on r.entity_id = re.entity_id
order by rov.review_id desc, rov.rating_id
limit 100;




