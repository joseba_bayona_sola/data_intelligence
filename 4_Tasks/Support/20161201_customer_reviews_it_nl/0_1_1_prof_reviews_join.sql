
-- Cardinalities 

select review_id, count(*)
from magento01.review
group by review_id
order by count(*) desc, review_id
limit 100;

select review_id, count(*)
from magento01.review_store
group by review_id
order by count(*) desc, review_id
limit 100;

select review_id, count(*)
from magento01.review_detail
group by review_id
order by count(*) desc, review_id
limit 100;


-- Joins 
select r.review_id, rst.store_id, rd.store_id, st.name, r.created_at, 
  r.entity_id, re.entity_code, r.status_id, rs.status_code, r.entity_pk_value, 
  rd.detail_id, rd.customer_id, rd.nickname, rd.title, rd.detail 
from 
    magento01.review r
  inner join 
    magento01.review_entity re on r.entity_id = re.entity_id
  inner join 
    magento01.review_status rs on r.status_id = rs.status_id
  inner join 
    magento01.review_store rst on r.review_id = rst.review_id
  inner join 
    magento01.review_detail rd on r.review_id = rd.review_id and rst.store_id = rd.store_id
  inner join 
    magento01.core_store st ON rd.store_id = st.store_id       
-- where r.review_id = 24673
-- where r.status_id = 3
order by created_at desc
limit 100;

