
-- rating_entity
select entity_id, entity_code
from magento01.rating_entity
limit 100;

-- rating
select rating_id, entity_id, rating_code, position
from magento01.rating
limit 100;

-- rating_option
select option_id, 
  rating_id, code, value, position
from magento01.rating_option
order by rating_id, code
limit 100;

-- rating_title
select rating_id, store_id, value
from magento01.rating_title
order by store_id, rating_id 
limit 100;


-- -----------------------------------------------------------------

-- rating_option_vote
select vote_id, 
  review_id, remote_ip, remote_ip_long, customer_id, entity_pk_value, 
  rating_id, option_id, percent, value
from magento01.rating_option_vote
limit 100;

  select rating_id, option_id, percent, value, count(*)
  from magento01.rating_option_vote
  group by rating_id, option_id, percent, value
  order by rating_id, option_id, percent, value

-- rating_store
select rating_id, store_id
from magento01.rating_store
limit 100;


-- -----------------------------------------------------------------


-- rating_option_vote_aggregated
select primary_id, 
  store_id, entity_pk_value, rating_id, 
  vote_count, vote_value_sum, percent, percent_approved
from magento01.rating_option_vote_aggregated
order by store_id, entity_pk_value, rating_id
limit 100;



