select pe.entity_id, pe.sku, pev.value
from 
    magento01.catalog_product_entity pe
  inner join 
    magento01.catalog_product_entity_varchar pev on pe.entity_id = pev.entity_id
  inner join 
    magento01.eav_attribute atr on pev.entity_type_id = atr.entity_type_id and pev.attribute_id = atr.attribute_id    
where pev.store_id = 0
  and atr.attribute_code = 'name'
  and pev.value in ('Dailies Aquacomfort Plus', 'Frequency Xcel Toric', 'Biofinity Toric', 'Focus Dailies All Day Comfort', 'Biofinity', 
	'Acuvue Oasys', 'Air Optix for Astigmatism', 'Air Optix Aqua', 'Dailies Total 1', '1 Day Acuvue Moist for Astigmatism');
  
  
 -- Italy 
select entity_pk_value, product_name, count(*)
from 
  (select 
    re.review_id, re.store_id, re.name, re.created_at, 
    re.entity_id, re.status_id, re.status_code, re.entity_pk_value, p.product_name,
    re.detail_id, re.customer_id, re.nickname, re.title, re.detail, 
    ra.rating_id, ra.entity_code, ra.rating_code, ra.option_id, ra.percent, ra.value
  from 
      (select r.review_id, rd.store_id, st.name, r.created_at, 
        r.entity_id, re.entity_code, r.status_id, rs.status_code, r.entity_pk_value, 
        rd.detail_id, rd.customer_id, rd.nickname, rd.title, rd.detail 
      from 
          magento01.review r
        inner join 
          magento01.review_entity re on r.entity_id = re.entity_id
        inner join 
          magento01.review_status rs on r.status_id = rs.status_id
        inner join 
          magento01.review_store rst on r.review_id = rst.review_id
        inner join 
          magento01.review_detail rd on r.review_id = rd.review_id and rst.store_id = rd.store_id
        inner join 
          magento01.core_store st ON rd.store_id = st.store_id) re      
    inner join
      (select rov.vote_id, 
        rov.review_id, rov.remote_ip, rov.remote_ip_long, rov.customer_id, rov.entity_pk_value, 
        rov.rating_id, re.entity_code, r.rating_code, rov.option_id, rov.percent, rov.value
      from 
          magento01.rating_option_vote rov
        inner join 
          magento01.rating r on rov.rating_id = r.rating_id
        inner join 
          magento01.rating_entity re on r.entity_id = re.entity_id) ra on re.review_id = ra.review_id
    inner join        
      (select pe.entity_id, pe.sku, pev.value product_name
      from 
          magento01.catalog_product_entity pe
        inner join 
          magento01.catalog_product_entity_varchar pev on pe.entity_id = pev.entity_id
        inner join 
          magento01.eav_attribute atr on pev.entity_type_id = atr.entity_type_id and pev.attribute_id = atr.attribute_id    
      where pev.store_id = 0
        and atr.attribute_code = 'name'
        and pev.value in ('Dailies Aquacomfort Plus', 'Frequency Xcel Toric', 'Biofinity Toric', 'Focus Dailies All Day Comfort', 'Biofinity', 
      	'Acuvue Oasys', 'Air Optix for Astigmatism', 'Air Optix Aqua', 'Dailies Total 1', '1 Day Acuvue Moist for Astigmatism')) p on re.entity_pk_value = p.entity_id        
  -- where re.review_id = 26017
  where re.name in ('visiondirect.it') 
    and re.created_at > curdate() - interval 360 day) t
group by entity_pk_value, product_name
order by entity_pk_value; 

select 