
drop view tableau.fact_nl_cancellations_v
go 

create view tableau.fact_nl_cancellations_v as
	select t.customer_id, t.num_cancellations, t.rep_f, t.num_ok_orders,
		cs.website_create, rank_num_tot_orders, num_tot_orders,
		t.c_order_id_bk, oh_c.order_date c_order_date, oh_c.order_status_name c_order_status_name, oh_c.payment_method_name c_payment_method_name, 
		oh_c.local_total_exc_vat c_local_total_exc_vat, oh_c.global_total_exc_vat c_global_total_exc_vat, 
		t.order_id_bk, oh.order_date, oh.order_status_name, oh.payment_method_name, 
		oh.local_total_exc_vat, oh.global_total_exc_vat
	from
		(select distinct customer_id, c_order_id_bk, 
			num_cancellations, first_cancel_oh, c_local_total_inc_vat, c_local_total_exc_vat, 
			num_rep num_ok_orders, order_id_bk, 
			case when (num_rep) = 0 then 'N' else 'Y' end rep_f
		from
			(select t.customer_id, t.c_order_id_bk,
				t.num_cancellations, t.first_cancel_oh, t.c_local_total_inc_vat, t.c_local_total_exc_vat,
				count(co.order_id_bk) over (partition by t.customer_id) num_rep, 
				min(co.order_id_bk) over (partition by t.customer_id) order_id_bk
			from
					(select customer_id, min(order_id_bk) c_order_id_bk,
						count(*) num_cancellations, min(order_date) first_cancel_oh, 
						min(local_total_inc_vat) c_local_total_inc_vat, min(local_total_exc_vat) c_local_total_exc_vat
					from Warehouse.sales.dim_order_header_v
					where order_date > '2017-11-01' 
						and store_name = 'visiondirect.nl'
						and order_status_name = 'CANCEL'
					group by customer_id) t
				left join
					(select *	
					from Warehouse.sales.dim_order_header_v
					where order_date > '2017-11-01' 
						and store_name = 'visiondirect.nl') co on t.customer_id = co.customer_id and co.order_status_name <> 'CANCEL' and co.order_date > t.first_cancel_oh) t) t
		inner join
			Warehouse.act.dim_customer_signature_v cs on t.customer_id = cs.customer_id
		inner join
			Warehouse.sales.dim_order_header_v oh_c on t.c_order_id_bk = oh_c.order_id_bk
		left join
			Warehouse.sales.dim_order_header_v oh on t.order_id_bk = oh.order_id_bk
