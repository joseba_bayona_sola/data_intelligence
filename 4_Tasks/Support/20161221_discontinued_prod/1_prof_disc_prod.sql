
select product_id, product_name
from DW_GetLenses_jbs.dbo.ex_discontinued_products
order by product_id

select product_id, name
from DW_GetLenses.dbo.products
where store_name = 'default' and product_lifecycle = 'discontinued'
order by product_id, store_name

-- 

select p1.product_id, p2.product_id, 
	p1.product_name, p2.name
from
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) p1
	full join
		(select product_id, name
		from DW_GetLenses.dbo.products
		where store_name = 'default' and product_lifecycle = 'discontinued') p2 on p1.product_id = p2.product_id
--where p1.product_id is not null and p2.product_id is not null 
where p1.product_id is not null and p2.product_id is null 
--where p1.product_id is null and p2.product_id is not null 
order by p1.product_id, p2.product_id

select store_name, product_id, name, product_lifecycle
from DW_GetLenses.dbo.products
where product_id in (1127, 1178, 2407, 2408)
order by product_id, store_name