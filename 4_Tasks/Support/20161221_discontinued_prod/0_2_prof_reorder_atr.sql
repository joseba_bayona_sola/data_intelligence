
select document_date, reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, reorder_cc_exp_month, reorder_cc_exp_year, automatic_reorder
from DW_GetLenses.dbo.order_headers
where reorder_date is not null
order by document_date desc

	select RD, count(*) num
	from
		(select 
			case when (reorder_date is null) then 'NO_REORDER_DATE - NULL' else 'YES_REORDER_DATE - NOT NULL' end RD
		from DW_GetLenses.dbo.order_headers) t
	group by RD
	order by RD;

	select automatic_reorder, count(*) num
	from DW_GetLenses.dbo.order_headers
	-- where reorder_date is null
	group by automatic_reorder
	order by automatic_reorder;

	select reorder_on_flag, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by reorder_on_flag
	order by reorder_on_flag;

	select reorder_interval, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by reorder_interval
	order by reorder_interval;

