
select id, startDate, endDate, interval_days, next_order_date, customer_id
from DW_GetLenses_jbs.dbo.ex_reorder_customers_magento;

select id, startDate, cast(replace(startDate, '"', '') as datetime) startDate_mod, endDate, cast(replace(endDate, '"', '') as datetime) endDate_mod,
	interval_days, 
	next_order_date, cast(replace(next_order_date, '"', '') as datetime) next_order_date_mod,
	-- to_date(replace(next_order_date, '"', ''), 'YYYY-MM-DD HH:MI:SS') next_order_date_mod,
	customer_id
from DW_GetLenses_jbs.dbo.ex_reorder_customers_magento;

-------------------------

select oh.order_id, oh.order_no, oh.document_id, oh.document_type, oh.order_date, oh.document_date, 
	oh.customer_id, re.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, oh.customer_order_seq_no,
	oh.automatic_reorder, oh.reorder_date, oh.reorder_interval, oh.reorder_profile_id,
	oh.reminder_type, oh.reminder_date, oh.reminder_period, 
	re.interval_days, cast(replace(re.next_order_date, '"', '') as datetime) next_order_date_mod
from 
		DW_GetLenses.dbo.order_headers oh
	inner join
		DW_GetLenses_jbs.dbo.ex_reorder_customers_magento re on oh.reorder_profile_id = re.id
order by oh.customer_id, oh.order_date

