
--select top 1000 dp.product_id, dp.product_name,
--	ol.order_id, ol.order_no, ol.document_type, ol.order_date, ol.document_date, ol.qty,
--	oh.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, 
--	oh.automatic_reorder, oh.reorder_date, oh.reorder_interval, oh.reorder_profile_id,
--	oh.reminder_type, oh.reminder_date, oh.reminder_period

select distinct oh.customer_id, oh.customer_email, c.cus_phone, oh.customer_firstname, oh.customer_lastname, 
	dp.product_id, dp.product_name, oh.reminder_date
from 
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, qty,
			product_id, name
		from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, 
			customer_id, customer_email, customer_firstname, customer_lastname, 
			automatic_reorder, reorder_date, reorder_interval, reorder_profile_id,
			reminder_type, reminder_date, reminder_period
		from DW_GetLenses.dbo.order_headers) oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
	inner join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where reminder_date > getutcdate()
order by dp.product_id, dp.product_name, oh.customer_id;

--select top 1000 dp.product_id, dp.product_name,
--	ol.order_id, ol.order_no, ol.document_type, ol.order_date, ol.document_date, ol.qty,
--	oh.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, 
--	oh.automatic_reorder, oh.reorder_date, oh.reorder_interval, oh.reorder_profile_id,
--	re.interval_days, cast(replace(re.next_order_date, '"', '') as datetime) next_order_date_mod,
--	oh.reminder_type, oh.reminder_date, oh.reminder_period

select distinct oh.customer_id, oh.customer_email, c.cus_phone, oh.customer_firstname, oh.customer_lastname, 
	dp.product_id, dp.product_name, cast(replace(re.next_order_date, '"', '') as datetime) reorder_date
from 
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, qty,
			product_id, name
		from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, 
			customer_id, customer_email, customer_firstname, customer_lastname, 
			automatic_reorder, reorder_date, reorder_interval, reorder_profile_id,
			reminder_type, reminder_date, reminder_period
		from DW_GetLenses.dbo.order_headers) oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
	inner join
		DW_GetLenses_jbs.dbo.ex_reorder_customers_magento re on oh.reorder_profile_id = re.id
	inner join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
order by dp.product_id, dp.product_name, oh.customer_id;

