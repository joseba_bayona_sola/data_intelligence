
select top 1000 order_id, order_no, 
	document_id, document_type, 
	order_date, document_date,
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent
from DW_GetLenses.dbo.order_headers
where order_date > getutcdate() - 1
order by order_id, document_type

	-- Profiling Attributes
	select num_tot, reminder_type, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, *
		from DW_GetLenses.dbo.order_headers) t
	group by num_tot, reminder_type
	order by num desc

	select num_tot, reminder_period, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, *
		from DW_GetLenses.dbo.order_headers) t
	group by num_tot, reminder_period
	order by num desc

	select num_tot, yyyy, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, 
			YEAR(reminder_date) yyyy
		from DW_GetLenses.dbo.order_headers) t
	group by num_tot, yyyy
	order by num desc, yyyy

	select num_tot, reminder_presc, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, *
		from DW_GetLenses.dbo.order_headers) t
	group by num_tot, reminder_presc
	order by num desc

	select num_tot, reminder_sent, reminder_follow_sent, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, *
		from DW_GetLenses.dbo.order_headers) t
	group by num_tot, reminder_sent, reminder_follow_sent
	order by num desc

	-- Profiling reminder_period, reminder_date
	select top 1000 order_id, order_no, 
		document_id, document_type, 
		order_date, convert(date, dateadd(d, convert(int, reminder_period), order_date)) reminder_date_calc,
		reminder_type, reminder_date, reminder_period
	from DW_GetLenses.dbo.order_headers
	where order_date > getutcdate() - 1
	-- where reminder_period in (1, 3) -- Max Date (2014/05)
	order by order_id, document_type

	select order_id, order_no, 
		document_id, document_type, 
		order_date, 
		reminder_type, 
		reminder_date, reminder_date_calc, datediff(d, reminder_date, reminder_date_calc) diff, 
		reminder_period
	from 
		(select top 1000 order_id, order_no, 
			document_id, document_type, 
			order_date, convert(date, dateadd(d, convert(int, reminder_period), order_date)) reminder_date_calc,
			reminder_type, reminder_date, reminder_period
		from DW_GetLenses.dbo.order_headers
		where order_date > getutcdate() - 1) t
	order by order_id, document_type

	select num_tot, diff, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over() num_tot,
			order_id, order_no, 
			document_id, document_type, 
			order_date, 
			reminder_type, 
			reminder_date, reminder_date_calc, datediff(d, reminder_date, reminder_date_calc) diff, 
			reminder_period
		from 
			(select order_id, order_no, 
				document_id, document_type, 
				order_date, convert(date, dateadd(d, convert(int, reminder_period), order_date)) reminder_date_calc,
				reminder_type, reminder_date, reminder_period
			from DW_GetLenses.dbo.order_headers
			where order_date > getutcdate() - 720) t) t
	group by num_tot, diff
	order by num desc, diff

	-- Attributes Behavior in different Order Types