
select product_id, product_name
from DW_GetLenses_jbs.dbo.ex_discontinued_products
order by product_id

select top 1000 order_id, order_no, document_type, order_date, document_date,
	product_id, name
from DW_GetLenses.dbo.order_lines
where product_id = 1091
order by document_date desc

select top 1000 order_id, order_no, document_type, order_date, document_date, 
	customer_id, customer_email, customer_firstname, customer_lastname, 
	automatic_reorder, reorder_date, reorder_interval,
	reminder_type, reminder_date, reminder_period
from DW_GetLenses.dbo.order_headers
where order_id = 95699

---------------------------------------------------------------------------

select top 1000 ol.order_id, ol.order_no, ol.document_type, ol.order_date, ol.document_date,
	ol.product_id, ol.name
from 
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
	inner join
		(select order_id, order_no, document_type, order_date, document_date,
			product_id, name
		from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
order by ol.document_date desc

	select dp.product_id, dp.product_name, count(*), min(order_date), max(order_date)
	from 
			(select product_id, product_name
			from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
		inner join
			(select order_id, order_no, document_type, order_date, document_date,
				product_id, name
			from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	group by dp.product_id, dp.product_name
	order by dp.product_id, dp.product_name;

	select ol.product_id, ol.name, count(*), min(order_date), max(order_date)
	from 
			(select product_id, product_name
			from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
		inner join
			(select order_id, order_no, document_type, order_date, document_date,
				product_id, name
			from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	group by ol.product_id, ol.name
	order by ol.product_id, ol.name;

---------------------------------------------------------------------------

select top 1000 dp.product_id, dp.product_name,
	ol.order_id, ol.order_no, ol.document_type, ol.order_date, ol.document_date, ol.qty,
	oh.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, 
	oh.automatic_reorder, oh.reorder_date, oh.reorder_interval, oh.reorder_profile_id,
	oh.reminder_type, oh.reminder_date, oh.reminder_period
from 
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, qty,
			product_id, name
		from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, 
			customer_id, customer_email, customer_firstname, customer_lastname, 
			automatic_reorder, reorder_date, reorder_interval, reorder_profile_id,
			reminder_type, reminder_date, reminder_period
		from DW_GetLenses.dbo.order_headers) oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
where reminder_date > getutcdate()
	and oh.customer_id in (290, 487975)
order by ol.order_date desc, ol.document_date desc

	select dp.product_id, dp.product_name, count(*), min(oh.order_date), max(oh.order_date)
	from 
			(select product_id, product_name
			from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
		inner join
			(select order_id, order_no, document_id, document_type, order_date, document_date,
				product_id, name
			from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
		inner join
			(select order_id, order_no, document_id, document_type, order_date, document_date, 
				customer_id, customer_email, customer_firstname, customer_lastname, 
				automatic_reorder, reorder_date, reorder_interval,
				reminder_type, reminder_date, reminder_period
			from DW_GetLenses.dbo.order_headers) oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
	group by dp.product_id, dp.product_name
	order by dp.product_id, dp.product_name

---------------------------------------------------------------------------


select top 1000 dp.product_id, dp.product_name,
	ol.order_id, ol.order_no, ol.document_type, ol.order_date, ol.document_date, ol.qty,
	oh.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, oh.customer_order_seq_no,
	oh.automatic_reorder, oh.reorder_date, oh.reorder_interval, oh.reorder_profile_id,
	oh.reminder_type, oh.reminder_date, oh.reminder_period
from 
		(select product_id, product_name
		from DW_GetLenses_jbs.dbo.ex_discontinued_products) dp
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, qty,
			product_id, name
		from DW_GetLenses.dbo.order_lines) ol on dp.product_id = ol.product_id
	inner join
		(select order_id, order_no, document_id, document_type, order_date, document_date, 
			customer_id, customer_email, customer_firstname, customer_lastname, customer_order_seq_no,
			automatic_reorder, reorder_date, reorder_interval, reorder_profile_id,
			reminder_type, reminder_date, reminder_period
		from DW_GetLenses.dbo.order_headers) oh on ol.document_id = oh.document_id and ol.document_type = oh.document_type
where automatic_reorder = -1
	and oh.order_date > getutcdate() - 365
order by oh.customer_id, ol.order_date desc, ol.document_date desc

---------------------------------------------------------------------------

select order_id, order_no, document_id, document_type, order_date, document_date, 
	customer_id, customer_email, customer_firstname, customer_lastname, customer_order_seq_no,
	automatic_reorder, reorder_date, reorder_interval, reorder_profile_id,
	reminder_type, reminder_date, reminder_period
from DW_GetLenses.dbo.order_headers
where reorder_profile_id in (205, 1830815)
order by order_date