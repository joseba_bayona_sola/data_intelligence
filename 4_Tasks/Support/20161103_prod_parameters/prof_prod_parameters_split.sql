
select product_code, 
	product_id, name, 
	sku,
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
from DW_Sales_Actual.dbo.Dim_Order_Parameters
where product_id in (1177, 2315)
--where 
	--lens_cylinder = '-0.75'
	--lens_colour = 'Blue'
order by lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty

	select lens_power, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_power
	order by lens_power

	select replace(lens_power, Char(13), ''), count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by lens_power
	order by lens_power

	select replace(lens_power, Char(13), ''), count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Parameters
	group by replace(lens_power, Char(13), '')
	order by replace(lens_power, Char(13), '')

-------------------------------------------------

select top 100 
	-- count(*) over () num_tot, 
	-- count(*) over (partition by sku) num_sku,
	product_code, 
	product_id, name, 
	sku,
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
from DW_Sales_Actual.dbo.Dim_Order_Parameters
--where product_id not in (2327, 1243, 11)
where sku = 'BFSM03-BC8.6-DI14.0-PO+0.25-LQ3'
order by sku, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty

select num_sku, sku, product_id, name, product_code, count(*)
from 
	(select --count(*) over () num_tot, 
		count(*) over (partition by sku) num_sku,
		product_code, 
		product_id, name, 
		sku,
		lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
		qty
	from DW_Sales_Actual.dbo.Dim_Order_Parameters) t
where num_sku > 1
group by num_sku, sku, product_id, name, product_code
order by sku, product_id

	select rank() over (partition by product_id order by sku) rank_prod,
		*
	from 
		(select num_sku, sku, product_id, name, product_code, count(*) num
		from 
			(select --count(*) over () num_tot, 
				count(*) over (partition by sku) num_sku,
				product_code, 
				product_id, name, 
				sku,
				lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
				qty
			from DW_Sales_Actual.dbo.Dim_Order_Parameters) t
		where num_sku > 1
		group by num_sku, sku, product_id, name, product_code) t
	where t.num_sku = t.num
	order by sku, product_id

	select op.product_code, 
		op.product_id, op.name, 
		op.sku,
		op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour, 
		op.qty
	from 
			(select rank() over (partition by product_id order by sku) rank_prod,
				*
			from 
				(select num_sku, sku, product_id, name, product_code, count(*) num
				from 
					(select --count(*) over () num_tot, 
						count(*) over (partition by sku) num_sku,
						product_code, 
						product_id, name, 
						sku,
						lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
						qty
					from DW_Sales_Actual.dbo.Dim_Order_Parameters) t
				where num_sku > 1
				group by num_sku, sku, product_id, name, product_code) t
			where t.num_sku = t.num) t
		inner join 
			DW_Sales_Actual.dbo.Dim_Order_Parameters op on t.sku = op.sku
	where rank_prod < 4
	order by op.sku, op.product_id

-------------------------------------------------

select top 100 
	count(*) over (partition by product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty) num_dist,
	product_code, 
	product_id, name, 
	sku,
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
from DW_Sales_Actual.dbo.Dim_Order_Parameters
order by num_dist desc, product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty

select num_dist,
	product_id, name, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty, 
	count(*)
from 
	(select 
		count(*) over (partition by product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty) num_dist,
		product_code, 
		product_id, name, 
		sku,
		lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
		qty
	from DW_Sales_Actual.dbo.Dim_Order_Parameters) t
where num_dist > 1
group by num_dist, 
	product_id, name, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty
order by product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty

-------------------------------------------------

select top 100 
	count(*) over (partition by product_code, product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty) num_dist,
	product_code, 
	product_id, name, 
	sku,
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
from DW_Sales_Actual.dbo.Dim_Order_Parameters
order by num_dist desc, product_id, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, qty

-------------------------------------------------

-- 457.321

select top 100 --count(*) over (),
	t1.*
from 
	(select product_code, 
		product_id, name, 
		sku,
		lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
		qty
	from DW_Sales_Actual.dbo.Dim_Order_Parameters) t1
inner join 
	(select product_code, 
		product_id, name, 
		sku,
		lens_base_curve, lens_diameter, lens_power, replace(lens_power, Char(13), '') lens_power_mod, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
		qty
	from DW_Sales_Actual.dbo.Dim_Order_Parameters) t2 on t1.product_id = t2.product_id and t1.lens_base_curve = t2.lens_base_curve and t1.lens_diameter = t2.lens_diameter and t1.lens_cylinder = t2.lens_cylinder 
		and t1.lens_axis = t2.lens_axis and t1.lens_addition = t2.lens_addition and t1.lens_dominance = t2.lens_dominance and t1.lens_colour = t2.lens_colour and t1.qty = t2.qty
		and t1.lens_power <> t2.lens_power and t1.lens_power = t2.lens_power_mod
