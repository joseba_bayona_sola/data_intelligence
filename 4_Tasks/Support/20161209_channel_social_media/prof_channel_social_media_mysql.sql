
select *
from dw_order_channel
where channel in ('Social', 'Social Media')
order by channel, order_id;

select bis_channel, raf_channel, ga_channel, coupon_channel, channel, count(*)
from dw_order_channel
where channel in ('Social', 'Social Media')
group by bis_channel, raf_channel, ga_channel, coupon_channel, channel
order by channel, bis_channel, raf_channel, ga_channel, coupon_channel;

