
select *
from DW_GetLenses.dbo.invoice_headers
where store_name = 'visiondirect.nl'
	and document_date > GETUTCDATE() - 9
order by document_date

select business_channel, count(*)
from DW_GetLenses.dbo.invoice_headers
where store_name = 'visiondirect.nl'
	and document_date > GETUTCDATE() - 9
group by business_channel
order by business_channel

select *
from DW_GetLenses.dbo.invoice_headers
where store_name = 'visiondirect.nl'
	and document_date > GETUTCDATE() - 9
	and business_channel in ('Social', 'Social Media')
order by document_date

---------------------------------------------------------------------------

select *
from DW_GetLenses.dbo.invoice_headers
where document_date > GETUTCDATE() - 30
	and business_channel in ('Social', 'Social Media')
order by document_date

select business_channel, business_source, affilCode, count(*)
from DW_GetLenses.dbo.invoice_headers
where document_date > GETUTCDATE() - 120
	and business_channel in ('Social', 'Social Media')
group by business_channel, business_source, affilCode
order by business_channel, business_source, affilCode


