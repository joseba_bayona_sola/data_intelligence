﻿
select orderstatus, count(*)
from orderprocessing$order
group by orderstatus
order by orderstatus

-- 32217
select count(*)
from orderprocessing$order
where orderstatus = 'Pending'

-- 103355 - 103355 - 103355 - 103352 - 103352
select count(*)
from
	orderprocessing$order o
inner join
	orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
inner join
	orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
inner join
	orderprocessing$orderlineabstract ola on ol.id = ola.id		
inner join
	orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid
inner join
	product$product p on olp.product$productid = p.id		
where orderstatus = 'Pending'
	and p.isbom = false -- 28655 // 74697
	-- and ola.quantityordered <> ola.quantityallocated -- 16778


select o.id, 
	o.magentoorderid, o.incrementid, o.createdat, o.orderstatus, 
	ola.id, p.description, p.isbom,
	ola.quantityordered, ola.quantityallocated
from 
	orderprocessing$order o
inner join
	orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
inner join
	orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
inner join
	orderprocessing$orderlineabstract ola on ol.id = ola.id	
inner join
	orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid	
inner join
	product$product p on olp.product$productid = p.id	
where o.orderstatus = 'Pending'
	-- and ola.quantityordered <> ola.quantityallocated
order by createdat desc, ola.id
limit 1000


---------------------------------------------------------------------------

-- 18805 - 18805
select count(*)
from 
		purchaseorders$orderlineshortage ols
	inner join
		purchaseorders$orderlineshortage_orderline olsol on ols.id = olsol.purchaseorders$orderlineshortageid
		
-- 18805
select count(*)
from purchaseorders$orderlineshortage_orderline

select olsol.orderprocessing$orderlineid, ols.unitquantity
from 
		purchaseorders$orderlineshortage ols
	inner join
		purchaseorders$orderlineshortage_orderline olsol on ols.id = olsol.purchaseorders$orderlineshortageid
limit 1000

select t.id_o, t.id_ol, t.id_ola,
	t.magentoorderid, t.incrementid, t.createdat, t.orderstatus, 
	t.description, t.isbom,
	t.quantityordered, t.quantityallocated, 
	ols.unitquantity
from
	(select o.id id_o, ol.id id_ol,
		o.magentoorderid, o.incrementid, o.createdat, o.orderstatus, 
		ola.id id_ola, p.description, p.isbom,
		ola.quantityordered, ola.quantityallocated
	from 
		orderprocessing$order o
	inner join
		orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
	inner join
		orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
	inner join
		orderprocessing$orderlineabstract ola on ol.id = ola.id	
	inner join
		orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid	
	inner join
		product$product p on olp.product$productid = p.id	
	where o.orderstatus = 'Pending') t
left join
	(select olsol.orderprocessing$orderlineid, ols.unitquantity
	from 
			purchaseorders$orderlineshortage ols
		inner join
			purchaseorders$orderlineshortage_orderline olsol on ols.id = olsol.purchaseorders$orderlineshortageid) ols on t.id_ol = ols.orderprocessing$orderlineid	
where t.isbom = false 
-- where (t.quantityordered <> t.quantityallocated) and (t.quantityordered <> t.quantityallocated + ols.unitquantity) 
order by createdat desc, id_ola
limit 1000

-- 103352 // 74697
select count(t.id_o)
from
	(select o.id id_o, ol.id id_ol,
		o.magentoorderid, o.incrementid, o.createdat, o.orderstatus, 
		ola.id id_ola, p.description, p.isbom,
		ola.quantityordered, ola.quantityallocated
	from 
		orderprocessing$order o
	inner join
		orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
	inner join
		orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
	inner join
		orderprocessing$orderlineabstract ola on ol.id = ola.id	
	inner join
		orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid	
	inner join
		product$product p on olp.product$productid = p.id	
	where o.orderstatus = 'Pending') t
left join
	(select olsol.orderprocessing$orderlineid, ols.unitquantity
	from 
			purchaseorders$orderlineshortage ols
		inner join
			purchaseorders$orderlineshortage_orderline olsol on ols.id = olsol.purchaseorders$orderlineshortageid) ols on t.id_ol = ols.orderprocessing$orderlineid	
where t.isbom = false 

---------------------------------------------------------------------------

-- 34344 - 34344
select count(*)
from 
		purchaseorders$bomshortage bs
	inner join
		purchaseorders$bomshortage_orderline bsol on bs.id = bsol.purchaseorders$bomshortageid
		
-- 34344
select count(*)
from purchaseorders$bomshortage_orderline

select bsol.orderprocessing$orderlineid, bs.parentquantity
from 
		purchaseorders$bomshortage bs
	inner join
		purchaseorders$bomshortage_orderline bsol on bs.id = bsol.purchaseorders$bomshortageid
limit 1000


select t.id_o, t.id_ol, t.id_ola,
	t.magentoorderid, t.incrementid, t.createdat, t.orderstatus, 
	t.description, t.isbom,
	t.quantityordered, t.quantityallocated, 
	bs.parentquantity
from
	(select o.id id_o, ol.id id_ol,
		o.magentoorderid, o.incrementid, o.createdat, o.orderstatus, 
		ola.id id_ola, p.description, p.isbom,
		ola.quantityordered, ola.quantityallocated
	from 
		orderprocessing$order o
	inner join
		orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
	inner join
		orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
	inner join
		orderprocessing$orderlineabstract ola on ol.id = ola.id	
	inner join
		orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid	
	inner join
		product$product p on olp.product$productid = p.id	
	where o.orderstatus = 'Pending') t
left join
	(select bsol.orderprocessing$orderlineid, bs.parentquantity
	from 
			purchaseorders$bomshortage bs
		inner join
			purchaseorders$bomshortage_orderline bsol on bs.id = bsol.purchaseorders$bomshortageid) bs on t.id_ol = bs.orderprocessing$orderlineid	
 
where t.isbom = true 
-- where (t.quantityordered <> t.quantityallocated) and (t.quantityordered <> t.quantityallocated + bs.parentquantity) 
order by createdat desc, id_ola
limit 1000

-- 103362 // 28665
select count(*)
from
	(select o.id id_o, ol.id id_ol,
		o.magentoorderid, o.incrementid, o.createdat, o.orderstatus, 
		ola.id id_ola, p.description, p.isbom,
		ola.quantityordered, ola.quantityallocated
	from 
		orderprocessing$order o
	inner join
		orderprocessing$orderline_order olo on o.id = olo.orderprocessing$orderid
	inner join
		orderprocessing$orderline ol on olo.orderprocessing$orderlineid = ol.id
	inner join
		orderprocessing$orderlineabstract ola on ol.id = ola.id	
	inner join
		orderprocessing$orderline_product olp on ol.id = olp.orderprocessing$orderlineabstractid	
	inner join
		product$product p on olp.product$productid = p.id	
	where o.orderstatus = 'Pending') t
left join
	(select bsol.orderprocessing$orderlineid, bs.parentquantity
	from 
			purchaseorders$bomshortage bs
		inner join
			purchaseorders$bomshortage_orderline bsol on bs.id = bsol.purchaseorders$bomshortageid) bs on t.id_ol = bs.orderprocessing$orderlineid	
 where t.isbom = true 

 ---------------------------------------------------------------------------

stockallocation$at_orderline

stockallocation$orderlinestockallocationtransaction (allocatedunits) (allocationtype, cancelled)