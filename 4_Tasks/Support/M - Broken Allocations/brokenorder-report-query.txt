

query 1:

SELECT "o"."orderstatus" AS "OrderStatus", 
"o"."createdat" AS "OrderDate", 
"o"."incrementid" AS "IncrementID", 
"o"."magentoorderid" AS "MagentoOrderID", 
"p"."description" AS "ProductDescription", 
"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", 
"dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
SUM(COALESCE("ols"."unitquantity", 0)) AS "ShortageTotal"
 FROM "orderprocessing$orderline" "ol"
 INNER JOIN "orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
 LEFT OUTER JOIN "purchaseorders$orderlineshortage_orderline" "a2purchaseorders$orderlineshortage_orderline" ON "a2purchaseorders$orderlineshortage_orderline"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "purchaseorders$orderlineshortage" "ols" ON "ols"."id" = "a2purchaseorders$orderlineshortage_orderline"."purchaseorders$orderlineshortageid"
 LEFT OUTER JOIN "orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
 LEFT OUTER JOIN "orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
 LEFT OUTER JOIN "product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"
 WHERE "p"."isbom" = FALSE AND "o"."orderstatus" = 'Pending'
 GROUP BY "ol"."id", 
"o"."orderstatus", 
"dj1orderprocessing$orderlineabstract"."quantityallocated", 
"dj1orderprocessing$orderlineabstract"."quantityordered", 
"o"."incrementid", 
"o"."magentoorderid", 
"o"."createdat", 
"p"."description"
 HAVING (SUM(COALESCE("ols"."unitquantity", 0)) + "dj1orderprocessing$orderlineabstract"."quantityallocated") != "dj1orderprocessing$orderlineabstract"."quantityordered"
 ORDER BY "OrderDate" ASC


 
 
SELECT 
	"o"."orderstatus" AS "OrderStatus",  "o"."createdat" AS "OrderDate", "o"."incrementid" AS "IncrementID", "o"."magentoorderid" AS "MagentoOrderID", 
	"p"."description" AS "ProductDescription", 
		"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", "dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
		
		SUM(COALESCE("ols"."unitquantity", 0)) AS "ShortageTotal"

FROM 
		"orderprocessing$orderline" "ol"
	INNER JOIN 
		"orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
	LEFT OUTER JOIN 
		"purchaseorders$orderlineshortage_orderline" "a2purchaseorders$orderlineshortage_orderline" ON "a2purchaseorders$orderlineshortage_orderline"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"purchaseorders$orderlineshortage" "ols" ON "ols"."id" = "a2purchaseorders$orderlineshortage_orderline"."purchaseorders$orderlineshortageid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
	LEFT OUTER JOIN 
		"product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"
		
WHERE "p"."isbom" = FALSE AND "o"."orderstatus" = 'Pending'

GROUP BY "ol"."id", 
	"o"."orderstatus", "o"."createdat", "o"."incrementid", "o"."magentoorderid", 
	"p"."description",
		"dj1orderprocessing$orderlineabstract"."quantityordered", "dj1orderprocessing$orderlineabstract"."quantityallocated" 

HAVING (SUM(COALESCE("ols"."unitquantity", 0)) + "dj1orderprocessing$orderlineabstract"."quantityallocated") != "dj1orderprocessing$orderlineabstract"."quantityordered"

ORDER BY "OrderDate" ASC
 

---------------------------------------------------------------------------------------------------------

 
query 2: 


SELECT "o"."orderstatus" AS "OrderStatus", 
"o"."createdat" AS "OrderDate", 
"o"."incrementid" AS "IncrementID", 
"o"."magentoorderid" AS "MagentoOrderID", 
"p"."description" AS "ProductDescription", 
"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", 
"dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
SUM(COALESCE("bs"."parentquantity", 0)) AS "ShortageTotal"
 FROM "orderprocessing$orderline" "ol"
 INNER JOIN "orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
 LEFT OUTER JOIN "purchaseorders$bomshortage_orderline" "a2purchaseorders$bomshortage_orderline" ON "a2purchaseorders$bomshortage_orderline"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "purchaseorders$bomshortage" "bs" ON "bs"."id" = "a2purchaseorders$bomshortage_orderline"."purchaseorders$bomshortageid"
 LEFT OUTER JOIN "orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
 LEFT OUTER JOIN "orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
 LEFT OUTER JOIN "product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"
 WHERE "p"."isbom" = TRUE AND "o"."orderstatus" = ?
 GROUP BY "ol"."id", 
"o"."orderstatus", 
"dj1orderprocessing$orderlineabstract"."quantityallocated", 
"dj1orderprocessing$orderlineabstract"."quantityordered", 
"o"."incrementid", 
"o"."magentoorderid", 
"o"."createdat", 
"p"."description"
 HAVING (SUM(DISTINCT COALESCE("bs"."parentquantity", 0)) + "dj1orderprocessing$orderlineabstract"."quantityallocated") != "dj1orderprocessing$orderlineabstract"."quantityordered"
 ORDER BY "OrderDate" ASC

 

SELECT 
	"o"."orderstatus" AS "OrderStatus", "o"."createdat" AS "OrderDate", "o"."incrementid" AS "IncrementID", "o"."magentoorderid" AS "MagentoOrderID", 
	"p"."description" AS "ProductDescription", 
		"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", "dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
		
		SUM(COALESCE("bs"."parentquantity", 0)) AS "ShortageTotal"

FROM 
		"orderprocessing$orderline" "ol"
	INNER JOIN 
		"orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
	LEFT OUTER JOIN 
		"purchaseorders$bomshortage_orderline" "a2purchaseorders$bomshortage_orderline" ON "a2purchaseorders$bomshortage_orderline"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"purchaseorders$bomshortage" "bs" ON "bs"."id" = "a2purchaseorders$bomshortage_orderline"."purchaseorders$bomshortageid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
	LEFT OUTER JOIN 
		"product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"

WHERE "p"."isbom" = TRUE AND "o"."orderstatus" = ?

GROUP BY "ol"."id", 
	"o"."orderstatus", "o"."createdat", "o"."incrementid", "o"."magentoorderid", 
	"p"."description", 
		"dj1orderprocessing$orderlineabstract"."quantityordered", "dj1orderprocessing$orderlineabstract"."quantityallocated", 

HAVING (SUM(DISTINCT COALESCE("bs"."parentquantity", 0)) + "dj1orderprocessing$orderlineabstract"."quantityallocated") != "dj1orderprocessing$orderlineabstract"."quantityordered"

ORDER BY "OrderDate" ASC

 
---------------------------------------------------------------------------------------------------------

 
query 3:

SELECT "o"."orderstatus" AS "OrderStatus", 
"o"."createdat" AS "OrderDate", 
"o"."incrementid" AS "IncrementID", 
"o"."magentoorderid" AS "MagentoOrderID", 
"p"."description" AS "ProductDescription", 
"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", 
"dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
SUM(COALESCE("olsat"."allocatedunits", 0)) AS "AllocationTotal"
 FROM "orderprocessing$orderline" "ol"
 INNER JOIN "orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
 LEFT OUTER JOIN "stockallocation$at_orderline" "a2stockallocation$at_orderline" ON "a2stockallocation$at_orderline"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "stockallocation$orderlinestockallocationtransaction" "olsat" ON "olsat"."id" = "a2stockallocation$at_orderline"."stockallocation$orderlinestockallocationtransactionid"
 LEFT OUTER JOIN "orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
 LEFT OUTER JOIN "orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
 LEFT OUTER JOIN "orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
 LEFT OUTER JOIN "product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"
 WHERE "p"."isbom" = FALSE AND ("olsat"."allocationtype" = ? OR "olsat"."allocationtype" = ?) AND "olsat"."cancelled" = FALSE
 GROUP BY "ol"."id", 
"o"."orderstatus", 
"dj1orderprocessing$orderlineabstract"."quantityallocated", 
"dj1orderprocessing$orderlineabstract"."quantityordered", 
"o"."incrementid", 
"o"."magentoorderid", 
"o"."createdat", 
"p"."description"
 HAVING SUM(COALESCE("olsat"."allocatedunits", 0)) != "dj1orderprocessing$orderlineabstract"."quantityallocated"
 ORDER BY "OrderDate" ASC

 
SELECT 
	"o"."orderstatus" AS "OrderStatus", "o"."createdat" AS "OrderDate", "o"."incrementid" AS "IncrementID", "o"."magentoorderid" AS "MagentoOrderID", 
	"p"."description" AS "ProductDescription", 
		"dj1orderprocessing$orderlineabstract"."quantityordered" AS "QuantityOrdered", "dj1orderprocessing$orderlineabstract"."quantityallocated" AS "QuantityAllocated", 
	
		SUM(COALESCE("olsat"."allocatedunits", 0)) AS "AllocationTotal"
 FROM 
		"orderprocessing$orderline" "ol"
	INNER JOIN 
		"orderprocessing$orderlineabstract" "dj1orderprocessing$orderlineabstract" ON "dj1orderprocessing$orderlineabstract"."id" = "ol"."id"
	LEFT OUTER JOIN 
		"stockallocation$at_orderline" "a2stockallocation$at_orderline" ON "a2stockallocation$at_orderline"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"stockallocation$orderlinestockallocationtransaction" "olsat" ON "olsat"."id" = "a2stockallocation$at_orderline"."stockallocation$orderlinestockallocationtransactionid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_order" "a3orderprocessing$orderline_order" ON "a3orderprocessing$orderline_order"."orderprocessing$orderlineid" = "ol"."id"
	LEFT OUTER JOIN 
		"orderprocessing$order" "o" ON "o"."id" = "a3orderprocessing$orderline_order"."orderprocessing$orderid"
	LEFT OUTER JOIN 
		"orderprocessing$orderline_product" "a4orderprocessing$orderline_product" ON "a4orderprocessing$orderline_product"."orderprocessing$orderlineabstractid" = "ol"."id"
	LEFT OUTER JOIN 
		"product$product" "p" ON "p"."id" = "a4orderprocessing$orderline_product"."product$productid"

	WHERE "p"."isbom" = FALSE AND ("olsat"."allocationtype" = ? OR "olsat"."allocationtype" = ?) AND "olsat"."cancelled" = FALSE
GROUP BY "ol"."id", 
	"o"."orderstatus", "o"."createdat", "o"."incrementid", "o"."magentoorderid", 
	"p"."description",
		"dj1orderprocessing$orderlineabstract"."quantityordered", "dj1orderprocessing$orderlineabstract"."quantityallocated"

HAVING SUM(COALESCE("olsat"."allocatedunits", 0)) != "dj1orderprocessing$orderlineabstract"."quantityallocated"

ORDER BY "OrderDate" ASC 