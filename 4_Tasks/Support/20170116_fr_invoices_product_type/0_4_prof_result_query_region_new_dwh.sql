
select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code = 'EU-FR'
	and order_date between '2018-01-01' and '2018-02-01'

select top 1000 *
from Landing.aux.sales_fact_order_line_aud

select order_id_bk, region_id_shipping_bk
into #fr_vat
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code = 'EU-FR'
	and invoice_date between '2018-01-01' and '2018-02-01'

select oh.order_id_bk, oh.invoice_id, oh.order_no, oh.invoice_no, oh.order_date, oh.invoice_date, 
	oh.country_id_shipping_bk, oh.region_id_shipping_bk, oh.region_name, oh.postcode_shipping, oh.order_status_name_bk, oh.order_type_name_bk, 
	ol.product_id_bk, ol.qty_unit, 
	ol.local_total_aft_refund_exc_vat, ol.local_total_aft_refund_vat, ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_prof_fee
from 
		(select order_id_bk, invoice_id, order_no, invoice_no, order_date, invoice_date, 
			country_id_shipping_bk, region_id_shipping_bk, r.region_name, postcode_shipping, order_status_name_bk, order_type_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				Warehouse.gen.dim_region r on oh.region_id_shipping_bk = r.region_id_bk
		where countries_registered_code = 'EU-FR'
			and invoice_date between '2018-01-01' and '2018-02-01') oh
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk

select sum(ol.local_total_aft_refund_exc_vat), sum(ol.local_total_aft_refund_vat), sum(ol.local_total_aft_refund_inc_vat), sum(ol.local_total_aft_refund_prof_fee), sum(ol.qty_unit)
from 
		(select order_id_bk, invoice_id, order_no, invoice_no, order_date, invoice_date, 
			country_id_shipping_bk, region_id_shipping_bk, r.region_name, postcode_shipping, order_status_name_bk, order_type_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				Warehouse.gen.dim_region r on oh.region_id_shipping_bk = r.region_id_bk
		where countries_registered_code = 'EU-FR'
			and invoice_date between '2018-01-01' and '2018-02-01') oh
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk

select sum(ol.local_total_aft_refund_exc_vat), sum(ol.local_total_aft_refund_vat), sum(ol.local_total_aft_refund_inc_vat), sum(ol.local_total_aft_refund_prof_fee), sum(ol.qty_unit)
from 
		#fr_vat oh
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk


------------------------------------------------

select sum(ol.local_total_aft_refund_exc_vat), sum(ol.local_total_aft_refund_vat), sum(ol.local_total_aft_refund_inc_vat), sum(ol.local_total_aft_refund_prof_fee), sum(ol.qty_unit), 
	count(*)
from 
		#fr_vat oh
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk

select sum(ol.local_total_exc_vat), sum(ol.local_total_vat), sum(ol.local_total_inc_vat), sum(ol.local_total_prof_fee), sum(ol.qty_unit), 
	count(*)
from 
		#fr_vat oh
	inner join
		Warehouse.sales.fact_order_line_inv_v ol on oh.order_id_bk = ol.order_id_bk