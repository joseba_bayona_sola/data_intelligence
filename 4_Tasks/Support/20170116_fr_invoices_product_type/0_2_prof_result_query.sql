
select oh.uniqueID code, 
	oh.invoice_no document_no, oh.document_type, oh.document_date, oh.order_no, oh.order_date, oh.invoice_date, oh.order_type,
	ol.product_type,
	ol.local_line_total_exc_vat, ol.local_line_total_vat, ol.local_line_total_inc_vat, ol.local_total_exc_vat_after_prof_fee, ol.line_weight
from
		(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
			store_name, invoice_id, invoice_no, document_id, document_type, cast(document_date as date) document_date, 
			order_no, cast(order_date as date) order_date, cast(invoice_date as date) invoice_date, order_type, 
			local_total_exc_vat, local_total_vat, local_total_inc_vat, total_qty, local_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, weight
		from DW_GetLenses.dbo.invoice_headers
		where shipping_country_id = 'FR'
			and document_date between '2016-12-01' and '2016-12-31'
			and document_no = '26000014434') oh
	inner join
		(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
			store_name, invoice_id, document_id, document_type, document_date, product_type,
			local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, local_line_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, line_weight
		from DW_GetLenses.dbo.invoice_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
order by oh.document_date

--- 

select code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type, 
	sum(local_line_total_exc_vat) local_line_total_exc_vat, sum(local_line_total_vat) local_line_total_vat, sum(local_line_total_inc_vat) local_line_total_inc_vat, 
	sum(qty) qty, sum(local_total_exc_vat_after_prof_fee) local_total_exc_vat_after_prof_fee, sum(line_weight) line_weight
from
	(select oh.uniqueID code, 
		oh.invoice_no document_no, oh.document_type, oh.document_date, oh.order_no, oh.order_date, oh.invoice_date, oh.order_type,
		ol.product_type,
		ol.local_line_total_exc_vat, ol.local_line_total_vat, ol.local_line_total_inc_vat, ol.qty, ol.local_total_exc_vat_after_prof_fee, ol.line_weight
	from
			(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
				store_name, invoice_id, invoice_no, document_id, document_type, cast(document_date as date) document_date, 
				order_no, cast(order_date as date) order_date, cast(invoice_date as date) invoice_date, order_type, 
				local_total_exc_vat, local_total_vat, local_total_inc_vat, total_qty, local_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, weight
			from DW_GetLenses.dbo.invoice_headers
			where shipping_country_id = 'FR'
				and document_date between '2016-11-01' and '2016-12-01'
				--and document_date between '2016-12-01' and '2017-01-01'
				--and document_no = '26000014278'
				) oh
		inner join
			(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
				store_name, invoice_id, document_id, document_type, document_date, product_type,
				local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, qty, local_line_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, line_weight
			from DW_GetLenses.dbo.invoice_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type) t
--where product_type is null
group by code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type
order by order_no, document_date 

------------------------------------------------------

select 
	sum(local_line_total_exc_vat) local_line_total_exc_vat, sum(local_line_total_vat) local_line_total_vat, sum(local_line_total_inc_vat) local_line_total_inc_vat, 
	sum(qty) qty, sum(local_total_exc_vat_after_prof_fee) local_total_exc_vat_after_prof_fee, sum(line_weight) line_weight
from 
	(select code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type, 
		sum(local_line_total_exc_vat) local_line_total_exc_vat, sum(local_line_total_vat) local_line_total_vat, sum(local_line_total_inc_vat) local_line_total_inc_vat, 
		sum(qty) qty, sum(local_total_exc_vat_after_prof_fee) local_total_exc_vat_after_prof_fee, sum(line_weight) line_weight
	from
		(select oh.uniqueID code, 
			oh.invoice_no document_no, oh.document_type, oh.document_date, oh.order_no, oh.order_date, oh.invoice_date, oh.order_type,
			ol.product_type,
			ol.local_line_total_exc_vat, ol.local_line_total_vat, ol.local_line_total_inc_vat, ol.qty, ol.local_total_exc_vat_after_prof_fee, ol.line_weight
		from
				(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
					store_name, invoice_id, invoice_no, document_id, document_type, cast(document_date as date) document_date, 
					order_no, cast(order_date as date) order_date, cast(invoice_date as date) invoice_date, order_type, 
					local_total_exc_vat, local_total_vat, local_total_inc_vat, total_qty, local_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, weight
				from DW_GetLenses.dbo.invoice_headers
				where shipping_country_id = 'FR'
					and document_date between '2016-11-01' and '2016-12-01'
					--and document_date between '2016-12-01' and '2017-01-01'
					--and document_no = '26000014278'
					) oh
			inner join
				(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
					store_name, invoice_id, document_id, document_type, document_date, product_type,
					local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, qty, local_line_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, line_weight
				from DW_GetLenses.dbo.invoice_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type) t
	group by code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type) t


--- 

select product_type,
	sum(local_line_total_exc_vat) local_line_total_exc_vat, sum(local_line_total_vat) local_line_total_vat, sum(local_line_total_inc_vat) local_line_total_inc_vat, 
	sum(qty) qty, sum(local_total_exc_vat_after_prof_fee) local_total_exc_vat_after_prof_fee, sum(line_weight) line_weight
from 
	(select code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type, 
		sum(local_line_total_exc_vat) local_line_total_exc_vat, sum(local_line_total_vat) local_line_total_vat, sum(local_line_total_inc_vat) local_line_total_inc_vat, 
		sum(qty) qty, sum(local_total_exc_vat_after_prof_fee) local_total_exc_vat_after_prof_fee, sum(line_weight) line_weight
	from
		(select oh.uniqueID code, 
			oh.invoice_no document_no, oh.document_type, oh.document_date, oh.order_no, oh.order_date, oh.invoice_date, oh.order_type,
			ol.product_type,
			ol.local_line_total_exc_vat, ol.local_line_total_vat, ol.local_line_total_inc_vat, ol.qty, ol.local_total_exc_vat_after_prof_fee, ol.line_weight
		from
				(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
					store_name, invoice_id, invoice_no, document_id, document_type, cast(document_date as date) document_date, 
					order_no, cast(order_date as date) order_date, cast(invoice_date as date) invoice_date, order_type, 
					local_total_exc_vat, local_total_vat, local_total_inc_vat, total_qty, local_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, weight
				from DW_GetLenses.dbo.invoice_headers
				where shipping_country_id = 'FR'
					and document_date between '2016-12-01' and '2017-01-01'
					--and document_no = '26000014278'
					) oh
			inner join
				(select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
					store_name, invoice_id, document_id, document_type, document_date, product_type,
					local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, qty, local_line_total_exc_vat - local_prof_fee local_total_exc_vat_after_prof_fee, line_weight
				from DW_GetLenses.dbo.invoice_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type) t
	group by code, document_no, document_type, document_date, order_no, order_date, invoice_date, order_type, product_type) t
group by product_type
order by product_type