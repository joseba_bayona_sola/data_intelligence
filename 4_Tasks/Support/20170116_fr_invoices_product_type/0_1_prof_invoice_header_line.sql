
select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
	store_name, document_id, invoice_id, document_type, document_date, order_no, order_date, invoice_date, order_type, 
	local_total_exc_vat, local_total_vat, local_total_inc_vat, total_qty, local_total_exc_vat - local_prof_fee, weight
	--global_line_total_vat, global_line_total_exc_vat, global_line_total_inc_vat, 

from DW_GetLenses.dbo.invoice_headers
where shipping_country_id = 'FR'
	and document_date between '2016-12-01' and '2016-12-31'
	and document_no = '26000014278'
order by document_date


select 'I'+ LEFT(document_type, 1) + cast(cast(document_id  AS int) AS varchar(255)) AS UniqueID,
	store_name, invoice_id, document_type, document_date, product_type,
	local_line_total_exc_vat, local_line_total_vat, local_line_total_inc_vat, local_line_total_exc_vat - local_prof_fee, line_weight

from DW_GetLenses.dbo.invoice_lines
where document_id = 4083562
order by document_date

select store_name, invoice_id, document_type, document_date, product_type,
	sum(local_line_total_exc_vat), sum(local_line_total_vat), sum(local_line_total_inc_vat), sum(local_line_total_exc_vat - local_prof_fee), sum(line_weight)

from DW_GetLenses.dbo.invoice_lines
where document_id = 4083562
group by store_name, invoice_id, document_type, document_date, product_type
order by document_date

