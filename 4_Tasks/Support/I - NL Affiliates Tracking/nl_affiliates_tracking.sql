
select status, origin, id, order_no, tp, country,
	affiliate_site, product_group, description, customer_email, 
	order_total, commission,
	device, ip, 
	reg_date, validation_date, click_date, click_sales
from DW_GetLenses_jbs.dbo.ex_nl_orders

	select id, order_no, tp, country,
		affiliate_site, order_total, commission,
		reg_date
	from DW_GetLenses_jbs.dbo.ex_nl_orders

	select nl.id, nl.order_no, nl.tp, nl.country,
		nl.affiliate_site, nl.order_total, nl.commission,
		nl.reg_date, 
		oh.order_id, oh.business_channel, oh.affilCode, oh.coupon_code, oh.customer_business_channel, oh.order_lifecycle, oh.customer_order_seq_no
	from 
			DW_GetLenses_jbs.dbo.ex_nl_orders nl
		left join
			(select store_name, order_id, order_no, document_type, document_date, 
				customer_id, customer_email, 
				business_channel, affilCode, coupon_code, customer_business_channel, order_lifecycle, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers) oh on nl.order_no = oh.order_no
	-- order by nl.affiliate_site, nl.reg_date
	order by nl.affiliate_site, oh.business_channel, nl.reg_date
