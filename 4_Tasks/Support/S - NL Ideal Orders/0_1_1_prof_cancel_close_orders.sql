
select count(*) over (partition by customer_id) num_rep,
	* 
from Warehouse.sales.dim_order_header_v
where store_name = 'visiondirect.nl'
	and order_status_name = 'CANCEL'
	and month(order_date) = 8 and year(order_date) = 2017
order by num_rep desc, order_date desc


select count(*) over (partition by customer_id) num_rep,
	order_id_bk, order_no, order_date, invoice_date, payment_method_name
from Warehouse.sales.dim_order_header_v
where invoice_date is null and order_status_magento_name = 'pending_payment'
	and month(order_date) = 10 and year(order_date) = 2017
-- order by num_rep desc, order_date desc
order by order_date desc
