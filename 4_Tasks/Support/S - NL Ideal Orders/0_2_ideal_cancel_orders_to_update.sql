select *
from Warehouse.sales.dim_order_header_v
where store_name = 'visiondirect.es'
	and year(order_date) = 2017 and month(order_date) in (8, 9)
	and invoice_id is null
	and order_status_name <> 'CANCEL'
order by order_date desc
