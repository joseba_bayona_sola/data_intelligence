
select oh.entity_id order_id_bk, oh.increment_id order_no, oh.created_at order_date, oh.store_id, oh.status,
	i.entity_id invoice_id, i.increment_id invoice_no, i.created_at invoice_date, 
	oh.base_total_canceled, 
	oh.base_total_refunded, oh.base_customer_balance_refunded
into #sales_flat_order_fix
from 
		(select *
		from Landing.mag.sales_flat_order_aud 
		where status = 'closed') oh
	left join
		(select order_id, entity_id, increment_id, created_at
		from
			(select i.order_id, i.entity_id, i.increment_id, i.created_at, 
				rank() over (partition by i.order_id order by i.entity_id) rank_inv
			from 
					(select entity_id order_id_bk
					from Landing.mag.sales_flat_order_aud
					where status = 'closed') oh
				inner join
					Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id) t
		where rank_inv = 1) i on oh.entity_id = i.order_id
where i.entity_id is null
order by oh.created_at


truncate table Landing.mag.sales_flat_order
truncate table Landing.mag.sales_flat_invoice
truncate table Landing.mag.sales_flat_creditmemo
truncate table Landing.mag.sales_flat_shipment


insert into Landing.mag.sales_flat_order(entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids,
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	idETLBatchRun, ins_ts)

	select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, status, 
		total_qty_ordered, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		base_to_global_rate, base_to_order_rate, order_currency_code,
		customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
		customer_taxvat, customer_gender, customer_note, customer_note_notify, 
		shipping_description, 
		coupon_code, applied_rule_ids,
		affilBatch, affilCode, affilUserRef, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
		presc_verification_method, prescription_verification_type, 
		referafriend_code, referafriend_referer, 
		telesales_method_code, telesales_admin_username, 
		remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
		idETLBatchRun, ins_ts
	from Landing.mag.sales_flat_order_aud
	where entity_id in 
		(select oh.order_id_bk
		from 
				#sales_flat_order_fix o
			inner join
				Warehouse.sales.dim_order_header_v oh on o.order_id_bk = oh.order_id_bk)

------------------------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - NL Closed/Cancelled Fix', @package_name_call = 'VisionDirectDataWarehouseLoadNoSRC' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
