
-- 81 k 
select count(*)
from Landing.mag.sales_flat_order_aud
where status = 'closed'

select year(created_at), count(*)
from Landing.mag.sales_flat_order_aud
where status = 'closed'
group by year(created_at)
order by year(created_at)

select store_id, count(*)
from Landing.mag.sales_flat_order_aud
where status = 'closed'
group by store_id
order by store_id

----------------------------------

drop table #sales_flat_order_fix

select oh.entity_id order_id_bk, oh.increment_id order_no, oh.created_at order_date, oh.store_id, oh.status,
	i.entity_id invoice_id, i.increment_id invoice_no, i.created_at invoice_date, 
	oh.base_total_canceled, 
	oh.base_total_refunded, oh.base_customer_balance_refunded
into #sales_flat_order_fix
from 
		(select *
		from Landing.mag.sales_flat_order_aud 
		where status = 'closed') oh
	left join
		(select order_id, entity_id, increment_id, created_at
		from
			(select i.order_id, i.entity_id, i.increment_id, i.created_at, 
				rank() over (partition by i.order_id order by i.entity_id) rank_inv
			from 
					(select entity_id order_id_bk
					from Landing.mag.sales_flat_order_aud
					where status = 'closed') oh
				inner join
					Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id) t
		where rank_inv = 1) i on oh.entity_id = i.order_id
where i.entity_id is null
order by oh.created_at

select order_id_bk, order_no, order_date, store_id,
	invoice_id, invoice_no, invoice_date, 
	base_total_canceled, 
	base_total_refunded, base_customer_balance_refunded
from #sales_flat_order_fix
-- where store_id not in (5, 22)
-- where base_total_canceled is not null
where (base_total_refunded is null or base_total_refunded = 0) and (base_customer_balance_refunded is null or base_customer_balance_refunded = 0)
order by order_date desc

	-- Condition that will be used on Landing SP code
	select *
	from #sales_flat_order_fix
	where ((status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0)) or (base_total_canceled is not null) or (status = 'closed')) 
		and invoice_id is null
		and ((base_total_refunded is null or base_total_refunded = 0) and (base_customer_balance_refunded is null or base_customer_balance_refunded = 0))

select year(order_date), count(*)
from #sales_flat_order_fix
group by year(order_date)
order by year(order_date)

select store_id, count(*)
from #sales_flat_order_fix
group by store_id
order by store_id

	select oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date, 
		oh.store_name, oh.order_status_name, oh.customer_id
	from 
			#sales_flat_order_fix o
		inner join
			Warehouse.sales.dim_order_header_v oh on o.order_id_bk = oh.order_id_bk
	order by oh.order_status_name, oh.order_date desc

	select cs.customer_id, cs.customer_email, cs.customer_status_name,
		cs.store_register, cs.register_date, cs.store_first, cs.first_order_date,
		cs.num_tot_orders, cs.num_tot_cancel, cs.num_tot_refund
	from 
		Warehouse.act.fact_customer_signature_v cs
	inner join
		(select distinct oh.customer_id
		from 
				#sales_flat_order_fix o
			inner join
				Warehouse.sales.dim_order_header_v oh on o.order_id_bk = oh.order_id_bk) oh on cs.customer_id = oh.customer_id
	where num_tot_orders = 0
	order by cs.num_tot_orders desc, cs.register_date desc

--------------------------------------------

