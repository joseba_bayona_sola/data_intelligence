﻿select id, 
	shipmentID, shipmentNumber, orderIncrementID, partitionID0_120,
	status, statusChange, 

	shippingMethod, shippingDescription, paymentMethod, 
	warehouseMethod, 
	fullyShipped, 
	labelRenderer, 
	shipmentValue, shippingTotal, wholeSaleCarriage, toFollowValue, 
	notify, syncedToMagento, preHoldStage, preHoldStatus, hold, processingFailed, intersite, 
	year, month, 
	dispatchDate, expectedDeliveryDate, expectedShippingDate, confirmedShippingDate, 
	createdDate
from customershipments$customershipment
order by createdDate desc
limit 1000;
