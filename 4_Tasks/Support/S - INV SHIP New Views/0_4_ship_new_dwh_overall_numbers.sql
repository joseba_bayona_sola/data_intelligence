

select top 1000 website, year(shipment_date) yyyy, month(shipment_date) mm, 
	count(distinct order_id_bk_c) num_orders, count(distinct customer_id_c) num_dist_customers, count(order_line_id_bk_c) num_lines,  
	sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat
from Warehouse.sales.fact_order_line_ship_v
where shipment_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website, year(shipment_date), month(shipment_date)
order by website, year(shipment_date), month(shipment_date)


