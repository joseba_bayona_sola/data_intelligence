
-- Sames as 0_1_oh_ol_comp_new_dwh

select top 1000 website, year(invoice_date) yyyy, month(invoice_date) mm, 
	count(distinct order_id_bk_c) num_orders, count(distinct customer_id_c) num_dist_customers, count(order_line_id_bk_c) num_lines, 
	sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat 
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website, year(invoice_date), month(invoice_date)
order by website, year(invoice_date), month(invoice_date)

