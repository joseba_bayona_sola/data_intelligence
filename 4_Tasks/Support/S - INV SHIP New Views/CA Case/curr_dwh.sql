select order_id, shipment_id, shipment_date, 
	document_id, document_type, document_date,
	global_subtotal_inc_vat, global_shipping_inc_vat, global_discount_inc_vat, 
	global_store_credit_inc_vat, global_adjustment_inc_vat, 
	global_total_inc_vat
from DW_GetLenses.dbo.shipment_headers
where order_id in (4807511)
order by order_id

select top 1000 sh.order_id,
	sl.line_id, sl.shipment_id, 
	sl.document_id, sl.document_type, 
	sl.product_id, sl.name,
	sl.global_line_subtotal_inc_vat, sl.global_shipping_inc_vat, sl.global_discount_inc_vat, 
	sl.global_store_credit_inc_vat, sl.global_adjustment_inc_vat, 
	sl.global_line_total_inc_vat
from 
		DW_GetLenses.dbo.shipment_lines sl
	inner join
		DW_GetLenses.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type
where sh.order_id in (4807511)
order by sh.order_id, sl.document_type, sl.name
