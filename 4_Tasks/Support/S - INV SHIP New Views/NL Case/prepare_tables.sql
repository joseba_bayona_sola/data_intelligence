
select 
	order_id, order_no, 
	store_name, 
	customer_id, 
	sum(convert(decimal(12, 4), global_line_total_inc_vat)) global_line_total_inc_vat, sum(convert(decimal(12, 4), global_line_total_exc_vat)) global_line_total_exc_vat
into #inv_curr_dwh
from DW_GetLenses_jbs.dbo.comp_current_dwh_inv
where convert(datetime, substring(document_date, 1, 19), 120) between '2017-04-01' and '2017-05-01'
	and store_name like 'visiondirect.nl' 
group by order_id, order_no, 
	store_name, 
	customer_id
order by order_id

select order_id_bk, order_no, 
	website, 
	customer_id, 
	sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_exc_vat 
into #inv_new_dwh
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-04-01' and '2017-05-01'
	and website like 'visiondirect.nl' 
group by order_id_bk, order_no, 
	website, 
	customer_id
order by order_id_bk

	-- 9445
	select count(distinct order_id_bk_c)
	from Warehouse.sales.fact_order_line_inv_v
	where invoice_date between '2017-04-01' and '2017-05-01'
		and website like 'visiondirect.nl' 

-- 507.585
select count(*),
	sum(global_line_total_inc_vat), sum(global_line_total_exc_vat)
from #inv_curr_dwh

-- 497.419
select count(*),
	sum(global_total_inc_vat), sum(global_total_exc_vat)
from #inv_new_dwh

-------------------------------------------------------------------------

select c.order_id, c.order_no, c.store_name, c.customer_id, 
	c.global_line_total_inc_vat, n.global_total_inc_vat, c.global_line_total_inc_vat - isnull(n.global_total_inc_vat, 0) diff_total_inc_vat,
	c.global_line_total_exc_vat, n.global_total_exc_vat
from 
		#inv_curr_dwh c
	full join
		#inv_new_dwh n on c.order_id = n.order_id_bk
order by diff_total_inc_vat desc, c.order_id

select *, sum(diff_total_inc_vat) over ()
from
	(select c.order_id, c.order_no, c.store_name, c.customer_id, 
		c.global_line_total_inc_vat, n.global_total_inc_vat, c.global_line_total_inc_vat - isnull(n.global_total_inc_vat, 0) diff_total_inc_vat,
		c.global_line_total_exc_vat, n.global_total_exc_vat
	from 
			#inv_curr_dwh c
		full join
			#inv_new_dwh n on c.order_id = n.order_id_bk) t
where abs(diff_total_inc_vat) > 1
order by diff_total_inc_vat desc, order_id


select *
from #inv_new_dwh
order by order_id_bk