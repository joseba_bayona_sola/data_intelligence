
select order_line_id_bk, order_id_bk, order_no, 
	invoice_id, invoice_no, invoice_date, 
	qty_unit, qty_pack, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_store_credit_given, local_bank_online_given, local_adjustment_refund,
	local_total_inc_vat, local_total_exc_vat, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where order_id_bk in (4787697, 5184731, 5148026)
order by order_id_bk, order_line_id_bk, invoice_id desc

select order_line_id_bk, order_id_bk, order_no, 
	invoice_id, invoice_no, invoice_date, 
	qty_unit, qty_pack, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, 
	local_total_exc_vat, local_total_vat, local_total_prof_fee 
from Warehouse.sales.fact_order_line_inv_v
where order_id_bk in (4787697, 5184731, 5148026)
order by order_id_bk, order_line_id_bk, invoice_id desc

---------------


