
select order_id, invoice_id, invoice_date, 
	document_id, document_type,
	global_subtotal_inc_vat, global_shipping_inc_vat, global_discount_inc_vat, 
	global_store_credit_inc_vat, global_adjustment_inc_vat, 
	global_total_inc_vat
from DW_GetLenses.dbo.invoice_headers
where order_id in (5206617, 4787697, 5184731, 5148026)
order by order_id
