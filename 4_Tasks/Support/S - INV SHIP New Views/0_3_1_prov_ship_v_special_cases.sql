
select order_line_id_bk, order_id_bk, order_no, 
	shipment_id, shipment_no, shipment_date, 
	qty_unit, qty_pack, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, 
	local_total_exc_vat, local_total_vat, local_total_prof_fee
from Warehouse.sales.fact_order_line_ship_v
where order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586, 5746859, 5839394, 5851665)
order by order_id_bk, order_line_id_bk, shipment_id desc

select order_line_id_bk, order_id_bk, order_no, 
	sum(qty_unit), sum(qty_pack), 
	sum(local_subtotal), sum(local_shipping), sum(local_discount), sum(local_store_credit_used), 
	sum(local_total_inc_vat), sum(local_total_exc_vat), sum(local_total_vat), sum(local_total_prof_fee)
from Warehouse.sales.fact_order_line_ship_v
where order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586, 5746859, 5839394, 5851665)
group by order_id_bk, order_line_id_bk, order_no
order by order_id_bk, order_line_id_bk, order_no

