

select order_line_id_bk, order_id_bk, invoice_date, shipment_date_r, shipment_date_c, shipment_week_day, 
	order_stage_name, qty_unit_refunded,
	local_subtotal, local_total_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.fact_order_line_v
where local_total_aft_refund_inc_vat <> 0
	and shipment_date is null
	-- and qty_unit_refunded <> 0
	and order_status_name in ('REFUND', 'PARTIAL REFUND')
order by local_total_aft_refund_inc_vat


	select order_line_id_bk, order_id_bk, invoice_date, shipment_date, shipment_date_r,
		order_status_name, line_status_name, qty_unit_refunded,
		local_subtotal, local_total_inc_vat,
		local_store_credit_given, local_bank_online_given, 
		local_total_aft_refund_inc_vat
	from Warehouse.sales.fact_order_line_v
	where local_total_aft_refund_inc_vat <> 0
		and shipment_date is null
		and order_status_name in ('REFUND', 'PARTIAL REFUND')
		and qty_unit_refunded = 0


	select order_line_id_bk, order_id_bk, invoice_date, shipment_date, shipment_date_r,
		order_status_name, line_status_name, qty_unit_refunded,
		local_subtotal, local_total_inc_vat,
		local_store_credit_given, local_bank_online_given, 
		local_total_aft_refund_inc_vat
	from Warehouse.sales.fact_order_line_v
	where local_store_credit_given <> 0 and local_bank_online_given <> 0
		and shipment_date is null
		and qty_unit_refunded <> 0
	order by local_total_aft_refund_inc_vat
