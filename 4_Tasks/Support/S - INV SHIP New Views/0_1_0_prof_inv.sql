
select top 1000 order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
	invoice_id invoice_id, 
	order_id_bk, order_id_bk order_id_bk_c, order_no, 
	invoice_no invoice_no, 

	invoice_date, invoice_date_c, invoice_week_day, 

	company_name, store_name, website_type, website_group, website, code_tld, market_name, 
	customer_id, customer_id customer_id_c, customer_email,  
	country_code_ship, country_name_ship, postcode_shipping, 
	country_code_bill, country_name_bill, postcode_billing, 
	
	order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
	payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
	reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
	channel_name, coupon_code,
	customer_status_name, customer_order_seq_no, order_source,

	manufacturer_name, 
	product_type_name, category_name, cl_type_name, cl_feature_name,
	product_id_magento, product_family_code, product_family_name, 

	base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
	glass_vision_type_name, glass_package_type_name, 
	sku_magento, sku_erp, 
	aura_product_f,

	qty_unit, -- qty_unit_refunded, 
	qty_pack, 
	-- weight, 

	price_type_name, discount_f,

	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

	global_subtotal, global_shipping, global_discount, global_store_credit_used, 
	global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
	-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	num_erp_allocation_lines 
from Warehouse.sales.fact_order_line_v
where invoice_date is not null


select top 1000 ol.order_line_id_bk,  null order_line_id_bk_c, 
	creditmemo_id invoice_id, 
	ol.order_id_bk, null order_id_bk_c, order_no, 
	creditmemo_no invoice_no, 

	refund_date invoice_date, refund_date_c invoice_date_c, refund_week_day invoice_week_day, -- invoice - refund

	company_name, store_name, website_type, website_group, website, code_tld, market_name, 
	customer_id, null customer_id_c, customer_email, 
	country_code_ship, country_name_ship, postcode_shipping, 
	country_code_bill, country_name_bill, postcode_billing, 
	
	order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
	payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
	reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
	channel_name, coupon_code,
	customer_status_name, customer_order_seq_no, order_source,

	manufacturer_name, 
	product_type_name, category_name, cl_type_name, cl_feature_name,
	product_id_magento, product_family_code, product_family_name, 

	base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
	glass_vision_type_name, glass_package_type_name, 
	sku_magento, sku_erp, 
	aura_product_f,

	qty_unit, qty_unit_refunded, 
	qty_pack, 
	qty_unit_refunded * -1 qty_unit, 
	convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack,
	-- weight, 

	price_type_name, discount_f,

	ol2.local_subtotal, ol2.local_shipping, ol2.local_discount, ol2.local_store_credit_used, 
	ol2.local_total_inc_vat, -- local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

	convert(decimal(12, 4), ol2.local_subtotal * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), ol2.local_shipping * ol.local_to_global_rate) global_shipping, 
	convert(decimal(12, 4), ol2.local_discount * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), ol2.local_store_credit_used * ol.local_to_global_rate) global_store_credit_used, 
	convert(decimal(12, 4), ol2.local_total_inc_vat * ol.local_to_global_rate) global_total_inc_vat, -- global_total_exc_vat, global_total_vat, global_total_prof_fee, 
	-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, ol.vat_rate, ol.prof_fee_rate,
	num_erp_allocation_lines 
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		(select order_line_id_bk, order_id_bk, 
			local_store_credit_given, local_bank_online_given, 
			local_subtotal_refund * -1 local_subtotal, local_shipping_refund * -1 local_shipping, local_discount_refund * -1 local_discount, 
			local_store_credit_used_refund * -1 local_store_credit_used, local_adjustment_refund local_adjustment, 
			local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
				+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat, 
			vat_rate, prof_fee_rate
		from Warehouse.sales.fact_order_line_v
		where invoice_date is not null
			and (local_store_credit_given <> 0 or local_bank_online_given <> 0)) ol2 on ol.order_line_id_bk = ol2.order_line_id_bk
--where ol.order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586)
order by ol.order_id_bk



select order_line_id_bk, order_id_bk, 
	local_store_credit_given, local_bank_online_given, 
	local_total_inc_vat, 
	local_subtotal_refund * -1 local_subtotal, local_shipping_refund * -1 local_shipping, local_discount_refund * -1 local_discount, 
	local_store_credit_used_refund * -1 local_store_credit_used, local_adjustment_refund local_adjustment, 
	local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
		+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat, 
	vat_rate, prof_fee_rate
from Warehouse.sales.fact_order_line_v
where invoice_date is not null
	and (local_store_credit_given <> 0 or local_bank_online_given <> 0)
	and order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586)
order by order_id_bk, order_line_id_bk