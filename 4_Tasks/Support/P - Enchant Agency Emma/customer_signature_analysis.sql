
select top 1000 *
from Warehouse.act.fact_customer_signature_v 

select top 1000 main_type_oh_name, count(*)
from Warehouse.act.fact_customer_signature_v 
group by main_type_oh_name
order by main_type_oh_name

select num_tot_orders, count(*)
from Warehouse.act.fact_customer_signature_v 
where main_type_oh_name = 'Daily'
group by num_tot_orders
order by num_tot_orders

select main_order_qty_time, count(*)
from Warehouse.act.fact_customer_signature_v 
where main_type_oh_name = 'Daily'
group by main_order_qty_time
order by main_order_qty_time
