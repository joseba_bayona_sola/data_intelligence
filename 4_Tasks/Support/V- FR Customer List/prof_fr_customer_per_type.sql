
select customer_id, customer_email, num_tot_orders, main_type_oh_name
from Warehouse.act.fact_customer_signature_v
where store_last = 'visiondirect.fr'
order by num_tot_orders desc

select top 1000 main_type_oh_name, count(*)
from Warehouse.act.fact_customer_signature_v
where store_last = 'visiondirect.fr'
group by main_type_oh_name
order by main_type_oh_name


select top 1000 *
from Warehouse.sales.fact_order_line_v
where customer_id = 1421599
order by order_id_bk desc



select top 1000 *
from Warehouse.sales.dim_order_header_product_type_v
where store_name = 'visiondirect.fr'
	and ((daily_perc is not null and two_weeklies_perc is not null) or (daily_perc is not null and monthlies_perc is not null))
order by order_date_c desc

--------------------------------------------------------------------------

select distinct 
	c.customer_id, c.customer_email, c.num_tot_orders, c.main_type_oh_name, 
	ol.product_type_name, ol.category_name --, ol.product_id_magento, ol.product_family_name
from
		(select customer_id, customer_email, num_tot_orders, main_type_oh_name
		from Warehouse.act.fact_customer_signature_v
		where store_last = 'visiondirect.fr') c
	inner join
		Warehouse.sales.fact_order_line_v ol on c.customer_id = ol.customer_id and c.num_tot_orders = ol.customer_order_seq_no
order by num_tot_orders desc

--------------------------------------------------------------------------

select customer_id, customer_email, main_type_oh_name, num_tot_orders 
from Warehouse.act.fact_customer_signature_v
where store_last = 'visiondirect.fr'
	and main_type_oh_name in ('CL - Two Weeklies')
order by customer_id
