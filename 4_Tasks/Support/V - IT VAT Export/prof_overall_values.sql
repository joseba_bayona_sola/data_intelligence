
-- Overall Values
select top 1000 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, 
	sum(local_total_prof_fee) local_total_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where country_code_ship = 'IT'
	and invoice_date between '2017-08-01' and '2017-09-01'

select top 1000 invoice_date_c, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, 
	sum(local_total_prof_fee) local_total_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where country_code_ship = 'IT'
	and invoice_date between '2017-08-01' and '2017-09-01'
group by invoice_date_c
order by invoice_date_c