
drop table #it_vat

select order_id_bk, invoice_no, invoice_date_c, customer_id, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, 
	sum(local_total_prof_fee) local_total_prof_fee 
into #it_vat
from Warehouse.sales.fact_order_line_inv_v
where country_code_ship = 'IT'
	and invoice_date between '2017-01-01' and '2017-09-01'
group by order_id_bk, invoice_no, invoice_date_c, customer_id
order by invoice_date_c, order_id_bk


select 
	it.invoice_no, it.invoice_date_c invoice_date, 
	c.firstname + ' ' + c.lastname customer_name, oha.street + ', ' + oha.postcode + ' ' + oha.city customer_address,
	c.taxvat customer_fiscal_code, 
	it.local_total_exc_vat, it.local_total_vat, it.local_total_inc_vat, 
	it.local_total_prof_fee, it.local_total_exc_vat - it.local_total_prof_fee local_total_exc_vat_after_prof_fee
from 
		#it_vat it
	inner join
		Landing.mag.customer_entity_flat_aud c on it.customer_id = c.entity_id
	inner join
		Landing.aux.sales_dim_order_header_aud oh on it.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.sales_flat_order_address_aud oha on oh.shipping_address_id = oha.entity_id
order by invoice_date_c, invoice_no



------------------------------------------------------------------------
-- Per Product 

drop table #it_vat_2


select order_id_bk, invoice_no, invoice_date_c, customer_id, 
	product_family_name, product_type_vat, vat_rate, prof_fee_rate,
	-- local_total_exc_vat, local_total_vat, local_total_inc_vat, local_total_prof_fee
	local_total_exc_vat_vf, local_total_vat_vf, local_total_inc_vat_vf, local_total_prof_fee_vf
into #it_vat_2
from Warehouse.sales.fact_order_line_inv_v
where country_code_ship = 'IT'
	-- and invoice_date between '2017-01-01' and '2017-09-01'
	and invoice_date between '2017-07-01' and '2018-01-01'
	and proforma = 'N'

select 
	it.invoice_date_c invoice_date, it.invoice_no, 
	c.firstname + ' ' + c.lastname customer_name, c.taxvat customer_fiscal_code, 
	-- oha.street + ', ' + oha.postcode + ' ' + oha.city customer_address,
	it.product_family_name, it.product_type_vat, -- it.vat_rate, it.prof_fee_rate,
	it.local_total_inc_vat_vf, it.local_total_prof_fee_vf, it.local_total_vat_vf
	-- it.local_total_exc_vat, it.local_total_exc_vat - it.local_total_prof_fee local_total_exc_vat_after_prof_fee
from 
		#it_vat_2 it
	inner join
		Landing.mag.customer_entity_flat_aud c on it.customer_id = c.entity_id
	inner join
		Landing.aux.sales_dim_order_header_aud oh on it.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.sales_flat_order_address_aud oha on oh.shipping_address_id = oha.entity_id
order by invoice_date_c, invoice_no

select year(invoice_date_c) yyyy, month(invoice_date_c) mm, 
	sum(local_total_exc_vat_vf) local_total_exc_vat_vf, sum(local_total_vat_vf) local_total_vat_vf, 
	sum(local_total_inc_vat_vf) local_total_inc_vat_vf, sum(local_total_prof_fee_vf) local_total_prof_fee_vf 
from #it_vat_2
group by year(invoice_date_c), month(invoice_date_c) 
order by year(invoice_date_c), month(invoice_date_c) 

select 
	sum(local_total_exc_vat_vf) local_total_exc_vat_vf, sum(local_total_vat_vf) local_total_vat_vf, 
	sum(local_total_inc_vat_vf) local_total_inc_vat_vf, sum(local_total_prof_fee_vf) local_total_prof_fee_vf 
from #it_vat_2


-----------------------

-- Previous Query
select 
	it.invoice_no, it.invoice_date_c invoice_date, 
	c.firstname + ' ' + c.lastname customer_name, oha.street + ', ' + oha.postcode + ' ' + oha.city customer_address,
	c.taxvat customer_fiscal_code, 
	it.product_family_name, it.product_type_vat, it.vat_rate, it.prof_fee_rate,
	it.local_total_exc_vat, it.local_total_vat, it.local_total_inc_vat, 
	it.local_total_prof_fee, it.local_total_exc_vat - it.local_total_prof_fee local_total_exc_vat_after_prof_fee
from 
		#it_vat_2 it
	inner join
		Landing.mag.customer_entity_flat_aud c on it.customer_id = c.entity_id
	inner join
		Landing.aux.sales_dim_order_header_aud oh on it.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.sales_flat_order_address_aud oha on oh.shipping_address_id = oha.entity_id
order by invoice_date_c, invoice_no