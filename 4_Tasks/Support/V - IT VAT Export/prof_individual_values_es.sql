
drop table #es_vat

select order_id_bk, invoice_no, invoice_date_c, customer_id, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, 
	sum(local_total_prof_fee) local_total_prof_fee 
into #es_vat
from Warehouse.sales.fact_order_line_inv_v
where country_code_ship = 'ES'
	and invoice_date between '2017-01-01' and '2017-09-01'
group by order_id_bk, invoice_no, invoice_date_c, customer_id
order by invoice_date_c, order_id_bk


select 
	es.invoice_no, es.invoice_date_c invoice_date, 
	c.firstname + ' ' + c.lastname customer_name, oha.street + ', ' + oha.postcode + ' ' + oha.city customer_address,
	c.taxvat customer_fiscal_code, 
	es.local_total_exc_vat, es.local_total_vat, es.local_total_inc_vat, 
	es.local_total_prof_fee, es.local_total_exc_vat - es.local_total_prof_fee local_total_exc_vat_after_prof_fee
from 
		#es_vat es
	inner join
		Landing.mag.customer_entity_flat_aud c on es.customer_id = c.entity_id
	inner join
		Landing.aux.sales_dim_order_header_aud oh on es.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.sales_flat_order_address_aud oha on oh.shipping_address_id = oha.entity_id
order by invoice_date_c, invoice_no



