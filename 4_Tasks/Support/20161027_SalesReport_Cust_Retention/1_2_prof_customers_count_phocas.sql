
	--> 2 or more: The customer has made 2 different orders in the same day
	--> 0: The order is a glass. 2 of the product lines are not in Products DIM: SUM never arrives to 1
	--> -1: An order with CREDITMEMO

	--> Special: Order + Credit Memo + Order

-- DW_GetLenses DWH
select *
from DW_GetLenses.dbo.invoice_headers 
where customer_id in (1494014, 1636442)
order by customer_id, order_date, document_date

select ih.customer_id, ih.order_date,
	il.*
from 
	DW_GetLenses.dbo.invoice_headers ih
inner join
	DW_GetLenses.dbo.invoice_lines il on ih.document_id = il.document_id and ih.document_type = il.document_type
where customer_id in (1494014, 1636442)
order by customer_id, order_date, il.document_date

-- DW_Sales_Actual Views
select *
from DW_Sales_Actual.dbo.Fact_Invoices
where customer_id in (1494014, 1636442)
order by customer_id, document_date


-- Phocas DB

select top 1000 *
from Phocas_Sales_Actual_Live.dbo.entity_28
where linkfield in (1494014, 1636442) -- 464808, 622953

select top 1000 *
from Phocas_Sales_Actual_Live.dbo.quantab
where branch_28 in (464808, 622953) 
order by branch_28, streamsID
