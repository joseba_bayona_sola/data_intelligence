	
------ INVOICED ----------------------------------------------------------------------------------------
-- Existing

	select 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- Total Customers MTD, New Customers MTD

		sum(case when i.product_id=2317 then i.global_line_total_exc_vat else 0 end) as product_total_exc_vat_2317, -- #Aura Orders �
		sum(case when i.product_id=2317 then i.product_count else 0 end) as product_count_2317, -- #Aura Orders
		
		sum(i.local_line_total_exc_vat_for_localtoglobal) as local_total, sum(i.global_line_total_exc_vat_for_localtoglobal) as global_total, -- Local Total, Global Total ???

		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday -- New Customers Day
	
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
		inner join 
			(select distinct website, store_name from DW_GetLenses.dbo.v_stores_all) st on i.store_name=st.store_name
	where i.store_name <> 'getlenses.it'
	group by 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end as [billing_country_id],
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD, Total Customers MTD
		
		0,0,0,0,0,0
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
	where i.store_name = 'visiondirect.co.uk'
	group by 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end

	--- NEW: Add Canada
	select 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- Total Customers MTD, New Customers MTD

		sum(case when i.product_id=2317 then i.global_line_total_exc_vat else 0 end) as product_total_exc_vat_2317, -- #Aura Orders �
		sum(case when i.product_id=2317 then i.product_count else 0 end) as product_count_2317, -- #Aura Orders
		
		sum(i.local_line_total_exc_vat_for_localtoglobal) as local_total, sum(i.global_line_total_exc_vat_for_localtoglobal) as global_total, -- Local Total, Global Total ???

		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday -- New Customers Day
	
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
		inner join 
			(select distinct website, store_name from DW_GetLenses.dbo.v_stores_all) st on i.store_name=st.store_name
	where i.store_name <> 'getlenses.it'
	group by 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end as [billing_country_id],
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		
		sum(i.document_count) as total_cust_mtd, sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD, Total Customers MTD
		
		0,0,0,0,0,0
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
	where i.store_name = 'visiondirect.co.uk'
	group by 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end
			
	--- NEW: Add Canada + Daily
	select 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		sum(case when i.document_date='2016-10-25' then i.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily, -- Invoiced Daily
		
		sum(i.document_count) as total_cust_mtd, -- Total Customers MTD
		sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD
		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday, -- New Customers Day

		sum(case when i.product_id=2317 then i.global_line_total_exc_vat else 0 end) as product_total_exc_vat_2317, -- #Aura Orders �
		sum(case when i.product_id=2317 then i.product_count else 0 end) as product_count_2317, -- #Aura Orders
		
		sum(i.local_line_total_exc_vat_for_localtoglobal) as local_total, sum(i.global_line_total_exc_vat_for_localtoglobal) as global_total, -- Local Total, Global Total ???
		sum(i.local_line_total_exc_vat_for_localtoglobal)/sum(i.global_line_total_exc_vat_for_localtoglobal) as avg_rate -- Local Total, Global Total ???


	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
		inner join 
			(select distinct website, store_name from DW_GetLenses.dbo.v_stores_all) st on i.store_name=st.store_name
	where i.store_name <> 'getlenses.it'
	group by 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end as [billing_country_id],
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		sum(case when i.document_date='2016-10-25' then i.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily, -- Invoiced Daily

		sum(i.document_count) as total_cust_mtd, -- Total Customers MTD
		sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD
		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday, -- New Customers Day

		0,0,0,0,0
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
	where i.store_name = 'visiondirect.co.uk'
	group by 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end

	--- NEW: Add Canada + Daily + Product Type
	select 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,
		p.product_type,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		sum(case when i.document_date='2016-10-25' then i.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily, -- Invoiced Daily
		
		sum(i.document_count) as total_cust_mtd, -- Total Customers MTD
		sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD
		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday, -- New Customers Day

		sum(case when i.product_id=2317 then i.global_line_total_exc_vat else 0 end) as product_total_exc_vat_2317, -- #Aura Orders �
		sum(case when i.product_id=2317 then i.product_count else 0 end) as product_count_2317, -- #Aura Orders
		
		sum(i.local_line_total_exc_vat_for_localtoglobal) as local_total, sum(i.global_line_total_exc_vat_for_localtoglobal) as global_total--, -- Local Total, Global Total ???
		--sum(i.local_line_total_exc_vat_for_localtoglobal)/sum(i.global_line_total_exc_vat_for_localtoglobal) as avg_rate -- Local Total, Global Total ???


	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
		inner join 
			(select *
			from DW_GetLenses.dbo.products
			where store_name = 'default') p on i.product_id = p.product_id
		inner join 
			(select distinct website, store_name from DW_GetLenses.dbo.v_stores_all) st on i.store_name=st.store_name
	where i.store_name <> 'getlenses.it'
	group by 
		case 
			when st.website in ('VisionDirect.co.uk', 'VisionDirect.nl', 'VisionDirect.es', 'VisionDirect.ie', 'VisionDirect.it', 'VisionDirect.be', 'VisionDirect.fr', 'VisionDirect.ca') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end,
		p.product_type
	--order by website, product_type
	union all

	select 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end as [billing_country_id],
		p.product_type,
		
		sum(i.global_line_total_exc_vat) as global_line_total_exc_vat, -- Invoiced MTD
		sum(case when i.document_date='2016-10-25' then i.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily, -- Invoiced Daily

		sum(i.document_count) as total_cust_mtd, -- Total Customers MTD
		sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd, -- New Customers MTD
		sum(case when i.document_date='2016-10-25' then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
		sum(case when i.document_date='2016-10-25' and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday, -- New Customers Day

		0,0,0,0--,0
	from 
			DW_GetLenses.dbo.v_invoice_headers ih
		inner join 
			(select *
			from DW_GetLenses.dbo.v_invoices 
			where document_date between '2016-10-01' and '2016-10-25') i on ih.uniqueid=i.uniqueid
		inner join 
			(select *
			from DW_GetLenses.dbo.products
			where store_name = 'default') p on i.product_id = p.product_id
	where i.store_name = 'visiondirect.co.uk'
	group by 
		case 
			when i.[shipping_country_id] in ('GB','US') then i.[shipping_country_id] 
			else 'Other countries' 
		end, 
		p.product_type
	order by [billing_country_id], product_type