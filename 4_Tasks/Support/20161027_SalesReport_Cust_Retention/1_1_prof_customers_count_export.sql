
-- [Finance].[spSalesDataExtract]

sum(i.document_count) as total_cust_mtd -- Total Customers MTD
sum(case when ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_mtd -- New Customers MTD



sum(case when i.document_date=@to_date then i.document_count else 0 end) as total_cust_lastday, -- Total Customers Day 
sum(case when i.document_date=@to_date and ih.order_lifecycle='New' then i.document_count else 0 end) as new_cust_lastday -- New Customers Day


-- The Customers # is really the count of the orders made