	
------ SHIPPED ----------------------------------------------------------------------------------------
-- Existing
	select 
		case 
			when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end as website,

	sum(s.global_line_total_exc_vat) as global_line_total_exc_vat -- Shipped MTD

	from 
			DW_GetLenses.dbo.v_shipments s
		inner join 
			(select distinct website,store_name from DW_GetLenses.dbo.v_stores_all) st on st.store_name=s.store_name
	where 
		s.document_date between '2016-10-01' and '2016-10-25' and 
		s.store_name<>'getlenses.it'
	group by
		case 
			when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr') then st.website
			when st.website like 'VisionDirect%' then 'VisionDirect Other'
			else 'Other' 
		end

	union all

	select
		case 
			when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] -- Shipped MTD 
			else 'Other countries' 
		end as [shipping_country_id], 
		
		sum(s.global_line_total_exc_vat) as global_line_total_exc_vat
	from 
		DW_GetLenses.dbo.v_shipments s
	where 
		s.document_date between '2016-10-01' and '2016-10-25' and 
		s.store_name='visiondirect.co.uk'
	group by 
		case 
			when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] 
			else 'Other countries' 
		end

	--- NEW: Add Canada
		select 
			case 
				when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr', 'VisionDirect.ca') then st.website
				when st.website like 'VisionDirect%' then 'VisionDirect Other'
				else 'Other' 
			end as website,

		sum(s.global_line_total_exc_vat) as global_line_total_exc_vat -- Shipped MTD

		from 
				DW_GetLenses.dbo.v_shipments s
			inner join 
				(select distinct website,store_name from DW_GetLenses.dbo.v_stores_all) st on st.store_name=s.store_name
		where 
			s.document_date between '2016-10-01' and '2016-10-25' and 
			s.store_name<>'getlenses.it'
		group by
			case 
				when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr', 'VisionDirect.ca') then st.website
				when st.website like 'VisionDirect%' then 'VisionDirect Other'
				else 'Other' 
			end

		union all

		select
			case 
				when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] -- Shipped MTD 
				else 'Other countries' 
			end as [shipping_country_id], 
		
			sum(s.global_line_total_exc_vat) as global_line_total_exc_vat
		from 
			DW_GetLenses.dbo.v_shipments s
		where 
			s.document_date between '2016-10-01' and '2016-10-25' and 
			s.store_name='visiondirect.co.uk'
		group by 
			case 
				when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] 
				else 'Other countries' 
			end

	--- NEW: Add Canada + Daily
		select 
			case 
				when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr', 'VisionDirect.ca') then st.website
				when st.website like 'VisionDirect%' then 'VisionDirect Other'
				else 'Other' 
			end as website,

			sum(s.global_line_total_exc_vat) as global_line_total_exc_vat, -- Shipped MTD
			sum(case when s.document_date='2016-10-25' then s.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily -- Shipped Daily

		from 
				DW_GetLenses.dbo.v_shipments s
			inner join 
				(select distinct website,store_name from DW_GetLenses.dbo.v_stores_all) st on st.store_name=s.store_name
		where 
			s.document_date between '2016-10-01' and '2016-10-25' and 
			s.store_name<>'getlenses.it'
		group by
			case 
				when st.website in ('VisionDirect.co.uk','VisionDirect.nl','VisionDirect.es','VisionDirect.ie','VisionDirect.it','VisionDirect.be','VisionDirect.fr', 'VisionDirect.ca') then st.website
				when st.website like 'VisionDirect%' then 'VisionDirect Other'
				else 'Other' 
			end

		union all

		select
			case 
				when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] 
				else 'Other countries' 
			end as [shipping_country_id], 
		
			sum(s.global_line_total_exc_vat) as global_line_total_exc_vat, -- Shipped MTD
			sum(case when s.document_date='2016-10-25' then s.global_line_total_exc_vat else 0 end) as global_line_total_exc_vat_daily -- Shipped Daily
		from 
			DW_GetLenses.dbo.v_shipments s
		where 
			s.document_date between '2016-10-01' and '2016-10-25' and 
			s.store_name='visiondirect.co.uk'
		group by 
			case 
				when s.[shipping_country_id] in ('GB','US') then s.[shipping_country_id] 
				else 'Other countries' 
			end

	--- NEW: Add Canada + Daily + Product Type
