
select replace('VisionDirect.co.uk, VisionDirect.es, VisionDirect.it',',',''',''')

select cast((cast(year('2014-10-01') as varchar(10)) + '-' + cast(month('2014-10-01') as varchar(10)) + '-01') as date) as period

select convert(varchar(11),'2014-10-01',106)

--- 
		select top 100
			cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
			ih.customer_id, ih.customer_order_seq_no, ih.document_type,

			SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]--, sum(isnull(il.global_prof_fee,0)) as [global_prof_fee],
			--sum(isnull(il.global_line_total_inc_vat,0)) as [global_revenue_in_VAT], sum(isnull(il.global_line_total_vat,0)) as [global_VAT],

			--SUM(isnull(il.local_line_total_exc_vat,0)) AS [local_revenue_ex_VAT], sum(isnull(il.local_prof_fee,0)) as [local_prof_fee],
			--sum(isnull(il.local_line_total_inc_vat,0)) as [local_revenue_in_VAT], sum(isnull(il.local_line_total_vat,0)) as [local_VAT]

		from 
				(select * 
				from DW_GetLenses.dbo.invoice_headers 
				union all 
				select * 
				from dw_proforma.dbo.invoice_headers 
				where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
			inner join 
				(select * 
				from DW_GetLenses.dbo.invoice_lines 
				union all 
				select * 
				from dw_proforma.dbo.invoice_lines 
				where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
					on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name

		where 
			ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
			--and il.product_id in ('+@products+')
			--and isnull(ih.business_channel,'Unknown') in ('+@order_channels+')
			and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
			and ih.customer_order_seq_no is not null
			and ih.customer_id is not null 
	
			and ih.customer_id in (1624, 522108, 225181, 860226, 998902, 1005732)
		group by 
			year(ih.document_Date), month(ih.document_date),
			ih.customer_id, ih.customer_order_seq_no, ih.document_type
		order by customer_id, period, customer_order_seq_no

		-- CLEAN
		select top 100
			cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
			ih.customer_id, ih.customer_order_seq_no,
			SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]
		from 
				(select * 
				from DW_GetLenses.dbo.invoice_headers 
				union all 
				select * 
				from dw_proforma.dbo.invoice_headers 
				where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
			inner join 
				(select * 
				from DW_GetLenses.dbo.invoice_lines 
				union all 
				select * 
				from dw_proforma.dbo.invoice_lines 
				where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
					on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name
		where 
			ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
			and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
			and ih.customer_order_seq_no is not null and ih.customer_id is not null 
		group by year(ih.document_Date), month(ih.document_date), ih.customer_id, ih.customer_order_seq_no


	select
		period, customer_order_seq_no, month_diff,
		count(distinct customer_id) as customer_count,
		SUM(global_revenue_ex_VAT) AS [global_revenue_ex_VAT], --sum(global_prof_fee) as [global_prof_fee], 
		--sum(global_revenue_in_VAT) as [global_revenue_in_VAT], sum(global_VAT) as [global_VAT],

		count(distinct next_order_customer_id) as next_order_customer_count,
		SUM(next_order_global_revenue_ex_VAT) AS [next_order_global_revenue_ex_VAT]--, sum(next_order_global_prof_fee) as [next_order_global_prof_fee],
		--sum(next_order_global_revenue_in_VAT) as [next_order_global_revenue_in_VAT], sum(next_order_global_VAT) as [next_order_global_VAT],

		--SUM(local_revenue_ex_VAT) AS [local_revenue_ex_VAT], sum(local_prof_fee) as [local_prof_fee],
		--sum(local_revenue_in_VAT) as [local_revenue_in_VAT], sum(local_VAT) as [local_VAT],

		--SUM(next_order_local_revenue_ex_VAT) AS [next_order_local_revenue_ex_VAT], sum(next_order_local_prof_fee) as [next_order_local_prof_fee],
		--sum(next_order_local_revenue_in_VAT) as [next_order_local_revenue_in_VAT], sum(next_order_local_VAT) as [next_order_local_VAT]

	from
		(select datediff(month, rs1.period, rs2.period) as month_diff, 
			rs1.*,
			rs2.customer_id as next_order_customer_id, 
			rs2.global_revenue_ex_VAT as next_order_global_revenue_ex_VAT--, rs2.global_prof_fee as next_order_global_prof_fee,
			--rs2.global_revenue_in_VAT as next_order_global_revenue_in_VAT, rs2.global_VAT as next_order_global_VAT,

			--rs2.local_revenue_ex_VAT as next_order_local_revenue_ex_VAT, rs2.local_prof_fee as next_order_local_prof_fee,
			--rs2.local_revenue_in_VAT as next_order_local_revenue_in_VAT, rs2.local_VAT as next_order_local_VAT

		from
				(select *, 
					row_number() over(partition by customer_id order by period,customer_order_seq_no) as rno 
				from 
					(select 
						cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
						ih.customer_id, ih.customer_order_seq_no,
						SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]
					from 
							(select * 
							from DW_GetLenses.dbo.invoice_headers 
							union all 
							select * 
							from dw_proforma.dbo.invoice_headers 
							where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
						inner join 
							(select * 
							from DW_GetLenses.dbo.invoice_lines 
							union all 
							select * 
							from dw_proforma.dbo.invoice_lines 
							where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
								on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name
					where 
						ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
						and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
						and ih.customer_order_seq_no is not null and ih.customer_id is not null 

						and ih.customer_id in (1624, 522108, 225181, 860226, 998902, 1005732)
					group by year(ih.document_Date), month(ih.document_date), ih.customer_id, ih.customer_order_seq_no)t) rs1
			left outer join 
				(select *, 
					row_number() over(partition by customer_id order by period,customer_order_seq_no) as rno 
				from 
					(select 
						cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
						ih.customer_id, ih.customer_order_seq_no,
						SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]
					from 
							(select * 
							from DW_GetLenses.dbo.invoice_headers 
							union all 
							select * 
							from dw_proforma.dbo.invoice_headers 
							where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
						inner join 
							(select * 
							from DW_GetLenses.dbo.invoice_lines 
							union all 
							select * 
							from dw_proforma.dbo.invoice_lines 
							where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
								on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name
					where 
						ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
						and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
						and ih.customer_order_seq_no is not null and ih.customer_id is not null 

						and ih.customer_id in (1624, 522108, 225181, 860226, 998902, 1005732)
					group by year(ih.document_Date), month(ih.document_date), ih.customer_id, ih.customer_order_seq_no)t) rs2 on rs1.customer_id = rs2.customer_id and rs2.rno = rs1.rno+1) rs 
	group by 
		period, customer_order_seq_no, month_diff
	order by period, customer_order_seq_no, month_diff

--------------

		select datediff(month, rs1.period, rs2.period) as month_diff, 
			rs1.*,
			rs2.customer_id as next_order_customer_id, 
			rs2.global_revenue_ex_VAT as next_order_global_revenue_ex_VAT--, rs2.global_prof_fee as next_order_global_prof_fee,
			--rs2.global_revenue_in_VAT as next_order_global_revenue_in_VAT, rs2.global_VAT as next_order_global_VAT,

			--rs2.local_revenue_ex_VAT as next_order_local_revenue_ex_VAT, rs2.local_prof_fee as next_order_local_prof_fee,
			--rs2.local_revenue_in_VAT as next_order_local_revenue_in_VAT, rs2.local_VAT as next_order_local_VAT

		from
				(select *, 
					row_number() over(partition by customer_id order by period,customer_order_seq_no) as rno 
				from 
					(select 
						cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
						ih.customer_id, ih.customer_order_seq_no,
						SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]
					from 
							(select * 
							from DW_GetLenses.dbo.invoice_headers 
							union all 
							select * 
							from dw_proforma.dbo.invoice_headers 
							where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
						inner join 
							(select * 
							from DW_GetLenses.dbo.invoice_lines 
							union all 
							select * 
							from dw_proforma.dbo.invoice_lines 
							where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
								on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name
					where 
						ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
						and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
						and ih.customer_order_seq_no is not null and ih.customer_id is not null 

						and ih.customer_id in (1624, 522108, 225181, 860226, 998902, 1005732)
					group by year(ih.document_Date), month(ih.document_date), ih.customer_id, ih.customer_order_seq_no)t) rs1
			left outer join 
				(select *, 
					row_number() over(partition by customer_id order by period,customer_order_seq_no) as rno 
				from 
					(select 
						cast((cast(year(ih.document_Date) as varchar(10)) + '-' + cast(month(ih.document_date) as varchar(10)) + '-01') as date) as period,
						ih.customer_id, ih.customer_order_seq_no,
						SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT]
					from 
							(select * 
							from DW_GetLenses.dbo.invoice_headers 
							union all 
							select * 
							from dw_proforma.dbo.invoice_headers 
							where document_date >= '01-feb-2014' and store_name <> 'visiooptik.fr') ih
						inner join 
							(select * 
							from DW_GetLenses.dbo.invoice_lines 
							union all 
							select * 
							from dw_proforma.dbo.invoice_lines 
							where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
								on ih.document_id = il.document_id and ih.document_type = il.document_type and ih.store_name = il.store_name
					where 
						ih.document_date >= convert(varchar(11), '2014-10-01', 106) and ih.document_date<dateadd(day,1, convert(varchar(11), '2016-08-01',106))
						and ih.store_name in ('getlenses.co.uk', 'postoptics.co.uk', 'visiondirect.co.uk')
						and ih.customer_order_seq_no is not null and ih.customer_id is not null 

						and ih.customer_id in (1624, 522108, 225181, 860226, 998902, 1005732)
					group by year(ih.document_Date), month(ih.document_date), ih.customer_id, ih.customer_order_seq_no)t) rs2 on rs1.customer_id = rs2.customer_id and rs2.rno = rs1.rno+1
		order by customer_id, period, customer_order_seq_no