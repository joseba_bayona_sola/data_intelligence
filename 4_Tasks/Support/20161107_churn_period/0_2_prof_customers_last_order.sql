
select top 100 store_name, order_id, order_no, document_type, 
	order_date, customer_id,
	count_rep, count_ord_cust, rank_ord_cust, max_ord_cust
from DW_GetLenses_jbs.dbo.order_headers_4

	-- 807001 (620593) total customers
	select top 100 count(*) over(), 
		customer_id, count(*) tot
	from DW_GetLenses_jbs.dbo.order_headers_4
	where store_name like 'visiondirect%'
	group by customer_id
	order by tot desc

	select store_name, count(*), 
		min(order_date), max(order_date)
	from DW_GetLenses_jbs.dbo.order_headers_4
	--where store_name like 'visiondirect%'
	group by store_name
	order by store_name desc

