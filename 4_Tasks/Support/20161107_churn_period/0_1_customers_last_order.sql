
select top 100 store_name, order_id, order_no, document_type, order_date, customer_id
from DW_GetLenses.dbo.order_headers

select top 100 *,
	count(*) over(partition by customer_id) count_ord_cust, 
	rank() over (partition by customer_id order by order_id) rank_ord_cust,
	max(order_id) over(partition by customer_id) max_ord_cust
from 
	(select 
		store_name, order_id, order_no, document_type, order_date, customer_id, 
		count(*) over(partition by order_no) count_rep
	from DW_GetLenses.dbo.order_headers) t
where count_rep = 1

select top 100 count(*) over (),
	*
from 
	(select *,
		count(*) over(partition by customer_id) count_ord_cust, 
		rank() over (partition by customer_id order by order_id) rank_ord_cust,
		max(order_id) over(partition by customer_id) max_ord_cust
	from 
		(select 
			store_name, order_id, order_no, document_type, order_date, customer_id, 
			count(*) over(partition by order_no) count_rep
		from DW_GetLenses.dbo.order_headers) t
	where count_rep = 1) t
--where count_ord_cust = rank_ord_cust
where rank_ord_cust > count_ord_cust - 4

	select document_type, count(*)
	from
		(select *
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_no) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep = 1) t
		--where count_ord_cust = rank_ord_cust
		where rank_ord_cust > count_ord_cust - 4) t
	group by document_type
	order by document_type

----------------------------------------------------------------------------------------------

drop table DW_GetLenses_jbs.dbo.order_headers_4

	select *, 
		count(*) over(partition by customer_id) count_ord_cust_4
	into DW_GetLenses_jbs.dbo.order_headers_4
	from 
		(select *,
			count(*) over(partition by customer_id) count_ord_cust, 
			rank() over (partition by customer_id order by order_id) rank_ord_cust,
			max(order_id) over(partition by customer_id) max_ord_cust
		from 
			(select 
				store_name, order_id, order_no, document_type, order_date, customer_id, 
				count(*) over(partition by order_no) count_rep
			from DW_GetLenses.dbo.order_headers) t
		where count_rep = 1) t
	--where count_ord_cust = rank_ord_cust
	where rank_ord_cust > count_ord_cust - 4;