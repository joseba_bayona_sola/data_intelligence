
select store_name, order_id, order_no, document_type, 
	customer_id, order_date, 
	lead(order_date) over(partition by customer_id order by order_date) order_date_2,
	count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4, 
	datediff(mm, order_date, lead(order_date) over(partition by customer_id order by order_date) ) diff_mm
from DW_GetLenses_jbs.dbo.order_headers_4
where customer_id in (32633, 195377, 245123, 267360, 276022, 302693, 311910, 396742, 422443, 438013)
order by customer_id, order_id

select top 100 *, 
	datediff(mm, order_date, order_date_2) diff_mm
from 
	(select store_name, order_id, order_no, document_type, 
		customer_id, order_date, 
		lead(order_date) over(partition by customer_id order by order_date) order_date_2,
		count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4
	from DW_GetLenses_jbs.dbo.order_headers_4) t
where count_ord_cust <> rank_ord_cust
	and order_date_2 is null
order by customer_id

---------------------------------------------------------

select num_tot, diff_mm, count(*)
from 
	(select *, count(*) over() num_tot,
		datediff(mm, order_date, order_date_2) diff_mm
	from 
		(select store_name, order_id, order_no, document_type, 
			customer_id, order_date, 
			lead(order_date) over(partition by customer_id order by order_date) order_date_2,
			count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4
		from DW_GetLenses_jbs.dbo.order_headers_4) t
	where count_ord_cust <> rank_ord_cust) t
group by num_tot, diff_mm
order by diff_mm

select diff_mm, num_tot, num_diff_mm, num_diff_mm*100/convert(decimal(10,2), num_tot) perc_num_tot
from
	(select num_tot, diff_mm, count(*) num_diff_mm
	from 
		(select *, count(*) over() num_tot,
			datediff(mm, order_date, order_date_2) diff_mm
		from 
			(select store_name, order_id, order_no, document_type, 
				customer_id, order_date, 
				lead(order_date) over(partition by customer_id order by order_date) order_date_2,
				count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4
			from DW_GetLenses_jbs.dbo.order_headers_4) t
		where count_ord_cust <> rank_ord_cust) t
	group by num_tot, diff_mm) t
order by diff_mm

	---
	select diff_mm, num_tot, num_diff_mm, num_diff_mm*100/convert(decimal(10,2), num_tot) perc_num_tot
	from
		(select num_tot, diff_mm, count(*) num_diff_mm
		from 
			(select *, count(*) over() num_tot,
				datediff(mm, order_date, order_date_2) diff_mm
			from 
				(select store_name, order_id, order_no, document_type, 
					customer_id, order_date, 
					lead(order_date) over(partition by customer_id order by order_date) order_date_2,
					count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4
				from DW_GetLenses_jbs.dbo.order_headers_4) t
			where count_ord_cust <> rank_ord_cust
				and count_ord_cust_4 = 4) t
		group by num_tot, diff_mm) t
	order by diff_mm
