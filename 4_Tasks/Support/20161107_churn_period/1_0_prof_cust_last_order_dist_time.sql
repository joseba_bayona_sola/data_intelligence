
select top 100 store_name, order_id, order_no, document_type, 
	order_date, customer_id,
	count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4, 
	datediff(mm, order_date, getutcdate()) diff_mm
from DW_GetLenses_jbs.dbo.order_headers_4
where count_ord_cust = rank_ord_cust

---------------------------------------------------------

select num_tot, diff_mm, count(*)
from 
	(select count(*) over() num_tot,
		store_name, order_id, order_no, document_type, 
		order_date, customer_id,
		count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4, 
		datediff(mm, order_date, getutcdate()) diff_mm
	from DW_GetLenses_jbs.dbo.order_headers_4
	where count_ord_cust = rank_ord_cust) t
where store_name like 'visiondirect%'
group by num_tot, diff_mm
order by diff_mm

select diff_mm, num_tot, num_diff_mm, num_diff_mm*100/convert(decimal(10,2), num_tot) perc_num_tot
from
	(select num_tot, diff_mm, count(*) num_diff_mm
	from 
		(select count(*) over() num_tot,
			store_name, order_id, order_no, document_type, 
			order_date, customer_id,
			count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4, 
			datediff(mm, order_date, getutcdate()) diff_mm
		from DW_GetLenses_jbs.dbo.order_headers_4
		where count_ord_cust = rank_ord_cust) t
	--where store_name like 'visiondirect%'
	group by num_tot, diff_mm) t
order by diff_mm

	---
	select diff_mm, num_tot, num_diff_mm, num_diff_mm*100/convert(decimal(10,2), num_tot) perc_num_tot
	from
		(select num_tot, diff_mm, count(*) num_diff_mm
		from 
			(select count(*) over() num_tot,
				store_name, order_id, order_no, document_type, 
				order_date, customer_id,
				count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4, 
				datediff(mm, order_date, getutcdate()) diff_mm
			from DW_GetLenses_jbs.dbo.order_headers_4
			where count_ord_cust = rank_ord_cust
				and count_ord_cust_4 = 4) t
		--where store_name like 'visiondirect%'
		group by num_tot, diff_mm) t
	order by diff_mm
