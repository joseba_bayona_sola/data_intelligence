
select top 100 store_name, order_id, order_no, document_type, 
	order_date, customer_id,
	count_rep, count_ord_cust, rank_ord_cust, max_ord_cust, count_ord_cust_4
from DW_GetLenses_jbs.dbo.order_headers_4

select count_ord_cust_4, count(*)
from DW_GetLenses_jbs.dbo.order_headers_4
group by count_ord_cust_4

------------------------------------------------------------------------------

select num_tot_cust, num_tot, num_part, num_part*100/convert(decimal(10,2), num_tot) perc_num_tot
from 
	(select num_tot, num_tot_cust, count(*) num_part
	from 
		(select count(*) over() num_tot, 
			customer_id, count(*) num_tot_cust
		from DW_GetLenses_jbs.dbo.order_headers_4
		where store_name like 'visiondirect%'
		group by customer_id) t
	group by num_tot, num_tot_cust) t
order by num_tot_cust


select num_tot_cust, num_tot, num_part, num_part*100/convert(decimal(10,2), num_tot) perc_num_tot
from 
	(select num_tot, num_tot_cust, count(*) num_part
	from 
		(select count(*) over() num_tot, 
			customer_id, count(*) num_tot_cust
		from DW_GetLenses_jbs.dbo.order_headers_4
		--where store_name like 'visiondirect%'
		group by customer_id) t
	group by num_tot, num_tot_cust) t
order by num_tot_cust