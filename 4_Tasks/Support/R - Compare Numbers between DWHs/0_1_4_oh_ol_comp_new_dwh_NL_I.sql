
drop table #nl_ideal_problem

select order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, payment_method_name,
	global_total_aft_refund_inc_vat, 
	count(*) over () num_orders,
	sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat, 
	count(*) over (partition by customer_id) num_rep_customer, 
	min(order_date) over (partition by customer_id) min_order_date,
	max(order_date) over (partition by customer_id) max_order_date, 
	rank() over (partition
	 by customer_id order by order_id_bk) ord_cust 
into #nl_ideal_problem
from Warehouse.sales.dim_order_header_v
where order_date between '2017-07-01' and '2017-08-01' and invoice_date is null and order_status_magento_name = 'closed'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.nl'
order by customer_id, order_id_bk

select *
from #nl_ideal_problem
where num_rep_customer > 3

select payment_method_name, count(*) 
from #nl_ideal_problem t
group by payment_method_name
order by payment_method_name

select count(*) over () num_tot_customer, *, datediff(minute, min_order_date, max_order_date)
from #nl_ideal_problem t
where ord_cust = 1
order by customer_id, order_id_bk

select num_tot_customer, num_rep_customer orders_try, count(*) num_customers, sum(global_total_aft_refund_inc_vat) total_sales
from
	(select count(*) over () num_tot_customer, *
	from #nl_ideal_problem		
	where ord_cust = 1) t
group by num_tot_customer, num_rep_customer
order by num_tot_customer, num_rep_customer


select t.*, oh.order_id_bk, oh.order_date, oh.order_stage_name, oh.payment_method_name, 
	rank() over (partition by t.customer_id order by oh.order_id_bk) ord_cust_2	
from
		(select count(*) over () num_tot_customer, *
		from #nl_ideal_problem t
		where ord_cust = 1) t
	left join
		Warehouse.sales.dim_order_header_v oh on t.customer_id = oh.customer_id and oh.order_date > t.max_order_date and oh.invoice_date is not null
order by t.customer_id, t.order_id_bk


select count(*) over () num_tot, 
	sum(global_total_aft_refund_inc_vat) over () sum_tot,
	*
from
	(select t.*, oh.order_id_bk order_id_bk_2, oh.order_date order_date_2, oh.order_stage_name order_stage_name_2, 
		rank() over (partition by t.customer_id order by oh.order_id_bk) ord_cust_2	
	from
			(select count(*) over () num_tot_customer, *
			from #nl_ideal_problem t
			where ord_cust = 1) t
		left join
			Warehouse.sales.dim_order_header_v oh on t.customer_id = oh.customer_id and oh.order_date > t.max_order_date and oh.invoice_date is not null) t
where ord_cust_2 = 1
	and order_id_bk_2 is null
order by t.customer_id, t.order_id_bk

select payment_method_name_2, 
	count(*), 
	sum(global_total_aft_refund_inc_vat) 
from
	(select t.*, oh.order_id_bk order_id_bk_2, oh.order_date order_date_2, oh.order_stage_name order_stage_name_2, oh.payment_method_name payment_method_name_2, 
		rank() over (partition by t.customer_id order by oh.order_id_bk) ord_cust_2	
	from
			(select count(*) over () num_tot_customer, *
			from #nl_ideal_problem t
			where ord_cust = 1) t
		left join
			Warehouse.sales.dim_order_header_v oh on t.customer_id = oh.customer_id and oh.order_date > t.max_order_date and oh.invoice_date is not null) t
where ord_cust_2 = 1
	and order_id_bk_2 is not null
group by payment_method_name_2
order by payment_method_name_2

---------------------------------------------------------

select payment_method_name, count(*)
from Warehouse.sales.dim_order_header_v
where order_date between '2017-01-01' and '2017-08-01' and invoice_date is not null
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.nl'
group by payment_method_name
order by payment_method_name

select year(order_date), month(order_date), payment_method_name, count(*)
from Warehouse.sales.dim_order_header_v
where order_date between '2017-01-01' and '2017-08-01' and invoice_date is not null
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.nl'
group by year(order_date), month(order_date), payment_method_name
order by payment_method_name, year(order_date), month(order_date)
