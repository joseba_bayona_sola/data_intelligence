
select 
	line_id, order_id, order_no, 
	convert(datetime, substring(document_date, 1, 19), 120) document_date, 
	store_name, 
	customer_id, product_id,
	document_type, 
	convert(decimal(12, 4), global_line_total_inc_vat) global_line_total_inc_vat, convert(decimal(12, 4), global_line_total_exc_vat) global_line_total_exc_vat
into #inv_curr_dwh
from DW_GetLenses_jbs.dbo.comp_current_dwh_inv
where convert(datetime, substring(document_date, 1, 19), 120) between '2017-01-01' and '2017-08-01'
	and store_name like 'visiondirect.fr' 
order by document_date


select invoice_line_id, 
	order_id_bk, order_no, invoice_date, refund_date, website, 
	customer_id_bk, product_id_bk, order_status_name_bk, adjustment_order,
	countries_registered_code, product_type_vat, vat_rate, prof_fee_rate, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat, 
	global_adjustment_refund
into #inv_new_dwh
from
	(select oh.order_id_bk, oh.order_no,
		ol.order_line_id_bk, ol.invoice_line_id, ol.shipment_line_id, 
		ol.invoice_date, ol.shipment_date, ol.refund_date,
		oh.order_status_name_bk, oh.adjustment_order,
		oh.store_id_bk, oh.customer_id_bk, 
		ol.product_id_bk, 
		ol.countries_registered_code, ol.product_type_vat, ol.vat_rate, ol.prof_fee_rate,
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, 
		ol.global_total_aft_refund_inc_vat, ol.global_total_aft_refund_exc_vat, 
		ol.global_adjustment_refund 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select order_id_bk, 
				order_line_id_bk, invoice_line_id, shipment_line_id, 
				invoice_date, shipment_date, refund_date,
				product_id_bk, 
				countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
				local_total_aft_refund_inc_vat * local_to_global_rate global_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat * local_to_global_rate global_total_aft_refund_exc_vat,  
				local_adjustment_refund * local_to_global_rate global_adjustment_refund
			from Landing.aux.sales_fact_order_line_aud) ol on oh.order_id_bk = ol.order_id_bk) t
	inner join
		Landing.aux.mag_gen_store_v s on t.store_id_bk = s.store_id
where invoice_date between '2017-01-01' and '2017-08-01'
	and order_status_name_bk in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.fr' 
order by invoice_date, invoice_line_id

---------------------------------

-- 576.729
select count(*),
	sum(global_line_total_inc_vat), sum(global_line_total_exc_vat)
from #inv_curr_dwh

-- 576.650
select count(*),
	sum(global_total_aft_refund_inc_vat), sum(global_total_aft_refund_exc_vat)
from #inv_new_dwh


---------------------------------------------


select *
from
	(select line_id, invoice_line_id, 
		order_id, order_no, document_date, invoice_date, refund_date, 
		customer_id, product_id, 
		document_type, order_status_name_bk, adjustment_order,
		countries_registered_code, product_type_vat, vat_rate, prof_fee_rate, 
		global_line_total_inc_vat, 
		global_total_aft_refund_inc_vat,
		-- global_line_total_exc_vat, 
		-- global_total_aft_refund_exc_vat, 
		sum(global_line_total_inc_vat) over (partition by order_id) global_line_total_inc_vat_s, 
		sum(global_total_aft_refund_inc_vat) over (partition by order_id) global_total_aft_refund_inc_vat_s, 
		-- sum(global_line_total_exc_vat) over (partition by order_id) global_line_total_exc_vat_s, 
		-- sum(global_total_aft_refund_exc_vat) over (partition by order_id) global_total_aft_refund_exc_vat_s 
		global_adjustment_refund
	from
		(select t1.line_id, t2.invoice_line_id, 
			t1.order_id, t1.order_no, t1.document_date, t2.invoice_date, t2.refund_date, 
			t1.customer_id, t1.product_id, 
			t1.document_type, t2.order_status_name_bk, t2.adjustment_order, 
			t2.countries_registered_code, t2.product_type_vat, t2.vat_rate, t2.prof_fee_rate, 
			t1.global_line_total_inc_vat, isnull(t2.global_total_aft_refund_inc_vat, 0) global_total_aft_refund_inc_vat,
			t1.global_line_total_exc_vat, isnull(t2.global_total_aft_refund_exc_vat, 0) global_total_aft_refund_exc_vat, 
			t2.global_adjustment_refund
		from 
				#inv_curr_dwh t1
			full join
				#inv_new_dwh t2 on t1.line_id =  t2.invoice_line_id) t) t
where abs(global_line_total_inc_vat_s - global_total_aft_refund_inc_vat_s) > 1
order by order_id, document_date

---------------------------------------------

select t1.line_id, t2.invoice_line_id, 
	t1.order_id, t1.order_no, t1.document_date, t2.invoice_date, t2.refund_date,
	t1.customer_id, t1.product_id, 
	t1.document_type, t2.order_status_name_bk, t2.adjustment_order,
	-- t2.countries_registered_code, t2.product_type_vat, t2.vat_rate, t2.prof_fee_rate, 
	t1.global_line_total_inc_vat, t2.global_total_aft_refund_inc_vat, 
	abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) diff_inc_vat,
	-- t1.global_line_total_exc_vat, t2.global_total_aft_refund_exc_vat, 
	-- abs(t1.global_line_total_exc_vat - t2.global_total_aft_refund_exc_vat) diff_exc_vat, 
	t2.global_adjustment_refund
from 
		#inv_curr_dwh t1
	left join
		#inv_new_dwh t2 on t1.line_id =  t2.invoice_line_id 
--where 
	-- t1.line_id is not null and t2.invoice_line_id is not null -- t1.line_id is null // -- t2.invoice_line_id is null
	-- and abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) > 1
order by order_id, line_id


select top 1000 *
from #inv_curr_dwh
