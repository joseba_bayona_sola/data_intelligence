

select order_id_bk, invoice_id, creditmemo_id, order_no, 
	customer_id, order_status_name, adjustment_order, 
	global_subtotal, global_shipping, global_discount, global_store_credit_used, global_total_inc_vat, 
	global_total_refund, global_store_credit_given, global_bank_online_given, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund, 
	global_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where order_id_bk in (4761157, 4815572, 4859721, 5044889, 5634599, 5643447)
order by order_id_bk

select order_id_bk, invoice_id, creditmemo_id, order_no, 
	customer_id, order_status_name, adjustment_order, 
	global_subtotal, global_shipping, global_discount, global_store_credit_used, global_total_inc_vat, 
	global_store_credit_given, global_bank_online_given, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund, 
	global_total_aft_refund_inc_vat
from Warehouse.sales.fact_order_line_v
where order_id_bk in (4761157, 4815572, 4859721, 5044889, 5634599, 5643447)
order by order_id_bk, order_line_id_bk
