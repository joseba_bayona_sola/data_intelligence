
select top 1000 order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.dim_order_header_v

select top 1000 order_line_id_bk,
	order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v

---------------------------------------------------------------------

-- order_date // invoice_date // shipment_date
-- OH
select top 1000 website, year(order_date) yyyy, month(order_date) mm, 
	count(*) num_orders, count(distinct customer_id) num_dist_customers, 
	-- sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from Warehouse.sales.dim_order_header_v
where order_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website, year(order_date), month(order_date)
order by website, year(order_date), month(order_date)

-- OL
select top 1000 website, year(order_date) yyyy, month(order_date) mm, 
	count(distinct order_id_bk) num_orders, count(distinct customer_id) num_dist_customers, count(*) num_lines, 
	-- sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where order_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website, year(order_date), month(order_date)
order by website, year(order_date), month(order_date)


select top 1000 website, year(invoice_date) yyyy, month(invoice_date) mm, 
	count(*) num_lines, count(distinct order_id_bk) num_orders, count(distinct customer_id_bk) num_dist_customers, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from
	(select oh.order_id_bk, oh.order_no,
		ol.order_line_id_bk, ol.invoice_line_id, ol.shipment_line_id, 
		ol.invoice_date, ol.shipment_date, 
		oh.order_status_name_bk,
		oh.store_id_bk, oh.customer_id_bk, 
		ol.product_id_bk, 
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, 
		ol.global_total_aft_refund_inc_vat, ol.global_total_aft_refund_exc_vat 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select order_id_bk, 
				order_line_id_bk, invoice_line_id, shipment_line_id, 
				invoice_date, shipment_date, 
				product_id_bk, 
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
				local_total_aft_refund_inc_vat * local_to_global_rate global_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat * local_to_global_rate global_total_aft_refund_exc_vat 
			from Landing.aux.sales_fact_order_line_aud) ol on oh.order_id_bk = ol.order_id_bk) t
	inner join
		Landing.aux.mag_gen_store_v s on t.store_id_bk = s.store_id
where invoice_date between '2017-01-01' and '2017-08-01'
	and order_status_name_bk in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website, year(invoice_date), month(invoice_date) 
order by website, year(invoice_date), month(invoice_date) 




---------------------------------------------------------------------

select top 1000 website, count(*) num_lines, count(distinct order_id_bk) num_orders, count(distinct customer_id) num_dist_customers, 
	sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where invoice_date between '2017-06-01' and '2017-07-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website
order by website


select top 1000 website, count(*) num_lines, count(distinct order_id_bk) num_orders, count(distinct customer_id_bk) num_dist_customers, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from
	(select oh.order_id_bk, oh.order_no,
		ol.order_line_id_bk, ol.invoice_line_id, ol.shipment_line_id, 
		ol.invoice_date, ol.shipment_date, 
		oh.order_status_name_bk,
		oh.store_id_bk, oh.customer_id_bk, 
		ol.product_id_bk, 
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, 
		ol.global_total_aft_refund_inc_vat, ol.global_total_aft_refund_exc_vat 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select order_id_bk, 
				order_line_id_bk, invoice_line_id, shipment_line_id, 
				invoice_date, shipment_date, 
				product_id_bk, 
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
				local_total_aft_refund_inc_vat * local_to_global_rate global_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat * local_to_global_rate global_total_aft_refund_exc_vat 
			from Landing.aux.sales_fact_order_line_aud) ol on oh.order_id_bk = ol.order_id_bk) t
	inner join
		Landing.aux.mag_gen_store_v s on t.store_id_bk = s.store_id
where invoice_date between '2017-06-01' and '2017-07-01'
	and order_status_name_bk in ('OK', 'REFUND', 'PARTIAL REFUND')
group by website
order by website

