
select order_id, invoice_id, invoice_date, 
	document_id, document_type,
	global_subtotal_inc_vat, global_shipping_inc_vat, global_discount_inc_vat, 
	global_store_credit_inc_vat, global_adjustment_inc_vat, 
	global_total_inc_vat
from DW_GetLenses.dbo.invoice_headers
where order_id in (4807511, 5161914)

select il.invoice_id, il.document_date, 
	il.document_id, il.document_type,
	il.global_line_subtotal_inc_vat, il.global_shipping_inc_vat, il.global_discount_inc_vat, 
	il.global_store_credit_inc_vat, il.global_adjustment_inc_vat, 
	il.global_line_total_inc_vat
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join
		DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
where ih.order_id in (4807511, 5161914)
order by invoice_id, document_date	


select store_name, UniqueID, order_no, document_date, document_type, 
	customer_id, product_id,
	global_line_total_inc_vat, global_line_total_exc_vat
from v_invoices
where order_no in ('3100000145', '3000000480')

select h.store_name, 
	cast(cast(h.customer_id as int) as varchar(255)) as 'customer_id', 'I' + LEFT(h.document_type, 1) + cast(CAST(h.document_id as int) AS varchar) AS UniqueID,
	h.order_no, cast(cast(l.product_id as int)as varchar(255)) as 'product_id', 
	l.global_line_total_inc_vat, l.global_line_total_vat
FROM        
	invoice_headers h 
INNER JOIN 
	invoice_lines l ON h.document_type = l.document_type AND h.document_id = l.document_id and h.store_name=l.store_name
LEFT JOIN 
	[dw_invoice_line_sum] ls ON ls.document_id = l.document_id AND ls.document_type =  l.document_type
LEFT JOIN 
	dw_invoice_line_product_sum ps ON ps.document_id = l.document_id AND ps.document_type =  l.document_type AND ps.product_id = l.product_id
where h.order_id in (4807511, 5161914)
