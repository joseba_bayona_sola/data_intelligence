

select st.website, count(distinct order_no) num_orders, count(distinct customer_id) num_dist_customers,
	sum(i.global_line_total_inc_vat) as global_line_total_inc_vat,
	sum(i.global_line_total_exc_vat) as global_line_total_exc_vat
from 
		#Temp_v_invoices i 
	inner join 
		(select distinct website,store_name from v_stores_all) st on i.store_name=st.store_name
where website = 'visiondirect.co.uk'
	and  document_date between '2017-06-01' and '2017-06-05'
group by st.website
order by st.website

select website, UniqueID, order_no, document_date, document_type, 
	customer_id, product_id,
	global_line_total_inc_vat, global_line_total_exc_vat
from 
		#Temp_v_invoices i 
	inner join 
		(select distinct website,store_name from v_stores_all) st on i.store_name=st.store_name
where website = 'visiondirect.fr'
	and  document_date between '2017-07-01' and '2017-07-01'
order by document_date, order_no

-------------------------------------------------------------------------------------

select 
 	line_id, order_id, order_no, 
	document_date, 
	store_name, 
	customer_id, product_id,
	document_type, 
	global_line_total_inc_vat, global_line_total_exc_vat
from #Temp_v_invoices_2
order by document_date, line_id

