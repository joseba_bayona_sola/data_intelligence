
update DW_GetLenses_jbs.dbo.comp_current_dwh_ship
set global_line_total_inc_vat = 0
where global_line_total_inc_vat = 'NULL'

update DW_GetLenses_jbs.dbo.comp_current_dwh_ship
set global_line_total_exc_vat = 0
where global_line_total_exc_vat = 'NULL'

select 
	line_id, order_id, order_no, 
	convert(datetime, substring(document_date, 1, 19), 120) document_date, 
	store_name, 
	customer_id, product_id,
	document_type, 
	convert(decimal(12, 4), global_line_total_inc_vat) global_line_total_inc_vat, convert(decimal(12, 4), global_line_total_exc_vat) global_line_total_exc_vat
into #ship_curr_dwh
from DW_GetLenses_jbs.dbo.comp_current_dwh_ship
where convert(datetime, substring(document_date, 1, 19), 120) between '2017-06-01' and '2017-06-06'
	and store_name = 'visiondirect.co.uk' 
order by document_date


select shipment_line_id, 
	order_id_bk, order_no, shipment_date_r, website, 
	customer_id_bk, product_id_bk, order_status_name_bk, 
	countries_registered_code, product_type_vat, vat_rate, prof_fee_rate, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
into #ship_new_dwh
from
	(select oh.order_id_bk, oh.order_no,
		ol.order_line_id_bk, ol.invoice_line_id, ol.shipment_line_id, 
		ol.invoice_date, ol.shipment_date, ol.shipment_date_r,
		oh.order_status_name_bk,
		oh.store_id_bk, oh.customer_id_bk, 
		ol.product_id_bk, 
		ol.countries_registered_code, ol.product_type_vat, ol.vat_rate, ol.prof_fee_rate,
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, 
		ol.global_total_aft_refund_inc_vat, ol.global_total_aft_refund_exc_vat 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select order_id_bk, 
				order_line_id_bk, invoice_line_id, shipment_line_id, 
				invoice_date, shipment_date, 
				case when (order_status_name_bk in ('REFUND', 'PARTIAL REFUND') and local_total_aft_refund_inc_vat <> 0) then refund_date else shipment_date end shipment_date_r,
				product_id_bk, 
				countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
				local_total_aft_refund_inc_vat * local_to_global_rate global_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat * local_to_global_rate global_total_aft_refund_exc_vat 
			from Landing.aux.sales_fact_order_line_aud) ol on oh.order_id_bk = ol.order_id_bk) t
	inner join
		Landing.aux.mag_gen_store_v s on t.store_id_bk = s.store_id
where shipment_date_r between '2017-06-01' and '2017-06-06'
	and order_status_name_bk in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.co.uk' 
order by shipment_date_r, shipment_line_id



-----------------------------------------------------------------

-- 666.669 // 584.446
select sum(global_line_total_inc_vat), sum(global_line_total_exc_vat)
from #ship_curr_dwh

-- 663.235 // 581.088
select sum(global_total_aft_refund_inc_vat), sum(global_total_aft_refund_exc_vat)
from #ship_new_dwh


select t1.line_id, t2.shipment_line_id, 
	t1.order_id, t1.order_no, t1.document_date, 
	t1.customer_id, t1.product_id, 
	t1.document_type, t2.order_status_name_bk, 
	t2.countries_registered_code, t2.product_type_vat, t2.vat_rate, t2.prof_fee_rate, 
	t1.global_line_total_inc_vat, t2.global_total_aft_refund_inc_vat, 
	abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) diff_inc_vat,
	t1.global_line_total_exc_vat, t2.global_total_aft_refund_exc_vat, 
	abs(t1.global_line_total_exc_vat - t2.global_total_aft_refund_exc_vat) diff_exc_vat
from 
		#ship_curr_dwh t1
	full join
		#ship_new_dwh t2 on t1.line_id =  t2.shipment_line_id 
where t1.line_id is not null and t2.shipment_line_id is not null -- t1.line_id is null // -- t2.invoice_line_id is null
	-- and abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) > 1
order by order_status_name_bk, diff_inc_vat desc, document_date, line_id



select t1.line_id, t2.shipment_line_id, 
	t1.order_id, t2.order_id_bk, t1.order_no, t1.document_date, 
	t1.customer_id, t1.product_id, 
	t1.document_type, t2.order_status_name_bk, 
	t2.countries_registered_code, t2.product_type_vat, t2.vat_rate, t2.prof_fee_rate, 
	t1.global_line_total_inc_vat, t2.global_total_aft_refund_inc_vat, 
	abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) diff_inc_vat,
	t1.global_line_total_exc_vat, t2.global_total_aft_refund_exc_vat, 
	abs(t1.global_line_total_exc_vat - t2.global_total_aft_refund_exc_vat) diff_exc_vat
from 
		#ship_curr_dwh t1
	full join
		#ship_new_dwh t2 on t1.line_id =  t2.shipment_line_id 
-- where t2.shipment_line_id is null and t2.order_id_bk is null order by document_type, t1.global_line_total_inc_vat, document_date, line_id
where t1.line_id is null order by shipment_line_id

---------------------------------------------
select t1.line_id, t2.invoice_line_id, 
	t1.order_id, t1.order_no, t1.document_date, 
	t1.customer_id, t1.product_id, 
	t1.document_type, t2.order_status_name_bk, 
	t2.countries_registered_code, t2.product_type_vat, t2.vat_rate, t2.prof_fee_rate, 
	t1.global_line_total_inc_vat, t2.global_total_aft_refund_inc_vat, 
	abs(t1.global_line_total_inc_vat - t2.global_total_aft_refund_inc_vat) diff_inc_vat,
	t1.global_line_total_exc_vat, t2.global_total_aft_refund_exc_vat, 
	abs(t1.global_line_total_exc_vat - t2.global_total_aft_refund_exc_vat) diff_exc_vat
from 
		#inv_curr_dwh t1
	full join
		#inv_new_dwh t2 on t1.line_id =  t2.invoice_line_id 
order by order_id, line_id
