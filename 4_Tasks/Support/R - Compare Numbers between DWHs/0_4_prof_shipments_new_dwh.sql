
-- Orders (OL) that have been SHIPPED but for some reason NO INVOICE
select top 1000 order_line_id_bk,
	order_id_bk, order_date, invoice_date, shipment_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, line_status_name,
	-- local_total_inc_vat, local_total_exc_vat, 
	global_total_inc_vat, global_total_exc_vat, 
	-- local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where shipment_date is not null and invoice_date is null
order by order_date

-- Orders (OL) that have been invoice but NOT SHIPPED
	-- Line: OK and Total Inc = 0: 2950 product for discount
	-- Line: OK and Total Inc <> 0: Missing SH record due to ERP problems (12/4 - 23/5) - Still Waiting to shipped - Specific Problem
	-- Line: REFUND and Total Inc Aft Refund = 0: Refunded to Bank Money
	-- Line: REFUND and Total Inc Aft Refund <> 0: Refunded to Store Credit
select top 1000 order_line_id_bk,
	order_id_bk, order_date, invoice_date, shipment_date, shipment_date_r, refund_date, website, 
	customer_id, 
	order_stage_name, order_status_name, line_status_name,
	product_id_magento,
	-- local_total_inc_vat, local_total_exc_vat, 
	global_total_inc_vat, global_total_exc_vat, 
	-- local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where (shipment_date is null and invoice_date between '2017-01-01' and '2017-08-01')
	-- and line_status_name in ('OK') and global_total_inc_vat <> 0
	and line_status_name in ('REFUND', 'PARTIAL REFUND') and global_total_aft_refund_inc_vat <> 0
order by invoice_date

-- Orders (OL) that have been invoice and SHIPPED and later REFUNDED
	-- Line: REFUND and Total Inc Aft Refund = 0: Refunded to Bank Money
	-- Line: REFUND and Total Inc Aft Refund <> 0: Refunded to Store Credit
select top 1000 order_line_id_bk,
	order_id_bk, order_date, invoice_date, shipment_date, website, 
	customer_id, 
	order_stage_name, order_status_name, line_status_name,
	product_id_magento,
	-- local_total_inc_vat, local_total_exc_vat, 
	global_total_inc_vat, global_total_exc_vat, 
	-- local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where (shipment_date is not null and shipment_date between '2017-01-01' and '2017-08-01')
	and line_status_name in ('REFUND', 'PARTIAL REFUND') and global_total_aft_refund_inc_vat = 0
order by invoice_date

