
-- Due to Decimals between OH - OL // 2017-01 invoice orders done on 2016-12 still to be process
select order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	global_total_aft_refund_inc_vat, 
	sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where invoice_date between '2017-03-01' and '2017-04-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.fr'


select order_line_id_bk,
	order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat,
	sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat, 
	sum(global_total_aft_refund_inc_vat) over (partition by order_id_bk) sum_global_total_aft_refund_inc_vat_order
from Warehouse.sales.fact_order_line_v
where invoice_date between '2017-03-01' and '2017-04-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.fr'


select ol.order_line_id_bk,
	ol.order_id_bk, ol.order_date, ol.invoice_date, ol.website, 
	ol.customer_id, ol.country_name_ship, 
	ol.order_stage_name, ol.order_status_name, 
	ol.global_total_aft_refund_inc_vat, 
	oh.global_total_aft_refund_inc_vat, ol.sum_global_total_aft_refund_inc_vat_order, 
	oh.global_total_aft_refund_inc_vat - ol.sum_global_total_aft_refund_inc_vat_order diff
from
		(select order_id_bk, order_date, invoice_date, website, 
			customer_id, country_name_ship, 
			order_stage_name, order_status_name, 
			global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat, 
			sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat
		from Warehouse.sales.dim_order_header_v
		where invoice_date between '2017-03-01' and '2017-04-01'
			and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
			and website = 'visiondirect.fr') oh
	inner join
		(select order_line_id_bk,
			order_id_bk, order_date, invoice_date, website, 
			customer_id, country_name_ship, 
			order_stage_name, order_status_name, 
			global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat,
			sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat, 
			sum(global_total_aft_refund_inc_vat) over (partition by order_id_bk) sum_global_total_aft_refund_inc_vat_order
		from Warehouse.sales.fact_order_line_v
		where invoice_date between '2017-03-01' and '2017-04-01'
			and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
			and website = 'visiondirect.fr') ol on oh.order_id_bk = ol.order_id_bk
-- where ol.sum_global_total_aft_refund_inc_vat_order is null
order by diff desc, order_id_bk


---------------------------------------------------------------------------------------------

-- Orders that have been CANCELLED by cust service but that have already an INVOICE: Problem for new DWH: OH Line In and OL Line Not In
select order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	global_total_aft_refund_inc_vat, 
	sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where invoice_date between '2017-05-01' and '2017-06-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.be'
	and order_id_bk in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)

select order_line_id_bk,
	order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat,
	sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat, 
	sum(global_total_aft_refund_inc_vat) over (partition by order_id_bk) sum_global_total_aft_refund_inc_vat_order
from Warehouse.sales.fact_order_line_v
where invoice_date between '2017-05-01' and '2017-06-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.be'
	and order_id_bk in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)

select ol.order_line_id_bk,
	ol.order_id_bk, oh.order_id_bk,
	oh.order_date, oh.invoice_date, oh.website, 
	oh.customer_id, oh.country_name_ship, 
	oh.order_stage_name, oh.order_status_name, 
	ol.global_total_aft_refund_inc_vat, 
	oh.global_total_aft_refund_inc_vat, ol.sum_global_total_aft_refund_inc_vat_order, 
	oh.global_total_aft_refund_inc_vat - ol.sum_global_total_aft_refund_inc_vat_order diff
from
		(select order_id_bk, order_date, invoice_date, website, 
			customer_id, country_name_ship, 
			order_stage_name, order_status_name, 
			global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat, 
			sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat
		from Warehouse.sales.dim_order_header_v
		where invoice_date between '2017-05-01' and '2017-06-01'
			and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
			and website = 'visiondirect.be') oh
	left join
		(select order_line_id_bk,
			order_id_bk, order_date, invoice_date, website, 
			customer_id, country_name_ship, 
			order_stage_name, order_status_name, 
			global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat,
			sum(global_total_aft_refund_inc_vat) over () sum_global_total_aft_refund_inc_vat, 
			sum(global_total_aft_refund_inc_vat) over (partition by order_id_bk) sum_global_total_aft_refund_inc_vat_order
		from Warehouse.sales.fact_order_line_v
		where invoice_date between '2017-05-01' and '2017-06-01'
			and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
			and website = 'visiondirect.be') ol on oh.order_id_bk = ol.order_id_bk
where ol.order_id_bk is null
order by diff desc, oh.order_id_bk


select *
from Landing.mag.sales_flat_order_aud
where entity_id in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)
order by entity_id

select top 1000 *
from Landing.mag.sales_flat_order_item_aud
where order_id in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)
order by order_id, item_id


select top 1000 *
from Landing.mag.sales_flat_invoice_aud
where order_id in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)

select top 1000 ii.*
from 
		Landing.mag.sales_flat_invoice_item_aud ii
	inner join
		Landing.mag.sales_flat_invoice_aud i on ii.parent_id = i.entity_id
where i.order_id in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)


select top 1000 *
from Landing.mag.sales_flat_shipment_aud
where order_id in (5256860, 5263131, 5270724, 5287502, 5317285, 5327525, 5488738)

