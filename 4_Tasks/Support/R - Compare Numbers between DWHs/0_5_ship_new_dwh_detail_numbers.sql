

select order_line_id_bk,
	order_id_bk, order_no, shipment_date_r, website, 
	customer_id, product_id_magento, order_stage_name, order_status_name, 
	-- global_total_inc_vat, global_total_exc_vat, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.fact_order_line_v
where shipment_date_r between '2017-06-01' and '2017-06-06'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.co.uk' 
order by shipment_date_r, order_line_id_bk

--------------------------------------------------

select shipment_line_id, 
	order_id_bk, order_no, shipment_date_r, website, 
	customer_id_bk, product_id_bk, order_status_name_bk, 
	countries_registered_code, product_type_vat, vat_rate, prof_fee_rate, 
	global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from
	(select oh.order_id_bk, oh.order_no,
		ol.order_line_id_bk, ol.invoice_line_id, ol.shipment_line_id, 
		ol.invoice_date, ol.shipment_date, ol.shipment_date_r,
		oh.order_status_name_bk,
		oh.store_id_bk, oh.customer_id_bk, 
		ol.product_id_bk, 
		ol.countries_registered_code, ol.product_type_vat, ol.vat_rate, ol.prof_fee_rate,
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, 
		ol.global_total_aft_refund_inc_vat, ol.global_total_aft_refund_exc_vat 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select order_id_bk, 
				order_line_id_bk, invoice_line_id, shipment_line_id, 
				invoice_date, shipment_date, 
				case when (order_status_name_bk in ('REFUND', 'PARTIAL REFUND') and local_total_aft_refund_inc_vat <> 0) then refund_date else shipment_date end shipment_date_r,
				product_id_bk, 
				countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,
				local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, 
				local_total_aft_refund_inc_vat * local_to_global_rate global_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat * local_to_global_rate global_total_aft_refund_exc_vat 
			from Landing.aux.sales_fact_order_line_aud) ol on oh.order_id_bk = ol.order_id_bk) t
	inner join
		Landing.aux.mag_gen_store_v s on t.store_id_bk = s.store_id
where shipment_date_r between '2017-06-01' and '2017-06-06'
	and order_status_name_bk in ('OK', 'REFUND', 'PARTIAL REFUND')
	and website = 'visiondirect.co.uk' 
order by shipment_date_r, shipment_line_id