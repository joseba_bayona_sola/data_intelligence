
select top 1000 website, year(order_date) yyyy, month(order_date) mm, 
	count(*) num_orders, count(distinct customer_id) num_dist_customers, 
	-- sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_inc_vat, 
	sum(global_total_aft_refund_inc_vat) global_total_aft_refund_inc_vat, sum(global_total_aft_refund_exc_vat) global_total_aft_refund_exc_vat
from Warehouse.sales.dim_order_header_v
where order_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and invoice_date is null
group by website, year(order_date), month(order_date)
order by website, year(order_date), month(order_date)

select order_id_bk, order_date, invoice_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, order_status_magento_name, payment_method_name,
	local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, global_total_aft_refund_inc_vat, global_total_aft_refund_exc_vat
from Warehouse.sales.dim_order_header_v
where order_date between '2017-01-01' and '2017-08-01'
	and order_status_name in ('OK', 'REFUND', 'PARTIAL REFUND')
	and invoice_date is null
order by website, order_date

