use DW_GetLenses
go

drop table #Temp_v_invoices

select store_name, UniqueID, order_no, document_date, document_type, 
	customer_id, product_id,
	global_line_total_inc_vat, global_line_total_exc_vat 
into #Temp_v_invoices from v_invoices where document_date between '2017-01-01' and '2017-07-31'

create nonclustered index ix_uniqueid on #Temp_v_invoices(uniqueid) 
create nonclustered index ix_storename on #Temp_v_invoices(store_name)

select st.website, year(document_date) yyyy, month(document_date) mm,
	count(distinct order_no) num_orders, count(distinct customer_id) num_dist_customers,
	sum(i.global_line_total_inc_vat) as global_line_total_inc_vat,
	sum(i.global_line_total_exc_vat) as global_line_total_exc_vat
from 
		#Temp_v_invoices i 
	inner join 
		(select distinct website,store_name from v_stores_all) st on i.store_name=st.store_name
group by st.website, year(document_date), month(document_date)
order by st.website, year(document_date), month(document_date)

--------------------------------------------------------------------------------

drop table #Temp_v_invoices_2

select il.store_name, 
 	ih.order_id, il.line_id, ih.order_no, 
	il.document_date, il.document_type, 
	ih.customer_id, il.product_id,
	il.global_line_total_inc_vat, il.global_line_total_exc_vat
into #Temp_v_invoices_2
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join
		DW_GetLenses.dbo.invoice_headers ih on ih.document_id = il.document_id and ih.document_type = il.document_type
where il.document_date between '2017-01-01' and '2017-08-01'


select st.website, year(document_date) yyyy, month(document_date) mm,
	count(distinct order_no) num_orders, count(distinct customer_id) num_dist_customers,
	sum(i.global_line_total_inc_vat) as global_line_total_inc_vat,
	sum(i.global_line_total_exc_vat) as global_line_total_exc_vat
from 
		#Temp_v_invoices_2 i 
	inner join 
		(select distinct website,store_name from DW_GetLenses.dbo.v_stores_all) st on i.store_name=st.store_name
group by st.website, year(document_date), month(document_date)
order by st.website, year(document_date), month(document_date)

-----


