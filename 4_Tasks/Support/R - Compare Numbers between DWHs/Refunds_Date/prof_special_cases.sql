
-- Special Cases: 
	-- 5146778: Adjusted Order to Store Credit
	-- 5188519: Refunded Order to Bank with Adjustment
	-- 5258841: Refunded Order to both Bank and Store Credit with Adjustment
	-- 5290586: Partial Refund with Adjustment
	-- 5297202: Refunded Order to Store Credit with Adjustment
	-- 5300026: Refunded Order to both Bank and Store Credit
	-- 5326340: Partial Refund to Store Credit
	-- 5330831: Partial Refund to Bank
	-- 5515006: Refunded Order to Bank
	-- 5519809: Refunded Order to Store Credit
	-- 5543199: Adjusted Order to Bank
	-- 5746859: Refunded Order paid with SC refunded to Store Credit
	-- 5839394: Refunded Order paid with SC refunded to Bank
	-- 5851665: Refunded Order paid with SC (Not All) refunded to Store Credit 

select top 1000 order_id_bk, order_date, invoice_date, refund_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, adjustment_order,
	global_total_refund, global_store_credit_given, global_bank_online_given,
	global_total_inc_vat, global_total_aft_refund_inc_vat, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund	
from Warehouse.sales.dim_order_header_v
where order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586, 5746859, 5839394, 5851665)
order by order_id_bk

select top 1000 order_id_bk, order_line_id_bk,
	order_date, invoice_date, refund_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, 
	global_store_credit_given, global_bank_online_given,
	global_total_inc_vat, global_total_aft_refund_inc_vat,
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund, 
	qty_unit, qty_unit_refunded
from Warehouse.sales.fact_order_line_v
where order_id_bk in (5519809, 5515006, 5543199, 5146778, 5258841, 5188519, 5297202, 5300026, 5326340, 5330831, 5290586, 5746859, 5839394, 5851665)
order by order_id_bk, order_line_id_bk


-----------------------------------

select top 1000 order_id_bk, order_date, invoice_date, refund_date, website, 
	customer_id, country_name_ship, 
	order_stage_name, order_status_name, adjustment_order,
	global_total_refund, global_store_credit_given, global_bank_online_given,
	global_total_inc_vat, global_total_aft_refund_inc_vat, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund	
from Warehouse.sales.dim_order_header_v
where order_status_name = 'PARTIAL REFUND' and adjustment_order = 'Y'
	-- and global_bank_online_given <> 0 and global_store_credit_given <> 0
order by website, order_date