	
	select st.website, year(document_date) yyyy, month(document_date) mm,
		count(distinct order_no) num_orders, count(distinct customer_id) num_dist_customers,
		sum(s.global_line_total_inc_vat) as global_line_total_inc_vat,
		sum(s.global_line_total_exc_vat) as global_line_total_exc_vat
	from 
		v_shipments s
	inner join 
		(select distinct website, store_name from v_stores_all) st on st.store_name=s.store_name
	where 
		s.document_date between '2017-01-01' and '2017-07-31'
	group by st.website, year(document_date), month(document_date)
	order by st.website, year(document_date), month(document_date)

--------------------------------------------------------------------------------

drop table #Temp_v_shipments_2

select sl.store_name, 
 	sh.order_id, sl.line_id, sh.order_no, 
	sl.document_date, sl.document_type, 
	sh.customer_id, sl.product_id,
	sl.global_line_total_inc_vat, sl.global_line_total_exc_vat
into #Temp_v_shipments_2
from 
		DW_GetLenses.dbo.shipment_lines sl
	inner join
		DW_GetLenses.dbo.shipment_headers sh on sh.document_id = sl.document_id and sh.document_type = sl.document_type
where sl.document_date between '2017-01-01' and '2017-08-01'


select st.website, year(document_date) yyyy, month(document_date) mm,
	count(distinct order_no) num_orders, count(distinct customer_id) num_dist_customers,
	sum(s.global_line_total_inc_vat) as global_line_total_inc_vat,
	sum(s.global_line_total_exc_vat) as global_line_total_exc_vat
from 
		#Temp_v_shipments_2 s 
	inner join 
		(select distinct website,store_name from v_stores_all) st on s.store_name=st.store_name
group by st.website, year(document_date), month(document_date)
order by st.website, year(document_date), month(document_date)






