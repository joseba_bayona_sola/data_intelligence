
select c.customer_id, rc.email
from 
		DW_GetLenses_jbs.dbo.ex_recovered_customers_fr rc
	left join 
		DW_GetLenses.dbo.customers c on rc.email = c.email
order by rc.email
