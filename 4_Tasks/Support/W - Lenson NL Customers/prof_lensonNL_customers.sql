
select top 1000 *
from Landing.mag.customer_entity_flat_aud

select top 1000 old_access_cust_no, count(*)
from Landing.mag.customer_entity_flat_aud
group by old_access_cust_no
order by count(*) desc


select top 1000 migrate_customer_store, count(*)
from Warehouse.gen.dim_customer_v
group by migrate_customer_store
order by migrate_customer_store

select *
from Landing.map.gen_migrate_customer_store

select *
from Landing.mag.customer_entity_flat_aud
where old_access_cust_no = 'LENSONNL'
	-- and idETLBatchRun in (1072, 1074)
	-- and store_id <> 22
order by entity_id desc


select *
from Warehouse.gen.dim_customer_v
where migrate_customer_store = 'LENSON NL'
order by customer_id_bk

-------------------------------------------------------

select mc.old_customer_id, mc.new_magento_id, c.migrate_customer_store, 
	cs.first_name, cs.last_name, cs.city, cs.postcode
from 
		Landing.migra_lensonnl.migrate_customerdata mc
	inner join
		Warehouse.gen.dim_customer_v c on mc.new_magento_id = c.customer_id_bk
	inner join
		Warehouse.gen.dim_customer_scd_v cs on c.idCustomer_sk = cs.idCustomer_sk_fk
order by mc.new_magento_id