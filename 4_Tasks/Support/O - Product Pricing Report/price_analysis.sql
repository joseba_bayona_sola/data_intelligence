
select top 1000 order_id_bk, order_no, order_date, order_status_name, product_family_name, 
	qty_unit, qty_pack, price_type_name, 
	local_price_pack, local_price_pack_discount, 
	local_subtotal, local_discount, coupon_code, discount_percent
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1083
	and store_name = 'visiondirect.co.uk'
	and local_price_pack not in (12.4500, 15.9900)
	-- and local_discount <> 0
order by order_date

select top 1000 price_type_name, local_price_pack, count(*), sum(local_subtotal), sum(local_discount)
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1083
	and store_name = 'visiondirect.co.uk'
group by price_type_name, local_price_pack

select top 1000 price_type_name, local_price_pack_discount, count(*), sum(local_subtotal), sum(local_discount)
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1083
	and store_name = 'visiondirect.co.uk'
	and local_discount <> 0
group by price_type_name, local_price_pack_discount


select * 
from Warehouse.sales.dim_price_type