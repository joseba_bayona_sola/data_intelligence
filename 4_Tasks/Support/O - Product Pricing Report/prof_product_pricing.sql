
select top 1000 order_id_bk, order_date_c, order_status_name, 
	store_name, 
	product_type_oh_name, product_id_magento, product_family_name, 
	case 
		when (price_type_name = 'Discounted') then 'Regular' 
		when (price_type_name = 'PLA & Discounted') then 'PLA' 
		when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
		else price_type_name
	end price_type_name, 
	qty_pack, local_price_pack, local_subtotal
from Warehouse.sales.fact_order_line_v
where order_date > '2017-01-01'
	and order_status_name not in ('CANCEL', 'REFUND')
	and product_id_magento = 1083
order by order_date

select store_name, category_name, product_id_magento, product_family_name, 
	price_type_name, local_price_pack, 
	count(*) num_lines, count(distinct order_id_bk) num_orders, sum(qty_pack) num_packs, sum(local_subtotal) total_subtotal
from 
	(select order_id_bk, order_date_c, order_status_name, 
		store_name, 
		category_name, product_id_magento, product_family_name, 
		case 
			when (price_type_name = 'Discounted') then 'Regular' 
			when (price_type_name = 'PLA & Discounted') then 'PLA' 
			when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
			else price_type_name
		end price_type_name, 
		qty_pack, local_price_pack, local_subtotal
	from Warehouse.sales.fact_order_line_v
	where order_date > '2017-01-01'
		and order_status_name not in ('CANCEL', 'REFUND')
		-- and product_id_magento = 108
		) ol
group by store_name, category_name, product_id_magento, product_family_name, 
	price_type_name, local_price_pack
order by product_id_magento, store_name, category_name,  
	price_type_name, local_price_pack


select store_name, category_name, product_id_magento, product_family_name, 
	price_type_name, local_price_pack, 
	num_lines, num_orders, num_packs, total_subtotal, 
	--sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name) sum_total_subtotal, 
	rank() over (partition by store_name, category_name, product_id_magento, product_family_name, price_type_name order by local_price_pack) rank_price,
	convert(decimal(12, 4), total_subtotal * 100 / sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name)) p_subtotal
from
	(select store_name, category_name, product_id_magento, product_family_name, 
		price_type_name, local_price_pack, 
		count(*) num_lines, count(distinct order_id_bk) num_orders, sum(qty_pack) num_packs, sum(local_subtotal) total_subtotal
	from 
		(select order_id_bk, order_date_c, order_status_name, 
			store_name, 
			category_name, product_id_magento, product_family_name, 
			case 
				when (price_type_name = 'Discounted') then 'Regular' 
				when (price_type_name = 'PLA & Discounted') then 'PLA' 
				when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
				else price_type_name
			end price_type_name, 
			qty_pack, local_price_pack, local_subtotal
		from Warehouse.sales.fact_order_line_v
		where order_date > '2017-01-01'
			and order_status_name not in ('CANCEL', 'REFUND')
			and product_id_magento not in (2949, 2950, 3124, 3125, 3158)
			-- and product_id_magento = 108
			) ol
	group by store_name, category_name, product_id_magento, product_family_name, 
		price_type_name, local_price_pack) t
where store_name = 'visiondirect.es'
order by product_id_magento, store_name, category_name,  
	price_type_name, local_price_pack


----------------------------------------------------------------------------------------------

select store_name, category_name, product_id_magento, product_family_name, 
	price_type_name, 'p1_' + price_type_name price_type_name_p1, 'p2_' + price_type_name price_type_name_p2, 
	'p3_' + price_type_name price_type_name_p3, 'p4_' + price_type_name price_type_name_p4, 
	local_price_pack, 
	num_lines, num_orders, num_packs, total_subtotal, 
	rank_price, p_subtotal
from
	(select store_name, category_name, product_id_magento, product_family_name, 
		price_type_name, local_price_pack, 
		num_lines, num_orders, num_packs, total_subtotal, 
		--sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name) sum_total_subtotal, 
		rank() over (partition by store_name, category_name, product_id_magento, product_family_name, price_type_name order by local_price_pack) rank_price,
		convert(decimal(12, 4), total_subtotal * 100 / sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name)) p_subtotal
	from
		(select store_name, category_name, product_id_magento, product_family_name, 
			price_type_name, local_price_pack, 
			count(*) num_lines, count(distinct order_id_bk) num_orders, sum(qty_pack) num_packs, sum(local_subtotal) total_subtotal
		from 
			(select order_id_bk, order_date_c, order_status_name, 
				store_name, 
				category_name, product_id_magento, product_family_name, 
				case 
					when (price_type_name = 'Discounted') then 'Regular' 
					when (price_type_name = 'PLA & Discounted') then 'PLA' 
					when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
					else price_type_name
				end price_type_name, 
				qty_pack, local_price_pack, local_subtotal
			from Warehouse.sales.fact_order_line_v
			where order_date > '2017-01-01'
				and order_status_name not in ('CANCEL', 'REFUND')
				and product_id_magento not in (2949, 2950, 3124, 3125, 3158)
				-- and product_id_magento = 108
				) ol
		group by store_name, category_name, product_id_magento, product_family_name, 
			price_type_name, local_price_pack) t
	where store_name = 'visiondirect.es') t
where rank_price = 1
order by product_id_magento, store_name, category_name,  
	price_type_name, local_price_pack


select 
	store_name, category_name, product_id_magento, 
	Regular regular_price_pack, PLA pla_price_pack, [Tier Pricing] tier_pricing_price_pack, 
	p1_Regular regular_num_orders, p1_PLA pla_num_orders, [p1_Tier Pricing] tier_pricing_num_orders, 
	p2_Regular regular_num_packs, p2_PLA pla_num_packs, [p2_Tier Pricing] tier_pricing_num_packs, 
	p3_Regular regular_total_subtotal, p2_PLA pla_total_subtotal, [p3_Tier Pricing] tier_pricing_total_subtotal, 
	p4_Regular regular_p_subtotal, p4_PLA pla_p_subtotal, [p4_Tier Pricing] tier_pricing_p_subtotal 
	 

from
		(select store_name, category_name, product_id_magento, product_family_name, 
			price_type_name, 'p1_' + price_type_name price_type_name_p1, 'p2_' + price_type_name price_type_name_p2, 
			'p3_' + price_type_name price_type_name_p3, 'p4_' + price_type_name price_type_name_p4, 
			local_price_pack, 
			num_lines, num_orders, num_packs, total_subtotal, 
			rank_price, p_subtotal
		from
			(select store_name, category_name, product_id_magento, product_family_name, 
				price_type_name, local_price_pack, 
				num_lines, num_orders, num_packs, total_subtotal, 
				--sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name) sum_total_subtotal, 
				rank() over (partition by store_name, category_name, product_id_magento, product_family_name, price_type_name order by local_price_pack) rank_price,
				convert(decimal(12, 4), total_subtotal * 100 / sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name)) p_subtotal
			from
				(select store_name, category_name, product_id_magento, product_family_name, 
					price_type_name, local_price_pack, 
					count(*) num_lines, count(distinct order_id_bk) num_orders, sum(qty_pack) num_packs, sum(local_subtotal) total_subtotal
				from 
					(select order_id_bk, order_date_c, order_status_name, 
						store_name, 
						category_name, product_id_magento, product_family_name, 
						case 
							when (price_type_name = 'Discounted') then 'Regular' 
							when (price_type_name = 'PLA & Discounted') then 'PLA' 
							when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
							else price_type_name
						end price_type_name, 
						qty_pack, local_price_pack, local_subtotal
					from Warehouse.sales.fact_order_line_v
					where order_date > '2017-01-01'
						and order_status_name not in ('CANCEL', 'REFUND')
						and product_id_magento not in (2949, 2950, 3124, 3125, 3158)
						-- and product_id_magento = 108
						) ol
				group by store_name, category_name, product_id_magento, product_family_name, 
					price_type_name, local_price_pack) t
			where store_name = 'visiondirect.es') t
		where rank_price = 1) t
	pivot
		(min(local_price_pack) for 
		price_type_name in (PLA, Regular, [Tier Pricing])) pvt
	pivot
		(min(num_orders) for 
		price_type_name_p1 in (p1_PLA, p1_Regular, [p1_Tier Pricing])) pvt1
	pivot
		(min(num_packs) for 
		price_type_name_p2 in (p2_PLA, p2_Regular, [p2_Tier Pricing])) pvt2
	pivot
		(min(total_subtotal) for 
		price_type_name_p3 in (p3_PLA, p3_Regular, [p3_Tier Pricing])) pvt3
	pivot
		(min(p_subtotal) for 
		price_type_name_p4 in (p4_PLA, p4_Regular, [p4_Tier Pricing])) pvt4

order by product_id_magento, store_name, category_name



select store_name, category_name, product_id_magento, product_family_name, -- local_price_pack,
	min(regular_price_pack) regular_price_pack, min(tier_pricing_price_pack) tier_pricing_price_pack, min(pla_price_pack) pla_price_pack, 
	min(regular_num_orders) regular_num_orders, min(tier_pricing_num_orders) tier_pricing_num_orders, min(pla_num_orders) pla_num_orders, 
	min(regular_num_packs) regular_num_packs, min(tier_pricing_num_packs) tier_pricing_num_packs, min(pla_num_packs) pla_num_packs, 
	min(regular_total_subtotal) regular_total_subtotal, min(tier_pricing_total_subtotal) tier_pricing_total_subtotal, min(pla_total_subtotal) pla_total_subtotal, 
	min(regular_p_subtotal) regular_p_subtotal, min(tier_pricing_p_subtotal) tier_pricing_p_subtotal, min(pla_p_subtotal) pla_p_subtotal
from
	(select 
		store_name, category_name, product_id_magento, product_family_name, -- local_price_pack,
		Regular regular_price_pack, PLA pla_price_pack, [Tier Pricing] tier_pricing_price_pack, 
		p1_Regular regular_num_orders, p1_PLA pla_num_orders, [p1_Tier Pricing] tier_pricing_num_orders, 
		p2_Regular regular_num_packs, p2_PLA pla_num_packs, [p2_Tier Pricing] tier_pricing_num_packs, 
		p3_Regular regular_total_subtotal, p2_PLA pla_total_subtotal, [p3_Tier Pricing] tier_pricing_total_subtotal, 
		p4_Regular regular_p_subtotal, p4_PLA pla_p_subtotal, [p4_Tier Pricing] tier_pricing_p_subtotal 
	from
			(select store_name, category_name, product_id_magento, product_family_name, 
				price_type_name, 
				'p1_' + price_type_name price_type_name_p1, 'p2_' + price_type_name price_type_name_p2, 
				'p3_' + price_type_name price_type_name_p3, 'p4_' + price_type_name price_type_name_p4, 
				local_price_pack, 
				num_lines, num_orders, num_packs, total_subtotal, 
				rank_price, p_subtotal
			from
				(select store_name, category_name, product_id_magento, product_family_name, 
					price_type_name, local_price_pack, 
					num_lines, num_orders, num_packs, total_subtotal, 
					--sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name) sum_total_subtotal, 
					rank() over (partition by store_name, category_name, product_id_magento, product_family_name, price_type_name order by local_price_pack) rank_price,
					convert(decimal(12, 4), total_subtotal * 100 / sum(total_subtotal) over (partition by store_name, category_name, product_id_magento, product_family_name)) p_subtotal
				from
					(select store_name, category_name, product_id_magento, product_family_name, 
						price_type_name, local_price_pack, 
						count(*) num_lines, count(distinct order_id_bk) num_orders, sum(qty_pack) num_packs, sum(local_subtotal) total_subtotal
					from 
						(select order_id_bk, order_date_c, order_status_name, 
							store_name, 
							category_name, product_id_magento, product_family_name, 
							case 
								when (price_type_name = 'Discounted') then 'Regular' 
								when (price_type_name = 'PLA & Discounted') then 'PLA' 
								when (price_type_name = 'Tier Pricing & Discounted') then 'Tier Pricing' 
								else price_type_name
							end price_type_name, 
							qty_pack, local_price_pack, local_subtotal
						from Warehouse.sales.fact_order_line_v
						where order_date > '2017-01-01'
							and order_status_name not in ('CANCEL', 'REFUND')
							and product_id_magento not in (2949, 2950, 3124, 3125, 3158)
							-- and product_id_magento = 108
							) ol
					group by store_name, category_name, product_id_magento, product_family_name, 
						price_type_name, local_price_pack) t
				where store_name = 'visiondirect.co.uk') t
			where rank_price = 1) t
		pivot
			(min(local_price_pack) for 
			price_type_name in (PLA, Regular, [Tier Pricing])) pvt
		pivot
			(min(num_orders) for 
			price_type_name_p1 in (p1_PLA, p1_Regular, [p1_Tier Pricing])) pvt1
		pivot
			(min(num_packs) for 
			price_type_name_p2 in (p2_PLA, p2_Regular, [p2_Tier Pricing])) pvt2
		pivot
			(min(total_subtotal) for 
			price_type_name_p3 in (p3_PLA, p3_Regular, [p3_Tier Pricing])) pvt3
		pivot
			(min(p_subtotal) for 
			price_type_name_p4 in (p4_PLA, p4_Regular, [p4_Tier Pricing])) pvt4) t
group by store_name, category_name, product_id_magento, product_family_name
order by product_id_magento, store_name, category_name

