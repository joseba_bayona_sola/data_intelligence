
select product_id_magento, manufacturer_name, product_type_name, category_name, cl_type_name, cl_feature_name, product_family_name
from Warehouse.prod.dim_product_family_v
-- where product_family_name like 'Dailies%' -- Everclear - 1 Day Acuvue - Clariti - Dailies
where cl_feature_name = 'SiHy'
order by category_name, cl_type_name, product_family_name

-- 3168 (Everclear Elite)
-- 1084 - 3164 (1 Day Acuvue Trueye)
-- 2301 (clariti 1 day)
-- 2298 - 3165 (Dailies Total 1)
-- 2334 (MyDay)


select top 1000 *
from Warehouse.sales.fact_order_line_v

select order_id_bk, order_no, order_date, 
	customer_id, line_status_name, customer_order_seq_no, 
	product_id_magento 
into #everclear_elite_sales_an
from Warehouse.sales.fact_order_line_v
where product_id_magento in (3168)
	and website_group = 'VisionDirect'

select count(distinct order_id_bk)
from #everclear_elite_sales_an

select year(order_date), month(order_date), day(order_date), count(distinct order_id_bk)
from #everclear_elite_sales_an
group by year(order_date), month(order_date), day(order_date)
order by year(order_date), month(order_date), day(order_date)

select *
from #everclear_elite_sales_an
where customer_id = 68961


select order_id_bk, customer_id, max(conversion) conversion_f
from
	(select *, 
		case when (product_id_magento in (1084, 2301, 2298, 2334)) then 'Y' else 'N' end conversion
	from
		(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
			-- ol.order_id_bk, ol.order_no, 
			ol.order_date order_date_prev, 
			-- ol.customer_id, ol.line_status_name, ol.customer_order_seq_no, 
			ol.product_id_magento, ol.product_family_name, 
			max(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) last_order_per_cust
		from
				(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
				from #everclear_elite_sales_an
				where customer_id in (1621567, 1621772, 2341141, 68961, 7296)
				) ee 
			left join
				Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date > ol.order_date) t
	where order_date_prev = last_order_per_cust or last_order_per_cust is null) t 
group by order_id_bk, customer_id
order by conversion_f, customer_id

select order_id_bk, customer_id, max(repetition) repetition_f
from
	(select *, 
		case when (product_id_magento in (3168)) then 'Y' else 'N' end repetition
	from
		(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
			-- ol.order_id_bk, ol.order_no, 
			ol.order_date order_date_next, 
			-- ol.customer_id, ol.line_status_name, ol.customer_order_seq_no, 
			ol.product_id_magento, ol.product_family_name, 
			min(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) first_order_per_cust
		from
				(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
				from #everclear_elite_sales_an
				-- where customer_id in (1621567, 1621772, 2341141, 68961, 7296)
				) ee 
			left join
				Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date < ol.order_date) t
	where order_date_next = first_order_per_cust or first_order_per_cust is null) t 
group by order_id_bk, customer_id
order by repetition_f, customer_id

select t1.order_id_bk, t1.customer_id, t1.conversion_f, t2.repetition_f
into #everclear_elite_orders
from
		(select order_id_bk, customer_id, max(conversion) conversion_f
		from
			(select *, 
				case when (product_id_magento in (1084, 2301, 2298, 2334)) then 'Y' else 'N' end conversion
			from
				(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
					ol.order_date order_date_prev, 
					ol.product_id_magento, ol.product_family_name, 
					max(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) last_order_per_cust
				from
						(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
						from #everclear_elite_sales_an) ee 
					left join
						Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date > ol.order_date) t
			where order_date_prev = last_order_per_cust or last_order_per_cust is null) t 
		group by order_id_bk, customer_id) t1
	inner join
		(select order_id_bk, customer_id, max(repetition) repetition_f
		from
			(select *, 
				case when (product_id_magento in (3168)) then 'Y' else 'N' end repetition
			from
				(select ee.order_id_bk, ee.order_date, ee.customer_id, ee.customer_order_seq_no, 
					ol.order_date order_date_next, 
					ol.product_id_magento, ol.product_family_name, 
					min(ol.order_date) over (partition by ee.customer_id, ee.order_id_bk) first_order_per_cust
				from
						(select distinct order_id_bk, order_date, customer_id, customer_order_seq_no
						from #everclear_elite_sales_an) ee 
					left join
						Warehouse.sales.fact_order_line_v ol on ee.customer_id = ol.customer_id and ee.order_date < ol.order_date) t
			where order_date_next = first_order_per_cust or first_order_per_cust is null) t 
		group by order_id_bk, customer_id) t2 on t1.order_id_bk = t2.order_id_bk
order by t1.conversion_f, t2.repetition_f

select *
from #everclear_elite_orders

select conversion_f, repetition_f, count(*)
from #everclear_elite_orders
group by conversion_f, repetition_f
order by conversion_f, repetition_f

------------------------------------------------------------------------

	select oh.order_id_bk, oh.order_no, oh.order_date_c, 
		oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.website_group_create,
		oh.market_name, 
		oh.customer_id, oh.customer_email, 
		oh.country_code_ship, oh.country_code_bill, 
		oh.order_stage_name, oh.order_status_name, 
		oh.channel_name, oh.marketing_channel_name, 
		oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, 
		oh.product_type_oh_name, oh.num_diff_product_type_oh, 
		
		oh.rank_order_qty_time, oh.order_qty_time, 
		
		acs.rank_next_order_freq_time, acs.next_order_freq_time,
		ee.conversion_f, ee.repetition_f,

		oh.local_total_inc_vat, oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, 
		oh.global_total_inc_vat, oh.global_total_exc_vat, oh.global_total_vat, oh.global_total_prof_fee,

		isnull(oh.idETLBatchRun_upd, oh.idETLBatchRun_ins) idETLBatchRun
	from 
			Warehouse.sales.dim_order_header_v oh
		inner join
			Warehouse.act.fact_activity_sales_v acs on oh.order_id_bk = acs.order_id_bk
		inner join
			Adhoc.sales.aux_oh_everclear_elite ee on oh.order_id_bk = ee.order_id_bk
	order by oh.order_date_c

	-- Missing ATR: Customer Unsubscribe - Shipment Days
	select top 1000 count(*) over () num_tot,
		oh.order_id_bk, oh.order_no, oh.order_date_c, 
		oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.website_group_create,
		oh.market_name, 
		oh.customer_id, oh.customer_email, 
		oh.country_code_ship, oh.country_code_bill, 
		oh.order_stage_name, oh.order_status_name, 
		oh.channel_name, oh.marketing_channel_name, 
		oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, 
		oh.product_type_oh_name, oh.num_diff_product_type_oh, 
		ol.product_type_name, ol.category_name, ol.cl_type_name, ol.cl_feature_name,
		ol.product_id_magento, ol.product_family_name,

		ol.qty_unit, ol.qty_pack, 
		ol.rank_qty_time, ol.qty_time, acs.rank_next_order_freq_time, acs.next_order_freq_time,
		ol.price_type_name, ol.discount_f, 
		ol.local_price_pack, ol.local_price_pack_discount, 
		
		isnull(ee.conversion_f, 'N') conversion_f, isnull(ee.repetition_f, 'N') repetition_f,

		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.global_total_inc_vat, ol.global_total_exc_vat, ol.global_total_vat, ol.global_total_prof_fee,

		isnull(oh.idETLBatchRun_upd, oh.idETLBatchRun_ins) idETLBatchRun
	from 
			Warehouse.sales.dim_order_header_v oh
		inner join
			Warehouse.act.fact_activity_sales_v acs on oh.order_id_bk = acs.order_id_bk
		inner join
			Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk
		left join
			Adhoc.sales.aux_oh_everclear_elite ee on oh.order_id_bk = ee.order_id_bk
	where oh.order_date_c > '2017-01-01'
		and ol.product_id_magento in (3168, 1084, 2301, 2298, 2334) -- 3168, 1084, 2301, 2298, 2334
	-- order by oh.order_date_c



	select *
	from Landing.mend.gen_prod_productfamilypacksize_v
	where magentoProductID_int = 3168
