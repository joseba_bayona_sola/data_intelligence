
	select document_type, count(*)
	from DW_Proforma.dbo.order_headers
	where store_name = 'lensbase.fr'
	group by document_type
	order by document_type

select top 100 *
--from DW_Proforma.dbo.order_headers where document_type = 'ORDER'
from DW_Proforma.dbo.invoice_headers where document_type = 'INVOICE'
	and customer_id in (1187444, 1187478, 1187479, 1187038, 1187480)
order by customer_id, order_date

select top 100 *
--from DW_GetLenses.dbo.order_headers where document_type = 'ORDER'
from DW_GetLenses.dbo.invoice_headers where document_type = 'INVOICE'
	and customer_id in (1187444, 1187478, 1187479, 1187038, 1187480)
order by customer_id, order_date

select top 1000 *
from DW_GetLenses.dbo.v_invoices 
where customer_id in (1187444, 1187478, 1187479, 1187038, 1187480)
order by document_date

select top 100 *
from DW_GetLenses.dbo.customers
where customer_id in (1187444, 1187478, 1187479, 1187038, 1187480)
order by customer_id

--------------------------------------------------------------

select *
from DW_GetLenses.dbo.V_Stores_All
order by store_id

	select co.*
	from
			(select 1187444 customer_id
			union
			select 1187478 customer_id
			union
			select 1187479 customer_id
			union
			select 1187038 customer_id
			union
			select 1187480 customer_id) fo
		inner join
			(select o.customer_id,
				case o.source 
					when 'visio_db' then 'VisioOptik'
					when 'lensbase_db' then 'lensbase' + s.tld
					when 'masterlens_db' then 'masterlens' + s.tld
					else s.store_name 
				end as store_name,
				case when source = 'getlenses' then 'gl' else 'hist' end as src,
				row_number() over(partition by o.customer_id order by o.order_date asc) as rno, 
				o.order_date
			from 
					(select ih.customer_id, 'getlenses' as source, store_id, order_date 
					from 
							DW_GetLenses.dbo.invoice_headers ih 
						inner join 
							DW_GetLenses.dbo.dw_stores_full s on s.store_name = ih.store_name 
						inner join 
							(select 1187444 customer_id
							union
							select 1187478 customer_id
							union
							select 1187479 customer_id
							union
							select 1187038 customer_id
							union
							select 1187480 customer_id) fo on fo.customer_id = ih.customer_id
					union
					select olc.customer_id, source, store_id, created_at 
					from 
							dw_proforma.dbo.order_lifecycle olc 
						inner join 
							(select 1187444 customer_id
							union
							select 1187478 customer_id
							union
							select 1187479 customer_id
							union
							select 1187038 customer_id
							union
							select 1187480 customer_id) fo on fo.customer_id = olc.customer_id
					union
					select oh.customer_id, source, store_id, created_at 
					from 
							DW_GetLenses.dbo.dw_hist_order oh 
						inner join
							(select 1187444 customer_id
							union
							select 1187478 customer_id
							union
							select 1187479 customer_id
							union
							select 1187038 customer_id
							union
							select 1187480 customer_id) fo on fo.customer_id=oh.customer_id) o
				inner join
					DW_GetLenses.dbo.V_Stores_All s on s.store_id=o.store_id) co on co.customer_id = fo.customer_id and co.rno = 1


	SELECT 
		i.store_name AS [Current Site],
		CASE 
			when i.store_name = firstorder.website and firstorder.src = 'gl' then 'New'
			else firstorder.website 
		end AS [Previous Site],
		DATEname(MONTH,i.document_date) + '-' + DATENAME(YEAR,i.document_date) AS [Date],

		SUM(i.global_line_total_exc_vat) AS [revenue_ex_VAT], sum(i.global_line_total_inc_vat) as [revenue_in_VAT], sum(i.global_line_total_vat) as [VAT],
		sum(i.global_prof_fee) as [prof_fee], 

		count(*) num_orders, count(distinct i.customer_id) num_customers 
	FROM 
			DW_GetLenses.dbo.v_invoices i
		inner join 
			(select 1187444 customer_id, 'lensbase.fr' website, 'hist' src, 1 rno,  '2010-01-04' order_date
			union
			select 1187478 customer_id, 'lensbase.fr' website, 'hist' src, 1 rno,  '2010-01-03' order_date
			union
			select 1187479 customer_id, 'lensbase.fr' website, 'hist' src, 1 rno,  '2010-01-03' order_date
			union
			select 1187038 customer_id, 'lensbase.fr' website, 'hist' src, 1 rno,  '2010-01-03' order_date
			union
			select 1187480 customer_id, 'lensbase.fr' website, 'hist' src, 1 rno,  '2010-01-03' order_date) firstorder on firstorder.customer_id = i.customer_id
	--WHERE i.document_date BETWEEN @from_date AND @to_date
	GROUP BY 
		i.store_name,
		CASE 
			when i.store_name=firstorder.website and firstorder.src='gl' then 'New'
			else firstorder.website 
		end,
		DATENAME(MONTH,i.document_date) + '-' + DATENAME(YEAR,i.document_date)