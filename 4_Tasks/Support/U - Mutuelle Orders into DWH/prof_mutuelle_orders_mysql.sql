
select entity_id order_id_bk, increment_id order_no, store_id, customer_id, created_at, status, 
	base_subtotal local_subtotal, base_shipping_amount local_shipping, 
	case when (base_discount_amount < 0) then base_discount_amount * -1 else base_discount_amount end local_discount, ifnull(base_customer_balance_amount, 0) local_store_credit_used, 
	base_grand_total local_total_inc_vat, 

	ifnull(base_customer_balance_refunded, 0) + ifnull(base_total_refunded, 0) local_total_refund, 
	ifnull(bs_customer_bal_total_refunded, 0) local_store_credit_given, 
	(ifnull(base_customer_balance_refunded, 0) + ifnull(base_total_refunded, 0)) - ifnull(bs_customer_bal_total_refunded, 0) local_bank_online_given, 
	mutual_amount
from magento01.sales_flat_order
where mutual_amount is not null
order by status, created_at