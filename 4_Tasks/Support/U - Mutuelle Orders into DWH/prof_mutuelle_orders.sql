
select *
from Landing.mag.sales_flat_order_aud
where mutual_amount is not null

select *
from Landing.mag.mutual_quotation_aud
order by created_at desc

select oh.entity_id order_id_bk, increment_id order_no, store_id, customer_id, oh.created_at, 
	mutual_amount, mutual_quotation_id, 
	mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount, 
	ohp.method,
	status, affilCode, coupon_code, 
	base_subtotal local_subtotal, base_shipping_amount local_shipping, 
	case when (base_discount_amount < 0) then base_discount_amount * -1 else base_discount_amount end local_discount, isnull(base_customer_balance_amount, 0) local_store_credit_used, 
	base_grand_total local_total_inc_vat, 

	isnull(base_customer_balance_refunded, 0) + isnull(base_total_refunded, 0) local_total_refund, 
	isnull(bs_customer_bal_total_refunded, 0) local_store_credit_given, 
	(isnull(base_customer_balance_refunded, 0) + isnull(base_total_refunded, 0)) - isnull(bs_customer_bal_total_refunded, 0) local_bank_online_given
from 
		Landing.mag.sales_flat_order_aud oh
	inner join
		Landing.mag.sales_flat_order_payment_aud ohp on oh.entity_id = ohp.parent_id
	left join
		Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
	left join 
		Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
where mutual_amount is not null
	-- and mutual_amount <> 1
	-- and oh.created_at between '2017-11-13' and '2017-11-14'
order by order_id_bk desc
 
select top 1000 *
from Landing.mag.sales_flat_invoice_aud
where mutual_amount is not null

select top 1000 *
from Landing.mag.sales_flat_order_payment_aud
where parent_id = 5915817

---------------------------------------------------

select method, count(*)
from Landing.mag.sales_flat_order_payment_aud
group by method
order by method

select *
from Landing.mag.sales_flat_order_payment_aud
where method = 'insured'
order by entity_id
