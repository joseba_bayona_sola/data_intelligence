

SELECT COUNT(*), MIN(shipment_id), MAX(shipment_id) 
FROM dw_flat.vw_shipment_headers_shipment

SELECT * 
FROM dw_flat.vw_shipment_headers_shipment
where document_date  between curdate() - interval 1 day and curdate()
order by document_id desc

SELECT * 
FROM dw_flat.vw_shipment_headers_shipment
where shipment_id < 0
order by document_date desc

SELECT * 
FROM dw_flat.dw_shipment_headers
where shipment_id < 0

SELECT count(*) 
FROM dw_flat.vw_shipment_headers_shipment
where shipment_id < 0
limit 1000

-- 
SELECT * 
FROM dw_flat.vw_shipment_headers_creditmemo
where shipment_id < 0
order by document_date desc


-- ------------------------------------------------------------

SELECT * 
FROM dw_flat.vw_shipment_lines_shipment
where shipment_id < 0
order by document_date desc


SELECT * 
FROM dw_flat.vw_shipment_lines_creditmemo
where shipment_id < 0
order by document_date desc
