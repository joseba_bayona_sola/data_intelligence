
select entity_id, increment_id, state, status, created_at, updated_at, store_id, 
  customer_firstname, customer_lastname, base_grand_total
from magento01.sales_flat_order
where increment_id in ('8002521454', '8002521452', '8002521156')
order by created_at desc;

select o.increment_id order_no, st.name website, o.created_at purchased_on, 
  o.customer_firstname, o.customer_lastname, o.base_grand_total gt_base, o.status
from 
    magento01.sales_flat_order o
  inner join 
    magento01.core_store st ON o.store_id = st.store_id
where o.status = 'warehouse'
-- where state = 'processing'
order by o.created_at desc
limit 1000


