
select ol.order_id_bk, ol.shipment_id,
	ol.order_no, ol.shipment_no, 
	ol.customer_id, 
	ol.order_date, ol.invoice_date, ol.shipment_date, 
	sdc.date_ship, sdc.date_closed
into #shipment_check_york
from
		(select shipment_no, customer_id, name, 
			consignment, status, 
			date_ship, date_closed
		from DW_GetLenses_jbs.dbo.shipment_days_check_york_v) sdc
	left join
		(select distinct order_id_bk, shipment_id, order_no, shipment_no, customer_id, order_date, invoice_date, shipment_date
		from Warehouse.sales.fact_order_line_v) ol on sdc.shipment_no = ol.shipment_no
where ol.shipment_no is not null 
order by ol.order_id_bk, sdc.shipment_no 

select count(*)
from #shipment_check_york

select order_id_bk order_id, 
	order_no, shipment_no, 
	customer_id, 
	order_date, invoice_date, 
	date_ship, date_closed, 
	shipment_date, 
	datediff(dd, date_closed, shipment_date) diff_days
from #shipment_check_york
where convert(date, shipment_date) <> convert(date, date_closed)
order by date_closed

select day(date_closed), diff_days, count(*)
from
	(select *, datediff(dd, date_closed, shipment_date) diff_days
	from #shipment_check_york) t
group by day(date_closed), diff_days
order by day(date_closed), diff_days

--------------------------------------------------------------------------------------------------------


select ol.order_id_bk, ol.shipment_id,
	ol.order_no, ol.shipment_no, 
	ol.customer_id, 
	ol.order_date, ol.invoice_date, ol.shipment_date, 
	sdc.date_ship, sdc.date_closed
into #shipment_check_ams
from
		(select shipment_no, customer_id, name, 
			consignment, status, 
			date_ship, date_closed
		from DW_GetLenses_jbs.dbo.shipment_days_check_ams_v) sdc
	left join
		(select distinct order_id_bk, shipment_id, order_no, shipment_no, customer_id, order_date, invoice_date, shipment_date
		from Warehouse.sales.fact_order_line_v) ol on sdc.shipment_no = ol.shipment_no
where ol.shipment_no is not null 
order by ol.order_id_bk, sdc.shipment_no 

select count(*)
from #shipment_check_ams

select order_id_bk order_id, 
	order_no, shipment_no, 
	customer_id, 
	order_date, invoice_date, 
	date_ship, date_closed, 
	shipment_date, 
	datediff(dd, date_closed, shipment_date) diff_days
from #shipment_check_ams
where convert(date, shipment_date) <> convert(date, date_closed)
order by date_closed

select day(date_closed), diff_days, count(*)
from
	(select *, datediff(dd, date_closed, shipment_date) diff_days
	from #shipment_check_ams) t
group by day(date_closed), diff_days
order by day(date_closed), diff_days
