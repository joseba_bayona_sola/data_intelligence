

select ol.order_id_bk, ol.shipment_id,
	ol.order_no, ol.shipment_no, -- sdc.shipment_no,
	ol.customer_id, -- sdc.customer_id,
	ol.order_date, ol.invoice_date, ol.shipment_date, 
	sdc.date_ship, sdc.date_closed
into #shipment_check
from
		(select shipment_no, customer_id, name, 
			consignment, status, 
			convert(datetime, date_ship + ':00', 103) date_ship,
			convert(datetime, date_closed + ':00', 103) date_closed
		from DW_GetLenses_jbs.dbo.shipment_days_check) sdc
	left join
		(select distinct order_id_bk, shipment_id, order_no, shipment_no, customer_id, order_date, invoice_date, shipment_date
		from Warehouse.sales.fact_order_line_v) ol on sdc.shipment_no = ol.shipment_no
where ol.shipment_no is not null
-- where ol.shipment_no is null -- 14 Orders where shipment is NOT in Magento / DWH (Refunded Orders_
order by ol.order_id_bk, sdc.shipment_no 


select *, datediff(dd, date_ship, shipment_date)
from #shipment_check
where convert(date, shipment_date) <> convert(date, date_ship)
order by date_ship

select day(date_ship), diff_days, count(*)
from
	(select *, datediff(dd, date_ship, shipment_date) diff_days
	from #shipment_check) t
group by day(date_ship), diff_days
order by day(date_ship), diff_days

