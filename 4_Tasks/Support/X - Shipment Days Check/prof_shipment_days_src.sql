select shipment_no, customer_id, name, 
	consignment, status, 
	convert(datetime, date_ship + ':00', 103) date_ship,
	convert(datetime, date_closed + ':00', 103) date_closed
from DW_GetLenses_jbs.dbo.shipment_days_check

select count(*)
from DW_GetLenses_jbs.dbo.shipment_days_check

select shipment_no, customer_id, name, 
	consignment, status, 
	convert(datetime, date_ship + ':00', 103) date_ship,
	convert(datetime, date_closed + ':00', 103) date_closed
from DW_GetLenses_jbs.dbo.shipment_days_check
where convert(date, date_ship + ':00', 103) <> convert(date, date_closed + ':00', 103)

-------------------------------------------------------------

select shipment_no, customer_id, name, 
	consignment, status, date_ship, date_closed,
	convert(datetime, date_ship + ':00', 103) date_ship,
	convert(datetime, date_closed + ':00', 103) date_closed	
-- from DW_GetLenses_jbs.dbo.shipment_days_check_york
from DW_GetLenses_jbs.dbo.shipment_days_check_ams
where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1

-- 14947
-- 2312
select count(*)
from DW_GetLenses_jbs.dbo.shipment_days_check_york
-- from DW_GetLenses_jbs.dbo.shipment_days_check_ams
where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1

select *
from #shipment_check_ams
where convert(date, date_ship) <> convert(date, date_closed)

-------------------------------------------------------------

select shipment_no, customer_id, name, 
	consignment, status, date_ship, date_closed,
	convert(datetime, date_ship + ':00', 103) date_ship,
	convert(datetime, date_closed + ':00', 103) date_closed	
-- from DW_GetLenses_jbs.dbo.shipment_days_check_york_s
from DW_GetLenses_jbs.dbo.shipment_days_check_ams_s
where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1

-- 1379
-- 156
select count(*)
-- from DW_GetLenses_jbs.dbo.shipment_days_check_york_s
from DW_GetLenses_jbs.dbo.shipment_days_check_ams_s
where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1

select *
from #shipment_check_ams
where convert(date, date_ship) <> convert(date, date_closed)

-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------

create view dbo.shipment_days_check_york_v as
	select shipment_no, customer_id, name, 
		consignment, status, 
		convert(datetime, date_ship + ':00', 103) date_ship,
		convert(datetime, date_closed + ':00', 103) date_closed
	from DW_GetLenses_jbs.dbo.shipment_days_check
	union
	select shipment_no, customer_id, name, 
		consignment, status, 
		convert(datetime, date_ship + ':00', 103) date_ship,
		convert(datetime, date_closed + ':00', 103) date_closed	
	from DW_GetLenses_jbs.dbo.shipment_days_check_york
	where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1
	union
	select shipment_no, customer_id, name, 
		consignment, status, 
		convert(datetime, date_ship + ':00', 103) date_ship,
		convert(datetime, date_closed + ':00', 103) date_closed	
	from DW_GetLenses_jbs.dbo.shipment_days_check_york_s
go

create view dbo.shipment_days_check_ams_v as
	select shipment_no, customer_id, name, 
		consignment, status, 
		convert(datetime, date_ship + ':00', 103) date_ship,
		convert(datetime, date_closed + ':00', 103) date_closed	
	from DW_GetLenses_jbs.dbo.shipment_days_check_ams
	where isdate(date_ship + ':00') = 1 and isdate(date_closed + ':00') = 1
	union
	select shipment_no, customer_id, name, 
		consignment, status, 
		convert(datetime, date_ship + ':00', 103) date_ship,
		convert(datetime, date_closed + ':00', 103) date_closed	
	from DW_GetLenses_jbs.dbo.shipment_days_check_ams_s
go
