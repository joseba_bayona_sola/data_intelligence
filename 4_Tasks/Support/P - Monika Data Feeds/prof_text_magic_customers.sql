
select *
from
	(select receive_time, from_number, '00' + from_number from_number_int, to_number, message, status, first_name, last_name
	from DW_GetLenses_jbs.dbo.ex_text_magic_customers_in) t
where from_number_int = '00447903406685'

select order_id, created_at, increment_id, store_id, reminder_mobile, international_format, 
	min(order_id) over (partition by international_format) order_id_min, 
	dense_rank() over (partition by international_format order by order_id) rank_r 
from DW_GetLenses_jbs.dbo.ex_text_magic_customers_all
where international_format = '00447903406685'
order by created_at
	
	select international_format, count(*)
	from DW_GetLenses_jbs.dbo.ex_text_magic_customers_all
	group by international_format
	order by count(*) desc


select t1.from_number_int, t1.from_number, t1.receive_time
from
		(select receive_time, from_number, '00' + from_number from_number_int, to_number, message, status, first_name, last_name
		from DW_GetLenses_jbs.dbo.ex_text_magic_customers_in) t1
	inner join
		(select order_id, international_format
		from
			(select order_id, international_format, 
				dense_rank() over (partition by international_format order by order_id) rank_r 
			from DW_GetLenses_jbs.dbo.ex_text_magic_customers_all) t
		where rank_r = 1) t2 on t1.from_number_int = t2.international_format
where t1.from_number_int = '00447903406685'



select t2.order_id, t2.international_format, t1.from_number, t1.from_number_int, t1.message, t1.receive_time
from
		(select receive_time, from_number, '00' + from_number from_number_int, to_number, message, status, first_name, last_name
		from DW_GetLenses_jbs.dbo.ex_text_magic_customers_in) t1
	left join
		(select order_id, international_format
		from
			(select order_id, international_format, 
				dense_rank() over (partition by international_format order by order_id) rank_r 
			from DW_GetLenses_jbs.dbo.ex_text_magic_customers_all) t
		where rank_r = 1) t2 on t1.from_number_int = t2.international_format
where t2.order_id is null
	and t1.message not like 's%'


select 
	t1.from_number_int, t1.from_number, t1.receive_time, -- t1.message, 
	oh2.customer_id, 
	oh2.order_id_bk, oh2.order_no, oh2.order_date, oh2.shipment_date, oh2.order_type_name
from
		(select receive_time, from_number, '00' + from_number from_number_int, to_number, message, status, first_name, last_name
		from DW_GetLenses_jbs.dbo.ex_text_magic_customers_in) t1
	inner join
		(select order_id, international_format
		from
			(select order_id, international_format, 
				dense_rank() over (partition by international_format order by order_id) rank_r 
			from DW_GetLenses_jbs.dbo.ex_text_magic_customers_all) t
		where rank_r = 1) t2 on t1.from_number_int = t2.international_format
	inner join
		Warehouse.sales.dim_order_header_v oh on t2.order_id = oh.order_id_bk
	inner join
		Warehouse.sales.dim_order_header_v oh2 on oh.customer_id = oh2.customer_id
where oh2.order_date > '2017-05-01'
order by oh2.customer_id, oh2.order_date
