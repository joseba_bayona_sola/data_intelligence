
select *
from DW_GetLenses_jbs.dbo.ex_trustpilot_customers tpc

select *
from DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201707 tpc

	select customer_email, count(*)
	from DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201707
	group by customer_email
	order by count(*) desc

select customer_email, review_date, convert(date, review_date, 103) review_date
from DW_GetLenses_jbs.dbo.ex_trustpilot_customers_all tpc

	select customer_email, count(*)
	from DW_GetLenses_jbs.dbo.ex_trustpilot_customers_all
	group by customer_email
	order by count(*) desc

select c.idCustomer_sk, c.customer_id_bk, tpc.customer_email, convert(date, tpc.review_date, 103) review_date
from 
		DW_GetLenses_jbs.dbo.ex_trustpilot_customers_all tpc
	inner join
		Warehouse.gen.dim_customer c on tpc.customer_email = c.customer_email
where tpc.customer_email = 'lisa.lintott@hotmail.co.uk'
order by review_date




select -- c.idCustomer_sk, 
	c.customer_id_bk customer_id, tpc.customer_email, convert(date, tpc.review_date, 103) review_date,
	year(convert(date, tpc.review_date, 103)) yyyy, month(convert(date, tpc.review_date, 103)) mm, 
	cs.country_code_s
from 
		DW_GetLenses_jbs.dbo.ex_trustpilot_customers_all tpc
	inner join
		Warehouse.gen.dim_customer c on tpc.customer_email = c.customer_email
	inner join
		Warehouse.gen.dim_customer_scd_v cs on c.idCustomer_sk = cs.idCustomer_sk_fk 
where c.customer_id_bk = 1585197
order by country_code_s, review_date, customer_id










select c.idCustomer_sk, c.customer_id_bk, tpc.customer_email, convert(date, tpc.review_date, 103) review_date,
	oh.order_id_bk, oh.order_no, oh.order_date_c, oh.country_code_ship
from 
		DW_GetLenses_jbs.dbo.ex_trustpilot_customers_all tpc
	inner join
		Warehouse.gen.dim_customer c on tpc.customer_email = c.customer_email
	inner join
		Warehouse.sales.dim_order_header_v oh on c.customer_id_bk = oh.customer_id
order by customer_id_bk, order_date_c


----------------------------

select c.customer_id_bk, tpc.customer_email, tpc.review_id, 
	c.country_code_s
from 
		(select customer_email, min(review_id) review_id
		from 
			-- DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201707 tpc
			-- DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201707b tpc
			-- DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201710 tpc
			DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201711 tpc
		where customer_email <> ''
		group by customer_email) tpc
	inner join
		Warehouse.gen.dim_customer_scd_v c on tpc.customer_email = c.customer_email
order by c.country_code_s, c.customer_id_bk

select c.idCustomer_sk, c.customer_id_bk, tpc.customer_email, tpc.review_id
	,oh.order_id_bk, oh.order_no, oh.order_date_c, oh.country_code_ship
from 
		(select customer_email, min(review_id) review_id
		from DW_GetLenses_jbs.dbo.ex_trustpilot_customers_201707 tpc
		where customer_email <> ''
		group by customer_email) tpc
	inner join
		Warehouse.gen.dim_customer_v c on tpc.customer_email = c.customer_email
	inner join
		Warehouse.sales.dim_order_header_v oh on c.customer_id_bk = oh.customer_id
order by customer_id_bk, order_date_c
