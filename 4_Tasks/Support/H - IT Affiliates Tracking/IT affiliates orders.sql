
select top 1000 store_name, order_id, order_no, document_type, document_date, 
	customer_id, customer_email, 
	affilCode, coupon_code, customer_business_channel, order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where order_no in ('24000053322', '24000053273', '24000052825', '24000052843', '24000052809', '24000052390', '24000052298', '24000052279', 
	'24000052259', '24000052207', '24000051389', '24000051315', '24000051237', '24000051063', '24000051002', '24000050852', '24000049484')
order by order_id

select top 1000 store_name, order_id, order_no, document_type, document_date, 
	customer_id, customer_email, 
	affilCode, coupon_code, customer_business_channel, order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.invoice_headers
where order_no in ('24000053322', '24000053273', '24000052825', '24000052843', '24000052809', '24000052390', '24000052298', '24000052279', 
	'24000052259', '24000052207', '24000051389', '24000051315', '24000051237', '24000051063', '24000051002', '24000050852', '24000049484')
order by order_id, document_date


select top 1000 store_name, order_id, order_no, document_type, document_date, 
	customer_id, customer_email, 
	affilCode, coupon_code, customer_business_channel, order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.invoice_headers
where order_no in ('17000295431')
order by order_id, document_date

select top 1000 store_name, order_id, order_no, document_type, document_date, 
	customer_id, customer_email, 
	affilCode, coupon_code, customer_business_channel, order_lifecycle, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where order_no in ('17000295431')
order by order_id, document_date

