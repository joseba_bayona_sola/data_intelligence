
select *
from Warehouse.prod.dim_product_family_v
-- where product_type_name = 'Glasses' -- 290
where product_id_magento in (2326, 2331)
order by product_id_magento

select *
from Landing.aux.mag_prod_product_family_v
where product_id_bk in (2326, 2331)

select *
from Landing.aux.prod_product_category
where product_id in (2326, 2331)

	select *
	from #product_magento_category
	where product_id in (2326, 2331)

select *
from Landing.aux.prod_product_category
where product_id in (2326, 2331)

select *
from Landing.aux.prod_product_product_type_oh
where product_id in (2326, 2331)

select *
from Landing.aux.prod_product_product_type_vat
where product_id in (2326, 2331)

-------------------------------------------------

select top 1000 order_line_id_bk, order_id_bk, order_no, order_date, 
	website_group, website, customer_id, 
	product_type_oh_name, 
	product_type_name, category_name, product_id_magento, local_subtotal, 
	product_type_vat
from Warehouse.sales.fact_order_line_v
where product_id_magento in (2326, 2331)
order by order_date
