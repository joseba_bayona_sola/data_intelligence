
select *
from Warehouse.sales.fact_order_line_v
where year(order_date) = 2017
	and shipment_date is null
	and order_status_magento_name = 'shipped'
	and product_id_magento not in (2950)

	-- and product_id_magento = 1083
order by order_date 

select product_id_magento, product_family_name, count(*)
from Warehouse.sales.fact_order_line_v
where year(order_date) = 2017
	and shipment_date is null
	and order_status_magento_name = 'shipped'
	and product_id_magento not in (2950)

	and order_date_c not in ('2017-04-11', '2017-04-12', '2017-04-13', '2017-05-22', '2017-05-23')
group by product_id_magento, product_family_name
order by product_id_magento, product_family_name

select order_date_c, count(*)
from Warehouse.sales.fact_order_line_v
where year(order_date) = 2017
	and shipment_date is null
	and order_status_magento_name = 'shipped'
	and product_id_magento not in (2950)
group by order_date_c
order by order_date_c
