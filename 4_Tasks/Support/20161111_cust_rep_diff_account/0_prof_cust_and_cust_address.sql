
select top 100 customer_id, store_name, 
	prefix, firstname, lastname, cus_phone, email
from DW_GetLenses.dbo.customers
where store_name = 'visiondirect.es'

select top 100 address_id, address_no, 
	customer_id, firstname, lastname, 
	street, city, postcode
from DW_GetLenses.dbo.customer_addresses

--------------------------------------------------------------

------------------------------ MULTIPLE ADDRESSES PER CUSTOMER -------------------------
select top 1000 
	count(*) over () num_tot_cust_addr, c.num_tot_cust,
	count(ca.address_id) over (partition by c.customer_id) num_rep_addr,
	c.customer_id, c.store_name, email,
	c.prefix, c.firstname, c.lastname, c.cus_phone, 
	ca.address_id, ca.address_no, ca.street, ca.city, ca.postcode
from 
		(select count(*) over () num_tot_cust,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.es') c
	inner join
		(select address_id, address_no, 
			customer_id, firstname, lastname, 
			street, city, postcode
		from DW_GetLenses.dbo.customer_addresses) ca on c.customer_id = ca.customer_id
order by num_rep_addr desc
--order by num_rep_cust desc

	select num_tot_cust, num_rep_addr, count(*) tot, count(*)*100/convert(decimal(10,2), num_tot_cust) perc
	from
		(select distinct num_tot_cust_addr, num_tot_cust, num_rep_addr, customer_id, store_name, email
		from
			(select 
				count(*) over () num_tot_cust_addr, c.num_tot_cust,
				count(ca.address_id) over (partition by c.customer_id) num_rep_addr,
				c.customer_id, c.store_name, email,
				c.prefix, c.firstname, c.lastname, c.cus_phone, 
				ca.address_id, ca.address_no, ca.street, ca.city, ca.postcode
			from 
					(select count(*) over () num_tot_cust,
						customer_id, store_name, 
						prefix, firstname, lastname, cus_phone, email
					from DW_GetLenses.dbo.customers
					--where store_name = 'visiondirect.es'
					) c
				left join
					(select address_id, address_no, 
						customer_id, firstname, lastname, 
						street, city, postcode
					from DW_GetLenses.dbo.customer_addresses) ca on c.customer_id = ca.customer_id) t) t
	group by num_tot_cust, num_rep_addr
	order by num_tot_cust, num_rep_addr

	select top 1000 count(*) over () num_tot,
		customer_id, store_name, email,
		prefix, firstname, lastname, cus_phone, 
		address_id, address_no, street, city, postcode
	from 
		(select 
			count(*) over () num_tot_cust_addr, c.num_tot_cust,
			count(ca.address_id) over (partition by c.customer_id) num_rep_addr,
			c.customer_id, c.store_name, email,
			c.prefix, c.firstname, c.lastname, c.cus_phone, 
			ca.address_id, ca.address_no, ca.street, ca.city, ca.postcode
		from 
				(select count(*) over () num_tot_cust,
					customer_id, store_name, 
					prefix, firstname, lastname, cus_phone, email
				from DW_GetLenses.dbo.customers
				where store_name = 'visiondirect.es') c
			inner join
				(select address_id, address_no, 
					customer_id, firstname, lastname, 
					street, city, postcode
				from DW_GetLenses.dbo.customer_addresses) ca on c.customer_id = ca.customer_id) t
	--where num_rep_addr > 4
	--where num_rep_addr in (3, 4)
	--where num_rep_addr = 2
	where num_rep_addr = 2
	order by num_rep_addr, customer_id

--------------------------------------------------------------

------------------------------ MULTIPLE ACCOUNTS PER CUSTOMER -------------------------

select top 100 
	count(*) over () num_tot_cust,
	count(*) over (partition by firstname, lastname) num_tot_cust_rep,
	count(*) over (partition by email) num_tot_cust_email_rep,
	customer_id, store_name, 
	prefix, firstname, lastname, cus_phone, email
from DW_GetLenses.dbo.customers
where store_name = 'visiondirect.es'
order by num_tot_cust_email_rep desc, num_tot_cust_rep desc, email

	-- Repeated Customer by Email
	select num_tot_cust, num_tot_cust_email_rep, count(*) tot, count(*)*100/convert(decimal(10,2), num_tot_cust) perc
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		--where store_name = 'visiondirect.es'
		) t
	group by num_tot_cust, num_tot_cust_email_rep
	order by num_tot_cust, num_tot_cust_email_rep

	select TOP 1000 
		num_tot_cust_email_rep,
		customer_id, store_name, 
		prefix, firstname, lastname, cus_phone, email
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		--where store_name = 'visiondirect.es'
		) t
	where num_tot_cust_email_rep > 1 
	order by num_tot_cust_email_rep, email, customer_id

		select TOP 1000 store_name, count(*)
		from 
			(select 
				count(*) over () num_tot_cust,
				count(*) over (partition by firstname, lastname) num_tot_cust_rep,
				count(*) over (partition by email) num_tot_cust_email_rep,
				count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
				customer_id, store_name, 
				prefix, firstname, lastname, cus_phone, email
			from DW_GetLenses.dbo.customers
			--where store_name = 'visiondirect.es'
			) t
		where num_tot_cust_email_rep > 1
		group by store_name
		order by store_name

	-- Repeated Customer by Name 
	select num_tot_cust, num_tot_cust_rep, --num_tot_cust_ph_rep,
		count(*) tot, count(*)*100/convert(decimal(10,2), num_tot_cust) perc
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		--where store_name = 'visiondirect.es'
		) t
	group by num_tot_cust, num_tot_cust_rep--, num_tot_cust_ph_rep
	order by num_tot_cust, num_tot_cust_rep--, num_tot_cust_ph_rep

	select 
		num_tot_cust_rep, num_tot_cust_ph_rep,
		customer_id, store_name, 
		prefix, firstname, lastname, cus_phone, email
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.es') t
	--where num_tot_cust_rep > 3
	where num_tot_cust_rep = 3 --and num_tot_cust_ph_rep = 1
	--where num_tot_cust_rep = 2
	--where num_tot_cust_rep = 1
	order by num_tot_cust_rep, firstname, lastname, customer_id

	-- Repeated Customer by Name + Phone
	select num_tot_cust, num_tot_cust_ph_rep, count(*) tot, count(*)*100/convert(decimal(10,2), num_tot_cust) perc
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		--where store_name = 'visiondirect.es'
		) t
	group by num_tot_cust, num_tot_cust_ph_rep
	order by num_tot_cust, num_tot_cust_ph_rep

	select 
		num_tot_cust_rep,
		customer_id, store_name, 
		prefix, firstname, lastname, cus_phone, email
	from 
		(select 
			count(*) over () num_tot_cust,
			count(*) over (partition by firstname, lastname) num_tot_cust_rep,
			count(*) over (partition by email) num_tot_cust_email_rep,
			count(*) over (partition by firstname, lastname, cus_phone) num_tot_cust_ph_rep,
			customer_id, store_name, 
			prefix, firstname, lastname, cus_phone, email
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.es') t
	--where num_tot_cust_ph_rep > 2
	where num_tot_cust_ph_rep = 2
	--where num_tot_cust_ph_rep = 1
	order by num_tot_cust_rep, firstname, lastname, customer_id

