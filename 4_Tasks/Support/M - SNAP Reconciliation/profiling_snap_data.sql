
select top 1000 shipment_no, order_no, 
	status, stage, warehouse, 
	date_created, date_ship, date_closed, 
	count(*) over (partition by order_no) num_rep
from DW_GetLenses_jbs.dbo.snap_data_york
order by num_rep desc

select top 1000 shipment_no, order_no, 
	status, stage, warehouse, 
	date_created, date_ship, date_closed
from DW_GetLenses_jbs.dbo.snap_data_amsterdam

------------------------------------------

select count(*), count(distinct order_no), count(distinct shipment_no)
from DW_GetLenses_jbs.dbo.snap_data_york

select count(*), count(distinct order_no), count(distinct shipment_no)
from DW_GetLenses_jbs.dbo.snap_data_amsterdam

------------------------------------------

select status, count(*)
from DW_GetLenses_jbs.dbo.snap_data_york
group by status
order by status

select status, count(*)
from DW_GetLenses_jbs.dbo.snap_data_york
group by status
order by status

select stage, count(*)
from DW_GetLenses_jbs.dbo.snap_data_york
group by stage
order by stage