
select mms.shipment_number, left(mms.shipment_number, charindex('-', mms.shipment_number)-1) order_number,
	s.entity_id, s.created_at
from 
		DW_GetLenses_jbs.dbo.mendix_magento_shipment mms
	left join
		Landing.mag.sales_flat_shipment_aud s on mms.shipment_number = s.increment_id
where s.entity_id is not null
order by s.created_at

select mms.shipment_number, left(mms.shipment_number, charindex('-', mms.shipment_number)-1) order_number,
	-- s.entity_id, s.created_at,
	o.entity_id, o.status, o.created_at--, o.store_id
from 
		DW_GetLenses_jbs.dbo.mendix_magento_shipment mms
	left join
		Landing.mag.sales_flat_shipment_aud s on mms.shipment_number = s.increment_id
	left join
		Landing.mag.sales_flat_order_aud o on left(mms.shipment_number, charindex('-', mms.shipment_number)-1) = o.increment_id
where s.entity_id is null
order by o.created_at
-- where o.entity_id is null
		
select top 1000 *
from Landing.mag.sales_flat_shipment_aud
where order_id in (3823271, 3823463, 3823808, 3825206, 3825248)


select top 1000 *
from Landing.mag.sales_flat_shipment

select *
from Landing.mag.sales_flat_order_aud_v
where entity_id = 5115981