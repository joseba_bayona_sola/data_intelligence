

select *, row_number() over ()
from DW_GetLenses_jbs.dbo.erp_orders_magento_lookup

select entity_id, increment_id, created_at, updated_at, status
into DW_GetLenses_jbs.dbo.magento_orders
from openquery(MAGENTO, 'select entity_id, increment_id, created_at, updated_at, status from magento01.sales_flat_order')

select t.*, o.status, o.updated_at
from 
		DW_GetLenses_jbs.dbo.erp_orders_magento_lookup t
	left join
		DW_GetLenses_jbs.dbo.magento_orders o on t.order_no = o.increment_id
where o.status is null
order by convert(datetime, t.[created at], 103)
