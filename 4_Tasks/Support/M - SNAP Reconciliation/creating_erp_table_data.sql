
drop table DW_GetLenses_jbs.dbo.erp_shipments

select id,  
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, 
		processingFailed, warehouseMethod, labelRenderer, 
		notify, fullyShipped, syncedToMagento, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate
into DW_GetLenses_jbs.dbo.erp_shipments
from openquery(MENDIX_DB_LOCAL, 
	'select id,  
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, 
		processingFailed, warehouseMethod, labelRenderer, 
		notify, fullyShipped, syncedToMagento, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate
	from public."customershipments$customershipment"')


---------------------------------------------------------------

drop table DW_GetLenses_jbs.dbo.erp_shipments_june

select id,  
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, 
		processingFailed, warehouseMethod, labelRenderer, 
		notify, fullyShipped, syncedToMagento, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate
into DW_GetLenses_jbs.dbo.erp_shipments_june
from openquery(MENDIX_DB_LOCAL, 
	'select id,  
		shipmentID, partitionID0_120,
		shipmentNumber, orderIncrementID, 
		status, 
		processingFailed, warehouseMethod, labelRenderer, 
		notify, fullyShipped, syncedToMagento, 
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		createdDate
	from public."customershipments$customershipment"')


select top 1000 *
from DW_GetLenses_jbs.dbo.erp_shipments_june
order by expectedShippingDate desc
