

select 'YORK' wh, shipment_no, order_no, 
	status, stage, warehouse, 
	date_created, date_ship, date_closed
from DW_GetLenses_jbs.dbo.snap_data_york
union
select 'AMS' wh, shipment_no, order_no, 
	status, stage, warehouse, 
	date_created, date_ship, date_closed
from DW_GetLenses_jbs.dbo.snap_data_amsterdam


select -- top 10000 count(*) over () num_tot,
	t2.orderIncrementID, t2.shipmentNumber,
	t1.status, t1.date_created, t1.date_ship, t1.date_closed, 
	t2.status, t2.partitionID0_120, t2.notify, t2.fullyShipped, t2.syncedToMagento, 
	t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate, t2.createdDate
from 
		DW_GetLenses_jbs.dbo.snap_data_york t1
	left join
		DW_GetLenses_jbs.dbo.erp_shipments t2 on t1.order_no = t2.orderIncrementID and t1.shipment_no = t2.shipmentNumber
where t2.orderIncrementID is not null and t2.status <> 'Complete'
-- where t2.orderIncrementID is null
order by t1.date_created

select -- top 10000 count(*) over () num_tot,
	t2.orderIncrementID, t2.shipmentNumber,
	t1.status, t1.date_created, t1.date_ship, t1.date_closed, 
	t2.status, t2.partitionID0_120, t2.notify, t2.fullyShipped, t2.syncedToMagento, 
	t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate, t2.createdDate
from 
		DW_GetLenses_jbs.dbo.snap_data_amsterdam t1
	left join
		DW_GetLenses_jbs.dbo.erp_shipments t2 on t1.order_no = t2.orderIncrementID and t1.shipment_no = t2.shipmentNumber
where t2.orderIncrementID is not null and t2.status <> 'Complete'
-- where t2.orderIncrementID is null
order by t1.date_created

--------------------------------------------------------------------

select t.id, t.date_created, t.shipmentNumber, t.orderIncrementID, t.status, t.warehouseMethod, t.syncedToMagento,
	o.entity_id, o.increment_id, o.created_at, o.status, o.state, o.updated_at, o.warehouse_approved_time
from
		(select count(*) over () num_tot,
			t1.date_created, t1.date_ship, t1.date_closed, 
			t2.id, t2.shipmentNumber, t2.orderIncrementID, t2.status, t2.warehouseMethod, t2.syncedToMagento, 
			t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate
		from 
				--DW_GetLenses_jbs.dbo.snap_data_york t1
				DW_GetLenses_jbs.dbo.snap_data_amsterdam t1
			left join
				DW_GetLenses_jbs.dbo.erp_shipments t2 on t1.order_no = t2.orderIncrementID and t1.shipment_no = t2.shipmentNumber
		where t2.orderIncrementID is not null and t2.status <> 'Complete') t
	inner join
		Landing.mag.sales_flat_order_aud o on t.orderIncrementID = o.increment_id
--where o.status <> 'shipped'
--where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
order by t.date_created

--------------------------------------------------------------------

select t.id, t.date_created, t.shipmentNumber, t.orderIncrementID, t.status, t.warehouseMethod, t.syncedToMagento,
	o.entity_id, o.increment_id, o.created_at, o.updated_at, o.status, o.state, o.warehouse_approved_time, 
	s.entity_id, s.increment_id, s.created_at shipment_date, 
	count(s.entity_id) over ()
from
		(select count(*) over () num_tot,
			t1.date_created, t1.date_ship, t1.date_closed, 
			t2.id, t2.shipmentNumber, t2.orderIncrementID, t2.status, t2.warehouseMethod, t2.syncedToMagento, 
			t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate
		from 
				DW_GetLenses_jbs.dbo.snap_data_york t1
				--DW_GetLenses_jbs.dbo.snap_data_amsterdam t1
			left join
				DW_GetLenses_jbs.dbo.erp_shipments t2 on t1.order_no = t2.orderIncrementID and t1.shipment_no = t2.shipmentNumber
		where t2.orderIncrementID is not null and t2.status <> 'Complete') t
	inner join
		Landing.mag.sales_flat_order_aud o on t.orderIncrementID = o.increment_id
	left join
		Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id 
--where o.status <> 'shipped'
--where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
-- where t.shipmentNumber = '5001375074-1'
where s.entity_id is not null
order by t.date_created

select entity_id, increment_id, created_at, status, state, warehouse_approved_time
from Landing.mag.sales_flat_order_aud
where increment_id = '8002660719'

select top 1000 *
from Landing.mag.sales_flat_shipment_aud

