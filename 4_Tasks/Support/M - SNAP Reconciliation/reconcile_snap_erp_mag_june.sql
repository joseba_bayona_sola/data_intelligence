
--------------------------------------------------------------------

select top 1000 t.id, t.shipmentNumber, t.orderIncrementID, t.status, t.warehouseMethod, t.syncedToMagento,
	o.entity_id, o.increment_id, o.created_at, o.status, o.state, o.updated_at, o.warehouse_approved_time
from
		(select count(*) over () num_tot,
			t2.id, t2.shipmentNumber, t2.orderIncrementID, t2.status, t2.warehouseMethod, t2.syncedToMagento, 
			t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate
		from DW_GetLenses_jbs.dbo.erp_shipments_june t2 
		where t2.status = 'Complete') t
	inner join
		Landing.mag.sales_flat_order_aud o on t.orderIncrementID = o.increment_id
where o.status not in ('shipped', 'closed')
-- where o.status in ('warehouse')
order by o.created_at desc


select t.id, t.shipmentNumber, t.orderIncrementID, t.status, t.warehouseMethod, t.syncedToMagento,
	o.entity_id, o.increment_id, o.created_at, o.status, o.state, o.updated_at, o.warehouse_approved_time
from
		(select count(*) over () num_tot,
			t2.id, t2.shipmentNumber, t2.orderIncrementID, t2.status, t2.warehouseMethod, t2.syncedToMagento, 
			t2.dispatchDate, t2.expectedShippingDate, t2.expectedDeliveryDate
		from DW_GetLenses_jbs.dbo.erp_shipments_june t2 
		where t2.status = 'Complete') t
	inner join
		Landing.mag.sales_flat_order_aud o on t.orderIncrementID = o.increment_id
	left join
		Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id 
where o.status = 'shipped'
	and s.order_id is null
order by o.created_at 

--------------------------------------------------------------------

select entity_id, increment_id, created_at, status, state, warehouse_approved_time
from Landing.mag.sales_flat_order_aud
where increment_id = '8002660719'

select top 1000 *
from Landing.mag.sales_flat_shipment_aud

