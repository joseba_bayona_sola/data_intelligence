
select top 1000 *
from DW_GetLenses.dbo.order_headers
order by document_date desc

select top 1000 order_id, order_no, document_date, price_type, product_id, name, qty,
	local_line_subtotal_inc_vat, local_discount_inc_vat, discount_percent
from DW_GetLenses.dbo.order_lines
where price_type = 'PLA & Discounted'
order by document_date desc

select order_id, order_no, document_date, store_name, 
	customer_id, order_lifecycle, customer_order_seq_no, coupon_code, telesales_admin_username,
	local_subtotal_inc_vat, local_discount_inc_vat, discount_percent
from DW_GetLenses.dbo.order_headers
where order_id = 5218595

--------------------------------------------

select distinct 
	oh.order_id, oh.order_no, oh.document_date, oh.document_type, oh.store_name, 
	oh.customer_id, oh.order_lifecycle, oh.customer_order_seq_no, oh.coupon_code, telesales_admin_username,
	oh.local_subtotal_inc_vat, oh.local_discount_inc_vat, oh.discount_percent 
from 
		(select top 1000 order_id, order_no, document_date, document_type, price_type, product_id, name, qty,
			local_line_subtotal_inc_vat, local_discount_inc_vat, discount_percent
		from DW_GetLenses.dbo.order_lines
		where price_type = 'PLA & Discounted'
			and document_date > getutcdate() - 180
			and document_type = 'ORDER') ol
	inner join
		(select order_id, order_no, document_date, document_type, store_name, 
			customer_id, order_lifecycle, customer_order_seq_no, coupon_code, telesales_admin_username,
			local_subtotal_inc_vat, local_discount_inc_vat, discount_percent
		from DW_GetLenses.dbo.order_headers) oh on ol.order_id = oh.order_id and ol.document_type = oh.document_type
order by oh.order_id

select 
	oh.order_id, oh.order_no, oh.document_date, oh.document_type, oh.store_name, 
	oh.customer_id, oh.order_lifecycle, oh.customer_order_seq_no, oh.coupon_code, 
	oh.local_subtotal_inc_vat, oh.local_discount_inc_vat, oh.discount_percent, 
	ol.price_type, ol.product_id, ol.name, ol.qty,
	ol.local_line_subtotal_inc_vat, ol.local_discount_inc_vat, ol.discount_percent
from 
		(select top 1000 order_id, order_no, document_date, document_type, price_type, product_id, name, qty,
			local_line_subtotal_inc_vat, local_discount_inc_vat, discount_percent
		from DW_GetLenses.dbo.order_lines
		where price_type = 'PLA & Discounted'
			and document_date > getutcdate() - 180
			and document_type = 'ORDER') ol
	inner join
		(select order_id, order_no, document_date, document_type, store_name, 
			customer_id, order_lifecycle, customer_order_seq_no, coupon_code, 
			local_subtotal_inc_vat, local_discount_inc_vat, discount_percent
		from DW_GetLenses.dbo.order_headers) oh on ol.order_id = oh.order_id and ol.document_type = oh.document_type
order by oh.order_id, ol.product_id

select coupon_code, count(*)
from
	(select distinct 
		oh.order_id, oh.order_no, oh.document_date, oh.document_type, oh.store_name, 
		oh.customer_id, oh.order_lifecycle, oh.customer_order_seq_no, oh.coupon_code, 
		oh.local_subtotal_inc_vat, oh.local_discount_inc_vat, oh.discount_percent 
	from 
			(select top 1000 order_id, order_no, document_date, document_type, price_type, product_id, name, qty,
				local_line_subtotal_inc_vat, local_discount_inc_vat, discount_percent
			from DW_GetLenses.dbo.order_lines
			where price_type = 'PLA & Discounted'
				and document_date > getutcdate() - 180
				and document_type = 'ORDER') ol
		inner join
			(select order_id, order_no, document_date, document_type, store_name, 
				customer_id, order_lifecycle, customer_order_seq_no, coupon_code, 
				local_subtotal_inc_vat, local_discount_inc_vat, discount_percent
			from DW_GetLenses.dbo.order_headers) oh on ol.order_id = oh.order_id and ol.document_type = oh.document_type) t
group by coupon_code
order by coupon_code