
select -- store_name, order_id, order_no, order_date, 
	oh.customer_id, oh.customer_firstname, oh.customer_lastname, oh.customer_email, c.created_at registration_date,
	-- pc.postcode_sector,
	oh.order_no order_ref_no, oh.order_date transaction_date, 
	oh.billing_street1 + ' ' + oh.billing_street2 billing_street, oh.billing_city, oh.billing_postcode, oh.billing_country_id billing_country, 
	oh.shiping_street1 + ' ' + oh.shipping_street2 billing_street, oh.shipping_city, oh.shipping_postcode, oh.shipping_country_id shipping_country, 
	oh.payment_method, oh.coupon_code, 
	oh.order_lifecycle, oh.customer_order_seq_no,
	oh.local_subtotal_inc_vat, oh.local_total_inc_vat, oh.local_total_exc_vat
from 
		DW_GetLenses.dbo.order_headers oh
	left join
		DW_GetLenses_jbs.dbo.qm_postcodes_campaign pc on oh.billing_postcode like pc.postcode_sector + '%'
	inner join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where 
	-- coupon_code = 'DIRECT10'
	-- (coupon_code is not null and coupon_code <> 'DIRECT10') 
	-- coupon_code is null
	-- and 
	document_type = 'ORDER'
	and order_date between '2017-04-01' and '2017-07-07'
	and billing_country_id = 'GB'
order by order_id desc

select line_id, order_id, document_type, product_type, name, sku, qty, 
	local_line_subtotal_inc_vat, local_line_subtotal_exc_vat
from DW_GetLenses.dbo.order_lines
where order_id = 5305846

------------------------------------------

select top 1000 -- store_name, order_id, order_no, order_date, 
	oh.customer_id, oh.customer_firstname + ' ' + oh.customer_lastname full_name,
	oh.billing_street1 + ' ' + oh.billing_street2 billing_street, oh.billing_city, oh.billing_postcode, oh.billing_country_id billing_country, 
	oh.shiping_street1 + ' ' + oh.shipping_street2 billing_street, oh.shipping_city, oh.shipping_postcode, oh.shipping_country_id shipping_country, 
	oh.payment_method, 
	-- order_lifecycle, customer_order_seq_no,
	oh.local_subtotal_inc_vat local_subtotal_inc_vat_oh, oh.local_total_inc_vat local_total_inc_vat_oh, oh.local_total_exc_vat local_total_exc_vat_oh,
	ol.name product_name, ol.sku product_sku, ol.qty qty_product, ol.local_line_subtotal_inc_vat local_line_subtotal_inc_vat_product
from 
		DW_GetLenses.dbo.order_headers oh
	inner join
		DW_GetLenses.dbo.order_lines ol on oh.order_id = ol.order_id and ol.document_type = 'ORDER'
where oh.coupon_code = 'DIRECT10'
	and oh.document_type = 'ORDER'
	and oh.order_date > '2017-01-01'
order by oh.order_id desc


------------------------------------------

select oh.customer_id, oh.customer_firstname, oh.customer_lastname, oh.customer_email, oh.registration_date,
	oh.order_ref_no, oh.transaction_date, 
	oh.billing_street, oh.billing_city, oh.billing_postcode, oh.billing_country, 
	oh.shipping_street, oh.shipping_city, oh.shipping_postcode, oh.shipping_country, 
	oh.payment_method, oh.coupon_code, 
	oh.local_subtotal_inc_vat, oh.local_total_inc_vat, oh.local_total_exc_vat, 
	ol.product_type, ol.name product_name, ol.sku product_sku, ol.qty qty_product, ol.local_line_subtotal_inc_vat local_line_subtotal_inc_vat_product
from
		(select oh.order_id,
			oh.customer_id, oh.customer_firstname, oh.customer_lastname, oh.customer_email, convert(date, c.created_at) registration_date,
			oh.order_no order_ref_no, convert(date, oh.order_date) transaction_date, 
			oh.billing_street1 + ' ' + oh.billing_street2 billing_street, oh.billing_city, oh.billing_postcode, oh.billing_country_id billing_country, 
			oh.shiping_street1 + ' ' + oh.shipping_street2 shipping_street, oh.shipping_city, oh.shipping_postcode, oh.shipping_country_id shipping_country, 
			oh.payment_method, oh.coupon_code, 
			-- order_lifecycle, customer_order_seq_no,
			oh.local_subtotal_inc_vat, oh.local_total_inc_vat, oh.local_total_exc_vat
		from 
				DW_GetLenses.dbo.order_headers oh
			left join
				DW_GetLenses_jbs.dbo.qm_postcodes_campaign pc on oh.billing_postcode like pc.postcode_sector + '%'
			inner join
				DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
		where 
			-- coupon_code = 'DIRECT10' 
			-- (coupon_code is not null and coupon_code <> 'DIRECT10') 
			-- coupon_code is null
			document_type = 'ORDER'
			and order_date between '2016-04-01' and '2016-07-07'
			and billing_country_id = 'GB') oh
	inner join
		DW_GetLenses.dbo.order_lines ol on oh.order_id = ol.order_id and ol.document_type = 'ORDER'
order by transaction_date desc
