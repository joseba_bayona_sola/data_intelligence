
select top 1000 *
from Landing.mag.customer_entity_flat_aud
where old_access_cust_no like 'LENSONCOUK%'
	and created_at > '2017-04-01'
order by created_at desc

select *
from Warehouse.gen.dim_customer_scd
where customer_id_bk = 1908839

----------------------------------------------------------------------
-- ALL REGISTERED CUSTOMERS IN UK
select customer_id_bk customer_id, convert(date, c.created_at) registered_date, 
	-- customer_email, first_name, last_name, 
	street, city, postcode --, fl.old_access_cust_no
from 
		Warehouse.gen.dim_customer_scd c
	left join
		DW_GetLenses_jbs.dbo.qm_postcodes_campaign pc on c.postcode like pc.postcode_sector + '%'
	inner join
		Landing.mag.customer_entity_flat_aud fl on c.customer_id_bk = fl.entity_id
where isCurrent = 'Y'
	and c.created_at between '2017-04-01' and '2017-07-07'
	and idCountry_sk_fk = 78
	and (fl.old_access_cust_no not in ('LENSWAYUK', 'LENSONCOUK') or fl.old_access_cust_no is null)
order by c.created_at desc

-- ALL REGISTERED CUSTOMERS IN POSTCODES
select customer_id_bk customer_id, convert(date, c.created_at) registered_date, 
	-- customer_email, first_name, last_name, 
	street, city, postcode --, fl.old_access_cust_no
from 
		Warehouse.gen.dim_customer_scd c
	inner join
		DW_GetLenses_jbs.dbo.qm_postcodes_campaign pc on c.postcode like pc.postcode_sector + '%'
	inner join
		Landing.mag.customer_entity_flat_aud fl on c.customer_id_bk = fl.entity_id
where isCurrent = 'Y'
	and c.created_at > '2017-05-31'
	and idCountry_sk_fk = 78
	and (fl.old_access_cust_no not in ('LENSWAYUK', 'LENSONCOUK') or fl.old_access_cust_no is null)
order by c.created_at desc




select top 1000 c.idCustomerSCD_sk, c.customer_id_bk, c.created_at, c.customer_email, 
	c.first_name, c.last_name, c.street, c.city, c.postcode, 
	oh.entity_id, oh.increment_id, oh.created_at order_date, oh.coupon_code, oh.applied_rule_ids
from 
		Warehouse.gen.dim_customer_scd c
	inner join
		DW_GetLenses_jbs.dbo.qm_postcodes_campaign pc on c.postcode like pc.postcode_sector + '%'
	left join
		Landing.mag.sales_flat_order_aud oh on c.customer_id_bk = oh.customer_id
where c.isCurrent = 'Y'
	and c.created_at > '2017-05-21'
order by c.created_at desc
