
select top 1000 count(*) over () num_tot,
	customer_id, store_name, created_at,
	email, firstname, lastname, 
	gender, 
	segment_lifecycle, first_order_date, last_order_date, num_of_orders
from DW_GetLenses.dbo.customers
where store_name = 'visiondirect.co.uk'
-- order by customer_id desc
order by created_at desc

select *
	-- address_id, customer_id, country_id, region, region_id, postcode
from DW_GetLenses.dbo.customer_addresses
where customer_id in (1875594)

select top 1000 count(*) over () num_tot,
	count(*) over (partition by c.customer_id) num_rep,
	rank() over (partition by c.customer_id order by ad.address_id) rank_address,
	c.customer_id, c.default_billing, c.default_shipping, c.store_name, c.created_at,
	c.email, c.firstname, c.lastname, 
	ad.address_id, ad.country_id, ad.region, ad.region_id, ad.postcode, ad.city, ad.street,
	c.gender, 
	c.segment_lifecycle, c.first_order_date, c.last_order_date, c.num_of_orders
from
		(select -- top 2000 
			-- count(*) over () num_tot,
			customer_id, default_billing, default_shipping,
			store_name, created_at,
			email, firstname, lastname, 
			gender, 
			segment_lifecycle, first_order_date, last_order_date, num_of_orders
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.co.uk'
		-- order by customer_id desc
		) c
	inner join
		(select address_id, customer_id, country_id, region, region_id, postcode, city, street
		from DW_GetLenses.dbo.customer_addresses) ad on c.customer_id = ad.customer_id and c.default_billing = ad.address_id
order by c.customer_id desc


select top 1000 count(*) over () num_tot, num_tot_prev,
	customer_id, created_at, 
	-- store_name, email, firstname, lastname, 
	-- address_id, country_id, 
	postcode, 
	gender, 
	segment_lifecycle, first_order_date, last_order_date, num_of_orders	
from
	(select count(*) over () num_tot_prev,
		count(*) over (partition by c.customer_id) num_rep,
		rank() over (partition by c.customer_id order by ad.address_id) rank_address,
		c.customer_id, c.store_name, c.created_at,
		c.email, c.firstname, c.lastname, 
		ad.address_id, ad.country_id, ad.region, ad.region_id, ad.postcode, ad.city, ad.street,
		c.gender, 
		c.segment_lifecycle, c.first_order_date, c.last_order_date, c.num_of_orders
	from
			(select 
				customer_id, default_billing, default_shipping, 
				store_name, created_at,
				email, firstname, lastname, 
				gender, 
				segment_lifecycle, first_order_date, last_order_date, num_of_orders
			from DW_GetLenses.dbo.customers
			-- where store_name = 'visiondirect.co.uk'
			) c
		inner join
			(select address_id, customer_id, country_id, region, region_id, postcode, city, street
			from DW_GetLenses.dbo.customer_addresses) ad on c.customer_id = ad.customer_id and c.default_billing = ad.address_id) t
where num_rep = rank_address
	and country_id = 'GB'
order by num_of_orders desc, customer_id desc