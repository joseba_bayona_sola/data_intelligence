

	select 
		customer_id, created_at, 
		postcode postcode_b, postcode_2 postcode_s,
		prefix, gender, 
		segment_lifecycle, first_order_date, last_order_date, num_of_orders	
	into 
		#dd_customers
	from
		(select count(*) over () num_tot_prev,
			count(*) over (partition by c.customer_id) num_rep,
			rank() over (partition by c.customer_id order by ad.address_id) rank_address,
			c.customer_id, c.store_name, c.created_at,
			c.email, c.prefix, c.firstname, c.lastname, 
			ad.address_id, ad.country_id, ad.region, ad.region_id, ad.postcode, ad.city, ad.street,
			ad_2.address_id address_id_2, ad_2.country_id country_id_2, ad_2.region region_2, ad_2.region_id region_id_2, ad_2.postcode postcode_2, ad_2.city city_2, ad_2.street street_2,
			c.gender, 
			c.segment_lifecycle, c.first_order_date, c.last_order_date, c.num_of_orders
		from
				(select 
					customer_id, default_billing, default_shipping, 
					store_name, created_at,
					email, prefix, firstname, lastname, 
					gender, 
					segment_lifecycle, first_order_date, last_order_date, num_of_orders
				from DW_GetLenses.dbo.customers
				-- where store_name = 'visiondirect.co.uk'
				) c
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad on c.customer_id = ad.customer_id and c.default_billing = ad.address_id
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad_2 on c.customer_id = ad_2.customer_id and c.default_shipping = ad_2.address_id) t
	where num_rep = rank_address
		and country_id = 'GB'
	order by num_of_orders desc, customer_id desc

	select top 1000 count(*) over () num_tot, count(*) over (partition by customer_id) num_rep, *
	from #dd_customers
	where segment_lifecycle <> 'RNB' -- 769.578
	order by num_rep desc

	select top 1000 *, count(*) over ()
	from #dd_customers
	where postcode_b <> postcode_s
	
	select prefix, count(*) num
	from #dd_customers
	group by prefix
	order by num desc

select top 1000 ho.customer_id, count(*) num_orders, sum(grand_total) total_orders
from 
		DW_GetLenses.dbo.dw_hist_order ho
	inner join
		#dd_customers c on ho.customer_id = c.customer_id
where grand_total <> 0.00
group by ho.customer_id
order by ho.customer_id

select top 1000 oh.customer_id, count(*) num_orders, sum(local_total_inc_vat) 
from 
		DW_GetLenses.dbo.order_headers oh
	inner join
		#dd_customers c on oh.customer_id = c.customer_id
where document_type = 'ORDER'
group by oh.customer_id
order by oh.customer_id

------------------

-- 773785
select top 1000 count(*) over () num_tot,
	case when (t2.customer_id is null) then t1.customer_id else t2.customer_id end customer_id, 
	isnull(t1.num_orders, 0) + isnull(t2.num_orders, 0) num_orders,
	isnull(t1.total_orders, 0) + isnull(t2.total_orders, 0) total_orders
from
		(select ho.customer_id, count(*) num_orders, sum(grand_total) total_orders
		from 
				DW_GetLenses.dbo.dw_hist_order ho
			inner join
				#dd_customers c on ho.customer_id = c.customer_id
		where grand_total <> 0.00
		group by ho.customer_id) t1
	full join
		(select oh.customer_id, count(*) num_orders, sum(local_total_inc_vat) total_orders 
		from 
				DW_GetLenses.dbo.order_headers oh
			inner join
				#dd_customers c on oh.customer_id = c.customer_id
		where document_type = 'ORDER'
		group by oh.customer_id) t2 on t1.customer_id = t2.customer_id
order by customer_id

------------------

	select 
		dd.customer_id, dd.created_at, dd.postcode_b, dd.postcode_s, dd.prefix, dd.gender, dd.segment_lifecycle, 
		dd.first_order_date, dd.last_order_date, 
		dd.num_of_orders, t.num_orders num_orders_calc, t.total_orders total_value_orders_calc
	into 
		#dd_customers_values
	from 
			#dd_customers dd
		left join	
			(select 
				case when (t2.customer_id is null) then t1.customer_id else t2.customer_id end customer_id, 
				isnull(t1.num_orders, 0) + isnull(t2.num_orders, 0) num_orders,
				isnull(t1.total_orders, 0) + isnull(t2.total_orders, 0) total_orders
			from
					(select ho.customer_id, count(*) num_orders, sum(grand_total) total_orders
					from 
							DW_GetLenses.dbo.dw_hist_order ho
						inner join
							#dd_customers c on ho.customer_id = c.customer_id
					where grand_total <> 0.00
					group by ho.customer_id) t1
				full join
					(select oh.customer_id, count(*) num_orders, sum(local_total_inc_vat) total_orders 
					from 
							DW_GetLenses.dbo.order_headers oh
						inner join
							#dd_customers c on oh.customer_id = c.customer_id
					where document_type = 'ORDER'
					group by oh.customer_id) t2 on t1.customer_id = t2.customer_id) t on dd.customer_id = t.customer_id
	order by dd.customer_id desc

---------------------

	select top 1000 count(*) over (), *
	from #dd_customers_values
	where isnull(num_of_orders, 0) - isnull(num_orders_calc, 0) in (-1, 0, 1)
		-- and segment_lifecycle = 'RNB' 
		and segment_lifecycle = 'RNB' and (num_of_orders is not null or num_orders_calc is not null)
		-- and segment_lifecycle <> 'RNB' and num_of_orders is not null and (total_value_orders_calc is null or total_value_orders_calc = 0)
	order by customer_id desc

	-- 0: 914465
	select isnull(num_of_orders, 0) - isnull(num_orders_calc, 0), count(*)
	from #dd_customers_values
	group by isnull(num_of_orders, 0) - isnull(num_orders_calc, 0)
	order by isnull(num_of_orders, 0) - isnull(num_orders_calc, 0)


---------------------

	select idCustomer, prefix title, postcode_b, postcode_s, register_date, customer_status, 
		total_purchases, first_purchase, last_purchase, amount_spent, 
		case when (customer_status = 'RNB') then null else amount_spent / total_purchases end average_transaction_value
	from 
		(select 
			rank() over(order by customer_id) idCustomer, prefix, postcode_b, postcode_s, created_at register_date, segment_lifecycle customer_status, 
			case when (segment_lifecycle = 'RNB') then null else num_orders_calc end total_purchases, 
			case when (segment_lifecycle = 'RNB') then null else first_order_date end first_purchase, 
			case when (segment_lifecycle = 'RNB') then null else last_order_date end last_purchase, 
			case when (segment_lifecycle = 'RNB') then null else total_value_orders_calc end amount_spent
		from #dd_customers_values
		where isnull(num_of_orders, 0) - isnull(num_orders_calc, 0) in (-1, 0, 1)) t
	order by idCustomer 

	select segment_lifecycle, count(*)
	from #dd_customers_values
	group by segment_lifecycle
	order by segment_lifecycle

	select *
	from #dd_customers_values
	where postcode_b <> postcode_s
	order by customer_id desc

---------------------
-- Special Postcodes: 
	-- EC1V 3AQ - EC2N 2DB - EC2Y 8HQ - EC4A 2BB
	-- WC1N 1AS - WC1V 7AA - WC1X 0HD - WC2N 5BY

	select customer_id, idCustomer, prefix title, postcode_b, postcode_s, register_date, customer_status, 
		total_purchases, first_purchase, last_purchase, amount_spent, 
		case when (customer_status = 'RNB') then null else amount_spent / total_purchases end average_transaction_value
	into #dd_customers_values_pc
	from 
		(select customer_id, 
			rank() over(order by customer_id) idCustomer, prefix, postcode_b, postcode_s, created_at register_date, segment_lifecycle customer_status, 
			case when (segment_lifecycle = 'RNB') then null else num_orders_calc end total_purchases, 
			case when (segment_lifecycle = 'RNB') then null else first_order_date end first_purchase, 
			case when (segment_lifecycle = 'RNB') then null else last_order_date end last_purchase, 
			case when (segment_lifecycle = 'RNB') then null else total_value_orders_calc end amount_spent
		from #dd_customers_values
		where isnull(num_of_orders, 0) - isnull(num_orders_calc, 0) in (-1, 0, 1)) t
	where postcode_b in ('EC1V 3AQ', 'EC2N 2DB', 'EC2Y 8HQ', 'EC4A 2BB', 'WC1N 1AS', 'WC1V 7AA', 'WC1X 0HD', 'WC2N 5BY')
	order by postcode_b, customer_id 

	select postcode_b, count(*)
	from #dd_customers_values_pc
	group by postcode_b
	order by postcode_b


		select -- count(*) over () num_tot_prev,
			-- count(*) over (partition by c.customer_id) num_rep,
			-- rank() over (partition by c.customer_id order by ad.address_id) rank_address,
			c.customer_id, c.store_name, c.created_at,
			c.email, c.prefix, c.firstname, c.lastname, 
			ad.address_id, ad.country_id, ad.region, ad.region_id, ad.postcode, ad.city, ad.street,
			-- ad_2.address_id address_id_2, ad_2.country_id country_id_2, ad_2.region region_2, ad_2.region_id region_id_2, ad_2.postcode postcode_2, ad_2.city city_2, ad_2.street street_2,
			-- c.gender, 
			c.segment_lifecycle, c.first_order_date, c.last_order_date, c.num_of_orders
		from
				(select 
					customer_id, default_billing, default_shipping, 
					store_name, created_at,
					email, prefix, firstname, lastname, 
					gender, 
					segment_lifecycle, first_order_date, last_order_date, num_of_orders
				from DW_GetLenses.dbo.customers
				where customer_id in 
					(select customer_id
					from #dd_customers_values_pc)) c
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad on c.customer_id = ad.customer_id and c.default_billing = ad.address_id
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad_2 on c.customer_id = ad_2.customer_id and c.default_shipping = ad_2.address_id
		order by postcode, customer_id


---------------------
-- Special Postcodes 2 


	select customer_id, idCustomer, prefix title, postcode_b, postcode_s, register_date, customer_status, 
		total_purchases, first_purchase, last_purchase, amount_spent, 
		case when (customer_status = 'RNB') then null else amount_spent / total_purchases end average_transaction_value
	into #dd_customers_values_pc2
	from 
		(select customer_id, 
			rank() over(order by customer_id) idCustomer, prefix, postcode_b, postcode_s, created_at register_date, segment_lifecycle customer_status, 
			case when (segment_lifecycle = 'RNB') then null else num_orders_calc end total_purchases, 
			case when (segment_lifecycle = 'RNB') then null else first_order_date end first_purchase, 
			case when (segment_lifecycle = 'RNB') then null else last_order_date end last_purchase, 
			case when (segment_lifecycle = 'RNB') then null else total_value_orders_calc end amount_spent
		from #dd_customers_values
		where isnull(num_of_orders, 0) - isnull(num_orders_calc, 0) in (-1, 0, 1)) t
	where postcode_b in (select distinct postcode from DW_GetLenses_jbs.dbo.ex_postcodes_to_be_checked)
	order by postcode_b, customer_id 

		select -- count(*) over () num_tot_prev,
			-- count(*) over (partition by c.customer_id) num_rep,
			-- rank() over (partition by c.customer_id order by ad.address_id) rank_address,
			c.customer_id, c.store_name, convert(date, c.created_at) created_at,
			c.email, c.prefix, c.firstname, c.lastname, 
			ad.address_id, -- ad.country_id, 
			ad.region, -- ad.region_id, 
			count(*) over (partition by ad.postcode) num_cust_per_postcode,
			ad.postcode, ad.city, ad.street,
			-- ad_2.address_id address_id_2, ad_2.country_id country_id_2, ad_2.region region_2, ad_2.region_id region_id_2, ad_2.postcode postcode_2, ad_2.city city_2, ad_2.street street_2,
			-- c.gender, 
			c.segment_lifecycle, convert(date, c.first_order_date) first_order_date, convert(date, c.last_order_date) last_order_date, c.num_of_orders
		from
				(select 
					customer_id, default_billing, default_shipping, 
					store_name, created_at,
					email, prefix, firstname, lastname, 
					gender, 
					segment_lifecycle, first_order_date, last_order_date, num_of_orders
				from DW_GetLenses.dbo.customers
				where customer_id in 
					(select customer_id
					from #dd_customers_values_pc2)) c
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad on c.customer_id = ad.customer_id and c.default_billing = ad.address_id
			inner join
				(select address_id, customer_id, country_id, region, region_id, postcode, city, street
				from DW_GetLenses.dbo.customer_addresses) ad_2 on c.customer_id = ad_2.customer_id and c.default_shipping = ad_2.address_id
		order by postcode, customer_id
