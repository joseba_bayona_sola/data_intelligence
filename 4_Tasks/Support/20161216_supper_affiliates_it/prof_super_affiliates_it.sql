
select *
from DW_GetLenses.dbo.coupon_channel
where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82');

select top 1000 *
from DW_GetLenses.dbo.order_headers
where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
order by document_date desc;

	select top 1000 coupon_code, year(document_date) yyyy, month(document_date) mm, count(*)
	from DW_GetLenses.dbo.order_headers
	where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
	group by coupon_code, year(document_date), month(document_date) 
	order by coupon_code, yyyy, mm;

	select top 1000 coupon_code, business_source, business_channel,
		count(*)
	from DW_GetLenses.dbo.order_headers
	where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
	group by coupon_code, business_source, business_channel 
	order by coupon_code, business_source, business_channel;

----------------------------------------------------------

select top 1000 *
from DW_GetLenses.dbo.order_headers
where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
	and year(document_date) = 2016 and month(document_date) = 12
order by document_date desc;

	select top 1000 coupon_code, year(document_date) yyyy, month(document_date) mm, count(*)
	from DW_GetLenses.dbo.order_headers
	where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
		and year(document_date) = 2016 and month(document_date) = 12
	group by coupon_code, year(document_date), month(document_date) 
	order by coupon_code, yyyy, mm;

	select top 1000 coupon_code, year(document_date) yyyy, month(document_date) mm, business_source, business_channel,
		count(*)
	from DW_GetLenses.dbo.order_headers
	where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
		and year(document_date) = 2016 and month(document_date) = 12
	group by coupon_code, year(document_date), month(document_date), business_source, business_channel 
	order by coupon_code, yyyy, mm, business_source, business_channel;
