
select *
from dw_order_headers
where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
  and extract(year from created_at) = 2016 and extract(month from created_at) = 12
limit 100;

select oc.*, oh.coupon_code
from 
  dw_order_channel oc
inner join
  (select *
  from dw_order_headers
  where coupon_code in ('COMP10', 'DOUGLAS10', 'DOUGLAS16', 'JET24', 'JET82')
    and extract(year from created_at) = 2016 and extract(month from created_at) = 12) oh on oc.order_id = oh.order_id
limit 1000;