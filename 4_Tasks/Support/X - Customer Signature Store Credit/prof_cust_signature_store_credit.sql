
select top 1000 cs.customer_id, cs.customer_email, c.first_name, c.last_name,
	store_last, last_order_date, 
	customer_status_name, num_tot_orders, subtotal_tot_orders, num_tot_refund, subtotal_tot_refund, 
	num_tot_store_credit_orders, store_credit_tot_value
from 
		Warehouse.act.fact_customer_signature_v cs
	inner join
		Warehouse.gen.dim_customer_scd_v c on cs.customer_id = c.customer_id_bk
order by store_credit_tot_value desc

-------------------------------------------------

select *
from Warehouse.gen.dim_customer_scd_v
where customer_id_bk = 452363

select *
from Warehouse.act.fact_activity_sales_v
where customer_id = 452363
order by customer_order_seq_no desc

select order_line_id_bk, order_id_bk, order_date, 
	customer_order_seq_no, 
	category_name, product_id_magento, product_family_name, sku_magento, 
	local_subtotal, local_store_credit_used, local_total_inc_vat
from Warehouse.sales.fact_order_line_v
where customer_id = 452363
order by customer_order_seq_no desc, order_line_id_bk
