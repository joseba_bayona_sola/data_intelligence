
-- Orders with ONE row - document types
select top 100 *,
	count(*) over(partition by customer_id) count_ord_cust, 
	rank() over (partition by customer_id order by order_id) rank_ord_cust,
	max(order_id) over(partition by customer_id) max_ord_cust
from 
	(select 
		store_name, order_id, order_no, document_type, order_date, customer_id, 
		count(*) over(partition by order_id) count_rep
	from DW_GetLenses.dbo.order_headers) t
where count_rep = 1

	select top 100 order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
	from 
		(select *,
			count(*) over(partition by customer_id) count_ord_cust, 
			rank() over (partition by customer_id order by order_id) rank_ord_cust,
			max(order_id) over(partition by customer_id) max_ord_cust
		from 
			(select 
				store_name, order_id, order_no, document_type, order_date, customer_id, 
				count(*) over(partition by order_id) count_rep
			from DW_GetLenses.dbo.order_headers) t
		where count_rep = 1) t
	group by order_id, customer_id, order_date
	order by order_id, customer_id

	select num_dist_doc_types, doc_type, count(*)
	from
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep = 1) t
		group by order_id, customer_id, order_date) t
	group by num_dist_doc_types, doc_type	
	order by num_dist_doc_types, doc_type

	select *
	from
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep = 1) t
		group by order_id, customer_id, order_date) t
	where doc_type <> 'ORDER'
	order by order_id
	

-- Orders with MANY rows - document types
select top 100 *,
	count(*) over(partition by customer_id) count_ord_cust, 
	rank() over (partition by customer_id order by order_id) rank_ord_cust,
	max(order_id) over(partition by customer_id) max_ord_cust
from 
	(select 
		store_name, order_id, order_no, document_type, order_date, customer_id, 
		count(*) over(partition by order_id) count_rep
	from DW_GetLenses.dbo.order_headers) t
where count_rep > 1

	select top 100 order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
	from 
		(select *,
			count(*) over(partition by customer_id) count_ord_cust, 
			rank() over (partition by customer_id order by order_id) rank_ord_cust,
			max(order_id) over(partition by customer_id) max_ord_cust
		from 
			(select 
				store_name, order_id, order_no, document_type, order_date, customer_id, 
				count(*) over(partition by order_id) count_rep
			from DW_GetLenses.dbo.order_headers) t
		where count_rep > 1) t
	group by order_id, customer_id, order_date
	order by order_id, customer_id

	select num_dist_doc_types, doc_type, count(*)
	from
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep > 1) t
		group by order_id, customer_id, order_date) t
	group by num_dist_doc_types, doc_type	
	order by num_dist_doc_types, doc_type

	select *
	from
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep > 1) t
		group by order_id, customer_id, order_date) t
	where num_dist_doc_types in (3)
	order by order_id
	
