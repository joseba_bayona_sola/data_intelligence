
select top 1000 count(*) over () num_tot,
	customer_id, 
	reg_date, reg_date_year, 
	convert(date, reg_date, 105), convert(date, reg_date_year, 105)
from DW_GetLenses_jbs.dbo.ex_customers_num_orders
where customer_id = 1115518

	select count(*), min(convert(date, reg_date, 105)), max(convert(date, reg_date, 105)) 
	from DW_GetLenses_jbs.dbo.ex_customers_num_orders

	select customer_id, count(*)
	from DW_GetLenses_jbs.dbo.ex_customers_num_orders
	group by customer_id
	having count(*) > 1
	order by count(*) desc, customer_id

	select customer_id, reg_date, reg_date_year
	from 
		(select count(*) over (partition by customer_id) num_tot_cust,
			customer_id, 
			reg_date, reg_date_year
		from DW_GetLenses_jbs.dbo.ex_customers_num_orders) t
	where num_tot_cust > 1
	order by customer_id

select customer_id, created_at, first_order_date, num_of_orders,
	datediff(day, created_at, first_order_date) diff
from DW_GetLenses.dbo.customers
where customer_id in (997580, 997831, 1003451)

--- JOINING with CUSTOMER

select top 1000 count(*) over () num_tot,
	cno.customer_id, 
	cno.reg_date, cno.reg_date_year, 
	c.created_at, c.first_order_date, 
	datediff(day, cno.reg_date, c.created_at) diff_ex_reg_date_create, c.diff_create_first_day, c.diff_create_first_mm, 
	c.num_of_orders
from 
		(select distinct customer_id, 
			convert(date, reg_date, 105) reg_date, convert(date, reg_date_year, 105) reg_date_year
		from DW_GetLenses_jbs.dbo.ex_customers_num_orders) cno
	left join
		(select customer_id, created_at, first_order_date, num_of_orders, 
			datediff(day, created_at, first_order_date) diff_create_first_day,
			datediff(month, created_at, first_order_date) diff_create_first_mm
		from DW_GetLenses.dbo.customers) c on cno.customer_id = c.customer_id
--where c.customer_id is null

	-- Diference between Dates (Excel Reg Date - DB Create Date // DB Create Date - DB First Order Date)
	select diff_ex_reg_date_create, diff_create_first_mm, count(*)
	from
		(select cno.customer_id, 
			cno.reg_date, cno.reg_date_year, 
			c.created_at, c.first_order_date, 
			datediff(day, cno.reg_date, c.created_at) diff_ex_reg_date_create, c.diff_create_first_day, c.diff_create_first_mm, 
			c.num_of_orders
		from 
				(select customer_id, 
					convert(date, reg_date, 105) reg_date, convert(date, reg_date_year, 105) reg_date_year
				from DW_GetLenses_jbs.dbo.ex_customers_num_orders) cno
			inner join
				(select customer_id, created_at, first_order_date, num_of_orders, 
					datediff(day, created_at, first_order_date) diff_create_first_day,
					datediff(month, created_at, first_order_date) diff_create_first_mm
				from DW_GetLenses.dbo.customers) c on cno.customer_id = c.customer_id) t
	group by diff_ex_reg_date_create, diff_create_first_mm
	order by diff_ex_reg_date_create, diff_create_first_mm
