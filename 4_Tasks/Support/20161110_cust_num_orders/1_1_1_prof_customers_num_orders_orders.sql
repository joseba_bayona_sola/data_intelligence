
	---------------------------- SOURCES
				-- Customers
				select top 1000 count(*) over () num_tot,
					cno.customer_id, 
					cno.reg_date, cno.reg_date_year, 
					c.created_at, c.first_order_date, 
					datediff(day, cno.reg_date, c.created_at) diff_ex_reg_date_create, c.diff_create_first_day, c.diff_create_first_mm, 
					c.num_of_orders
				from 
						(select distinct customer_id, 
							convert(date, reg_date, 105) reg_date, convert(date, reg_date_year, 105) reg_date_year
						from DW_GetLenses_jbs.dbo.ex_customers_num_orders) cno
					inner join
						(select customer_id, created_at, first_order_date, num_of_orders, 
							datediff(day, created_at, first_order_date) diff_create_first_day,
							datediff(month, created_at, first_order_date) diff_create_first_mm
						from DW_GetLenses.dbo.customers) c on cno.customer_id = c.customer_id
				order by cno.customer_id

				-- Orders (1 Type)
				select top 100 *,
					count(*) over(partition by customer_id) count_ord_cust, 
					rank() over (partition by customer_id order by order_id) rank_ord_cust,
					max(order_id) over(partition by customer_id) max_ord_cust
				from 
					(select 
						store_name, order_id, order_no, document_type, order_date, customer_id, 
						count(*) over(partition by order_id) count_rep
					from DW_GetLenses.dbo.order_headers) t
				where count_rep = 1
					and customer_id = 1000001
				order by order_id

				-- Orders (Many Type)
				select top 100 order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
				from 
					(select *,
						count(*) over(partition by customer_id) count_ord_cust, 
						rank() over (partition by customer_id order by order_id) rank_ord_cust,
						max(order_id) over(partition by customer_id) max_ord_cust
					from 
						(select 
							store_name, order_id, order_no, document_type, order_date, customer_id, 
							count(*) over(partition by order_id) count_rep
						from DW_GetLenses.dbo.order_headers) t
					where count_rep > 1) t
				where customer_id = 1000001
				group by order_id, customer_id, order_date
				order by order_id, customer_id


---------------------------------------------------------------------------
--- CUSTOMER - ORDER LEVEL
select c.customer_id, 
	c.reg_date, c.reg_date_year, 
	o1.order_id, o1.document_type, o1.order_date, 
	o2.order_id, o2.doc_type, o2.order_date 
from 
		(select top 1000 count(*) over () num_tot,
			cno.customer_id, 
			cno.reg_date, cno.reg_date_year, 
			c.created_at, c.first_order_date, 
			datediff(day, cno.reg_date, c.created_at) diff_ex_reg_date_create, c.diff_create_first_day, c.diff_create_first_mm, 
			c.num_of_orders
		from 
				(select distinct customer_id, 
					convert(date, reg_date, 105) reg_date, convert(date, reg_date_year, 105) reg_date_year
				from DW_GetLenses_jbs.dbo.ex_customers_num_orders) cno
			inner join
				(select customer_id, created_at, first_order_date, num_of_orders, 
					datediff(day, created_at, first_order_date) diff_create_first_day,
					datediff(month, created_at, first_order_date) diff_create_first_mm
				from DW_GetLenses.dbo.customers) c on cno.customer_id = c.customer_id
		where cno.customer_id = 1002833
		) c
	left join 
		(select *,
			count(*) over(partition by customer_id) count_ord_cust, 
			rank() over (partition by customer_id order by order_id) rank_ord_cust,
			max(order_id) over(partition by customer_id) max_ord_cust
		from 
			(select 
				store_name, order_id, order_no, document_type, order_date, customer_id, 
				count(*) over(partition by order_id) count_rep
			from DW_GetLenses.dbo.order_headers) t
		where count_rep = 1) o1 on c.customer_id = o1.customer_id and o1.order_date between c.reg_date and c.reg_date_year
	left join 
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep > 1) t
		group by order_id, customer_id, order_date) o2 on c.customer_id = o2.customer_id and o2.order_date between c.reg_date and c.reg_date_year

--------------------------
--- CUSTOMER LEVEL
select c.customer_id, 
	c.reg_date, c.reg_date_year, 
	count(distinct o1.order_id) num_order_ok, count(distinct o2.order_id) num_order_esp
from 
		(select count(*) over () num_tot,
			cno.customer_id, 
			cno.reg_date, cno.reg_date_year, 
			c.created_at, c.first_order_date, 
			datediff(day, cno.reg_date, c.created_at) diff_ex_reg_date_create, c.diff_create_first_day, c.diff_create_first_mm, 
			c.num_of_orders
		from 
				(select distinct customer_id, 
					convert(date, reg_date, 105) reg_date, convert(date, reg_date_year, 105) reg_date_year
				from DW_GetLenses_jbs.dbo.ex_customers_num_orders) cno
			inner join
				(select customer_id, created_at, first_order_date, num_of_orders, 
					datediff(day, created_at, first_order_date) diff_create_first_day,
					datediff(month, created_at, first_order_date) diff_create_first_mm
				from DW_GetLenses.dbo.customers) c on cno.customer_id = c.customer_id) c
	left join 
		(select *,
			count(*) over(partition by customer_id) count_ord_cust, 
			rank() over (partition by customer_id order by order_id) rank_ord_cust,
			max(order_id) over(partition by customer_id) max_ord_cust
		from 
			(select 
				store_name, order_id, order_no, document_type, order_date, customer_id, 
				count(*) over(partition by order_id) count_rep
			from DW_GetLenses.dbo.order_headers) t
		where count_rep = 1) o1 on c.customer_id = o1.customer_id and o1.order_date between c.reg_date and c.reg_date_year
	left join 
		(select order_id, customer_id, order_date, count(*) num_rows, count(distinct document_type) num_dist_doc_types, min(document_type) doc_type
		from 
			(select *,
				count(*) over(partition by customer_id) count_ord_cust, 
				rank() over (partition by customer_id order by order_id) rank_ord_cust,
				max(order_id) over(partition by customer_id) max_ord_cust
			from 
				(select 
					store_name, order_id, order_no, document_type, order_date, customer_id, 
					count(*) over(partition by order_id) count_rep
				from DW_GetLenses.dbo.order_headers) t
			where count_rep > 1) t	
		group by order_id, customer_id, order_date) o2 on c.customer_id = o2.customer_id and o2.order_date between c.reg_date and c.reg_date_year
group by c.customer_id, c.reg_date, c.reg_date_year
order by c.customer_id