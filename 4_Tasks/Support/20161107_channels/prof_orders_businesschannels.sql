
select *
from DW_GetLenses.dbo.business_source
order by channel, source_code

select top 100 store_name, order_id, order_no, document_type, 
	order_date, customer_id, 
	business_channel
from DW_GetLenses.dbo.order_headers

----------------------------------------------------------------------

select business_channel, count(*), count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot,
	min(order_date), max(order_date)
from 
	(select count(*) over() num_tot,
		store_name, order_id, order_no, document_type, 
		order_date, customer_id, 
		business_channel
	from DW_GetLenses.dbo.order_headers) t
group by num_tot, business_channel
order by num_tot, business_channel

	---
	select business_channel, count(*), count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot,
		min(order_date), max(order_date)
	from 
		(select count(*) over() num_tot,
			store_name, order_id, order_no, document_type, 
			order_date, customer_id, 
			business_channel
		from DW_GetLenses.dbo.order_headers
		where document_type = 'CREDITMEMO') t
	group by num_tot, business_channel
	order by num_tot, business_channel

	--------------------------------------------------------
	-- Same Order Different Channels depending on document_type
	select business_channel, count(*), count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot, count(distinct coupon_code),
		min(order_date), max(order_date)
	from 
		(select count(*) over() num_tot,
			store_name, order_id, order_no, document_type, 
			order_date, customer_id, 
			business_channel, coupon_code
		from DW_GetLenses.dbo.order_headers
		where coupon_code is not null and len(ltrim(coupon_code)) <> 0) t
	group by num_tot, business_channel
	order by num_tot, business_channel

	select coupon_code, count(*), count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot, count(distinct business_channel),
		min(order_date), max(order_date)
	from 
		(select count(*) over() num_tot,
			store_name, order_id, order_no, document_type, 
			order_date, customer_id, 
			business_channel, coupon_code
		from DW_GetLenses.dbo.order_headers
		where coupon_code is not null and len(ltrim(coupon_code)) <> 0) t
	group by num_tot, coupon_code
	order by count(*) desc, coupon_code

	
		select business_channel, coupon_code, count(*), count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot, count(distinct coupon_code),
			min(order_date), max(order_date)
		from 
			(select count(*) over(partition by coupon_code) num_tot,
				store_name, order_id, order_no, document_type, 
				order_date, customer_id, 
				business_channel, coupon_code
			from DW_GetLenses.dbo.order_headers
			where coupon_code IN ('SURPRISE', 'WELCOME7', 'DISCOUNT10', 'Lenses32', 'Lenses7')) t
		group by num_tot, business_channel, coupon_code
		order by num_tot, coupon_code, business_channel
