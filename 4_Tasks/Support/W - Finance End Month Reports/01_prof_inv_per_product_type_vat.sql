
select top 1000 order_line_id_bk, order_line_id_bk_c, order_id_bk, order_id_bk_c, order_no, 
	invoice_id, invoice_no, invoice_date, invoice_date_c, 
	-- website_type, 
	store_name, 
	product_id_magento, product_type_vat, countries_registered_code,
	local_total_exc_vat, local_total_vat, local_total_inc_vat, local_total_prof_fee,
	global_total_exc_vat, global_total_vat, global_total_inc_vat, global_total_prof_fee, 
	local_to_global_rate
from Warehouse.sales.fact_order_line_inv_vn
where invoice_date between '2017-08-01' and '2017-09-01'
	and countries_registered_code = 'EU-ES'
order by shipment_date

select product_type_vat, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee, 
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-ES'
group by product_type_vat
order by local_total_exc_vat desc

select invoice_date_c, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee, 
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_vn
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-IT'
group by invoice_date_c
order by invoice_date_c

select country_code_ship, country_name_ship,
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee, 
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-ZZ'
group by country_code_ship, country_name_ship
order by local_total_exc_vat desc




select order_currency_code, 
	product_type_vat, 
	sum(local_total_exc_vat) local_total_exc_vat, sum(local_total_vat) local_total_vat, sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_prof_fee) local_total_prof_fee, 
	sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-SE'
group by order_currency_code, product_type_vat
order by order_currency_code, local_total_exc_vat desc
