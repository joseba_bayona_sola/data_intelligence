
select invoice_date_c, order_no,
	sum(local_total_exc_vat), sum(local_total_vat), sum(local_total_inc_vat), sum(local_total_prof_fee), 
	sum(local_total_inc_vat) - sum(local_total_prof_fee), sum(local_total_vat), sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-NL'
group by invoice_date_c, order_no
order by invoice_date_c, order_no


select invoice_date_c, order_no,
	sum(local_total_inc_vat), sum(local_total_prof_fee), sum(local_total_inc_vat) - sum(local_total_prof_fee), 
	sum(local_total_vat), sum(local_total_exc_vat) - sum(local_total_prof_fee) local_total_exc_vat_after_prof_fee,
	sum(local_total_exc_vat)
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2017-09-01' and '2017-10-01'
	and countries_registered_code = 'EU-NL'
group by invoice_date_c, order_no
order by invoice_date_c, order_no
