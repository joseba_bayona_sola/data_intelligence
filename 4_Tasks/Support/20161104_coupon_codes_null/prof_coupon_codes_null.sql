
select top 100 order_lifecycle, coupon_code, local_discount_inc_vat
from DW_GetLenses.dbo.order_headers
where store_name = 'visiondirect.nl'
	and document_date between '2016-10-01' and '2016-10-30'

select top 100 coupon_code, 
	count(*), sum(global_total_exc_vat), sum(local_discount_inc_vat) 
from DW_GetLenses.dbo.order_headers
where store_name = 'visiondirect.nl'
	and document_date between '2016-10-01' and '2016-10-30'
group by coupon_code
order by sum(global_total_exc_vat) desc

select top 100 order_lifecycle, coupon_code, 
	count(*), sum(global_total_exc_vat), sum(local_discount_inc_vat) 
from DW_GetLenses.dbo.order_headers
where store_name = 'visiondirect.nl'
	and document_date between '2016-10-01' and '2016-10-30'
group by order_lifecycle, coupon_code
order by order_lifecycle, coupon_code