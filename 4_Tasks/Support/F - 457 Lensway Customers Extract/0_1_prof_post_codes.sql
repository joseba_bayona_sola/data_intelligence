
select post_code, info, 
	SUBSTRING(postcode, 1, charindex(' ', post_code
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius

update DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
set 
	post_code = REPLACE(post_code, '"', ''), 
	info = REPLACE(info, '"', '');

alter table DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius add post_code_clean varchar(50);

update DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
set 
	post_code_clean = SUBSTRING(post_code, 1, charindex(' ', post_code)-1) 
	
select post_code, info, post_code_clean
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
where post_code_clean = 'SG12'

select distinct post_code_clean
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
order by post_code_clean

--------------------------------------

select post_code_clean, count(*) 
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
group by post_code_clean
order by count(*) desc

select post_code, info, post_code_clean, 
	count(*) over (partition by post_code_clean) num_pc_rep
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius
order by post_code
