
select entity_id, email, prefix, firstname, lastname, cus_phone, created_at
from lensway.lensway_customer
where entity_id = 10005348352
order by created_at desc
limit 1000;

-- lensway_customer_address (106.831)
select *
from lensway.lensway_customer_address
where parent_id in (3817111689, 10014294024, 3976134829, 12451119134)
order by parent_id, created_at
limit 1000;


select entity_id, parent_id, 
  prefix, firstname, lastname, company,
  city, postcode, region, country_id, 
  telephone
from lensway.lensway_customer_address
where parent_id = 3976134829
limit 1000;

  select parent_id, count(*)
  from lensway.lensway_customer_address
  group by parent_id
  order by count(*) desc
  limit 1000 

  select region, count(*)
  from lensway.lensway_customer_address
  group by region
  order by region
  
  select postcode, count(*)
  from lensway.lensway_customer_address
  group by postcode
  order by count(*) desc, postcode
  
-- ----------------------------------------

    select o.entity_id, o.increment_id, opr.item_product_family,
			o.store_name, o.customer_id, 
			o.created_at, STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p') created_at_d,
			o.base_grand_total
		from 
				lensway.lensway_order o
			inner join
				lensway.lensway_aux_order_pr_family opr on o.entity_id = opr.order_id and opr.num_dif_pr = 1
    where STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p') > curdate() - interval 183 day
    order by STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p')
      

    select oi.item_id, oi.order_id, oi.order_number, oi.product_id, oi.name
		from 
				lensway.lensway_order o
			inner join
				lensway.lensway_aux_order_pr_family opr on o.entity_id = opr.order_id and opr.num_dif_pr = 1
      inner join 
        lensway.lensway_order_item oi on o.entity_id = oi.order_id
    where STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p') > curdate() - interval 183 day
    order by STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p')

select oi.item_id, oi.order_id, oi.order_number, oi.product_id, oi.name, oi.row_total
from lensway.lensway_order_item oi    
where STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') > curdate() - interval 183 day

select product_id, lensway_name, magento_id, magento_name
from lensway.lensway_product_mapping
