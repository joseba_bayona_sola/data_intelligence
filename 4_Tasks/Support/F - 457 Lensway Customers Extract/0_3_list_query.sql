
select t.entity_id, t.email, t.firstname, t.lastname, t.cus_phone, 
	o.increment_id, o.item_product_family, o.created_at_dt, 
	rank() over (partition by t.entity_id order by o.entity_id) num_order
from	
		(select count(*) over () num_tot,
			c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
		from
			(select count(*) over () num_tot, -- 118.552 // 45.925
				rank() over (partition by parent_id order by entity_id) ord_addr,
				entity_id, parent_id, 
				prefix, firstname, lastname, 
				city, postcode, region, country_id, telephone, 
				pc.post_code_clean
			from 
					DW_GetLenses_jbs.dbo.lensway_customer_address ca
				left join
					(select distinct post_code_clean
					from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
			where pc.post_code_clean is not null) t
		inner join
			DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
		where ord_addr = 1) t
	inner join
		DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id
order by t.entity_id

select *
from
	(select t.entity_id, t.email, t.firstname, t.lastname, t.cus_phone, 
		o.increment_id, o.item_product_family, o.created_at_dt, 
		rank() over (partition by t.entity_id order by o.entity_id) num_order, 
		count(*) over (partition by t.entity_id) tot_order
	from	
			(select count(*) over () num_tot,
				c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
			from
				(select count(*) over () num_tot, -- 118.552 // 45.925
					rank() over (partition by parent_id order by entity_id) ord_addr,
					entity_id, parent_id, 
					prefix, firstname, lastname, 
					city, postcode, region, country_id, telephone, 
					pc.post_code_clean
				from 
						DW_GetLenses_jbs.dbo.lensway_customer_address ca
					left join
						(select distinct post_code_clean
						from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
				where pc.post_code_clean is not null) t
			inner join
				DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
			where ord_addr = 1) t
		inner join
			DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id) t
where num_order = tot_order

-- JOIN WITH PRODUCT ITEM

select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
	o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
	oi.product_id, oi.name, pm.magento_id, pm.magento_name,
	oi.row_total,
	rank() over (partition by o.entity_id order by pm.magento_id, oi.row_total desc, oi.item_id) item_ord
from
		(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
			o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt
		from
			(select o.customer_id, t.email, t.firstname, t.lastname, t.cus_phone, 
				o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
				rank() over (partition by t.entity_id order by o.entity_id) num_order, 
				count(*) over (partition by t.entity_id) tot_order
			from	
					(select count(*) over () num_tot,
						c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
					from
						(select count(*) over () num_tot, -- 118.552 // 45.925
							rank() over (partition by parent_id order by entity_id) ord_addr,
							entity_id, parent_id, 
							prefix, firstname, lastname, 
							city, postcode, region, country_id, telephone, 
							pc.post_code_clean
						from 
								DW_GetLenses_jbs.dbo.lensway_customer_address ca
							left join
								(select distinct post_code_clean
								from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
						where pc.post_code_clean is not null) t
					inner join
						DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
					where ord_addr = 1) t
				inner join
					DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id) o
		where num_order = tot_order) o
	inner join
		DW_GetLenses_jbs.dbo.lensway_order_item_6m oi on o.entity_id = oi.order_id
	inner join
		DW_GetLenses_jbs.dbo.lensway_product_mapping pm on oi.product_id = pm.product_id
order by o.customer_id, o.entity_id, oi.product_id

---- 

select *, 
	count(*) over (partition by item_product_family) num_prod_type
from
	(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
		o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
		oi.product_id, oi.name, pm.magento_id, pm.magento_name,
		oi.row_total,
		rank() over (partition by o.entity_id order by pm.magento_id, oi.row_total desc, oi.item_id) item_ord
	from
			(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
				o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt
			from
				(select o.customer_id, t.email, t.firstname, t.lastname, t.cus_phone, 
					o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
					rank() over (partition by t.entity_id order by o.entity_id) num_order, 
					count(*) over (partition by t.entity_id) tot_order
				from	
						(select count(*) over () num_tot,
							c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
						from
							(select count(*) over () num_tot, -- 118.552 // 45.925
								rank() over (partition by parent_id order by entity_id) ord_addr,
								entity_id, parent_id, 
								prefix, firstname, lastname, 
								city, postcode, region, country_id, telephone, 
								pc.post_code_clean
							from 
									DW_GetLenses_jbs.dbo.lensway_customer_address ca
								left join
									(select distinct post_code_clean
									from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
							where pc.post_code_clean is not null) t
						inner join
							DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
						where ord_addr = 1) t
					inner join
						DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id) o
			where num_order = tot_order) o
		inner join
			DW_GetLenses_jbs.dbo.lensway_order_item_6m oi on o.entity_id = oi.order_id
		inner join
			DW_GetLenses_jbs.dbo.lensway_product_mapping pm on oi.product_id = pm.product_id) t
where item_ord = 1
order by customer_id, entity_id, product_id

------------------------------------

select concat(firstname, ' ', lastname) customer_name, email customer_email, cus_phone customer_phone_number, 
	 convert(date, created_at_dt) last_order_date, item_product_family product_type, magento_name last_product_name
from
	(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
		o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
		oi.product_id, oi.name, pm.magento_id, pm.magento_name,
		oi.row_total,
		rank() over (partition by o.entity_id order by pm.magento_id, oi.row_total desc, oi.item_id) item_ord
	from
			(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
				o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt
			from
				(select o.customer_id, t.email, t.firstname, t.lastname, t.cus_phone, 
					o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
					rank() over (partition by t.entity_id order by o.entity_id) num_order, 
					count(*) over (partition by t.entity_id) tot_order
				from	
						(select count(*) over () num_tot,
							c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
						from
							(select count(*) over () num_tot, -- 118.552 // 45.925
								rank() over (partition by parent_id order by entity_id) ord_addr,
								entity_id, parent_id, 
								prefix, firstname, lastname, 
								city, postcode, region, country_id, telephone, 
								pc.post_code_clean
							from 
									DW_GetLenses_jbs.dbo.lensway_customer_address ca
								left join
									(select distinct post_code_clean
									from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
							where pc.post_code_clean is not null) t
						inner join
							DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
						where ord_addr = 1) t
					inner join
						DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id) o
			where num_order = tot_order) o
		inner join
			DW_GetLenses_jbs.dbo.lensway_order_item_6m oi on o.entity_id = oi.order_id
		inner join
			DW_GetLenses_jbs.dbo.lensway_product_mapping pm on oi.product_id = pm.product_id) t
where item_ord = 1 and item_product_family = 'GLASSES' -- CONTACTS / GLASSES
order by last_product_name, last_order_date

-- 

select concat(t.firstname, ' ', t.lastname) customer_name, t.email customer_email, t.cus_phone customer_phone_number, 
	 convert(date, created_at_dt) last_order_date, item_product_family product_type, magento_name last_product_name
from
	(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
		o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
		oi.product_id, oi.name, pm.magento_id, pm.magento_name,
		oi.row_total,
		rank() over (partition by o.entity_id order by pm.magento_id, oi.row_total desc, oi.item_id) item_ord
	from
			(select o.customer_id, o.email, o.firstname, o.lastname, o.cus_phone, 
				o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt
			from
				(select o.customer_id, t.email, t.firstname, t.lastname, t.cus_phone, 
					o.entity_id, o.increment_id, o.item_product_family, o.created_at_dt, 
					rank() over (partition by t.entity_id order by o.entity_id) num_order, 
					count(*) over (partition by t.entity_id) tot_order
				from	
						(select count(*) over () num_tot,
							c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
						from
							(select count(*) over () num_tot, -- 118.552 // 45.925
								rank() over (partition by parent_id order by entity_id) ord_addr,
								entity_id, parent_id, 
								prefix, firstname, lastname, 
								city, postcode, region, country_id, telephone, 
								pc.post_code_clean
							from 
									DW_GetLenses_jbs.dbo.lensway_customer_address ca
								left join
									(select distinct post_code_clean
									from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
							where pc.post_code_clean is not null) t
						inner join
							DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
						where ord_addr = 1) t
					inner join
						DW_GetLenses_jbs.dbo.lensway_order_6m o on t.entity_id = o.customer_id) o
			where num_order = tot_order) o
		inner join
			DW_GetLenses_jbs.dbo.lensway_order_item_6m oi on o.entity_id = oi.order_id
		inner join
			DW_GetLenses_jbs.dbo.lensway_product_mapping pm on oi.product_id = pm.product_id) t
	left join
		DW_GetLenses.dbo.customers c on t.email = c.email
	left join
		DW_GetLenses_jbs.dbo.lensway_customers_opt_out coo on t.email = coo.email_address
where item_ord = 1 and item_product_family = 'GLASSES' -- CONTACTS / GLASSES
	and c.email is null
	and coo.email_address is null
order by last_product_name, last_order_date
