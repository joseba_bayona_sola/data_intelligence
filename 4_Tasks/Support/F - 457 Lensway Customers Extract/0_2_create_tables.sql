
-- Customer

drop table DW_GetLenses_jbs.dbo.lensway_customer;

create table DW_GetLenses_jbs.dbo.lensway_customer(
	entity_id			bigint, 
	email				varchar(255), 
	prefix				varchar(255),
	firstname			varchar(255),
	lastname			varchar(255),
	cus_phone			varchar(255),
	created_at			datetime)

insert into DW_GetLenses_jbs.dbo.lensway_customer(entity_id, email, prefix, firstname, lastname, cus_phone, created_at)

	select cast(entity_id as bigint) entity_id, email, prefix, firstname, lastname, cus_phone, created_at
	from openquery(MYSQL_TEST,
		'select entity_id, email, prefix, firstname, lastname, cus_phone, created_at
		from lensway.lensway_customer')

-- Customer Address

drop table DW_GetLenses_jbs.dbo.lensway_customer_address;

create table DW_GetLenses_jbs.dbo.lensway_customer_address(
	entity_id			bigint, 
	parent_id			bigint, 
	prefix				varchar(500),
	firstname			varchar(500),
	lastname			varchar(500),
	city				varchar(500),
	postcode			varchar(500),
	region				varchar(500),
	country_id			varchar(500),
	telephone			varchar(500))

insert into DW_GetLenses_jbs.dbo.lensway_customer_address(entity_id, parent_id, 
	prefix, firstname, lastname, 
	city, postcode, region, country_id, telephone)

	select cast(entity_id as bigint) entity_id, cast(parent_id as bigint) parent_id, 
		prefix, firstname, lastname, 
		city, postcode, region, country_id, telephone
	from openquery(MYSQL_TEST,
		'select entity_id, parent_id, 
			prefix, firstname, lastname, city, postcode, region, country_id, telephone
		from lensway.lensway_customer_address')

-- Orders
create table DW_GetLenses_jbs.dbo.lensway_order_6m(
	entity_id				bigint, 
	increment_id			int, 
	item_product_family		varchar(50),
	store_name				varchar(50), 
	customer_id				bigint, 
	created_at_dt			datetime, 
	created_at				date, 
	base_grand_total		decimal(12, 2)); 

insert into DW_GetLenses_jbs.dbo.lensway_order_6m(entity_id,
	increment_id, item_product_family,
	store_name, customer_id, 
	created_at_dt, created_at,
	base_grand_total)

	select cast(entity_id as bigint) entity_id,
		cast(increment_id as int) increment_id, item_product_family,
		store_name, cast(customer_id as bigint) customer_id, 
		created_at_d created_at_dt, convert(date, created_at_d) created_at,
		cast(base_grand_total as decimal(12,2)) base_grand_total
	from openquery(MYSQL_TEST, 
		'select o.entity_id, o.increment_id, opr.item_product_family,
			o.store_name, o.customer_id, 
			o.created_at, STR_TO_DATE(o.created_at, ''%b %d %Y %h:%i%p'') created_at_d,
			o.base_grand_total
		from 
				lensway.lensway_order o
			inner join
				lensway.lensway_aux_order_pr_family opr on o.entity_id = opr.order_id and opr.num_dif_pr = 1
		where STR_TO_DATE(o.created_at, ''%b %d %Y %h:%i%p'') > curdate() - interval 183 day')

-- Orders Items
create table DW_GetLenses_jbs.dbo.lensway_order_item_6m(
	item_id					bigint,
	order_id				bigint, 
	order_number			int, 
	product_id				bigint, 
	name					varchar(255), 
	row_total				decimal(12, 2)); 

insert into DW_GetLenses_jbs.dbo.lensway_order_item_6m(item_id, order_id, order_number, product_id, name, row_total)

	select item_id, order_id, order_number, product_id, name, row_total
	from openquery(MYSQL_TEST, 
		'select item_id, order_id, order_number, product_id, name, row_total
		from lensway.lensway_order_item oi    
		where STR_TO_DATE(created_at, ''%b %d %Y %h:%i%p'') > curdate() - interval 183 day')


-- Product Mapping
drop table DW_GetLenses_jbs.dbo.lensway_product_mapping

create table DW_GetLenses_jbs.dbo.lensway_product_mapping(
	product_id			bigint, 
	lensway_name		varchar(255), 
	magento_id			bigint, 
	magento_name		varchar(255))

insert into DW_GetLenses_jbs.dbo.lensway_product_mapping(product_id, lensway_name, magento_id, magento_name)

	select cast(product_id as bigint) product_id, lensway_name, cast(magento_id as bigint) magento_id, magento_name
	from openquery(MYSQL_TEST, 
		'select product_id, lensway_name, magento_id, magento_name
		from lensway.lensway_product_mapping')