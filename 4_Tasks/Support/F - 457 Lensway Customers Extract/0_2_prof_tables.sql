
select top 1000 entity_id, email, prefix, firstname, lastname, cus_phone, created_at 
from DW_GetLenses_jbs.dbo.lensway_customer

select top 1000 entity_id, parent_id, 
	prefix, firstname, lastname, 
	city, postcode, region, country_id, telephone
from DW_GetLenses_jbs.dbo.lensway_customer_address
order by parent_id, entity_id

select entity_id, increment_id, item_product_family, store_name, customer_id, created_at_dt, created_at, base_grand_total
from DW_GetLenses_jbs.dbo.lensway_order_6m

select item_id, order_id, order_number, product_id, name, row_total
from DW_GetLenses_jbs.dbo.lensway_order_item_6m

select product_id, lensway_name, magento_id, magento_name
from DW_GetLenses_jbs.dbo.lensway_product_mapping;

-----------------------------------------------------------------

	select postcode, count(*)
	from DW_GetLenses_jbs.dbo.lensway_customer_address
	group by postcode
	order by postcode

	select postcode, substring(postcode, 1, charindex(' ', postcode)), count(*)
	from DW_GetLenses_jbs.dbo.lensway_customer_address
	group by postcode
	order by postcode

	select post_code_clean, sum(num)
	from 
		(select postcode, substring(postcode, 1, charindex(' ', postcode)) post_code_clean, count(*) num
		from DW_GetLenses_jbs.dbo.lensway_customer_address
		group by postcode) t
	where post_code_clean is not null
	group by post_code_clean
	order by post_code_clean


-----------------------------------------------------------------

	select top 1000 count(*) over () num_tot, -- 118.552 // 45.925
		entity_id, parent_id, 
		prefix, firstname, lastname, 
		city, postcode, region, country_id, telephone, 
		pc.post_code_clean
	from 
			DW_GetLenses_jbs.dbo.lensway_customer_address ca
		left join
			(select distinct post_code_clean
			from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on ca.postcode like concat(pc.post_code_clean, '%')
	where pc.post_code_clean is not null
	order by parent_id, entity_id


	-- 
	select top 1000 count(*) over () num_tot, -- 118.552 // 45.925
		rank() over (partition by parent_id order by entity_id) ord_addr,
		entity_id, parent_id, 
		prefix, firstname, lastname, 
		city, postcode, region, country_id, telephone, 
		pc.post_code_clean
	from 
			DW_GetLenses_jbs.dbo.lensway_customer_address ca
		left join
			(select distinct post_code_clean
			from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
	where pc.post_code_clean is not null
	order by parent_id, entity_id

	select top 1000 count(*) over () num_tot_2, *
	from
		(select count(*) over () num_tot, -- 118.552 // 45.925
			rank() over (partition by parent_id order by entity_id) ord_addr,
			entity_id, parent_id, 
			prefix, firstname, lastname, 
			city, postcode, region, country_id, telephone, 
			pc.post_code_clean
		from 
				DW_GetLenses_jbs.dbo.lensway_customer_address ca
			left join
				(select distinct post_code_clean
				from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
		where pc.post_code_clean is not null) t
	where ord_addr = 1
	order by parent_id, entity_id

	-- 

	select top 1000 count(*) over () num_tot,
		c.entity_id, c.email, c.firstname, c.lastname, c.cus_phone
	from
		(select count(*) over () num_tot, -- 118.552 // 45.925
			rank() over (partition by parent_id order by entity_id) ord_addr,
			entity_id, parent_id, 
			prefix, firstname, lastname, 
			city, postcode, region, country_id, telephone, 
			pc.post_code_clean
		from 
				DW_GetLenses_jbs.dbo.lensway_customer_address ca
			left join
				(select distinct post_code_clean
				from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(ca.postcode, 1, charindex(' ', ca.postcode)) = pc.post_code_clean
		where pc.post_code_clean is not null) t
	inner join
		DW_GetLenses_jbs.dbo.lensway_customer c on t.parent_id = c.entity_id
	where ord_addr = 1
	order by t.parent_id, t.entity_id
		