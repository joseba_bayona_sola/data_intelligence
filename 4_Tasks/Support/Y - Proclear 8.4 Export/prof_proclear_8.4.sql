
select *
from Warehouse.prod.dim_product_family_v
where product_family_name like 'Proclear Toric'

select top 1000 order_line_id_bk, order_id_bk, order_date, 
	website_group, website, customer_id, customer_email, 
	product_id_magento, base_curve, diameter, power, cylinder, axis, addition, dominance, 
	qty_unit, qty_pack
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1129
	and base_curve = '8.4'
order by order_date desc, order_line_id_bk

select top 1000 order_line_id_bk, order_id_bk, order_date, 
	website_group, website, customer_id, customer_email, 
	product_id_magento, base_curve, diameter, power, cylinder, axis, addition, dominance, 
	qty_unit, qty_pack
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1129
	and base_curve = '8.4'
	and order_date > getutcdate() - 365
order by order_date desc, order_line_id_bk


select distinct website, customer_email
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1129
	and base_curve = '8.4'
	and order_date > getutcdate() - 365
order by website


