
select top 100 
	document_date, order_date, 
	reminder_type, reminder_date, convert(date, reminder_date) reminder_date_2,
	reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent

from DW_GetLenses.dbo.order_headers
where reminder_type = 'email'
	and reminder_period = '30';

select reminder_type, count(*)
from DW_GetLenses.dbo.order_headers
where reminder_period = '30'
group by reminder_type
order by reminder_type;

select reminder_period, count(*)
from DW_GetLenses.dbo.order_headers
group by reminder_period
order by count(*) desc, reminder_period;

select reminder_type, reminder_period, count(*)
from DW_GetLenses.dbo.order_headers
group by reminder_type, reminder_period
order by reminder_type, reminder_period;


select top 100 year(reminder_date) yyyy,  month(reminder_date) mm, count(*)
from DW_GetLenses.dbo.order_headers
where reminder_type = 'email'
	and year(reminder_date) in (2015, 2016)
group by year(reminder_date), month(reminder_date)
order by yyyy desc, mm
