
select city, count(*)
from 
		Landing.migra_lenswaynl.migrate_customerdata mc
	inner join
		Warehouse.gen.dim_customer_v c on mc.new_magento_id = c.customer_id_bk
	inner join
		Warehouse.gen.dim_customer_scd_v cs on c.idCustomer_sk = cs.idCustomer_sk_fk
group by city
order by count(*) desc, city

select mc.old_customer_id, mc.new_magento_id, c.migrate_customer_store, 
	c.customer_email,
	cs.first_name, cs.last_name, cs.city, cs.postcode, cs.phone_number
into #lensway_nl_usability_cust
from 
		Landing.migra_lenswaynl.migrate_customerdata mc
	inner join
		Warehouse.gen.dim_customer_v c on mc.new_magento_id = c.customer_id_bk
	inner join
		Warehouse.gen.dim_customer_scd_v cs on c.idCustomer_sk = cs.idCustomer_sk_fk
where c.migrate_customer_store is not null
	and mc.new_magento_id > 2175370
	and city = 'Amsterdam'
	and cs.postcode like '10%'
order by cs.postcode, mc.new_magento_id

select *
into #lensway_nl_usability_cust_orders
from
	(select uc.*, 
		od.magento_order_id, od.new_increment_id, od.created_at, 
		count(*) over (partition by uc.new_magento_id) num_ord_cust, 
		rank() over (partition by uc.new_magento_id order by od.created_at) rank_ord_cust
	from 
			#lensway_nl_usability_cust uc
		inner join
			Landing.migra_lenswaynl.migrate_orderdata od on uc.new_magento_id = od.magento_customer_id ) t
where num_ord_cust = rank_ord_cust
order by new_magento_id, magento_order_id

select first_name + ' ' + last_name customer_name, customer_email, phone_number customer_telephone, 
	convert(date, created_at) last_order, productName last_product, num_ord_cust num_orders
from
	(select uco.*, odl.product_id, odl.productName, odl.product_type_oh_bk, 
		count(*) over (partition by uco.new_magento_id) num_prod_cust, 
		rank() over (partition by uco.new_magento_id order by odl.product_id) rank_prod_cust
	from 
			#lensway_nl_usability_cust_orders uco
		inner join
			(select distinct odl.magento_order_id, odl.product_id, odl.productName, pp.product_type_oh_bk
			from 
					Landing.migra_lenswaynl.migrate_orderlinedata odl
				inner join
					Landing.aux.prod_product_product_type_oh pp on odl.product_id = pp.product_id) odl on uco.magento_order_id = odl.magento_order_id
	where odl.product_type_oh_bk in ('CL - Daily', 'CL - Two Weeklies', 'CL - Monthlies')
		and uco.created_at > '2017-04-01') t
where num_prod_cust = rank_prod_cust
order by last_product, created_at

