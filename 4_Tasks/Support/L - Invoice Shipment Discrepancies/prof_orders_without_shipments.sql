

select o.entity_id, o.increment_id, o.store_id, o.created_at,
	o.state, o.status, 
	o.base_grand_total
from 
		Landing.mag.sales_flat_order_aud o
	left join
		Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id
where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
	and s.entity_id is null
	and o.status not in ('canceled', 'closed', 'shipped')
order by o.created_at

select status, count(*), sum(base_grand_total)
from
	(select o.entity_id, o.increment_id, o.store_id, o.created_at,
		o.state, o.status, 
		o.base_grand_total
	from 
			Landing.mag.sales_flat_order_aud o
		left join
			Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id
	where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
		and s.entity_id is null) t
group by status
order by status


--------------------------------------------------------------

select o.entity_id, o.increment_id, o.store_id, o.created_at,
	o.state, o.status, 
	o.base_grand_total, 
	count(*) over (partition by o.status) num_status
from 
		Landing.mag.sales_flat_order_aud o
	left join
		Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id
	inner join
		DW_GetLenses_jbs.dbo.snap_data_york t1 on o.increment_id = t1.order_no
where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
	and s.entity_id is null
order by o.created_at


--------------------------------------------------------------

select o.entity_id, o.increment_id, o.store_id, o.created_at,
	o.state, o.status, 
	o.base_grand_total, 
	count(*) over (partition by o.status) num_status,
	t1.order_no, 
	t2.shipmentNumber, t2.status, t2.syncedToMagento
from 
		Landing.mag.sales_flat_order_aud o
	left join
		Landing.mag.sales_flat_shipment_aud s on o.entity_id = s.order_id
	left join
		DW_GetLenses_jbs.dbo.snap_data_york t1 on o.increment_id = t1.order_no
	left join
		DW_GetLenses_jbs.dbo.erp_shipments t2 on o.increment_id = t2.orderIncrementID
where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
	and o.status = 'shipped'
	and s.entity_id is null
	--and t1.order_no is not null
order by o.created_at