
select entity_id, increment_id, store_id, created_at, status, 
	base_grand_total, base_to_global_rate
from Landing.mag.sales_flat_order_aud
where created_at > '2017-04-01' and created_at < '2017-05-01'
order by created_at

select convert(date, created_at), status, 
	count(*), total_count, count(*)*100/convert(decimal(10,2), total_count) perc_total_count,
	sum(base_grand_total * base_to_global_rate), total_sum, sum(base_grand_total * base_to_global_rate)*100/convert(decimal(10,2), total_sum) perc_total_sum
from 
	(select entity_id, increment_id, store_id, created_at, status, 
		base_grand_total, base_to_global_rate, 
		count(*) over (partition by convert(date, created_at)) total_count,
		sum(base_grand_total * base_to_global_rate) over (partition by convert(date, created_at)) total_sum
	from Landing.mag.sales_flat_order_aud
	-- where created_at > '2017-04-01' and created_at < '2017-05-01'
	where created_at > '2017-05-01' -- and created_at < '2017-05-01'
	) t
where status = 'shipped'
group by convert(date, created_at), status, total_count, total_sum
-- order by status, convert(date, created_at)
order by convert(date, created_at), status

----------------------------------------------------------------

select s.entity_id, s.increment_id, s.order_id, s.store_id, s.created_at, 
	o.entity_id, o.increment_id, o.store_id, o.created_at, o.status, 
	o.base_grand_total, o.base_to_global_rate
from 
		Landing.mag.sales_flat_shipment_aud s
	inner join
		Landing.mag.sales_flat_order_aud o on s.order_id = o.entity_id
where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'

select convert(date, created_at), status, count(distinct entity_id), count(*)
from
	(select -- s.entity_id, s.increment_id, s.order_id, s.store_id, s.created_at, 
		o.entity_id, o.increment_id, o.store_id, o.created_at, o.status, 
		o.base_grand_total, o.base_to_global_rate, 
		s.created_at created_at_s
	from 
			Landing.mag.sales_flat_shipment_aud s
		inner join
			Landing.mag.sales_flat_order_aud o on s.order_id = o.entity_id
	--where o.created_at > '2017-04-01' and o.created_at < '2017-05-01'
	where o.created_at > '2017-05-01' --and o.created_at < '2017-05-01'
	) t
where status = 'shipped'
	--and created_at_s < '2017-05-01'
group by convert(date, created_at), status
-- order by status, convert(date, created_at)
order by convert(date, created_at), status

