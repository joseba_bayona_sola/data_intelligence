
select order_id, order_no, store_name, document_date, status, 
	global_total_inc_vat 
from DW_GetLenses.dbo.order_headers
where document_date > '2017-04-01' and document_date < '2017-05-01'
	and document_type = 'ORDER'
order by document_date

select order_id, order_no, store_name, document_date, status, 
	global_total_inc_vat, 
	count(*) over (partition by convert(date, document_date)) total_count,
	sum(global_total_inc_vat) over (partition by convert(date, document_date)) total_sum
from DW_GetLenses.dbo.order_headers
where document_date > '2017-04-01' and document_date < '2017-05-01'
	and document_type = 'ORDER'

select convert(date, document_date), status, 
	count(*), total_count, count(*)*100/convert(decimal(10,2), total_count) perc_total_count,
	sum(global_total_inc_vat), sum(global_total_exc_vat), total_sum, sum(global_total_inc_vat)*100/convert(decimal(10,2), total_sum) perc_total_sum
from
	(select order_id, order_no, store_name, document_date, status, 
		global_total_inc_vat, global_total_exc_vat, 
		count(*) over (partition by convert(date, document_date)) total_count,
		sum(global_total_inc_vat) over (partition by convert(date, document_date)) total_sum
	from DW_GetLenses.dbo.order_headers
	--where document_date > '2017-04-01' and document_date < '2017-05-01'
	where document_date > '2017-05-01' --and document_date < '2017-05-01'
		and document_type = 'ORDER') t
-- where status = 'shipped'
group by convert(date, document_date), status, total_count, total_sum
order by convert(date, document_date), status

----------------------------------------------------------------

select order_id, order_no, store_name, order_date, document_date, document_type, status, 
	global_total_inc_vat, 
	count(*) over (partition by convert(date, order_date)) total_count,
	sum(global_total_inc_vat) over (partition by convert(date, order_date)) total_sum	 
from DW_GetLenses.dbo.shipment_headers
where order_date > '2017-04-01' and document_date < '2017-05-01'
	and document_type = 'SHIPMENT'

select convert(date, order_date), status, 
	count(distinct order_id), count(*), total_count, count(*)*100/convert(decimal(10,2), total_count) perc_total_count,
	sum(global_total_inc_vat), sum(global_total_exc_vat), total_sum, sum(global_total_inc_vat)*100/convert(decimal(10,2), total_sum) perc_total_sum, 
	count(distinct order_id)
from
	(select order_id, order_no, store_name, order_date, document_date, document_type, status, 
		global_total_inc_vat, global_total_exc_vat, 
		count(*) over (partition by convert(date, order_date)) total_count,
		sum(global_total_inc_vat) over (partition by convert(date, order_date)) total_sum	 
	from DW_GetLenses.dbo.shipment_headers
	--where order_date > '2017-04-01' and order_date < '2017-05-01'
	where order_date > '2017-05-01' --and order_date < '2017-05-01'
		and document_type = 'SHIPMENT') t
where status = 'shipped'
group by convert(date, order_date), status, total_count, total_sum
order by convert(date, order_date), status