
select store_name, invoice_id, invoice_no, order_id, document_date, 
	global_total_inc_vat, global_total_exc_vat, 
	count(*) over (partition by order_id) num_inv_rep
from DW_GetLenses.dbo.invoice_headers
where document_date between '2017-04-01' and '2017-05-01'

select * , sum(global_total_exc_vat) over () /2
from
	(select store_name, invoice_id, invoice_no, order_id, document_date, document_type,
		global_total_inc_vat, global_total_exc_vat, 
		count(*) over (partition by order_id) num_inv_rep
	from DW_GetLenses.dbo.invoice_headers
	where document_date between '2017-04-01' and '2017-05-01'
		and document_type = 'INVOICE') t
where num_inv_rep > 1
order by num_inv_rep desc, order_id, invoice_id


select store_name, invoice_id, invoice_no, order_id, document_date, 
	global_total_inc_vat, global_total_exc_vat, 
	count(*) over (partition by order_id) num_inv_rep
from DW_GetLenses.dbo.invoice_headers
where order_id in (5283593, 4959167, 5279957, 5282163, 5279682)