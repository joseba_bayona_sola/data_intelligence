
-- Checking for records from different months

select 
	YEAR(ih.order_date) o_yyyy, MONTH(ih.order_date) o_mm,
	YEAR(fi.document_date) d_yyyy, MONTH(fi.document_date) d_mm,
	count(*)
from 
		DW_Sales_Actual.dbo.Fact_Invoices fi
	inner join 
		DW_Sales_Actual.dbo.Dim_Invoice_Headers ih on fi.uniqueID = ih.uniqueID
where store_name = 'visiondirect.fr'
	and ih.order_date > '2016-08-01'
group by YEAR(ih.order_date), MONTH(ih.order_date), YEAR(fi.document_date), MONTH(fi.document_date) 
order by o_yyyy, o_mm, d_yyyy, d_mm


select ih.order_date, fi.document_date, fi.document_type, ih.business_channel, ih.coupon_code, 
	fi.global_discount_exc_vat
from 
		DW_Sales_Actual.dbo.Fact_Invoices fi
	inner join 
		DW_Sales_Actual.dbo.Dim_Invoice_Headers ih on fi.uniqueID = ih.uniqueID
where store_name = 'visiondirect.fr'
	and ih.order_date > '2016-08-01'
	and YEAR(ih.order_date) = YEAR(fi.document_date) and MONTH(ih.order_date) <> MONTH(fi.document_date) 
order by ih.order_date

------------------------------------------------------------------------------

select YEAR(fi.document_date) d_yyyy, MONTH(fi.document_date) d_mm, ih.business_channel,
	sum(fi.global_discount_exc_vat)
from 
		DW_Sales_Actual.dbo.Fact_Invoices fi
	inner join 
		DW_Sales_Actual.dbo.Dim_Invoice_Headers ih on fi.uniqueID = ih.uniqueID
where store_name = 'visiondirect.fr'
	and ih.order_date > '2016-08-31'
group by YEAR(fi.document_date), MONTH(fi.document_date), ih.business_channel
order by d_yyyy, d_mm, sum(fi.global_discount_exc_vat) 

select ih.order_date, fi.document_date, fi.document_type, ih.business_channel, ih.coupon_code, 
	fi.sku, 
	fi.global_discount_exc_vat
from 
		DW_Sales_Actual.dbo.Fact_Invoices fi
	inner join 
		DW_Sales_Actual.dbo.Dim_Invoice_Headers ih on fi.uniqueID = ih.uniqueID
where store_name = 'visiondirect.fr'
	and YEAR(fi.document_date) = 2016 and MONTH(fi.document_date) = 9 and ih.business_channel = 'Email'
	and fi.global_discount_exc_vat <> 0
order by ih.order_date

select ih.order_date, fi.document_date, fi.document_type, ih.business_channel, ih.customer_business_channel,
	ih.coupon_code, 
	fi.sku, 
	fi.global_discount_exc_vat
from 
		DW_Sales_Actual.dbo.Fact_Invoices fi
	inner join 
		DW_Sales_Actual.dbo.Dim_Invoice_Headers ih on fi.uniqueID = ih.uniqueID
where store_name = 'visiondirect.fr'
	--and YEAR(fi.document_date) = 2016 and MONTH(fi.document_date) = 9 and ih.business_channel = 'Email'
	and ih.order_date > '2016-09-01' 
		and ih.business_channel = 'Email' -- business_channel = 'Email'
	and fi.global_discount_exc_vat <> 0
order by ih.order_date, ih.order_no, fi.document_type

