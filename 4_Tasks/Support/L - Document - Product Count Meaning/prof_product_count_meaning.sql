
select line_id, order_id, document_id, order_no, store_name, document_date, 
	product_type, product_id, name, 
	qty, local_line_subtotal_vat, local_line_total_vat
from DW_GetLenses.dbo.order_lines
where order_id in 
	(select distinct order_id
	from DW_GetLenses.dbo.order_lines
	where product_id = 2318 and document_date > getutcdate() - 100)
and order_no in ('8002868820', '8002869303')
order by order_id desc, product_id, line_id

------------------------------------------------------------------

select top 1000 *
from dw_getlenses.dbo.dw_order_line_sum
where document_id in (5255679, 5256326)

select top 1000 *
from dw_getlenses.dbo.dw_order_line_product_sum
where document_id in (5255679, 5256326)

------------------------------------------------------------------

select top 1000 UniqueID, order_no, order_date	
from DW_Sales_Actual.dbo.Dim_Order_Headers
where order_no in ('8002868820', '8002869303')

select top 1000 UniqueID, order_no, store_name, customer_id, 
	product_id, UniqueIDQP, sku_for_parameters,
	qty, local_line_subtotal_vat, local_line_total_vat, 
	document_count, product_count, line_count
from DW_Sales_Actual.dbo.Fact_Orders
where uniqueID in ('OOR5255684', 'OOR5256326')
order by order_no, product_id
