
select order_id_bk, order_no, -- order_date, 
	invoice_date_c invoice_date, convert(date, shipment_date) shipment_date, -- shipment_date_c shipment_date, -- refund_date, 
	line_status_name, 
	c.first_name + ' ' + c.last_name customer, -- customer_id, customer_email, 
	-- store_name, 
	product_family_code, product_family_name, glass_package_type_name, -- glass_vision_type_name, 
	coupon_code, 
	qty_unit, 
	price_type_name, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Warehouse.gen.dim_customer_scd c on ol.customer_id = c.customer_id_bk and c.isCurrent = 'Y'
where product_type_name = 'GLASSES' and product_family_name not in ('Prescription Swimming Goggles', 'Junior Swimming Goggles')
	and invoice_date is not null 
	and ol.store_name <> 'lensway.co.uk'
order by invoice_date, order_no

select order_id_bk, order_no, -- order_date, invoice_date, refund_date, 
	shipment_date_c, 
	line_status_name, 
	c.first_name + ' ' + c.last_name customer, -- customer_id, customer_email, 
	-- store_name, 
	product_family_code, product_family_name, glass_package_type_name, -- glass_vision_type_name, 
	coupon_code, 
	qty_unit, 
	price_type_name, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat
from
		Warehouse.sales.fact_order_line_v ol
	inner join
		Warehouse.gen.dim_customer_scd c on ol.customer_id = c.customer_id_bk and c.isCurrent = 'Y'
where product_type_name = 'GLASSES' and product_family_name not in ('Prescription Swimming Goggles', 'Junior Swimming Goggles')
	and shipment_date is not null
	and ol.store_name <> 'lensway.co.uk'
order by shipment_date, order_no
