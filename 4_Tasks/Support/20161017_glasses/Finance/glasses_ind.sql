
			select il.document_id, il.document_type, il.document_date, il.invoice_date,
				ih.order_no, ih.order_date, 
				il.product_id, il.sku, il.name, 
				--ih.local_subtotal_inc_vat, ih.local_total_inc_vat,
				il.local_line_total_exc_vat, il.local_line_subtotal_exc_vat, il.local_discount_exc_vat, il.local_shipping_exc_vat, il.local_store_credit_exc_vat, 
				il.local_margin_amount
			from 
					DW_GetLenses.dbo.invoice_lines il
				inner join 
					DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
			where il.document_date > '2016-10-13' and 
						il.product_id = 2707
			order by order_no, document_date