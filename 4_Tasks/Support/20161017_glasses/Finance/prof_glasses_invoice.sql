select t.order_no, convert(date, t.invoice_date) date, convert(date, s.shipment_date) shipment_date,
	t.customer_firstname + ' ' + t.customer_lastname customer,
	t.sku, t.name frame_name, t.name_pkg package_name, t.customer_order_seq_no, t.coupon_code, 
	--t.local_subtotal_inc_vat order_total, t.local_total_inc_vat order_total_discount,
	t.local_line_total_exc_vat frame_total_all, t.local_line_total_exc_vat_pkg package_total_all, 
	t.local_line_subtotal_exc_vat frame_total, t.local_line_subtotal_exc_vat_pkg package_total, 
	t.local_shipping_exc_vat frame_shipping, t.local_shipping_exc_vat_pkg package_shipping, 
	t.local_discount_exc_vat frame_discount, t.local_discount_exc_vat_pkg package_discount, 
	t.local_store_credit_exc_vat frame_store_credit, t.local_store_credit_exc_vat_pkg package_store_credit 
from 
	(select 
		count(*) over (partition by t1.order_no) num_rep_no,
		count(*) over (partition by t1.document_id, t1.document_type) num_rep,
		t1.document_id, t1.document_type, t1.document_date, t1.invoice_date,
		t1.order_no, t1.order_date, 
		t1.customer_id, t1.customer_firstname, t1.customer_lastname, t1.customer_order_seq_no, t1.coupon_code,
		t1.product_id product_id_pkg, t1.sku sku_pkg, t1.name name_pkg, 
		t1.local_line_total_exc_vat local_line_total_exc_vat_pkg, t1.local_line_subtotal_exc_vat local_line_subtotal_exc_vat_pkg, t1.local_discount_exc_vat local_discount_exc_vat_pkg,
		t1.local_shipping_exc_vat local_shipping_exc_vat_pkg, t1.local_store_credit_exc_vat local_store_credit_exc_vat_pkg,
		t2.product_id, t2.sku, t2.name, 
		--t2.local_subtotal_inc_vat, t2.local_total_inc_vat,
		t2.local_line_total_exc_vat, t2.local_line_subtotal_exc_vat, t2.local_discount_exc_vat, t2.local_shipping_exc_vat, t2.local_store_credit_exc_vat
	from 
			(select il.document_id, il.document_type, il.document_date, il.invoice_date,
				ih.order_no, ih.order_date, 
				ih.customer_id, ih.customer_firstname, ih.customer_lastname, ih.customer_order_seq_no, ih.coupon_code,
				il.product_id, il.sku, il.name, 
				il.local_line_total_exc_vat, il.local_line_subtotal_exc_vat, il.local_discount_exc_vat, il.local_shipping_exc_vat, il.local_store_credit_exc_vat
			from 
					DW_GetLenses.dbo.invoice_lines il
				inner join 
					DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
			where il.document_date > '2016-10-13' and
				il.product_type = 'Other' and 
				il.product_id in (2971, 2972, 2973)) t1
		inner join
			(select il.document_id, il.document_type, il.document_date, il.invoice_date,
				ih.order_no, ih.order_date, 
				il.product_id, il.sku, il.name, 
				--ih.local_subtotal_inc_vat, ih.local_total_inc_vat,
				il.local_line_total_exc_vat, il.local_line_subtotal_exc_vat, il.local_discount_exc_vat, il.local_shipping_exc_vat, il.local_store_credit_exc_vat
			from 
					DW_GetLenses.dbo.invoice_lines il
				inner join 
					DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
			where il.document_date > '2016-10-13' and 
						il.category_id = 541) t2 
						on t1.order_no = t2.order_no and t1.document_type = t2.document_type and t1.product_id <> t2.product_id and t2.product_id not in (2968, 2969)) t
		left join
			--(select document_id, document_type, document_date, shipment_date, 
			--	order_no
			--from DW_GetLenses.dbo.shipment_headers) s on t.order_no = s.order_no and s.document_type = 'SHIPMENT'
			(select distinct sh.document_id, sh.document_type, sh.document_date, sh.shipment_date, sh.order_no
			from 
					DW_GetLenses.dbo.shipment_headers sh
				inner join 
					DW_GetLenses.dbo.shipment_lines sl on sl.document_id = sh.document_id and sl.document_type = sh.document_type
			where sl.product_type = 'Glasses') s 
						on t.order_no = s.order_no and s.document_type = 'SHIPMENT' 
--where num_rep_no = 1 and num_rep = 1
--where num_rep_no = 4 and num_rep = 4
where num_rep_no > 4 and num_rep > 4
order by t.order_no, t.document_date, t.sku


