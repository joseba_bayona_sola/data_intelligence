
select * 
from DW_GetLenses.dbo.invoice_lines
where document_date > '2016-10-13' and 
	product_type = 'Other'

select product_id, sku, name, count(*) 
from DW_GetLenses.dbo.invoice_lines
where document_date > '2016-10-13' and 
	product_type = 'Other'
group by product_id, sku, name
order by product_id, sku, name

-----------------------------------------------------------------------

-- Other: Packages: Silver - Gold - Platinum
select il.document_id, il.document_type, il.document_date, il.invoice_date,
	ih.order_no, ih.order_date, 
	ih.customer_id, ih.customer_firstname, ih.customer_lastname, ih.customer_order_seq_no, ih.coupon_code,
	il.product_id, il.sku, il.name, 
	il.local_line_total_inc_vat, il.local_discount_inc_vat
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join 
		DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
where il.document_date > '2016-10-13' and 
	il.product_type = 'Other' and 
	il.product_id in (2971, 2972, 2973)
order by ih.order_no, il.document_date

	select il.document_id, il.document_type, il.document_date, il.invoice_date,
		ih.order_no, ih.order_date, 
		ih.customer_id, ih.customer_firstname, ih.customer_lastname, ih.customer_order_seq_no, ih.coupon_code,
		il.product_id, il.sku, il.name, 
		il.local_line_total_inc_vat, il.local_discount_inc_vat
	from 
			DW_GetLenses.dbo.invoice_lines il
		inner join 
			DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
	where il.document_date > '2016-10-13' and 
		il.product_type = 'Other' and 
		ih.order_no = '8002308926'
	order by ih.order_no, il.document_date

-- Other: Distance - Reading Vision
select il.document_id, il.document_type, il.document_date, il.invoice_date,
	ih.order_no, ih.order_date, 
	ih.customer_id, ih.customer_firstname, ih.customer_lastname, ih.customer_order_seq_no, ih.coupon_code,
	il.product_id, il.sku, il.name, 
	il.local_line_total_inc_vat, il.local_discount_inc_vat
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join 
		DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
where il.document_date > '2016-10-13' and 
	il.product_type = 'Other' and 
	il.product_id in (2968, 2969)
order by ih.order_no, il.document_date	

-- Other: Non Prescription
select il.document_id, il.document_type, il.document_date, il.invoice_date,
	ih.order_no, ih.order_date, 
	ih.customer_id, ih.customer_firstname, ih.customer_lastname, ih.customer_order_seq_no, ih.coupon_code,
	il.product_id, il.sku, il.name, 
	il.local_line_total_inc_vat, il.local_discount_inc_vat
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join 
		DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type
where il.document_date > '2016-10-13' and 
	il.product_type = 'Other' and 
	il.product_id in (2974, 2977, 2978)
order by ih.order_no, il.document_date	


