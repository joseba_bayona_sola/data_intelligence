select top 1000 
	line_id, 
	order_id, order_no, store_name, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from DW_GetLenses.dbo.order_lines
where --order_no = '8002311390' --(product_id = 2792 or order_id = 4503067)
	--product_id = 2694 and 
	order_id = 4509294
	and store_name = 'visiondirect.co.uk'
	and order_date > '2016-10-11'
order by order_no desc;

select *
from DW_GetLenses.dbo.order_headers
where order_id = 4503067