
select count(*)
from Warehouse.gen.dim_customer_v

select top 1000 count(*) over () num_tot,
	idCustomer_sk_fk, customer_id_bk, 
	customer_email, store_name, 
	reminder_f, reminder_type_name, reorder_f
from Warehouse.gen.dim_customer_scd_v
where reminder_f = 'Y' or reorder_f = 'Y' 

	select top 1000 reminder_type_name, count(*) 
	from Warehouse.gen.dim_customer_scd_v
	where reminder_f = 'Y' 
	group by reminder_type_name
	order by reminder_type_name

select top 1000 order_id_bk, order_no, order_date, 
	store_name, customer_id, customer_email, 
	reminder_type_name, reminder_date, 
	reorder_f, reorder_date, 
	product_id_magento, product_family_name, base_curve, 
	qty_unit, qty_pack
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1129
	and reminder_type_name in ('BOTH', 'EMAIL', 'SMS')
	and base_curve = '8.4'
	and qty_unit in (3,9,15,21)
	and reminder_date > GETUTCDATE()
order by reminder_date desc

	select distinct ol.order_no, ol.store_name, ol.customer_id, ol.customer_email, c.first_name, c.last_name,
		ol.reminder_type_name, ol.reminder_date, ol.qty_unit
	from 
			Warehouse.sales.fact_order_line_v ol
		inner join
			Warehouse.gen.dim_customer_scd_v c on ol.customer_id = c.customer_id_bk
	where ol.product_id_magento = 1129
		and ol.reminder_type_name in ('BOTH', 'EMAIL', 'SMS')
		and ol.base_curve = '8.4'
		and ol.qty_unit in (3,9,15,21)
		and ol.reminder_date > GETUTCDATE()
	order by ol.reminder_date 

select top 1000 order_id_bk, order_no, order_date, 
	store_name, customer_id, customer_email, 
	reminder_type_name, reminder_date, 
	reorder_f, reorder_date, 
	product_id_magento, product_family_name, base_curve, 
	qty_unit, qty_pack
from Warehouse.sales.fact_order_line_v
where product_id_magento = 1129
	and reorder_f = 'Y'
	and base_curve = '8.4'
	and qty_unit in (3,9,15,21)
order by reminder_date desc