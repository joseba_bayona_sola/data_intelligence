
select oi.old_item_id, oi.old_order_id, o.created_at, 
  oi.old_product_id, oi.product_id, oi.productName, oi.row_total, oi.quantity, oi.packsize, 
  o.base_grand_total, o.base_shipping_amount, o.base_discount_amount
from 
    migrate_orderlinedata oi
  inner join  
    (select old_order_id, created_at, old_customer_id, 
      base_grand_total, base_shipping_amount, base_discount_amount
    from migrate_orderdata) o on oi.old_order_id = o.old_order_id
where ProductName like 'Splash%'
  and o.created_at between curdate() - interval 60 day and curdate()
order by o.created_at desc
limit 1000


select oi.old_product_id, oi.product_id, oi.productName, oi.quantity, oi.packsize, oi.row_total, oi.row_total / oi.packsize, 
  count(*)
from 
    migrate_orderlinedata oi
  inner join  
    (select old_order_id, created_at, old_customer_id, 
      base_grand_total, base_shipping_amount, base_discount_amount
    from migrate_orderdata) o on oi.old_order_id = o.old_order_id
where ProductName like 'Splash%'
  and o.created_at between curdate() - interval 60 day and curdate()
group by oi.old_product_id, oi.product_id, oi.productName, oi.quantity, oi.packsize, oi.row_total
order by oi.old_product_id, oi.product_id, oi.productName, oi.quantity, oi.packsize, oi.row_total
limit 1000


