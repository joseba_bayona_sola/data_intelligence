﻿select
p.name as [product_name],
p.product_id as [product_id],
char(34)+REPLACE(replace(p.[short_description],char(13),''),char(10),'')+char(34) as [description],
'Contact lenses' as [merchant_category],
'http://www.'+p.store_name+'/'+url_key as [deep_link],
'http://www.'+p.store_name+'/media/product-images/g_'+cast(p.product_id as varchar(50))+'.jpg' as [image_url],
FORMAT(pp.price,'#0.00') as [price],
'EN' as [language]

from 
products p
left outer join (
select 
[entity_id] as product_id,
store_name,
qty,
[value] as unit_price,
(qty*value) as price
from
(select 
[entity_id]
,website_id
,qty
,[value]
,row_number() over(partition by website_id,[entity_id] order by qty) rno 
from openquery(MAGENTO,'select * from magento01.catalog_product_entity_tier_price where promo_key=''''')) rs
inner join v_stores_all store on store.store_id=rs.website_id
where rs.rno=1 ) pp on pp.product_id=p.product_id and p.store_name=pp.store_name
where 
p.store_name in ('visiondirect.co.uk')
and p.status=1
and isnull(p.product_lifecycle,'')<>'discontinued'
and p.product_type like 'contact lenses%'
and isnull(pp.price,0)>0
AND p.telesales_only=0