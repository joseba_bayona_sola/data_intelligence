﻿select
p.product_id as [prod_number],
p.name as [prod_name],
FORMAT(pp.price,'#0.00') as [prod_price],
'EUR' AS [currency_symbol],
'http://www.'+p.store_name+'/'+url_key as [prod_url],
CASE 
WHEN p.product_type like 'contact lenses%' THEN 'Lentes de Contacto'
when p.product_type LIKE 'Solutions & Eye Care%' THEN 'Líquidos y cuidado de ojos'
END AS [category],
char(34)+REPLACE(replace(p.[short_description],char(13),''),char(10),'')+char(34) as [prod_description_long],
'http://www.'+p.store_name+'/media/product-images/Zanox-product-images-100x100/'+cast(p.product_id as varchar(50))+'.jpg' as [img_small],
'http://www.'+p.store_name+'/media/product-images/landing-pages/'+cast(p.product_id as varchar(50))+'.jpg' AS [img_medium],
'http://www.'+p.store_name+'/media/product-images/landing-pages/'+cast(p.product_id as varchar(50))+'.jpg' AS [img_large],
p.manufacturer as [manufacturer],
pdt.despatch_time AS [delivery_time]

from 
products p
LEFT OUTER JOIN 
(SELECT product_id,MIN([Hours]/24) AS despatch_time  FROM product_despatch_times GROUP BY product_id) pdt ON pdt.product_id=p.product_id
LEFT outer join (
select 
[entity_id] as product_id,
store_name,
qty,
[value] as unit_price,
(qty*value) as price
from
(select 
[entity_id]
,website_id
,qty
,[value]
,row_number() over(partition by website_id,[entity_id] order by qty) rno 
from openquery(MAGENTO,'select * from magento01.catalog_product_entity_tier_price where promo_key=''''')) rs
inner join v_stores_all store on store.store_id=rs.website_id
where rs.rno=1 ) pp on pp.product_id=p.product_id and p.store_name=pp.store_name
where 
p.store_name in ('visiondirect.es')
and p.status=1
and isnull(p.product_lifecycle,'')<>'discontinued'
and (p.product_type like 'contact lenses%' or p.product_type like 'Solutions & Eye Care%')
and isnull(pp.price,0)>0
AND p.telesales_only=0