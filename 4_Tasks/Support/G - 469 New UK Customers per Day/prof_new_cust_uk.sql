
select top 1000 
	store_name, order_id, order_no, document_type, document_date, convert(date, document_date), 
	customer_id, order_lifecycle, shipping_country_id
from DW_GetLenses.dbo.order_headers
where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl')
	and shipping_country_id = 'GB'

select dd, count(*) num_new_orders
from
	(select 
		store_name, order_id, order_no, document_type, document_date, convert(date, document_date) dd, 
		customer_id, order_lifecycle, shipping_country_id
	from DW_GetLenses.dbo.order_headers
	where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl')
		and shipping_country_id = 'GB'
		and document_type = 'ORDER') t
where dd >= '2012-01-01'
group by dd
order by dd

select dd, count(*) num_new_orders
from
	(select 
		store_name, order_id, order_no, document_type, document_date, convert(date, document_date) dd, 
		customer_id, order_lifecycle, shipping_country_id
	from DW_GetLenses.dbo.order_headers
	where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl')
		and shipping_country_id = 'GB'
		and document_type = 'ORDER'
		and order_lifecycle = 'New') t
where dd >= '2012-01-01'
group by dd
order by dd

---------------------

select t1.dd, t2.dd, t1.num_orders, t2.num_new_orders, 
	t2.num_new_orders*100/convert(decimal(10,2), t1.num_orders) perc_num_tot
from
		(select dd, count(*) num_orders
		from
			(select 
				store_name, order_id, order_no, document_type, document_date, convert(date, document_date) dd, 
				customer_id, order_lifecycle, shipping_country_id
			from DW_GetLenses.dbo.order_headers
			where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl')
				and shipping_country_id = 'GB'
				and document_type = 'ORDER') t
		where dd >= '2012-01-01'
		group by dd) t1
	full join
		(select dd, count(*) num_new_orders
		from
			(select 
				store_name, order_id, order_no, document_type, document_date, convert(date, document_date) dd, 
				customer_id, order_lifecycle, shipping_country_id
			from DW_GetLenses.dbo.order_headers
			where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl')
				and shipping_country_id = 'GB'
				and document_type = 'ORDER'
				and order_lifecycle = 'New') t
		where dd >= '2012-01-01'
		group by dd) t2 on t1.dd = t2.dd
order by t1.dd

