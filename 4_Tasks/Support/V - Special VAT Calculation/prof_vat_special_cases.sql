

select countries_registered_code, count(*)
from Warehouse.sales.dim_order_header
group by countries_registered_code
order by countries_registered_code


-- Current Cases where countries_registered_code set to NULL (Loaded before VAT Logic was implemented - Not Reload done (Previous 2015))
select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code is null
order by order_date

select top 1000 oh.*
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.map.vat_countries_registered_store crs on oh.store_id_bk = crs.store_id

select top 1000 oh.order_source, oh.countries_registered_code, count(*)
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.map.vat_countries_registered_store crs on oh.store_id_bk = crs.store_id
group by oh.order_source, oh.countries_registered_code
order by oh.order_source, oh.countries_registered_code


-- EU Non Registered Orders
select country_id_shipping_bk, count(*)
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code = 'EU-ZZ'
group by country_id_shipping_bk
order by country_id_shipping_bk

select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code = 'EU-ZZ'
	and vat_percent = 0
order by order_date


-- Special Irish Case -- What happened
select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where countries_registered_code = 'EU-ZZ'
	AND country_id_shipping_bk = 'IE'



-- ES Orders and VAT Registration Code
select top 1000 countries_registered_code, count(*)
from Landing.aux.sales_dim_order_header_aud
where country_id_shipping_bk = 'ES'
group by countries_registered_code
order by countries_registered_code
