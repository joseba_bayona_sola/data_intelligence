
--- mag.lnd_stg_get_aux_sales_order_line_vat: 

	-- Temp Table #sales_order_line_vat: Add Allocation Rate (order_allocation_rate - order_allocation_rate_refund)

	create table #sales_order_line_vat(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL, 
		countries_registered_code		varchar(10) NOT NULL,
		product_type_vat				varchar(255),
		vat_type_rate_code				char(1), 
		invoice_date					datetime,
	
		vat_rate						decimal(12, 4),
		prof_fee_rate					decimal(12, 4),

		local_total_inc_vat				decimal(12, 4),
		local_total_exc_vat				decimal(12, 4),
		local_total_vat					decimal(12, 4),
		local_total_prof_fee			decimal(12, 4),

		local_total_aft_refund_inc_vat	decimal(12, 4),
		local_total_aft_refund_exc_vat	decimal(12, 4),
		local_total_aft_refund_vat		decimal(12, 4),
		local_total_aft_refund_prof_fee	decimal(12, 4),
		
		order_allocation_rate			decimal(12, 4))

	-- Insert Values: Add order_allocation_rate
	insert into #sales_order_line_vat(order_line_id_bk, order_id_bk, 
		countries_registered_code, invoice_date, 
		local_total_inc_vat, local_total_aft_refund_inc_vat, 
		order_allocation_rate)

		select ol.order_line_id_bk, ol.order_id_bk, 
			oh_v.countries_registered_code, oh_v.invoice_date, 
			o_m.local_total_inc_vat, o_m.local_total_aft_refund_inc_vat, 
			o_m.order_allocation_rate
		from 
				#sales_order_header_vat oh_v
			inner join
				Landing.aux.sales_order_line_o_i_s_cr ol on oh_v.order_id_bk = ol.order_id_bk
			inner join
				Landing.aux.sales_order_line_measures o_m on ol.order_line_id_bk = o_m.order_line_id_bk

	-- Set Product Type VAT: OK

	-- Set VAT Values: 2 ways depending countries_registered_code - prof_fee_fixed_value
		-- New: No Prof Fee Rate is set up
			merge into #sales_order_line_vat trg
			using 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					ol_v.countries_registered_code, ol_v.product_type_vat, ol_v.invoice_date,
					vr.vat_type_rate_code, 
					vr.vat_rate, pff.prof_fee_fixed_value
				from 
						(select order_line_id_bk, order_id_bk, 
							ol.countries_registered_code, 
							ol.countries_registered_code countries_registered_code_pf,
							product_type_vat, invoice_date, 
							local_total_inc_vat
						from 
								#sales_order_line_vat ol
							inner join
								(select distinct countries_registered_code
								from Landing.map.vat_prof_fee_fixed_value) pff on ol.countries_registered_code = pff.countries_registered_code) ol_v
					inner join
						Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
							and ol_v.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
					inner join
						Landing.map.vat_prof_fee_fixed_value_aud pff on ol_v.countries_registered_code_pf = pff.countries_registered_code 
							and ol_v.local_total_inc_vat between pff.order_value_min and pff.order_value_max) src
			on trg.order_line_id_bk = src.order_line_id_bk
			when matched then 
				update set
					trg.vat_type_rate_code = src.vat_type_rate_code, 
					trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_fixed_value;

		-- Normal: As always
			merge into #sales_order_line_vat trg
			using 
				(select ol_v.order_line_id_bk, ol_v.order_id_bk, 
					ol_v.countries_registered_code, ol_v.product_type_vat, ol_v.invoice_date,
					vr.vat_type_rate_code, 
					vr.vat_rate, pfr.prof_fee_rate
				from 
						(select order_line_id_bk, order_id_bk, 
							ol.countries_registered_code, 
							ol.countries_registered_code countries_registered_code_pf,
							product_type_vat, invoice_date
						from 
								#sales_order_line_vat ol
							left join
								(select distinct countries_registered_code
								from Landing.map.vat_prof_fee_fixed_value) pff on ol.countries_registered_code = pff.countries_registered_code
						where pff.countries_registered_code is null) ol_v
					inner join
						Landing.map.vat_vat_rate_aud vr on ol_v.countries_registered_code = vr.countries_registered_code and ol_v.product_type_vat = vr.product_type_vat 
							and ol_v.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
					inner join
						Landing.map.vat_prof_fee_rate_aud pfr on ol_v.countries_registered_code_pf = pfr.countries_registered_code and ol_v.product_type_vat = pfr.product_type_vat 
							and ol_v.invoice_date between pfr.vat_time_period_start and pfr.vat_time_period_finish) src
			on trg.order_line_id_bk = src.order_line_id_bk
			when matched then 
				update set
					trg.vat_type_rate_code = src.vat_type_rate_code, 
					trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate;

	-- Calculate Prof Fee Values: 2 ways depending countries_registered_code - prof_fee_fixed_value
		-- New: local_total_prof_fee and local_total_aft_refund_prof_fee with fixed values depending on order value (multiply per rate)
			update #sales_order_line_vat
			set 
				local_total_prof_fee = prof_fee_rate * order_allocation_rate, 
				local_total_aft_refund_prof_fee = prof_fee_rate * order_allocation_rate
			where countries_registered_code in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)

			update #sales_order_line_vat
			set 
				prof_fee_rate = 0
			where countries_registered_code in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)
		
		-- Normal: As always
			update #sales_order_line_vat
			set 
				local_total_prof_fee = local_total_inc_vat * (prof_fee_rate / 100), 
				local_total_aft_refund_prof_fee = local_total_aft_refund_inc_vat * (prof_fee_rate / 100)
			where countries_registered_code not in
				(select distinct countries_registered_code
				from Landing.map.vat_prof_fee_fixed_value)

	-- Calculate VAT Values: OK

	-- DELETE - INSERT: OK