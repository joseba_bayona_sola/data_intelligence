
	select num_tot_rep_def, num_tot_rep, num_tot,
		document_type_1, document_type_2, 
		order_id_1, order_no_1, order_date_1, invoice_date, shipment_date, 
		order_id_2, order_no_2, order_date_2, 
		invoice_to_shipment,
		customer_id, customer_order_seq_no_1, customer_order_seq_no_2, 
		num_by_order, rank_by_order
	from DW_GetLenses_jbs.dbo.shipment_spain_res

	select num_tot_rep_def, num_tot_rep, num_tot,
		document_type_1, document_type_2, 
		order_id_1, order_no_1, order_date_1, invoice_date, shipment_date, 
		order_id_2, order_no_2, order_date_2, 
		invoice_to_shipment,
		customer_id, customer_order_seq_no_1, customer_order_seq_no_2, qty
		num_by_order, rank_by_order
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty

	-- 1: 
	select num_tot, qty, 
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty
	group by num_tot, qty
	order by num_tot, qty

	select num_tot, 
		qty, num_tot_qty,  num_tot_qty * 100 / convert(decimal(10,2), num_tot) perc_qty,
		invoice_to_shipment,
		count(*) num_invoice_to_shipment, count(*) * 100 / convert(decimal(10,2), num_tot_qty) perc_invoice_to_shipment
	from 
		(select count(*) over (partition by qty) num_tot_qty, *
		from DW_GetLenses_jbs.dbo.shipment_spain_res_qty) t
	group by num_tot, num_tot_qty, qty, invoice_to_shipment
	order by num_tot, qty, invoice_to_shipment

	-- 2:
	select num_tot, qty, year(order_date_1) yyyy, month(order_date_1) mm,
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty
	group by num_tot, qty, year(order_date_1), month(order_date_1)
	order by num_tot, qty, year(order_date_1), month(order_date_1)
