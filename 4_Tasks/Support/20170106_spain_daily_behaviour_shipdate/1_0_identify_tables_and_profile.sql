
-- Identify Tables + Attributes

select top 100 
	document_id, document_date, document_type,
	order_id, order_no, order_date, 
	customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where store_name = 'visiondirect.es'
	and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)

select top 100 line_id,
	document_id, document_date, document_type,
	order_id, order_no, order_date,
	product_type, product_id, name, qty
from DW_GetLenses.dbo.order_lines

-- 

select top 1000 
	document_id, document_date, document_type,
	shipment_id, shipment_no, shipment_date,
	invoice_id, invoice_no, invoice_date,
	order_id, order_no, order_date, 	
	length_of_time_to_invoice, length_of_time_invoice_to_this_shipment,
	customer_id, customer_order_seq_no
from DW_GetLenses.dbo.shipment_headers
where store_name = 'visiondirect.es'
	and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11);

select top 100 line_id,
	document_id, document_date, document_type,
	shipment_id, shipment_no, shipment_date,
	product_type, product_id, name, qty
from DW_GetLenses.dbo.shipment_lines

-------------------------------------------------------------------------------
-- Profile and Understand Data

	-- Document Type: OK (ORDER, INVOICE, SHIPMENT) - CANCEL - CREDITMEMO
	select document_type, count(*)
	from DW_GetLenses.dbo.order_headers
	where store_name = 'visiondirect.es'
		and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)
	group by document_type
	order by document_type

	select document_type, count(*)
	from DW_GetLenses.dbo.shipment_headers
	where store_name = 'visiondirect.es'
		and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)
	group by document_type
	order by document_type

	-- SHIPMENT: LENGTH_OF_TIME XXXX CORRECT?
	select top 1000 
		document_type,		
		order_date, invoice_date, shipment_date, 	
		datediff(day, order_date, invoice_date) order_to_invoice, datediff(day, invoice_date, shipment_date) invoice_to_shipment, 
		length_of_time_to_invoice, length_of_time_invoice_to_this_shipment
	from DW_GetLenses.dbo.shipment_headers
	where YEAR(order_date) = 2016
		--and document_type = 'SHIPMENT' and datediff(day, invoice_date, shipment_date) <> length_of_time_invoice_to_this_shipment;

	select document_type, o_i, i_s, count(*)
	from
		(select document_type,		
			order_date, invoice_date, shipment_date,
			order_to_invoice, length_of_time_to_invoice, 
				case when (order_to_invoice = length_of_time_to_invoice) then 1 else 0 end o_i,
			invoice_to_shipment, length_of_time_invoice_to_this_shipment, 
				case when (invoice_to_shipment = length_of_time_invoice_to_this_shipment) then 1 else 0 end i_s
		from
			(select 
				document_type,		
				order_date, invoice_date, shipment_date, 	
				datediff(day, order_date, invoice_date) order_to_invoice, datediff(day, invoice_date, shipment_date) invoice_to_shipment, 
				length_of_time_to_invoice, length_of_time_invoice_to_this_shipment
			from DW_GetLenses.dbo.shipment_headers
			where YEAR(order_date) = 2016) t) t
	group by document_type, o_i, i_s
	order by document_type, o_i, i_s

	-- SHIPMENT: SHIPMENT DAYS
	select num_tot, order_to_invoice, invoice_to_shipment, 
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from
		(select count(*) over () num_tot,
			document_type,		
			order_date, invoice_date, shipment_date, 	
			datediff(day, order_date, invoice_date) order_to_invoice, datediff(day, invoice_date, shipment_date) invoice_to_shipment, 
			length_of_time_to_invoice, length_of_time_invoice_to_this_shipment
		from DW_GetLenses.dbo.shipment_headers
		where store_name = 'visiondirect.es' and document_type = 'SHIPMENT'
			and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) t
	group by num_tot, order_to_invoice, invoice_to_shipment
	order by num_tot, order_to_invoice, invoice_to_shipment

	-- ORDER: Header - Line JOIN
	select top 100 
		oh.document_id, oh.document_date, oh.document_type,
		oh.order_id, oh.order_no, oh.order_date, 
		oh.customer_id, oh.customer_order_seq_no, 
		ol.line_id, ol.product_type, ol.product_id, ol.name, ol.qty
	from
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es'
				and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) oh
		inner join
			(select line_id,
				document_id, document_date, document_type,
				order_id, order_no, order_date,
				product_type, product_id, name, qty
			from DW_GetLenses.dbo.order_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
	order by oh.order_id, oh.document_type, ol.line_id;

	-- SHIPMENT: Header - Line JOIN
	select top 100
		sh.order_id, sh.order_no, 
		sh.document_id, sh.document_date, sh.document_type,
		sh.shipment_id, sh.shipment_no, 
		sh.order_date, sh.invoice_date, sh.shipment_date,
		sh.length_of_time_to_invoice, sh.length_of_time_invoice_to_this_shipment,
		sh.customer_id, sh.customer_order_seq_no, 
		sl.line_id, sl.product_type, sl.product_id, sl.name, sl.qty
	from
			(select 
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				invoice_id, invoice_no, invoice_date,
				order_id, order_no, order_date, 	
				length_of_time_to_invoice, length_of_time_invoice_to_this_shipment,
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.shipment_headers
			where store_name = 'visiondirect.es'
				and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) sh
		inner join
			(select line_id,
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				product_type, product_id, name, qty
			from DW_GetLenses.dbo.shipment_lines) sl on sh.document_id = sl.document_id and sh.document_type = sl.document_type
	order by sh.order_id, sh.document_type, sl.line_id;

	-- Product Type + Qty
	select top 100
		sh.document_type, sl.product_type, sl.qty, count(*), count(distinct sh.order_id)
	from
			(select 
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				invoice_id, invoice_no, invoice_date,
				order_id, order_no, order_date, 	
				length_of_time_to_invoice, length_of_time_invoice_to_this_shipment,
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.shipment_headers
			where store_name = 'visiondirect.es'
				and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) sh
		inner join
			(select line_id,
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				product_type, product_id, name, qty
			from DW_GetLenses.dbo.shipment_lines) sl on sh.document_id = sl.document_id and sh.document_type = sl.document_type
	group by sh.document_type, sl.product_type, sl.qty
	order by sh.document_type, sl.product_type, sl.qty;

	select num_tot_orders,
		num_tot, num_tot_doc_type, num_tot_doc_type * 100 / convert(decimal(10,2), num_tot) perc_doc_type,
		num_tot_prod_type, num_tot_prod_type * 100 / convert(decimal(10,2), num_tot) perc_prod_type,
		document_type, product_type, qty, 
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot_prod_type) perc_prod_type_qty
	from
		(select sh.document_type, sl.product_type, sl.qty, 
			sh.num_tot_orders,
			count(*) over () num_tot, 
			count(*) over (partition by sh.document_type) num_tot_doc_type, 
			count(*) over (partition by sl.product_type) num_tot_prod_type
		from
				(select count(*) over () num_tot_orders, 
					document_id, document_date, document_type,
					shipment_id, shipment_no, shipment_date,
					invoice_id, invoice_no, invoice_date,
					order_id, order_no, order_date, 	
					length_of_time_to_invoice, length_of_time_invoice_to_this_shipment,
					customer_id, customer_order_seq_no
				from DW_GetLenses.dbo.shipment_headers
				where store_name = 'visiondirect.es'
					and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) sh
			inner join
				(select line_id,
					document_id, document_date, document_type,
					shipment_id, shipment_no, shipment_date,
					product_type, product_id, name, qty
				from DW_GetLenses.dbo.shipment_lines) sl on sh.document_id = sl.document_id and sh.document_type = sl.document_type) t
	group by num_tot_orders, num_tot, num_tot_doc_type, num_tot_prod_type,
		document_type, product_type, qty
	order by num_tot_orders, num_tot, num_tot_doc_type, num_tot_prod_type,
		document_type, product_type, qty;

	-- Customers Repeating Orders
	select top 100 count(*) over () num_tot,
		oh1.document_type, oh2.document_type, 
		oh1.order_id order_id_1, oh1.order_no order_no_1, oh1.order_date order_date_2, oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
		oh1.customer_id, oh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, 
		count(*) over (partition by oh1.order_id) num_by_order,
		rank() over (partition by oh1.order_id order by oh2.order_id) rank_by_order
	from
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es'
				and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) oh1
		inner join
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es' and document_type = 'ORDER'
				and order_date > '2016-09-01') oh2 on oh1.customer_id = oh2.customer_id and oh1.document_type = oh2.document_type and oh1.order_date < oh2.order_date
	--where oh1.customer_id = 1054043
	order by oh1.order_id, oh2.order_id

	select top 1000 count(*) over () num_tot_def, *
	from
		(select count(*) over () num_tot,
			oh1.document_type document_type_1, oh2.document_type document_type_2, 
			oh1.order_id order_id_1, oh1.order_no order_no_1, oh1.order_date order_date_1, oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
			oh1.customer_id, oh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, 
			count(*) over (partition by oh1.order_id) num_by_order,
			rank() over (partition by oh1.order_id order by oh2.order_id) rank_by_order
		from
				(select 
					document_id, document_date, document_type,
					order_id, order_no, order_date, 
					customer_id, customer_order_seq_no
				from DW_GetLenses.dbo.order_headers
				where store_name = 'visiondirect.es'
					and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) oh1
			inner join
				(select 
					document_id, document_date, document_type,
					order_id, order_no, order_date, 
					customer_id, customer_order_seq_no
				from DW_GetLenses.dbo.order_headers
				where store_name = 'visiondirect.es' and document_type = 'ORDER'
					and order_date > '2016-09-01') oh2 on oh1.customer_id = oh2.customer_id and oh1.document_type = oh2.document_type and oh1.order_date < oh2.order_date) t
	where rank_by_order = 1
	order by order_id_1, order_id_2
