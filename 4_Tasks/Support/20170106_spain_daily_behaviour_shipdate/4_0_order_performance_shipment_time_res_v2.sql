
	select num_tot_rep_def, num_tot_rep, num_tot,
		document_type_1, document_type_2, 
		order_id_1, order_no_1, order_date_1, invoice_date, shipment_date, 
		order_id_2, order_no_2, order_date_2, 
		rep_flag,
		diff_order_days,
		invoice_to_shipment,
		customer_id, customer_order_seq_no_1, customer_order_seq_no_2, qty
		num_by_order, rank_by_order
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2

	-- 1: 
	select num_tot, rep_flag, qty, 
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2
	group by num_tot, qty, rep_flag
	order by num_tot, qty, rep_flag

	select num_tot, rep_flag, 
		qty, num_tot_qty,  num_tot_qty * 100 / convert(decimal(10,2), num_tot) perc_qty,
		invoice_to_shipment,
		count(*) num_invoice_to_shipment, count(*) * 100 / convert(decimal(10,2), num_tot_qty) perc_invoice_to_shipment
	from 
		(select count(*) over (partition by rep_flag, qty) num_tot_qty, *
		from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2) t
	group by num_tot, rep_flag, num_tot_qty, qty, invoice_to_shipment
	order by num_tot, qty, rep_flag, invoice_to_shipment

	-- 2:
	select num_tot, rep_flag, qty, year(order_date_1) yyyy, month(order_date_1) mm,
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2
	group by num_tot, rep_flag, qty, year(order_date_1), month(order_date_1)
	order by num_tot, rep_flag, qty, year(order_date_1), month(order_date_1)

	-- 3
	select num_tot, invoice_to_shipment, 
		count(*) num, count(*) * 100 / convert(decimal(10,2), num_tot) perc
	from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2
	group by num_tot, invoice_to_shipment
	order by num_tot, invoice_to_shipment

	select num_tot, invoice_to_shipment, 
		num_tot_invoice_to_shipment, num_tot_invoice_to_shipment * 100 / convert(decimal(10,2), num_tot) perc_invoice_to_shipment,
		rep_flag,
		count(*) num_rep_flag, count(*) * 100 / convert(decimal(10,2), num_tot_invoice_to_shipment) perc_rep_flag
	from 
		(select count(*) over (partition by invoice_to_shipment) num_tot_invoice_to_shipment, *
		from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2) t
	group by num_tot, num_tot_invoice_to_shipment, invoice_to_shipment, rep_flag
	order by num_tot, invoice_to_shipment, rep_flag
		
		select t1.num_tot, t1.invoice_to_shipment, t1.num_tot_invoice_to_shipment, t1.perc_invoice_to_shipment, 
			t1.rep_flag, t1.num_rep_flag, t1.perc_rep_flag, t2.rep_flag, t2.num_rep_flag, t2.perc_rep_flag
		from
				(select num_tot, invoice_to_shipment, 
					num_tot_invoice_to_shipment, num_tot_invoice_to_shipment * 100 / convert(decimal(10,2), num_tot) perc_invoice_to_shipment,
					rep_flag,
					count(*) num_rep_flag, count(*) * 100 / convert(decimal(10,2), num_tot_invoice_to_shipment) perc_rep_flag
				from 
					(select count(*) over (partition by invoice_to_shipment) num_tot_invoice_to_shipment, *
					from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2) t
				group by num_tot, num_tot_invoice_to_shipment, invoice_to_shipment, rep_flag) t1
			full join
				(select num_tot, invoice_to_shipment, 
					num_tot_invoice_to_shipment, num_tot_invoice_to_shipment * 100 / convert(decimal(10,2), num_tot) perc_invoice_to_shipment,
					rep_flag,
					count(*) num_rep_flag, count(*) * 100 / convert(decimal(10,2), num_tot_invoice_to_shipment) perc_rep_flag
				from 
					(select count(*) over (partition by invoice_to_shipment) num_tot_invoice_to_shipment, *
					from DW_GetLenses_jbs.dbo.shipment_spain_res_qty_v2) t
				group by num_tot, num_tot_invoice_to_shipment, invoice_to_shipment, rep_flag) t2 on t1.invoice_to_shipment = t2.invoice_to_shipment
		where t1.rep_flag = 'NO_REP' and t2.rep_flag = 'REP'
		order by t1.num_tot, t1.invoice_to_shipment, t1.rep_flag

	-- http://stackoverflow.com/questions/15931607/convert-rows-to-columns-using-pivot-in-sql-server 

