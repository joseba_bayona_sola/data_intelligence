
select top 100 count(*) over () num_tot, -- 4007
	order_id, order_no, 
	document_id, document_date, document_type,
	shipment_id, shipment_no, 
	order_date, invoice_date, shipment_date,
	order_to_invoice, length_of_time_to_invoice, 
	invoice_to_shipment, length_of_time_invoice_to_this_shipment,
	customer_id, customer_order_seq_no, 
	line_id, product_type, product_id, name, qty
from DW_GetLenses_jbs.dbo.shipment_spain
where product_type = 'Contact Lenses - Dailies'
	and qty in ('30.0000', '60.0000'); 

select top 100 count(*) over () num_tot, * -- 2112
from
	(select distinct 
		document_type,
		order_id, order_no, shipment_id, shipment_no, 
		order_date, invoice_date, shipment_date,
		order_to_invoice, length_of_time_to_invoice, 
		invoice_to_shipment, length_of_time_invoice_to_this_shipment,
		customer_id, customer_order_seq_no
	from DW_GetLenses_jbs.dbo.shipment_spain
	where product_type = 'Contact Lenses - Dailies'
		and qty in ('30.0000', '60.0000')) t 

select top 100 count(*) over () num_tot, * -- 2204
from
	(select distinct 
		document_type,
		order_id, order_no, shipment_id, shipment_no, 
		order_date, invoice_date, shipment_date,
		order_to_invoice, length_of_time_to_invoice, 
		invoice_to_shipment, length_of_time_invoice_to_this_shipment,
		customer_id, customer_order_seq_no, 
		qty
	from DW_GetLenses_jbs.dbo.shipment_spain
	where product_type = 'Contact Lenses - Dailies'
		and qty in ('30.0000', '60.0000')) t 

		select top 100 count(*) over () num_tot, 
			count(*) over (partition by order_id) num_tot_qty,
			* 
		from
			(select distinct 
				document_type,
				order_id, order_no, shipment_id, shipment_no, 
				order_date, invoice_date, shipment_date,
				order_to_invoice, length_of_time_to_invoice, 
				invoice_to_shipment, length_of_time_invoice_to_this_shipment,
				customer_id, customer_order_seq_no, 
				qty
			from DW_GetLenses_jbs.dbo.shipment_spain
			where product_type = 'Contact Lenses - Dailies'
				and qty in ('30.0000', '60.0000')) t 
		order by num_tot_qty desc

		select *
		from DW_GetLenses_jbs.dbo.shipment_spain
		where customer_id = 1051528
		order by order_id, line_id

----------------------------------------------------------------------------

select top 1000 count(*) over () num_tot_rep, num_tot,
	sh1.document_type document_type_1, oh2.document_type document_type_2, 
	sh1.order_id order_id_1, sh1.order_no order_no_1, sh1.order_date order_date_1, sh1.invoice_date, sh1.shipment_date, 
	oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
	sh1.invoice_to_shipment,
	sh1.customer_id, sh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, qty,
	count(*) over (partition by sh1.order_id) num_by_order,
	rank() over (partition by sh1.order_id order by oh2.order_id) rank_by_order
from
		(select count(*) over () num_tot, * 
		from
			(select distinct 
				document_type,
				order_id, order_no, shipment_id, shipment_no, 
				order_date, invoice_date, shipment_date,
				order_to_invoice, length_of_time_to_invoice, 
				invoice_to_shipment, length_of_time_invoice_to_this_shipment,
				customer_id, customer_order_seq_no, qty
			from DW_GetLenses_jbs.dbo.shipment_spain
			where product_type = 'Contact Lenses - Dailies'
				and qty in ('30.0000', '60.0000')) t) sh1
	inner join
		(select 
			document_id, document_date, document_type,
			order_id, order_no, order_date, 
			customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers
		where store_name = 'visiondirect.es' and document_type = 'ORDER'
			and order_date > '2016-09-01') oh2 on sh1.customer_id = oh2.customer_id and sh1.document_type = 'SHIPMENT' and oh2.document_type = 'ORDER' and sh1.order_date < oh2.order_date
--where sh1.customer_id = 1051528
order by order_id_1, order_id_2

select count(*) over () num_tot_rep_def, *
from
	(select count(*) over () num_tot_rep, num_tot,
		sh1.document_type document_type_1, oh2.document_type document_type_2, 
		sh1.order_id order_id_1, sh1.order_no order_no_1, sh1.order_date order_date_1, sh1.invoice_date, sh1.shipment_date, 
		oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
		sh1.invoice_to_shipment,
		sh1.customer_id, sh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, qty,
		count(*) over (partition by sh1.order_id) num_by_order,
		rank() over (partition by sh1.order_id order by oh2.order_id) rank_by_order
	from
			(select count(*) over () num_tot, * -- 2112
			from
				(select distinct 
					document_type,
					order_id, order_no, shipment_id, shipment_no, 
					order_date, invoice_date, shipment_date,
					order_to_invoice, length_of_time_to_invoice, 
					invoice_to_shipment, length_of_time_invoice_to_this_shipment,
					customer_id, customer_order_seq_no, qty
				from DW_GetLenses_jbs.dbo.shipment_spain
				where product_type = 'Contact Lenses - Dailies'
					and qty in ('30.0000', '60.0000')) t) sh1
		inner join
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es' and document_type = 'ORDER'
				and order_date > '2016-09-01') oh2 on sh1.customer_id = oh2.customer_id and sh1.document_type = 'SHIPMENT' and oh2.document_type = 'ORDER' and sh1.order_date < oh2.order_date) t
where rank_by_order = 1
	--and customer_id = 1051528
order by order_id_1, order_id_2

----------------------------------------------------

select count(*) over () num_tot_rep_def, *
into DW_GetLenses_jbs.dbo.shipment_spain_res 
from
	(select count(*) over () num_tot_rep, num_tot,
		sh1.document_type document_type_1, oh2.document_type document_type_2, 
		sh1.order_id order_id_1, sh1.order_no order_no_1, sh1.order_date order_date_1, sh1.invoice_date, sh1.shipment_date, 
		oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
		sh1.invoice_to_shipment,
		sh1.customer_id, sh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, 
		count(*) over (partition by sh1.order_id) num_by_order,
		rank() over (partition by sh1.order_id order by oh2.order_id) rank_by_order
	from
			(select count(*) over () num_tot, * -- 2112
			from
				(select distinct 
					document_type,
					order_id, order_no, shipment_id, shipment_no, 
					order_date, invoice_date, shipment_date,
					order_to_invoice, length_of_time_to_invoice, 
					invoice_to_shipment, length_of_time_invoice_to_this_shipment,
					customer_id, customer_order_seq_no
				from DW_GetLenses_jbs.dbo.shipment_spain
				where product_type = 'Contact Lenses - Dailies'
					and qty in ('30.0000', '60.0000')) t) sh1
		inner join
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es' and document_type = 'ORDER'
				and order_date > '2016-09-01') oh2 on sh1.customer_id = oh2.customer_id and sh1.document_type = 'SHIPMENT' and oh2.document_type = 'ORDER' and sh1.order_date < oh2.order_date) t
where rank_by_order = 1
order by order_id_1, order_id_2

-- 

select count(*) over () num_tot_rep_def, *
into DW_GetLenses_jbs.dbo.shipment_spain_res_qty 
from
	(select count(*) over () num_tot_rep, num_tot,
		sh1.document_type document_type_1, oh2.document_type document_type_2, 
		sh1.order_id order_id_1, sh1.order_no order_no_1, sh1.order_date order_date_1, sh1.invoice_date, sh1.shipment_date, 
		oh2.order_id order_id_2, oh2.order_no order_no_2, oh2.order_date order_date_2, 
		sh1.invoice_to_shipment,
		sh1.customer_id, sh1.customer_order_seq_no customer_order_seq_no_1, oh2.customer_order_seq_no customer_order_seq_no_2, qty,
		count(*) over (partition by sh1.order_id) num_by_order,
		rank() over (partition by sh1.order_id order by oh2.order_id) rank_by_order
	from
			(select count(*) over () num_tot, * -- 2112
			from
				(select distinct 
					document_type,
					order_id, order_no, shipment_id, shipment_no, 
					order_date, invoice_date, shipment_date,
					order_to_invoice, length_of_time_to_invoice, 
					invoice_to_shipment, length_of_time_invoice_to_this_shipment,
					customer_id, customer_order_seq_no, qty
				from DW_GetLenses_jbs.dbo.shipment_spain
				where product_type = 'Contact Lenses - Dailies'
					and qty in ('30.0000', '60.0000')) t) sh1
		inner join
			(select 
				document_id, document_date, document_type,
				order_id, order_no, order_date, 
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.order_headers
			where store_name = 'visiondirect.es' and document_type = 'ORDER'
				and order_date > '2016-09-01') oh2 on sh1.customer_id = oh2.customer_id and sh1.document_type = 'SHIPMENT' and oh2.document_type = 'ORDER' and sh1.order_date < oh2.order_date) t
where rank_by_order = 1
order by order_id_1, order_id_2