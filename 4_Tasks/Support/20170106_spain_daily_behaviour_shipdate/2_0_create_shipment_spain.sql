
	drop table DW_GetLenses_jbs.dbo.shipment_spain;

	select 
		sh.order_id, sh.order_no, 
		sh.document_id, sh.document_date, sh.document_type,
		sh.shipment_id, sh.shipment_no, 
		sh.order_date, sh.invoice_date, sh.shipment_date,
		datediff(day, sh.order_date, sh.invoice_date) order_to_invoice, datediff(day, sh.invoice_date, sh.shipment_date) invoice_to_shipment, 
		sh.length_of_time_to_invoice, sh.length_of_time_invoice_to_this_shipment,
		sh.customer_id, sh.customer_order_seq_no, 
		sl.line_id, sl.product_type, sl.product_id, sl.name, sl.qty
	into 
		DW_GetLenses_jbs.dbo.shipment_spain 
	from
			(select 
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				invoice_id, invoice_no, invoice_date,
				order_id, order_no, order_date, 	
				length_of_time_to_invoice, length_of_time_invoice_to_this_shipment,
				customer_id, customer_order_seq_no
			from DW_GetLenses.dbo.shipment_headers
			where store_name = 'visiondirect.es'
				and YEAR(order_date) = 2016 and MONTH(order_date) in (9, 10, 11)) sh
		inner join
			(select line_id,
				document_id, document_date, document_type,
				shipment_id, shipment_no, shipment_date,
				product_type, product_id, name, qty
			from DW_GetLenses.dbo.shipment_lines) sl on sh.document_id = sl.document_id and sh.document_type = sl.document_type
	order by sh.order_id, sh.document_type, sl.line_id;

	select top 100
		order_id, order_no, 
		document_id, document_date, document_type,
		shipment_id, shipment_no, 
		order_date, invoice_date, shipment_date,
		order_to_invoice, length_of_time_to_invoice, 
		invoice_to_shipment, length_of_time_invoice_to_this_shipment,
		customer_id, customer_order_seq_no, 
		line_id, product_type, product_id, name, qty
	from DW_GetLenses_jbs.dbo.shipment_spain; 
