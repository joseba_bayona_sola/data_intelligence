

select top 1000 order_id_bk, invoice_id, shipment_id, 
	order_no, invoice_no, shipment_no, 
	order_date, invoice_date, shipment_date, 
	format(datediff(day, order_date, shipment_date), '000') days_to_shipment, 
	format(datediff(day, invoice_date, shipment_date), '000') days_invoice_to_shipment
from Warehouse.sales.dim_order_header_v
where shipment_date is not null
order by order_date desc



