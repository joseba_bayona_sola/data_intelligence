
select customer_id, email, store_name, created_at,
	prefix, firstname, lastname, 
	default_billing, default_shipping,
	last_order_date, num_of_orders, segment_lifecycle
from DW_GetLenses.dbo.customers
where store_name = 'visiondirect.fr'
order by created_at desc

select top 1000 address_id, customer_id, created_at, 
	street, city, postcode, region_id, region, country_id, telephone
from DW_GetLenses.dbo.customer_addresses
where address_id = 2823954

select postcode, count(*)
from DW_GetLenses.dbo.customer_addresses
where country_id = 'FR'
group by postcode
order by postcode

select substring(postcode, 1, 2) pc, count(*)
from DW_GetLenses.dbo.customer_addresses
where country_id = 'FR'
group by substring(postcode, 1, 2)
order by substring(postcode, 1, 2)

--------------------------------------------------------------------

select c.customer_id, c.email, c.store_name, c.created_at,
	ca.address_id, ca.customer_id, ca.created_at, 
	c.prefix, c.firstname, c.lastname, 
	c.default_billing, c.default_shipping,
	c.last_order_date, c.num_of_orders,
	ca.street, ca.city, ca.postcode, ca.region_id, ca.region, ca.country_id, ca.telephone
from
		(select customer_id, email, store_name, created_at,
			prefix, firstname, lastname, 
			default_billing, default_shipping,
			last_order_date, num_of_orders
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.fr') c
	left join
		DW_GetLenses.dbo.customer_addresses ca on c.default_billing = ca.address_id
where ca.address_id is null
order by c.created_at desc


select c.customer_id, c.email, c.store_name, c.created_at,
	ca.address_id, ca.customer_id, ca.created_at, 
	c.prefix, c.firstname, c.lastname, 
	c.default_billing, c.default_shipping,
	c.last_order_date, c.num_of_orders,
	ca.street, ca.city, ca.postcode, ca.region_id, ca.region, ca.country_id, ca.telephone
from
		(select customer_id, email, store_name, created_at,
			prefix, firstname, lastname, 
			default_billing, default_shipping,
			last_order_date, num_of_orders
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.fr') c
	inner join
		DW_GetLenses.dbo.customer_addresses ca on c.default_billing = ca.address_id and substring(ca.postcode, 1, 2) in ('75', '77', '78', '91', '92', '93', '94', '95')
where c.num_of_orders is not null and c.num_of_orders <> 1
order by c.created_at desc

--------------------------------------------------------------------

select c.customer_id, 
	c.prefix, c.firstname, c.lastname, c.email, 
	ca.telephone, 
	-- ca.street, 
	ca.city, ca.postcode, ca.region_id, ca.region, -- ca.country_id, 
	c.last_order_date, c.num_of_orders
from
		(select customer_id, email, store_name, created_at,
			prefix, firstname, lastname, 
			default_billing, default_shipping,
			last_order_date, num_of_orders
		from DW_GetLenses.dbo.customers
		where store_name = 'visiondirect.fr') c
	inner join
		DW_GetLenses.dbo.customer_addresses ca on c.default_billing = ca.address_id and substring(ca.postcode, 1, 2) in ('75', '77', '78', '91', '92', '93', '94', '95')
where c.num_of_orders is not null and c.num_of_orders <> 1
order by ca.postcode, ca.region, c.created_at desc


