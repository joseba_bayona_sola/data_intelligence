

select oh.order_id, oh.order_no, oh.document_id, oh.document_type, oh.document_date, oh.customer_id, oh.customer_order_seq_no, 
	ol.product_type, ol.product_id, ol.name
from
		(select order_id, order_no, document_id, document_type, document_date, customer_id,
			customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) oh
	inner join
		(select line_id, order_id, order_no, document_id, document_type, 
			product_id, product_type, name
		from DW_GetLenses.dbo.order_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.customer_id = 1592552

----
drop table #customer_order_list_fr

select c.customer_id, 
	c.prefix, c.firstname, c.lastname, c.email, 
	c.telephone, c.city, c.postcode, c.region_id, c.region, 
	c.last_order_date, c.num_of_orders, 
	o.order_id, o.order_no, o.document_type, o.document_date, o.product_type, o.product_id, o.name
into #customer_order_list_fr
from
		(select c.customer_id, 
			c.prefix, c.firstname, c.lastname, c.email, 
			ca.telephone, 
			-- ca.street, 
			ca.city, ca.postcode, ca.region_id, ca.region, -- ca.country_id, 
			c.last_order_date, c.num_of_orders
		from
				(select customer_id, email, store_name, created_at,
					prefix, firstname, lastname, 
					default_billing, default_shipping,
					last_order_date, num_of_orders
				from DW_GetLenses.dbo.customers
				where store_name = 'visiondirect.fr') c
			inner join
				DW_GetLenses.dbo.customer_addresses ca on c.default_billing = ca.address_id and substring(ca.postcode, 1, 2) in ('75', '77', '78', '91', '92', '93', '94', '95')
		where c.num_of_orders is not null and c.num_of_orders <> 1) c
	left join
		(select oh.order_id, oh.order_no, oh.document_id, oh.document_type, oh.document_date, oh.customer_id, oh.customer_order_seq_no, 
			ol.product_type, ol.product_id, ol.name
		from
				(select order_id, order_no, document_id, document_type, document_date, customer_id,
					customer_order_seq_no
				from DW_GetLenses.dbo.order_headers) oh
			inner join
				(select line_id, order_id, order_no, document_id, document_type, 
					product_id, product_type, name
				from DW_GetLenses.dbo.order_lines) ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type) o on c.customer_id = o.customer_id


---- 

select count(distinct customer_id)
from #customer_order_list_fr
where order_id is not null

select customer_id, count(distinct order_id)
from #customer_order_list_fr
where order_id is not null
group by customer_id
having count(distinct order_id) > 1

select customer_id, order_id, order_no, document_type, document_date
from #customer_order_list_fr
where customer_id in (1360845, 1360848)
order by customer_id, order_id, document_date

select customer_id, order_id, count (distinct document_type)
from #customer_order_list_fr
group by customer_id, order_id
having count (distinct document_type) = 1

select customer_id, count(distinct order_id)
from 
	(select customer_id, order_id, count (distinct document_type) num
	from #customer_order_list_fr
	group by customer_id, order_id
	having count (distinct document_type) = 1) t
where order_id is not null
group by customer_id
having count(distinct order_id) > 1


select customer_id, 
	prefix, firstname, lastname, email, 
	telephone, city, postcode, region_id, region, 
	last_order_date, num_of_orders, 
	dense_rank() over (partition by customer_id order by order_id) order_rank,
	order_id, order_no, document_type, document_date, product_type, product_id, name 
from #customer_order_list_fr
--where order_no is null
order by customer_id, order_id, document_date, product_id

----

select co.customer_id, 
	co.prefix, co.firstname, co.lastname, co.email, 
	co.telephone, co.city, co.postcode, co.region_id, co.region, 
	co.last_order_date, co.num_of_orders, 
	t.num_orders,
	dense_rank() over (partition by co.customer_id order by co.order_id) order_rank,
	co.order_id, co.order_no, co.document_type, co.document_date, co.product_type, co.product_id, co.name 
from 
		(select o.*
		from 
			#customer_order_list_fr o
		inner join
			(select customer_id, order_id, count (distinct document_type) num
			from #customer_order_list_fr
			group by customer_id, order_id
			having count (distinct document_type) = 1) o2 on o.customer_id = o2.customer_id and o.order_id = o2.order_id) co
	inner join
		(select customer_id, count(distinct order_id) num_orders
		from 
			(select customer_id, order_id, count (distinct document_type) num
			from #customer_order_list_fr
			group by customer_id, order_id
			having count (distinct document_type) = 1) t
		where order_id is not null
		group by customer_id
		having count(distinct order_id) > 1	) t on co.customer_id = t.customer_id
order by customer_id, order_id, document_date, product_id


----

select *, dense_rank() over (order by customer_id) rank_cust, 
	dense_rank() over (partition by customer_id order by product_type, product_id) rank_prod
from
	(select co.customer_id, 
		co.prefix, co.firstname, co.lastname, co.email, 
		co.telephone, co.city, co.postcode, co.region_id, co.region, 
		co.last_order_date, co.num_of_orders, 
		t.num_orders,
		dense_rank() over (partition by co.customer_id order by co.order_id) order_rank,
		co.order_id, co.order_no, co.document_type, co.document_date, co.product_type, co.product_id, co.name 
	from 
			(select o.*
			from 
				#customer_order_list_fr o
			inner join
				(select customer_id, order_id, count (distinct document_type) num
				from #customer_order_list_fr
				group by customer_id, order_id
				having count (distinct document_type) = 1) o2 on o.customer_id = o2.customer_id and o.order_id = o2.order_id) co
		inner join
			(select customer_id, count(distinct order_id) num_orders
			from 
				(select customer_id, order_id, count (distinct document_type) num
				from #customer_order_list_fr
				group by customer_id, order_id
				having count (distinct document_type) = 1) t
			where order_id is not null
			group by customer_id
			having count(distinct order_id) > 1	) t on co.customer_id = t.customer_id) t
where num_orders = order_rank
order by customer_id, order_id, document_date, product_id

----

select distinct firstname + ' ' + lastname customer_name, email customer_email, telephone customer_contact_number, 
	city customer_town, postcode customer_postcode, region, product_type, name contact_lens_ordered
--select distinct *
from 
	(select *, dense_rank() over (order by customer_id) rank_cust, 
		dense_rank() over (partition by customer_id order by product_type, product_id) rank_prod
	from
		(select co.customer_id, 
			co.prefix, co.firstname, co.lastname, co.email, 
			co.telephone, co.city, co.postcode, co.region_id, co.region, 
			co.last_order_date, co.num_of_orders, 
			t.num_orders,
			dense_rank() over (partition by co.customer_id order by co.order_id) order_rank,
			co.order_id, co.order_no, co.document_type, co.document_date, co.product_type, co.product_id, co.name 
		from 
				(select o.*
				from 
					#customer_order_list_fr o
				inner join
					(select customer_id, order_id, count (distinct document_type) num
					from #customer_order_list_fr
					group by customer_id, order_id
					having count (distinct document_type) = 1) o2 on o.customer_id = o2.customer_id and o.order_id = o2.order_id) co
			inner join
				(select customer_id, count(distinct order_id) num_orders
				from 
					(select customer_id, order_id, count (distinct document_type) num
					from #customer_order_list_fr
					group by customer_id, order_id
					having count (distinct document_type) = 1) t
				where order_id is not null
				group by customer_id
				having count(distinct order_id) > 1	) t on co.customer_id = t.customer_id) t
	where num_orders = order_rank) t
where rank_prod = 1
	and product_type like 'Contact%'
order by name, postcode, region, city


---- 

-- select postcode, region_id, region, city, count(*)
select region_id, region, count(*)
from
	(select distinct *
	from 
		(select *, dense_rank() over (order by customer_id) rank_cust, 
			dense_rank() over (partition by customer_id order by product_type, product_id) rank_prod
		from
			(select co.customer_id, 
				co.prefix, co.firstname, co.lastname, co.email, 
				co.telephone, co.city, co.postcode, co.region_id, co.region, 
				co.last_order_date, co.num_of_orders, 
				t.num_orders,
				dense_rank() over (partition by co.customer_id order by co.order_id) order_rank,
				co.order_id, co.order_no, co.document_type, co.document_date, co.product_type, co.product_id, co.name 
			from 
					(select o.*
					from 
						#customer_order_list_fr o
					inner join
						(select customer_id, order_id, count (distinct document_type) num
						from #customer_order_list_fr
						group by customer_id, order_id
						having count (distinct document_type) = 1) o2 on o.customer_id = o2.customer_id and o.order_id = o2.order_id) co
				inner join
					(select customer_id, count(distinct order_id) num_orders
					from 
						(select customer_id, order_id, count (distinct document_type) num
						from #customer_order_list_fr
						group by customer_id, order_id
						having count (distinct document_type) = 1) t
					where order_id is not null
					group by customer_id
					having count(distinct order_id) > 1	) t on co.customer_id = t.customer_id) t
		where num_orders = order_rank) t
	where rank_prod = 1) t
group by region_id, region
order by region_id, region
-- group by postcode, region_id, region, city
-- order by postcode, region_id, region, city