
select top 1000 count(*) over () num_tot, customer_id, count(*) num_orders  
from DW_GetLenses.dbo.invoice_headers
where year(document_date) = 2016	
group by customer_id

select num_tot, num_orders, count(*) tot_num_orders, count(*)*100/convert(decimal(10,2), num_tot) perc_num_orders
from 
	(select count(*) over () num_tot, customer_id, count(*) num_orders  
	from DW_GetLenses.dbo.invoice_headers
	where year(document_date) = 2016 and document_type = 'INVOICE'	
	group by customer_id) t
group by num_tot, num_orders
order by num_tot, num_orders


---------------------------------------------

select distinct top 1000 customer_id
from DW_GetLenses.dbo.invoice_headers
where year(document_date) = 2016 and document_type = 'INVOICE'		

select top 1000 c.customer_id, ih.invoice_date, ih.customer_order_seq_no, 
	min(ih.invoice_date) over (partition by c.customer_id) min_date, max(ih.invoice_date) over (partition by c.customer_id) max_date, 
	count(*) over (partition by c.customer_id) num_orders
from 
		(select distinct customer_id
		from DW_GetLenses.dbo.invoice_headers
		where year(document_date) = 2016 and document_type = 'INVOICE') c		
	inner join
		DW_GetLenses.dbo.invoice_headers ih on c.customer_id = ih.customer_id and ih.document_type = 'INVOICE'
order by c.customer_id, ih.invoice_date

---------------------------------------------

select distinct top 1000 customer_id, min_date, max_date, datediff(day, min_date, max_date) diff_days, num_orders, 
	datediff(day, min_date, max_date) / num_orders frecuency_dd, (datediff(day, min_date, max_date) / num_orders) / 30 frequency_mm 
from 
	(select c.customer_id, ih.invoice_date, ih.customer_order_seq_no, 
		min(ih.invoice_date) over (partition by c.customer_id) min_date, max(ih.invoice_date) over (partition by c.customer_id) max_date, 
		count(*) over (partition by c.customer_id) num_orders
	from 
			(select distinct customer_id
			from DW_GetLenses.dbo.invoice_headers
			where year(document_date) = 2016 and document_type = 'INVOICE') c		
		inner join
			DW_GetLenses.dbo.invoice_headers ih on c.customer_id = ih.customer_id and ih.document_type = 'INVOICE') t
where num_orders = 1
order by customer_id

select num_tot, frequency_mm, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc_num
from
	(select count(*) over () num_tot, min_date, max_date, diff_days, num_orders, frecuency_dd, frequency_mm
	from 
		(select distinct customer_id, min_date, max_date, datediff(day, min_date, max_date) diff_days, num_orders, 
			datediff(day, min_date, max_date) / num_orders frecuency_dd, (datediff(day, min_date, max_date) / num_orders) / 30 frequency_mm 
		from 
			(select c.customer_id, ih.invoice_date, ih.customer_order_seq_no, 
				min(ih.invoice_date) over (partition by c.customer_id) min_date, max(ih.invoice_date) over (partition by c.customer_id) max_date, 
				count(*) over (partition by c.customer_id) num_orders
			from 
					(select distinct customer_id
					from DW_GetLenses.dbo.invoice_headers
					-- where year(document_date) = 2016 and document_type = 'INVOICE'
					-- where year(document_date) = 2015 and document_type = 'INVOICE'
					-- where year(document_date) = 2014 and document_type = 'INVOICE'
					where year(document_date) = 2013 and document_type = 'INVOICE'
					) c		
				inner join
					DW_GetLenses.dbo.invoice_headers ih on c.customer_id = ih.customer_id and ih.document_type = 'INVOICE') t) t) t
group by num_tot, frequency_mm
order by num_tot, frequency_mm