
--------------------------------------------------------------------------
--------------------------------------------------------------------------
	
	-- CONFLICT PRODUCTS TO TEMP TABLE 

	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, 
		case when (t.product_id is not null) then 1 else 0 end flag_pf_cat, 
		esi.num_dist_param, 
		m_p.magentoSKU
	into DW_GetLenses_jbs.dbo.dim_product_family_v_201708
	from 
			Warehouse.prod.dim_product_family_v pf
		left join
			(select distinct product_id
			from #product_magento_category) t on pf.product_id_magento = t.product_id
		left join
			(select product_id, count(*) num_dist_param
			from Landing.aux.mag_edi_stock_item
			group by product_id) esi on pf.product_id_magento = esi.product_id	
		left join
			(select distinct magentoProductID_int, magentoSKU, name
			from Landing.mend.gen_prod_productfamilypacksize_v) m_p on pf.product_id_magento = m_p.magentoProductID_int
	order by product_lifecycle_name, product_type_name, category_name, flag_pf_cat, product_id_magento

	

	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, 
		flag_pf_cat, num_dist_param, 
		magentoSKU
	from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
	where product_type_name = 'Other'
	order by product_lifecycle_name, product_type_name, category_name, flag_pf_cat, product_id_magento

--------------------------------------------------------------------------

	-- Check Old - New Data
	select t.*, pf.product_type_name, pf.category_name, pf.cl_type_name, pf.cl_feature_name,
		pf.product_lifecycle_name, pf.product_family_name
	from
			(select product_id_magento, product_type_name, category_name, product_lifecycle_name
			from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
			where product_type_name = 'Other') t
		inner join
			Warehouse.prod.dim_product_family_v pf on t.product_id_magento = pf.product_id_magento
	where t.product_type_name = pf.product_type_name
	order by t.product_lifecycle_name, t.product_type_name, t.category_name, t.product_id_magento	


	select product_type_name, category_name, product_id_magento, 
		product_family_name, product_lifecycle_name
	from Warehouse.prod.dim_product_family_v
	where product_type_name = 'Other'
	order by product_type_name, category_name, product_lifecycle_name, product_family_name

	select product_type_name, category_name, product_id_magento, 
		product_family_name, product_lifecycle_name
	from Warehouse.prod.dim_product_family_v
	order by product_id_magento


	select t.product_type_name, t.category_name, pf.product_type_name, pf.category_name, pf.cl_type_name, pf.cl_feature_name,
		pf.product_id_magento, pf.product_family_name, 
		ptoh.product_type_oh_bk, ptvat.product_type_vat
	from
			(select product_id_magento, product_type_name, category_name, product_lifecycle_name
			from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
			where product_type_name = 'Other') t
		inner join
			Warehouse.prod.dim_product_family_v pf on t.product_id_magento = pf.product_id_magento
		inner join
			Landing.aux.prod_product_product_type_oh ptoh on t.product_id_magento = ptoh.product_id
		inner join
			Landing.aux.prod_product_product_type_vat ptvat on t.product_id_magento = ptvat.product_id
	order by t.product_type_name, t.category_name, pf.product_type_name, pf.category_name, pf.product_family_name

--------------------------------------------------------------------------

	-- CHECK HISTORICAL ORDERS
	select pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status,  
		count(item_id) num_lines, count(distinct order_id) num_orders, sum(base_row_total) sum_total_orders, 
		min(ol.created_at) min_order_date, max(ol.created_at) min_order_date, 
		pf2.category_name
	from 
			Landing.mag.sales_flat_order_item_aud ol
		inner join
			(select product_type_name, category_name, 
				product_id_magento, magento_sku, product_family_name, 
				product_lifecycle_name, status, 
				flag_pf_cat, num_dist_param, 
				magentoSKU
			from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
			where product_type_name = 'Other') pf on ol.product_id = pf.product_id_magento
		inner join
			Warehouse.prod.dim_product_family_v pf2 on pf.product_id_magento = pf2.product_id_magento
	where pf2.category_name = 'Other'
	group by pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status, 
		pf2.category_name
	order by pf.product_lifecycle_name, pf.product_type_name, pf.category_name, pf.product_id_magento	

	

-------------------------------------------------------------------

	-- Orders in new DWH that belong to Conflicting Records (Ones used for reload)
	select pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status,
		count(order_line_id_bk) num_lines, count(distinct order_id_bk) num_orders, sum(local_subtotal) sum_total_orders, 
		min(ol.order_date_c) min_order_date, max(ol.order_date_c) min_order_date
	from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		(select product_type_name, category_name, 
			product_id_magento, magento_sku, product_family_name, 
			product_lifecycle_name, status, 
			flag_pf_cat, num_dist_param, 
			magentoSKU
		from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
		where product_type_name = 'Other') pf on ol.product_id_magento = pf.product_id_magento
	group by pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status
	order by pf.product_lifecycle_name, pf.product_type_name, pf.category_name, pf.product_id_magento	

	select ol.*
	from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		(select product_type_name, category_name, 
			product_id_magento, magento_sku, product_family_name, 
			product_lifecycle_name, status, 
			flag_pf_cat, num_dist_param, 
			magentoSKU
		from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
		where product_type_name = 'Other') pf on ol.product_id_magento = pf.product_id_magento
	where ol.product_id_magento not in (2949, 2950, 3124, 3125, 3158)
	order by ol.product_id_magento, ol.order_line_id_bk
