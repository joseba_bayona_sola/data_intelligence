

select product_id_magento, product_family_name, 
	product_type_name, category_name
from Warehouse.prod.dim_product_family_v
where category_name in ('Daily', 'Two Weeklies', 'Monthlies')
order by product_family_name
order by category_name, product_family_name

-----

select product_type_name, category_name, 
	product_id_magento, magento_sku, product_family_name, 
	category_magento, 
	num_lines, num_orders 
from DW_GetLenses_jbs.dbo.ex_product_family_analysis_category_rel
order by category_magento, product_family_name
