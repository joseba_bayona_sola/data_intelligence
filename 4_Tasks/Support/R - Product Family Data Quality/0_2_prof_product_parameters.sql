
	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status
	from Warehouse.prod.dim_product_family_v pf
	order by product_id_magento desc

	select top 1000 *
	from Landing.mag.edi_stock_item_aud
	where product_id = 3169

	select top 1000 *
	from Landing.aux.mag_edi_stock_item
	where product_id = 1332

	-------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------

	-- PF that don't have matching records in edi_stock_item
	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, esi.num_dist_param
	from 
			Warehouse.prod.dim_product_family_v pf
		left join
			(select product_id, count(*) num_dist_param
			from Landing.aux.mag_edi_stock_item
			group by product_id) esi on pf.product_id_magento = esi.product_id	
	where esi.product_id is null
	order by product_lifecycle_name, product_type_name, category_name, product_id_magento


	-------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------------

	select esi.bc, p.base_curve, esi.num
	from
			(select bc, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by bc) esi
		left join
			Landing.map.prod_param_base_curve_aud p on esi.bc = p.base_curve
	order by esi.bc

	select esi.di, p.diameter, esi.num
	from
			(select di, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by di) esi
		left join
			Landing.map.prod_param_diameter_aud p on esi.di = p.diameter
	order by esi.di

	select esi.po, p.power, esi.num
	from
			(select po, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by po) esi
		left join
			Landing.map.prod_param_power_aud p on esi.po = p.power
	order by esi.po



	select esi.cy, p.cylinder, esi.num
	from
			(select cy, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by cy) esi
		left join
			Landing.map.prod_param_cylinder_aud p on esi.cy = p.cylinder
	order by esi.cy

	select esi.ax, p.axis, esi.num
	from
			(select ax, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by ax) esi
		left join
			Landing.map.prod_param_axis_aud p on esi.ax = p.axis
	order by esi.ax



	select esi.ad, p.addition, esi.num
	from
			(select ad, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by ad) esi
		left join
			Landing.map.prod_param_addition_aud p on esi.ad = p.addition
	order by esi.ad

	select esi.do, p.dominance, esi.num
	from
			(select do, count(*) num
			from Landing.aux.mag_edi_stock_item
			group by do) esi
		left join
			Landing.map.prod_param_dominance_aud p on esi.do = p.dominance
	order by esi.do
