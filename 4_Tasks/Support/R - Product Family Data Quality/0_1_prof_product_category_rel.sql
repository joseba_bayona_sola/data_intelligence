
--------------------------------------------------------------------------------
---------------------------------------------------------------------------------
	-- CREATE TEMP TABLES

	select d1.entity_id category_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
	into #catalog_category_entity_path
    from 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d1 
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d2 ON d1.parent_id = d2.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d3 ON d2.parent_id = d3.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d4 ON d3.parent_id = d4.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d5 ON d4.parent_id = d5.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d6 ON d5.parent_id = d6.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d7 ON d6.parent_id = d7.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d8 ON d7.parent_id = d8.entity_id
	order by d1.entity_id;

	select product_id, name, category_name, count(*) num
	into #product_magento_category
	from
		(select product_id, name, category_num, category_name
		from
				(SELECT p.entity_id product_id, p.name,
					cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
				FROM 
						(select entity_id, name, product_type, telesales_only
						from Landing.mag.catalog_product_entity_flat
						where store_id = 0) p 
					LEFT JOIN 
						Landing.mag.catalog_category_product cpc ON p.entity_id = cpc.product_id
					LEFT JOIN 
						Landing.mag.catalog_category_entity c ON cpc.category_id = c.entity_id 
					LEFT JOIN 
						#catalog_category_entity_path cp ON c.entity_id = cp.category_id) t
			unpivot
				(category_name for category_num in 
					(name1, name2, name3, name4, name5, name6, name7, name8)
				) unpvt) t
	group by product_id, name, category_name


--------------------------------------------------------------------------------
---------------------------------------------------------------------------------
	-- SELECT TABLES

select *
from Landing.map.prod_category

select *
from #catalog_category_entity_path

select *
from #product_magento_category
-- where product_id = 2401
order by product_id, category_name

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
	-- CHECK FOR MAGENTO CATEGORY WORDS NO PRESENT IN MAP TABLE

select t1.category_name, t2.category, t1.num
from
		(select category_name, count(*) num
		from #product_magento_category
		group by category_name) t1
	left join
		Landing.map.prod_category t2 on t1.category_name = t2.category_magento
order by t1.category_name

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
	-- PRODUCT FAMILY - CATEGORY RELATION SELECT

		select p.entity_id product_id, isnull(pc.category, 'Other') category_bk
		from 
				Landing.mag.catalog_product_entity_flat p
			left join
				(select product_id, name, category
				from
					(select product_id, name, category,
						count(*) over (partition by product_id) num_rep, 
						rank() over (partition by product_id order by category) num_ord
					from
						(select distinct t1.product_id, t1.name, t2.category 
						from
								#product_magento_category t1
							inner join
								Landing.map.prod_category t2 on t1.category_name = t2.category_magento) t) t
				where num_ord = 1) pc on p.entity_id = pc.product_id
		where p.store_id = 0
		order by product_id;

		-- PRODUCT FAMILY - CATEGORY RELATION SELECT: PRODUCTS THAT ARE NOT MAPPED
		select p.entity_id product_id, p.name
		from 
				Landing.mag.catalog_product_entity_flat p
			left join
				(select product_id, name, category
				from
					(select product_id, name, category,
						count(*) over (partition by product_id) num_rep, 
						rank() over (partition by product_id order by category) num_ord
					from
						(select distinct t1.product_id, t1.name, t2.category 
						from
								#product_magento_category t1
							inner join
								Landing.map.prod_category t2 on t1.category_name = t2.category_magento) t) t
				where num_ord = 1) pc on p.entity_id = pc.product_id
		where p.store_id = 0
			and pc.category is null
		order by product_id;

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
	-- NEW DWH: CURRENT PRODUCT FAMILY AND CORRESPONDING CATEGORY - PRODUCT TYPE

	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, 
		case when (t.product_id is not null) then 1 else 0 end flag_pf_cat, 
		esi.num_dist_param, 
		m_p.magentoSKU
	from 
			Warehouse.prod.dim_product_family_v pf
		left join
			(select distinct product_id
			from #product_magento_category) t on pf.product_id_magento = t.product_id
		left join
			(select product_id, count(*) num_dist_param
			from Landing.aux.mag_edi_stock_item
			group by product_id) esi on pf.product_id_magento = esi.product_id	
		left join
			(select distinct magentoProductID_int, magentoSKU, name
			from Landing.mend.gen_prod_productfamilypacksize_v) m_p on pf.product_id_magento = m_p.magentoProductID_int
	order by product_lifecycle_name, product_type_name, category_name, flag_pf_cat, product_id_magento
	-- order by product_id_magento desc

---------------------------------------------------------------------------------
---------------------------------------------------------------------------------

	-- PRODUCT FAMILY THAT ARE SET TO OTHER BUT STILL GOT A MAGENTO CATEGORY RELATED

	select t.*, pmc.category_name
	from
			(select product_type_name, category_name, 
				product_id_magento, magento_sku, product_family_name, 
				product_lifecycle_name, status, 
				case when (t.product_id is not null) then 1 else 0 end flag_pf_cat
			from 
					Warehouse.prod.dim_product_family_v pf
				left join
					(select distinct product_id
					from #product_magento_category) t on pf.product_id_magento = t.product_id
			where product_type_name = 'Other' and t.product_id is not null) t
		inner join
			#product_magento_category pmc on t.product_id_magento = pmc.product_id
	where pmc.category_name in ('All Eye Care & Cosmetics', 'All Eye Care & Costmetics', 'All Solutions', 'Eye Care',
		'Contact Lenses', 'Monthly', 'Two Weeklies', 'Two Weekly Contact Lenses', 'Colours')
	order by t.product_lifecycle_name, t.product_type_name, t.category_name, t.flag_pf_cat, t.product_id_magento, pmc.category_name


select pmc.category_name, count(*)
from
		(select product_type_name, category_name, 
			product_id_magento, magento_sku, product_family_name, 
			product_lifecycle_name, status, 
			case when (t.product_id is not null) then 1 else 0 end flag_pf_cat
		from 
				Warehouse.prod.dim_product_family_v pf
			left join
				(select distinct product_id
				from #product_magento_category) t on pf.product_id_magento = t.product_id
		where product_type_name = 'Other' and t.product_id is not null) t
	inner join
		#product_magento_category pmc on t.product_id_magento = pmc.product_id
group by pmc.category_name
order by pmc.category_name


--------------------------------------------------------------------------
--------------------------------------------------------------------------
	
	-- CONFLICT PRODUCTS TO TEMP TABLE + CHECK HISTORICAL ORDERS

	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, 
		case when (t.product_id is not null) then 1 else 0 end flag_pf_cat, 
		esi.num_dist_param, 
		m_p.magentoSKU
	into DW_GetLenses_jbs.dbo.dim_product_family_v_201708
	from 
			Warehouse.prod.dim_product_family_v pf
		left join
			(select distinct product_id
			from #product_magento_category) t on pf.product_id_magento = t.product_id
		left join
			(select product_id, count(*) num_dist_param
			from Landing.aux.mag_edi_stock_item
			group by product_id) esi on pf.product_id_magento = esi.product_id	
		left join
			(select distinct magentoProductID_int, magentoSKU, name
			from Landing.mend.gen_prod_productfamilypacksize_v) m_p on pf.product_id_magento = m_p.magentoProductID_int
	order by product_lifecycle_name, product_type_name, category_name, flag_pf_cat, product_id_magento

	
	select product_type_name, category_name, 
		product_id_magento, magento_sku, product_family_name, 
		product_lifecycle_name, status, 
		flag_pf_cat, num_dist_param, 
		magentoSKU
	from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
	where product_type_name = 'Other'
	order by product_lifecycle_name, product_type_name, category_name, flag_pf_cat, product_id_magento

	select pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status,
		count(item_id) num_lines, count(distinct order_id) num_orders, sum(base_row_total) sum_total_orders, 
		min(ol.created_at) min_order_date, max(ol.created_at) min_order_date
	from 
		Landing.mag.sales_flat_order_item_aud ol
	inner join
		(select product_type_name, category_name, 
			product_id_magento, magento_sku, product_family_name, 
			product_lifecycle_name, status, 
			flag_pf_cat, num_dist_param, 
			magentoSKU
		from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
		where product_type_name = 'Other') pf on ol.product_id = pf.product_id_magento
	group by pf.product_type_name, pf.category_name, 
		pf.product_id_magento, pf.magento_sku, pf.product_family_name, 
		pf.product_lifecycle_name, pf.status
	order by pf.product_lifecycle_name, pf.product_type_name, pf.category_name, pf.product_id_magento	

	
	select t.*, pf.product_type_name, pf.category_name, pf.cl_type_name, pf.cl_feature_name,
		pf.product_lifecycle_name
	from
			(select product_id_magento, product_type_name, category_name, product_lifecycle_name
			from DW_GetLenses_jbs.dbo.dim_product_family_v_201708
			where product_type_name = 'Other') t
		inner join
			Warehouse.prod.dim_product_family_v pf on t.product_id_magento = pf.product_id_magento
	-- where t.product_type_name <> pf.product_type_name
	order by t.product_lifecycle_name, t.product_type_name, t.category_name, t.product_id_magento	
