
alter table Warehouse.sales.dim_order_header add customer_order_seq_no_web int;

alter table Warehouse.sales.dim_order_header_wrk add customer_order_seq_no_web int;


alter table Staging.sales.dim_order_header add customer_order_seq_no_web int;

----------------------------------------------------------

alter table Warehouse.act.fact_activity_sales add customer_order_seq_no_web int;

alter table Warehouse.act.fact_activity_sales_wrk add customer_order_seq_no_web int;


alter table Staging.act.fact_activity_sales add customer_order_seq_no_web int;

----------------------------------------------------------

alter table Warehouse.act.fact_activity_other drop constraint [UNIQ_act_fact_activity_other_activity_id_bk_idActivity_Type_sk_fk]

----------------------------------------------------------

alter table Warehouse.act.fact_customer_signature add idStoreCreate_sk_fk int;
alter table Warehouse.act.fact_customer_signature add idCalendarCreateDate_sk_fk int;

alter table Warehouse.act.fact_customer_signature add num_tot_orders_web int;

alter table Warehouse.act.fact_customer_signature add num_tot_lapsed int;
alter table Warehouse.act.fact_customer_signature add num_tot_reactivate int;

update Warehouse.act.fact_customer_signature
set num_tot_lapsed = 0
where num_tot_lapsed is null 

update Warehouse.act.fact_customer_signature
set num_tot_reactivate = 0
where num_tot_reactivate is null 

alter table Warehouse.act.fact_customer_signature add stdev_order_qty_time decimal(12, 4);
alter table Warehouse.act.fact_customer_signature add stdev_order_freq_time decimal(12, 4);


alter table Warehouse.act.fact_customer_signature_wrk add idStoreCreate_sk_fk int;
alter table Warehouse.act.fact_customer_signature_wrk add idCalendarCreateDate_sk_fk int;

alter table Warehouse.act.fact_customer_signature_wrk add num_tot_orders_web int;

alter table Warehouse.act.fact_customer_signature_wrk add num_tot_lapsed int;
alter table Warehouse.act.fact_customer_signature_wrk add num_tot_reactivate int;

alter table Warehouse.act.fact_customer_signature_wrk add stdev_order_qty_time decimal(12, 4);
alter table Warehouse.act.fact_customer_signature_wrk add stdev_order_freq_time decimal(12, 4);

---------------------------------------------------------------------------------------

alter table Landing.aux.act_fact_activity_sales add customer_order_seq_no_web int;

alter table Landing.aux.act_fact_activity_sales add prev_activity_date datetime;
alter table Landing.aux.act_fact_activity_sales add next_activity_date datetime;

alter table Landing.aux.act_fact_activity_other drop constraint [UNIQ_act_fact_activity_other_cust_act_type_act_seq_no]

---------------------------------------------------------------------------------------

alter table Warehouse.act.fact_customer_signature_aux_orders add num_tot_orders_web int;
alter table Warehouse.act.fact_customer_signature_aux_sales add num_tot_orders_web int;


alter table Warehouse.act.fact_customer_signature_aux_sales_main add stdev_order_qty_time decimal(12, 4);
alter table Warehouse.act.fact_customer_signature_aux_sales_main add stdev_order_freq_time decimal(12, 4);

alter table Warehouse.act.fact_customer_signature_aux_sales_main_product add stdev_order_qty_time decimal(12, 4);
alter table Warehouse.act.fact_customer_signature_aux_sales_main_freq add stdev_order_freq_time decimal(12, 4);


alter table Warehouse.act.fact_customer_signature_aux_other add idStoreCreate_sk_fk int;
alter table Warehouse.act.fact_customer_signature_aux_other add idCalendarCreateDate_sk_fk int;
alter table Warehouse.act.fact_customer_signature_aux_other add num_tot_lapsed int;
alter table Warehouse.act.fact_customer_signature_aux_other add num_tot_reactivate int;


