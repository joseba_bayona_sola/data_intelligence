
select count(distinct customer_id)
from Warehouse.act.fact_activity_sales_v
where activity_type_name = 'ORDER'


select top 1000 order_id_bk, activity_date, 
	website_group, store_name, customer_id, 
	customer_status_name, customer_order_seq_no
from Warehouse.act.fact_activity_sales_v
where activity_type_name = 'ORDER'

select top 1000 customer_id, count(distinct website_group) dist_website
from Warehouse.act.fact_activity_sales_v
where activity_type_name = 'ORDER'
group by customer_id
having count(distinct website_group) = 2
order by dist_website desc, customer_id

select dist_website, count(*)
from
	(select customer_id, count(distinct website_group) dist_website
	from Warehouse.act.fact_activity_sales_v
	where activity_type_name = 'ORDER'
	group by customer_id) t
group by dist_website
order by dist_website

----------------------------------------------------------------------

select top 1000 website_group, count(*)
from Warehouse.act.fact_activity_sales_v
where activity_type_name = 'ORDER'
	and customer_order_seq_no = 1
group by website_group
order by website_group

----------------------------------------------------------------------

select website_transition, count(*)
from
	(select customer_id, website_group, min_order, num_website_group,
		website_group + ' - ' + lead(website_group) over (partition by customer_id order by num_website_group) website_transition
	from
		(select customer_id, website_group, min_order, 
			rank() over (partition by customer_id order by min_order) num_website_group
		from
			(select distinct customer_id, website_group, 
				min(customer_order_seq_no) over (partition by customer_id, website_group) min_order
				-- lag(website_group) over (partition by customer_id)
			from Warehouse.act.fact_activity_sales_v
			where activity_type_name = 'ORDER'
				and customer_id in 
					(select customer_id
					from Warehouse.act.fact_activity_sales_v
					where activity_type_name = 'ORDER'
					group by customer_id
					having count(distinct website_group) = 2))t) t) t
where num_website_group = 1
group by website_transition
order by website_transition

