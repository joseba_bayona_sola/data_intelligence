
select top 1000 store_first, count(*)
from Warehouse.act.fact_customer_signature_v
where first_order_date < register_date
group by store_first
order by store_first

select idCustomer_sk_fk, customer_id, customer_email, 
	store_register, register_date, 
	store_first, first_order_date, 
	customer_status_name, num_tot_orders
from Warehouse.act.fact_customer_signature_v
where first_order_date < register_date
	and store_first = 'lenson.co.uk'


select top 1000 *
from Warehouse.act.fact_customer_signature_v
where first_order_date = register_date
