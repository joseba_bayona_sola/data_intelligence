
select top 1000 *
from Warehouse.gen.dim_customer_v
where customer_id_bk = 490286

select top 1000 
	count(*) over () num_tot,
	t.*, c.created_at
from
		(select count(*) over () num_tot,
			migra_website_name, etl_name, old_customer_id, new_customer_id, customer_email, 
			count(*) over (partition by new_customer_id) num_rep, 
			count(*) over (partition by new_customer_id, migra_website_name) num_rep_web
		from Landing.migra.match_customer) t
	inner join
		Warehouse.gen.dim_customer_v c on t.new_customer_id = c.customer_id_bk
order by num_rep desc, new_customer_id, etl_name, migra_website_name

select 
	count(*) over () num_tot_gen,
	t.*, c.created_at, c.created_in, c.old_access_cust_no, c.old_customer_id old_customer_id_magento, null created_at_migrate
into #match_magento_customers
from
		(select count(*) over () num_tot,
			migra_website_name, etl_name, old_customer_id, new_customer_id, customer_email, 
			count(*) over (partition by new_customer_id) num_rep, 
			count(*) over (partition by new_customer_id, migra_website_name) num_rep_web
		from Landing.migra.match_customer) t
	inner join
		Landing.mag.customer_entity_flat_aud c on t.new_customer_id = c.entity_id
order by num_rep desc, new_customer_id, etl_name, migra_website_name

select top 100 *
from #match_magento_customers
order by num_rep desc, new_customer_id, etl_name, migra_website_name

select etl_name, migra_website_name, count(*)
from #match_magento_customers
group by etl_name, migra_website_name
order by etl_name, migra_website_name

select c.old_customer_id, c.new_customer_id, c.customer_email, c.num_rep, c.num_rep_web,
	c.created_at, c.created_in, 
	cd.created_at
from 
	#match_magento_customers c
left join
	Landing.migra.migrate_customerdata_v cd on c.old_customer_id = cd.old_customer_id and cd.db = c.migra_website_name
where migra_website_name = 'vd150324'
order by c.old_customer_id
