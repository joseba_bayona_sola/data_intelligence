
select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate
from Landing.mag.sales_flat_invoice_aud
where order_id = 5164959

select *
from
	(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, 
		count(*) over (partition by order_id) dup_invoice
	from Landing.mag.sales_flat_invoice_aud
	where base_grand_total <> 0
	) t
where dup_invoice <> 1
	and store_id in (5, 22)
	and year(created_at) = 2017 and month(created_at) = 10
order by order_id, created_at

select store_id, count(*), sum(base_grand_total), min(created_at), max(created_at)
from
	(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, 
		count(*) over (partition by order_id) dup_invoice
	from Landing.mag.sales_flat_invoice_aud
	where base_grand_total <> 0
	) t
where dup_invoice <> 1
group by store_id
order by store_id


-------------------------------------------------------------


select store_id, year(created_at), month(created_at), count(*), 
	sum(base_grand_total), sum(base_grand_total) / 2
	-- sum(base_grand_total * base_to_global_rate), sum(base_grand_total * base_to_global_rate) / 2
from
	(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, order_currency_code,
		count(*) over (partition by order_id) dup_invoice
	from Landing.mag.sales_flat_invoice_aud
	where base_grand_total <> 0
	) t
where dup_invoice <> 1
	and store_id in (5, 22)
group by store_id, year(created_at), month(created_at)
order by store_id, year(created_at), month(created_at)

select entity_id, increment_id, order_id, store_id, created_at, base_grand_total * base_to_global_rate base_grand_total_global
from
	(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, 
		count(*) over (partition by order_id) dup_invoice
	from Landing.mag.sales_flat_invoice_aud
	where base_grand_total <> 0
	) t
where dup_invoice <> 1
	and store_id in (5, 22)
	and year(created_at) = 2017 and month(created_at) = 10
order by order_id desc, entity_id

-------------------------------------------------------------------------

select store_name, order_id_bk, order_no, 
	-- order_date, 
	invoice_date, 
	order_status_name, payment_method_name, 
	countries_registered_code, order_currency_code,
	local_subtotal, local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee
	-- global_subtotal, global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee
from 
	Warehouse.sales.dim_order_header_v oh
inner join
	(select distinct order_id
	from
		(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, 
			count(*) over (partition by order_id) dup_invoice
		from Landing.mag.sales_flat_invoice_aud
		where base_grand_total <> 0
		) t
	where dup_invoice <> 1
		and store_id in (5, 22)
		and year(created_at) in (2015, 2016, 2017)) t on oh.order_id_bk = t.order_id
order by order_id_bk 

select store_name, order_id_bk, order_no, 
	-- order_date, 
	invoice_date, 
	order_status_name, payment_method_name, 
	countries_registered_code, product_type_vat, order_currency_code,
	global_subtotal, global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
	vat_rate, prof_fee_rate
from 
	Warehouse.sales.fact_order_line_v ol
inner join
	(select distinct order_id
	from
		(select entity_id, increment_id, order_id, store_id, created_at, base_grand_total, base_to_global_rate, 
			count(*) over (partition by order_id) dup_invoice
		from Landing.mag.sales_flat_invoice_aud
		where base_grand_total <> 0
		) t
	where dup_invoice <> 1
		and store_id in (5, 22)
		and year(created_at) in (2015, 2016, 2017)) t on ol.order_id_bk = t.order_id
order by order_id_bk
