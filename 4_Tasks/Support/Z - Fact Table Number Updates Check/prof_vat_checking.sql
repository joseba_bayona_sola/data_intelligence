
drop table #vat_checking

select order_line_id_bk, order_id_bk, order_no, 
	shipment_date,
	website, customer_id, customer_email, 
	local_total_exc_vat, local_total_vat, local_total_inc_vat, local_total_prof_fee, 
	idETLBatchRun_ins, idETLBatchRun_upd 
into #vat_checking
from Warehouse.sales.fact_order_line_ship_v
where store_name = 'visiondirect.es'
	and shipment_date between '2017-06-01' and '2017-07-01'


select sum(local_total_exc_vat), sum(local_total_vat), sum(local_total_inc_vat), sum(local_total_prof_fee)
from #vat_checking

select *
from #vat_checking
-- where local_total_exc_vat < 0
order by idETLBatchRun_upd desc

select *
from #vat_checking
-- where local_total_exc_vat < 0
order by idETLBatchRun_ins desc
