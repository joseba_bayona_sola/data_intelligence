
select top 1000 ho.order_id, ho.source, ho.created_at, -- s.store_id, s.store_name,
	ho.grand_total, ho.customer_id
from 
	DW_GetLenses.dbo.dw_hist_order ho
inner join
	DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
where customer_id = 328400
order by created_at

select top 1000 *
from DW_GetLenses.dbo.dw_hist_order ho
where customer_id = 328400

select s.store_id, s.store_name, count(*)
from 
	DW_GetLenses.dbo.dw_hist_order ho
inner join
	DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
where ho.source = 'lensbase_db'
group by s.store_id, s.store_name
order by s.store_id, s.store_name
