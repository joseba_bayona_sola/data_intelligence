USE [DW_GetLenses]
GO

select *
from #FirstOrders

declare @from_date as date='01-jan-17', @to_date as date='31-jan-17'

	create table #FirstOrders(
		customer_id int,
		website varchar(100),
		src varchar(20))

	insert into #FirstOrders(customer_id)
		select distinct i.customer_id
		FROM v_invoices i 
		WHERE i.document_date BETWEEN @from_date AND @to_date

	create unique nonclustered index IX_FirstOrders on #FirstOrders(customer_id) include(website,src)

	update fo
	set fo.website = co.store_name, fo.src = co.src
	from
			#FirstOrders fo
		inner join
			(select o.customer_id,
				case o.source 
					when 'visio_db' then 'VisioOptik'
					when 'lensbase_db' then 'lensbase' + s.tld
					when 'masterlens_db' then 'masterlens' + s.tld
					else s.store_name 
				end as store_name,
				case when source = 'getlenses' then 'gl' else 'hist' end as src,
				row_number() over(partition by o.customer_id order by o.order_date asc) as rno
			from 
					(select ih.customer_id, 'getlenses' as source, store_id, order_date 
					from 
							invoice_headers ih 
						inner join 
							dw_stores_full s on s.store_name = ih.store_name 
						inner join 
							#FirstOrders fo on fo.customer_id = ih.customer_id
					union
					select olc.customer_id, source, store_id, created_at 
					from 
							dw_proforma.dbo.order_lifecycle olc 
						inner join 
							#FirstOrders fo on fo.customer_id = olc.customer_id
					union
					select oh.customer_id, source, store_id, created_at 
					from 
							dbo.dw_hist_order oh 
						inner join
							#FirstOrders fo on fo.customer_id=oh.customer_id) o
				inner join
					V_Stores_All s on s.store_id=o.store_id) co on co.customer_id = fo.customer_id and co.rno = 1

declare @from_date as date='01-jan-17', @to_date as date='31-jan-17'

select store_name, previous_site, order_no, customer_id, document_date, 
	sum(global_line_total_exc_vat) global_line_total_exc_vat, sum(global_line_total_vat) global_line_total_vat, sum(global_prof_fee) global_prof_fee, 
	sum(global_line_total_exc_vat - global_prof_fee) global_line_total_exc_vat_after_prof_fee
from
	(SELECT 
		i.store_name AS store_name, 
		CASE 
			when i.store_name = firstorder.website and firstorder.src = 'gl' then 'New'
			else firstorder.website 
		end AS previous_site,
		i.customer_id, i.order_no, i.document_date, 

		i.global_line_total_exc_vat, i.global_line_total_vat, i.global_prof_fee, 
		i.global_line_total_exc_vat - i.global_prof_fee global_line_total_exc_vat_after_prof_fee
	FROM 
			v_invoices i
		inner join 
			#FirstOrders firstorder on firstorder.customer_id = i.customer_id
	WHERE 
		i.document_date BETWEEN @from_date AND @to_date) t
-- where store_name = 'visiondirect.co.uk'
	-- and previous_site = 'getlenses.it'
group by store_name, previous_site, order_no, customer_id, document_date
order by store_name, previous_site, order_no, customer_id, document_date
