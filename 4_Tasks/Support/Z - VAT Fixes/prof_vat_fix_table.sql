
select *
from Landing.aux.sales_exchange_rate


select order_id_bk, order_no, order_date, 
	countries_registered_code, order_currency_code,
	local_total_inc_vat, local_total_exc_vat, local_total_vat
from Landing.aux.sales_dim_order_header oh
where order_source = 'M'
	and year(order_date) = 2017 
	and 
		((countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and order_currency_code = 'EUR') or
		(countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and order_currency_code <> 'EUR'))

select ol.order_line_id_bk, oh.order_id_bk, oh.order_no, oh.order_date, 
	oh.countries_registered_code, oh.order_currency_code, 
	-- oh.local_to_global_rate exchange_rate, ex.gbp_to_eur,
	case when (oh.order_currency_code = 'EUR') then oh.local_to_global_rate else ex.gbp_to_eur end exchange_rate,
	ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
	ol.local_store_credit_given, ol.local_bank_online_given, 
	ol.local_subtotal_refund, ol.local_shipping_refund, ol.local_discount_refund, ol.local_store_credit_used_refund, ol.local_adjustment_refund
from 
		Landing.aux.sales_dim_order_header oh
	inner join
		Landing.aux.sales_fact_order_line ol on oh.order_id_bk = ol.order_id_bk
	inner join
		Landing.aux.sales_exchange_rate ex on convert(date, oh.order_date) = ex.exchange_rate_day
where oh.order_source = 'M'
	and year(oh.order_date) = 2017 
	and 
		((oh.countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and oh.order_currency_code = 'EUR') or
		(oh.countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and oh.order_currency_code <> 'EUR'))

select order_line_id_bk, order_id_bk order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, exchange_rate,
	local_total_inc_vat * exchange_rate local_total_inc_vat_vf, local_total_exc_vat * exchange_rate local_total_exc_vat_vf, 
	local_total_vat * exchange_rate local_total_vat_vf, local_total_prof_fee * exchange_rate local_total_prof_fee_vf, 
	local_store_credit_given * exchange_rate local_store_credit_given_vf, local_bank_online_given * exchange_rate local_bank_online_given_vf, 
	local_subtotal_refund * exchange_rate local_subtotal_refund_vf, local_shipping_refund * exchange_rate local_shipping_refund_vf, local_discount_refund * exchange_rate local_discount_refund_vf, 
	local_store_credit_used_refund * exchange_rate local_store_credit_used_refund_vf, local_adjustment_refund * exchange_rate local_adjustment_refund_vf
from
	(select ol.order_line_id_bk, oh.order_id_bk, oh.order_no, oh.order_date, 
		oh.countries_registered_code, oh.order_currency_code, 
		case when (oh.order_currency_code = 'EUR') then oh.local_to_global_rate else ex.gbp_to_eur end exchange_rate,
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.local_store_credit_given, ol.local_bank_online_given, 
		ol.local_subtotal_refund, ol.local_shipping_refund, ol.local_discount_refund, ol.local_store_credit_used_refund, ol.local_adjustment_refund
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
		inner join
			Landing.aux.sales_exchange_rate ex on convert(date, oh.order_date) = ex.exchange_rate_day
	where oh.order_source = 'M'
		and year(oh.order_date) > 2017 
		and 
			((oh.countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and oh.order_currency_code = 'EUR') or
			(oh.countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and oh.order_currency_code <> 'EUR'))) t
