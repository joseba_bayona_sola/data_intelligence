
drop table Warehouse.sales.fact_order_line_vat_fix
go
drop table Warehouse.sales.fact_order_line_vat_fix_wrk
go


create table Warehouse.sales.fact_order_line_vat_fix(
	idOrderLine_sk_fk					int NOT NULL, 
	idOrderHeader_sk_fk					int NOT NULL,

	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),
	exchange_rate						decimal(12, 4),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [PK_sales_fact_order_line_vat_fix]
	primary key clustered (idOrderLine_sk_fk);
go

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [DF_sales_fact_order_line_vat_fix_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [UNIQ_sales_fact_order_line_vat_fix_order_line_id_bk]
	unique (order_line_id_bk);
go

alter table Warehouse.sales.fact_order_line_vat_fix add constraint [FK_sales_fact_order_line_vat_fix_fact_order_line]
	foreign key (idOrderLine_sk_fk) references Warehouse.sales.fact_order_line (idOrderLine_sk);
go



create table Warehouse.sales.fact_order_line_vat_fix_wrk(
	idOrderLine_sk_fk					int NOT NULL, 
	idOrderHeader_sk_fk					int NOT NULL,

	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 


alter table Warehouse.sales.fact_order_line_vat_fix_wrk add constraint [PK_sales_fact_order_line_vat_fix_wrk]
	primary key clustered (idOrderLine_sk_fk);
go

alter table Warehouse.sales.fact_order_line_vat_fix_wrk add constraint [DF_sales_fact_order_line_vat_fix_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- 

select idOrderLine_sk_fk, idOrderHeader_sk_fk, 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, 
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.sales.fact_order_line_vat_fix

select idOrderLine_sk_fk, idOrderHeader_sk_fk, 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, 
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun_ins, ins_ts
from Warehouse.sales.fact_order_line_vat_fix_wrk


	-- stg_dwh_merge_sales_order_line_vat_fix

	merge into Warehouse.sales.fact_order_line_vat_fix with (tablock) as trg
	using Warehouse.sales.fact_order_line_vat_fix_wrk src
		on (trg.idOrderLine_sk_fk = src.idOrderLine_sk_fk)
	when matched and not exists 
		(select 
			isnull(trg.idOrderHeader_sk_fk, 0), 
			isnull(trg.order_line_id_bk, 0), isnull(trg.order_id, 0), isnull(trg.order_no, ''), isnull(trg.order_date, ''), 
			isnull(trg.countries_registered_code, ''), isnull(trg.order_currency_code, ''), 
			isnull(trg.local_total_inc_vat_vf, 0), isnull(trg.local_total_exc_vat_vf, 0), isnull(trg.local_total_vat_vf, 0), isnull(trg.local_total_prof_fee_vf, 0), 
			isnull(trg.local_store_credit_given_vf, 0), isnull(trg.local_bank_online_given_vf, 0), 
			isnull(trg.local_subtotal_refund_vf, 0), isnull(trg.local_shipping_refund_vf, 0), isnull(trg.local_discount_refund_vf, 0), isnull(trg.local_store_credit_used_refund_vf, 0), isnull(trg.local_adjustment_refund_vf, 0)
		intersect
		select 
			isnull(src.idOrderHeader_sk_fk, 0), 
			isnull(src.order_line_id_bk, 0), isnull(src.order_id, 0), isnull(src.order_no, ''), isnull(src.order_date, ''), 
			isnull(src.countries_registered_code, ''), isnull(src.order_currency_code, ''), 
			isnull(src.local_total_inc_vat_vf, 0), isnull(src.local_total_exc_vat_vf, 0), isnull(src.local_total_vat_vf, 0), isnull(src.local_total_prof_fee_vf, 0), 
			isnull(src.local_store_credit_given_vf, 0), isnull(src.local_bank_online_given_vf, 0), 
			isnull(src.local_subtotal_refund_vf, 0), isnull(src.local_shipping_refund_vf, 0), isnull(src.local_discount_refund_vf, 0), isnull(src.local_store_credit_used_refund_vf, 0), isnull(src.local_adjustment_refund_vf, 0))
		
		then 
			update set
				trg.idOrderHeader_sk_fk = src.idOrderHeader_sk_fk, 
				trg.order_line_id_bk = src.order_line_id_bk, trg.order_id = src.order_id, trg.order_no = src.order_no, trg.order_date = src.order_date, 
				trg.countries_registered_code = src.countries_registered_code, trg.order_currency_code = src.order_currency_code, 
				trg.local_total_inc_vat_vf = src.local_total_inc_vat_vf, trg.local_total_exc_vat_vf = src.local_total_exc_vat_vf, 
				trg.local_total_vat_vf = src.local_total_vat_vf, trg.local_total_prof_fee_vf = src.local_total_prof_fee_vf, 
				trg.local_store_credit_given_vf = src.local_store_credit_given_vf, trg.local_bank_online_given_vf = src.local_bank_online_given_vf, 
				trg.local_subtotal_refund_vf = src.local_subtotal_refund_vf, trg.local_shipping_refund_vf = src.local_shipping_refund_vf, trg.local_discount_refund_vf = src.local_discount_refund_vf, 
				trg.local_store_credit_used_refund_vf = src.local_store_credit_used_refund_vf, trg.local_adjustment_refund_vf = src.local_adjustment_refund_vf, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (idOrderLine_sk_fk, idOrderHeader_sk_fk, 
				order_line_id_bk, order_id, order_no, order_date, 
				countries_registered_code, order_currency_code, 
				local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
				local_store_credit_given_vf, local_bank_online_given_vf, 
				local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf,
				idETLBatchRun_ins)

			values (src.idOrderLine_sk_fk, src.idOrderHeader_sk_fk, 
				src.order_line_id_bk, src.order_id, src.order_no, src.order_date, 
				src.countries_registered_code, src.order_currency_code, 
				src.local_total_inc_vat_vf, src.local_total_exc_vat_vf, src.local_total_vat_vf, src.local_total_prof_fee_vf, 
				src.local_store_credit_given_vf, src.local_bank_online_given_vf, 
				src.local_subtotal_refund_vf, src.local_shipping_refund_vf, src.local_discount_refund_vf, src.local_store_credit_used_refund_vf, src.local_adjustment_refund_vf, 
				@idETLBatchRun)

------------------------------------------------------

drop table Staging.sales.fact_order_line_vat_fix
go

create table Staging.sales.fact_order_line_vat_fix(
	order_line_id_bk					bigint NOT NULL,
	order_id							bigint NOT NULL,
	order_no							varchar(50) NOT NULL, 
	order_date							datetime NOT NULL,

	countries_registered_code			varchar(10), 
	order_currency_code					varchar(255),

	local_total_inc_vat_vf				decimal(12, 4),
	local_total_exc_vat_vf				decimal(12, 4),
	local_total_vat_vf					decimal(12, 4),
	local_total_prof_fee_vf				decimal(12, 4),

	local_store_credit_given_vf			decimal(12, 4),
	local_bank_online_given_vf			decimal(12, 4),
	local_subtotal_refund_vf			decimal(12, 4),
	local_shipping_refund_vf			decimal(12, 4),
	local_discount_refund_vf			decimal(12, 4),
	local_store_credit_used_refund_vf	decimal(12, 4),
	local_adjustment_refund_vf			decimal(12, 4),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Staging.sales.fact_order_line_vat_fix add constraint [PK_sales_fact_order_line_vat_fix]
	primary key clustered (order_line_id_bk);
go

alter table Staging.sales.fact_order_line_vat_fix add constraint [DF_sales_fact_order_line_vat_fix_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


select 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, 
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun_ins, ins_ts
from Staging.sales.fact_order_line_vat_fix

-- stg_dwh_get_sales_order_line_vat_fix
select 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, 
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf,
	@idETLBatchRun
from Staging.sales.fact_order_line_vat_fix