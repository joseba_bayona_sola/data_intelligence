
select idOrderHeader_sk, order_id_bk, order_no, order_date, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, 
	order_currency_code, local_to_global_rate
from Warehouse.sales.dim_order_header
where order_source = 'M'
	and year(order_date) = 2017 
	and 
		((countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and order_currency_code = 'EUR') or
		(countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and order_currency_code <> 'EUR'))

select top 1000 oh.idOrderHeader_sk, ol.idOrderLine_sk,
	oh.order_id_bk, oh.order_no, oh.order_date, 
	ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
	ol.local_subtotal_refund, ol.local_shipping_refund, ol.local_discount_refund, ol.local_store_credit_used_refund, ol.local_adjustment_refund, 
	ol.local_store_credit_given, ol.local_store_credit_used_refund,
	oh.order_currency_code, oh.local_to_global_rate
from 
		Warehouse.sales.dim_order_header oh
	inner join
		Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk
where oh.order_source = 'M'
	and year(oh.order_date) = 2017 
	and 
		((oh.countries_registered_code in ('UK-GB', 'EU-DK', 'EU-SE', 'EU-ZZ') and oh.order_currency_code = 'EUR') or
		(oh.countries_registered_code in ('EU-NL', 'EU-IE', 'EU-ES', 'EU-IT', 'EU-FR', 'EU-BE') and oh.order_currency_code <> 'EUR'))

select top 1000 *
from Warehouse.sales.fact_order_line_v
where website_group = 'VisionDirect'
	and year(order_date) = 2017 
	and countries_registered_code = 'UK-GB' and order_currency_code = 'EUR' -- and website <> 'visiondirect.co.uk'
