
select top 1000 year(order_date), month(order_date), day(order_date), local_to_global_rate, count(*), 
	count(*) over (partition by year(order_date), month(order_date), day(order_date)) num_rep
from Warehouse.sales.dim_order_header_v
where website_group = 'VisionDirect'
	and order_currency_code = 'EUR'
	and year(order_date) = 2017
	and order_status_name = 'OK'
group by year(order_date), month(order_date), day(order_date), local_to_global_rate
-- having local_to_global_rate = 1
order by num_rep desc, year(order_date), month(order_date), day(order_date), local_to_global_rate
-- order by num_rep desc, year(order_date), month(order_date), day(order_date), local_to_global_rate


select order_id_bk, order_no, order_date, website, customer_id, order_status_name, order_stage_name, local_to_global_rate
from Warehouse.sales.dim_order_header_v
where website_group = 'VisionDirect'
	and order_currency_code = 'EUR'
	-- and year(order_date) = 2017 and month(order_date) = 5 and day(order_date) = 4
	-- and year(order_date) = 2017 and month(order_date) = 1 and day(order_date) = 18
	-- and year(order_date) = 2017 and month(order_date) = 1 and day(order_date) = 30
	-- and year(order_date) = 2017 and month(order_date) = 10 and day(order_date) = 18
	and year(order_date) = 2015 and month(order_date) = 09 and day(order_date) = 23
order by local_to_global_rate, order_date

----------------------------------------------------------------------------

select year(order_date) yyyy, month(order_date) mm, day(order_date) dd, local_to_global_rate, count(*), 
	count(*) over (partition by year(order_date), month(order_date), day(order_date)) num_rep, 
	rank() over (partition by year(order_date), month(order_date), day(order_date) order by count(*)) ord_rep
from Warehouse.sales.dim_order_header_v
where website_group = 'VisionDirect'
	and order_currency_code = 'EUR'
	and year(order_date) = 2017
	and order_status_name = 'OK'
group by year(order_date), month(order_date), day(order_date), local_to_global_rate
order by year(order_date), month(order_date), day(order_date), count(*)

select exchange_rate_day, local_to_global_rate eur_to_gbp, convert(decimal(12, 4), 1 / local_to_global_rate) gbp_to_eur, 
	count(*) over (partition by exchange_rate_day) num_rep
from
	(select distinct exchange_rate_day, local_to_global_rate
	from
		(select convert(date, order_date) exchange_rate_day, local_to_global_rate, 
			count(*) over (partition by convert(date, order_date)) num_rep, 
			rank() over (partition by convert(date, order_date) order by count(*)) ord_rep
		from Warehouse.sales.dim_order_header_v
		where website_group = 'VisionDirect'
			and order_currency_code = 'EUR'
			-- and year(order_date) = 2016
			and order_status_name = 'OK'
		group by convert(date, order_date), local_to_global_rate) t
	where num_rep = ord_rep) t
-- where local_to_global_rate is null
order by num_rep desc, exchange_rate_day

----------------------------------

select year(exchange_rate_day), count(*), avg(gbp_to_eur), min(gbp_to_eur), max(gbp_to_eur)
from Warehouse.sales.dim_exchange_rate
group by year(exchange_rate_day)
order by year(exchange_rate_day)

select year(exchange_rate_day), month(exchange_rate_day), count(*), avg(gbp_to_eur), min(gbp_to_eur), max(gbp_to_eur)
from Warehouse.sales.dim_exchange_rate
where year(exchange_rate_day) = 2017
group by year(exchange_rate_day), month(exchange_rate_day)
order by year(exchange_rate_day), month(exchange_rate_day)

select *
from Warehouse.sales.dim_exchange_rate
where eur_to_gbp = 1