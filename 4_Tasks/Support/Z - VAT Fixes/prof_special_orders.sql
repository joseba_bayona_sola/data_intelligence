
select order_id_bk, order_no, order_date, 
	website, customer_id, customer_email, country_code_ship, country_code_bill, countries_registered_code, 
	order_status_name, order_stage_name, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, 
	global_total_inc_vat, global_total_exc_vat, global_total_vat, 
	order_currency_code, local_to_global_rate
from Warehouse.sales.dim_order_header_v
where website_group = 'VisionDirect'
	and year(order_date) = 2017 -- and order_stage_name <> 'ORDER'
	-- and countries_registered_code = 'UK-GB' and order_currency_code = 'EUR' -- and website <> 'visiondirect.co.uk'
	and countries_registered_code = 'EU-NL' and order_currency_code <> 'EUR' 
	-- and countries_registered_code = 'EU-IE' and order_currency_code <> 'EUR' 
	-- and countries_registered_code = 'EU-ES' and order_currency_code <> 'EUR' 
	-- and countries_registered_code = 'EU-IT' and order_currency_code <> 'EUR' 
	-- and countries_registered_code = 'EU-FR' and order_currency_code <> 'EUR' 
	-- and countries_registered_code = 'EU-BE' and order_currency_code <> 'EUR' 
	-- and countries_registered_code in ('EU-DK', 'EU-SE') and order_currency_code = 'EUR' 
	-- and countries_registered_code = 'EU-ZZ' and order_currency_code = 'EUR' 
order by website, order_date

-- local_total_inc_vat - oh.local_total_exc_vat - oh.local_total_vat - oh.local_total_prof_fee 

-- local_total_inc_vat_m (local_subtotal_refund - local_shipping_refund - local_discount_refund - local_store_credit_used_refund - local_adjustment_refund 
--	- local_store_credit_given - local_store_credit_used_refund)