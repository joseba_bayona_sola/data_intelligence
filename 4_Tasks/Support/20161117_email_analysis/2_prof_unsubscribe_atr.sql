
select top 1000 customer_id, 
	created_at, updated_at, 
	website_group, 
	unsubscribe_all, unsubscribe_all_date, 
	DATEDIFF(d, created_at, unsubscribe_all_date)
from DW_GetLenses.dbo.customers
where 
	--unsubscribe_all in ('YES', '1')
	unsubscribe_all in ('NO', '0')
order by customer_id desc
	
	-- Profiling Attributes
	select num_tot, unsubscribe_all, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc, 
		min(created_at), max(created_at)
	from 
		(select count(*) over () num_tot, *
		from DW_GetLenses.dbo.customers) t
	group by num_tot, unsubscribe_all
	order by num desc

	select num_tot, sub_date, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, 
			case when (unsubscribe_all_date is null) then 'NULL' else 'NOT_NULL' end sub_date
		from DW_GetLenses.dbo.customers) t
	group by num_tot, sub_date
	order by num desc

	select num_tot, sub_date, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc
	from 
		(select count(*) over () num_tot, unsubscribe_all,
			case when (unsubscribe_all_date is null) then 'NULL' else 'NOT_NULL' end sub_date
		from DW_GetLenses.dbo.customers) t
	group by num_tot, unsubscribe_all, sub_date
	order by num desc
