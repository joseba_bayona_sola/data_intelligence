
select t1.store_name, t1.yyyy, t1.mm, t1.num num_month, sum(t2.num) num_acc_month, 
	t1.num * 100 / convert(decimal(10,2), sum(t2.num)) perc_month 
from 
		(select rank() over (partition by store_name order by yyyy, mm) rank_num,
			store_name, yyyy, mm, count(*) num
		from 
			(select c.customer_id, c.store_name, YEAR(created_at) yyyy, MONTH(created_at) mm
			from 
					DW_GetLenses.dbo.customers c
				left join 
					DW_GetLenses.dbo.order_headers oh on c.customer_id = oh.customer_id
			where oh.order_id is null) t
		group by store_name, yyyy, mm) t1
	left join 
		(select rank() over (partition by store_name order by yyyy, mm) rank_num,
			store_name, yyyy, mm, count(*) num
		from 
			(select c.customer_id, c.store_name, YEAR(created_at) yyyy, MONTH(created_at) mm
			from 
					DW_GetLenses.dbo.customers c
				left join 
					DW_GetLenses.dbo.order_headers oh on c.customer_id = oh.customer_id
			where oh.order_id is null) t
		group by store_name, yyyy, mm) t2 on t1.store_name = t2.store_name and t1.rank_num >= t2.rank_num
where t1.store_name in ('visiondirect.be/fr', 'visiondirect.be/nl', 'visiondirect.co.uk', 'visiondirect.es', 'visiondirect.fr', 'visiondirect.ie', 'visiondirect.it', 'visiondirect.nl')
	and t1.yyyy in (2014, 2015, 2016)
group by t1.store_name, t1.yyyy, t1.mm, t1.num
order by t1.store_name, t1.yyyy, t1.mm