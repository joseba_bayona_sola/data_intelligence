
-- Attributes Needed for Query
select top 1000 t.num_tot,
	oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, oh_cc.order_date, 
	oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
	oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
from 
		DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
	left join 
		DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

		(select count(distinct customer_id) num_tot
		from DW_GetLenses_jbs.dbo.order_headers_disc2) t

--------	
-- Attributes Needed for Query + NEXT ORDER values (DATE, CC, CHANNEL)
select top 1000 count(*) over () num_tot_def, t.num_tot,
	oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
	oh_cc.order_date, 
	lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
	oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
	lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
	lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

	oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
from 
		DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
	left join 
		DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

		(select count(distinct customer_id) num_tot
		from DW_GetLenses_jbs.dbo.order_headers_disc2) t
order by oh_cc.customer_id

-- Attributes Needed for Query + NEXT ORDER values (DATE, CC, CHANNEL) for ONLY 1ST ORDERS
select top 1000 count(*) over () num_tot_def, *, 
	ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel, 
	case when (channel is NULL) then 0 else 1 end disc_1, case when (next_channel is NULL) then 0 else 1 end disc_2,
	datediff(day, order_date, next_order_date) diff_order_dates
from 
	(select t.num_tot,
		oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
		oh_cc.order_date, 
		lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
		oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
		lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
		lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

		oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
	from 
			DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
		left join 
			DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

			(select count(distinct customer_id) num_tot
			from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
where t.rank_ord_cust = 1
order by t.customer_id

--------------------------------------------------------------

	-- COUNTS ands % per Trans-Channel
	select num_tot_def, trans_channel, disc_1, disc_2, count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_def) perc_num_tot
	from 
		(select count(*) over () num_tot_def, *, 
			ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel,
			case when (channel is NULL) then 0 else 1 end disc_1, 
			case when (next_channel is NULL) then 0 else 1 end disc_2, 
			datediff(day, order_date, next_order_date) diff_order_dates
		from 
			(select t.num_tot,
				oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
				oh_cc.order_date, 
				lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
				oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
				lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
				lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

				oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
			from 
					DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
				left join 
					DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

					(select count(distinct customer_id) num_tot
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
		where t.rank_ord_cust = 1) t
	group by num_tot_def, trans_channel, disc_1, disc_2
	order by disc_1, disc_2, trans_channel

	-- COUNTS ands % per Disc1->Disc2
	select num_tot_def, disc_1, disc_2, count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_def) perc_num_tot
	from 
		(select count(*) over () num_tot_def, *, 
			ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel,
			case when (channel is NULL) then 0 else 1 end disc_1, 
			case when (next_channel is NULL) then 0 else 1 end disc_2, 
			datediff(day, order_date, next_order_date) diff_order_dates
		from 
			(select t.num_tot,
				oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
				oh_cc.order_date, 
				lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
				oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
				lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
				lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

				oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
			from 
					DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
				left join 
					DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

					(select count(distinct customer_id) num_tot
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
		where t.rank_ord_cust = 1) t
	group by num_tot_def, disc_1, disc_2
	order by disc_1, disc_2

------------------------------------

	-- COUNTS ands % per Trans-Channel + Num Orders (1-2)
	select num_tot_def, num_tot_def_cust, count_ord_cust_disc2, disc_1, disc_2, trans_channel, 
		count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_def) perc_num_tot, 
		count(*)*100/convert(decimal(10,2), num_tot_def_cust) perc_num_tot_cust, 
		avg(diff_order_dates) avg_diff_order_dates
	from 
		(select count(*) over () num_tot_def, *, 
			count(*) over (partition by count_ord_cust_disc2) num_tot_def_cust,
			ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel,
			case when (channel is NULL) then 0 else 1 end disc_1, 
			case when (next_channel is NULL) then 0 else 1 end disc_2, 
			datediff(day, order_date, next_order_date) diff_order_dates
		from 
			(select t.num_tot,
				oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
				oh_cc.order_date, 
				lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
				oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
				lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
				lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

				oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
			from 
					DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
				left join 
					DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

					(select count(distinct customer_id) num_tot
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
		where t.rank_ord_cust = 1) t
	group by num_tot_def, num_tot_def_cust, count_ord_cust_disc2, disc_1, disc_2, trans_channel
	order by count_ord_cust_disc2, disc_1, disc_2, trans_channel

	-- COUNTS ands % per Disc1->Disc2 + Num Orders (1-2)
	select num_tot_def, num_tot_def_cust, count_ord_cust_disc2, disc_1, disc_2, 
		count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_def) perc_num_tot, 
		count(*)*100/convert(decimal(10,2), num_tot_def_cust) perc_num_tot_cust 
	from 
		(select count(*) over () num_tot_def, *, 
			count(*) over (partition by count_ord_cust_disc2) num_tot_def_cust,
			ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel,
			case when (channel is NULL) then 0 else 1 end disc_1, 
			case when (next_channel is NULL) then 0 else 1 end disc_2, 
			datediff(day, order_date, next_order_date) diff_order_dates
		from 
			(select t.num_tot,
				oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
				oh_cc.order_date, 
				lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
				oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
				lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
				lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

				oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
			from 
					DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
				left join 
					DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

					(select count(distinct customer_id) num_tot
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
		where t.rank_ord_cust = 1) t
	group by num_tot_def, num_tot_def_cust, count_ord_cust_disc2, disc_1, disc_2
	order by count_ord_cust_disc2, disc_1, disc_2

------------------------------------

	-- COUNTS ands % per Disc1->Disc2 + Num Orders (ALL)
	select num_tot_def, num_tot_def_cust, count_ord_cust, disc_1, disc_2, 
		count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_def) perc_num_tot, 
		count(*)*100/convert(decimal(10,2), num_tot_def_cust) perc_num_tot_cust 
	from 
		(select count(*) over () num_tot_def, *, 
			count(*) over (partition by count_ord_cust) num_tot_def_cust,
			ISNULL(channel, 'NULL') + '->' + ISNULL(next_channel, 'NULL') trans_channel,
			case when (channel is NULL) then 0 else 1 end disc_1, 
			case when (next_channel is NULL) then 0 else 1 end disc_2, 
			datediff(day, order_date, next_order_date) diff_order_dates
		from 
			(select t.num_tot,
				oh_cc.store_name, oh_cc.order_id, oh_cc.order_no, oh_cc.document_type, 
				oh_cc.order_date, 
				lead(oh_cc.order_date) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_order_date,
				oh_cc.customer_id, cc.channel, oh_cc.coupon_code, 
				lead(cc.channel) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_channel,
				lead(oh_cc.coupon_code) over (partition by oh_cc.customer_id order by oh_cc.rank_ord_cust) next_coupon_code,

				oh_cc.count_ord_cust, oh_cc.count_ord_cust_disc2, oh_cc.rank_ord_cust
			from 
					DW_GetLenses_jbs.dbo.order_headers_disc2 oh_cc
				left join 
					DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code, 

					(select count(distinct customer_id) num_tot
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
		where t.rank_ord_cust = 1) t
	group by num_tot_def, num_tot_def_cust, count_ord_cust, disc_1, disc_2
	order by count_ord_cust, disc_1, disc_2