
select top 1000 count(*) over () num_tot,
	customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
from DW_GetLenses.dbo.customers

select yyyy, mm, count(*) num
from 
	(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
	from DW_GetLenses.dbo.customers) t
group by yyyy, mm
order by yyyy, mm


select yyyy, mm, num, 
	lag(num) over(order by yyyy, mm) num_next, 
	num + lag(num) over(order by yyyy, mm) sum
	--, lag(num + lag(num) over(order by yyyy, mm)) over (order by yyyy, mm)
from
	(select yyyy, mm, count(*) num
	from 
		(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
		from DW_GetLenses.dbo.customers) t
	group by yyyy, mm) t
order by yyyy, mm

select 
	t1.yyyy, t1.mm, t1.num, 
	t2.yyyy, t2.mm, t2.num 
from 
		(select rank() over (order by yyyy, mm) rank_num,
			yyyy, mm, count(*) num
		from 
			(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by yyyy, mm) t1
	left join 
		(select rank() over (order by yyyy, mm) rank_num,
			yyyy, mm, count(*) num
		from 
			(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by yyyy, mm) t2 
			-- on (t1.yyyy >= t2.yyyy and t1.mm > t2.mm) 
			-- on (t1.yyyy >= t2.yyyy and t1.mm > t2.mm) OR (t1.yyyy < t2.yyyy and t1.mm <= t2.mm)
			on (t1.rank_num >= t2.rank_num) 
order by t1.yyyy, t1.mm, t2.yyyy, t2.mm


-----------------------------------------------------------------------------

select t1.yyyy, t1.mm, t1.num num_month, sum(t2.num) num_acc_month, 
	t1.num * 100 / convert(decimal(10,2), sum(t2.num)) perc_month 
from 
		(select rank() over (order by yyyy, mm) rank_num,
			yyyy, mm, count(*) num
		from 
			(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by yyyy, mm) t1
	left join 
		(select rank() over (order by yyyy, mm) rank_num,
			yyyy, mm, count(*) num
		from 
			(select customer_id, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by yyyy, mm) t2 on t1.rank_num >= t2.rank_num
group by t1.yyyy, t1.mm, t1.num
order by t1.yyyy, t1.mm

-----------------------------------------------------------------------------

select t1.store_name, t1.yyyy, t1.mm, t1.num num_month, sum(t2.num) num_acc_month, 
	t1.num * 100 / convert(decimal(10,2), sum(t2.num)) perc_month 
from 
		(select rank() over (partition by store_name order by yyyy, mm) rank_num,
			store_name, yyyy, mm, count(*) num
		from 
			(select customer_id, store_name, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by store_name, yyyy, mm) t1
	left join 
		(select rank() over (partition by store_name order by yyyy, mm) rank_num,
			store_name, yyyy, mm, count(*) num
		from 
			(select customer_id, store_name, YEAR(created_at) yyyy, MONTH(created_at) mm
			from DW_GetLenses.dbo.customers) t
		group by store_name, yyyy, mm) t2 on t1.store_name = t2.store_name and t1.rank_num >= t2.rank_num
where t1.store_name in ('visiondirect.be/fr', 'visiondirect.be/nl', 'visiondirect.co.uk', 'visiondirect.es', 'visiondirect.fr', 'visiondirect.ie', 'visiondirect.it', 'visiondirect.nl')
	and t1.yyyy in (2014, 2015, 2016)
group by t1.store_name, t1.yyyy, t1.mm, t1.num
order by t1.store_name, t1.yyyy, t1.mm
