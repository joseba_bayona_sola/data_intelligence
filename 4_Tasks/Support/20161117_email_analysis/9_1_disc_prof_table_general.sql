
select top 1000 count(*) over (), *
from DW_GetLenses_jbs.dbo.order_headers_disc2

-- Order Dates
	select min(convert(date, order_date)), max(convert(date, order_date))
	from DW_GetLenses_jbs.dbo.order_headers_disc2

	select store_name, count(*), count(*)*100/convert(decimal(10,2), 1274195) perc_num_tot,
		min(convert(date, order_date)), max(convert(date, order_date))
	from DW_GetLenses_jbs.dbo.order_headers_disc2
	group by store_name
	order by store_name;

-- Num of Customers
	select count(distinct customer_id) -- 816202
	from DW_GetLenses_jbs.dbo.order_headers_disc2

	select store_name, count(distinct customer_id) 
	from DW_GetLenses_jbs.dbo.order_headers_disc2
	group by store_name
	order by store_name

-- Num Customers and Orders Made Total
	select num_tot, count_ord_cust, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot
	from
		(select count(*) over () num_tot, *
		from 
			(select distinct store_name, customer_id, count_ord_cust
			from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
	group by num_tot, count_ord_cust
	order by num_tot, count_ord_cust;

	select num_tot, num_tot_store, 
		num_tot_store*100/convert(decimal(10,2), num_tot) perc_num_tot,
		store_name, count_ord_cust, count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_store) perc_num_tot_store
	from
		(select 
			count(*) over () num_tot, 
			count(*) over (partition by store_name) num_tot_store, 
			*
		from 
			(select distinct store_name, customer_id, count_ord_cust
			from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
	group by num_tot, num_tot_store, store_name, count_ord_cust
	order by num_tot, store_name, count_ord_cust;

		select 
			t1.num_tot_store, t1.store_name, t1.count_ord_cust, t1.num, t1.perc_num_tot_store, 
			t2.num_tot_store, t2.store_name, t2.count_ord_cust, t2.num, t2.perc_num_tot_store
		from 
			(select num_tot, num_tot_store, 
				num_tot_store*100/convert(decimal(10,2), num_tot) perc_num_tot,
				store_name, count_ord_cust, count(*) num, 
				count(*)*100/convert(decimal(10,2), num_tot_store) perc_num_tot_store
			from
				(select 
					count(*) over () num_tot, 
					count(*) over (partition by store_name) num_tot_store, 
					*
				from 
					(select distinct store_name, customer_id, count_ord_cust
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
			group by num_tot, num_tot_store, store_name, count_ord_cust) t1
		inner join 
			(
			select 'gbp.getlenses.nl' t1_store, 'visiondirect.nl' t2_store
			union
			select 'getlenses.co.uk' t1_store, 'visiondirect.co.uk'
			union
			select 'getlenses.ie' t1_store, 'visiondirect.ie'
			union
			select 'getlenses.es' t1_store, 'visiondirect.es'
			union
			select 'getlenses.it' t1_store, 'visiondirect.it'
			) junc on t1.store_name = junc.t1_store
		inner join 	
			(select num_tot, num_tot_store, 
				num_tot_store*100/convert(decimal(10,2), num_tot) perc_num_tot,
				store_name, count_ord_cust, count(*) num, 
				count(*)*100/convert(decimal(10,2), num_tot_store) perc_num_tot_store
			from
				(select 
					count(*) over () num_tot, 
					count(*) over (partition by store_name) num_tot_store, 
					*
				from 
					(select distinct store_name, customer_id, count_ord_cust
					from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
			group by num_tot, num_tot_store, store_name, count_ord_cust) t2 on junc.t2_store = t2.store_name and t1.count_ord_cust = t2.count_ord_cust
		order by t1.store_name, t1.count_ord_cust;

-- Num Customers and Orders Made (1-2)
	select num_tot, count_ord_cust_disc2, count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot
	from
		(select count(*) over () num_tot, *
		from 
			(select distinct store_name, customer_id, count_ord_cust_disc2
			from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
	group by num_tot, count_ord_cust_disc2
	order by num_tot, count_ord_cust_disc2;

	select num_tot, num_tot_store, 
		num_tot_store*100/convert(decimal(10,2), num_tot) perc_num_tot,
		store_name, count_ord_cust_disc2, count(*) num, 
		count(*)*100/convert(decimal(10,2), num_tot_store) perc_num_tot_store
	from
		(select 
			count(*) over () num_tot, 
			count(*) over (partition by store_name) num_tot_store, 
			*
		from 
			(select distinct store_name, customer_id, count_ord_cust_disc2
			from DW_GetLenses_jbs.dbo.order_headers_disc2) t) t
	group by num_tot, num_tot_store, store_name, count_ord_cust_disc2
	order by num_tot, store_name, count_ord_cust_disc2;

-- Discount Overall
	select num_tot, coupon_code, count(*) num,
		count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot
	from 
		(select count(*) over () num_tot, coupon_code
		from DW_GetLenses_jbs.dbo.order_headers_disc2) t
	group by num_tot, coupon_code
	order by num_tot, coupon_code

	select oh_cc.num_tot, cc.channel, oh_cc.coupon_code, 
		oh_cc.num, oh_cc.perc_num_tot
	from 
			(select num_tot, coupon_code, count(*) num,
				count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot
			from 
				(select count(*) over () num_tot, coupon_code
				from DW_GetLenses_jbs.dbo.order_headers_disc2) t
			group by num_tot, coupon_code) oh_cc
		left join 
			DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code
	order by cc.channel, oh_cc.coupon_code, oh_cc.num

	select oh_cc.num_tot, cc.channel, sum(oh_cc.num) num, sum(oh_cc.num)*100/convert(decimal(10,2), oh_cc.num_tot) perc_num_tot
	from 
			(select num_tot, coupon_code, count(*) num,
				count(*)*100/convert(decimal(10,2), num_tot) perc_num_tot
			from 
				(select count(*) over () num_tot, coupon_code
				from DW_GetLenses_jbs.dbo.order_headers_disc2) t
			group by num_tot, coupon_code) oh_cc
		left join 
			DW_GetLenses.dbo.coupon_channel cc on oh_cc.coupon_code = cc.coupon_code
	group by oh_cc.num_tot, cc.channel
	order by cc.channel
