
select store_name, document_type, document_date, 
	shipment_id, shipment_no, 
	invoice_id, invoice_no, 
	order_id, order_no, 
	customer_id
from DW_GetLenses.dbo.shipment_headers
where store_name = 'visiondirect.fr' -- es, it, nl
	and document_date between '2016-11-18' and '2016-11-22'
order by document_date