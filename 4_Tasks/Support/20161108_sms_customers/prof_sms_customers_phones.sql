
select date, time, 
	country_code, phone_number, full_phone_number, 
	[To],
	Message, Status, 
	[First name], [Last name]
from DW_GetLenses_jbs.dbo.ex_sms_customers_phones;

	select phone_number, count(*)
	from DW_GetLenses_jbs.dbo.ex_sms_customers_phones
	group by phone_number
	order by count(*) desc

	select Message, Status, count(*)
	from DW_GetLenses_jbs.dbo.ex_sms_customers_phones
	group by Message, Status
	order by Message, Status 

-----------------------------------------------------------------

select 
	sms.date, sms.time, sms.phone_number_orig,
	c.customer_id, c.email, c.store_name, 
	c.firstname, c.lastname, 
	count(*) over (partition by sms.phone_number_orig) count_cust_phone_rep,
	count(*) over (partition by c.customer_id) count_cust_rep	
from 
		(select date, time, '0' + phone_number phone_number, phone_number phone_number_orig
		from DW_GetLenses_jbs.dbo.ex_sms_customers_phones) sms
	left join
		DW_GetLenses.dbo.customers c on sms.phone_number = c.cus_phone
order by sms.date, sms.time, c.customer_id

	select date, time, phone_number_orig, customer_id, 
		DENSE_RANK() over (order by date, time, phone_number_orig) 
	from 
		(select 
			sms.date, sms.time, sms.phone_number_orig,
			c.customer_id, c.email, c.store_name, 
			c.firstname, c.lastname, 
			count(*) over (partition by sms.phone_number_orig) count_cust_phone_rep,
			count(*) over (partition by c.customer_id) count_cust_rep	
		from 
				(select date, time, '0' + phone_number phone_number, phone_number phone_number_orig
				from DW_GetLenses_jbs.dbo.ex_sms_customers_phones) sms
			left join
				DW_GetLenses.dbo.customers c on sms.phone_number = c.cus_phone) t
	where 
		--t.count_cust_phone_rep = 1 and t.count_cust_rep = 1
		t.count_cust_phone_rep > 1 
		--t.count_cust_phone_rep = 1 and t.count_cust_rep > 1
	order by date, time, customer_id
