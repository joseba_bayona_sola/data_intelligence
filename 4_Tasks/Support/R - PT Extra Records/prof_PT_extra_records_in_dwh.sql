
select top 1000 idOrderHeader_sk_fk, sum(order_percentage)
from Warehouse.sales.dim_order_header_product_type
group by idOrderHeader_sk_fk
having sum(order_percentage) > 101
order by sum(order_percentage) desc

select top 1000 t.perc, ohpt.*
into DW_GetLenses_jbs.dbo.dim_order_header_product_type_wrong
from 
		Warehouse.sales.dim_order_header_product_type_v ohpt
	inner join
		(select top 1000 idOrderHeader_sk_fk, sum(order_percentage) perc
		from Warehouse.sales.dim_order_header_product_type
		group by idOrderHeader_sk_fk
		having sum(order_percentage) > 101) t on ohpt.idOrderHeader_sk = t.idOrderHeader_sk_fk
order by ohpt.order_id_bk

select *
from DW_GetLenses_jbs.dbo.dim_order_header_product_type_wrong

select ol.order_line_id_bk, ol.order_id_bk, 
	ol.product_type_name, ol.category_name, ol.product_id_magento, ol.product_family_name, 
	ol.local_subtotal
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		DW_GetLenses_jbs.dbo.dim_order_header_product_type_wrong ohpt on ol.order_id_bk = ohpt.order_id_bk
order by ol.order_id_bk, ol.order_line_id_bk

------------------------------------------------

-- After Fix: 
select top 1000 idOrderHeader_sk_fk, sum(order_percentage)
from Warehouse.sales.dim_order_header_product_type
where idOrderHeader_sk_fk in 
	(select distinct idOrderHeader_sk
	from DW_GetLenses_jbs.dbo.dim_order_header_product_type_wrong)
group by idOrderHeader_sk_fk
order by sum(order_percentage) desc

select top 1000 ohpt.*
from 
		Warehouse.sales.dim_order_header_product_type_v ohpt
	inner join
		DW_GetLenses_jbs.dbo.dim_order_header_product_type_wrong t on ohpt.idOrderHeader_sk = t.idOrderHeader_sk
order by ohpt.order_id_bk
