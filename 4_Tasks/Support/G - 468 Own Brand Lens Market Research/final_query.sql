
drop table #tmp_research_orders

create table #tmp_research_orders(
	store_name			varchar(100), 
	order_id			int, 
	order_no			varchar(50), 
	order_date			datetime,
	customer_id			int, 
	shipping_postcode	varchar(20), 
	shipping_city		varchar(100),
	product_id			int, 
	name				varchar(100))

go

insert into #tmp_research_orders
	select store_name, order_id, order_no, document_date, 
		customer_id, shipping_postcode, shipping_city, product_id, name
	from
		(select count(*) over (partition by customer_id) num_dist_products, *
		from
			(select distinct
				oh.num_tot, oh.num_tot_cust, oh.rank_cust,
				oh.store_name, oh.order_id, oh.order_no, oh.document_id, oh.document_date, oh.document_type, oh.customer_id, oh.shipping_postcode, oh.shipping_city,
				oi.product_id, oi.name--, oi.lens_power
			from
					(select count(*) over () num_tot, 
						count(*) over (partition by customer_id) num_tot_cust,
						rank() over (partition by customer_id order by order_id) rank_cust,
						store_name, order_id, order_no, document_id, document_date, document_type, customer_id, shipping_postcode, shipping_city
					from 
							DW_GetLenses.dbo.order_headers oh
						inner join
							(select distinct post_code_clean
							from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
					where document_date > GETUTCDATE() - 365
						and document_type = 'ORDER'
						and store_name = 'visiondirect.co.uk') oh
				inner join
					DW_GetLenses.dbo.order_lines oi on oh.document_id = oi.document_id and oh.document_type = oi.document_type
				left join
					(select distinct p.product_id, t.product_name
					from
							(select '1 Day Acuvue Trueye' product_name
							union
							select 'Clariti 1 day'
							union
							select 'Dailies total 1'
							union
							select 'MyDay'
							union
							select 'Acuvue Oasys'
							union
							select 'Acuvue Advance'
							union
							select 'Air optix aqua'
							union
							select 'Biofinity'
							union
							select 'Clariti'
							union
							select 'Purevision') t 
						inner join
							DW_GetLenses.dbo.products p on t.product_name = p.name) p on oi.product_id = p.product_id
			where p.product_id is not null
				and num_tot_cust = rank_cust) t) t
	where num_dist_products = 1

select ro.store_name, ro.order_id, ro.order_no, ro.order_date, 
	ro.customer_id, ro.shipping_postcode, ro.shipping_city, ro.product_id, ro.name, 
	rank() over (partition by ro.order_id, ro.product_id order by ol.lens_power, ol.line_id) num_power, 
	ol.lens_power
from 
		#tmp_research_orders ro
	inner join
		DW_GetLenses.dbo.order_lines ol on ro.order_id = ol.order_id and ro.product_id = ol.product_id

select store_name, order_id, order_no, order_date, 
	customer_id, shipping_postcode, shipping_city, product_id, name, [1] lens_power_1, [2] lens_power_2
from
	(select ro.store_name, ro.order_id, ro.order_no, ro.order_date, 
		ro.customer_id, ro.shipping_postcode, ro.shipping_city, ro.product_id, ro.name, 
		rank() over (partition by ro.order_id, ro.product_id order by ol.lens_power, ol.line_id) num_power, 
		ol.lens_power
	from 
			#tmp_research_orders ro
		inner join
			DW_GetLenses.dbo.order_lines ol on ro.order_id = ol.order_id and ro.product_id = ol.product_id) t
	pivot
		(min(lens_power) for 
		num_power in ([1], [2])) pvt
order by product_id, order_date

select t.customer_id, c.firstname, c.lastname, c.email, c.cus_phone, t.shipping_city, t.shipping_postcode, 
	t.product_id, t.name, t.lens_power_1, t.lens_power_2, t.order_date
from
		(select store_name, order_id, order_no, order_date, 
			customer_id, shipping_postcode, shipping_city, product_id, name, [1] lens_power_1, [2] lens_power_2
		from
			(select ro.store_name, ro.order_id, ro.order_no, ro.order_date, 
				ro.customer_id, ro.shipping_postcode, ro.shipping_city, ro.product_id, ro.name, 
				rank() over (partition by ro.order_id, ro.product_id order by ol.lens_power, ol.line_id) num_power, 
				ol.lens_power
			from 
					#tmp_research_orders ro
				inner join
					DW_GetLenses.dbo.order_lines ol on ro.order_id = ol.order_id and ro.product_id = ol.product_id) t
			pivot
				(min(lens_power) for 
				num_power in ([1], [2])) pvt) t
	inner join
		DW_GetLenses.dbo.customers c on t.customer_id = c.customer_id
order by t.product_id, t.order_date
