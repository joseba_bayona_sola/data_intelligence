
select top 1000 order_id, product_id, 
	rank() over (partition by order_id, product_id order by lens_power, line_id) num_powner, 
	lens_power
from DW_GetLenses.dbo.order_lines
where order_id = 4156475 and product_id = 1092

select order_id, product_id, [1], [2]
from
		(select top 1000 order_id, product_id, 
			rank() over (partition by order_id, product_id order by lens_power, line_id) num_power, 
			lens_power
		from DW_GetLenses.dbo.order_lines
		where order_id = 4506709 and product_id = 1099) t
	pivot
		(min(lens_power) for 
		num_power in ([1], [2])) pvt
