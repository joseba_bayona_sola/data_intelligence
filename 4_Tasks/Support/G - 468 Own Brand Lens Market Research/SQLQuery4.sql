
select *
from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius


-- 846.196
select top 100 count(*) over () num_tot, store_name, order_id, order_no, document_date, document_type, customer_id, shipping_postcode
from DW_GetLenses.dbo.order_headers
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'
	and store_name = 'visiondirect.co.uk'

-- 228.092
select top 100 count(*) over () num_tot, store_name, order_id, order_no, document_date, document_type, customer_id, shipping_postcode
from 
		DW_GetLenses.dbo.order_headers oh
	inner join
		(select distinct post_code_clean
		from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'
	and store_name = 'visiondirect.co.uk'

---------------------------------------------------------------

select top 100 count(*) over () num_tot, 
	count(*) over (partition by customer_id) num_tot_cust,
	rank() over (partition by customer_id order by order_id) rank_cust,
	store_name, order_id, order_no, document_date, document_type, customer_id, shipping_postcode
from 
		DW_GetLenses.dbo.order_headers oh
	inner join
		(select distinct post_code_clean
		from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'
	and store_name = 'visiondirect.co.uk'

---------------------------------------------

select top 1000 count(*) over () num_tot_2, 
	oh.num_tot, oh.num_tot_cust, oh.rank_cust,
	oh.store_name, oh.order_id, oh.order_no, oh.document_id, oh.document_date, oh.document_type, oh.customer_id, oh.shipping_postcode, 
	oi.line_id, oi.product_id, oi.sku, oi.name, 
	p.product_id
from
		(select count(*) over () num_tot, 
			count(*) over (partition by customer_id) num_tot_cust,
			rank() over (partition by customer_id order by order_id) rank_cust,
			store_name, order_id, order_no, document_id, document_date, document_type, customer_id, shipping_postcode
		from 
				DW_GetLenses.dbo.order_headers oh
			inner join
				(select distinct post_code_clean
				from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER'
			and store_name = 'visiondirect.co.uk') oh
	inner join
		DW_GetLenses.dbo.order_lines oi on oh.document_id = oi.document_id and oh.document_type = oi.document_type
	left join
		(select distinct p.product_id, t.product_name
		from
				(select '1 Day Acuvue Trueye' product_name
				union
				select 'Clariti 1 day'
				union
				select 'Dailies total 1'
				union
				select 'MyDay'
				union
				select 'Acuvue Oasys'
				union
				select 'Acuvue Advance'
				union
				select 'Air optix aqua'
				union
				select 'Biofinity'
				union
				select 'Clariti'
				union
				select 'Purevision') t 
			inner join
				DW_GetLenses.dbo.products p on t.product_name = p.name) p on oi.product_id = p.product_id
where p.product_id is not null
	and num_tot_cust = rank_cust
order by oh.customer_id, oh.order_id, oi.line_id

-- 

select count(*) over (partition by customer_id) num_dist_products, *
from
	(select distinct
		oh.num_tot, oh.num_tot_cust, oh.rank_cust,
		oh.store_name, oh.order_id, oh.order_no, oh.document_id, oh.document_date, oh.document_type, oh.customer_id, oh.shipping_postcode, 
		oi.product_id, oi.name
	from
			(select count(*) over () num_tot, 
				count(*) over (partition by customer_id) num_tot_cust,
				rank() over (partition by customer_id order by order_id) rank_cust,
				store_name, order_id, order_no, document_id, document_date, document_type, customer_id, shipping_postcode
			from 
					DW_GetLenses.dbo.order_headers oh
				inner join
					(select distinct post_code_clean
					from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER'
				and store_name = 'visiondirect.co.uk') oh
		inner join
			DW_GetLenses.dbo.order_lines oi on oh.document_id = oi.document_id and oh.document_type = oi.document_type
		left join
			(select distinct p.product_id, t.product_name
			from
					(select '1 Day Acuvue Trueye' product_name
					union
					select 'Clariti 1 day'
					union
					select 'Dailies total 1'
					union
					select 'MyDay'
					union
					select 'Acuvue Oasys'
					union
					select 'Acuvue Advance'
					union
					select 'Air optix aqua'
					union
					select 'Biofinity'
					union
					select 'Clariti'
					union
					select 'Purevision') t 
				inner join
					DW_GetLenses.dbo.products p on t.product_name = p.name) p on oi.product_id = p.product_id
	where p.product_id is not null
		and num_tot_cust = rank_cust) t

--------------------------------------------------------

select *
from
	(select count(*) over (partition by customer_id) num_dist_products, *
	from
		(select distinct
			oh.num_tot, oh.num_tot_cust, oh.rank_cust,
			oh.store_name, oh.order_id, oh.order_no, oh.document_id, oh.document_date, oh.document_type, oh.customer_id, oh.shipping_postcode, 
			oi.product_id, oi.name--, oi.lens_power
		from
				(select count(*) over () num_tot, 
					count(*) over (partition by customer_id) num_tot_cust,
					rank() over (partition by customer_id order by order_id) rank_cust,
					store_name, order_id, order_no, document_id, document_date, document_type, customer_id, shipping_postcode
				from 
						DW_GetLenses.dbo.order_headers oh
					inner join
						(select distinct post_code_clean
						from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
				where document_date > GETUTCDATE() - 365
					and document_type = 'ORDER'
					and store_name = 'visiondirect.co.uk') oh
			inner join
				DW_GetLenses.dbo.order_lines oi on oh.document_id = oi.document_id and oh.document_type = oi.document_type
			left join
				(select distinct p.product_id, t.product_name
				from
						(select '1 Day Acuvue Trueye' product_name
						union
						select 'Clariti 1 day'
						union
						select 'Dailies total 1'
						union
						select 'MyDay'
						union
						select 'Acuvue Oasys'
						union
						select 'Acuvue Advance'
						union
						select 'Air optix aqua'
						union
						select 'Biofinity'
						union
						select 'Clariti'
						union
						select 'Purevision') t 
					inner join
						DW_GetLenses.dbo.products p on t.product_name = p.name) p on oi.product_id = p.product_id
		where p.product_id is not null
			and num_tot_cust = rank_cust) t) t
where num_dist_products = 1


select product_id, name, count(*), min(convert(date, document_date)), max(convert(date, document_date))
from
	(select count(*) over (partition by customer_id) num_dist_products, *
	from
		(select distinct
			oh.num_tot, oh.num_tot_cust, oh.rank_cust,
			oh.store_name, oh.order_id, oh.order_no, oh.document_id, oh.document_date, oh.document_type, oh.customer_id, oh.shipping_postcode, 
			oi.product_id, oi.name
		from
				(select count(*) over () num_tot, 
					count(*) over (partition by customer_id) num_tot_cust,
					rank() over (partition by customer_id order by order_id) rank_cust,
					store_name, order_id, order_no, document_id, document_date, document_type, customer_id, shipping_postcode
				from 
						DW_GetLenses.dbo.order_headers oh
					inner join
						(select distinct post_code_clean
						from DW_GetLenses_jbs.dbo.lensway_postcodes_25_mile_radius) pc on substring(oh.shipping_postcode, 1, charindex(' ', oh.shipping_postcode)) = pc.post_code_clean
				where document_date > GETUTCDATE() - 365
					and document_type = 'ORDER'
					and store_name = 'visiondirect.co.uk') oh
			inner join
				DW_GetLenses.dbo.order_lines oi on oh.document_id = oi.document_id and oh.document_type = oi.document_type
			left join
				(select distinct p.product_id, t.product_name
				from
						(select '1 Day Acuvue Trueye' product_name
						union
						select 'Clariti 1 day'
						union
						select 'Dailies total 1'
						union
						select 'MyDay'
						union
						select 'Acuvue Oasys'
						union
						select 'Acuvue Advance'
						union
						select 'Air optix aqua'
						union
						select 'Biofinity'
						union
						select 'Clariti'
						union
						select 'Purevision') t 
					inner join
						DW_GetLenses.dbo.products p on t.product_name = p.name) p on oi.product_id = p.product_id
		where p.product_id is not null
			and num_tot_cust = rank_cust) t) t
where num_dist_products = 1
group by product_id, name
order by product_id, name
