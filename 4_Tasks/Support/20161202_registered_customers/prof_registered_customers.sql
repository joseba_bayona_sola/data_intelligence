
select top 100 *
from DW_GetLenses.dbo.customers; 

select top 100 customer_id, store_name, created_at, year(created_at) yyyyy, month(created_at) mm
from DW_GetLenses.dbo.customers; 

select store_name, yyyy, mm, count(*)
from 
	(select customer_id, store_name, created_at, year(created_at) yyyy, month(created_at) mm 
	from DW_GetLenses.dbo.customers) c
group by store_name, yyyy, mm
order by store_name, yyyy, mm;

select store_name, yyyy, mm, num
from 
	(select store_name, yyyy, mm, count(*) num
	from 
		(select customer_id, store_name, created_at, year(created_at) yyyy, month(created_at) mm 
		from DW_GetLenses.dbo.customers) c
	group by store_name, yyyy, mm) t
where store_name = 'visiondirect.co.uk' and yyyy = 2016
order by store_name, yyyy, mm;
