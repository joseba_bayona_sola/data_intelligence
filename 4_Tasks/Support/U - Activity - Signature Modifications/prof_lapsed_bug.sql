
select order_id_bk, activity_date, prev_activity_date, next_activity_date, datediff(day, activity_date, next_activity_date) diff_days,
	customer_id_bk,
	count(*) over ()
from Landing.aux.act_fact_activity_sales
where datediff(day, activity_date, next_activity_date) = 451 -- > 448
	and activity_type_name_bk = 'ORDER'
order by diff_days desc, activity_date

select (lapsed_num_days * -1) -1 start_range, (lapsed_num_days * -1) +1 finish_range, 
	dateadd(day, (lapsed_num_days * -1) -1, getutcdate()), 
	dateadd(day, (lapsed_num_days * -1) +1, getutcdate())
from Landing.map.act_lapsed_num_days

---------------------------------------------

			select activity_id_bk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), activity_date, 112))) idCalendarActivityDate, activity_date, 
				CONVERT(INT, (CONVERT(VARCHAR(8), lag(activity_date) over (partition by customer_id_bk order by activity_seq_no), 112))) idCalendarPrevActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				activity_seq_no
			from
				(select customer_id_bk activity_id_bk, 
					dateadd(day, l.lapsed_num_days, activity_date) activity_date,
					'LAPSED' activity_type_name_bk, store_id_bk, customer_id_bk, 
					dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) activity_seq_no
				from Landing.aux.act_fact_activity_sales, 
					Landing.map.act_lapsed_num_days l
				where (abs(datediff(day, activity_date, next_activity_date)) > l.lapsed_num_days and activity_type_name_bk = 'ORDER'))l
			order by activity_date

----------------------------------------------

select top 1000 *
from Warehouse.act.fact_activity_other
where (idETLBatchRun_ins = 1023 or idETLBatchRun_upd = 1023)
	and idActivityType_sk_fk = 4
order by activity_date

select top 1000 *
from Warehouse.act.fact_activity_other_v
where customer_id = 640606

select top 1000 *
from Warehouse.act.fact_activity_sales_v
where customer_id = 640606

select activity_date_c, activity_type_name, count(*)
from Warehouse.act.fact_activity_other_v
where activity_type_name = 'LAPSED'
group by activity_date_c, activity_type_name
order by activity_date_c desc

select top 1000 *
from Warehouse.act.fact_activity_other_v
where customer_id in (1562792, 1562790, 1562801)

select top 1000 *
from Warehouse.act.fact_activity_sales_v
where customer_id in (1562792, 1562790, 1562801)

-----------------------------------------

select *
from Warehouse.act.fact_activity_other_v
where activity_type_name = 'LAPSED'
	and activity_date_c = '2017-09-27'
