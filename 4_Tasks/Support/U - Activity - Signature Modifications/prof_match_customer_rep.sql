
select migra_website_name, etl_name, old_customer_id, new_customer_id, customer_email, 
	count(*) over (partition by new_customer_id) num_rep, 
	count(*) over (partition by new_customer_id, migra_website_name) num_rep_web
from Landing.migra.match_customer
order by num_rep desc, new_customer_id, etl_name, migra_website_name

select *
from
	(select migra_website_name, etl_name, old_customer_id, new_customer_id, customer_email, 
		count(*) over (partition by new_customer_id) num_rep, 
		count(*) over (partition by new_customer_id, migra_website_name) num_rep_web
	from Landing.migra.match_customer) mc
where num_rep > 1
order by num_rep desc, new_customer_id, etl_name, migra_website_name

select *
from
	(select migra_website_name, etl_name, old_customer_id, new_customer_id, customer_email, 
		count(*) over (partition by new_customer_id) num_rep, 
		count(*) over (partition by new_customer_id, migra_website_name) num_rep_web
	from Landing.migra.match_customer) mc
where num_rep = 1
order by num_rep desc, new_customer_id, etl_name, migra_website_name
