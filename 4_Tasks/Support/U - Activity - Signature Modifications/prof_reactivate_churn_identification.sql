
		select order_id_bk, 
			activity_date, previous_activity_date, next_activity_date, DATEDIFF(day, activity_date, isnull(next_activity_date, getutcdate())) diff_days,
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			case when (customer_order_seq_no = 1) then 'NEW' else 'REGULAR' end customer_status_name_bk,
			customer_order_seq_no
		from
			(select order_id_bk, 
				idCalendarActivityDate, activity_date, 
				lag(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarPrevActivityDate, 
				lag(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) previous_activity_date,
				lead(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) next_activity_date,
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				customer_status_name_bk, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) customer_order_seq_no
			from Landing.aux.act_fact_activity_sales) t
		where customer_id_bk = 68539
		order by diff_days desc
