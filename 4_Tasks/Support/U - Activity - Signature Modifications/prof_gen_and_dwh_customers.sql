
select *
from Warehouse.act.dim_activity_type_v
order by activity_group_name, activity_type_name

select top 1000
	idCustomer_sk, customer_id_bk, 
	created_at, store_id_bk, store_name, 
	migrate_customer_f, migrate_customer_store
from Warehouse.gen.dim_customer_v

select migrate_customer_f, migrate_customer_store, count(*)
from Warehouse.gen.dim_customer_v
group by migrate_customer_f, migrate_customer_store
order by migrate_customer_f, migrate_customer_store

select top 1000
	idCustomer_sk, customer_id_bk, 
	created_at, store_id_bk, store_name, 
	migrate_customer_f, migrate_customer_store
from Warehouse.gen.dim_customer_v
where migrate_customer_store = 'VisioOptik' -- Lensbase - VisioOptik - Lenson - Lensway
order by created_at

select top 1000 convert(date, created_at), count(*)
from Warehouse.gen.dim_customer_v
where migrate_customer_store = 'Lenson' -- Lensbase - VisioOptik - Lenson - Lensway
group by convert(date, created_at)
order by count(*) desc, convert(date, created_at)
