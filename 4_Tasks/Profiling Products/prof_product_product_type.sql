
-- Parameters

select TOP 100 op.product_id, op.name, 
	op.sku, op.product_code, 
	op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour, 
	op.qty,
	count(*) over () num
from 
		DW_Sales_Actual.dbo.Dim_Order_Parameters op
	left join
		dw_sales_actual.dbo.Dim_Products p on op.product_id = p.product_id
where p.product_id is null
order by product_id, product_code;

select TOP 1000 
	p.product_type, p.manufacturer, p.category,
	op.product_id, op.name, 
	op.sku, op.product_code, 
	op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour, 
	op.qty
from 
		DW_Sales_Actual.dbo.Dim_Order_Parameters op
	inner join
		dw_sales_actual.dbo.Dim_Products p on op.product_id = p.product_id
where p.product_type = 'Contact Lenses - Monthlies & Other' -- 'Contact Lenses - Monthlies & Other', 'Solutions & Eye Care', 'Other, 'Sunglasses'
order by p.product_type, p.category, p.manufacturer, product_id, product_code;

	select TOP 1000 
		op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour, count(*)
	from 
			DW_Sales_Actual.dbo.Dim_Order_Parameters op
		inner join
			dw_sales_actual.dbo.Dim_Products p on op.product_id = p.product_id
	where p.product_type = 'Solutions & Eye Care' -- 'Contact Lenses - Monthlies & Other', 'Solutions & Eye Care', 'Other, 'Sunglasses'
	group by op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour
	order by op.lens_base_curve, op.lens_diameter, op.lens_power, op.lens_cylinder, op.lens_axis, op.lens_addition, op.lens_dominance, op.lens_colour;

-- Qty Price
select TOP 1000 qp.UniqueIDQP, 
	qp.product_id, qp.name, 
	qp.name_qty, qp.name_packprice, qp.name_pricetype, 
	qp.qty, qp.pack_price, qp.price_type, 
	qp.lines, 
	qp.pack_qty, qp.total_qty
	--,count(*) over () num
from 
		DW_Sales_Actual.dbo.Dim_Order_Qty_Price qp
	left join
		dw_sales_actual.dbo.Dim_Products p on qp.product_id = p.product_id
where p.product_id is null
order by qp.product_id, qp.qty, qp.price_type, qp.pack_price

select distinct TOP 1000 
	-- qp.UniqueIDQP, 
	qp.product_id, qp.name, 
	qp.name_qty, qp.name_packprice, qp.name_pricetype, 
	qp.qty, qp.pack_price, qp.price_type, 
	qp.lines, 
	qp.pack_qty, qp.total_qty
	--,count(*) over () num
from 
		DW_Sales_Actual.dbo.Dim_Order_Qty_Price qp
	inner join
		dw_sales_actual.dbo.Dim_Products p on qp.product_id = p.product_id
where p.product_type = 'Solutions & Eye Care' -- 'Contact Lenses - Monthlies & Other', 'Solutions & Eye Care', 'Other, 'Sunglasses'
order by qp.product_id, qp.qty, qp.price_type, qp.pack_price