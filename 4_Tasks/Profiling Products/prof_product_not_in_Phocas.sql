
	-- In Phocas
	select product_type, manufacturer, product_id, store_name, name, sku, pack_qty, base_pack_qty, base_no_packs, weight
	from dw_getlenses.dbo.products
	where products.product_type not like 'glasses%'
		and store_name='Default'
	order by product_type, manufacturer, product_id

	select product_type, manufacturer, product_id, store_name, name, sku, pack_qty, base_pack_qty, base_no_packs, weight
	from dw_sales_actual.dbo.Dim_Products
	order by product_type, manufacturer, product_id

	------------------------------------------------------------
	 
	-- Products in DW_GetLenses not in DW_Sales_Actual
	select distinct p1.product_type, p1.manufacturer, p1.product_id, p1.name, p1.sku, p1.pack_qty, p1.base_pack_qty, p1.base_no_packs, p1.weight
		-- ,p1.store_name 
	from 
			dw_getlenses.dbo.products p1
		left join
			(select product_type, manufacturer, product_id, store_name, name, sku, pack_qty, base_pack_qty, base_no_packs, weight
			from dw_getlenses.dbo.products
			where products.product_type not like 'glasses%'
				and store_name='Default') p2 on p1.product_id = p2.product_id
	where p2.product_id is null	
	order by p1.product_type, p1.manufacturer, p1.product_id
		--, p1.store_name

	------------------------------------------------------------
	 
	-- Products in DW_GetLenses not in DW_Sales_Actual
	select distinct p0.product_id, p0.num,
		p1.product_type, p1.manufacturer, p1.product_id, p1.name, p1.sku, p1.pack_qty, p1.base_pack_qty, p1.base_no_packs, p1.weight
		-- ,p1.store_name 
	from 
			(select product_id, count(*) num
			from DW_Sales_Actual.dbo.Fact_Orders
			group by product_id) p0
		left join	
			dw_getlenses.dbo.products p1 on p0.product_id = p1.product_id
		left join
			(select product_type, manufacturer, product_id, store_name, name, sku, pack_qty, base_pack_qty, base_no_packs, weight
			from dw_getlenses.dbo.products
			where products.product_type not like 'glasses%'
				and store_name='Default') p2 on p1.product_id = p2.product_id
	where p1.product_id is null or p2.product_id is null	
	order by p1.product_type, p1.manufacturer, p1.product_id
		--, p1.store_name

	select p0.product_id, p0.num,
		fo.order_no, fo.local_line_total_inc_vat, 
		p1.product_type, p1.manufacturer, p1.product_id, p1.name, p1.sku, p1.pack_qty, p1.base_pack_qty, p1.base_no_packs, p1.weight
		-- ,p1.store_name 
	from 
			(select product_id, count(*) num
			from DW_Sales_Actual.dbo.Fact_Orders
			group by product_id) p0
		inner join 
			DW_Sales_Actual.dbo.Fact_Orders fo on p0.product_id = fo.product_id
		left join	
			dw_getlenses.dbo.products p1 on p0.product_id = p1.product_id
		left join
			(select product_type, manufacturer, product_id, store_name, name, sku, pack_qty, base_pack_qty, base_no_packs, weight
			from dw_getlenses.dbo.products
			where products.product_type not like 'glasses%'
				and store_name='Default') p2 on p1.product_id = p2.product_id
	where p1.product_id is null or p2.product_id is null	
	order by p1.product_type, p1.manufacturer, p1.product_id

