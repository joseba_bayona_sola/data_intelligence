
-- DW_Sales_Actual
select TOP 100 UniqueIDQP, 
	product_id, name, 
	name_qty, name_packprice, name_pricetype, 
	qty, pack_price, price_type, 
	lines, 
	pack_qty, total_qty
	--,count(*) over () num
from DW_Sales_Actual.dbo.Dim_Order_Qty_Price
where UniqueIDQP in ('OOR441835-1083-30.0000', 'OOR327478-1083-30.0000', 'OOR563667-1083-150.0000', 'OOR2236176-1083-150.0000', 'OOR4137714-1083-150.0000') or
	UniqueIDQP in ('OCR1220303-1083-28.0000', 'OOR3278400-1083-30.0000', 'OCR1220303-1083-25.0000', 'OCR1194432-1083-84.0000', 'OOR705067-1083-90.0000')
order by product_id, qty, price_type, pack_price

select TOP 100 product_id, name, 
	sku, product_code, 
	lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, 
	qty
	--,count(*) over () num
from DW_Sales_Actual.dbo.Dim_Order_Parameters
where 
	sku in ('1DAM90-BC8.5-DI14.2-PO-3.00-LQ30', '1DAM90-BC8.5-DI14.2-PO-3.50-LQ30', '1DAM90-BC9.0-DI14.2-PO-1.75-LQ30', '1DAM90-BC9.0-DI14.2-PO-2.25-LQ30', '1DAM90-BC9.0-DI14.2-PO-2.50-LQ30', '1DAM90-BC9.0-DI14.2-PO-2.75-LQ30') or 
	sku = '1DAM90-BC8.5-DI14.2-PO+1.25-LQ150' or 
	sku = '1DAM90-BC8.5-DI14.2-PO+2.75-LQ84' or 
	sku = '1DAM90-BC8.5-DI14.2-PO+2.75-LQ28'
order by product_id, product_code;

select top 1000 
	store_name, customer_id, product_id,
	UniqueID, UniqueIDQP, 
	order_no, invoice_no, 
	
	shipping_country_id, 

	document_type, document_date, document_day, document_time, document_hour, 
	
	shipping_carrier, 
	length_of_time_to_invoice, length_of_time_invoice_to_shipment, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, 
	referafriend_code, referafriend_referer, 
	presc_verification_method, 

	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	line_weight, 
	sku, sku_for_parameters, 

	qty, 
	local_prof_fee, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, 

	global_prof_fee, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount,

	local_to_global_rate, 
	global_line_total_exc_vat_for_localtoglobal, local_line_total_exc_vat_for_localtoglobal, 

	document_count, product_count, line_count

from DW_Sales_Actual.dbo.Fact_Orders
--where UniqueIDQP in ('OOR441835-1083-30.0000', 'OOR327478-1083-30.0000') or sku_for_parameters in ('1DAM90-BC8.5-DI14.2-PO+1.25-LQ150', '1DAM90-BC8.5-DI14.2-PO+2.75-LQ84', '1DAM90-BC8.5-DI14.2-PO+2.75-LQ28')
where order_no in ('1100000878', '1200596418', '1800000150', '8001028694', '8002037814', '400179232', '8001400591')
order by order_no, sku

----------------------------------------------------------------------------------------

-- DW_GetLenses
select 
	product_id, store_name,
	sku, equivalent_sku, replacement_sku, autoreorder_alter_sku, 
	status, 
	category_id, product_type, manufacturer, 
	
	product_lifecycle, product_lifecycle_text, 
	brand, telesales_only, stocked_lens, tax_class_id, 

	name, description, short_description, category_list_description, 
	meta_title, meta_keyword, meta_description, 
	image, small_image, thumbnail, url_key, url_path, visibility, 
	image_label, small_image_label, thumbnail_label, 

	average_cost, weight, 
	daysperlens, alt_daysperlens, equivalence, availability, condition,
	upsell_text, price_comp_price, rrp_price, 
	promotional_product, promotion_rule, promotion_rule2, promotional_text, 
	google_base_price, google_feed_title_prefix, google_feed_title_suffix, google_shopping_gtin
	
	glasses_colour, glasses_material, glasses_size, glasses_sex, glasses_style, glasses_shape, glasses_lens_width, glasses_bridge_width, 

	pack_qty, packtext, alt_pack_qty, 
	base_pack_qty, base_no_packs, 
	local_base_unit_price_inc_vat, local_base_unit_price_exc_vat, 
	local_base_value_inc_vat, local_base_value_exc_vat, 
	local_base_pack_price_inc_vat, local_base_pack_price_exc_vat, 	
	local_base_prof_fee, 
	local_base_product_cost, local_base_shipping_cost, local_base_total_cost, 
	local_base_margin_amount, local_base_margin_percent, 

	global_base_unit_price_inc_vat, global_base_unit_price_exc_vat, 
	global_base_value_inc_vat, global_base_value_exc_vat, 
	global_base_pack_price_inc_vat, global_base_pack_price_exc_vat, 	
	global_base_prof_fee, global_base_product_cost, global_base_shipping_cost, global_base_total_cost, 
	global_base_margin_amount, global_base_margin_percent, 

	multi_pack_qty, multi_no_packs, 
	local_multi_unit_price_inc_vat, local_multi_unit_price_exc_vat, 
	local_multi_value_inc_vat, local_multi_value_exc_vat, 
	local_multi_pack_price_inc_vat, local_multi_pack_price_exc_vat, 	
	local_multi_prof_fee, local_multi_product_cost, local_multi_shipping_cost, local_multi_total_cost, 
	local_multi_margin_amount, local_multi_margin_percent, 
	
	global_multi_unit_price_inc_vat, global_multi_unit_price_exc_vat, 
	global_multi_value_inc_vat, global_multi_value_exc_vat, 
	global_multi_pack_price_inc_vat, global_multi_pack_price_exc_vat, 
	global_multi_prof_fee, global_multi_product_cost, global_multi_shipping_cost, global_multi_total_cost, 
	global_multi_margin_amount, global_multi_margin_percent, 

	local_to_global_rate, store_currency_code, 
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, 

	created_at, updated_at,
	 
	count(*) over () num_tot			 
from DW_GetLenses.dbo.products
where product_id in (1083)
order by product_id, store_name;

select top 1000 
	order_id, order_no, 
	store_name, 

	order_type, order_lifecycle, 
	auto_verification, presc_verification_method, 

	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, approved_time, 

	business_source, business_channel, 
	affilBatch, affilCode, affilUserRef,
	telesales_method_code, telesales_admin_username, 

	customer_id, customer_email, 
	customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_gender, customer_dob, customer_taxvat, 
	customer_note, customer_note_notify, 
	customer_first_order_date, customer_business_channel, 
	customer_order_seq_no,  
	remote_ip, 

	status, coupon_code, 
	weight, 

	reorder_on_flag, reorder_profile_id, reorder_date, reorder_interval, 
	reorder_cc_exp_month, reorder_cc_exp_year, 
	automatic_reorder, 

	reminder_type, reminder_date, reminder_period, reminder_presc, reminder_mobile, reminder_sent, reminder_follow_sent,

	referafriend_code, referafriend_referer, 

	shipping_carrier, shipping_method, 

	payment_method, local_payment_authorized, local_payment_canceled, 
	cc_exp_month, cc_exp_year, cc_start_month, cc_start_year, cc_last4, cc_status_description, 
	cc_owner, cc_type, cc_status, cc_issue_no, cc_avs_status, 
	payment_info1, payment_info2, payment_info3, payment_info4, payment_info5, 

	billing_email, billing_company, 
	billing_prefix, billing_firstname, billing_middlename, billing_lastname, billing_suffix, 
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, 
	billing_telephone, billing_fax, 
	
	shipping_email, shipping_company, 
	shipping_prefix, shipping_firstname, shipping_middlename, shipping_lastname, shipping_suffix, 
	shiping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, 
	shipping_telephone, shipping_fax, 
	
	has_lens, total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, 
	
	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_subtotal_inc_vat, global_subtotal_vat, global_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_total_inc_vat, global_total_vat, global_total_exc_vat, 
	
	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 
	
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent
from DW_GetLenses.dbo.order_headers
where order_no in ('1100000878', '1200596418', '1800000150', '8001028694', '8002037814', '400179232', '8001400591')
-- where order_id in (327478, 563667, 441835, 2236176, 4137714, 705067, 3278400)
order by order_no;

select top 1000 
	line_id, 
	order_id, order_no, store_name, 
	
	document_id, document_type, document_date, document_updated_at, 
	order_date, updated_at, 

	product_id, category_id, 
	product_type, sku, name, 
	product_options, product_size, product_colour, 

	is_lens, 
	lens_eye, lens_base_curve, lens_diameter, lens_power, lens_cylinder, lens_axis, lens_addition, lens_dominance, lens_colour, lens_days, 

	unit_weight, line_weight, 

	price_type, is_virtual, free_shipping, no_discount, 

	qty, 
	local_prof_fee, 
	local_price_inc_vat, local_price_vat, local_price_exc_vat, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_exc_vat, 

	local_subtotal_cost, local_shipping_cost, local_total_cost, 
	local_margin_amount, local_margin_percent, 
	local_to_global_rate, order_currency_code, 

	global_prof_fee, 
	global_price_inc_vat, global_price_vat, global_price_exc_vat, 
	global_line_subtotal_inc_vat, global_line_subtotal_vat, global_line_subtotal_exc_vat, 
	global_shipping_inc_vat, global_shipping_vat, global_shipping_exc_vat, 
	global_discount_inc_vat, global_discount_vat, global_discount_exc_vat, 
	global_store_credit_inc_vat, global_store_credit_vat, global_store_credit_exc_vat, 
	global_adjustment_inc_vat, global_adjustment_vat, global_adjustment_exc_vat, 
	global_line_total_inc_vat, global_line_total_vat, global_line_total_exc_vat, 

	global_subtotal_cost, global_shipping_cost, global_total_cost, 
	global_margin_amount, global_margin_percent, 

	prof_fee_percent, vat_percent_before_prof_fee, vat_percent, discount_percent

from DW_GetLenses.dbo.order_lines
where order_no in ('1100000878', '1200596418', '1800000150', '8001028694', '8002037814', '400179232', '8001400591')

order by order_no;