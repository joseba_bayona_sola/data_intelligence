
use Landing
go

drop view mend.gen_invrec_invoice_v 
go 

create view mend.gen_invrec_invoice_v   as

	select id invoiceid, ia.accountid, w.warehouseid, s.supplier_id,
		w.code, w.name warehouse_name, 
		s.supplierID, s.supplierName, s.currencyCode,
		i.invoiceref, i.netsuiteno, i.invoicedate, i.status, i.paymentduedate, 
		i.invoicegoodsamount, i.invoicecarriageamount, i.invoicediscountamount, i.invoiceimportdutyamount, i.invoicenetamount,
		i.invoicegrossamount, i.invoicevatamount, i.invoicereconciledamount,
		i.erpgoodsamount, i.erpcarriageamount, i.erpdiscountamount, i.erpimportdutyamount, i.erpnetamount, i.erpgrossamount, i.erpvatamount, i.erpadjustedamount,
		i.netdifference, i.creditreceived, i.linetotaladjusted -- comment, auditcomment, attachmentscount, createddate
	from 
			Landing.mend.invrec_invoice i
		inner join
			Landing.mend.invrec_invoice_assignedto ia on i.id = ia.invoiceid
		inner join
			Landing.mend.invrec_invoice_warehouse iw on i.id = iw.invoiceid
		inner join	
			Landing.mend.gen_wh_warehouse_v w on iw.warehouseid = w.warehouseid
		inner join
			Landing.mend.invrec_invoice_supplier isu on i.id = isu.invoiceid
		left join
			Landing.mend.gen_supp_supplier_v s on isu.supplierid = s.supplier_id
go	

drop view mend.gen_invrec_invoicelinepoheader_v 
go 

create view mend.gen_invrec_invoicelinepoheader_v   as
	select i.invoiceid, i.accountid, i.warehouseid, i.supplier_id, ilpoh.id invoicelinepoheaderid, ilpohpo.purchaseorderid,
		i.code, i.warehouse_name, 
		i.supplierID, i.supplierName,
		i.invoiceref, i.netsuiteno, i.invoicedate, i.status, 
		ilpoh.index_int, ilpoh.adjustedtotalcost, ilpoh.totalquantity, ilpoh.validated, ilpoh.editable
	from 
			Landing.mend.gen_invrec_invoice_v i
		inner join
			Landing.mend.invrec_invoicelinepoheader_invoice ilpohi on i.invoiceid = ilpohi.invoiceid
		inner join
			Landing.mend.invrec_invoicelinepoheader ilpoh on ilpohi.invoicelinepoheaderid = ilpoh.id
		inner join 
			Landing.mend.invrec_invoicelinepoheader_purchaseorder ilpohpo on ilpoh.id = ilpohpo.invoicelinepoheaderid
go

drop view mend.gen_invrec_invoicelineheader_v 
go 

create view mend.gen_invrec_invoicelineheader_v   as
	select i.invoiceid, ilh.id invoicelineheaderid, i.accountid, i.warehouseid, i.supplier_id, ilhpf.productfamilyid, ilhps.packsizeid,
		i.code, i.warehouse_name, 
		i.supplierID, i.supplierName,
		i.invoiceref, i.netsuiteno, i.invoicedate, i.status, 
		pfps.magentoProductID_int, pfps.magentoProductID, pfps.name, pfps.size, 
		ilh.pricedifference, ilh.totalquantity, ilh.totalcost, ilh.adjustedtotalcost, ilh.adjustmentapplied, ilh.editable
	from 
			Landing.mend.gen_invrec_invoice_v i
		inner join
			Landing.mend.invrec_invoicelineheader_invoice ilhi on i.invoiceid = ilhi.invoiceid
		inner join
			Landing.mend.invrec_invoicelineheader ilh on ilhi.invoicelineheaderid = ilh.id
		inner join
			Landing.mend.invrec_invoicelineheader_productfamily ilhpf on ilh.id = ilhpf.invoicelineheaderid
		inner join
			Landing.mend.invrec_invoicelineheader_packsize ilhps on ilh.id = ilhps.invoicelineheaderid
		inner join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on ilhpf.productfamilyid = pfps.productfamilyid and ilhps.packsizeid = pfps.packsizeid
go

drop view mend.gen_invrec_invoiceline_v 
go 

create view mend.gen_invrec_invoiceline_v   as
	select ilh.invoiceid, ilh.invoicelineheaderid, il.id invoicelineid, ilh.accountid, ilh.warehouseid, ilh.supplier_id, ilh.productfamilyid, ilh.packsizeid, 
		ililpoh.invoicelinepoheaderid, ilrl.receiptlineid, rl.stockitemid,
		ilh.code, ilh.warehouse_name, 
		ilh.supplierID, ilh.supplierName,
		ilh.invoiceref, ilh.netsuiteno, ilh.invoicedate, ilh.status, 
		rl.receipt_id, rl.orderNumber, rl.stockitem, si.description, ilh.size,
		rl.quantityAccepted, il.quantity, 
		rl.unitcost receiptUnitCost, rl.invoiceUnitCost,
		il.unitcost, il.totalcost, il.adjustedunitcost, il.adjustedtotalcost, il.adjustmentapplied
	from 
			Landing.mend.gen_invrec_invoicelineheader_v ilh
		inner join
			Landing.mend.invrec_invoiceline_invoicelineheader ililh on ilh.invoicelineheaderid = ililh.invoicelineheaderid
		inner join
			Landing.mend.invrec_invoiceline il on ililh.invoicelineid = il.id
		inner join
			Landing.mend.invrec_invoiceline_invoicelinepoheader ililpoh on il.id = ililpoh.invoicelineid
		inner join
			Landing.mend.invrec_invoiceline_receiptline ilrl on il.id = ilrl.invoicelineid
		inner join
			Landing.mend.gen_whship_receiptline_1_v rl on ilrl.receiptlineid = rl.receiptlineid
		inner join
			Landing.mend.gen_prod_stockitem_v si on rl.stockitemid = si.stockitemid
go

drop view mend.gen_invrec_adjustmenthistory_v 
go 

create view mend.gen_invrec_adjustmenthistory_v as
	select a.id adjustmentid, ah.id adjustmenthistoryid, 
		a.adjustmenttype, a.submetaobjectname, a.amount,
		ah.createddate, ah.comment, ah.user_name
	from 
			Landing.mend.invrec_adjustment a
		inner join
			Landing.mend.invrec_adjustmenthistory_adjustment aha on a.id = aha.adjustmentid
		inner join
			Landing.mend.invrec_adjustmenthistory ah on aha.adjustmenthistoryid = ah.id
go

drop view mend.gen_invrec_invoiceadjustmenthistory_v 
go 

create view mend.gen_invrec_invoiceadjustmenthistory_v as

	select i.invoiceid, i.accountid, i.warehouseid, i.supplier_id, ah.adjustmentid, ah.adjustmenthistoryid,
		i.code, i.warehouse_name, 
		i.supplierID, i.supplierName,
		i.invoiceref, i.netsuiteno, i.invoicedate, i.status, 
		i.invoicegoodsamount, i.invoicecarriageamount, i.invoicenetamount, 
		i.invoicegrossamount, i.invoicevatamount, 
		i.erpgoodsamount, i.erpcarriageamount, i.erpnetamount, 
		i.erpadjustedamount,
		i.netdifference, i.creditreceived, 
		ah.adjustmenttype, ah.submetaobjectname, ah.amount,
		ah.createddate
	from 
			Landing.mend.gen_invrec_invoice_v i
		inner join
			Landing.mend.invrec_adjustmenthistory_invoice ahi on i.invoiceid = ahi.invoiceid
		inner join
			Landing.mend.gen_invrec_adjustmenthistory_v ah on ahi.adjustmenthistoryid = ah.adjustmenthistoryid
go

drop view mend.gen_invrec_invoicelineheaderadjustmenthistory_v 
go 

create view mend.gen_invrec_invoicelineheaderadjustmenthistory_v as

	select ilh.invoiceid, ilh.accountid, ilh.warehouseid, ilh.supplier_id, ilh.invoicelineheaderid, ilh.productfamilyid, ilh.packsizeid, ah.adjustmentid, ah.adjustmenthistoryid,
		ilh.code, ilh.warehouse_name, 
		ilh.supplierID, ilh.supplierName,
		ilh.invoiceref, ilh.netsuiteno, ilh.invoicedate, ilh.status, 
		ilh.magentoProductID_int, ilh.magentoProductID, ilh.name, ilh.size, 
		ilh.pricedifference, ilh.totalquantity, ilh.totalcost, ilh.adjustedtotalcost, ilh.adjustmentapplied, ilh.editable,
		ah.adjustmenttype, ah.submetaobjectname, ah.amount,
		ah.createddate
	from 
			Landing.mend.gen_invrec_invoicelineheader_v ilh
		inner join
			Landing.mend.invrec_adjustmenthistory_invoicelineheader ahilh on ilh.invoicelineheaderid = ahilh.invoicelineheaderid
		inner join
			Landing.mend.gen_invrec_adjustmenthistory_v ah on ahilh.adjustmenthistoryid = ah.adjustmenthistoryid
go

drop view mend.gen_invrec_invoicelineadjustmenthistory_v 
go 

create view mend.gen_invrec_invoicelineadjustmenthistory_v as
	select il.invoiceid, il.accountid, il.warehouseid, il.supplier_id, il.invoicelineheaderid, il.productfamilyid, il.packsizeid, 
		il.invoicelinepoheaderid, il.receiptlineid, il.stockitemid, ah.adjustmentid, ah.adjustmenthistoryid,
		il.code, il.warehouse_name, 
		il.supplierID, il.supplierName,
		il.invoiceref, il.netsuiteno, il.invoicedate, il.status, 
		il.receipt_id, il.orderNumber, il.stockitem, il.description, il.size,
		il.quantityAccepted, il.quantity, 
		il.receiptUnitCost, il.invoiceUnitCost,
		il.unitcost, il.totalcost, il.adjustedunitcost, il.adjustedtotalcost, il.adjustmentapplied, 
		ah.adjustmenttype, ah.submetaobjectname, ah.amount,
		ah.createddate
	from 
			Landing.mend.gen_invrec_invoiceline_v il
		inner join
			Landing.mend.invrec_adjustmenthistory_invoiceline ahil on il.invoicelineid = ahil.invoicelineid
		inner join
			Landing.mend.gen_invrec_adjustmenthistory_v ah on ahil.adjustmenthistoryid = ah.adjustmenthistoryid
go




drop view mend.gen_invrec_invoicelineheaderadjustment_v 
go 

create view mend.gen_invrec_invoicelineheaderadjustment_v as
	select ilha.id invoicelineheaderadjustmentid, 
		ilha.submetaobjectname, ilha.unitcost, ilha.adjustedunitcost, ilha.comment,
		ilhba.posubmitteddatefrom, ilhba.posubmitteddateto, ilhba.stockregistereddatefrom, ilhba.stockregistereddateto	
	from 
			Landing.mend.invrec_invoicelineheaderadjustment ilha
		inner join
			Landing.mend.invrec_invoicelineheaderbulkadjustment ilhba on ilha.id = ilhba.id
go

drop view mend.gen_invrec_invoicelineheaderadjustment_il_v 
go 

create view mend.gen_invrec_invoicelineheaderadjustment_il_v as
	select il.invoiceid, il.accountid, il.warehouseid, il.supplier_id, il.invoicelineheaderid, il.productfamilyid, il.packsizeid, 
		il.invoicelinepoheaderid, il.receiptlineid, il.stockitemid, ilha.invoicelineheaderadjustmentid,
		il.code, il.warehouse_name, 
		il.supplierID, il.supplierName,
		il.invoiceref, il.netsuiteno, il.invoicedate, il.status, 
		il.receipt_id, il.orderNumber, il.stockitem, il.description, il.size,
		il.quantityAccepted, il.quantity, 
		il.receiptUnitCost, il.invoiceUnitCost,
		il.unitcost, il.totalcost, il.adjustedunitcost, il.adjustedtotalcost, il.adjustmentapplied, 
		ilha.submetaobjectname, ilha.unitcost unitcost_ilha, ilha.adjustedunitcost adjustedunitcost_ilha, ilha.comment,
		ilha.posubmitteddatefrom, ilha.posubmitteddateto, ilha.stockregistereddatefrom, ilha.stockregistereddateto
	from 
			Landing.mend.gen_invrec_invoiceline_v il
		inner join
			Landing.mend.invrec_invoicelineheaderadjustment_invoiceline ilhail on il.invoicelineid = ilhail.invoicelineid
		inner join
			Landing.mend.gen_invrec_invoicelineheaderadjustment_v ilha on ilhail .invoicelineheaderadjustmentid = ilha.invoicelineheaderadjustmentid
go



drop view mend.gen_invrec_invoicecarriage_v 
go 

create view mend.gen_invrec_invoicecarriage_v  as

	select i.invoiceid, i.accountid, i.warehouseid, i.supplier_id, c.id carriageid,
		i.code, i.warehouse_name, 
		i.supplierID, i.supplierName,
		i.invoiceref, i.netsuiteno, i.invoicedate, i.status, 
		i.invoicegoodsamount, i.invoicecarriageamount, i.invoicenetamount, 
		i.invoicegrossamount, i.invoicevatamount, 
		i.erpgoodsamount, i.erpcarriageamount, i.erpnetamount, 
		i.erpadjustedamount,
		i.netdifference, i.creditreceived, 
		c.carriagetype, c.cost, 
		c.comment, 
		c.createddate, c.changeddate
	from 
			Landing.mend.gen_invrec_invoice_v i
		inner join
			Landing.mend.invrec_carriage_invoice ci on i.invoiceid = ci.invoiceid
		inner join
			Landing.mend.invrec_carriage c on ci.carriageid = c.id
go


drop view mend.gen_invrec_invoicecredit_v 
go 

create view mend.gen_invrec_invoicecredit_v   as

	select cr.id creditnoteid, s.supplier_id, ic.id invoicecreditid, i.invoiceid,
		cr.creditnoteno, cr.reference, 
		cr.totalamount, 
		s.supplierID, s.supplierName, 
		cr.status credit_note_status, 
		cr.creditdate, cr.createddate, cr.processeddate, 
		cr.goodscreditamount, cr.carriagecreditamount, cr.vatcreditamount, 
		i.invoiceref, i.status, i.invoicenetamount, i.erpnetamount, i.netdifference,
		ic.goodscreditamount goodscreditamount_i, ic.carriagecreditamount carriagecreditamount_i, 
		cr.comment
	from 
			Landing.mend.invrec_creditnote cr
		inner join
			Landing.mend.invrec_creditnote_supplier crs on cr.id = crs.creditnoteid
		inner join
			Landing.mend.gen_supp_supplier_v s on crs.supplierid = s.supplier_id
		inner join
			Landing.mend.invrec_invoicecredit_creditnote iccr on cr.id = iccr.creditnoteid
		inner join
			Landing.mend.invrec_invoicecredit ic on iccr.invoicecreditid = ic.id
		inner join
			Landing.mend.invrec_invoicecredit_invoice ici on ic.id = ici.invoicecreditid
		inner join
			Landing.mend.gen_invrec_invoice_v i on ici.invoiceid = i.invoiceid
go
