
select invoiceid, accountid, warehouseid, supplier_id,
	code, warehouse_name, 
	supplierID, supplierName, currencyCode,
	invoiceref, netsuiteno, invoicedate, status, paymentduedate, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, -- invoicediscountamount, invoiceimportdutyamount, 
	invoicegrossamount, invoicevatamount, -- invoicereconciledamount,
	erpgoodsamount, erpcarriageamount, erpnetamount, erpgrossamount, -- erpdiscountamount, erpimportdutyamount, 
	erpvatamount, erpadjustedamount,
	netdifference, creditreceived, linetotaladjusted
from Landing.mend.gen_invrec_invoice_v
-- where invoiceref = '111 73241682'
order by invoicedate, invoiceid

	select status, count(*)
	from Landing.mend.gen_invrec_invoice_v
	group by status
	order by status

	select warehouse_name, currencyCode, count(*)
	from Landing.mend.gen_invrec_invoice_v
	group by warehouse_name, currencyCode
	order by warehouse_name, currencyCode

select invoiceid, accountid, warehouseid, supplier_id, invoicelinepoheaderid, purchaseorderid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	index_int, adjustedtotalcost, totalquantity, validated, editable
from Landing.mend.gen_invrec_invoicelinepoheader_v 
where invoiceref = '06411643'
order by invoicedate, invoiceid, index_int

	select validated, editable, count(*)
	from Landing.mend.gen_invrec_invoicelinepoheader_v
	group by validated, editable
	order by validated, editable

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable
from Landing.mend.gen_invrec_invoicelineheader_v
where invoiceref = '06411643'
order by invoicedate, invoiceid, name

	select editable, count(*)
	from Landing.mend.gen_invrec_invoicelineheader_v
	group by editable
	order by editable

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied
from Landing.mend.gen_invrec_invoiceline_v
-- where invoiceref = '06411643'
-- where receiptlineid = 74590868831844081
where adjustmentapplied <> 0
order by invoicedate, invoiceid, description, orderNumber, receipt_id

	select receiptlineid, count(*)
	from Landing.mend.gen_invrec_invoiceline_v
	group by receiptlineid
	order by count(*) desc

select adjustmentid, adjustmenthistoryid, 
	adjustmenttype, submetaobjectname, amount,
	createddate, comment, user_name
from Landing.mend.gen_invrec_adjustmenthistory_v

	select adjustmenttype, count(*)
	from Landing.mend.gen_invrec_adjustmenthistory_v
	group by adjustmenttype
	order by adjustmenttype

	select submetaobjectname, count(*)
	from Landing.mend.gen_invrec_adjustmenthistory_v
	group by submetaobjectname
	order by submetaobjectname

select invoiceid, accountid, warehouseid, supplier_id, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoiceadjustmenthistory_v

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v
-- where invoiceref = '06411643'
order by invoicedate, invoiceid, name

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineadjustmenthistory_v
-- where invoiceref = '06411643'
order by invoicedate, invoiceid, description, orderNumber, receipt_id





select invoicelineheaderadjustmentid, 
	submetaobjectname, unitcost, adjustedunitcost, comment,
	posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from Landing.mend.gen_invrec_invoicelineheaderadjustment_v

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid, invoicelineheaderadjustmentid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	submetaobjectname, unitcost_ilha, adjustedunitcost_ilha, comment,
	posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from Landing.mend.gen_invrec_invoicelineheaderadjustment_il_v
order by invoicedate, invoiceid, description, orderNumber, receipt_id



select invoiceid, accountid, warehouseid, supplier_id, carriageid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived,
	carriagetype, cost, 
	comment, 
	createddate, changeddate
from Landing.mend.gen_invrec_invoicecarriage_v 


select creditnoteid, supplier_id, invoicecreditid, invoiceid,
	creditnoteno, reference, 
	totalamount, 
	supplierID, supplierName, 
	credit_note_status, 
	creditdate, createddate, processeddate, 
	goodscreditamount, carriagecreditamount, vatcreditamount, 
	invoiceref, status, invoicenetamount, erpnetamount, netdifference,
	goodscreditamount_i, carriagecreditamount_i, 
	comment
from Landing.mend.gen_invrec_invoicecredit_v 
