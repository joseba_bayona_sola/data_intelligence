
select top 1000 *
from Warehouse.invrec.dim_invoice_reconciliation_v
where invrec_status_name = 'Submitted'

select top 1000 idInvoiceReconciliation_sk, invoiceid_bk, 
	invrec_status_name, invoice_ref, net_suite_no, invoice_date, supplier_name,
	invoice_goods_amount, invoice_carriage_amount, invoice_discount_amount, invoice_import_duty_amount, 
	invoice_net_amount, invoice_gross_amount, invoice_vat_amount, invoice_reconciled_amount, 
	
	erp_goods_amount, erp_carriage_amount, erp_discount_amount, erp_import_duty_amount, 
	erp_net_amount, erp_gross_amount, erp_vat_amount, erp_adjusted_amount, 
	
	net_difference, credit_received

from Warehouse.invrec.dim_invoice_reconciliation_v
where invoiceid_bk = invoiceid_bk
	-- and invoice_net_amount <> invoice_goods_amount + invoice_carriage_amount - invoice_discount_amount + invoice_import_duty_amount
	-- and invoice_gross_amount <> invoice_net_amount + invoice_vat_amount
	-- and invoice_carriage_amount <> 0
	-- and invoice_discount_amount <> 0
	-- and invoice_import_duty_amount <> 0
	-- and invoice_reconciled_amount <> 0

	-- and erp_net_amount <> erp_goods_amount + erp_carriage_amount - erp_discount_amount + erp_import_duty_amount + erp_adjusted_amount
	-- and erp_gross_amount <> erp_net_amount + erp_vat_amount
	and erp_adjusted_amount <> 0
order by invoice_date	

	select count(*)
	from Warehouse.invrec.dim_invoice_reconciliation_v

	select invrec_status_name, count(*)
	from Warehouse.invrec.dim_invoice_reconciliation_v
	group by invrec_status_name
	order by invrec_status_name