
select top 1000 *
from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
where row_type = 'TDT'
-- where row_type = 'TOT'

	select top 1000 count(*)
	from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019

	select top 1000 row_type, count(*)
	from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
	group by row_type
	order by row_type

select top 1000 *
from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
where row_type = 'TOT' 
	and sku = '01125B4D4DS0000000000301'

	select top 1000 sku, count(*)
	from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
	where row_type = 'TOT'
	group by sku
	order by count(*) desc, sku

	select top 1000 d, count(*) 
	from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
	where row_type = 'TOT'
	group by d
	order by d


select count(*) over () num_tot,
	wsi.warehousestockitemid_bk, 
	wsi.warehouse_name, wsi.product_id_magento, wsi.product_family_name, wsi.packsize, wsi.sku, s.sku, s.qty,
	wsi.qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_on_hold
from
		(select sku, qty
		from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
		where row_type = 'TOT') s
	full join
		(select warehousestockitemid_bk, 
			warehouse_name, product_id_magento, product_family_name, packsize, sku, 
			qty_received, qty_available, qty_outstanding_allocation, qty_on_hold
		from Warehouse.stock.dim_wh_stock_item_v
		where warehouse_name = 'Amsterdam') wsi on s.sku = wsi.sku
-- where wsi.sku is null -- 33
where s.sku is null -- 11648
-- where s.sku is null and wsi.qty_available <> 0 -- 2678
order by wsi.product_id_magento, wsi.product_family_name, wsi.packsize, wsi.sku, s.sku

------------------------------------------------------------------

SELECT 
	row_type, sku, qty --, a, b, c, d, e, f, g, h
FROM 
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
where 
	row_type = 'TOT';


select min(cast (qty as integer)), max(cast (qty as integer)) from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot where row_type like 'TOT'

select * into DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019 where row_type = 'TOT';

select * from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot  where qty = 0 

select 
	count(1)
from 
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot

select count(1), row_type 
from
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019
group by row_type

select count(1), sku
from
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot
group by sku
order by 1 desc

select distinct qty from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot;

select sku, b, sum(cast (qty as integer))
from
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot
group by sku, b
order by 3 desc


select * from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot
where sku ='01125B4D4DS0000000000301'

-------------------------------------------------------------------------------

--35575 in snap
--47190 in wsi

--35542 in common

select top 1000 count(1)
	/*wsi.SKU, t.qty, wsi.qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, 
	wsi.qty_issued_stock, wsi.qty_on_hold, wsi.qty_registered, wsi.qty_disposed, wsi.qty_due_in*/
from 
	Warehouse.stock.dim_wh_stock_item_v wsi
	inner join
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot t
		on wsi.sku = t.sku
where 
	wsi.warehouse_name like 'Amsterdam' 

--33 sku from snap not matching
select count(1) from DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot t
where t.sku not in (select sku from Warehouse.stock.dim_wh_stock_item_v wsi where wsi.warehouse_name like 'Amsterdam' )


--15k matching t.qty = wsi.qty_available
select top 1000 count(1)
	--wsi.SKU, t.qty, wsi.qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, 
	--wsi.qty_issued_stock, wsi.qty_on_hold, wsi.qty_registered, wsi.qty_disposed, wsi.qty_due_in
from 
	Warehouse.stock.dim_wh_stock_item_v wsi
	inner join
	DW_GetLenses_jbs.dbo.snap_amsterdam_01022019_tot t
		on wsi.sku = t.sku
where 
	wsi.warehouse_name like 'Amsterdam' and t.qty = wsi.qty_available