
	select entity_id, increment_id, created_at, status, base_grand_total, mutual_amount, mutual_quotation_id
	into #mutuelle_orders
	from Landing.mag.sales_flat_order_aud
	where mutual_quotation_id is not null

	select mq.quotation_id quotation_id_bk, 
		mq.mutual_id mutual_id_bk, mq.type_id type_id_bk, mq.status_id status_id_bk, isnull(c.customer_id_bk, -1) customer_id_bk, mo.entity_id order_id_bk, 
		mq.amount mutual_amount, mq.created_at quotation_date, 
		case when (mq.error_id = 0) then null else mq.error_id end error_id, 
		case when (m.is_online = 1) then 'A' else 'M' end is_online, reference_number reference_no, isnull(mc.mutual_customer_id, -1) mutual_customer_id, 
		mo.order_date, mo.order_amount, 
		sum(mo.order_amount) over (partition by year(mo.order_date))
	from 
			Landing.mag.mutual_quotation_aud mq
		inner join
			Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
		left join
			(select mutual_quotation_id, entity_id, mutual_amount, order_date, order_amount
			from
				(select mutual_quotation_id, entity_id, mutual_amount, created_at order_date, base_grand_total order_amount,
					count(*) over (partition by mutual_quotation_id) num_rep, 
					rank() over (partition by mutual_quotation_id order by entity_id) max_rep
				from #mutuelle_orders) mo
			where num_rep = max_rep) mo on mq.quotation_id = mo.mutual_quotation_id
		left join
			Landing.mag.mutual_customer_aud mc on mq.mutual_customer_id = mc.mutual_customer_id
		left join
			Landing.aux.gen_dim_customer_aud c on mc.customer_id = c.customer_id_bk

	-- where mq.type_id not in (-29, 0) -- and mq.error_id = 0 
	where mq.type_id in (-29, 0) and mo.entity_id is not null
	order by quotation_date

---------------------------------------------------

select idOrderHeader_sk, order_id_bk, order_no
into #mutuelle_oh_mch
from Warehouse.sales.dim_order_header_v
where website = 'visiondirect.fr' and marketing_channel_name = 'Mutuelles Auto'

	select m.*, oh.order_date, oh.invoice_date, oh.global_total_exc_vat, sum(oh.global_total_exc_vat) over (partition by year(oh.invoice_date))
	from 
			#mutuelle_oh_mch m
		left join
			Warehouse.sales.fact_mutual_quotation2_v mq on m.idOrderHeader_sk = mq.idOrderHeader_sk_fk
		inner join
			Warehouse.sales.dim_order_header_v oh on m.idOrderHeader_sk = oh.idOrderHeader_sk
	where mq.idOrderHeader_sk_fk is null
		and oh.invoice_date is not null
	order by oh.order_date
