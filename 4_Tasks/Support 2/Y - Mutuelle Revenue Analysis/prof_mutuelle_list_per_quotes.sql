
select m.mutual_id_bk, m.mutual_code, m.mutual_name, mq.num num_quotation_auto, mq2.num_quotation num_quotation_manual
from 
		Warehouse.sales.dim_mutual m
	left join
		(select mutual_name, count(*) num
		from Warehouse.sales.fact_mutual_quotation2_v
		group by mutual_name) mq on m.mutual_name = mq.mutual_name
	left join
		(select mutual_id, count(*) num_quotation
		from Landing.mag.mutual_quotation_aud
		where type_id = 0
		group by mutual_id) mq2 on m.mutual_id_bk = mq2.mutual_id		
-- where mq.mutual_name is null
order by m.mutual_name

select mutual_name, count(*)
from Warehouse.sales.fact_mutual_quotation2_v
group by mutual_name
order by mutual_name

select mutual_name, is_online, count(*)
from Warehouse.sales.fact_mutual_quotation2_v
group by mutual_name, is_online
order by mutual_name, is_online


select *
from Warehouse.sales.fact_mutual_quotation2_v

select type_id, is_online, count(*)
from Landing.mag.mutual_quotation_aud
group by type_id, is_online
order by type_id, is_online

select mutual_id, count(*) num_quotations
from Landing.mag.mutual_quotation_aud
where type_id = 0
group by mutual_id

