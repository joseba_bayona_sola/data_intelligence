
select customer_id_bk
into #fraud_customers
from Warehouse.gen.dim_customer_scd_v
where postcode = '15120-1069'

select *
from Warehouse.gen.dim_customer_scd_v
where customer_id_bk in 
	(select customer_id_bk 
	from #fraud_customers)

select top 1000 order_id_bk, order_no, order_date, invoice_date, shipment_date, datediff(hh, order_date, shipment_date) diff_hh,
	website, order_status_name, order_status_magento_name,
	customer_id, customer_email, customer_name, country_code_ship, postcode_shipping, 
	payment_method_name, cc_type_name, rank_seq_no_gen, customer_order_seq_no_gen, local_total_inc_vat, sum(local_total_inc_vat) over ()
from Warehouse.sales.dim_order_header_v
where customer_id in 
	(select customer_id_bk 
	from #fraud_customers)
order by customer_id, order_date

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in 
	(select customer_id_bk 
	from #fraud_customers)

----------------------------------------------

select top 1000 order_id_bk, order_no, order_date, invoice_date, shipment_date, 
	website, order_status_name, order_status_magento_name,
	customer_id, customer_email, customer_name, country_code_ship, postcode_shipping, 
	payment_method_name, cc_type_name, rank_seq_no_gen, customer_order_seq_no_gen, local_total_inc_vat, sum(local_total_inc_vat) over ()
from Warehouse.sales.dim_order_header_v
where order_date > '2018-01-01' 
	and local_total_inc_vat > 900
order by customer_id, order_date