
select top 1000 idCustomer_sk_fk, customer_id, 
	customer_email, first_name, last_name,
	num_tot_orders, num_diff_product_type_oh, main_type_oh_name 
from Warehouse.act.dim_customer_signature_v
where market_name = 'visiondirect.co.uk' and country_code_ship = 'GB'
	and num_tot_orders between 1 and 3
order by customer_id

select 
	cs1.idCustomer_sk_fk, cs1.customer_id, 
	cs1.first_name, cs1.last_name, cs1.customer_email, c.phone_number, c.city, c.postcode,
	cs2.first_order_date, cs2.last_order_date, cs2.subtotal_tot_orders,
	cs1.num_tot_orders, cs1.num_diff_product_type_oh, cs1.main_type_oh_name
into #customer_cross_sell
from
		(select idCustomer_sk_fk, customer_id, 
			customer_email, first_name, last_name,
			num_tot_orders, num_diff_product_type_oh, main_type_oh_name
		from Warehouse.act.dim_customer_signature_v
		where market_name = 'visiondirect.co.uk' and country_code_ship = 'GB'
			and num_tot_orders between 1 and 3) cs1
	inner join
		Warehouse.act.fact_customer_signature_v cs2 on cs1.idCustomer_sk_fk = cs2.idCustomer_sk_fk
	inner join
		Warehouse.gen.dim_customer_scd_v c on cs1.customer_id = c.customer_id_bk
where last_order_date > '2016-09-15'
order by cs1.customer_id

select top 1000 count(*)
from #customer_cross_sell
where first_order_date > '2016-01-01'

-- select top 1000 t.*, ol.order_line_id_bk, ol.order_id_bk, ol.order_date, ol.website, ol.product_type_name
select distinct t.customer_id
into #customer_cross_sell_sug_gl
from 
		#customer_cross_sell t
	inner join
		Warehouse.sales.fact_order_line_v ol on t.customer_id = ol.customer_id and t.first_order_date > '2016-01-01' and ol.website = 'visiondirect.co.uk'
			and ol.order_status_name = 'OK' and ol.product_type_name in ('Glasses', 'Sunglasses') and main_type_oh_name like 'CL%'
-- order by t.customer_id, ol.order_id_bk


select top 3000 -- t1.customer_id,
	t1.first_name + ' ' + t1.last_name full_name, t1.customer_email, t1.phone_number, t1.city, t1.postcode, 
	t1.first_order_date first_purchase, t1.last_order_date last_purchase, convert(decimal(12, 4), (t1.subtotal_tot_orders / t1.num_tot_orders)) average_order_value
from 
		#customer_cross_sell t1
	inner join
		#customer_cross_sell_sug_gl t2 on t1.customer_id = t2.customer_id

select top 17000 -- t1.customer_id,
	t1.first_name + ' ' + t1.last_name full_name, t1.customer_email, t1.phone_number, t1.city, t1.postcode, 
	t1.first_order_date first_purchase, t1.last_order_date last_purchase, convert(decimal(12, 4), (t1.subtotal_tot_orders / t1.num_tot_orders)) average_order_value
from 
		#customer_cross_sell t1
	left join
		#customer_cross_sell_sug_gl t2 on t1.customer_id = t2.customer_id
where t2.customer_id is null

drop table #customer_cross_sell_sug_gl

drop table #customer_cross_sell