
select top 1000 count(*)
from Warehouse.act.fact_customer_signature_v
where website_group_create = 'Lensway NL'

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where website_group_create = 'Lensway NL'


drop table #customers_sms

select c1.customer_id_bk, c1.customer_email, c1.phone_number
	-- , c1.created_at, c2.last_order_date, vmd.migration_date
into #customers_sms
from 
		Warehouse.gen.dim_customer_scd_v c1
	inner join
		(select website_group_create, customer_id, last_order_date
		from Warehouse.act.fact_customer_signature_v
		where website_group_create = 'Lensway') c2 --  Lensway - Lenson - Lenson NL- Lensway NL - Yourlenses NL
			on c1.customer_id_bk = c2.customer_id
	inner join
		Landing.map.act_website_migrate_date_aud vmd on c2.website_group_create = vmd.website_group
where  (c1.phone_number like '447%' or c1.phone_number like '07%') -- c1.phone_number like '06%'  
	and (c2.last_order_date < vmd.migration_date or c2.last_order_date is null)
order by c2.last_order_date, c1.customer_id_bk

select distinct 
	c.customer_id_bk, c.customer_email, c.phone_number
from 
		#customers_sms c
	inner join
		Warehouse.act.fact_customer_product_signature_v cps on c.customer_id_bk = cps.customer_id and cps.category_name in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies')

