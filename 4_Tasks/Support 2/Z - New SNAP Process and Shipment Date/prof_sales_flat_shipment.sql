
select entity_id, increment_id, order_id, 
	created_at, created_at_dw, despatched_at, despatched_at_dw, 
	ins_ts
from Landing.mag.sales_flat_shipment_aud
where entity_id = 7582879
order by created_at desc

select count(*)
from Landing.mag.sales_flat_shipment_aud

select top 1000 entity_id, increment_id, order_id, 
	created_at, created_at_dw, despatched_at, despatched_at_dw, datediff(mi, despatched_at_dw, despatched_at),
	shipping_description,
	ins_ts, upd_ts
from Landing.mag.sales_flat_shipment_aud
-- where created_at <> created_at_dw
where despatched_at <> despatched_at_dw
order by created_at_dw

select top 1000 entity_id, increment_id, order_id, 
	-- created_at, despatched_at, 
	created_at_dw, despatched_at_dw, datediff(mi, despatched_at_dw, created_at), datediff(d, despatched_at_dw, created_at),
	shipping_description,
	ins_ts, upd_ts
from Landing.mag.sales_flat_shipment_aud
where created_at_dw > '2019-02-14'
	and despatched_at_dw < created_at_dw
	-- and despatched_at_dw < created_at
	and datediff(mi, despatched_at_dw, created_at) > 60
	and convert(date, created_at_dw) <> convert(date, despatched_at_dw)
-- order by datediff(mi, despatched_at_dw, created_at) desc, entity_id desc
order by entity_id desc

select top 1000 *
from Landing.mag.sales_flat_shipment_aud
where despatched_at is not null
order by entity_id desc


select count(*)
from Landing.mag.sales_flat_shipment_aud_v

select num_records, count(*)
from Landing.mag.sales_flat_shipment_aud_v
group by num_records

--21443
select count(*)
from Landing.mag.sales_flat_shipment_aud_hist

select top 1000 *
from Landing.mag.sales_flat_shipment_aud_hist
order by entity_id desc
-- order by aud_dateTo desc

---------------------------------------------------

select top 1000 *
from Landing.mag.sales_flat_shipment_aud_v
where num_records >1 
order by entity_id desc, record_type, aud_dateFrom 

select top 1000 entity_id, count(distinct created_at)
from Landing.mag.sales_flat_shipment_aud_v
where num_records > 1 
group by entity_id
having count(distinct created_at) > 1
order by entity_id desc

select top 1000 entity_id, count(distinct despatched_at)
from Landing.mag.sales_flat_shipment_aud_v
where num_records > 1 
group by entity_id
having count(distinct despatched_at) > 1
order by entity_id desc
