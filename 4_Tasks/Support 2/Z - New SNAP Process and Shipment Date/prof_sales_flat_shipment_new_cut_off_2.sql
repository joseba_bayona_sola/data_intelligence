
select entity_id, increment_id, order_id, created_at, updated_at, despatched_at, 
from Landing.mag.sales_flat_shipment
where despatched_at is not null
order by despatched_at desc


select entity_id, increment_id, order_id, created_at, updated_at, despatched_at
from Landing.mag.sales_flat_shipment_aud
where despatched_at is not null
	and despatched_at > getutcdate() - 5
order by despatched_at


drop table #sales_flat_shipment_aud

select entity_id, increment_id, order_id, despatched_at_dw created_at, created_at created, updated_at, despatched_at
into #sales_flat_shipment_aud
from Landing.mag.sales_flat_shipment_aud
where despatched_at is not null
	and despatched_at > getutcdate() - 5
order by despatched_at

select s_sh.order_id, s_sh.entity_id, s_sh.increment_id, 
	s_sh.created, s_sh.updated_at,
	s_sh.created_at_db_utc, s_sh.created_at created_at_orig_brit, 
	CASE WHEN s_sh.created_at <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, s_sh.created_at)) + ' ' + '01:30')
		THEN DATEADD(MINUTE, -90, s_sh.created_at) 
	ELSE s_sh.created_at END created_at, 
	count(*) over (partition by convert(date, s_sh.created_at))
from
	(select s.order_id, s.entity_id, s.increment_id, 
		s.created, s.updated_at,
		s.created_at created_at_db_utc, 
		case when (s.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, s.created_at) else s.created_at end created_at
	from
			#sales_flat_shipment_aud s
		left join	 
			Landing.map.sales_summer_time_period_aud stp on year(s.created_at) = stp.year and stp.sales_f = 'Y') s_sh
where s_sh.created_at <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, s_sh.created_at)) + ' ' + '01:30')
order by created_at desc

-- 

			select s_sh.order_id, s_sh.entity_id, s_sh.increment_id, s_sh.created_at_db, s_sh.created_at created_at_orig, 
				CASE WHEN s_sh.created_at <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, s_sh.created_at)) + ' ' + '01:30')
					THEN DATEADD(MINUTE, -90, s_sh.created_at) 
				ELSE s_sh.created_at END created_at
			from
				(select s.order_id, s.entity_id, s.increment_id, s.created_at created_at_db, 
					case when (s.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, s.created_at) else s.created_at end created_at
				from
					(select order_id, entity_id, increment_id, created_at
					from
						(select s.order_id, s.entity_id, s.increment_id, 
							-- s.created_at, 
							case when (s.created_at_dw > '2019-02-14') then s.despatched_at_dw else s.created_at_dw end created_at,
							rank() over (partition by s.order_id order by s.entity_id) rank_ship
						from 
								Landing.aux.sales_order_header_o oh
							inner join
								Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id) t
					where rank_ship = 1) s
				left join	 
					Landing.map.sales_summer_time_period_aud stp on year(s.created_at) = stp.year and stp.sales_f = 'Y') s_sh
			where created_at <= CONVERT(datetime, CONVERT(varchar, CONVERT(date, s_sh.created_at)) + ' ' + '01:30')
			order by created_at desc