
select top 10000 *
from Warehouse.sales.dim_order_header_v
where order_id_bk in (8693779, 8709794, 8709742)
order by order_date

select top 10000 *
from Warehouse.sales.fact_order_line_v
where order_id_bk in (8693779, 8709794, 8709742)
order by order_date

-- oh: shipment_id, shipment_no
-- ol: shipment_id, shipment_no, shipment_line_id, order_item_id

select top 10000 *
from Warehouse.sales.dim_order_header_v
where shipment_id is not null and shipment_date is null
order by order_date

--------------------------------------------------
--------------------------------------------------

select top 1000 count(*) over (), 
	order_id_bk, order_line_id_bk, order_no, 
	order_date, invoice_date, shipment_date, refund_date, datediff(mi, shipment_date, refund_date),
	qty_unit_refunded, local_total_inc_vat, local_store_credit_given, local_total_aft_refund_inc_vat
from Warehouse.sales.fact_order_line_v ol
where order_date > '2019-01-01' and (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0)
	and shipment_date is not null
	-- and refund_date < shipment_date
order by datediff(mi, shipment_date, refund_date) 
