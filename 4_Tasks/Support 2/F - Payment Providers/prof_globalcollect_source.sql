
select top 1000 *
from DW_GetLenses_jbs.dbo.ex_global_collect
where order_no = '2900004476'

select top 1000 order_no, received_date, num_transaction, range, 
	customer_id, order_country_code, currency_code, amount, 
	payment_reference, payment_product_id, payment_product_description, payment_method, 
	status_id, status_description, rejection_reason, remarks, 
	final_summary
from DW_GetLenses_jbs.dbo.ex_global_collect
-- where order_no like '3%' -- 3 - 9
-- where order_no = '21000403372'
-- where order_no like '170%'
where order_no = '992600001214'
order by order_no

	select count(*), count(distinct order_no), min(received_date), max(received_date)
	from DW_GetLenses_jbs.dbo.ex_global_collect

	select substring(order_no, 1, 3), count(*)
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by substring(order_no, 1, 3)
	order by substring(order_no, 1, 3)

	select payment_product_id, payment_product_description, payment_method, count(*)
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by payment_product_id, payment_product_description, payment_method
	order by payment_product_id, payment_product_description, payment_method

	select status_id, status_description, count(*)
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by status_id, status_description
	order by status_id, status_description

	select rejection_reason, count(*)
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by rejection_reason
	order by rejection_reason

	select final_summary, count(*)
	from DW_GetLenses_jbs.dbo.ex_global_collect
	where order_no like '170%'
	group by final_summary
	order by final_summary

select top 1000 order_no, received_date, num_transaction, range, 
	status_id, status_description, rejection_reason, remarks, 
	final_summary, 
	count(*) over (partition by order_no) num_rep
from DW_GetLenses_jbs.dbo.ex_global_collect
order by num_rep desc, order_no, final_summary

	select order_no, min(final_summary) final_summary
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by order_no
	order by order_no

	select order_no, count(*) 
	from DW_GetLenses_jbs.dbo.ex_global_collect
	group by order_no
	order by count(*) desc