
select oh.order_id_bk, gc.order_no, gc.final_summary, 
	
	oh.store_id_bk, s.store_name, oh.order_date, oh.status_bk, oh.payment_method_name_bk, oh.payment_method_code, 
	oh.customer_id_bk
from
		(select order_no, min(final_summary) final_summary
		from DW_GetLenses_jbs.dbo.ex_global_collect
		group by order_no) gc
	left join
		Landing.aux.sales_dim_order_header_aud oh on gc.order_no = oh.order_no
	left join
		Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id
-- where oh.order_id_bk is null and gc.final_summary = 'A'
-- where oh.store_id_bk = 20 and gc.final_summary = 'R' and oh.payment_method_name_bk = 'PayPal'
-- where gc.final_summary = 'A' and oh.payment_method_name_bk = 'Free'
order by gc.order_no

select store_name, final_summary, payment_method_name_bk, count(*)
from
	(select oh.order_id_bk, gc.order_no, gc.final_summary, 
		oh.store_id_bk, s.store_name, oh.order_date, oh.status_bk, oh.payment_method_name_bk, oh.payment_method_code
	from
			(select order_no, min(final_summary) final_summary
			from DW_GetLenses_jbs.dbo.ex_global_collect
			group by order_no) gc
		left join
			Landing.aux.sales_dim_order_header_aud oh on gc.order_no = oh.order_no
		left join
			Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id) t
group by store_name, final_summary, payment_method_name_bk
order by store_name, final_summary, payment_method_name_bk



select final_summary, payment_method_name_bk, count(*)
from
	(select oh.order_id_bk, gc.order_no, gc.final_summary, 
		oh.store_id_bk, s.store_name, oh.order_date, oh.status_bk, oh.payment_method_name_bk, oh.payment_method_code
	from
			(select order_no, min(final_summary) final_summary
			from DW_GetLenses_jbs.dbo.ex_global_collect
			group by order_no) gc
		left join
			Landing.aux.sales_dim_order_header_aud oh on gc.order_no = oh.order_no
		left join
			Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id) t
group by final_summary, payment_method_name_bk
order by final_summary, payment_method_name_bk
