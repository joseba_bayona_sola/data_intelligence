
select *
from Warehouse.sales.dim_payment_method
order by payment_method_name

select *
from Landing.map.sales_payment_method

select *
from Landing.map.sales_cc_type

select top 1000 payment_method_code, count(*)
from Landing.aux.sales_dim_order_header
group by payment_method_code
order by payment_method_code

select *
from Landing.aux.sales_dim_order_header_aud
where order_id_bk = 6657458

select top 1000 payment_method_name, cc_type_name, count(*)
from Warehouse.sales.dim_order_header_v
where order_date > '2018-06-01'
group by payment_method_name, cc_type_name
order by payment_method_name, cc_type_name
