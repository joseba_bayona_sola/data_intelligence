
select top 1000 order_id_bk, order_no, order_date,
	website, customer_id, customer_email, 
	order_status_name, order_status_magento_name, payment_method_name
from Warehouse.sales.dim_order_header_v
where order_date > '2018-09-01'
	and website = 'visiondirect.nl'
	and invoice_date is null
	and order_status_name <> 'CANCEL'
order by order_date