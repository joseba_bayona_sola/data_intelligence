
drop table DW_GetLenses.dbo.vat_uk_ih
drop table DW_GetLenses.dbo.vat_uk_il
drop table DW_GetLenses.dbo.vat_uk_il_ih


select 
	document_id, order_id, invoice_id, 
	order_no, invoice_no, 
	document_type, 
	invoice_date, document_date, 
	store_name,
	customer_id, customer_firstname + ' ' + customer_lastname customer_name, 
	shipping_country_id, 
	local_total_exc_vat, local_total_inc_vat, local_total_vat, local_prof_fee, 
	global_total_exc_vat, global_total_inc_vat, global_total_vat, global_prof_fee
into DW_GetLenses.dbo.vat_uk_ih
from DW_GetLenses.dbo.invoice_headers
where document_date between '2015-11-01' and '2017-08-01'

select 
	line_id, 
	document_id, invoice_id, invoice_no, 
	document_type, 
	invoice_date, document_date, 
	store_name, 
	local_line_total_exc_vat, local_line_total_inc_vat, local_line_total_vat, local_prof_fee, 
	global_line_total_exc_vat, global_line_total_inc_vat, global_line_total_vat, global_prof_fee
into DW_GetLenses.dbo.vat_uk_il
from DW_GetLenses.dbo.invoice_lines
where document_date between '2015-11-01' and '2017-08-01'


select il.line_id, 
	il.document_id, ih.order_id, ih.invoice_id, 
	ih.order_no, ih.invoice_no, 
	il.document_type, 
	ih.invoice_date, il.document_date, 
	il.store_name, 
	ih.customer_id, ih.customer_name, 
	ih.shipping_country_id, 
	il.local_line_total_exc_vat, il.local_line_total_inc_vat, il.local_line_total_vat, il.local_prof_fee, 
	il.global_line_total_exc_vat, il.global_line_total_inc_vat, il.global_line_total_vat, il.global_prof_fee
into DW_GetLenses.dbo.vat_uk_il_ih
from 
		DW_GetLenses.dbo.vat_uk_il il	
	left join
		DW_GetLenses.dbo.vat_uk_ih ih on il.document_id = ih.document_id and il.document_type = ih.document_type	

-----------------------------------------

select top 1000 *
from DW_GetLenses.dbo.vat_uk_ih
where document_date <> invoice_date


	select top 1000 count(*), sum(local_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_ih

	select top 1000 store_name, count(*), sum(local_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_ih
	group by store_name
	order by store_name

	select top 1000 document_type, count(*), sum(local_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_ih
	group by document_type
	order by document_type

	select top 1000 shipping_country_id, count(*), sum(local_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_ih
	group by shipping_country_id
	order by shipping_country_id

	select top 1000 year(document_date) yyyy, month(document_date) mm, 
		sum(local_total_exc_vat), sum(local_total_vat), sum(local_total_inc_vat)
	from DW_GetLenses.dbo.vat_uk_ih
	where shipping_country_id = 'GB'
	group by year(document_date), month(document_date)
	order by year(document_date), month(document_date)

select top 1000 *
from DW_GetLenses.dbo.vat_uk_il
-- where invoice_date is null and document_type <> 'CREDITMEMO'

	select top 1000 count(*), sum(local_line_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_il

	select top 1000 store_name, count(*), sum(local_line_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_il
	group by store_name
	order by store_name

	select top 1000 document_type, count(*), sum(local_line_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_il
	group by document_type
	order by document_type

select *
from DW_GetLenses.dbo.vat_uk_il_ih
where order_id is null
-- where order_id is not null and document_date <> invoice_date
order by document_date, line_id

	select top 1000 count(*), sum(local_line_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_il_ih

	select top 1000 count(*), sum(local_line_total_exc_vat)
	from DW_GetLenses.dbo.vat_uk_il_ih
	where order_id is not null

	select top 1000 year(document_date) yyyy, month(document_date) mm, 
		-- sum(local_line_total_inc_vat), sum(local_prof_fee), sum(local_line_total_vat), sum(local_line_total_exc_vat)
		sum(global_line_total_inc_vat), sum(global_prof_fee), sum(global_line_total_inc_vat) - sum(global_prof_fee), 
		sum(global_line_total_vat), sum(global_line_total_exc_vat) - sum(global_prof_fee)
	from DW_GetLenses.dbo.vat_uk_il_ih
	-- where order_id is not null and shipping_country_id = 'GB'
	-- where order_id is not null and shipping_country_id not in ('NL', 'IE', 'ES', 'IT', 'BE', 'FR') 
	-- where order_id is not null and shipping_country_id not in ('NL', 'IE', 'ES', 'IT', 'BE', 'FR', 'DK', 'SE') 
	-- where order_id is not null and store_name = 'visiondirect.co.uk'
	group by year(document_date), month(document_date)
	order by year(document_date), month(document_date)

-----------------------------------------------------------

select ih.local_total_exc_vat, il.local_total_exc_vat, il_ih.local_total_exc_vat, 
	abs(ih.local_total_exc_vat - il.local_total_exc_vat), 
	abs(il_ih.local_total_exc_vat - il.local_total_exc_vat)
from 
	(select top 1000 sum(local_total_exc_vat) local_total_exc_vat
	from DW_GetLenses.dbo.vat_uk_ih) ih,
	(select top 1000 sum(local_line_total_exc_vat) local_total_exc_vat
	from DW_GetLenses.dbo.vat_uk_il) il,
	(select top 1000 sum(local_line_total_exc_vat) local_total_exc_vat
	from DW_GetLenses.dbo.vat_uk_il_ih
	where order_id is not null) il_ih 

-----------------------------------------------------------

	select shipping_country_id, order_no, invoice_no, convert(date, document_date) invoice_date, customer_id, customer_name, 
		sum(global_line_total_inc_vat) total_inc_vat, sum(global_prof_fee) prof_fee, sum(global_line_total_inc_vat) - sum(global_prof_fee) gross_ex_prof_fee, 
		sum(global_line_total_vat) total_vat, sum(global_line_total_exc_vat) - sum(global_prof_fee) total_exc_vat_after_prof_fee
	from DW_GetLenses.dbo.vat_uk_il_ih
	where order_id is not null -- and shipping_country_id = 'GB'
		-- and document_date between '2015-11-01' and '2016-02-01'
		-- and document_date between '2016-02-01' and '2016-05-01'
		-- and document_date between '2016-05-01' and '2016-08-01'
		-- and document_date between '2016-08-01' and '2016-11-01'
		-- and document_date between '2016-11-01' and '2017-02-01' and shipping_country_id not in ('NL', 'IE', 'ES', 'IT', 'BE', 'FR') 
		-- and document_date between '2017-02-01' and '2017-05-01'
		and document_date between '2017-05-01' and '2017-08-01' and shipping_country_id = 'GB'
	group by shipping_country_id, order_no, invoice_no, document_date, customer_id, customer_name
	order by shipping_country_id, order_no, invoice_no, document_date, customer_id, customer_name
