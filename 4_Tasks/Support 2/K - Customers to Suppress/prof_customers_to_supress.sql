
select *
from DW_GetLenses.dbo.Dotmailer_Supressed_Data
order by timestamp desc

select *
from DW_GetLenses.dbo.unsubscribes
order by unsubscribeDate desc

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('visiondirect@rjmcdowall.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('vanbui@gmx.net', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('kam.binning@hotmail.co.uk', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('karimkassam5492@googlemail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('racylady01@yahoo.co.uk', getutcdate())
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('emer.kellyspedding@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('sdaleymum@gmail.com', getutcdate())
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('fm-spm@posteo.eu', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('kenyon.emma@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('kemaldinc7@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('t.m.dijkstra@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('nicoleheller1@icloud.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('gergana.lozanova-marfitt@prsformusic.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('ekundan@yahoo.co.uk', getutcdate())
go


insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('Tracy.elliott@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('tine.beks@kpnmail.nl', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('johnsbean@hotmail.co.uk', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('rowena.tasker@hotmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('amylouisebuckley_@hotmail.co.uk', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('g.petrachi@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('mariskavannieuwkoop@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('christineshouli@gmail.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('m.stinis@kpnplanet.nl', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('8emaza@web.de', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('leighbradley47@yahoo.com', getutcdate())
go
insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate) values ('othman.benbida@hotmail.com', getutcdate())
go


insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_14_12
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_02_01_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_24_01_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_08_02_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_04_03_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_04_04_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_29_04_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_29_05_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_26_07_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_20_08_19
go

insert into DW_GetLenses.dbo.unsubscribes (email, unsubscribeDate)
	select customer_email, GETUTCDATE()
	from DW_GetLenses_jbs.dbo.gdpr_16_09_19
go
