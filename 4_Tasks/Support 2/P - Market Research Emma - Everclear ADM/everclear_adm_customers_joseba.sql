select 
	scd.prefix,
	scd.first_name,
	scd.last_name,
	cps.customer_email,
	scd.street, scd.city, scd.postcode, scd.region,
	scd.phone_number,
	ol.base_curve, 
	ol.diameter, 
	ol.power,
	cps.num_tot_orders_product,
	cps.last_order_date
from 
	warehouse.act.fact_customer_product_signature_v cps
	inner join
	warehouse.gen.dim_customer_scd_v scd
on	cps.idCustomer_sk_fk = scd.idCustomer_sk_fk	
	inner join
	warehouse.sales.fact_order_line_v ol
on	cps.order_id_bk_last = ol.order_id_bk
	and ol.product_family_name = cps.product_family_name
where 
	cps.product_id_magento = 2317
	and cps.last_order_date > dateadd(month, -6, getdate())
	and cps.num_tot_orders_product > 1
	and convert(decimal(4,2), ol.power) between -6 and 4
	and scd.store_name like 'visiondirect.co.uk'
	order by 1;

---------------------------------------------------------------

select cps.idCustomer_sk_fk, cps.customer_id, 
	cps.customer_email, 
	cps.num_tot_orders_customer, cps.num_dist_products, 
	cps.product_id_magento, cps.first_order_date, cps.last_order_date, 
	cps.order_id_bk_last, cps.num_tot_orders_product, cps.product_percentage
into #market_research_adm_customers	 
from Warehouse.act.fact_customer_product_signature_v cps
where cps.product_id_magento = 2317
	and cps.last_order_date > dateadd(month, -6, getdate())
	and cps.num_tot_orders_product > 1

	select count(*) -- 24353
	from #market_research_adm_customers

select cps.idCustomer_sk_fk, cps.customer_id, 
	scd.prefix, scd.first_name, scd.last_name, cps.customer_email, 
	scd.street, scd.city, scd.postcode, scd.region, scd.phone_number,
	cps.num_tot_orders_customer, cps.num_dist_products, 
	cps.product_id_magento, cps.first_order_date, cps.last_order_date, 
	cps.order_id_bk_last, cps.num_tot_orders_product, cps.product_percentage
into #market_research_adm_customers_full
from 
		#market_research_adm_customers cps 
	inner join
		Warehouse.gen.dim_customer_scd_v scd on	cps.idCustomer_sk_fk = scd.idCustomer_sk_fk	
where scd.store_name like 'visiondirect.co.uk'	

	select count(*) -- 20853
	from #market_research_adm_customers_full

select cps.idCustomer_sk_fk, cps.customer_id, 
	cps.prefix, cps.first_name, cps.last_name, cps.customer_email, 
	cps.street, cps.city, cps.postcode, cps.region, cps.phone_number,
	cps.num_tot_orders_customer, cps.num_dist_products, 
	cps.product_id_magento, cps.first_order_date, cps.last_order_date, 
	cps.order_id_bk_last, cps.num_tot_orders_product, cps.product_percentage, 
	ol.order_line_id_bk, ol.order_id_bk, ol.base_curve, ol.diameter, ol.power, ol.eye, 
	count(*) over (partition by cps.customer_id) num_lines_per_cust, 
	rank() over (partition by cps.customer_id order by ol.eye, ol.order_line_id_bk) ord_lines_per_cust
into #market_research_adm_customers_ol
from 
		#market_research_adm_customers_full cps
	inner join
		Warehouse.sales.fact_order_line_v ol on cps.order_id_bk_last = ol.order_id_bk and ol.product_id_magento = 2317 and convert(decimal(4,2), ol.power) between -6 and 4

	select *
	from #market_research_adm_customers_ol
	where num_lines_per_cust > 2
	order by customer_id, ord_lines_per_cust

	select count(*), count(distinct customer_id) -- 20853
	from #market_research_adm_customers_ol

	select num_lines_per_cust, count(*), count(distinct customer_id) 
	from #market_research_adm_customers_ol
	group by num_lines_per_cust
	order by num_lines_per_cust

select cps.idCustomer_sk_fk, cps.customer_id, 
	cps.prefix, cps.first_name, cps.last_name, cps.customer_email, 
	cps.street, cps.city, cps.postcode, cps.region, cps.phone_number,
	cps.num_tot_orders_customer, cps.num_dist_products, 
	cps.product_id_magento, cps.first_order_date, cps.last_order_date, 
	cps.order_id_bk_last, cps.num_tot_orders_product, cps.product_percentage, 
	ol1.eye, ol1.power, ol2.eye, ol2.power
from 
		#market_research_adm_customers_full cps
	inner join
		(select customer_id, eye, power
		from #market_research_adm_customers_ol
		where num_lines_per_cust in (1, 2) and ord_lines_per_cust = 1) ol1 on cps.customer_id = ol1.customer_id
	left join
		(select customer_id, eye, power
		from #market_research_adm_customers_ol
		where num_lines_per_cust in (1, 2) and ord_lines_per_cust = 2) ol2 on cps.customer_id = ol2.customer_id

----------------------------------------

select cps.customer_id, 
	-- cps.prefix + ' ' + 
	cps.first_name + ' ' + cps.last_name customer_name, cps.customer_email email_address, 
	cps.street + ', ' + cps.postcode + ', ' + cps.city delivery_address, cps.phone_number phone_number,
	ol1.eye eye1, ol1.power power1, ol2.eye eye2, ol2.power power2,
	cps.num_tot_orders_product num_orders_everclear_adm, cps.last_order_date last_order_date
	 
	-- cps.num_tot_orders_customer, cps.num_dist_products, cps.product_percentage, 
from 
		#market_research_adm_customers_full cps
	inner join
		(select customer_id, eye, power
		from #market_research_adm_customers_ol
		where num_lines_per_cust in (1, 2) and ord_lines_per_cust = 1) ol1 on cps.customer_id = ol1.customer_id
	left join
		(select customer_id, eye, power
		from #market_research_adm_customers_ol
		where num_lines_per_cust in (1, 2) and ord_lines_per_cust = 2) ol2 on cps.customer_id = ol2.customer_id
order by num_orders_everclear_adm, last_order_date

