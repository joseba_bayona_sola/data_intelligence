

	select warehouse_name_from, warehouse_name_to, 
		intersite_transfer_number, created_date intersite_transfer_date, purchase_order_number_t, purchase_order_number_s, order_no_erp, 
		sku, stock_item_description, 
		qty_sent, qty_remaining, qty_registered, qty_missing
	from Warehouse.stock.dim_intransit_stock_item_batch_v
	-- where qty_sent <> qty_remaining + qty_registered + qty_missing 
	-- where qty_sent <> qty_remaining + qty_registered + qty_missing and (qty_remaining <> 0 or qty_registered <> 0 or qty_missing <> 0)
	-- where qty_remaining = 0 and qty_registered = 0 and qty_missing = 0
	-- where qty_remaining <> 0 or qty_registered <> 0 or qty_missing <> 0
	-- where qty_remaining <> 0
	-- where qty_registered <> 0
	-- where qty_missing <> 0

	-- where qty_remaining = 0 and qty_sent <> (qty_registered + qty_missing)
	-- where qty_remaining <> 0 and qty_sent <> (qty_registered + qty_missing + qty_remaining)

	where qty_remaining <> 0
	order by created_date desc
