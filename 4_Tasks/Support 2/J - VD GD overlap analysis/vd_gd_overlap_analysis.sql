
drop table #vd_gd_customers
drop table #cs
drop table #oh
drop table #ol

-------------------------------------------

select top 1000 email
from DW_GetLenses_jbs.dbo.vd_gd_overlap_email

select top 1000 count(*), count(distinct email)
from DW_GetLenses_jbs.dbo.vd_gd_overlap_email

select vdgd.email, c.customer_id_bk, c.store_name, c.market_name, c.country_code_s
into #vd_gd_customers
from 
		(select distinct email
		from DW_GetLenses_jbs.dbo.vd_gd_overlap_email) vdgd
	left join
		Warehouse.gen.dim_customer_scd_v c on vdgd.email = c.customer_email

select *
from #vd_gd_customers
where customer_id_bk is null

select market_name, country_code_s, count(*)
from #vd_gd_customers
group by market_name, country_code_s
order by market_name, country_code_s

select *
from #vd_gd_customers
where market_name = 'visiondirect.co.uk (UK)'

select *, count(*) over (partition by email) num_rep
from #vd_gd_customers
order by num_rep desc

-------------------------------------------------------------

select cs.customer_id, cs.customer_email, cs.website_create,
	cs.last_order_date, cs.order_id_bk_last, 
	cs.num_tot_orders_gen, cs.num_tot_orders_web 
into #cs
from 
		(select customer_id_bk, market_name
		from
			(select customer_id_bk, market_name, 
				count(*) over (partition by email) num_rep, 
				rank() over (partition by email order by customer_id_bk) ord_rep
			from #vd_gd_customers) vdgd
		where num_rep = ord_rep) vdgd	 
	inner join
		Warehouse.act.fact_customer_signature_v cs on vdgd.customer_id_bk = cs.customer_id
where vdgd.market_name = 'visiondirect.co.uk (UK)'
	and first_order_date > '2010-01-01'
	-- and cs.order_id_bk_last < 0
order by cs.last_order_date

select oh.order_id_bk, oh.order_date, oh.website, oh.market_name, oh.customer_id, oh.order_status_name, oh.customer_order_seq_no_gen, oh.product_type_oh_name
into #oh
from 
		#cs cs
	inner join
		Warehouse.sales.dim_order_header_v oh on cs.customer_id = oh.customer_id
where oh.order_source <> 'P'

select ol.order_line_id_bk, ol.order_id_bk, ol.customer_id, ol.product_id_magento
into #ol
from 
		(select order_id_bk, customer_id
		from
			(select order_id_bk, customer_id, order_date, 
				count(*) over (partition by customer_id) num_rep,
				rank() over (partition by customer_id order by order_date) ord_rep
			from #oh) t
		where num_rep = ord_rep) oh
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk


select count(*), count(distinct customer_id) from #cs
select count(*), count(distinct customer_id) from #oh
select count(*), count(distinct customer_id) from #ol

select website_create, count(*)
from #cs
group by website_create
order by website_create

select website, count(*)
from #oh
group by website
order by website

select order_status_name, count(*)
from #oh
group by order_status_name
order by order_status_name



--------------------------------------------------------------------------------------------------------------------

-- % of orders depending on type

select product_type_oh_name, count(*), convert(decimal(12, 4), count(*) * 100) / num_tot_orders
from
	(select oh.product_type_oh_name, count(*) over () num_tot_orders
	from 
			#cs cs
		inner join
			#oh oh on cs.order_id_bk_last = oh.order_id_bk) t
group by product_type_oh_name, num_tot_orders
order by product_type_oh_name

-- % ownbrand penettration

select ownbrand_product, count(*), convert(decimal(12, 4), count(*) * 100) / num_tot_orders
from
	(select order_id_bk, max(ownbrand_product) ownbrand_product, count(*) over () num_tot_orders
	from
		(select ol.*, 
			case when (pf.product_id_bk is null) then 0 else 1 end ownbrand_product
		from 
				#ol ol
			left join 
				(select pf.product_id_bk, pf.product_family_name
				from Warehouse.prod.dim_product_family pf
				where upper(product_family_name) like '%EVERCLEAR%' or upper(product_family_name) like '%ADM%') pf on  ol.product_id_magento = pf.product_id_bk) t
	group by order_id_bk) t
group by ownbrand_product, num_tot_orders

-- frequency of purchase ON VD 2010 - 2018

select customer_id, min(order_date), max(order_date), count(*)
from #oh
group by customer_id

-- frequency of purchase ON VD 2017

order by product_id_bk


