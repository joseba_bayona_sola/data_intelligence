--Check only order from 2018

--722374
select top 1000 *
from Landing.mag.sales_flat_order_aud o
where created_at between '01-01-2018' and '12-01-2018'

--693961
select top 1000 *
from Landing.mag.sales_flat_invoice_aud i
where created_at between '01-01-2018' and '12-01-2018'

select top 1000 oi.qty_canceled
from Landing.mag.sales_flat_order_item_aud oi
where created_at between '01-01-2018' and '12-01-2018'

select top 1000 * 
from 
		Landing.mag.sales_flat_invoice_item_aud ii
	inner join
		Landing.mag.sales_flat_invoice_aud i on i.entity_id = ii.parent_id

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

select top 1000 *
into #temp_missing_inv_lines
from Landing.mag.sales_flat_invoice_aud i
where i.created_at between '01-01-2018' and '12-12-2018'
	and not exists 
		(select 1 
		from Landing.mag.sales_flat_invoice_item_aud ii 
		where i.entity_id = ii.parent_id)

select top 1000 i.*
into #temp_missing_inv_lines_2
from 
		Landing.mag.sales_flat_invoice_aud i
	left join
		Landing.mag.sales_flat_invoice_item_aud ii  on i.entity_id = ii.parent_id
where i.created_at between '01-01-2018' and '12-12-2018'
	and ii.parent_id is null

----------------------------

select *
from #temp_missing_inv_lines
order by created_at

select *
from #temp_missing_inv_lines_2
order by created_at

---------------------------


--order, order items and inv_lines info
select o.entity_id, o.increment_id, o.store_id, o.state, o.status, o.created_at order_date, t.created_at invoice_date,
	op.method, op.base_amount_ordered, op.base_amount_canceled,
	o.base_subtotal, o.base_total_canceled, o.base_grand_total,
	o.total_qty_ordered
from	
		#temp_missing_inv_lines t 
	inner join
		Landing.mag.sales_flat_order_aud o on t.order_id = o.entity_id 
	inner join
		Landing.mag.sales_flat_order_payment_aud op on o.entity_id = op.parent_id
-- where o.increment_id in ('5001660242', '28000072708')
order by o.created_at


--order item
select o.entity_id, o.increment_id, o.store_id, o.state, o.status, o.created_at order_date, t.created_at invoice_date,
	oi.product_id, oi.name, oi.qty_ordered, oi.qty_canceled, oi.qty_invoiced
from	
		#temp_missing_inv_lines t 
	inner join
		Landing.mag.sales_flat_order_aud o on t.order_id = o.entity_id 
	inner join
		Landing.mag.sales_flat_order_item_aud oi on o.entity_id = oi.order_id
-- where o.increment_id in ('5001660242', '28000072708')
order by o.created_at


------------------------------

select oh.order_id_bk, oh.order_no, oh.order_date, 
	oh.invoice_id, oh.invoice_date, oh.shipment_date, oh.refund_date,
	oh.website, 
	oh.order_status_name, oh.order_status_magento_name, oh.payment_method_name
from	
		#temp_missing_inv_lines t 
	inner join
		Landing.mag.sales_flat_order_aud o on t.order_id = o.entity_id 
	inner join
		Warehouse.sales.dim_order_header_v oh on o.entity_id = oh.order_id_bk
order by oh.order_date

select oh.order_id_bk, oh.order_no, oh.order_date, 
	oh.invoice_id, oh.invoice_date, oh.shipment_date, oh.refund_date,
	oh.website, 
	oh.order_status_name, oh.order_status_magento_name, oh.payment_method_name
from	
		#temp_missing_inv_lines t 
	inner join
		Landing.mag.sales_flat_order_aud o on t.order_id = o.entity_id 
	inner join
		Warehouse.sales.fact_order_line_v oh on o.entity_id = oh.order_id_bk
order by oh.order_date
