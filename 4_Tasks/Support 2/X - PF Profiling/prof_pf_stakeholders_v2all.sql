
select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
into #product_family_v2
from
	(select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	union
	select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2m) pf


	select count(*)
	from #product_family_v2

	select manufacturer_name, count(*)
	from #product_family_v2
	group by manufacturer_name
	order by manufacturer_name

	select product_type_name, count(*)
	from #product_family_v2
	group by product_type_name
	order by product_type_name

	select category_name, count(*)
	from #product_family_v2
	group by category_name
	order by category_name

	select product_family_group_name, count(*)
	from #product_family_v2
	group by product_family_group_name
	order by product_family_group_name

	select brand_name, count(*)
	from #product_family_v2
	group by brand_name
	order by brand_name

	select product_family_name, count(*)
	from #product_family_v2
	group by product_family_name
	order by count(*) desc, product_family_name

-------------------------------------------

select pfg1.product_family_group_name, pfg2.product_family_group_name, pfg1.num
from
		(select product_family_group_name, count(*) num
		from #product_family_v2
		group by product_family_group_name) pfg1
	full join
		Warehouse.prod.dim_product_family_group pfg2 on pfg1.product_family_group_name = pfg2.product_family_group_name
order by pfg1.product_family_group_name, pfg2.product_family_group_name

