
-- Duplicates in Magento SRC
-- Missing rows in Excel
-- Use of Product ID for identification


select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name, 
	count(*) over (partition by product_family_name) num_rep
from DW_GetLenses_jbs.dbo.salesds_product_family_v2
order by num_rep desc, product_family_name

	select count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2

	select manufacturer_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by manufacturer_name
	order by manufacturer_name

	select product_type_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by product_type_name
	order by product_type_name

	select category_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by category_name
	order by category_name

	select product_family_group_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by product_family_group_name
	order by product_family_group_name

	select brand_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by brand_name
	order by brand_name

	select product_family_name, count(*)
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	group by product_family_name
	order by count(*) desc, product_family_name

----------------------------------------------------

select product_id_magento, manufacturer_name, product_type_name, category_name, product_family_group_name, product_family_name, 
	product_lifecycle_name, cl_type_name, cl_feature_name,
	count(*) over (partition by product_family_name) num_rep
from Warehouse.prod.dim_product_family_v
order by num_rep desc, product_family_name, product_id_magento

	select count(*)
	from Warehouse.prod.dim_product_family_v

----------------------------------------------------

select pf2.product_id_magento, 
	pf2.manufacturer_name, pf1.manufacturer_name manufacturer_name_new, pf2.product_type_name, pf1.product_type_name product_type_name_new, pf2.category_name, pf1.category_name category_name_new, 
	pf2.product_family_group_name, pf1.product_family_group_name product_family_group_name_new, pf1.brand_name brand_name_new, pf2.product_family_name, pf1.product_family_name product_family_name_new, 
	pf2.product_lifecycle_name, pf2.cl_type_name, pf2.cl_feature_name, 
	count(*) over (partition by pf2.product_family_name) num_rep
from 
		DW_GetLenses_jbs.dbo.salesds_product_family_v2 pf1
	full join
		Warehouse.prod.dim_product_family_v pf2 on pf1.product_family_name = pf2.product_family_name
	left join
		DW_GetLenses_jbs.dbo.salesds_product_family_v1 pf3 on pf2.product_family_name = pf3.product_family_name
where pf1.product_family_name is null and pf3.product_family_name is not null
-- where pf2.product_family_name is null
-- where pf1.product_family_name is not null and pf2.product_family_name is not null
	-- and isnull(pf1.manufacturer_name, '') <> isnull(pf2.manufacturer_name, '') -- manufacturer_name - product_type_name - category_name - product_family_group_name 
-- order by num_rep desc, pf2.product_family_name, pf2.product_id_magento
order by pf2.product_id_magento

select pf2.product_id_magento, 
	pf2.manufacturer_name, pf1.manufacturer_name manufacturer_name_new, pf2.product_type_name, pf1.product_type_name product_type_name_new, pf2.category_name, pf1.category_name category_name_new, 
	pf2.product_family_group_name, pf1.product_family_group_name product_family_group_name_new, pf1.brand_name brand_name_new, pf2.product_family_name, pf1.product_family_name product_family_name_new, 
	pf2.product_lifecycle_name, pf2.cl_type_name, pf2.cl_feature_name, 
	count(*) over (partition by pf2.product_family_name) num_rep
from 
		DW_GetLenses_jbs.dbo.salesds_product_family_v2 pf1
	full join
		Warehouse.prod.dim_product_family_v pf2 on pf1.product_id = pf2.product_id_magento
where pf1.product_id is null 
-- where pf2.product_family_name is null
-- where pf1.product_family_name is not null and pf2.product_family_name is not null
	-- and isnull(pf1.manufacturer_name, '') <> isnull(pf2.manufacturer_name, '') -- manufacturer_name - product_type_name - category_name - product_family_group_name 
-- order by num_rep desc, pf2.product_family_name, pf2.product_id_magento
order by pf2.product_id_magento


drop table #product_family_prof

select pf2.product_id_magento, 
	isnull(pf2.manufacturer_name, '') manufacturer_name,  isnull(pf1.manufacturer_name, '') manufacturer_name_new, 
	isnull(pf2.product_type_name, '') product_type_name, isnull(pf1.product_type_name, '') product_type_name_new, isnull(pf2.category_name, '') category_name, isnull(pf1.category_name, '') category_name_new, 
	isnull(pf2.product_family_group_name, '') product_family_group_name, isnull(pf1.product_family_group_name, '') product_family_group_name_new, isnull(pf1.brand_name, '') brand_name_new, 
	pf2.product_family_name, pf1.product_family_name product_family_name_new, 
	pf2.product_lifecycle_name, pf2.cl_type_name, pf2.cl_feature_name, 
	count(*) over (partition by pf2.product_family_name) num_rep
into #product_family_prof
from 
		DW_GetLenses_jbs.dbo.salesds_product_family_v2 pf1
	full join
		Warehouse.prod.dim_product_family_v pf2 on pf1.product_id = pf2.product_id_magento

select *
from #product_family_prof
where num_rep > 1
order by product_id_magento

-- Manufacturer
select *
from #product_family_prof
where num_rep = 1 and product_family_name_new is not null and product_family_name is not null and manufacturer_name <> manufacturer_name_new
order by product_id_magento

	select manufacturer_name, manufacturer_name_new, case when (manufacturer_name = manufacturer_name_new) then 1 else 0 end same_f, count(*)
	from #product_family_prof
	where num_rep = 1 and product_family_name_new is not null and product_family_name is not null 
		-- and manufacturer_name <> manufacturer_name_new
	group by manufacturer_name, manufacturer_name_new
	order by same_f, manufacturer_name, manufacturer_name_new

-- Product Type Name
select *
from #product_family_prof
where num_rep = 1 and product_family_name_new is not null and product_family_name is not null and product_type_name <> product_type_name_new
order by product_id_magento

	select product_type_name, product_type_name_new, case when (product_type_name = product_type_name_new) then 1 else 0 end same_f, count(*)
	from #product_family_prof
	where num_rep = 1 and product_family_name_new is not null and product_family_name is not null 
		-- and manufacturer_name <> manufacturer_name_new
	group by product_type_name, product_type_name_new
	order by same_f, product_type_name, product_type_name_new

-- Category Name
select *
from #product_family_prof
where num_rep = 1 and product_family_name_new is not null and product_family_name is not null and category_name <> category_name_new
order by product_id_magento

	select category_name, category_name_new, case when (category_name = category_name_new) then 1 else 0 end same_f, count(*)
	from #product_family_prof
	where num_rep = 1 and product_family_name_new is not null and product_family_name is not null 
		-- and manufacturer_name <> manufacturer_name_new
	group by category_name, category_name_new
	order by same_f, category_name, category_name_new

-- Product Family Group
select *
from #product_family_prof
where num_rep = 1 and product_family_name_new is not null and product_family_name is not null and product_family_group_name <> product_family_group_name_new
order by product_id_magento

	select pf1.product_family_group_name, pf2.product_family_group_name_new, pf1.num_1, pf2.num_2
	from
			(select product_family_group_name, count(*) num_1
			from #product_family_prof
			group by product_family_group_name) pf1
		full join
			(select product_family_group_name_new, count(*) num_2
			from #product_family_prof
			group by product_family_group_name_new) pf2 on pf1.product_family_group_name = pf2.product_family_group_name_new
	order by product_family_group_name, product_family_group_name_new


	select product_family_group_name, product_family_group_name_new, case when (product_family_group_name = product_family_group_name_new) then 1 else 0 end same_f, count(*)
	from #product_family_prof
	where num_rep = 1 and product_family_name_new is not null and product_family_name is not null 
		-- and manufacturer_name <> manufacturer_name_new
	group by product_family_group_name, product_family_group_name_new
	order by same_f, product_family_group_name, product_family_group_name_new

-- Brand
select product_id_magento, manufacturer_name_new, manufacturer_name, product_family_name, brand_name_new
from #product_family_prof
where brand_name_new <> ''
order by manufacturer_name_new, brand_name_new, product_id_magento
