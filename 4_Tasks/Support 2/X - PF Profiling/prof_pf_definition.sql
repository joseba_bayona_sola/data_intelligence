
select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
into #product_family_v2
from
	(select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2
	union
	select product_id, manufacturer_name, product_type_name, category_name, product_family_group_name, brand_name, product_family_name
	from DW_GetLenses_jbs.dbo.salesds_product_family_v2m) pf


-- manufacturer
select t1.manufacturer_name, t2.manufacturer_name, t1.num, t2.num
from
		(select manufacturer_name, count(*) num
		from Warehouse.prod.dim_product_family_v
		group by manufacturer_name) t1
	full join
		(select manufacturer_name, count(*) num
		from #product_family_v2
		group by manufacturer_name) t2 on t1.manufacturer_name = t2.manufacturer_name
order by t1.manufacturer_name, t2.manufacturer_name



	select product_type_name, count(*)
	from #product_family_v2
	group by product_type_name
	order by product_type_name

	select category_name, count(*)
	from #product_family_v2
	group by category_name
	order by category_name

	select product_family_group_name, count(*)
	from #product_family_v2
	group by product_family_group_name
	order by product_family_group_name
