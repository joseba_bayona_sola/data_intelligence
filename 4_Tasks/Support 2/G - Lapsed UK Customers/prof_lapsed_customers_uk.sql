
select top 1000 *
from Warehouse.act.dim_customer_signature_v
where customer_status_name = 'LAPSED'
	and num_tot_orders > 3
	and website_register = 'visiondirect.co.uk'
	and country_code_ship = 'GB'

select top 1000 count(*)
from Warehouse.act.dim_customer_signature_v
where customer_status_name = 'LAPSED'
	and num_tot_orders > 3
	and website_register = 'visiondirect.co.uk'
	and country_code_ship = 'GB'

select top 1000 num_dist_products, count(*)
from Warehouse.act.dim_customer_signature_v
where customer_status_name = 'LAPSED'
	and num_tot_orders > 3
	and website_register = 'visiondirect.co.uk'
	and country_code_ship = 'GB'
group by num_dist_products
order by num_dist_products

------------------------------------------------------------

drop table #lapsed_uk_customers
drop table #lapsed_uk_customers_2

select idCustomer_sk_fk, customer_id, customer_email, 
	first_name, last_name, website_create, num_tot_orders, num_dist_products, customer_unsubscribe_name
into #lapsed_uk_customers
from Warehouse.act.dim_customer_signature_v
where customer_status_name = 'LAPSED'
	and num_tot_orders > 3
	and website_register = 'visiondirect.co.uk'
	and country_code_ship = 'GB'


select top 5000 lc.*, cs.last_order_date, cs.store_last, 
	c.phone_number, c.city, c.postcode_s
from 
		#lapsed_uk_customers lc
	inner join
		Warehouse.act.fact_customer_signature_v cs on lc.idCustomer_sk_fk = cs.idCustomer_sk_fk
	inner join
		Warehouse.gen.dim_customer_scd_v c on lc.idCustomer_sk_fk = c.idCustomer_sk_fk
where cs.store_last = 'visiondirect.co.uk' and datediff(dd, cs.last_order_date, getutcdate()) >= 450
order by cs.last_order_date desc
 


select top 5000 lc.idCustomer_sk_fk, lc.customer_id,
	lc.first_name + ' ' + lc.last_name customer_name, lc.customer_email, c.phone_number, c.city, c.postcode_s postcode, 
	lc.num_tot_orders num_purchases, cs.last_order_date, 
	convert(decimal(12, 4), cs.subtotal_tot_orders / cs.num_tot_orders) average_order_value, 
	cs.first_subtotal_amount first_order_value, cs.first_discount_amount first_discount_value, cs.avg_order_freq_time average_num_days_bet_orders
into #lapsed_uk_customers_2
from 
		#lapsed_uk_customers lc
	inner join
		Warehouse.act.fact_customer_signature_v cs on lc.idCustomer_sk_fk = cs.idCustomer_sk_fk
	inner join
		Warehouse.gen.dim_customer_scd_v c on lc.idCustomer_sk_fk = c.idCustomer_sk_fk
where cs.store_last = 'visiondirect.co.uk' and datediff(dd, cs.last_order_date, getutcdate()) >= 450
order by cs.last_order_date desc
 

select t.idCustomer_sk_fk, t.customer_id,
	t.customer_name, t.customer_email, t.phone_number, t.city, t.postcode, 
	t.num_purchases, t.last_order_date, 
	cps.product_family_name, cps.idCalendarLastOrderDate_sk_fk, cps.subtotal_tot_orders, 
	rank() over (partition by t.idCustomer_sk_fk order by cps.idCalendarLastOrderDate_sk_fk desc, cps.subtotal_tot_orders desc, cps.product_id_magento ) ord_product, 
	t.average_order_value, t.first_order_value, t.first_discount_value, t.average_num_days_bet_orders
from 
		#lapsed_uk_customers_2 t
	inner join
		Warehouse.act.fact_customer_product_signature_v cps on t.idCustomer_sk_fk = cps.idCustomer_sk_fk
order by last_order_date desc, customer_id, cps.product_id_magento 

select customer_name, customer_email, phone_number, city, postcode, category_name, product_family_name product_name, num_purchases, last_order_date, 
	average_order_value, first_order_value, first_discount_value, average_num_days_bet_orders
from
	(select t.idCustomer_sk_fk, t.customer_id,
		t.customer_name, t.customer_email, t.phone_number, t.city, t.postcode, 
		t.num_purchases, t.last_order_date, 
		cps.category_name, cps.product_family_name, cps.idCalendarLastOrderDate_sk_fk, cps.subtotal_tot_orders, 
		rank() over (partition by t.idCustomer_sk_fk order by cps.idCalendarLastOrderDate_sk_fk desc, cps.subtotal_tot_orders desc, cps.product_id_magento) ord_product, 
		t.average_order_value, t.first_order_value, t.first_discount_value, t.average_num_days_bet_orders
	from 
			#lapsed_uk_customers_2 t
		inner join
			Warehouse.act.fact_customer_product_signature_v cps on t.idCustomer_sk_fk = cps.idCustomer_sk_fk) t
where t.ord_product = 1
	and category_name like 'CL%'
order by last_order_date desc, customer_id


