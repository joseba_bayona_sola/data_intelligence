
select *
from Warehouse.sales.dim_order_header_v
where order_id_bk = 6861596

select *
from Landing.mag.sales_flat_order_aud
where entity_id = 6861596

select *
from Landing.aux.sales_dim_order_header_aud
where order_id_bk = 3269071

select *
from Landing.mag.sales_flat_order_address_aud
-- where entity_id in (7491740, 7491739)
-- where entity_id in (16684575, 16684574)
where entity_id in (14676709, 14676708)

select *
from Landing.mag.sales_flat_order_address_aud
where customer_id = 376404
order by entity_id

	select o.entity_id, 
		o.store_id store_id_bk, o.customer_id customer_id_bk, o.shipping_address_id, o.billing_address_id, 
		isnull(t.country_code_OK, oas.country_id) country_id_shipping, oas.region_id region_id_shipping, oas.postcode postcode_shipping, 
		isnull(t2.country_code_OK, oab.country_id) country_id_billing, oab.region_id region_id_billing, oab.postcode postcode_billing
	from
			Landing.mag.sales_flat_order_aud o 
		inner join
			Landing.mag.sales_flat_order_address_aud oas on o.shipping_address_id = oas.entity_id
		left join
			(select 'Sp' country_code_wrong, 'ES' country_code_OK) t on oas.country_id = t.country_code_wrong
		inner join
			Landing.mag.sales_flat_order_address_aud oab on o.billing_address_id = oab.entity_id
		left join
			(select 'Sp' country_code_wrong, 'ES' country_code_OK) t2 on oab.country_id = t2.country_code_wrong
	where o.store_id is not null
		and o.entity_id = 3269071
	order by o.entity_id