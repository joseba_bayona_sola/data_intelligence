
drop table #magento_oh
drop table #erp_oh

select entity_id, increment_id, created_at, store_id, customer_id, status, base_grand_total, 
	presc_verification_method, prescription_verification_type, warehouse_approved_time
into #magento_oh
from Landing.mag.sales_flat_order_aud
where created_at between '2018-03-01' and '2018-04-04'
order by created_at

select id, magentoOrderID, incrementID, _type, createdDate
into #erp_oh
from Landing.mend.order_order_aud
where createdDate between '2018-03-01' and '2018-04-04'
order by createddate

	select count(*)
	from #magento_oh

	select count(*)
	from #erp_oh

---------------

drop table #magento_erp_oh

select moh.entity_id, eoh.magentoOrderID,
	moh.increment_id, eoh.incrementID, eoh._type, moh.created_at order_date, ih.created_at invoice_date, moh.warehouse_approved_time, eoh.createdDate erp_order_date, 
	datediff(mi, moh.created_at, eoh.createdDate) diff_o,
	datediff(mi, ih.created_at, eoh.createdDate) diff_i,
	datediff(mi, moh.warehouse_approved_time, eoh.createdDate) diff_a,
	moh.presc_verification_method, moh.prescription_verification_type, ph.method,
	moh.store_id, moh.customer_id, moh.status, moh.base_grand_total
into #magento_erp_oh
from 
		#magento_oh moh
	left join
		Landing.mag.sales_flat_invoice_aud ih on moh.entity_id = ih.order_id
	left join
		Landing.mag.sales_flat_order_payment_aud ph on moh.entity_id = ph.parent_id
	full join
		#erp_oh eoh on moh.entity_id = eoh.magentoOrderID


select entity_id, magentoOrderID,
	increment_id, incrementID, _type, order_date, invoice_date, warehouse_approved_time, erp_order_date, diff_o, diff_i, diff_i / 60, diff_a, diff_a / 60,
	presc_verification_method, prescription_verification_type, method,
	store_id, customer_id, status, base_grand_total
from #magento_erp_oh
where 
	-- entity_id is null and magentoOrderID is not null order by erp_order_date
	-- magentoOrderID is null and status not in ('canceled', 'pending', 'pending_payment', 'closed') and prescription_verification_type is null order by order_date
	entity_id is not null and magentoOrderID is not null 
		-- and diff_o > 10 order by diff_o desc
		-- and diff_i > 10 order by diff_i desc
		-- and diff_a > 10 order by diff_a desc

select entity_id order_id, increment_id order_no, s.store_name,
	order_date, invoice_date, warehouse_approved_time, erp_order_date, 
	presc_verification_method, prescription_verification_type, method payment_method
from 
	#magento_erp_oh t
inner join
	Landing.aux.mag_gen_store_v s on t.store_id = s.store_id 
where 
	-- entity_id is null and magentoOrderID is not null order by erp_order_date
	-- magentoOrderID is null and status not in ('canceled', 'pending', 'pending_payment', 'closed') and prescription_verification_type is null order by order_date
	entity_id is not null and magentoOrderID is not null 
		and (diff_o > 10 or diff_i > 10 or diff_a > 10)
order by order_date

select *
from Landing.aux.mag_gen_store_v