
select top 1000 *
from Warehouse.gen.dim_customer_scd


select days_worn_info, count(*) num_cust, count(*) * 100 / convert(decimal(12, 4), num_tot_cust) perc_cust
from
	(select customer_id_bk, days_worn_info, count(*) over () num_tot_cust
	from Warehouse.gen.dim_customer_scd_v
	where created_at > '2018-01-01') t
group by days_worn_info, num_tot_cust
order by days_worn_info, num_tot_cust
