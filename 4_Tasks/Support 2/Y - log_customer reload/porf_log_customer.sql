
			select log_id, visitor_id, customer_id, 
				login_at, logout_at, store_id
			into #log_customer
			from openquery(MAGENTO, 
				'select log_id, visitor_id, customer_id, 
					login_at, logout_at, store_id
				from magento01.log_customer')

	-- Compare existing data with Magento data
		-- 3508162
		select top 1000 *, count(*) over ()
		from Landing.mag.log_customer_aud
	
		-- 1256987
		select top 1000 *, count(*) over ()
		from #log_customer

		-- 100361
		select top 1000 t2.*, count(*) over ()
		from 
				Landing.mag.log_customer_aud t1
			right join
				#log_customer t2 on t1.log_id = t2.log_id
		where t1.log_id is null

	-- Reload data
		-- insert in Landing.mag.log_customer: Trigger will automatically go on and insert in aud
		delete from Landing.mag.log_customer

		insert into Landing.mag.log_customer (log_id, visitor_id, customer_id, login_at, logout_at, store_id, idETLBatchRun)
			select log_id, visitor_id, customer_id, login_at, logout_at, store_id, 1 idETLBatchRun
			from #log_customer

	-- See where data affects				
		-- lnd_stg_get_act_activity_login

	drop table #log_customer