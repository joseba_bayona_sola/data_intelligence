
select *
from Warehouse.sales.dim_coupon_code cc
where coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10')


--Coupons used 1368 times by 1366 customers
select count(oh.idOrderHeader_sk) , count(distinct(oh.idCustomer_sk_fk))
from warehouse.sales.dim_order_header oh
where oh.idcouponcode_sk_fk in 
	(select idCouponCode_sk
	from Warehouse.sales.dim_coupon_code cc
	where coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10'))

select count(oh.idOrderHeader_sk) , count(distinct(oh.customer_id))
from warehouse.sales.dim_order_header_v oh
where coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10')

--Headers and lines of the discounted orders
select oh.order_id_bk, oh.order_no, oh.order_date, oh.website,
	oh.customer_id, oh.coupon_code, 
	oh.local_total_inc_vat, oh.local_total_exc_vat 
from Warehouse.sales.dim_order_header_v oh 
where oh.coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10')
-- order by oh.customer_id, oh.order_id_bk
order by oh.coupon_code, oh.order_id_bk

select oh.customer_id, oh.order_id_bk, oh.coupon_code, ol.product_family_group_name, ol.product_family_name, 
	ol.qty_time, ol.rank_qty_time , oh.order_date, oh.local_total_exc_vat h_local_total_exc_vat, ol.local_total_exc_vat l_local_total_exc_vat
from 
		Warehouse.sales.dim_order_header_v oh 
	inner join
		Warehouse.tableau.fact_ol_v ol on ol.order_id_bk = oh.order_id_bk
where oh.coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10')

--customer list
select 
	oh.customer_id, 
	count(oh.customer_id) over (partition by oh.customer_id) num_orders_cust,
	row_number() over (partition by oh.customer_id order by oh.order_date) cust_order_number, 
	oh.rank_seq_no, 
	oh.order_no,
	oh.coupon_code, 
	cs.customer_status_name, 
	cs.num_tot_discount_orders,
	oh.order_date, 
	oh.order_qty_time,
	datediff(month,lead(oh.order_date, 1,0) over (partition by oh.customer_id order by oh.order_date), oh.order_date) diff_date_between_orders
from 
	warehouse.sales.dim_order_header_v oh 
	inner join 
	act.fact_customer_signature_v cs
	on oh.customer_id = cs.customer_id
where 
	oh.customer_id in (
		select 
			oh.customer_id
		from 
			warehouse.sales.dim_order_header_v oh 
		where 
			oh.coupon_code in ('TRIAL','FBTRIAL','TRIAL2','FREETRIAL','CLEAR10'))
order by customer_id, order_date


select max(rank_seq_no) from sales.dim_order_header_v where customer_id in
(select customer_id from sales.dim_order_header_v where coupon_code like 'CLEAR10')

select rank_seq_no,coupon_code from sales.dim_order_header_v where customer_id = 2067116