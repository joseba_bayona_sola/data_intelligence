
select top 1000 count(*)
from Landing.aux.act_customer_cr_reg

select top 1000 count(*)
from Warehouse.gen.dim_customer

select top 1000 count(*)
from Warehouse.act.fact_customer_signature

------------------------

select c.idCustomer_sk
from 
		Warehouse.gen.dim_customer c
	left join
		Warehouse.act.fact_customer_signature cs on c.idCustomer_sk = cs.idCustomer_sk_fk
where cs.idCustomer_sk_fk is null
order by c.created_at

select top 1000 count(*), sum(local_total_inc_vat), sum(local_total_exc_vat)
select *
from Warehouse.sales.dim_order_header
where idCustomer_sk_fk in 
	(select top 1000 c.idCustomer_sk
	from 
			Warehouse.gen.dim_customer c
		left join
			Warehouse.act.fact_customer_signature cs on c.idCustomer_sk = cs.idCustomer_sk_fk
	where cs.idCustomer_sk_fk is null)
order by order_date
