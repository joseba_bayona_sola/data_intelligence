


select top 1000 *
from dw_proforma.dbo.order_lifecycle
order by created_at desc

select top 1000 store_id, count(*)
from dw_proforma.dbo.order_lifecycle
group by store_id
order by store_id

select top 1000 source, store_id, count(*), min(created_at), max(created_at)
from dw_proforma.dbo.order_lifecycle
group by source, store_id
order by source, store_id


----------------------------------------------------------------

	select top 1000 *
	from #FirstOrders

	select website, src, count(*)
	from #FirstOrders
	group by website, src
	order by website, src

	drop table #FirstOrders

	create table #FirstOrders(
		customer_id int,
		website varchar(100),
		src varchar(20))

	insert into #FirstOrders(customer_id)
		select distinct i.customer_id
		FROM v_invoices i 
		WHERE i.document_date BETWEEN '2017-12-01' AND '2017-12-31'

	create unique nonclustered index IX_FirstOrders on #FirstOrders(customer_id) include(website,src)

	update fo
	set fo.website = co.store_name, fo.src = co.src
	from
			#FirstOrders fo
		inner join
			(select o.customer_id,
				case o.source 
					when 'visio_db' then 'VisioOptik'
					when 'lensbase_db' then 'lensbase' + s.tld
					when 'masterlens_db' then 'masterlens' + s.tld
					else s.store_name 
				end as store_name,
				case when source = 'getlenses' then 'gl' else 'hist' end as src,
				row_number() over(partition by o.customer_id order by o.order_date asc) as rno
			from 
					(select ih.customer_id, 'getlenses' as source, store_id, order_date 
					from 
							invoice_headers ih 
						inner join 
							dw_stores_full s on s.store_name = ih.store_name 
						inner join 
							#FirstOrders fo on fo.customer_id = ih.customer_id
					union
					select olc.customer_id, source, store_id, created_at 
					from 
							dw_proforma.dbo.order_lifecycle olc 
						inner join 
							#FirstOrders fo on fo.customer_id = olc.customer_id
					union
					select oh.customer_id, source, store_id, created_at 
					from 
							dbo.dw_hist_order oh 
						inner join
							#FirstOrders fo on fo.customer_id=oh.customer_id) o
				inner join
					V_Stores_All s on s.store_id=o.store_id) co on co.customer_id = fo.customer_id and co.rno = 1


	SELECT 
		i.store_name AS [Current Site],
		CASE 
			when i.store_name = firstorder.website and firstorder.src = 'gl' then 'New'
			else firstorder.website 
		end AS [Previous Site],
		DATEname(MONTH,i.document_date) + '-' + DATENAME(YEAR,i.document_date) AS [Date],

		SUM(i.global_line_total_exc_vat) AS [revenue_ex_VAT], sum(i.global_line_total_inc_vat) as [revenue_in_VAT], sum(i.global_line_total_vat) as [VAT],
		sum(i.global_prof_fee) as [prof_fee]
	FROM 
			v_invoices i
		inner join 
			#FirstOrders firstorder on firstorder.customer_id = i.customer_id
	WHERE 
		i.document_date BETWEEN '2017-12-01' AND '2017-12-31'
	GROUP BY 
		i.store_name,
		CASE 
			when i.store_name=firstorder.website and firstorder.src='gl' then 'New'
			else firstorder.website 
		end,
		DATENAME(MONTH,i.document_date) + '-' + DATENAME(YEAR,i.document_date)
	order by [Current Site], [Previous Site]