
----------------------------------------------

select c.customer_id, cb.balance_id, cb.website_id, cb.amount,
	c.num_balance, c.final_amount amount_customer, c.final_amount_label	
from
		(select customer_id, num_balance, final_amount, final_amount_label
		from
			(select customer_id, count(distinct balance_id) num_balance, sum(amount) final_amount, 
				case 
					when (sum(amount) = 0) then 'ZERO'
					when (sum(amount) < 0) then 'NEG'
					when (sum(amount) > 0) then 'POS'
				end final_amount_label
			from Landing.mag.enterprise_customerbalance
			group by customer_id) t
		where final_amount_label = 'POS' -- NEG - POS - ZERO
		) c
	inner join
		Landing.mag.enterprise_customerbalance cb on c.customer_id = cb.customer_id
order by c.customer_id, cb.balance_id


----------------------------------------------------------------------

select top 1000 cb.customer_id, cb.balance_id, cb.website_id, cb.amount,
	-- history_id, 
	cbh.updated_at,
	cbh.action, cbh.balance_amount, cbh.balance_delta, 
	-- cbh.additional_info, cbh.frontend_comment, cbh.is_customer_notified, 
	sum(cbh.balance_delta) over (partition by cb.customer_id) sum_bal_hist_cust, 
	sum(cbh.balance_delta) over (partition by cb.balance_id) sum_bal_hist_bal
from 
		Landing.mag.enterprise_customerbalance cb
	inner join
		Landing.mag.enterprise_customerbalance_history_aud cbh on cb.balance_id = cbh.balance_id

order by cb.customer_id, updated_at


----------------------------------------------------------------------

drop table #prep_cust_balance

select cb.customer_id, cb.balance_id, cb.website_id, cb.amount,
	cb.num_balance, cb.amount_customer, cb.final_amount_label,	
	history_id, 
	cbh.updated_at,
	cbh.action, cbh.balance_amount, cbh.balance_delta, 
	-- cbh.additional_info, cbh.frontend_comment, cbh.is_customer_notified, 
	sum(cbh.balance_delta) over (partition by cb.customer_id) sum_bal_hist_cust, 
	sum(cbh.balance_delta) over (partition by cb.balance_id) sum_bal_hist_bal
into #prep_cust_balance
from 
	(select c.customer_id, cb.balance_id, cb.website_id, cb.amount,
		c.num_balance, c.final_amount amount_customer, c.final_amount_label	
	from
			(select customer_id, num_balance, final_amount, final_amount_label
			from
				(select customer_id, count(distinct balance_id) num_balance, sum(amount) final_amount, 
					case 
						when (sum(amount) = 0) then 'ZERO'
						when (sum(amount) < 0) then 'NEG'
						when (sum(amount) > 0) then 'POS'
					end final_amount_label
				from Landing.mag.enterprise_customerbalance
				group by customer_id) t
			-- where final_amount_label = 'POS' -- NEG - POS - ZERO
			) c
		inner join
			Landing.mag.enterprise_customerbalance cb on c.customer_id = cb.customer_id) cb
	left join
		Landing.mag.enterprise_customerbalance_history_aud cbh on cb.balance_id = cbh.balance_id

order by cb.customer_id, updated_at

--------------------------------------------------

select top 1000 *
from #prep_cust_balance
where customer_id = 452363
--order by website_id, updated_at
order by updated_at

	select top 1000 count(*), count(distinct customer_id), count(distinct balance_id)
	from #prep_cust_balance

	select website_id, count(*)
	from #prep_cust_balance
	where updated_at is null
	group by website_id

	select customer_id, amount_customer, final_amount_label, count(*)
	from #prep_cust_balance
	-- where updated_at is null and final_amount_label <> 'ZERO'		
	group by customer_id, amount_customer, final_amount_label
	order by customer_id, amount_customer, final_amount_label

	select customer_id, amount_customer, final_amount_label, sum_bal_hist_cust, count(*)
	from #prep_cust_balance
	group by customer_id, amount_customer, final_amount_label, sum_bal_hist_cust
	order by customer_id, amount_customer, final_amount_label, sum_bal_hist_cust

	select *, abs(amount_customer - isnull(sum_bal_hist_cust, 0))
	from
		(select customer_id, amount_customer, final_amount_label, sum_bal_hist_cust, count(*) num_rows
		from #prep_cust_balance
		group by customer_id, amount_customer, final_amount_label, sum_bal_hist_cust) t
	where amount_customer <> isnull(sum_bal_hist_cust, 0)
	order by final_amount_label, customer_id, amount_customer, sum_bal_hist_cust

	----------------------------

	select balance_id, amount, final_amount_label, sum_bal_hist_bal, count(*)
	from #prep_cust_balance
	where amount <> isnull(sum_bal_hist_bal, 0)
	-- where sum_bal_hist_bal is null and amount <> 0
	group by balance_id, amount, final_amount_label, sum_bal_hist_bal
	order by balance_id, amount, final_amount_label, sum_bal_hist_bal
