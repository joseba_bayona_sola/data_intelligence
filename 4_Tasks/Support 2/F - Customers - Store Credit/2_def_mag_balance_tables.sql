
drop table #prep_cust_balance

select cb.customer_id, cb.balance_id, cb.website_id, cb.amount,
	cb.num_balance, cb.final_amount amount_customer, cb.final_amount_label,	
	-- history_id, 
	cbh.updated_at,
	cbh.action, cbh.balance_amount, cbh.balance_delta, 
	cbh.additional_info, cbh.frontend_comment, -- cbh.is_customer_notified, 
	sum(cbh.balance_delta) over (partition by cb.customer_id) sum_bal_hist_cust, 
	sum(cbh.balance_delta) over (partition by cb.balance_id) sum_bal_hist_bal
into #prep_cust_balance
from 
	(select c.customer_id, cb.balance_id, cb.website_id, cb.amount,
		c.num_balance, c.final_amount, c.final_amount_label	
	from
			(select customer_id, num_balance, final_amount, final_amount_label
			from
				(select customer_id, count(distinct balance_id) num_balance, sum(amount) final_amount, 
					case 
						when (sum(amount) = 0) then 'ZERO'
						when (sum(amount) < 0) then 'NEG'
						when (sum(amount) > 0) then 'POS'
					end final_amount_label
				from Landing.mag.enterprise_customerbalance
				group by customer_id) t
			where final_amount_label = 'POS' -- NEG - POS - ZERO
			) c
		inner join
			Landing.mag.enterprise_customerbalance cb on c.customer_id = cb.customer_id) cb
	left join
		Landing.mag.enterprise_customerbalance_history_aud cbh on cb.balance_id = cbh.balance_id

order by cb.customer_id, updated_at



select *
from #prep_cust_balance
where amount_customer < 0.1
order by customer_id, updated_at

delete from #prep_cust_balance
where customer_id in 
	(select distinct customer_id 
	from #prep_cust_balance
	where amount_customer < 0.1) 

delete from #prep_cust_balance
where customer_id in 
	(select customer_id -- *, abs(final_amount - isnull(sum_bal_hist_cust, 0))
	from
		(select customer_id, amount_customer, final_amount_label, sum_bal_hist_cust, count(*) num_rows
		from #prep_cust_balance
		group by customer_id, amount_customer, final_amount_label, sum_bal_hist_cust) t
	where amount_customer <> isnull(sum_bal_hist_cust, 0))
	

	select top 1000 count(*), count(distinct customer_id), count(distinct balance_id)
	from #prep_cust_balance
	-- where final_amount < 1

	select count(*), count(distinct customer_id), 
		sum(amount_customer), sum(sum_bal_hist_cust)
	from
		(select customer_id, amount_customer, final_amount_label, sum_bal_hist_cust, count(*) num_rows
		from #prep_cust_balance
		-- where final_amount < 1
		group by customer_id, amount_customer, final_amount_label, sum_bal_hist_cust) t


----------------------------------------

select customer_id, sum(balance_delta) balance_after
from #prep_cust_balance
where updated_at > '2017-03-31'
	and balance_delta > 0
group by customer_id
order by customer_id

------------------------------------

drop table #final_cust_balance

select pcb.customer_id, pcb.balance_id, pcb.website_id, pcb.amount_customer, 
	pcb.updated_at, pcb.action, pcb.balance_amount, pcb.balance_delta, 
	pcb.frontend_comment, sum_bal_hist_cust, 
	t.balance_after
into #final_cust_balance
from 
		#prep_cust_balance pcb
	left join
		(select customer_id, sum(balance_delta) balance_after
		from #prep_cust_balance
		where updated_at > '2017-03-31'
			and balance_delta > 0
		group by customer_id) t on pcb.customer_id = t.customer_id
where t.balance_after is null or t.balance_after < pcb.amount_customer
-- where t.balance_after >= pcb.amount_customer
order by pcb.website_id, pcb.customer_id, updated_at


-------------------------------------------------

select top 1000 *
from #final_cust_balance
where customer_id = 2039455
order by website_id, customer_id

	select count(*), count(distinct customer_id), count(distinct balance_id)
	from #final_cust_balance

	select customer_id, 
		count(distinct website_id) dist_websites, max(website_id) website_id, amount_customer, balance_after
	from #final_cust_balance
	group by customer_id, amount_customer, balance_after
	order by dist_websites desc, website_id, customer_id

	select website_id, s.store_name, count(*) num_customers, sum(amount_customer) store_credit_to_expire, sum(isnull(balance_after, 0)) store_credit_in_time
	from
			(select customer_id, 
				count(distinct website_id) dist_websites, max(website_id) website_id, amount_customer, balance_after
			from #final_cust_balance
			group by customer_id, amount_customer, balance_after) t
		left join
			Landing.aux.mag_gen_store_v s on t.website_id = s.store_id
	group by t.website_id, s.store_name
	order by t.website_id, s.store_name

	select website_id, s.store_name, cs.customer_status_name, count(*) num_customers, sum(amount_customer) store_credit_to_expire, sum(isnull(balance_after, 0)) store_credit_in_time
	from
			(select customer_id, 
				count(distinct website_id) dist_websites, max(website_id) website_id, amount_customer, balance_after
			from #final_cust_balance
			group by customer_id, amount_customer, balance_after) t
		left join
			Landing.aux.mag_gen_store_v s on t.website_id = s.store_id
		left join
			Warehouse.act.dim_customer_signature_v cs on t.customer_id = cs.customer_id
	group by t.website_id, s.store_name, cs.customer_status_name
	order by t.website_id, s.store_name, cs.customer_status_name

	select s.store_name, t.customer_id, c.customer_email, t.amount_customer, t.amount_customer - isnull(t.balance_after, 0) balance_expiring
	from
			(select customer_id, 
				count(distinct website_id) dist_websites, max(website_id) website_id, amount_customer, balance_after
			from #final_cust_balance
			group by customer_id, amount_customer, balance_after) t
		left join
			Landing.aux.mag_gen_store_v s on t.website_id = s.store_id
		left join
			Warehouse.gen.dim_customer_scd_v c on t.customer_id = c.customer_id_bk
	where s.store_name in ('visiondirect.co.uk') --  visiondirect.co.uk - 'visiondirect.nl', 'visiondirect.fr'
		and c.unsubscribe_mark_email_magento_f = 'N'
	order by s.store_name, t.customer_id

---------

	select t.*, cs.customer_status_name
	from
			(select customer_id, 
				count(distinct website_id) dist_websites, max(website_id) website_id, amount_customer, balance_after
			from #final_cust_balance
			group by customer_id, amount_customer, balance_after) t
		left join
			Warehouse.act.dim_customer_signature_v cs on t.customer_id = cs.customer_id
	where website_id = 2


----------------------------------

select top 1000 *
from #final_cust_balance
where not(balance_after is null or balance_after < amount_customer)
	and not(balance_after > amount_customer)
order by customer_id