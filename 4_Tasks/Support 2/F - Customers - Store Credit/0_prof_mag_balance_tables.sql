
select top 1000 balance_id, 
	customer_id, website_id, amount, base_currency_code, 
	case 
		when (amount = 0) then 'ZERO'
		when (amount < 0) then 'NEG'
		when (amount > 0) then 'POS'
	end amount_label
from Landing.mag.enterprise_customerbalance
order by customer_id, balance_id

	select count(*), count(distinct balance_id), count(distinct customer_id)
	from Landing.mag.enterprise_customerbalance

	select customer_id, website_id, count(*)
	from Landing.mag.enterprise_customerbalance
	group by customer_id, website_id
	having count(*) > 1
	order by customer_id, website_id

	select website_id, count(*)
	from Landing.mag.enterprise_customerbalance
	group by website_id
	order by website_id

	select cb.website_id, s.tld, s.store_name, cb.num
	from
			(select website_id, count(*) num
			from Landing.mag.enterprise_customerbalance
			group by website_id) cb 
		left join
			Landing.aux.mag_gen_store_v s on cb.website_id = s.store_id
	order by s.tld, cb.website_id

	select base_currency_code, count(*)
	from Landing.mag.enterprise_customerbalance
	group by base_currency_code
	order by base_currency_code

	select 
		case 
			when (amount = 0) then 'ZERO'
			when (amount < 0) then 'NEG'
			when (amount > 0) then 'POS'
		end amount_label, count(*)
	from Landing.mag.enterprise_customerbalance
	group by 
		case 
			when (amount = 0) then 'ZERO'
			when (amount < 0) then 'NEG'
			when (amount > 0) then 'POS'
		end 


	select customer_id, count(distinct balance_id) num_balance, sum(amount) final_amount, 
		case 
			when (sum(amount) = 0) then 'ZERO'
			when (sum(amount) < 0) then 'NEG'
			when (sum(amount) > 0) then 'POS'
		end final_amount_label
	from Landing.mag.enterprise_customerbalance
	group by customer_id
	order by customer_id

	select final_amount_label, count(*), sum(final_amount)
	from
		(select customer_id, sum(amount) final_amount, 
			case 
				when (sum(amount) = 0) then 'ZERO'
				when (sum(amount) < 0) then 'NEG'
				when (sum(amount) > 0) then 'POS'
			end final_amount_label
		from Landing.mag.enterprise_customerbalance
		group by customer_id) t
	group by final_amount_label

	select customer_id, final_amount, final_amount_label
	from
		(select customer_id, sum(amount) final_amount, 
			case 
				when (sum(amount) = 0) then 'ZERO'
				when (sum(amount) < 0) then 'NEG'
				when (sum(amount) > 0) then 'POS'
			end final_amount_label
		from Landing.mag.enterprise_customerbalance
		group by customer_id) t
	where final_amount_label = 'NEG' -- NEG - POS - ZERO
	order by customer_id



	select num_balance, count(*)
	from
		(select customer_id, count(distinct balance_id) num_balance, sum(amount) final_amount, 
			case 
				when (sum(amount) = 0) then 'ZERO'
				when (sum(amount) < 0) then 'NEG'
				when (sum(amount) > 0) then 'POS'
			end final_amount_label
		from Landing.mag.enterprise_customerbalance
		group by customer_id) t
	group by num_balance

--------------------------------------------------------

select top 1000 history_id, updated_at,
	balance_id, action, balance_amount, balance_delta, 
	additional_info, frontend_comment, is_customer_notified
from Landing.mag.enterprise_customerbalance_history_aud
where action = 5 -- 1 - 2 - 3 - 4 - 5
order by updated_at desc

	select count(*), count(distinct history_id), count(distinct balance_id) 
	from Landing.mag.enterprise_customerbalance_history_aud

	-- 1: Add (Cust Serv, Ref Fr, NULL) / 2: Add (Cust Serv, Ref Fr, NULL) / 3: Used in Order / 4: Add (Cust Serv REFUND)/ 5: Add for Used in Order (Cust Serv)
	select action, count(*) 
	from Landing.mag.enterprise_customerbalance_history_aud
	group by action
	order by action