use Warehouse
go


SELECT t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME
	--t2.TableName, 
	-- t2.Records
from
		(SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, TABLE_TYPE
		FROM INFORMATION_SCHEMA.TABLES
		-- WHERE TABLE_SCHEMA = 'dbo'
		) t1
WHERE t1.TABLE_TYPE = 'BASE TABLE'
	and t1.table_name not like '%_wrk' and t1.table_name not like '%aux%'
	and t1.TABLE_SCHEMA in ('gen', 'prod', 'sales', 'act', 'stat')
ORDER BY t1.TABLE_CATALOG, t1.TABLE_SCHEMA, t1.TABLE_NAME;

