
select countries_registered_code, product_type_vat, vat_type_rate_code, vat_rate
from Landing.map.vat_vat_rate
where getutcdate() between vat_time_period_start and vat_time_period_finish
	and (countries_registered_code like 'EU%' or countries_registered_code like 'UK%' or countries_registered_code = 'NON-EU')
order by countries_registered_code, product_type_vat

select ptv.product_id, pf.product_family_name, ptv.product_type_vat, 
	vr.countries_registered_code, vr.vat_rate
from 
		Landing.aux.prod_product_product_type_vat ptv
	inner join
		Warehouse.prod.dim_product_family_v pf on ptv.product_id = pf.product_id_magento
	inner join
		(select countries_registered_code, product_type_vat, vat_type_rate_code, vat_rate
		from Landing.map.vat_vat_rate
		where getutcdate() between vat_time_period_start and vat_time_period_finish
			and (countries_registered_code like 'EU%' or countries_registered_code like 'UK%' or countries_registered_code = 'NON-EU')) vr on ptv.product_type_vat = vr.product_type_vat
order by ptv.product_id, pf.product_family_name, ptv.product_type_vat, vr.countries_registered_code
	
select *
from Warehouse.prod.dim_product_family_v
