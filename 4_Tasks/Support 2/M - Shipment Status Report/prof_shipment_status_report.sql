
select 
	order_id_bk, order_no, order_date, invoice_no, invoice_date, shipment_no, shipment_date, 
	website, 
	order_status_magento_name, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat, local_total_prof_fee
	global_total_inc_vat, global_total_vat, global_total_exc_vat, global_total_prof_fee
from Warehouse.sales.dim_order_header_v
where invoice_date between '2018-04-01' and '2018-07-01'
	and (shipment_date is null or shipment_date > '2018-07-01')
	-- and order_status_magento_name <> 'closed'
	and order_status_magento_name in ('archived', 'awaiting', 'canceled', 'checking', 'closed', 'holded', 'partshipped', 'payment_review', 
		'paypal_canceled_reversal', 'paypal_reversed', 'processing', 'processing_ogone', 'ready_to_ship', 'shipped,warehouse')
order by shipment_date, invoice_date

select order_status_magento_name, count(*)
from Warehouse.sales.dim_order_header_v
where invoice_date between '2018-04-01' and '2018-07-01'
	-- and (shipment_date is null or shipment_date > '2018-07-01')
	and (shipment_date is null)
group by order_status_magento_name
order by order_status_magento_name
