
select order_id_bk, order_no, order_date, local_subtotal, local_total_inc_vat, local_bank_online_given
from Warehouse.sales.dim_order_header_v
where order_id_bk in (1520606, 546361, 449595, 8024668)
order by order_id_bk

select order_line_id_bk, order_id_bk, order_no, order_date, product_id_magento, product_family_name, local_subtotal, local_total_inc_vat, local_bank_online_given
from Warehouse.sales.fact_order_line_v
where order_id_bk in (1520606, 546361, 449595, 8024668)
order by order_id_bk, order_line_id_bk

select order_id_bk, order_no, order_date_c, product_id_magento, product_family_name, local_total_inc_vat
from Warehouse.tableau.fact_ol_v
where order_id_bk in (1520606, 546361, 449595, 8024668)

select order_line_id_bk, order_id_bk, order_no, order_date_c, product_id_magento, product_family_name, local_total_inc_vat, local_bank_online_given
from Warehouse.tableau.fact_ol_new_v
where order_id_bk in (1520606, 546361, 449595, 8024668)


-----------

select top 1000 *
from Landing.aux.mag_sales_flat_creditmemo
where order_id in (1520606, 449595, 546361, 8024668)
order by order_id

select top 1000 item_id, order_id, store_id, created_at, product_id, name, qty_ordered, qty_refunded, base_row_total, base_amount_refunded
from Landing.mag.sales_flat_order_item_aud
where order_id in (1520606, 449595, 546361, 8024668)
order by order_id, item_id

select *
from Landing.aux.mag_sales_flat_creditmemo_item
where order_id in (1520606, 449595, 546361, 8024668)
order by order_id, item_id

-----------

select top 1000 item_id, order_id, store_id, created_at, product_id, name, qty_ordered, qty_refunded, base_row_total, base_amount_refunded
from Landing.mag.sales_flat_order_item_aud
where base_amount_refunded <> 0
	and base_row_total < base_amount_refunded 
order by created_at desc
