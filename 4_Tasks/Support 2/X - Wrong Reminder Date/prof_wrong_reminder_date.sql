
select oh.entity_id, oh.increment_id order_no, oh.created_at order_date, oh.store_id, oh.customer_id, oh.status, 
	oh.reorder_profile_id, 
	oh.reminder_type, 
	oh.reminder_period, rp.interval_days, 
	oh.reminder_date, rp.next_order_date, 
	--dateadd(dd, oh.reminder_period, oh.created_at) order_date_plus_reminder_period,
	dateadd(dd, rp.interval_days, oh.created_at) order_date_plus_interval_days
from 
		Landing.mag.sales_flat_order_aud oh -- sales_flat_order
	left join
		Landing.mag.po_reorder_profile_aud rp on oh.reorder_profile_id = rp.id -- po_reorder_profile
where oh.created_at > '2018-11-27'
	-- and rp.next_order_date <> oh.reminder_date
	-- and rp.interval_days <> oh.reminder_period
-- where oh.entity_id = 8328735
order by entity_id
