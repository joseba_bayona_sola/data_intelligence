

select top 1000 order_line_id_bk, order_id_bk, order_no, 
	product_id_magento, product_family_name, pack_size, qty_pack,
	price_type_name, local_price_pack, global_price_pack, 
	local_subtotal, local_total_inc_vat, local_total_exc_vat, 
	--global_subtotal, global_total_inc_vat, global_total_exc_vat, 
	local_product_cost, local_total_cost,

	local_total_exc_vat - local_total_cost margin, 
	(local_total_exc_vat - local_total_cost) / local_total_exc_vat margin_perc, 

	-- local_subtotal / qty_pack price_pack_1, 
	local_total_exc_vat / qty_pack price_pack_avg, 
	local_total_cost / qty_pack local_cost_avg, 


	(local_total_exc_vat / qty_pack) - (local_total_cost / qty_pack) margin_pkg,
	((local_total_exc_vat / qty_pack) - (local_total_cost / qty_pack)) / (local_total_exc_vat / qty_pack) margin_perc_pack
from Warehouse.tableau.fact_ol_cost_v
where order_id_bk = 8470123

