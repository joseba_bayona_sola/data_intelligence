
-- 7304889: order wrongly refunded (through cancel) + adjustment to give store credit (qty_refunded = 0)
-- 4022714: partial refund: when rounding to int it goes to 0

select order_line_id_bk, order_id_bk, order_no, 
	order_date, invoice_date, shipment_date, refund_date, 
	order_status_name, line_status_name, adjustment_order, 
	product_id_magento, qty_unit, qty_unit_refunded, qty_pack, 
	local_subtotal, local_total_inc_vat, local_total_exc_vat, 
	local_store_credit_given, local_bank_online_given
from Warehouse.sales.fact_order_line_v
where order_id_bk = 7206804

select order_line_id_bk, order_id_bk, order_no, 
	order_date, invoice_date, shipment_date, refund_date, 
	order_status_name, line_status_name, adjustment_order, 
	product_id_magento, qty_unit, qty_pack, 
	local_subtotal, local_total_inc_vat, local_total_exc_vat, 
	local_store_credit_given, local_bank_online_given
from Warehouse.sales.fact_order_line_trans_v
where order_id_bk = 4022714
