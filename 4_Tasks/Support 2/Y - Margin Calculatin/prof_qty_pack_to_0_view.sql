	select idOrderHeader_sk, idOrderLine_sk,
		order_line_id_bk, null order_line_id_bk_c, 
		order_id_bk, null order_id_bk_c, invoice_id, shipment_id, creditmemo_id,
		order_no, invoice_no, shipment_no, creditmemo_no,
		
		order_date, order_date_c, order_week_day, order_time_name, order_day_part,
		case when (invoice_date is not null) then refund_date else null end invoice_date, case when (invoice_date is not null) then refund_date_c else null end invoice_date_c, 
		case when (invoice_date is not null) then refund_week_day else null end invoice_week_day,
		case when (shipment_date_r is not null) then refund_date else null end shipment_date, case when (shipment_date_r is not null) then refund_date_c else null end shipment_date_c, 
		case when (shipment_date_r is not null) then refund_week_day else null end shipment_week_day, null shipment_time_name, null shipment_day_part,
		refund_date, refund_date_c, refund_week_day,

		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name,
		customer_id, null customer_id_c, customer_email, customer_name,
		country_code_ship, country_name_ship, region_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
		customer_unsubscribe_name,
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, prescription_method_name,
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, group_coupon_code_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen,
		order_source, proforma,
		product_type_oh_name, order_qty_time, num_diff_product_type_oh,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name, 
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		eye,
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		warehouse_name, 

		countries_registered_code, product_type_vat,

		qty_unit_refunded * -1 qty_unit, convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack, rank_qty_time, qty_time,

		price_type_name, discount_f,
		local_price_unit, local_price_pack, local_price_pack_discount, 
		global_price_unit, global_price_pack, global_price_pack_discount,

		local_subtotal_m local_subtotal, local_shipping_m local_shipping, local_discount_m local_discount, local_store_credit_used_m local_store_credit_used, 
		local_total_inc_vat_m local_total_inc_vat, local_total_exc_vat_m local_total_exc_vat, local_total_vat_m local_total_vat, local_total_prof_fee_m local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 

		convert(decimal(12, 4), local_subtotal_m * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping_m * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), local_discount_m * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used_m * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), local_total_inc_vat_m * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat_m * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), local_total_vat_m * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee_m * ol.local_to_global_rate) global_total_prof_fee, 
		global_store_credit_given, global_bank_online_given, 

		convert(decimal(12, 4), local_total_inc_vat_vf_m) local_total_inc_vat_vf, convert(decimal(12, 4), local_total_exc_vat_vf_m) local_total_exc_vat_vf, 
		convert(decimal(12, 4), local_total_vat_vf_m) local_total_vat_vf, convert(decimal(12, 4), local_total_prof_fee_vf_m) local_total_prof_fee_vf, 

		0 local_product_cost, 0 local_shipping_cost, 0 local_total_cost, 
		0 global_product_cost, 0 global_shipping_cost, 0 global_total_cost, 

		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		num_erp_allocation_lines
	from 
		(select *, 
			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund * -1 local_discount_m, 
			local_store_credit_used_refund * -1 local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

			local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
			(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
			local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
		from
			(select *,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
				local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
					+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
			from Warehouse.sales.fact_order_line_v
			where -- website = 'visiondirect.es' and -- website_group = 'VisionDirect' -- 
				(local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0)) ol) ol
	where order_id_bk = 4022714