
select top 1000 order_id_bk, order_no, 
	order_date, invoice_date, shipment_date, refund_date, 
	product_type_oh_name, order_status_name, local_total_inc_vat, local_total_exc_vat, local_total_prof_fee, local_total_vat
from Warehouse.sales.dim_order_header_v
where order_date > '2018-09-01'
	and country_code_ship = 'ES'
	and order_status_name <> 'OK'

select *
from Warehouse.sales.fact_order_line_v
where order_id_bk = 7859588

select *
from Warehouse.sales.fact_order_line_inv_v
where order_id_bk = 7859588
