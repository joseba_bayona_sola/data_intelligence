
		select oli.orderlineid_bk, olm.orderlinemagentoid
		into #olim
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm on oli.orderlineid_bk = olm.orderlineid

		select olm.orderlinemagentoid, olm.shipmentNumber, 
			olm.snap_complete, olm.expectedShippingDate, olm.snap_cage
		into #olm
		from 
				Landing.mend.gen_ship_customershipment_olm_v olm
			inner join
				#olim olim on olm.orderlinemagentoid = olim.orderlinemagentoid

		select olim.orderlineid_bk, max(slolm.snap_complete) snap_complete, max(slolm.expectedShippingDate) expectedShippingDate, max(slolm.snap_cage) snap_cage
		into #ol_ship
		from 
				#olim olim
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from #olm
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp2.year) slolm on olim.orderlinemagentoid = slolm.orderlinemagentoid 
		group by olim.orderlineid_bk


		select wsibi.batchstockissueid batchstockissueid_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate,

			CONVERT(INT, (CONVERT(VARCHAR(8), wsiba.allocation_date, 112))) idCalendarAllocationDate, wsiba.allocation_date allocation_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), wsibi.issue_date, 112))) idCalendarIssueDate, wsibi.issue_date issue_date
		into #alloc_order_line_erp_issue_dates
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				(select batchstockissueid, wsibi.issue_date
					-- case when (wsibi.issue_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsibi.issue_date) else wsibi.issue_date end issue_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsibi.issue_date) = stp.year) wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				(select batchstockallocationid, 
					case when (wsiba.allocation_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsiba.allocation_date) else wsiba.allocation_date end allocation_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsiba.allocation_date) = stp.year) wsiba on oli.batchstockallocationid = wsiba.batchstockallocationid
			left join
				#ol_ship slolm on oli.orderlineid_bk = slolm.orderlineid_bk

		select t1.batchstockissueid_bk, 
			t1.snap_complete_date, t2.snap_complete_date, 
			t1.expected_shipment_date, t2.expected_shipment_date, 
			t1.cage_shipment_date, t2.cage_shipment_date, 
			t1.allocation_date, t2.allocation_date, 
			t1.issue_date, t2.issue_date
		from 
				#alloc_order_line_erp_issue_dates t1
			full join
				Landing.aux.alloc_order_line_erp_issue_dates t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
		-- where t1.batchstockissueid_bk is null or t2.batchstockissueid_bk is null
		-- where convert(datetime, t1.snap_complete_date) <> t2.snap_complete_date
		where convert(datetime, t1.expected_shipment_date) <> t2.expected_shipment_date
		-- where convert(datetime, t1.cage_shipment_date) <> t2.cage_shipment_date
		-- where convert(datetime, t1.allocation_date) <> t2.allocation_date
		-- where convert(datetime, t1.issue_date) <> t2.issue_date

		drop table #alloc_order_line_erp_issue_dates
		drop table #olim
		drop table #olm
		drop table #ol_ship
