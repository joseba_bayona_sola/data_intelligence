
	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, sp.duration,
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_aux_alloc_order_line_erp_issue_dates' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime desc

	select count(*)
	from Landing.aux.alloc_order_line_erp_issue_dates

	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_issue_dates

		select oli.orderlineid_bk, max(slolm.snap_complete) snap_complete, max(slolm.expectedShippingDate) expectedShippingDate, max(slolm.snap_cage) snap_cage
		-- into #ol_ship
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm on oli.orderlineid_bk = olm.orderlineid
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp2.year) slolm on olm.orderlinemagentoid = slolm.orderlinemagentoid 
		group by oli.orderlineid_bk

	-- INSERT STATEMENT: From XXXX
		-- issue_date is shippeddate so need to change view: DONE
		-- Need to take Shipment related attributes: From Shipment Records (rel to OLM - OH)
	insert into Landing.aux.alloc_order_line_erp_issue_dates (batchstockissueid_bk, 
		idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
		idCalendarExpectedShipmentDate, expected_shipment_date, 
		idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

		idCalendarAllocationDate, allocation_date, 
		idCalendarIssueDate, issue_date)

		select wsibi.batchstockissueid batchstockissueid_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate,

			CONVERT(INT, (CONVERT(VARCHAR(8), wsiba.allocation_date, 112))) idCalendarAllocationDate, wsiba.allocation_date allocation_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), wsibi.issue_date, 112))) idCalendarIssueDate, wsibi.issue_date issue_date
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				(select batchstockissueid, wsibi.issue_date
					-- case when (wsibi.issue_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsibi.issue_date) else wsibi.issue_date end issue_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsibi.issue_date) = stp.year) wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				(select batchstockallocationid, 
					case when (wsiba.allocation_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsiba.allocation_date) else wsiba.allocation_date end allocation_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsiba.allocation_date) = stp.year) wsiba on oli.batchstockallocationid = wsiba.batchstockallocationid
			left join
				#ol_ship slolm on oli.orderlineid_bk = slolm.orderlineid_bk

-------------------------------------------------------------

		select oli.orderlineid_bk, max(slolm.snap_complete) snap_complete, max(slolm.expectedShippingDate) expectedShippingDate, max(slolm.snap_cage) snap_cage
		-- into #ol_ship
		select *
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm on oli.orderlineid_bk = olm.orderlineid
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp2.year) slolm on olm.orderlinemagentoid = slolm.orderlinemagentoid 
		group by oli.orderlineid_bk


				select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp2.year
				where orderlinemagentoid = 1970324945638773
				
----		
		
		select wsibi.batchstockissueid batchstockissueid_bk, 
			-- CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			-- RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			-- CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			-- CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			-- RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate,

			CONVERT(INT, (CONVERT(VARCHAR(8), wsiba.allocation_date, 112))) idCalendarAllocationDate, wsiba.allocation_date allocation_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), wsibi.issue_date, 112))) idCalendarIssueDate, wsibi.issue_date issue_date
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				(select batchstockissueid, wsibi.issue_date
					-- case when (wsibi.issue_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsibi.issue_date) else wsibi.issue_date end issue_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsibi.issue_date) = stp.year) wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				(select batchstockallocationid, 
					case when (wsiba.allocation_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsiba.allocation_date) else wsiba.allocation_date end allocation_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsiba.allocation_date) = stp.year) wsiba on oli.batchstockallocationid = wsiba.batchstockallocationid

