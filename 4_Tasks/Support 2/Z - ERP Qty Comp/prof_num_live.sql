
	select idWHStockItem_sk, warehousestockitemid_bk, warehouse_name, product_id_magento, stock_item_description, 
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_registered, qty_disposed, qty_on_hold,
		qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold)
	from Warehouse.stock.dim_wh_stock_item_v
	where product_id_magento = product_id_magento
		-- and qty_available = 0 and qty_allocated_stock = 0 and qty_received = 0 -- 178
		and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 710 (2410) (709) (610)
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold and qty_registered = 0 and qty_disposed = 0 -- 122 (969) (123)
		-- and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock -- 0
	order by product_id_magento, stock_item_description, warehouse_name

	select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, batch_stock_register_date,
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, 
		qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, 
		qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where product_id_magento = product_id_magento
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold
		-- and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock
		-- and qty_registered <> qty_registered_sm -- 1691 (1636)
		and qty_disposed <> qty_disposed_sm -- 2490 (2453)

		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 0
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold -- 1104 (1124) (1104) (1007)
		-- and qty_registered - qty_disposed <> qty_registered_sm - qty_disposed_sm -- 1104 (1124) (1104) (1007)
	-- order by product_id_magento, stock_item_description, warehouse_name, batch_stock_register_date
	order by batch_stock_register_date desc, product_id_magento, stock_item_description, warehouse_name

	select top 1000 count(*) over (), 
		wsib.idWHStockItemBatch_sk, wsib.warehousestockitembatchid_bk, 
		wsib.product_id_magento, wsib.product_family_name, wsib.packsize, wsib.warehouse_name, wsib.batch_id, 
		wsib.qty_received, wsib.qty_issued_stock, 
		oli.qty_stock_issue, 
		wsib.local_product_unit_cost
	from
			(select idWHStockItemBatch_sk, sum(qty_stock) qty_stock_issue
			from Warehouse.alloc.fact_order_line_erp_issue_v
			where cancelled_f = 'N'
			group by idWHStockItemBatch_sk) oli
		right join
			Warehouse.stock.dim_wh_stock_item_batch_v wsib on oli.idWHStockItemBatch_sk = wsib.idWHStockItemBatch_sk
	-- where oli.idWHStockItemBatch_sk is null and wsib.qty_issued_stock <> 0 -- 0
	where wsib.qty_issued_stock < oli.qty_stock_issue -- 489
	-- where wsib.qty_issued_stock = oli.qty_stock_issue -- 51629
	-- where wsib.qty_issued_stock > oli.qty_stock_issue -- 2
	order by wsib.product_id_magento, wsib.product_family_name, wsib.packsize, wsib.warehouse_name, wsib.batch_id

	select *
	from Warehouse.alloc.fact_order_line_erp_issue_v
	where cancelled_f = 'N'
		and batch_id = 1667885

	select *
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where batch_id = 1667885

	select *
	from Landing.mend.gen_wh_warehousestockitembatch_v
	where batch_id = 1667885

	select *
	from Landing.mend.wh_warehousestockitembatch_aud
	where batch_id = 1667885


			select id,
				batch_id,
				fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
				receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
				groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
				productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
				exchangeRate, currency,
				createdDate, changeddate
			from openquery(MENDIX_DB_RESTORE,'
				select id,
					batch_id,
					fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
					receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
					groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
					productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
					exchangeRate, currency,
					createdDate, changeddate 					
				from public."inventory$warehousestockitembatch"
				where batch_id = 1667885')
