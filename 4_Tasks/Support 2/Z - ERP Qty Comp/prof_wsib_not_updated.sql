
		drop table #wsib

			-- 
			select id,
				batch_id,
				fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
				receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
				groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
				productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
				exchangeRate, currency,
				createdDate, changeddate
			into #wsib
			from openquery(MENDIX_DB_RESTORE,'
				select id,
					batch_id,
					fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
					receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
					groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
					productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
					exchangeRate, currency,
					createdDate, changeddate 					
				from public."inventory$warehousestockitembatch"
				limit 1000000')

				select t1.*, t2.issuedquantity, t2.changeddate
				from 
						#wsib t1
					inner join 
						Landing.mend.wh_warehousestockitembatch_aud t2 on t1.id = t2.id
				-- where t1.receivedquantity <> t2.receivedquantity
				-- where t1.allocatedquantity <> t2.allocatedquantity
				where t1.issuedquantity <> t2.issuedquantity
				-- where t1.onHoldQuantity <> t2.onHoldQuantity
				-- where t1.stockRegisteredDate <> t2.stockRegisteredDate

				-- where t1.productUnitCost <> t2.productUnitCost
				-- where t1.changeddate <> t2.changeddate
				order by t1.changeddate

--------------------------------------------------------------

	select  
		wsib.warehousestockitembatchid_bk, wsib.batch_id, wsib.qty_issued_stock, oli.qty_stock_issue
	into #wsib_discrepancies
	from
			(select idWHStockItemBatch_sk, sum(qty_stock) qty_stock_issue
			from Warehouse.alloc.fact_order_line_erp_issue_v
			where cancelled_f = 'N'
			group by idWHStockItemBatch_sk) oli
		right join
			Warehouse.stock.dim_wh_stock_item_batch_v wsib on oli.idWHStockItemBatch_sk = wsib.idWHStockItemBatch_sk
	where wsib.qty_issued_stock < oli.qty_stock_issue -- 719

	select wsibd.*, wsib.issuedquantity
	from 
			#wsib_discrepancies wsibd
		inner join
			Landing.mend.wh_warehousestockitembatch_full wsib on wsibd.warehousestockitembatchid_bk = wsib.id
	where wsibd.qty_stock_issue <> wsib.issuedquantity
