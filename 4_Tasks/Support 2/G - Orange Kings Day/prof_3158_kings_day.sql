
select *
from Warehouse.prod.dim_product_family_v
where product_id_magento = 3158

select order_line_id_bk, order_id_bk, order_no, order_date, 
	website, customer_id, customer_email, 
	order_stage_name, order_status_name, 
	product_id_magento, product_family_code, qty_unit, local_total_inc_vat
into #kings_day_promotion
from Warehouse.sales.fact_order_line_v
where product_id_magento = 3158
order by order_date desc

-----

select *
from #kings_day_promotion
order by order_date desc

select ol.order_line_id_bk, ol.order_id_bk, ol.order_no, ol.order_date, 
	ol.website, ol.customer_id, ol.customer_email, 
	ol.order_stage_name, ol.order_status_name, 
	ol.product_id_magento, ol.product_family_code, ol.product_family_name, ol.qty_unit, ol.local_total_inc_vat
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		#kings_day_promotion kdp on ol.order_id_bk = kdp.order_id_bk
order by ol.order_id_bk, ol.order_line_id_bk

select distinct customer_id
from #kings_day_promotion

select c.customer_id_bk, c.customer_email-- , c.unsubscribe_mark_email_magento_f
from
		(select distinct customer_id
		from #kings_day_promotion) kdp
	inner join
		Warehouse.gen.dim_customer_scd_v c on kdp.customer_id = c.customer_id_bk
where c.unsubscribe_mark_email_magento_f = 'N'

drop table #kings_day_promotion