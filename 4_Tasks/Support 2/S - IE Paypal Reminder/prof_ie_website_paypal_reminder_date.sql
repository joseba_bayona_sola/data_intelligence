
select order_id_bk, order_no, order_date, invoice_date, 
	website, payment_method_name, reminder_date
into #ie_payment_method_reminder
from Warehouse.sales.dim_order_header_v
where website = 'visiondirect.ie' and payment_method_name = 'PayPal'
	
select *
from #ie_payment_method_reminder
where reminder_date between '2018-10-01' and getutcdate()
order by reminder_date

drop table #ie_payment_method_reminder