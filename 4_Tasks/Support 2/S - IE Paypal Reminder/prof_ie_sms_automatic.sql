
select top 1000 order_id_bk, order_no, order_date, 
	website, customer_id, customer_email, 
	marketing_channel_name, reminder_date, 
	datediff(dd, order_date, reminder_date)
from Warehouse.sales.dim_order_header_v
where order_date > '2018-10-01'
	and website = 'visiondirect.ie'
	and marketing_channel_name = 'SMS Automatic'
order by order_date
