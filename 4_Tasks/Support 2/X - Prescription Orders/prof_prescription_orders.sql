
select order_id_bk, order_no, order_date, website, 
	customer_id, customer_email, customer_name, 
	prescription_method_name
into #prescription_orders
from Warehouse.sales.dim_order_header_v
where order_date > '2018-12-01' and 
	prescription_method_name in ('Call', 'Upload/Fax/Scan')
order by order_date

select order_id_bk, order_no, order_date, website, 
	customer_id, customer_email, customer_name, 
	prescription_method_name
from #prescription_orders
where customer_id <> 877816
order by order_date

select year(order_date) yyyy, count(*) num_orders
from #prescription_orders
where customer_id <> 877816
group by year(order_date)
order by year(order_date)
