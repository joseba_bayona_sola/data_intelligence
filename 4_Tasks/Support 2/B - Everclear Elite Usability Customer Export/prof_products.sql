
select *
from Warehouse.prod.dim_product_family_v
where product_id_magento in (1108, 1109, 2298, 1083, 1084, 2301)
order by product_family_group_name

-- Rank Seq No - Rank Qty Time - Rank Freq Time - Discount

-- 500 people in the Study

-- How to measure the results

select order_line_id_bk, order_id_bk, order_date, 
	customer_id, customer_order_seq_no, 
	product_id_magento, product_family_name, base_curve, diameter, power, cylinder, axis, addition, dominance, colour, eye, qty_time, 
	count(*) over (partition by order_id_bk) num_lines
into #usability_everclear_elite_ol
from Warehouse.sales.fact_order_line_v
where order_date > '2017-12-01'
	and product_id_magento in (1108, 1109, 2298, 1083, 1084, 2301)
	and country_code_ship = 'GB'
	and order_status_name = 'OK'
order by product_id_magento, customer_order_seq_no

	select product_id_magento, product_family_name, num_lines, count(*), 
		convert(decimal(12, 4), count(*)) * 100 / sum(count(*)) over (partition by product_id_magento) 
	from #usability_everclear_elite_ol
	group by product_id_magento, product_family_name, num_lines
	order by product_id_magento, product_family_name, num_lines

	-- 71240
	select 
		c.first_name + ' ' + c.last_name full_name, c.customer_email, c.phone_number, c.city, c.postcode, 
		ol.customer_order_seq_no cust_order_number, ol.order_date last_purchase_date,
		ol.product_id_magento, ol.product_family_name, ol.base_curve, ol.diameter, ol.power, ol.eye, 
		ol.qty_time
		-- , count(*) over (partition by ol.product_id_magento), count(*) over (partition by ol.product_id_magento, ol.customer_order_seq_no) 
	from 
			#usability_everclear_elite_ol ol
		inner join
			Warehouse.act.dim_customer_signature_v cs on ol.customer_id = cs.customer_id
		inner join
			Warehouse.gen.dim_customer_scd_v c on ol.customer_id = c.customer_id_bk
	where ol.num_lines = 2
		and ol.customer_order_seq_no = cs.num_tot_orders
		and c.country_code_s = 'GB'
	order by product_id_magento, customer_order_seq_no, ol.order_id_bk, ol.eye


drop table #usability_everclear_elite_ol
