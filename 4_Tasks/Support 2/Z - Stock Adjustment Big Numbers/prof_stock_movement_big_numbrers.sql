
select top 1000 batchstockmovementid_bk, 
	warehouse_name, batch_movement_date,
	product_id_magento, product_family_name, packsize, sku, stock_item_description,
	batch_id, batch_stock_move_id, move_id, move_line_id, stock_adjustment_type_name, stock_movement_type_name, 
	qty_movement, local_product_unit_cost, 
	sum(qty_movement) over (partition by batch_id, stock_adjustment_type_name)
from Warehouse.tableau.fact_wh_stock_item_batch_movement_v
order by qty_movement desc


select top 1000 batchstockmovementid_bk, 
	warehouse_name, batch_movement_date,
	product_id_magento, product_family_name, packsize, sku, stock_item_description,
	batch_id, batch_stock_move_id, move_id, move_line_id, stock_adjustment_type_name, stock_movement_type_name, 
	qty_movement, local_product_unit_cost, 
	sum(qty_movement) over (partition by batch_id, stock_adjustment_type_name)
from Warehouse.tableau.fact_wh_stock_item_batch_movement_v
where qty_movement > 21000
	and stock_adjustment_type_name <> 'Standard'
order by stock_item_description desc
