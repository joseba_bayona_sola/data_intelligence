use `dw_flat`;

CREATE OR REPLACE VIEW `dw_flat`.`vw_customers` AS 

SELECT
  `c`.`customer_id`                  AS `customer_id`,
  `c`.`email`                        AS `email`,
  `ds`.`store_name`                  AS `store_name`,
  `c`.`created_at`                   AS `created_at`,
  `c`.`updated_at`                   AS `updated_at`,
  `ds`.`website_group`               AS `website_group`,
  (CASE WHEN (`c`.`old_access_cust_no` = 'LENSWAYUK') THEN 'lensway.co.uk' 
   WHEN (`c`.`old_access_cust_no` = 'LENSONCOUK') THEN 'lenson.co.uk' 
   WHEN (`c`.`old_access_cust_no` = 'LENSONIE') THEN 'lenson.ie'
   WHEN (`c`.`old_access_cust_no` = 'LENSONNL') THEN 'lenson.nl'
   WHEN (`c`.`old_access_cust_no` = 'LENSWAYNL') THEN 'lensway.nl'
   WHEN (`c`.`old_access_cust_no` = 'YOURLENSESNL') THEN 'yourlenses.nl'
   ELSE COALESCE(`cim`.`new_customer_created_in`,`ds2`.`store_name`,`ds`.`store_name`) END) AS `created_in`,
  `c`.`prefix`                       AS `prefix`,
  `c`.`firstname`                    AS `firstname`,
  `c`.`middlename`                   AS `middlename`,
  `c`.`lastname`                     AS `lastname`,
  `c`.`suffix`                       AS `suffix`,
  `c`.`taxvat`                       AS `taxvat`,
  `c`.`postoptics_send_post`         AS `postoptics_send_post`,
  `c`.`facebook_id`                  AS `facebook_id`,
  CAST(`c`.`facebook_permissions` AS CHAR(255) CHARSET latin1) AS `facebook_permissions`,
  CAST('' AS CHAR(1) CHARSET latin1) AS `gender`,
  `c`.`dob`                          AS `dob`,
  `c`.`unsubscribe_all`              AS `unsubscribe_all`,
  `days_worn`.`value`                AS `days_worn`,
  `c`.`parent_customer_id`           AS `parent_customer_id`,
  `c`.`is_parent_customer`           AS `is_parent_customer`,
  `c`.`eyeplan_credit_limit`         AS `eyeplan_credit_limit`,
  `c`.`eyeplan_approved_flag`        AS `eyeplan_approved_flag`,
  `c`.`eyeplan_can_ref_new_customer` AS `eyeplan_can_ref_new_customer`,
  `c`.`cus_phone`                    AS `cus_phone`,
  `c`.`found_us_info`                AS `found_us_info`,
  `c`.`password_hash`                AS `password_hash`,
  `c`.`referafriend_code`            AS `referafriend_code`,
  `c`.`alternate_email`              AS `alternate_email`,
  `c`.`old_access_cust_no`           AS `old_access_cust_no`,
  `c`.`old_web_cust_no`              AS `old_web_cust_no`,
  `c`.`old_customer_id`              AS `old_customer_id`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin1`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin2`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin3`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin4`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin5`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `language`,
  `ohma`.`first_order_date`          AS `first_order_date`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `last_logged_in_date`,
  `ohma`.`last_order_date`           AS `last_order_date`,
  `tord`.`no_of_orders`              AS `num_of_orders`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `card_expiry_date`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifecycle`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_usage`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_geog`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_purch_behaviour`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_eysight`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_sport`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_professional`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifestage`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_vanity`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_2`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_3`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_4`,
  `c`.`default_billing`              AS `default_billing`,
  `c`.`default_shipping`             AS `default_shipping`,
  `ochan`.`channel`                  AS `business_channel`,
  `c`.`website_id`                   AS `website_id`,
  `c`.`dw_synced_at`                 AS `dw_synced_at`,
  `c`.`customer_id`                  AS `unique_id`,
  `c`.`unsubscribe_all_date`         AS `unsubscribe_all_date`
FROM (((((((((`dw_flat`.`dw_customers` `c`
           LEFT JOIN `dw_flat`.`dw_stores` `ds`
             ON ((`ds`.`store_id` = `c`.`store_id`)))
          LEFT JOIN `magento01`.`eav_attribute_option_value` `days_worn`
            ON (((`days_worn`.`option_id` = `c`.`days_worn_info`)
                 AND (`days_worn`.`store_id` = 0))))
         LEFT JOIN `dw_flat`.`customer_created_in_map` `cim`
           ON ((`cim`.`customer_created_in` = `c`.`created_in`)))
        LEFT JOIN `dw_flat`.`customer_first_order` `fo`
          ON ((`fo`.`customer_id` = `c`.`customer_id`)))
       LEFT JOIN `dw_flat`.`dw_order_headers` `oh`
         ON ((`oh`.`order_id` = `fo`.`order_id`)))
      LEFT JOIN `dw_flat`.`dw_stores` `ds2`
        ON ((`oh`.`store_id` = `ds2`.`store_id`)))
     LEFT JOIN `dw_flat`.`dw_order_channel` `ochan`
       ON ((`ochan`.`order_id` = `oh`.`order_id`)))
    LEFT JOIN `dw_flat`.`dw_order_headers_marketing_agg` `ohma`
      ON ((`ohma`.`customer_id` = `c`.`customer_id`)))
   LEFT JOIN `dw_flat`.`dw_customer_total_orders` `tord`
     ON ((`tord`.`customer_id` = `c`.`customer_id`)))
WHERE (`c`.`website_group_id` <> 100)
