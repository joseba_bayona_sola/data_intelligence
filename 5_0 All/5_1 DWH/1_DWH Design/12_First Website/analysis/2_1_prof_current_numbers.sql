
-------------------------------------------------
------ Total Numbers -------

-- 1.906.381
select count(*)
from Landing.mag.customer_entity_flat_aud

-- 1.442.238 // 1.425.618
select count(distinct customer_id_bk)
from Landing.aux.sales_dim_order_header_aud
where order_status_name_bk in ('OK', 'PARTIAL REFUND')

-- 1.425.618
select count(distinct customer_id)
from Warehouse.act.fact_activity_sales_v
where activity_type_name = 'ORDER'

----------------------------------------------------

select top 1000 customer_id_bk, count(distinct s.website_group) dist_website
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id
where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
group by customer_id_bk
having count(distinct s.website_group) = 2
order by dist_website desc, customer_id_bk

select dist_website, count(*)
from
	(select customer_id_bk, count(distinct s.website_group) dist_website
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id
	where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	group by customer_id_bk) t
group by dist_website
order by dist_website

----------------------------------------------------

select top 1000 customer_id_bk, count(distinct s.website_group) dist_website
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
		from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
group by customer_id_bk
having count(distinct s.website_group) = 3
order by dist_website desc, customer_id_bk

select dist_website, count(*)
from
	(select customer_id_bk, count(distinct s.website_group) dist_website
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
			from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
	where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	group by customer_id_bk) t
group by dist_website
order by dist_website


----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------

select c.customer_id_bk, c.dist_website, 
	oh.order_id_bk, oh.order_date, oh.store_id_bk, oh.website_group
into #dist_website_cust_orders
from
		(select customer_id_bk, count(distinct s.website_group) dist_website
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
				from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
		where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
		group by customer_id_bk
		having count(distinct s.website_group) > 1) c
	inner join
		(select oh.order_id_bk, oh.order_date, oh.customer_id_bk, 
			oh.store_id_bk, s.website_group
		from
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
				from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
		where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')) oh on c.customer_id_bk = oh.customer_id_bk
order by c.customer_id_bk, oh.order_date, oh.order_id_bk

select top 1000 customer_id_bk, dist_website, website_group,
	rank() over (partition by customer_id_bk order by order_date, order_id_bk) r_order, 
	count(*) over (partition by customer_id_bk) num_order
from #dist_website_cust_orders
order by customer_id_bk
