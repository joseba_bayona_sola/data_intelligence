
select top 1000 old_order_id, magento_order_id, new_increment_id, created_at,
	store_id, 
	old_customer_id, magento_customer_id, email
from Landing.migra_lenson.migrate_orderdata

select top 1000 db,
	old_order_id, magento_order_id, new_increment_id, created_at,
	store_id, 
	old_customer_id, magento_customer_id, email
from Landing.migra.migrate_orderdata_v

select db, count(old_order_id), count(magento_order_id)
from
	(select db, 
		old_order_id,
		case when (magento_order_id = 0 or magento_order_id is null) then null else magento_order_id end magento_order_id
	from Landing.migra.migrate_orderdata_v) t
group by db
order by db
