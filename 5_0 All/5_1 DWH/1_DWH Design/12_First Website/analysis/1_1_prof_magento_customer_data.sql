
select top 1000 entity_id, email, 
	created_at, store_id, website_id, old_access_cust_no
from Landing.mag.customer_entity_flat_aud

select c.store_id, s.website_group, s.store_name, count(*)
from 
		Landing.mag.customer_entity_flat_aud c
	inner join
		Landing.aux.mag_gen_store_v s on c.store_id = s.store_id
group by c.store_id, s.website_group, s.store_name
order by c.store_id, s.website_group

select old_access_cust_no, count(*)
from Landing.mag.customer_entity_flat_aud
group by old_access_cust_no
having count(*) > 1
order by old_access_cust_no

---------------------------------------------
---------------------------------------------

select top 1000 *
from Landing.mag.customer_entity_flat_aud
where store_id = 1
order by created_at desc

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where store_register = 'postoptics.co.uk' 
	and customer_status_name = 'NEW' 