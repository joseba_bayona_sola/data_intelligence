
select top 1000 order_id_bk, order_no, order_date, 
	store_id_bk, customer_id_bk, 
	order_status_name_bk
from Landing.aux.sales_dim_order_header_aud

select top 1000 order_id_bk, order_no, order_date, 
	oh.store_id_bk, s.website_group, s.store_name, 
	customer_id_bk, 
	order_status_name_bk
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id
where customer_id_bk in (9, 28, 14882, 123102, 233161)
order by customer_id_bk, order_date

select oh.store_id_bk, s.website_group, s.store_name, count(*)
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.aux.mag_gen_store_v s on oh.store_id_bk = s.store_id
where order_status_name_bk <> 'OK'
group by oh.store_id_bk, s.website_group, s.store_name

