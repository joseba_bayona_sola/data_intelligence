
-- Step 1: customer_id_bk, email, store_id, created_at, old_access_cust_no // store_id_oacn, website_group_oacn // migrate_db, migrate_old_customer_id, migrate_created_at

-- Step 2: num_dist_websites - store_id_first_all - first_all_date - store_id_first_non_vd - first_non_vd_date

-- Step 3: migration_date

-- Step 4: store_id_last_vd_before_migration - last_vd_before_migration_date - customer_status_at_migration_date

-- Step 5: store_id_bk_cr - create_date // store_id_bk_reg - register_date


customer_id_bk -- YES
email 
store_id
created_at

old_access_cust_no
store_id_oacn
website_group_oacn

migrate_db
migrate_old_customer_id
migrate_created_at

num_dist_websites

store_id_first_all
first_all_date

store_id_first_non_vd
first_non_vd_date

store_id_last_vd_before_migration
last_vd_before_migration_date
customer_status_at_migration_date

migration_date

store_id_bk_cr -- YES
website_group_cr
create_date -- YES

store_id_bk_reg -- YES
website_group_reg
register_date -- YES

idETLBatchRun_ins
ins_ts
idETLBatchRun_upd
upd_ts

-- Questions

	-- RNB Customers: Haven't done any order	
		-- VD: Will be CR - REG as VD (because the store_id they have) and we don't have information to allocate to right store - date
		-- MIG: Don't have any order info

--------------------------------------------------------------
--------------------------------------------------------------


-- Landing
	-- aux.act_customer_cr_reg
	-- lnd_stg_get_aux_act_customer_cr_reg

-- Staging
	-- dim_customer_cr_reg
	-- stg_dwh_get_act_customer_cr_reg

-- Warehouse
	-- dim_customer_cr_reg
	-- stg_dwh_merge_act_customer_cr_reg

