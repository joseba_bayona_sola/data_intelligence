
select top 1000 count(*)
from Warehouse.gen.dim_customer_v
where migrate_customer_store = 'Lensway NL'

select *
from Warehouse.act.fact_activity_sales_v
where customer_id = 1428164

select cs.customer_status_name, count(*)
from
		(select customer_id_bk
		from Warehouse.gen.dim_customer_v
		where migrate_customer_store = 'Lenson NL') c
	inner join
		Warehouse.act.fact_customer_signature_v cs on c.customer_id_bk = cs.customer_id
group by cs.customer_status_name
order by cs.customer_status_name

select cs.customer_id, 
	oh.order_id_bk, oh.order_no, oh.order_date, oh.store_name, oh.order_status_name
from
		(select customer_id_bk
		from Warehouse.gen.dim_customer_v
		where migrate_customer_store = 'Lenson NL') c
	inner join
		Warehouse.act.fact_customer_signature_v cs on c.customer_id_bk = cs.customer_id
	inner join
		Warehouse.sales.dim_order_header_v oh on cs.customer_id = oh.customer_id
where cs.customer_status_name = 'REGULAR'
order by cs.customer_id
