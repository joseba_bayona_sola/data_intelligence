

	-- Step 1
	select *
	from #act_customer_cr_reg
	-- where old_access_cust_no = 'YOURLENSESNL'
	-- where num_dist_websites = 4
	where store_id_first_all = 20 and store_id_first_non_vd <> 20

	select old_access_cust_no, count(*)
	from #act_customer_cr_reg
	group by old_access_cust_no
	order by old_access_cust_no

	-- Step 2: num_dist_websites / store_id_first_all, first_all_date / store_id_first_non_vd, first_non_vd_date

	select num_dist_websites, count(*)
	from #act_customer_cr_reg
	group by num_dist_websites
	order by num_dist_websites

	select c.num_dist_websites, cs.*
	from 
		#act_customer_cr_reg c
	inner join
		Warehouse.act.fact_customer_signature_v cs on c.customer_id_bk = cs.customer_id
	where customer_status_name = 'RNB' and num_dist_websites <> 0
	order by cs.customer_id desc

	select c.store_id_first_all, s.store_name, s.website_group, count(*)
	from 
			#act_customer_cr_reg c
		inner join
			Landing.aux.mag_gen_store_v s on c.store_id_first_all = s.store_id
	group by c.store_id_first_all, s.store_name, s.website_group
	order by c.store_id_first_all, s.store_name, s.website_group

	select c.store_id_first_non_vd, s.store_name, s.website_group, count(*)
	from 
			#act_customer_cr_reg c
		inner join
			Landing.aux.mag_gen_store_v s on c.store_id_first_non_vd = s.store_id
	group by c.store_id_first_non_vd, s.store_name, s.website_group
	order by c.store_id_first_non_vd, s.store_name, s.website_group

	select c.store_id_last_non_vd, s.store_name, s.website_group, count(*)
	from 
			#act_customer_cr_reg c
		inner join
			Landing.aux.mag_gen_store_v s on c.store_id_last_non_vd = s.store_id
	group by c.store_id_last_non_vd, s.store_name, s.website_group
	order by c.store_id_last_non_vd, s.store_name, s.website_group

	select c.store_id_first_all, s.store_name, s.website_group, 
		c.store_id_first_non_vd, s2.store_name, s2.website_group, count(*)
	from 
			#act_customer_cr_reg c
		inner join
			Landing.aux.mag_gen_store_v s on c.store_id_first_all = s.store_id
		inner join
			Landing.aux.mag_gen_store_v s2 on c.store_id_first_non_vd = s2.store_id
	group by c.store_id_first_all, s.store_name, s.website_group, 
		c.store_id_first_non_vd, s2.store_name, s2.website_group 
	order by c.store_id_first_all, s2.store_name, s2.website_group


	select c.store_id_first_non_vd, s.store_name, s.website_group, 
		c.store_id_last_non_vd, s2.store_name, s2.website_group, count(*)
	from 
			#act_customer_cr_reg c
		inner join
			Landing.aux.mag_gen_store_v s on c.store_id_first_non_vd = s.store_id
		inner join
			Landing.aux.mag_gen_store_v s2 on c.store_id_last_non_vd = s2.store_id
	group by c.store_id_first_non_vd, s.store_name, s.website_group, 
		c.store_id_last_non_vd, s2.store_name, s2.website_group 
	order by c.store_id_first_non_vd, s2.store_name, s2.website_group

			select 
				oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group
			from 
					Landing.aux.sales_dim_order_header_aud oh 
				left join
					(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
					from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
			where oh.customer_id_bk = 294446
			order by oh.order_date


			select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
				oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
				rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
				rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
			from 
					#act_customer_cr_reg t
				left join
					Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				left join
					(select store_id, 
						case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
						case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
					from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
			where t.customer_id_bk in (294446, 234855, 370202)
			order by t.customer_id_bk, oh.order_date

	-- Step 3: migration_date
	select migration_date, count(*)
	from #act_customer_cr_reg
	group by migration_date
	order by migration_date

	select store_id_first_non_vd, migration_date, count(*)
	from #act_customer_cr_reg
	where store_id_first_non_vd is not null
	group by store_id_first_non_vd, migration_date
	order by store_id_first_non_vd, migration_date

	select *
	from #act_customer_cr_reg
	where migration_date < last_non_vd_date
	order by store_id_last_non_vd, last_non_vd_date

	-- Step 4: store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date
	select customer_id_bk, email, 
		store_id, created_at, old_access_cust_no, 
		store_id_first_all, first_all_date,
		store_id_first_non_vd, first_non_vd_date, 
		store_id_last_vd_before_migration, last_vd_before_migration_date, migration_date 
	from #act_customer_cr_reg
	where migration_date is not null
		and store_id_last_vd_before_migration is not null

	-- Step 5: store_id_bk_cr, website_group_cr, create_date / store_id_bk_reg, website_group_reg, register_date

