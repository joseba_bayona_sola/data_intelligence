select customer_id_bk, email, 
	t.store_id, 
	store_id_bk_cr, create_date,

	isnull(wrm.store_id_register, t.store_id) store_id_bk_reg, 
	isnull(wmd.migration_date, t.create_date) register_date
from 
		#act_customer_cr_reg t
	left join
		Landing.map.act_website_register_mapping_aud wrm on t.store_id = wrm.store_id
	left join
		Landing.aux.mag_gen_store_v s on t.store_id_bk_cr = s.store_id
	left join
		Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group