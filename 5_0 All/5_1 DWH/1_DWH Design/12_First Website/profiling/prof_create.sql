

-- Cases
	-- 0 Orders: VD (OK - old_access_cust_no - date) / VD Prev. / Migrate (old_access_cust_no)
	-- XX Orders + Only VD Website (OK - Migrate old_access_cust_no - GL old_access_cust_no - NULL old_access_cust_no and (prev store_id - current store_id (wrong)) // Dates
	-- XX Orders + Only Migrate Website (or VD only post)
	-- XX Orders + Both VD - Migrate Website 

-- 0 orders
select customer_id_bk, email, num_dist_websites,
	store_id, created_at, old_access_cust_no,
	migrate_db, migrate_created_at, 
	store_id_first_all

	, store_id_bk_cr, create_date
from #act_customer_cr_reg
where store_id_first_all is null
order by old_access_cust_no, created_at

-- XX Orders + Only VD Website
select customer_id_bk, email, num_dist_websites,
	store_id, created_at, old_access_cust_no,
	migrate_db, migrate_created_at,
	store_id_first_all, first_all_date, store_id_first_vd, first_vd_date, migration_date

	, store_id_bk_cr, create_date
from #act_customer_cr_reg
where store_id_first_all is not null and store_id_last_non_vd is null
	-- and created_at > first_all_date
order by old_access_cust_no, first_all_date

-- XX Orders + Only Migrate Website (or VD only post)
select customer_id_bk, email, num_dist_websites,
	store_id, created_at, old_access_cust_no,
	migrate_db, migrate_created_at,
	store_id_first_all, first_all_date, store_id_last_non_vd, last_non_vd_date, migration_date, non_vd_customer_status_at_migration_date, 
	store_id_first_vd, first_vd_date

	, store_id_bk_cr, create_date
from #act_customer_cr_reg
where store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null
	-- and created_at > first_all_date
	-- and store_id_first_vd is not null and first_vd_date < last_non_vd_date
order by old_access_cust_no, store_id_first_all, created_at

-- XX Orders + Both VD - Migrate Website 
select customer_id_bk, email, num_dist_websites,
	store_id, created_at, old_access_cust_no,
	-- migrate_db, migrate_created_at,
	-- store_id_first_all, first_all_date, 
	store_id_first_vd, first_vd_date, 
	store_id_last_non_vd, last_non_vd_date, non_vd_customer_status_at_migration_date, 
	store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date, 
	migration_date

	, store_id_bk_cr, create_date
from #act_customer_cr_reg
where store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null
	-- and non_vd_customer_status_at_migration_date = 'LAPSED' and customer_status_at_migration_date = 'LAPSED'
order by store_id_last_non_vd, created_at
-- order by old_access_cust_no, store_id_last_non_vd, created_at


--------------------------------------------------------------------------------------------------------




--------------------------------------------------------------------------------------------------------
