
-- MutuellesOrders
select [entity_id] as Order_ID, mutual_quotation_id
from landing.[mag].[sales_flat_order_aud] orders 
where mutual_quotation_id is not null  

-- OrderData
select * 
from warehouse.[tableau].[fact_ol_new_v]
where year(order_date_c) > 2016 and
	[tld]  = '.fr' and
	[order_id_bk] in (select Order_ID from MutuellesOrders)

SELECT [quotation_id], od.order_id_bk as OrderID,
	m.[name], api.code as [Platform],
	cus.customer_id_bk, cus.customer_email, cus.first_name,
	[type_id], mq.[is_online], [status_id], [error_id], [reference_number], [amount], [expired_at], 
	mq.[created_at], mq.[updated_at], mq.[idETLBatchRun], mq.[ins_ts], 

	[order_line_id_bk], [order_line_id_bk_c], [order_id_bk], [order_id_bk_c],
	[order_no], [invoice_no],[shipment_no], 
	[order_date_c], [order_week_day]
      ,[order_day_part]
      ,[invoice_date_c]
      ,[invoice_week_day]
      ,[shipment_date_c]
      ,[shipment_week_day]
      ,[rank_shipping_days]
      ,[shipping_days]
      ,[tld]
      ,[website_group]
      ,[website]
      ,[store_name]
      ,[company_name_create]
      ,[website_group_create]
      ,[website_create]
      ,[customer_origin_name]
      ,[market_name]
      ,[customer_id_c]
      ,[customer_name]
      ,[country_code_ship]
      ,[region_name_ship]
      ,[country_code_bill]
      ,[order_stage_name]
      ,[order_status_name]
      ,[line_status_name]
      ,[order_type_name]
      ,[order_status_magento_name]
      ,[payment_method_name]
      ,[shipping_carrier_name]
      ,[reminder_type_name]
      ,[reorder_f]
      ,[channel_name]
      ,[marketing_channel_name]
      ,[group_coupon_code_name]
      ,[coupon_code]
      ,[customer_status_name]
      ,[rank_seq_no]
      ,[customer_order_seq_no]
      ,[rank_seq_no_web]
      ,[customer_order_seq_no_web]
      ,[proforma]
      ,[manufacturer_name]
      ,[product_type_name]
      ,[category_name]
      ,[product_family_group_name]
      ,[cl_type_name]
      ,[cl_feature_name]
      ,[product_id_magento]
      ,[product_family_name]
      ,[base_curve]
      ,[diameter]
      ,[power]
      ,[cylinder]
      ,[axis]
      ,[addition]
      ,[dominance]
      ,[colour]
      ,[sku_magento]
	  ,[price_type_name]
      ,[discount_f]
      ,[local_price_unit]
      ,[local_price_pack]
      ,[local_price_pack_discount]
      ,[local_shipping]
      ,[local_discount]
      ,[local_store_credit_used]
      ,[local_total_inc_vat]
      ,[local_total_exc_vat]
      ,[local_total_vat]
      ,[local_total_prof_fee]
      ,[local_store_credit_given]
      ,[local_bank_online_given]
      ,[global_shipping]
      ,[global_discount]
      ,[global_store_credit_used]
      ,[global_total_inc_vat]
      ,[global_total_exc_vat]
      ,[global_total_vat]
      ,[global_total_prof_fee]
      ,[global_store_credit_given]
      ,[global_bank_online_given]
      ,[local_total_inc_vat_vf]
      ,[local_total_exc_vat_vf]
      ,[local_total_vat_vf]
      ,[local_total_prof_fee_vf]
  FROM
		[Landing].[mag].[mutual_quotation] mq
	left join 
		landing.[mag].[mutual_customer] mc on mc.[mutual_customer_id] = mq.mutual_customer_id
	left join 
		landing.[mag].[mutual] m on m.mutual_id = mq.mutual_id
	left join 
		landing.[mag].[mutual_api] api on m.api_id = api.api_id
	left join 
		[Warehouse].[gen].[dim_customer] cus on mc.customer_id = cus.customer_id_bk
	left join 
		MutuellesOrders mo on mq.[quotation_id] = mo.mutual_quotation_id
	left join 
		OrderData od on mo.Order_ID = od.order_id_bk

