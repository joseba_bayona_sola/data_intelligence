--Updates required to correct the GA export mutuelle names values

update dbo.ga_mutuelle_step2
	set Event_Label = 'Unéo' where Event_Label = 'UnÃ©o'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Prévifrance' where Event_Label = 'PrÃ©vifrance'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Mutuelle Générale' where Event_Label = 'Mutuelle GÃ©nÃ©rale'
	
update dbo.ga_mutuelle_step2
	set Event_Label = 'Intégrance' where Event_Label = 'IntÃ©grance'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Prévifrance' where Event_Label = 'PrÃ©vifrance'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Ipsec Avenir Santé' where Event_Label = 'Ipsec Avenir SantÃ©'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Viasanté' where Event_Label = 'ViasantÃ©'

update dbo.ga_mutuelle_step2
	set Event_Label = 'VIASANTÉ' where Event_Label = 'VIASANTÃ‰'

update dbo.ga_mutuelle_step2
	set Event_Label = 'Apréva' where Event_Label = 'AprÃ©va'	
	
-----------------------------------------------------------------------------------

--ga_mutuelle_journey: join of the 3 tables populated with magento data 

drop view dbo.ga_mutuelle_journey_v
go

create view dbo.ga_mutuelle_journey_v as
select 
	convert(date, convert(varchar(8), s1.Date)) ga_session_date, 
	s1.User_ID			ga_user_id, 
	s1.Session_ID		ga_session_id,	
	s1.Event_Label		new_existing_mut,
	s2.Event_Action		auto_manual_quote, 
	s2.Event_Label		mutuelle,
	s3.Event_Action		quote_result, 
	s3.Event_Label		percent_funded,
	mq.*
from 
	dbo.ga_mutuelle_step1 s1	--first step: user clicked on mutuelle
left outer join 
	dbo.ga_mutuelle_step2 s2	--second step: user requested a quote
on
	s1.session_id = s2.session_id
	and (case when s1.Event_Label = 'existing mutuelle' then 1 
			  when s1.Event_Label = 'new mutuelle' then 2 
		 end) =
		(case when s2.Event_Category = 'existing mutuelle interaction' then 1 
		      when s2.Event_Category = 'new mutuelle interaction' then 2 
		 end) 
left outer join
	dbo.ga_mutuelle_step3 s3	--3rd step: quote was accepted or rejected
on s2.session_id = s3.session_id
left join
	Warehouse.sales.fact_mutual_quotation_v mq --join with the magento mutuelle data
on	s1.date = convert(date, mq.quotation_date)
	and s1.User_ID = mq.customer_id
	and s2.Event_Label = mq.mutual_name
--only including customers with one combination of date, user and session id
where 
	s1.User_ID in 
	(select 
		User_ID
	from 
		dbo.ga_mutuelle_step1
	group by 
		date, User_ID, Session_ID
	having count(1) = 1);

----------------------------------------------------------------

--GA data join with Mutuelles Magento Data

/*drop view dbo.ga_mutuelle_journey_v2
go

create view dbo.ga_mutuelle_journey_v2 as
select *
from 
	dbo.ga_mutuelle_journey_v ga
left join
	Warehouse.sales.fact_mutual_quotation_v mq
on	ga.date = convert(date, mq.quotation_date)
	and ga.User_ID = mq.customer_id
	and ga.mutuelle = mq.mutual_name*/

--Query used to check the amounts and percentages financed by mutuelles
	
select 
	ga.user_id, ga.date, ga.percent_funded, ga.local_total_inc_vat, ga.mutual_amount, ga.order_id_bk,
	ga.mutuelle, ga.auto_manual_quote, ga.quote_result, ga.customer_id
from dbo.ga_mutuelle_journey_v ga
where order_id_bk is not null