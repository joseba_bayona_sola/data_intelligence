
use DW_GetLenses_jbs
go


/*Step 1 : Anyone who clicked on a mutuelle field
event_label: the customer chose either an existing or new mutuelle
*/

select * 
from dbo.ga_mutuelle_step1 s1;

select session_id, date, user_id, 
	event_category, event_label, 
	sessions, total_events, unique_events, 
	count(*) over (partition by session_id) num_rep, 
	count(*) over (partition by user_id) num_rep_cust 
from dbo.ga_mutuelle_step1 s1
-- order by num_rep desc, user_id, date, session_id, event_label;
-- order by num_rep_cust desc, num_rep desc, user_id, date, session_id, event_label;
order by date

	select count(*), count(distinct session_id), count(distinct user_id)
	from dbo.ga_mutuelle_step1 s1;

	select session_id, count(*)
	from dbo.ga_mutuelle_step1 s1
	group by session_id
	order by count(*) desc, session_id;

	select session_id, event_label, count(*)
	from dbo.ga_mutuelle_step1 s1
	group by session_id, event_label
	order by count(*) desc, session_id;

	select session_id, user_id, date, event_label, count(*)
	from dbo.ga_mutuelle_step1 s1
	group by session_id, user_id, date, event_label
	order by count(*) desc, session_id;

	select event_category, count(*)
	from dbo.ga_mutuelle_step1 
	group by event_category
	order by event_category

	select event_label, count(*)
	from dbo.ga_mutuelle_step1 
	group by event_label
	order by event_label

		----------------------- 
		select 
			count(1), 
			count(distinct(event_label)) distinct_ev_label
		from dbo.ga_mutuelle_step1 s1;

		select 
			count(1), 
			count(distinct(session_id)), 
			count(distinct(user_id)) 
		from dbo.ga_mutuelle_step1 s1;

		select distinct Event_Category from dbo.ga_mutuelle_step1 s1;

		select distinct Event_Label from dbo.ga_mutuelle_step1 s1;

		-----------------------

/*Step 2: anyone who was accepted or rejected from a mutuelle
Event_Category: wether or not the customer is interacting with a new or existing mutuelle
event_label: mutuelle involved in the process
*/

select * from dbo.ga_mutuelle_step2 s2;

select session_id, user_id, 
	event_category, event_label, event_action,
	sessions, total_events, unique_events, 
	count(*) over (partition by session_id) num_rep, 
	count(*) over (partition by user_id) num_rep_cust  
from dbo.ga_mutuelle_step2 s2
where session_id in ('1525364200313.1hxmh35', '1530612652604.wyq4mk1b')
-- order by num_rep desc, user_id, session_id, event_label;
order by num_rep_cust desc, num_rep desc, user_id, session_id, event_label;

	select count(*), count(distinct session_id), count(distinct user_id)
	from dbo.ga_mutuelle_step2 s1;

	select session_id, count(*)
	from dbo.ga_mutuelle_step2 s1
	group by session_id
	order by count(*) desc, session_id;

	select session_id, event_category, event_label, count(*)
	from dbo.ga_mutuelle_step2 s1
	group by session_id, event_category, event_label
	order by count(*) desc, session_id;

	select session_id, user_id, event_category, event_label, count(*)
	from dbo.ga_mutuelle_step2 s1
	group by session_id, user_id, event_category, event_label
	order by count(*) desc, session_id;

	select event_category, count(*)
	from dbo.ga_mutuelle_step2 
	group by event_category
	order by event_category

	select event_label, count(*)
	from dbo.ga_mutuelle_step2 
	group by event_label
	order by event_label

	select event_action, count(*)
	from dbo.ga_mutuelle_step2 
	group by event_action
	order by event_action

		-----------------------
		select 
			count(1),
			count(distinct(Event_Category)) event_category,
			count(distinct(Event_Action)) event_action
		from dbo.ga_mutuelle_step2 s2;

		select 
			count(1),	
			count(distinct(session_id)), 
			count(distinct(user_id)) 
		from dbo.ga_mutuelle_step2 s2;

		select distinct Event_Category from dbo.ga_mutuelle_step2 s2;

		select distinct Event_Action from dbo.ga_mutuelle_step2 s2;

		select distinct Event_Label from dbo.ga_mutuelle_step2 s2;
		-----------------------


/*Step 3: anyone who requested a manual or automatic quote
event_action: status of the mutuelle quotation request (accepted, refused, other payment, more info)
event_label: percentage of the order financed by the mutuelle
*/

select * from dbo.ga_mutuelle_step3 s3;

select session_id, user_id, date,
	event_label, event_action,
	sessions, total_events, unique_events, 
	count(*) over (partition by session_id) num_rep, 
	count(*) over (partition by user_id) num_rep_cust   
from dbo.ga_mutuelle_step3 s3
where session_id in ('1527084977820.ouiabkdx', '1533593132919.bum5sui')

	select count(*), count(distinct session_id), count(distinct user_id)
	from dbo.ga_mutuelle_step2 s1;

	select session_id, count(*)
	from dbo.ga_mutuelle_step3 s1
	group by session_id
	order by count(*) desc, session_id;

	select session_id, user_id, event_action, event_label, count(*)
	from dbo.ga_mutuelle_step3 s1
	group by session_id, user_id, event_action, event_label
	order by count(*) desc, session_id;

	select event_label, count(*)
	from dbo.ga_mutuelle_step3 
	group by event_label
	order by event_label

	select event_action, count(*)
	from dbo.ga_mutuelle_step3
	group by event_action
	order by event_action

		-----------------------
		select 
			distinct Event_Action
		from dbo.ga_mutuelle_step3 s3;

		select
			count(1),	
			count(distinct(session_id)), 
			count(distinct(user_id)) 
		from dbo.ga_mutuelle_step3 s3;

		select distinct Event_Action from dbo.ga_mutuelle_step3 s3;
		-----------------------


