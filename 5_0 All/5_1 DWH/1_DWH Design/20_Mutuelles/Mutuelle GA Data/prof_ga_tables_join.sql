
select session_id, date, user_id, 
	event_category, event_label, 
	case 
		when (event_label = 'new mutuelle') then 'N' 
		when (event_label = 'existing mutuelle') then 'E' 
		else NULL 
	end interaction_type,
	sessions, total_events, unique_events
into #ga_mutuelle_step1
from dbo.ga_mutuelle_step1 s1

select session_id, user_id, 
	event_category, event_label, event_action,
	case 
		when (event_category = 'new mutuelle interaction') then 'N' 
		when (event_category = 'existing mutuelle interaction') then 'E' 
		else NULL 
	end interaction_type,
	sessions, total_events, unique_events
into #ga_mutuelle_step2
from dbo.ga_mutuelle_step2 s2

select session_id, user_id, date,
	event_label, event_action,
	sessions, total_events, unique_events   
into #ga_mutuelle_step3
from dbo.ga_mutuelle_step3 s3

-----------------------------------------------------------

	select s1.session_id, s1.date, s1.user_id, 
		-- s1.event_category, s1.event_label, 
		s1.interaction_type, 
		-- s2.event_category, 
		s2.event_label mutuel, s2.event_action quote_type,
		s1.sessions, s1.total_events, s1.unique_events, 
		s2.sessions, s2.total_events, s2.unique_events, 
		count(*) over (partition by s1.session_id, s1.interaction_type) num_rep, 
		count(*) over (partition by s1.session_id, s1.interaction_type, s2.event_label) num_rep2  
	from 
			#ga_mutuelle_step1 s1
		left join
			#ga_mutuelle_step2 s2 on s1.session_id = s2.session_id and s1.interaction_type = s2.interaction_type
	-- where s1.session_id in ('1525364200313.1hxmh35', '1530612652604.wyq4mk1b')
	-- where s2.session_id is null
	order by num_rep desc

	select s1.session_id, s1.date, s1.user_id, 
		-- s1.event_category, s1.event_label, 
		s1.interaction_type, 
		-- s2.event_category, 
		s2.event_label mutuel, s2.event_action quote_type, s3.event_action,
		s1.sessions, s1.total_events, s1.unique_events, 
		s2.sessions, s2.total_events, s2.unique_events, 
		s3.sessions, s3.total_events, s3.unique_events,   
		count(*) over (partition by s1.session_id, s1.interaction_type) num_rep, 
		count(*) over (partition by s1.session_id, s1.interaction_type, s2.event_label) num_rep2  
	from 
			#ga_mutuelle_step1 s1
		left join
			#ga_mutuelle_step2 s2 on s1.session_id = s2.session_id and s1.interaction_type = s2.interaction_type
		left join
			#ga_mutuelle_step3 s3 on s2.session_id = s3.session_id 
	-- where s1.session_id in ('1525364200313.1hxmh35', '1530612652604.wyq4mk1b')
	-- where s2.session_id is null and s3.session_id is not null
	order by num_rep desc

-------------------------------------------------------------

select 
	convert(date, convert(varchar(8), s1.Date)) ga_session_date, 
	s1.User_ID			ga_user_id, 
	s1.Session_ID		ga_session_id,	
	s1.Event_Label		new_existing_mut,
	s2.Event_Action		auto_manual_quote, 
	s2.Event_Label		mutuelle,
	s3.Event_Action		quote_result, 
	s3.Event_Label		percent_funded, 
	mq.*
from 
	dbo.ga_mutuelle_step1 s1	--first step: user clicked on mutuelle
left outer join 
	dbo.ga_mutuelle_step2 s2	--second step: user requested a quote
on
	s1.session_id = s2.session_id
	and (case when s1.Event_Label = 'existing mutuelle' then 1 
			  when s1.Event_Label = 'new mutuelle' then 2 
		 end) =
		(case when s2.Event_Category = 'existing mutuelle interaction' then 1 
		      when s2.Event_Category = 'new mutuelle interaction' then 2 
		 end) 
left outer join
	dbo.ga_mutuelle_step3 s3	--3rd step: quote was accepted or rejected
on s2.session_id = s3.session_id
left join
	Warehouse.sales.fact_mutual_quotation_v mq --join with the magento mutuelle data
on	s1.date = convert(date, mq.quotation_date)
	and s1.User_ID = mq.customer_id
	and s2.Event_Label = mq.mutual_name
where 
	s1.User_ID in 
	(select 
		User_ID
	from 
		dbo.ga_mutuelle_step1
	group by 
		date, User_ID, Session_ID
	having count(1) = 1);
