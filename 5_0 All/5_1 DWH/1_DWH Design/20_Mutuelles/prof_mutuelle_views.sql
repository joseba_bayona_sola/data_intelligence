
	select mq.idMutualQuotation_sk, mq.quotation_id_bk, mc.customer_id_bk, mq.mutual_customer_id,  oh.order_id_bk, 
		mq.quotation_date, 
		m.mutual_name, mq.is_online, m.api_code,
		mqt.mutual_quotation_type_name, 
		mq.error_id, mq.mutual_amount, 
		mc.customer_email, mc.landing_page_info, -- count of error previous a purchase

		oh.order_date, -- oh.invoice_date, oh.shipment_date, 
		--oh.rank_shipping_days, oh.shipping_days, 
		--oh.website,
		--oh.customer_id, oh.customer_email, oh.country_code_ship, 
		--oh.order_status_name, oh.payment_method_name, 
		--oh.customer_status_name, oh.rank_seq_no_gen, oh.customer_order_seq_no_gen,
		--oh.rank_order_qty_time, oh.order_qty_time,
		oh.local_total_inc_vat, oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, oh.local_total_refund, 
		oh.global_total_inc_vat, oh.global_total_exc_vat, oh.global_total_vat, oh.global_total_prof_fee, oh.global_total_refund		
	from 
			Warehouse.sales.fact_mutual_quotation mq
		inner join
			Warehouse.sales.dim_mutual m on mq.idMutual_sk_fk = m.idMutual_sk
		inner join
			Warehouse.sales.dim_mutual_quotation_type mqt on mq.idMutualQuotationType_sk_fk = mqt.idMutualQuotationType_sk
		left join
			Warehouse.sales.dim_mutual_customer mc on mq.idCustomer_sk_fk = mc.idCustomer_sk_fk
		left outer join
			Warehouse.sales.dim_order_header_v oh on mq.idOrderHeader_sk_fk = oh.idOrderHeader_sk	

	-- where customer_id_bk is not null
	order by customer_id_bk

	select mqrh.idMutualQuotationRequestHistory_sk, mqrh.history_id_bk, mc.customer_id_bk, mqrh.mutual_customer_id, 
		mqrh.quotation_request_history_date,
		m.mutual_name, m.is_online, m.api_code,
		mqe.error_name, 
		mc.customer_email, mc.landing_page_info
	from 
			Warehouse.sales.fact_mutual_quotation_request_history mqrh
		inner join
			Warehouse.sales.dim_mutual m on mqrh.idMutual_sk_fk = m.idMutual_sk
		inner join
			Warehouse.sales.dim_mutual_quotation_error mqe on mqrh.idMutualQuotationError_sk_fk = mqe.idMutualQuotationError_sk
		inner join
			Warehouse.sales.dim_mutual_customer mc on mqrh.idCustomer_sk_fk = mc.idCustomer_sk_fk

select *
from Warehouse.sales.fact_mutual_quotation_request_history

select *
from Warehouse.sales.dim_mutual_quotation_error
order by error_id
