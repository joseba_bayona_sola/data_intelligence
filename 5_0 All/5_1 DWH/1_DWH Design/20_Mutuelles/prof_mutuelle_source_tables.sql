
-- Full - mutual

select count(*)
from Landing.mag.mutual

select count(*)
from Landing.mag.mutual_aud

select mutual_id, code, name, is_online, 
	amc_number, opening_hours, api_id, priority
from Landing.mag.mutual_aud
order by is_online desc, name

	select is_online, count(*)
	from Landing.mag.mutual_aud
	group by is_online
	order by is_online

------------------------------------------------------------------

-- Full - mutual_api

select count(*)
from Landing.mag.mutual_api

select count(*)
from Landing.mag.mutual_api_aud

	select api_id, code, convention_type, 
		endpoint, proxy_host, proxy_port
	from Landing.mag.mutual_api_aud

	select m.mutual_id, m.code, m.name, m.is_online, 
		m.amc_number, m.opening_hours, m.priority, 
		m.api_id, ma.code, ma.convention_type, ma.endpoint, ma.proxy_host, ma.proxy_port
	from 
			Landing.mag.mutual_aud m
		left join 
			Landing.mag.mutual_api_aud ma on m.api_id = ma.api_id
	order by is_online desc, m.api_id, name


------------------------------------------------------------------

-- Full - mutual_customer

select count(*)
from Landing.mag.mutual_customer

select count(*)
from Landing.mag.mutual_customer_aud

	select mc1.*
	from 
			Landing.mag.mutual_customer_aud mc1
		left join
			Landing.mag.mutual_customer mc2 on mc1.mutual_customer_id = mc2.mutual_customer_id
	where mc2.mutual_customer_id is null
	order by mc1.mutual_customer_id

	select mutual_customer_id, 
		customer_id, mutual_id, 
		firstname, lastname, dob, phone_number, 
		created_at, updated_at, 
		count(*) over (partition by customer_id) num_rep_customer, 
		count(*) over (partition by customer_id, mutual_id) num_rep_customer_mutual
	from Landing.mag.mutual_customer_aud
	where customer_id in (1554902, 2446439)
	-- order by num_rep_customer_mutual desc, num_rep_customer desc, mutual_customer_id
	order by customer_id, mutual_customer_id

	select count(*), count(distinct mutual_customer_id), count(distinct customer_id)
	from Landing.mag.mutual_customer_aud

	select customer_id, count(*) n1, count(distinct mutual_id) n2, count(distinct firstname + ' ' + lastname) n3
	from Landing.mag.mutual_customer_aud
	group by customer_id
	order by n3 desc, customer_id

	select mc.customer_id, c.customer_email, 
		mc.n1, mc.n2, mc.n3 
	from
			(select customer_id, count(*) n1, count(distinct mutual_id) n2, count(distinct firstname + ' ' + lastname) n3
			from Landing.mag.mutual_customer_aud
			group by customer_id) mc
		left join
			Landing.aux.gen_dim_customer_aud c on mc.customer_id = c.customer_id_bk
	-- where c.customer_id_bk is null
	order by n3 desc, customer_id

------------------------------------------------------------------


-- Full - mutual_quotation
	
	-- Why Physical deletes on mutual_quotation 
select count(*)
from Landing.mag.mutual_quotation

select count(*)
from Landing.mag.mutual_quotation_aud

	select mq1.*
	from 
			Landing.mag.mutual_quotation_aud mq1
		left join
			Landing.mag.mutual_quotation mq2 on mq1.quotation_id = mq2.quotation_id
	where mq2.quotation_id is null
	order by mq1.quotation_id

	select mq1.mutual_customer_id, mc.customer_id, mc.firstname, mc.lastname, mq1.num
	from
			(select mq1.mutual_customer_id, count(*) num
			from 
					Landing.mag.mutual_quotation_aud mq1
				left join
					Landing.mag.mutual_quotation mq2 on mq1.quotation_id = mq2.quotation_id
			where mq2.quotation_id is null
			group by mq1.mutual_customer_id) mq1
		left join
			Landing.mag.mutual_customer_aud mc on mq1.mutual_customer_id = mc.mutual_customer_id
	order by mq1.mutual_customer_id

			

select quotation_id, 
	mutual_id, mutual_customer_id, 
	type_id, is_online, status_id, error_id, reference_number, 
	amount, 
	created_at, expired_at, updated_at
from Landing.mag.mutual_quotation_aud
where mutual_customer_id = 8
order by quotation_id

	-- Meaning of values
	select type_id, count(*)
	from Landing.mag.mutual_quotation_aud
	group by type_id
	order by type_id

	select type_id, is_online, count(*)
	from Landing.mag.mutual_quotation_aud
	group by type_id, is_online
	order by type_id, is_online

	select is_online, count(*)
	from Landing.mag.mutual_quotation_aud
	group by is_online
	order by is_online

	select status_id, count(*)
	from Landing.mag.mutual_quotation_aud
	group by status_id
	order by status_id

	select error_id, count(*)
	from Landing.mag.mutual_quotation_aud
	group by error_id
	order by error_id

	select mutual_customer_id, count(*)
	from Landing.mag.mutual_quotation_aud
	group by mutual_customer_id
	order by count(*) desc, mutual_customer_id



		select mq.quotation_id, 
			mq.mutual_id, m.code, m.name, m.api_id, m.code_api, m.convention_type, m.endpoint,
			mq.mutual_customer_id, mc.customer_id, mc.firstname, mc.lastname,
			mq.type_id, mq.is_online, mq.status_id, mq.error_id, mq.reference_number, 
			mq.amount, 
			mq.created_at-- , expired_at, updated_at
		from 
				Landing.mag.mutual_quotation_aud mq
			left join
				(select m.mutual_id, m.code, m.name, m.is_online, 
					m.amc_number, m.opening_hours, m.priority, 
					m.api_id, ma.code code_api, ma.convention_type, ma.endpoint, ma.proxy_host, ma.proxy_port
				from 
						Landing.mag.mutual_aud m
					left join 
						Landing.mag.mutual_api_aud ma on m.api_id = ma.api_id) m on mq.mutual_id = m.mutual_id
			left join
				Landing.mag.mutual_customer_aud mc on mq.mutual_customer_id = mc.mutual_customer_id
		-- where m.mutual_id is null -- 0
		-- where m.api_id is null -- 937
		-- where mc.mutual_customer_id is null -- 469
		-- where mc.mutual_customer_id is not null and mq.mutual_id <> mc.mutual_id -- 0
		-- where mc.customer_id = 2446439
		where mq.mutual_customer_id = 4715
		order by mq.quotation_id


------------------------------------------------------------------


-- Full - mutual_quotation_request_history
	
	-- Why Physical deletes on mutual_quotation 
select count(*)
from Landing.mag.mutual_quotation_request_history

select count(*)
from Landing.mag.mutual_quotation_request_history_aud

	select history_id, 
		mutual_customer_id, mutual_quotation_id, status_id, 
		error_id, error_message, 
		created_at
	from Landing.mag.mutual_quotation_request_history_aud
	-- where mutual_customer_id = 1551
	-- where error_id = 0
	order by mutual_customer_id, history_id

		select mutual_customer_id, count(*)
		from Landing.mag.mutual_quotation_request_history_aud
		group by mutual_customer_id
		order by count(*) desc, mutual_customer_id

		select mutual_quotation_id, count(*)
		from Landing.mag.mutual_quotation_request_history_aud
		group by mutual_quotation_id
		order by count(*) desc, mutual_quotation_id

		select status_id, count(*)
		from Landing.mag.mutual_quotation_request_history_aud
		group by status_id
		order by status_id

		select error_id, error_message, count(*)
		from Landing.mag.mutual_quotation_request_history_aud
		group by error_id, error_message
		order by error_id, error_message

		select mq.quotation_id, mq.mutual_customer_id, mq.error_id, mq.created_at, mqrh.min_created_at
		from
				(select quotation_id, mutual_customer_id, error_id, created_at
				from Landing.mag.mutual_quotation_aud mq
				where mq.error_id <> 0) mq
			inner join
				(select mutual_customer_id, error_id, convert(date, min(created_at)) min_created_at
				from Landing.mag.mutual_quotation_request_history_aud
				where error_id <> 0 
				group by mutual_customer_id, error_id) mqrh on mq.mutual_customer_id = mqrh.mutual_customer_id and mq.error_id = mqrh.error_id and mq.created_at >= mqrh.min_created_at
		order by mq.mutual_customer_id, mq.created_at


		select 
			case when (mq.mutual_customer_id is null) then mqrh.mutual_customer_id else mq.mutual_customer_id end mutual_customer_id, 
			case when (mq.error_id is null) then mqrh.error_id else mq.error_id end error_id, 
			mq.num1, mqrh.num2
		from
				(select mutual_customer_id, error_id, count(*) num1
				from Landing.mag.mutual_quotation_aud mq
				where error_id <> 0
				group by mutual_customer_id, error_id) mq
			full join
				(select mutual_customer_id, error_id, count(*) num2
				from Landing.mag.mutual_quotation_request_history_aud
				where error_id <> 0 
				group by mutual_customer_id, error_id) mqrh on mq.mutual_customer_id = mqrh.mutual_customer_id and mq.error_id = mqrh.error_id
		where mq.num1 <> mqrh.num2
		-- where mq.num1 is null or mqrh.num2 is null
		order by mutual_customer_id, error_id

-------------------------------------------------
-------------------------------------------------
--------------------------------------------------
--- drop table #mutuelle_orders

select entity_id, increment_id, created_at, status, base_grand_total, mutual_amount, mutual_quotation_id
into #mutuelle_orders
from Landing.mag.sales_flat_order_aud
where mutual_quotation_id is not null

	select *
	from #mutuelle_orders
	order by created_at, mutual_amount


	-- What about orders done with quotation that is already deleted
	select o.*, mq2.*
	from 
			#mutuelle_orders o
		left join
			Landing.mag.mutual_quotation mq2 on o.mutual_quotation_id = mq2.quotation_id
	where mq2.quotation_id is null
	order by o.entity_id

	-- Why quotation used in many orders
	select *, count(*) over (partition by mutual_quotation_id) num_rep
	from #mutuelle_orders
	order by num_rep desc, entity_id

--------------------------------------------------

select mq.quotation_id, 
	mq.mutual_id, m.code, m.name, m.is_online is_online_mutual, 
	mq.mutual_customer_id, 
	mq.type_id, mq.is_online, mq.status_id, mq.error_id, mq.reference_number, 
	mq.amount, 
	mq.created_at, mq.expired_at, mq.updated_at, 
	mo.entity_id, mo.status, mo.mutual_amount
from 
		Landing.mag.mutual_quotation_aud mq
	inner join
		Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
	left join
		#mutuelle_orders mo on mq.quotation_id = mo.mutual_quotation_id
-- where mo.entity_id is not null and mq.type_id = 0 
-- where mo.entity_id is null
order by mq.created_at

select mq.type_id, mq.error_id, 
	case when (mo.entity_id is not null) then 1 else 0 end mag_order, count(*)
from 
		Landing.mag.mutual_quotation_aud mq
	inner join
		Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
	left join
		#mutuelle_orders mo on mq.quotation_id = mo.mutual_quotation_id

group by mq.type_id, mq.error_id, 
	case when (mo.entity_id is not null) then 1 else 0 end
order by mq.type_id, mq.error_id, mag_order

-- Filters: type_id not in (-29, 0) error_id = 0

-- What about type_id = 0 and mag_order = 1

-- What about error_id <> 0 and type_id = 1, 2

-- What about type_id = 2 and not magento order

---------------------------------------------------------

select mq.quotation_id quotation_id_bk, 
	mq.mutual_id mutual_id_bk, mq.type_id type_id_bk, mo.entity_id order_id_bk, 
	mq.amount mutual_amount, mo.mutual_amount,  mq.created_at quotation_date,
	mo.mutual_amount
from 
		Landing.mag.mutual_quotation_aud mq
	inner join
		Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
	left join
		(select mutual_quotation_id, entity_id, mutual_amount
		from
			(select mutual_quotation_id, entity_id, mutual_amount, 
				count(*) over (partition by mutual_quotation_id) num_rep, 
				rank() over (partition by mutual_quotation_id order by entity_id) max_rep
			from #mutuelle_orders) mo
		where num_rep = max_rep) mo on mq.quotation_id = mo.mutual_quotation_id
-- where mq.type_id not in (-29, 0) -- and mq.error_id = 0 
	-- and mo.mutual_quotation_id is not null
	-- and mq.amount <> mo.mutual_amount
-- where mo.entity_id = 7825797
where mq.type_id in (0)

---------------------------------------------------------

				select oh.entity_id order_id_bk, 
					oh.mutual_amount, oh.mutual_quotation_id, 
					mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount
				from 
						Landing.mag.sales_flat_order_aud oh
					inner join
						Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
					inner join 
						Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
				where mutual_amount is not null
					and type_id <> 2
				order by mutual_amount desc, amount desc

---------------------------------------

select *
from Warehouse.tableau.fact_mutual_quotation_v
where quotation_date between '2018-05-01' and '2018-06-01'
	and mutual_quotation_type_name = 'QUOTATION'
	and order_id_bk is null
	-- and mutual_amount > local_total_inc_vat
order by quotation_date

--------------------------------------

	select type_id type_id_bk, 
		case
			when (type_id = -2) then 'PENDING PEC'
			when (type_id = -1) then 'PENDING QUOTATION'
			when (type_id = 1) then 'QUOTATION'
			when (type_id = 2) then 'PEC'
			when (type_id = 0) then 'DEFAULT'
			else '??'
		end mutual_quotation_type_name
	from
		(select distinct type_id type_id
		from Landing.mag.mutual_quotation_aud) mq
	where type_id not in (-29, 0)

	select entity_id, increment_id, created_at, status, base_grand_total, mutual_amount, mutual_quotation_id
	into #mutuelle_orders
	from Landing.mag.sales_flat_order_aud
	where mutual_quotation_id is not null

	select mq.quotation_id quotation_id_bk, 
		mq.mutual_id mutual_id_bk, mq.type_id type_id_bk, mo.entity_id order_id_bk, 
		mq.amount mutual_amount, mq.created_at quotation_date, 
		case when (mq.error_id = 0) then null else mq.error_id end error_id
	from 
			Landing.mag.mutual_quotation_aud mq
		inner join
			Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
		left join
			(select mutual_quotation_id, entity_id, mutual_amount
			from
				(select mutual_quotation_id, entity_id, mutual_amount, 
					count(*) over (partition by mutual_quotation_id) num_rep, 
					rank() over (partition by mutual_quotation_id order by entity_id) max_rep
				from #mutuelle_orders) mo
			where num_rep = max_rep) mo on mq.quotation_id = mo.mutual_quotation_id
	where mq.type_id not in (-29, 0) -- and mq.error_id = 0 