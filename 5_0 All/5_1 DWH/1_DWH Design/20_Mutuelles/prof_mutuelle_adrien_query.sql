

SELECT mc.mutual_customer_id, 
  MONTH(mqrh.created_at) AS "Sort by Month", 
  MONTHNAME(mqrh.created_at) AS "Month", m.name AS "Mutuelle", COUNT(mqrh.history_id) AS "Nb of Quotations",
  mqrh.error_message AS "Error Message"

FROM 
    mutual_quotation_request_history mqrh
  JOIN 
    mutual_customer mc on mc.mutual_customer_id = mqrh.mutual_customer_id
  JOIN 
    mutual m ON m.mutual_id = mc.mutual_id
  JOIN 
    mutual_api ma ON ma.api_id = m.api_id
WHERE mqrh.created_at > "2018-01-01 00:00:00"
  AND mc.mutual_customer_id = 1551
GROUP BY m.mutual_id, mqrh.error_message, MONTH(mqrh.created_at)
ORDER BY 1, 4 ASC