

-- dw_countries

select country_code, country_name, 
	country_type, country_zone, country_continent, country_state
from openquery(MAGENTO, 'select * from dw_flat.dw_countries')
order by country_zone, country_type, country_continent, country_state, country_code

select country_type, count(*)
from openquery(MAGENTO, 'select * from dw_flat.dw_countries')
group by country_type
order by country_type

select country_zone, count(*)
from openquery(MAGENTO, 'select * from dw_flat.dw_countries')
group by country_zone
order by country_zone

select country_continent, count(*)
from openquery(MAGENTO, 'select * from dw_flat.dw_countries')
group by country_continent
order by country_continent

select country_state, count(*)
from openquery(MAGENTO, 'select * from dw_flat.dw_countries')
group by country_state
order by country_state

----------------------------------------------------------------

select top 1000 entity_id, city, postcode, region_id, region, country_id
from Landing.mag.customer_address_entity_flat_aud
where region_id = 12

	select country_id, count(*)
	from Landing.mag.customer_address_entity_flat_aud
	group by country_id
	order by country_id

		select adr.country_id, c.country_code, c.country_name, c.country_type, c.country_zone, c.country_continent, c.country_state, adr.num
		from
				(select country_id, count(*) num
				from Landing.mag.customer_address_entity_flat_aud
				group by country_id) adr
			left join
				(select country_code, country_name, 
					country_type, country_zone, country_continent, country_state
				from openquery(MAGENTO, 'select * from dw_flat.dw_countries')) c on adr.country_id = c.country_code
		-- where c.country_code is null
		-- order by adr.country_id
		order by adr.num desc

	-- OK FOR US (1 - 65), CA (66 - 78), DE (79 - 94, 486), AT (96 -103), CH (104 - 128), ES (131 - 181), FR (183 - 277), RO (292 - 311)
		-- FI (320 - 337), LV (359 -479),  IT (488 - 647), 
		-- CN (648 - 674, 696, 699, 819, 820, 896, 986, 1455, 1709, 1711), JP (694, 695, 702, 751, 1461, 2047), MX (774, 838, 1746)
	select country_id, region_id, count(*) num, count(*) over (partition by region_id) num_rep
	from Landing.mag.customer_address_entity_flat_aud
	group by country_id, region_id
	order by num_rep desc, region_id, country_id

	-- OK FOR ES, IT*, US, FR, CA
	select region_id, region, count(*) num
	from Landing.mag.customer_address_entity_flat_aud
	where country_id = 'BE' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by region_id, region
	order by region_id, region

	select city, count(*) num
	from Landing.mag.customer_address_entity_flat_aud
	where country_id = 'US' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by city
	order by city

	select postcode, count(*) num
	from Landing.mag.customer_address_entity_flat_aud
	where country_id = 'GB' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by postcode
	order by postcode
----------------------------------------------------------------

select top 1000 entity_id, city, postcode, region_id, region, country_id
from Landing.mag.sales_flat_order_address_aud

	select country_id, count(*)
	from Landing.mag.sales_flat_order_address_aud
	group by country_id
	order by country_id

		select adr.country_id, c.country_code, c.country_name, c.country_type, c.country_zone, c.country_continent, c.country_state, adr.num
		from
				(select country_id, count(*) num
				from Landing.mag.sales_flat_order_address_aud
				group by country_id) adr
			left join
				(select country_code, country_name, 
					country_type, country_zone, country_continent, country_state
				from openquery(MAGENTO, 'select * from dw_flat.dw_countries')) c on adr.country_id = c.country_code
		-- where c.country_code is null
		-- order by adr.country_id
		order by adr.num desc

	-- OK FOR US (1 - 65), CA (66 - 78), DE (79 - 94, 486), AT (96 -103), CH (104 - 128), ES (131 - 181), FR (183 - 277), RO (292 - 311)
		-- FI (320 - 337), LV (359 -479),  IT (488 - 647), 
		-- CN (648 - 674, 696, 699, 819, 820, 896, 986, 1455, 1709, 1711), JP (694, 695, 702, 751, 1461, 2047), MX (774, 838, 1746)
	select country_id, region_id, count(*) num, count(*) over (partition by region_id) num_rep
	from Landing.mag.sales_flat_order_address_aud
	group by country_id, region_id
	order by num_rep desc, region_id, country_id

	-- OK FOR ES, IT*, US, FR, CA
	select region_id, region, count(*) num
	from Landing.mag.sales_flat_order_address_aud
	where country_id = 'US' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by region_id, region
	order by region_id, region

	select city, count(*) num
	from Landing.mag.sales_flat_order_address_aud
	where country_id = 'US' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by city
	order by city

	select postcode, count(*) num
	from Landing.mag.sales_flat_order_address_aud
	where country_id = 'GB' -- GB - NL - IE - ES - IT- US - BE - FR - CA // DE, CH, CN
	group by postcode
	order by postcode


----------------------------------------------------------------

	select country_id, region_id, region, count(*) num, 
		count(*) over (partition by region_id) num_rep, 
		max(count(*)) over (partition by region_id) max_rep
	from Landing.mag.sales_flat_order_address_aud
	where (region_id is not null or region_id <> 0)	
		-- and region is not null
		and country_id in ('US', 'CA', 'DE', 'AT', 'CH', 'ES', 'FR', 'RO', 'FI', 'LV', 'IT')
	group by country_id, region_id, region
	order by num_rep desc, region_id, region, country_id

	select *
	from
		(select country_id, region_id, region, count(*) num, 
			count(*) over (partition by region_id) num_rep, 
			max(count(*)) over (partition by region_id) max_rep
		from Landing.mag.sales_flat_order_address_aud
		where (region_id is not null or region_id <> 0)
			-- and region is not null
			and country_id in ('US', 'CA', 'DE', 'AT', 'CH', 'ES', 'FR', 'RO', 'FI', 'LV', 'IT')		
		group by country_id, region_id, region) t
	where num = max_rep
	order by region_id, region, country_id

	-- 

