
-- DW_GetLenses
select country_code, country_name, 
	country_type, country_zone, country_continent, country_state, 
	len(country_code) country_code_len
from DW_GetLenses.dbo.countries
-- order by country_zone, country_type, country_continent, country_state, country_code
order by country_code_len desc

-- DW_Sales_Actual
select country_code, country_name, 
	country_type, country_zone, country_continent, country_state
from DW_Sales_Actual.dbo.dim_countries
order by country_zone, country_type, country_continent, country_state, country_code

-------------------------------------------------------------------------------------

-- DW_GetLenses
select top 1000 address_id, 
	street, city, postcode, region_id, region, country_id
from DW_GetLenses.dbo.customer_addresses

-- DW_Sales_Actual: No Table

-------------------------------------------------------------------------------------

-- DW_GetLenses
select top 1000 order_id, 
	billing_street1, billing_street2, billing_city, billing_postcode, billing_region_id, billing_region, billing_country_id
from DW_GetLenses.dbo.order_headers

select top 1000 order_id, 
	shiping_street1, shipping_street2, shipping_city, shipping_postcode, shipping_region_id, shipping_region, shipping_country_id
from DW_GetLenses.dbo.order_headers
where shipping_country_id = 'FR'

-- DW_Sales_Actual
select top 1000 uniqueid, order_no,
	billing_address, billing_postcode, billing_country_id, country_name
from DW_Sales_Actual.dbo.dim_order_headers

select top 1000 uniqueid, order_no,
	shipping_address, shipping_postcode, shipping_country_id, [Delivery Country]
from DW_Sales_Actual.dbo.dim_order_headers


-------------------------------------------------------------------------------------

select shipping_postcode, count(*) num 
from DW_GetLenses.dbo.order_headers
where shipping_country_id = 'GB'
	and document_date > getutcdate() - 30
group by shipping_postcode
-- order by num desc, shipping_postcode
order by shipping_postcode

