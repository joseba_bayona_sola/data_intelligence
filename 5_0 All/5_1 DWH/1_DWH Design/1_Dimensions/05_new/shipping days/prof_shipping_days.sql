
select top 1000 order_id_bk, order_date, invoice_date, shipment_date, 
	datediff(dd, order_date, shipment_date)
from Warehouse.sales.dim_order_header_v
where shipment_date is not null
	and year(order_date) = 2017 -- and month(order_date) = 12
	and datediff(dd, order_date, shipment_date) = 4
order by order_date

select datediff(dd, order_date, shipment_date), count(*), sum(count(*)) over (),
	count(*) * 100 / convert(decimal(12, 4), sum(count(*)) over ())
from Warehouse.sales.dim_order_header_v
where shipment_date is not null
	and year(order_date) = 2017 -- and month(order_date) = 11
group by datediff(dd, order_date, shipment_date)
order by datediff(dd, order_date, shipment_date)

-- Ranges: 0 - 1 - 2 - 3 - 4 - 5 - 7 - 10 - 20 - ++ 


select datediff(dd, order_date, shipment_date), count(*), sum(count(*)) over (),
	count(*) * 100 / convert(decimal(12, 4), sum(count(*)) over ())
from Warehouse.sales.dim_order_header_v
where -- shipment_date is not null and 
	year(order_date) = 2017 -- and month(order_date) = 11
group by datediff(dd, order_date, shipment_date)
order by datediff(dd, order_date, shipment_date)

select datediff(dd, order_date, shipment_date_r), count(*), sum(count(*)) over (),
	count(*) * 100 / convert(decimal(12, 4), sum(count(*)) over ())
from Warehouse.sales.fact_order_line_v
where -- shipment_date is not null and 
	year(order_date) = 2017 -- and month(order_date) = 11
group by datediff(dd, order_date, shipment_date_r)
order by datediff(dd, order_date, shipment_date_r)

-------------------------------------------------------------------

select top 1000 order_id_bk, order_date, invoice_date, shipment_date, payment_method_name,
	datediff(dd, order_date, invoice_date)
from Warehouse.sales.dim_order_header_v
where invoice_date is not null
	and year(order_date) = 2017 -- and month(order_date) = 12
	and datediff(dd, order_date, invoice_date) = 3
order by order_date

select datediff(dd, order_date, invoice_date), count(*), sum(count(*)) over (),
	count(*) * 100 / convert(decimal(12, 4), sum(count(*)) over ())
from Warehouse.sales.dim_order_header_v
where invoice_date is not null
	and year(order_date) = 2017 -- and month(order_date) = 11
group by datediff(dd, order_date, invoice_date)
order by datediff(dd, order_date, invoice_date)
