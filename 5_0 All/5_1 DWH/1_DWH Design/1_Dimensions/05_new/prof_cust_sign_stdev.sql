
select top 1000 *
from Warehouse.tableau.fact_activity_sales_v
where activity_type_name = 'LAPSED' and num_tot_orders = 0

select top 1000 customer_id, customer_email, num_tot_orders, 
	main_order_qty_time, avg_order_qty_time, stdev_order_qty_time,
	avg_order_freq_time, stdev_order_freq_time
from Warehouse.act.fact_customer_signature_v
where num_tot_orders not in (0, 1, 2) and avg_order_freq_time <> 0
-- order by num_tot_orders desc
order by stdev_order_freq_time 

select customer_id, customer_email, num_tot_orders, 
	main_order_qty_time, avg_order_qty_time, stdev_order_qty_time,
	avg_order_freq_time, stdev_order_freq_time
from Warehouse.act.fact_customer_signature_v
where num_tot_orders not in (0, 1, 2) and avg_order_freq_time <> 0 and stdev_order_freq_time < 2
order by num_tot_orders desc

select stdev_order_freq_time, count(*)
from Warehouse.act.fact_customer_signature_v
where num_tot_orders in (0, 1, 2)
group by stdev_order_freq_time
order by stdev_order_freq_time


select *
from Warehouse.act.fact_activity_sales_v
where customer_id = 1407311

select *
from Warehouse.sales.fact_order_line_v
where customer_id = 252060
