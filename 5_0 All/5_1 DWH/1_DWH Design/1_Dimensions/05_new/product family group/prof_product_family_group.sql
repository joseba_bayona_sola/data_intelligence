
-- AURA 1 DAY = EVERCLEAR ADM (D - ST)
-- AURA MONTHLY = EVERCLEAR UV (M - ST)
-- EVERCLEAR ELITE (D - SIHY)
-- EVERCLEAR MULTI PURPOSE SOLUTION
-- EVERCLEAR DROPS

select product_id_magento, 
	product_type_name, category_name, cl_feature_name, cl_type_name, product_family_name
from Warehouse.prod.dim_product_family_v
where product_id_magento in (2318, 2317, 2682, 2315, 2316, 3157)
order by product_type_name, category_name, cl_feature_name, cl_type_name, product_id_magento


select product_id_magento, 
	product_type_name, category_name, cl_feature_name, cl_type_name, product_family_name
from Warehouse.prod.dim_product_family_v
where product_type_name = 'Contact Lenses'
	and category_name <> 'CL - Yearly'
order by product_type_name, category_name, cl_feature_name, cl_type_name, product_id_magento

-- CL - Daily / Colour

-- CL - Yearly
-- Glasses
-- Other
-- Solutions & Eye Care
-- Sunglasses