
select shipping_country_id, count(*)
from DW_GetLenses.dbo.invoice_headers
where document_date between '2016-12-01' and '2016-12-31'
group by shipping_country_id
order by count(*) desc, shipping_country_id

select *
from DW_GetLenses.dbo.invoice_headers
where shipping_country_id = 'FR' and document_date between '2016-12-01' and '2016-12-31'

select shipping_region_id, shipping_region, shipping_postcode, count(*)
from DW_GetLenses.dbo.invoice_headers
where shipping_country_id = 'FR' and document_date between '2016-12-01' and '2016-12-31'
group by shipping_region_id, shipping_region, shipping_postcode
order by shipping_region_id, shipping_region, shipping_postcode

select shipping_region_id, shipping_region, count(*)
from DW_GetLenses.dbo.invoice_headers
where shipping_country_id = 'GB' and document_date between '2016-12-01' and '2016-12-31'
group by shipping_region_id, shipping_region
order by count(*) desc, shipping_region_id, shipping_region

