
select *
from Landing.mag.customer_entity_flat c

select c.entity_id, 
	o.entity_id order_id, o.created_at, o.reminder_type, o.reminder_period, o.reminder_date
from 
		Landing.mag.customer_entity_flat c
	left join 
		(select *
		from Landing.mag.sales_flat_order_aud) o on c.entity_id = o.customer_id
where status not in ('cancel', 'close')
order by c.entity_id, o.created_at, o.entity_id

select c.entity_id, 
	o.entity_id order_id, o.created_at, o.reminder_type, o.reminder_period, o.reminder_date, 
	count(*) over (partition by c.entity_id) num_rows_cust, 
	rank() over (partition by c.entity_id order by o.created_at, o.entity_id) rank_rows_cust
from 
		Landing.mag.customer_entity_flat c
	left join 
		(select *
		from Landing.mag.sales_flat_order_aud 
		where status not in ('cancel', 'close')) o on c.entity_id = o.customer_id
order by c.entity_id, o.created_at, o.entity_id


select entity_id, 
	case 
		when (reminder_type in ('both', 'email', 'sms', 'reorder')) then 'Y'
		else 'N'
	end reminder_f,
	reminder_type reminder_type_name_bk, reminder_period reminder_period_bk
from
	(select c.entity_id, 
		o.entity_id order_id, o.created_at, o.reminder_type, o.reminder_period, o.reminder_date, 
		count(*) over (partition by c.entity_id) num_rows_cust, 
		rank() over (partition by c.entity_id order by o.created_at, o.entity_id) rank_rows_cust
	from 
			Landing.mag.customer_entity_flat c
		left join 
			(select *
			from Landing.mag.sales_flat_order_aud 
			where status not in ('cancel', 'close')) o on c.entity_id = o.customer_id) t
where num_rows_cust = rank_rows_cust
order by entity_id


-----------------------------------------------------------------------------

select c.entity_id, 
	case when (rp.customer_id is not null) then 'Y' else 'N' end reorder_f
from 
		Landing.mag.customer_entity_flat c
	left join
		(select customer_id, count(*) num
		from Landing.mag.po_reorder_profile_aud
		where enddate > startdate and enddate > getutcdate()
		group by customer_id) rp on c.entity_id = rp.customer_id


-----------------------------------------------------------------------------

	select *
	from Landing.aux.gen_customer_unsubscribe_magento

	select unsubscribe_mark_email_magento_f, count(*)
	from Landing.aux.gen_customer_unsubscribe_magento
	group by unsubscribe_mark_email_magento_f
	order by unsubscribe_mark_email_magento_f

	select *
	from Landing.aux.gen_customer_reminder

	select reminder_f, reminder_type_name_bk, count(*)
	from Landing.aux.gen_customer_reminder
	group by reminder_f, reminder_type_name_bk
	order by reminder_f, reminder_type_name_bk

	select *
	from Landing.aux.gen_customer_reorder

	select reorder_f, count(*)
	from Landing.aux.gen_customer_reorder
	group by reorder_f
	order by reorder_f

