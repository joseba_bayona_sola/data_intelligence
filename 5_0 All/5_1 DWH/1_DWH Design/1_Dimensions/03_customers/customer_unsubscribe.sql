
select top 1000 *
from Landing.mag.dotmailer_suppressed_contact

-------------------------------------------

select top 1000 *
from Staging.gen.dim_customer

select top 1000 idCustomerSCD_sk, idCustomer_sk_fk, customer_id_bk, 
	idCustomerUnsubscribe_sk_fk, customer_unsubscribe_d,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk
select top 1000 *
from Warehouse.gen.dim_customer_scd

select top 1000 idCustomerSCD_sk, idCustomer_sk_fk, customer_id_bk, 
	customer_unsubscribe_name, customer_unsubscribe_date, 
	customer_unsubscribe_dotmailer_name, customer_unsubscribe_dotmailer_date
from Warehouse.gen.dim_customer_scd_v

-- logic for unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk + 
-- logic for idCustomerUnsubscribe_sk_fk

-- reload of data

-------------------------------------------

select top 1000 *
from Warehouse.act.fact_customer_signature

select top 1000 idCustomer_sk_fk, customer_id, 
	customer_unsubscribe_name, customer_unsubscribe_date
from Warehouse.act.fact_customer_signature_v

select top 1000 *
from Warehouse.act.dim_customer_signature_v

-- new attributes + logic for Warehouse.act.dim_customer_signature_v

-- new attributes for fact_customer_signature_v

-------------------------------------------

select top 1000 *
from Warehouse.sales.dim_order_header

select top 1000 *
from Warehouse.sales.dim_order_header_v

-- new attributes on dim_order_header_v based on act.dim_customer_signature_v


-------------------------------------------

select top 1000 *
from Warehouse.stat.fact_magento_unsubscribe

-------------------------------------------
-------------------------------------------
-------------------------------------------

select order_id_bk, order_no, order_date, customer_id_bk, referafriend_code, referafriend_referer, coupon_id_bk, coupon_code
from Landing.aux.sales_dim_order_header
where marketing_channel_name_bk like 'Refer a%'
order by referafriend_referer