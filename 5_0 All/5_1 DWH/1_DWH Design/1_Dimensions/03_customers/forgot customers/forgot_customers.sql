
select entity_id, email
into #forgot_customers
from Landing.mag.customer_entity_aud
-- where email in ('visiondirect@rjmcdowall.com', 'vanbui@gmx.net', 'kam.binning@hotmail.co.uk', 'karimkassam5492@googlemail.com', 'racylady01@yahoo.co.uk')
where email in ('emer.kellyspedding@gmail.com', 'sdaleymum@gmail.com')
-- where email in ('m.stinis@kpnplanet.nl', 'christineshouli@gmail.com')

select *
from #forgot_customers
order by entity_id

-------------------------------------------------------------

-- Customer

select top 1000 cev.*
from 
		Landing.mag.customer_entity_varchar_aud cev
	inner join
		#forgot_customers fg on cev.entity_id = fg.entity_id
where attribute_id in (5, 7)
order by entity_id, attribute_id

select cef.entity_id, cef.email, cef.firstname, cef.lastname, cef.default_billing, cef.default_shipping
from 
		Landing.mag.customer_entity_flat_aud cef
	inner join
		#forgot_customers fg on cef.entity_id = fg.entity_id
order by entity_id

select c.*
from 
		Landing.aux.gen_dim_customer_aud c
	inner join
		#forgot_customers fg on c.customer_id_bk = fg.entity_id

select c.*
from 
		Warehouse.gen.dim_customer_scd_v c
	inner join
		#forgot_customers fg on c.customer_id_bk = fg.entity_id

-------------------------------------------------------------

-- Customer Address

select cef.entity_id, cef.email, cef.firstname, cef.lastname, cef.default_billing, cef.default_shipping, 
	caef.*
from 
		Landing.mag.customer_entity_flat_aud cef
	inner join
		#forgot_customers fg on cef.entity_id = fg.entity_id
	inner join
		Landing.mag.customer_address_entity_varchar_aud caef on cef.default_billing = caef.entity_id
where caef.attribute_id in (18, 19, 24, 25)
order by cef.entity_id

select cef.entity_id, cef.email, cef.firstname, cef.lastname, cef.default_billing, cef.default_shipping, 
	caef.*
from 
		Landing.mag.customer_entity_flat_aud cef
	inner join
		#forgot_customers fg on cef.entity_id = fg.entity_id
	inner join
		Landing.mag.customer_address_entity_text_aud caef on cef.default_billing = caef.entity_id
order by cef.entity_id

select cef.entity_id, cef.email, cef.firstname, cef.lastname, cef.default_billing, cef.default_shipping, 
	caef.*
from 
		Landing.mag.customer_entity_flat_aud cef
	inner join
		#forgot_customers fg on cef.entity_id = fg.entity_id
	inner join
		Landing.mag.customer_address_entity_flat_aud caef on cef.default_billing = caef.entity_id
order by cef.entity_id

-------------------------------------------------------------

-- Orders

select entity_id
into #forgot_customers_oh
from
	(select oh.entity_id
	from 
			Landing.mag.sales_flat_order_aud oh 
		inner join
			#forgot_customers fg on oh.customer_id = fg.entity_id
	union 
	select oh.entity_id
	from 
			Landing.mag.sales_flat_order_aud_hist oh 
		inner join
			#forgot_customers fg on oh.customer_id = fg.entity_id) t

select oh.entity_id, oh.increment_id, oh.created_at, oh.updated_at,
	oh.customer_id, oh.customer_email, oh.customer_firstname, oh.customer_lastname, 
	oh.billing_address_id, oh.shipping_address_id
from 
		Landing.mag.sales_flat_order_aud oh
	inner join
		#forgot_customers_oh oh2 on oh.entity_id = oh2.entity_id
order by oh.customer_email, oh.created_at

select oh.*
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		#forgot_customers_oh oh2 on oh.order_id_bk = oh2.entity_id
order by oh.customer_id_bk

---------------------------------------------------

-- Orders Address

select oh.entity_id, oh.customer_email, oh.created_at, 
	oa.firstname, oa.lastname, oa.street, oa.city, oa.postcode
from 
		Landing.mag.sales_flat_order_aud oh
	inner join
		#forgot_customers_oh oh2 on oh.entity_id = oh2.entity_id
	inner join
		Landing.mag.sales_flat_order_address_aud oa on oh.billing_address_id = oa.entity_id
order by oh.customer_email, oh.created_at

select oh.entity_id, oh.customer_email, oh.created_at, 
	oa.firstname, oa.lastname, oa.street, oa.city, oa.postcode
from 
		Landing.mag.sales_flat_order_aud oh
	inner join
		#forgot_customers_oh oh2 on oh.entity_id = oh2.entity_id
	inner join
		Landing.mag.sales_flat_order_address_aud oa on oh.shipping_address_id = oa.entity_id
order by oh.customer_email, oh.created_at

