
select top 10000 *
from Landing.mag.customer_entity_flat_aud_v
--where entity_id in (646260, 17578, 1864406, 105275)
-- order by entity_id, ins_ts
order by entity_id desc

select top 1000 entity_id, count(*) num_rep,
	count(distinct email) num_dist_email, count(distinct store_id) num_dist_store
from Landing.mag.customer_entity_flat_aud_v
group by entity_id
having count(distinct email) > 1 or count(distinct store_id) > 1
-- order by count(distinct email) desc
order by count(distinct store_id) desc

------------------------------------------------------------

select top 1000 prefix, count(*)
from Landing.mag.customer_entity_flat_aud
group by prefix
order by count(*) desc

select top 1000 middlename, count(*)
from Landing.mag.customer_entity_flat_aud
group by middlename
order by count(*) desc

select top 1000 suffix, count(*)
from Landing.mag.customer_entity_flat_aud
group by suffix
order by count(*) desc


------------------------------------------------------------

select *
from Landing.mag.customer_entity_flat_aud
where email is null

select *
from Landing.mag.customer_entity_flat_aud
where store_id is null

select *
from Landing.mag.customer_entity_flat_aud
where dob is not null

select *
from Landing.mag.customer_entity_flat_aud
where gender is not null

------------------------------------------------------------

	-- Is No-Yes Current Values and 0-1 Old Values
		-- 0 records: Customers created till 2012-10
		-- 1 records: Customers updated until 2014-08
		-- NULL values ??? Customers created till 2014-12 --> Unsub to NO
	-- What are really the people that say that don't want to receive Mark Emails? YES - 1?
	-- Unsubscribe Date
		-- What is the meaning on No - 0: When the row was created // unsubscribe flag changed value
		-- NULL values for No until 2013-12
		-- Some records with yes: Could be Imported Customer. Could put current date as uns_date

	-- LOGIC: Say Unsubscribe = Y when Yes / 1
		-- Only put unsubscribe_date when = Y

select top 1000 unsubscribe_all, count(*)
from Landing.mag.customer_entity_flat_aud
group by unsubscribe_all
order by count(*) desc

select top 1000 entity_id, email, unsubscribe_all, unsubscribe_all_date, created_at, updated_at
from Landing.mag.customer_entity_flat_aud
where unsubscribe_all = 'No' -- Yes - No 
-- where unsubscribe_all = '1' -- 0 - 1 -- 2012
--where unsubscribe_all not in ('Yes', 'No', '0', '1') -- 2014
order by entity_id desc

select top 1000 entity_id, email, unsubscribe_all, unsubscribe_all_date, created_at, updated_at, 
	count(*) over (partition by unsubscribe_all) num_uns_type
from Landing.mag.customer_entity_flat_aud
where unsubscribe_all in ('Yes', 'No')
	and unsubscribe_all_date is null
order by entity_id desc


select top 1000 record_type, 
	entity_id, email, unsubscribe_all, unsubscribe_all_date, created_at, updated_at, 
	ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
from Landing.mag.customer_entity_flat_aud_v
where entity_id in (77530, 224316, 564791)
order by entity_id, ins_ts

select top 1000 entity_id, count(*) num_rep,
	count(distinct unsubscribe_all) num_unsubscribe_all
from Landing.mag.customer_entity_flat_aud_v
group by entity_id
having count(distinct unsubscribe_all) > 1
-- order by count(distinct email) desc
order by count(distinct unsubscribe_all) desc

------------------------------------------------------------

	select *, UPPER(LEFT(value, 2))
	from Landing.mag.core_config_data 
	where path = 'design/footer/copyright'
	order by scope_id

	select *
	from openquery(MAGENTO, 'select * from dw_flat.dw_core_config_data ') 
	where path = 'general/locale/code' 
	order by store_id

------------------------------------------------------------

select top 1000 days_worn_info, count(*)
from Landing.mag.customer_entity_flat_aud
group by days_worn_info
order by count(*) desc
	
		-- magento01`.`eav_attribute_option_value` `days_worn` on(((`days_worn`.`option_id` = `c`.`days_worn_info`) and (`days_worn`.`store_id` = 0)))) 

------------------------------------------------------------

	-- What if the Order is cancelled / refunded: Do I take it or no --> In Magento don't send Reminder if cancel/refund (Based on status: cancel/close)

	-- How do we take the last order for the Customer (Need to query aud order table)
	-- Once taken the last order take reminder_type, reminder_period from it
		-- Customer Base + Join with Order Aud + Filter Cancel - Refund ?? + Count - Rank and Take Last One

select top 1000 entity_id, increment_id, created_at,
	reminder_type, reminder_period, reminder_date, 
	reminder_mobile, reminder_presc, 
	reminder_sent, reminder_follow_sent
from Landing.mag.sales_flat_order_aud
order by entity_id desc

select top 1000 reminder_type, count(*)
from Landing.mag.sales_flat_order_aud
group by reminder_type
order by reminder_type

	select top 1000 entity_id, increment_id, created_at,
		reminder_type, reminder_period, reminder_date, 
		reminder_mobile, reminder_presc, 
		reminder_sent, reminder_follow_sent
	from Landing.mag.sales_flat_order_aud
	where reminder_type not in ('both', 'email', 'sms', 'none', 'reorder') or reminder_type is null
	order by entity_id desc

select top 1000 reminder_period, count(*)
from Landing.mag.sales_flat_order_aud
group by reminder_period
order by count(*) desc

	select top 1000 entity_id, increment_id, created_at,
		reminder_type, reminder_period, reminder_date, 
		reminder_mobile, reminder_presc, 
		reminder_sent, reminder_follow_sent
	from Landing.mag.sales_flat_order_aud
	-- where reminder_period is null
	where reminder_period not in (30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360)
	order by entity_id desc

select top 1000 reminder_presc, count(*)
from Landing.mag.sales_flat_order_aud
group by reminder_presc
order by reminder_presc

	select top 1000 entity_id, increment_id, created_at,
		reminder_type, reminder_period, reminder_date, 
		reminder_mobile, reminder_presc, 
		reminder_sent, reminder_follow_sent
	from Landing.mag.sales_flat_order_aud
	where reminder_presc is not null
	order by entity_id desc

select top 1000 reminder_sent, reminder_follow_sent, count(*)
from Landing.mag.sales_flat_order_aud
group by reminder_sent, reminder_follow_sent
order by reminder_sent, reminder_follow_sent

	select top 1000 entity_id, increment_id, created_at,
		reminder_type, reminder_period, reminder_date, 
		reminder_mobile, reminder_presc, 
		reminder_sent, reminder_follow_sent
	from Landing.mag.sales_flat_order_aud
	where reminder_sent = 1
	order by reminder_date desc
	order by entity_id desc

------------------------------------------------------------

	-- What if the Order is cancelled / refunded --> In Magento don't send Reminder if cancel/refund

	-- How do we take the last order for the Customer (Need to query aud order table)
	-- Once taken the last order join with reorder profile and set reorder flag
		-- Customer Base + Join with Order Aud + Filter Cancel - Refund ?? + Count - Rank and Take Last One

	-- All Orders have a Reorder Profile ? (Glasses or Errors) 
	-- Is the profile repeated over diferent orders? (Reordering one -- Cancellations)
	-- Can a Profile be related with more than one customer
	-- For Reoder = Y next_order_date between startdate - enddate
	-- Why sometimes startdate > enddate and other times enddate < startdate

select top 1000 entity_id, increment_id, customer_id, created_at, 
	reorder_profile_id, automatic_reorder, reorder_on_flag
from Landing.mag.sales_flat_order_aud
where automatic_reorder = 1
order by entity_id desc

select top 1000 automatic_reorder, count(*)
from Landing.mag.sales_flat_order_aud
group by automatic_reorder
order by automatic_reorder

select top 1000 reorder_on_flag, count(*)
from Landing.mag.sales_flat_order_aud
group by reorder_on_flag
order by reorder_on_flag


	select top 1000 o.entity_id, o.increment_id, o.customer_id, o.created_at, 
		o.reorder_profile_id, o.automatic_reorder, o.reorder_on_flag, 
		po.id, po.created_at, po.startdate, po.enddate, po.interval_days, po.next_order_date, 
		po.reorder_quote_id
	from 
			Landing.mag.sales_flat_order_aud o
		left join
			Landing.mag.po_reorder_profile_aud po on o.reorder_profile_id = po.id
	where automatic_reorder = 1
	order by o.entity_id desc


select top 1000 id, created_at
	startdate, enddate, 
	interval_days, next_order_date, 
	reorder_quote_id
from Landing.mag.po_reorder_profile_aud
--where customer_id = 1404992
-- where enddate > startdate and enddate > getutcdate()
where next_order_date between startdate and enddate
order by id desc

--

select top 1000 *
from Landing.mag.sales_flat_order_aud
where reorder_profile_id is null
order by created_at desc


select top 1000 reorder_profile_id, count(*)
from Landing.mag.sales_flat_order_aud
group by reorder_profile_id
order by count(*) desc

select *
from Landing.mag.sales_flat_order_aud
where reorder_profile_id = 2928947
order by created_at desc


select id, count(*)
from Landing.mag.po_reorder_profile_aud
group by id
order by count(*) desc