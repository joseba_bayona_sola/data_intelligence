
-- ALL ATTRIBUTES
select top 50 
	customer_id, 
	email, password_hash, 
	prefix, firstname, middlename, lastname, suffix, 
	gender, dob, language, cus_phone, alternate_email, 
	card_expiry_date, default_billing, default_shipping, taxvat,  
	parent_customer_id, is_parent_customer, 

	business_channel, store_name, website_id, website_group, created_in, found_us_info,   
	days_worn, eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer, 
	unsubscribe_all, unsubscribe_all_date, 
	facebook_id, facebook_permissions, 
	referafriend_code, postoptics_send_post, 

	first_order_date, last_logged_in_date, last_order_date, num_of_orders, 

	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, 
	segment_sport, segment_professional, segment_lifestage, segment_vanity, 

	segment, segment_2, segment_3, segment_4, 

	old_access_cust_no, old_web_cust_no, old_customer_id, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 

	created_at, updated_at, 

	count(*) over () num_tot
from DW_GetLenses.dbo.customers;

-- IMPORTANT ATTRIBUTES
	select top 1000 
		customer_id, 
		email, 
		prefix, firstname, middlename, lastname, suffix, 
		gender, dob, language, cus_phone,  -- gender? dob?
	  
		store_name, website_id, website_group, 

		unsubscribe_all, unsubscribe_all_date, 
		days_worn,

		old_access_cust_no, old_web_cust_no, old_customer_id, 
	

		created_at, updated_at
	from DW_GetLenses.dbo.customers
	-- where suffix is not null and suffix <> '' -- middlename - suffix
	-- where old_access_cust_no is not null
	where gender is not null and gender <> ''
	-- where dob is not null
	order by customer_id desc;

	-- NOT IMPORTANT ATTRIBUTES
	select top 1000 
		customer_id,
		password_hash, alternate_email, 
		card_expiry_date, default_billing, default_shipping, taxvat,
		parent_customer_id, is_parent_customer, 
		days_worn, eyeplan_credit_limit, eyeplan_approved_flag, eyeplan_can_ref_new_customer, 
		facebook_id, facebook_permissions, 
		referafriend_code, postoptics_send_post, 
		segment_usage, segment_purch_behaviour, segment_eysight, 
		segment_sport, segment_professional, segment_lifestage, segment_vanity, 

		segment, segment_2, segment_3, segment_4, 
		emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5
	from DW_GetLenses.dbo.customers
	order by customer_id desc;

	-- SIGNATURE ATTRIBUTES
	select top 1000 
		customer_id,
		business_channel, created_in, found_us_info, 
		first_order_date, last_logged_in_date, last_order_date, num_of_orders, 
		segment_lifecycle, segment_geog
	from DW_GetLenses.dbo.customers
	-- where old_access_cust_no is not null
	where found_us_info is not null
	order by customer_id desc;
	 

-------------------------------------------------------------------------------------------------

	-- same attributes in DW_Sales_Actual

	select old_access_cust_no, count(*)
	from DW_GetLenses.dbo.customers
	group by old_access_cust_no
	order by count(*) desc

	select top 10000 customer_id, store_name, created_at, old_access_cust_no, old_web_cust_no, old_customer_id
	from DW_GetLenses.dbo.customers
	-- where old_access_cust_no in 
		-- ('GLIE', 'GLUK') -- old_customer_id sometimes
		-- ('getlensesie', 'eurgetlensesnl') -- old_customer_id
		-- ('VISIO') -- old_customer_id = VISIO + XXXX
		-- ('LENSWAYUK') -- -- old_customer_id = LENSWAYUK + XXXX 19930284143 (19.930.284.143)
		-- ('LBUK', 'LBIE', 'LBNLDESE', 'LBNLBEFR', 'LBES', 'LBNLBEFRBEL-NLB', 'LBNLBEFRBEL-FRA') -- old_customer_id = LBxxxx + XXXX
	where old_access_cust_no is null
	order by customer_id  desc

	select top 10000 customer_id, store_name, created_at, old_access_cust_no, old_web_cust_no, old_customer_id
	from DW_GetLenses.dbo.customers
	where customer_id in 
		(select distinct customer_id
		from  DW_Proforma.dbo.order_headers
		where store_name = 'visiondirect.co.uk') -- lensbase.co.uk - lenshome.es - visiooptik.fr - visiondirect.co.uk
	order by customer_id  desc

	select top 1000 count(*) over () num_tot,
		customer_id, store_name, created_at, old_access_cust_no, old_web_cust_no, old_customer_id, 
		CHARINDEX(old_access_cust_no, old_customer_id, 1) c, 
		SUBSTRING(old_customer_id, len(old_access_cust_no) + 1, len(old_customer_id) - len(old_access_cust_no)), 
		CHARINDEX('%[0-9]%', old_customer_id, 1) c2
	from DW_GetLenses.dbo.customers
	where old_customer_id is not null and old_customer_id <> 'manual_entry'
		and old_customer_id LIKE '%[^0-9]%'
		and CHARINDEX(old_access_cust_no, old_customer_id, 1) <> 1
	order by c desc

	select customer_id, store_name, created_at, old_access_cust_no, old_web_cust_no, old_customer_id, 
		cast(old_customer_id as int) old_customer_id_int
	from DW_GetLenses.dbo.customers
	where old_customer_id is not null
		and old_customer_id NOT LIKE '%[^0-9]%'
