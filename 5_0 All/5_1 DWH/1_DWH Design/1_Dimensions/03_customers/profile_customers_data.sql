
select customer_created_in, new_customer_created_in
from openquery(MAGENTO, 'select * from dw_flat.customer_created_in_map')
where customer_created_in <> new_customer_created_in 
order by customer_created_in, new_customer_created_in

select top 1000 customer_id, order_id
from openquery(MAGENTO, 'select * from dw_flat.customer_first_order')

select top 1000 order_id, order_no, 
	bis_channel, raf_channel, ga_channel, coupon_channel, channel
from openquery(MAGENTO, 'select * from dw_flat.dw_order_channel')

select top 1000 customer_id, 
	first_order_date, last_order_date, no_of_orders
from openquery(MAGENTO, 'select * from dw_flat.dw_order_headers_marketing_agg')

select top 1000 customer_id, 
	max_order_seq_plus, min_order_seq_minus, no_of_orders
from openquery(MAGENTO, 'select * from dw_flat.dw_customer_total_orders')

