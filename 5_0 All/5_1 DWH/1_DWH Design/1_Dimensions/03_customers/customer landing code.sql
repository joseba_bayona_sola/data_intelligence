
drop table #customer_data

create table #customer_data(
	customer_id_bk								int NOT NULL, 
	customer_email								varchar(255),
	created_at									datetime, 
	updated_at									datetime, 
	store_id_bk									int NOT NULL, 
	customer_type_name_bk						varchar(50) NOT NULL, 
	customer_status_name_bk						varchar(50),
	prefix										varchar(255),
	first_name									varchar(255),
	last_name									varchar(255),
	dob											datetime2,
	gender										char(1),
	language									char(2),
	phone_number								varchar(255),
	street										varchar(1000),
	city										varchar(255),
	postcode									varchar(255),
	region										varchar(255),
	region_id_bk								int, 
	country_id_bk								char(2), 
	postcode_s									varchar(255),
	country_id_s_bk								char(2), 
	referafriend_code							varchar(255),
	days_worn_info								varchar(255),
	migrate_customer_f							char(1) NOT NULL,
	migrate_customer_store						varchar(255),
	migrate_customer_id							bigint)

select *
from Landing.mag.customer_entity_flat c


	


select *
from #customer_data