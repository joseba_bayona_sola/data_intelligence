
select top 1000 customer_id, email, last_logged_in_date, last_order_date, segment_lifecycle, num_of_orders
from DW_GetLenses.dbo.customers

select top 1000 segment_lifecycle, count(*)
from DW_GetLenses.dbo.customers
group by segment_lifecycle
order by segment_lifecycle

select year(last_order_date) yyyy, month(last_order_date) mm, count(*)
from DW_GetLenses.dbo.customers
where segment_lifecycle = 'REACTIVATED' -- NEW - REGULAR - LAPSED - REACTIVATED - RNB - SURVEY
group by year(last_order_date), month(last_order_date) 
order by year(last_order_date), month(last_order_date) 

select num_of_orders, count(*)
from DW_GetLenses.dbo.customers
where segment_lifecycle = 'NEW' -- NEW - REGULAR - LAPSED - REACTIVATED - RNB - SURVEY
group by num_of_orders
order by num_of_orders