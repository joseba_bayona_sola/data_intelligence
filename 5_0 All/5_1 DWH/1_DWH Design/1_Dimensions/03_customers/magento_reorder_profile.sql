
-- 6959
select id, created_at, customer_id,
	startdate, enddate, 
	interval_days, next_order_date, 
	reorder_quote_id
from magento01.po_reorder_profile
where next_order_date between startdate and enddate
  -- and enddate < current_date()
order by id desc

select id, created_at, customer_id,
	startdate, enddate, 
	interval_days, next_order_date, 
	reorder_quote_id
from magento01.po_reorder_profile
where enddate > startdate and enddate > current_date()
  -- and not(next_order_date between startdate and enddate)
order by enddate, id desc


-- -------------------------------------------------------------

select id, created_at, customer_id,
	startdate, enddate, 
	interval_days, next_order_date, 
	reorder_quote_id
from magento01.po_reorder_profile
where customer_id =471939

select *
from magento01.sales_flat_order
where reorder_profile_id = 471939

select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
        reorder_on_flag, reorder_profile_id, automatic_reorder
from magento01.sales_flat_order
where customer_id = 471939
order by created_at

