
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Raul Arza Marcos
-- Create date: 2014-04-25
-- Description:	This stored procedure will retrieve customer information based on brandIdList
-- Change Log
--		  2014-08-28  rama	 Added IsNull for Customer GUID and customer create date and customerSportsbookRiskGeneralLimit (Cubes don't like NULLS at all, we should always have a default value.
--		  2014-09-22  rama	 Changed endDate to load all the Customers which have changed today
--		  2014-09-22  rama	 Changed zipCode
--		  2015-02-04  rama	 Changed ISNULL(tic.[customerGender],'U') AS [customerGender]
--		  2015-03-12  jora1	 Added sk_deviceType
--		  2015-05-13  rama1	 Changed LOWER(ISNULL(rtrim(ltrim(REPLACE(customerCity,CHAR(9),''))),'Unknown'))
--		  2015-07-16  rama1	 Changed upper limit for @endDate
--		  2015-10-06  crfi	 Add PhoneNumberVerified and EmailVerified in the Customer dimension 	
--		  2016-04-19  sasa	 Trim Country Code to exclude any trailing spaces
--		  2016-05-25  rumi   Ensure phone numbers are not truncated 
-- =============================================
-- exec [dbo].[tdwMarts_GetCustomerBasicInformation] @packageRunID=null,@fromDate='2015-05-01',@toDate='2015-08-01',@brandIdList='-1,1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25'
CREATE PROCEDURE [dbo].[tdwMarts_GetCustomerBasicInformation]
   @packageRunID Int = NULL
  ,@fromDate DateTime = NULL
  ,@toDate DateTime = NULL
  ,@brandIdList Varchar(255) = NULL
AS
BEGIN
   SET NOCOUNT ON;
	-- Log start
	--
   DECLARE @logID Int
	 ,@msg Varchar(512)
	 ,@procName Varchar(50) = OBJECT_NAME(@@procid);
   DECLARE @startDate DateTime
	 ,@endDate DateTime;

   EXEC dbo.logStart @spName = @procName, @packageRunID = @packageRunID, @outputID = @logID OUTPUT;
	 
   SET @startDate = @fromDate;
   SET @endDate = DATEADD(DAY, +1, @toDate); --In order to load today's changes

   EXEC dbo.logParameters @logID = @logID, @fromDate = @startDate, @toDate = @endDate, @idParameter = NULL, @brandIdList = @brandIdList;  

	-- Get the brand list
   DECLARE @sk_brandList Table
	  (
	   sk_brand Smallint PRIMARY KEY
	  );

   INSERT	INTO @sk_brandList
			(sk_brand)
			SELECT	 sk_brand
			FROM	 dbo.getskBrandList(@brandIdList); 

   SELECT	tic.sk_internalCustomer
		   ,tc.sk_customer
		   ,CONVERT(Int, CONVERT(Varchar(8), tic.effectiveDateBeginCET, 112)) AS effectiveDateBeginCET
		   ,ISNULL(CONVERT(Int, CONVERT(Varchar(8), tic.effectiveDateEndCET, 112)), 99991231) AS effectiveDateEndCET
		   ,tic.isCurrent
		   ,ISNULL(tc.customerGuid, '00000000-0000-0000-0000-000000000000') AS customerGuid
		   ,tc.src_customerId
		   ,tc.globalUserId
		   ,CONVERT(DateTime2(0), ISNULL(tc.customerCreateDateGMT, '1900-01-01')) AS customerCreateDateGMT
		   ,CONVERT(DateTime2(0), tc.[activateDate]) AS [activateDate]
		   ,tc.[customerLoginName]
		   ,tic.[customerFirstName]
		   ,tic.[customerLastName]
		   ,tic.[customerAddress]
		   ,CONVERT(Varchar(20), REPLACE(tic.[customerZipCode], CHAR(9), '')) AS [customerZipCode]
		   ,CONVERT(NVarchar(255), LOWER(ISNULL(RTRIM(LTRIM(REPLACE(customerCity, CHAR(9), ''))), 'Unknown'))) AS [customerCity]
		   ,tic.[customerPhone]
		   ,tic.[customerCellPhone]
		   ,CONVERT(Varchar(320), tic.[customerEmail]) AS [customerEmail]
		   ,tic.[customerBirthDate]
		   ,ISNULL(tic.[customerGender], 'U') AS [customerGender]
		   ,tc.[socialSecurityNumber]
		   ,CONVERT(Varchar(20), tic.[referralCode]) AS [referralCode]
		   ,CONVERT(Varchar(25), tc.[registrationSource]) AS [registrationSource]
		   ,tc.[pre_migration_userid]
		   ,CONVERT(DateTime2(0), tc.[migrationDateGMT]) AS [migrationDateGMT]
		   ,tic.[customerPwdChecksum]
		   ,tc.[externalCustomerID]
		   ,CONVERT(Varchar(45), tc.[customerRegIP]) AS [customerRegIP]
		   ,CONVERT(DateTime2(0), tc.[temporaryAccountExpiryDate]) AS [temporaryAccountExpiryDate]
		   ,CONVERT(DateTime2(0), tc.[temporaryAccountFreezeDate]) AS [temporaryAccountFreezeDate]
		   ,tc.[temporaryAccountUseFreezeAccount] AS [temporaryAccountUseFreezeAccount]
		   ,CONVERT(DateTime2(0), tc.[temporaryAccountEncourageVerificationDate]) AS [temporaryAccountEncourageVerificationDate]
		   ,ISNULL(tic.[customerSportsbookRiskGeneralLimit], 1) AS [customerSportsbookRiskGeneralLimit]
		   ,CONVERT(DateTime2(0), tic.[selfExcludedEndDate]) AS [selfExcludedEndDate]
		   ,tic.[isBonusAbuser]
		   ,ISNULL(tic.[AcceptEmailOffers], 0) AS [acceptEmailOffers]
		   ,ISNULL(tic.[AcceptSmsOffers], 0) AS [acceptSmsOffers]
		   ,ISNULL(tic.[acceptTeleSalesOffers], 0) AS [acceptTeleSalesOffers]
		   ,ISNULL(tic.[sk_customerState], -2) AS [sk_customerState]
		   ,ISNULL(tic.[sk_customerType], -2) AS [sk_customerType]
		   ,CASE WHEN ISNULL(tc.registrationLanguageCode, '') = '' THEN '--'
				 ELSE UPPER(CAST(tc.registrationLanguageCode AS Char(2)))
			END registrationLanguageCode
			,CASE WHEN ISNULL(tic.[countryCode], '') = '' THEN '--'
             ELSE UPPER(RTRIM(CAST(tic.[countryCode] AS CHAR(3))))
			END [countryCode]
		   ,ISNULL(CONVERT(Char(3), RTRIM(tic.[currencyCode])), 'N/A') AS [currencyCode]
		   ,ISNULL(tc.[acquisitionSourceID], -1) AS [acquisitionSourceID]
		   ,ISNULL(tc.[sk_channel], -1) AS [sk_channel]
		   ,ISNULL(tic.[customerMarketID], -1) AS [customerMarketID]
		   ,ISNULL(tic.[sk_affiliate], -1) AS [sk_affiliate]
		   ,CONVERT(Int, ISNULL(tic.[sk_zbanCode], -1)) AS [sk_zbanCode]
		   ,ISNULL(tc.sk_deviceType, -1) AS sk_deviceType
		   ,ISNULL(tic.PhoneNumberVerified,0) AS PhoneNumberVerified
		   ,ISNULL(tic.EmailVerified,0) AS EmailVerified
   FROM		dbo.tcustomer AS tc
   INNER JOIN dbo.tinternalCustomer AS tic
			ON tc.sk_customer = tic.sk_customer
   INNER JOIN @sk_brandList brandlist
			ON brandlist.sk_brand = tic.sk_registerBrand
   LEFT JOIN dbo.tcustomerMarket AS tcm
			ON tic.customerMarketID = tcm.customerMarketID
   LEFT JOIN dbo.taffiliate AS ta
			ON tic.sk_affiliate = ta.sk_affiliate
   WHERE	((tic.regDate >= @startDate
			  OR tic.changeDate >= @startDate)
			 OR ((tcm.regDate >= @startDate
				 OR tcm.changeDate >= @startDate))
				--We may add other changeDate from other tables...
			 ) ;

   SET @msg = 'Number of rows: ' + CONVERT(Varchar(10), @@ROWCOUNT);
   EXEC dbo.logInfo @logID = @logID, @msg = @msg;

   EXEC dbo.logStop @logID = @logID;
END;

GO
