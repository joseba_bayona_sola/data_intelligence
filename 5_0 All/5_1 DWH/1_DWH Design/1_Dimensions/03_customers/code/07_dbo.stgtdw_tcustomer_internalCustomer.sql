
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ==========================================================================================

-- Author: Andreas Henningsson
-- Date: 2010-09-22
-- Changed:
--	2012-06-20	wiol	broken out from stgtdw_tcustomer
--	2012-06-27	anhe	Changed the join to affiliate to avoid 'Not Defined' Affiliate Codes
--	2012-07-14	anhe	Fixed bug, wrong join against general risk limit table
--	2012-08-13	mabo	We have been recently receiving duplicate sportsbook user mapping entries which were ending up in 'tch_getCustomerSportsbookMapping'.
--						This duplicate mapping was causing the ETL to fail on the customer pk constraint. One of the temporary solution was to remove the join on 
--						'tch_getCustomerSportsbookMapping' during the population of #intCustomer. We will then do a seperate update for 'riskGeneralLimit' which will 
--						prevent pk constraing errors even though the 'tch_getCustomerSportsbookMapping' contains duplicate user mappings.
--	2012-11-13	vlca	added locking mechanism during Merge statement 
--	2013-01-28	rama	added DK temporary accounts
--	2013-03-05	acla	added time conversion function
--  2013-04-02  zobl	added new logic for calculating the customer market for Betsson.dk NRCs (from 04th of April, assigned to Danish Market)
--  2013-04-10	stca	removed function fnUTC_to_CET
--	2013-05-21	ANCA	Changed the non-affiliated customers logic [removed the not defined]
--	2013-06-26	joah1	added logic to handle new gunners gameing customers
--	2013-07-08	joah1	fixed a bug with the GG customers	
--	2013-07-17	jora1	added column acceptTeleSalesOffers to the Mergestatements and implemented the error table update for it
--	2013-09-30	mabu2	Added PhoneNumberVerified and EmailVerified fields.
--	2013-11-04	jora1	Added city in the update for error_tch_getCustomers to remove null CustomerCity from table
-- 	2014-01-30 	rata	Changed the join condition of tch_getCustomerSportsbookMapping from sb_merchantID to merchantID
-- 	2014-04-17 	jofe	Remove any current logic related to SB risk limit, this will now be moved to stgtdw_tcustomer_internalCustomer_spo
--	2014-04-11	alra	Placed a logic in retrieving the gender for standardization
--	2014-09-19	kosa	Added logic to assign B2B Dhoze customer market for customers registered before 8th Aug 2014
--  2015-01-20  stca	Removed RiskGeneralLimit Data handling from this sp because the only link between spo_GetGameStatisticsRiskGeneralLimitData 
--						and tinternalCustomer is to use tcustomerProviderMapping and at this stage, it is not populated yet. That will happen in
--						stgtdw_tcustomer_internalCustomer_spo.
--  2015-01-30 gohr    CONVERT(NVARCHAR(20),u.ZipCode) AS ZipCode
--  2015-03-16 rama    Changed ISNULL(u.FirstName,'') AS FirstName and LastName
--  2015-12-03 leto    Fix dbo.fnConvertTime(@sourceTimeZone, @biWarehouseTimeZone,...) not to throw overflow error
-- ==========================================================================================

-- Description:
--	Do not run alone! Should be run by stored procedure stgtdw_tcustomer 
--	Moves customer info data from stage to TDW. Updates or creates new tinternalCustomer. 
--	TinternalCustomer has SCD2 history handling on certain attributes, and SCD1 attributes on all others.
--	
--	This proc loads from stage and also from error table. Users can end up in the error table if they fail pre-
--	defined checks when creating the tcustomer object. Once they are fixed (error message removed) an internalCustomer
--	is created/updated based on data in the error table.
--	
--	As we can recieve several updates to a customer on a single day, and we only have history tracking
--	on a date level, if we recieve more than one update on any of the SCD2 attributes we treat it as
--	a SCD1 attribute and overwrite it. And to avoid ambiguity of when a change occured, we set the date 
--	of the change to tomorrow.
--	
--	The SCD2 attributes as of 2012-06-25 are: brand, customerState, customerMarketID, affiliate, zbanCode,
--	affiliateStrikeBack, zbanCodeStrikeBack, customerSportsbookRiskGeneralLimit, customerType, selfExcludedEndDate
--	
--	The logic below is a first merge to update already created SCD2 rows (ie more than one change in one
--	day). Then a merge for creating new SCD2 rows using output over DML. Lastly there is a merge to update
--	SCD1 attributes

-- ==========================================================================================

CREATE PROCEDURE [dbo].[stgtdw_tcustomer_internalCustomer] 
	@packageRunID INT = NULL,
	@sourceTimeZone VARCHAR(50),
	@biWarehouseTimeZone VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	-- log start
	DECLARE @logID INT ,@msg VARCHAR(512), @procName VARCHAR(50) = OBJECT_NAME(@@procid), @rows INT
	EXEC logStart @procName, @packageRunID, @logID OUTPUT

	-- Error handling variables
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	DECLARE @totalAffectedRows INT,@insertCount INT, @updateCount INT, @deleteCount INT
	DECLARE @rowCount TABLE(mergeAction NVARCHAR(10))

	-- process the users and scd2 dimension
	DECLARE @newUsers INT
			,@scd2ChangedUsers INT
			,@changedUsers INT
			,@newMigratedUsers INT
	SELECT	@newUsers  = 0
			,@scd2ChangedUsers = 0
			,@changedUsers = 0
			,@totalAffectedRows = 0
			,@insertCount = 0
			,@updateCount = 0
			,@deleteCount = 0

	DECLARE @breakDate DATE
	-- here we should use the date from source that the change for the user took place
	-- // however this works as long as there are daily loads occurring
	SET		@breakDate = CAST(DATEADD(day,1+DATEDIFF(day,0,GETUTCDATE()),0) AS DATE) -- This is tomorrow CET


	-- load meta data, populated by stored proc stgtdw_tcustomer_affiliate
		SELECT *
		INTO #metaInfo2
		FROM stage.dbo.transform_customerMetaInfo t

	
	CREATE TABLE #intCustomer
	(
		[customerId] INT NOT NULL,
		[merchantId] INT NOT NULL,
		[sk_customer] [int] NOT NULL PRIMARY KEY,
		[effectiveDateBeginCET] [date] NULL,
		[effectiveDateEndCET] [date] NULL,
		[isCurrent] [bit] NOT NULL,
		[sk_affiliate] [int] NOT NULL,	
		[sk_zbanCode] [int] NOT NULL,
		[customerMarketID] [smallint] NOT NULL,
		[sk_affiliateStrikeBack] [int] NOT NULL,
		[sk_zbanCodeStrikeBack] [int] NOT NULL,
		[sk_registerBrand] SMALLINT NOT NULL,
		[sk_customerState] SMALLINT NOT NULL,		
		[currencyCode] [nvarchar](3) NOT NULL,
		[customerFirstName] [nvarchar](255) NOT NULL,
		[customerLastName] [nvarchar](255) NOT NULL,
		[customerAddress] [nvarchar](255) NULL,
		[customerZipCode] [nvarchar](20) NULL,
		[customerCity] [nvarchar](255) NULL,
		[countryCode] [char](2) NOT NULL,
		[customerPhone] [nvarchar](255) NULL,
		[customerEmail] [nvarchar](255) NULL,
		[customerBirthDate] [date] NULL,
		[customerGender] [nvarchar](7) NULL,
		[customerCellPhone] [nvarchar](255) NULL,
		[referralCode] [nvarchar](20) NULL,
		[AcceptEmailOffers] [bit] NOT NULL,
		[AcceptSmsOffers] [bit] NOT NULL,
		[customerPwdChecksum] [int] NOT NULL,
		[customerSportsbookRiskGeneralLimit] REAL NULL,	
		[affiliateStrikeBackTagDate] DATETIME NULL,
		[externalCustomerID] [int] NULL,
		[sk_customerType] [smallint] NOT NULL, 
		[selfExcludedEndDate] DATETIME NULL,
		[regDate] [datetime] NOT NULL,
		[changeDate] [datetime] NULL,
		[isBonusAbuser] [bit] NULL,
		[temporaryAccountExpiryDate] DATETIME NULL,
		[acceptTeleSalesOffers] BIT NULL
	)

	CREATE TABLE #scdUser
	(
		[sk_customer] [int] NOT NULL PRIMARY KEY,
		[effectiveDateBeginCET] [date] NULL,
		[effectiveDateEndCET] [date] NULL,
		[isCurrent] [bit] NOT NULL,
		[sk_affiliate] [int] NOT NULL,	
		[sk_zbanCode] [int] NOT NULL,
		[customerMarketID] [smallint] NOT NULL,
		[sk_affiliateStrikeBack] [int] NOT NULL,
		[sk_zbanCodeStrikeBack] [int] NOT NULL,
		[sk_registerBrand] SMALLINT NOT NULL,
		[sk_customerState] SMALLINT NOT NULL,		
		[currencyCode] [nvarchar](3) NOT NULL,
		[customerFirstName] [nvarchar](255) NOT NULL,
		[customerLastName] [nvarchar](255) NOT NULL,
		[customerAddress] [nvarchar](255) NULL,
		[customerZipCode] [nvarchar](20) NULL,
		[customerCity] [nvarchar](255) NULL,
		[countryCode] [char](2) NOT NULL,
		[customerPhone] [nvarchar](255) NULL,
		[customerEmail] [nvarchar](255) NULL,
		[customerBirthDate] [date] NULL,
		[customerGender] [nvarchar](7) NULL,
		[customerCellPhone] [nvarchar](255) NULL,
		[referralCode] [nvarchar](20) NULL,
		[AcceptEmailOffers] [bit] NOT NULL,
		[AcceptSmsOffers] [bit] NOT NULL,
		[customerPwdChecksum] [int] NOT NULL,
		[customerSportsbookRiskGeneralLimit] REAL NULL,  
		[affiliateStrikeBackTagDate] DATETIME NULL,
		[sk_customerType] [smallint] NOT NULL,
		[selfExcludedEndDate] DATETIME NULL,
		[regDate] [datetime] NOT NULL,
		[changeDate] [datetime] NULL,
		[isBonusAbuser] [bit] NULL,
		[acceptTeleSalesOffers] BIT NULL,
		[PhoneNumberVerified] BIT NULL,
		[EmailVerified] BIT NULL
	)


	--=============================================	===================	
	-- Set Customer Market
	-------------------------------------------------------------------------------------
	
	-- Log
	EXEC dbo.logInfo @logID, 'Start handling customer market'

	BEGIN TRY 

	-- Create temporary table with customer market
		CREATE TABLE #customerMarket (
			sk_customer INT,
			src_customerId INT,
			sk_merchant SMALLINT,
			sk_brand SMALLINT,
			[customerMarketID] SMALLINT,
			twoLetterISORegionName NVARCHAR(50),
			customerCreateDateGMT date
		)


	-- Get all customers for customer market
		INSERT INTO #customerMarket ( sk_customer,src_customerId,sk_merchant,sk_brand,customerMarketID,twoLetterISORegionName, customerCreateDateGMT)
		SELECT 
		tc.sk_customer,
		tc.src_customerId,
		tc.sk_merchant, 
		ISNULL(sk_registerBrand,tb.sk_brand), 
		ISNULL(tic.customermarketid,-1) AS customerMarketID, 
		ISNULL(tic.countryCode,stage.twoLetterISORegionName),
		tc.customerCreateDateGMT
		FROM TDW.dbo.tcustomer tc
			LEFT JOIN tdw.dbo.tmerchant tm ON tc.sk_merchant = tm.sk_merchant
			LEFT JOIN stage.dbo.tch_getCustomers stage ON tc.src_customerId = stage.customerId AND tm.merchantID = stage.merchantId
			LEFT JOIN tdw.dbo.tbrand tb ON tb.brandID = stage.registerBrandId
			LEFT JOIN TDW.dbo.tinternalCustomer tic 
				ON tc.sk_customer = tic.sk_customer AND tic.isCurrent = 1 

		DECLARE @ggCustomerMarket INT = (
			SELECT customerMarketID 
			FROM tdw.dbo.tcustomerMarket tcm
			INNER JOIN tdw.dbo.tbrand tb ON tcm.sk_brand = tb.sk_brand
			WHERE level1 = 'GunnersGaming' AND tb.brandName = 'Betsson'
		)

        --B2B Market for Dhoze before 8th August 2014
		DECLARE @oldDhozeMarket INT = ( SELECT  customerMarketID
                                        FROM    tdw.dbo.tcustomerMarket m
                                                INNER JOIN tdw.dbo.tbrand b ON b.sk_brand = m.sk_brand
                                        WHERE   brandName = 'Dhoze'
                                                AND level1 = 'Portugal'
                                                AND businessChannelID = 1
                                      )

	-- Calculate market only for customers having unknown market, no changing of market allowed
	-- CASE CLAUSE: if betsson.dk and NRC comes after 4th of April, connect this to Danish Market
		UPDATE cm SET [customerMarketID] = 
				CASE 
					WHEN (tb.brandid = 13 AND cm.customerCreateDateGMT >= '2013-04-04 00:00:00') THEN 90
					WHEN (tb.brandName = 'Dhoze' AND cm.customerCreateDateGMT < '2014-08-08 00:00:00') THEN @oldDhozeMarket
					WHEN  cV.[key]='betssoncombrand' AND cV.value = 'gunnersgaming' THEN @ggCustomerMarket
					ELSE (	isnull(isnull(mrm.customerMarketID,tab.defaultCustomerMarketID),-1))
				END	
		FROM #customerMarket cm 
		LEFT JOIN TDW.dbo.tbrand tb ON cm.sk_brand = tb.sk_brand
		LEFT JOIN tactiveBrand tab ON tab.brandid = tb.brandID
		LEFT JOIN tcustomerMarketRegionMapping mrm ON mrm.twoLetterISORegionName = cm.twoLetterISORegionName AND mrm.brandid = tab.brandid
		LEFT JOIN stage.dbo.tch_getCustomersMetaData cV ON cV.customerId=cm.src_customerId AND cm.sk_merchant=8 AND cV.[key]='betssoncombrand' --In order to tag GG-customers we need to check the customerVariab
		WHERE 
			cm.customerMarketID = -1 -- Only set on new customers

	-- Create index to fix performance	
		CREATE NONCLUSTERED INDEX [IX_customerMarket__sk_customer]
		ON #customerMarket ([sk_customer])
		INCLUDE ([customerMarketID])
	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		RETURN;
	END CATCH;
	
		-- Log
		EXEC logInfo @logID, 'End handling customer market'
	

	-------------------------------------------------------------------------------------
	-- End handling of customer market
	--=============================================	===================	

	-------------------------------------------------------------------------------------
	-- Populate error table
	-- The customer object is placed in the error table during processing of tcustomer 
	-- Additional data is added here, toghether with related data that may only arrive on that specific day.
	-- Update only those that have an error
	----
	--=============================================	===================	

			--update error table to contain all fields necessary to create internal customer 
			UPDATE err
			SET err.registerBrandId = stage.registerBrandId
				,err.isInactive = stage.isInactive
				,err.isSelfExcluded = stage.isSelfExcluded
				,err.isTestUser = stage.isTestUser
				,err.inactiveType = stage.inactiveType
				,err.inactiveReason = stage.inactiveReason
				,err.acceptEmailOffers = stage.acceptEmailOffers
				,err.acceptSmsOffers = stage.acceptSmsOffers
				,err.firstName = stage.firstName
				,err.lastName = stage.lastName
				,err.address = stage.address
				,err.zipCode = stage.zipCode
				,err.twoLetterISORegionName = stage.twoLetterISORegionName
				,err.emailAddress = stage.emailAddress
				,err.phoneNumber = stage.phoneNumber
				,err.cellPhoneNumber = stage.cellPhoneNumber
				,err.birthDate = stage.birthDate
				,err.gender = stage.gender
				,err.isoCurrencySymbol = stage.isoCurrencySymbol
				,err.timeZoneId = stage.timeZoneId
				,err.passwordCheckSum = stage.passwordCheckSum  
				,err.selfExcludedEndDate = stage.selfExcludedEndDate
				,err.meta_registrationCode = meta.registrationCode
				,err.meta_zbanCode = meta.zbanCode
				,err.meta_isBonusAbuser = meta.isBonusAbuser
				,err.meta_affiliateCode = meta.affiliateCode
				--,err.sp_riskGeneralLimit = gl.riskGeneralLimit
				,err.acceptTeleSalesOffers = stage.acceptTeleSalesOffers
				,err.city = stage.city --	2013-11-04	jora1

			FROM tdw.dbo.error_tch_getCustomers err
			JOIN stage.dbo.tch_getCustomers stage ON stage.customerid = err.customerId AND stage.merchantId = err.merchantId
			LEFT JOIN tdw.dbo.tmerchant tm ON tm.merchantID = stage.merchantID
			LEFT JOIN #metaInfo2 meta ON meta.customerId = stage.customerId AND meta.sk_merchant = tm.sk_merchant
			LEFT JOIN tactiveMerchant tam ON err.merchantid = tam.merchantid
			--LEFT JOIN stage.dbo.tch_getCustomerSportsbookMapping tsm ON tsm.customerId = err.customerId AND tsm.merchantId = tam.merchantid --the merchantID in tch_getCustomerSportsbookMapping is not SB merchantID
			--LEFT JOIN Stage.[dbo].[spo_GetGameStatisticsRiskGeneralLimitData] gl ON gl.merchantUserId = tsm.userId AND gl.merchantId = tam.SB_merchantID	--should the join not be on sp merchant?			
			WHERE err.errorMessage IS NOT NULL

	-------------------------------------------------------------------------------------
	-- End populate error table
	--=============================================	===================	


	--=============================================	===================	
	-- Generate base table, mainly surrogate key replacements
	-------------------------------------------------------------------------------------	
		-- Log
		EXEC logInfo @logID, 'Start create #intCustomer'

	BEGIN TRY

	-- Convert data to format in TDW, mainly surrogate key replacements
		INSERT INTO #intCustomer
		(
			[customerId],
			[merchantId],
			[sk_customer],
			[effectiveDateBeginCET],
			[effectiveDateEndCET],
			[isCurrent],
			[sk_registerBrand],
			[sk_customerState],		
			[sk_affiliateStrikeBack],
			[sk_zbanCodeStrikeBack],
			[sk_affiliate],	
			[sk_zbanCode],
			[customerMarketID],
			[currencyCode],
			[customerFirstName],
			[customerLastName],
			[customerAddress],
			[customerZipCode],
			[customerCity],
			[countryCode],
			[customerPhone],
			[customerEmail],
			[customerBirthDate],
			[customerGender],
			[customerCellPhone],
			[referralCode],
			[AcceptEmailOffers],
			[AcceptSmsOffers],
			[customerPwdChecksum],
			[customerSportsbookRiskGeneralLimit], 
			[sk_customerType],
			[affiliateStrikeBackTagDate],
			[selfExcludedEndDate],
			[regDate],
			[changeDate],
			[isBonusAbuser],
			[temporaryAccountExpiryDate],
			[acceptTeleSalesOffers]
		)
		SELECT 
			u.customerId
			,u.merchantId
			,tc.sk_customer
			,CASE
				WHEN u.registrationDate >= '9999-12-31' THEN '9999-12-31'
				ELSE CAST(dbo.fnConvertTime(@sourceTimeZone, @biWarehouseTimeZone,u.registrationDate) AS DATE)
			 END AS effectiveDateBeginCET
			,NULL AS effectiveDateEndCET
			,1 AS isCurrent
			,vb.sk_brand
			,COALESCE(cs.sk_customerState,-1) AS sk_customerState
			,-1 AS sk_affiliateStrikeBack--COALESCE(tasb.sk_affiliateSB, -1)
			,-1 AS sk_zbanCodeStrikeBack --COALESCE(tzc.sk_zbanCode, -1)
			,COALESCE(ram.sk_affiliate, ta.sk_affiliate,-1) AS sk_affiliate			
			,
			-- Normally we send zban code to commission lounge. 
			-- If we dont have one we send affiliate code. So when we get
			-- a registration code affiliate override we set the zban code to -1 
			-- and send the affiliate code to zban.
			CASE 
				WHEN ram.sk_affiliate IS NOT NULL THEN -1
				ELSE COALESCE(zc.sk_zbanCode, -1)
			 END AS sk_zbanCode
			,COALESCE(cm.customerMarketID,-1) AS customerMarketID
			--,fran.sk_franchisee
			--,ts.sk_site		
			,COALESCE(tcur.currencyCode,'N/A') AS currencyCode
			,ISNULL(u.FirstName,'') AS FirstName
			,ISNULL(u.LastName,'') AS LastName
			,u.Address
			,CONVERT(NVARCHAR(20),u.ZipCode) AS ZipCode
			,u.City		
			,u.TwoLetterISORegionName
			,u.PhoneNumber
			,u.EmailAddress				
			,u.BirthDate
			,CASE 
				WHEN LTRIM(u.Gender) = '' THEN 'U' 
				WHEN LTRIM(RTRIM(u.Gender)) = 'NULL' THEN 'U'
				ELSE ISNULL(LEFT(LTRIM(u.Gender),1),'U') 
			 END AS Gender
			,u.CellPhoneNumber
			--,u.userNotes		
			,NULL AS referralCode -- Currently we do not get this in Noak, it does not exist yet
			,AcceptEmailOffers
			,AcceptSmsOffers
			,ISNULL(passwordCheckSum,0) AS passwordCheckSum
			--,gl.riskGeneralLimit
			,1 -- riskGeneralLimit
			,( --- sk_customerType
				CASE	
							-- flagged as test account --> TEST CUSTOMER
							WHEN u.isTestUser = 1 THEN (SELECT t.sk_customerType FROM TDW.dbo.tcustomerType t WITH(NOLOCK) WHERE t.customerTypeName = 'TEST CUSTOMER') 
							-- not flagged as test account, but in exception table (i.e. robots, fraud accounts etc..) use that customer type.
							WHEN u.isTestUser = 0 AND  ISNULL((SELECT COUNT(*) FROM tcustomerTypeException cc WITH(NOLOCK) WHERE cc.customerID = u.customerId AND cc.merchantID = u.merchantId),0) = 1 THEN 
							(	SELECT ctt.sk_customerType 
								FROM tcustomerTypeException cte WITH(NOLOCK) 
									JOIN TDW.dbo.tcustomerType ctt WITH(NOLOCK) ON cte.customerTypeName = ctt.customerTypeName
								WHERE cte.customerID = u.customerId AND cte.merchantID = u.merchantId
							) 
							-- non of the above and not in exception --> "Regular" customer.
							WHEN u.isTestUser = 0 AND ISNULL((SELECT COUNT(*) FROM tcustomerTypeException cc WITH(NOLOCK) WHERE cc.customerID = u.customerId AND cc.merchantID = u.merchantId),0) = 0 THEN
							(
								SELECT sk_customerType
								FROM TDW.dbo.tcustomerType ctt WITH(NOLOCK) 
								WHERE ctt.customerTypeName = 'CUSTOMER'
							)
							-- in case we screw up..						
							ELSE -1 -- default -1 N/A..
					END
			)  AS sk_customerType --- end sk_customerType 
			,NULL AS affiliateStrikeBackTagDate -- Not valid for our turkish customers
			,CASE
				WHEN u.selfExcludedEndDate >= '9999-12-31' THEN '9999-12-31'
				ELSE dbo.fnConvertTime(@sourceTimeZone, @biWarehouseTimeZone,u.selfExcludedEndDate)
			 END AS selfExcludedEndDate
			,u.regDate
			,NULL AS changeDate
			,isBonusAbuser
			,tc.[temporaryAccountExpiryDate]
			,u.acceptTeleSalesOffers   
		FROM Stage.dbo.tch_getCustomers u	
		LEFT JOIN tactiveMerchant tam ON u.merchantid = tam.merchantid
		-- left join stage.dbo.tch_getCustomerSportsbookMapping tsm on tsm.customerId = u.customerId and tsm.merchantId = tam.merchantId
		-- left join Stage.[dbo].[spo_GetGameStatisticsRiskGeneralLimitData] gl on gl.merchantUserId = tsm.userId and gl.merchantId = tam.SB_merchantId
		JOIN TDW.dbo.vbrand vb ON u.MerchantId = vb.merchantID AND u.registerBrandId = vb.brandID
		JOIN TDW.dbo.tcustomer tc ON tc.src_customerID = u.CustomerId AND tc.sk_merchant = vb.sk_merchant
		LEFT JOIN #metaInfo2 mi ON mi.customerid = u.customerId AND mi.sk_merchant = vb.sk_merchant
		LEFT JOIN #customerMarket cm ON cm.sk_customer = tc.sk_customer
		LEFT JOIN TDW.dbo.tcurrency tcur ON tcur.currencyCode = u.ISOCurrencySymbol
		-- left join TDW.dbo.taffiliate ta on ta.affiliateCode = COALESCE(mi.affiliateCode, 'Not Defined')	
		LEFT JOIN TDW.dbo.taffiliate ta ON ta.affiliateCode = mi.affiliateCode
		LEFT JOIN TDW.dbo.tregistrationAffiliateMapping ram ON ram.registrationCode = mi.registrationCode AND ram.sk_merchant = mi.sk_merchant
		-- left join TDW.dbo.tzbanCode zc on zc.zbanCode = COALESCE(mi.zbanCode, 'Not Defined') 
		LEFT JOIN TDW.dbo.tzbanCode zc ON zc.zbanCode = mi.zbanCode
		LEFT JOIN TDW.dbo.tcustomerState cs ON cs.customerStatus 	
				 = CASE 
				 --Migration complete or not an migrated user, set standard statuses
				WHEN tc.migrationStatusID IN (1,0) THEN 
					CASE 
						-- Customer is active
						WHEN u.isInactive = 0 AND u.isSelfExcluded = 0
							THEN 'Active'
						
						--Customer is self excluded	
						WHEN u.isInactive	 = 0 AND u.isSelfExcluded = 1
							THEN 'ExclusionRequestedByCustomer'
						-- Customer is inactive - check reason
						ELSE 
							COALESCE(u.inactiveReason,'N/A') 
					END
			
				-- Migration in progress, set migration statuses
				WHEN tc.migrationStatusID = 2 THEN 'Awaiting Customer Data'
				WHEN tc.migrationStatusID = 3 THEN 'Migration in Progress'
			
				-- Something wrong and user not catched in previous cases default to unknown status
				ELSE 'N/A'
			END
			AND -- Oh, this is one ugly join
				cs.customerStatusGroup = 
				CASE
					-- Migration complete or not an migrated user, set standard statuses
				WHEN tc.migrationStatusID IN (1,0) THEN 
					CASE 
						-- Customer is active
						WHEN u.isInactive = 0 AND u.isSelfExcluded = 0
							THEN 'Active'
						--Customer is self excluded	
						WHEN u.isInactive	 = 0 AND u.isSelfExcluded = 1
							THEN 'TemporaryInactivated'  
						-- Customer is inactive - check reason
						ELSE COALESCE(u.inactiveType,'N/A') 
					END
				-- Migration in progress, set migration statuses
				WHEN tc.migrationStatusID IN (2,3) THEN 'Migration'
			
				-- Something wrong and user not caught in previous cases default to unknown status
				ELSE 'N/A'
			END
		
		-- Temp Fix (mabo : 2012-08-13 12:15) - We have been recently receiving duplicate sportsbook user mapping entries which were ending up in 'tch_getCustomerSportsbookMapping'.
		-- This duplicate mapping was causing the ETL to fail on the customer pk constraint. One of the temporary solution was to remove the join on 
		-- 'tch_getCustomerSportsbookMapping' during the population of #intCustomer. We will then do a seperate update for 'riskGeneralLimit' which will 
		-- prevent pk constraing errors even though the 'tch_getCustomerSportsbookMapping' contains duplicate user mappings.
		--UPDATE ic
		--SET ic.customerSportsbookRiskGeneralLimit = ISNULL(gl.riskGeneralLimit,1)
		--	FROM #intCustomer ic
		--LEFT JOIN tactiveMerchant tam ON ic.merchantid = tam.merchantID
		--LEFT JOIN stage.dbo.tch_getCustomerSportsbookMapping tsm ON tsm.customerId = ic.customerId AND tsm.merchantId = tam.merchantid --the merchantid in tch_getCustomerSportsbookMapping is not sb merchantid
		--LEFT JOIN Stage.dbo.spo_GetGameStatisticsRiskGeneralLimitData gl ON gl.merchantUserId = tsm.userId AND gl.merchantId = tam.SB_merchantID
	
		--do the same insert for fixed (=no error) customers from the error table
		INSERT INTO #intCustomer
		(
			[customerId],
			[merchantId],
			[sk_customer],
			[effectiveDateBeginCET],
			[effectiveDateEndCET],
			[isCurrent],
			[sk_registerBrand],
			[sk_customerState],		
			[sk_affiliateStrikeBack],
			[sk_zbanCodeStrikeBack],
			[sk_affiliate],	
			[sk_zbanCode],
			[customerMarketID],
			[currencyCode],
			[customerFirstName],
			[customerLastName],
			[customerAddress],
			[customerZipCode],
			[customerCity],
			[countryCode],
			[customerPhone],
			[customerEmail],
			[customerBirthDate],
			[customerGender],
			[customerCellPhone],
			[referralCode],
			[AcceptEmailOffers],
			[AcceptSmsOffers],
			[customerPwdChecksum],
			[customerSportsbookRiskGeneralLimit], 
			[sk_customerType],
			[affiliateStrikeBackTagDate],
			[selfExcludedEndDate],
			[regDate],
			[changeDate],
			[isBonusAbuser],
			[temporaryAccountExpiryDate],
			[acceptTeleSalesOffers]
		)
		SELECT 
			u.customerId
			,u.merchantId
			,tc.sk_customer
			,CAST(u.registrationDate AS DATE) AS effectiveDateBeginCET
			,NULL AS effectiveDateEndCET
			,1 AS isCurrent
			,vb.sk_brand
			,COALESCE(cs.sk_customerState,-1) AS sk_customerState
			,-1 AS sk_affiliateStrikeBack
			,-1 AS sk_zbanCodeStrikeBack 
			,COALESCE(ram.sk_affiliate, ta.sk_affiliate,-1) AS sk_affiliate			
			,CASE 
				WHEN ram.sk_affiliate IS NOT NULL THEN -1
				ELSE COALESCE(zc.sk_zbanCode, -1)
			 END AS sk_zbanCode
			,COALESCE(cm.customerMarketID,-1) AS customerMarketID
			--,fran.sk_franchisee
			--,ts.sk_site		
			,COALESCE(tcur.currencyCode,'N/A') AS currencyCode
			,ISNULL(u.FirstName,'') AS FirstName
			,ISNULL(u.LastName,'') AS LastName
			,u.Address
			,CONVERT(NVARCHAR(20),u.ZipCode) AS ZipCode
			,u.City		
			,u.TwoLetterISORegionName
			,u.PhoneNumber
			,u.EmailAddress				
			,u.BirthDate
			,CASE 
				WHEN LTRIM(u.Gender) = '' THEN 'U' 
				WHEN LTRIM(RTRIM(u.Gender)) = 'NULL' THEN 'U'
				ELSE ISNULL(LEFT(LTRIM(u.Gender),1),'U') 
			 END AS Gender
			,u.CellPhoneNumber
			,NULL AS referralCode -- Currently we do not get this in Noak, it does not exist yet
			,AcceptEmailOffers
			,AcceptSmsOffers
			,ISNULL(passwordCheckSum,0) AS passwordCheckSum
			,u.sp_riskGeneralLimit  
			,( --- sk_customerType
				CASE	
							WHEN u.isTestUser = 1 THEN (SELECT t.sk_customerType FROM TDW.dbo.tcustomerType t WITH(NOLOCK) WHERE t.customerTypeName = 'TEST CUSTOMER') 
							-- not flagged as test account, but in exception table (i.e. robots, fraud accounts etc..) use that customer type.
							WHEN u.isTestUser = 0 AND  ISNULL((SELECT COUNT(*) FROM tcustomerTypeException cc WITH(NOLOCK) WHERE cc.customerID = u.customerId AND cc.merchantID = u.merchantId),0) = 1 THEN 
							(	SELECT ctt.sk_customerType 
								FROM tcustomerTypeException cte WITH(NOLOCK) 
									JOIN TDW.dbo.tcustomerType ctt WITH(NOLOCK) ON cte.customerTypeName = ctt.customerTypeName
								WHERE cte.customerID = u.customerId AND cte.merchantID = u.merchantId
							) 
							-- non of the above and not in exception --> "Regular" customer.
							WHEN u.isTestUser = 0 AND ISNULL((SELECT COUNT(*) FROM tcustomerTypeException cc WITH(NOLOCK) WHERE cc.customerID = u.customerId AND cc.merchantID = u.merchantId),0) = 0 THEN
							(
								SELECT sk_customerType
								FROM TDW.dbo.tcustomerType ctt WITH(NOLOCK) 
								WHERE ctt.customerTypeName = 'CUSTOMER'
							)
							-- in case we screw up..						
							ELSE -1 -- default -1 N/A..
					END
			)  AS sk_customerType --- end sk_customerType 
			,NULL AS affiliateStrikeBackTagDate -- Not valid for our turkish customers
			,u.selfExcludedEndDate
			,u.regDate
			,NULL 	AS changeDate
			,u.meta_isBonusAbuser
			,tc.[temporaryAccountExpiryDate]
			,u.[acceptTeleSalesOffers]
		FROM tdw.dbo.error_tch_getCustomers u	
		JOIN TDW.dbo.vbrand vb ON u.MerchantId = vb.merchantID AND u.registerBrandId = vb.brandID
		JOIN TDW.dbo.tcustomer tc ON tc.src_customerID = u.CustomerId AND tc.sk_merchant = vb.sk_merchant
		-- no join against #metainfo since it might be missing for error row users
		-- no join against #stage.dbo.tch_getCustomerSportsbookMapping or Stage.[dbo].[spo_GetGameStatisticsRiskGeneralLimitData] since it might be missing for error row users
		LEFT JOIN #customerMarket cm ON cm.sk_customer = tc.sk_customer
		LEFT JOIN TDW.dbo.tcurrency tcur ON tcur.currencyCode = u.ISOCurrencySymbol
	--left join TDW.dbo.taffiliate ta on ta.affiliateCode = COALESCE(u.meta_affiliateCode, 'Not Defined')	
	left join TDW.dbo.taffiliate ta on ta.affiliateCode = COALESCE(u.meta_affiliateCode, 'N/A')		
		LEFT JOIN TDW.dbo.tregistrationAffiliateMapping ram ON ram.registrationCode = u.meta_registrationCode AND ram.sk_merchant = vb.sk_merchant
	--left join TDW.dbo.tzbanCode zc on zc.zbanCode = COALESCE(u.meta_zbanCode, 'Not Defined')
	left join TDW.dbo.tzbanCode zc on zc.zbanCode = COALESCE(u.meta_zbanCode, 'N/A')
		LEFT JOIN TDW.dbo.tcustomerState cs ON cs.customerStatus 	
				 = CASE 
				 --Migration complete or not an migrated user, set standard statuses
				WHEN tc.migrationStatusID IN (1,0) THEN 
					CASE 
						-- Customer is active
						WHEN u.isInactive = 0 AND u.isSelfExcluded = 0
							THEN 'Active'	
						--Customer is self excluded	
						WHEN u.isInactive	 = 0 AND u.isSelfExcluded = 1
							THEN 'ExclusionRequestedByCustomer'
						-- Customer is inactive - check reason
						ELSE 
							COALESCE(u.inactiveReason,'N/A') 
					END	
				-- Migration in progress, set migration statuses
				WHEN tc.migrationStatusID = 2 THEN 'Awaiting Customer Data'
				WHEN tc.migrationStatusID = 3 THEN 'Migration in Progress'
				-- Something wrong and user not catched in previous cases default to unknown status
				ELSE 'N/A'
			END
			AND -- Oh, this is one ugly join
				cs.customerStatusGroup = 
				CASE
					-- Migration complete or not an migrated user, set standard statuses
				WHEN tc.migrationStatusID IN (1,0) THEN 
					CASE 
						-- Customer is active
						WHEN u.isInactive = 0 AND u.isSelfExcluded = 0
							THEN 'Active'
						--Customer is self excluded	
						WHEN u.isInactive	 = 0 AND u.isSelfExcluded = 1
							THEN 'TemporaryInactivated'  
						-- Customer is inactive - check reason
						ELSE COALESCE(u.inactiveType,'N/A') 
					END
			
				-- Migration in progress, set migration statuses
				WHEN tc.migrationStatusID IN (2,3) THEN 'Migration'
			
				-- Something wrong and user not caught in previous cases default to unknown status
				ELSE 'N/A'
			END
		WHERE NOT EXISTS (SELECT * FROM #intCustomer ic 
								WHERE ic.customerID = u.customerId AND ic.merchantID = u.merchantID)
			AND u.errorMessage IS NULL

		DECLARE @sk_customerStateActive INT  
		DECLARE @sk_customerStateTemporaryAwaitingActivation INT
		DECLARE @sk_customerStateTemporaryAccountExpired  INT 

		SELECT @sk_customerStateActive=sk_customerState FROM tdw.dbo.tcustomerState WHERE customerStatus='Active'
		SELECT @sk_customerStateTemporaryAwaitingActivation=sk_customerState FROM tdw.dbo.tcustomerState WHERE customerStatus='Temporary Awaiting Activation'
		SELECT @sk_customerStateTemporaryAccountExpired=sk_customerState FROM tdw.dbo.tcustomerState WHERE customerStatus='Temporary Account Expired'

		UPDATE temp
		SET temp.[sk_customerState]=tic.sk_customerState
		FROM #intCustomer temp
		INNER JOIN tdw.dbo.tinternalcustomer tic
				ON temp.sk_customer=tic.sk_Customer
		WHERE 1=1
			AND [temporaryAccountExpiryDate] IS NOT NULL 
			AND temp.[sk_customerState]=1 --When the status is 'Active', the status could actually be 'Active', 'Temporary Awaiting Activation' or 'Temporary Account Expired'
			AND tic.sk_internalCustomer IN (
											SELECT sk_internalCustomer
											FROM 
											(
												SELECT RANK() OVER (PARTITION BY sk_customer ORDER BY effectiveDateBeginCET DESC) AS ranks,sk_internalCustomer
												FROM tdw.dbo.tinternalcustomer
												WHERE 1=1
													AND sk_customerState IN (@sk_customerStateActive,@sk_customerStateTemporaryAwaitingActivation,@sk_customerStateTemporaryAccountExpired)
											) tmp
											WHERE tmp.ranks=1 --We take the latest status among 'Active', 'Temporary Awaiting Activation' and 'Temporary Account Expired'
										)

	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		RETURN;
	END CATCH;	
			
	
		-- Log
		EXEC logInfo @logID, 'End create #intCustomer'

	
	-------------------------------------------------------------------------------------
	-- End creating base table
	--=============================================	===================	
	
	--=============================================	===================	
	-- --Handle more than one change per day
		-- In this case there has been more than one change
		-- to a user in one day (detected by that the effectiveDateBegin
		-- is equal to breakDate). In this case we simply update the row
		-- The full changelog will be visible in the Audit database.
	-------------------------------------------------------------------------------------	
	
		-- Log
		EXEC logInfo @logID, 'Update #intCustomer with change'	
	
	BEGIN TRY

		BEGIN TRANSACTION

		MERGE INTO tdw.dbo.tinternalCustomer WITH (TABLOCKX) t
		USING #intCustomer s
		ON (s.sk_customer = t.sk_customer
			AND t.effectiveDateEndCET IS NULL
			AND t.effectiveDateBeginCET = @breakdate)
		WHEN MATCHED AND ( -- SCD2 attribute check
			s.sk_registerBrand <> t.sk_registerBrand
			OR s.sk_customerState <> t.sk_customerState	
			OR s.customerMarketID <> t.customerMarketID
			OR s.sk_affiliate <> t.sk_affiliate
			OR s.sk_zbanCode <> t.sk_zbanCode
			OR s.sk_affiliateStrikeBack <> t.sk_affiliateStrikeBack
			OR s.sk_zbanCodeStrikeBack <> t.sk_zbanCodeStrikeBack
			OR ISNULL(s.customerSportsbookRiskGeneralLimit, 1) <> ISNULL(t.customerSportsbookRiskGeneralLimit, 1) 
			OR s.sk_customerType <> t.sk_customerType
			OR ISNULL(s.selfExcludedEndDate,GETUTCDATE()) <> ISNULL(t.selfExcludedEndDate,GETUTCDATE())
		)
		THEN UPDATE SET 
				t.sk_registerBrand = s.sk_registerBrand
				,t.sk_customerState	= s.sk_customerState
				,t.customerMarketID = s.customerMarketID
				,t.sk_affiliate = s.sk_affiliate
				,t.sk_zbanCode = s.sk_zbanCode
				,t.sk_affiliateStrikeBack = s.sk_affiliateStrikeBack
				,t.sk_zbanCodeStrikeBack = s.sk_zbanCodeStrikeBack
				,t.sk_customerType = s.sk_customerType
				,t.customerFirstName = s.customerFirstName
				,t.customerLastName = s.customerLastName
				,t.customerAddress = s.customerAddress
				,t.customerZipCode = s.customerZipCode
				,t.customerCity = s.customerCity
				,t.countryCode = s.countryCode
				,t.customerPhone = s.customerPhone
				,t.customerEmail = s.customerEmail
				,t.EmailVerified =	CASE 
										WHEN t.customerEmail != s.customerEmail THEN 0
										ELSE t.EmailVerified
									END
				,t.customerBirthDate = s.customerBirthDate
				,t.customerGender = s.customerGender
				,t.customerCellPhone = s.customerCellPhone
				,t.PhoneNumberVerified =	CASE 
												WHEN t.customerCellPhone != s.customerCellPhone THEN 0
												ELSE t.PhoneNumberVerified
											END
				,t.currencyCode = s.currencyCode
				,t.referralCode = s.referralCode
				,t.[AcceptEmailOffers] = s.[AcceptEmailOffers]
				,t.[AcceptSmsOffers] = s.[AcceptSmsOffers]	
				,t.customerPwdChecksum = s.customerPwdChecksum
				,t.customerSportsbookRiskGeneralLimit = s.customerSportsbookRiskGeneralLimit 
				,t.affiliateStrikeBackTagDate = s.affiliateStrikeBackTagDate
				,t.selfExcludedEndDate = s.selfExcludedEndDate
				,t.changeDate = GETUTCDATE()
				,t.[acceptTeleSalesOffers] = s.[acceptTeleSalesOffers]	
				OUTPUT $ACTION INTO @rowCount;

		COMMIT TRANSACTION
            
	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	
		ROLLBACK TRANSACTION
    
		RETURN;
	END CATCH;
			
					-- log message
			 SELECT @insertCount=[INSERT],
					@updateCount=[UPDATE],
					@deleteCount=[DELETE]
				FROM (SELECT mergeAction,1 rows       
					  FROM   @rowCount ) c
							 PIVOT(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE],[DELETE])) as pvt

			SET @msg = 'Handling more than one change in the SCD attributes during a day, updated ' + LTRIM(ISNULL(@updateCount,0)) + ' rows in tinernalCustomer'
			SET @totalAffectedRows = ISNULL(@insertCount,0)+ISNULL(@updateCount,0)+ISNULL(@deleteCount,0)
			EXEC logMerge @logID, @msg, @totalAffectedRows	
			
				-- Horrible workaround below to handle the fact that
				-- lazy MS engineers did not implement support for
				-- foreign keys when you have a nested insert. So,
				-- we have to use yet another temp table...

	-------------------------------------------------------------------------------------
	-- End handling more than one change per day
	--=============================================	===================	


	--=============================================	===================	
	-- Create SCD2 rows
	-------------------------------------------------------------------------------------
	BEGIN TRY

		BEGIN TRANSACTION
				-- Do SCD
				INSERT #scdUser
				(
					sk_customer
					,effectiveDateBeginCET
					,effectiveDateEndCET
					,isCurrent
					,sk_affiliate
					,sk_zbanCode
					,sk_affiliateStrikeBack
					,sk_zbanCodeStrikeBack
					,customerMarketID
					,sk_registerBrand
					,sk_customerState
					,customerFirstName
					,customerLastName
					,customerAddress
					,customerZipCode
					,customerCity
					,countryCode
					,customerPhone
					,customerEmail
					,customerBirthDate
					,customerGender
					,customerCellPhone
					,currencyCode
					,referralCode
					,[AcceptEmailOffers]
					,[AcceptSmsOffers]
					,customerPwdChecksum
					,customerSportsbookRiskGeneralLimit 
					,sk_customerType
					,affiliateStrikeBackTagDate
					,selfExcludedEndDate
					,regDate
					,isBonusAbuser
					,acceptTeleSalesOffers
					,PhoneNumberVerified
					,EmailVerified
				)
				SELECT		
					scd2.sk_customer
					,scd2.target_effectiveDateEnd   -- Note this is to set the begin date to the previous rows end date
					,NULL
					,1
					,scd2.sk_affiliate
					,scd2.sk_zbanCode
					,scd2.sk_affiliateStrikeBack
					,scd2.sk_zbanCodeStrikeBack
					,scd2.customerMarketID
					,scd2.sk_registerBrand	
					,scd2.sk_customerState	
					,scd2.customerFirstName
					,scd2.customerLastName
					,scd2.customerAddress
					,scd2.customerZipCode
					,scd2.customerCity
					,scd2.countryCode
					,scd2.customerPhone
					,scd2.customerEmail
					,scd2.customerBirthDate
					,scd2.customerGender
					,scd2.customerCellPhone
					,scd2.currencyCode
					,scd2.referralCode
					,scd2.[AcceptEmailOffers]
					,scd2.[AcceptSmsOffers]				
					,scd2.customerPwdChecksum
					,scd2.customerSportsbookRiskGeneralLimit
					,scd2.sk_customerType
					,scd2.affiliateStrikeBackTagDate
					,scd2.selfExcludedEndDate
					,GETUTCDATE()
					,scd2.isBonusAbuser
					,scd2.acceptTeleSalesOffers
					,scd2.PhoneNumberVerified
					,scd2.EmailVerified
				FROM (
				
				MERGE INTO tdw.dbo.tinternalCustomer WITH (TABLOCKX) t
				USING #intCustomer s
				ON (s.sk_customer = t.sk_customer
					AND t.effectiveDateEndCET IS NULL
					AND t.effectiveDateBeginCET <> @breakdate)
				WHEN MATCHED AND ( -- SCD2 attribute check
					t.sk_registerBrand <> s.sk_registerBrand
					OR t.sk_customerState <> s.sk_customerState
					OR s.customerMarketID <> t.customerMarketID
					OR s.sk_affiliate <> t.sk_affiliate
					OR s.sk_zbanCode <> t.sk_zbanCode
					OR s.sk_affiliateStrikeBack <> t.sk_affiliateStrikeBack
					OR s.sk_zbanCodeStrikeBack <> t.sk_zbanCodeStrikeBack
					OR COALESCE(s.customerSportsbookRiskGeneralLimit, 1) <> COALESCE(t.customerSportsbookRiskGeneralLimit, 1) 
					OR s.sk_customerType <> t.sk_customerType
					OR ISNULL(s.selfExcludedEndDate,GETUTCDATE()) <> ISNULL(t.selfExcludedEndDate,GETUTCDATE())
					OR ISNULL(s.isBonusAbuser, 0) <> t.isBonusAbuser
				)
				THEN UPDATE SET 
						t.effectiveDateEndCET = @breakdate
						,t.isCurrent = 0
						,t.changeDate = GETUTCDATE()
				OUTPUT 
					$action
					,s.sk_customer
					,s.sk_affiliate
					,s.sk_zbanCode
					,s.sk_affiliateStrikeBack
					,s.sk_zbanCodeStrikeBack
					,s.customerMarketID
					,s.sk_registerBrand	
					,s.sk_customerState		 
					,s.customerFirstName
					,s.customerLastName
					,s.customerAddress
					,s.customerZipCode
					,s.customerCity
					,s.countryCode
					,s.customerEmail
					,s.customerBirthDate
					,s.customerGender
					,s.customerPhone
					,s.customerCellPhone
					,s.currencyCode
					,s.referralCode
					,s.[AcceptEmailOffers]
					,s.[AcceptSmsOffers]				
					,s.customerPwdChecksum
					,s.customerSportsbookRiskGeneralLimit
					,s.sk_customerType
					,s.affiliateStrikeBackTagDate
					,s.selfExcludedEndDate
					,s.regDate
					,inserted.effectiveDateEndCET
					,s.isBonusAbuser
					,s.acceptTeleSalesOffers
					,inserted.PhoneNumberVerified
					,inserted.EmailVerified
					) AS scd2
					(
						output_action, 
						sk_customer, 
						sk_affiliate, 
						sk_zbanCode,
						sk_affiliateStrikeBack,
						sk_zbanCodeStrikeBack,
						customerMarketID,
						sk_registerBrand,
						sk_customerState,
						customerFirstName, 
						customerLastName, 
						customerAddress, 
						customerZipCode, 
						customerCity,
						countryCode, 
						customerEmail, 
						customerBirthDate, 
						customerGender, 
						customerPhone, 
						customerCellPhone,
						currencyCode, 
						referralCode,
						AcceptEmailOffers, 
						AcceptSmsOffers, 
						customerPwdChecksum,	
						customerSportsbookRiskGeneralLimit,
						sk_customerType,	
						affiliateStrikeBackTagDate,		
						selfExcludedEndDate,	
						regDate, 
						target_effectiveDateEnd,
						isBonusAbuser,
						acceptTeleSalesOffers,
						PhoneNumberVerified,
						EmailVerified
					) 
					WHERE 
						scd2.output_action = 'UPDATE'
						AND scd2.target_effectiveDateEnd IS NOT NULL;
	
				-- Log
				EXEC logInfo @logID, 'End update #intCustomer with change'
			


	-------------------------------------------------------------------------------------
	-- End creating SCD2 rows
	--=============================================	===================	


	--=============================================	===================	
	-- Insert SCD2 rows into tinternalCustomer
	-------------------------------------------------------------------------------------

				-- Log
				EXEC logInfo @logID, 'insert data into tinternalCustomer'
		

				
				INSERT TDW.dbo.tinternalCustomer WITH (TABLOCKX) (
					sk_customer, 
					effectiveDateBeginCET, 
					effectiveDateEndCET, 
					isCurrent, 
					sk_registerBrand, 
					sk_customerState,
					sk_affiliate,
					sk_zbanCode,
					sk_affiliateStrikeBack,
					sk_zbanCodeStrikeBack,
					customerMarketID,
					currencyCode, 
					customerFirstName, 
					customerLastName,
					customerAddress, 
					customerZipCode, 
					customerCity, 
					countryCode, 
					customerPhone,
					customerCellPhone, 
					customerEmail, 
					customerBirthDate,
					customerGender, 
					AcceptEmailOffers, 
					AcceptSmsOffers,  
					customerPwdChecksum,  
					customerSportsbookRiskGeneralLimit,  
					sk_customerType,
					referralCode,
					affiliateStrikeBackTagDate,  
					selfExcludedEndDate,          
					changeDate, 
					regDate,
					isBonusAbuser,
					acceptTeleSalesOffers,
					PhoneNumberVerified,
					EmailVerified)                         
				SELECT        
					sk_customer, 
					effectiveDateBeginCET, 
					effectiveDateEndCET, 
					isCurrent, 
					sk_registerBrand, 
					sk_customerState,
					sk_affiliate,
					sk_zbanCode,
					sk_affiliateStrikeBack,
					sk_zbanCodeStrikeBack,
					customerMarketID,
					currencyCode, 
					customerFirstName, 
					customerLastName, 
					customerAddress, 
					customerZipCode, 
					customerCity, 
					countryCode, 
					customerPhone,
					customerCellPhone, 
					customerEmail, 
					customerBirthDate, 
					customerGender, 
					AcceptEmailOffers, 
					AcceptSmsOffers,   
					customerPwdChecksum,    
					customerSportsbookRiskGeneralLimit, 
					sk_customerType,   
					referralCode,
					affiliateStrikeBackTagDate,      
					selfExcludedEndDate,  
					changeDate, 
					regDate,
					ISNULL(isBonusAbuser, 0) AS isBonusAbuser,
					acceptTeleSalesOffers,
					PhoneNumberVerified,
					EmailVerified
				FROM #scdUser
										
				SET @scd2ChangedUsers = @@ROWCOUNT

				COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	
		ROLLBACK TRANSACTION
    
		RETURN;
	END CATCH;			
				-- Log
				EXEC logInfo @logID, 'End insert data into tinternalCustomer'

	-------------------------------------------------------------------------------------
	-- End adding new SCD2 rows
	--=============================================	===================	


	--=============================================	===================	
	-- Merge SCD1 changes and insert new rows
	-------------------------------------------------------------------------------------

				-- Log
				EXEC logInfo @logID, 'Start SCD1 merge'

	BEGIN TRY

	BEGIN TRANSACTION

				-- Changes to SCD1 attributes	
				MERGE INTO tdw.dbo.tinternalCustomer WITH (TABLOCKX) t
				USING #intCustomer s
				ON (s.sk_customer = t.sk_customer				
					AND t.effectiveDateEndCET IS NULL)
				WHEN MATCHED AND ( -- No change in the scd2 attributes
					s.sk_registerBrand = t.sk_registerBrand
					AND s.customerMarketID = t.customerMarketID
					AND s.sk_affiliate = t.sk_affiliate
					AND s.sk_zbanCode = t.sk_zbanCode
					AND s.sk_affiliateStrikeBack = t.sk_affiliateStrikeBack
					AND s.sk_zbanCodeStrikeBack = t.sk_zbanCodeStrikeBack
					AND s.sk_customerType = t.sk_customerType -- added 20100118, maja
					AND ISNULL(s.selfExcludedEndDate,GETUTCDATE()) = ISNULL(t.selfExcludedEndDate,GETUTCDATE())
					AND COALESCE(s.customerSportsbookRiskGeneralLimit, 1) = COALESCE(t.customerSportsbookRiskGeneralLimit, 1) 
					) 
					AND				
					(COALESCE(s.customerfirstName, '') <> COALESCE(t.customerFirstName, '')
					OR COALESCE(s.customerLastName, '') <> COALESCE(t.customerLastName, '')
					OR COALESCE(s.customerAddress, '') <> COALESCE(t.customerAddress, '')
					OR COALESCE(s.customerZipCode, '') <> COALESCE(t.customerZipCode, '')
					OR COALESCE(s.customerCity, '') <> COALESCE(t.customerCity, '')
					OR s.countryCode <> t.countryCode
					OR COALESCE(s.customerPhone, '') <> COALESCE(t.customerPhone, '')
					OR COALESCE(s.customerEmail, '') <> COALESCE(t.customerEmail, '')
					OR COALESCE(s.customerBirthDate, '1900-01-01') <> COALESCE(t.customerBirthDate, '1900-01-01')
					OR COALESCE(s.customerGender, '') <> COALESCE(t.customerGender, '')
					OR COALESCE(s.customerCellPhone, '') <> COALESCE(t.customerCellPhone, '')
					OR s.currencyCode <> t.currencyCode				
					OR COALESCE(s.referralCode, '') <> COALESCE(t.referralCode, '')
					OR COALESCE(s.AcceptEmailOffers, -1) <> COALESCE(t.AcceptEmailOffers, -1)
					OR COALESCE(s.AcceptSmsOffers, -1) <> COALESCE(t.AcceptSmsOffers, -1)				
					OR COALESCE(s.affiliateStrikeBackTagDate, '1900-01-01') <> COALESCE(t.affiliateStrikeBackTagDate, '1900-01-01')
					OR s.customerPwdChecksum <> t.customerPwdChecksum
					OR s.sk_customerState <> t.sk_customerState
					OR (s.customerSportsbookRiskGeneralLimit IS NOT NULL AND t.customerSportsbookRiskGeneralLimit IS NULL)
						-- SB risk limit is here again, we only see it as a SCD2 change if it goes from a set value to another value
						-- If we go from null to 1, we see it as an scd1 change
					OR COALESCE(s.acceptTeleSalesOffers, -1) <> COALESCE(t.acceptTeleSalesOffers, -1)-- using COALESCE instead of ISNULL because ISNULL(value,-1) will return always 1 when NULL
				)
				THEN UPDATE SET 				
					t.customerFirstName = s.customerFirstName
					,t.customerLastName = s.customerLastName
					,t.customerAddress = s.customerAddress
					,t.customerZipCode = s.customerZipCode
					,t.customerCity = s.customerCity
					,t.countryCode = s.countryCode
					,t.sk_customerState = s.sk_customerState
					,t.customerPhone = s.customerPhone
					,t.customerEmail = s.customerEmail 
					,t.EmailVerified =	CASE 
											WHEN t.customerEmail != s.customerEmail THEN 0
											ELSE t.EmailVerified
										END
					,t.customerBirthDate = s.customerBirthDate
					,t.customerGender = s.customerGender
					,t.customerCellPhone = s.customerCellPhone
					,t.PhoneNumberVerified =	CASE 
													WHEN t.customerCellPhone != s.customerCellPhone THEN 0
													ELSE t.PhoneNumberVerified
												END
					,t.currencyCode = s.currencyCode
					,t.referralCode = s.referralCode
					,t.AcceptEmailOffers = s.AcceptEmailOffers
					,t.AcceptSmsOffers = s.AcceptSmsOffers				
					,t.affiliateStrikeBackTagDate = s.affiliateStrikeBackTagDate
					,t.customerPwdChecksum = s.customerPwdChecksum
					,t.customerSportsbookRiskGeneralLimit = s.customerSportsbookRiskGeneralLimit
					,t.changeDate = GETUTCDATE()
					,t.acceptTeleSalesOffers = s.acceptTeleSalesOffers	
				--WHEN NOT MATCHED -- Insert new users
				--	THEN INSERT (
				--		sk_customer
				--		,effectiveDateBeginCET
				--		,isCurrent
				--		,sk_affiliate
				--		,sk_zbanCode
				--		,customerMarketID
				--		,sk_affiliateStrikeBack
				--		,sk_zbanCodeStrikeBack
				--		,sk_registerBrand
				--		,sk_customerState
				--		,customerFirstName
				--		,customerLastName
				--		,customerAddress
				--		,customerZipCode
				--		,customerCity
				--		,countryCode
				--		,customerPhone
				--		,customerEmail
				--		,customerBirthDate
				--		,customerGender
				--		,customerCellPhone
				--		,currencyCode
				--		,referralCode
				--		,AcceptEmailOffers
				--		,AcceptSmsOffers					
				--		,customerPwdChecksum
				--		,customerSportsbookRiskGeneralLimit 
				--		,affiliateStrikeBackTagDate
				--		,selfExcludedEndDate
				--		, sk_customerType -- added 20100118, maja
				--		,regDate
				--	)
				--	VALUES (
				--		s.sk_customer
				--		,s.effectiveDateBeginCET
				--		,s.isCurrent
				--		,s.sk_affiliate
				--		,s.sk_zbanCode
				--		,s.customerMarketID
				--		,s.sk_affiliateStrikeBack
				--		,s.sk_zbanCodeStrikeBack
				--		,s.sk_registerBrand	
				--		,s.sk_customerState
				--		,s.customerFirstName
				--		,s.customerLastName
				--		,s.customerAddress
				--		,s.customerZipCode
				--		,s.customerCity
				--		,s.countryCode
				--		,s.customerPhone
				--		,s.customerEmail
				--		,s.customerBirthDate
				--		,s.customerGender
				--		,s.customerCellPhone
				--		,s.currencyCode
				--		,s.referralCode
				--		,s.AcceptEmailOffers
				--		,s.AcceptSmsOffers					
				--		,s.customerPwdChecksum
				--		,s.customerSportsbookRiskGeneralLimit 
				--		,s.affiliateStrikeBackTagDate
				--		,s.selfExcludedEndDate
				--		,s.sk_customerType
				--		,s.regDate
				--	)
					OUTPUT $ACTION INTO @rowCount;

		COMMIT TRANSACTION
                
	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	
		ROLLBACK TRANSACTION
    
		RETURN;
	END CATCH;
	
		-- log message
			 SELECT @insertCount=[INSERT],
					@updateCount=[UPDATE],
					@deleteCount=[DELETE]
				FROM (SELECT mergeAction,1 rows       
					  FROM   @rowCount ) c
							 PIVOT(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE],[DELETE])) as pvt

			SET @msg = 'Inserted ' + LTRIM(ISNULL(@insertCount,0)) + ' rows, (SCD1) updated ' + LTRIM(ISNULL(@updateCount,0)) + ' rows in tinernalCustomer'
			SET @totalAffectedRows = ISNULL(@insertCount,0)+ISNULL(@updateCount,0)+ISNULL(@deleteCount,0)
			EXEC logMerge @logID, @msg, @totalAffectedRows	

			
					SET @changedUsers = @updateCount			
				
					-- Log
					EXEC logInfo @logID, 'End SCD1 merge'

	-------------------------------------------------------------------------------------
	-- End adding new rows and merging SCD1 changes
	--=============================================	===================				
									
	-- log message
	SET @msg = 'Merged NewUsers=' + LTRIM(ISNULL(@newUsers, 0)) + ' in tinternalUser'
	EXEC logInsert @logID, @msg, @newUsers

	SET @msg = 'ChangedUsers (scd2)=' + LTRIM(ISNULL(@scd2ChangedUsers, 0))  + ' in tinternalUser'
	EXEC logInsert @logID, @msg, @scd2ChangedUsers

	SET @msg = 'ChangedUsers(scd1)=' + LTRIM(ISNULL(@changedUsers, 0)) + ' in tinternalUser'
	EXEC logInsert @logID, @msg, @changedUsers


	DROP TABLE #intCustomer, #scdUser

	-- log end
	EXEC logStop @logID

	RETURN 0

END
GO
