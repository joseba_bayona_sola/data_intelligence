
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [dbo].[stgtdw_tcustomer_customer] 
	@packageRunID int = NULL,
	@sourceTimeZone VARCHAR(50),
	@biWarehouseTimeZone VARCHAR(50)
as
set nocount on

/*
Created: 2012-06-19 wiol. Broken out of stgtdw_tcustomer

Description: Do not run alone! Should be run by stored procedure stgtdw_tcustomer. 
			 Moves user info data from stage to TDW. Updates existing users or creates new users. 
			 Reads transform table transform_customerMetaInfo to get metadata

			 Users are tested for a few known errors. If errors are encountered an error message is appended and user is placed in 
			 error table to be fixed manually. 
History:
2012-11-13: vlca added locking mechanism during Merge statement 
2013-01-28: rama added DK temporary accounts
2013-03-07: stca added timezone functionality
*/

-- log start
declare @logID int ,@msg varchar(512), @procName varchar(50) = object_name(@@procid), @rows int
exec ProcessRepository.dbo.logStart @procName, @packageRunID, @logID output

-- Error handling variables
DECLARE @ErrorMessage NVARCHAR(4000);
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;

DECLARE @totalAffectedRows INT,@insertCount INT, @updateCount INT, @deleteCount INT
DECLARE @rowCount TABLE(mergeAction NVARCHAR(10))

declare @newUsers int
		,@newMigratedUsers int
select	@newUsers  = 0
		,@newMigratedUsers = 0

-- load meta data
	select *
	into #metaInfo2
	from stage..transform_customerMetaInfo t
	
--=============================================	===================
-- Begin migration handling code. Only affects users that already exist in TDW and that have now been migrated by platform.
-------------------------------------------------------------------------------------	
	
	-- Log
	exec ProcessRepository.dbo.logInfo @logID, 'Start handling premigrated users'
	
	BEGIN TRY
		-- Create temporary work table for newly migrated not tagged customers
		select 
				tgc.customerID
				, vb.sk_merchant
				,ms.migrationSourceID
				,processRepository.dbo.fnConvertTime(@sourceTimeZone, @biWarehouseTimeZone, tgc.registrationDate) registrationDate
				, mi.pre_migration_userid
		INTO #MigratedCustomers
		from stage.dbo.tch_getCustomers tgc
			inner join tdw.dbo.tmerchant vb with (nolock) on tgc.merchantID = vb.merchantID 
			left join #metaInfo2 mi with (nolock) on 	mi.customerid = tgc.customerID 
										and mi.sk_merchant = vb.sk_merchant
			left join TDW..tcustomer trg with (nolock) on trg.pre_migration_userid = mi.pre_migration_userid 
										and trg.sk_merchant = mi.sk_merchant
			join tdw.dbo.tmigrationSource ms with (nolock) on ms.migrationSourceID = mi.migrationSourceID --joins away users without migration info
		where	isnull(mi.pre_migration_userid,0) <> 0
				AND isnull(trg.src_customerId,0) = 0 -- No source id yet 
				AND CAST(tgc.customerId AS VARCHAR(50)) + '_' + CAST(vb.sk_merchant AS VARCHAR(50)) NOT IN ('434361_1', '434235_1','434365_1') -- vlca added to eliminate customer until platform issue is solved

		
		-- Add customer id to premigrated customers and change migration status to "in progress"
		MERGE INTO TDW..tcustomer trg
		USING #MigratedCustomers src
		ON (trg.pre_migration_userid = src.pre_migration_userid and trg.sk_merchant = src.sk_merchant)
		WHEN MATCHED AND isnull(trg.src_customerId,0) = 0
		THEN 
			UPDATE SET 
				trg.src_customerId = src.customerID
				,trg.migrationSourceID = src.migrationSourceID
				,trg.migrationDateGMT = src.registrationDate
				-- Set migration as complete!
				,trg.migrationStatusID = 1
				,trg.changeDate = getutcdate();
		
		Set @newMigratedUsers = @@ROWCOUNT
	END TRY
	BEGIN CATCH
		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
		return;
	END CATCH;
	
	-- Log
	exec ProcessRepository.dbo.logInfo @logID, 'End handling premigrated users'

-------------------------------------------------------------------------------------
-- End handlling premigrated users
--=============================================	===================
	
	

--=============================================	===================	
-- Create new (or update) customers in tcustomer
-------------------------------------------------------------------------------------

	-- Log
	exec ProcessRepository.dbo.logInfo @logID, 'Begin - Load Error table transactions'

	--create temp table
	create table #src
	(customerId int, merchantId int, sk_merchant int, registerBrandId int, migrationsourceID int, registrationDate datetime, externalCustomerID bigint, pre_migration_userid int
	,twoLetterISOLanguageName nvarchar(50), regDate datetime, customerGuid uniqueidentifier, migrationStatusID int, errorMessage nvarchar(100), errorDate datetime)

	--place customers in temp table
	insert into #src
	select 
		tgc.customerID
		, tgc.merchantId
		, coalesce(tm.sk_merchant, -1) as sk_merchant	
		, tgc.registerBrandId as registerBrandId
		, isnull(ms.migrationSourceID,-1) as migrationSourceID
		, processRepository.dbo.fnConvertTime(@sourceTimeZone, @biWarehouseTimeZone, tgc.registrationDate)
		, tgc.externalCustomerID
		, mi.pre_migration_userid as pre_migration_userid
		, tgc.twoLetterISOLanguageName
		, tgc.regDate
		, tgc.customerGuid
		-- Set initial status for customers missing premigration data
		, case when mi.pre_migration_userid > 0 then 2 else 0 end as migrationStatusID
		,null as errorMessage
		,null as errorDate
	from stage.dbo.tch_getCustomers tgc with (nolock)
		left join tdw.dbo.tmerchant tm with (nolock) on tgc.merchantID = tm.merchantID 
		left join #metaInfo2 mi with (nolock) on mi.customerid = tgc.customerID and mi.sk_merchant = tm.sk_merchant
		left join tdw.dbo.tmigrationSource ms with (nolock) on ms.migrationSourceID = mi.migrationSourceID

	--add customers from error table
	insert into #src
	select 
		err.customerID
		, err.merchantId
		, coalesce(vb.sk_merchant, -1) as sk_merchant	
		, err.registerBrandId
		, coalesce(ms.migrationSourceID, err.meta_migrationSourceID, -1) as migrationSourceID
		, err.registrationDate
		, err.externalCustomerID
		, coalesce(mi.pre_migration_userid, err.meta_pre_migration_userid) as pre_migration_userid
		, err.twoLetterISOLanguageName
		, err.regDate
		, err.customerGuid
		-- Set initial status for customers missing premigration data
		, case when  coalesce(mi.pre_migration_userid, err.meta_pre_migration_userid) > 0 then 2 else 0 end as migrationStatusID
		,null as errorMessage
		,null as errorDate
	from tdw..error_tch_getCustomers err 
		left join tdw.dbo.tmerchant vb with (nolock) on err.merchantID = vb.merchantID 
		left join #metaInfo2 mi with (nolock) on mi.customerid = err.customerID and mi.sk_merchant = vb.sk_merchant
		left join tdw.dbo.tmigrationSource ms with (nolock) on ms.migrationSourceID = mi.migrationSourceID
	where not exists (select * from #src s
						where s.customerId = err.customerId and s.merchantId = err.merchantId)

	-- Log
	exec ProcessRepository.dbo.logInfo @logID, 'End - Load Error table transactions'

--=============================================	===================	
-- Section for pre-defined tests

-- This is a test to see that no user has the same GUID as someone else. Should not happen! 	
	update t
	set t.errorMessage = 'User GUID is already taken'
		,t.errorDate = getutcdate()
	from #src t
	join TDW..tmerchant tm with (nolock) on tm.merchantID = t.merchantId
	left join TDW..tcustomer tc with (nolock) on tc.customerGuid = t.customerGuid  
				and not (tc.src_customerId = t.customerID and tc.sk_merchant = tm.sk_merchant) 
	where tc.sk_customer is not null

-- This is a test to see that no users have changed customerGuid. Should not happen!	
	update t
	set t.errorMessage = 'User has changed GUID, customerid match'
		,t.errorDate = getutcdate()
	from #src t
	join TDW..tcustomer tc with (nolock) on src_customerId = t.customerId 
	join TDW..tmerchant tm with (nolock) on tc.sk_merchant = tm.sk_merchant and tm.merchantID = t.merchantId
	where t.customerGuid <> tc.customerGuid

-- This is another test to see that no users have changed customerGuid. Should not happen!	
	update t
	set t.errorMessage = 'User has changed GUID, premigration match'
		,t.errorDate = getutcdate()
	from #src t
	join TDW..tcustomer tc with (nolock) on tc.pre_migration_userid = t.pre_migration_userid 
	join TDW..tmerchant tm with (nolock) on tc.sk_merchant = tm.sk_merchant and tm.merchantID = t.merchantId
	where t.customerGuid <> tc.customerGuid
	AND t.pre_migration_userid IS NOT NULL
    
--This is a test to see that no users have changed pre-mig-userid. Should not happen!	
	update t
	set t.errorMessage = 'User has changed pre-migration user id'
		,t.errorDate = getutcdate()
	from #src t
	join TDW..tcustomer tc with (nolock) on src_customerId = t.customerId 
	join TDW..tmerchant tm with (nolock) on tc.sk_merchant = tm.sk_merchant and tm.merchantID = t.merchantId
	where coalesce(t.pre_migration_userid,0) <> coalesce(tc.pre_migration_userid,0) 

--This is a test to see that no user is tied to an invalid merchant. Also stopped by transform stage table
	update t
	set t.errorMessage = 'User has unknown merchant'
		,t.errorDate = getutcdate()
	from #src t
	where t.sk_merchant = -1

--This is a test to see that no GUID is null
	update t
	set t.errorMessage = 'GUID is null'
		,t.errorDate = getutcdate()
	from #src t
	where t.customerGuid is NULL
 
 --This is a test to see if the customer is new
	update s
	set s.errorMessage = 'Customer should be inserted by NServiceBus'
		,s.errorDate = getutcdate()
	from #src s
	LEFT JOIN TDW..tcustomer t
			ON t.src_customerId = s.customerID and t.sk_merchant = s.sk_merchant
	where t.sk_customer is NULL

--=============================================	===================	

BEGIN TRY
	
	--create a table to keep track of those customers that were merged sucessfully
	create table #mergedSuccessfully (
	customerID int,
	merchantID int,
	outputAction varchar(50) --for debugging
)
	
	BEGIN TRANSACTION
					
	-- Merge new and updated customers
	insert	#mergedSuccessfully (customerID, merchantID, outputAction)
	select customerID, merchantID, outputAction
	from (

			MERGE INTO TDW..tcustomer WITH (tablockx) t
			USING #src s
			ON (t.src_customerId = s.customerID and t.sk_merchant = s.sk_merchant)
			--WHEN NOT MATCHED  and s.errorMessage is null
			--INSERT (src_customerID, sk_merchant, customerCreateDateGMT, externalCustomerID, migrationSourceID, pre_migration_userid, migrationStatusID,registrationLanguageCode, customerGuid, regDate)
			--values(s.customerID, s.sk_merchant, s.registrationDate,s.externalCustomerID, s.migrationSourceID, s.pre_migration_userid, s.migrationStatusID,twoLetterISOLanguageName, s.customerGuid, s.regDate)
			WHEN MATCHED AND 
				(	
					(s.externalCustomerID <> isnull(t.externalCustomerID, 0) 
					OR s.migrationSourceID <> t.migrationSourceID 
					OR isnull(t.pre_migration_userid,0) <> s.pre_migration_userid 
					OR (s.customerGuid <> isnull(t.customerGuid, newID()) AND t.customerGuid is null) -- CustomerGuid shall never be changed once it got a value.
					) AND
					s.errorMessage is null
				)
			THEN UPDATE SET 		
				t.externalCustomerID = s.externalCustomerID,
				t.migrationSourceID = s.migrationSourceID,
				t.pre_migration_userid = s.pre_migration_userid,
				t.customerGuid = s.customerGuid,
				t.changeDate = getutcdate()
			OUTPUT $ACTION 
					,s.customerID
					,s.merchantID

			) AS mergeOutput 
			(outputAction, 
			 customerID,
			 merchantID
			 )
		where
		outputAction IN ('INSERT', 'UPDATE')
		
		-- save inserted/updated rowcount		
		declare @mergedrows int 
		select @mergedrows = count(*)
		from #mergedSuccessfully	

		COMMIT TRANSACTION
        
---- Populate error table
		-- Update fixed customers in the error table, 
		-- and add new ones (only customers with error message)
		create table #insertedError (outputAction varchar(50))
		insert into #insertedError
		select outputAction
		from (
			MERGE INTO tdw..error_tch_getCustomers dst
			USING #src src
			ON (src.customerID = dst.customerID and src.merchantID = dst.merchantID)
			WHEN MATCHED 
			THEN UPDATE SET
			   dst.registrationDate = src.registrationDate
			   ,dst.twoLetterISOLanguageName = src.twoLetterISOLanguageName
			   ,dst.externalCustomerID = src.externalCustomerID
			   ,dst.customerGuid = src.customerGuid 
			   ,dst.meta_pre_migration_userid = src.pre_migration_userid
			   ,dst.meta_migrationSourceID = src.migrationSourceID
			   ,dst.errorMessage = src.errorMessage
			   ,dst.errorDate = src.errorDate
			   ,dst.changeDate = getutcdate()
			WHEN NOT MATCHED BY TARGET and src.errorMessage is not null
			THEN
			INSERT
				(	customerId
					, merchantId
					, registrationDate
					, twoLetterISOLanguageName
					, externalCustomerID
					, customerGuid
					, meta_pre_migration_userid
					, meta_migrationSourceID
					, errorMessage
					, errorDate
					, regDate)
			Values
				(	src.customerId
					, src.merchantId
					, src.registrationDate
					, src.twoLetterISOLanguageName
					, src.externalCustomerID
					, src.customerGuid
					, src.pre_migration_userid
					, src.migrationSourceID
					, src.errorMessage
					, src.errorDate
					, getutcdate()
			
				)
			--WHEN NOT MATCHED BY SOURCE THEN DELETE --DELETE is done in the sp '[dbo].[stgtdw_tcustomer]'
			OUTPUT $ACTION
		) AS mergeOutput
			(outputAction)
		  where outputAction = 'INSERT'


END TRY
BEGIN CATCH
	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
			   @ErrorSeverity, -- Severity.
			   @ErrorState -- State.
			   );
	
	ROLLBACK TRANSACTION
    
	return;
END CATCH;

--log if there is a new customer inserted into the error table. 
declare @errors int = (select count(*) from #insertedError)
if ( @errors > 0)
begin 
	set @msg = 'inserted ' + ltrim(isnull(@errors,0)) + ' rows in error_tch_getCustomers'
	exec ProcessRepository.dbo.logInfo @logID, @msg --, @mergedrows
end 

-- Log customer update
set @msg = 'Merged ' + ltrim(isnull(@mergedrows,0)) + ' rows in tcustomer'
exec ProcessRepository.dbo.logMerge @logID, @msg, @mergedrows
				
-- Log
exec ProcessRepository.dbo.logInfo @logID, 'Done inserting rows in TCustomer'

drop table #src
drop table #insertedError
drop table #mergedSuccessfully


-- log end
exec ProcessRepository.dbo.logStop @logID

return 0

GO
