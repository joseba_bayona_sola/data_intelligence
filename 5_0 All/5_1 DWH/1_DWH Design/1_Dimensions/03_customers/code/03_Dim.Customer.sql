
[effectiveDateBeginCET] [int] NOT NULL,
[effectiveDateEndCET] [int] NULL,
[isCurrent] [bit] NOT NULL,

CREATE TABLE [Dim].[Customer]
(
[mk_Customer] [int] NOT NULL,
[regDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_Customer_regDate] DEFAULT (getutcdate()),
[changeDate] [datetime2] (0) NOT NULL CONSTRAINT [DF_Customer_changeDate] DEFAULT (getutcdate()),
[mk_CurrentCustomer] [int] NOT NULL,
[effectiveDateBeginCET] [int] NOT NULL,
[effectiveDateEndCET] [int] NULL,
[isCurrent] [bit] NOT NULL,
[customerGuid] [uniqueidentifier] NULL,
[src_customerId] [int] NULL,
[globalUserId] [int] NULL,
[customerCreateDateGMT] [datetime2] (0) NULL,
[activateDate] [datetime2] (0) NULL,
[betssonGroupId] [int] NULL,
[customerLoginName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[customerFirstName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[customerLastName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[customerAddress] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerZipCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[customerCity] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerPhone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerCellPhone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerEmail] [varchar] (320) COLLATE Latin1_General_CI_AS NULL,
[customerBirthDate] [date] NULL,
[customerGender] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[socialSecurityNumber] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[referralCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[registrationSource] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[pre_migration_userid] [int] NULL,
[migrationDateGMT] [datetime2] (0) NULL,
[customerPwdChecksum] [int] NULL,
[externalCustomerID] [int] NULL,
[customerRegIP] [varchar] (45) COLLATE Latin1_General_CI_AS NULL,
[temporaryAccountExpiryDate] [datetime2] (0) NULL,
[temporaryAccountFreezeDate] [datetime2] (0) NULL,
[temporaryAccountUseFreezeAccount] [bit] NULL,
[temporaryAccountEncourageVerificationDate] [datetime2] (0) NULL,
[customerSportsbookRiskGeneralLimit] [real] NULL,
[selfExcludedEndDate] [datetime2] (0) NULL,
[isBonusAbuser] [bit] NOT NULL,
[acceptEmailOffers] [bit] NOT NULL,
[acceptSmsOffers] [bit] NOT NULL,
[acceptTeleSalesOffers] [bit] NULL,
[mk_CustomerStatus] [smallint] NOT NULL,
[mk_CustomerStatusName] [smallint] NOT NULL,
[customerStatusName] [varchar] (65) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_CustomerStatusGroup] [smallint] NOT NULL,
[customerStatusGroupName] [varchar] (35) COLLATE Latin1_General_CI_AS NOT NULL,
[emailValid] [bit] NOT NULL,
[mk_CustomerType] [smallint] NOT NULL,
[customerTypeName] [varchar] (25) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Language] [smallint] NOT NULL,
[languageName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[languageCode] [char] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Country] [smallint] NOT NULL,
[countryName] [varchar] (80) COLLATE Latin1_General_CI_AS NOT NULL,
[countryCode] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Subregion] [smallint] NOT NULL,
[subRegionName] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Region] [smallint] NOT NULL,
[regionName] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Currency] [smallint] NOT NULL,
[currencyName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[currencyCode] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_AcquisitionSource] [smallint] NOT NULL,
[acquisitionSourceName] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Channel] [smallint] NOT NULL,
[channelName] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_CustomerMarket] [smallint] NOT NULL,
[mk_CustomerMarketFriendlyName] [smallint] NOT NULL,
[customerMarketFriendlyName] [varchar] (60) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_BusinessChannel] [smallint] NOT NULL,
[businessChannelName] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_CustomerMarketName] [smallint] NOT NULL,
[customerMarketName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Brand] [smallint] NOT NULL,
[brandName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[smsBrandName] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Merchant] [smallint] NOT NULL,
[merchantName] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL,
[reportingCurrencyCode] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Partner] [smallint] NOT NULL,
[partnerName] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_Affiliate] [int] NOT NULL,
[affiliateCode] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[alAffiliateId] [int] NULL,
[clAffiliateId] [int] NULL,
[mk_AffiliateStatus] [smallint] NOT NULL,
[affiliateStatusName] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_AffiliateSystem] [smallint] NOT NULL,
[affiliateSystemName] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[mk_AffiliateSegment] [smallint] NOT NULL,
[affiliateSegmentName] [varchar] (30) COLLATE Latin1_General_CI_AS NOT NULL,
[isReportableSegment] [bit] NOT NULL,
[isReportableAffiliate] [bit] NOT NULL,
[mk_Zban] [int] NOT NULL,
[zbanCode] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[age] AS (isnull(datediff(hour,[customerBirthDate],getdate())/(8766),(-1))),
[mk_ageBucket] AS (case when datediff(hour,[customerBirthDate],getdate())/(8766)<(18) OR [customerBirthDate] IS NULL then (1) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(18) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(20) then (2) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(20) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(25) then (3) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(25) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(30) then (4) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(30) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(35) then (5) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(35) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(40) then (6) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(40) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(45) then (7) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(45) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(50) then (8) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(50) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(60) then (9) when datediff(hour,[customerBirthDate],getdate())/(8766)>=(60) then (10)  end),
[ageBucket] AS (case when datediff(hour,[customerBirthDate],getdate())/(8766)<(18) OR [customerBirthDate] IS NULL then 'Unknown' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(18) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(20) then '18-19' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(20) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(25) then '20-24' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(25) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(30) then '25-29' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(30) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(35) then '30-34' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(35) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(40) then '35-39' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(40) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(45) then '40-44' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(45) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(50) then '45-49' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(50) AND datediff(hour,[customerBirthDate],getdate())/(8766)<(60) then '50-59' when datediff(hour,[customerBirthDate],getdate())/(8766)>=(60) then '>60'  end),
[mk_DeviceType] [int] NOT NULL CONSTRAINT [DF_Customer_mk_DeviceType] DEFAULT ((-1)),
[deviceName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[mk_City] [int] NOT NULL CONSTRAINT [DF_Customer_mk_City] DEFAULT ((-1)),
[phoneNumberVerified] [bit] NOT NULL CONSTRAINT [DF__Customer__phoneN__65E50B2B] DEFAULT ((0)),
[emailVerified] [bit] NOT NULL CONSTRAINT [DF__Customer__emailV__66D92F64] DEFAULT ((0))
) ON [Marts_Data]
GO
ALTER TABLE [Dim].[Customer] ADD CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED  ([mk_Customer]) ON [Marts_Data]
GO
CREATE NONCLUSTERED INDEX [IX_Customer__isCurrent] ON [Dim].[Customer] ([isCurrent]) INCLUDE ([customerBirthDate], [mk_CurrentCustomer]) WITH (DATA_COMPRESSION = PAGE) ON [Marts_Index]
GO
CREATE NONCLUSTERED INDEX [IX_Customer_isCurrent_Includes] ON [Dim].[Customer] ([isCurrent]) INCLUDE ([acceptEmailOffers], [acceptSmsOffers], [acceptTeleSalesOffers], [acquisitionSourceName], [affiliateCode], [affiliateSegmentName], [affiliateStatusName], [alAffiliateId], [brandName], [businessChannelName], [channelName], [clAffiliateId], [countryCode], [countryName], [currencyCode], [currencyName], [customerCreateDateGMT], [customerFirstName], [customerGuid], [customerLastName], [customerMarketFriendlyName], [customerSportsbookRiskGeneralLimit], [customerStatusGroupName], [customerStatusName], [customerTypeName], [isBonusAbuser], [isReportableAffiliate], [isReportableSegment], [languageCode], [languageName], [merchantName], [mk_AcquisitionSource], [mk_Affiliate], [mk_AffiliateSegment], [mk_AffiliateStatus], [mk_AffiliateSystem], [mk_Brand], [mk_BusinessChannel], [mk_Channel], [mk_Country], [mk_Currency], [mk_CurrentCustomer], [mk_CustomerMarketFriendlyName], [mk_CustomerStatusGroup], [mk_CustomerStatusName], [mk_CustomerType], [mk_Language], [mk_Merchant], [mk_Partner], [mk_Region], [mk_Subregion], [mk_Zban], [partnerName], [regionName], [zbanCode]) WHERE ([isCurrent]=(1)) ON [Marts_Index]
GO
CREATE STATISTICS [_dta_stat_556737236_40_7] ON [Dim].[Customer] ([acceptEmailOffers], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_41_7] ON [Dim].[Customer] ([acceptSmsOffers], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_42_7] ON [Dim].[Customer] ([acceptTeleSalesOffers], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_91_89_92_7] ON [Dim].[Customer] ([affiliateSegmentName], [isCurrent], [mk_AffiliateSegment], [mk_AffiliateSystem])
GO
CREATE STATISTICS [_dta_stat_556737236_85_7] ON [Dim].[Customer] ([alAffiliateId], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_75_78_81_76_7] ON [Dim].[Customer] ([brandName], [isCurrent], [mk_Brand], [mk_Merchant], [mk_Partner])
GO
CREATE STATISTICS [_dta_stat_556737236_86_7] ON [Dim].[Customer] ([clAffiliateId], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_56_7] ON [Dim].[Customer] ([countryCode], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_63_7] ON [Dim].[Customer] ([currencyCode], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_7_4_61_62_63] ON [Dim].[Customer] ([currencyCode], [isCurrent], [mk_CurrentCustomer], [mk_Currency], [currencyName])
GO
CREATE STATISTICS [_dta_stat_556737236_61_62_63_7] ON [Dim].[Customer] ([currencyName], [currencyCode], [isCurrent], [mk_Currency])
GO
CREATE STATISTICS [_dta_stat_556737236_37_7] ON [Dim].[Customer] ([customerSportsbookRiskGeneralLimit], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_39_7] ON [Dim].[Customer] ([isBonusAbuser], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_85] ON [Dim].[Customer] ([isCurrent], [alAffiliateId], [mk_CurrentCustomer])
GO
CREATE STATISTICS [_dta_stat_556737236_93_91_89_7] ON [Dim].[Customer] ([isCurrent], [isReportableSegment], [mk_AffiliateSegment], [mk_AffiliateSystem])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_93_91_89] ON [Dim].[Customer] ([isCurrent], [isReportableSegment], [mk_AffiliateSegment], [mk_AffiliateSystem], [mk_CurrentCustomer])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_53] ON [Dim].[Customer] ([isCurrent], [languageCode], [mk_CurrentCustomer])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_87_94_88] ON [Dim].[Customer] ([isCurrent], [mk_AffiliateStatus], [isReportableAffiliate], [affiliateStatusName], [mk_CurrentCustomer])
GO
CREATE STATISTICS [_dta_stat_556737236_4_16_7] ON [Dim].[Customer] ([isCurrent], [mk_CurrentCustomer], [customerLastName])
GO
CREATE STATISTICS [_dta_stat_556737236_7_4_95_96] ON [Dim].[Customer] ([isCurrent], [mk_CurrentCustomer], [mk_Zban], [zbanCode])
GO
CREATE STATISTICS [_dta_stat_556737236_44_46_45_7] ON [Dim].[Customer] ([isCurrent], [mk_CustomerStatusName], [mk_CustomerStatusGroup], [customerStatusName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_44_46_45] ON [Dim].[Customer] ([isCurrent], [mk_CustomerStatusName], [mk_CustomerStatusGroup], [customerStatusName], [mk_CurrentCustomer])
GO
CREATE STATISTICS [_dta_stat_556737236_51_52_53_7] ON [Dim].[Customer] ([isCurrent], [mk_Language], [languageName], [languageCode])
GO
CREATE STATISTICS [_dta_stat_556737236_95_96_7] ON [Dim].[Customer] ([isCurrent], [mk_Zban], [zbanCode])
GO
CREATE STATISTICS [_dta_stat_556737236_94_7] ON [Dim].[Customer] ([isReportableAffiliate], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_53_7] ON [Dim].[Customer] ([languageCode], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_78_81_79_7] ON [Dim].[Customer] ([merchantName], [isCurrent], [mk_Merchant], [mk_Partner])
GO
CREATE STATISTICS [_dta_stat_556737236_64_65_7] ON [Dim].[Customer] ([mk_AcquisitionSource], [acquisitionSourceName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_4_95_66_69_75_78_81_71_83_94_87_93_91_89_64_51] ON [Dim].[Customer] ([mk_AffiliateSegment], [mk_AffiliateSystem], [mk_AcquisitionSource], [mk_Language], [mk_Partner], [mk_BusinessChannel], [mk_Affiliate], [isReportableAffiliate], [mk_AffiliateStatus], [isReportableSegment], [mk_CurrentCustomer], [mk_Zban], [mk_Channel], [mk_CustomerMarketFriendlyName], [mk_Brand], [mk_Merchant])
GO
CREATE STATISTICS [_dta_stat_556737236_87_94_88_7] ON [Dim].[Customer] ([mk_AffiliateStatus], [isReportableAffiliate], [affiliateStatusName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_7_4_83_94_87_93_91_89_84_85] ON [Dim].[Customer] ([mk_AffiliateStatus], [isReportableSegment], [mk_AffiliateSegment], [mk_AffiliateSystem], [affiliateCode], [alAffiliateId], [isCurrent], [mk_CurrentCustomer], [mk_Affiliate], [isReportableAffiliate])
GO
CREATE STATISTICS [_dta_stat_556737236_83_94_87_93_91_89_84_85_86_7_4] ON [Dim].[Customer] ([mk_AffiliateSystem], [affiliateCode], [alAffiliateId], [clAffiliateId], [isCurrent], [mk_CurrentCustomer], [mk_Affiliate], [isReportableAffiliate], [mk_AffiliateStatus], [isReportableSegment], [mk_AffiliateSegment])
GO
CREATE STATISTICS [_dta_stat_556737236_89_90_7] ON [Dim].[Customer] ([mk_AffiliateSystem], [affiliateSystemName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_69_75_78_81_70_7] ON [Dim].[Customer] ([mk_Brand], [mk_Merchant], [mk_Partner], [customerMarketFriendlyName], [isCurrent], [mk_CustomerMarketFriendlyName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_69_75_78_81_70] ON [Dim].[Customer] ([mk_Brand], [mk_Merchant], [mk_Partner], [customerMarketFriendlyName], [mk_CurrentCustomer], [isCurrent], [mk_CustomerMarketFriendlyName])
GO
CREATE STATISTICS [_dta_stat_556737236_71_72_7] ON [Dim].[Customer] ([mk_BusinessChannel], [businessChannelName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_66_67_7] ON [Dim].[Customer] ([mk_Channel], [channelName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_54_55_57_56_7] ON [Dim].[Customer] ([mk_Country], [countryName], [mk_Subregion], [countryCode], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_4_22] ON [Dim].[Customer] ([mk_CurrentCustomer], [customerEmail])
GO
CREATE STATISTICS [_dta_stat_556737236_4_15] ON [Dim].[Customer] ([mk_CurrentCustomer], [customerFirstName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_40] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [acceptEmailOffers])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_41] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [acceptSmsOffers])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_63] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [currencyCode])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_15] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [customerFirstName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_39] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [isBonusAbuser])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_94] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [isReportableAffiliate])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_64_65] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_AcquisitionSource], [acquisitionSourceName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_89_90] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_AffiliateSystem], [affiliateSystemName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_71_72] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_BusinessChannel], [businessChannelName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_66_67] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_Channel], [channelName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_81_82] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_Partner], [partnerName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_59_60] ON [Dim].[Customer] ([mk_CurrentCustomer], [isCurrent], [mk_Region], [regionName])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_95_66_69_75_78_81_71_83_94_87_93_91_89_64] ON [Dim].[Customer] ([mk_CurrentCustomer], [mk_AffiliateSegment], [mk_AffiliateSystem], [mk_AcquisitionSource], [mk_Partner], [mk_BusinessChannel], [mk_Affiliate], [isReportableAffiliate], [mk_AffiliateStatus], [isReportableSegment], [isCurrent], [mk_Zban], [mk_Channel], [mk_CustomerMarketFriendlyName], [mk_Brand], [mk_Merchant])
GO
CREATE STATISTICS [_dta_stat_556737236_73_74_7] ON [Dim].[Customer] ([mk_CustomerMarketName], [customerMarketName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_46_47_7] ON [Dim].[Customer] ([mk_CustomerStatusGroup], [customerStatusGroupName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_49_50_7] ON [Dim].[Customer] ([mk_CustomerType], [customerTypeName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_4_7_78_81_79] ON [Dim].[Customer] ([mk_Partner], [merchantName], [mk_CurrentCustomer], [isCurrent], [mk_Merchant])
GO
CREATE STATISTICS [_dta_stat_556737236_81_82_7] ON [Dim].[Customer] ([mk_Partner], [partnerName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_59_60_7] ON [Dim].[Customer] ([mk_Region], [regionName], [isCurrent])
GO
CREATE STATISTICS [_dta_stat_556737236_7_4_54_55_57_56] ON [Dim].[Customer] ([mk_Subregion], [countryCode], [isCurrent], [mk_CurrentCustomer], [mk_Country], [countryName])
GO
CREATE STATISTICS [_dta_stat_556737236_57_58_59_7] ON [Dim].[Customer] ([mk_Subregion], [subRegionName], [mk_Region], [isCurrent])
GO
