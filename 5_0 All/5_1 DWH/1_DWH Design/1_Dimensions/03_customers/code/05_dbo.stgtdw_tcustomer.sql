
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [dbo].[stgtdw_tcustomer] 
	@packageRunID int = NULL,
	@sourceTimeZone VARCHAR(50),
	@biWarehouseTimeZone VARCHAR(50)
     
as
set nocount on

/*

Description: moves customer info data from stage to TDW. Updates or creates new customers. 
			 Updates both tables tcustomer and tinternalCustomer.
			 tinternalCustomer has SCD2 history handling on certain attributes, and SCD1 attributes on all
			others.

			As we can recieve several updates to a user on a single day, and we only have history tracking
			on a date level, if we recieve more than one update on any of the SCD2 attributes we treat it as
			a SCD1 attribute and overwrite it. And to avoid ambiguity of when a change occured, we set the date 
			of the change to tomorrow.

History:
20100922 anhe, migrated from bdw
20101018 sala, added sportsbook general risk limit
20110118 maja, added customer type
20110629 sala, added selfExcludedEndDate
20111017 anbe2, added customerGuid
2011	 ???? ,	added fix in 'Get metadata info' section. Both customerId and merchantId must match.
2011	 ???? , added isnull-check for passwordCheckSum
20120103 anbe2 & anhe, added Affiliate Lounge registrationCode. Added all migration source mappings from production and changed the load of zbancode.
20120202 rola, added isBonusAbuser
20120404 rola, added a insertion to CustomerGuidChangesLog (to track if customerguids have changed).
20120518 wiol, prevented change of premiguserid
20120529 anbe2, added a Temporary solultion which deletes SvenskaLotter customers from stage.
20120601 rola, added a insertion to CustomerMetaDataChangesLog (to track if pre_mig_userid have changed).
20120620 wiol, split sp into three parts:
				-stgtdw_tcustomer_affiliate
				-stgtdw_tcustomer_customer
				-stgtdw_tcustomer_internalCustomer
				The SPs share two permanent tables
					-stage.dbo.transform_customerMetaInfo
					-tdw.dbo.error_tch_getCustomers
20130130    rama, added expired accounts
2013-03-05: ACLA added time conversion function
2013-08-23	stgr	Added expired self exclusion SP
2013-10-25	mabu2	Added the execution of stgtdw_CustomerContractInformationValidated_nsb which sets the email and phone verification flags
2014-04-17	jofe	take the SB risk limits we have in Stage and update the current internal customer entry of the respective customer.
2014-07-03	jofe	Moved stgtdw_tcustomer_internalCustomer_spo to ETL package stgtdw so that the customer table will be populated first before the execution of this procedure.
*/

-- log start
declare @logID int ,@msg varchar(512), @procName varchar(50) = object_name(@@procid), @rows int
exec dbo.logStart @procName, @packageRunID, @logID output

-- Log
	exec dbo.logInfo @logID, 'Begin processing customer metadata'

-- run procedure to process metadata 
	exec dbo.stgtdw_tcustomer_affiliate @packageRunID  --- ???

-- Log
	exec dbo.logInfo @logID, 'Begin processing tCustomer'

-- Run procedure to create the tcustomer object	
	exec dbo.stgtdw_tcustomer_customer @packageRunID, @sourceTimeZone , @biWarehouseTimeZone

-- Log
	exec dbo.logInfo @logID, 'Begin processing tinternalCustomer'

-- Run procedure to create the tinternalcustomer object	
    exec dbo.stgtdw_tcustomer_internalCustomer @packageRunID, @sourceTimeZone , @biWarehouseTimeZone

	
-- Run Procedure to update the Customer email and phone validation flags.
	EXEC dbo.stgtdw_CustomerContractInformationValidated_nsb @packageRunID

-- clean up error tables. Removes rows that are ok
		delete tdw.dbo.error_tch_getCustomers 
		from tdw.dbo.error_tch_getCustomers t
		where t.errorMessage is null

-- Log
	exec dbo.logInfo @logID, 'Begin processing expired accounts'

-- Run procedure to create the tinternalcustomer object	
    exec dbo.[tdwtdw_tcustomer_expiredAccount] @packageRunID

	EXEC dbo.tdwtdw_tcustomer_expiredSelfExclusion @packageRunID 

-- log end
exec dbo.logStop @logID

return 0









GO
