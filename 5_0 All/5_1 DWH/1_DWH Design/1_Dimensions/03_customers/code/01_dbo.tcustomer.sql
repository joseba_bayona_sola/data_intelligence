CREATE TABLE [dbo].[tcustomer]
(
[sk_customer] [int] NOT NULL IDENTITY(1, 1),
[src_customerId] [int] NULL,
[pre_migration_userid] [int] NULL,
[sk_merchant] [smallint] NOT NULL,
[customerCreateDateGMT] [datetime] NULL,
[customerRegIP] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[activateDate] [datetime] NULL,
[migrationSourceId] [smallint] NOT NULL CONSTRAINT [DF_tcustomer_migrationSourceId] DEFAULT ((-1)),
[externalCustomerID] [int] NULL,
[migrationStatusID] [smallint] NOT NULL CONSTRAINT [DF_tcustomer_migrationStatusID] DEFAULT ((0)),
[migrationDateGMT] [datetime] NULL,
[registrationLanguageCode] [varchar] (2) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF_tcustomer_registrationLanguageCode] DEFAULT (''),
[customerGuid] [uniqueidentifier] NULL,
[regDate] [datetime] NOT NULL CONSTRAINT [DF_tcustomer_regDate] DEFAULT (getutcdate()),
[changeDate] [datetime] NULL,
[registrationSource] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[globalUserId] [int] NULL,
[socialSecurityNumber] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[temporaryAccountExpiryDate] [datetime] NULL,
[temporaryAccountFreezeDate] [datetime] NULL,
[temporaryAccountUseFreezeAccount] [bit] NULL,
[temporaryAccountEncourageVerificationDate] [datetime] NULL,
[sk_channel] [smallint] NULL,
[customerLoginName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[acquisitionSourceID] [smallint] NULL CONSTRAINT [DF_tcustomer_acquisitionSourceID] DEFAULT ((4)),
[sk_deviceType] [int] NOT NULL CONSTRAINT [DF_tcustomer_sk_deviceType] DEFAULT ((-1))
) ON [TDW_Data]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tcustomer_pre_migration_userid_sk_merchant] ON [dbo].[tcustomer] ([pre_migration_userid], [sk_merchant], [migrationSourceId]) WHERE ([pre_migration_userid] IS NOT NULL AND [pre_migration_userid]<>(759119)) ON [TDW_Index]

ALTER TABLE [dbo].[tcustomer] WITH NOCHECK ADD
CONSTRAINT [FK_tcustomer_tdeviceIdMapping] FOREIGN KEY ([sk_deviceType]) REFERENCES [dbo].[tdeviceIdMapping] ([sk_deviceType])
ALTER TABLE [dbo].[tcustomer] ADD 
CONSTRAINT [PK_tcustomer] PRIMARY KEY CLUSTERED  ([sk_customer]) ON [TDW_Data]
CREATE NONCLUSTERED INDEX [IX_tcustomer__migrationStatusID_sk_customer] ON [dbo].[tcustomer] ([migrationStatusID], [sk_customer]) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [IX_tcustomer__pre_migration_userid_sk_merchant_sk_customer] ON [dbo].[tcustomer] ([pre_migration_userid], [sk_merchant], [sk_customer]) INCLUDE ([src_customerId]) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [IX_tcustomer_sk_merchant_migrationSourceId_pre_migration_userid] ON [dbo].[tcustomer] ([sk_merchant], [migrationSourceId], [pre_migration_userid], [sk_customer]) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [IX_tcustomer__sk_merchant_sk_customer_pre_migration_userid] ON [dbo].[tcustomer] ([sk_merchant], [sk_customer], [pre_migration_userid]) INCLUDE ([customerCreateDateGMT]) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [UK_tcustomer__src_customerId_sk_merchant] ON [dbo].[tcustomer] ([src_customerId], [sk_merchant]) INCLUDE ([sk_customer]) ON [TDW_Index]



CREATE UNIQUE NONCLUSTERED INDEX [UQ_tcustomer_customerGuid] ON [dbo].[tcustomer] ([customerGuid]) WHERE ([customerGuid] IS NOT NULL) ON [TDW_Index]

ALTER TABLE [dbo].[tcustomer] ADD
CONSTRAINT [FK_tcustomer_tchannel] FOREIGN KEY ([sk_channel]) REFERENCES [dbo].[tchannel] ([sk_channel])
ALTER TABLE [dbo].[tcustomer] ADD
CONSTRAINT [FK_tcustomer_tmerchant] FOREIGN KEY ([sk_merchant]) REFERENCES [dbo].[tmerchant] ([sk_merchant])
ALTER TABLE [dbo].[tcustomer] ADD
CONSTRAINT [FK_tcustomer_tcustomerSource] FOREIGN KEY ([migrationSourceId]) REFERENCES [dbo].[tmigrationSource] ([migrationSourceID])





















GO


GRANT SELECT ON  [dbo].[tcustomer] TO [app_bi]
GRANT SELECT ON  [dbo].[tcustomer] TO [app_controlpanel]
GO
