CREATE TABLE [dbo].[tinternalCustomer]
(
[sk_internalCustomer] [int] NOT NULL IDENTITY(1, 1),
[sk_customer] [int] NOT NULL,
[effectiveDateBeginCET] [date] NOT NULL,
[effectiveDateEndCET] [date] NULL,
[isCurrent] [bit] NOT NULL,
[sk_affiliate] [int] NOT NULL,
[sk_zbanCode] [int] NOT NULL,
[sk_affiliateStrikeBack] [int] NOT NULL,
[sk_zbanCodeStrikeBack] [int] NOT NULL,
[sk_registerBrand] [smallint] NOT NULL,
[currencyCode] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[sk_customerState] [smallint] NOT NULL,
[customerMarketID] [smallint] NOT NULL,
[customerFirstName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[customerLastName] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[customerAddress] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerZipCode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[customerCity] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[countryCode] [char] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[customerPhone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerCellPhone] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerEmail] [nvarchar] (255) COLLATE Latin1_General_CI_AS NULL,
[customerBirthDate] [date] NULL,
[customerGender] [char] (1) COLLATE Latin1_General_CI_AS NULL,
[customerNotes] [ntext] COLLATE Latin1_General_CI_AS NULL,
[referralCode] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
[AcceptEmailOffers] [bit] NOT NULL,
[AcceptSmsOffers] [bit] NOT NULL,
[customerPwdChecksum] [int] NULL,
[customerSportsbookRiskGeneralLimit] [real] NULL,
[affiliateStrikeBackTagDate] [datetime] NULL,
[sk_customerType] [smallint] NOT NULL CONSTRAINT [DF_tinternalCustomer_sk_customerType] DEFAULT ((-1)),
[selfExcludedEndDate] [datetime] NULL,
[regDate] [datetime] NOT NULL CONSTRAINT [DF_tinternalCustomer_regDate] DEFAULT (getutcdate()),
[changeDate] [datetime] NULL,
[isBonusAbuser] [bit] NOT NULL CONSTRAINT [DF_tinternalCustomer_isBonusAbuser] DEFAULT ((0)),
[acceptTeleSalesOffers] [bit] NULL,
[PhoneNumberVerified] [bit] NULL CONSTRAINT [DF_tinternalCustomer_PhoneNumberVerified] DEFAULT ((0)),
[EmailVerified] [bit] NULL CONSTRAINT [DF_tinternalCustomer_EmailVerified] DEFAULT ((0))
) ON [TDW_Data] TEXTIMAGE_ON [TDW_Data]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_sk_customer_effectiveDateBeginCET] ON [dbo].[tinternalCustomer] ([sk_customer], [effectiveDateBeginCET]) INCLUDE ([effectiveDateEndCET], [sk_internalCustomer], [sk_registerBrand]) WITH (FILLFACTOR=80, DATA_COMPRESSION = PAGE) ON [TDW_Index]

CREATE UNIQUE NONCLUSTERED INDEX [UQ_tinternalCustomer_sk_customer_effectiveDateEndCET] ON [dbo].[tinternalCustomer] ([sk_customer], [effectiveDateEndCET]) INCLUDE ([isCurrent], [sk_customerType], [sk_internalCustomer]) WITH (FILLFACTOR=80, DATA_COMPRESSION = PAGE) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [IX_isCurrent_sk_customerType_INCLUDES] ON [dbo].[tinternalCustomer] ([isCurrent], [sk_customerType]) INCLUDE ([countryCode], [currencyCode], [customerMarketID], [selfExcludedEndDate], [sk_affiliate], [sk_affiliateStrikeBack], [sk_customer], [sk_customerState], [sk_registerBrand], [sk_zbanCode]) ON [TDW_Index]



CREATE NONCLUSTERED INDEX [IX_sk_customerType] ON [dbo].[tinternalCustomer] ([sk_customerType], [sk_internalCustomer], [customerMarketID]) INCLUDE ([sk_customer], [sk_customerState], [sk_registerBrand]) ON [TDW_Index]







GO
GRANT SELECT ON  [dbo].[tinternalCustomer] TO [app_bi]
GRANT SELECT ON  [dbo].[tinternalCustomer] TO [roleSSRS]
GO

ALTER TABLE [dbo].[tinternalCustomer] ADD 
CONSTRAINT [PK_tinternalCustomer] PRIMARY KEY CLUSTERED  ([sk_internalCustomer]) ON [TDW_Data]
GO
ALTER TABLE [dbo].[tinternalCustomer] ADD CONSTRAINT [UC_internalUser_sk_internalUser_effectiveDateBegin] UNIQUE NONCLUSTERED  ([sk_internalCustomer], [effectiveDateBeginCET]) ON [TDW_Index]

CREATE NONCLUSTERED INDEX [IX_tinternalCustomer_customerMarketID_sk_customerType_sk_affiliate_sk_internalCustomer] ON [dbo].[tinternalCustomer] ([customerMarketID], [sk_customerType], [sk_affiliate], [sk_internalCustomer]) ON [TDW_Index]













CREATE UNIQUE NONCLUSTERED INDEX [UQ_tinternalCustomer_sk_customer_isCurrent] ON [dbo].[tinternalCustomer] ([sk_customer], [isCurrent]) WHERE ([isCurrent]=(1)) ON [TDW_Index]

ALTER TABLE [dbo].[tinternalCustomer] ADD
CONSTRAINT [FK_tinternalCustomer_tcustomer] FOREIGN KEY ([sk_customer]) REFERENCES [dbo].[tcustomer] ([sk_customer])

GO
