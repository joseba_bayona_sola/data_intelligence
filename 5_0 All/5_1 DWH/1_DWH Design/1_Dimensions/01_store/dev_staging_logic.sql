
select store_id, name, 
	case 
		when website like '%.co.uk' then '.co.uk'
		else right(website, charindex('.', reverse(website), 1)) 
	end as tld, website
from 
	(select store_id, name, 
		left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end) as website	
	from Landing.mag.core_store) cs

select store_name, tld, store_type
from Landing.map.gen_store_type

select store_name, name, website_group
from Landing.map.gen_website_group



select store_id, store_name, store_type website_type, website_group
from Landing.migra_pr.dw_stores_full


-----------------

select store_id, store_name, website_type, website_group, website, tld, 
	case 
		when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
		when website_type = 'White Label' then 'wl'
		else '' 
	end + TLD as code_tld
from
	(select cs.store_id, cs.name store_name, 
		case when (st.store_type is null) then st2.store_type else st.store_type end website_type,
		case when (wg.website_group is null) then wg2.website_group else wg.website_group end website_group,
		cs.website, cs.tld
	from 
			(select store_id, name, 
				case 
					when website like '%.co.uk' then '.co.uk'
					else right(website, charindex('.', reverse(website), 1)) 
				end as tld, website
			from 
				(select store_id, name, 
					left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end) as website	
				from Landing.mag.core_store) cs) cs
		left join
			Landing.map.gen_store_type st on cs.name = st.store_name
		left join
			Landing.map.gen_store_type st2 on cs.tld = st2.tld
		left join
			Landing.map.gen_website_group wg on cs.name = wg.store_name
		left join
			Landing.map.gen_website_group wg2 on charindex(wg2.name, cs.name) > 0
	where cs.store_id <> 0) cs

select store_id, store_name, website_type, website_group, website, tld, 
	case 
		when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
		when website_type = 'White Label' then 'wl'
		else '' 
	end + TLD as code_tld
from 
	(select store_id, store_name, website_type, website_group, website, 
		case 
			when website like '%.co.uk' then '.co.uk'
			else right(website, charindex('.', reverse(website), 1)) 
		end as tld
	from 
		(select store_id, store_name, store_type website_type, website_group, 
			left(store_name, case when charindex('/', store_name, 1) = 0 then len(store_name) else charindex('/', store_name, 1) - 1 end) as website
		from Landing.migra_pr.dw_stores_full) sf) sf

---- 
select store_id, store_name, website_type, website_group, website, tld, code_tld
from
	(select store_id, store_name, website_type, website_group, website, tld, 
		case 
			when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
			when website_type = 'White Label' then 'wl'
			else '' 
		end + TLD as code_tld
	from
		(select cs.store_id, cs.name store_name, 
			case when (st.store_type is null) then st2.store_type else st.store_type end website_type,
			case when (wg.website_group is null) then wg2.website_group else wg.website_group end website_group,
			cs.website, cs.tld
		from 
				(select store_id, name, 
					case 
						when website like '%.co.uk' then '.co.uk'
						else right(website, charindex('.', reverse(website), 1)) 
					end as tld, website
				from 
					(select store_id, name, 
						left(name, case when charindex('/', name, 1) = 0 then len(name) else charindex('/', name, 1) - 1 end) as website	
					from Landing.mag.core_store) cs) cs
			left join
				Landing.map.gen_store_type st on cs.name = st.store_name
			left join
				Landing.map.gen_store_type st2 on cs.tld = st2.tld
			left join
				Landing.map.gen_website_group wg on cs.name = wg.store_name
			left join
				Landing.map.gen_website_group wg2 on charindex(wg2.name, cs.name) > 0
		where cs.store_id <> 0) cs
	union
	select store_id * -1, store_name, website_type, website_group, website, tld, 
		case 
			when website_type = 'Core UK' and tld <> '.co.uk' then 'gbp'
			when website_type = 'White Label' then 'wl'
			else '' 
		end + TLD as code_tld
	from 
		(select store_id, store_name, website_type, website_group, website, 
			case 
				when website like '%.co.uk' then '.co.uk'
				else right(website, charindex('.', reverse(website), 1)) 
			end as tld
		from 
			(select store_id, store_name, store_type website_type, website_group, 
				left(store_name, case when charindex('/', store_name, 1) = 0 then len(store_name) else charindex('/', store_name, 1) - 1 end) as website
			from Landing.migra_pr.dw_stores_full) sf) sf
	where store_id not in (20, 21)) stores

------------------------------------------------------------------------------------------

select store_id_bk, store_name, website_type, website_group, website, tld, code_tld
from Staging.gen.dim_store

create table #MergeResults (MergeAction char(6) NOT NULL);

merge into Warehouse.gen.dim_store with (tablock) as trg
using Warehouse.gen.dim_store_wrk src
	on (trg.store_id_bk = src.store_id_bk)
when matched and not exists 
	(select isnull(trg.store_name, ''), isnull(trg.website_type, ''), isnull(trg.website_group, ''), isnull(trg.website, ''), isnull(trg.tld, ''), isnull(trg.code_tld, '')
	intersect
	select isnull(src.store_name, ''), isnull(src.website_type, ''), isnull(src.website_group, ''), isnull(src.website, ''), isnull(src.tld, ''), isnull(src.code_tld, ''))
	then 
		update set
			trg.store_name = src.store_name, 
			trg.website_type = src.website_type, trg.website_group = src.website_group, trg.website = src.website, 
			trg.tld = src.tld, trg.code_tld = src.code_tld, 
			trg.idETLBatchRun_upd = 1, trg.upd_ts = getutcdate()
when not matched
	then 
		insert (store_id_bk, store_name, website_type, website_group, website, tld, code_tld, idETLBatchRun_ins)
			values (src.store_id_bk, src.store_name, src.website_type, src.website_group, src.website, src.tld, src.code_tld, 1)
			
OUTPUT $action INTO #MergeResults;			

select *
from #MergeResults

SELECT  [INSERT], [UPDATE], [DELETE]
FROM    
		(SELECT mergeAction, 1 rows
		 FROM #MergeResults) c 
	 PIVOT
		(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt