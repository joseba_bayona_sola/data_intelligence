
select s.website_group, s.store_name, 
	oh.num_orders, oh.min_order, oh.max_order
from 
		(select store_name, 
			count(*) num_orders, 
			min(convert(date, order_date)) min_order, max(convert(date, order_date)) max_order
		from DW_GetLenses.dbo.order_headers
		group by store_name) oh
	inner join
		DW_GetLenses.dbo.stores s on oh.store_name = s.store_name
order by s.website_group, s.store_name

--------------

select s.website_group, s.store_name, 
	oh.num_orders, oh.min_order, oh.max_order
from 
		(select store_name, 
			count(*) num_orders, 
			min(convert(date, order_date)) min_order, max(convert(date, order_date)) max_order
		from DW_Proforma.dbo.order_headers
		group by store_name) oh
	inner join
		DW_Proforma.dbo.stores s on oh.store_name = s.store_name
order by s.website_group, s.store_name

--------------
select s.website_group, s.store_name, 
	oh.num_orders, oh.min_order, oh.max_order
from
		(select store_id,
			count(*) num_orders, 
			min(convert(date, created_at)) min_order, max(convert(date, created_at)) max_order
		from DW_GetLenses.dbo.dw_hist_order
		group by store_id) oh
	inner join
		DW_GetLenses.dbo.dw_stores_full s on oh.store_id = s.store_id
order by s.website_group, s.store_name

	select oh.source, s.website_group, s.store_name, 
		oh.num_orders, oh.min_order, oh.max_order
	from
			(select source, store_id,
				count(*) num_orders, 
				min(convert(date, created_at)) min_order, max(convert(date, created_at)) max_order
			from DW_GetLenses.dbo.dw_hist_order
			group by source, store_id) oh
		inner join
			DW_GetLenses.dbo.dw_stores_full s on oh.store_id = s.store_id
	order by oh.source, s.website_group, s.store_name
