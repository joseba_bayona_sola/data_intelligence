
-------------------------- Magento01 - dw_flat --------------------------------------

DROP TABLE IF EXISTS dw_stores_map
CREATE TABLE IF NOT EXISTS dw_stores_map

INSERT INTO dw_stores_map (store_id, store_name, website_group, store_type, visible) VALUES 
	('11','asda-contactlenses.co.uk','ASDA','White Label','1'),
	('5','eur.getlenses.nl','GetLenses','Europe','1'),
	('19','EyePlan','EyePlan','White Label','1'),
	('4','gbp.getlenses.nl','Getlenses','UK','1'),
	('6','getlenses.be','Getlenses','Europe','1'),
	('2','getlenses.co.uk','Getlenses','UK','1'),
	('18','getlenses.de','Getlenses','Europe','1'),
	('17','getlenses.es','Getlenses','Europe','1'),
	('3','getlenses.ie','Getlenses','Europe','1'),
	('10','getlenses.it','Getlenses','Europe','1'),
	('1','postoptics.co.uk','Getlenses','UK','1'),
	('8','surveys.getlenses.co.uk','Getlenses','UK','1'),
	('9','surveys.getlenses.nl','Getlenses','UK','1'),
	('7','vhi-contactlenses.ie','VHI','White Label','1'),
	('0','Default','Default','Default','0');

--------------- ??????????? dw_stores_map (More Insert??) --> Can be done manually without script - script just executed once

 
CREATE TABLE IF NOT EXISTS dw_stores(
    store_id INT NOT NULL,
    store_name VARCHAR(100),
    website_group VARCHAR(100),
    store_type VARCHAR(100),
    visible INT,
    PRIMARY KEY(store_id)) 

	SELECT * FROM dw_stores_map

CREATE OR REPLACE VIEW vw_stores AS 
    SELECT store_name,website_group,store_type
    FROM dw_stores
    WHERE visible =1;

CREATE OR REPLACE VIEW vw_stores_full AS 
    SELECT *
    FROM dw_stores

--- 0.1.4 --> 0.1.5

UPDATE dw_stores SET store_type = 'Core UK' WHERE store_type = 'UK'
UPDATE dw_stores_map SET store_type = 'Core UK' WHERE store_type = 'UK'

-- Build.php

DROP TABLE IF EXISTS dw_stores;

CREATE TABLE IF NOT EXISTS dw_stores (
  store_id      INT NOT NULL,
  store_name    VARCHAR(100),
  website_group VARCHAR(100),
  store_type    VARCHAR(100),
  visible       INT,
  PRIMARY KEY (store_id))
	
	SELECT * FROM dw_stores_map;

INSERT INTO dw_stores
	SELECT website_id, name, '', '', 1
	FROM {$this->dbname}.core_website
	WHERE website_id NOT IN (SELECT store_id FROM dw_stores);


-------------------------- dw_flat - DW_GetLenses (SSIS) --------------------------------------

	-- DW GETLENSES
		SELECT * FROM dw_flat.vw_stores

		SELECT * FROM dw_flat.vw_stores_full

	-- DW PROFORMA

		select distinct DomainName
		from migrate_orderdata
		where ifnull(DomainName,'')<>''

		-- Add store_id by script
		id_prefix variable (lb=8 - lb=9

		counter As Integer = 0

		counter = counter + 1
		Row.storeid = CInt(CStr(Variables.idprefix) + CStr(counter))
		Row.storename = Row.DomainName

		-- Lookup DomainStoreTypes

		-- Lookup dw_stores_full (Proforma - DW_GetLenses) 

		-- Derived Columns
			-- store_id: ISNULL(cur_store_id) ==  FALSE  ? cur_store_id : store_id
			-- website_group: (DT_STR,100,1252)(ISNULL(cur_website_group) ==  FALSE  ? cur_website_group : @[User::website_group])
			-- store_type: ISNULL(cur_store_type) ==  FALSE  ? cur_store_type : store_type
			-- visible: ISNULL(cur_visible) ==  FALSE  ? cur_visible : 1

-------------------------- DW_GetLenses - DW_SalesActual --------------------------------------

	-- dbo.Dim_Stores_All 
	select *,
		case 
			when store_type='Core UK' and tld<>'.co.uk' then 'gbp'
			when store_type='White Label' then 'wl'
			else '' end +TLD as code_tld

	from 
		(select *,
			case 
				when website like '%.co.uk' then '.co.uk'
				else right(website,charindex('.',reverse(website),1)) 
			end as TLD 
		from
			(select distinct store_name, website_group, store_type,
				left(store_name,case when charindex('/',store_name,1)=0 then len(store_name) else charindex('/',store_name,1)-1 end) as website,
				store_id
			from
				(select * from dw_getlenses.dbo.dw_stores_full where visible=1
				union
				select * from dw_proforma.dbo.dw_stores_full where visible=1) rs) rs) rs