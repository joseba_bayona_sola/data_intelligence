
-- Store Type
select store_type, count(*)
from DW_Sales_Actual.dbo.dim_stores_all
group by store_type
order by store_type

select store_id,
	store_name, website_group, store_type, 
	website, tld, code_tld
from DW_Sales_Actual.dbo.dim_stores_all
-- where store_type = 'Core UK'
-- where store_type = 'Europe'
-- where store_type = 'International'
-- where store_type = 'ROW'
where store_type = 'White Label'
order by store_id

-- Website Group
select website_group, count(*)
from DW_Sales_Actual.dbo.dim_stores_all
group by website_group
order by website_group

select store_id,
	store_name, website_group, store_type, 
	website, tld, code_tld
from DW_Sales_Actual.dbo.dim_stores_all
-- where website_group = 'Getlenses'
-- where website_group = 'Visiondirect'
-- where website_group in ('lensbase', 'lenshome', 'VisioOptik')
where website_group in ('ASDA', 'EyePlan', 'VHI')
order by store_id

