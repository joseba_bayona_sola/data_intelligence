
select store_id, store_name, website_group, store_type, visible
from dw_flat.dw_stores_map
order by store_type, website_group, store_name;

select store_id, store_name, website_group, store_type, visible
from dw_flat.dw_stores
order by store_type, website_group, store_name;

-- 

select store_name, website_group, store_type
from dw_flat.vw_stores
order by store_type, website_group, store_name;

select store_id, store_name, website_group, store_type, visible
from dw_flat.vw_stores_full
order by store_type, website_group, store_name;