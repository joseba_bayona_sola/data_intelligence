
-- DW_GetLenses
select 
	store_name, website_group, store_type
from DW_GetLenses.dbo.stores
order by store_type, website_group, store_name

select store_id, 
	store_name, website_group, store_type, 
	visible
from DW_GetLenses.dbo.dw_stores_full
order by store_type, website_group, store_name

-- DW_Proforma
select 
	store_name, website_group, store_type
from DW_Proforma.dbo.stores
order by store_type, website_group, store_name

select store_id, 
	store_name, website_group, store_type, 
	visible
from DW_Proforma.dbo.dw_stores_full
--order by store_type, website_group, store_name
order by store_id

select domain, store_type
from DW_Proforma.dbo.DomainStoreTypes


--- DW_Sales_Actual
select store_id,
	store_name, website_group, store_type, 
	website, tld, code_tld
from DW_Sales_Actual.dbo.dim_stores_all
--where store_name <> website
order by -- store_type, 
	website_group, store_name
-- order by store_id
	
	select distinct tld
	from DW_Sales_Actual.dbo.dim_stores_all
