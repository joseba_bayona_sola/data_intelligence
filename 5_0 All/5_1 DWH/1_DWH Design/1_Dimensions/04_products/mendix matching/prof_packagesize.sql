
select entity_id, sku, name, status, product_lifecycle
from Landing.mag.catalog_product_entity_flat_aud
where store_id = 0

select magentoProductID_int, magentoProductID, magentoSKU, name, status, 
	size, description, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	weight, height, width, depth
from Landing.mend.gen_productfamilypacksize_v

select dense_rank() over (partition by p.entity_id order by pfps.size) prod_size_rank,
	p.entity_id, p.sku, p.name, p.status, p.product_lifecycle, 
	pfps.magentoProductID, pfps.magentoSKU, pfps.name, pfps.status, 
	pfps.size, pfps.description, 
	pfps.packagingNo, pfps.allocationPreferenceOrder, pfps.purchasingPreferenceOrder, 
	pfps.weight, pfps.height, pfps.width, pfps.depth
from 
		(select *
		from Landing.mag.catalog_product_entity_flat_aud 
		where store_id = 0) p
	full join
		Landing.mend.gen_productfamilypacksize_v pfps on p.entity_id = pfps.magentoProductID_int 
-- where p.entity_id is null
-- where pfps.magentoProductID_int is null
where p.entity_id is not null and pfps.magentoProductID_int is not null
order by p.entity_id, pfps.magentoProductID
