
select entity_id, manufacturer, name, 
	status, product_lifecycle, promotional_product, visibility, telesales_only
from Landing.mag.catalog_product_entity_flat
where store_id = 0

select entity_id, manufacturer, name, 
	status, product_lifecycle, promotional_product, visibility, telesales_only
from Landing.mag.catalog_product_entity_flat
where store_id = 20

select t1.entity_id, t2.entity_id, t1.manufacturer, t2.manufacturer, t1.name, t2.name, 
	t1.status, t2.status, t1.product_lifecycle, t2.product_lifecycle, 
	t1.promotional_product, t2.promotional_product, t1.visibility, t2.visibility, t1.telesales_only, t2.telesales_only
from
		(select entity_id, manufacturer, name, 
			status, product_lifecycle, promotional_product, visibility, telesales_only
		from Landing.mag.catalog_product_entity_flat
		where store_id = 0) t1
	left join
		(select entity_id, manufacturer, name, 
			status, product_lifecycle, promotional_product, visibility, telesales_only
		from Landing.mag.catalog_product_entity_flat
		where store_id = 23) t2 on t1.entity_id = t2.entity_id
order by t1.entity_id


------------------------------------------------------

select status, count(*) -- product_lifecycle, promotional_product, visibility, telesales_only
from Landing.mag.catalog_product_entity_flat
where store_id = 0
group by status
order by status

select product_lifecycle, count(*) 
from Landing.mag.catalog_product_entity_flat
where store_id = 0
group by product_lifecycle
order by product_lifecycle

select promotional_product, count(*) 
from Landing.mag.catalog_product_entity_flat
where store_id = 0
group by promotional_product
order by promotional_product

select visibility, count(*) 
from Landing.mag.catalog_product_entity_flat
where store_id = 0
group by visibility
order by visibility

select telesales_only, count(*) 
from Landing.mag.catalog_product_entity_flat
where store_id = 0
group by telesales_only
order by telesales_only

--------------------------------------------------

select entity_id, manufacturer, name, 
	status, product_lifecycle, promotional_product, visibility, telesales_only
from Landing.mag.catalog_product_entity_flat
where store_id = 0
	and promotional_product = 1
	-- and visibility <> 4
	-- and telesales_only = 1
order by entity_id