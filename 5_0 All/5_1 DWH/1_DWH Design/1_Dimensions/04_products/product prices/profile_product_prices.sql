
select *
from Landing.mag.catalog_product_entity_tier_price_aud
where entity_id = 2364
order by website_id

select *
from Landing.mag.catalog_product_entity_tier_price_aud
where customer_group_id <> 0

select *
from Landing.mag.catalog_product_entity_flat_aud
where entity_id = 1083

select entity_id, store_id, sku, name
from Landing.mag.catalog_product_entity_flat_aud
where entity_id = 1177
order by store_id

select count(*)
from Landing.mag.catalog_product_entity_tier_price_aud

select tp.value_id, tp.entity_id, p.sku, p.name, tp.website_id, p.product_lifecycle, s.name store_name, 
	count(*) over (partition by tp.entity_id, tp.website_id) num_rep_prod,
	count(*) over (partition by tp.entity_id, tp.website_id, tp.promo_key) num_rep_prod_price,
	min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
	tp.customer_group_id, tp.all_groups, 
	tp.value, tp.qty, tp.value * tp.qty pack,
	tp.promo_key
from 
		Landing.mag.catalog_product_entity_tier_price_aud tp
	inner join
		(select *
		from Landing.mag.catalog_product_entity_flat_aud 
		where store_id = 0) p on tp.entity_id = p.entity_id 
	inner join
		Landing.mag.core_store_aud s on tp.website_id = s.store_id
order by entity_id, website_id, qty, promo_key
-- order by num_rep desc, entity_id, website_id, qty, promo_key

select *, qty / min_qty_prod, pack_price / (qty / min_qty_prod)
from
	(select tp.value_id, tp.entity_id, p.sku, p.name, tp.website_id, p.product_lifecycle, s.name store_name, 
		count(*) over (partition by tp.entity_id, tp.website_id) num_rep_prod,
		count(*) over (partition by tp.entity_id, tp.website_id, tp.promo_key) num_rep_prod_price,
		min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
		tp.customer_group_id, tp.all_groups, 
		tp.value, tp.qty, tp.value * tp.qty pack_price,
		tp.promo_key
	from 
			Landing.mag.catalog_product_entity_tier_price_aud tp
		inner join
			(select *
			from Landing.mag.catalog_product_entity_flat_aud 
			where store_id = 0) p on tp.entity_id = p.entity_id 
		inner join
			Landing.mag.core_store_aud s on tp.website_id = s.store_id) t
--where num_rep_prod_price > 1 and website_id = 20
order by entity_id, website_id, qty, promo_key

select value_id, entity_id, website_id, 
	customer_group_id, all_groups, 
	value, qty, value * qty,
	promo_key
from Landing.mag.catalog_product_entity_tier_price_aud
where entity_id = 1083
order by entity_id, website_id, qty, promo_key


select tp.value_id, tp.entity_id, tp.website_id, 
	tp.customer_group_id, tp.all_groups, 
	tp.value, tp.qty, tp.value * tp.qty,
	tp.promo_key
from 
		Landing.mag.catalog_product_entity_tier_price_aud tp 
	inner join
		Landing.mag.catalog_product_website_aud pw on tp.entity_id = pw.product_id and tp.website_id = pw.website_id
where entity_id = 1083
order by tp.entity_id, tp.website_id, tp.qty, tp.promo_key


select value_id, entity_id, website_id, 
	customer_group_id, all_groups, 
	value, qty, value * qty,
	promo_key, len(promo_key)
from Landing.mag.catalog_product_entity_tier_price_aud
where promo_key is not null and promo_key <> ''
order by entity_id, website_id, qty, promo_key

-----------------------------------------------------------------------------------

select store_id, store_name, 
	product_id, name, product_lifecycle, 
	value, qty, min_qty_prod, num_packs_tier_price, 
	total_price, pack_price, 
	(max_pack_price - pack_price) * 100 / case when (max_pack_price = 0) then 1 else max_pack_price end disc_percent,
	max_pack_price,
	promo_key, 
	-- case when (promo_key <> '') then  
	-- 	case when (num_packs_tier_price > 1) then 'PLA & Tier Pricing' else 'PLA' end
	-- 	else case when (num_packs_tier_price > 1) then 'Tier Pricing' else 'Regular' end end price_type

	case when (promo_key <> '') then 'PLA' else
		case when (num_packs_tier_price > 1) then 'Tier Pricing' else 'Regular' end end price_type
from
	(select store_id, store_name, 
		product_id, name, product_lifecycle, 
		value, qty, min_qty_prod, qty / min_qty_prod num_packs_tier_price, 
		total_price,
		total_price / (qty / min_qty_prod) pack_price, 
		promo_key, 
		max(total_price / (qty / min_qty_prod)) over (partition by product_id, store_id) max_pack_price
	from
		(select tp.website_id store_id, s.name store_name, 
			tp.entity_id product_id, p.name, p.product_lifecycle, 
			tp.value, tp.qty, 
			min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
			-- tp.customer_group_id, tp.all_groups, 
			tp.value * tp.qty total_price,
			tp.promo_key
		from 
				Landing.mag.catalog_product_entity_tier_price tp
			inner join
				(select entity_id, sku, name, product_lifecycle
				from Landing.mag.catalog_product_entity_flat_aud 
				where store_id = 0) p on tp.entity_id = p.entity_id 
			inner join
				Landing.mag.core_store_aud s on tp.website_id = s.store_id
		where tp.customer_group_id = 0 and tp.all_groups = 1) t) t
where store_id in (0, 29)  -- and product_lifecycle = ''
-- where promo_key <> ''
order by product_id, store_id, qty, promo_key
-- order by disc_percent desc, product_id, store_id, qty, promo_key