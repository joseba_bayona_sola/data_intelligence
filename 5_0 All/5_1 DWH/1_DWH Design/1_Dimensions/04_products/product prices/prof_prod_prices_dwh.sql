
select price_id, 
	product_id, store_name, 
	qty, pack_size, 
	local_unit_price_inc_vat, local_pack_price_inc_vat, local_qty_price_inc_vat, 
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_prof_fee
from DW_GetLenses.dbo.product_prices
where product_id = 1083
order by store_name

select product_id, store_name, 
	sku, name, 
	pack_qty, base_pack_qty, base_no_packs, 
	local_base_unit_price_inc_vat, local_base_value_inc_vat, local_base_pack_price_inc_vat, 
	multi_pack_qty, multi_no_packs
	local_multi_unit_price_inc_cat, local_multi_value_inc_vat, local_multi_pack_price_inc_vat
from DW_GetLenses.dbo.products
where product_id = 1083
order by store_name