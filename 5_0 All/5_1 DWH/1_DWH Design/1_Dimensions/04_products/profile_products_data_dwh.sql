
-- Category

select category_id, 
	product_type, category, 
	modality, type, feature, promotional_product
from DW_GetLenses.dbo.categories
order by product_type, category, modality, type, feature;

-- Product
select 
	product_id, store_name,
	sku, 
	status, 
	category_id, product_type, manufacturer, 
	
	product_lifecycle, visibility,
	 
	name, 

	telesales_only, promotional_product, promotion_rule, 	
	
	created_at, updated_at,
	 
	count(*) over () num_tot			 
from DW_GetLenses.dbo.products
-- where product_id in (1083, 1352, 2253, 2975, 2998) and store_name in ('Default', 'visiondirect.co.uk')
where store_name = 'visiondirect.co.uk' -- Default - 'visiondirect.co.uk'
order by product_id, store_name;

select product_id, sku, len(sku)
from DW_GetLenses.dbo.products
where store_name = 'Default' 
order by product_id

select product_id, store_name,
	brand, stocked_lens, tax_class_id, visibility, 
	average_cost, weight, daysperlens, alt_daysperlens, 
	glasses_colour, glasses_material, glasses_size, glasses_sex, glasses_style, glasses_shape, glasses_lens_width, glasses_bridge_width

from DW_GetLenses.dbo.products
-- where product_id in (1083, 1352, 2253, 2975, 2998) and store_name in ('Default', 'visiondirect.co.uk')
where store_name = 'visiondirect.co.uk' -- Default - 'visiondirect.co.uk'
order by product_id, store_name;

select product_id, store_name,
	pack_qty, packtext, alt_pack_qty, 

	base_pack_qty, base_no_packs, 
	local_base_unit_price_inc_vat, local_base_unit_price_exc_vat, 
	local_base_value_inc_vat, local_base_value_exc_vat, 
	local_base_pack_price_inc_vat, local_base_pack_price_exc_vat, 	
	local_base_prof_fee, 
	local_base_product_cost, local_base_shipping_cost, local_base_total_cost, 
	local_base_margin_amount, local_base_margin_percent, 

	global_base_unit_price_inc_vat, global_base_unit_price_exc_vat, 
	global_base_value_inc_vat, global_base_value_exc_vat, 
	global_base_pack_price_inc_vat, global_base_pack_price_exc_vat, 	
	global_base_prof_fee, global_base_product_cost, global_base_shipping_cost, global_base_total_cost, 
	global_base_margin_amount, global_base_margin_percent, 

	multi_pack_qty, multi_no_packs, 
	local_multi_unit_price_inc_vat, local_multi_unit_price_exc_vat, 
	local_multi_value_inc_vat, local_multi_value_exc_vat, 
	local_multi_pack_price_inc_vat, local_multi_pack_price_exc_vat, 	
	local_multi_prof_fee, local_multi_product_cost, local_multi_shipping_cost, local_multi_total_cost, 
	local_multi_margin_amount, local_multi_margin_percent, 
	
	global_multi_unit_price_inc_vat, global_multi_unit_price_exc_vat, 
	global_multi_value_inc_vat, global_multi_value_exc_vat, 
	global_multi_pack_price_inc_vat, global_multi_pack_price_exc_vat, 
	global_multi_prof_fee, global_multi_product_cost, global_multi_shipping_cost, global_multi_total_cost, 
	global_multi_margin_amount, global_multi_margin_percent, 

	local_to_global_rate, store_currency_code, 
	prof_fee_percent, vat_percent_before_prof_fee, vat_percent

from DW_GetLenses.dbo.products
-- where product_id in (1083, 1352, 2253, 2975, 2998) and store_name in ('Default', 'visiondirect.co.uk')
where store_name = 'visiondirect.co.uk' -- Default - 'visiondirect.co.uk'
	and base_pack_qty <> multi_pack_qty
order by product_id, store_name;


select product_id, store_name,
	equivalent_sku, replacement_sku, autoreorder_alter_sku, 
	product_lifecycle_text, 
	description, short_description, category_list_description, 
	meta_title, meta_keyword, meta_description, 
	image, small_image, thumbnail, url_key, url_path, 
	image_label, small_image_label, thumbnail_label,
	equivalence, availability, condition,
	upsell_text, price_comp_price, rrp_price, 
	promotion_rule2, promotional_text, 
	google_base_price, google_feed_title_prefix, google_feed_title_suffix, google_shopping_gtin

from DW_GetLenses.dbo.products
-- where product_id in (1083, 1352, 2253, 2975, 2998) and store_name in ('Default', 'visiondirect.co.uk')
where store_name = 'visiondirect.co.uk' -- Default - 'visiondirect.co.uk'
order by product_id, store_name;



-----------------------------------------------------------------------------

select product_lifecycle, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'  
group by product_lifecycle
order by product_lifecycle

select status, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'  
group by status
order by status

select telesales_only, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'  
group by telesales_only
order by telesales_only

select promotional_product, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'  
group by promotional_product
order by promotional_product

select visibility, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'  
group by visibility
order by visibility
