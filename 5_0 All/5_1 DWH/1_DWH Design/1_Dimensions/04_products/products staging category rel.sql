
SELECT p.entity_id product_id, p.name, p.status, p.product_lifecycle, p.visibility, p.promotional_product, p.telesales_only
FROM 
		(select entity_id, name, product_type, status, product_lifecycle, visibility, promotional_product, telesales_only
		from Landing.mag.catalog_product_entity_flat_aud
		where store_id = 0) p 
	LEFT JOIN 
		Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
where cpc.product_id is null
order by p.entity_id

--------------------------------------------------------------------------

	SELECT d1.entity_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
    FROM 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d1 
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d2 ON d1.parent_id = d2.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d3 ON d2.parent_id = d3.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d4 ON d3.parent_id = d4.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d5 ON d4.parent_id = d5.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d6 ON d5.parent_id = d6.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d7 ON d6.parent_id = d7.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d8 ON d7.parent_id = d8.entity_id
	-- order by name1, name2, name3, name4, name5, name6, name7, name8
	order by d1.entity_id;

--------------------------------------------------------------------------

select product_id, name, category,
	count(*) over (partition by product_id) num_rep
from
	(select distinct t1.product_id, t1.name, t2.category 
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			Landing.map.prod_category t2 on t1.category_name = t2.category_magento) t
order by num_rep desc, product_id, name, category;


select product_id, name, cl_type,
	count(*) over (partition by product_id) num_rep
from
	(select distinct t1.product_id, t1.name, t2.cl_type 
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			Landing.map.prod_cl_type t2 on t1.category_name = t2.cl_type_magento) t
order by num_rep desc, product_id, name, cl_type;


select product_id, name, cl_feature,
	count(*) over (partition by product_id) num_rep
from
	(select distinct t1.product_id, t1.name, t2.cl_feature 
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			Landing.map.prod_cl_feature t2 on t1.category_name = t2.cl_feature_magento) t
order by num_rep desc, product_id, name, cl_feature;

select cl_feature, cl_feature_magento,
	idETLBatchRun, ins_ts
from Landing.map.prod_cl_feature