
SELECT d
FROM
	(SELECT d = DATEADD(DAY, rn - 1, '2017-01-01')
	FROM 
		(SELECT TOP (DATEDIFF(DAY, '2017-01-01', '2017-12-31')) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
		FROM 
				sys.all_objects s1
			CROSS JOIN 
				sys.all_objects s2
		ORDER BY s1.object_id) x) y;