
SELECT RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, GETDATE())), 2) + ':00';

-- CONVERT(VARCHAR(8), pmt.pmtCreatedDate, 112) AS mk_Calendar_Created

select top 10000 document_date, convert(int, (CONVERT(VARCHAR(8), document_date, 112))), 
	datepart(hour, document_date), datepart(minute, document_date), 
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, document_date)), 2), 
	LEFT(CONVERT(TIME(0), document_date), 2)
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 2
	-- and order_id = 5105137
order by document_date desc

select top 10000 document_date, CONVERT(int, (CONVERT(varchar(8), document_date, 112))) date_int, 
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, document_date)), 2) hh, 
	case when (datepart(minute, document_date) between 1 and 29) then '00' else '30' end mm,
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, document_date)), 2) + ':' + case when (datepart(minute, document_date) between 1 and 29) then '00' else '30' end + ':00'  
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 2
	-- and order_id = 5105137
order by document_date desc

select RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, getutcdate())), 2) + ':' + case when (DATEPART(MINUTE, getutcdate()) between 1 and 29) then '00' else '30' end + ':00'  

drop TABLE #dim
go

CREATE TABLE #dim(
	[date]       DATE PRIMARY KEY, 
	[day]        AS DATEPART(DAY,      [date]),
	[month]      AS DATEPART(MONTH,    [date]),
	FirstOfMonth AS CONVERT(DATE, DATEADD(MONTH, DATEDIFF(MONTH, 0, [date]), 0)),
	[MonthName]  AS DATENAME(MONTH,    [date]),
	[week]       AS DATEPART(WEEK,     [date]),
	[ISOweek]    AS DATEPART(ISO_WEEK, [date]),
	[DayOfWeek]  AS DATEPART(WEEKDAY,  [date]),
	[quarter]    AS DATEPART(QUARTER,  [date]),
	[year]       AS DATEPART(YEAR,     [date]),
	FirstOfYear  AS CONVERT(DATE, DATEADD(YEAR,  DATEDIFF(YEAR,  0, [date]), 0)),
	Style112     AS CONVERT(CHAR(8),   [date], 112),
	Style101     AS CONVERT(CHAR(10),  [date], 101),
	Style103     AS CONVERT(CHAR(10),  [date], 103));

SET DATEFIRST 1;
SET DATEFORMAT mdy;
SET LANGUAGE US_ENGLISH;

DECLARE @StartDate DATE = '20000101', @NumberOfYears INT = 50;
DECLARE @CutoffDate DATE = DATEADD(YEAR, @NumberOfYears, @StartDate);


INSERT #dim([date]) 
	SELECT d
	FROM
		(SELECT d = DATEADD(DAY, rn - 1, @StartDate)
		 FROM 
			(SELECT TOP (DATEDIFF(DAY, @StartDate, @CutoffDate)) rn = ROW_NUMBER() OVER (ORDER BY s1.[object_id])
			FROM 
					sys.all_objects AS s1
				CROSS JOIN	
					sys.all_objects AS s2
			ORDER BY s1.[object_id]) x) y;


select date, style112, style101, style103, 
	day, 
	week, ISOweek, DayOfWeek,
	month, FirstOfMonth, MonthName, 
	quarter, 
	year, FirstOfYear
from #dim
where year = 2017


SELECT
  DateKey       = CONVERT(INT, Style112), --> YES
  [Date]        = [date], --> YES
  [Day]         = CONVERT(TINYINT, [day]), --> YES?
  DaySuffix     = CONVERT(CHAR(2), CASE WHEN [day] / 10 = 1 THEN 'th' ELSE 
                  CASE RIGHT([day], 1) WHEN '1' THEN 'st' WHEN '2' THEN 'nd' 
	              WHEN '3' THEN 'rd' ELSE 'th' END END),
  [Weekday]     = CONVERT(TINYINT, [DayOfWeek]), --> YES
  [WeekDayName] = CONVERT(VARCHAR(10), DATENAME(WEEKDAY, [date])), --> YES
  [IsWeekend]   = CONVERT(BIT, CASE WHEN [DayOfWeek] IN (6,7) THEN 1 ELSE 0 END), --> YES
  [IsHoliday]   = CONVERT(BIT, 0),
  HolidayText   = CONVERT(VARCHAR(64), NULL),
  [DOWInMonth]  = CONVERT(TINYINT, ROW_NUMBER() OVER 
                  (PARTITION BY FirstOfMonth, [DayOfWeek] ORDER BY [date])),
  [DayOfYear]   = CONVERT(SMALLINT, DATEPART(DAYOFYEAR, [date])),
  WeekOfMonth   = CONVERT(TINYINT, DENSE_RANK() OVER 
                  (PARTITION BY [year], [month] ORDER BY [week])),
  WeekOfYear    = CONVERT(TINYINT, [week]),
  ISOWeekOfYear = CONVERT(TINYINT, ISOWeek),
  [Month]       = CONVERT(TINYINT, [month]),
  [MonthName]   = CONVERT(VARCHAR(10), [MonthName]),
  [Quarter]     = CONVERT(TINYINT, [quarter]),
  QuarterName   = CONVERT(VARCHAR(6), CASE [quarter] WHEN 1 THEN 'First' 
                  WHEN 2 THEN 'Second' WHEN 3 THEN 'Third' WHEN 4 THEN 'Fourth' END), 
  [Year]        = [year],
  MMYYYY        = CONVERT(CHAR(6), LEFT(Style101, 2)    + LEFT(Style112, 4)),
  MonthYear     = CONVERT(CHAR(7), LEFT([MonthName], 3) + LEFT(Style112, 4)),
  FirstDayOfMonth     = FirstOfMonth,
  LastDayOfMonth      = MAX([date]) OVER (PARTITION BY [year], [month]),
  FirstDayOfQuarter   = MIN([date]) OVER (PARTITION BY [year], [quarter]),
  LastDayOfQuarter    = MAX([date]) OVER (PARTITION BY [year], [quarter]),
  FirstDayOfYear      = FirstOfYear,
  LastDayOfYear       = MAX([date]) OVER (PARTITION BY [year]),
  FirstDayOfNextMonth = DATEADD(MONTH, 1, FirstOfMonth),
  FirstDayOfNextYear  = DATEADD(YEAR,  1, FirstOfYear)
FROM #dim
where year = 2017
order by datekey


select CONVERT(INT, Style112) idCalendar_sk, 
	[date] calendar_date, CONVERT(TINYINT, [day]) calendar_date_num, Style103 calendar_date_name, 
	CONVERT(TINYINT, [DayOfWeek]) calendar_date_week, CONVERT(VARCHAR(10), DATENAME(WEEKDAY, [date])) calendar_date_week_name, 
	CONVERT(BIT, CASE WHEN [DayOfWeek] IN (6,7) THEN 1 ELSE 0 END) is_weekend, 
	CONVERT(TINYINT, [week]) week_year_num, convert(varchar, [year]) + ' - WK: ' + convert(varchar, [week]) week_year_name, 
	CONVERT(TINYINT, DENSE_RANK() OVER (PARTITION BY [year], [month] ORDER BY [week])) week_month_num, 
	LEFT([MonthName], 3) + ' - WK: ' +  CONVERT(VARCHAR, DENSE_RANK() OVER (PARTITION BY [year], [month] ORDER BY [week])) week_month_name, 
	CONVERT(TINYINT, [month]) month_num, CONVERT(VARCHAR(10), [MonthName]) month_name, convert(varchar, [year]) + ' - ' + LEFT([MonthName], 3) month_year_name, 
	CONVERT(TINYINT, [quarter]) quarter_num, 'QT - ' +  CONVERT(VARCHAR, [quarter]) quarter_name, convert(varchar, [year]) + ' - ' + 'QT - ' +  CONVERT(VARCHAR, [quarter]) quarter_year_name, 
	[year] year_num
from #dim
where year = 2017
order by idCalendar_sk

