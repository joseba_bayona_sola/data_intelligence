
-------------------------------------------------------
-- DW_GETLENSES

select count(*) -- 3.7M
from DW_GetLenses.dbo.order_headers
where document_type = 'ORDER'

select year(order_date) yyyy, count(*)
from DW_GetLenses.dbo.order_headers
where document_type = 'ORDER'
group by year(order_date)
order by year(order_date)

-------------------------------------------------------
-- DW_PROFORMA

select document_type, count(*) 
from DW_Proforma.dbo.order_headers
group by document_type 
order by document_type

select count(*) -- 1.4M
from DW_Proforma.dbo.order_headers
where document_type = 'ORDER'

select year(order_date) yyyy, count(*)
from DW_Proforma.dbo.order_headers
where document_type = 'ORDER'
group by year(order_date)
order by year(order_date)

select store_name, count(*)
from DW_Proforma.dbo.order_headers
where document_type = 'ORDER'
group by store_name
order by store_name

	select store_name, document_type, count(*)
	from DW_Proforma.dbo.order_headers
	where document_type <> 'ORDER'
	group by store_name, document_type
	order by document_type, store_name



-------------------------------------------------------
-- HIST

select count(*) -- 2.18M (1.5M already in Proforma)
from DW_GetLenses.dbo.dw_hist_order

select year(created_at), count(*)
from DW_GetLenses.dbo.dw_hist_order
group by year(created_at)
order by year(created_at)

select count(*) -- 4.26M (1.5M already in Proforma)
from DW_GetLenses.dbo.dw_hist_order_item

	-- ORDER ID - SHIPMENT ID: DO THEY MEET
		-- 10: Orders with no Shipments: Cancel? + Asdba Orders: 197750 -> 50197750
		-- 01: Asdba Orders: 197750 -> 50197750
	select type, count(*)
	from
		(select ho.order_id, hs.shipment_id, ho.source, 
			ho.created_at cr_o, hs.created_at cr_s, 
			ho.store_id, ho.customer_id, 
			case when (ho.order_id is null) then '01' else case when (hs.shipment_id is null) then '10' else '11' end end type
		from 
				DW_GetLenses.dbo.dw_hist_shipment hs
			full join
				DW_GetLenses.dbo.dw_hist_order ho on hs.shipment_id = ho.order_id and hs.customer_id = ho.customer_id) t
	group by type
	order by type


	-- Source - Store DIM Values
	select source, count(*), min(created_at), max(created_at), min(order_id), max(order_id)
	from DW_GetLenses.dbo.dw_hist_order
	group by source
	order by source

	select ho.source, ho.store_id, s.store_name, ho.num
	from
			(select source, store_id, count(*) num
			from DW_GetLenses.dbo.dw_hist_order
			group by source, store_id) ho
		left join
			DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
	order by ho.source, ho.store_id

	
	
select count(*) -- 2.10M (1.5M already in Proforma)
from DW_GetLenses.dbo.dw_hist_shipment

select count(*) -- 4.18M (1.5M already in Proforma)
from DW_GetLenses.dbo.dw_hist_shipment_item
