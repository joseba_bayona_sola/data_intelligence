
select count(*)
from Landing.mag.sales_flat_order_aud
where status = 'archived'
	and old_order_id is not null

select year(created_at), count(*)
from Landing.mag.sales_flat_order_aud
where status <> 'archived'
group by year(created_at)
order by year(created_at)

select o.store_id, s.name, count(*)
from 
		Landing.mag.sales_flat_order_aud o
	inner join
		Landing.mag.core_store_aud s on o.store_id = s.store_id
where status <> 'archived'
	and year(created_at) = 2014
group by o.store_id, s.name
order by o.store_id, s.name

select year(created_at), o.store_id, s.name, count(*)
from 
		Landing.mag.sales_flat_order_aud o
	inner join
		Landing.mag.core_store_aud s on o.store_id = s.store_id
where status <> 'archived'
group by year(created_at), o.store_id, s.name
order by year(created_at), o.store_id, s.name

-------------------------------

select count(*)
from Warehouse.sales.dim_order_header


select year(order_date), count(*)
from Warehouse.sales.dim_order_header
group by year(order_date)
order by year(order_date)
