
select *
from dw_flat.dw_hist_order
where source = 'lenson_nl_db' -- lensway_nl_db - lenson_nl_db
  -- and customer_id is null
order by created_at
limit 1000

select source, count(*), count(customer_id)
from dw_flat.dw_hist_order
group by source
order by source


-- ----------------------------------

select *
from lenson_nl.migrate_customerdata 
where new_magento_id = 2088869
limit 1000


-- ----------------------------------

select *
from dw_flat.dw_hist_order_item
where source = 'lenson_nl_db' -- lensway_nl_db - lenson_nl_db
order by order_id
limit 1000

select source, count(*), count(distinct order_id)
from dw_flat.dw_hist_order_item
group by source
order by source
