
select document_type, count(*)
from DW_Proforma.dbo.order_headers
group by document_type
order by document_type

	select document_type, store_name, count(*)
	from DW_Proforma.dbo.order_headers
	group by document_type, store_name
	order by document_type, store_name

select status, count(*)
from DW_Proforma.dbo.order_headers
group by status
order by status


select shipping_carrier, shipping_method, count(*)
from DW_Proforma.dbo.order_headers
group by shipping_carrier, shipping_method
order by shipping_carrier, shipping_method

select business_channel, count(*)
from DW_Proforma.dbo.order_headers
group by business_channel
order by business_channel

select payment_method, count(*)
from DW_Proforma.dbo.order_headers
group by payment_method
order by payment_method

select cc_type, count(*)
from DW_Proforma.dbo.order_headers
group by cc_type
order by cc_type

select price_type, count(*)
from DW_Proforma.dbo.order_lines
group by price_type
order by price_type


-----------------------------------------------------------------

select ol.product_id, p.name, count(*)
from 
		DW_Proforma.dbo.order_lines ol
	left join
		DW_GetLenses.dbo.products p on ol.product_id = p.product_id and p.store_name = 'default'
group by ol.product_id, p.name
order by ol.product_id, p.name

