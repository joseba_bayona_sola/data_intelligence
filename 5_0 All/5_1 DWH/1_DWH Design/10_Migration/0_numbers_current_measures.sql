
select top 1000 order_id, order_no, store_name, 
	local_subtotal_inc_vat, local_subtotal_exc_vat, 
	local_total_inc_vat, local_total_exc_vat, 
	local_to_global_rate, order_currency_code, 
	global_total_inc_vat, global_total_exc_vat, 
	prof_fee_percent, vat_percent
from DW_Proforma.dbo.order_headers
where document_type = 'ORDER'
	and (prof_fee_percent is not null or vat_percent is not null)

select top 1000 order_id, order_no, store_name, 
	local_subtotal_inc_vat, local_subtotal_exc_vat, 
	local_total_inc_vat, local_total_exc_vat, 
	local_to_global_rate, order_currency_code, 
	global_total_inc_vat, global_total_exc_vat, 
	prof_fee_percent, vat_percent
from DW_Proforma.dbo.order_headers
where document_type = 'ORDER'
	and store_name like 'visiooptik%' -- lensbase - lenshome - visiondirect - visiooptik

select top 1000 order_id, order_no, store_name, 
	local_price_inc_vat,
	local_line_subtotal_inc_vat, local_line_subtotal_exc_vat, 
	local_line_total_inc_vat, local_line_total_exc_vat, 
	local_to_global_rate, order_currency_code, 
	global_line_total_inc_vat, global_line_total_exc_vat, 
	prof_fee_percent, vat_percent
from DW_Proforma.dbo.order_lines
where document_type = 'ORDER'
