
select top 1000 ho.order_id, 
	ho.source, ho.store_id, ho.customer_id, 
	ho.created_at, ho.grand_total, 

	case 
		when (source = 'postoptics_db') then 'postoptics' 
		when (source in ('getlenses_db', 'getlenses_nl_')) then 'getlenses'
		when (source = 'masterlens_db') then 'masterlens'
		when (source = 'asda_db') then 'asda'
		when (source = 'vhi_db') then 'vh1'
	end migra_website_name, 'hist' etl_name, 
	s.store_id, s.store_name, 
	s2.store_id, s2.store_name, 
	c.entity_id
from 
		Landing.migra_hist.dw_hist_order ho
	left join
		Landing.aux.mag_gen_store_v s on ho.store_id = s.store_id -- ho.source <> 'masterlens_db' and 
	left join
		Landing.aux.mag_gen_store_v s2 on ho.source = 'masterlens_db' and s2.store_name like 'masterlens%' and s.tld = s2.tld 

	left join
		Landing.mag.customer_entity_flat_aud c on ho.customer_id = c.entity_id
where source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
	-- and c.entity_id is null
-- order by created_at
order by grand_total



select source, count(*)
from Landing.migra_hist.dw_hist_order
where source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
group by source
order by source

select source, store_id, count(*)
from Landing.migra_hist.dw_hist_order
where source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
group by source, store_id
order by source, store_id


----------------------------------------------

select top 1000 hoi.order_id, 
	hoi.source, 
	hoi.product_id, hoi.sku, 
	hoi.qty_ordered, hoi.row_total
from 
		Landing.migra_hist.dw_hist_order_item hoi
	inner join
		Landing.migra_hist.dw_hist_order ho on hoi.order_id = ho.order_id and 
			hoi.source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
order by ho.created_at

select top 1000 hoi.product_id, p.entity_id, p.name, count(*)
from 
		Landing.migra_hist.dw_hist_order_item hoi
	inner join
		Landing.migra_hist.dw_hist_order ho on hoi.order_id = ho.order_id and 
			hoi.source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
	left join
		Landing.mag.catalog_product_entity_flat_aud p on hoi.product_id = p.entity_id and p.store_id = 0
group by hoi.product_id, p.entity_id, p.name
order by hoi.product_id

select product_id, sku, count(*)
from Landing.migra_hist.dw_hist_order_item
where product_id in (-4, -3, -2, -1)
group by product_id, sku
order by product_id, sku

----------------------------------------------



select top 1000 ho.order_id, 
	ho.source, ho.store_id, ho.customer_id, 
	ho.created_at, ho.grand_total,  
	hoi.product_id, hoi.sku, 
	hoi.qty_ordered, hoi.row_total, 
	sum(hoi.row_total) over (partition by ho.order_id) sum_row_total
from 
		Landing.migra_hist.dw_hist_order_item hoi
	inner join
		Landing.migra_hist.dw_hist_order ho on hoi.order_id = ho.order_id and 
			hoi.source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db') 
order by ho.order_id

select top 1000 *, 
	abs(grand_total - sum_row_total) diff, 
	count(*) over () num_tot
from 
	(select ho.order_id, 
		ho.source, ho.store_id, ho.customer_id, 
		ho.created_at, ho.grand_total,  
		hoi.product_id, hoi.sku, 
		hoi.qty_ordered, hoi.row_total, 
		sum(hoi.row_total) over (partition by ho.order_id) sum_row_total
	from 
			Landing.migra_hist.dw_hist_order_item hoi
		inner join
			Landing.migra_hist.dw_hist_order ho on hoi.order_id = ho.order_id and 
				hoi.source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db')) t 
where abs(grand_total - sum_row_total) > 1
order by diff, order_id
