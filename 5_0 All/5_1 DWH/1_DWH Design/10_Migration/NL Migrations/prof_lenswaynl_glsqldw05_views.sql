
select count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v

	select top 1000 item_id, item_number, item_desc, 
		category_code, category_desc, 
		number_of_lenses, qty_in_ml, 
		power, bc, diam, axis, cyl, colour, [add], type, material
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v

select count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_productdata_v

	select top 1000 item_id, item_number, category_code, magento_id, 
		num_lenses, 
		base_curve, diameter, power, cylinder, axis, [add], dominant, color
	from DW_GetLenses_jbs.dbo.lenswaynl_productdata_v
	where magento_id <> ''
	order by magento_id

	select distinct magento_id, category_code, num_lenses
	from DW_GetLenses_jbs.dbo.lenswaynl_productdata_v
	where magento_id <> ''
	order by magento_id, num_lenses


select count(*), 
	count(distinct customer_id),
	count(distinct email)
from DW_GetLenses_jbs.dbo.lenswaynl_customerdata_v

	select top 1000 customer_id, 
		first_name, last_name, 
		email, creation_date, update_date,
		address1, address2, address3, address4, city, postal_code, country, telephone, 
		org_id, brand, marketing_via_email, marketing_via_sms, subscriber, 
		personal_number, party_id, party_type, 
		num_rep, num_rep_email
	from DW_GetLenses_jbs.dbo.lenswaynl_customerdata_v

select count(*), 
	count(distinct order_number),
	count(distinct customer_id), 
	min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v

	select top 1000 order_number, website_name, 
		customer_id, email_address, 
		order_date, shipping_date, 
		first_name, last_name, street_address, city, postal_code, country, phone_number, customers_dob, 
		payment_method, shipping_description, 
		base_grand_total, base_shipping_amount, base_discount_amount, total_order_discount, order_currency,
		order_source, order_category
	from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v

select count(*), 
	count(distinct line_id),
	count(distinct order_number)
from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v	

	select top 1000 line_id, order_number, 
		product_id, category_code, product_name, 
		eye, base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		ordered_quantity, row_total, row_subtotal, ol_discount, discount
	from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v		
	where product_id is null