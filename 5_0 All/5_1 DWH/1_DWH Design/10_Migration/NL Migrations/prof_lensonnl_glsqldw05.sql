
select count(*)
from DW_GetLenses_jbs.dbo.lensonnl_products

	select products_id, 
		ProductGroup, categories_name, products_name, products_model, 
		manufacturers_name, 
		available_for_purchase, to_be_disc_or_discontinued, 
		price_eur, 
		url, product_image, products_description, products_saletext, products_info, products_userinfo, products_expired_text
	from DW_GetLenses_jbs.dbo.lensonnl_products

select count(*)
from DW_GetLenses_jbs.dbo.lensonnl_product_mapping

	select top 1000 product_id, magento_id, multiplier, lenson_packsize, magento_packsize
	from DW_GetLenses_jbs.dbo.lensonnl_product_mapping
	order by magento_id

select count(*), 
	count(distinct customers_id),
	count(distinct customers_email_address)
from DW_GetLenses_jbs.dbo.lensonnl_customers

	select top 1000 customers_id, 
		customers_firstname, customers_lastname, 
		date_account_created, customers_email_address, 
		customers_dob, 
		entry_street_address, entry_postcode, entry_city, entry_country, customers_telephone, 
		customers_newsletter, customers_reminder, 
		customers_discount, customers_password
	from DW_GetLenses_jbs.dbo.lensonnl_customers

select count(*), 
	count(distinct orders_id),
	count(distinct customers_id), 
	min(date_purchased), max(date_purchased)
from DW_GetLenses_jbs.dbo.lensonnl_orders

	select top 1000 orders_id, 
		date_purchased, shipped, 
		customers_id, customers_name, customers_email_address, customers_dob,
		customers_street_address, customers_city, customers_postcode, customers_country, customers_telephone, 
		payment_method, delivery_method, 
		referers_name, referers_categories_name
	from DW_GetLenses_jbs.dbo.lensonnl_orders

select count(*), 
	count(distinct orders_products_id),
	count(distinct orders_id), 
	count(distinct products_id)
from DW_GetLenses_jbs.dbo.lensonnl_orders_products

	select top 1000 orders_products_id, 
		orders_id, 
		products_id, visma_id, products_name, products_model, info, ProductGroup,
		products_price, products_quantity, 
		total_orders_products_discount, 
		campaignName, campaignDesc, campaignType, campaignGivesFreeShippping
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products

select count(*), 
	count(distinct orders_total_id),
	count(distinct orders_id)
from DW_GetLenses_jbs.dbo.lensonnl_orders_total

	select top 1000 orders_total_id, 
		orders_id, class, title, value
	from DW_GetLenses_jbs.dbo.lensonnl_orders_total
	order by orders_id

