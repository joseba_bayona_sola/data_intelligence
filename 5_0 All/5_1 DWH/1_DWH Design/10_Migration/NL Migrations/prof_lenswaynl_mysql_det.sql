
select c1.customer_id, c2.email, c1.num_orders
from
  (select customer_id, count(*) num_orders
  from dw_flat.dw_hist_order
  where source = 'lensway_nl_db'
  group by customer_id) c1
left join  
  (select distinct new_magento_id, email
  from lensway_nl.migrate_customerdata) c2 on c1.customer_id = c2.new_magento_id
where c2.new_magento_id is null


-- ----------------------------------------

select * 
from lensway_nl.migrate_customerdata
where new_magento_id in (2209973)

select *
from lensway_nl.migrate_orderdata
where old_customer_id in (1751734)
order by old_customer_id, created_at desc

  select *
  from dw_flat.dw_hist_order
  where source = 'lensway_nl_db'
    and customer_id in (2209973)
    and created_at > '2015-09-01'
  order by customer_id, created_at desc;

-- ----------------------------------------

select oh1.old_order_id, oh1.magento_order_id, oh2.order_id,
  oh1.created_at, oh2.created_at,
  oh1.old_customer_id, oh1.magento_customer_id, oh2.customer_id
from 
    lensway_nl.migrate_orderdata oh1
  left join
  -- right join
    (select order_id, created_at, customer_id
    from dw_flat.dw_hist_order
    where source = 'lensway_nl_db'
      and created_at > '2015-09-01') oh2 on oh1.created_at = oh2.created_at and oh1.magento_customer_id = oh2.customer_id
-- where oh1.old_order_id is not null and oh2.order_id is not null order by oh1.created_at desc
where oh1.old_order_id is not null and oh2.order_id is null order by oh1.created_at desc
-- where oh1.old_order_id is null and oh2.order_id is not null order by oh2.created_at desc
limit 1000  
