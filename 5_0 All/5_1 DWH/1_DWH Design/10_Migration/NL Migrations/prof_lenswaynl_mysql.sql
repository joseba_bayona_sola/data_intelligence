
select count(*), count(distinct old_customer_id), count(distinct new_magento_id)
from lensway_nl.migrate_customerdata; 
  
  select *
  from lensway_nl.migrate_customerdata
  order by new_magento_id
  limit 1000; 

  select old_customer_id, 
  	new_magento_id, email, created_at, 
  	domainID, domainName, 
  	unsubscribe_newsletter, disable_saved_card, RAF_code
  from lensway_nl.migrate_customerdata
  order by new_magento_id
  limit 1000; 
  
  select new_magento_id, count(*)
  from lensway_nl.migrate_customerdata
  group by new_magento_id
  order by count(*) desc
  
select count(*), count(distinct magento_customer_id), count(distinct old_customer_id), 
  min(created_at), max(created_at)
from lensway_nl.migrate_orderdata
where magento_customer_id <> 0;

  select *
  from lensway_nl.migrate_orderdata
  order by created_at
  limit 1000; 
  
  select old_order_id, 
  	magento_order_id, new_increment_id, 
  	store_id, website_name, domainID, domainName, 
  	email, old_customer_id, magento_customer_id, 
  	created_at,
  	migrated, -- orderStatusName, 
  	shipping_description, -- channel, source_code, 
  	billing_postcode, billing_country, shipping_postcode, shipping_country, 
  	base_grand_total, base_shipping_amount, base_discount_amount -- , 
  	-- base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
  	-- tax_rate, tax_free_amount, 
  	-- order_currency, exchangeRate
  from lensway_nl.migrate_orderdata
  order by created_at desc
  limit 1000; 

  select created_at, old_customer_id, count(*)
  from lensway_nl.migrate_orderdata
  group by created_at, old_customer_id
  having count(*) > 1
  order by count(*) desc;
  
select count(*), count(distinct magento_order_id)
from lensway_nl.migrate_orderlinedata; 

  select *
  from lenson_nl.migrate_orderlinedata
  limit 1000; 

  select old_item_id, 
  	old_order_id, magento_order_id, 
  	product_id, productName, packsize, eye, -- productID, 
  	base_curve, diameter, power, cylinder, axis, "add", dominant, color, 
  	quantity, row_total
  from lensway_nl.migrate_orderlinedata
  limit 1000; 

  select product_id, count(*)
  from lensway_nl.migrate_orderlinedata
  group by product_id
  order by product_id
  
    select *
    from lensway_nl.migrate_orderlinedata
    where product_id is null
    order by old_product_id
    
-- ---------------------------------------------

select source, count(*), count(distinct customer_id), min(created_at), max(created_at), count(distinct created_at)
from dw_flat.dw_hist_order
where source = 'lensway_nl_db';

  select *
  from dw_flat.dw_hist_order
  where source = 'lensway_nl_db'
  order by created_at desc
  limit 1000;

  select created_at, customer_id, count(*)
  from dw_flat.dw_hist_order
  where source = 'lensway_nl_db'
  group by created_at, customer_id
  having count(*) > 1
  order by count(*) desc;
  
select source, count(*), count(distinct order_id)
from dw_flat.dw_hist_order_item
where source = 'lensway_nl_db';

  select *
  from dw_flat.dw_hist_order_item
  where source = 'lensway_nl_db'

-- ---------------------------------------------
