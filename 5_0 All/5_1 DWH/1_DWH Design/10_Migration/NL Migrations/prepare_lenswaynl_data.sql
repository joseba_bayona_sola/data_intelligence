
	-- Views with first set of data

	select order_number, website_name, 
		customer_id, email_address, 
		order_date, 
		postal_code, country, 
		payment_method, shipping_description, 
		base_grand_total, base_shipping_amount, base_discount_amount, order_currency
	into #lenswaynl_oh_export
	from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v

	select line_id, order_number, 
		product_id, category_code, product_name, 
		eye, base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		ordered_quantity, row_subtotal, ol_discount
	into #lenswaynl_ol_export	
	from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v		


	-------------------------------


	select distinct magento_id, category_code, num_lenses
	from DW_GetLenses_jbs.dbo.lenswaynl_productdata_v
	where magento_id <> ''
	order by magento_id, num_lenses

	select *
	from #lenswaynl_oh_export

	select *
	from #lenswaynl_ol_export



	-- Views with second set of data (delta 2)

	select t1.order_number, website_name, 
		customer_id, email_address, 
		order_date, 
		postal_code, country, 
		payment_method, shipping_description, 
		base_grand_total, base_shipping_amount, base_discount_amount, order_currency
	from 
		DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v t1
	left join	
		(select order_number
		from #lenswaynl_oh_export) t2 on t1.order_number = t2.order_number
	where t2.order_number is null
		and t1.order_number = 51332649
	order by order_date desc

	select line_id, t1.order_number, 
		product_id, category_code, product_name, 
		eye, base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
		ordered_quantity, row_subtotal, ol_discount
	from 
		DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v t1
	left join	
		(select order_number
		from #lenswaynl_oh_export) t2 on t1.order_number = t2.order_number
	where t2.order_number is null
		and t1.order_number = 51332649
	order by order_date desc

	-------------------------------


	drop table #lenswaynl_oh_export

	drop table #lenswaynl_ol_export