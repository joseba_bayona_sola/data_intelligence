
select count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
-- from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2

	select top 1000 id, item_id, item_number, item_desc, 
		category_code, category_desc, 
		number_of_lenses, qty_in_ml, 
		power, bc, diam, axis, cyl, colour, [add], type, material
	-- from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2

select count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping

	select product_id, category_code, magento_id
	from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping
	where magento_id <> ''
	order by magento_id

select count(*), 
	count(distinct customer_id),
	count(distinct email)
-- from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
-- from DW_GetLenses_jbs.dbo.lenswaynl_customer_full_delta
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full_delta_2

	select top 1000 customer_id, 
		first_name, last_name, 
		email, creation_date,
		address1, address2, address3, address4, city, postal_code, country, telephone, 
		brand, org_id, personal_number, party_id, party_type, 
		marketing_via_email, marketing_via_sms, subscriber, 
		last_update_date, p_last_update_date
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	order by customer_id

select count(*), 
	count(distinct order_number),
	count(distinct customer_id), 
	min(order_date), max(order_date)
-- from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
-- from DW_GetLenses_jbs.dbo.lenswaynl_oh_full_delta
from DW_GetLenses_jbs.dbo.lenswaynl_oh_full_delta_2

	select top 10000 brand, order_number, 
		order_date, shipping_date, 
		order_source, 
		customer_id, email_address, 
		first_name, last_name, street_address, city, postal_code, country, phone_number, customers_dob, 
		payment_terms, ship_method, order_category
	-- from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	-- from DW_GetLenses_jbs.dbo.lenswaynl_oh_full_delta
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full_delta_2
	order by order_date desc

select count(*), 
	count(distinct line_id),
	count(distinct order_number), 
	count(distinct inventory_item_id)
-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta_2

	select top 1000 line_id, order_number, 
		inventory_item_id, item_number, category_desc, 
		-- base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges, 
		ordered_quantity, unit_list_price, list_price, unit_selling_price, 
		sales_inc_vat, 
		discount, 
		total_order_price, total_order_discount
	-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta
	-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta_2

