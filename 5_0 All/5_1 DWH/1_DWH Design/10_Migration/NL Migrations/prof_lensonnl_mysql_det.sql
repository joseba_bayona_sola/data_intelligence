
select c1.customer_id, c2.old_customer_id, c2.email, c1.num_orders
from
  (select customer_id, count(*) num_orders
  from dw_flat.dw_hist_order
  where source = 'lenson_nl_db'
  group by customer_id) c1
left join  
  lenson_nl.migrate_customerdata c2 on c1.customer_id = c2.new_magento_id
-- where c2.old_customer_id is null


  select oh.old_order_id, 
  	oh.magento_order_id, oh.new_increment_id, 
  	oh.email, oh.old_customer_id, oh.magento_customer_id, c.new_magento_id,
  	oh.created_at
  from 
      lenson_nl.migrate_orderdata oh
    left join
      lenson_nl.migrate_customerdata c on oh.old_customer_id = c.old_customer_id
  where oh.magento_customer_id = 0
    -- and c.new_magento_id is null
  order by oh.created_at; 

-- ----------------------------------------

select * 
from lenson_nl.migrate_customerdata
where new_magento_id in (2126523, 2130743)

select *
from lenson_nl.migrate_orderdata
where old_customer_id in (993401, 1031931)
order by old_customer_id, created_at

  select *
  from dw_flat.dw_hist_order
  where source = 'lenson_nl_db'
    and customer_id in (2126523, 2130743)
  order by customer_id, created_at;

-- ----------------------------------------

select oh1.old_order_id, oh1.magento_order_id, oh2.order_id,
  oh1.created_at, oh2.created_at,
  oh1.old_customer_id, oh1.magento_customer_id, oh2.customer_id
from 
    (select oh.old_order_id, oh.magento_order_id, 
      	oh.created_at,
        oh.old_customer_id, c.new_magento_id magento_customer_id   	
      from 
          lenson_nl.migrate_orderdata oh
        left join
          lenson_nl.migrate_customerdata c on oh.old_customer_id = c.old_customer_id) oh1
  -- left join
  right join
    (select order_id, created_at, customer_id
    from dw_flat.dw_hist_order
    where source = 'lenson_nl_db') oh2 on oh1.created_at = oh2.created_at and oh1.magento_customer_id = oh2.customer_id 
-- where oh1.old_order_id is not null and oh2.order_id is not null order by oh1.created_at desc
-- where oh1.old_order_id is not null and oh2.order_id is null order by oh1.created_at desc
where oh1.old_order_id is null and oh2.order_id is not null order by oh2.created_at desc
limit 1000  


select oh1.old_order_id, oh1.magento_order_id, oh2.order_id,
  oh1.created_at, oh2.created_at,
  oh1.old_customer_id, oh1.magento_customer_id, oh2.customer_id
from 
    (select *
    from lenson_nl.migrate_orderdata
    where magento_customer_id <> 0) oh1
  left join
  -- right join
    (select order_id, created_at, customer_id
    from dw_flat.dw_hist_order
    where source = 'lenson_nl_db') oh2 on oh1.created_at = oh2.created_at and oh1.magento_customer_id = oh2.customer_id 
-- where oh1.old_order_id is not null and oh2.order_id is not null order by oh1.created_at desc
where oh1.old_order_id is not null and oh2.order_id is null order by oh1.created_at desc
-- where oh1.old_order_id is null and oh2.order_id is not null order by oh2.created_at desc
limit 1000  




  
