
select domain, store_type
from DW_Proforma.dbo.DomainStoreTypes

select orderStatusName, descriptions, 
	dw_status, 
	dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment
from DW_Proforma.dbo.mapping_order_status
order by dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment

select old_shipping_carrier, 
	new_shipping_carrier, new_shipping_method
from DW_Proforma.dbo.mapping_shipping_carrier_method

