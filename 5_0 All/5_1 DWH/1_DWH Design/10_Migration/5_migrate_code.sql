
-------------------------------------------------------------
-- Vision Direct - Lensbase - Lenshome - Visio Optik
select top 1000 count(*) over () num_tot,
	migra_website_name, etl_name,
	moi.order_id, moi.proforma_order_id, oh.order_no, moi.num_order_line,
	moi.source, 
	moi.store_id, 
	moi.customer_id_hist, oh.customer_id, moi.customer_email, 
	moi.created_at 
from
		(select migra_website_name, etl_name,
			order_id, 600000000 + order_id proforma_order_id, num_order_line, -- vd: 700000000 - lb: 800000000 - lh: 900000000 - visioOptik = 600000000
			source, 
			store_id, 
			customer_id_hist, customer_id, customer_email, 
			created_at 
		from Landing.aux.migra_migrate_order_item_v) moi
	inner join
		Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
	left join
		Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
-- where oh.order_id is null
where c.entity_id is null
order by created_at desc

select top 1000 count(*) over () num_tot,
	migra_website_name, etl_name,
	moi.order_id, moi.proforma_order_id, oh.order_no, o.entity_id, moi.num_order_line,
	moi.source, 
	moi.store_id, 
	moi.customer_id_hist, oh.customer_id, moi.customer_email, 
	moi.created_at 
from
		(select migra_website_name, etl_name,
			order_id, 700000000 + order_id proforma_order_id, num_order_line,
			source, 
			store_id, 
			customer_id_hist, customer_id, customer_email, 
			created_at 
		from Landing.aux.migra_migrate_order_item_v) moi
	inner join
		Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
	left join
		Landing.mag.sales_flat_order_aud o on o.status = 'archived' and moi.customer_email = o.customer_email and moi.created_at = o.created_at
-- where oh.order_id is null
order by created_at desc
	
	
------------------------------------------------------------

-- Lensway - Lenson

select top 1000 count(*) over () num_tot,
	migra_website_name, etl_name,
	moi.order_id, null proforma_order_id, o.increment_id order_no, o.entity_id, moi.num_order_line,
	moi.source, 
	moi.store_id, 
	moi.customer_id_hist, o.customer_id, moi.customer_email, 
	moi.created_at 
from
		(select migra_website_name, etl_name,
			order_id, num_order_line,
			source, 
			store_id, 
			customer_id_hist, customer_id, customer_email, 
			created_at 
		from Landing.aux.migra_migrate_order_item_v) moi
	left join
		Landing.mag.sales_flat_order_aud o on o.status = 'archived' and moi.customer_email = o.customer_email and moi.created_at = o.created_at
-- where moi.order_id = 18618712313
where 

select migra_website_name, order_id, count(distinct entity_id) 
from
	(select 
		migra_website_name, etl_name,
		moi.order_id, null proforma_order_id, o.increment_id order_no, o.entity_id, moi.num_order_line,
		moi.source, 
		moi.store_id, 
		moi.customer_id_hist, o.customer_id, moi.customer_email, 
		moi.created_at 
	from
			(select migra_website_name, etl_name,
				order_id, num_order_line,
				source, 
				store_id, 
				customer_id_hist, customer_id, customer_email, 
				created_at 
			from Landing.aux.migra_migrate_order_item_v) moi
		left join
			Landing.mag.sales_flat_order_aud o on o.status = 'archived' and moi.customer_email = o.customer_email and moi.created_at = o.created_at) t
group by migra_website_name, order_id
having count(distinct entity_id) > 1
order by count(distinct entity_id) desc, order_id

----------------------------------------------------------------------------

select moi.*, c.entity_id, c.old_access_cust_no, c.old_customer_id
from
		(select distinct migra_website_name, etl_name,
			customer_id_hist, customer_id, customer_email
		from Landing.aux.migra_migrate_order_item_v) moi
	left join
		Landing.mag.customer_entity_flat_aud c on moi.customer_email = c.email
