
select old_customer_id, new_magento_id, 
	email, password, 
	created_at, 
	-- store_id, website_group_id, 
  domainid, domainName, 
	-- store_credit_amount, store_credit_comment, -- target_store_credit_amoount, 
	unsubscribe_newsletter, disable_saved_card, raf_code, 
	migrated
from vd150324.migrate_customerdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson'
limit 1000 



-- ----------------------------------------------------------------------------

select old_order_id, 
	magento_order_id, new_increment_id, 
	store_id, website_name, domainID, domainName, 
	email, old_customer_id, magento_customer_id, 
	created_at, 
	migrated, 
	shipping_description,
	base_grand_total, base_shipping_amount, base_discount_amount, 
	order_currency
from lh150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson'
-- where magento_order_id <> 0
limit 1000 


select old_order_id, 
	magento_order_id, new_increment_id, 
  created_at, -- invoice_date, shipment_date, 
  orderStatusName, 
  tax_rate, tax_free_amount, 
  base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
  exchangeRate
from lb150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 
-- where magento_order_id <> 0
limit 1000 

  select orderStatusName, count(*)
  from lb150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 
  group by orderStatusName
  order by orderStatusName

  select tax_rate, count(*)
  from vd150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 
  group by tax_rate
  order by tax_rate

  select exchangeRate, count(*)
  from VisioOptik.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 
  group by exchangeRate
  order by exchangeRate


select old_order_id, 
	magento_order_id, new_increment_id, 
  created_at, 
  channel, source_code
from lb150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 
where channel <> ''
limit 1000 


select old_order_id, 
	magento_order_id, new_increment_id, 
  created_at, 
  billing_prefix, billing_firstname, billing_lastname, billing_street1, billing_street2, 
  -- billing_city, billing_region, billing_region_id, 
  billing_postcode, billing_country, billing_telephone
from vd150324.migrate_orderdata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson'
limit 1000 

-- ----------------------------------------------------------------------------

select old_item_id, 
  old_order_id, magento_order_id, 
  -- store_id, migrated, priority, debug, 
  -- old_product_id, 
  product_id, productCode, productName, productCodeFull, packsize, 
  eye, 
  base_curve, diameter, power, cylinder, axis, color, -- [add], dominant, params
  quantity, row_total
from lb150324.migrate_orderlinedata -- 'lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson'
limit 1000 

-- ----------------------------------------------------------------------------

select internal_order_id, user_visible_order_id, has_lenses, has_solutions
from lb150324.migrate_order_id_mapping -- 'lb150324', 'lh150324', 'vd150324', 
limit 1000 


select count(*)
from magento01.sales_flat_order
where old_order_id is not null

select status, count(*)
from magento01.sales_flat_order
where old_order_id is not null
group by status
order by status

