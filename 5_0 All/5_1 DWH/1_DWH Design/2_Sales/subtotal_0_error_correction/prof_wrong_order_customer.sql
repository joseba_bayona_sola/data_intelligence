
select top 1000 *
from Warehouse.sales.dim_order_header_v
where customer_id = 205741
order by order_date

select top 1000 *
from Warehouse.act.fact_activity_sales
where idCustomer_sk_fk = 407744
order by activity_date

select top 1000 *
from Warehouse.act.fact_activity_sales_v
where customer_id = 205741



select top 1000 *
from Warehouse.act.fact_customer_signature_aux_orders
where idCustomer_sk_fk = 407744
order by activity_date


select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where customer_id_bk = 205741
order by order_date

select top 1000 order_id_bk, order_no, order_date, 
	countries_registered_code, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	vat_percent, prof_fee_percent
from Landing.aux.sales_dim_order_header_aud
where customer_id_bk = 205741
order by order_date

select top 1000 *
from Landing.aux.sales_fact_order_line_aud
where order_id_bk in 
	(select order_id_bk
	from Landing.aux.sales_dim_order_header_aud
	where customer_id_bk = 205741)
order by invoice_date

select top 1000 order_line_id_bk, order_id_bk, 
	countries_registered_code, product_type_vat, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	discount_percent,
	vat_percent, prof_fee_percent, vat_rate, prof_fee_rate, 
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
from Landing.aux.sales_fact_order_line_aud
where order_id_bk in 
	(select order_id_bk
	from Landing.aux.sales_dim_order_header_aud
	where customer_id_bk = 205741)
order by invoice_date
