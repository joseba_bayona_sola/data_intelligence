
select count(*)
from Landing.aux.sales_dim_order_header_aud

select count(*)
from Warehouse.sales.dim_order_header

select count(*)
from Warehouse.act.fact_activity_sales

-- 4950
select s.*
from 
		Landing.aux.sales_dim_order_header_aud s
	left join
		Warehouse.sales.dim_order_header oh on s.order_id_bk = oh.order_id_bk
where oh.order_id_bk is null
order by order_date

-- 67
select s.*
from 
		Warehouse.act.fact_activity_sales s
	left join
		Warehouse.sales.dim_order_header oh on s.order_id_bk = oh.order_id_bk
where oh.order_id_bk is null
order by activity_date

select 9432572 - 9432505

select 9437455 - 9432505


------------------------------------------------------------------

select s.order_id_bk
into #wrong_orders
from 
		Landing.aux.sales_dim_order_header_aud s
	left join
		Warehouse.sales.dim_order_header oh on s.order_id_bk = oh.order_id_bk
where oh.order_id_bk is null


-- Landing

delete from Landing.aux.sales_fact_order_line_aud
where order_id_bk in 
	(select order_id_bk
	from #wrong_orders)

delete from Landing.aux.sales_dim_order_header_aud
where order_id_bk in 
	(select order_id_bk
	from #wrong_orders)

-- Warehouse
delete from Warehouse.act.fact_activity_sales
where order_id_bk in 
	(select order_id_bk
	from #wrong_orders)
