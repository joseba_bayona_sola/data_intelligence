
select top 1000 *
from Warehouse.act.fact_activity_sales
where idETLBatchRun_upd = 2587
	and local_total_exc_vat is null
	-- and local_store_credit_used is null
	-- and local_discount is null
	-- and order_currency_code is null
order by idCustomer_sk_fk

select top 1000 count(*)
from Warehouse.act.fact_activity_sales
where idETLBatchRun_upd = 2580
	and local_total_exc_vat is null

select top 1000 idETLBatchRun_upd, count(*)
from Warehouse.act.fact_activity_sales
where local_total_exc_vat is null
group by idETLBatchRun_upd
order by idETLBatchRun_upd


select top 1000 count(*)
from Landing.aux.sales_dim_order_header_aud
where local_total_exc_vat is null

select top 1000 *
from Landing.aux.sales_fact_order_line_aud
where order_id_bk = 11

select top 1000 *
from Warehouse.sales.fact_order_line
where order_id = 11

---

select top 1000 *
from 
		Warehouse.sales.dim_order_header oh
	inner join
		Warehouse.act.fact_activity_sales a on oh.order_id_bk = a.order_id_bk
where 
	oh.local_total_inc_vat <> a.local_total_inc_vat or (oh.local_total_exc_vat <> a.local_total_exc_vat)

----------------------------------------------------------
----------------------------------------------------------

select order_id_bk, idCustomer_sk_fk,
	countries_registered_code, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	vat_percent, prof_fee_percent
into #oh_measures
from Warehouse.sales.dim_order_header

	-- MERGE STATEMENT 
	merge into Warehouse.act.fact_activity_sales with (tablock) as trg
	using #oh_measures src
		on (trg.order_id_bk = src.order_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0)
		intersect
		select 
			isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0))			
		then 
			update set
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat; 

	-- MERGE STATEMENT 
	merge into Landing.aux.sales_dim_order_header_aud with (tablock) as trg
	using #oh_measures src
		on (trg.order_id_bk = src.order_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.local_total_exc_vat, 0), isnull(trg.local_total_vat, 0), isnull(trg.local_total_prof_fee, 0), 
			isnull(trg.local_total_aft_refund_exc_vat, 0), isnull(trg.local_total_aft_refund_vat, 0), isnull(trg.local_total_aft_refund_prof_fee, 0), 
			isnull(trg.vat_percent, 0), isnull(trg.prof_fee_percent, 0)
		intersect
		select 
			isnull(src.local_total_exc_vat, 0), isnull(src.local_total_vat, 0), isnull(src.local_total_prof_fee, 0), 
			isnull(src.local_total_aft_refund_exc_vat, 0), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, 0), 
			isnull(src.vat_percent, 0), isnull(src.prof_fee_percent, 0))			
		then 
			update set
				trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee,
				trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
				trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent;

---------------------------------------

select order_line_id_bk, 
	-- countries_registered_code, product_type_vat, 
	local_shipping, local_store_credit_used, -- local_subtotal, local_discount, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_shipping_refund, local_store_credit_used_refund, local_adjustment_refund, --local_subtotal_refund, local_discount_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	-- discount_percent,
	vat_percent, prof_fee_percent, vat_rate, prof_fee_rate, 
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
into #ol_measures
from Warehouse.sales.fact_order_line

	-- MERGE STATEMENT 
	merge into Landing.aux.sales_fact_order_line_aud with (tablock) as trg
	using #ol_measures src
		on (trg.order_line_id_bk = src.order_line_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.local_shipping, -1), isnull(trg.local_store_credit_used, -1), 
			isnull(trg.local_total_inc_vat, -1), isnull(trg.local_total_exc_vat, -1), isnull(trg.local_total_vat, -1), isnull(trg.local_total_prof_fee, -1), 
			isnull(trg.local_store_credit_given, -1), isnull(trg.local_bank_online_given, -1), 
			isnull(trg.local_shipping_refund, -1), isnull(trg.local_store_credit_used_refund, -1), isnull(trg.local_adjustment_refund, -1), 
			isnull(trg.local_total_aft_refund_inc_vat, -1), isnull(trg.local_total_aft_refund_exc_vat, -1), isnull(trg.local_total_aft_refund_vat, -1), isnull(trg.local_total_aft_refund_prof_fee, -1), 
			isnull(trg.vat_percent, -1), isnull(trg.prof_fee_percent, -1), isnull(trg.vat_rate, -1), isnull(trg.prof_fee_rate, -1), 
			isnull(trg.order_allocation_rate, -1), isnull(trg.order_allocation_rate_aft_refund, -1), isnull(trg.order_allocation_rate_refund, -1)
		intersect
		select 
			isnull(src.local_shipping, -1), isnull(src.local_store_credit_used, -1), 
			isnull(src.local_total_inc_vat, -1), isnull(src.local_total_exc_vat, -1), isnull(src.local_total_vat, -1), isnull(src.local_total_prof_fee, -1), 
			isnull(src.local_store_credit_given, -1), isnull(src.local_bank_online_given, -1), 
			isnull(src.local_shipping_refund, -1), isnull(src.local_store_credit_used_refund, -1), isnull(src.local_adjustment_refund, -1), 
			isnull(src.local_total_aft_refund_inc_vat, -1), isnull(src.local_total_aft_refund_exc_vat, -1), isnull(src.local_total_aft_refund_vat, 0), isnull(src.local_total_aft_refund_prof_fee, -1), 
			isnull(src.vat_percent, -1), isnull(trg.prof_fee_percent, -1), isnull(src.vat_rate, -1), isnull(src.prof_fee_rate, -1), 
			isnull(src.order_allocation_rate, -1), isnull(src.order_allocation_rate_aft_refund, -1), isnull(src.order_allocation_rate_refund, -1))			
		then 
			update set
				trg.local_shipping = src.local_shipping, trg.local_store_credit_used = src.local_store_credit_used, 
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, trg.local_total_vat = src.local_total_vat, trg.local_total_prof_fee = src.local_total_prof_fee,
				trg.local_store_credit_given = src.local_store_credit_given, trg.local_bank_online_given = src.local_bank_online_given, 
				trg.local_shipping_refund = src.local_shipping_refund, trg.local_store_credit_used_refund = src.local_store_credit_used_refund, trg.local_adjustment_refund = src.local_adjustment_refund, 
				trg.local_total_aft_refund_inc_vat = src.local_total_aft_refund_inc_vat, trg.local_total_aft_refund_exc_vat = src.local_total_aft_refund_exc_vat, 
				trg.local_total_aft_refund_vat = src.local_total_aft_refund_vat, trg.local_total_aft_refund_prof_fee = src.local_total_aft_refund_prof_fee, 
				trg.vat_percent = src.vat_percent, trg.prof_fee_percent = src.prof_fee_percent, trg.vat_rate = src.vat_rate, trg.prof_fee_rate = src.prof_fee_rate,
				trg.order_allocation_rate = src.order_allocation_rate, trg.order_allocation_rate_aft_refund = src.order_allocation_rate_aft_refund, trg.order_allocation_rate_refund = src.order_allocation_rate_refund;
