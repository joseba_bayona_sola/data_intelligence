
-- ORDER TYPE

select automatic_reorder, count(*)
from Landing.mag.sales_flat_order_aud
group by automatic_reorder
order by automatic_reorder

select postoptics_source, count(*)
from Landing.mag.sales_flat_order_aud
group by postoptics_source
order by postoptics_source

-- ORDER STATUS MAGENTO
select status, count(*)
from Landing.mag.sales_flat_order_aud
group by status
order by status

-- PAYMENT METHOD
select method, count(*)
from Landing.mag.sales_flat_order_payment_aud
group by method
order by method

select payment_method, payment_method_name
from openquery(MAGENTO, 'select * from dw_flat.dw_payment_method_map')
order by payment_method_name, payment_method


-- CREDIT CARD TYPE
select cc_type, count(*)
from Landing.mag.sales_flat_order_payment_aud
group by cc_type
order by cc_type

select card_type, card_type_name
from openquery(MAGENTO, 'select * from dw_flat.dw_card_type_map')
order by card_type, card_type_name


-- SHIPPING CARRIER - METHOD
select shipping_description, count(*) num
from Landing.mag.sales_flat_order_aud
where status = 'archived'
group by shipping_description
order by shipping_description


-- TELESALE
select telesales_admin_username, count(*)
from Landing.mag.sales_flat_order_aud
group by telesales_admin_username
order by telesales_admin_username

select u.user_id, o.telesales_admin_username, o.num, 
	u.firstname, u.lastname
from
		(select telesales_admin_username, count(*) num
		from Landing.mag.sales_flat_order_aud
		group by telesales_admin_username) o
	left join
		Landing.mag.admin_user_aud u on o.telesales_admin_username = u.username
order by o.telesales_admin_username



-- AUTO VERIFICATION
select postoptics_auto_verification, count(*)
from Landing.mag.sales_flat_order_aud
group by postoptics_auto_verification
order by postoptics_auto_verification

-- PRESC VERIFICATION METHOD
select presc_verification_method, count(*)
from Landing.mag.sales_flat_order_aud
group by presc_verification_method
order by presc_verification_method

-- TELESALES METHOD CODE
select telesales_method_code, count(*)
from Landing.mag.sales_flat_order_aud
group by telesales_method_code
order by telesales_method_code