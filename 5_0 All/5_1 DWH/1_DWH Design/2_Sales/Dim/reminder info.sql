
select reminder_type reminder_type_name_bk, UPPER(reminder_type) reminder_type_name
from
	(select reminder_type, count(*) num
	from Landing.mag.sales_flat_order_aud
	group by reminder_type) t
where reminder_type in ('both', 'email', 'sms', 'none', 'reorder')
order by reminder_type


select top 1000 reminder_period, reminder_period / 30, reminder_period % 30, 
	count(*) num
from Landing.mag.sales_flat_order_aud
where reminder_period is not null
group by reminder_period
-- order by reminder_period % 30, reminder_period
order by num desc

select reminder_period_int reminder_period_bk,
	case 
		when len(reminder_period) = 2 then '00' + reminder_period
		when len(reminder_period) = 3 then '0' + reminder_period
		else reminder_period
	end reminder_period_name
from
	(select reminder_period, cast(reminder_period as int) reminder_period_int, 
		count(*) num
	from Landing.mag.sales_flat_order_aud
	where reminder_period is not null
		and (reminder_period % 30 = 0) or (reminder_period % 15 = 0)
	group by reminder_period) t
order by reminder_period_name

select top 1000 reminder_period, cast(reminder_period as int) reminder_period_int, reminder_period / 30, reminder_period % 30, 
	count(*) num
from Landing.mag.sales_flat_order_aud
where reminder_period is not null
	and not ((reminder_period % 30 = 0) or (reminder_period % 15 = 0))
group by reminder_period
order by reminder_period_int
