

        (CASE
            WHEN
                (((`oh`.`presc_verification_method` = 'call') OR ISNULL(`oh`.`presc_verification_method`))
                    AND (`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN 'Call'
            WHEN
                ((`oh`.`presc_verification_method` = 'upload')
                    AND (`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN 'Upload / Fax / Scan'
            WHEN 
                ((`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` = 'Yes'))
            THEN 'Automatic'
            ELSE 'Not Required'
        END) AS `presc_verification_method`,

select top 1000 *
from Landing.mag.sales_flat_order
order by created_at desc

select top 1000 *
from Landing.mag.sales_flat_order_item
order by created_at desc


select top 1000 presc_verification_method, postoptics_auto_verification, count(*)
from Landing.mag.sales_flat_order_aud
where store_id = 20
group by presc_verification_method, postoptics_auto_verification
order by presc_verification_method, postoptics_auto_verification


select *
from Landing.aux.mag_core_config_data_store
where path = 'prescription/settings/enable'
order by store_id

select oh.*
from
		(select top 1000 *
		from Landing.mag.sales_flat_order_aud
		where store_id = 26
			and presc_verification_method = 'call') oh
	inner join
		Landing.mag.sales_flat_order_item_aud oi on oh.entity_id = oi.order_id
where oi.is_lens = 1
order by created_at desc