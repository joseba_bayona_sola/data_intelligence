
-- CHANNEL

select affilCode, count(*)
from Landing.mag.sales_flat_order_aud
group by affilCode
order by affilCode

select o.affilCode, sm.source_code, sm.channel, o.num
from
		(select affilCode, count(*) num
		from Landing.mag.sales_flat_order_aud
		group by affilCode) o
	left join
		openquery(MAGENTO, 'select * from dw_flat.dw_bis_source_map') sm on o.affilCode = sm.source_code
--where sm.source_code is null
order by channel, o.affilCode


-----------------------------------------------------------

-- Google Analytics
	-- Medium: cpc - email - referral - ppc - organic

select top 1000 *
from openquery(MAGENTO, 'select * from magento01.ga_entity_transaction_data where medium = ''organic''')

select top 1000 *
from openquery(MAGENTO, 
	'select medium, count(*) 
	from magento01.ga_entity_transaction_data
	group by medium
	order by medium')

select top 1000 *
from openquery(MAGENTO, 
	'select medium, channel, min(date), max(date), count(*) 
	from magento01.ga_entity_transaction_data
	group by medium, channel
	order by channel, medium')


select top 1000 *
from openquery(MAGENTO, 
	'select source, count(*) 
	from magento01.ga_entity_transaction_data
	where medium = ''ppc''
	group by source
	order by source')



