
use DW_GetLenses
go 

	select source_code, channel
	from DW_GetLenses.dbo.business_source -- dw_bis_source_map
	order by channel, source_code

	select top 10000 order_id, order_no, document_date, 
		affilCode, business_source, business_channel
	from DW_GetLenses.dbo.order_headers
	where affilCode is not null 
		-- and affilCode <> business_source
	order by document_date desc

	select affilCode, count(*)
	from DW_GetLenses.dbo.order_headers
	group by affilCode
	order by affilCode

		select o.affilCode, sm.source_code, sm.channel, convert(date, doc_date_min), convert(date, doc_date_max), o.num
		from
				(select affilCode, min(document_date) doc_date_min, max(document_date) doc_date_max, count(*) num
				from DW_GetLenses.dbo.order_headers
				group by affilCode) o
			left join
				openquery(MAGENTO, 'select * from dw_flat.dw_bis_source_map') sm on o.affilCode = sm.source_code
		--where sm.source_code is null
		order by channel, o.affilCode

	select business_channel, count(*)
	from DW_GetLenses.dbo.order_headers
	group by business_channel
	order by business_channel

	-- X: View 
	`oh`.`affilCode` AS `affilCode`,
	`oh`.`affilCode` AS `business_source`,
    `ochan`.`channel` AS `business_channel`, -- dw_flat`.`dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))


		-- dw_bis_source_map
			-- INSERT SCRIPT

		-- dw_bis_map_dynamic_types
		INSERT INTO  dw_bis_map_dynamic_types VALUES('adword','Adwords'),('affiliate','Affiliates');

		-- dw_bis_map_generic
		SELECT DISTINCT affilCode bis_source_code , NULL AS channel  
		FROM dw_order_headers
		WHERE affilCode IS NOT NULL AND affilCode != '';

		UPDATE dw_bis_map_generic mg INNER JOIN dw_bis_source_map mp ON mp.source_code = mg.bis_source_code
		SET mg.channel = mp.channel
		WHERE mp.channel != '' AND mp.channel IS NOT NULL

		UPDATE dw_bis_map_generic mg, dw_bis_map_dynamic_types dt
		SET mg.channel = dt.channel
		WHERE bis_source_code LIKE CONCAT(dt.prefix,'%') AND (mg.channel IS NULL OR mg.channel = '');

		-- dw_flat`.`dw_order_channel`
		SELECT oh.order_id, oh.order_no, 
			mg.channel bis_channel, cm.channel raf_channel, ga.channel ga_channel, cm2.channel coupon_channel,
			COALESCE(mg.channel, cm.channel, ga.channel, cm2.channel, 'Unknown') channel
		FROM 
				dw_order_headers oh	
			INNER JOIN 
				dw_updated_customers uc ON uc.customer_id = oh.customer_id
			LEFT JOIN 
				`dw_bis_map_generic`mg ON `bis_source_code`= affilcode
			LEFT JOIN 
				`dw_coupon_code_map`cm ON cm.`coupon_code`= oh.coupon_code AND cm.`is_raf` =1
			LEFT JOIN 
				{$this->dbname}.`ga_entity_transaction_data` ga ON ga.`transactionId`= oh.order_no
			LEFT JOIN 
				`dw_coupon_code_map`cm2 ON cm2.`coupon_code`= oh.coupon_code AND cm2.`is_raf` =0;

-----------------------------------------------------------------------------------------------

	-- dw_bis_source_map
	select source_code, channel
	from openquery(MAGENTO, 'select * from dw_flat.dw_bis_source_map')
	order by channel, source_code

	-- dw_bis_map_dynamic_types
	select prefix, channel
	from openquery(MAGENTO, 'select * from dw_flat.dw_bis_map_dynamic_types')

	-- dw_bis_map_generic
	select bis_source_code, channel
	from openquery(MAGENTO, 'select * from dw_flat.dw_bis_map_generic')
	order by channel, bis_source_code