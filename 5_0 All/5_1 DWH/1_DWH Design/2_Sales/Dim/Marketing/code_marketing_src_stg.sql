
select channel, sup_channel, description
from
	(select channel, sup_channel, min(description) description
	from Landing.map.sales_channel
	group by channel, sup_channel) t


select publisher_id, 
	isnull(isnull(affiliate_name, affiliate_website), 'N/A') affiliate_name, affiliate_website, affiliate_network, affiliate_type, affiliate_status
from
	(select publisher_id, 
		min(affiliate_name) affiliate_name, min(affiliate_website) affiliate_website, min(affiliate_network) affiliate_network, 
		min(affiliate_type) affiliate_type, min(affiliate_status) affiliate_status
	from Landing.map.sales_affiliate
	group by publisher_id) t
--where affiliate_name is null
order by publisher_id


select isnull(channel, 'NULL') channel
from 
	(select distinct channel
	from Landing.mag.salesrule_aud
	-- where channel is not null
	) t
order by channel

	select 1 coupon_id_bk, 'Test' channel_bk, 'Test' coupon_code, 'Test' coupon_code_name, 
		'10/10/2010' from_date, '10/10/2010' to_date, '10/10/2010' expiration_date, 'Test' discount_type_name, 10 discount_amount, 'Y' only_new_customer, 'Y' refer_a_friend, @idETLBatchRun


	select isnull(src.coupon_id, sr.rule_id * -1), sr.rule_id, count(src.coupon_id) over (partition by sr.rule_id) num_rep,
		isnull(sr.channel, 'NULL'), isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) code, sr.name, 
		sr.from_date, sr.to_date, src.expiration_date, 
		sr.simple_action, sr.discount_amount, 
		case when (sr.postoptics_only_new_customer = 1) then 'Y' else 'N' end only_new_customer,
		case when (ref.value is not null) then 'Y' else 'N' end refer_a_friend
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
		left join
			(select distinct value
			from Landing.mag.core_config_data
			where path = 'referafriend/invite/voucher') ref on src.coupon_id = ref.value
	-- where sr.channel is not null and src.code is not null
	order by num_rep desc, sr.channel, src.code
