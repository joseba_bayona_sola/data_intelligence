
select publisherId, company, status, email, website, type, joinDate
from DW_GetLenses_jbs.dbo.ex_affiliates_uk
-- where publisherId = 65970
order by type, website

select status, count(*)
from DW_GetLenses_jbs.dbo.ex_affiliates_uk
group by status
order by status

select type, count(*)
from DW_GetLenses_jbs.dbo.ex_affiliates_uk
group by type
order by type
