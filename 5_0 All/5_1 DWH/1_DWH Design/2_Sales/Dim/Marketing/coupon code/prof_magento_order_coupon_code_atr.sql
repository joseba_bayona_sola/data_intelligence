
select entity_id, increment_id, created_at, coupon_code, coupon_rule_name, applied_rule_ids, length(applied_rule_ids)
from magento01.sales_flat_order
where coupon_code is not null
  and length(applied_rule_ids) > 4
order by created_at desc
limit 1000

select entity_id, increment_id, created_at, coupon_code, coupon_rule_name, applied_rule_ids, length(applied_rule_ids), base_discount_amount
from magento01.sales_flat_order
where coupon_code is null
  and length(applied_rule_ids) > 0
order by created_at desc
limit 1000



select entity_id, increment_id, created_at, coupon_code, coupon_rule_name, applied_rule_ids, length(applied_rule_ids), base_subtotal, base_discount_amount
from magento01.sales_flat_order
where entity_id in (5055967, 5055990, 5056011, 5056023, 5056068, 5056133, 5056138, 5056173, 5056182, 5056187, 5056191, 5056192, 5056278, 5056346)


select entity_id, increment_id, created_at, coupon_code, coupon_rule_name, applied_rule_ids, length(applied_rule_ids), base_subtotal, base_discount_amount
from magento01.sales_flat_order
where coupon_code is null
  and length(applied_rule_ids) = 0
  and base_discount_amount < 0
