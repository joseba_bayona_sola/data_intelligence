
use DW_GetLenses
go 

	select coupon_code, channel,
		description,
		start_date, end_date, 
		source_code, new_customer
	from DW_GetLenses.dbo.coupon_channel
	order by coupon_code

	select coupon_code, count(*)
	from DW_GetLenses.dbo.order_headers
	group by coupon_code
	order by coupon_code

	select *
	from DW_GetLenses.dbo.order_headers
	where coupon_code = 'TENWEG' -- ACHMEA TENWEG
	order by document_date desc

		select cc1.coupon_code, cc2.coupon_code, cc2.min_date, cc2.max_date, cc2.num
		from
				DW_GetLenses.dbo.coupon_channel cc1
			full join
				(select coupon_code, convert(date, min(document_date)) min_date, convert(date, max(document_date)) max_date, count(*) num
				from DW_GetLenses.dbo.order_headers
				group by coupon_code) cc2 on cc1.coupon_code = cc2.coupon_code
		where cc1.coupon_code is null or cc2.coupon_code is null
		-- where cc1.coupon_code is not null or cc2.coupon_code is not null
		order by cc1.coupon_code, cc2.coupon_code


	-- X: View 
	`oh`.`coupon_code` AS `coupon_code`,

		-- dw_coupon_code_map
			-- INSERT SCRIPT

			REPLACE INTO dw_coupon_code_map
			SELECT  
				src.code, channel,
				CASE 
					WHEN EXISTS(SELECT `value`	FROM `dw_core_config_data` WHERE path = 'referafriend/invite/voucher' AND CAST(`value` AS UNSIGNED) = src.coupon_id) THEN 1
					ELSE 0
				END is_raf
			FROM {$this->dbname}.salesrule  sr INNER JOIN {$this->dbname}.salesrule_coupon src ON sr.rule_id = src.rule_id
			WHERE channel != '' AND  channel  IS NOT NULL;

		-- `vw_coupon_channel`
		select 
			`sc`.`code` AS `coupon_code`,
			`sr`.`from_date` AS `start_date`,`sr`.`to_date` AS `end_date`,
			coalesce(NULL,'') AS `source_code`,
			`sr`.`channel` AS `channel`,
			`sr`.`postoptics_only_new_customer` AS `new_customer`,
			cast(`sr`.`description` as char(4000) charset utf8) AS `description` 
	
		from (
				`magento01`.`salesrule_coupon` `sc` 
			join 
				`magento01`.`salesrule` `sr` on((`sc`.`rule_id` = `sr`.`rule_id`)));

-----------------------------------------------------------------------------------------------

	-- dw_coupon_code_map
	select coupon_code, channel, is_raf
	from openquery(MAGENTO, 'select * from dw_flat.dw_coupon_code_map')
	order by channel, coupon_code


		select cc1.coupon_code, cc2.coupon_code, cc2.channel
		from
				DW_GetLenses.dbo.coupon_channel cc1
			full join
				openquery(MAGENTO, 'select * from dw_flat.dw_coupon_code_map') cc2 on cc1.coupon_code = cc2.coupon_code
		where cc1.coupon_code is null or cc2.coupon_code is null
		-- where cc1.coupon_code is not null or cc2.coupon_code is not null
		order by cc1.coupon_code, cc2.coupon_code

	-- `vw_coupon_channel		
	select coupon_code, start_date, end_date, source_code, channel, new_customer, description
	from openquery(MAGENTO, 'select * from dw_flat.vw_coupon_channel')
	order by channel, coupon_code