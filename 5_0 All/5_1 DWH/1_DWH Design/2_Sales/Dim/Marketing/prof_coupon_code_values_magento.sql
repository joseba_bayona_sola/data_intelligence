

-- COUPON CODE - SALES RULE

	select rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_aud
	order by channel, name

	select coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.salesrule_coupon_aud

	select sr.rule_id, src.coupon_id, count(src.coupon_id) over (partition by sr.rule_id) num_rep,
		sr.channel, src.code, sr.name, sr.description, sr.from_date, sr.to_date,
		sr.simple_action, sr.discount_amount, sr.discount_qty, 
		src.created_at, src.expiration_date, sr.is_active,
		sr.postoptics_only_new_customer,
		-- src.usage_limit, src.usage_per_customer, src.is_primary, src.referafriend_credit,
		src.idETLBatchRun, src.ins_ts, src.upd_ts
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
	-- where code = 'JET23'
	where expiration_date > GETUTCDATE() OR expiration_date is null
	order by num_rep desc, sr.channel, src.code

-- COUPON CODE - ORDERS

select coupon_code, count(*)
from Landing.mag.sales_flat_order_aud
group by coupon_code
order by coupon_code

----------------------------------------------------------------

select o.coupon_code, o.num
from
		(select coupon_code, count(*) num
		from Landing.mag.sales_flat_order_aud
		group by coupon_code) o
	left join
		(select sr.rule_id, src.coupon_id, count(src.coupon_id) over (partition by sr.rule_id) num_rep,
			sr.channel, src.code coupon_code, sr.name, sr.description, sr.from_date, sr.to_date,
			sr.simple_action, sr.discount_amount, sr.discount_qty, 
			src.created_at, src.expiration_date, sr.is_active,
			sr.postoptics_only_new_customer,
			-- src.usage_limit, src.usage_per_customer, src.is_primary, src.referafriend_credit,
			src.idETLBatchRun, src.ins_ts, src.upd_ts
		from 
				Landing.mag.salesrule_coupon_aud src
			right join
				Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) c on o.coupon_code = c.coupon_code
where c.coupon_code is null
order by o.coupon_code
