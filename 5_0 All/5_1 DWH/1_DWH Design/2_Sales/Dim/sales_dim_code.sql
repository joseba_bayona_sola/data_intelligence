
select order_stage order_stage_name_bk, order_stage order_stage_name, description
from 
	(select order_stage, max(description) description
	from Landing.map.sales_order_stage
	group by order_stage) t

select order_status order_status_name_bk, order_status order_status_name, description
from
	(select order_status, max(description) description
	from Landing.map.sales_order_status
	group by order_status) t

select order_type order_type_name_bk, order_type order_type_name, description
from
	(select order_type, max(description) description
	from Landing.map.sales_order_type
	group by order_type) t

select order_status_magento status_bk, order_status_magento order_status_magento_name, description
from
	(select order_status_magento, max(description) description
	from Landing.map.sales_order_status_magento
	group by order_status_magento) t
union
select o.status status_bk, o.status order_status_magento_name, 'No Desc.' description
from
		(select distinct status
		from Landing.mag.sales_flat_order
		where status not in ('archived', 'cancel_ogone', 'processing_ogone')) o
	left join
		Landing.map.sales_order_status_magento osm on o.status = osm.order_status_magento_code
where osm.order_status_magento is null
	
select payment_method payment_method_name_bk, payment_method payment_method_name, description
from
	(select payment_method, max(description) description
	from Landing.map.sales_payment_method
	group by payment_method) t
union
select op.method payment_method_name_bk, op.method payment_method_name, 'No Desc.' description
from
		(select distinct method
		from Landing.mag.sales_flat_order_payment_aud
		where method not in ('storecredit')) op
	left join
		Landing.map.sales_payment_method pm on op.method = pm.payment_method_code
where pm.payment_method is null


select cc_type cc_type_name_bk, cc_type cc_type_name
from
	(select distinct cc_type
	from Landing.map.sales_cc_type) t



select shipping_carrier shipping_carrier_name_bk, shipping_carrier shipping_carrier_name
from
	(select distinct substring(shipping_method, 1, charindex('-', shipping_method) -1) shipping_carrier
	from Landing.map.sales_shipping_method) t

select shipping_method shipping_description_bk, shipping_method shipping_method_name, shipping_carrier shipping_carrier_name_bk, shipping_method description
from
	(select distinct shipping_method, substring(shipping_method, 1, charindex('-', shipping_method) -1) shipping_carrier
	from Landing.map.sales_shipping_method) t


select price_type price_type_name_bk, price_type price_type_name, description
from
	(select price_type, max(description) description
	from Landing.map.sales_price_type
	group by price_type) t

select o.telesales_admin_username telesales_admin_username_bk, o.telesales_admin_username telesales_username, 
	u.user_id, u.firstname telesales_first_name, u.lastname telesales_last_name
from
		(select telesales_admin_username, count(*) num
		from Landing.mag.sales_flat_order_aud
		group by telesales_admin_username) o
	inner join
		Landing.mag.admin_user u on o.telesales_admin_username = u.username

select *
from 
