
-- ORDER TYPE

	-- Automatic - Telesales - Web	
	select top 1000 order_type, count(*)
	from DW_GetLenses.dbo.order_headers
	-- where automatic_reorder = -1 --> Numbers OK
	-- where telesales_admin_username is not null --> Numbers OK
	group by order_type
	order by order_type

	-- Automatic - Telesales - Web	
	select top 1000 order_type, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by order_type
	order by order_type

	--X: View
	CASE
		WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
        WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
        WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
		WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
        ELSE `oh`.`postoptics_source`
	END) AS `order_type`,

	-- Y: View
	o.order_type


-- ORDER STATUS MAGENTO

	-- shipped - canceled - closed - pending - warehouse - awaiting - holded - ....
	select top 1000 status, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by status
	order by status

	-- 
	select top 1000 xxxx, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by xxxx
	order by xxxx

	--X: View
	`oh`.`status` AS `status`,

	-- Y: View
	NOT USED

-- PAYMENT METHOD

	-- Card - New, 	Card - Stored, PayPal, Ideal, Free, Bank Transfer, Klarna
	-- storecredit?
	select top 1000 payment_method, count(*) num
	from DW_GetLenses.dbo.order_headers
	where document_date > GETUTCDATE() - 365
	group by payment_method
	order by payment_method

	-- Automatic - Telesales - Web	
	select top 1000 payment_method, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by payment_method
	order by payment_method

	--X: View
	CASE
		WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
        ELSE IFNULL(CONVERT( `pm`.`payment_method_name` USING UTF8), `op`.`method`) -- vw_sales_flat_order_payment, dw_payment_method_map // ON ((CONVERT( `pm`.`payment_method` USING UTF8) = `op`.`method`)))
	END AS `payment_method`,

	-- Y: View
	ISNULL(o.payment_method,'Unknown') as payment_method,


-- CC Type

	-- Visa - Mastercard - Maestro - NULL
	-- Visa Debit? MasterCard Debit? Laser? Empty?
	select top 1000 cc_type, count(*)
	from DW_GetLenses.dbo.order_headers
	-- where document_date > GETUTCDATE() - 365
	where payment_method in ('Paypal', 'Free', 'Cheque')
	group by cc_type
	order by cc_type

		select top 1000 payment_method, count(*)
		from DW_GetLenses.dbo.order_headers
		where cc_type is not null 
		group by payment_method
		order by payment_method

	-- Visa - Mastercard - Maestro - NULL
	-- Visa Debit? MasterCard Debit? Laser? Empty?
	select top 1000 payment_method, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by payment_method
	order by payment_method

	--X: View
	IFNULL(CONVERT( `ctm`.`card_type_name` USING UTF8), `op`.`cc_type`) AS `cc_type`, -- vw_sales_flat_order_payment, dw_card_type_map // ON ((CONVERT( `ctm`.`card_type` USING UTF8) = `op`.`cc_type`)))

	-- Y: View
	ISNULL(o.payment_method,'Unknown') as payment_method,

	select top 1000 *
	from DW_GetLenses.dbo.order_headers
	where cc_type in ('Visa Debit')


-- SHIPPING CARRIER - METHOD

	-- 13Ten - Citylink - DHL - DX - Fastway - IMX - MRW - Parcel Force - PostNL - Royal Mail - TNT Post - UKMail 
	select top 1000 shipping_carrier, shipping_method, count(*)
	from DW_GetLenses.dbo.order_headers
	-- where document_date > getutcdate() - 365
	group by shipping_carrier, shipping_method
	order by shipping_carrier, shipping_method

		select
			t1.shipping_carrier shipping_carrier_365, t1.shipping_method shipping_method_365, 
			t2.shipping_carrier shipping_carrier_ALL, t2.shipping_method shipping_method_ALL, 
			num1, num2
		from
				(select top 1000 shipping_carrier, shipping_method, count(*) num1
				from DW_GetLenses.dbo.order_headers
				group by shipping_carrier, shipping_method) t1
			left join
				(select top 1000 shipping_carrier, shipping_method, count(*) num2
				from DW_GetLenses.dbo.order_headers
				where document_date > '01/01/2017'
				group by shipping_carrier, shipping_method) t2 on t1.shipping_carrier = t2.shipping_carrier and t1.shipping_method = t2.shipping_method

		order by t1.shipping_carrier, t1.shipping_method

	select top 1000 shipping_carrier, shipping_method, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by shipping_carrier, shipping_method
	order by shipping_carrier, shipping_method

	--X: View
	 LEFT(`oh`.`shipping_description`, LOCATE(' - ', `oh`.`shipping_description`)) AS `shipping_carrier`,
     `oh`.`shipping_description` AS `shipping_method`,

	-- Y: View
	o.shipping_carrier, o.shipping_method,

-- TELESALE

	-- .................
	select top 1000 telesales_admin_username, count(*)
	from DW_GetLenses.dbo.order_headers
	where order_type = 'Telesales' -- Some NULL records 33k
	group by telesales_admin_username
	order by telesales_admin_username

	-- .................
	select top 1000 telesales_agent, count(*)
	from DW_Sales_Actual.dbo.Dim_Order_Headers
	group by telesales_agent
	order by telesales_agent

	--X: View
	`oh`.`telesales_admin_username` AS `telesales_admin_username`,

	-- Y: View
	o.telesales_admin_username AS 'telesales_agent',

	select *
	from DW_GetLenses.dbo.order_headers
	where order_type = 'Telesales' and telesales_admin_username is null
	order by document_date desc

-- PRICE TYPE

	-- Regular - Discounted - PLA - PLA & Discounted
	select top 1000 price_type, count(*) num
	from DW_GetLenses.dbo.order_lines
	group by price_type
	order by price_type

	-- Regular - Discounted - PLA - PLA & Discounted
	select top 1000 price_type, count(*)
	from DW_Sales_Actual.dbo.dim_Order_Qty_Price
	group by price_type
	order by price_type

	--X: View
	order_item_id -- magento01.po_sales_pla_item

	(DT_STR,20,1252)
		(local_discount_exc_vat != 0 && ISNULL(pla_order_item_id) ==  FALSE  ? "PLA & Discounted" : 
			(local_discount_exc_vat != 0 ? "Discounted" : 
			(ISNULL(pla_order_item_id) ==  FALSE  ? "PLA" : 
			"Regular")))

	-- Y: View
	l.price_type

---------------------------------------------------------------------------------------------------------------------------

-- AUTO VERIFICATION

	-- NULL - Yes - No
	select top 1000 auto_verification, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by auto_verification
	order by auto_verification

	-- NULL - Yes - No
	select top 1000 xxxx, count(*)
	from DW_Sales_Actual.dbo.dim_Order_Qty_Price
	group by xxxx
	order by xxxx

	--X: View
	`oh`.`postoptics_auto_verification` AS `auto_verification`,

	-- Y: View
	

-- PRESC VERIFICATION METHOD

	-- Automatic - Not Required - Call - Upload/Fax/Scan
	select top 1000 presc_verification_method, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by presc_verification_method
	order by presc_verification_method

	-- Automatic - Not Required - Call - Upload/Fax/Scan - NULL
	select top 1000 presc_verification_method, count(*)
	from DW_Sales_Actual.dbo.dim_Order_Headers
	group by presc_verification_method
	order by presc_verification_method

	--X: View
	CASE
		WHEN (((`oh`.`presc_verification_method` = 'call') OR ISNULL(`oh`.`presc_verification_method`)) 
			AND (`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
		WHEN ((`oh`.`presc_verification_method` = 'upload') 
			AND (`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
		WHEN 
			((`conf`.`value` = 1) AND (`ov`.`has_lens` = 1) AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
		ELSE 'Not Required'
	END AS `presc_verification_method`,

select s.store_id, s.name, 
	c.scope_id, c.path, c.value
from 
		Landing.aux.mag_core_config_data_store c
	inner join
		Landing.mag.core_store_aud s on c.store_id = s.store_id
where path = 'prescription/settings/enable' 

	
	
	-- Y: View
	o.presc_verification_method, 

-- TELESALES METHOD CODE

	-- NULL - Call Optician and verify, Match to Fax / Email, Match to Uploaded File, Glasses upload/fax - Send Details Later
	select top 1000 telesales_method_code, count(*) num
	from DW_GetLenses.dbo.order_headers
	group by telesales_method_code
	order by telesales_method_code

	-- 
	select top 1000 xxxx, count(*)
	from DW_Sales_Actual.dbo.dim_Order_Qty_Price
	group by xxxx
	order by xxxx

	--X: View
	`oh`.`telesales_method_code` AS `telesales_method_code`,

	-- Y: View
	l.price_type

-- LOCAL PAYMENT AUTHORIZED

	-- Regular - Discounted - PLA - PLA & Discounted
	select top 1000 local_payment_authorized, count(*)
	from DW_GetLenses.dbo.order_headers
	group by local_payment_authorized
	order by local_payment_authorized

	select top 1000 local_payment_canceled, count(*)
	from DW_GetLenses.dbo.order_headers
	group by local_payment_canceled
	order by local_payment_canceled

	-- 
	select top 1000 xxxx, count(*)
	from DW_Sales_Actual.dbo.dim_Order_Qty_Price
	group by xxxx
	order by xxxx

	--X: View
	`op`.`amount_authorized` AS `local_payment_authorized`,  `op`.`amount_canceled` AS `local_payment_canceled`,

	-- Y: View
	
---------------------------------------------------------------------------------

	-- approved_time
	CASE `oh`.`warehouse_approved_time` WHEN '0000-00-00 00:00:00' THEN NULL ELSE `oh`.`warehouse_approved_time` END AS `approved_time`,

	-- has_lens
	`ov`.`has_lens` AS `has_lens`, -- order_headers_vat

	-- remote_ip
	`oh`.`remote_ip` AS `remote_ip`,

	-- automatic_reorder
	`oh`.`automatic_reorder` AS `automatic_reorder`,

	-- referafriend_code, referafriend_referer
	`oh`.`referafriend_code` AS `referafriend_code`, `oh`.`referafriend_referer` AS `referafriend_referer`,


-------------------------------------------------------------

