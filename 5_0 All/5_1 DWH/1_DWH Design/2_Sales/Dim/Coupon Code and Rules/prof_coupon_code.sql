
	select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, isnull(sr.channel, 'NULL') channel_bk, sr.rule_id, 
		isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, sr.name coupon_code_name, 
		sr.from_date, sr.to_date, src.expiration_date, 
		sr.simple_action discount_type_name, sr.discount_amount, 
		case when (sr.postoptics_only_new_customer = 1) then 'Y' else 'N' end only_new_customer,
		case when (ref.value is not null) then 'Y' else 'N' end refer_a_friend
	from 
			Landing.mag.salesrule_coupon_aud src
		right join
			Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id
		left join
			(select distinct value
			from Landing.mag.core_config_data
			where path = 'referafriend/invite/voucher') ref on src.coupon_id = ref.value
	where src.coupon_id is null or sr.rule_id = 3057
	order by src.code

select *
from Staging.sales.dim_coupon_code
where rule_id = 3057

select *
from Warehouse.sales.dim_coupon_code
where rule_id = 3057

select *
from Warehouse.sales.dim_coupon_code_v
where coupon_code like 'RULE%'
order by coupon_code
