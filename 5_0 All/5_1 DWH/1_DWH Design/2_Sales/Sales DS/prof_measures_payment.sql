
select order_id_bk, order_no, order_date, 
	payment_id, payment_method_name_bk, 
	local_total_inc_vat, local_store_credit_given, local_bank_online_given
from Landing.aux.sales_dim_order_header

 
select top 1000 oh.order_id_bk, oh.order_no, oh.order_date, oh.order_status_name_bk, oh.reimbursement_type_name_bk, oh2.mutual_amount,
	oh.payment_id, oh.payment_method_name_bk, oh.payment_method_code,
	oh.local_total_inc_vat, oh.local_store_credit_given, oh.local_bank_online_given, 
	op.amount_authorized, op.base_amount_ordered, op.base_amount_refunded, op.base_amount_refunded_online
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		Landing.mag.sales_flat_order_payment_aud op on oh.order_id_bk = op.parent_id
	inner join
		Landing.mag.sales_flat_order_aud oh2 on oh.order_id_bk = oh2.entity_id
where oh.local_total_inc_vat <> op.base_amount_ordered and oh.reimbursement_type_name_bk is null -- base_amount_ordered is used
-- where op.amount_authorized is null
order by oh.order_status_name_bk, oh.reimbursement_type_name_bk 

select top 1000 entity_id, parent_id, 
	method, cc_type, additional_data, cybersource_stored_id,
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online
from Landing.mag.sales_flat_order_payment_aud
where parent_id in (1597892)
order by parent_id desc


select top 1000 entity_id, parent_id, 
	method, cc_type, additional_data, cybersource_stored_id,
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online
from Landing.mag.sales_flat_order_payment_aud
order by parent_id desc
