

		select o.entity_id,
			pm.payment_method payment_method_name_bk, cc.cc_type cc_type_name_bk, p.entity_id payment_id, p.method payment_method_code, 
			o.reminder_type reminder_type_name_bk, o.reminder_period reminder_period_bk, 
			case when isdate(o.reminder_date) = 1 then convert(datetime, o.reminder_date) else null end reminder_date, -- o.reminder_date reminder_date, 
			o.reminder_sent reminder_sent, 
			case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then 'Y' else 'N' end reorder_f, 
			case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then rp.next_order_date else null end reorder_date, 
			o.reorder_profile_id reorder_profile_id, o.automatic_reorder automatic_reorder, 
			'M' order_source, 'N' proforma,
			null product_type_oh_bk, null order_qty_time, null aura_product_p, 

			o.referafriend_code, o.referafriend_referer, 
			o.postoptics_auto_verification, o.presc_verification_method, o.warehouse_approved_time
		from 
				Landing.mag.sales_flat_order o 

			inner join
				Landing.mag.sales_flat_order_payment_aud p on o.entity_id = p.parent_id
			left join
				Landing.map.sales_payment_method_aud pm on p.method = pm.payment_method_code
			left join
				Landing.map.sales_cc_type_aud cc on p.cc_type = cc.card_type_code

			left join
				Landing.mag.po_reorder_profile_aud rp on o.reorder_profile_id = rp.id	

select top 1000 *
from Landing.mag.sales_flat_order_payment_aud

	select top 1000 method, count(*)
	from Landing.mag.sales_flat_order_payment
	group by method
	order by method

	select top 1000 cc_type, count(*)
	from Landing.mag.sales_flat_order_payment
	group by cc_type
	order by cc_type

	select *
	from Landing.map.sales_payment_method_aud

	select *
	from Landing.map.sales_cc_type_aud
