
select *
from Landing.map.prod_product_30_90_aud

select *
from Landing.aux.mag_prod_product_family_v
where product_id_bk in (2298, 3165)

select *
from Warehouse.prod.dim_product_family_v
where product_id_magento in (2298, 3165)

select product_id_magento, product_family_name
from Warehouse.prod.dim_product_family_v
where product_id_magento in (select product_id_new from Landing.map.prod_product_30_90_aud)

select *
from Landing.mend.gen_prod_productfamilypacksize_v
where productfamilysizerank = 1

		select oi.item_id, oi.order_id, 
			-- oi.product_id, 
			case when (pr_90.product_id is not null) then pr_90.product_id else oi.product_id end product_id,
			esi.bc, esi.di, esi.po, esi.cy, esi.ax, esi.ad, esi.do, esi.co, 
			case when (oi.lens_group_eye in ('Right', 'Left')) then left(oi.lens_group_eye, 1) else null end eye,
			oi.sku sku_magento, null sku_erp, 
			case when (pr_aura.product_id is not null) then 'Y' else 'N' end aura_product_f, ppt.product_type_oh_bk
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_edi_stock_item esi on oi.sku = esi.product_code and oi.product_id = esi.product_id
			left join
				Landing.map.prod_product_aura_aud pr_aura on oi.product_id = pr_aura.product_id
			inner join
				Landing.map.prod_product_30_90_aud pr_90 on oi.product_id = pr_90.product_id_new
			inner join
				Landing.aux.prod_product_product_type_oh ppt on oi.product_id = ppt.product_id
		order by oi.item_id, oi.order_id


		select ol.order_line_id_bk, ol.order_id_bk, 

			oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, 
			case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end qty_pack, -- broken orders
			oi.qty_ordered / pc.num_months qty_time,
			oi.qty_ordered * oi.weight weight,
			oi.base_price local_price_unit, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack, 
			(oi.base_row_total - oi.base_discount_amount) / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack_discount, 

			oi.base_row_total local_subtotal, oi.base_discount_amount local_discount, 
			oi.base_amount_refunded local_refund, isnull(oicr.sum_base_discount_amount, 0) local_discount_refund, 
			oi.discount_percent discount_percent,
			null order_allocation_rate, null order_allocation_rate_aft_refund, null order_allocation_rate_refund
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr on ol.order_line_id_bk = oicr.order_item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 

		where olp.order_line_id_vision_type is null 
			--and pfps.magentoProductID_int is null

---------------------------------------------------------------

select *
from Warehouse.prod.dim_product_family_pack_size_v
where allocation_preference_order not in (0, 1)
order by product_id_magento

select *
from Warehouse.prod.dim_product_family_pack_size_v
where product_id_magento in (select convert(varchar, product_id_new) from Landing.map.prod_product_30_90_aud)
order by product_id_magento

----------------------------------------------

-- Auxiliar Table from aux ol with product list

	select top 1000 order_line_id_bk, order_id_bk, product_id_bk, qty_unit, qty_pack
	from Landing.aux.sales_fact_order_line_aud

	select top 1000 ol.order_line_id_bk, ol.order_id_bk, ol.product_id_bk, pfps.name, pfps.size, ol.qty_unit, ol.qty_pack
	from 
			Landing.aux.sales_fact_order_line_aud ol
		left join 
			Landing.mend.gen_prod_productfamilypacksize_v pfps on ol.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1

	select top 1000 product_id_bk, count(*) num
	into #produts_all
	from Landing.aux.sales_fact_order_line_aud
	group by product_id_bk
	order by product_id_bk

	-- IMPORTANT
	select p.product_id_bk, pf.product_family_name, pf.category_bk category_name, pf.pack_size,
		pfps.size pack_size, -- pc.size pack_size_2, 
		p.num
	from 
			#produts_all p
		inner join
			Landing.aux.mag_prod_product_family_v pf on p.product_id_bk = pf.product_id_bk
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on p.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
	where pfps.magentoProductID_int is not null and pfps.size <> pf.pack_size 
	-- where pfps.magentoProductID_int is null and pf.pack_size = 1
	-- where pesp.product_id_new is not null
	order by category_name, p.num desc

	select *
	from Landing.aux.mag_prod_product_family_v
	where product_id_bk in (select product_id_new from Landing.map.prod_product_30_90_aud)

	select p.product_id_bk, pf.product_family_name, pf.category_name, pfps.size pack_size, pc.size pack_size_2, p.num
	from 
			#produts_all p
		inner join
			Warehouse.prod.dim_product_family_v pf on p.product_id_bk = pf.product_id_magento
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on p.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
		inner join
			(select product_id, category_bk, 
				case 
					when (category_bk = 'CL - Daily') then 30
					when (category_bk = 'CL - Two Weeklies') then 6
					when (category_bk = 'CL - Monthlies') then 3
					when (category_bk = 'CL - Yearly') then 1
					when (category_bk = 'Sunglasses') then 1
					when (category_bk = 'Glasses') then 1
					else 1
				end size 
			from Landing.aux.prod_product_category) pc on p.product_id_bk = pc.product_id

	-- where pfps.magentoProductID_int is null
	where pfps.magentoProductID_int is not null and pfps.size <> pc.size 
	-- order by p.product_id_bk
	order by p.num desc, p.product_id_bk


----------------------------------------------

select value_id, entity_type_id, attribute_id, entity_id, store_id, value
from Landing.mag.catalog_product_entity_varchar
where attribute_id = 514 and store_id = 0

	select value, convert(int, isnull(case when (value = '') then NULL else value end, 1)) pack_size, count(*)
	from Landing.mag.catalog_product_entity_varchar
	where attribute_id = 514 and store_id = 0
	group by value
	order by pack_size, value