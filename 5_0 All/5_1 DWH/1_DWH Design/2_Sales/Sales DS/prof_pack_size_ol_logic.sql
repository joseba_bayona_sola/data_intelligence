	DROP TABLE #sales_order_header_line_measures
	
	create table #sales_order_header_line_measures(
		order_line_id_bk				bigint NOT NULL,
		order_id_bk						bigint NOT NULL,
		order_status_name_bk			varchar(20) NOT NULL, 
		price_type_name_bk				varchar(40),
		discount_f						char(1),
		pack_size						int, 
		qty_unit						decimal(12, 4),
		qty_unit_refunded				decimal(12, 4),
		qty_pack						int,
		qty_time						int,
		weight							decimal(12, 4),

		local_price_unit				decimal(12, 4),
		local_price_pack				decimal(12, 4),
		local_price_pack_discount		decimal(12, 4),

		local_subtotal					decimal(12, 4),
		local_discount					decimal(12, 4),
		local_subtotal_refund			decimal(12, 4),
		local_discount_refund			decimal(12, 4),

		discount_percent				decimal(12, 4),
		order_allocation_rate			decimal(12, 4), 
		order_allocation_rate_aft_refund decimal(12, 4),
		order_allocation_rate_refund	decimal(12, 4))
	

	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		pack_size, 
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		weight,
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, local_subtotal_refund, local_discount_refund, 
		discount_percent, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			case when (odo.order_status_name_bk in ('OK', 'CANCEL')) then odo.order_status_name_bk else 
				case when (oi.qty_ordered = oi.qty_refunded) then 'REFUND'
					 when (oi.qty_ordered > oi.qty_refunded and oi.qty_refunded > 0) then 'PARTIAL REFUND'
					 else 'OK' end end order_status_name_bk,
			--case when (oi.base_discount_amount <> 0) then
			--	case 
			--		when (pla.order_item_id is not null) then 'PLA & Discounted'
			--		when (pp.value is not null) then 'Tier Pricing & Discounted'
			--		else 'Discounted'
			--	end
			--	else case 
			--		when (pla.order_item_id is not null) then 'PLA'
			--		when (pp.value is not null) then 'Tier Pricing'
			--		else 'Regular'
			--	end
			--end price_type_name_bk,
			case 
				-- when (pla.order_item_id is not null) then 'PLA'
				when (pla.order_item_id is not null and pla.promo_key not like 'PPC%') then 'PLA'
				when (pla.order_item_id is not null and pla.promo_key like 'PPC%') then 'PPC'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end price_type_name_bk, 
			case when (oi.base_discount_amount <> 0) then 'Y' else 'N' end discount_f,
			isnull(pf.pack_size, 1) pack_size,
			oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, 
			-- case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end qty_pack, -- broken orders
			case when (convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) end qty_pack,
			oi.qty_ordered / pc.num_months qty_time,
			oi.qty_ordered * oi.weight weight,
			oi.base_price local_price_unit, 
			-- oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack, 
			-- (oi.base_row_total - oi.base_discount_amount) / case when (convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) end local_price_pack_discount, 
			oi.base_row_total / case when (convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) end local_price_pack,
			(oi.base_row_total - oi.base_discount_amount) / case when (convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) = 0) then 1 else convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) end local_price_pack_discount, 

			oi.base_row_total local_subtotal, oi.base_discount_amount local_discount, 
			oi.base_amount_refunded local_refund, isnull(oicr.sum_base_discount_amount, 0) local_discount_refund, 
			oi.discount_percent discount_percent,
			null order_allocation_rate, null order_allocation_rate_aft_refund, null order_allocation_rate_refund
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_prod_product_family_size_v pf on oi.product_id = pf.product_id_bk
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr on ol.order_line_id_bk = oicr.order_item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
			left join
				Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
			left join
				(select store_id, product_id, min(value) value, min(qty) qty
				from Landing.aux.mag_catalog_product_price_aud
				where price_type = 'Tier Pricing'
				group by store_id, product_id) pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
		where olp.order_line_id_vision_type is null 
		order by ol.order_id_bk, ol.order_line_id_bk

	-- Insert Glasses Products (Special Treatment for Package Type Lines)
	insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
		order_status_name_bk, price_type_name_bk, discount_f,
		pack_size, 
		qty_unit, qty_unit_refunded, qty_pack, qty_time, 
		weight,
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_discount, local_subtotal_refund, local_discount_refund, 
		discount_percent, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund) 

		select ol.order_line_id_bk, ol.order_id_bk, 
			case when (odo.order_status_name_bk in ('OK', 'CANCEL')) then odo.order_status_name_bk else 
				case when (oi.qty_ordered = oi.qty_refunded) then 'REFUND'
					 when (oi.qty_ordered > oi.qty_refunded and oi.qty_refunded > 0) then 'PARTIAL REFUND'
					 else 'OK' end end order_status_name_bk,
			--case when (oi.base_discount_amount <> 0) then
			--	case 
			--		when (pla.order_item_id is not null) then 'PLA & Discounted'
			--		when (pp.value is not null) then 'Tier Pricing & Discounted'
			--		else 'Discounted'
			--	end
			--	else case 
			--		when (pla.order_item_id is not null) then 'PLA'
			--		when (pp.value is not null) then 'Tier Pricing'
			--		else 'Regular'
			--	end
			--end price_type_name_bk,
			case 
				-- when (pla.order_item_id is not null) then 'PLA'
				when (pla.order_item_id is not null and pla.promo_key not like 'PPC%') then 'PLA'
				when (pla.order_item_id is not null and pla.promo_key like 'PPC%') then 'PPC'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end price_type_name_bk, 
			case when (oi.base_discount_amount <> 0) then 'Y' else 'N' end discount_f,
			isnull(pf.pack_size, 1) pack_size,
			oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, 
			-- convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) qty_pack, 
			convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) qty_pack, 
			oi.qty_ordered / pc.num_months qty_time,
			oi.qty_ordered * oi.weight weight,
			oi.base_price local_price_unit, 
			-- oi.base_row_total / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack, 
			-- (oi.base_row_total - oi.base_discount_amount) / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack_discount, 
			oi.base_row_total / convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) local_price_pack, 
			(oi.base_row_total - oi.base_discount_amount) / convert(int, (oi.qty_ordered / isnull(pf.pack_size, 1))) local_price_pack_discount, 

			oi.base_row_total + oi2.base_row_total local_subtotal, oi.base_discount_amount + oi2.base_discount_amount local_discount, 
			oi.base_amount_refunded + oi2.base_amount_refunded local_refund, isnull(oicr.sum_base_discount_amount, 0) + isnull(oicr2.sum_base_discount_amount, 0) local_discount_refund, 
			oi.discount_percent discount_percent,
			null order_allocation_rate, null order_allocation_rate_aft_refund, null order_allocation_rate_refund
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
			inner join
				Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			inner join
				Landing.mag.sales_flat_order_item_aud oi2 on olp.order_line_id_package_type = oi2.item_id
			left join
				Landing.aux.mag_prod_product_family_size_v pf on oi.product_id = pf.product_id_bk
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr on ol.order_line_id_bk = oicr.order_item_id
			left join
				Landing.aux.mag_sales_flat_creditmemo_item oicr2 on olp.order_line_id_package_type = oicr2.order_item_id
			left join 
				Landing.mend.gen_prod_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			inner join
				(select product_id, category_bk, 
					case 
						when (category_bk = 'CL - Daily') then 30
						when (category_bk = 'CL - Two Weeklies') then 2
						when (category_bk = 'CL - Monthlies') then 1
						when (category_bk = 'CL - Yearly') then 0.08333
						when (category_bk = 'Sunglasses') then 0.08333
						when (category_bk = 'Glasses') then 0.08333
						else 1
					end num_months
				from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
			left join
				Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
			left join
				(select store_id, product_id, value, qty
				from Landing.aux.mag_catalog_product_price_aud
				where price_type = 'Tier Pricing') pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
		where olp.order_line_id_package_type is not null 
		order by ol.order_id_bk, ol.order_line_id_bk