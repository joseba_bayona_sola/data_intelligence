
-- Things to Do

	-- Excluding VAT Values: Subtotal, Shipping, Discount, Store Credit (using vat_percent)

	-- Right Signs on Order - Credits: For Discount, Store Credit, Payment, Reimbursement, Costs

	-- Store Credit Logic

	-- Payment Logic

select count(*) over (partition by order_line_id_bk) num_rep, *
from Warehouse.sales.fact_order_line_trans_dev_v
-- where local_total_inc_vat <> local_subtotal + local_shipping + local_discount + local_store_credit -- 0
-- where abs(local_total_exc_vat - (local_subtotal_exc_vat + local_shipping_exc_vat + local_discount_exc_vat + local_store_credit_exc_vat)) > 0.1

where abs(global_total_inc_vat - (global_subtotal + global_shipping + global_discount + global_store_credit)) > 0.1
-- where abs(global_total_exc_vat - (global_subtotal_exc_vat + global_shipping_exc_vat + global_discount_exc_vat + global_store_credit_exc_vat)) > 0.1

-- where abs(local_total_inc_vat - (local_payment_online + local_payment_offline + local_reimbursement)) > 0.1 -- Orders with complete line discounted (Payment is allocated in all lines)
-- where abs(global_total_inc_vat - (global_payment_online + global_payment_offline + global_reimbursement)) > 0.1

order by num_rep desc, order_id_bk, order_line_id_bk


select count(*) over (partition by order_line_id_bk) num_rep, *
from Warehouse.sales.fact_order_line_trans_dev_v
where order_id_bk = 8940709
order by num_rep desc, order_id_bk, order_line_id_bk


select count(*) over (partition by order_line_id_bk) num_rep, 
	transaction_type,
	order_line_id_bk, order_line_id_bk_c, order_id_bk, order_id_bk_c, order_date, order_no,
	product_id_magento, product_family_name, 

	local_subtotal, local_shipping, local_discount, local_store_credit, 
	local_subtotal_exc_vat, local_shipping_exc_vat, local_discount_exc_vat, local_store_credit_exc_vat, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 

	-- global_subtotal, global_shipping, global_discount, global_store_credit, 
	-- global_subtotal_exc_vat, global_shipping_exc_vat, global_discount_exc_vat, global_store_credit_exc_vat, 
	-- global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 

	-- local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf,

	local_payment_online, local_payment_offline, local_reimbursement,
	-- global_payment_online, global_payment_offline, global_reimbursement,

	local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost, 
	-- global_product_cost, global_shipping_cost, global_freight_cost, global_total_cost, 

	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate
from Warehouse.sales.fact_order_line_trans_dev_v
-- where local_total_inc_vat <> local_subtotal + local_shipping + local_discount + local_store_credit -- 0
-- where abs(local_total_exc_vat - (local_subtotal_exc_vat + local_shipping_exc_vat + local_discount_exc_vat + local_store_credit_exc_vat)) > 0.1 -- 2: decimal problems on refunds

where abs(local_total_inc_vat - (local_payment_online + local_payment_offline + local_reimbursement)) > 0.1 -- Orders with complete line discounted (Payment is allocated in all lines)
order by num_rep desc, order_id_bk, order_line_id_bk



select count(*) over (partition by order_line_id_bk) num_rep, 
	transaction_type,
	order_line_id_bk, order_line_id_bk_c, order_id_bk, order_id_bk_c, order_date,
	product_id_magento, product_family_name, 

	local_subtotal, local_shipping, local_discount, local_store_credit, 
	local_subtotal_exc_vat, local_shipping_exc_vat, local_discount_exc_vat, local_store_credit_exc_vat, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 

	-- global_subtotal, global_shipping, global_discount, global_store_credit, 
	-- global_subtotal_exc_vat, global_shipping_exc_vat, global_discount_exc_vat, global_store_credit_exc_vat, 
	-- global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 

	-- local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf,

	local_payment_online, local_payment_offline, local_reimbursement,
	-- global_payment_online, global_payment_offline, global_reimbursement,

	local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost, 
	-- global_product_cost, global_shipping_cost, global_freight_cost, global_total_cost, 

	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate
from Warehouse.sales.fact_order_line_trans_dev_v
where order_id_bk = 8967973
order by num_rep desc, order_id_bk, order_line_id_bk