

-- Important Attributes
select top 1000 order_id_bk, invoice_id, order_no, order_date, customer_id_bk, payment_id, 
	payment_method_name_bk, payment_method_code, 
	local_store_credit_used,
	local_total_inc_vat, local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Landing.aux.sales_dim_order_header
where order_status_name_bk = 'REFUND'
order by order_id_bk desc

select top 1000 order_id_bk, invoice_id, order_no, order_date, customer_id_bk, payment_id, 
	payment_method_name_bk, payment_method_code, 
	local_store_credit_used,
	local_total_inc_vat, local_total_refund, local_store_credit_given, local_bank_online_given
from Landing.aux.sales_dim_order_header_aud
where order_id_bk in (8902983, 8922894, 8805251, 8922204)
order by order_id_bk desc


select top 1000 entity_id, parent_id, 
	method, cc_type, additional_data, cybersource_stored_id,
	amount_authorized, amount_canceled, 
	base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online
from Landing.mag.sales_flat_order_payment_aud
-- where parent_id in (8902983, 8922894, 8805251, 8922204)
where base_amount_refunded is not null and base_amount_refunded_online is not null and base_amount_refunded <> base_amount_refunded_online
order by parent_id desc

select top 1000 entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, bs_customer_bal_total_refunded, 
	base_to_global_rate, order_currency_code
from Landing.mag.sales_flat_order_aud
where entity_id in (8902983, 8922894, 8805251, 8922204)
order by entity_id desc

select order_id, num_order, entity_id, increment_id,
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total,
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
from Landing.aux.mag_sales_flat_creditmemo
where order_id in (8902983, 8922894, 8805251, 8922204)
order by order_id desc


select top 1000 order_id, item_id, 
	base_row_total, base_discount_amount, 
	base_amount_refunded 
from Landing.mag.sales_flat_order_item_aud
where order_id in (8902983, 8922894, 8805251, 8922204)
order by order_id desc, item_id

select top 1000 order_id, order_item_id, 
	sum_base_row_total, sum_base_discount_amount
from Landing.aux.mag_sales_flat_creditmemo_item
where order_id in (8902983, 8922894, 8805251, 8922204)
order by order_id desc, order_item_id

select top 1000 order_id_bk, order_line_id_bk, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	local_total_aft_refund_inc_vat
from Landing.aux.sales_fact_order_line_aud
where order_id_bk in (8902983, 8922894, 8805251, 8922204)
order by order_id_bk desc, order_line_id_bk


select top 1000 order_id_bk, order_line_id_bk, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	vat_percent, vat_rate, prof_fee_percent, prof_fee_rate
from Warehouse.sales.fact_order_line_v
where order_id_bk in (8902983, 8922894, 8805251, 8922204)

----------------------------------------------------------------

-- Landing.mag.sales_flat_order_payment_aud Cardinalities
select top 1000 count(*) over (partition by parent_id) num_rep, *
from Landing.mag.sales_flat_order_payment_aud
order by num_rep desc, entity_id

	select top 1000 count(*)
	from Landing.mag.sales_flat_order_payment_aud

----------------------------------------------------------------

-- Exc VAT values
select top 1000 order_id_bk, order_line_id_bk, countries_registered_code, product_type_vat,
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_store_credit_given, local_total_inc_vat, 
	local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	vat_percent, vat_rate, prof_fee_percent, prof_fee_rate, 

	case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_exc_vat end vat_percent_1, 
	case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end vat_percent_2, 

	-- local_subtotal - (local_subtotal * vat_percent/100), 
	local_subtotal - (local_subtotal * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_shipping - (local_shipping * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_discount - (local_discount * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_store_credit_used - (local_store_credit_used * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 

	(local_subtotal - (local_subtotal * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100)) + 
	(local_shipping - (local_shipping * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100)) - 
	(local_discount - (local_discount * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100)) - 
	(local_store_credit_used - (local_store_credit_used * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100)) 

from Warehouse.sales.fact_order_line_v
where order_id_bk in (8902983, 8922894, 8805251, 8922204, 8921819, 8889104)
order by order_id_bk, order_line_id_bk

select order_id_bk, order_line_id_bk, transaction_type, countries_registered_code, product_type_vat,
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_store_credit_given, local_total_inc_vat, 
	local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	vat_percent, vat_rate, prof_fee_percent, prof_fee_rate, 

	case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end vat_percent_2, 

	local_subtotal - (local_subtotal * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_shipping - (local_shipping * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_discount - (local_discount * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100), 
	local_store_credit_used - (local_store_credit_used * case when (local_total_exc_vat = 0) then 0 else local_total_vat * 100 / local_total_inc_vat end/100)
from Warehouse.sales.fact_order_line_trans_v
where order_id_bk in (8902983, 8922894, 8805251, 8922204, 8921819, 8889104)
order by order_id_bk, order_line_id_bk, transaction_type desc

----------------------------------------------------------------

-- Store Credit Logic
