
	-- Marketing Channel
				select oh.entity_id order_id_bk, oh.created_at, oh.status,
					oh.mutual_amount, oh.mutual_quotation_id, 
					mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount, oh.base_grand_total
				from 
						Landing.mag.sales_flat_order_aud oh
					left join
						Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
					left join 
						Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
				where mutual_amount is not null
					-- and mq.amount = 1
				order by order_id_bk desc

	-- Mutuelle Quotation

	drop table #mutuelle_orders

	select entity_id, increment_id, created_at, status, base_grand_total, mutual_amount, mutual_quotation_id
	into #mutuelle_orders
	from Landing.mag.sales_flat_order_aud
	where mutual_quotation_id is not null 
		and mutual_amount is null

	select mq.quotation_id quotation_id_bk, 
		mq.mutual_id mutual_id_bk, mq.type_id type_id_bk, mq.status_id status_id_bk, isnull(c.customer_id_bk, -1) customer_id_bk, mo.entity_id order_id_bk, 
		mq.amount mutual_amount, mq.created_at quotation_date, 
		case when (mq.error_id = 0) then null else mq.error_id end error_id, 
		case when (m.is_online = 1) then 'A' else 'M' end is_online, reference_number reference_no, isnull(mc.mutual_customer_id, -1) mutual_customer_id, @idETLBatchRun
	from 
			Landing.mag.mutual_quotation_aud mq
		inner join
			Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
		left join
			(select mutual_quotation_id, entity_id, mutual_amount
			from
				(select mutual_quotation_id, entity_id, mutual_amount, 
					count(*) over (partition by mutual_quotation_id) num_rep, 
					rank() over (partition by mutual_quotation_id order by entity_id) max_rep
				from #mutuelle_orders) mo
			where num_rep = max_rep) mo on mq.quotation_id = mo.mutual_quotation_id
		left join
			Landing.mag.mutual_customer_aud mc on mq.mutual_customer_id = mc.mutual_customer_id
		left join
			Landing.aux.gen_dim_customer_aud c on mc.customer_id = c.customer_id_bk

	where mq.type_id not in (-29, 0)

	-- Payment
	drop table #mutuelle_orders

	select oh.entity_id order_id_bk, oh.created_at, oh.status,
		oh.mutual_amount, oh.mutual_quotation_id, 
		mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount, oh.base_grand_total
	into #mutuelle_orders
	from 
			Landing.mag.sales_flat_order_aud oh
		left join
			Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
		left join 
			Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
	where oh.mutual_quotation_id is not null

select top 1000 *
from Landing.mag.sales_flat_order_payment_aud
where parent_id in (8882931, 8882510, 6858363, 8876264)

select mo.*, op.method, op.cc_type, op.amount_authorized, op.base_amount_ordered
from 
		#mutuelle_orders mo
	left join
		Landing.mag.sales_flat_order_payment_aud op on mo.order_id_bk = op.parent_id
order by mo.is_online, mo.order_id_bk desc
