
-- ORDERS AS REFUNDS BECAUSE THEY GOT RECORDS IN CREDITMEMO BUT NO REFLECTED ON OH TABLE
select o.entity_id, cr.order_id, o.increment_id, o.created_at, 
	o.base_subtotal, cr.sum_base_subtotal,
	o.base_shipping_amount, o.base_discount_amount, o.base_customer_balance_amount, o.base_grand_total, 
	o.base_customer_balance_refunded, o.base_total_refunded, 
	cr.sum_base_grand_total, 
	abs(isnull(o.base_total_refunded, 0) - isnull(cr.sum_base_grand_total, 0)) diff
from
		(select entity_id, increment_id, created_at, 
			base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
			base_customer_balance_refunded, base_total_refunded, 
			base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
		from Landing.mag.sales_flat_order_aud
		where 
			(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
			(base_total_refunded is not null and base_total_refunded <> 0)) o
	full join
		(select order_id, 
			sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
			sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
			sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative
		from Landing.aux.mag_sales_flat_creditmemo
		group by order_id) cr on o.entity_id = cr.order_id
where o.entity_id is null or cr.order_id is null
-- where o.entity_id is not null and cr.order_id is not null -- and o.base_total_refunded <> cr.sum_base_grand_total
order by diff desc, o.entity_id, cr.order_id

-- CR BASE GRAND TOTAL REFUND vs OH BASE_TOTAL_REFUNDED (WHAT ABOUT BASE_CUSOTMER_BALANCE_REFUNDED: OK TOO. Grand Total is 0)
select o.entity_id, cr.order_id, o.increment_id, o.created_at, 
	o.base_subtotal, cr.sum_base_subtotal,
	o.base_shipping_amount, cr.sum_base_shipping_amount, 
	o.base_discount_amount, o.base_customer_balance_amount, o.base_grand_total, 
	o.base_customer_balance_refunded, o.base_total_refunded, 
	cr.sum_base_grand_total, 
	cr.sum_base_adjustment,
	abs(isnull(o.base_total_refunded, 0) - isnull(cr.sum_base_grand_total, 0)) diff, 
	cr.min_num_order
from
		(select entity_id, increment_id, created_at, 
			base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
			base_customer_balance_refunded, base_total_refunded, 
			base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
		from Landing.mag.sales_flat_order_aud
		where 
			(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
			(base_total_refunded is not null and base_total_refunded <> 0)) o
	full join
		(select order_id, 
			sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
			sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
			sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative,
			min(num_order) min_num_order
		from Landing.aux.mag_sales_flat_creditmemo
		group by order_id) cr on o.entity_id = cr.order_id
where o.entity_id is not null and cr.order_id is not null 
	-- and cr.sum_base_adjustment <> 0
	-- and cr.min_num_order = 2 -- Refunds where not products are refunded
order by diff desc, o.entity_id, cr.order_id

---------------------------------------------------------------------------

select oi.order_id, cri.order_id, oi.item_id, cri.order_item_id, cri.num_rep_rows,
	oi.qty_ordered, oi.qty_refunded, cri.sum_qty, 
	oi.base_row_total, oi.base_amount_refunded, cri.sum_base_row_total
from
		(select item_id, order_id, 
			qty_ordered, qty_refunded, 
			base_row_total, base_amount_refunded
		from Landing.mag.sales_flat_order_item_aud
		where qty_refunded is not null and qty_refunded <> 0) oi
	full join
		Landing.aux.mag_sales_flat_creditmemo_item cri on oi.order_id = cri.order_id and oi.item_id = cri.order_item_id
-- where oi.order_id is null or cri.order_id is null
where oi.order_id is not null and cri.order_id is not null
	-- and cri.num_rep_rows <> 1
	-- and (oi.qty_refunded <> cri.sum_qty) 
	and (oi.base_amount_refunded <> cri.sum_base_row_total)
order by oi.order_id, oi.item_id, cri.order_id, cri.order_item_id

select oi.order_id, cri.order_id, oi.item_id, cri.order_item_id, cri.num_rep_rows,
	oi.qty_ordered, oi.qty_refunded, cri.sum_qty, 
	oi.base_row_total, oi.base_amount_refunded, cri.sum_base_row_total
from
		(select item_id, order_id, 
			qty_ordered, qty_refunded, 
			base_row_total, base_amount_refunded
		from Landing.mag.sales_flat_order_item_aud
		where qty_refunded is not null and qty_refunded <> 0) oi
	full join
		Landing.aux.mag_sales_flat_creditmemo_item cri on oi.order_id = cri.order_id and oi.item_id = cri.order_item_id
-- where oi.order_id is null or cri.order_id is null
where oi.order_id = 2179568
order by oi.order_id, oi.item_id, cri.order_id, cri.order_item_id

