
select olp.order_line_id_bk, olp.order_id_bk, 
	olp.product_type_oh_bk, 
	olm.qty_time, olm.local_subtotal, 
	sum(olm.local_subtotal) over (partition by olp.order_id_bk) total_local_subtotal
from 
		Landing.aux.sales_order_line_product olp
	inner join
		Landing.aux.sales_order_line_measures olm on olp.order_line_id_bk = olm.order_line_id_bk
where olp.order_id_bk = 1885277
order by order_id_bk, order_line_id_bk

select order_id_bk, product_type_oh_bk, total_local_subtotal, 
	sum(local_subtotal), max(qty_time), count(distinct qty_time), 
	convert(decimal(12, 4), sum(local_subtotal) * 100 / total_local_subtotal)
from
	(select olp.order_line_id_bk, olp.order_id_bk, 
		olp.product_type_oh_bk, 
		olm.qty_time, olm.local_subtotal, 
		sum(olm.local_subtotal) over (partition by olp.order_id_bk) total_local_subtotal
	from 
			Landing.aux.sales_order_line_product olp
		inner join
			Landing.aux.sales_order_line_measures olm on olp.order_line_id_bk = olm.order_line_id_bk) ol
group by order_id_bk, product_type_oh_bk, total_local_subtotal
order by order_id_bk, product_type_oh_bk, total_local_subtotal



