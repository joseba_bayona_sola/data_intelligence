

select order_id_bk,
	order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
	payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
	shipping_description_bk, shipping_description_code, telesales_admin_username_bk, 
	reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
	reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
	order_source, 
	product_type_oh_bk, order_qty_time, aura_product_p, 

	referafriend_code, referafriend_referer, 
	postoptics_auto_verification, presc_verification_method, warehouse_approved_time
from Landing.aux.sales_order_header_dim_order

select order_id_bk, product_type_oh_bk, 
	order_percentage, order_flag, order_qty_time, 
	count(*) over (partition by order_id_bk) num_rep, 
	dense_rank() over (partition by order_id_bk order by order_percentage, product_type_oh_bk) ord_rep
from Landing.aux.sales_order_header_oh_pt
order by order_id_bk, product_type_oh_bk

select order_id_bk, product_type_oh_bk, 
	order_percentage, order_flag, order_qty_time, 
	num_rep
from
	(select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time, 
		count(*) over (partition by order_id_bk) num_rep, 
		dense_rank() over (partition by order_id_bk order by order_percentage desc, product_type_oh_bk) ord_rep
	from Landing.aux.sales_order_header_oh_pt) t
where ord_rep = 1
order by order_id_bk, product_type_oh_bk


-------------------------------------------------------------------


select *
from
	(select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time, 
		count(*) over (partition by order_id_bk) num_rep
	from Landing.aux.sales_order_header_oh_pt) t
where num_rep = 4
order by order_id_bk, product_type_oh_bk

	select product_type_oh_bk, count(*)
	from
		(select order_id_bk, product_type_oh_bk, 
			order_percentage, order_flag, order_qty_time, 
			count(*) over (partition by order_id_bk) num_rep
		from Landing.aux.sales_order_header_oh_pt) t
	where num_rep = 1
	group by product_type_oh_bk
	order by product_type_oh_bk

select order_qty_time, count(*)
from
	(select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time, 
		count(*) over (partition by order_id_bk) num_rep
	from Landing.aux.sales_order_header_oh_pt) t
where num_rep = 1
	and product_type_oh_bk = ''
group by order_qty_time
order by order_qty_time
