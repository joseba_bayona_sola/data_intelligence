
select *
from Landing.aux.sales_order_header_o

select *
from Landing.aux.sales_order_header_o_i_s_cr


select *
from Landing.mag.sales_flat_order

select item_id, order_id, product_id, sku
from Landing.mag.sales_flat_order_item
where product_id = 2971

select top 1000 *
from Landing.mag.edi_stock_item_aud
where product_code = '1DAM90-BC8.5-DI14.2-PO-3.75'

select top 1000 product_id, sku, product_code, 
	case when (bc = '') then NULL else bc end bc, case when (di = '') then NULL else di end di, case when (po = '') then NULL else po end po, 
	case when (cy = '') then NULL else cy end cy, case when (ax = '') then NULL else ax end ax, 
	case when (ad = '') then NULL else ad end ad, case when (do = '') then NULL else do end do, case when (co = '') then NULL else co end co 
from 
	(select distinct product_id, sku, product_code, 
		bc, di, po, cy, ax, ad, do, co
	from Landing.mag.edi_stock_item_aud) t
--where product_id = 2318
where product_code = '1DAM90-BC8.5-DI14.2-PO-3.75'



--------------------- Parameters --------------------------
select distinct product_id, sku, product_code, 
	bc, di, po, cy, ax, ad, do, co
from Landing.mag.edi_stock_item_aud
where product_code = 'FDT130-AX180-BC8.6-CY-0.75-DI14.2-PO-2.50' --  'BFSM03-BC8.6-DI14.0-PO+0.25'

select top 1000 *
from #edi_stock_item_new
where product_code = 'BFSM03-BC8.6-DI14.0-PO+0.25' --  ''

select top 1000 *
from Landing.aux.mag_edi_stock_item
where product_code = 'BFTM03-AX170-BC8.7-CY-2.25-DI14.5-PO-2.00' --  ''FDT130-AX180-BC8.6-CY-0.75-DI14.2-PO-2.50

			select 
				product_id, sku, product_code, 
				bc, 
				left(di, 4) di, 
				case 
					when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
					when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
					when (len(po) = 1) then '+0' + po + '.00'
					when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
					when (len(po) = 3 and charindex('-', po) = 1) then po + '.00'
					when (len(po) = 3 and charindex('-', po) = 0) then '+0' + po + '0'
					when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
					when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
					when (len(po) = 12) then '+0' + substring(po, 1, 4)
					when (len(po) = 13) then substring(po, 1, 1) + '0' + + substring(po, 2, 4)
					when (len(po) = 14) then substring(po, 1, 6)
					else po
				end po,
				cy, 
				case
					when (len(ax) = 1) then '00' + ax
					when (len(ax) = 2) then '0' + ax
					else ax end ax, 
				case 
					when ad in ('Medium', 'MF', 'MID') then 'MED'
					else ad end ad, 
				do, 
				co
			into #edi_stock_item_new
			from 
				(select product_id, sku, product_code, 
					case when (bc = '') then NULL else bc end bc, case when (di = '') then NULL else di end di, case when (po = '') then NULL else replace(po, char(13), '') end po, 
					case when (cy = '') then NULL else cy end cy, case when (ax = '') then NULL else ax end ax, 
					case when (ad = '') then NULL else ad end ad, case when (do = '') then NULL else do end do, case when (co = '') then NULL else co end co 
				from
					(select distinct product_id, sku, product_code, 
						bc, di, po, cy, ax, ad, do, co
					from Landing.mag.edi_stock_item_aud) esi) esi

		select bc, base_curve, num
		from
			(select bc, count(*) num
			from #edi_stock_item_new
			group by bc) t
		left join
			Landing.map.prod_param_base_curve p on t.bc = p.base_curve
		order by bc

		select di, diameter, num
		from
			(select di, count(*) num
			from #edi_stock_item_new
			group by di) t
		left join
			Landing.map.prod_param_diameter p on t.di = p.diameter
		order by di

		select po, power, num
		from
			(select po, count(*) num
			from #edi_stock_item_new
			group by po) t
		left join
			Landing.map.prod_param_power p on t.po = p.power
		order by po

		select cy, cylinder, num
		from
			(select cy, count(*) num
			from #edi_stock_item_new
			group by cy) t
		left join
			Landing.map.prod_param_cylinder p on t.cy = p.cylinder
		order by cy

		select ax, axis, num
		from
			(select ax, count(*) num
			from #edi_stock_item_new
			group by ax) t
		left join
			Landing.map.prod_param_axis p on t.ax = p.axis
		order by ax

		select ad, addition, num
		from
			(select ad, count(*) num
			from #edi_stock_item_new
			group by ad) t
		left join
			Landing.map.prod_param_addition p on t.ad = p.addition
		order by ad

		select do, dominance, num
		from
			(select do, count(*) num
			from #edi_stock_item_new
			group by do) t
		left join
			Landing.map.prod_param_dominance p on t.do = p.dominance
		order by do

		-------------------------------------------

		drop table #temp_line_prod_param

		select oi.item_id, oi.order_id, oi.product_id, oi.sku, 
			esi.bc, esi.di, esi.po, esi.cy, esi.ax, esi.ad, esi.do, esi.co
		--into #temp_line_prod_param
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			inner join
				Landing.mag.sales_flat_order_item_aud oi on o.entity_id = oi.order_id
			left join
				Landing.aux.mag_edi_stock_item esi on oi.sku = esi.product_code and oi.product_id = esi.product_id
		order by oi.product_id, oi.sku

		select oi.item_id, oi.order_id, oi.product_id, oi.sku, 
			esi.bc, esi.di, esi.po, esi.cy, esi.ax, esi.ad, esi.do, esi.co
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.aux.mag_edi_stock_item esi on oi.sku = esi.product_code and oi.product_id = esi.product_id
		order by oi.product_id, oi.sku


		

		select item_id, order_id, product_id, sku, 
			bc, di, po, cy, ax, ad, do, co
		from #temp_line_prod_param

		select po, count(*)
		from #temp_line_prod_param
		group by po
		order by po



	-- Sku Magento

	-- Sku ERP

--------------------- Glasses --------------------------

select *
from #temp_line_prod_param
where product_id = 2375

select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
	ep.product_id
from 
		Landing.mag.sales_flat_order_item_aud oi
	left join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
where oi.product_id = 2703
order by order_id, oi.product_id

select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
	ep.product_id, 
	rank() over (partition by oi.order_id order by item_id) rank_glass_row
from 
		Landing.mag.sales_flat_order_item_aud oi
	left join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
	inner join
		Landing.aux.prod_product_category pc on oi.product_id = pc.product_id and pc.category_bk = 'Glasses' and pc.product_id not in (3135, 3136, 3137, 3138)
where 
	--order_id in (5258886, 5288953, 5278580) and 
	ep.product_id is null
order by order_id, oi.item_id

select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
	ep.product_id, 
	rank() over (partition by oi.order_id order by item_id) rank_vision_type_row
from 
		Landing.mag.sales_flat_order_item_aud oi
	inner join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'vision type'

select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
	ep.product_id, 
	rank() over (partition by oi.order_id order by item_id) rank_package_type_row
from 
		Landing.mag.sales_flat_order_item_aud oi
	inner join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'package type'

--------------------------

select gl.item_id, gl.order_id, 
	gl.product_id, gl.sku, gl.name, gl.qty_ordered, gl.base_price, gl.base_row_total,
	vi.item_id, vi.product_id, vi.name, vi.product_name, pk.item_id, pk.product_id, pk.name, pk.product_name, pk.base_price, pk.base_row_total
from
		(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
			rank() over (partition by oi.order_id order by item_id) rank_glass_row
		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join	
				Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
			left join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
			inner join
				Landing.aux.prod_product_category pc on oi.product_id = pc.product_id and pc.category_bk = 'Glasses' and pc.product_id not in (2326, 2331, 3135, 3136, 3137, 3138)
		where ep.product_id is null) gl
	left join
		(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, ep.product_name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
			rank() over (partition by oi.order_id order by item_id) rank_vision_type_row
		from 
				Landing.mag.sales_flat_order_item_aud oi
			inner join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'vision type') vi on gl.order_id = vi.order_id and gl.rank_glass_row = vi.rank_vision_type_row
	left join
		(select oi.item_id, oi.order_id, oi.product_id, oi.sku, oi.name, ep.product_name, oi.qty_ordered, oi.base_price, oi.base_row_total, 
			rank() over (partition by oi.order_id order by item_id) rank_package_type_row
		from 
				Landing.mag.sales_flat_order_item_aud oi
			inner join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id and ep.info = 'package type') pk on gl.order_id = pk.order_id and gl.rank_glass_row = pk.rank_package_type_row
-- where pk.order_id is null
order by gl.order_id, gl.item_id