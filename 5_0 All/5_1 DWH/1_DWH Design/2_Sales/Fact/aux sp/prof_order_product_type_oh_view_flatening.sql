
select count(distinct idOrderHeader_sk_fk)
from Warehouse.sales.dim_order_header_product_type 

select oh.customer_id,
	oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.customer_status_name, oh.customer_order_seq_no,
	pt.idProductTypeOH_sk, pt.product_type_oh_name, 
	ohpt.order_percentage, ohpt.order_flag, ohpt.order_qty_time
from 
		Warehouse.sales.dim_order_header_product_type ohpt
	inner join
		Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
	inner join
		Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk	
where oh.customer_id = 1681296
order by customer_id, customer_order_seq_no, product_type_oh_name

select customer_id,
	idOrderHeader_sk, order_id_bk, order_no, customer_status_name, customer_order_seq_no, 
	Daily daily_perc, [Two Weeklies] two_weeklies_perc, Monthlies monthlies_perc, Colour colour_perc,
	Sunglasses sunglasses_perc, Glasses glasses_perc, Other other_perc
from
		(select oh.customer_id,
			oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.customer_status_name, oh.customer_order_seq_no,
			--pt.idProductTypeOH_sk, ohpt.order_flag, ohpt.order_qty_time
			pt.product_type_oh_name, ohpt.order_percentage			
		from 
				Warehouse.sales.dim_order_header_product_type ohpt
			inner join
				Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
			inner join
				Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk	
		--where oh.customer_id = 1681296
		) t
	pivot
		(min(order_percentage) for
		product_type_oh_name in (Daily, [Two Weeklies], Monthlies, Colour, Sunglasses, Glasses, Other)) pvt


select customer_id,
	idOrderHeader_sk, order_id_bk, order_no, customer_status_name, customer_order_seq_no, 
	Daily daily_qty_time, [Two Weeklies] two_weeklies_qty_time, Monthlies monthlies_qty_time, Colour colour_qty_time,
	Sunglasses sunglasses_qty_time, Glasses glasses_qty_time, Other other_qty_time
from
		(select oh.customer_id,
			oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.customer_status_name, oh.customer_order_seq_no,
			--pt.idProductTypeOH_sk, ohpt.order_flag, ohpt.order_percentage
			pt.product_type_oh_name, ohpt.order_qty_time			
		from 
				Warehouse.sales.dim_order_header_product_type ohpt
			inner join
				Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
			inner join
				Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk) t
	pivot
		(min(order_qty_time) for
		product_type_oh_name in (Daily, [Two Weeklies], Monthlies, Colour, Sunglasses, Glasses, Other)) pvt


order by customer_id, customer_order_seq_no, product_type_oh_name


---------------------------------------------


select 
	idOrderHeader_sk, order_id_bk, order_no, customer_id, customer_status_name, customer_order_seq_no, 
	Daily daily_perc, [Two Weeklies] two_weeklies_perc, Monthlies monthlies_perc, Colour colour_perc,
	Sunglasses sunglasses_perc, Glasses glasses_perc, Other other_perc,
	p_Daily daily_qty_time, [p_Two Weeklies] two_weeklies_qty_time, p_Monthlies monthlies_qty_time, p_Colour colour_qty_time,
	p_Sunglasses sunglasses_qty_time, p_Glasses glasses_qty_time, p_Other other_qty_time
from
		(select oh.customer_id,
			oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.customer_status_name, oh.customer_order_seq_no,
			--pt.idProductTypeOH_sk, ohpt.order_flag, ohpt.order_qty_time
			pt.product_type_oh_name, 'p_' + pt.product_type_oh_name product_type_oh_name_2, 
			ohpt.order_percentage, ohpt.order_qty_time			
		from 
				Warehouse.sales.dim_order_header_product_type ohpt
			inner join
				Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
			inner join
				Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk	
		where oh.customer_id = 1681296
		) t
	pivot
		(min(order_percentage) for
		product_type_oh_name in (Daily, [Two Weeklies], Monthlies, Colour, Sunglasses, Glasses, Other)) pvt
	pivot
		(min(order_qty_time) for
		product_type_oh_name_2 in (p_Daily, [p_Two Weeklies], p_Monthlies, p_Colour, p_Sunglasses, p_Glasses, p_Other)) pvt2


select idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	min(daily_perc) daily_perc, min(daily_qty_time) daily_qty_time, min(two_weeklies_perc) two_weeklies_perc, min(two_weeklies_qty_time) two_weeklies_qty_time, 
	min(monthlies_perc) monthlies_perc, min(monthlies_qty_time) monthlies_qty_time, min(colour_perc) colour_perc, min(colour_qty_time) colour_qty_time, 
	min(sunglasses_perc) sunglasses_perc, min(sunglasses_qty_time) sunglasses_qty_time, min(glasses_perc) glasses_perc, min(glasses_qty_time) glasses_qty_time, min(other_perc) other_perc, min(other_qty_time) other_qty_time
from
	(select customer_id,
		idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name,
		customer_status_name, customer_order_seq_no, 
		Daily daily_perc, [Two Weeklies] two_weeklies_perc, Monthlies monthlies_perc, Colour colour_perc,
		Sunglasses sunglasses_perc, Glasses glasses_perc, Other other_perc,
		p_Daily daily_qty_time, [p_Two Weeklies] two_weeklies_qty_time, p_Monthlies monthlies_qty_time, p_Colour colour_qty_time,
		p_Sunglasses sunglasses_qty_time, p_Glasses glasses_qty_time, p_Other other_qty_time
	from
			(select oh.customer_id,
				oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date_c, oh.store_name,
				oh.customer_status_name, oh.customer_order_seq_no,
				--pt.idProductTypeOH_sk, ohpt.order_flag, ohpt.order_qty_time
				pt.product_type_oh_name, 'p_' + pt.product_type_oh_name product_type_oh_name_2, 
				ohpt.order_percentage, ohpt.order_qty_time			
			from 
					Warehouse.sales.dim_order_header_product_type ohpt
				inner join
					Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
				inner join
					Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk	
			-- where oh.customer_id = 1681296
			) t
		pivot
			(min(order_percentage) for
			product_type_oh_name in (Daily, [Two Weeklies], Monthlies, Colour, Sunglasses, Glasses, Other)) pvt
		pivot
			(min(order_qty_time) for
			product_type_oh_name_2 in (p_Daily, [p_Two Weeklies], p_Monthlies, p_Colour, p_Sunglasses, p_Glasses, p_Other)) pvt2) t
group by idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name, customer_id, customer_status_name, customer_order_seq_no
order by customer_id, idOrderHeader_sk, order_id_bk, order_no, order_date_c, customer_status_name, customer_order_seq_no