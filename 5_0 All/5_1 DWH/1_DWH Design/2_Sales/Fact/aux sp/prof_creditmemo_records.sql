
select top 1000 entity_id, increment_id, order_id, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_adjustment, base_adjustment_positive, base_adjustment_negative
from Landing.mag.sales_flat_creditmemo_aud

select top 1000 entity_id, parent_id, order_item_id, 
	qty, base_row_total
from Landing.mag.sales_flat_creditmemo_item_aud


select top 1000 count(*) over () num_tot, 
	cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
	cri.qty, cr.base_subtotal, cri.base_row_total, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
	cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
from 
		Landing.mag.sales_flat_creditmemo_aud cr
	inner join	
		Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
where cr.order_id = 757758
order by cr.order_id, cri.order_item_id

select top 1000 count(*) over () num_tot, 
	cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
	cri.qty, cr.base_subtotal, cri.base_row_total, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
	cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
from 
		Landing.mag.sales_flat_creditmemo_aud cr
	left join	
		Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
where cri.parent_id is null
	and cr.order_id = 4062007
order by cr.order_id, cri.order_item_id

---------------------------------------------------------
--- CREDIT MEMO - ORDER ITEM INFORMATION

select top 1000 count(*) over () num_tot, 
	order_id, order_item_id, count(*) num_rep_rows, -- min(entity_id) min_creditmemo_id, 
	sum(qty) qty, sum(base_row_total) base_row_total, 
	sum(base_subtotal) base_subtotal, sum(base_shipping_amount) base_shipping_amount, sum(base_discount_amount) base_discount_amount, 
	sum(base_customer_balance_amount) base_customer_balance_amount, sum(base_grand_total) base_grand_total, 
	sum(base_adjustment) base_adjustment, sum(base_adjustment_positive) base_adjustment_positive, sum(base_adjustment_negative) base_adjustment_negative
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id, 
		dense_rank() over (partition by order_id, order_item_id order by cr.entity_id) num_cr,
		sum(cri.qty) over (partition by order_id, order_item_id) sum_qty,
		sum(cri.base_row_total) over (partition by order_id, order_item_id) sum_base_row_total,
		cri.qty, cri.base_row_total, 
		cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from 
			Landing.mag.sales_flat_creditmemo_aud cr
		inner join	
			Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) t
group by order_id, order_item_id
-- order by order_id, order_item_id
order by num_rep_rows desc, order_id, order_item_id

select count(*) over () num_tot, 
	order_id, order_item_id, entity_id, increment_id,
	num_rep_rows, sum_qty, sum_base_row_total
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id, 
		dense_rank() over (partition by order_id, order_item_id order by cr.entity_id) num_cr,
		count(*) over (partition by order_id, order_item_id) num_rep_rows,
		sum(cri.qty) over (partition by order_id, order_item_id) sum_qty,
		sum(cri.base_row_total) over (partition by order_id, order_item_id) sum_base_row_total,
		cri.qty, cri.base_row_total, 
		cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from 
			Landing.mag.sales_flat_creditmemo_aud cr
		inner join	
			Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) t
where num_cr = 1
order by num_rep_rows desc, order_id, order_item_id

----------------------------------------------------------
--- CREDIT MEMO - ORDER HEADER INFORMATION: ROWS WITH ITEM ROWS + ROWS WTIH NO ITEM ROWS

-- ROWS WITH ITEM ROWS 
select top 1000 count(*) over () num_tot, 
	order_id, count(*) num_rep,
	sum(base_subtotal) base_subtotal, sum(base_shipping_amount) base_shipping_amount, sum(base_discount_amount) base_discount_amount, 
	sum(base_customer_balance_amount) base_customer_balance_amount, sum(base_grand_total) base_grand_total, 
	sum(base_adjustment) base_adjustment, sum(base_adjustment_positive) base_adjustment_positive, sum(base_adjustment_negative) base_adjustment_negative
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, 
		cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from Landing.mag.sales_flat_creditmemo_aud cr
	where cr.entity_id in 
		(select distinct parent_id
		from Landing.mag.sales_flat_creditmemo_item_aud) ) t
group by order_id
order by num_rep desc, order_id

select count(*) over () num_tot, 
	order_id, entity_id, increment_id,
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, 
		dense_rank() over (partition by order_id order by cr.entity_id) num_cr,
		count(*) over (partition by order_id) num_rep_rows,
		sum(cr.base_subtotal) over (partition by order_id) sum_base_subtotal, 
		sum(cr.base_shipping_amount) over (partition by order_id) sum_base_shipping_amount, sum(cr.base_discount_amount) over (partition by order_id) sum_base_discount_amount, 
		sum(cr.base_customer_balance_amount) over (partition by order_id) sum_base_customer_balance_amount, sum(cr.base_grand_total) over (partition by order_id) sum_base_grand_total, 
		sum(cr.base_adjustment) over (partition by order_id) sum_base_adjustment, 
		sum(cr.base_adjustment_positive) over (partition by order_id) sum_base_adjustment_positive, sum(cr.base_adjustment_negative) over (partition by order_id) sum_base_adjustment_negative, 

		cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from Landing.mag.sales_flat_creditmemo_aud cr
	where cr.entity_id in 
		(select distinct parent_id
		from Landing.mag.sales_flat_creditmemo_item_aud) ) t
where num_cr = 1
order by num_rep_rows desc, order_id


-- ROWS WTIH NO ITEM ROWS
select top 1000 count(*) over () num_tot, 
	order_id, count(*) num_rep,
	sum(base_subtotal) base_subtotal, sum(base_row_total) base_row_total, sum(base_shipping_amount) base_shipping_amount, sum(base_discount_amount) base_discount_amount, 
	sum(base_customer_balance_amount) base_customer_balance_amount, sum(base_grand_total) base_grand_total, 
	sum(base_adjustment) base_adjustment, sum(base_adjustment_positive) base_adjustment_positive, sum(base_adjustment_negative) base_adjustment_negative
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
		cri.qty, cr.base_subtotal, cri.base_row_total, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from 
			Landing.mag.sales_flat_creditmemo_aud cr
		left join	
			Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
	where cri.parent_id is null) t
group by order_id
-- having sum(base_shipping_amount) = 0 and sum(base_adjustment) = 0 
-- having sum(base_shipping_amount) <> 0
-- having sum(base_adjustment) <> 0
order by num_rep desc, order_id

select count(*) over () num_tot, 
	order_id, entity_id, increment_id,
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
from
	(select  
		cr.entity_id, cr.increment_id, cr.order_id, 
		dense_rank() over (partition by order_id order by cr.entity_id) num_cr,
		count(*) over (partition by order_id) num_rep_rows,
		sum(cr.base_subtotal) over (partition by order_id) sum_base_subtotal, 
		sum(cr.base_shipping_amount) over (partition by order_id) sum_base_shipping_amount, sum(cr.base_discount_amount) over (partition by order_id) sum_base_discount_amount, 
		sum(cr.base_customer_balance_amount) over (partition by order_id) sum_base_customer_balance_amount, sum(cr.base_grand_total) over (partition by order_id) sum_base_grand_total, 
		sum(cr.base_adjustment) over (partition by order_id) sum_base_adjustment, 
		sum(cr.base_adjustment_positive) over (partition by order_id) sum_base_adjustment_positive, sum(cr.base_adjustment_negative) over (partition by order_id) sum_base_adjustment_negative, 

		cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
		cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
	from 
			Landing.mag.sales_flat_creditmemo_aud cr
		left join	
			Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
	where cri.parent_id is null) t
where num_cr = 1
order by num_rep_rows desc, order_id


---------------------------------------------------------------------------------------------------------------------------------------

select t.*, t2.*
from
		(select count(*) over () num_tot, 
			order_id, order_item_id, count(*) num_rep,
			sum(qty) qty,
			sum(base_subtotal) base_subtotal, sum(base_row_total) base_row_total, sum(base_shipping_amount) base_shipping_amount, sum(base_discount_amount) base_discount_amount, 
			sum(base_customer_balance_amount) base_customer_balance_amount, sum(base_grand_total) base_grand_total, 
			sum(base_adjustment) base_adjustment, sum(base_adjustment_positive) base_adjustment_positive, sum(base_adjustment_negative) base_adjustment_negative
		from
			(select  
				cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
				cri.qty, cr.base_subtotal, cri.base_row_total, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
				cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
			from 
					Landing.mag.sales_flat_creditmemo_aud cr
				inner join	
					Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) t
		group by order_id, order_item_id) t
	inner join
		(select top 1000 count(*) over () num_tot, 
			order_id, count(*) num_rep,
			sum(base_subtotal) base_subtotal, sum(base_row_total) base_row_total, sum(base_shipping_amount) base_shipping_amount, sum(base_discount_amount) base_discount_amount, 
			sum(base_customer_balance_amount) base_customer_balance_amount, sum(base_grand_total) base_grand_total, 
			sum(base_adjustment) base_adjustment, sum(base_adjustment_positive) base_adjustment_positive, sum(base_adjustment_negative) base_adjustment_negative
		from
			(select  
				cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
				cri.qty, cr.base_subtotal, cri.base_row_total, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
				cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
			from 
					Landing.mag.sales_flat_creditmemo_aud cr
				left join	
					Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
			where cri.parent_id is null) t
		group by order_id
		having sum(base_shipping_amount) <> 0 or sum(base_adjustment) <> 0) t2 on t.order_id = t2.order_id
order by t.order_id, t.order_item_id