


select top 1000 order_id_bk, idCustomer_sk_fk,
	idCalendarActivityDate_sk_fk, activity_date, 
	num_order, num_tot_orders,
	idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	idChannel_sk_fk, channel_name, idPriceType_sk_fk, price_type_name 
from Warehouse.act.fact_customer_signature_aux_orders
order by num_tot_orders desc, idCustomer_sk_fk, num_order

select top 1000 order_id_bk, idCustomer_sk_fk,
	num_order, num_tot_orders,
	idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	idChannel_sk_fk, channel_name, idPriceType_sk_fk, price_type_name,
	global_subtotal, 

	count(*) over (partition by idCustomer_sk_fk, idProductTypeOH_sk_fk, order_qty_time) num_rep_product_type_oh, 
	count(*) over (partition by idCustomer_sk_fk, idChannel_sk_fk) num_rep_channel,  
	count(*) over (partition by idCustomer_sk_fk, idPriceType_sk_fk) num_rep_price_type, 
	
	sum(global_subtotal) over (partition by idCustomer_sk_fk, idProductTypeOH_sk_fk, order_qty_time) num_rep_product_type_oh, 
	sum(global_subtotal) over (partition by idCustomer_sk_fk, idChannel_sk_fk) sum_rep_channel,  
	sum(global_subtotal) over (partition by idCustomer_sk_fk, idPriceType_sk_fk) sum_rep_price_type

from Warehouse.act.fact_customer_signature_aux_orders
where idCustomer_sk_fk = 131399
order by num_tot_orders desc, idCustomer_sk_fk, num_order

-----------------------------------------------------------------------------------



select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	count(*) num_rep_product_type_oh, sum(global_subtotal) sum_rep_product_type_oh
from Warehouse.act.fact_customer_signature_aux_orders
group by idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	num_rep_product_type_oh, sum_rep_product_type_oh, 
	max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk, num_tot_orders) max_rep_product_type_oh
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
		count(*) num_rep_product_type_oh, sum(global_subtotal) sum_rep_product_type_oh
	from Warehouse.act.fact_customer_signature_aux_orders
	group by idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time) t
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	num_rep_product_type_oh, sum_rep_product_type_oh, 
	count(*) over (partition by idCustomer_sk_fk, num_tot_orders) num_rep_max_product_type_oh, 
	max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk, num_tot_orders) max_rep_product_type_oh
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
		num_rep_product_type_oh, sum_rep_product_type_oh, 
		max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk, num_tot_orders) max_rep_product_type_oh
	from
		(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
			count(*) num_rep_product_type_oh, sum(global_subtotal) sum_rep_product_type_oh
		from Warehouse.act.fact_customer_signature_aux_orders
		group by idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time) t) t
where num_rep_product_type_oh = max_rep_product_type_oh
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	num_rep_product_type_oh, sum_rep_product_type_oh
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
		num_rep_product_type_oh, sum_rep_product_type_oh, 
		dense_rank() over (partition by idCustomer_sk_fk order by order_qty_time, product_type_oh_name) num_row
	from
		(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
			num_rep_product_type_oh, sum_rep_product_type_oh, 
			count(*) over (partition by idCustomer_sk_fk) num_rep_max_product_type_oh, 
			max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
		from
			(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
				num_rep_product_type_oh, sum_rep_product_type_oh, 
				max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
			from
				(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
					count(*) num_rep_product_type_oh, sum(global_subtotal) sum_rep_product_type_oh
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time) t) t
		where num_rep_product_type_oh = max_rep_product_type_oh) t
	where sum_rep_product_type_oh = max_rep_product_type_oh) t
where num_row = 1
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc



-----------------------------------------------------------------------------------

select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
from Warehouse.act.fact_customer_signature_aux_orders
group by idCustomer_sk_fk, num_tot_orders
having count(distinct idProductTypeOH_sk_fk) = 1
order by num_tot_orders desc, idCustomer_sk_fk

select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh,
	count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
from 
		Warehouse.act.fact_customer_signature_aux_orders so
	inner join
		(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
		from Warehouse.act.fact_customer_signature_aux_orders
		group by idCustomer_sk_fk, num_tot_orders
		having count(distinct idProductTypeOH_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh
order by so.num_tot_orders desc, so.idCustomer_sk_fk, num_rep_product_type_oh desc


select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
	count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
from 
		Warehouse.act.fact_customer_signature_aux_orders so
	inner join
		(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
		from Warehouse.act.fact_customer_signature_aux_orders
		group by idCustomer_sk_fk, num_tot_orders
		having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name 
order by so.num_tot_orders desc, so.idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
	num_rep_product_type_oh, sum_rep_product_type_oh, 
	max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
from
	(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
		count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
	from 
			Warehouse.act.fact_customer_signature_aux_orders so
		inner join
			(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
			from Warehouse.act.fact_customer_signature_aux_orders
			group by idCustomer_sk_fk, num_tot_orders
			having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
	group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name) t 
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
	num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
	max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_sum_rep_product_type_oh
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
		num_dist_product_type_oh,
		num_rep_product_type_oh, sum_rep_product_type_oh, 
		max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
	from
		(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
			t.num_dist_product_type_oh,
			count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
		from 
				Warehouse.act.fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
		group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh) t) t 
where num_rep_product_type_oh = max_rep_product_type_oh
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
	num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
	dense_rank() over (partition by idCustomer_sk_fk order by product_type_oh_name) num_row
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
		num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
		max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_sum_rep_product_type_oh
	from
		(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
			num_dist_product_type_oh,
			num_rep_product_type_oh, sum_rep_product_type_oh, 
			max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
		from
			(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
				t.num_dist_product_type_oh,
				count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
			from 
					Warehouse.act.fact_customer_signature_aux_orders so
				inner join
					(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
					from Warehouse.act.fact_customer_signature_aux_orders
					group by idCustomer_sk_fk, num_tot_orders
					having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
			group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh) t) t 
	where num_rep_product_type_oh = max_rep_product_type_oh) t
where sum_rep_product_type_oh = max_sum_rep_product_type_oh
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc

select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
	num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh
from
	(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
		num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
		dense_rank() over (partition by idCustomer_sk_fk order by product_type_oh_name) num_row
	from
		(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
			num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
			max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_sum_rep_product_type_oh
		from
			(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
				num_dist_product_type_oh,
				num_rep_product_type_oh, sum_rep_product_type_oh, 
				max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
			from
				(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
					t.num_dist_product_type_oh,
					count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
				from 
						Warehouse.act.fact_customer_signature_aux_orders so
					inner join
						(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
						from Warehouse.act.fact_customer_signature_aux_orders
						group by idCustomer_sk_fk, num_tot_orders
						having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
				group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh) t) t 
		where num_rep_product_type_oh = max_rep_product_type_oh) t
	where sum_rep_product_type_oh = max_sum_rep_product_type_oh) t
where num_row = 1
order by num_tot_orders desc, idCustomer_sk_fk, num_rep_product_type_oh desc