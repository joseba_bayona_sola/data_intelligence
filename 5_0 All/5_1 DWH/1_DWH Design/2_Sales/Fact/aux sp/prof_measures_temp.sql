


select oh.order_id_bk, odo.order_status_name_bk, odo.adjustment_order, 
	oh.local_subtotal, oh.local_shipping, oh.local_shipping_given, oh.local_discount, 
	oh.local_store_credit_used, oh.local_store_credit_given, oh.local_bank_online_given, oh.adjustment_pos, oh.adjustment_neg, 
	oh.local_total_inc_vat, 
	oh.local_refund, 
	oh.local_to_global_rate, oh.order_currency_code, 
	oh.discount_percent
from 
		#sales_order_header_measures oh
	inner join
		Landing.aux.sales_order_header_dim_order odo on oh.order_id_bk = odo.order_id_bk
order by oh.order_id_bk

select oh.order_id_bk, 
	oh.local_total_inc_vat, oh.local_subtotal + oh.local_shipping - oh.local_discount - oh.local_store_credit_used local_total_inc_vat, 
	abs(oh.local_total_inc_vat - (oh.local_subtotal + oh.local_shipping - oh.local_discount - oh.local_store_credit_used)) diff_total
from 
		#sales_order_header_measures oh
	inner join
		Landing.aux.sales_order_header_dim_order odo on oh.order_id_bk = odo.order_id_bk
order by oh.order_id_bk


select oh.order_id_bk, odo.order_status_name_bk, odo.adjustment_order, 
	oh.local_subtotal, oh.local_shipping, oh.local_shipping_given, oh.local_discount, 
	oh.local_store_credit_used, oh.local_store_credit_given, oh.local_bank_online_given, oh.adjustment_pos, oh.adjustment_neg, 
	oh.local_total_inc_vat, 
	oh.local_refund
from 
		#sales_order_header_measures oh
	inner join
		Landing.aux.sales_order_header_dim_order odo on oh.order_id_bk = odo.order_id_bk
-- where oh.local_refund <> 0
where odo.adjustment_order = 'Y' and oh.local_shipping_given <> 0 
-- where oh.adjustment_pos <> 0 or oh.adjustment_neg <> 0
order by oh.order_id_bk


----------------------------------------------------

select ol.order_id_bk, ol.order_line_id_bk, odo.order_status_name_bk, odo.adjustment_order, 
	ol.price_type_name_bk,
	ol.qty_unit, ol.qty_unit_refunded, ol.qty_pack, ol.qty_time, 
	ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
	ol.local_subtotal, ol.local_discount, 
	ol.local_refund, 
	ol.discount_percent, 
	ol.order_allocation_rate
from 
		#sales_order_header_line_measures ol
	inner join
		Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
where ol.order_id_bk = 4795183
order by ol.order_id_bk, ol.order_line_id_bk


4795183
4845811



