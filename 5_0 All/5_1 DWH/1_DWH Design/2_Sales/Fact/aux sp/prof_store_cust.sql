
select o.entity_id, o.store_id, o.customer_id, 
	o.shipping_address_id, o.billing_address_id, 
	oas.country_id country_id_shipping, oas.region_id region_id_shipping, oas.postcode postcode_shipping, 
	oab.country_id country_id_billing, oab.region_id region_id_billing, oab.postcode postcode_billing
from 
		Landing.aux.sales_order_header_o oh
	inner join
		Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	inner join -- left
		Landing.mag.sales_flat_order_address_aud oas on o.shipping_address_id = oas.entity_id
	inner join -- left
		Landing.mag.sales_flat_order_address_aud oab on o.billing_address_id = oab.entity_id
order by o.entity_id

-- select store_id, count(*)
-- select country_id_shipping, count(*)
select region_id_shipping, count(*)
from
	(select o.entity_id, o.store_id, o.customer_id, 
		o.shipping_address_id, o.billing_address_id, 
		oas.country_id country_id_shipping, oas.region_id region_id_shipping, oas.postcode postcode_shipping, 
		oab.country_id country_id_billing, oab.region_id region_id_billing, oab.postcode postcode_billing
	from 
			Landing.aux.sales_order_header_o oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		inner join
			Landing.mag.sales_flat_order_address_aud oas on o.shipping_address_id = oas.entity_id
		inner join
			Landing.mag.sales_flat_order_address_aud oab on o.billing_address_id = oab.entity_id)  t
--group by store_id
--order by store_id
-- group by country_id_shipping
-- order by country_id_shipping
group by region_id_shipping
order by region_id_shipping



