
drop table #sales_order_header_measures

create table #sales_order_header_measures(
	order_id_bk						bigint NOT NULL,
	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_shipping_given			decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	adjustment_pos					decimal(12, 4),
	adjustment_neg					decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_refund					decimal(12, 4),
	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),
	discount_percent				decimal(12, 4))

	insert into #sales_order_header_measures(order_id_bk, 
		local_subtotal, local_shipping, local_shipping_given, local_discount, 
		local_store_credit_used, local_store_credit_given, local_bank_online_given, adjustment_pos, adjustment_neg, 
		local_total_inc_vat, 
		local_refund, 
		local_to_global_rate, order_currency_code, 
		discount_percent)

		select oh.order_id_bk, -- odo.order_status_name_bk, odo.adjustment_order, 
			o.base_subtotal local_subtotal, o.base_shipping_amount local_shipping, isnull(cr.sum_base_shipping_amount, 0) local_shipping_given,
			case when (o.base_discount_amount < 0) then o.base_discount_amount * -1 else o.base_discount_amount end local_discount, 
			o.base_customer_balance_amount local_store_credit_used, isnull(o.bs_customer_bal_total_refunded, 0) local_store_credit_given, 
			(isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0)) - isnull(o.bs_customer_bal_total_refunded, 0) local_bank_online_given, 
			isnull(cr.sum_base_adjustment_positive, 0) adjustment_pos, isnull(cr.sum_base_adjustment_negative, 0) adjustment_neg, 
			o.base_grand_total local_total_inc_vat, -- cr.sum_base_subtotal_cr,
			isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0) local_refund, 
			o.base_to_global_rate local_to_global_rate, o.order_currency_code order_currency_code, 
			convert(decimal(10,1), (case when (o.base_discount_amount < 0) then o.base_discount_amount * -1 else o.base_discount_amount end * 100 / o.base_subtotal)) discount_percent
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.aux.sales_order_header_dim_order odo on oh.order_id_bk = odo.order_id_bk
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				(select cr.order_id, min(cr.num_order) min_num_order, max(cr.num_order) max_num_order,
					sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal_cr, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total_cr, 
					sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount,
					sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative 
				from 
						Landing.aux.mag_sales_flat_creditmemo cr
					inner join
						Landing.aux.sales_order_header_o_i_s_cr oh on cr.order_id = oh.order_id_bk
				group by order_id) cr on oh.order_id_bk = cr.order_id
		-- where odo.order_status_name_bk not in ('OK', 'CANCEL')
		-- where o.base_discount_amount <> 0
		where odo.adjustment_order = 'Y'
		order by oh.order_id_bk

select order_id_bk, 
	local_total_inc_vat, local_subtotal + local_shipping - local_discount - local_store_credit_used local_total_inc_vat, 
	abs(local_total_inc_vat - (local_subtotal + local_shipping - local_discount - local_store_credit_used)) diff_total, 
	local_refund
from #sales_order_header_measures
order by diff_total desc