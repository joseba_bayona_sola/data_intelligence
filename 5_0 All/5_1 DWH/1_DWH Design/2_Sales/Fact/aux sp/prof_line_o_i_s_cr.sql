
select *
from Landing.mag.sales_flat_order_aud
where entity_id in (5275010, 5279957, 5090579, 5056738, 5002487)

select * -- order_id, item_id
from Landing.mag.sales_flat_order_item_aud
where order_id in (5275010, 5279957, 5090579, 5056738, 5002487)
order by order_id, item_id

select * --order_id, entity_id, increment_id, created_at
from Landing.mag.sales_flat_invoice_aud
where order_id in (5275010, 5279957, 5090579, 5056738)
order by order_id

select * -- parent_id, entity_id, order_item_id
from Landing.mag.sales_flat_invoice_item_aud
where parent_id in (4437266, 4618012, 4622914, 4622916, 4404050)
order by parent_id, order_item_id


select * -- order_id, entity_id, increment_id, created_at
from Landing.mag.sales_flat_shipment_aud
where order_id in (5275010, 5279957, 5090579, 5056738)
order by order_id

select * -- parent_id, entity_id, order_item_id
from Landing.mag.sales_flat_shipment_item
where parent_id in (4460796, 4645995, 4646564, 4643730, 4432676)


select * -- order_id, entity_id, increment_id, created_at
from Landing.mag.sales_flat_creditmemo_aud
where order_id in (5275010, 5279957, 5090579, 5056738, 5002487)
order by order_id

select * -- parent_id, entity_id, order_item_id
from Landing.mag.sales_flat_creditmemo_item_aud
where parent_id in (1253242, 1253167, 1253168, 1251531, 1252956)

----------------------------------------------------------------------

select 
 	oi.item_id order_line_id_bk, oh.order_id_bk, 
	oh.order_no
from 
		Landing.aux.sales_order_header_o_i_s_cr oh
	inner join
		Landing.mag.sales_flat_order_item_aud oi on oh.order_id_bk = oi.order_id

select 
 	oi.item_id order_line_id_bk, oh.order_id_bk, 
	ii.invoice_id, si.shipment_id, cri.creditmemo_id,
	oh.order_no, ii.invoice_no, si.shipment_no, cri.creditmemo_no,
	ii.invoice_line_id, si.shipment_line_id, cri.creditmemo_line_id,
	si.num_shipment_lines, cri.num_creditmemo_lines,
	ii.idCalendarInvoiceDate, ii.invoice_date, 
	si.shipment_date,
	cri.refund_date
from 
		Landing.aux.sales_order_header_o_i_s_cr oh
	inner join
		Landing.mag.sales_flat_order_item_aud oi on oh.order_id_bk = oi.order_id
	left join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
	left join
		(select oh.order_id_bk, oh.invoice_id, oh.invoice_no, 
			ii.entity_id invoice_line_id, ii.order_item_id,
			oh.idCalendarInvoiceDate, oh.invoice_date
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_invoice_aud i on oh.invoice_id = i.entity_id
			inner join
				Landing.mag.sales_flat_invoice_item_aud ii on i.entity_id = ii.parent_id) ii on oh.order_id_bk = ii.order_id_bk and oi.item_id = ii.order_item_id
	left join		
		(select order_id_bk, shipment_id, shipment_no, 
			shipment_line_id, order_item_id, 
			num_shipment_lines,
			shipment_date
		from
			(select oh.order_id_bk, 
				s.entity_id shipment_id, s.increment_id shipment_no, 
				si.entity_id shipment_line_id, si.order_item_id,
				s.created_at shipment_date,
				count(*) over (partition by si.order_item_id) num_shipment_lines,
				rank() over (partition by si.order_item_id order by s.entity_id) rank_s
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id
				inner join
					Landing.mag.sales_flat_shipment_item_aud si on s.entity_id = si.parent_id) s --where oh.order_id_bk = 5275010
		where rank_s = 1) si on oh.order_id_bk = si.order_id_bk and oi.item_id = si.order_item_id
	left join
		(select order_id_bk, creditmemo_id, creditmemo_no, 
			creditmemo_line_id, order_item_id, 
			num_creditmemo_lines,
			refund_date
		from
			(select oh.order_id_bk, 
				cr.entity_id creditmemo_id, cr.increment_id creditmemo_no, 
				cri.entity_id creditmemo_line_id, cri.order_item_id,
				cr.created_at refund_date,
				count(*) over (partition by cri.order_item_id) num_creditmemo_lines,
				rank() over (partition by cri.order_item_id order by cr.entity_id) rank_cr
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				inner join
					Landing.mag.sales_flat_creditmemo_aud cr on oh.order_id_bk = cr.order_id
				inner join
					Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) cr -- where cr.order_id = 5002487
		where rank_cr = 1) cri on oh.order_id_bk = cri.order_id_bk and oi.item_id = cri.order_item_id
where ep.product_id is null
order by oh.order_id_bk, oi.item_id 

-------------------------------------------------
	-- SPECIAL CASES -- 
	
	-- ORDERS ITEMS IN MANY INVOICES (DUPLICATED INVOICES)
	select *
	from
		(select count(*) over (partition by order_item_id) num_rep_inv, *
		from Landing.mag.sales_flat_invoice_item_aud) t
	where num_rep_inv > 1
	order by num_rep_inv desc, order_item_id, entity_id

	-- ORDERS ITEMS IN MANY SHIPMENTS
	select *
	from
		(select count(*) over (partition by order_item_id) num_rep_sh, *
		from Landing.mag.sales_flat_shipment_item_aud) t
	where num_rep_sh > 1
	order by num_rep_sh desc, order_item_id, entity_id

	-- ORDERS ITEMS IN MANY CREDITMEMOS
	select *
	from
		(select count(*) over (partition by order_item_id) num_rep_cr, *
		from Landing.mag.sales_flat_creditmemo_item_aud) t
	where num_rep_cr > 1
	order by num_rep_cr desc, order_item_id, entity_id


---------------------------------

