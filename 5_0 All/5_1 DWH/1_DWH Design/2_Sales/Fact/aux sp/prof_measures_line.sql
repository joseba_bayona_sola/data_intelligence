
drop table #sales_order_header_line_measures

create table #sales_order_header_line_measures(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	price_type_name_bk				varchar(40),
	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),
	local_subtotal					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_refund					decimal(12, 4),

	discount_percent				decimal(12, 4),
	order_allocation_rate			decimal(12, 4))
go

insert into #sales_order_header_line_measures(order_line_id_bk, order_id_bk, 
	price_type_name_bk,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_discount, 
	local_refund, 
	discount_percent, 
	order_allocation_rate) 

	select ol.order_line_id_bk, ol.order_id_bk, -- odo.order_status_name_bk, odo.adjustment_order, 
		-- oi.store_id, olp.product_id_bk, 
		-- pla.promo_key, pla.price, pp.value,
		case when (oi.base_discount_amount <> 0) then
			case 
				when (pla.order_item_id is not null) then 'PLA & Discounted'
				when (pp.value is not null) then 'Tier Pricing & Discounted'
				else 'Discounted'
			end
			else case 
				when (pla.order_item_id is not null) then 'PLA'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end
		end price_type_name_bk,
		-- convert(int, isnull(pfps.size, 1)) pack_size,
		oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) qty_pack, 
		oi.qty_ordered / pc.num_months qty_time,
		oi.base_price local_price_unit, 
		oi.base_row_total / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack, 
		(oi.base_row_total - oi.base_discount_amount) / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack_discount, 
		oi.base_row_total local_subtotal, oi.base_discount_amount local_discount, 
		oi.base_amount_refunded local_refund, 
		oi.discount_percent discount_percent,
		null order_allocation_rate
	from 
			Landing.aux.sales_order_line_o_i_s_cr ol
		inner join
			Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
		inner join
			Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
		inner join
			Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
		left join 
			Landing.mend.gen_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
		inner join
			(select product_id, category_bk, 
				case 
					when (category_bk = 'Daily') then 30
					when (category_bk = 'Two Weeklies') then 2
					when (category_bk = 'Monthlies') then 1
					when (category_bk = 'Sunglasses') then 0.08333
					when (category_bk = 'Glasses') then 0.08333
					else 1
				end num_months
			from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
		left join
			Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
		left join
			(select store_id, product_id, value, qty
			from Landing.aux.mag_catalog_product_price_aud
			where price_type = 'Tier Pricing') pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
	where olp.order_line_id_vision_type is null -- and pfps.magentoProductID_int is null
		-- and odo.order_status_name_bk not in ('OK', 'CANCEL')
		-- and oi.base_discount_amount <> 0
	
	order by ol.order_id_bk, ol.order_line_id_bk
	-- order by olp.product_id_bk, oi.store_id, ol.order_id_bk, ol.order_line_id_bk


	select ol.order_line_id_bk, ol.order_id_bk, -- odo.order_status_name_bk, odo.adjustment_order, 
		-- oi.store_id, olp.product_id_bk, 
		-- pla.promo_key, pla.price, pp.value,
		case when (oi.base_discount_amount <> 0) then
			case 
				when (pla.order_item_id is not null) then 'PLA & Discounted'
				when (pp.value is not null) then 'Tier Pricing & Discounted'
				else 'Discounted'
			end
			else case 
				when (pla.order_item_id is not null) then 'PLA'
				when (pp.value is not null) then 'Tier Pricing'
				else 'Regular'
			end
		end price_type_name_bk,
		-- convert(int, isnull(pfps.size, 1)) pack_size,
		oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) qty_pack, 
		oi.qty_ordered / pc.num_months qty_time,
		oi.base_price local_price_unit, 
		oi.base_row_total / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack, 
		(oi.base_row_total - oi.base_discount_amount) / convert(int, (oi.qty_ordered / isnull(pfps.size, 1))) local_price_pack_discount, 
		oi.base_row_total + oi2.base_row_total local_subtotal, 
		oi.base_discount_amount local_discount, 
		oi.base_amount_refunded local_refund, 
		oi.discount_percent discount_percent,
		null order_allocation_rate
	from 
			Landing.aux.sales_order_line_o_i_s_cr ol
		inner join
			Landing.aux.sales_order_header_dim_order odo on ol.order_id_bk = odo.order_id_bk
		inner join
			Landing.aux.sales_order_line_product olp on ol.order_line_id_bk = olp.order_line_id_bk
		inner join
			Landing.mag.sales_flat_order_item_aud oi on ol.order_line_id_bk = oi.item_id
		left join 
			Landing.mend.gen_productfamilypacksize_v pfps on olp.product_id_bk = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
		inner join
			(select product_id, category_bk, 
				case 
					when (category_bk = 'Daily') then 30
					when (category_bk = 'Two Weeklies') then 2
					when (category_bk = 'Monthlies') then 1
					when (category_bk = 'Sunglasses') then 0.08333
					when (category_bk = 'Glasses') then 0.08333
					else 1
				end num_months
			from Landing.aux.prod_product_category) pc on olp.product_id_bk = pc.product_id 
		left join
			Landing.mag.po_sales_pla_item_aud pla on ol.order_line_id_bk = pla.order_item_id
		left join
			(select store_id, product_id, value, qty
			from Landing.aux.mag_catalog_product_price_aud
			where price_type = 'Tier Pricing') pp on oi.store_id = pp.store_id and olp.product_id_bk = pp.product_id and oi.qty_ordered >= pp.qty
		inner join
			Landing.mag.sales_flat_order_item_aud oi2 on olp.order_line_id_package_type = oi2.item_id
	where olp.order_line_id_package_type is not null -- and pfps.magentoProductID_int is null
		-- and odo.order_status_name_bk not in ('OK', 'CANCEL')
		-- and oi.base_discount_amount <> 0
	order by ol.order_id_bk, ol.order_line_id_bk

	select *
	from Landing.mend.gen_productfamilypacksize_v

	select product_id, category_bk, 
		case 
			when (category_bk = 'Daily') then 30
			when (category_bk = 'Two Weeklies') then 2
			when (category_bk = 'Monthlies') then 1
			when (category_bk = 'Sunglasses') then 0.08333
			when (category_bk = 'Glasses') then 0.08333
			else 1
		end num_months
	from Landing.aux.prod_product_category 
	order by category_bk

	select store_id, product_id, value, qty
	from Landing.aux.mag_catalog_product_price_aud
	where price_type = 'Tier Pricing'
	order by product_id

	select top 1000 *
	from Landing.mag.po_sales_pla_item_aud



-----------------------------------------------------------

	select o.local_subtotal, sum(ol.local_subtotal) over (partition by ol.order_id_bk) total_subtotal, 
		convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal) allocation_rate,
		sum(convert(decimal(12, 4), ol.local_subtotal / o.local_subtotal)) over (partition by ol.order_id_bk) total_allocation_rate,
		*
	from 
			#sales_order_header_line_measures ol
		inner join
			#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk
	-- where ol.order_id_bk in (5197796, 4406838, 5300217, 4845811)
	order by total_allocation_rate desc, ol.order_id_bk, ol.order_line_id_bk

-----------------------------------------------------------






