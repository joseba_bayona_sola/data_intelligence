

select *
from Landing.aux.sales_order_header_o

select *
from Landing.aux.sales_order_header_o_i_s_cr

select entity_id, affilCode, coupon_code, applied_rule_ids
from Landing.mag.sales_flat_order
order by entity_id desc

	-- channel_name_bk
	select o.entity_id, o.affilCode
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	where o.affilCode is not null
	order by o.affilCode

	select affilCode, count(*)
	from Landing.mag.sales_flat_order_aud
	group by affilCode
	order by affilCode

	-- coupon_id_bk
	select o.entity_id, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	where o.coupon_code is not null or o.applied_rule_ids is not null
	order by o.applied_rule_ids, o.coupon_code

	select applied_rule_ids, count(*)
	from Landing.mag.sales_flat_order_aud
	group by applied_rule_ids
	order by applied_rule_ids

	select entity_id, increment_id, created_at, 
		base_subtotal, base_discount_amount, 
		applied_rule_ids, coupon_code
	from Landing.mag.sales_flat_order_aud
	where len(applied_rule_ids) > 4


select o.order_id_bk, cc.coupon_id_bk,
	o.coupon_code, o.applied_rule_ids, 
	cc.coupon_code
from
		(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		where (o.coupon_code is not null or o.applied_rule_ids is not null)
			and len(o.applied_rule_ids) < 5) o
	left join
		(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
			isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code
		from 
				Landing.mag.salesrule_coupon_aud src
			right join
				Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
order by o.applied_rule_ids, o.coupon_code

select o.order_id_bk, cc.coupon_id_bk,
	o.coupon_code, o.applied_rule_ids, 
	cc.coupon_code
from
		(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			right join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		where (o.coupon_code is not null or o.applied_rule_ids is not null)
			and len(o.applied_rule_ids) >= 5) o
	left join
		(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
			isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code
		from 
				Landing.mag.salesrule_coupon_aud src
			right join
				Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code
order by o.applied_rule_ids, o.coupon_code