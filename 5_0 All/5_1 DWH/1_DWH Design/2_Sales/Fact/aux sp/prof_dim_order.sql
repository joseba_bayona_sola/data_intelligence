
select *
from Landing.aux.sales_order_header_o

select *
from Landing.aux.sales_order_header_o_i_s_cr

select entity_id, postoptics_source, presc_verification_method, postoptics_auto_verification,
	telesales_method_code, 
	referafriend_code, referafriend_referer
from Landing.mag.sales_flat_order
order by entity_id desc

select top 1000 entity_id, parent_id, 
	method, cc_type, 
	amount_authorized, amount_canceled, 
	cc_exp_month, cc_exp_year, cc_ss_start_month, cc_ss_start_year, cc_last4, cc_status_description, cc_owner, cc_status, cc_ss_issue, cc_avs_status
from Landing.mag.sales_flat_order_payment
order by parent_id desc

---------------------------------------------------------------------------------------------------------

	-- order_stage_name_bk (ORDER - INVOICE - SHIPMENT) --> OK
	select order_id_bk, 
		case when (shipment_id is not null) then 'SHIPMENT' else 
			case when (invoice_id is not null) then 'INVOICE' else 'ORDER' end end order_stage_name_bk
	from Landing.aux.sales_order_header_o_i_s_cr

		select invoice_f, shipment_f, count(*)
		from
			(select 
				case when (invoice_id is null) then 0 else 1 end invoice_f, 
				case when (shipment_id is null) then 0 else 1 end shipment_f
			from Landing.aux.sales_order_header_o_i_s_cr) t
		group by invoice_f, shipment_f
		order by invoice_f, shipment_f

	-- order_status_name_bk (OK - CANCEL - REFUND - PARTIAL REFUND)

		-- cancel --> status and base_subtotal_canceled / base_total_canceled
		select top 10000 count(*) over () num_tot,
			o.entity_id, o.increment_id, o.store_id, o.customer_id, o.created_at, oh.invoice_id,
			o.status, 
			o.base_subtotal, o.base_subtotal_canceled, 
			o.base_grand_total, o.base_total_canceled,
			o.base_total_refunded
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			right join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		where ((o.status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0)) or (base_total_canceled is not null)) 
			and oh.invoice_id is null
			and ((o.base_total_refunded is null or o.base_total_refunded = 0) and (o.base_customer_balance_refunded is null or o.base_customer_balance_refunded = 0))

		-- where ((o.status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0)) or (base_total_canceled is not null)) and o.base_total_refunded is not null --> Refunded should be 0

		-- where o.status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0)  --> "Manual" way to Cancel just changing the status when invoice still not created
		-- where o.status <> 'canceled' and base_total_canceled is not null --> Error in Current DWH: Really Cancelled orders don't appear as Cancel
		order by o.created_at desc

		-- refund any / adjusment
		select top 1000 cr.min_num_order, cr.max_num_order,
			case when (o.entity_id is not null) then o.entity_id else cr.order_id end order_id_bk,
			case when (cr.min_num_order = 2) then 'OK'
				else case when (abs(base_grand_total - base_total_refunded) < 0.1 and abs(isnull(o.base_customer_balance_amount, 0) - isnull(o.base_customer_balance_refunded, 0)) < 0.1) then 'REFUND' 
					else 'PARTIAL REFUND' end end order_status_name_bk,
			case when (cr.max_num_order = 2) then 'Y' else 'N' end adjustment_order,
			o.base_subtotal, o.base_grand_total,
			abs(base_grand_total - base_total_refunded) diff,
			abs(isnull(o.base_customer_balance_amount, 0) - isnull(o.base_customer_balance_refunded, 0)) diff_2,
			o.base_customer_balance_amount, o.base_customer_balance_refunded,
			o.base_total_refunded, cr.sum_base_grand_total_cr,
			base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			right join
				(select cr.order_id, min(cr.num_order) min_num_order, max(cr.num_order) max_num_order,
					sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total_cr
				from 
						Landing.aux.mag_sales_flat_creditmemo cr
					inner join
						Landing.aux.sales_order_header_o_i_s_cr oh on cr.order_id = oh.order_id_bk
				group by order_id) cr on o.entity_id = cr.order_id
			left join
				#oh_status t on oh.order_id_bk = t.order_id_bk			
		where t.order_id_bk is null and
			((base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or (base_total_refunded is not null and base_total_refunded <> 0)
				or cr.order_id is not null)
		order by cr.min_num_order, diff desc, diff_2 desc, o.created_at desc

		-- refund total --> status and base_total_refunded - base_total_offline_refunded - base_total_online_refunded - bs_customer_bal_total_refunded and creditmemo records
		-- Also Considier base_customer_balance_amount <> base_customer_balance_refunded as Partial
		select top 1000 count(*) over () num_tot,
			o.entity_id, o.increment_id, o.store_id, o.customer_id, o.created_at, oh.invoice_id,
			o.status, 
			o.base_subtotal, o.base_grand_total,
			abs(base_grand_total - base_total_refunded) diff,
			o.base_customer_balance_amount, o.base_customer_balance_refunded,
			o.base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			right join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		-- where o.status = 'closed' and abs(base_grand_total - base_total_refunded) < 0.1
		where o.status <> 'closed' and (base_total_refunded is not null) and abs(base_grand_total - base_total_refunded) < 0.1 -- and base_total_refunded <> 0
		order by diff desc, o.created_at desc

		-- refund partial 
		select top 1000 count(*) over () num_tot,
			o.entity_id, o.increment_id, o.store_id, o.customer_id, o.created_at, oh.invoice_id,
			o.status, 
			o.base_subtotal, o.base_grand_total,
			abs(base_grand_total - base_total_refunded) diff,
			o.base_customer_balance_amount, o.base_customer_balance_refunded,
			o.base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			right join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		-- where o.status = 'closed' and abs(base_grand_total - base_total_refunded) > 0.1
		where o.status <> 'closed' and base_total_refunded is not null and abs(base_grand_total - base_total_refunded) > 0.1
		order by o.created_at desc

	-- order_type_name_bk (Web - Automatic - Telesales)
	select o.entity_id, o.automatic_reorder, o.postoptics_source, 
		case 
			when (o.automatic_reorder = 1) then 'Automatic' 
			when (o.postoptics_source = 'user') then 'Web'
			when (o.postoptics_source = 'telesales') then 'Telesales'
			when o.postoptics_source is null then 'Web'
			else o.postoptics_source
		end order_type_name_bk
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id

	-- status_bk
	select oh.status, sm.order_status_magento, num
	from 
			(select o.status, count(*) num
			from 
						Landing.aux.sales_order_header_o_i_s_cr oh
					inner join
						Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			group by o.status) oh
		left join
			Landing.map.sales_order_status_magento_aud sm on oh.status = sm.order_status_magento_code
	order by oh.status

	-- payment_method_name_bk // cc_type_name_bk
	select o.entity_id, 
		p.entity_id, 
		p.method, pm.payment_method,
		p.cc_type, cc.cc_type

	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		inner join
			Landing.mag.sales_flat_order_payment_aud p on o.entity_id = p.parent_id
		left join
			Landing.map.sales_payment_method_aud pm on p.method = pm.payment_method_code
		left join
			Landing.map.sales_cc_type_aud cc on p.cc_type = cc.card_type_code
	order by pm.payment_method, o.entity_id

	
	-- shipping_description_bk
	select o.entity_id, o.status,
		o.shipping_description, sm.shipping_method

	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		left join
			Landing.map.sales_shipping_method_aud sm on o.shipping_description = sm.shipping_method_code
	order by sm.shipping_method, o.shipping_description, o.entity_id

	-- telesales_admin_username_bk
	select o.entity_id, o.status,
		o.telesales_admin_username

	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	where o.telesales_admin_username is not null
	order by o.telesales_admin_username
			

	-- reminder_type_name_bk - reminder_period_bk // reminder_date / reminder_sent
	select o.entity_id, o.reminder_type, o.reminder_period, o.reminder_date, o.reminder_sent
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			
	-- reorder_f, reorder_date // reorder_profile_id, automatic_reorder
	select o.entity_id, o.customer_id, rp.customer_id, rp.startdate, rp.enddate, rp.next_order_date,
		case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then 'Y' else 'N' end reorder_f, 
		case when (rp.enddate > rp.startdate and rp.enddate > getutcdate()) then rp.next_order_date else null end reorder_date, o.reorder_profile_id, rp.reorder_quote_id, o.automatic_reorder
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		left join
			Landing.mag.po_reorder_profile_aud rp on o.reorder_profile_id = rp.id	
	order by o.entity_id

select top 1000 *
from Landing.mag.po_reorder_profile_aud

	-- 

---------------------------------------------------------------------------------------------------------

	-- ORDERS WITH MANY PAYMENTS
	select *
	from
		(select count(*) over (partition by parent_id) num_rep_paym, *
		from Landing.mag.sales_flat_order_payment_aud) t
	where num_rep_paym > 1
	order by num_rep_paym desc, parent_id, entity_id
