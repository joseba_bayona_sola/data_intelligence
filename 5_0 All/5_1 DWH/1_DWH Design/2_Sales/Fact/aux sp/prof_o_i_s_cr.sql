
select order_id_bk
from Landing.aux.sales_order_header_o oh

select *
from Landing.aux.sales_order_header_o_i_s_cr oh

select oh.order_id_bk, count(*) over (partition by oh.order_id_bk) num_rep,
	i.entity_id invoice_id, s.entity_id shipment_id, cr.entity_id creditmemo_id, 
	o.increment_id order_no, i.increment_id invoice_no, s.increment_id shipment_no, cr.increment_id creditmemo_no, 
	o.created_at order_date, 
	i.created_at invoice_date, 
	s.created_at shipment_date, 
	cr.created_at refund_date
from 
		Landing.aux.sales_order_header_o oh
	inner join
		Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	left join
		Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id
	left join
		Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id
	left join
		Landing.mag.sales_flat_creditmemo_aud cr on oh.order_id_bk = cr.order_id
order by oh.order_id_bk

select oh.order_id_bk, 
	i.entity_id invoice_id, s.entity_id shipment_id, cr.entity_id creditmemo_id, 
	o.increment_id order_no, i.increment_id invoice_no, s.increment_id shipment_no, cr.increment_id creditmemo_no, 
	CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarOrderDate, 
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeOrderDate, 
	o.created_at order_date, 
	CONVERT(INT, (CONVERT(VARCHAR(8), i.created_at, 112))) idCalendarInvoiceDate, i.created_at invoice_date, 
	CONVERT(INT, (CONVERT(VARCHAR(8), s.created_at, 112))) idCalendarShipmentDate, 
	RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, s.created_at)), 2) + ':' + case when (DATEPART(MINUTE, s.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
	s.created_at shipment_date, 
	CONVERT(INT, (CONVERT(VARCHAR(8), cr.created_at, 112))) idCalendarRefundDate, cr.created_at refund_date
from 
		Landing.aux.sales_order_header_o oh
	inner join
		Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
	left join
		(select order_id, entity_id, increment_id, created_at
		from
			(select i.order_id, i.entity_id, i.increment_id, i.created_at, 
				rank() over (partition by i.order_id order by i.entity_id) rank_inv
			from 
					Landing.aux.sales_order_header_o oh
				inner join
					Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id) t
		where rank_inv = 1) i on oh.order_id_bk = i.order_id
	left join
		(select order_id, entity_id, increment_id, created_at
		from
			(select s.order_id, s.entity_id, s.increment_id, s.created_at, 
				rank() over (partition by s.order_id order by s.entity_id) rank_ship
			from 
					Landing.aux.sales_order_header_o oh
				inner join
					Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id) t
		where rank_ship = 1) s on oh.order_id_bk = s.order_id
	left join
		(select order_id, entity_id, increment_id, created_at
		from
			(select cr.order_id, cr.entity_id, cr.increment_id, cr.created_at, 
				rank() over (partition by cr.order_id order by cr.entity_id) rank_cr
			from 
					Landing.aux.sales_order_header_o oh
				inner join
					Landing.mag.sales_flat_creditmemo_aud cr on oh.order_id_bk = cr.order_id) t
		where rank_cr = 1) cr on oh.order_id_bk = cr.order_id
order by oh.order_id_bk

			
-------------------------------------------------
	-- SPECIAL CASES -- 
	
	-- ORDERS WITH MANY INVOICES
	select *
	from
		(select count(*) over (partition by order_id) num_rep_inv, *
		from Landing.mag.sales_flat_invoice_aud) t
	where num_rep_inv > 1
	order by num_rep_inv desc, order_id, entity_id

	-- ORDERS WITH MANY CREDITMEMOS
	select count(*) over (partition by order_id), *
	from
		(select count(*) over (partition by order_id) num_rep_cr, *
		from Landing.mag.sales_flat_creditmemo_aud) t
	where num_rep_cr > 1
		and base_subtotal = 0
	order by num_rep_cr desc, order_id, entity_id
