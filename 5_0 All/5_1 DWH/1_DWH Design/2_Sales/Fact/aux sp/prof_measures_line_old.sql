
select *
from Landing.aux.sales_order_line_o_i_s_cr

select *
from Landing.mag.sales_flat_order
--where base_customer_balance_amount <> 0
order by entity_id desc

select *
from Landing.mag.sales_flat_order_item
where order_id = 5301632
order by order_id desc

select o.entity_id, oi.item_id, o.increment_id, 
	o.total_qty_ordered, oi.qty_ordered, 
	oi.base_price local_price_unit, 
	o.base_subtotal, oi.base_row_total, 
	o.base_shipping_amount, 
	o.base_discount_amount, oi.base_discount_amount, oi.discount_percent, 
	o.base_customer_balance_amount, 
	o.base_grand_total, 
	oi.qty_refunded, o.base_subtotal_refunded, oi.base_amount_refunded, o.base_shipping_refunded, o.base_discount_refunded, o.base_customer_balance_refunded, 
	o.base_total_refunded, cr.base_grand_total,

	o.base_total_offline_refunded, o.base_total_online_refunded, o.bs_customer_bal_total_refunded,
	cr.base_adjustment, 
	o.base_to_global_rate, o.order_currency_code 
from 
		Landing.mag.sales_flat_order o
	inner join	
		Landing.mag.sales_flat_order_item oi on o.entity_id = oi.order_id
	left join
		(select cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
			cr.base_subtotal, cri.qty, cri.base_row_total,
			cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_adjustment, cr.base_grand_total
		from 
				Landing.mag.sales_flat_creditmemo_aud cr
			left join
				Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) cr 
					on (o.entity_id = cr.order_id and oi.item_id = cr.order_item_id) OR (o.entity_id = cr.order_id and cr.order_item_id is null)

--where o.base_discount_amount <> 0
where (o.base_customer_balance_refunded is not null or o.base_customer_balance_refunded <> 0) or (o.base_total_refunded is not null or o.base_total_refunded <> 0) 
--where o.base_customer_balance_amount <> 0
order by o.entity_id desc, oi.item_id


select cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
	cr.base_subtotal, cri.qty, cri.base_row_total,
	cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_adjustment, cr.base_grand_total
from 
		Landing.mag.sales_flat_creditmemo_aud cr
	left join
		Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
where cri.parent_id is not null
	and cr.base_adjustment <> 0
order by cr.order_id desc, cri.order_item_id


select cr.entity_id, cr.increment_id, cr.order_id, cri.order_item_id,
	cr.base_subtotal, cri.qty, cri.base_row_total,
	cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_adjustment, cr.base_grand_total
from 
		Landing.mag.sales_flat_creditmemo_aud cr
	left join
		Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
where cri.parent_id is null
order by cr.order_id desc, cri.order_item_id




select oi.item_id, oi.order_id, 
	oi.qty_ordered qty_unit, oi.qty_refunded qty_unit_refunded, null qty_pack, null qty_time, null weight,
	oi.base_price local_price_unit, null local_price_pack, null local_price_pack_discount, 
	oi.base_price * oi.qty_ordered local_subtotal, oi.base_row_total local_subtotal_2, 
	o.base_shipping_amount local_shipping, oi.base_discount_amount local_discount, 
	o.base_customer_balance_amount local_store_credit_used, o.bs_customer_bal_total_refunded local_store_credit_given, 
	null adjustment_pos, null adjustment_neg, 
	null local_total_inc_vat, null local_total_exc_vat, null local_total_vat, null local_total_prof_fee,
	oi.base_amount_refunded local_refund,
	null local_product_cost, null local_shipping_cost, null local_total_cost, null local_margin, 
	o.base_to_global_rate local_to_global_rate, o.order_currency_code order_currency_code, 
	oi.discount_percent discount_percent, null vat_percent, null prof_fee_percent, null vat_rate, null prof_fee_rate,
	null order_allocation_rate,
	null num_erp_allocation_lines

from 
		Landing.mag.sales_flat_order_item oi
	inner join
		Landing.mag.sales_flat_order o on oi.order_id = o.entity_id
--where base_discount_amount <> 0
where qty_refunded <> 0
