
select t.idCustomer_sk_fk, cs.idCustomerStatus_sk idCustomerStatusLifecycle_sk_fk, 
	CONVERT(INT, (CONVERT(VARCHAR(8), t.activity_date, 112))) idCalendarStatusUpdateDate_sk_fk
from
		(select idCustomer_sk_fk, 
			case 
				when (activity_type_name = 'REGISTER') then 'RNB'
				when (activity_type_name = 'REACTIVATE') then 'REACTIVATED'
				else activity_type_name
			end activity_type_name, activity_date
		from
			(select idCustomer_sk_fk, activity_type_name, activity_date, 
				count(*) over (partition by idCustomer_sk_fk) num_rep_activities,
				dense_rank() over (partition by idCustomer_sk_fk order by activity_date, activity_type_name) ord_activities
			from 
				(select idCustomer_sk_fk, activity_type_name, activity_date
				from Warehouse.act.fact_activity_other_full_v
				where activity_type_name in ('REGISTER', 'LAPSED', 'REACTIVATE')
				union
				select idCustomer_sk_fk, customer_status_name activity_type_name, activity_date
				from Warehouse.act.fact_customer_signature_aux_orders) t -- REACTIVATE IN BOTH??
				) t
		where num_rep_activities = ord_activities) t
	inner join
		Warehouse.gen.dim_customer_status cs on t.activity_type_name = cs.customer_status_name_bk
order by t.idCustomer_sk_fk

			select top 1000 *
			from Warehouse.act.fact_customer_signature_aux_orders

			select top 1000 *
			from Warehouse.act.fact_activity_sales_full_v

			select *
			from Warehouse.act.dim_activity_type_v
			order by activity_group_name, activity_type_name

			-- LAPSED - NEW - REACTIVATE - REGISTER

			select *
			from Warehouse.gen.dim_customer_status

			-- LAPSED - NEW - REACTIVATED - REGULAR - RNB



