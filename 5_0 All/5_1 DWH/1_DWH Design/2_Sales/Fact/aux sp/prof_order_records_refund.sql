
-- Orders with SC Used minus --> Add value to Grand Total
select top 1000 count(*) over() num_tot,
	entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, 
	base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
from Landing.mag.sales_flat_order_aud
where base_customer_balance_amount < 0


select top 1000 count(*) over() num_tot,
	entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, 
	base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
from Landing.mag.sales_flat_order_aud
where 
	(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
	(base_total_refunded is not null and base_total_refunded <> 0)


	

-- base_total_refunded = base_total_offline_refunded + base_total_online_refunded
select top 1000 count(*) over() num_tot,
	entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, 
	base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, 
	abs(base_total_refunded - isnull(base_total_offline_refunded, 0) - isnull(base_total_online_refunded, 0)) diff
from Landing.mag.sales_flat_order_aud
where 
	(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
	(base_total_refunded is not null and base_total_refunded <> 0)
order by abs(base_total_refunded - isnull(base_total_offline_refunded, 0) - isnull(base_total_online_refunded, 0)) desc

-- Orders where Store Credit was not used -- 75996
	-- bs_customer_bal_total_refunded
select count(*) over() num_tot, 
	entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, 
	base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, 
	abs(base_total_offline_refunded - isnull(bs_customer_bal_total_refunded, 0)) diff 
from Landing.mag.sales_flat_order_aud
where 
	((base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or (base_total_refunded is not null and base_total_refunded <> 0))
	and (base_customer_balance_refunded is null or base_customer_balance_refunded = 0)

	and ((base_total_offline_refunded is not null and base_total_offline_refunded <> 0) and bs_customer_bal_total_refunded is null) -- 7k where is null - Really refunded Online?
	-- and ((base_total_offline_refunded is not null and base_total_offline_refunded <> 0) and bs_customer_bal_total_refunded is not null) -- 38k where is null - 200 k not match

	-- and ((base_total_online_refunded is not null and base_total_online_refunded <> 0) and bs_customer_bal_total_refunded is null) -- 21k -- 150k with offline value too
	-- and ((base_total_online_refunded is not null and base_total_online_refunded <> 0) and bs_customer_bal_total_refunded is not null) -- 10k 
order by diff desc, entity_id

-- Orders where Store Credit was used -- 5111
select count(*) over() num_tot,
	entity_id, increment_id, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_grand_total, 
	base_customer_balance_refunded, base_total_refunded, 
	base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, 
	abs(ISNULL(bs_customer_bal_total_refunded, 0) - base_customer_balance_refunded - isnull(base_total_refunded, 0)) diff 
from Landing.mag.sales_flat_order_aud
where 
	((base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or (base_total_refunded is not null and base_total_refunded <> 0))
	and (base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0)

	and base_total_refunded = 0  and bs_customer_bal_total_refunded is not null -- 3.6k (1.4 k NULL)
	--and base_total_refunded <> 0 and bs_customer_bal_total_refunded is not null  -- 1.4k (400 NULL)

	-- and base_customer_balance_refunded <> base_customer_balance_amount -- 237
order by diff desc, entity_id