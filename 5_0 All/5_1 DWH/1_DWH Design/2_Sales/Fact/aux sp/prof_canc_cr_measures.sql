
------------------------------------ CANCELLATIONS ---------------------------------------------

----------------------------------- VALUES IN ORDER TABLES -----------------------------------

select count(*) over () num_tot,
	entity_id, increment_id, store_id, customer_id, created_at, 
	status, 
	base_subtotal, base_grand_total,
	abs(base_grand_total - base_total_canceled) diff,
	base_total_canceled
from Landing.mag.sales_flat_order_aud
--where (base_total_canceled is not null and base_total_canceled <> 0) 
where status = 'canceled' and (base_total_canceled is null or base_total_canceled = 0) 
order by created_at
-- order by diff desc, created_at

------------------------------------ CREDITMEMOS ---------------------------------------------

----------------------------------- VALUES IN ORDER TABLES -----------------------------------

select top 1000 count(*) over () num_tot,
	entity_id, increment_id, store_id, customer_id, created_at, 
	status, 
	base_subtotal, base_grand_total,
	abs(base_grand_total - base_total_refunded) diff,
	base_customer_balance_amount, base_customer_balance_refunded,
	base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded 
from Landing.mag.sales_flat_order_aud
where status = 'closed' 
	and (base_customer_balance_refunded is null or base_customer_balance_refunded = 0) 
	and (base_total_refunded is null or base_total_refunded = 0) 
order by created_at

select count(*) over () num_tot,
	entity_id, increment_id, store_id, customer_id, created_at, 
	status, 
	base_subtotal, base_grand_total,
	abs(base_grand_total - base_total_refunded) diff,
	base_customer_balance_amount, base_customer_balance_refunded,
	base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded,

	abs(base_total_refunded - (isnull(base_total_offline_refunded, 0) + isnull(base_total_online_refunded, 0))) diff_2,
	abs(isnull(bs_customer_bal_total_refunded, 0) - (isnull(base_customer_balance_amount, 0) + isnull(base_total_offline_refunded, 0)))  diff_3
from Landing.mag.sales_flat_order_aud
where 
	(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
	(base_total_refunded is not null and base_total_refunded <> 0)
order by diff desc, created_at
-- order by diff_2 desc, created_at
-- order by diff_3 desc, created_at

----------------------------------- VALUES IN CREDITMEMO TABLES -----------------------------------


select entity_id, increment_id, order_id, created_at, 
 	base_subtotal, 
	base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_adjustment, 
	-- base_adjustment_positive, base_adjustment_negative, 	
	base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0) + isnull(base_adjustment, 0) base_grand_total_calc,
	base_grand_total,
	abs(base_grand_total - (base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0) + isnull(base_adjustment, 0))) diff
from Landing.mag.sales_flat_creditmemo_aud
order by diff desc, order_id

select entity_id, parent_id, qty, base_row_total
from Landing.mag.sales_flat_creditmemo_item_aud
where parent_id = 5211676


select entity_id, parent_id, increment_id, order_id, created_at, 
 	base_subtotal, isnull(base_row_total_refunded, 0) base_row_total_refunded, abs(base_subtotal - isnull(base_row_total_refunded, 0)) diff, 
	base_shipping_amount, base_discount_amount, base_customer_balance_amount, base_adjustment, 
	-- base_adjustment_positive, base_adjustment_negative, 	
	base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0) + isnull(base_adjustment, 0) base_grand_total_calc,
	base_grand_total,
	abs(base_grand_total - (base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0) + isnull(base_adjustment, 0))) diff_2 
from 
		Landing.mag.sales_flat_creditmemo_aud cr
	left join
		(select parent_id, sum(base_row_total) base_row_total_refunded
		from Landing.mag.sales_flat_creditmemo_item_aud
		group by parent_id) cri on cr.entity_id = cri.parent_id
order by diff desc, diff_2 desc, order_id 

select order_id, 
	sum(base_subtotal) base_subtotal, sum(base_grand_total) base_grand_total, sum(isnull(base_customer_balance_amount, 0)) base_customer_balance_amount
from Landing.mag.sales_flat_creditmemo_aud cr
group by order_id


select count(*) over () num_tot,
	o.entity_id, o.increment_id, o.store_id, o.customer_id, o.created_at, 
	o.status,
	-- o.base_subtotal, cr.base_subtotal,
	o.base_customer_balance_refunded, cr.base_customer_balance_amount, abs(isnull(o.base_customer_balance_refunded, 0) - cr.base_customer_balance_amount) diff_1,
	o.base_total_refunded, cr.base_grand_total, abs(o.base_total_refunded - cr.base_grand_total) diff_2
from	
		(select order_id, 
			sum(base_subtotal) base_subtotal, sum(base_grand_total) base_grand_total, sum(isnull(base_customer_balance_amount, 0)) base_customer_balance_amount
		from Landing.mag.sales_flat_creditmemo_aud
		group by order_id) cr	
	inner join
		Landing.mag.sales_flat_order_aud o on cr.order_id = o.entity_id
order by entity_id

select count(*) over () num_tot, cr.order_id,
	o.entity_id, o.increment_id, o.store_id, o.customer_id, o.created_at, 
	o.status,
	-- o.base_subtotal, cr.base_subtotal,
	o.base_customer_balance_refunded, cr.base_customer_balance_amount, abs(isnull(o.base_customer_balance_refunded, 0) - cr.base_customer_balance_amount) diff_1,
	o.base_total_refunded, cr.base_grand_total, abs(o.base_total_refunded - cr.base_grand_total) diff_2
from	
		(select order_id, 
			sum(base_subtotal) base_subtotal, sum(base_grand_total) base_grand_total, sum(isnull(base_customer_balance_amount, 0)) base_customer_balance_amount
		from Landing.mag.sales_flat_creditmemo_aud
		group by order_id) cr	
	full join
		(select * 
		from Landing.mag.sales_flat_order_aud 
		where 
			(base_customer_balance_refunded is not null and base_customer_balance_refunded <> 0) or 
			(base_total_refunded is not null and base_total_refunded <> 0)) o on cr.order_id = o.entity_id
where cr.order_id is null or o.entity_id is null
order by entity_id



