	create table #oh_prescription_method(
		order_id_bk						bigint NOT NULL, 
		prescription_method_name_bk		varchar(50))

	insert into #oh_prescription_method (order_id_bk, prescription_method_name_bk)

		select order_id_bk, pm.prescription_method prescription_method_name_bk
		from
				(select oh.order_id_bk, 
					case
						when ((o.presc_verification_method = 'call' or o.presc_verification_method is null) and cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification <> 'Yes') then 'Call'
						when (o.presc_verification_method = 'upload' and cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification <> 'Yes') then 'Upload/Fax/Scan'
						when (cds.value = 1 and oi.is_lens = 1 and o.postoptics_auto_verification = 'Yes') then 'Automatic'
						else 'Not Required'
					end prescription_method_name_bk
				from 
						Landing.aux.sales_order_header_o_i_s_cr oh
					inner join
						Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					inner join
						Landing.aux.mag_core_config_data_store cds on o.store_id = cds.store_id and cds.path = 'prescription/settings/enable'
					inner join
						(select order_id, max(is_lens) is_lens
						from Landing.mag.sales_flat_order_item_aud
						group by order_id) oi on oh.order_id_bk = oi.order_id) t
			inner join
				Landing.map.sales_prescription_method pm on t.prescription_method_name_bk = pm.prescription_method


		select prescription_method_name_bk, count(*)
		from #oh_prescription_method
		group by prescription_method_name_bk
		order by prescription_method_name_bk

		select oh.order_id_bk, o.store_id, 
		 	cds.value conf, o.presc_verification_method, o.postoptics_auto_verification, 
			oi.is_lens
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			inner join
				Landing.aux.mag_core_config_data_store cds on o.store_id = cds.store_id and cds.path = 'prescription/settings/enable'
			inner join
				(select order_id, max(is_lens) is_lens
				from Landing.mag.sales_flat_order_item_aud
				group by order_id) oi on oh.order_id_bk = oi.order_id

		where cds.value = 1 and o.presc_verification_method = 'upload' and o.postoptics_auto_verification = 'No' and oi.is_lens = 0


		select cds.value conf, o.presc_verification_method, o.postoptics_auto_verification, oi.is_lens, count(*)
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			inner join
				Landing.aux.mag_core_config_data_store cds on o.store_id = cds.store_id and cds.path = 'prescription/settings/enable'
			inner join
				(select order_id, max(is_lens) is_lens
				from Landing.mag.sales_flat_order_item_aud
				group by order_id) oi on oh.order_id_bk = oi.order_id

		group by cds.value, o.presc_verification_method, o.postoptics_auto_verification, oi.is_lens
		order by cds.value, o.presc_verification_method, o.postoptics_auto_verification, oi.is_lens


		select *
		from Landing.mag.sales_flat_order_item_aud
		where order_id = 5523946

		select is_lens, count(*)
		from Landing.mag.sales_flat_order_item_aud
		group by is_lens
		order by is_lens
