
select item_id, order_id, rate, 
	dense_rank() over (order by order_id) num_order,
	count(*) over (partition by order_id) num_lines,
	sum(rate) over (partition by order_id) sum_lines
from openquery(MAGENTO, 'select * from dw_flat.order_lines_allocation_rate order by order_id desc limit 10000')
order by sum_lines desc


	SELECT ol.item_id, ov.{$entity_id},
		CASE 
			WHEN max_row_total = 0 AND min_row_total = 0 AND ROUND(local_subtotal_inc_vat,2) = 0 THEN ol.{$this->itemQtyCol[$item_entity]}/ ov.total_qty
            WHEN ol.sku = 'ADJUSTMENT' THEN 1
            ELSE ol.base_row_total / ov.local_subtotal_inc_vat
		END AS rate
