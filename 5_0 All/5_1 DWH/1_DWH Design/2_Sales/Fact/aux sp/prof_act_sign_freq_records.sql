
select order_id_bk, idCustomer_sk_fk,
	idCalendarActivityDate_sk_fk, activity_date, 
	lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
	num_order, 
	customer_order_seq_no
from Warehouse.act.fact_customer_signature_aux_orders
order by num_tot_orders desc, idCustomer_sk_fk, num_order

select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
	idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
	datediff(day, activity_date, next_activity_date) diff_days,
	num_order
from
	(select order_id_bk, idCustomer_sk_fk,
		idCalendarActivityDate_sk_fk, activity_date, 
		lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
		num_tot_orders, num_order, 
		customer_order_seq_no
	from Warehouse.act.fact_customer_signature_aux_orders) t
order by num_tot_orders desc, idCustomer_sk_fk, num_order

select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
	idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
	diff_days, num_order 
from
	(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
		idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
		datediff(day, activity_date, next_activity_date) diff_days,
		num_order
	from
		(select order_id_bk, idCustomer_sk_fk,
			idCalendarActivityDate_sk_fk, activity_date, 
			lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
			num_tot_orders, num_order, 
			customer_order_seq_no
		from Warehouse.act.fact_customer_signature_aux_orders) t) t
where next_activity_date is not null
order by num_tot_orders desc, idCustomer_sk_fk, num_order

select idCustomer_sk_fk, num_tot_orders, 
	min(diff_days) min_order_freq_time, avg(diff_days) avg_order_freq_time, max(diff_days) max_order_freq_time
from 
	(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
		idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
		diff_days, num_order 
	from
		(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
			idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
			datediff(day, activity_date, next_activity_date) diff_days,
			num_order
		from
			(select order_id_bk, idCustomer_sk_fk,
				idCalendarActivityDate_sk_fk, activity_date, 
				lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
				num_tot_orders, num_order, 
				customer_order_seq_no
			from Warehouse.act.fact_customer_signature_aux_orders) t) t
	where next_activity_date is not null) t
group by idCustomer_sk_fk, num_tot_orders
order by num_tot_orders desc, idCustomer_sk_fk



	create table #signature_sales_main_freq(
		idCustomer_sk_fk					int NOT NULL, 
		main_order_freq_time				int, 
		min_order_freq_time					int, 
		avg_order_freq_time					decimal(12, 4),
		max_order_freq_time					int)

	-- INSERT STATEMENT
	insert into #signature_sales_main_freq(idCustomer_sk_fk, main_order_freq_time, 
		min_order_freq_time, avg_order_freq_time, max_order_freq_time)

		select idCustomer_sk_fk, null main_order_freq_time, 
			min(diff_days) min_order_freq_time, avg(diff_days) avg_order_freq_time, max(diff_days) max_order_freq_time
		from 
			(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
				idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
				diff_days, num_order 
			from
				(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
					idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
					datediff(day, activity_date, next_activity_date) diff_days,
					num_order
				from
					(select order_id_bk, idCustomer_sk_fk,
						idCalendarActivityDate_sk_fk, activity_date, 
						lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
						num_tot_orders, num_order, 
						customer_order_seq_no
					from Warehouse.act.fact_customer_signature_aux_orders) t) t
			where next_activity_date is not null) t
		group by idCustomer_sk_fk, num_tot_orders

