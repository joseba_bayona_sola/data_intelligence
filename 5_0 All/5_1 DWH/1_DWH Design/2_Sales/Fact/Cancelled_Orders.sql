
select top 1000 order_id, order_no, order_date, 
	document_type, document_id, document_date, document_updated_at, 
	status, order_type,
	store_name, customer_id, customer_email, order_lifecycle, customer_order_seq_no, 
	business_channel, coupon_code, 
	payment_method, cc_type, shipping_carrier, shipping_method, 
	reminder_type, reminder_date, reminder_period, 
	automatic_reorder, reorder_on_flag, reorder_interval,
	presc_verification_method,
	total_qty, local_prof_fee, prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_subtotal_inc_vat, local_shipping_inc_vat, local_discount_inc_vat, local_store_credit_inc_vat, local_adjustment_inc_vat, local_total_inc_vat
from DW_GetLenses.dbo.order_headers
where document_type = 'CANCEL'
	and store_name = 'visiondirect.co.uk'
order by document_date desc

select store_name, order_type, count(*)
from 
	(select top 1000 *
	from DW_GetLenses.dbo.order_headers
	where document_type = 'CANCEL'
	order by document_date desc) t
group by store_name, order_type
order by store_name, order_type

select store_name, payment_method, count(*)
from 
	(select top 1000 *
	from DW_GetLenses.dbo.order_headers
	where document_type = 'CANCEL'
	order by document_date desc) t
group by store_name, payment_method
order by store_name, payment_method

----

select store_name, count(*)
from 
	(select top 1000 *
	from DW_GetLenses.dbo.order_headers
	where document_type = 'CREDITMEMO'
	order by document_date desc) t
group by store_name
order by store_name