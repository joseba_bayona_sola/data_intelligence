
-- param_bc, param_di, param_po, param_cy, param_ax, param_ad, param_do
	-- Values populated from MAP table: Landing.map.prod_param_xxxx

-- param_col
	-- Values populated from edi_stock_item

select top 1000 order_line_id_bk, order_id_bk, product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk
from Landing.aux.sales_order_line_product

select count(*)
from Landing.aux.mag_edi_stock_item

select top 1000 *
from Landing.aux.mag_edi_stock_item

	-- BASE CURVE: OK (1 record in 9.8)
	select esi.bc, p.base_curve_bk, p.idBC_sk, esi.num_rows
	from
			(select top 1000 bc, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by bc) esi
		left join
			Warehouse.prod.dim_param_BC p on esi.bc = p.base_curve_bk
	order by esi.bc

	-- DIAMETER: OK
	select esi.di, p.diameter_bk, p.idDI_sk, esi.num_rows
	from
			(select top 1000 di, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by di) esi
		left join
			Warehouse.prod.dim_param_DI p on esi.di = p.diameter_bk
	order by esi.di

	-- POWER: OK (3 records in -00.00)
	select esi.po, p.power_bk, p.idPO_sk, esi.num_rows
	from
			(select top 1000 po, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by po) esi
		left join
			Warehouse.prod.dim_param_PO p on esi.po = p.power_bk
	order by esi.po

	-- CYLINDER: OK (1 record in 0.00)
	select esi.cy, p.cylinder_bk, p.idCY_sk, esi.num_rows
	from
			(select top 1000 cy, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by cy) esi
		left join
			Warehouse.prod.dim_param_CY p on esi.cy = p.cylinder_bk
	order by esi.cy

	-- AXIS: OK (1 record in 000)
	select esi.ax, p.axis_bk, p.idAX_sk, esi.num_rows
	from
			(select top 1000 ax, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by ax) esi
		left join
			Warehouse.prod.dim_param_AX p on esi.ax = p.axis_bk
	order by esi.ax

	-- ADDITION: WRONG (High, Low not match if CASE-SENSITIVE)
	select esi.ad, p.addition_bk, p.idAD_sk, esi.num_rows
	from
			(select top 1000 ad, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			-- where product_id = 3126 and product_code = '3126-ADHIGH-BC8.5-DI14.1-PO+1.50'
			group by ad) esi
		left join
			Warehouse.prod.dim_param_AD p on esi.ad = p.addition_bk
	order by esi.ad

		select addition_bk, count(*)
		from Landing.aux.sales_fact_order_line_aud
		where addition_bk is not null
		group by addition_bk
		order by addition_bk

	-- DOMINANCE: OK
	select esi.do, p.dominance_bk, p.idDO_sk, esi.num_rows
	from
			(select top 1000 do, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by do) esi
		left join
			Warehouse.prod.dim_param_DO p on esi.do = p.dominance_bk
	order by esi.do

	-- COLOUR: OK
	select esi.co, p.colour_bk, p.idCOL_sk, esi.num_rows
	from
			(select top 1000 co, count(*) num_rows
			from Landing.aux.mag_edi_stock_item
			group by co) esi
		left join
			Warehouse.prod.dim_param_COL p on esi.co = p.colour_bk
	order by esi.co

------------------------------------------------
------------------------------------------------

-- ERP: All coming from Product (SI, WSI, WSIB - These ones used in All views)
	-- Product SP Logic: Parsing of different parameters + put as BK + join in SSIS for SK
	-- View solving SK to text

	select p.productid product_id_erp_bk, pf.product_id_bk,
		bc base_curve_bk, 
		case 
			when (len(di) = 2) then di + '.0'
			else di 
		end diameter_bk, 
		case 
			when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
			when (len(po) = 3) then po + '.00'
			when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
			when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
			when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
			when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
			else po 
		end power_bk, 
		cy cylinder_bk, 
		case 
			when (len(ax) = 1) then '00' + ax
			when (len(ax) = 2) then '0' + ax
			else ax 
		end axis_bk, 
		case 
			when ad in ('Medium', 'MF', 'MID') then 'MED'
			else ad 
		end addition_bk, 
		_do dominance_bk, co colour_bk	
	into #dim_product
	from 
			Landing.mend.gen_prod_product_v p
		inner join
			Landing.aux.mag_prod_product_family_v pf on p.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.prod_product p2 on p.productid = p2.id
	where p.productid is not null


select p1.product_id_erp_bk, p1.product_id_bk, 
	p1.base_curve_bk, p2.base_curve, p1.diameter_bk, p2.diameter, p1.power_bk, p2.power, 
	p1.cylinder_bk, p2.cylinder, p1.axis_bk, p2.axis, 
	p1.addition_bk, p2.addition, p1.dominance_bk, p2.dominance, 
	p1.colour_bk, p2.colour
from 
		-- #dim_product p1
		Landing.aux.prod_dim_product_v p1
	inner join
		Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk

	-- BASE CURVE: OK 
	select p1.base_curve_bk, p2.base_curve, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.base_curve_bk, p2.base_curve
	order by p1.base_curve_bk, p2.base_curve

	-- DIAMETER: OK
	select p1.diameter_bk, p2.diameter, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.diameter_bk, p2.diameter
	order by p1.diameter_bk, p2.diameter

	-- POWER: OK
	select p1.power_bk, p2.power, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.power_bk, p2.power
	order by p1.power_bk, p2.power

	-- CYLINDER: OK
	select p1.cylinder_bk, p2.cylinder, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.cylinder_bk, p2.cylinder
	order by p1.cylinder_bk, p2.cylinder

	-- AXIS: OK
	select p1.axis_bk, p2.axis, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.axis_bk, p2.axis 
	order by p1.axis_bk, p2.axis

	-- ADDITION: WRONG (High, Low not match if CASE-SENSITIVE)
	select p1.addition_bk, p2.addition, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	-- where p2.addition is null
	group by p1.addition_bk, p2.addition
	order by p1.addition_bk, p2.addition

	-- DOMINANCE: OK
	select p1.dominance_bk, p2.dominance, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	group by p1.dominance_bk, p2.dominance
	order by p1.dominance_bk, p2.dominance

	-- WRONG: MISSING values
	select p1.colour_bk, p2.colour, count(*)
	from 
			-- #dim_product p1
			Landing.aux.prod_dim_product_v p1
		inner join
			Warehouse.prod.dim_product_v p2 on p1.product_id_erp_bk = p2.productid_erp_bk
	-- where p2.colour is null
	group by p1.colour_bk, p2.colour
	order by p1.colour_bk, p2.colour


------------------------------------------------
------------------------------------------------

	-- Development Steps: 

	-- Addition: 
		-- SP: We convert to UPPER case: aux.mag_edi_stock_item + Landing.mend.gen_prod_product_v

		-- Reload: We need to update fact_order_line: Script that updates directly // Product will be updated automatically in ETL

	-- Colour: 
		-- SP: We add to lnd_stg_get_prod_param_col SP the colours from Landing.mend.gen_prod_product_v // All values in UPPER
		-- SP: In aux.mag_edi_stock_item we convert to UPPER

		-- Reload: Product will be updated automatically in ETL

select order_line_id_bk, order_id_bk, order_no, addition_bk, colour_bk
from Landing.aux.sales_fact_order_line

	select addition_bk, count(*)
	from Landing.aux.sales_fact_order_line
	group by addition_bk
	order by addition_bk

	select colour_bk, count(*)
	from Landing.aux.sales_fact_order_line
	group by colour_bk
	order by colour_bk

--- 

select order_line_id_bk, order_id_bk, order_no, addition_bk, colour_bk
from Staging.sales.fact_order_line

	select addition_bk, count(*)
	from Staging.sales.fact_order_line
	group by addition_bk
	order by addition_bk

	select colour_bk, count(*)
	from Staging.sales.fact_order_line
	group by colour_bk
	order by colour_bk

--- 

select order_line_id_bk, order_no, idAD_sk_fk, idCOL_sk_fk
from Warehouse.sales.fact_order_line_wrk

	select idAD_sk_fk, count(*)
	from Warehouse.sales.fact_order_line_wrk
	group by idAD_sk_fk
	order by idAD_sk_fk

	select idCOL_sk_fk, count(*)
	from Warehouse.sales.fact_order_line_wrk
	group by idCOL_sk_fk
	order by idCOL_sk_fk

-------------------------------------------------------

select count(*) over (partition by olp.order_line_id_bk) num_rep,
	olp.order_line_id_bk, olp.order_id_bk, olp.product_id_bk, p.productid_erp_bk,
	olp.base_curve_bk, olp.diameter_bk, olp.power_bk, olp.cylinder_bk, olp.axis_bk, olp.addition_bk, olp.dominance_bk, olp.colour_bk
from 
		Landing.aux.sales_order_line_product olp
	left join
		Staging.prod.dim_product p on olp.product_id_bk = p.product_id_bk and 
			isnull(olp.base_curve_bk, 0) = isnull(p.base_curve_bk, 0) and isnull(olp.diameter_bk, 0) = isnull(p.diameter_bk, 0) and isnull(olp.power_bk, 0) = isnull(p.power_bk, 0) and 
			isnull(olp.cylinder_bk, 0) = isnull(p.cylinder_bk, 0) and isnull(olp.axis_bk, 0) = isnull(p.axis_bk, 0) and 
			isnull(olp.addition_bk, 0) = isnull(p.addition_bk, 0) and isnull(olp.dominance_bk, 0) = isnull(p.dominance_bk, 0) and 
			isnull(olp.colour_bk, 0) = isnull(p.colour_bk, 0) and p.delete_f = 'N'
where p.productid_erp_bk is null
	-- and olp.colour_bk is null
order by num_rep desc, olp.colour_bk, olp.order_line_id_bk

	select olp.colour_bk, count(*)
	from
			Landing.aux.sales_order_line_product olp
		left join
			Staging.prod.dim_product p on olp.product_id_bk = p.product_id_bk and 
				isnull(olp.base_curve_bk, 0) = isnull(p.base_curve_bk, 0) and isnull(olp.diameter_bk, 0) = isnull(p.diameter_bk, 0) and isnull(olp.power_bk, 0) = isnull(p.power_bk, 0) and 
				isnull(olp.cylinder_bk, 0) = isnull(p.cylinder_bk, 0) and isnull(olp.axis_bk, 0) = isnull(p.axis_bk, 0) and 
				isnull(olp.addition_bk, 0) = isnull(p.addition_bk, 0) and isnull(olp.dominance_bk, 0) = isnull(p.dominance_bk, 0) and 
				isnull(olp.colour_bk, 0) = isnull(p.colour_bk, 0) and p.delete_f = 'N'
	where p.productid_erp_bk is null
	group by olp.colour_bk
	order by olp.colour_bk


----------------------------------------

select order_line_id_bk, order_id, order_no, 
	product_id_bk, base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, 
	sku_magento, sku_erp, productid_erp_bk
from Staging.sales.fact_order_line
where productid_erp_bk is not null
order by product_id_bk, base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk
