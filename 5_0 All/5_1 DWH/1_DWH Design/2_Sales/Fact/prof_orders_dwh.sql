
select top 1000 document_id, document_type, 
	order_id, order_no, order_date, document_date,
	customer_id, customer_email, 
	total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat
from DW_GetLenses.dbo.order_headers
-- where document_type = 'CANCEL'
-- where local_store_credit_inc_vat < 0 -- and document_type <> 'ORDER'
-- where local_store_credit_inc_vat > 0 -- and document_type <> 'CREDITMEMO'
where local_store_credit_inc_vat < 0 and local_total_inc_vat <> 0
-- where document_type = 'CREDITMEMO' and local_store_credit_inc_vat <= 0 -- and local_total_inc_vat >= 0
order by document_date desc

select top 1000 document_id, document_type, 
	order_id, order_no, order_date, document_date,
	customer_id, customer_email, shipping_country_id,
	total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat,
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat
from DW_GetLenses.dbo.order_headers
-- where document_type = 'ORDER' and local_store_credit_inc_vat = 0
-- where document_type = 'ORDER' and local_store_credit_inc_vat < 0 and local_total_inc_vat = 0
-- where document_type = 'ORDER' and local_store_credit_inc_vat < 0 and local_total_inc_vat > 0
-- where document_type = 'ORDER' and (local_adjustment_inc_vat is not null and local_adjustment_inc_vat <> 0)
-- where document_type = 'CANCEL' and local_store_credit_inc_vat = 0
-- where document_type = 'CANCEL' and local_store_credit_inc_vat > 0
-- where document_type = 'CANCEL' and (local_adjustment_inc_vat is not null and local_adjustment_inc_vat <> 0)
-- where document_type = 'CREDITMEMO' and local_store_credit_inc_vat = 0 --> Refunded to Bank Account ALL
-- where document_type = 'CREDITMEMO' and local_store_credit_inc_vat > 0 and local_total_inc_vat = 0 --> Refunded to Store Credit ALL
where document_type = 'CREDITMEMO' and local_store_credit_inc_vat > 0 and local_total_inc_vat = 0 and (local_subtotal_inc_vat + local_shipping_inc_vat + local_discount_inc_vat + local_store_credit_inc_vat <> 0)
	--> Refunded to Store Credit ALL - Store Credit Value bigger than Total - Store Credit used before
-- where document_type = 'CREDITMEMO' and local_store_credit_inc_vat > 0 and local_total_inc_vat < 0 --> Refunded to Store Credit NOT ALL: Partial Refund - Also Bank Account
-- where document_type = 'CREDITMEMO' and (local_adjustment_inc_vat is not null and local_adjustment_inc_vat <> 0)
order by document_date desc


-------------------------------------------------------------------------------------------

select top 1000 customer_id, customer_email, count(*), 
	count(case when (document_type = 'ORDER') then 1 else NULL end) num_ok,
	count(case when (document_type = 'CANCEL') then 1 else NULL end) num_canc,
	count(case when (document_type = 'CREDITMEMO') then 1 else NULL end) num_ref
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 365
group by customer_id, customer_email
order by count(*) desc

-------------------------------------------------------------------------------------------

select top 1000 document_id, document_type, 
	order_id, order_no, order_date, document_date,
	customer_id, customer_email, 
	total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 365 and 
	customer_id in (1268097, 1529849, 1441133, 369676, 1656402)
order by customer_id, order_id, document_date 

select top 1000 document_id, document_type, 
	order_id, order_no, order_date, document_date,
	customer_id, customer_email, 
	total_qty, 
	local_prof_fee, 
	local_subtotal_inc_vat, local_subtotal_vat, local_subtotal_exc_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_exc_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_exc_vat, 
	local_store_credit_inc_vat, local_store_credit_vat, local_store_credit_exc_vat, 
	local_adjustment_inc_vat, local_adjustment_vat, local_adjustment_exc_vat, 
	local_total_inc_vat, local_total_vat, local_total_exc_vat
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 365 and 
	customer_id = 1409201
order by customer_id, order_id, document_date 

