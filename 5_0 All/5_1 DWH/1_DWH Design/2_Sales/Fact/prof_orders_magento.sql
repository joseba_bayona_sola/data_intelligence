
select top 1000 count(*) over (partition by customer_id),
	entity_id, increment_id, created_at, state, status, 
	customer_id, coupon_code, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
	base_grand_total, 
	base_subtotal + base_shipping_amount + base_discount_amount - base_customer_balance_amount base_grand_calc,
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, 
	base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, 
	base_subtotal_refunded + base_shipping_refunded + base_discount_refunded - base_customer_balance_refunded base_total_refunded_calc,
	base_total_due,
	ins_ts
from mag.sales_flat_order_aud
where customer_id in (1409201, 1422325)
-- where base_discount_amount < 0
order by customer_id, entity_id desc

select top 100 item_id, order_id, store_id, created_at, updated_at,
	product_id, sku, name, description, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	weight, row_weight, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, 
	base_discount_amount, discount_percent, 
	base_amount_refunded,
	product_type, product_options, is_virtual, is_lens, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_order_item_aud
order by order_id desc, item_id;