
select product_id, name, qty, unit_weight, line_weight, count(*)
from DW_GetLenses.dbo.order_lines
where document_date > getutcdate() -30
	and product_id in (1083, 1100)
group by product_id, name, qty, unit_weight, line_weight
order by product_id, name, qty, unit_weight, line_weight

select product_type, product_id, name, unit_weight, count(*)
from DW_GetLenses.dbo.order_lines
where document_date > getutcdate() -30
--	and product_id in (1083, 1100)
group by product_type, product_id, name, unit_weight
order by product_type, product_id, name, unit_weight

