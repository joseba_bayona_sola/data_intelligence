
SELECT CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, GETUTCDATE()), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS ColumnInLocalTime, GETUTCDATE(), 
	CONVERT(datetimeoffset, GETUTCDATE()), SYSDATETIMEOFFSET()


select top 1000 order_id_bk, order_no, order_date, 
	CONVERT(datetime, SWITCHOFFSET(CONVERT(datetimeoffset, order_date), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) 
from Warehouse.sales.dim_order_header
order by order_date
