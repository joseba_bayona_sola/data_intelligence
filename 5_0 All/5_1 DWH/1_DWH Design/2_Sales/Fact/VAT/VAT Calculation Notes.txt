VAT Notes - Andrew Whiteley

	VAT Document: 
		Written by him with information from Finance Person left + Review by Michael, Richard --> Approved

	Mendix Module: 
		VAT Calculation Mendix Module written in order to calculate VAT for wholesales
			Retail Sale: Sales made to customers on Magento website
			Wholesales: Sales made to other companies (small % - not stored in Magento - not in DWH)

			VAT Calculation is the same between Retail and Wholesale

		Module: 
			Name: VAT
			Model
			Process: GetVATRate

			Website: 
				VAT Schedule (Product Administration)
				VAT Rates (Country Management): VAT Logic Configuration
				VAT Commodity: VAT Dispense Configuration