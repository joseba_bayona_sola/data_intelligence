
use DW_GetLenses_jbs
go


---------------------- comp_es_ol -----------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.comp_es_ol

	select top 1000 count(*), count(distinct order_id_bk), sum(global_total_aft_refund_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_ol
	where order_date between '2000-01-01' and '2018-09-01' -- order_date: with time on it - Matches Tableau
 	-- where order_date_c between '2000-01-01' and '2018-09-01' -- order_date_c: no time on it - Not match Tableau - Also Includes '2018-09-01' - Should be '2018-08-31'

select ol.*
from
		(select ol1.order_line_id_bk
		from
				(select order_line_id_bk
				from DW_GetLenses_jbs.dbo.comp_es_ol
				where order_date_c between '2000-01-01' and '2018-08-31' -- '2018-09-01' - '2018-08-31' 
					) ol1
			full join
				(select order_line_id_bk
				from DW_GetLenses_jbs.dbo.comp_es_ol
				where order_date between '2000-01-01' and '2018-09-01') ol2 on ol1.order_line_id_bk = ol2.order_line_id_bk
		where ol2.order_line_id_bk is null or ol1.order_line_id_bk is null) t
	inner join
		DW_GetLenses_jbs.dbo.comp_es_ol ol on t.order_line_id_bk = ol.order_line_id_bk
order by ol.order_date


	select top 1000 count(*), count(distinct order_id_bk), sum(global_total_aft_refund_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_ol
	-- where invoice_date_c is not null
	where shipment_date_c is not null

	select top 1000 count(*), count(distinct order_id_bk), sum(global_total_aft_refund_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_ol
	-- where order_date_c between '2000-01-01' and '2018-08-31' 
	where invoice_date_c between '2000-01-01' and '2018-08-31' 
	-- where shipment_date_c between '2000-01-01' and '2018-08-31' 

		select top 1000 year(invoice_date_c),
			count(distinct order_id_bk), sum(global_total_aft_refund_exc_vat)
		from DW_GetLenses_jbs.dbo.comp_es_ol
		where invoice_date_c between '2000-01-01' and '2018-08-31' 
		group by year(invoice_date_c)
		order by year(invoice_date_c)
				
		select top 1000 year(shipment_date_c),
			count(distinct order_id_bk), sum(global_total_aft_refund_exc_vat)
		from DW_GetLenses_jbs.dbo.comp_es_ol
		where shipment_date_c between '2000-01-01' and '2018-08-31' 
		group by year(shipment_date_c)
		order by year(shipment_date_c)
				