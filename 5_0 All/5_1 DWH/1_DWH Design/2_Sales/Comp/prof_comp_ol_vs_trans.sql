
select t1.order_id_bk, t2.order_id_bk, t1.order_date,
	t1.rev1, t2.rev2, abs(t1.rev1 - t2.rev2), sum(abs(t1.rev1 - t2.rev2)) over ()
from
		(select order_id_bk, order_date, sum(global_total_aft_refund_exc_vat) rev1
		from DW_GetLenses_jbs.dbo.comp_es_ol
		group by order_id_bk, order_date) t1
	full join
		(select order_id_bk, sum(global_total_exc_vat) rev2
		from DW_GetLenses_jbs.dbo.comp_es_trans
		group by order_id_bk) t2 on t1.order_id_bk = t2.order_id_bk
-- where t1.order_id_bk is null or t2.order_id_bk is null
where t1.rev1 <> t2.rev2
	and abs(t1.rev1 - t2.rev2) > 0.1
-- order by abs(t1.rev1 - t2.rev2) desc
order by t1.order_date

select *
from DW_GetLenses_jbs.dbo.comp_es_ol
where order_id_bk = 2109476 -- 2084113 -- 7526862

select *
from DW_GetLenses_jbs.dbo.comp_es_trans
where order_id_bk = 2109476 -- 2084113 -- 7526862

------------------------------------------------------------

select t1.order_id_bk, t2.order_id_bk, t1.rev1, t2.rev2, abs(t1.rev1 - t2.rev2), sum(abs(t1.rev1 - t2.rev2)) over ()
from
		(select order_id_bk, sum(global_total_aft_refund_exc_vat) rev1
		from DW_GetLenses_jbs.dbo.comp_es_ol
		where invoice_date_c is not null
		group by order_id_bk) t1
	full join
		(select order_id_bk, sum(global_total_exc_vat) rev2
		from DW_GetLenses_jbs.dbo.comp_es_trans
		where invoice_date_c is not null
		group by order_id_bk) t2 on t1.order_id_bk = t2.order_id_bk
where t1.order_id_bk is null or t2.order_id_bk is null
-- where t1.rev1 <> t2.rev2
order by abs(t1.rev1 - t2.rev2) desc

------------------------------------------------------------

select t1.order_id_bk, t2.order_id_bk, t1.rev1, t2.rev2, abs(t1.rev1 - t2.rev2), sum(abs(t1.rev1 - t2.rev2)) over ()
from
		(select order_id_bk, sum(global_total_aft_refund_exc_vat) rev1
		from DW_GetLenses_jbs.dbo.comp_es_ol
		where shipment_date_c is not null
		group by order_id_bk) t1
	full join
		(select order_id_bk, sum(global_total_exc_vat) rev2
		from DW_GetLenses_jbs.dbo.comp_es_trans
		where shipment_date_c is not null
		group by order_id_bk) t2 on t1.order_id_bk = t2.order_id_bk
where t1.order_id_bk is null or t2.order_id_bk is null
--where t1.rev1 <> t2.rev2
order by abs(t1.rev1 - t2.rev2) desc



select *
from DW_GetLenses_jbs.dbo.comp_es_ol
where order_id_bk = 4482001

select *
from DW_GetLenses_jbs.dbo.comp_es_trans
where order_id_bk = 4482001