
select top 1000 *
from DW_GetLenses_jbs.dbo.comp_es_trans

	select top 1000 count(*), count(distinct order_id_bk), sum(global_total_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_trans

	select top 1000 count(*), count(distinct order_id_bk_c), sum(global_total_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_trans
	-- where invoice_date_c is not null
	where shipment_date_c is not null

	select top 1000 count(*), count(distinct order_id_bk_c), sum(global_total_exc_vat)
	from DW_GetLenses_jbs.dbo.comp_es_trans
	-- where order_date_c between '2000-01-01' and '2018-08-31' 
	where invoice_date_c between '2000-01-01' and '2018-08-31' 
	where shipment_date_c between '2000-01-01' and '2018-08-31' 

		select top 1000 year(invoice_date_c),
			count(distinct order_id_bk_c), sum(global_total_exc_vat)
		from DW_GetLenses_jbs.dbo.comp_es_trans 
		where invoice_date_c between '2000-01-01' and '2018-08-31' 
		group by year(invoice_date_c)
		order by year(invoice_date_c)

		select top 1000 year(shipment_date_c),
			count(distinct order_id_bk_c), sum(global_total_exc_vat)
		from DW_GetLenses_jbs.dbo.comp_es_trans 
		where invoice_date_c between '2000-01-01' and '2018-08-31' 
		group by year(shipment_date_c)
		order by year(shipment_date_c)