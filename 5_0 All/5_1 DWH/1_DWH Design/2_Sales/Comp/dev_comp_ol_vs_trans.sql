
use DW_GetLenses_jbs
go

---------------------- comp_es_ol ----------------------------------------- 5 min
	
	drop table DW_GetLenses_jbs.dbo.comp_es_ol

	select oh.idOrderHeader_sk, ol.idOrderLine_sk, 
		ol.order_line_id_bk, 
		oh.order_id_bk, oh.order_no, 
 		oh.order_date, oh.order_date_c, 
		ol.invoice_date, cal_i.calendar_date invoice_date_c, 
		ol.shipment_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) 
			else ol.shipment_date end shipment_date_r, cal_s.calendar_date shipment_date_c, 
		isnull(ol.refund_date, oh.refund_date) refund_date, cal_re.calendar_date refund_date_c, 

		oh.acquired, oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.customer_id, oh.customer_email, oh.order_status_name, 
		ol.qty_unit, ol.qty_unit_refunded,

		convert(decimal(12, 4), ol.local_subtotal * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), ol.local_shipping * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), ol.local_discount * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), ol.local_store_credit_used * ol.local_to_global_rate) global_store_credit_used, 
	
		convert(decimal(12, 4), ol.local_total_inc_vat * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), ol.local_total_exc_vat * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), ol.local_total_vat * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), ol.local_total_prof_fee * ol.local_to_global_rate) global_total_prof_fee, 

		convert(decimal(12, 4), ol.local_store_credit_given * ol.local_to_global_rate) global_store_credit_given, convert(decimal(12, 4), ol.local_bank_online_given * ol.local_to_global_rate) global_bank_online_given, 

		convert(decimal(12, 4), ol.local_total_aft_refund_inc_vat * ol.local_to_global_rate) global_total_aft_refund_inc_vat, convert(decimal(12, 4), ol.local_total_aft_refund_exc_vat * ol.local_to_global_rate) global_total_aft_refund_exc_vat, 
		convert(decimal(12, 4), ol.local_total_aft_refund_vat * ol.local_to_global_rate) global_total_aft_refund_vat, convert(decimal(12, 4), ol.local_total_aft_refund_prof_fee * ol.local_to_global_rate) global_total_aft_refund_prof_fee

	into DW_GetLenses_jbs.dbo.comp_es_ol
	from 
			Warehouse.sales.dim_order_header_v oh 
		inner join
			Warehouse.sales.fact_order_line ol on oh.order_id_bk = ol.order_id 
		left join
			Warehouse.gen.dim_calendar cal_i on ol.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_s 
				on case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.idCalendarShipmentDate_sk_fk, oh.idCalendarRefundDate_sk_fk) else ol.idCalendarShipmentDate_sk_fk end = cal_s.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_re on isnull(ol.idCalendarRefundDate_sk_fk, oh.idCalendarRefundDate_sk_fk) = cal_re.idCalendar_sk
	where oh.website = 'visiondirect.co.uk'

---------------------- comp_es_trans --------------------------------------

drop table DW_GetLenses_jbs.dbo.comp_es_trans

select *
into DW_GetLenses_jbs.dbo.comp_es_trans
from 
	(select order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
		order_id_bk, order_id_bk order_id_bk_c, order_no, 
		
		order_date, order_date_c,
		invoice_date, invoice_date_c, 
		shipment_date_r shipment_date, shipment_date_c,
		refund_date, refund_date_c,

		acquired, tld, website_group, website, store_name, 
		customer_id, customer_id customer_id_c, customer_email, 

		order_status_name, line_status_name, 

		qty_unit, qty_unit_refunded,

		global_subtotal, global_shipping, global_discount, global_store_credit_used, 
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 

		global_store_credit_given, global_bank_online_given -- Should only appear at Refund level ??
	from Warehouse.sales.fact_order_line_v
	where website = 'visiondirect.co.uk'
	union
	select order_line_id_bk, null order_line_id_bk_c, 
		order_id_bk, null order_id_bk_c, order_no, 
		
		order_date, order_date_c,
		case when (invoice_date is not null) then refund_date else null end invoice_date, case when (invoice_date is not null) then refund_date_c else null end invoice_date_c, 
		case when (shipment_date_r is not null) then refund_date else null end shipment_date, case when (shipment_date_r is not null) then refund_date_c else null end shipment_date_c,
		refund_date, refund_date_c,

		acquired, tld, website_group, website, store_name, 
		customer_id, null customer_id_c, customer_email, 

		order_status_name, line_status_name, 

		qty_unit, qty_unit_refunded, -- only one qty_unit: here qty_unit_refunded * -1 ???

		convert(decimal(12, 4), local_subtotal_m * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping_m * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), local_discount_m * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used_m * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), local_total_inc_vat_m * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat_m * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), local_total_vat_m * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee_m * ol.local_to_global_rate) global_total_prof_fee, 

		global_store_credit_given, global_bank_online_given
	from 
		(select *, 
			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund * -1 local_discount_m, 
			local_store_credit_used_refund * -1 local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m
		from
			(select *,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m
			from Warehouse.sales.fact_order_line_v
			where website = 'visiondirect.co.uk'
				and (local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0)) ol) ol) t


---------------------------------------------------------------------------------

-- Filtering Refund records
	-- local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0 ---> Calculate NUM records

	-- Solution 1: Filter records on View
	-- Solution 2: Store records on fact_order_line_refund with only PK attributes + local_store_credit_given, local_bank_online_given, qty_unit_refunded
	-- Solution 3: Store records on fact_order_line_refund - same structure as fact_order_line changing the measure values to neg

-- Preparing Refund records measures
	-- local_total_inc_vat_m: local_total_exc_vat_m - local_total_vat_m - local_total_prof_fee_m
	-- local_total_inc_vat_vf_m: local_total_exc_vat_vf_m - local_total_vat_vf_m - local_total_prof_fee_vf_m
	-- local_subtotal_m - local_shipping_m - local_discount_m - local_store_credit_used_m - local_adjustment_m

-- Preparing Refund related attributes
	-- null order_line_id_bk_c, null order_id_bk_c, null customer_id_c
	-- refund_date invoice_date, refund_date_c invoice_date_c, 
	-- case when (shipment_date_r is not null) then refund_date else null end shipment_date, case when (shipment_date_r is not null) then refund_date_c else null end shipment_date_c,

	-- qty_unit_refunded * -1 qty_unit, 
	-- convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack,

	-- local - global - local_vf

---------------------------------------------------------------------------------

-- Attributes: 

/*
	select ol.order_id_bk, ol.invoice_id, ol.shipment_id, ol.creditmemo_id,
		ol.order_no,
		ol.order_date_c, ol.invoice_date_c, ol.shipment_date_c, ol.refund_date_c,
		ol.invoice_week_day order_week_day, ol.order_day_part,
		ol.rank_shipping_days, ol.shipping_days,
		-- acquired, 
		ol.tld, ol.website_group, ol.website, ol.store_name, 
		ol.website_group_create, 
		ol.customer_origin_name,
		ol.market_name,
		ol.customer_id, ol.customer_email, 
		ol.country_code_ship, ol.region_name_ship, ol.country_code_bill, 
		ol.order_stage_name, ol.order_status_name, ol.line_status_name, ol.order_status_magento_name, -- adjustment_order, 
		payment_method_name, shipping_carrier_name, -- cc_type_name, shipping_method_name, telesales_username, 
		ol.reminder_type_name, ol.reorder_f, -- reminder_period_name, 
		ol.channel_name, ol.marketing_channel_name, group_coupon_code_name, coupon_code,
		ol.proforma, -- order_source, 
		ol.customer_status_name, ol.rank_seq_no_gen rank_seq_no, ol.customer_order_seq_no_gen customer_order_seq_no, -- ol.rank_seq_no, ol.customer_order_seq_no, 
		ol.rank_seq_no_web, ol.customer_order_seq_no_web, 

		ol.manufacturer_name, 
		ol.product_type_name, ol.category_name, ol.product_family_group_name, ol.cl_type_name, ol.cl_feature_name,
		ol.product_id_magento, ol.product_family_name, 
		ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.addition, ol.dominance, ol.colour, 
		-- glass_vision_type_name, glass_package_type_name, 
		ol.sku_magento, -- sku_erp, 
		-- aura_product_f,

		ol.qty_unit, ol.qty_pack, 
		ol.rank_qty_time, ol.qty_time, 
		rft3.rank_freq_time rank_prev_order_freq_time, acs.prev_order_freq_time, rft2.rank_freq_time rank_next_order_freq_time, acs.next_order_freq_time,
		ol.price_type_name, ol.discount_f, 
		ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 

		ol.countries_registered_code, ol.product_type_vat,

		ol.local_shipping, ol.local_discount, 
		-- ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.local_total_aft_refund_inc_vat local_total_inc_vat, ol.local_total_aft_refund_exc_vat local_total_exc_vat, ol.local_total_aft_refund_vat local_total_vat, ol.local_total_aft_refund_prof_fee local_total_prof_fee, 
		ol.global_shipping, ol.global_discount, 
		-- ol.global_total_inc_vat, ol.global_total_exc_vat, ol.global_total_vat, ol.global_total_prof_fee,
		ol.global_total_aft_refund_inc_vat global_total_inc_vat, ol.global_total_aft_refund_exc_vat global_total_exc_vat, ol.global_total_aft_refund_vat global_total_vat, ol.global_total_aft_refund_prof_fee global_total_prof_fee, 
		isnull(ol.idETLBatchRun_upd, ol.idETLBatchRun_ins) idETLBatchRun
*/