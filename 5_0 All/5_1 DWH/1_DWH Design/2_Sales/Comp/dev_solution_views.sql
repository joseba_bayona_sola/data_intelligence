
use Warehouse
go

drop view sales.fact_order_line_trans_v
go 

create view sales.fact_order_line_trans_v as

select *
from 
	(select idOrderHeader_sk, idOrderLine_sk,
		order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
		order_id_bk, order_id_bk order_id_bk_c, invoice_id, shipment_id, creditmemo_id, -- creditmemo_id, creditmemo_no only for Refund ??
		order_no, invoice_no, shipment_no, creditmemo_no,
		
		order_date, order_date_c, order_week_day, order_time_name, order_day_part,
		invoice_date, invoice_date_c, invoice_week_day,
		shipment_date_r shipment_date, shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part,
		refund_date, refund_date_c, refund_week_day, -- refund_date only for refunds

		rank_shipping_days, shipping_days, -- what about shipping_days on refunds

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name,
		customer_id, customer_id customer_id_c, customer_email, customer_name,
		country_code_ship, country_name_ship, region_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
		customer_unsubscribe_name,
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, prescription_method_name,
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, group_coupon_code_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen,
		order_source, proforma,
		product_type_oh_name, order_qty_time, num_diff_product_type_oh,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name, 
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		eye,
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		countries_registered_code, product_type_vat,

		qty_unit, qty_pack, rank_qty_time, qty_time, -- what about qty_time on refunds

		price_type_name, discount_f,
		local_price_unit, local_price_pack, local_price_pack_discount, 
		global_price_unit, global_price_pack, global_price_pack_discount,

		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		0 local_store_credit_given, 0 local_bank_online_given, 

		global_subtotal, global_shipping, global_discount, global_store_credit_used, 
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		0 global_store_credit_given, 0 global_bank_online_given,  -- Should only appear at Refund level ??

		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf,

		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		num_erp_allocation_lines

	from Warehouse.sales.fact_order_line_v
	-- where website = 'visiondirect.es' -- website_group = 'VisionDirect' -- 
	union
	select idOrderHeader_sk, idOrderLine_sk,
		order_line_id_bk, null order_line_id_bk_c, 
		order_id_bk, null order_id_bk_c, invoice_id, shipment_id, creditmemo_id,
		order_no, invoice_no, shipment_no, creditmemo_no,
		
		order_date, order_date_c, order_week_day, order_time_name, order_day_part,
		case when (invoice_date is not null) then refund_date else null end invoice_date, case when (invoice_date is not null) then refund_date_c else null end invoice_date_c, 
		case when (invoice_date is not null) then refund_week_day else null end invoice_week_day,
		case when (shipment_date_r is not null) then refund_date else null end shipment_date, case when (shipment_date_r is not null) then refund_date_c else null end shipment_date_c, 
		case when (shipment_date_r is not null) then refund_week_day else null end shipment_week_day, null shipment_time_name, null shipment_day_part,
		refund_date, refund_date_c, refund_week_day,

		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name,
		customer_id, null customer_id_c, customer_email, customer_name,
		country_code_ship, country_name_ship, region_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
		customer_unsubscribe_name,
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, prescription_method_name,
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, group_coupon_code_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen,
		order_source, proforma,
		product_type_oh_name, order_qty_time, num_diff_product_type_oh,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name, 
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		eye,
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		countries_registered_code, product_type_vat,

		qty_unit_refunded * -1 qty_unit, convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack, rank_qty_time, qty_time,

		price_type_name, discount_f,
		local_price_unit, local_price_pack, local_price_pack_discount, 
		global_price_unit, global_price_pack, global_price_pack_discount,

		local_subtotal_m local_subtotal, local_shipping_m local_shipping, local_discount_m local_discount, local_store_credit_used_m local_store_credit_used, 
		local_total_inc_vat_m local_total_inc_vat, local_total_exc_vat_m local_total_exc_vat, local_total_vat_m local_total_vat, local_total_prof_fee_m local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 

		convert(decimal(12, 4), local_subtotal_m * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping_m * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), local_discount_m * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used_m * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), local_total_inc_vat_m * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat_m * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), local_total_vat_m * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee_m * ol.local_to_global_rate) global_total_prof_fee, 
		global_store_credit_given, global_bank_online_given, 

		convert(decimal(12, 4), local_total_inc_vat_vf_m) local_total_inc_vat_vf, convert(decimal(12, 4), local_total_exc_vat_vf_m) local_total_exc_vat_vf, 
		convert(decimal(12, 4), local_total_vat_vf_m) local_total_vat_vf, convert(decimal(12, 4), local_total_prof_fee_vf_m) local_total_prof_fee_vf, 

		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		num_erp_allocation_lines
	from 
		(select *, 
			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund * -1 local_discount_m, 
			local_store_credit_used_refund * -1 local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

			local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
			(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
			local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
		from
			(select *,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
				local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
					+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
			from Warehouse.sales.fact_order_line_v
			where -- website = 'visiondirect.es' and -- website_group = 'VisionDirect' -- 
				(local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0)) ol) ol) t
go

drop view tableau.fact_ol_trans_v
go 

create view tableau.fact_ol_trans_v as

	select ol.order_line_id_bk, ol.order_line_id_bk_c, 
		ol.order_id_bk, ol.order_id_bk_c, ol.invoice_id, ol.shipment_id, ol.creditmemo_id,
		ol.order_no, 
		
		ol.order_date, ol.order_date_c, 
		ol.invoice_date, ol.invoice_date_c, 
		ol.shipment_date, ol.shipment_date_c,
		ol.refund_date, ol.refund_date_c,

		ol.acquired, ol.tld, ol.website_group, ol.website, ol.store_name, 
		ol.customer_id, ol.customer_id_c, ol.customer_email, 

		ol.order_status_name, ol.line_status_name, 

		ol.qty_unit, 

		ol.local_subtotal, ol.local_shipping, ol.local_discount, ol.local_store_credit_used, 
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.local_store_credit_given, ol.local_bank_online_given, 

		ol.global_subtotal, ol.global_shipping, ol.global_discount, ol.global_store_credit_used, 
		ol.global_total_inc_vat, ol.global_total_exc_vat, ol.global_total_vat, ol.global_total_prof_fee, 
		ol.global_store_credit_given, ol.global_bank_online_given,  

		ol.local_total_inc_vat_vf, ol.local_total_exc_vat_vf, ol.local_total_vat_vf, ol.local_total_prof_fee_vf,
		ol.local_to_global_rate, ol.order_currency_code
	from 
			Warehouse.sales.fact_order_line_trans_v ol
		inner join
			act.fact_activity_sales acs on ol.order_id_bk = acs.order_id_bk
		left join
			Warehouse.sales.dim_rank_freq_time rft2 on acs.next_order_freq_time = rft2.idFreq_time_sk
		left join
			Warehouse.sales.dim_rank_freq_time rft3 on acs.prev_order_freq_time = rft3.idFreq_time_sk
go



drop view tableau.fact_ol_new_v
go 

create view tableau.fact_ol_new_v as

	select ol.order_line_id_bk, ol.order_line_id_bk_c, 
		ol.order_id_bk, ol.order_id_bk_c, 
		ol.order_no, ol.invoice_no, ol.shipment_no, -- ol.creditmemo_no, 
		
		ol.order_date_c, order_week_day, order_day_part, -- ol.order_date, 
		ol.invoice_date_c, invoice_week_day, -- ol.invoice_date, 
		ol.shipment_date_c, shipment_week_day, -- ol.shipment_date, 
		-- ol.refund_date, ol.refund_date_c,
		
		ol.rank_shipping_days, ol.shipping_days,

		ol.tld, ol.website_group, ol.website, ol.store_name, -- ol.acquired, 
		ol.company_name_create, ol.website_group_create, ol.website_create,
		ol.customer_origin_name,
		ol.market_name,
		ol.customer_id, ol.customer_id_c, ol.customer_email, ol.customer_name,
		ol.country_code_ship, ol.region_name_ship, -- ol.country_name_ship, ol.postcode_shipping, 
		ol.country_code_bill, -- ol.country_name_bill, ol.postcode_billing, 
		-- ol.customer_unsubscribe_name,
		ol.order_stage_name, ol.order_status_name, ol.line_status_name, ol.order_type_name, ol.order_status_magento_name, -- ol.adjustment_order, 
		ol.payment_method_name, ol.shipping_carrier_name, -- ol.cc_type_name, ol.shipping_method_name, ol.telesales_username, ol.prescription_method_name,
		ol.reminder_type_name, ol.reorder_f, -- ol.reminder_period_name, ol.reminder_date, ol.reorder_date, 
		ol.channel_name, ol.marketing_channel_name, ol.group_coupon_code_name, ol.coupon_code,
		ol.customer_status_name, ol.rank_seq_no_gen rank_seq_no, ol.customer_order_seq_no_gen customer_order_seq_no, ol.rank_seq_no_web, ol.customer_order_seq_no_web, -- ol.rank_seq_no, ol.customer_order_seq_no, 
		ol.proforma, -- ol.order_source, 
		-- ol.product_type_oh_name, ol.order_qty_time, ol.num_diff_product_type_oh,

		ol.manufacturer_name, 
		ol.product_type_name, ol.category_name, ol.product_family_group_name, ol.cl_type_name, ol.cl_feature_name, 
		ol.product_id_magento, ol.product_family_name, -- ol.product_family_code, 

		ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.addition, ol.dominance, ol.colour, 
		-- ol.eye,
		-- ol.glass_vision_type_name, ol.glass_package_type_name, 
		ol.sku_magento, -- ol.sku_erp, 
		-- ol.aura_product_f,

		ol.countries_registered_code, ol.product_type_vat, ol.vat_rate, ol.prof_fee_rate,

		ol.qty_unit, ol.qty_pack, ol.rank_qty_time, ol.qty_time,
		rft3.rank_freq_time rank_prev_order_freq_time, acs.prev_order_freq_time, rft2.rank_freq_time rank_next_order_freq_time, acs.next_order_freq_time,

		ol.price_type_name, ol.discount_f,
		ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
		-- ol.global_price_unit, ol.global_price_pack, ol.global_price_pack_discount,

		ol.local_shipping, ol.local_discount, ol.local_store_credit_used, 
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.local_store_credit_given, ol.local_bank_online_given, 

		ol.global_shipping, ol.global_discount, ol.global_store_credit_used, 
		ol.global_total_inc_vat, ol.global_total_exc_vat, ol.global_total_vat, ol.global_total_prof_fee, 
		ol.global_store_credit_given, ol.global_bank_online_given,  

		ol.local_total_inc_vat_vf, ol.local_total_exc_vat_vf, ol.local_total_vat_vf, ol.local_total_prof_fee_vf

		-- ol.local_to_global_rate, ol.order_currency_code, 
		-- ol.discount_percent, ol.vat_percent, ol.prof_fee_percent, 
		-- ol.num_erp_allocation_lines
	from 
			Warehouse.sales.fact_order_line_trans_v ol
		inner join
			act.fact_activity_sales acs on ol.order_id_bk = acs.order_id_bk
		left join
			Warehouse.sales.dim_rank_freq_time rft2 on acs.next_order_freq_time = rft2.idFreq_time_sk
		left join
			Warehouse.sales.dim_rank_freq_time rft3 on acs.prev_order_freq_time = rft3.idFreq_time_sk

go
