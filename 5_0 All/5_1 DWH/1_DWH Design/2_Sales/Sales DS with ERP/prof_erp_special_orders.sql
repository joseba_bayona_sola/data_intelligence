
-- BOM orders
	-- fact_order_line_erp_v
select top 1000 *
from Warehouse.alloc.fact_order_line_erp_v

select top 1000 idOrderHeaderERP_sk, idOrderLineERP_sk, 
	orderlineid_bk, orderid_bk, 
	order_no_erp, 
	order_line_id_bk, order_id_bk, order_date_sync, issue_date, 
	bom_f, 
	product_id_magento, product_family_name,
	qty_unit, qty_unit_allocated
from Warehouse.alloc.fact_order_line_erp_v
where bom_f = 'Y'
order by orderlineid_bk desc

select top 1000 order_type_erp_f, bom_f, count(*)
from Warehouse.alloc.fact_order_line_erp_v
group by order_type_erp_f, bom_f
order by order_type_erp_f, bom_f

	select product_id_magento, product_family_name, count(*)
	from Warehouse.alloc.fact_order_line_erp_v
	where bom_f = 'Y'
	group by product_id_magento, product_family_name
	order by product_id_magento, product_family_name


-- Many Batch Issues