
-- Things to DO
	-- fact_order_line_erp_issue_v: 
		-- Include NOT ISSUED OH, OL rows
		-- Add new Service related columns (even they are empty)
		-- Add PROD data from Issue 
		-- Add dates from Receipt, Batch
		

select top 1000 *
from Warehouse.alloc.dim_order_header_erp_v
where order_id_bk = 9177537

select top 1000 count(*)
from Warehouse.alloc.dim_order_header_erp_v
where order_date_sync between '2019-08-01' and '2019-09-01'

-- 

select *
from Warehouse.alloc.fact_order_line_erp_v
where order_id_bk = 9177537

select top 1000 idOrderHeaderERP_sk, idOrderLineERP_sk, order_no_erp, 
	order_line_id_bk, order_id_bk, 
	order_date_sync, 
	order_type_erp_f, order_type_erp_name, order_status_erp_name, 
	warehouse_name_pref, 
	bom_f, product_type_name, category_name, product_id_magento, product_family_name, 
	parameter, product_description, 
	pack_size_opt_desc_pref, inventory_level_success_f, po_source_name_sh, po_type_name_sh, purchase_order_number_sh, 
	qty_unit, global_subtotal
from Warehouse.alloc.fact_order_line_erp_v
where order_id_bk = 9177537

select top 1000 count(*)
from Warehouse.alloc.fact_order_line_erp_v
where order_date_sync between '2019-08-01' and '2019-09-01'

	select top 1000 order_type_erp_f, count(*), sum(global_subtotal)
	from Warehouse.alloc.fact_order_line_erp_v
	where order_date_sync between '2019-08-01' and '2019-09-01'
	group by order_type_erp_f
	order by order_type_erp_f

-- 

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where order_id_bk = 9177537

select top 1000 idOrderHeaderERP_sk, idOrderLineERP_sk, idOrderLineERPIssue_sk, order_no_erp, 
	order_line_id_bk, order_id_bk, 
	order_date_sync, 
	order_type_erp_f, order_type_erp_name, order_status_erp_name, 
	warehouse_name_pref, 
	bom_f, product_type_name, category_name, product_id_magento, product_family_name, 
	parameter, product_description, SKU, stock_item_description, -- prod issue related attributes
	-- service related attributes
	issue_id, warehouse_name, batch_id, supplier_name, po_source_name, po_type_name, purchase_order_number, wh_shipment_type_name, receipt_number, -- invoice rec ??, dates ??

	qty_unit, qty_stock, global_total_unit_cost, global_total_unit_cost_discount
from Warehouse.alloc.fact_order_line_erp_issue_v
where order_id_bk = 9177537



select top 1000 count(*)
from Warehouse.alloc.fact_order_line_erp_issue_v
where order_date_sync between '2019-08-01' and '2019-09-01'
