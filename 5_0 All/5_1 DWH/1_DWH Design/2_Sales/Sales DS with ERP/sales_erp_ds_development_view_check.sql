
	select top 1000 count(*) over (), cnt1, transaction_type, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f
	from
		(select distinct count(*) over () cnt1, transaction_type, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f
		from Warehouse.alloc.fact_order_line_erp_issue_all
		where idOrderLine_sk <> -1
			and transaction_type in ('O', 'C')) t

	select top 1000 count(*) over (), cnt1, transaction_type, idOrderLine_sk, deduplicate_f
	from
		(select distinct count(*) over () cnt1, transaction_type, idOrderLine_sk, deduplicate_f
		from Warehouse.alloc.fact_order_line_erp_issue_all
		where idOrderLine_sk <> -1
			and transaction_type in ('O', 'C')) t

	--------------------

	select top 1000 count(*) over (partition by transaction_type, idOrderLine_sk) num_rep, *
	from Warehouse.alloc.fact_order_line_erp_issue_all
	where idOrderLine_sk <> -1
		and transaction_type in ('O', 'C')
		-- and qty_unit is null
	order by num_rep desc, idOrderLine_sk

		select top 1000 *
		from Warehouse.alloc.fact_order_line_erp_issue
		where idOrderLineERP_sk_fk = 10318670

	select transaction_type, year(invoice_date), month(invoice_date), day(invoice_date), sum(global_total_exc_vat)
	from 
		(select *, invoice_date calc_date
		from Warehouse.alloc.fact_order_line_erp_issue_all) ol
	group by transaction_type, year(invoice_date), month(invoice_date), day(invoice_date)
	order by transaction_type, year(invoice_date), month(invoice_date), day(invoice_date)

	select year(calc_date), month(calc_date), sum(global_total_exc_vat)
	from 
		(select *, invoice_date calc_date
		from Warehouse.alloc.fact_order_line_erp_issue_all) ol
	group by year(calc_date), month(calc_date)
	order by year(calc_date), month(calc_date)
