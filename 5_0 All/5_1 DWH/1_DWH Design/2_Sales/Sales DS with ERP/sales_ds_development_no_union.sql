select *
from Warehouse.sales.fact_order_line_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)

select *
from Warehouse.sales.fact_order_line_refund_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)

select *
from Warehouse.sales.fact_order_line_adj_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)

	select *
	from Warehouse.sales.fact_order_line_adj
	where idOrderLine_sk_fk in (17289447, 17289448, 17744510, 17744511)

select *
from Warehouse.sales.fact_order_line_trans_tr_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
order by idOrderLine_sk

---------------------------------------------

	select top 1000 count(*) over (), *
	from
		(select 'O' transaction_type, idOrderLine_sk, -1 idOrderLineAdj_sk
		from Warehouse.sales.fact_order_line
		union
		select 'C' transaction_type, idOrderLine_sk_fk idOrderLine_sk, -1 idOrderLineAdj_sk
		from Warehouse.sales.fact_order_line_refund
		union
		select 'A' transaction_type, idOrderLine_sk_fk idOrderLine_sk, idOrderLineAdj_sk
		from Warehouse.sales.fact_order_line_adj) t

---------------------------------------------

-- can work also with OL with many ADJ lines (when doing the union we avoid duplicates)

select transaction_type, idOrderLine_sk
into #ol_trans
from
	(select 'O' transaction_type, idOrderLine_sk
	from Warehouse.sales.fact_order_line_v
	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
	union
	select 'C' transaction_type, idOrderLine_sk
	from Warehouse.sales.fact_order_line_refund_v
	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
	union
	select 'A' transaction_type, idOrderLine_sk
	from Warehouse.sales.fact_order_line_adj_v
	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)) t

-- order_line_id_bk_c
-- order_id_bk_c
-- invoice_date, invoice_date_c, invoice_week_day
-- shipment_date, shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part,
-- customer_id_c
-- qty_unit, qty_pack, weight

-- local_subtotal, local_shipping, local_discount, local_store_credit, 
-- local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
-- global_subtotal, global_shipping, global_discount, global_store_credit, 
-- global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
-- local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf,

-- local_payment_online, local_payment_offline, local_reimbursement,
-- global_payment_online, global_payment_offline, global_reimbursement,

-- local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost, 
-- global_product_cost, global_shipping_cost, global_freight_cost, global_total_cost,

	select 
		olt.transaction_type,
		coalesce(ol.idOrderHeader_sk, olr.idOrderHeader_sk, ola.idOrderHeader_sk) idOrderHeader_sk, olt.idOrderLine_sk,
		coalesce(ol.order_line_id_bk, olr.order_line_id_bk, ola.order_line_id_bk) order_line_id_bk, case when (olt.transaction_type = 'O') then ol.order_line_id_bk else null end order_line_id_bk_c, 
		coalesce(ol.order_id_bk, olr.order_id_bk, ola.order_id_bk) order_id_bk, case when (olt.transaction_type = 'O') then ol.order_id_bk else null end order_id_bk_c, 
		coalesce(ol.invoice_id, olr.invoice_id, ola.invoice_id) invoice_id, coalesce(ol.shipment_id, ola.shipment_id, olr.shipment_id) shipment_id, 
			coalesce(ol.creditmemo_id, ola.creditmemo_id, olr.creditmemo_id) creditmemo_id, -- creditmemo_id, creditmemo_no only for Refund ??
		coalesce(ol.order_no, olr.order_no, ola.order_no) order_no, coalesce(ol.invoice_no, olr.invoice_no, ola.invoice_no) invoice_no, 
		coalesce(ol.shipment_no, olr.shipment_no, ola.shipment_no) shipment_no, coalesce(ol.creditmemo_no, olr.creditmemo_no, ola.creditmemo_no) creditmemo_no,
		
		coalesce(ol.order_date, olr.order_date, ola.order_date) order_date, coalesce(ol.order_date_c, olr.order_date_c, ola.order_date_c) order_date_c, 
		coalesce(ol.order_week_day, olr.order_week_day, ola.order_week_day) order_week_day, coalesce(ol.order_time_name, olr.order_time_name, ola.order_time_name) order_time_name, 
		coalesce(ol.order_day_part, olr.order_day_part, ola.order_day_part) order_day_part, 
		case 
			when (olt.transaction_type = 'O') then ol.invoice_date
			when (olt.transaction_type = 'C') then case when (olr.invoice_date is not null) then olr.refund_date else null end
			when (olt.transaction_type = 'A') then case when (ola.invoice_date is not null) then ola.invoice_posted_date else null end
		end invoice_date, 
		case 
			when (olt.transaction_type = 'O') then ol.invoice_date_c
			when (olt.transaction_type = 'C') then case when (olr.invoice_date is not null) then olr.refund_date_c else null end
			when (olt.transaction_type = 'A') then case when (ola.invoice_date is not null) then ola.invoice_posted_date_c else null end
		end invoice_date_c, 
		case 
			when (olt.transaction_type = 'O') then ol.invoice_week_day
			when (olt.transaction_type = 'C') then case when (olr.invoice_date is not null) then olr.refund_week_day else null end
			when (olt.transaction_type = 'A') then case when (ola.invoice_date is not null) then ola.refund_week_day else null end -- need invoice_posted_week_day
		end invoice_week_day,
		case 
			when (olt.transaction_type = 'O') then ol.shipment_date_r
			when (olt.transaction_type = 'C') then case when (olr.shipment_date_r is not null) then olr.refund_date else null end
			when (olt.transaction_type = 'A') then case when (ola.shipment_date_r is not null) then ola.invoice_posted_date else null end
		end shipment_date, 
		case 
			when (olt.transaction_type = 'O') then ol.shipment_date_c
			when (olt.transaction_type = 'C') then case when (olr.shipment_date_r is not null) then olr.refund_date_c else null end
			when (olt.transaction_type = 'A') then case when (ola.shipment_date_r is not null) then ola.invoice_posted_date_c else null end
		end shipment_date_c, 
		case 
			when (olt.transaction_type = 'O') then ol.shipment_week_day
			when (olt.transaction_type = 'C') then case when (olr.shipment_date_r is not null) then olr.refund_week_day else null end
			when (olt.transaction_type = 'A') then case when (ola.shipment_date_r is not null) then ola.refund_week_day else null end -- need invoice_posted_week_day
		end shipment_week_day, 
		case when (olt.transaction_type = 'O') then ol.shipment_time_name else null end shipment_time_name, case when (olt.transaction_type = 'O') then ol.shipment_day_part else null end shipment_day_part,
		coalesce(ol.refund_date, olr.refund_date, ola.refund_date) refund_date, coalesce(ol.refund_date_c, olr.refund_date_c, ola.refund_date_c) refund_date_c, 
		coalesce(ol.refund_week_day, olr.refund_week_day, ola.refund_week_day) refund_week_day, -- refund_date only for refunds

		coalesce(ol.rank_shipping_days, olr.rank_shipping_days, ola.rank_shipping_days) rank_shipping_days, coalesce(ol.shipping_days, olr.shipping_days, ola.shipping_days) shipping_days, -- what about shipping_days on refunds

		coalesce(ol.acquired, olr.acquired, ola.acquired) acquired, coalesce(ol.tld, olr.tld, ola.tld) tld, coalesce(ol.website_group, olr.website_group, ola.website_group) website_group, 
		coalesce(ol.website, olr.website, ola.website) website, coalesce(ol.store_name, olr.store_name, ola.store_name) store_name, 
		coalesce(ol.company_name_create, olr.company_name_create, ola.company_name_create) company_name_create, coalesce(ol.website_group_create, olr.website_group_create, ola.website_group_create) website_group_create, 
			coalesce(ol.website_create, olr.website_create, ola.website_create) website_create, 
		coalesce(ol.customer_origin_name, olr.customer_origin_name, ola.customer_origin_name) customer_origin_name, coalesce(ol.current_customer_status_name, olr.current_customer_status_name, ola.current_customer_status_name) current_customer_status_name, 
		coalesce(ol.market_name, olr.market_name, ola.market_name) market_name, 
		coalesce(ol.customer_id, olr.customer_id, ola.customer_id) customer_id, 
		case when (olt.transaction_type = 'O') then ol.customer_id else null end customer_id_c, 
		coalesce(ol.customer_email, olr.customer_email, ola.customer_email) customer_email, coalesce(ol.customer_name, olr.customer_name, ola.customer_name) customer_name, 
		coalesce(ol.country_zone_ship, olr.country_zone_ship, ola.country_zone_ship) country_zone_ship, coalesce(ol.country_continent_ship, olr.country_continent_ship, ola.country_continent_ship) country_continent_ship, 
			coalesce(ol.country_code_ship, olr.country_code_ship, ola.country_code_ship) country_code_ship, coalesce(ol.country_name_ship, olr.country_name_ship, ola.country_name_ship) country_name_ship, 
			coalesce(ol.region_name_ship, olr.region_name_ship, ola.region_name_ship) region_name_ship, coalesce(ol.postcode_shipping, olr.postcode_shipping, ola.postcode_shipping) postcode_shipping, 
		coalesce(ol.country_code_bill, olr.country_code_bill, ola.country_code_bill) country_code_bill, coalesce(ol.country_name_bill, olr.country_name_bill, ola.country_name_bill) country_name_bill, 
			coalesce(ol.postcode_billing, olr.postcode_billing, ola.postcode_billing) postcode_billing, 
		coalesce(ol.customer_unsubscribe_name, olr.customer_unsubscribe_name, ola.customer_unsubscribe_name) customer_unsubscribe_name, 
		coalesce(ol.order_stage_name, olr.order_stage_name, ola.order_stage_name) order_stage_name, coalesce(ol.order_status_name, olr.order_status_name, ola.order_status_name) order_status_name, 
			coalesce(ol.line_status_name, olr.line_status_name, ola.line_status_name) line_status_name, coalesce(ol.adjustment_order, olr.adjustment_order, ola.adjustment_order) adjustment_order, 
			coalesce(ol.order_type_name, olr.order_type_name, ola.order_type_name) order_type_name, coalesce(ol.order_status_magento_name, olr.order_status_magento_name, ola.order_status_magento_name) order_status_magento_name, 
		coalesce(ol.payment_method_name, olr.payment_method_name, ola.payment_method_name) payment_method_name, coalesce(ol.cc_type_name, olr.cc_type_name, ola.cc_type_name) cc_type_name, 
			coalesce(ol.shipping_carrier_name, olr.shipping_carrier_name, ola.shipping_carrier_name) shipping_carrier_name, coalesce(ol.shipping_method_name, olr.shipping_method_name, ola.shipping_method_name) shipping_method_name, 
			coalesce(ol.telesales_username, olr.telesales_username, ola.telesales_username) telesales_username, coalesce(ol.prescription_method_name, olr.prescription_method_name, ola.prescription_method_name) prescription_method_name, 
		coalesce(ol.reminder_type_name, olr.reminder_type_name, ola.reminder_type_name) reminder_type_name, coalesce(ol.reminder_period_name, olr.reminder_period_name, ola.reminder_period_name) reminder_period_name, 
			coalesce(ol.reminder_date, olr.reminder_date, ola.reminder_date) reminder_date, coalesce(ol.reorder_f, olr.reorder_f, ola.reorder_f) reorder_f, coalesce(ol.reorder_date, olr.reorder_date, ola.reorder_date) reorder_date, 
		coalesce(ol.channel_name, olr.channel_name, ola.channel_name) channel_name, coalesce(ol.marketing_channel_name, olr.marketing_channel_name, ola.marketing_channel_name) marketing_channel_name, 
			coalesce(ol.group_coupon_code_name, olr.group_coupon_code_name, ola.group_coupon_code_name) group_coupon_code_name, coalesce(ol.coupon_code, olr.coupon_code, ola.coupon_code) coupon_code, 
		coalesce(ol.reimbursement_type_name, olr.reimbursement_type_name, ola.reimbursement_type_name) reimbursement_type_name, coalesce(ol.reimburser_name, olr.reimburser_name, ola.reimburser_name) reimburser_name, 
		coalesce(ol.customer_status_name, olr.customer_status_name, ola.customer_status_name) customer_status_name, 
			coalesce(ol.rank_seq_no, olr.rank_seq_no, ola.rank_seq_no) rank_seq_no, coalesce(ol.customer_order_seq_no, olr.customer_order_seq_no, ola.customer_order_seq_no) customer_order_seq_no, 
			coalesce(ol.rank_seq_no_web, olr.rank_seq_no_web, ola.rank_seq_no_web) rank_seq_no_web, coalesce(ol.customer_order_seq_no_web, olr.customer_order_seq_no_web, ola.customer_order_seq_no_web) customer_order_seq_no_web, 
			coalesce(ol.rank_seq_no_gen, olr.rank_seq_no_gen, ola.rank_seq_no_gen) rank_seq_no_gen, coalesce(ol.customer_order_seq_no_gen, olr.customer_order_seq_no_gen, ola.customer_order_seq_no_gen) customer_order_seq_no_gen, 
		coalesce(ol.order_source, olr.order_source, ola.order_source) order_source, coalesce(ol.proforma, olr.proforma, ola.proforma) proforma, coalesce(ol.platform, olr.platform, ola.platform) platform, 
		coalesce(ol.product_type_oh_name, olr.product_type_oh_name, ola.product_type_oh_name) product_type_oh_name, coalesce(ol.order_qty_time, olr.order_qty_time, ola.order_qty_time) order_qty_time, 
			coalesce(ol.num_diff_product_type_oh, olr.num_diff_product_type_oh, ola.num_diff_product_type_oh) num_diff_product_type_oh, 

		coalesce(ol.manufacturer_name, olr.manufacturer_name, ola.manufacturer_name) manufacturer_name, coalesce(ol.brand_name, olr.brand_name, ola.brand_name) brand_name, 
		coalesce(ol.product_type_name, olr.product_type_name, ola.product_type_name) product_type_name, coalesce(ol.category_name, olr.category_name, ola.category_name) category_name, 
			coalesce(ol.product_family_group_name, olr.product_family_group_name, ola.product_family_group_name) product_family_group_name, 
			coalesce(ol.cl_type_name, olr.cl_type_name, ola.cl_type_name) cl_type_name, coalesce(ol.cl_feature_name, olr.cl_feature_name, ola.cl_feature_name) cl_feature_name, 
		coalesce(ol.product_id_magento, olr.product_id_magento, ola.product_id_magento) product_id_magento, 
			coalesce(ol.product_family_code, olr.product_family_code, ola.product_family_code) product_family_code, coalesce(ol.product_family_name, olr.product_family_name, ola.product_family_name) product_family_name, 

		coalesce(ol.base_curve, olr.base_curve, ola.base_curve) base_curve, coalesce(ol.diameter, olr.diameter, ola.diameter) diameter, coalesce(ol.power, olr.power, ola.power) power, 
			coalesce(ol.cylinder, olr.cylinder, ola.cylinder) cylinder, coalesce(ol.axis, olr.axis, ola.axis) axis, 
			coalesce(ol.addition, olr.addition, ola.addition) addition, coalesce(ol.dominance, olr.dominance, ola.dominance) dominance, coalesce(ol.colour, olr.colour, ola.colour) colour, 
		coalesce(ol.eye, olr.eye, ola.eye) eye, 
		coalesce(ol.glass_vision_type_name, olr.glass_vision_type_name, ola.glass_vision_type_name) glass_vision_type_name, coalesce(ol.glass_package_type_name, olr.glass_package_type_name, ola.glass_package_type_name) glass_package_type_name, 
		coalesce(ol.sku_magento, olr.sku_magento, ola.sku_magento) sku_magento, coalesce(ol.sku_erp, olr.sku_erp, ola.sku_erp) sku_erp, 
		coalesce(ol.aura_product_f, olr.aura_product_f, ola.aura_product_f) aura_product_f, 

		coalesce(ol.warehouse_name, olr.warehouse_name, ola.warehouse_name) warehouse_name, 

		coalesce(ol.countries_registered_code, olr.countries_registered_code, ola.countries_registered_code) countries_registered_code, coalesce(ol.product_type_vat, olr.product_type_vat, ola.product_type_vat) product_type_vat, 
		
		coalesce(ol.pack_size, olr.pack_size, ola.pack_size) pack_size,
		case 
			when (olt.transaction_type = 'O') then ol.qty_unit
			when (olt.transaction_type = 'C') then olr.qty_unit_refunded * -1
			when (olt.transaction_type = 'A') then ola.qty_unit
		end qty_unit,
		case 
			when (olt.transaction_type = 'O') then ol.qty_pack
			when (olt.transaction_type = 'C') then convert(int, (olr.qty_unit_refunded * -1) / (olr.qty_unit / olr.qty_pack))
			when (olt.transaction_type = 'A') then ola.qty_pack
		end qty_pack, 
		coalesce(ol.rank_qty_time, olr.rank_qty_time, ola.rank_qty_time) rank_qty_time, coalesce(ol.qty_time, olr.qty_time, ola.qty_time) qty_time, -- what about qty_time on refunds
		case 
			when (olt.transaction_type = 'O') then ol.weight
			when (olt.transaction_type = 'C') then olr.weight * -1 
			when (olt.transaction_type = 'A') then ola.weight
		end weight,
		
		coalesce(ol.price_type_name, olr.price_type_name, ola.price_type_name) price_type_name, coalesce(ol.discount_f, olr.discount_f, ola.discount_f) discount_f, 
		coalesce(ol.local_price_unit, olr.local_price_unit, ola.local_price_unit) local_price_unit, coalesce(ol.local_price_unit_discount, olr.local_price_unit_discount, ola.local_price_unit_discount) local_price_unit_discount, 
			coalesce(ol.local_price_pack, olr.local_price_pack, ola.local_price_pack) local_price_pack, coalesce(ol.local_price_pack_discount, olr.local_price_pack_discount, ola.local_price_pack_discount) local_price_pack_discount, 
		coalesce(ol.global_price_unit, olr.global_price_unit, ola.global_price_unit) global_price_unit, coalesce(ol.global_price_unit_discount, olr.global_price_unit_discount, ola.global_price_unit_discount) global_price_unit_discount, 
			coalesce(ol.global_price_pack, olr.global_price_pack, ola.global_price_pack) global_price_pack, coalesce(ol.global_price_pack_discount, olr.global_price_pack_discount, ola.global_price_pack_discount) global_price_pack_discount, 

		case 
			when (olt.transaction_type = 'O') then ol.local_subtotal
			when (olt.transaction_type = 'C') then olr.local_subtotal_m
			when (olt.transaction_type = 'A') then ola.local_subtotal
		end local_subtotal, 
		case 
			when (olt.transaction_type = 'O') then ol.local_shipping
			when (olt.transaction_type = 'C') then olr.local_shipping_m
			when (olt.transaction_type = 'A') then ola.local_shipping
		end local_shipping, 
		case 
			when (olt.transaction_type = 'O') then ol.local_discount * -1 
			when (olt.transaction_type = 'C') then olr.local_discount_m
			when (olt.transaction_type = 'A') then ola.local_discount * -1 
		end local_discount, 
		case 
			when (olt.transaction_type = 'O') then ol.local_store_credit_used * -1
			when (olt.transaction_type = 'C') then olr.local_store_credit_given
			when (olt.transaction_type = 'A') then ola.local_store_credit_used * -1
		end local_store_credit, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_inc_vat
			when (olt.transaction_type = 'C') then olr.local_total_inc_vat_m
			when (olt.transaction_type = 'A') then ola.local_total_inc_vat
		end local_total_inc_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_exc_vat
			when (olt.transaction_type = 'C') then olr.local_total_exc_vat_m
			when (olt.transaction_type = 'A') then ola.local_total_exc_vat
		end local_total_exc_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_vat
			when (olt.transaction_type = 'C') then olr.local_total_vat_m
			when (olt.transaction_type = 'A') then ola.local_total_vat
		end local_total_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_prof_fee
			when (olt.transaction_type = 'C') then olr.local_total_prof_fee_m
			when (olt.transaction_type = 'A') then ola.local_total_prof_fee
		end local_total_prof_fee, 
		-- 0 local_store_credit_given, 0 local_bank_online_given, 

		case 
			when (olt.transaction_type = 'O') then ol.global_subtotal
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_subtotal_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_subtotal
		end global_subtotal, 
		case 
			when (olt.transaction_type = 'O') then ol.global_shipping
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_shipping_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_shipping
		end global_shipping, 
		case 
			when (olt.transaction_type = 'O') then ol.global_discount * -1 
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_discount_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_discount * -1
		end global_discount, 
		case 
			when (olt.transaction_type = 'O') then ol.global_store_credit_used * -1 
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.global_store_credit_given * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_store_credit_used * -1
		end global_store_credit, 
		case 
			when (olt.transaction_type = 'O') then ol.global_total_inc_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_inc_vat_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_total_inc_vat
		end global_total_inc_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.global_total_exc_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_exc_vat_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_total_exc_vat
		end global_total_exc_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.global_total_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_vat_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_total_vat
		end global_total_vat, 
		case 
			when (olt.transaction_type = 'O') then ol.global_total_prof_fee
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_prof_fee_m * olr.local_to_global_rate)
			when (olt.transaction_type = 'A') then ola.global_total_prof_fee
		end global_total_prof_fee, 
		-- 0 global_store_credit_given, 0 global_bank_online_given,  -- Should only appear at Refund level ??

		case 
			when (olt.transaction_type = 'O') then ol.local_total_inc_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_inc_vat_vf_m)
			when (olt.transaction_type = 'A') then ola.local_total_inc_vat_vf
		end local_total_inc_vat_vf, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_exc_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_exc_vat_vf_m)
			when (olt.transaction_type = 'A') then ola.local_total_exc_vat_vf
		end local_total_exc_vat_vf, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_vat_vf_m)
			when (olt.transaction_type = 'A') then ola.local_total_vat_vf
		end local_total_vat_vf, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_prof_fee_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), olr.local_total_prof_fee_vf_m)
			when (olt.transaction_type = 'A') then ola.local_total_prof_fee_vf
		end local_total_prof_fee_vf,

		case 
			when (olt.transaction_type = 'O') then ol.local_payment_online
			when (olt.transaction_type = 'C') then olr.local_payment_refund_online * -1 
			when (olt.transaction_type = 'A') then ola.local_payment_online
		end local_payment_online, 
		case 
			when (olt.transaction_type = 'O') then 0 
			when (olt.transaction_type = 'C') then (olr.local_bank_online_given - olr.local_payment_refund_online) * -1 
			when (olt.transaction_type = 'A') then 0
		end local_payment_offline, 
		case 
			when (olt.transaction_type = 'O') then ol.local_reimbursement
			when (olt.transaction_type = 'C') then olr.local_reimbursement * -1 
			when (olt.transaction_type = 'A') then ola.local_reimbursement
		end local_reimbursement,
		case 
			when (olt.transaction_type = 'O') then ol.global_payment_online
			when (olt.transaction_type = 'C') then olr.global_payment_refund_online * -1 
			when (olt.transaction_type = 'A') then ola.global_payment_online
		end global_payment_online, 
		case 
			when (olt.transaction_type = 'O') then 0 
			when (olt.transaction_type = 'C') then (olr.global_bank_online_given - olr.global_payment_refund_online) * -1 
			when (olt.transaction_type = 'A') then 0
		end global_payment_offline, 
		case 
			when (olt.transaction_type = 'O') then ol.global_reimbursement
			when (olt.transaction_type = 'C') then olr.global_reimbursement * -1 
			when (olt.transaction_type = 'A') then ola.global_reimbursement
		end global_reimbursement,

		case 
			when (olt.transaction_type = 'O') then ol.local_product_cost_discount 
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.local_product_cost_discount
		end local_product_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.local_shipping_cost
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.local_shipping_cost
		end local_shipping_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.local_freight_cost
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.local_freight_cost
		end local_freight_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.local_total_cost_discount 
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.local_total_cost_discount
		end local_total_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.global_product_cost_discount 
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.global_product_cost_discount
		end global_product_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.global_shipping_cost
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.global_shipping_cost
		end global_shipping_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.global_freight_cost
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.global_freight_cost
		end global_freight_cost, 
		case 
			when (olt.transaction_type = 'O') then ol.global_total_cost_discount 
			when (olt.transaction_type = 'C') then 0
			when (olt.transaction_type = 'A') then ola.global_total_cost_discount
		end global_total_cost, 

		coalesce(ol.local_to_global_rate, olr.local_to_global_rate, ola.local_to_global_rate) local_to_global_rate, coalesce(ol.order_currency_code, olr.order_currency_code, ola.order_currency_code) order_currency_code, 
		coalesce(ol.discount_percent, olr.discount_percent, ola.discount_percent) discount_percent, coalesce(ol.vat_percent, olr.vat_percent, ola.vat_percent) vat_percent, 
			coalesce(ol.prof_fee_percent, olr.prof_fee_percent, ola.prof_fee_percent) prof_fee_percent, 
			coalesce(ol.vat_rate, olr.vat_rate, ola.vat_rate) vat_rate, coalesce(ol.prof_fee_rate, olr.prof_fee_rate, ola.prof_fee_rate) prof_fee_rate, 
		coalesce(ol.num_erp_allocation_lines, olr.num_erp_allocation_lines, ola.num_erp_allocation_lines) num_erp_allocation_lines

	from 
			#ol_trans olt
		left join
			Warehouse.sales.fact_order_line_v ol on olt.idOrderLine_sk = ol.idOrderLine_sk and olt.transaction_type = 'O'
		left join
			(select *, 
				local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund local_discount_m, 
				local_store_credit_used_refund local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

				local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
				(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
				local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

				local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
				(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
				local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
			from
				(select *,
					local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
						+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
					local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
						+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
				from Warehouse.sales.fact_order_line_refund_v) ol) olr on olt.idOrderLine_sk = olr.idOrderLine_sk and olt.transaction_type = 'C'	
		left join
			Warehouse.sales.fact_order_line_adj_v ola on olt.idOrderLine_sk = ola.idOrderLine_sk and olt.transaction_type = 'A'	
	order by olt.idOrderLine_sk, olt.transaction_type

	select 
		olt.transaction_type,
		idOrderHeader_sk, olt.idOrderLine_sk,
		order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
		order_id_bk, order_id_bk order_id_bk_c, invoice_id, shipment_id, creditmemo_id, -- creditmemo_id, creditmemo_no only for Refund ??
		order_no, invoice_no, shipment_no, creditmemo_no,
		
		order_date, order_date_c, order_week_day, order_time_name, order_day_part,
		invoice_date, invoice_date_c, invoice_week_day,
		shipment_date_r shipment_date, shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part,
		refund_date, refund_date_c, refund_week_day, -- refund_date only for refunds

		rank_shipping_days, shipping_days, -- what about shipping_days on refunds

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name, current_customer_status_name, 
		market_name,
		customer_id, customer_id customer_id_c, customer_email, customer_name,
		country_zone_ship, country_continent_ship, country_code_ship, country_name_ship, region_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
		customer_unsubscribe_name,
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, prescription_method_name,
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, group_coupon_code_name, coupon_code,
		reimbursement_type_name, reimburser_name,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen,
		order_source, proforma, platform,
		product_type_oh_name, order_qty_time, num_diff_product_type_oh,

		manufacturer_name, brand_name,
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name, 
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		eye,
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		warehouse_name, 

		countries_registered_code, product_type_vat,
		
		pack_size, 
		qty_unit, qty_pack, rank_qty_time, qty_time, -- what about qty_time on refunds
		weight,

		price_type_name, discount_f,
		local_price_unit, local_price_unit_discount, local_price_pack, local_price_pack_discount, 
		global_price_unit, global_price_unit_discount, global_price_pack, global_price_pack_discount,

		local_subtotal, local_shipping, local_discount * -1 local_discount, local_store_credit_used * -1 local_store_credit, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		-- 0 local_store_credit_given, 0 local_bank_online_given, 

		global_subtotal, global_shipping, global_discount * -1 global_discount, global_store_credit_used * -1 global_store_credit, 
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		-- 0 global_store_credit_given, 0 global_bank_online_given,  -- Should only appear at Refund level ??

		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf,

		local_payment_online, 0 local_payment_offline, local_reimbursement,
		global_payment_online, 0 global_payment_offline, global_reimbursement,

		local_product_cost_discount local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost_discount local_total_cost, 
		global_product_cost_discount global_product_cost, global_shipping_cost, global_freight_cost, global_total_cost_discount global_total_cost, 

		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		num_erp_allocation_lines
	from 
			#ol_trans olt
		inner join
			Warehouse.sales.fact_order_line_v ol on olt.idOrderLine_sk = ol.idOrderLine_sk and olt.transaction_type = 'O'