
-- fact_order_line_erp_v
	select top 10000 idOrderLineERP_sk, order_no_erp, 
		order_date_sync, -- order_date_sync_c,
		promised_shipment_date, -- promised_shipment_date_c,
		promised_delivery_date, -- promised_delivery_date_c,
		cage_shipment_date, -- cage_shipment_date_c, 
		snap_complete_date, -- snap_complete_date_c, 

		order_type_erp_f, order_status_erp_name, shipment_status_erp_name, allocation_status_name, allocation_strategy_name,

		warehouse_name_pref, 

		bom_f, 
		manufacturer_name, brand_name
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name -- product_type_oh_name, 
		product_id_magento, product_family_name, 
		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		-- eye
		parameter, product_description,

		inventory_level_success_f,
		pack_size_opt_desc_pref pack_size_optimum_dist, 
		po_source_name_sh shortage_PO_source, po_type_name_sh shortage_PO_type, purchase_order_number_sh shortage_PO_number, 
	
		qty_unit, qty_unit_allocated,
		local_subtotal, global_subtotal, 
		local_to_global_rate, order_currency_code
	from Warehouse.alloc.fact_order_line_erp_v
	-- where order_type_erp_f = 'W'
	order by idOrderLineERP_sk desc


---------------------------------

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v

	select 11455252 - 11441636

	select top 1000 count(*)
	from Warehouse.alloc.fact_order_line_erp_issue

	select top 1000 count(*)
	from Warehouse.alloc.fact_order_line_erp_issue_v


	-- Current OLI records not in OLI_v
	select count(*) over (), oli.idOrderLineERP_sk_fk, oli.idOrderLineERPIssue_sk, oli.batchstockissueid_bk, oli.orderlinestockallocationtransactionid_bk, oli.issue_id,
		ole.order_no_erp, ole.product_id_magento, ole.product_family_name,
		oli.issue_date, oli.idWHStockItemBatch_sk_fk, oli.qty_stock, oli.qty_percentage
	from 
			Warehouse.alloc.fact_order_line_erp_issue oli 
		inner join
			Warehouse.alloc.fact_order_line_erp_v ole on oli.idOrderLineERP_sk_fk = ole.idOrderLineERP_sk 
		left join
			Warehouse.stock.dim_wh_stock_item_batch_v wsib on oli.idWHStockItemBatch_sk_fk = wsib.idWHStockItemBatch_sk
	where wsib.idWHStockItemBatch_sk is null
	order by oli.issue_date

	select oli.idWHStockItemBatch_sk_fk, ole.product_id_magento, ole.product_family_name, count(*)
	from 
			Warehouse.alloc.fact_order_line_erp_issue oli 
		inner join
			Warehouse.alloc.fact_order_line_erp_v ole on oli.idOrderLineERP_sk_fk = ole.idOrderLineERP_sk 
		left join
			Warehouse.stock.dim_wh_stock_item_batch_v wsib on oli.idWHStockItemBatch_sk_fk = wsib.idWHStockItemBatch_sk
	where wsib.idWHStockItemBatch_sk is null
	group by oli.idWHStockItemBatch_sk_fk, ole.product_id_magento, ole.product_family_name
	order by oli.idWHStockItemBatch_sk_fk, ole.product_id_magento, ole.product_family_name


	------------------------

	select count(*)
	from 
			Warehouse.alloc.fact_order_line_erp_v ole
		left join
			Warehouse.alloc.fact_order_line_erp_issue oli on ole.idOrderLineERP_sk = oli.idOrderLineERP_sk_fk
	where oli.idOrderLineERP_sk_fk is null

