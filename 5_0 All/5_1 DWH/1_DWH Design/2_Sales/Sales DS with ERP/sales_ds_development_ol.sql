
--select top 1000 *
--from Warehouse.sales.fact_order_line_refund

--	select top 1000 count(*)
--	from Warehouse.sales.fact_order_line_refund olr

--	select top 1000 count(*)
--	from 
--			Warehouse.sales.fact_order_line_refund olr
--		inner join
--			Warehouse.sales.fact_order_line ol on olr.idOrderLine_sk_fk = ol.idOrderLine_sk
--		left join
--			Warehouse.sales.fact_order_line_vat_fix olvf on ol.idOrderLine_sk = olvf.idOrderLine_sk_fk
--		left join
--			Warehouse.gen.dim_calendar cal_i on ol.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
--		left join	
--			Warehouse.gen.dim_time tim_s on ol.idTimeShipmentDate_sk_fk = tim_s.idTime_sk
--		inner join
--			Warehouse.sales.dim_order_status o_stat on ol.idOrderStatus_line_sk_fk = o_stat.idOrderStatus_sk
--		inner join
--			Warehouse.sales.dim_price_type prt on ol.idPriceType_sk_fk = prt.idPriceType_sk	
--		inner join
--			Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
--		left join
--			Warehouse.prod.dim_glass_vision_type gvt on ol.idGlassVisionType_sk_fk = gvt.idGlassVisionType_sk
--		left join
--			Warehouse.prod.dim_glass_package_type gpt on ol.idGlassPackageType_sk_fk = gpt.idGlassPackageType_sk
--		left join
--			Warehouse.sales.dim_rank_qty_time rqt on ol.qty_time = rqt.idQty_time_sk
--		left join
--			Warehouse.gen.dim_warehouse_v w on ol.idWarehouse_sk_fk = w.idWarehouse_sk
--		left join
--			Warehouse.prod.dim_param_col col on ol.idCOL_sk_fk = col.idCOL_sk
--		 inner join
--			 Warehouse.sales.dim_order_header_v oh on ol.order_id = oh.order_id_bk 
--		 left join
--		 	Warehouse.gen.dim_calendar cal_s -- on ol.idCalendarShipmentDate_sk_fk = cal_s.idCalendar_sk
--		 		on case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.idCalendarShipmentDate_sk_fk, oh.idCalendarRefundDate_sk_fk) else ol.idCalendarShipmentDate_sk_fk end = cal_s.idCalendar_sk
--		 left join
--		 	Warehouse.gen.dim_calendar cal_re on isnull(ol.idCalendarRefundDate_sk_fk, oh.idCalendarRefundDate_sk_fk) = cal_re.idCalendar_sk
--		 left join
--		 	Warehouse.sales.dim_rank_shipping_days rsd on 
--		 		datediff(dd, oh.order_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end) = rsd.idShipping_Days_sk

--select top 1000 *
--from Warehouse.sales.fact_order_line_refund_v

--	select top 1000 count(*)
--	from Warehouse.sales.fact_order_line_refund_v

--------------------------------------------

--select top 1000 *
--from Warehouse.sales.fact_order_line_adj

--	select top 1000 count(*)
--	from Warehouse.sales.fact_order_line_adj

--select top 1000 *
--from Warehouse.sales.fact_order_line_adj_v

--	select top 1000 count(*)
--	from Warehouse.sales.fact_order_line_adj_v

--------------------------------------------
--------------------------------------------

--select transaction_type, idOrderLine_sk
--into #ol_trans
--from
--	(select 'O' transaction_type, idOrderLine_sk
--	from Warehouse.sales.fact_order_line_v
--	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
--	union
--	select 'C' transaction_type, idOrderLine_sk
--	from Warehouse.sales.fact_order_line_refund_v
--	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
--	union
--	select 'A' transaction_type, idOrderLine_sk
--	from Warehouse.sales.fact_order_line_adj_v
--	where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)) t

select top 1000 *
from Warehouse.sales.fact_order_line_all_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
order by idOrderLine_sk, transaction_type

select top 1000 *
from Warehouse.sales.fact_order_line_trans_tr_v
where idOrderLine_sk in (17289447, 17289448, 17744510, 17744511, 20476160, 20476161)
order by idOrderLine_sk, transaction_type

select olt.transaction_type, oh.idOrderHeader_sk, ol.idOrderLine_sk, 
	ol.order_line_id_bk, case when (olt.transaction_type = 'O') then ol.order_line_id_bk else null end order_line_id_bk_c, 
	oh.order_id_bk, case when (olt.transaction_type = 'O') then oh.order_id_bk else null end order_id_bk_c, 
	ol.invoice_id, ol.shipment_id, isnull(ol.creditmemo_id, oh.creditmemo_id) creditmemo_id,
	oh.order_no, ol.invoice_no, ol.shipment_no, isnull(ol.creditmemo_no, oh.creditmemo_no) creditmemo_no,
 	oh.order_date, oh.order_date_c, oh.order_week_day, oh.order_time_name, oh.order_day_part,
	
	case 
		when (olt.transaction_type = 'O') then ol.invoice_date
		when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then isnull(ol.refund_date, oh.refund_date) else null end
		when (olt.transaction_type = 'A') then case when (ol.invoice_date is not null) then ola.invoice_posted_date else null end
	end invoice_date, 
	case 
		when (olt.transaction_type = 'O') then cal_i.calendar_date
		when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then cal_re.calendar_date else null end
		when (olt.transaction_type = 'A') then case when (ol.invoice_date is not null) then cal_ai.calendar_date else null end
	end invoice_date_c, 
	case 
		when (olt.transaction_type = 'O') then cal_i.calendar_date_week_name
		when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then cal_re.calendar_date_week_name else null end
		when (olt.transaction_type = 'A') then case when (ol.invoice_date is not null) then cal_ai.calendar_date_week_name else null end -- need invoice_posted_week_day
	end invoice_week_day,	

	case 
		when (olt.transaction_type = 'O') then ol.shipment_date_r
		when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then isnull(ol.refund_date, oh.refund_date) else null end
		when (olt.transaction_type = 'A') then case when (ol.shipment_date_r is not null) then ola.invoice_posted_date else null end
	end shipment_date, 
	case 
		when (olt.transaction_type = 'O') then cal_s.calendar_date
		when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then cal_re.calendar_date else null end
		when (olt.transaction_type = 'A') then case when (ol.shipment_date_r is not null) then cal_ai.calendar_date else null end
	end shipment_date_c, 
	case 
		when (olt.transaction_type = 'O') then cal_s.calendar_date_week_name
		when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then cal_re.calendar_date_week_name else null end
		when (olt.transaction_type = 'A') then case when (ol.shipment_date_r is not null) then cal_ai.calendar_date_week_name else null end -- need invoice_posted_week_day
	end shipment_week_day, 
	case when (olt.transaction_type = 'O') then tim_s.time_name else null end shipment_time_name, case when (olt.transaction_type = 'O') then tim_s.day_part else null end shipment_day_part,

	isnull(ol.refund_date, oh.refund_date) refund_date, cal_re.calendar_date refund_date_c, cal_re.calendar_date_week_name refund_week_day, 
		
	rsd.rank_shipping_days, 
	datediff(dd, oh.order_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end) shipping_days,

	oh.acquired, oh.tld, oh.website_group, oh.website, oh.store_name, 
	oh.company_name_create, oh.website_group_create, oh.website_create,
	oh.customer_origin_name, oh.current_customer_status_name,
	oh.market_name,
	oh.customer_id, case when (olt.transaction_type = 'O') then oh.customer_id else null end customer_id_c, 
	oh.customer_email, oh.customer_name,
	oh.country_zone_ship, oh.country_continent_ship, oh.country_code_ship, oh.country_name_ship, oh.region_name_ship, oh.postcode_shipping, 
	oh.country_code_bill, oh.country_name_bill, oh.postcode_billing, 
	oh.customer_unsubscribe_name,
	oh.order_stage_name, oh.order_status_name, o_stat.order_status_name line_status_name, oh.adjustment_order, oh.order_type_name, oh.order_status_magento_name, 
	oh.payment_method_name, oh.cc_type_name, oh.shipping_carrier_name, oh.shipping_method_name, oh.telesales_username, oh.prescription_method_name,
	oh.reminder_type_name, oh.reminder_period_name, oh.reminder_date, oh.reorder_f, oh.reorder_date, 
	oh.channel_name, oh.marketing_channel_name, oh.group_coupon_code_name, oh.coupon_code,
	oh.reimbursement_type_name, oh.reimburser_name,
	oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, oh.rank_seq_no_gen, oh.customer_order_seq_no_gen,
	oh.order_source, oh.proforma, oh.platform,
	oh.product_type_oh_name, oh.order_qty_time, oh.num_diff_product_type_oh,

	pf.manufacturer_name, pf.brand_name,
	pf.product_type_name, pf.category_name, pf.product_family_group_name, pf.cl_type_name, pf.cl_feature_name, 
	pf.product_id_magento, pf.magento_sku product_family_code, pf.product_family_name, 

	ol.idBC_sk_fk base_curve, ol.idDI_sk_fk diameter, ol.idPO_sk_fk power, ol.idCY_sk_fk cylinder, ol.idAX_sk_fk axis, ol.idAD_sk_fk addition, ol.idDO_sk_fk dominance, col.colour_name colour, -- ol.idCOL_sk_fk colour, 
	ol.eye,
	gvt.glass_vision_type_name, gpt.glass_package_type_name, 
	ol.sku_magento, ol.sku_erp, 
	ol.aura_product_f,

	w.warehouse_name, 

	ol.countries_registered_code, ol.product_type_vat,

	ol.pack_size,
	case 
		when (olt.transaction_type = 'O') then ol.qty_unit
		when (olt.transaction_type = 'C') then ol.qty_unit_refunded * -1
		when (olt.transaction_type = 'A') then 0
	end qty_unit,	
	case 
		when (olt.transaction_type = 'O') then ol.qty_pack
		when (olt.transaction_type = 'C') then convert(int, (ol.qty_unit_refunded * -1) / (ol.qty_unit / ol.qty_pack))
		when (olt.transaction_type = 'A') then 0
	end qty_pack, 
	rqt.rank_qty_time, ol.qty_time, 
	case 
		when (olt.transaction_type = 'O') then ol.weight
		when (olt.transaction_type = 'C') then ol.weight * -1 
		when (olt.transaction_type = 'A') then 0
	end weight,

	prt.price_type_name, oh.discount_f,
	ol.local_price_unit, convert(decimal(12, 4), (ol.local_subtotal - ol.local_discount) / ol.qty_unit) local_price_unit_discount,
	ol.local_price_pack, ol.local_price_pack_discount, 
	convert(decimal(12, 4), ol.local_price_unit * ol.local_to_global_rate) global_price_unit, 
	convert(decimal(12, 4), ((ol.local_subtotal - ol.local_discount) / ol.qty_unit) * ol.local_to_global_rate) global_price_unit_discount,
	convert(decimal(12, 4), ol.local_price_pack * ol.local_to_global_rate) global_price_pack, 
	convert(decimal(12, 4), ol.local_price_pack_discount * ol.local_to_global_rate) global_price_pack_discount, 

	case 
		when (olt.transaction_type = 'O') then ol.local_subtotal
		when (olt.transaction_type = 'C') then ol.local_subtotal_m
		when (olt.transaction_type = 'A') then 0
	end local_subtotal, 
	case 
		when (olt.transaction_type = 'O') then ol.local_shipping
		when (olt.transaction_type = 'C') then ol.local_shipping_m
		when (olt.transaction_type = 'A') then 0
	end local_shipping, 
	case 
		when (olt.transaction_type = 'O') then ol.local_discount * -1 
		when (olt.transaction_type = 'C') then ol.local_discount_m
		when (olt.transaction_type = 'A') then 0
	end local_discount, 
	case 
		when (olt.transaction_type = 'O') then ol.local_store_credit_used * -1
		when (olt.transaction_type = 'C') then ol.local_store_credit_given
		when (olt.transaction_type = 'A') then 0
	end local_store_credit, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_inc_vat
		when (olt.transaction_type = 'C') then ol.local_total_inc_vat_m
		when (olt.transaction_type = 'A') then 0
	end local_total_inc_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_exc_vat
		when (olt.transaction_type = 'C') then ol.local_total_exc_vat_m
		when (olt.transaction_type = 'A') then 0
	end local_total_exc_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_vat
		when (olt.transaction_type = 'C') then ol.local_total_vat_m
		when (olt.transaction_type = 'A') then 0
	end local_total_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_prof_fee
		when (olt.transaction_type = 'C') then ol.local_total_prof_fee_m
		when (olt.transaction_type = 'A') then 0
	end local_total_prof_fee, 

	case 
		when (olt.transaction_type = 'O') then ol.global_subtotal
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_subtotal_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_subtotal, 
	case 
		when (olt.transaction_type = 'O') then ol.global_shipping
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_shipping_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_shipping, 
	case 
		when (olt.transaction_type = 'O') then ol.global_discount * -1 
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_discount_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_discount, 
	case 
		when (olt.transaction_type = 'O') then ol.global_store_credit_used * -1 
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.global_store_credit_given * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_store_credit, 
	case 
		when (olt.transaction_type = 'O') then ol.global_total_inc_vat
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_inc_vat_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_total_inc_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.global_total_exc_vat
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_exc_vat_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_total_exc_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.global_total_vat
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_vat_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_total_vat, 
	case 
		when (olt.transaction_type = 'O') then ol.global_total_prof_fee
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_prof_fee_m * ol.local_to_global_rate)
		when (olt.transaction_type = 'A') then 0
	end global_total_prof_fee, 

	case 
		when (olt.transaction_type = 'O') then ol.local_total_inc_vat_vf
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_inc_vat_vf_m)
		when (olt.transaction_type = 'A') then 0
	end local_total_inc_vat_vf, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_exc_vat_vf
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_exc_vat_vf_m)
		when (olt.transaction_type = 'A') then 0
	end local_total_exc_vat_vf, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_vat_vf
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_vat_vf_m)
		when (olt.transaction_type = 'A') then 0
	end local_total_vat_vf, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_prof_fee_vf
		when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_prof_fee_vf_m)
		when (olt.transaction_type = 'A') then 0
	end local_total_prof_fee_vf,

	case 
		when (olt.transaction_type = 'O') then ol.local_payment_online
		when (olt.transaction_type = 'C') then ol.local_payment_refund_online * -1 
		when (olt.transaction_type = 'A') then 0
	end local_payment_online, 
	case 
		when (olt.transaction_type = 'O') then 0 
		when (olt.transaction_type = 'C') then (ol.local_bank_online_given - ol.local_payment_refund_online) * -1 
		when (olt.transaction_type = 'A') then 0
	end local_payment_offline, 
	case 
		when (olt.transaction_type = 'O') then ol.local_reimbursement
		when (olt.transaction_type = 'C') then ol.local_reimbursement * -1 
		when (olt.transaction_type = 'A') then 0
	end local_reimbursement,
	case 
		when (olt.transaction_type = 'O') then ol.global_payment_online
		when (olt.transaction_type = 'C') then ol.global_payment_refund_online * -1 
		when (olt.transaction_type = 'A') then 0
	end global_payment_online, 
	case 
		when (olt.transaction_type = 'O') then 0 
		when (olt.transaction_type = 'C') then (ol.global_bank_online_given - ol.global_payment_refund_online) * -1 
		when (olt.transaction_type = 'A') then 0
	end global_payment_offline, 
	case 
		when (olt.transaction_type = 'O') then ol.global_reimbursement
		when (olt.transaction_type = 'C') then ol.global_reimbursement * -1 
		when (olt.transaction_type = 'A') then 0
	end global_reimbursement,

	case 
		when (olt.transaction_type = 'O') then ol.local_product_cost_discount 
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then ola.local_product_cost_discount
	end local_product_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.local_shipping_cost
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then 0 -- no values yet
	end local_shipping_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.local_freight_cost
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then 0 -- no values yet
	end local_freight_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.local_total_cost_discount 
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then ola.local_total_cost_discount
	end local_total_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.global_product_cost_discount 
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then convert(decimal(12, 4), ola.local_product_cost_discount * ol.local_to_global_rate)
	end global_product_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.global_shipping_cost
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then 0 -- no values yet
	end global_shipping_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.global_freight_cost
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then 0 -- no values yet
	end global_freight_cost, 
	case 
		when (olt.transaction_type = 'O') then ol.global_total_cost_discount 
		when (olt.transaction_type = 'C') then 0
		when (olt.transaction_type = 'A') then convert(decimal(12, 4), ola.local_total_cost_discount * ol.local_to_global_rate)
	end global_total_cost, 

	ol.local_to_global_rate, ol.order_currency_code, 
	ol.discount_percent, ol.vat_percent/100 vat_percent, ol.prof_fee_percent/100 prof_fee_percent, ol.vat_rate, ol.prof_fee_rate,
	ol.num_erp_allocation_lines, 
	ol.idETLBatchRun_ins, ol.idETLBatchRun_upd
from 
		#ol_trans olt
	inner join
		(select *, 
			case when (qty_unit_refunded <> 0 and local_store_credit_given <> 0) then isnull(shipment_date, refund_date) else shipment_date end shipment_date_r, 

			convert(decimal(12, 4), local_subtotal * local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping * local_to_global_rate) global_shipping, 
			convert(decimal(12, 4), local_discount * local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used * local_to_global_rate) global_store_credit_used, 
			convert(decimal(12, 4), local_total_inc_vat * local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat * local_to_global_rate) global_total_exc_vat, 
			convert(decimal(12, 4), local_total_vat * local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee * local_to_global_rate) global_total_prof_fee, 
			convert(decimal(12, 4), local_store_credit_given * local_to_global_rate) global_store_credit_given, convert(decimal(12, 4), local_bank_online_given * local_to_global_rate) global_bank_online_given,

			convert(decimal(12, 4), local_payment_online * local_to_global_rate) global_payment_online, convert(decimal(12, 4), local_payment_refund_online * local_to_global_rate) global_payment_refund_online, 
			convert(decimal(12, 4), local_reimbursement * local_to_global_rate) global_reimbursement, 

			convert(decimal(12, 4), local_product_cost * local_to_global_rate) global_product_cost, convert(decimal(12, 4), local_shipping_cost * local_to_global_rate) global_shipping_cost, 
			convert(decimal(12, 4), local_freight_cost * local_to_global_rate) global_freight_cost, convert(decimal(12, 4), local_total_cost * local_to_global_rate) global_total_cost, -- convert(decimal(12, 4), ol.local_margin * ol.local_to_global_rate) global_margin, 
			convert(decimal(12, 4), local_product_cost_discount * local_to_global_rate) global_product_cost_discount, convert(decimal(12, 4), local_total_cost_discount * local_to_global_rate) global_total_cost_discount, 

			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund local_discount_m, 
			local_store_credit_used_refund local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

			local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
			(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
			local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
		from
			(select ol.*,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
				local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
					+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
			from
				(select ol.*, 
					isnull(olvf.local_total_inc_vat_vf, ol.local_total_inc_vat) local_total_inc_vat_vf, isnull(olvf.local_total_exc_vat_vf, ol.local_total_exc_vat) local_total_exc_vat_vf,
					isnull(olvf.local_total_vat_vf, ol.local_total_vat) local_total_vat_vf, isnull(olvf.local_total_prof_fee_vf, ol.local_total_prof_fee) local_total_prof_fee_vf,
					isnull(olvf.local_store_credit_given_vf, ol.local_store_credit_given) local_store_credit_given_vf, isnull(olvf.local_bank_online_given_vf, ol.local_bank_online_given) local_bank_online_given_vf,
					isnull(olvf.local_subtotal_refund_vf, ol.local_subtotal_refund) local_subtotal_refund_vf, isnull(olvf.local_shipping_refund_vf, ol.local_shipping_refund) local_shipping_refund_vf, 
					isnull(olvf.local_discount_refund_vf, ol.local_discount_refund) local_discount_refund_vf, isnull(olvf.local_store_credit_used_refund_vf, ol.local_store_credit_used_refund) local_store_credit_used_refund_vf, 
					isnull(olvf.local_adjustment_refund_vf, ol.local_adjustment_refund) local_adjustment_refund_vf

				from 
						Warehouse.sales.fact_order_line ol
					left join
						Warehouse.sales.fact_order_line_vat_fix olvf on ol.idOrderLine_sk = olvf.idOrderLine_sk_fk) ol) ol) ol on olt.idOrderLine_sk = ol.idOrderLine_sk
	left join
		Warehouse.sales.fact_order_line_adj ola on ol.idOrderLine_sk = ola.idOrderLine_sk_fk and olt.transaction_type = 'A'	 
	left join
		Warehouse.gen.dim_calendar cal_ai on ola.idCalendarInvoicePostedDate_sk_fk = cal_ai.idCalendar_sk
	inner join
		Warehouse.sales.dim_order_header_v oh on ol.order_id = oh.order_id_bk 
	left join
		Warehouse.sales.fact_order_line_vat_fix olvf on ol.idOrderLine_sk = olvf.idOrderLine_sk_fk
	left join
		Warehouse.gen.dim_calendar cal_i on ol.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
	left join
		Warehouse.gen.dim_calendar cal_s -- on ol.idCalendarShipmentDate_sk_fk = cal_s.idCalendar_sk
			on case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.idCalendarShipmentDate_sk_fk, oh.idCalendarRefundDate_sk_fk) else ol.idCalendarShipmentDate_sk_fk end = cal_s.idCalendar_sk
	left join	
		Warehouse.gen.dim_time tim_s on ol.idTimeShipmentDate_sk_fk = tim_s.idTime_sk
	left join
		Warehouse.gen.dim_calendar cal_re on isnull(ol.idCalendarRefundDate_sk_fk, oh.idCalendarRefundDate_sk_fk) = cal_re.idCalendar_sk
	inner join
		Warehouse.sales.dim_order_status o_stat on ol.idOrderStatus_line_sk_fk = o_stat.idOrderStatus_sk
	inner join
		Warehouse.sales.dim_price_type prt on ol.idPriceType_sk_fk = prt.idPriceType_sk	
	inner join
		Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
	left join
		Warehouse.prod.dim_glass_vision_type gvt on ol.idGlassVisionType_sk_fk = gvt.idGlassVisionType_sk
	left join
		Warehouse.prod.dim_glass_package_type gpt on ol.idGlassPackageType_sk_fk = gpt.idGlassPackageType_sk
	left join
		Warehouse.sales.dim_rank_qty_time rqt on ol.qty_time = rqt.idQty_time_sk
	left join
		Warehouse.sales.dim_rank_shipping_days rsd on 
			datediff(dd, oh.order_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end) = rsd.idShipping_Days_sk
	left join
		Warehouse.gen.dim_warehouse_v w on ol.idWarehouse_sk_fk = w.idWarehouse_sk
	left join
		Warehouse.prod.dim_param_col col on ol.idCOL_sk_fk = col.idCOL_sk
order by olt.idOrderLine_sk, olt.transaction_type


---------------------------------------------------------

	select -- count(*) over (), 
		olt.transaction_type, olt.idOrderLine_sk, isnull(ola.idOrderLineAdj_sk, -1) idOrderLineAdj_sk,
		oh.order_date,
		case 
			when (olt.transaction_type = 'O') then ol.invoice_date
			when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then isnull(ol.refund_date, oh.refund_date) else null end
			when (olt.transaction_type = 'A') then case when (ol.invoice_date is not null) then ola.invoice_posted_date else null end
		end invoice_date,  

		case 
			when (olt.transaction_type = 'O') then case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end
			when (olt.transaction_type = 'C') then case when (case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end is not null) then isnull(ol.refund_date, oh.refund_date) else null end
			when (olt.transaction_type = 'A') then case when (case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end is not null) then ola.invoice_posted_date else null end
		end shipment_date
	into #a
	from
			(select 'O' transaction_type, idOrderLine_sk
			from Warehouse.sales.fact_order_line
			union
			select 'C' transaction_type, idOrderLine_sk_fk idOrderLine_sk
			from Warehouse.sales.fact_order_line_refund
			union
			select 'A' transaction_type, idOrderLine_sk_fk idOrderLine_sk
			from Warehouse.sales.fact_order_line_adj) olt
		inner join
			Warehouse.sales.fact_order_line ol on olt.idOrderLine_sk = ol.idOrderLine_sk
		left join
			Warehouse.sales.fact_order_line_adj ola on ol.idOrderLine_sk = ola.idOrderLine_sk_fk and olt.transaction_type = 'A'
		inner join
			Warehouse.sales.dim_order_header oh on ol.order_id = oh.order_id_bk 