
select top 1000 *
from Warehouse.alloc.fact_order_line_mag_erp
-- where snap_complete_date is not null and (qty_percentage is null or qty_percentage = 0)

-- where deduplicate_f = 'N' and qty_percentage not in (0, 1)
where deduplicate_f = 'Y' and qty_percentage = 0.22222222
order by idOrderLine_sk_fk desc

-- Why NULL: Still not issued - qty_unit_erp = NULL OR Error
-- Why 0: Still not allocated - qty_unit_mag = 0
select top 1000 deduplicate_f, qty_percentage, count(*)
from Warehouse.alloc.fact_order_line_mag_erp
where snap_complete_date is not null
-- where snap_complete_date is null and cage_shipment_date is not null
-- where snap_complete_date is null and cage_shipment_date is null
group by deduplicate_f, qty_percentage
order by deduplicate_f, qty_percentage

-- Not matching qty_percentage
select top 1000 *
from
	(select sum(qty_percentage) over (partition by idOrderLineERP_sk_fk) sum_qty_percentage, 
		*
	from Warehouse.alloc.fact_order_line_mag_erp) olme
where sum_qty_percentage <> 1 and sum_qty_percentage <> 0
order by sum_qty_percentage desc, idOrderLineERP_sk_fk


