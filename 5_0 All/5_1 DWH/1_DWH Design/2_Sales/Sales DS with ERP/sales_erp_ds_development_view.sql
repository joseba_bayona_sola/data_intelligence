
drop table #fact_order_line_erp_issue_all

create table #fact_order_line_erp_issue_all(
	transaction_type					char(1) NOT NULL,
	idOrderLineERPIssue_sk				int NOT NULL,
	idOrderLineERP_sk					int NOT NULL, 
	idOrderLine_sk						int NOT NULL, 

	deduplicate_f						char(1));

alter table #fact_order_line_erp_issue_all add constraint [PK_fact_order_line_erp_issue_all]
	primary key clustered (transaction_type, idOrderLineERPIssue_sk, idOrderLineERP_sk, idOrderLine_sk);
go

	select count(*)
	from #fact_order_line_erp_issue_all
	where idOrderLine_sk <> -1

	select transaction_type, count(*)
	from #fact_order_line_erp_issue_all
	where idOrderLine_sk <> -1
	group by transaction_type
	order by transaction_type

select top 1000 count(*) over (), *, 
	count(*) over (partition by transaction_type, idOrderLineERP_sk) num_rep1, 
	count(*) over (partition by transaction_type, idOrderLineERP_sk, idOrderLineERPIssue_sk) num_rep2
from #fact_order_line_erp_issue_all oli
-- where idOrderLineERP_sk <> -1
where idOrderLineERP_sk <> -1 and idOrderLineERPIssue_sk = -1
order by num_rep2 desc, num_rep1 desc

---------------------------------------------------------------
--------------------------------------------------------------- INSERT
---------------------------------------------------------------

select top 1000 erp_f, count(*)
from Warehouse.sales.fact_order_line_all
group by erp_f
order by erp_f

select count(*)
from Warehouse.alloc.dim_order_header_erp ohe

select count(*)
from Warehouse.alloc.fact_order_line_erp

select count(*)
from Warehouse.alloc.fact_order_line_mag_erp

select count(*)
from Warehouse.alloc.fact_order_line_erp_issue 

-----------------------------


select top 1000 count(*) over (), *
from Warehouse.sales.fact_order_line_all
where transaction_type in ('O', 'C') and -- in ('O', 'C')
	(order_date between '2019-09-01' and '2019-10-01' or invoice_date between '2019-09-01' and '2019-10-01' or shipment_date between '2019-09-01' and '2019-10-01') 

	select top 1000 count(*)
	from Warehouse.sales.fact_order_line_all
	where transaction_type in ('O', 'C') and
		(order_date between '2019-09-01' and '2019-10-01' or invoice_date between '2019-09-01' and '2019-10-01' or shipment_date between '2019-09-01' and '2019-10-01') 

select top 10000 -- count(*) over (), 
	ol.transaction_type, ol.idOrderLine_sk, olme.idOrderLineERP_sk_fk, olme.deduplicate_f,
	count(*) over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk) num_rep, 
	dense_rank() over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk order by ol.idOrderLine_sk) ord_rep
from
		(select *
		from Warehouse.sales.fact_order_line_all
		where transaction_type in ('O') and -- in ('O', 'C') ok with C: No duplicates are created
			(order_date between '2019-09-01' and '2019-10-01' or invoice_date between '2019-09-01' and '2019-10-01' or shipment_date between '2019-09-01' and '2019-10-01')) ol
	left join
		Warehouse.alloc.fact_order_line_mag_erp olme on ol.idOrderLine_sk = olme.idOrderLine_sk_fk
-- where ol.idOrderLine_sk = 22988656

-- select 321213 + 42487
-- select 328607 + 43280
select top 1000 count(*) over (),
	ol.transaction_type, ol.idOrderLine_sk, ol.idOrderLineERP_sk, ol.deduplicate_f
from
	(select 
		ol.transaction_type, ol.idOrderLine_sk, olme.idOrderLineERP_sk_fk idOrderLineERP_sk, olme.deduplicate_f,
		count(*) over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk) num_rep, 
		dense_rank() over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk order by ol.idOrderLine_sk) ord_rep
	from
			(select *
			from Warehouse.sales.fact_order_line_all
			where transaction_type in ('O', 'C') and -- in ('O', 'C') ok with C: No duplicates are created
				(order_date between '2019-09-01' and '2019-10-01' or invoice_date between '2019-09-01' and '2019-10-01' or shipment_date between '2019-09-01' and '2019-10-01')) ol
		left join
			Warehouse.alloc.fact_order_line_mag_erp olme on ol.idOrderLine_sk = olme.idOrderLine_sk_fk) ol
-- where (ol.idOrderLineERP_sk is not null and ol.ord_rep = 1) or ol.idOrderLineERP_sk is null
where (ol.idOrderLineERP_sk is not null and ol.ord_rep <> 1) 

-- insert_1
insert into #fact_order_line_erp_issue_all (transaction_type, idOrderLineERPIssue_sk, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f)
	select 
		ol.transaction_type, isnull(oli.idOrderLineERPIssue_sk, -1) idOrderLineERPIssue_sk, isnull(ol.idOrderLineERP_sk, -1) idOrderLineERP_sk, ol.idOrderLine_sk, 
		isnull(ol.deduplicate_f, 'N') deduplicate_f
	from
			(select ol.transaction_type, ol.idOrderLine_sk, ol.idOrderLineERP_sk, ol.deduplicate_f
			from
				(select 
					ol.transaction_type, ol.idOrderLine_sk, olme.idOrderLineERP_sk_fk idOrderLineERP_sk, olme.deduplicate_f,
					count(*) over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk) num_rep, 
					dense_rank() over (partition by ol.transaction_type, olme.idOrderLineERP_sk_fk order by ol.idOrderLine_sk) ord_rep
				from
						(select *
						from Warehouse.sales.fact_order_line_all
						where transaction_type in ('O', 'C') and -- in ('O', 'C') ok with C: No duplicates are created
							(order_date between '2019-09-01' and '2019-10-01' or invoice_date between '2019-09-01' and '2019-10-01' or shipment_date between '2019-09-01' and '2019-10-01')) ol
					left join
						Warehouse.alloc.fact_order_line_mag_erp olme on ol.idOrderLine_sk = olme.idOrderLine_sk_fk) ol
			where (ol.idOrderLineERP_sk is not null and ol.ord_rep = 1) or ol.idOrderLineERP_sk is null) ol
		left join
			Warehouse.alloc.fact_order_line_erp_issue oli on ol.idOrderLineERP_sk = oli.idOrderLineERP_sk_fk
go

-- insert_2
insert into #fact_order_line_erp_issue_all (transaction_type, idOrderLineERPIssue_sk, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f)
	select 'O' transaction_type, 
		isnull(oli.idOrderLineERPIssue_sk, -1) idOrderLineERPIssue_sk, isnull(ole.idOrderLineERP_sk, -1) idOrderLineERP_sk, -1 idOrderLine_sk, 
		'N'deduplicate_f
	from 
			Warehouse.alloc.dim_order_header_erp ohe
		inner join
			Warehouse.alloc.fact_order_line_erp ole on ohe.idOrderHeaderERP_sk = ole.idOrderHeaderERP_sk_fk
		left join
			Warehouse.alloc.fact_order_line_mag_erp olme on ole.idOrderLineERP_sk = olme.idOrderLineERP_sk_fk
			-- #fact_order_line_erp_issue_all olme on ole.idOrderLineERP_sk = olme.idOrderLineERP_sk
		left join
			Warehouse.alloc.fact_order_line_erp_issue oli on ole.idOrderLineERP_sk = oli.idOrderLineERP_sk_fk
	where ohe.order_date_sync between '2019-09-01' and '2019-10-01'
		and olme.idOrderLineERP_sk_fk is null

-- insert_3
insert into #fact_order_line_erp_issue_all (transaction_type, idOrderLineERPIssue_sk, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f)

	select 'A' transaction_type, oli.idOrderLineERPIssue_sk, oli.idOrderLineERP_sk, oli.idOrderLine_sk, oli.deduplicate_f
	from 
			#fact_order_line_erp_issue_all oli
		inner join
			Warehouse.alloc.fact_order_line_erp_issue_adj olia on oli.idOrderLineERPIssue_sk = olia.idOrderLineERPIssue_sk_fk and oli.transaction_type = 'O'

-- DELETE sentences on view (PK is based on all 4 attributes so UPDATE in merge won't happen)

-- All rows vs only incremental (using wrk tables ??)

---------------------------------------------------------------
--------------------------------------------------------------- VIEW
---------------------------------------------------------------

drop table #oli_all
drop table #ol

	select -- top 10000 count(*) over (), 
		oli.transaction_type, oli.idOrderLineERP_sk, oli.idOrderLine_sk idOrderLineDedup_sk, isnull(olme.idOrderLine_sk_fk, oli.idOrderLine_sk) idOrderLine_sk,
		oli.deduplicate_f,
		count(*) over (partition by oli.transaction_type, isnull(olme.idOrderLine_sk_fk, oli.idOrderLine_sk)) num_rep
	into #oli_all
	from 
		(select distinct transaction_type, idOrderLineERP_sk, idOrderLine_sk, deduplicate_f
		from #fact_order_line_erp_issue_all
		where idOrderLine_sk <> -1
			and transaction_type in ('O', 'C')) oli
		left join
			Warehouse.alloc.fact_order_line_mag_erp olme on oli.idOrderLineERP_sk = olme.idOrderLineERP_sk_fk
	order by num_rep desc, idOrderLineDedup_sk, idOrderLine_sk

select *
into #ol
from
	(select 
		olt.transaction_type, olt.idOrderLineERP_sk, olt.idOrderLineDedup_sk, olt.idOrderLine_sk, olt.deduplicate_f,
		case 
			when (olt.transaction_type = 'O') then ol.invoice_date
			when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then isnull(ol.refund_date, oh.refund_date) else null end
		end invoice_date, 
		case 
			when (olt.transaction_type = 'O') then cal_i.calendar_date
			when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then cal_re.calendar_date else null end
		end invoice_date_c, 
		case 
			when (olt.transaction_type = 'O') then cal_i.calendar_date_week_name
			when (olt.transaction_type = 'C') then case when (ol.invoice_date is not null) then cal_re.calendar_date_week_name else null end
		end invoice_week_day,	

		case 
			when (olt.transaction_type = 'O') then ol.shipment_date_r
			when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then isnull(ol.refund_date, oh.refund_date) else null end
		end shipment_date, 
		case 
			when (olt.transaction_type = 'O') then cal_s.calendar_date
			when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then cal_re.calendar_date else null end
		end shipment_date_c, 
		case 
			when (olt.transaction_type = 'O') then cal_s.calendar_date_week_name
			when (olt.transaction_type = 'C') then case when (ol.shipment_date_r is not null) then cal_re.calendar_date_week_name else null end
		end shipment_week_day, 
		
		sum(case 
			when (olt.transaction_type = 'O') then ol.qty_unit
			when (olt.transaction_type = 'C') then ol.qty_unit_refunded * -1
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) qty_unit_sum,
		sum(case 
			when (olt.transaction_type = 'O') then ol.qty_pack
			when (olt.transaction_type = 'C') then convert(int, (ol.qty_unit_refunded * -1) / (ol.qty_unit / ol.qty_pack))
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) qty_pack, 
		
		sum(case 
			when (olt.transaction_type = 'O') then ol.weight
			when (olt.transaction_type = 'C') then ol.weight * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) weight,
		
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_subtotal
			when (olt.transaction_type = 'C') then ol.local_subtotal_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_subtotal, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_shipping
			when (olt.transaction_type = 'C') then ol.local_shipping_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_shipping, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_discount * -1 
			when (olt.transaction_type = 'C') then ol.local_discount_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_discount, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_store_credit_used * -1
			when (olt.transaction_type = 'C') then ol.local_store_credit_given
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_store_credit, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_inc_vat
			when (olt.transaction_type = 'C') then ol.local_total_inc_vat_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_inc_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_exc_vat
			when (olt.transaction_type = 'C') then ol.local_total_exc_vat_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_exc_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_vat
			when (olt.transaction_type = 'C') then ol.local_total_vat_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_prof_fee
			when (olt.transaction_type = 'C') then ol.local_total_prof_fee_m
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_prof_fee, 

		sum(case 
			when (olt.transaction_type = 'O') then ol.global_subtotal
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_subtotal_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_subtotal, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_shipping
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_shipping_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_shipping, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_discount * -1 
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_discount_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_discount, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_store_credit_used * -1 
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.global_store_credit_given * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_store_credit, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_total_inc_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_inc_vat_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_total_inc_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_total_exc_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_exc_vat_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_total_exc_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_total_vat
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_vat_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_total_vat, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_total_prof_fee
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_prof_fee_m * ol.local_to_global_rate)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_total_prof_fee, 

		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_inc_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_inc_vat_vf_m)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_inc_vat_vf, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_exc_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_exc_vat_vf_m)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_exc_vat_vf, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_vat_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_vat_vf_m)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_vat_vf, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_total_prof_fee_vf
			when (olt.transaction_type = 'C') then convert(decimal(12, 4), ol.local_total_prof_fee_vf_m)
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_prof_fee_vf,

		sum(case 
			when (olt.transaction_type = 'O') then ol.local_payment_online
			when (olt.transaction_type = 'C') then ol.local_payment_refund_online * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_payment_online, 
		sum(case 
			when (olt.transaction_type = 'O') then 0 
			when (olt.transaction_type = 'C') then (ol.local_bank_online_given - ol.local_payment_refund_online) * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_payment_offline, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.local_reimbursement
			when (olt.transaction_type = 'C') then ol.local_reimbursement * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_reimbursement,
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_payment_online
			when (olt.transaction_type = 'C') then ol.global_payment_refund_online * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_payment_online, 
		sum(case 
			when (olt.transaction_type = 'O') then 0 
			when (olt.transaction_type = 'C') then (ol.global_bank_online_given - ol.global_payment_refund_online) * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_payment_offline, 
		sum(case 
			when (olt.transaction_type = 'O') then ol.global_reimbursement
			when (olt.transaction_type = 'C') then ol.global_reimbursement * -1 
		end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_reimbursement

		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.local_product_cost_discount 
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_product_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.local_shipping_cost
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_shipping_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.local_freight_cost
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_freight_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.local_total_cost_discount 
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) local_total_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.global_product_cost_discount 
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_product_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.global_shipping_cost
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_shipping_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.global_freight_cost
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_freight_cost, 
		--sum(case 
		--	when (olt.transaction_type = 'O') then ol.global_total_cost_discount 
		--	when (olt.transaction_type = 'C') then 0
		--end) over (partition by olt.transaction_type, olt.idOrderLineDedup_sk) global_total_cost
	from 
			#oli_all olt
		inner join
			(select *, 
				case when (qty_unit_refunded <> 0 and local_store_credit_given <> 0) then isnull(shipment_date, refund_date) else shipment_date end shipment_date_r, 

				convert(decimal(12, 4), local_subtotal * local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping * local_to_global_rate) global_shipping, 
				convert(decimal(12, 4), local_discount * local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used * local_to_global_rate) global_store_credit_used, 
				convert(decimal(12, 4), local_total_inc_vat * local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat * local_to_global_rate) global_total_exc_vat, 
				convert(decimal(12, 4), local_total_vat * local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee * local_to_global_rate) global_total_prof_fee, 
				convert(decimal(12, 4), local_store_credit_given * local_to_global_rate) global_store_credit_given, convert(decimal(12, 4), local_bank_online_given * local_to_global_rate) global_bank_online_given,

				convert(decimal(12, 4), local_payment_online * local_to_global_rate) global_payment_online, convert(decimal(12, 4), local_payment_refund_online * local_to_global_rate) global_payment_refund_online, 
				convert(decimal(12, 4), local_reimbursement * local_to_global_rate) global_reimbursement, 

				convert(decimal(12, 4), local_product_cost * local_to_global_rate) global_product_cost, convert(decimal(12, 4), local_shipping_cost * local_to_global_rate) global_shipping_cost, 
				convert(decimal(12, 4), local_freight_cost * local_to_global_rate) global_freight_cost, convert(decimal(12, 4), local_total_cost * local_to_global_rate) global_total_cost, -- convert(decimal(12, 4), ol.local_margin * ol.local_to_global_rate) global_margin, 
				convert(decimal(12, 4), local_product_cost_discount * local_to_global_rate) global_product_cost_discount, convert(decimal(12, 4), local_total_cost_discount * local_to_global_rate) global_total_cost_discount, 

				local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund local_discount_m, 
				local_store_credit_used_refund local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

				local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
				(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
				local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

				local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
				(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
				local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
			from
				(select ol.*,
					local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
						+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
					local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
						+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
				from
					(select ol.*, 
						isnull(olvf.local_total_inc_vat_vf, ol.local_total_inc_vat) local_total_inc_vat_vf, isnull(olvf.local_total_exc_vat_vf, ol.local_total_exc_vat) local_total_exc_vat_vf,
						isnull(olvf.local_total_vat_vf, ol.local_total_vat) local_total_vat_vf, isnull(olvf.local_total_prof_fee_vf, ol.local_total_prof_fee) local_total_prof_fee_vf,
						isnull(olvf.local_store_credit_given_vf, ol.local_store_credit_given) local_store_credit_given_vf, isnull(olvf.local_bank_online_given_vf, ol.local_bank_online_given) local_bank_online_given_vf,
						isnull(olvf.local_subtotal_refund_vf, ol.local_subtotal_refund) local_subtotal_refund_vf, isnull(olvf.local_shipping_refund_vf, ol.local_shipping_refund) local_shipping_refund_vf, 
						isnull(olvf.local_discount_refund_vf, ol.local_discount_refund) local_discount_refund_vf, isnull(olvf.local_store_credit_used_refund_vf, ol.local_store_credit_used_refund) local_store_credit_used_refund_vf, 
						isnull(olvf.local_adjustment_refund_vf, ol.local_adjustment_refund) local_adjustment_refund_vf

					from 
							Warehouse.sales.fact_order_line ol
						left join
							Warehouse.sales.fact_order_line_vat_fix olvf on ol.idOrderLine_sk = olvf.idOrderLine_sk_fk) ol) ol) ol on olt.idOrderLine_sk = ol.idOrderLine_sk
		inner join
			Warehouse.sales.dim_order_header oh on ol.order_id = oh.order_id_bk 
		left join
			Warehouse.gen.dim_calendar cal_i on ol.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_s -- on ol.idCalendarShipmentDate_sk_fk = cal_s.idCalendar_sk
				on case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.idCalendarShipmentDate_sk_fk, oh.idCalendarRefundDate_sk_fk) else ol.idCalendarShipmentDate_sk_fk end = cal_s.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_re on isnull(ol.idCalendarRefundDate_sk_fk, oh.idCalendarRefundDate_sk_fk) = cal_re.idCalendar_sk) olt
where idOrderLineDedup_sk = idOrderLine_sk
order by olt.idOrderLineDedup_sk, olt.idOrderLine_sk

	select top 1000 count(*) over (), *
	from #ol
	-- where deduplicate_f = 'Y' and transaction_type = 'C'
	where idOrderLine_sk = 23026893
	order by idOrderLine_sk desc

	select transaction_type, year(invoice_date), month(invoice_date), day(invoice_date), sum(global_total_exc_vat)
	from 
		(select *, invoice_date calc_date
		from #ol) ol
	group by transaction_type, year(invoice_date), month(invoice_date), day(invoice_date)
	order by transaction_type, year(invoice_date), month(invoice_date), day(invoice_date)

	select year(calc_date), month(calc_date), sum(global_total_exc_vat)
	from 
		(select *, invoice_date calc_date
		from #ol) ol
	group by year(calc_date), month(calc_date)
	order by year(calc_date), month(calc_date)
