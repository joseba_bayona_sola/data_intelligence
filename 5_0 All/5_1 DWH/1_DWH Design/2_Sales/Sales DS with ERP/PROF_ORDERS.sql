
-- where order_id_bk = 8799014 -- OL_MAG_ERP: Null on qty_unit_erp on OL_MAG_ERP
-- where order_id_bk = 8811748 -- OL_MAG_ERP: Deduplicate = 'N' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 7638615 -- OL_MAG_ERP: Deduplicate = 'Y' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8012051 -- OL_MAG_ERP: Deduplicate = 'Y' and with Many Batches
-- where order_id_bk = 8812365 -- OL_MAG_ERP: Deduplicate = 'Y' and BOM = 'Y'
-- where order_id_bk = 8814519 -- OL_ERP: BOM product
-- where order_id_bk = 7263518 -- OL_ERP: BOM product with Many Batches
-- where order_id_bk = 8802263 -- OL_ERP_ISSUE: Many Batches

-------------------------------------------------------

-- OL
select top 1000 idOrderHeader_sk, idOrderLine_sk, order_line_id_bk, order_id_bk, 
	order_no, order_date, website, customer_id, 
	product_id_magento, product_family_name,
	qty_unit, qty_pack, 
	local_subtotal, local_total_inc_vat, local_total_exc_vat, order_currency_code
from Warehouse.sales.fact_order_line_v
-- where order_id_bk = 7263518 -- OL_ERP: BOM product with Many Batches
-- where order_id_bk = 7638615 -- OL_MAG_ERP: Deduplicate = 'Y' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8012051 -- OL_MAG_ERP: Deduplicate = 'Y' and with Many Batches
-- where order_id_bk = 8799014 -- OL_MAG_ERP: Null on qty_unit_erp on OL_MAG_ERP
-- where order_id_bk = 8802263 -- OL_ERP_ISSUE: Many Batches
-- where order_id_bk = 8811748 -- OL_MAG_ERP: Deduplicate = 'N' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8812365 -- OL_MAG_ERP: Deduplicate = 'Y' and BOM = 'Y'
-- where order_id_bk = 8814519 -- OL_ERP: BOM product
where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
order by idOrderLine_sk

-- OL_MAG_ERP

select olm.idOrderLine_sk_fk, ol.order_line_id_bk, ol.order_id_bk, 
	olm.idOrderLineERP_sk_fk, olm.orderlinemagentoid_bk, 
	olm.deduplicate_f, olm.qty_unit_mag, olm.qty_unit_erp, olm.qty_percentage
from 
		Warehouse.alloc.fact_order_line_mag_erp olm
	inner join
		Warehouse.sales.fact_order_line_v ol on olm.idOrderLine_sk_fk = ol.idOrderLine_sk
-- where order_id_bk = 7263518 -- OL_ERP: BOM product with Many Batches
-- where order_id_bk = 7638615 -- OL_MAG_ERP: Deduplicate = 'Y' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8012051 -- OL_MAG_ERP: Deduplicate = 'Y' and with Many Batches
-- where order_id_bk = 8799014 -- OL_MAG_ERP: Null on qty_unit_erp on OL_MAG_ERP
-- where order_id_bk = 8802263 -- OL_ERP_ISSUE: Many Batches
-- where order_id_bk = 8811748 -- OL_MAG_ERP: Deduplicate = 'N' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8812365 -- OL_MAG_ERP: Deduplicate = 'Y' and BOM = 'Y'
-- where order_id_bk = 8814519 -- OL_ERP: BOM product

where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
order by olm.idOrderLine_sk_fk

-- OL_ERP

select top 1000 idOrderHeaderERP_sk, idOrderLineERP_sk, 
	orderlineid_bk, orderid_bk, order_line_id_bk, order_id_bk, 
	order_no_erp, order_date_sync, bom_f, 
	product_id_magento, product_family_name, parameter,
	qty_unit, qty_unit_allocated
from Warehouse.alloc.fact_order_line_erp_v
-- where order_id_bk = 7263518 -- OL_ERP: BOM product with Many Batches
-- where order_id_bk = 7638615 -- OL_MAG_ERP: Deduplicate = 'Y' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8012051 -- OL_MAG_ERP: Deduplicate = 'Y' and with Many Batches
-- where order_id_bk = 8799014 -- OL_MAG_ERP: Null on qty_unit_erp on OL_MAG_ERP
-- where order_id_bk = 8802263 -- OL_ERP_ISSUE: Many Batches
-- where order_id_bk = 8811748 -- OL_MAG_ERP: Deduplicate = 'N' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8812365 -- OL_MAG_ERP: Deduplicate = 'Y' and BOM = 'Y'
-- where order_id_bk = 8814519 -- OL_ERP: BOM product

where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
order by order_id_bk

-- OL_ERP_ISSUE

select top 1000 idOrderHeaderERP_sk, idOrderLineERP_sk, 
	orderlineid_bk, orderid_bk, batchstockissueid_bk, order_line_id_bk, order_id_bk, 
	order_no_erp, order_date_sync, bom_f, 
	product_id_magento, product_family_name, sku, stock_item_description,
	issue_id, batch_id, cancelled_f,
	qty_percentage,
	qty_unit_order_line, qty_unit_allocated_order_line, qty_unit, qty_stock, 
	local_total_unit_cost, local_total_cost
from Warehouse.alloc.fact_order_line_erp_issue_v
-- where order_id_bk = 7263518 -- OL_ERP: BOM product with Many Batches
-- where order_id_bk = 7638615 -- OL_MAG_ERP: Deduplicate = 'Y' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8012051 -- OL_MAG_ERP: Deduplicate = 'Y' and with Many Batches
-- where order_id_bk = 8799014 -- OL_MAG_ERP: Null on qty_unit_erp on OL_MAG_ERP
-- where order_id_bk = 8802263 -- OL_ERP_ISSUE: Many Batches
-- where order_id_bk = 8811748 -- OL_MAG_ERP: Deduplicate = 'N' and qty_percentage <> 1 on OL_MAG_ERP
-- where order_id_bk = 8812365 -- OL_MAG_ERP: Deduplicate = 'Y' and BOM = 'Y'
-- where order_id_bk = 8814519 -- OL_ERP: BOM product

where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
order by order_id_bk, idOrderLineERP_sk


---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------

select top 1000 ol.idOrderHeader_sk, ol.idOrderLine_sk, olm.idOrderLineERP_sk_fk, 
	ol.order_line_id_bk, ol.order_id_bk, 
	ol.order_no, ol.order_date, ol.website, ol.customer_id, 
	ol.product_id_magento, ol.product_family_name,
	ol.qty_unit, ol.qty_pack, 
	ol.local_subtotal, ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_cost, ol.local_total_cost_discount, ol.order_currency_code, 
	olm.deduplicate_f, olm.qty_unit_mag, olm.qty_unit_erp, olm.qty_percentage
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Warehouse.alloc.fact_order_line_mag_erp olm on ol.idOrderLine_sk = olm.idOrderLine_sk_fk 
where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
order by idOrderLine_sk

select top 1000 ol.idOrderHeader_sk, olm.idOrderLineERP_sk_fk, 
	ol.order_id_bk, 
	ol.order_no, ol.order_date, ol.website, ol.customer_id, 
	ol.product_id_magento, ol.product_family_name,
	sum(ol.qty_unit) qty_unit, sum(ol.qty_pack) qty_pack, 
	sum(ol.local_subtotal) local_subtotal, sum(ol.local_total_inc_vat) local_total_inc_vat, sum(ol.local_total_exc_vat) local_total_exc_vat, 
	sum(ol.local_total_cost) local_total_cost, sum(ol.local_total_cost_discount) local_total_cost_discount, 
	ol.order_currency_code, 
	olm.deduplicate_f, sum(olm.qty_unit_mag) qty_unit_mag, olm.qty_unit_erp, sum(olm.qty_percentage) qty_percentage
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Warehouse.alloc.fact_order_line_mag_erp olm on ol.idOrderLine_sk = olm.idOrderLine_sk_fk 
where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
group by ol.idOrderHeader_sk, olm.idOrderLineERP_sk_fk, 
	ol.order_id_bk, 
	ol.order_no, ol.order_date, ol.website, ol.customer_id, 
	ol.product_id_magento, ol.product_family_name,
	ol.order_currency_code, 
	olm.deduplicate_f, olm.qty_unit_erp
order by ol.order_id_bk

select olm.*, 
	ol.qty_unit, ol.qty_unit_allocated, 
	oli.sku, oli.stock_item_description,
	oli.issue_id, oli.batch_id, oli.cancelled_f,
	oli.qty_percentage,
	oli.qty_unit_order_line, oli.qty_unit_allocated_order_line, oli.qty_unit, oli.qty_stock, 
	olm.local_subtotal * oli.qty_percentage local_subtotal_p, olm.local_total_inc_vat * oli.qty_percentage local_total_inc_vat_p, olm.local_total_exc_vat * oli.qty_percentage local_total_exc_vat_p, 
	oli.local_total_unit_cost, oli.local_total_cost
from
		(select top 1000 ol.idOrderHeader_sk, olm.idOrderLineERP_sk_fk, 
			ol.order_id_bk, 
			ol.order_no, ol.order_date, ol.website, ol.customer_id, 
			ol.product_id_magento, ol.product_family_name,
			sum(ol.qty_unit) qty_unit, sum(ol.qty_pack) qty_pack, 
			sum(ol.local_subtotal) local_subtotal, sum(ol.local_total_inc_vat) local_total_inc_vat, sum(ol.local_total_exc_vat) local_total_exc_vat, 
			sum(ol.local_total_cost) local_total_cost, sum(ol.local_total_cost_discount) local_total_cost_discount, 
			ol.order_currency_code, 
			olm.deduplicate_f, sum(olm.qty_unit_mag) qty_unit_mag, olm.qty_unit_erp, sum(olm.qty_percentage) qty_percentage
		from 
				Warehouse.sales.fact_order_line_v ol
			inner join
				Warehouse.alloc.fact_order_line_mag_erp olm on ol.idOrderLine_sk = olm.idOrderLine_sk_fk 
		where order_id_bk in (8799014, 8811748, 7638615, 8012051, 8812365, 8814519, 7263518, 8802263)
		group by ol.idOrderHeader_sk, olm.idOrderLineERP_sk_fk, 
			ol.order_id_bk, 
			ol.order_no, ol.order_date, ol.website, ol.customer_id, 
			ol.product_id_magento, ol.product_family_name,
			ol.order_currency_code, 
			olm.deduplicate_f, olm.qty_unit_erp) olm
	inner join
		Warehouse.alloc.fact_order_line_erp_v ol on olm.idOrderLineERP_sk_fk = ol.idOrderLineERP_sk
	inner join
		Warehouse.alloc.fact_order_line_erp_issue_v oli on olm.idOrderLineERP_sk_fk = oli.idOrderLineERP_sk
order by order_id_bk