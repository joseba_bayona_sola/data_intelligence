
-- review: creditmemo_id, creditmemo_no, rank_shipping_days, shipping_days

select top 1000 
	olit.transaction_type, ol.idOrderHeader_sk, ol.idOrderLine_sk, 
	
	ol.order_line_id_bk, case when (olit.transaction_type = 'O') then ol.order_line_id_bk else null end order_line_id_bk_c, 
	ol.order_id_bk, case when (olit.transaction_type = 'O') then ol.order_id_bk else null end order_id_bk_c, 

	olitv.orderlineid_bk, case when (olit.transaction_type = 'O') then olitv.orderlineid_bk else null end orderlineid_bk_c, 
	olitv.orderid_bk, case when (olit.transaction_type = 'O') then olitv.orderid_bk else null end orderid_bk_c, 
	olitv.batchstockissueid_bk, case when (olit.transaction_type = 'O') then olitv.batchstockissueid_bk else null end batchstockissueid_bk_c, 

	-- ol.invoice_id, ol.shipment_id, ol.creditmemo_id,
	
	isnull(ol.order_no, olitv.order_no_erp) order_no,
	ol.invoice_no, ol.shipment_no, ol.creditmemo_no,

 	ol.order_date, olit.invoice_date, olit.shipment_date, 
	olitv.order_date_sync, olitv.promised_shipment_date, olitv.promised_delivery_date, 
	olitv.cage_shipment_date, olitv.snap_complete_date,
	olitv.allocation_date, 
	case when (olit.transaction_type = 'A') then olitv.invoice_posted_date else olitv.issue_date end issue_date,

	ol.rank_shipping_days, ol.shipping_days,

	ol.acquired, ol.tld, ol.website_group, ol.website, ol.store_name, 
	ol.company_name_create, ol.website_group_create, ol.website_create,
	ol.customer_origin_name, ol.current_customer_status_name,
	ol.market_name,
	ol.customer_id, case when (olit.transaction_type = 'O') then ol.customer_id else null end customer_id_c, 
	ol.customer_email, ol.customer_name,
	ol.country_zone_ship, ol.country_continent_ship, ol.country_code_ship, ol.country_name_ship, ol.region_name_ship, ol.postcode_shipping, 
	ol.country_code_bill, ol.country_name_bill, ol.postcode_billing, 
	ol.customer_unsubscribe_name,
	ol.order_stage_name, ol.order_status_name, ol.line_status_name, ol.adjustment_order, ol.order_type_name, ol.order_status_magento_name, 
	ol.payment_method_name, ol.cc_type_name, ol.shipping_carrier_name, ol.shipping_method_name, ol.telesales_username, ol.prescription_method_name,
	ol.reminder_type_name, ol.reminder_period_name, ol.reminder_date, ol.reorder_f, ol.reorder_date, 
	ol.channel_name, ol.marketing_channel_name, ol.group_coupon_code_name, ol.coupon_code,
	ol.reimbursement_type_name, ol.reimburser_name,
	ol.customer_status_name, ol.rank_seq_no, ol.customer_order_seq_no, ol.rank_seq_no_web, ol.customer_order_seq_no_web, ol.rank_seq_no_gen, ol.customer_order_seq_no_gen,
	ol.order_source, ol.proforma, ol.platform,
	ol.product_type_oh_name, ol.order_qty_time, ol.num_diff_product_type_oh,

	olitv.order_type_erp_f, olitv.order_status_erp_name, olitv.shipment_status_erp_name, olitv.allocation_status_name, olitv.allocation_strategy_name,

	isnull(ol.manufacturer_name, olitv.manufacturer_name) manufacturer_name, isnull(ol.brand_name, olitv.brand_name) brand_name,
	isnull(ol.product_type_name, olitv.product_type_name) product_type_name, isnull(ol.category_name, olitv.category_name) category_name, isnull(ol.product_family_group_name, olitv.product_family_group_name) product_family_group_name, 
	isnull(ol.cl_type_name, olitv.cl_type_name) cl_type_name, isnull(ol.cl_feature_name, olitv.cl_feature_name) cl_feature_name,
	isnull(ol.product_id_magento, olitv.product_id_magento) product_id_magento, isnull(ol.product_family_name, olitv.product_family_name) product_family_name, -- ol.product_family_code, 

	isnull(ol.base_curve, olitv.base_curve) base_curve, isnull(ol.diameter, olitv.diameter) diameter, isnull(ol.power, olitv.power) power, 
	isnull(ol.cylinder, olitv.cylinder) cylinder, isnull(ol.axis, olitv.axis) axis, isnull(ol.addition, olitv.addition) addition, isnull(ol.dominance, olitv.dominance) dominance, 
	isnull(ol.colour, olitv.colour) colour, 
	isnull(ol.sku_erp, olitv.parameter) parameter, 
	ol.eye,

	-- ol.glass_vision_type_name, ol.glass_package_type_name, 
	-- ol.sku_magento, 
	-- ol.aura_product_f,

	olitv.manufacturer_name_I, olitv.brand_name_I, 
	olitv.product_type_name_I, olitv.category_name_I, olitv.product_family_group_name_I, 
	olitv.product_id_magento_I, olitv.product_family_name_I, 
	olitv.SKU, olitv.stock_item_description,

	olitv.warehouse_name_pref, olitv.warehouse_name,

	ol.countries_registered_code, ol.product_type_vat, 

	olitv.bom_f, olitv.inventory_level_success_f,
	olitv.pack_size_optimum_dist, olitv.pack_size_optimum_pack_size, olitv.pack_size_optimum_f,
	olitv.shortage_PO_source, olitv.shortage_PO_type, olitv.shortage_PO_number, 

	olitv.issue_id, olitv.allocation_type_name, olitv.allocation_record_type_name, olitv.cancelled_f, 
	olitv.batch_id, olitv.batch_stock_register_date,
		
	olitv.supplier_name, olitv.wh_shipment_type_name, olitv.receipt_number, 
	olitv.purchase_order_number, olitv.po_source_name, olitv.po_type_name, olitv.created_by, olitv.created_date_po, olitv.confirmed_date_po, olitv.submitted_date_po,

	ol.pack_size pack_size_web, olitv.packsize pack_size_erp, 
	
	olit.qty_unit qty_unit_web, olit.qty_pack qty_pack_web, 
	olitv.qty_unit qty_unit_erp, olitv.qty_stock qty_pack_erp, 

	ol.rank_qty_time, ol.qty_time, 
	
	ol.price_type_name, ol.discount_f,
	ol.local_price_unit, ol.local_price_unit_discount,
	ol.local_price_pack, ol.local_price_pack_discount, 
	ol.global_price_unit, ol.global_price_unit_discount,
	ol.global_price_pack, ol.global_price_pack_discount, 

	-- isnull(olitv.qty_percentage, 1) qty_percentage,

	olit.qty_unit * isnull(olitv.qty_percentage, 1) qty_unit_web_m, olit.qty_pack * isnull(olitv.qty_percentage, 1) qty_pack_web_m, 
	case when (olit.transaction_type = 'A') then 0 else olitv.qty_unit end qty_unit_erp_m, -- C for refunds ??
	case when (olit.transaction_type = 'A') then 0 else olitv.qty_stock end qty_pack_erp_m, -- C for refunds ?? 
	olit.weight * isnull(olitv.qty_percentage, 1) weight, 

	isnull(olit.local_subtotal, olitv.local_subtotal) * isnull(olitv.qty_percentage, 1) local_subtotal, olit.local_shipping * isnull(olitv.qty_percentage, 1) local_shipping, 
		olit.local_discount * isnull(olitv.qty_percentage, 1) local_discount, olit.local_store_credit * isnull(olitv.qty_percentage, 1) local_store_credit, 
	isnull(olit.local_total_inc_vat, olitv.local_subtotal) * isnull(olitv.qty_percentage, 1) local_total_inc_vat, olit.local_total_exc_vat * isnull(olitv.qty_percentage, 1) local_total_exc_vat, 
		olit.local_total_vat * isnull(olitv.qty_percentage, 1) local_total_vat, olit.local_total_prof_fee * isnull(olitv.qty_percentage, 1) local_total_prof_fee, 

	olit.global_subtotal * isnull(olitv.qty_percentage, 1) global_subtotal, olit.global_shipping * isnull(olitv.qty_percentage, 1) global_shipping, 
		olit.global_discount * isnull(olitv.qty_percentage, 1) global_discount, olit.global_store_credit * isnull(olitv.qty_percentage, 1) global_store_credit, 
	olit.global_total_inc_vat * isnull(olitv.qty_percentage, 1) global_total_inc_vat, olit.global_total_exc_vat * isnull(olitv.qty_percentage, 1) global_total_exc_vat, 
		olit.global_total_vat * isnull(olitv.qty_percentage, 1) global_total_vat, olit.global_total_prof_fee * isnull(olitv.qty_percentage, 1) global_total_prof_fee, 

	olit.local_total_inc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_inc_vat_vf, olit.local_total_exc_vat_vf * isnull(olitv.qty_percentage, 1) local_total_exc_vat_vf, 
		olit.local_total_vat_vf * isnull(olitv.qty_percentage, 1) local_total_vat_vf, olit.local_total_prof_fee_vf * isnull(olitv.qty_percentage, 1) local_total_prof_fee_vf,

	olit.local_payment_online * isnull(olitv.qty_percentage, 1) local_payment_online, olit.local_payment_offline * isnull(olitv.qty_percentage, 1) local_payment_offline, 
		olit.local_reimbursement * isnull(olitv.qty_percentage, 1) local_reimbursement,
	olit.global_payment_online * isnull(olitv.qty_percentage, 1) global_payment_online, olit.global_payment_offline * isnull(olitv.qty_percentage, 1) global_payment_offline, 
		olit.global_reimbursement * isnull(olitv.qty_percentage, 1) global_reimbursement, 

	olitv.local_total_cost_discount local_product_cost, 0 local_shipping_cost, 0 local_freight_cost, olitv.local_total_cost_discount local_total_cost, 
	olitv.global_total_cost_discount global_product_cost, 0 global_shipping_cost, 0 global_freight_cost, olitv.global_total_cost_discount global_total_cost, 

	ol.local_to_global_rate, ol.order_currency_code, 
	ol.discount_percent, ol.vat_percent, ol.prof_fee_percent, ol.vat_rate, ol.prof_fee_rate,
	ol.num_erp_allocation_lines
from 
		Warehouse.alloc.fact_order_line_erp_issue_all olit
	left join
		Warehouse.sales.fact_order_line_v ol on olit.idOrderLine_sk = ol.idOrderLine_sk
	left join
		Warehouse.alloc.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk
			and case when (olit.transaction_type in ('O', 'C')) then 'O' else olit.transaction_type end = olitv.transaction_type
where olit.idOrderLine_sk in (22754809, 23055657, 23060066)


	--select count(*)
	--from 
	--		Warehouse.alloc.fact_order_line_erp_issue_all olit
	--	left join
	--		Warehouse.sales.fact_order_line_v ol on olit.idOrderLine_sk = ol.idOrderLine_sk
	--	left join
	--		Warehouse.alloc.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk	
	--			and case when (olit.transaction_type in ('O', 'C')) then 'O' else olit.transaction_type end = olitv.transaction_type

	--select count(*)
	--from 
	--		Warehouse.alloc.fact_order_line_erp_issue_all olit
	--	--inner join
	--	--	Warehouse.sales.fact_order_line_v ol on olit.idOrderLine_sk = ol.idOrderLine_sk
	--where olit.idOrderLine_sk <> -1

	--select count(*) 
	--from 
	--		Warehouse.alloc.fact_order_line_erp_issue_all olit
	--	--inner join
	--	--	Warehouse.alloc.fact_order_line_erp_issue_lj_v olitv on olit.idOrderLineERPIssue_sk = olitv.idOrderLineERPIssue_sk and olit.idOrderLineERP_sk = olitv.idOrderLineERP_sk
	--	--		and case when (olit.transaction_type in ('O', 'C')) then 'O' else olit.transaction_type end = olitv.transaction_type
	--where olit.idOrderLineERP_sk <> -1


--select 555672 - 533296

