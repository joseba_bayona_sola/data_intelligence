
select top 1000 count(*) from Warehouse.alloc.fact_order_line_erp_issue_all
select top 1000 count(*) from #fact_order_line_erp_issue_all

select top 1000 count(*) over (), *
from Warehouse.alloc.fact_order_line_erp_issue_all
where idOrderLine_sk in (23204644, 23180181,23180182)

select top 1000 count(*) over (), *
from #fact_order_line_erp_issue_all
where idOrderLine_sk in (23204644, 23180181, 23180182)

select 572279 - 541511
select 572279 - 572420
-------------------------------------

-- idOrderLine_sk with no idOrderLineERP_sk, idOrderLineERPIssue_sk before
select count(*) over (partition by oli1.transaction_type, oli1.idOrderLine_sk) num_rep, 
	oli1.transaction_type, oli1.idOrderLineERPIssue_sk, oli2.idOrderLineERPIssue_sk, oli1.idOrderLineERP_sk, oli2.idOrderLineERP_sk, oli1.idOrderLine_sk, 
	oli1.deduplicate_f, 
	oli1.invoice_date, oli2.invoice_date, oli1.shipment_date, oli2.shipment_date
from 
		(select *
		from Warehouse.alloc.fact_order_line_erp_issue_all oli1
		where idOrderLine_sk <> -1 and idOrderLineERP_sk = -1) oli1
	inner join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLine_sk = oli2.idOrderLine_sk
where (oli1.idOrderLineERP_sk <> oli2.idOrderLineERP_sk) or (oli1.idOrderLineERPIssue_sk <> oli2.idOrderLineERPIssue_sk) or
	(isnull(oli1.invoice_date, '') <> isnull(oli2.invoice_date, '')) or (isnull(oli1.shipment_date, '') <> isnull(oli2.shipment_date, ''))
order by num_rep desc

-- idOrderLineERP_sk with no idOrderLineERPIssue_sk before
select count(*) over (partition by oli1.transaction_type, oli1.idOrderLineERP_sk) num_rep, 
	oli1.transaction_type, oli1.idOrderLineERPIssue_sk, oli2.idOrderLineERPIssue_sk, oli1.idOrderLineERP_sk, oli2.idOrderLineERP_sk, oli1.idOrderLine_sk, 
	oli1.deduplicate_f, 
	oli1.invoice_date, oli2.invoice_date, oli1.shipment_date, oli2.shipment_date
from 
		(select *
		from Warehouse.alloc.fact_order_line_erp_issue_all oli1
		where idOrderLineERP_sk <> -1 and idOrderLineERPIssue_sk = -1) oli1
	inner join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk
where (oli1.idOrderLineERP_sk <> oli2.idOrderLineERP_sk) or (oli1.idOrderLineERPIssue_sk <> oli2.idOrderLineERPIssue_sk) or
	(isnull(oli1.invoice_date, '') <> isnull(oli2.invoice_date, '')) or (isnull(oli1.shipment_date, '') <> isnull(oli2.shipment_date, ''))
order by num_rep desc

-------------------------------------

select top 1000 count(*) over (), 
	oli1.transaction_type, oli2.transaction_type, oli1.idOrderLineERPIssue_sk, oli2.idOrderLineERPIssue_sk, oli1.idOrderLineERP_sk, oli2.idOrderLineERP_sk, oli1.idOrderLine_sk, oli2.idOrderLine_sk, 
	oli1.deduplicate_f, oli2.deduplicate_f, 
	oli1.invoice_date, oli2.invoice_date, oli1.shipment_date, oli2.shipment_date
from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	inner join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
where (isnull(oli1.invoice_date, '') <> isnull(oli2.invoice_date, '')) or (isnull(oli1.shipment_date, '') <> isnull(oli2.shipment_date, ''))


select top 1000 count(*) over (), 
	oli1.transaction_type, oli2.transaction_type, oli1.idOrderLineERPIssue_sk, oli2.idOrderLineERPIssue_sk, oli1.idOrderLineERP_sk, oli2.idOrderLineERP_sk, oli1.idOrderLine_sk, oli2.idOrderLine_sk, 
	oli1.deduplicate_f, oli2.deduplicate_f, 
	oli1.invoice_date, oli2.invoice_date, oli1.shipment_date, oli2.shipment_date
from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	full join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
where oli1.idOrderLineERPIssue_sk is null order by oli2.transaction_type, oli2.idOrderLine_sk, oli2.idOrderLineERP_sk, oli2.idOrderLineERPIssue_sk
-- where oli2.idOrderLineERPIssue_sk is null order by oli2.transaction_type, oli1.idOrderLine_sk, oli1.idOrderLineERP_sk, oli1.idOrderLineERPIssue_sk  


-------------------------------------


-- DELETE IDs queries

	select distinct oli2.idOrderLine_sk
	into #t_idOrderLine_sk_1
	from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	full join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
	where oli1.idOrderLineERPIssue_sk is null and oli2.idOrderLine_sk <> -1

	select distinct oli1.idOrderLine_sk
	into #t_idOrderLine_sk_2
	from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	full join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
	where oli2.idOrderLineERPIssue_sk is null and oli1.idOrderLine_sk <> -1

	-- 

	select distinct oli2.idOrderLineERP_sk
	into #t_idOrderLineERP_sk_1
	from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	full join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
	where oli1.idOrderLineERPIssue_sk is null and oli2.idOrderLineERP_sk <> -1

	select distinct oli1.idOrderLineERP_sk
	into #t_idOrderLineERP_sk_2
	from 
		Warehouse.alloc.fact_order_line_erp_issue_all oli1
	full join
		#fact_order_line_erp_issue_all oli2 on oli1.transaction_type = oli2.transaction_type and oli1.idOrderLineERPIssue_sk = oli2.idOrderLineERPIssue_sk
			and oli1.idOrderLineERP_sk = oli2.idOrderLineERP_sk and oli1.idOrderLine_sk = oli2.idOrderLine_sk
	where oli2.idOrderLineERPIssue_sk is null and oli1.idOrderLineERP_sk <> -1	


	--------------------------------

	select count(*) from #t_idOrderLine_sk_1
	select count(*) from #t_idOrderLine_sk_2
	select count(*) from (select * from #t_idOrderLine_sk_1 union select * from #t_idOrderLine_sk_2) t
	select count(*) from (select * from #t_idOrderLine_sk_2 except select * from #t_idOrderLine_sk_1) t


	select count(*) from #t_idOrderLineERP_sk_1
	select count(*) from #t_idOrderLineERP_sk_2
	select count(*) from (select * from #t_idOrderLineERP_sk_1 union select * from #t_idOrderLineERP_sk_2) t
	select count(*) from (select * from #t_idOrderLineERP_sk_2 except select * from #t_idOrderLineERP_sk_1) t

	select oli.*
	from
			(select idOrderLine_sk from #t_idOrderLine_sk_2 except select idOrderLine_sk from #t_idOrderLine_sk_1) t
		inner join
			Warehouse.alloc.fact_order_line_erp_issue_all oli on t.idOrderLine_sk = oli.idOrderLine_sk

	select olme.*
	from
			(select idOrderLine_sk from #t_idOrderLine_sk_2 except select idOrderLine_sk from #t_idOrderLine_sk_1) t
		inner join
			Warehouse.alloc.fact_order_line_mag_erp olme on t.idOrderLine_sk = olme.idOrderLine_sk_fk

	--------------------------------

	delete from Warehouse.alloc.fact_order_line_erp_issue_all
	where idOrderLineERP_sk in (select idOrderLineERP_sk from #t_idOrderLineERP_sk_1)

	delete from Warehouse.alloc.fact_order_line_erp_issue_all
	where idOrderLine_sk in (select idOrderLine_sk from #t_idOrderLine_sk_1)

	-- merge statement

	delete from Warehouse.alloc.fact_order_line_erp_issue_all
	where idOrderLine_sk in 
		(select oli.idOrderLine_sk
		from 
				(select distinct idOrderLine_sk, deduplicate_f
				from Warehouse.alloc.fact_order_line_erp_issue_all
				where idOrderLine_sk <> -1) oli
			inner join
				Warehouse.alloc.fact_order_line_mag_erp olme on oli.idOrderLine_sk = olme.idOrderLine_sk_fk 
		where oli.deduplicate_f <> olme.deduplicate_f) 

	--------------------------------

	drop table #fact_order_line_erp_issue_all

	drop table #t_idOrderLine_sk_1
	drop table #t_idOrderLine_sk_2
	drop table #t_idOrderLineERP_sk_1
	drop table #t_idOrderLineERP_sk_2
