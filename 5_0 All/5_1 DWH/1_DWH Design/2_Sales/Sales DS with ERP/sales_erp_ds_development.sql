
select top 1000 count(*)
from Warehouse.sales.dim_order_header_v
where order_date between '2019-08-01' and '2019-09-01'
	or invoice_date between '2019-08-01' and '2019-09-01'
	or shipment_date between '2019-08-01' and '2019-09-01'

drop table #ol_pk -- 386592

select idOrderLine_sk, order_line_id_bk
into #ol_pk
from
	(select idOrderLine_sk, order_line_id_bk
	from Warehouse.sales.fact_order_line_v
	where order_date between '2019-08-01' and '2019-09-01'
	union
	select idOrderLine_sk, order_line_id_bk
	from Warehouse.sales.fact_order_line
	where invoice_date between '2019-08-01' and '2019-09-01'
		or shipment_date between '2019-08-01' and '2019-09-01') ol -- also_refund_date to have shipment OK

select top 1000 ol.idOrderHeader_sk, ol.idOrderLine_sk, 
	ol.order_line_id_bk, 
	ol.order_id_bk, ol.order_no, 
	ol.order_date, ol.invoice_date, ol.shipment_date, ol.shipment_date_r,
	ol.website, ol.store_name, 
	ol.customer_id, ol.customer_email, 
	ol.order_status_name,
	ol.customer_status_name, ol.rank_seq_no, ol.customer_order_seq_no,
	ol.product_type_name, ol.category_name, ol.product_id_magento, ol.product_family_name, 
	ol.sku_magento, ol.sku_erp, 
	ol.warehouse_name, 
	ol.pack_size, ol.qty_unit, ol.qty_pack, 
	ol.global_subtotal, ol.global_total_exc_vat, ol.global_product_cost
from 
		#ol_pk olpk
	inner join
		Warehouse.sales.fact_order_line_v ol on olpk.idOrderLine_sk = ol.idOrderLine_sk
where ol.invoice_date is null
order by ol.order_id_bk

	select sum(ol.global_total_exc_vat), count(distinct ol.customer_id), count(distinct ol.order_id_bk)
	from 
			#ol_pk olpk
		inner join
			Warehouse.sales.fact_order_line_v ol on olpk.idOrderLine_sk = ol.idOrderLine_sk
	-- where order_date between '2019-08-01' and '2019-09-01'
	where invoice_date between '2019-08-01' and '2019-09-01'
	-- where shipment_date_r between '2019-08-01' and '2019-09-01'

------------------------------------------------------------------

	select top 10000 count(*) over (), 
		ol.idOrderHeader_sk, ol.idOrderLine_sk, ole.idOrderLineERP_sk, 
		ol.order_line_id_bk, 
		ol.order_id_bk, ol.order_no, 
		ol.order_date, ol.invoice_date, ol.shipment_date, ol.shipment_date_r,
		ole.order_date_sync, 
		ole.order_type_erp_f, ole.order_type_erp_name, ole.order_status_erp_name, 
		ol.website, ol.store_name, 
		ol.customer_id, ol.customer_email, 
		ol.order_status_name,
		ol.customer_status_name, ol.rank_seq_no, ol.customer_order_seq_no,
		ole.bom_f, ol.product_type_name, ol.category_name, ol.product_id_magento, ol.product_family_name, 
		ol.sku_magento, ol.sku_erp, 
		ole.warehouse_name_pref, ol.warehouse_name, 
		ole.pack_size_opt_desc_pref, ole.inventory_level_success_f, ole.po_source_name_sh, ole.po_type_name_sh, ole.purchase_order_number_sh, 
		ol.pack_size, ol.qty_unit, ol.qty_pack, 
		ole.qty_unit, 
		ol.global_subtotal, ole.global_subtotal, ol.global_total_exc_vat, ol.global_product_cost
	from 
			#ol_pk olpk
		inner join
			Warehouse.sales.fact_order_line_v ol on olpk.idOrderLine_sk = ol.idOrderLine_sk
		left join
			Warehouse.alloc.fact_order_line_erp_v ole on ol.order_line_id_bk = ole.order_line_id_bk
	-- where ol.order_id_bk in (9177537, 9722279, 9607391)
	-- where ole.order_line_id_bk is null -- 2 reasons: not synced / deduplicated order (need to solve it using fact_order_line_mag_erp)
	order by ol.order_id_bk, ol.order_line_id_bk

	select count(*) over (), 
		ol.idOrderHeader_sk, ol.idOrderLine_sk, ole.idOrderLineERP_sk, 
		ol.order_line_id_bk, 
		ol.order_id_bk, ol.order_no, 
		ol.order_date, ol.invoice_date, ol.shipment_date, ol.shipment_date_r,
		ole.order_date_sync, 
		ole.order_type_erp_f, ole.order_type_erp_name, ole.order_status_erp_name, 
		ol.website, ol.store_name, 
		ol.customer_id, ol.customer_email, 
		ol.order_status_name,
		ol.customer_status_name, ol.rank_seq_no, ol.customer_order_seq_no,
		ole.bom_f, ol.product_type_name, ol.category_name, ol.product_id_magento, ol.product_family_name, 
		ol.sku_magento, ol.sku_erp, 
		ole.warehouse_name_pref, ol.warehouse_name, 
		ole.pack_size_opt_desc_pref, ole.inventory_level_success_f, ole.po_source_name_sh, ole.po_type_name_sh, ole.purchase_order_number_sh, 
		ol.pack_size, ol.qty_unit, ol.qty_pack, 
		ole.qty_unit, 
		ol.global_subtotal, ole.global_subtotal, ol.global_total_exc_vat, ol.global_product_cost
	from 
			Warehouse.alloc.fact_order_line_erp_v ole
		left join
			#ol_pk olpk on ole.order_line_id_bk = olpk.order_line_id_bk
		left join
			Warehouse.sales.fact_order_line_v ol on ol.order_line_id_bk = ole.order_line_id_bk
	where ole.order_date_sync between '2019-08-01' and '2019-09-01'
		and olpk.order_line_id_bk is null
	order by ole.order_date_sync
