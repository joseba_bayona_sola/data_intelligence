
-- OL that are not in OL_MAG_ERP
	-- Only those where OH is synced to ERP (rows in gen_order_orderlinemagento_v)

select top 1000 count(*)
from Warehouse.sales.fact_order_line
where idOrderLine_sk > 15000000

select top 1000 count(*)
from Warehouse.alloc.fact_order_line_mag_erp
where idOrderLine_sk_fk > 15000000

select top 1000 idOrderLine_sk, order_line_id_bk, order_id_bk, order_no, website, 
	order_date, invoice_date, shipment_date, refund_date, 
	order_status_name, order_status_magento_name, payment_method_name
from Warehouse.sales.fact_order_line_v
where order_date > '2019-09-01'
	and idOrderLine_sk not in (select idOrderLine_sk_fk from Warehouse.alloc.fact_order_line_mag_erp)
	and order_status_name  <> 'CANCEL'
	-- and shipment_date is not null
order by order_date

-----------------------------------------

-- OL ERP repetitions

select top 1000 count(*) over (partition by idOrderLineERP_sk_fk) num_rep, sum(qty_percentage) over (partition by idOrderLineERP_sk_fk) sum_perc,
	idOrderLine_sk_fk, idOrderLineERP_sk_fk, orderlinemagentoid_bk, deduplicate_f, 
	qty_unit_mag, qty_unit_erp, qty_percentage, 
	local_total_cost_mag, local_total_cost_erp
from Warehouse.alloc.fact_order_line_mag_erp
-- where idOrderLineERP_sk_fk = 570993
order by num_rep desc, idOrderLine_sk_fk

	select deduplicate_f, count(*)
	from Warehouse.alloc.fact_order_line_mag_erp
	group by deduplicate_f
	
	select top 1000 count(*) over (), idOrderLineERP_sk_fk, count(*)
	from Warehouse.alloc.fact_order_line_mag_erp
	group by idOrderLineERP_sk_fk
	having count(*) > 1
	order by count(*) desc

-----------------------------------------

-- qty_percentage
	-- NULL values: 50k (recent orders (still not processes in ERP) + REFUND orders (don't have issue)) (120 strange orders (have snap_complete_date - don't have issue record in ERP)
	-- 0 values: 29k (recent orders (still not processes in ERP) + REFUND orders (don't have issue)) -- Why different: NULL when OL was allocated but not issued and 0 when OL even not allocated

-- NULL values
select top 100000 count(*) over (),
	sum(qty_percentage) over (partition by idOrderLineERP_sk_fk) sum_perc,
	idOrderLine_sk_fk, idOrderLineERP_sk_fk, orderlinemagentoid_bk, snap_complete_date, deduplicate_f, 
	qty_unit_mag, qty_unit_erp, qty_percentage, 
	local_total_cost_mag, local_total_cost_erp, ins_ts
from Warehouse.alloc.fact_order_line_mag_erp
where qty_percentage is null
	-- and snap_complete_date is not null
order by idOrderLine_sk_fk desc

	select convert(date, ins_ts), count(*)
	from Warehouse.alloc.fact_order_line_mag_erp
	where qty_percentage is null
	group by convert(date, ins_ts)
	order by count(*) desc

	select top 100000 count(*) over (), olme.*, 
		ol.order_id_bk, ol.order_no, ol.website, 
		ol.order_date, ol.invoice_date, ol.shipment_date, ol.refund_date, 
		ol.order_status_name
	from
			(select 
				sum(qty_percentage) over (partition by idOrderLineERP_sk_fk) sum_perc,
				idOrderLine_sk_fk, idOrderLineERP_sk_fk, orderlinemagentoid_bk, snap_complete_date, deduplicate_f, 
				qty_unit_mag, qty_unit_erp, qty_percentage, 
				local_total_cost_mag, local_total_cost_erp, ins_ts
			from Warehouse.alloc.fact_order_line_mag_erp
			where qty_percentage is null
				-- and snap_complete_date is not null
				) olme
		inner join
			Warehouse.sales.fact_order_line_v ol on olme.idOrderLine_sk_fk = ol.idOrderLine_sk
	order by idOrderLine_sk_fk desc

-- 0 values
select top 100000 count(*) over (), *
from
	(select 
		sum(qty_percentage) over (partition by idOrderLineERP_sk_fk) sum_perc,
		idOrderLine_sk_fk, idOrderLineERP_sk_fk, orderlinemagentoid_bk, snap_complete_date, deduplicate_f, 
		qty_unit_mag, qty_unit_erp, qty_percentage, 
		local_total_cost_mag, local_total_cost_erp, ins_ts
	from Warehouse.alloc.fact_order_line_mag_erp) t
where abs(1 - sum_perc) > 0.1
order by abs(1 - sum_perc) desc, idOrderLine_sk_fk desc

	select top 100000 count(*) over (), idOrderLineERP_sk_fk, count(*), sum(qty_percentage) 
	from Warehouse.alloc.fact_order_line_mag_erp
	group by idOrderLineERP_sk_fk
	having sum(qty_percentage) <> 1
	order by sum(qty_percentage) desc


-- 

	select distinct idOrderLineERP_sk_fk
	into #ol_mag_erp_null_zero
	from Warehouse.alloc.fact_order_line_mag_erp
	where qty_percentage is null
	union
	select distinct idOrderLineERP_sk_fk
	from Warehouse.alloc.fact_order_line_mag_erp
	group by idOrderLineERP_sk_fk
	having sum(qty_percentage) <> 1

	select 
		sum(qty_percentage) over (partition by olme.idOrderLineERP_sk_fk) sum_perc,
		idOrderLine_sk_fk, olme.idOrderLineERP_sk_fk, 
		snap_complete_date, deduplicate_f, 
		qty_unit_mag, qty_unit_erp, qty_percentage, 
		local_total_cost_mag, local_total_cost_erp, ins_ts
	from 
			Warehouse.alloc.fact_order_line_mag_erp olme
		inner join
			#ol_mag_erp_null_zero t on olme.idOrderLineERP_sk_fk = t.idOrderLineERP_sk_fk

	select 
		sum(qty_percentage) over (partition by olme.idOrderLineERP_sk_fk) sum_perc,
		idOrderLine_sk_fk, olme.idOrderLineERP_sk_fk, 
		ol.order_id_bk, ol.order_no, ol.website, 
		ol.order_date, ol.invoice_date, ol.shipment_date, ol.refund_date, 
		ol.order_status_name
		snap_complete_date, deduplicate_f, 
		qty_unit_mag, qty_unit_erp, qty_percentage, 
		local_total_cost_mag, local_total_cost_erp, ins_ts
	from 
			Warehouse.alloc.fact_order_line_mag_erp olme
		inner join
			#ol_mag_erp_null_zero t on olme.idOrderLineERP_sk_fk = t.idOrderLineERP_sk_fk
		inner join
			Warehouse.sales.fact_order_line_v ol on olme.idOrderLine_sk_fk = ol.idOrderLine_sk
	order by idOrderLine_sk_fk desc 

	select 
		sum(olme.qty_percentage) over (partition by olme.idOrderLineERP_sk_fk) sum_perc,
		idOrderLine_sk_fk, olme.idOrderLineERP_sk_fk, 
		olme.snap_complete_date, deduplicate_f, 
		qty_unit_mag, qty_unit_erp, olme.qty_percentage, 
		local_total_cost_mag, local_total_cost_erp, olme.ins_ts, 
		oli.batchstockissueid_bk, oli.issue_id
	from 
			Warehouse.alloc.fact_order_line_mag_erp olme
		inner join
			#ol_mag_erp_null_zero t on olme.idOrderLineERP_sk_fk = t.idOrderLineERP_sk_fk
		left join
			Warehouse.alloc.fact_order_line_erp_issue oli on olme.idOrderLineERP_sk_fk = oli.idOrderLineERP_sk_fk
	where oli.batchstockissueid_bk is not null
	order by idOrderLine_sk_fk desc 
