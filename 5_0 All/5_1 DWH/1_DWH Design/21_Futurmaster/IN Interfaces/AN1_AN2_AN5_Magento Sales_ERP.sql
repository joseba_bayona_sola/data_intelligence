﻿
-- P_PACKS_SQL: PF with Pack Size Available
select pf.magentoproductid, pf.name, ps.size 
from 
	product$productfamily pf
    JOIN 
	product$packsize_productfamily ps_pf ON ps_pf.product$productfamilyid = pf.id
    JOIN 
	product$packsize ps on ps.id = ps_pf.product$packsizeid 
-- where magentoproductid in ('2316', '1226', '1101', '1127', '1145', '1178', '1181', '1190', '1206', '1360', '2307', '2313', '2314', '2322', '2327', '2330', '2332', '3141')	-- Manual Products
-- where magentoproductid in ('3162', '3163', '3164', '3165', '3191', '3166') -- 90 pack size products

 
order by magentoproductid

-- P_PROD_KEYS
select thetype, value, code
from product$keycodes
where thetype = 'PO'
order by thetype, value, code

	select thetype, count(*)
	from product$keycodes
	group by thetype
	order by thetype

-- P_VALIDATE_SKU
select distinct sku, oldsku, manufacturercodenumber, packsize
from product$stockitem	
limit 1000 