
-- order_id_bk, order_date, invoice_date
-- pack_size
-- global_subtotal_exc_vat


select order_line_id_bk, order_id_bk, order_no, 
	order_date, invoice_date, customer_id, store_name, country_code_ship, order_status_name,
	product_id_magento, sku_magento, sku_erp, 
	pack_size, qty_unit, 
	-- global_subtotal, global_total_exc_vat, 
	global_subtotal - (global_subtotal * vat_percent) global_subtotal_exc_vat
from Warehouse.sales.fact_order_line_v
where order_id_bk = 10318536 
	or (customer_id = 1121534 and order_date between '2019-10-29' and '2019-11-14')
	or (customer_id = 1088593 and order_date between '2019-10-29' and '2019-11-14')
order by order_date

	-- deduplication
	select order_no, sku_magento, sku_erp, sum(qty_unit) qty_unit, sum(global_subtotal) global_subtotal
	from Warehouse.sales.fact_order_line_v
	where order_id_bk = 10318536 
		or (customer_id = 1121534 and order_date between '2019-10-29' and '2019-11-14')
		or (customer_id = 1088593 and order_date between '2019-10-29' and '2019-11-14')
		
	group by order_no, sku_magento, sku_erp

	-- getting SKU value
	select t.sku_erp, si.stockitemid_bk, si.sku, si.packsize
	from
			(select distinct sku_erp
			from Warehouse.sales.fact_order_line_v
			where order_id_bk = 10318536 
				or (customer_id = 1121534 and order_date between '2019-10-29' and '2019-11-14')) t
		left join
			Warehouse.prod.dim_stock_item_v si on t.sku_erp = si.parameter
	order by t.sku_erp, si.packsize

	-- Optimum Pack Size Allocation

	select top 1000 *
	from Warehouse.prod.dim_product_family_pack_size_v
	where product_id_magento in ('1122', '2296', '2301', '3177')
	order by product_id_magento, size

	select top 1000 *
	from Warehouse.prod.dim_product_family_pack_size_optimum_v
	where product_id_magento in ('1122', '2296', '2301', '3177')
	order by product_id_magento, quantity_lens

	select count(*) over (partition by product_id_magento) num_rep, product_id_magento, size_1, size_2, num
	from 
		(select product_id_magento, size_1, size_2, count(*) num
		from Warehouse.prod.dim_product_family_pack_size_optimum_v
		-- where optimum_type = 'size'
		group by product_id_magento, size_1, size_2) p
	-- where product_id_magento = '1083'
	order by num_rep desc, product_id_magento

	-- 

	select top 1000 *
	from Warehouse.prod.dim_product_family_v
	where product_id in ('2316', '1226', '1101', '1127', '1145', '1178', '1181', '1190', '1206', '1360', '2307', '2313', '2314', '2322', '2327', '2330', '2332', '3141')
	order by product_id

	select top 1000 *
	from Warehouse.prod.dim_product_family_pack_size_v
	where product_id_magento in ('2316', '1226', '1101', '1127', '1145', '1178', '1181', '1190', '1206', '1360', '2307', '2313', '2314', '2322', '2327', '2330', '2332', '3141')

