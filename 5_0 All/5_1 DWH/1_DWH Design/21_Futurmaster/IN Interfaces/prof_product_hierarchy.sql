﻿
-- AN4: Product Hierarchy

        " FROM  Product.Barcode AS bc " +
        " LEFT OUTER JOIN bc/Product.Barcode_StockItem/Product.StockItem AS si  " +
       " LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize AS ps " +
       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product AS p "   +
       " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +
       " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily AS pf " +
       " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory AS pc " +
       " WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')";

select pf.id, pf.magentoproductid, pf.name, pf.ManufacturerProductFamily, 
	p.producttype, pc.name, p.sku, p.description, si.sku, si.stockitemdescription
	-- pc.code, 
from 
		product$barcode bc 
	left join
		product$barcode_stockitem bc_si on bc_si.product$barcodeid = bc.id
	left join	
		product$stockitem si on si.id = bc_si.product$stockitemid
	left join
		product$stockitem_packsize si_ps on si.id = si_ps.product$stockitemid
	left join
		product$packsize ps on si_ps.product$packsizeid = ps.id
	left join
		product$stockitem_product si_p on si_p.product$stockitemid = si.id
	left join
		product$product	p on p.id = si_p.product$productid
	left join
		product$contactlens cl on p.id = cl.id
	left join
		product$product_productfamily p_pf on p_pf.product$productid = p.id
	left join
		product$productfamily pf on pf.id = p_pf.product$productfamilyid
	left join
		product$productfamily_productcategory pf_pc on pf.id = pf_pc.product$productfamilyid
	left join
		product$productcategory pc on pf_pc.product$productcategoryid = pc.id	
where pc.code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
	-- and pf.magentoproductid = '1083'
	and (si.id is null or ps.id is null or p.id is null or cl.id is null or pf.id is null or pc.id is null)
order by pc.code, pf.magentoproductid, p.sku	

--168271

select count(*)
from 
		product$barcode bc 
	left join
		product$barcode_stockitem bc_si on bc_si.product$barcodeid = bc.id
	left join	
		product$stockitem si on si.id = bc_si.product$stockitemid
	left join
		product$stockitem_packsize si_ps on si.id = si_ps.product$stockitemid
	left join
		product$packsize ps on si_ps.product$packsizeid = ps.id
	left join
		product$stockitem_product si_p on si_p.product$stockitemid = si.id
	left join
		product$product	p on p.id = si_p.product$productid
	left join
		product$contactlens cl on p.id = cl.id
	left join
		product$product_productfamily p_pf on p_pf.product$productid = p.id
	left join
		product$productfamily pf on pf.id = p_pf.product$productfamilyid
	left join
		product$productfamily_productcategory pf_pc on pf.id = pf_pc.product$productfamilyid
	left join
		product$productcategory pc on pf_pc.product$productcategoryid = pc.id	
where pc.code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
		
--------------------------------------------------------------------------------

select pf.id, pf.magentoproductid, pf.name, pf.ManufacturerProductFamily, 
	p.producttype, pc.name, p.sku, p.description, si.sku, si.stockitemdescription
	-- pc.code, 
from 
		product$productfamily pf
	inner join
		product$productfamily_productcategory pf_pc on pf.id = pf_pc.product$productfamilyid
	inner join
		product$productcategory pc on pf_pc.product$productcategoryid = pc.id
	inner join
		product$product_productfamily p_pf on pf.id = p_pf.product$productfamilyid
	inner join
		product$product	p on p_pf.product$productid = p.id
	inner join
		product$contactlens cl on p.id = cl.id
	inner join
		product$stockitem_product si_p on p.id = si_p.product$productid
	inner join
		product$stockitem si on si_p.product$stockitemid = si.id
	inner join
		product$stockitem_packsize si_ps on si.id = si_ps.product$stockitemid
	inner join
		product$packsize ps on si_ps.product$packsizeid = ps.id
	inner join
		product$barcode_stockitem bc_si on si.id = bc_si.product$stockitemid
	inner join
		product$barcode bc on bc_si.product$barcodeid = bc.id
where pc.code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
	and pf.magentoproductid = '1083'
order by pc.code, pf.magentoproductid, p.sku	

-- 168144
select count(*)
from 
		product$productfamily pf
	inner join
		product$productfamily_productcategory pf_pc on pf.id = pf_pc.product$productfamilyid
	inner join
		product$productcategory pc on pf_pc.product$productcategoryid = pc.id
	inner join
		product$product_productfamily p_pf on pf.id = p_pf.product$productfamilyid
	inner join
		product$product	p on p_pf.product$productid = p.id
	inner join
		product$contactlens cl on p.id = cl.id
	inner join
		product$stockitem_product si_p on p.id = si_p.product$productid
	inner join
		product$stockitem si on si_p.product$stockitemid = si.id
	inner join
		product$stockitem_packsize si_ps on si.id = si_ps.product$stockitemid
	inner join
		product$packsize ps on si_ps.product$packsizeid = ps.id
	inner join
		product$barcode_stockitem bc_si on si.id = bc_si.product$stockitemid
	inner join
		product$barcode bc on bc_si.product$barcodeid = bc.id
where pc.code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
