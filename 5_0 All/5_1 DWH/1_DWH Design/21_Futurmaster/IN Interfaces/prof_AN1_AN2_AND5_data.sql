
select top 1000 SKU, customer_website, country_category, year, month, day, quantity
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
where customer_website like '1121534%' or customer_website like '2965324%' or customer_website like '1088593%'
-- where sku = '02682B3D4BE0000000000051' and customer_website = '1121534_VD-UK' and year = '2019' and month = '10' and day = '30'
-- where sku = '01882B3D4BS0000000000301' and customer_website = '2324351_VD-NL' and year = '2019' and month = '10' and day = '30'
-- where sku = '01115B3D7BQ00000000AH021' and customer_website = '2794708_VD-UK' and year = '2019' and month = '11' and day = '5'
-- where customer_website = '2965324_VD-FR'
order by customer_website, country_category, year, month, day, sku, quantity

	select count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D

	-- Duplicated per CUST, SKU, COUNTRY, DAY: Would be excluded
	select sku, customer_website, country_category, year, month, day, count(*), count(distinct quantity)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	-- where customer_website = '3107580_VD-UK'
	group by sku, customer_website, country_category, year, month, day
	having count(*) > 1
	-- having count(distinct quantity) > 1
	order by count(*) desc


	select sku, customer_website, year, month, day, count(*), count(distinct quantity)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	group by sku, customer_website, year, month, day
	-- having count(distinct quantity) > 1
	order by count(*) desc

	select sku, customer_website, year, month, day, quantity, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	group by sku, customer_website, year, month, day, quantity
	-- having count(*) > 1
	order by count(*) desc

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	group by sku
	order by sku

	select customer_website, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	group by customer_website
	order by customer_website

	select country_category, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	group by country_category
	order by country_category

	select year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
	-- where day <> '13'
	group by year, month, day
	order by year, month, convert(int, day)

-------------------------------------------------

select top 1000 SKU, customer_website, country_category, year, month, day, value
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
where customer_website like '1121534%' or customer_website like '2965324%' or customer_website like '1088593%'
-- where sku = '02682B3D4BE0000000000051' and customer_website = '1121534_VD-UK' and year = '2019' and month = '10' and day = '30'
-- where sku = '01882B3D4BS0000000000301' and customer_website = '2324351_VD-NL' and year = '2019' and month = '10' and day = '30'
where customer_website = '2965324_VD-FR'

	select count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D

	select sku, customer_website, year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
	group by sku, customer_website, year, month, day
	order by count(*) desc

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
	group by sku
	order by sku

	select customer_website, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
	group by customer_website
	order by customer_website

	select country_category, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
	group by country_category
	order by country_category

	select year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_VALUE_D
	group by year, month, day
	order by year, month, day

-------------------------------------------------

select top 1000 SKU, customer_website, country_category, year, month, day, quantity
from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
where customer_website like '1121534%' or customer_website like '2965324%'
-- where sku = '02682B3D4BE0000000000051' and customer_website = '1121534_VD-UK' and year = '2019' and month = '10' and day = '30'
-- where sku = '01882B3D4BS0000000000301' and customer_website = '2324351_VD-NL' and year = '2019' and month = '10' and day = '30'
where customer_website = '2965324_VD-FR'


	select count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D

	select sku, customer_website, year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
	group by sku, customer_website, year, month, day
	order by count(*) desc

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
	group by sku
	order by sku

	select customer_website, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
	group by customer_website
	order by customer_website

	select country_category, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
	group by country_category
	order by country_category

	select year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
	group by year, month, day
	order by year, month, day

-------------------------------------------------
-------------------------------------------------
-------------------------------------------------

select *
from
		(select sku, customer_website, year, month, day, quantity, count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
		group by sku, customer_website, year, month, day, quantity) t1
	full join
		(select sku, customer_website, year, month, day, quantity, count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMANA_EXACT_SALES_D
		group by sku, customer_website, year, month, day, quantity) t2 on t1.sku = t2.sku and t1.customer_website = t2.customer_website 
			and t1.year = t2.year and t1.month = t2.month and t1.day = t2.day and t1.num_rows = t2.num_rows
where t1.sku is null or t2.sku is null