// This file was generated by Mendix Modeler.
//
// WARNING: Only the following code will be retained when actions are regenerated:
// - the import list
// - the code between BEGIN USER CODE and END USER CODE
// - the code between BEGIN EXTRA CODE and END EXTRA CODE
// Other code you write will be lost the next time you deploy the project.
// Special characters, e.g., é, ö, à, etc. are supported in comments.

package futurmaster.actions;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import com.mendix.core.Core;
import com.mendix.core.CoreException;
import com.mendix.systemwideinterfaces.connectionbus.data.IDataRow;
import com.mendix.systemwideinterfaces.connectionbus.data.IDataTable;
import com.mendix.systemwideinterfaces.connectionbus.requests.types.IOQLTextGetRequest;
import com.mendix.systemwideinterfaces.core.IContext;
import com.mendix.webui.CustomJavaAction;
import futurmaster.proxies.FMFTPConfigurations;
import com.mendix.systemwideinterfaces.core.IMendixObject;

public class JAV_FMDRP2UnallocatedSKU extends CustomJavaAction<java.lang.Boolean>
{
	private IMendixObject __FMFTPConfigurationsParameter1;
	private futurmaster.proxies.FMFTPConfigurations FMFTPConfigurationsParameter1;

	public JAV_FMDRP2UnallocatedSKU(IContext context, IMendixObject FMFTPConfigurationsParameter1)
	{
		super(context);
		this.__FMFTPConfigurationsParameter1 = FMFTPConfigurationsParameter1;
	}

	@Override
	public java.lang.Boolean executeAction() throws Exception
	{
		this.FMFTPConfigurationsParameter1 = __FMFTPConfigurationsParameter1 == null ? null : futurmaster.proxies.FMFTPConfigurations.initialize(getContext(), __FMFTPConfigurationsParameter1);

		// BEGIN USER CODE
		
		
		// this.addTextMessageFeedback(MessageType.INFO, "Message to send", true);
		
		return  executeQuery(FMFTPConfigurationsParameter1);
		// END USER CODE
	}

	/**
	 * Returns a string representation of this action
	 */
	@Override
	public java.lang.String toString()
	{
		return "JAV_FMDRP2UnallocatedSKU";
	}

	// BEGIN EXTRA CODE

	// @formatter:off
	private boolean executeQuery(FMFTPConfigurations FMFTPConfiguration) {
		
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		JAV_FMUtil jav_FMUtil = new JAV_FMUtil(getContext());
		///////////////////////////////////////////////////////////////////////////////
		StringBuilder sb = new StringBuilder();
		String delimiter = ";" ;
		String fileName  	  = "ERP_FMDRP_Unall_SKU_Req.txt";
		/** "TEST_" will be auto appended to the beginning of the fileName String in the case of Dev **/
		String fileShortName = "*** DRP2 - " ;
//		///////////////////////////////////////////////////////////////// 
//		boolean isInDevLocal  = false;  // 127.0.0.1
//		boolean isInDevLive   = false;  // 52.30.165.176 "//VD_Test
//		boolean isLive		  = false;  // 52.30.165.176 "//		
//		//////////////////////////////////////////////////////////////// 
////		isInDevLocal  		  = true;   // 127.0.0.1
////		isInDevLive     	  = true;  	// 52.30.165.176 "//VD_Test
//		isLive		  		  = true;   // 52.30.165.176 "//" Live
//		///////////////////////////////////////////////////////////////////////////////
//		///////////////////////////////////////////////////////////////////////////////	//////
		///////////////////////////////////////////////////////////////////////////////	
		
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		String oqlQuery = getOQLQueryRetail();
		IOQLTextGetRequest request = Core.createOQLTextGetRequest();
		request.setQuery(oqlQuery);
		IDataTable resultDT;
		
		///////////////////////////////////////////////////////////////////////////////
		
		String oqlQueryw = getOQLQueryWholeSale();
		IOQLTextGetRequest requestw = Core.createOQLTextGetRequest();
		requestw.setQuery(oqlQueryw);
		IDataTable resultDTw;
		
		
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////
		Date date = new Date(); // your date
		
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    
	    String year 	= String.valueOf(cal.get(Calendar.YEAR));
	    
	    int mth 		= cal.get(Calendar.MONTH) + 1;
	    String month 	= ((mth < 10) ? "0" + mth : "" + mth);
	    
	    int d 			= cal.get(Calendar.DAY_OF_MONTH);
	    String day 		= ((d < 10) ? "0" + d : "" + d);
	    
//	    String day 		= String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		
	
		try {

			resultDT = Core.retrieveOQLDataTable(getContext(), request);
			List<? extends IDataRow> RowsList 	= resultDT.getRows(); 
			
			double totalPackQuantity;
			List<? extends IDataRow> RowsListSh;
			IDataTable resultDTSh ;
			
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////// RETAIL //////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			for (IDataRow row : RowsList) {
				totalPackQuantity = 0.0;

				sb.append((String)row.getValue(getContext(), "si_SKU"));
				sb.append(delimiter);
				sb.append(row.getValue(getContext(), "w_ShortName").toString().toUpperCase());
				sb.append(delimiter);
				sb.append(year);
				sb.append(delimiter);
				sb.append(month);
				sb.append(delimiter);
				sb.append(day);
				sb.append(delimiter);

				request  = sumUnallocatedShotagesRetail(row.getValue(getContext(), "wsi_id").toString(), request);
				resultDTSh = Core.retrieveOQLDataTable(getContext(), request);
				
				if (resultDTSh.getRowCount() == 0) {
					sb.append((int) totalPackQuantity);					
					sb.append("\r\n");
					continue;
				}
				
				
				RowsListSh = resultDTSh.getRows(); 
				for (IDataRow rowSh : RowsListSh) {
					totalPackQuantity = totalPackQuantity + Double.parseDouble(rowSh.getValue(getContext(), "PackQuantity").toString());
				}
				
				sb.append((int) totalPackQuantity);	
				sb.append("\r\n");
			}
			
			
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////// WholeSale /////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			
			resultDTw = Core.retrieveOQLDataTable(getContext(), requestw);
			List<? extends IDataRow> RowsListw 	= resultDTw.getRows(); 
			
			List<? extends IDataRow> RowsListShw;
			IDataTable resultDTShw ;		
			
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			for (IDataRow row : RowsListw) {
				totalPackQuantity = 0.0;

				sb.append((String)row.getValue(getContext(), "si_SKU"));
				sb.append(delimiter);
				sb.append(row.getValue(getContext(), "w_ShortName").toString().toUpperCase());
				sb.append(delimiter);
				sb.append(year);
				sb.append(delimiter);
				sb.append(month);
				sb.append(delimiter);
				sb.append(day);
				sb.append(delimiter);

				requestw  = sumUnallocatedShotagesWholeSale(row.getValue(getContext(), "wsi_id").toString(), requestw);
				resultDTShw = Core.retrieveOQLDataTable(getContext(), requestw);
				
				if (resultDTShw.getRowCount() == 0) {
					sb.append((int) totalPackQuantity);					
					sb.append("\r\n");
					continue;
				}
				
				
				RowsListShw = resultDTShw.getRows(); 
				for (IDataRow rowSh : RowsListShw) {
					totalPackQuantity = totalPackQuantity + Double.parseDouble(rowSh.getValue(getContext(), "PackQuantity").toString());
				}
				
				sb.append((int) totalPackQuantity);	
				sb.append("\r\n");
			}
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////
		
			
            ///////////////////////////////////////////////////
            jav_FMUtil.sFtpProcess(FMFTPConfiguration, sb,  fileShortName, fileName);
            ///////////////////////////////////////////////////
			
			
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return true;
        
	}
	
	private String getOQLQueryRetail () {
		String strOQLQuery =
				" SELECT " + 
						
				 " coalesce(wsi.id, 0) 						AS wsi_id, " +
				 " coalesce(w.ShortName, '')+'_RETAIL'		AS w_ShortName , " +
				 " coalesce(si.SKU,'') 	                    AS si_SKU  " +
				 
				 " FROM  Inventory.WarehouseStockItem AS wsi " +

				 " LEFT OUTER JOIN wsi/Inventory.Located_at/Inventory.Warehouse AS w  " +
				 " LEFT OUTER JOIN wsi/Inventory.WarehouseStockItem_StockItem/Product.StockItem AS si " +  	 
				 " WHERE (wsi.AvailableQty > 0 OR wsi.ForwardDemand > 0) " + 		
				 " ORDER BY w_ShortName, si_SKU " ;		
		
		return strOQLQuery;
	}

	
	private IOQLTextGetRequest sumUnallocatedShotagesRetail (String id, IOQLTextGetRequest request) {
		// Double totalPackQuantity = 0.00;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		String OQLQuery =
			 " FROM  Inventory.WarehouseStockItem AS wsi "
			  + " INNER JOIN wsi/PurchaseOrders.Shortage_WarehouseStockItem/PurchaseOrders.Shortage AS s "
			  + " WHERE wsi.id = " + id  + " AND (s.Status = 'Pending' AND CAST(s.Wholesale as String) = 'false' ) " 
//			  + " WHERE wsi.id = " + id  + " AND (s.Status = 'Pending' AND CAST(s.Wholesale as String) = 'false' )" 
	
			+ " SELECT "
			+ " wsi.id AS wsi_id, "
			+ " s.Status, "
			+ " s.Wholesale,"
			+ " s.PackQuantity AS PackQuantity" ;
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// IOQLTextGetRequest request = Core.createOQLTextGetRequest();
		
	
		request.setQuery(OQLQuery);
		return request;
		
	}	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	private String getOQLQueryWholeSale() {
		String strOQLQuery =
				" SELECT " + 
						
				 " coalesce(wsi.id, 0) 						AS wsi_id, " +
				 " coalesce(w.ShortName, '')+'_WholeSale'	AS w_ShortName , " +
				 " coalesce(si.SKU,'') 	                    AS si_SKU  " +
				 
				 " FROM  Inventory.WarehouseStockItem AS wsi " +

				 " LEFT OUTER JOIN wsi/Inventory.Located_at/Inventory.Warehouse AS w  " +
				 " LEFT OUTER JOIN wsi/Inventory.WarehouseStockItem_StockItem/Product.StockItem AS si " +  	 
				 " WHERE (wsi.AvailableQty > 0 OR wsi.ForwardDemand > 0) " + 		
				 " ORDER BY w_ShortName, si_SKU " ;		
		
		return strOQLQuery;
	}

	
	private IOQLTextGetRequest sumUnallocatedShotagesWholeSale (String id, IOQLTextGetRequest request) {
		// Double totalPackQuantity = 0.00;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		String OQLQuery =
			 " FROM  Inventory.WarehouseStockItem AS wsi "
			  + " INNER JOIN wsi/PurchaseOrders.Shortage_WarehouseStockItem/PurchaseOrders.Shortage AS s "
			  + " WHERE wsi.id = " + id  + " AND (s.Status = 'Pending' AND CAST(s.Wholesale as String) = 'true' ) " 
//			  + " WHERE wsi.id = " + id  + " AND (s.Status = 'Pending' AND CAST(s.Wholesale as String) = 'false' )" 
	
			+ " SELECT "
			+ " wsi.id AS wsi_id, "
			+ " s.Status, "
			+ " s.Wholesale,"
			+ " s.PackQuantity AS PackQuantity" ;
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// IOQLTextGetRequest request = Core.createOQLTextGetRequest();
		
	
		request.setQuery(OQLQuery);
		return request;
		
	}		
	
	
/*	
	private Double sumUnallocatedShotages_BAK (String id, IOQLTextGetRequest request) {
		Double totalPackQuantity = 0.00;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		String OQLQuery =
			 " FROM  Inventory.WarehouseStockItem AS wsi "
			  + " INNER JOIN wsi/PurchaseOrders.Shortage_WarehouseStockItem/PurchaseOrders.Shortage AS s "
			  + " WHERE wsi.id = " + id  + " AND (s.Status = 'Pending' AND CAST(s.Wholesale as String) = 'false' )" 
	
			+ " SELECT "
			+ " wsi.id AS wsi_id, "
			+ " s.Status, "
			+ " s.Wholesale,"
			+ " s.PackQuantity AS PackQuantity" ;
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// IOQLTextGetRequest request = Core.createOQLTextGetRequest();
		request.setQuery(OQLQuery);

		
		try {
			IDataTable resultDT = Core.retrieveOQLDataTable(getContext(), request);
		//	if (resultDT.getRowCount() == 0) return totalPackQuantity;
			
			List<? extends IDataRow> RowsList = resultDT.getRows(); 
			
			for (IDataRow row : RowsList) {
//				totalPackQuantity = totalPackQuantity + Double.parseDouble(row.getValue(getContext(), "PackQuantity").toString());
				totalPackQuantity = totalPackQuantity + (Double) row.getValue(getContext(), "PackQuantity");
			
			}
			System.out.println("nothing");
		} catch (CoreException e) {
			e.printStackTrace();
			}	
		return totalPackQuantity;
	}
	*/

		////////////////////////////////////////////////////////////////////////////////////////	

	// @formatter:on	

	// END EXTRA CODE
}
