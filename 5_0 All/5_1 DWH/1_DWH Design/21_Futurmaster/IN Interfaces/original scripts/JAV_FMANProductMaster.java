// This file was generated by Mendix Modeler.
//
// WARNING: Only the following code will be retained when actions are regenerated:
// - the import list
// - the code between BEGIN USER CODE and END USER CODE
// - the code between BEGIN EXTRA CODE and END EXTRA CODE
// Other code you write will be lost the next time you deploy the project.
// Special characters, e.g., é, ö, à, etc. are supported in comments.

package futurmaster.actions;

import java.util.List;
import com.mendix.core.Core;
import com.mendix.core.CoreException;
import com.mendix.systemwideinterfaces.connectionbus.data.IDataRow;
import com.mendix.systemwideinterfaces.connectionbus.data.IDataTable;
import com.mendix.systemwideinterfaces.connectionbus.requests.types.IOQLTextGetRequest;
import com.mendix.systemwideinterfaces.core.IContext;
import com.mendix.webui.CustomJavaAction;
import futurmaster.proxies.FMFTPConfigurations;
import com.mendix.systemwideinterfaces.core.IMendixObject;

public class JAV_FMANProductMaster extends CustomJavaAction<java.lang.Boolean>
{
	private IMendixObject __FMFTPConfigurationsParameter1;
	private futurmaster.proxies.FMFTPConfigurations FMFTPConfigurationsParameter1;

	public JAV_FMANProductMaster(IContext context, IMendixObject FMFTPConfigurationsParameter1)
	{
		super(context);
		this.__FMFTPConfigurationsParameter1 = FMFTPConfigurationsParameter1;
	}

	@Override
	public java.lang.Boolean executeAction() throws Exception
	{
		this.FMFTPConfigurationsParameter1 = __FMFTPConfigurationsParameter1 == null ? null : futurmaster.proxies.FMFTPConfigurations.initialize(getContext(), __FMFTPConfigurationsParameter1);

		// BEGIN USER CODE
        ////////////////////////////// AN3 //////////////////////////
        ////////////////////////////// AN3 //////////////////////////
        return  executeFMQuery(FMFTPConfigurationsParameter1);
        ////////////////////////////// AN3 //////////////////////////
        ////////////////////////////// AN3 //////////////////////////
        
		// END USER CODE
	}

	/**
	 * Returns a string representation of this action
	 */
	@Override
	public java.lang.String toString()
	{
		return "JAV_FMANProductMaster";
	}

	// BEGIN EXTRA CODE
    
    // @formatter:off
    
    private boolean executeFMQuery(FMFTPConfigurations FMFTPConfiguration) throws CoreException {
        
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////
        JAV_FMUtil jav_FMUtil = new JAV_FMUtil(getContext());
        /////////////////////////////////////////////////////////////////////////////// 
        StringBuilder sb = new StringBuilder();
        String delimiter = ";" ;
        int size, packsPerPallet;
        String fileName       = "ERP_FMAN_Product_Master.txt";
        /** "TEST_" will be auto appended to the beginning of the fileName String in the case in Dev **/
        String fileShortName = "*** AN3 - " ;
        ///////////////////////////////////////////////////////////////// 
//        boolean isInDevLocal  = false;  // 127.0.0.1
//        boolean isInDevLive   = false;  // 52.30.165.176 "//VD_Test
//        boolean isLive        = false;  // 52.30.165.176 "//       
        //////////////////////////////////////////////////////////////// 
//      isInDevLocal          = true;   // 127.0.0.1
//      isInDevLive           = true;   // 52.30.165.176 "//VD_Test
        //isLive                = true;   // 52.30.165.176 "//" Live
        ///////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////             
        
        String oqlQuery = getFMOQLQuery();
        
        IOQLTextGetRequest request = Core.createOQLTextGetRequest();
        request.setQuery(oqlQuery);
        IDataTable resultDT;
        try {
            resultDT = Core.retrieveOQLDataTable(getContext(), request);
            List<? extends IDataRow> RowsList     = resultDT.getRows(); 
            
            for (IDataRow row : RowsList) {
                
                size			= row.getValue(getContext(),  "col09");		// col09 -> coalesce(si.PackSize,0)
                packsPerPallet 	= row.getValue(getContext(),  "PacksPerPallet");
                
                sb.append((String)row.getValue(getContext(),  "col01")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col02")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col03")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col04")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col05")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col06")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col07")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col08")) ;
                sb.append(delimiter);
                
//              sb.append(row.getValue(getContext(), "col09")) ;              // ps.Size
                sb.append(size) ;                                             // ps.Size - replaces col09
                sb.append(delimiter);
                
                sb.append((String) row.getValue(getContext(), "col10")) ;
                sb.append(delimiter);
                sb.append((int) row.getValue(getContext(),    "col11")) ;       // ps.packsPerCarton
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col12")) ;
                sb.append(delimiter);
                
                ///////////////////////// col13 ////////////////////
//              sb. append(row.getValue(getContext(), "col13")) ;
                sb.append(packsPerPallet * size) ;                              // replaces col13
                sb.append(delimiter);
                
                sb.append((String) row.getValue(getContext(), "col14")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col15")) ;
                sb.append(delimiter);
                
                ///////////////////////// col16 ////////////////////
                sb.append(size) ;                                               // ps.Size  replaces col16
                sb.append(delimiter);
                
                sb.append((String) row.getValue(getContext(), "col17")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col18")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col19")) ;     
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col20")) ;
                sb.append(delimiter);
                sb.append((String) row.getValue(getContext(), "col21")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col22")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col23")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col24")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col25")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col26")) ;
                sb.append(delimiter);               
                sb.append((int) row.getValue(getContext(),    "col27")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col28")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col29")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col30")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col31")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col32")) ;
                sb.append(delimiter);               
                sb.append((String) row.getValue(getContext(), "col33")) ;
                
                sb.append("\r\n");
            }
            
            ///////////////////////////////////////////////////
            jav_FMUtil.sFtpProcess(FMFTPConfiguration, sb,  fileShortName, fileName);
            ///////////////////////////////////////////////////
            
                    
        } catch (CoreException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }   
        
        
        return true;
        
    }   
    
    
   private String getFMOQLQuery () {
        
        ///////////////////////////////////////// success //////////////////////////////////////////        
        ////////////////////////////////////////////////////////////////////////////////////////////
        String strFMOQLQuery =
                
//              " SELECT DISTINCT " + 
                " SELECT  " + 
                
                        " coalesce(si.SKU,'')                          AS col01 , " +
                        " coalesce(si.StockItemDescription,'')         AS col02 , " +
                        " ''                                           AS col03 , " +
                        " ''                                           AS col04 , " +
                        " ''                                           AS col05 , " +
                        " ''                                           AS col06 , " +     
                        " ''                                           AS col07 , " +
                        " 'Lenses'                                     AS col08 , " + 
                        " coalesce(si.PackSize,0)                      AS col09 , " +     // PackSize.size - Alex Y. new changes
                        " 'Packs'                                      AS col10 , " +
                        " coalesce(ps.PacksPerCarton,1)                AS col11 , " +     // Akex Y.  ps.PacksPerCarton
                        " 'Cartons'                                    AS col12 , " +
                        " ''							               AS col13 , " +     // Calculated field 
                        " 'Pallets'                                    AS col14 , " +
                        " ''                                           AS col15 , " +
//                      " coalesce(si.PackSize,0)                      AS col16 , " +     // PackSize.size same as col09
                        

                        " coalesce(cl.PO, '')                          AS col17 , " +
                        " coalesce(cl.DI, '')                          AS col18 , " +
                        " coalesce(cl.BC, '')                          AS col19 , " +
                        " coalesce(cl.AX, '')                          AS col20 , " +
                        " coalesce(cl.AD, '')                          AS col21 , " +
                        " coalesce(cl._DO,'')                          AS col22 , " +
                        " coalesce(cl.CO, '')                          AS col23 , " +
                        " coalesce(cl.CY, '')                          AS col24 , " +
                        
//                        "  ''				                           AS col17 , " +
//                        "  ''				                           AS col18 , " +
//                        "  ''				                           AS col19 , " +
//                        "  ''				                           AS col20 , " +
//                        "  ''				                           AS col21 , " +
//                        "  ''				                           AS col22 , " +
//                        "  ''				                           AS col23 , " +
//                        "  ''				                           AS col24 , " +
                         
                        
                        " coalesce(bc.UPCCode,'')                      AS col25 , " +
                        " coalesce(pf.Status,'')                       AS col26 , " +
                        " ps.PurchasingPreferenceOrder                 AS col27 , " + 
                        " coalesce(pf.Manufacturer, '')                AS col28 , " +
                        " ''                                           AS col29 , " +
                        " ''                                           AS col30 , " +
                        " ''                                           AS col31 , " +
                        " ''                                           AS col32 , " +
                        " ''                                           AS col33 , " +
                        
//                      " coalesce(ps.PacksPerCarton,0)                AS PacksPerCarton , " +
                        " coalesce(ps.PacksPerPallet,0)                AS PacksPerPallet " +
                        
                        
						" FROM  Product.Barcode AS bc " +
						" LEFT OUTER JOIN bc/Product.Barcode_StockItem/Product.StockItem AS si  " +
                        " LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize AS ps " +
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product AS p "   + 
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +  // remed to include solutions
                        " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily AS pf " +               
                        " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory AS pc " +
                        " WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')";
        
/*                      
                        " FROM  Product.StockItem AS si " +
                        " LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize                   AS ps " +
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product                     AS p  " +
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens                 AS cl " + 
                        " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily            AS pf " +               
                        " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory AS pc " +
                        " LEFT OUTER JOIN si/Product.Barcode_StockItem/Product.Barcode                     AS bc " +              
                        " WHERE bc.IsCurrent "; 
        
*/

        
//                      " LEFT OUTER JOIN si/Product.BOM_StockItem/Product.BOMQuantity                      AS b " +
//                      " LEFT OUTER JOIN b/Product.Parent_Product/Product.Product                          AS p2 " +
//                      " WHERE bc.IsCurrent "; 
        
        //" LIMIT 1000 ";
        ///////////////////////////////////////////////////////////////////////////////////////////////
        
        return strFMOQLQuery;
    }       
    
    
  //  private String getFMOQLQuery () {
   	private String getFMOQLQuery_original () {
        
        ///////////////////////////////////////// success //////////////////////////////////////////        
        ////////////////////////////////////////////////////////////////////////////////////////////
        String strFMOQLQuery =
                
//              " SELECT DISTINCT " + 
                " SELECT  " + 
                
                        " coalesce(si.SKU,'')                          AS col01 , " +
                        " coalesce(si.StockItemDescription,'')         AS col02 , " +
                        " ''                                           AS col03 , " +
                        " ''                                           AS col04 , " +
                        " ''                                           AS col05 , " +
                        " ''                                           AS col06 , " +     
                        " ''                                           AS col07 , " +
                        " 'Lenses'                                     AS col08 , " + 
                        " coalesce(si.PackSize,0)                      AS col09 , " +     // PackSize.size - Alex Y. new changes
                        " 'Packs'                                      AS col10 , " +
                        " coalesce(ps.PacksPerCarton,1)                AS col11 , " +     // Akex Y.  ps.PacksPerCarton
                        " 'Cartons'                                    AS col12 , " +
                        " ''							               AS col13 , " +     // Calculated field 
                        " 'Pallets'                                    AS col14 , " +
                        " ''                                           AS col15 , " +
//                      " coalesce(si.PackSize,0)                      AS col16 , " +     // PackSize.size same as col09
                        " coalesce(cl.PO, '')                          AS col17 , " +
                        " coalesce(cl.DI, '')                          AS col18 , " +
                        " coalesce(cl.BC, '')                          AS col19 , " +
                        " coalesce(cl.AX, '')                          AS col20 , " +
                        " coalesce(cl.AD, '')                          AS col21 , " +
                        " coalesce(cl._DO, '')                         AS col22 , " +
                        " coalesce(cl.CO, '')                          AS col23 , " +
                        " coalesce(cl.CY, '')                          AS col24 , " +
                        " coalesce(bc.UPCCode,'')                      AS col25 , " +
                        " coalesce(pf.Status,'')                       AS col26 , " +
                        " ps.PurchasingPreferenceOrder                 AS col27 , " + 
                        " coalesce(pf.Manufacturer, '')                AS col28 , " +
                        " ''                                           AS col29 , " +
                        " ''                                           AS col30 , " +
                        " ''                                           AS col31 , " +
                        " ''                                           AS col32 , " +
                        " ''                                           AS col33 , " +
                        
//                      " coalesce(ps.PacksPerCarton,0)                AS PacksPerCarton , " +
                        " coalesce(ps.PacksPerPallet,0)                AS PacksPerPallet " +
                        
                        
                        " FROM  Product.StockItem AS si " +
                        " INNER JOIN si/Product.StockItem_PackSize/Product.PackSize     AS ps " +
                        " INNER JOIN si/Product.StockItem_Product/Product.Product AS p"   + 
                        " INNER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +
                        //" LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens AS cl " +  // remed to include solutions
                        " INNER JOIN p/Product.Product_ProductFamily/Product.ProductFamily AS pf " +               
                        " INNER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory  AS pc " +
                        " INNER JOIN si/Product.Barcode_StockItem/Product.Barcode AS bc " +                       
                        " WHERE bc.IsCurrent and pc.Code NOT IN ('05', '07', '08', '09', '10', '11', '12', '13')";
        
/*                      
                        " FROM  Product.StockItem AS si " +
                        " LEFT OUTER JOIN si/Product.StockItem_PackSize/Product.PackSize                   AS ps " +
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.Product                         AS p  " +
                        " LEFT OUTER JOIN si/Product.StockItem_Product/Product.ContactLens                     AS cl " + 
                        " LEFT OUTER JOIN p/Product.Product_ProductFamily/Product.ProductFamily            AS pf " +               
                        " LEFT OUTER JOIN pf/Product.ProductFamily_ProductCategory/Product.ProductCategory     AS pc " +
                        " LEFT OUTER JOIN si/Product.Barcode_StockItem/Product.Barcode                     AS bc " +              
                        " WHERE bc.IsCurrent "; 
        
*/

        
//                      " LEFT OUTER JOIN si/Product.BOM_StockItem/Product.BOMQuantity                      AS b " +
//                      " LEFT OUTER JOIN b/Product.Parent_Product/Product.Product                          AS p2 " +
//                      " WHERE bc.IsCurrent "; 
        
        //" LIMIT 1000 ";
        ///////////////////////////////////////////////////////////////////////////////////////////////
        
        return strFMOQLQuery;
    }       
    
    

   	
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // @formatter:on    
	// END EXTRA CODE
}
