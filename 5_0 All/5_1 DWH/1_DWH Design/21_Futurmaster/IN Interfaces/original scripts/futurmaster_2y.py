#! /usr/bin/python
# -*- coding: utf-8 -*-

import MySQLdb
import psycopg2
import math

P_PACKS_SQL = """
select magentoproductid, size from product$productfamily
    JOIN product$packsize_productfamily ON product$packsize_productfamily.product$productfamilyid = product$productfamily.id
    JOIN product$packsize on product$packsize.id = product$packsize_productfamily.product$packsizeid order by magentoproductid;
"""

P_PROD_KEYS = """
select thetype,value,code from product$keycodes;
"""

M_ORD_HISTORY_Q = """
select order_no, product_id, sku as SKU, concat(oh.customer_id, '_VD-',
  CASE WHEN st.store_name like '%.uk%' THEN 'UK'
       WHEN st.store_name like '%.it%' THEN 'IT'
       WHEN st.store_name like '%.ie%' THEN 'IE'
       WHEN st.store_name like '%.es%' THEN 'ES'
       WHEN st.store_name like '%.ca%' THEN 'CA'
       WHEN st.store_name like '%.be%' THEN 'BE'
       WHEN st.store_name like '%.fr%' THEN 'FR'
       WHEN st.store_name like '%.nl%' THEN 'NL'
       ELSE 'GB'
  END

                   ) as Customer_Country, concat(CASE WHEN country_id = 'GB' THEN 'UK' ELSE country_id END ,'_RETAIL') as warehouse, YEAR(oh.created_at) as year, MONTH(oh.created_at) as month, DAY(oh.created_at) as day, FLOOR(qty_ordered) as qty,
                   olv.global_line_subtotal_exc_vat as price
          from dw_order_lines join dw_order_headers oh using (order_id) JOIN dw_order_address oa on oa.order_id = oh.order_id and address_type = 'shipping'
                          JOIN dw_stores st on oh.store_id = st.store_id JOIN order_lines_vat olv using (item_id)
                          where cast(oh.created_at as date) between  DATE(now()) - INTERVAL 2 YEAR  AND (DATE(NOW()) - INTERVAL DAY(NOW()) DAY) order by oh.created_at asc, order_no asc;
"""
P_VALIDATE_SKU = """
select distinct
sku,oldsku,manufacturercodenumber,packsize
from "product$stockitem"
where sku = '{sku}'
"""

packsizes = {2316 : [90,30],
             1226: [4,2,1],
             1101: [90,30], #1 Month (30 pairs)|3 Months (90 pairs)|6 months + 2 weeks free (180 + 15 pairs)|12 Months + 1 month free (360 + 30 pairs)
             1127: [12,6], #6 Months (6 Pairs)|12 Months (12 + 1 Pairs)
             1145: [6,4,1], #1pack|4 packs|6 packs
             1178: [6,4,2,1],#1pack|4 packs|6 packs
             1181: [6,4,2,1],#1pack|2 packs|4 packs|6 packs
             1190: [4,2,1],#1 pack|2 packs - free delivery|4 packs - save £5
             1206: [4,2,1],#3 Months Supply (1 pack)|6 Months Supply (2 packs - free delivery)|12 Months Supply (4 packs for price of 3)
             1360: [6,2,1],#3 Months Supply (1 pack)|6 Months Supply (2 packs - free delivery)|12 Months Supply
             2307: [5,3,1],#1 pack|3 packs|5 packs - free delivery and save an extra £5
             2313: [4,2,1],#3 Months Supply (1 pack)|6 Months Supply (2 packs - free delivery)|12 Months Supply (4 packs - save £5)
             2314: [6,4,2,1],#3 Months Supply (1 pack)|6 Months Supply (2 packs - free delivery)|12 Months Supply (4 packs - save £5)
             2322: [4,2,1],#1 Months Supply (1 pack)|2 Months Supply (2 packs)|4 Months Supply (4 packs - free delivery)
             2327: [1],#
             2330: [5,3,1],#1 pack|3 packs|5 packs - free delivery and save an extra £5
             2332: [6,3,1],#3 Months (3 Pairs)|6 Months (6 Pairs)|12 Months + 1 month free (12 + 1 Pairs)
             3141: [2,1],#1 pair|2 pairs
             }  # key: prodid (old AURA not in ERP)
prodkeys = {}  # key: par+value
validSkus = {}


import pysftp
cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

def upload(filenames = None):
    with pysftp.Connection('52.30.165.176', username='fmsftpuser1', password='Wha1%6tGUd', cnopts=cnopts, port=22) as sftp:
        #with sftp.cd('store'):
        for fn in filenames:
            try:
                sftp.remove(fn)
            except:
                pass
            sftp.put('store/'+fn)

def connectDB():
    global cnx, cursor, pcnx, pcur
    cnx = MySQLdb.connect(user='root', passwd='SoMlInexPA',
                                  host='10.0.1.141',
                                  db ='dw_flat')
    cursor = cnx.cursor()
    pcnx = psycopg2.connect("dbname='database' user='username' host='erp-bi-prod.visiondirect.info' password='password'")

    pcur = pcnx.cursor()

def closeDB():
    global cnx, cursor, pcnx, pcur
    cursor.close()
    cnx.close()
    pcur.close()
    pcnx.close()

def getNewSKU(prodid, oldsku, packsize = None):

    global prodkeys, validSkus
    newsku = '0'
    newsku += ('%04d' % prodid)
    i = oldsku.find('-')
    pars = ['BC','DI','PO','CY','DO','AD','AX','CO']

    if i > 0:
        oldsku = oldsku[i:]
        for p in pars:
            i = oldsku.find('-'+p)
            if i<0:
                newsku+='00'
                continue
            t = oldsku[i+3:]
            i = t.find('-',1)
            if i<0:
                newsku+= prodkeys[p+t]
            else:
                newsku += prodkeys[p + t[:i]]

    else:
        newsku+='0000000000000000'

    if packsize:
        if prodid == 1136 and packsize == 30:
            newsku += ('%02d2' % packsize)
        else:
            newsku += ('%02d1' % packsize)
    # check if this is a valid sku
    try:
        if(validSkus[newsku] == True):
            return newsku
    except KeyError:
        # if not in list of valid sku query erp to see if this exists
        # NOTE this is not good fpr made to order products will check if this matters
        pcur.execute(P_VALIDATE_SKU.format(sku=newsku))
        l = pcur.fetchall()
        found = False
        for r in l:
            found = True
            validSkus[newsku] = True
            return newsku
        if found == False:
            validSkus[newsku] = False
            raise ValueError('Genrated sku is not valid debug output oldsku,newsku,packsize', oldsku,newsku,packsize)
    # if not valid and but no index error it must an invalid sku
    raise ValueError('Genrated sku is not valid debug output oldsku,newsku,packsize', oldsku,newsku,packsize)

def printOrder(order, r, fq, fv, fe):
    # r = ('8001223953', 1882, 'BM1DE-BC8.6-DI14.2-PO-3.50', '720655_GB_VD-GB', 'N/A', 2015, 5, 25, 30)
    global packsizes
    for o in order.keys():
        prodid= order[o][2]
        lens = order[o][0]
        val = order[o][1]

        if not r[3] or r[3] == 'None':
            print(r)

        try:
            lv = val/lens
        except:
            print('val/lens error %s:' % r[0])
            print(order)
            lv = 0
        try:
            sizes = packsizes[prodid]
        except:
            sizes = [1]

        # try biggest packsize first
        sizes.sort(reverse = True)
        sku = "ERROR-" + o # if it doesnt get set must have been a real problem
        for ps in sizes:
            try:
                sku = getNewSKU(prodid,o,ps)
            except ValueError as err:
                print(err.args)
                continue

            s = int(math.floor(lens / ps))
            lens = lens % ps
            if s > 0:
                #Q 01083B2D4AS0000000030;12345_VD-UK;UK_RETAIL;2017;01;23;60

                fq.write("%s;%s;%s;%s;%s;%s;%s\n" % (sku, r[3], r[4], r[5], r[6], r[7], s * ps))
                #V

                #01083B2D4AS0000000030;12345_VD-UK;UK_RETAIL;2017;01;23;25.98
                fv.write("%s;%s;%s;%s;%s;%s;%0.2f\n" % (sku, r[3], r[4], r[5], r[6], r[7], s * ps * lv))

                #01083B2D4AS0000000030;12345_VD-UK;UK_RETAIL;2017;01;23;60
                fe.write("%s;%s;%s;%s;%s;%s;%s\n" % (sku, r[3], r[4], r[5], r[6], r[7], s * ps))

        # anything left over here woulde be a bad sign this is almost ceratainly due to bad order quantiy in magento
        if lens>0:
            print("Error unexpected remaining qty %s  sku: %s , order qty %s , order id  %s" % (lens,sku,order[o][0],r[0]))


def main():
    connectDB()

    global packsizes
    global prodkeys

    # packsizes
    pcur.execute(P_PACKS_SQL)

    l = pcur.fetchall()
    for i in l:
        try:
            pid = int(i[0])
        except:
            #print(i)
            continue
        if pid in packsizes.keys():
            try:
                packsizes[pid].index(int(i[1]))
            except:
                packsizes[pid].append(int(i[1]))
        else:
            packsizes[pid] = [int(i[1])]
        packsizes[pid].sort(reverse = True)

    # f = open("productids.txt", 'tr')
    # l = f.readline()
    # while l:
    #     try:
    #         packsizes[int(l[:-1])]
    #     except:
    #         print(l[:-1]+',')
    #     l = f.readline()
    #
    # exit()
    # codes

    pcur.execute(P_PROD_KEYS)

    l = pcur.fetchall()
    for i in l:
        x = i[0]
        if i[0][:1] == '_':
            x = i[0][1:]
        prodkeys[x+i[1]] = i[2]

    # orders_q
    cursor.execute(M_ORD_HISTORY_Q)

    cn = 0
    l = cursor.fetchall()

    fq = open('store/ERP_FMAN_SALES_HISTORY_D_2Y.TXT', 'wt')
    fv = open('store/ERP_FMAN_SALES_VALUE_D_2Y.TXT', 'wt')
    fe = open('store/ERP_FMANA_EXACT_SALES_D_2Y.TXT', 'wt')

    #print('loaded...')
    order = {}
    oid = None
    for r in l:
        #cn += 1
        #if cn % 100000 == 0:
        #    print(cn)
        if r[0] == oid:
            if r[2] in order.keys():
                order[r[2]][0] += r[8]
                if r[9]:
                    order[r[2]][1] += r[9]
            else:
                order[r[2]] = [r[8], r[9], r[1]]
        else:
            #process order
            if oid:
                printOrder(order, pr, fq, fv, fe)
            oid = r[0]
            order = {}
            order[r[2]] = [r[8],r[9], r[1]]

        pr = r
        #r = cursor.fetchone()

    printOrder(order, pr, fq, fv, fe)

    fq.close()
    fv.close()
    fe.close()

    closeDB()

if __name__ == '__main__':
    main()
    upload(['ERP_FMAN_SALES_HISTORY_D_2Y.TXT', 'ERP_FMAN_SALES_VALUE_D_2Y.TXT', 'ERP_FMANA_EXACT_SALES_D_2Y.TXT'])
