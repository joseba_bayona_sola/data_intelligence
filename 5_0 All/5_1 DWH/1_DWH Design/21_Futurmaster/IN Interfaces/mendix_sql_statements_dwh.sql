
----------------------------Product Hierarchy--------------------------

select top 1000 count(*) over (), count(*) over (partition by si.sku) num_rep,
	coalesce(si.SKU, '') AS col01, coalesce(si.StockItemDescription,'')	AS col02, coalesce(p.SKU, '') AS col03, 
	coalesce(p.Description,'') AS col04, coalesce(pf.MagentoProductID,'') AS col05, coalesce(pf.ManufacturerProductFamily,'') AS col06,              
	coalesce(pc.Name,'') AS col07, '' AS col08, coalesce(p.ProductType,'') AS col09, '' AS col10, 'All_Products' AS c0l11
	-- , 	bc.*   
from  
		Landing.mend.prod_stockitem AS si 
	left outer join 
		Landing.mend.prod_stockitem_product si_p on si.id = si_p.stockitemid
	left outer join 
		Landing.mend.prod_product p on si_p.productid = p.id	
	left outer join 
		Landing.mend.prod_product_productfamily p_pf on p.id = p_pf.productid
	left outer join 
		Landing.mend.prod_productfamily pf on p_pf.productfamilyid = pf.id
	left outer join 
		Landing.mend.prod_productfamily_productcategory pf_pc on pf.id = pf_pc.productfamilyid
	left outer join 
		Landing.mend.prod_productcategory pc on pf_pc.productcategoryid = pc.id
	--left join
	--	Landing.mend.prod_barcode_stockitem bc_si on si.id = bc_si.stockitemid
	--left join
	--	Landing.mend.prod_barcode bc on bc_si.barcodeid = bc.id
	--pc.code filter also excludes all rows
WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
order by num_rep desc, si.sku

	select coalesce(p.ProductType,''), coalesce(pc.Name,''), count(*)
	from  
			Landing.mend.prod_stockitem_aud AS si 
		left outer join 
			Landing.mend.prod_stockitem_product_aud si_p on si.id = si_p.stockitemid
		left outer join 
			Landing.mend.prod_product_aud p on si_p.productid = p.id	
		left outer join 
			Landing.mend.prod_product_productfamily_aud p_pf on p.id = p_pf.productid
		left outer join 
			Landing.mend.prod_productfamily_aud pf on p_pf.productfamilyid = pf.id
		left outer join 
			Landing.mend.prod_productfamily_productcategory_aud pf_pc on pf.id = pf_pc.productfamilyid
		left outer join 
			Landing.mend.prod_productcategory_aud pc on pf_pc.productcategoryid = pc.id
	WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')
	group by coalesce(p.ProductType,''), coalesce(pc.Name,'')
	order by coalesce(p.ProductType,''), coalesce(pc.Name,'')

----------------------------Product Master --------------------------

select top 1000 coalesce(si.SKU,'') AS col01, coalesce(si.StockItemDescription,'') AS col02, '' AS col03, 
	'' AS col04, '' AS col05, '' AS col06, '' AS col07, 
	'Lenses' AS col08, coalesce(si.PackSize,0) AS col09, -- PackSize.size - Alex Y. new changes
	'Packs' AS col10, 
	coalesce(ps.PacksPerCarton,1) AS col11, -- Akex Y.  ps.PacksPerCarton
	'Cartons' AS col12, '' AS col13, -- Calculated field 
	'Pallets' AS col14, '' AS col15, 
	coalesce(si.PackSize,0) AS col16, -- PackSize.size same as col09
	coalesce(cl.PO, '') AS col17, coalesce(cl.DI, '') AS col18, coalesce(cl.BC, '') AS col19, coalesce(cl.AX, '') AS col20, coalesce(cl.AD, '') AS col21, coalesce(cl._DO, '') AS col22, coalesce(cl.CO, '') AS col23, coalesce(cl.CY, '') AS col24, 
	-- coalesce(bc.UPCCode,'') AS col25, 
	coalesce(pf.Status,'') AS col26, ps.PurchasingPreferenceOrder AS col27, coalesce(pf.Manufacturer, '') AS col28, 
	'' AS col29, '' AS col30, '' AS col31, '' AS col32, '' AS col33, 
	coalesce(ps.PacksPerCarton,0) AS PacksPerCarton, coalesce(ps.PacksPerPallet,0) AS PacksPerPallet 
from  
		Landing.mend.prod_stockitem_aud AS si 
	left outer join
		Landing.mend.prod_stockitem_packsize si_ps on si.id = si_ps.stockitemid
	left outer join
		Landing.mend.prod_packsize ps on si_ps.packsizeid = ps.id
	left outer join 
		Landing.mend.prod_stockitem_product_aud si_p on si.id = si_p.stockitemid
	left outer join 
		Landing.mend.prod_product_aud p on si_p.productid = p.id	
	left outer join
		Landing.mend.prod_contactlens cl on si_p.productid = cl.id
	left outer join 
		Landing.mend.prod_product_productfamily_aud p_pf on p.id = p_pf.productid
	left outer join 
		Landing.mend.prod_productfamily_aud pf on p_pf.productfamilyid = pf.id
	left outer join 
		Landing.mend.prod_productfamily_productcategory_aud pf_pc on pf.id = pf_pc.productfamilyid
	left outer join 
		Landing.mend.prod_productcategory_aud pc on pf_pc.productcategoryid = pc.id
	--missing barcode joins
	--pc.code filter also excludes all rows
WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13')


----------------------------------------------------------------------------------
----------------------------------------------------------------------------------


--DRP1 AVAILABLE STOCK

	select top 1000 coalesce(si.sku, '') as si_sku, coalesce(wh.shortname, '') + '_RETAIL' as wh_shortname, coalesce(wsi.availableqty, 0) as wsi_availableqty
	from
			landing.mend.wh_warehousestockitem_aud wsi
		left outer join 
			landing.mend.wh_located_at_aud lat on wsi.id = lat.warehousestockitemid
		left outer join 
			landing.mend.wh_warehouse_aud wh on lat.warehouseid = wh.id
		left outer join 
			landing.mend.wh_warehousestockitem_stockitem_aud wsi_si on wsi.id = wsi_si.warehousestockitemid
		left outer join 
			landing.mend.prod_stockitem_aud si on wsi_si.stockitemid = si.id
	where wsi.availableqty > 0 
	order by wh.shortname, si.sku;

--DRP2 UNALLOCATED STOCK

--Retail Mendix
	select top 1000 coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') + '_RETAIL' as wh_shortname, coalesce(si.sku, '') as si_sku
	from
			landing.mend.wh_warehousestockitem_aud wsi
		left outer join 
			landing.mend.wh_located_at_aud lat on wsi.id = lat.warehousestockitemid
		left outer join 
			landing.mend.wh_warehouse_aud wh on lat.warehouseid = wh.id
		left outer join 
			landing.mend.wh_warehousestockitem_stockitem_aud wsi_si on wsi.id = wsi_si.warehousestockitemid
		left outer join 
			landing.mend.prod_stockitem_aud si on wsi_si.stockitemid = si.id
	where wsi.availableqty > 0 or wsi.forwarddemand > 0 
	order by wh.shortname, si.sku;

	select wsi.id as wsi_id, sh.status, sh.wholesale, sh.packquantity as packquantity
	from 
			Landing.mend.wh_warehousestockitem_aud wsi 
		inner join 
			Landing.mend.short_shortage_warehousestockitem sh_wsi on sh_wsi.warehousestockitemid = wsi.id
		inner join 
			Landing.mend.short_shortage sh on sh_wsi.shortageid = sh.id
		inner join
			(select coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') + '_RETAIL' as wh_shortname, coalesce(si.sku, '') as si_sku
			from
					landing.mend.wh_warehousestockitem_aud wsi
				left outer join 
					landing.mend.wh_located_at_aud lat on wsi.id = lat.warehousestockitemid
				left outer join 
					landing.mend.wh_warehouse_aud wh on lat.warehouseid = wh.id
				left outer join 
					landing.mend.wh_warehousestockitem_stockitem_aud wsi_si on wsi.id = wsi_si.warehousestockitemid
				left outer join 
					landing.mend.prod_stockitem_aud si on wsi_si.stockitemid = si.id
			where wsi.availableqty > 0 or wsi.forwarddemand > 0) t on wsi.id = t.wsi_id
	where sh.status = 'Pending' and sh.wholesale = 0;

--Wholesale Mendix

	select wsi.id as wsi_id, sh.status, sh.wholesale, sh.packquantity as packquantity
	from 
			Landing.mend.wh_warehousestockitem_aud wsi 
		inner join 
			Landing.mend.short_shortage_warehousestockitem sh_wsi on sh_wsi.warehousestockitemid = wsi.id
		inner join 
			Landing.mend.short_shortage sh on sh_wsi.shortageid = sh.id
		inner join
			(select coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') + '_RETAIL' as wh_shortname, coalesce(si.sku, '') as si_sku
			from
					landing.mend.wh_warehousestockitem_aud wsi
				left outer join 
					landing.mend.wh_located_at_aud lat on wsi.id = lat.warehousestockitemid
				left outer join 
					landing.mend.wh_warehouse_aud wh on lat.warehouseid = wh.id
				left outer join 
					landing.mend.wh_warehousestockitem_stockitem_aud wsi_si on wsi.id = wsi_si.warehousestockitemid
				left outer join 
					landing.mend.prod_stockitem_aud si on wsi_si.stockitemid = si.id
			where wsi.availableqty > 0 or wsi.forwarddemand > 0) t on wsi.id = t.wsi_id
	where sh.status = 'Pending' and sh.wholesale = 1;

--DRP3 SCHEDULED RECEIPTS

select top 1000 count(*) over (), 
	coalesce(pol.PO_LineID, 0) AS PO_LineID, coalesce(si.PackSize, 0) AS PackSize, coalesce(r.ShipmentType, '') AS ShipmentType,
	coalesce(rl.StockItem, '') AS col01, coalesce(wh.ShortName,   '') AS col02, coalesce(po.OrderNumber,'') AS col03, 
	coalesce(rl.QuantityOrdered, 0.0) AS col04,  
	rl.Created AS col05,
	coalesce(r.Status, '') AS col17, coalesce(r.ReceiptId, '') AS col18, coalesce(po.Status, '') AS col19, coalesce(po.Source, '') AS col20, coalesce(su.SupplierName, '') AS col21, 
	coalesce(rl.UnitCost, 0.0) AS col27, 
	coalesce(rl.QuantityAccepted, 0.0) AS col28,
	r.createdDate AS col37, po.createdDate AS col40, r.DueDate AS col43, r.ArrivedDate AS col46, r.ConfirmedDate AS col49, r.StockRegisteredDate AS col52 
from
		Landing.mend.whship_receiptline rl
	left outer join 
		Landing.mend.whship_receiptline_receipt rl_r on rl.id = rl_r.receiptlineid
	left outer join 
		Landing.mend.whship_receipt r on rl_r.receiptid = r.id
	left outer join 
		Landing.mend.whship_receipt_warehouse r_wh on r.id = r_wh.receiptid
	left outer join 
		Landing.mend.wh_warehouse wh on r_wh.warehouseid = wh.id
	left outer join 
		Landing.mend.whship_receiptline_purchaseorderline rl_pol on rl.id = rl_pol.receiptlineid
	left outer join 
		Landing.mend.purc_purchaseorderline pol on rl_pol.purchaseorderlineid = pol.id
	left outer join 
		Landing.mend.purc_purchaseorderline_stockitem pol_si on pol.id = pol_si.purchaseorderlineid
	left outer join 
		Landing.mend.prod_stockitem_aud si on pol_si.stockitemid = si.id
	left outer join 
		Landing.mend.purc_purchaseorderline_purchaseorderlineheader pol_polh on pol.id = pol_polh.purchaseorderlineid
	left outer join 
		Landing.mend.purc_purchaseorderlineheader polh on pol_polh.purchaseorderlineheaderid = polh.id
	left outer join 
		Landing.mend.purc_purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorderlineheaderid
	left outer join 
		Landing.mend.purc_purchaseorder po on polh_po.purchaseorderid = po.id
	left outer join 
		Landing.mend.purc_purchaseorder_supplier po_su on po.id = po_su.purchaseorderid
	left outer join 
		Landing.mend.supp_supplier su on po_su.supplierid = su.id
where r.status NOT IN ('Cancelled', 'Stock_Registered', 'Aggregated')
	and rl.StockItem = '02317B3D4CF0000000000301'

--DRP4 BOM PRODUCTS

	select coalesce(p.sku, '') as pSKU, coalesce(si.sku,'') as siSKU
	from
			Landing.mend.prod_product_aud p 
		left outer join 
			Landing.mend.prod_stockitem_product_aud si_p on p.id = si_p.productid
		left outer join 
			Landing.mend.prod_stockitem_aud si on si_p.stockitemid = si.id 
	where p.isbom = 1
	order by p.sku 

	select p_orig.pSKU, p_orig.siSKU, coalesce(si.sku,''), coalesce(bom.quantityper, 0)
	from 
			Landing.mend.prod_product_aud as p 
		left outer join 
			Landing.mend.prod_parent_product_aud p_p on p.id = p_p.productid
		left outer join 
			Landing.mend.prod_bomquantity_aud bom on p_p.bomquantityid = bom.id
		left outer join 
			Landing.mend.prod_bom_stockitem_aud b_si on bom.id = b_si.bomquantityid
		left outer join 
			Landing.mend.prod_stockitem_aud si on b_si.stockitemid = si.id
		inner join
			(select coalesce(p.sku, '') as pSKU, coalesce(si.sku,'') as siSKU
			from
					Landing.mend.prod_product_aud p 
				left outer join 
					Landing.mend.prod_stockitem_product_aud si_p on p.id = si_p.productid
				left outer join 
					Landing.mend.prod_stockitem_aud si on si_p.stockitemid = si.id 
			where p.isbom = 1) p_orig on p.sku = p_orig.pSKU  		
	-- where p.sku = ''
	order by p_orig.siSKU, si.sku
