﻿
----------------------------Product Hierarchy--------------------------


select coalesce(si.SKU, '') AS col01, coalesce(si.StockItemDescription,'') AS col02, coalesce(p.SKU, '') AS col03, 
	coalesce(p.Description,'') AS col04, coalesce(pf.MagentoProductID,'') AS col05, coalesce(pf.ManufacturerProductFamily,'') AS col06,              
	coalesce(pc.Name,'') AS col07, '' AS col08, coalesce(p.ProductType,'') AS col09, '' AS col10, 'All_Products' AS c0l11   
from  
		Product$StockItem AS si 
	left outer join 
		Product$StockItem_Product  si_p on si.id = si_p.product$stockitemid
	left outer join 
		Product$Product p on si_p.product$productid = p.id	
	left outer join 
		Product$Product_ProductFamily p_pf on p.id = p_pf.Product$productid
	left outer join 
		Product$ProductFamily pf on p_pf.product$productfamilyid = pf.id
	left outer join 
		Product$ProductFamily_ProductCategory pf_pc on pf.id = pf_pc.product$productfamilyid
	left outer join 
		Product$ProductCategory pc on pf_pc.product$productcategoryid = pc.id 
	left outer join 
		product$barcode_stockitem bc_si on si.id = bc_si.product$stockitemid 
	left outer join 
		product$barcode bc on bc_si.product$barcodeid = bc.id 
--pc.Code filter excludes all rows in mendix		
WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13') limit 50

----------------------------Product Master --------------------------

select coalesce(si.SKU,'') AS col01, coalesce(si.StockItemDescription,'') AS col02, '' AS col03, 
	'' AS col04, '' AS col05, '' AS col06, '' AS col07, 
	'Lenses' AS col08, coalesce(si.PackSize,0) AS col09, -- PackSize.size - Alex Y. new changes
	'Packs' AS col10, 
	coalesce(ps.PacksPerCarton,1) AS col11, -- Akex Y.  ps.PacksPerCarton
	'Cartons' AS col12, '' AS col13, -- Calculated field 
	'Pallets' AS col14, '' AS col15, 
	coalesce(si.PackSize,0) AS col16, -- PackSize.size same as col09
	coalesce(cl.PO, '') AS col17, coalesce(cl.DI, '') AS col18, coalesce(cl.BC, '') AS col19, coalesce(cl.AX, '') AS col20, coalesce(cl.AD, '') AS col21, coalesce(cl._DO, '') AS col22, coalesce(cl.CO, '') AS col23, coalesce(cl.CY, '') AS col24, 
	coalesce(bc.UPCCode,'') AS col25, coalesce(pf.Status,'') AS col26, ps.PurchasingPreferenceOrder AS col27, coalesce(pf.Manufacturer, '') AS col28, 
	'' AS col29, '' AS col30, '' AS col31, '' AS col32, '' AS col33, 
	coalesce(ps.PacksPerCarton,0) AS PacksPerCarton, coalesce(ps.PacksPerPallet,0) AS PacksPerPallet 
from  
		Product$StockItem AS si 
	left outer join
		product$stockitem_packsize si_ps on si.id = si_ps.product$stockitemid
	left outer join 
		product$packsize ps on si_ps.product$packsizeid = ps.id
	left outer join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	left outer join
		product$product p on si_p.product$productid = p.id
	left outer join
		product$contactlens cl on si_p.product$productid = cl.id
	left outer join
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	left outer join
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
	left outer join
		product$productfamily_productcategory pf_pc on pf.id = pf_pc.product$productfamilyid
	left outer join
		product$productCategory pc on pf_pc.product$productcategoryid = pc.id
	left outer join
		product$barcode_stockitem bc_si on si.id = bc_si.product$stockitemid
	left outer join
		product$barcode bc on bc_si.product$barcodeid = bc.id
WHERE pc.Code NOT IN ('05', '07', '08',  '09', '10', '11', '12', '13');


----------------------------------------------------------------------------------
----------------------------------------------------------------------------------


--DRP1 AVAILABLE STOCK

select coalesce(si.sku, '') as si_SKU, coalesce(wh.shortname, '') || '_RETAIL' as wh_shortname, coalesce(wsi.availableqty, 0) as wsi_availableqty
from
	inventory$warehousestockitem as wsi
	left outer join 
		inventory$located_at la  on wsi.id = la.inventory$warehousestockitemid
	left outer join 
		inventory$warehouse wh on la.inventory$warehouseid = wh.id
	left outer join 
		inventory$warehousestockitem_stockitem wsi_si on wsi.id = wsi_si.inventory$warehousestockitemid
	left outer join 
		product$stockitem si on wsi_si.product$stockitemid = si.id
where wsi.availableqty > 0 
order by wh.shortname, si.sku



--DRP2 UNALLOCATED STOCK

--Retail Mendix
select coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') || '_RETAIL' as wh_shortname, coalesce(si.sku, '') as si_sku
from 
		inventory$warehousestockitem as wsi
	left outer join 
		inventory$located_at la on wsi.id = la.inventory$warehousestockitemid
	left outer join 
		inventory$warehouse wh on la.inventory$warehouseid = wh.id
	left outer join 
		inventory$warehousestockitem_stockitem wsi_si on wsi.id = wsi_si.inventory$warehousestockitemid
	left outer join 
		product$stockitem si on wsi_si.product$stockitemid = si.id
where wsi.availableqty > 0 or wsi.forwarddemand > 0
order by wh.shortname, si.sku;

select wsi.id as wsi_id, sh.status, sh.wholesale, sh.packquantity as packquantity
from 
		inventory$warehousestockitem as wsi
	inner join 
		purchaseorders$shortage_warehousestockitem sh_wsi on sh_wsi.inventory$warehousestockitemid = wsi.id
	inner join 
		purchaseorders$shortage sh on sh_wsi.purchaseorders$shortageid = sh.id
	left join -- inner
		(select coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') || '_RETAIL' as wh_shortname, coalesce(si.sku, '') as si_sku
		from 
				inventory$warehousestockitem as wsi
			left outer join 
				inventory$located_at la on wsi.id = la.inventory$warehousestockitemid
			left outer join 
				inventory$warehouse wh on la.inventory$warehouseid = wh.id
			left outer join 
				inventory$warehousestockitem_stockitem wsi_si on wsi.id = wsi_si.inventory$warehousestockitemid
			left outer join 
				product$stockitem si on wsi_si.product$stockitemid = si.id
		where wsi.availableqty > 0 or wsi.forwarddemand > 0) t on wsi.id = t.wsi_id			
where (sh.status = 'Pending' and cast(sh.wholesale as text) = 'false');
	-- wsi.id = 1 and 


--Wholesale Mendix

select coalesce(wsi.id, 0) as wsi_id, coalesce(wh.shortname, '') || '_Wholesale' as wh_shortname, coalesce(si.sku, '') as si_sku
from 
		inventory$warehousestockitem as wsi
	left outer join 
		inventory$located_at la on wsi.id = la.inventory$warehousestockitemid
	left outer join 
		inventory$warehouse wh on la.inventory$warehouseid = wh.id
	left outer join 
		inventory$warehousestockitem_stockitem wsi_si on wsi.id = wsi_si.inventory$warehousestockitemid
	left outer join 
		product$stockitem si on wsi_si.product$stockitemid = si.id
where wsi.availableqty > 0 or wsi.forwarddemand > 0
order by wh.shortname, si.sku;

select wsi.id as wsi_id, sh.status, sh.wholesale, sh.packquantity as packquantity
from 
		inventory$warehousestockitem as wsi
	inner join 
		purchaseorders$shortage_warehousestockitem sh_wsi on sh_wsi.inventory$warehousestockitemid = wsi.id
	inner join 
		purchaseorders$shortage sh on sh_wsi.purchaseorders$shortageid = sh.id
where wsi.id = 1 and (sh.status = 'Pending' and cast(sh.wholesale as text) = 'true');





--DRP3 SCHEDULED RECEIPTS


SELECT 
	coalesce(pol.PO_LineID, 0) AS PO_LineID,
	coalesce(si.PackSize, 0) AS PackSize,
	coalesce(r.ShipmentType, '') AS ShipmentType,
--	coalesce(wsol.QuantityOpen, 0.0)  			AS QuantityOpen,  
--  	coalesce(shtg.PackQuantity, 0.0)			AS shortage,
--	coalesce(wsh.ShipmentId, '')				AS shipmentId, 
--	shtg.Wholesale	 							AS isWholesale,

	coalesce(rl.StockItem, '') AS col01, coalesce(wh.ShortName,   '') AS col02, coalesce(po.OrderNumber,'') AS col03, 

----------------------/ Alex Y.  new changes 2017/09/19 ----------------------/
----------------------/ Use sol instead of pol          ----------------------/
	coalesce(rl.QuantityOrdered, 0.0) AS col04,  
	rl.Created AS col05 ,
------------------------------------------------------------------------------/
------------------------------------------------------------------------------/
	
--	po.createdDate AS col06, 
--	po.createdDate AS col07, 		
	
	------------ col08 - col16  blank ------------
	';' AS col08, ';' AS col09, ';' AS col10, ';' AS col11, ';' AS col12, ';' AS col13, ';' AS col14, ';' AS col15, ';' AS col16,
	
	coalesce(r.Status, '') AS col17, coalesce(r.ReceiptId, '') AS col18, coalesce(po.Status, '') AS col19, coalesce(po.Source, '') AS col20, coalesce(su.SupplierName, '') AS col21, 
--	coalesce(wfp.SupplierName, '') 				AS col23, 			

	------------ col22 - col26  blank ------------
	';' AS col22, ';' AS col23, ';' AS col24, ';' AS col25, ';' AS col26, 			
	

	coalesce(rl.UnitCost, 0.0) AS col27, 
----------------------/ Alex Y.  new changes 2017/09/19 ----------------------/
------------------------------------------------------------------------------/
	-- coalesce(sol.QuantityReceived, 0.0)		AS col28,
	-- coalesce(sol.QuantityOrdered, 0.0)		AS quantityOrdered,	-- col29
	-- 											AS col29, QuantityOpen * UnitCost <--- col 27  -- see (MF) SUB_GetOpenQuantity
	-- 											AS col30, QuantityReceived * UnitCost <-- in col 27
	
	-------------------------------- new ------------------------------
	coalesce(rl.QuantityAccepted, 0.0) 		AS col28 , 
	--coalesce(wsol.QuantityOrdered,  0.0)		AS quantityOrdered,
	-- calculated fields
	--+											AS col29,   -- Col4 (sol.QuantityOrdered)  * col27(sol.UnitCost), see (MF) SUB_GetOpenQuantity
	--+											AS col30,   -- Col27(sol.UnitCost) * col28(sol.QuantityAccepted), see (MF) SUB_GetOpenQuantity
	
	
------------------------------------------------------------------------------/
------------------------------------------------------------------------------/
	
	-- ------------ col31 - col36  blank ------------
	';' AS col31, ';' AS col32, ';' AS col33, ';' AS col34, ';' AS col35, ';' AS col36,
	
	r.createdDate          								AS col37 , 
	--po.createdDate								AS col41, 
	--po.createdDate								AS col42,		
	
	po.createdDate									AS col40 , 
	--po.createdDate								AS col41, 
	--po.createdDate								AS col42, 

	r.DueDate									AS col43 , 
	--wsh.DueDate									AS col44, 
	--wsh.DueDate									AS col45, 
	
	r.ArrivedDate									AS col46 , 
	--wsh.ArrivedDate								AS col47, 
	--wsh.ArrivedDate								AS col48, 
	
	r.ConfirmedDate  			    					AS col49 , 
	--wsh.ConfirmedDate								AS col50, 
	--wsh.ConfirmedDate								AS col51, 
	
	r.StockRegisteredDate								AS col52 
	--wsh.StockRegisteredDate							AS col53, 
	--wsh.StockRegisteredDate							AS col54  		 
from
		warehouseshipments$receiptline rl
	left outer join 
		warehouseshipments$receiptline_purchaseorderline rl_pol on rl.id = rl_pol.warehouseshipments$receiptlineid
	left outer join 
		purchaseorders$purchaseorderline pol on rl_pol.purchaseorders$purchaseorderlineid = pol.id
	left outer join 
		purchaseorders$purchaseorderline_stockitem pol_si on pol.id = pol_si.purchaseorders$purchaseorderlineid
	left outer join 
		product$stockitem si on pol_si.product$stockitemid = si.id
	left outer join 
		purchaseorders$purchaseorderline_purchaseorderlineheader pol_polh on pol.id = pol_polh.purchaseorders$purchaseorderlineid
	left outer join 
		purchaseorders$purchaseorderlineheader polh on pol_polh.purchaseorders$purchaseorderlineheaderid = polh.id
	left outer join 
		purchaseorders$purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
	left outer join 
		purchaseorders$purchaseorder po on polh_po.purchaseorders$purchaseorderid = po.id
	left outer join 
		purchaseorders$purchaseorder_supplier po_su on po.id = po_su.purchaseorders$purchaseorderid
	left outer join 
		suppliers$supplier su on po_su.suppliers$supplierid = su.id
	left outer join 
		warehouseshipments$receiptline_supplier rl_su on su.id = rl_su.suppliers$supplierid --and rl_su.receiptlineid = rl.id
	left outer join 
		warehouseshipments$receiptline_receipt rl_r on rl.id = rl_r.warehouseshipments$receiptlineid
	left outer join 
		warehouseshipments$receipt r on rl_r.warehouseshipments$receiptid = r.id
	left outer join 
		warehouseshipments$receipt_warehouse r_wh on r.id = r_wh.warehouseshipments$receiptid
	left outer join 
		inventory$warehouse wh on r_wh.inventory$warehouseid = wh.id
WHERE /*po.IsTestPO='false' field not available, amount others and */ (r.Status NOT IN ('Cancelled', 'Stock_Registered', 'Aggregated')) 
ORDER BY coalesce(po.OrderNumber,''), coalesce(rl.StockItem,''), coalesce(r.ReceiptId,'');
	

	
	
--DRP4 BOM PRODUCTS
	
select
	coalesce(p.sku, '') as pSKU,
	coalesce(si.sku,'') as siSKU
from
	product$product p 
	left outer join product$stockitem_product si_p
		on p.id = si_p.product$productid
	left outer join product$stockitem si
		on si_p.product$stockitemid = si.id 
where p.isbom = true
order by p.sku limit 1000


select p_orig.pSKU, coalesce(si.sku,''), coalesce(bom.quantityper, 0)
from 
		product$product as p 
	left outer join 
		product$parent_product p_p on p.id = p_p.product$productid
	left outer join 
		product$bomquantity bom on p_p.product$bomquantityid = bom.id
	left outer join 
		product$bom_stockitem b_si on bom.id = b_si.product$bomquantityid
	left outer join 
		product$stockitem si on b_si.product$stockitemid = si.id
	inner join
		(select coalesce(p.sku, '') as pSKU, coalesce(si.sku,'') as siSKU
		from
			product$product p 
			left outer join product$stockitem_product si_p
				on p.id = si_p.product$productid
			left outer join product$stockitem si
				on si_p.product$stockitemid = si.id 
		where p.isbom = true) p_orig on p.sku = p_orig.pSKU 
-- where p.sku = ''
order by si.sku	

--Query call is disabled.
select
	coalesce(count(si.sku), 0) as maxCount
from
	product$product p
	left outer join product$parent_product p_p
		on p.id = p_p.product$productid
	left outer join product$bomquantity bom
		on p_p.product$bomquantityid = bom.id
	left outer join product$bom_stockitem b_si
		on bom.id = b_si.product$bomquantityid
	left outer join product$stockitem si
		on b_si.product$stockitemid = si.id
group by p.sku, p.isbom, si.sku
having p.isbom = true
order by count(si.sku) desc


