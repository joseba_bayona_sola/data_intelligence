
select order_no, product_id, sku as SKU, 
  concat(oh.customer_id, '_VD-',
    CASE WHEN st.store_name like '%.uk%' THEN 'UK'
       WHEN st.store_name like '%.it%' THEN 'IT'
       WHEN st.store_name like '%.ie%' THEN 'IE'
       WHEN st.store_name like '%.es%' THEN 'ES'
       WHEN st.store_name like '%.ca%' THEN 'CA'
       WHEN st.store_name like '%.be%' THEN 'BE'
       WHEN st.store_name like '%.fr%' THEN 'FR'
       WHEN st.store_name like '%.nl%' THEN 'NL'
       ELSE 'GB'
    END ) as Customer_Country, concat(CASE WHEN country_id = 'GB' THEN 'UK' ELSE country_id END ,'_RETAIL') as warehouse, 
    YEAR(oh.created_at) as year, MONTH(oh.created_at) as month, DAY(oh.created_at) as day, 
    FLOOR(qty_ordered) as qty,
    olv.global_line_subtotal_exc_vat as price
from 
    dw_order_lines 
  join 
    dw_order_headers oh using (order_id) 
  JOIN 
    dw_order_address oa on oa.order_id = oh.order_id and address_type = 'shipping'
  JOIN 
    dw_stores st on oh.store_id = st.store_id 
  JOIN 
    order_lines_vat olv using (item_id)
where 
  -- cast(oh.created_at as date) between  DATE(now()) - INTERVAL 2 YEAR  AND (DATE(NOW()) - INTERVAL DAY(NOW()) DAY) 
  cast(oh.created_at as date) BETWEEN DATE(now()) - INTERVAL 3 DAY AND  DATE(now()) - INTERVAL 1 DAY
order by oh.created_at asc, order_no asc;

  
0 : order_no
1 : product_id
2 : SKU 
3 : Customer_Country 
4 : warehouse 
5 : year
6 : month 
7 : day
8 : qty
9 : price