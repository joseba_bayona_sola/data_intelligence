
-- AN1

		select sku, 
			convert(varchar, customer_id) + '_VD-' +  
				CASE 
					WHEN store_name like '%.uk%' THEN 'UK'
					WHEN store_name like '%.it%' THEN 'IT'
					WHEN store_name like '%.ie%' THEN 'IE'
					WHEN store_name like '%.es%' THEN 'ES'
					WHEN store_name like '%.ca%' THEN 'CA'
					WHEN store_name like '%.be%' THEN 'BE'
					WHEN store_name like '%.fr%' THEN 'FR'
					WHEN store_name like '%.nl%' THEN 'NL'
					ELSE 'GB'
				END customer_website, 
			country_code + '_RETAIL' country_category, 
			year(order_date) year, month(order_date) month, day(order_date) day, 
			qty_unit quantity
		into #an1
		from
			(select -- ol.order_id, 
				ol.order_date, ol.customer_id, ol.store_name, ol.country_code, ol.sku_erp, ol.pack_size, isnull(si.sku, si2.sku) sku, ol.qty_unit, ol.global_subtotal_exc_vat
			from
					(select -- order_id, 
						order_date, customer_id, store_name, country_code, product_id_magento, sku_erp, pack_size, qty_unit, global_subtotal_exc_vat
					from 
						-- Warehouse.fm.fact_in_files_ol_optimum_pack_size 
						Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust 
					where sku_erp is not null) ol
				left join
					-- Warehouse.prod.dim_stock_item_v si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize
					(select parameter, packsize, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter, packsize) si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize -- avoid duplications based on duplicated SI
				left join
					(select parameter, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter) si2 on ol.sku_erp = si2.parameter -- join with SI that with set pack size don't match
			) ol

	select sku, customer_website, country_category, year, month, day, quantity
	from #an1

	select year, month, day, count(*)
	from #an1
	group by year, month, day
	order by year, month, day

	select top 1000 count(*) over (partition by sku, customer_website, year, month, day) num_rep,
		sku, customer_website, country_category, year, month, day, quantity
	from #an1
	order by num_rep desc, sku, customer_website, year, month, day

	-- NOTE: Done correctly at CUSTOMER level - Duplicates due to wrong sku (1317) - Orders in 2 diff websites
	select top 1000 count(*) over (partition by sku, customer_website, country_category, year, month, day) num_rep,
		sku, customer_website, country_category, year, month, day, quantity
	from #an1
	order by num_rep desc, sku, customer_website, country_category, year, month, day

	select *
	from Warehouse.fm.fact_in_files_ol
	where customer_id = 478174

	select *
	from Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust
	where customer_id = 478174

-------------------------------------------------------------------------

	select top 1000 *
	from Warehouse.fm.fact_in_files_ol
	where customer_id = 2816218
	order by order_id, customer_id, store_name, country_code, convert(date, order_date), sku_erp

	select top 1000 count(*), count(distinct order_id), min(order_date), max(order_date)
	from Warehouse.fm.fact_in_files_ol

	select customer_id, store_name, country_code, convert(date, order_date), sku_erp, count(distinct order_id) dist_order
	from Warehouse.fm.fact_in_files_ol
	group by customer_id, store_name, country_code, convert(date, order_date), sku_erp
	having count(distinct order_id) > 1
	order by dist_order desc

	
