		truncate table Warehouse.fm.fact_in_files_ol

		insert into Warehouse.fm.fact_in_files_ol (order_line_id_bk, order_id, order_no, order_date, invoice_date, 
			customer_id, store_name, country_code, order_status_name, 
			product_id_magento, sku_magento, sku_erp, 
			pack_size, qty_unit, global_subtotal_exc_vat, 
			idETLBatchRun_ins)

			select order_line_id_bk, order_id_bk order_id, order_no, order_date, invoice_date, 
				customer_id, store_name, country_code_ship country_code, order_status_name, 
				product_id_magento, sku_magento, sku_erp, 
				pack_size, qty_unit, 
				global_subtotal - (global_subtotal * vat_percent) global_subtotal_exc_vat,
				1
			from Warehouse.sales.fact_order_line_v
			where -- order_status_name <> 'CANCEL' and 
				order_date between '2020-02-01' and '2020-02-15' -- invoice_date ??




		-- Warehouse.fm.fact_in_files_ol_optimum_pack_size: Version 1: At Customer Level
		truncate table Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust

		insert into Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust(order_date, 
			customer_id, store_name, country_code, 
			product_id_magento, sku_erp, 
			pack_size, qty_unit, global_subtotal_exc_vat, 
			idETLBatchRun_ins)
						
				select 
					ol.order_date, 
					ol.customer_id, ol.store_name, ol.country_code, 
					ol.product_id_magento, ol.sku_erp, 
					case when (pfps.size_type = 'size1') then pfps.size_1 else pfps.size_2 end pack_size,
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end qty_unit, 
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end * ol.global_subtotal_exc_vat_lens global_subtotal_exc_vat, 
					1
				from
					(select convert(date, order_date) order_date, customer_id, store_name, country_code, convert(varchar, product_id_magento) product_id_magento, sku_erp, 
						sum(qty_unit) qty_unit, sum(global_subtotal_exc_vat) global_subtotal_exc_vat, sum(global_subtotal_exc_vat)/sum(qty_unit) global_subtotal_exc_vat_lens
					from Warehouse.fm.fact_in_files_ol 
					where sku_erp is not null
					group by convert(date, order_date), customer_id, store_name, country_code, product_id_magento, sku_erp) ol
				inner join
					(select distinct product_id_magento, 'size1' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size'
					union
					select distinct product_id_magento, 'size2' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size' and size_2 is not null) pfps on ol.product_id_magento = pfps.product_id_magento -- first join size_1 then size_2 where is not null					

				where case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end <> 0
					
		-- Warehouse.fm.fact_in_files_ol_optimum_pack_size: Version 2: At Order Level
		truncate table Warehouse.fm.fact_in_files_ol_optimum_pack_size

		insert into Warehouse.fm.fact_in_files_ol_optimum_pack_size(order_id, order_no, order_date, 
			customer_id, store_name, country_code, 
			product_id_magento, sku_erp, -- sku_magento, 
			pack_size, qty_unit, global_subtotal_exc_vat, 
			idETLBatchRun_ins)
						
				select 
					ol.order_id, ol.order_no, ol.order_date, 
					ol.customer_id, ol.store_name, ol.country_code, 
					ol.product_id_magento, ol.sku_erp, -- ol.sku_magento, 
					-- ol.qty_unit, ol.global_subtotal_exc_vat, ol.global_subtotal_exc_vat_lens, 
					case when (pfps.size_type = 'size1') then pfps.size_1 else pfps.size_2 end pack_size,
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end qty_unit, 
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end * ol.global_subtotal_exc_vat_lens global_subtotal_exc_vat, 
					1 
				from
					(select order_id, order_no, order_date, customer_id, store_name, country_code, convert(varchar, product_id_magento) product_id_magento, sku_erp, -- sku_magento
						sum(qty_unit) qty_unit, sum(global_subtotal_exc_vat) global_subtotal_exc_vat, sum(global_subtotal_exc_vat)/sum(qty_unit) global_subtotal_exc_vat_lens
					from Warehouse.fm.fact_in_files_ol 
					where sku_erp is not null
						-- and customer_id in (2965324, 1121534, 1088593, 2950038)	
					group by order_id, order_no, order_date, customer_id, store_name, country_code, product_id_magento, sku_erp) ol
				inner join
					(select distinct product_id_magento, 'size1' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size'
					union
					select distinct product_id_magento, 'size2' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size' and size_2 is not null) pfps on ol.product_id_magento = pfps.product_id_magento -- first join size_1 then size_2 where is not null					

				-- where pfps.size_1 is null
				-- where ol.product_id_magento = '1083'
				where case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end <> 0

-----------------------------------------
		
		drop table DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
		
		-- Version 1: At Customer Level
		select sku, 
			convert(varchar, customer_id) + '_VD-' +  
				CASE 
					WHEN store_name like '%.uk%' THEN 'UK'
					WHEN store_name like '%.it%' THEN 'IT'
					WHEN store_name like '%.ie%' THEN 'IE'
					WHEN store_name like '%.es%' THEN 'ES'
					WHEN store_name like '%.ca%' THEN 'CA'
					WHEN store_name like '%.be%' THEN 'BE'
					WHEN store_name like '%.fr%' THEN 'FR'
					WHEN store_name like '%.nl%' THEN 'NL'
					ELSE 'GB'
				END customer_website, 
			country_code + '_RETAIL' country_category, 
			year(order_date) year, month(order_date) month, day(order_date) day, 
			qty_unit quantity
		into DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
		from
			(select -- ol.order_id, 
				ol.order_date, ol.customer_id, ol.store_name, ol.country_code, ol.sku_erp, ol.pack_size, isnull(si.sku, si2.sku) sku, ol.qty_unit, ol.global_subtotal_exc_vat
			from
					(select -- order_id, 
						order_date, customer_id, store_name, country_code, product_id_magento, sku_erp, pack_size, qty_unit, global_subtotal_exc_vat
					from 
						-- Warehouse.fm.fact_in_files_ol_optimum_pack_size 
						Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust 
					where sku_erp is not null) ol
				left join
					--  Warehouse.prod.dim_stock_item_v si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize
					(select parameter, packsize, sku
					from
						(select parameter, packsize, sku, stock_item_lifecycle_name, stockitemid_bk,
							dense_rank() over (partition by parameter, packsize order by stock_item_lifecycle_name, stockitemid_bk) ord_rep
						from Warehouse.prod.dim_stock_item_v) si
					where ord_rep = 1) si 
					--(select parameter, packsize, min(sku) sku
					--from Warehouse.prod.dim_stock_item_v
					--group by parameter, packsize) si 
						on ol.sku_erp = si.parameter and ol.pack_size = si.packsize -- avoid duplications based on duplicated SI
				left join
					(select parameter, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter) si2 on ol.sku_erp = si2.parameter -- join with SI that with set pack size don't match
			) ol

		-- Version 2: At Order Level
		select sku, 
			convert(varchar, customer_id) + '_VD-' +  
				CASE 
					WHEN store_name like '%.uk%' THEN 'UK'
					WHEN store_name like '%.it%' THEN 'IT'
					WHEN store_name like '%.ie%' THEN 'IE'
					WHEN store_name like '%.es%' THEN 'ES'
					WHEN store_name like '%.ca%' THEN 'CA'
					WHEN store_name like '%.be%' THEN 'BE'
					WHEN store_name like '%.fr%' THEN 'FR'
					WHEN store_name like '%.nl%' THEN 'NL'
					ELSE 'GB'
				END customer_website, 
			country_code + '_RETAIL' country_category, 
			year(order_date) year, month(order_date) month, day(order_date) day, 
			qty_unit quantity, 
			ol.sku_erp, ol.pack_size
		into DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
		from
			(select ol.order_id, ol.order_date, ol.customer_id, ol.store_name, ol.country_code, ol.sku_erp, ol.pack_size, isnull(si.sku, si2.sku) sku, ol.qty_unit, ol.global_subtotal_exc_vat
			from
					(select order_id, order_date, customer_id, store_name, country_code, product_id_magento, sku_erp, pack_size, qty_unit, global_subtotal_exc_vat
					from Warehouse.fm.fact_in_files_ol_optimum_pack_size 
					where sku_erp is not null
						-- and product_id_magento = '1122'
						) ol
				left join
					-- Warehouse.prod.dim_stock_item_v si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize
					(select parameter, packsize, sku
					from
						(select parameter, packsize, sku, stock_item_lifecycle_name, stockitemid_bk,
							dense_rank() over (partition by parameter, packsize order by stock_item_lifecycle_name, stockitemid_bk) ord_rep
						from Warehouse.prod.dim_stock_item_v) si
					where ord_rep = 1) si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize -- avoid duplications based on duplicated SI
				left join
					(select parameter, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter) si2 on ol.sku_erp = si2.parameter -- join with SI that with set pack size don't match
			) ol
	

-----------------------------------------

select *
from Warehouse.fm.fact_in_files_ol
where sku_erp is null
order by product_id_magento

	select ol.product_id_magento, pf.product_family_name, ol.num
	from
		(select product_id_magento, count(*) num
		from Warehouse.fm.fact_in_files_ol
		where sku_erp is null
		group by product_id_magento) ol
	inner join
		Warehouse.prod.dim_product_family_v pf on ol.product_id_magento = pf.product_id_magento	
	order by product_id_magento

	
-----------------------------------------

select top 1000 SKU, customer_website, country_category, year, month, day, quantity
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
where customer_website like '1121534%' or customer_website like '2965324%' or customer_website like '1088593%'
order by customer_website, country_category, year, month, day, sku, quantity

	select count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2

	select sku, customer_website, year, month, day, count(*), count(distinct quantity)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by sku, customer_website, year, month, day
	-- having count(distinct quantity) > 1
	order by count(*) desc

	select sku, customer_website, year, month, day, quantity, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by sku, customer_website, year, month, day, quantity
	-- having count(*) > 1
	order by count(*) desc

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by sku
	order by sku

	select customer_website, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by customer_website
	order by customer_website

	select country_category, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by country_category
	order by country_category

	select year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
	group by year, month, day
	order by year, month, day

	-- Problems with 1122, 1136, 2311, 2338, 2364 // 1120, 1129, 1235
select substring(isnull(t1.sku, t2.sku), 2, 4), t1.sku, t2.sku, t1.num_rows, t2.num_rows
from
		(select sku, count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
		where day <> '15'
		group by sku) t1
	full join
		(select sku, count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
		where day in ('5', '6', '7', '8', '9', '10', '11', '12', '13', '14')
		group by sku) t2 on t1.sku = t2.sku -- and t1.num_rows = t2.num_rows
-- where t1.sku = '01083B2D4BL0000000000301'
where (t1.sku is null or t2.sku is null or (t1.num_rows <> t2.num_rows))
	--and (substring(isnull(t1.sku, t2.sku), 2, 4) in (1103, 1120, 1235, 2311, 2334, 2338, 2364, 3179) -- Missing SKU ERP
	--	or substring(isnull(t1.sku, t2.sku), 2, 4) in (1122, 1136) -- Duplicated / Wrong SKUs
	--	or substring(isnull(t1.sku, t2.sku), 2, 4) in (2296)) -- Complete RevitaLens: 2 rows with same Pack Size
order by isnull(t1.sku, t2.sku)

select *
from
		(select sku, customer_website, convert(int, year) year, convert(int, month) month, convert(int, day) day, quantity, count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
		where day <> '15'
		-- where customer_website like '2965324%'
		group by sku, customer_website, year, month, day, quantity) t1
	full join
		(select sku, customer_website, year, month, day, quantity,
			-- convert(int, quantity) quantity, 
			count(*) num_rows
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
		where day in ('5', '6', '7', '8', '9', '10', '11', '12', '13', '14')
		-- where customer_website like '2965324%'
		group by sku, customer_website, year, month, day, quantity) t2 on t1.sku = t2.sku and t1.customer_website = t2.customer_website 
			and t1.year = t2.year and t1.month = t2.month and t1.day = t2.day and t1.num_rows = t2.num_rows
where (t1.sku is null or t2.sku is null)
	--and (substring(isnull(t1.sku, t2.sku), 2, 4) in (1103, 1120, 1235, 2311, 2334, 2338, 2364, 3179, 3216) -- Missing SKU ERP
	--	or substring(isnull(t1.sku, t2.sku), 2, 4) in (1122, 1136) -- Duplicated / Wrong SKUs
	--	or substring(isnull(t1.sku, t2.sku), 2, 4) in (2296)) -- Complete RevitaLens: 2 rows with same Pack Size
order by isnull(t1.sku, t2.sku)

select product_id_magento, product_family_name
from Warehouse.prod.dim_product_family_v
where product_id_magento in (1122, 1136)
order by product_id_magento

select product_id_magento, product_family_name
from Warehouse.prod.dim_product_family_v
where product_id_magento in (1120, 1235, 2311, 2334, 2338, 2364, 3179, 3216)
order by product_id_magento

select top 1000 stockitemid_bk, product_id_magento, product_family_name, packsize, 
	base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
	parameter, -- product_description, 
	sku, stock_item_description, stock_item_lifecycle_name
from Warehouse.prod.dim_stock_item_v
where sku in ('01122B3D7CR00000000AA011', '01122B3D7CR00000000AA021')
	or sku in ('01122B3D7CR00000000AA021', '01122B3D7CR00000000AW021', '01122B3D7CR00000000AY021')
	or sku in ('01136B3D4BC0000000000301', '01136B3D4BC0000000000302')
	or sku in ('022960000000000000000011')
	or sku in ('02311B3D0CJ0000000000101')
	or sku in ('02364B3D4CV0000070000301')
order by sku

select *		
from Warehouse.prod.dim_stock_item_v
-- where parameter = '01122B3D7BW00000000AW'
-- where product_id_magento = '1122' and base_curve = '8.6' and diameter = '14.5' and power = '-04.50' and colour = 'Pacific Blue'
where product_id_magento = '1122' and packsize = 2

	select parameter, packsize, min(sku) sku, count(*) 
	from Warehouse.prod.dim_stock_item_v
	group by parameter, packsize
	having count(*) > 1
	order by parameter, packsize

-----------------------------------------------------

select *
from Warehouse.fm.fact_in_files_ol
where customer_id = 2387296

select *
from Warehouse.fm.fact_in_files_ol_optimum_pack_size
where customer_id = 2387296

select *
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D
where customer_website = '2387296_VD-UK'

select *
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_2
where customer_website = '2387296_VD-UK' -- 2230041_VD-NL - 2941598_VD-FR


select top 1000 *
from Warehouse.sales.fact_order_line_v
where customer_id = 2708497
order by order_id_bk, order_date, sku_erp


-----------------------------------------------------

-- Products where sku_erp is null - 2 REASONS: 
	-- Not able to find in ERP: Colour, Not parameterse
	-- Wrong data in EDI Stock Item
select top 1000 t1.order_line_id_bk, t1.order_id, t1.order_no, t1.order_date, t1.customer_id, t1.store_name, t1.order_status_name,
	t1.product_id_magento, t2.product_family_name, t2.base_curve, t2.diameter, t2.power, t2.cylinder, t2.axis, t2.addition, t2.dominance, t2.colour, t1.sku_magento, t1.sku_erp
from 
	Warehouse.fm.fact_in_files_ol t1
inner join	
	Warehouse.sales.fact_order_line_v t2 on t1.order_line_id_bk = t2.order_line_id_bk
where t1.sku_erp is null
order by t1.product_id_magento

	select top 1000 *
	from Landing.mag.sales_flat_order_item_aud
	where item_id = 21895271

	select top 1000 *
	from Landing.mag.edi_stock_item_aud
	where product_code = 'ULTRAAST-AX60-BC8.6-CY-0.75-DI14.5-PO-3.50'

	select top 1000 *
	from Landing.aux.mag_edi_stock_item
	where product_code = 'BTP1D30-ADLOW-BC8.6-DI14.2-PO+2.50'

select *
from Landing.mag.edi_stock_item_aud
where product_id in (1120, 2334, 2364, 3179, 3215, 3216)
order by product_id, product_code

select *
from Landing.aux.mag_edi_stock_item
where product_id in (1120, 2334, 2364, 3179, 3215, 3216)
order by product_id, product_code