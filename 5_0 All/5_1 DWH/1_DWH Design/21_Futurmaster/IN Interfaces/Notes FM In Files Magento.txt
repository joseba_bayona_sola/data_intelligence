
packsizes: Dictionary with product_id and the different pack sizes ordered in optimun way (bigger to smaller)
	ERP Data (P_PACKS_SQL) + Manual products

prodkeys: Dictionary with parameter values + code. The code is used to create the ERP SKU string
	ERP DATA (P_PROD_KEYS)

pr: Dictionary based on Magento Orders
	Magento DATA (M_ORD_HISTORY_Q)

0 : order_no
1 : product_id
2 : SKU 
3 : Customer_Country 
4 : warehouse 
5 : year
6 : month 
7 : day
8 : qty
9 : price

order: Dictionary for SKU - qty - price - product. 
	It is used in main for doing the OL deduplication per SKU in an order

printOrder: Function for writing to a file the deduplicated SKU data + Optimun Pack Size Calculation
	It is used in main - once there is not more OL for OH

	packsizes: Dictionary with possible packsizes per product id
		The optimun one allocation is tried: Division being a int + have remaining

getNewSKU: Function for constructing the ERP SKU from Magento Parameters
	Using prodkeys
	The string created is validated against ERP data (P_VALIDATE_SKU)

----------------------------------------------------
----------------------------------------------------

Deduplication + Optimun Pack Size is done
BOM products are not considered

The data is not synced at the moment as Flattenning process is done after the Futurmaster script