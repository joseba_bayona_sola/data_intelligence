		truncate table Warehouse.fm.fact_in_files_ol

		insert into Warehouse.fm.fact_in_files_ol (order_line_id_bk, order_id, order_no, order_date, invoice_date, 
			customer_id, store_name, country_code, order_status_name, 
			product_id_magento, sku_magento, sku_erp, 
			pack_size, qty_unit, global_subtotal_exc_vat, 
			idETLBatchRun_ins)

			select order_line_id_bk, order_id_bk order_id, order_no, order_date, invoice_date, 
				customer_id, store_name, country_code_ship country_code, order_status_name, 
				product_id_magento, sku_magento, sku_erp, 
				pack_size, qty_unit, 
				global_subtotal - (global_subtotal * vat_percent) global_subtotal_exc_vat,
				1
			from Warehouse.sales.fact_order_line_v
			where order_status_name <> 'CANCEL' and 
				order_date between '2020-01-27' and '2020-02-27' -- invoice_date ??


		-- Warehouse.fm.fact_in_files_ol_optimum_pack_size: Version 1: At Customer Level
		truncate table Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust

		insert into Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust(order_date, 
			customer_id, store_name, country_code, 
			product_id_magento, sku_erp, 
			pack_size, qty_unit, global_subtotal_exc_vat, 
			idETLBatchRun_ins)
						
				select 
					ol.order_date, 
					ol.customer_id, ol.store_name, ol.country_code, 
					ol.product_id_magento, ol.sku_erp, 
					case when (pfps.size_type = 'size1') then pfps.size_1 else pfps.size_2 end pack_size,
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end qty_unit, 
					case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end * ol.global_subtotal_exc_vat_lens global_subtotal_exc_vat, 
					1
				from
					(select convert(date, order_date) order_date, customer_id, store_name, country_code, convert(varchar, product_id_magento) product_id_magento, sku_erp, 
						sum(qty_unit) qty_unit, sum(global_subtotal_exc_vat) global_subtotal_exc_vat, sum(global_subtotal_exc_vat)/sum(qty_unit) global_subtotal_exc_vat_lens
					from Warehouse.fm.fact_in_files_ol 
					where sku_erp is not null
					group by convert(date, order_date), customer_id, store_name, country_code, product_id_magento, sku_erp) ol
				inner join
					(select distinct product_id_magento, 'size1' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size'
					union
					select distinct product_id_magento, 'size2' size_type, size_1, size_2
					from Warehouse.prod.dim_product_family_pack_size_optimum_v
					where optimum_type = 'size' and size_2 is not null) pfps on ol.product_id_magento = pfps.product_id_magento -- first join size_1 then size_2 where is not null					

				where case when (pfps.size_type = 'size1') then (ol.qty_unit - (ol.qty_unit % pfps.size_1)) else (ol.qty_unit % pfps.size_1) end <> 0

		-- AN1
		select sku, 
			convert(varchar, customer_id) + '_VD-' +  
				CASE 
					WHEN store_name like '%.uk%' THEN 'UK'
					WHEN store_name like '%.it%' THEN 'IT'
					WHEN store_name like '%.ie%' THEN 'IE'
					WHEN store_name like '%.es%' THEN 'ES'
					WHEN store_name like '%.ca%' THEN 'CA'
					WHEN store_name like '%.be%' THEN 'BE'
					WHEN store_name like '%.fr%' THEN 'FR'
					WHEN store_name like '%.nl%' THEN 'NL'
					ELSE 'GB'
				END customer_website, 
			country_code + '_RETAIL' country_category, 
			year(order_date) year, month(order_date) month, day(order_date) day, 
			convert(int, qty_unit) quantity
		from
			(select -- ol.order_id, 
				ol.order_date, ol.customer_id, ol.store_name, ol.country_code, ol.sku_erp, ol.pack_size, isnull(si.sku, si2.sku) sku, ol.qty_unit, ol.global_subtotal_exc_vat
			from
					(select -- order_id, 
						order_date, customer_id, store_name, country_code, product_id_magento, sku_erp, pack_size, qty_unit, global_subtotal_exc_vat
					from 
						-- Warehouse.fm.fact_in_files_ol_optimum_pack_size 
						Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust 
					where sku_erp is not null) ol
				left join
					-- Warehouse.prod.dim_stock_item_v si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize
					(select parameter, packsize, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter, packsize) si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize -- avoid duplications based on duplicated SI
				left join
					(select parameter, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter) si2 on ol.sku_erp = si2.parameter -- join with SI that with set pack size don't match
			) ol
		order by year, month, day, sku, customer_website, country_category

		select sku, 
			convert(varchar, customer_id) + '_VD-' +  
				CASE 
					WHEN store_name like '%.uk%' THEN 'UK'
					WHEN store_name like '%.it%' THEN 'IT'
					WHEN store_name like '%.ie%' THEN 'IE'
					WHEN store_name like '%.es%' THEN 'ES'
					WHEN store_name like '%.ca%' THEN 'CA'
					WHEN store_name like '%.be%' THEN 'BE'
					WHEN store_name like '%.fr%' THEN 'FR'
					WHEN store_name like '%.nl%' THEN 'NL'
					ELSE 'GB'
				END customer_website, 
			country_code + '_RETAIL' country_category, 
			year(order_date) year, month(order_date) month, day(order_date) day, 
			convert(decimal(12, 2), global_subtotal_exc_vat) value
		from
			(select -- ol.order_id, 
				ol.order_date, ol.customer_id, ol.store_name, ol.country_code, ol.sku_erp, ol.pack_size, isnull(si.sku, si2.sku) sku, ol.qty_unit, ol.global_subtotal_exc_vat
			from
					(select -- order_id, 
						order_date, customer_id, store_name, country_code, product_id_magento, sku_erp, pack_size, qty_unit, global_subtotal_exc_vat
					from 
						-- Warehouse.fm.fact_in_files_ol_optimum_pack_size 
						Warehouse.fm.fact_in_files_ol_optimum_pack_size_cust 
					where sku_erp is not null) ol
				left join
					-- Warehouse.prod.dim_stock_item_v si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize
					(select parameter, packsize, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter, packsize) si on ol.sku_erp = si.parameter and ol.pack_size = si.packsize -- avoid duplications based on duplicated SI
				left join
					(select parameter, min(sku) sku
					from Warehouse.prod.dim_stock_item_v
					group by parameter) si2 on ol.sku_erp = si2.parameter -- join with SI that with set pack size don't match
			) ol
		order by year, month, day, sku, customer_website, country_category

--------------------------------------------------------------

