
select top 1000 SKU, customer_website, country_category, year, month, day, quantity
from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
where customer_website = '2864681_VD-ES'
order by customer_website, country_category, year, month, day, sku, quantity

	select count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh

	select sku, customer_website, year, month, day, count(*), count(distinct quantity)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by sku, customer_website, year, month, day
	-- having count(distinct quantity) > 1
	order by count(*) desc

	select sku, customer_website, year, month, day, quantity, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by sku, customer_website, year, month, day, quantity
	-- having count(*) > 1
	order by count(*) desc

	select sku, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by sku
	order by sku

	select customer_website, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by customer_website
	order by customer_website

	select country_category, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by country_category
	order by country_category

	select year, month, day, count(*)
	from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
	group by year, month, day
	order by year, month, day

--------------------------------------------------------------
--------------------------------------------------------------

-- 127563
select top 1000 count(*) over (),
	t1.SKU, t1.customer_website, t1.country_category, t1.year, t1.month, t1.day, 
	t2.SKU, t2.customer_website, t2.country_category, t2.year, t2.month, t2.day, 
	t1.quantity, t2.quantity
from	
		DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh t1 
	inner join
		DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_magento t2 on 
			t1.SKU = t2.SKU and t1.customer_website = t2.customer_website and t1.country_category = t2.country_category and t1.year = t2.year and t1.month = t2.month and t1.day = t1.day
				-- and t1.quantity = t2.quantity
-- where t1.customer_website = '1000181_VD-UK'
-- where t1.country_category like 'GB_RETAIL'
order by t1.customer_website, t1.country_category, t1.year, t1.month, t1.day, t1.sku, t1.quantity

select count(*) over (),
	t1.SKU, t1.customer_website, t1.country_category, t1.year, t1.month, t1.day, 
	t2.SKU, t2.customer_website, t2.country_category, t2.year, t2.month, t2.day, 
	t1.quantity, t2.quantity
from	
		DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh t1 
	left join
		DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_magento t2 on 
			t1.SKU = t2.SKU and t1.customer_website = t2.customer_website and t1.country_category = t2.country_category and t1.year = t2.year and t1.month = t2.month and t1.day = t1.day
				-- and t1.quantity = t2.quantity
-- where t2.customer_website is null and t1.country_category like 'GB_RETAIL' -- 168931
where t2.customer_website is null and t1.country_category not like 'GB_RETAIL' and t1.day not in (26, 27, 28, 29)
order by t1.year, t1.month, t1.day, t1.customer_website, t1.country_category, t1.sku, t1.quantity

--------------------------------------------------------------

select t1.country_category, t2.country_category, t1.num1, t2.num2
from
		(select country_category, count(*) num1
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_dwh
		group by country_category) t1
	full join
		(select country_category, count(*) num2
		from DW_GetLenses_jbs.dbo.ERP_FMAN_SALES_HISTORY_D_magento
		group by country_category) t2 on t1.country_category = t2.country_category
order by t1.country_category, t2.country_category