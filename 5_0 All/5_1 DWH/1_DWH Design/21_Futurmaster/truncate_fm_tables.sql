
alter table Warehouse.fm.dim_wh_stock_item_static_data_aud drop constraint [FK_fm_wh_stock_item_static_data_aud_dim_wh_stock_item_static_data]
go

truncate table Warehouse.fm.dim_wh_stock_item_static_data
go

alter table Warehouse.fm.dim_wh_stock_item_static_data_aud add constraint [FK_fm_wh_stock_item_static_data_aud_dim_wh_stock_item_static_data]
	foreign key (idWHStockItemStaticData_sk_fk) references Warehouse.fm.dim_wh_stock_item_static_data (idWHStockItemStaticData_sk);
go

-- 
alter table Warehouse.fm.fact_fm_data_aud drop constraint [FK_fm_data_aud_fm_data_aud]
go

truncate table Warehouse.fm.fact_fm_data
go

alter table Warehouse.fm.fact_fm_data_aud add constraint [FK_fm_data_aud_fm_data_aud]
	foreign key (idFMData_sk_fk) references Warehouse.fm.fact_fm_data (idFMData_sk)
go


-- 
alter table Warehouse.fm.fact_planned_requirements disable trigger trg_planned_requirements_aud
go

delete from Warehouse.fm.fact_planned_requirements
go

alter table Warehouse.fm.fact_planned_requirements enable trigger trg_planned_requirements_aud
go
