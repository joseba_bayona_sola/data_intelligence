
	select -- count(*) over (partition by idFMData_sk) num_records, 
		record_type, idFMData_sk, 
		warehouse_name, product_id_magento, product_family_name, packsize, sku, -- stock_item_description, 
		fm_date, convert(date, aud_dateTo),
		qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
		qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
	from Warehouse.fm.fact_fm_data_aud_v
	where product_id_magento = '1083' 
		and sku = '01083B2D4CX0000000000301' and warehouse_name = 'York' and fm_date = '2019-07-18'
	order by fm_date, record_type, convert(date, aud_dateTo)

	select 
		record_type, idFMData_sk, 
		warehouse_name, product_id_magento, product_family_name, packsize, sku, -- stock_item_description, 
		fm_date, 
		qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
		qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions, 
		convert(date, aud_dateTo)
	from Warehouse.fm.fact_fm_data_aud_v
	where product_id_magento = '1083' 
		and sku = '01083B2D4CX0000000000301' and warehouse_name = 'York' -- and fm_date = '2019-07-18'
		and convert(date, aud_dateTo) = '2019-04-20'
	order by fm_date

	select 
		record_type, idFMData_sk, 
		warehouse_name, product_id_magento, product_family_name, packsize, sku, -- stock_item_description, 
		fm_date, 
		qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
		qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions, 
		convert(date, aud_dateTo)
	from Warehouse.fm.fact_fm_data_aud_v
	where product_id_magento = '1083' 
		and sku = '01083B2D4CX0000000000301' and warehouse_name = 'York' -- and fm_date = '2019-07-18'
		and convert(date, aud_dateTo) is null
	order by fm_date

-------------------------------------------------------------------

	select 
		record_type, idFMData_sk, 
		warehouse_name, product_id_magento, product_family_name, packsize, sku, -- stock_item_description, 
		fm_date, convert(date, aud_dateTo) data_date,
		qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
		qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
		
	from Warehouse.fm.fact_fm_data_aud_v
	where product_id_magento = '1083' 
		and sku = '01083B2D4CX0000000000301' and warehouse_name = 'York' -- and fm_date = '2019-07-18'
		-- and convert(date, aud_dateTo) is null
	order by fm_date, record_type, convert(date, aud_dateTo)
