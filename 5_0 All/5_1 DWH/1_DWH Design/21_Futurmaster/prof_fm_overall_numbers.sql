
select top 1000 count(*) over (), *
from Warehouse.prod.dim_product_family_pack_size_v

select top 1000 count(*) over (), *
from Warehouse.prod.dim_stock_item_v

select top 1000 count(*) over (), *
from Warehouse.stock.dim_wh_stock_item_v

---------------------------------------------------

select *
from Warehouse.fm.dim_fm_product_family_sales_value

	select idProductFamily_sk_fk, count(*)
	from Warehouse.fm.dim_fm_product_family_sales_value
	group by idProductFamily_sk_fk
	order by idProductFamily_sk_fk

select *
from Warehouse.fm.dim_fm_product_family_cost_value

	select idProductFamily_sk_fk, count(*)
	from Warehouse.fm.dim_fm_product_family_cost_value
	group by idProductFamily_sk_fk
	order by idProductFamily_sk_fk

---------------------------------------------------

select top 1000 *
from Warehouse.fm.fact_fm_data

	select top 1000 count(*)
	from Warehouse.fm.fact_fm_data

	select top 1000 idWarehouse_sk_fk, idStockItem_sk_fk, count(*), count(*) over ()
	from Warehouse.fm.fact_fm_data
	group by idWarehouse_sk_fk, idStockItem_sk_fk
	order by idWarehouse_sk_fk, idStockItem_sk_fk

	select top 1000 idStockItem_sk_fk, count(*), count(*) over ()
	from Warehouse.fm.fact_fm_data
	group by idStockItem_sk_fk
	order by idStockItem_sk_fk

	select top 1000 idWarehouse_sk_fk, count(*), count(*) over ()
	from Warehouse.fm.fact_fm_data
	group by idWarehouse_sk_fk
	order by idWarehouse_sk_fk

	select top 1000 idCalendarFMDate_sk_fk, count(*), count(*) over ()
	from Warehouse.fm.fact_fm_data
	group by idCalendarFMDate_sk_fk
	order by idCalendarFMDate_sk_fk

select top 1000 *
from Warehouse.fm.fact_fm_data_v

	select product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.fm.fact_fm_data_v
	group by product_id_magento, product_family_name, packsize
	order by product_id_magento, product_family_name, packsize

	select warehouse_name, product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.fm.fact_fm_data_v
	group by warehouse_name, product_id_magento, product_family_name, packsize
	order by warehouse_name, product_id_magento, product_family_name, packsize

-------------------------------------------

select warehouseid_bk
from Staging.fm.fact_fm_data

	select top 1000 count(*)
	from Staging.fm.fact_fm_data

--------------------------------------------

select top 1000 *
from Warehouse.fm.fact_fm_data_aud

select top 1000 *
from Warehouse.fm.fact_fm_data_aud_v

	select top 1000 count(*)
	from Warehouse.fm.fact_fm_data_aud

select top 1000 *
from Warehouse.fm.fact_fm_data_aud
