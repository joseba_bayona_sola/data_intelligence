
drop table #product_family_cost_value

select *
from #product_family_cost_value

select warehouse_name, product_id_magento, packsize, 
	qty_available + qty_outstanding_allocation + qty_on_hold qty_on_hand, 
	local_unit_cost, global_unit_cost
into #product_family_cost_value
from Warehouse.tableau.stock_report_ds_v
where qty_available + qty_outstanding_allocation + qty_on_hold > 0

select warehouse_name, product_id_magento, packsize, 
	sum(local_unit_cost * qty_on_hand) / sum_qty_on_hand local_product_cost, 
	sum(local_unit_cost * qty_on_hand) / sum_qty_on_hand local_total_cost, 
	sum(global_unit_cost * qty_on_hand) / sum_qty_on_hand global_product_cost, 
	sum(global_unit_cost * qty_on_hand) / sum_qty_on_hand global_total_cost 

from
	(select warehouse_name, product_id_magento, packsize, 
		qty_on_hand, sum(qty_on_hand) over (partition by warehouse_name, product_id_magento, packsize) sum_qty_on_hand,
		local_unit_cost, global_unit_cost
	from #product_family_cost_value) pfcv
group by warehouse_name, product_id_magento, packsize, sum_qty_on_hand
order by warehouse_name, product_id_magento, packsize

select w.idWarehouse_sk idWarehouse_sk_fk, pf.idProductFamily_sk idProductFamily_sk_fk, t.packsize pack_size, 
	t.local_product_cost, t.local_total_cost, 
	t.global_product_cost, t.global_total_cost
from
		(select warehouse_name, product_id_magento, packsize, 
			sum(local_unit_cost * qty_on_hand) / sum_qty_on_hand local_product_cost, 
			sum(local_unit_cost * qty_on_hand) / sum_qty_on_hand local_total_cost, 
			sum(global_unit_cost * qty_on_hand) / sum_qty_on_hand global_product_cost, 
			sum(global_unit_cost * qty_on_hand) / sum_qty_on_hand global_total_cost 

		from
			(select warehouse_name, product_id_magento, packsize, 
				qty_on_hand, sum(qty_on_hand) over (partition by warehouse_name, product_id_magento, packsize) sum_qty_on_hand,
				local_unit_cost, global_unit_cost
			from #product_family_cost_value) pfcv
		group by warehouse_name, product_id_magento, packsize, sum_qty_on_hand) t
	inner join
		Warehouse.gen.dim_warehouse w on t.warehouse_name = w.warehouse_name
	inner join
		Warehouse.prod.dim_product_family_v pf on t.product_id_magento = pf.product_id
order by idWarehouse_sk_fk, idProductFamily_sk_fk, pack_size


select *
from Warehouse.prod.dim_product_family_v
order by product_id_magento