
select distinct warehouse_name, sku, due_date
from Warehouse.fm.fact_planned_requirements_current_v

select top 1000 warehouse_name, sku, due_date, count(*)
from Warehouse.fm.fact_planned_requirements_current_v
group by warehouse_name, sku, due_date
having count(*) > 1
order by count(*) desc


select top 1000 warehouse_name, sku, due_date, fm_reference, count(*)
from Warehouse.fm.fact_planned_requirements_current_v
group by warehouse_name, sku, due_date, fm_reference
having count(*) > 1
order by count(*) desc

select *
from Warehouse.fm.fact_planned_requirements_current_v
where warehouse_name = 'Girona'	and sku = '02338B3D4CF00000000AF021' and due_date = '2019-07-10'

select pass_num, count(*)
from
	(select fm_reference, 
		substring(fm_reference, charindex('PASS', fm_reference), 5) pass_num
	from Warehouse.fm.fact_planned_requirements_current_v) t
group by pass_num