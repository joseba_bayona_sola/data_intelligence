
drop table #scheduled_receipts


	select 
		coalesce(pol.PO_LineID, 0) AS PO_LineID, coalesce(si.PackSize, 0) AS PackSize, coalesce(r.ShipmentType, '') AS ShipmentType,
		coalesce(rl.StockItem, '') AS sku, coalesce(wh.ShortName,   '') AS warehouse_name, coalesce(po.OrderNumber,'') AS purchase_order_number, 
		coalesce(rl.QuantityOrdered, 0.0) AS qty_ordered,  
		rl.Created AS created_date,
		coalesce(r.Status, '') AS receipt_status_name, coalesce(r.ReceiptId, '') AS receipt_number, coalesce(po.Status, '') AS po_status_name, coalesce(po.Source, '') AS po_source_name, coalesce(su.SupplierName, '') AS supplier_name, 
		coalesce(rl.UnitCost, 0.0) AS purchase_unit_cost, 
		coalesce(rl.QuantityAccepted, 0.0) AS qty_accepted,
		r.createdDate AS created_date2, po.createdDate AS po_created_date, r.DueDate AS due_date, r.ArrivedDate AS arrived_date, r.ConfirmedDate AS confirmed_date, r.StockRegisteredDate AS stock_registered_date 
	into #scheduled_receipts
	from
			Landing.mend.whship_receiptline rl
		left outer join 
			Landing.mend.whship_receiptline_receipt rl_r on rl.id = rl_r.receiptlineid
		left outer join 
			Landing.mend.whship_receipt r on rl_r.receiptid = r.id
		left outer join 
			Landing.mend.whship_receipt_warehouse r_wh on r.id = r_wh.receiptid
		left outer join 
			Landing.mend.wh_warehouse wh on r_wh.warehouseid = wh.id
		left outer join 
			Landing.mend.whship_receiptline_purchaseorderline rl_pol on rl.id = rl_pol.receiptlineid
		left outer join 
			Landing.mend.purc_purchaseorderline pol on rl_pol.purchaseorderlineid = pol.id
		left outer join 
			Landing.mend.purc_purchaseorderline_stockitem pol_si on pol.id = pol_si.purchaseorderlineid
		left outer join 
			Landing.mend.prod_stockitem_aud si on pol_si.stockitemid = si.id
		left outer join 
			Landing.mend.purc_purchaseorderline_purchaseorderlineheader pol_polh on pol.id = pol_polh.purchaseorderlineid
		left outer join 
			Landing.mend.purc_purchaseorderlineheader polh on pol_polh.purchaseorderlineheaderid = polh.id
		left outer join 
			Landing.mend.purc_purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorderlineheaderid
		left outer join 
			Landing.mend.purc_purchaseorder po on polh_po.purchaseorderid = po.id
		left outer join 
			Landing.mend.purc_purchaseorder_supplier po_su on po.id = po_su.purchaseorderid
		left outer join 
			Landing.mend.supp_supplier su on po_su.supplierid = su.id
	where r.status NOT IN ('Cancelled', 'Stock_Registered', 'Aggregated')

	select top 1000 *
	from #scheduled_receipts

	select count(*)
	from #scheduled_receipts

		select shipmentType, po_source_name, count(*)
		from #scheduled_receipts
		where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
		group by shipmentType, po_source_name
		order by shipmentType, po_source_name

---------------------------------------------------------------------------------



select warehouse_name, parameter, fm_date, product_id_magento, product_family_name, packsize, sku, 
	qty_sales_forecast, qty_orders, qty_stock_on_hand
from Warehouse.fm.fact_fm_data_v
where warehouse_name = 'York' and sku = '02317B3D4CH0000000000301'
	and qty_orders <> 0
order by warehouse_name, parameter, fm_date, sku

select top 1000 warehouse_name, parameter, product_id_magento, product_family_name, packsize, sku, 
	fm_reference, release_date, due_date, quantity
from Warehouse.fm.fact_planned_requirements_current_v
where warehouse_name = 'York' and sku = '02317B3D4CH0000000000301'
order by warehouse_name, parameter, due_date, sku

	select due_date, count(*), sum(quantity)
	from Warehouse.fm.fact_planned_requirements_current_v
	group by due_date
	order by due_date

select isnull(t1.warehouse_name, t2.warehouse_name) warehouse_name, isnull(t1.fm_date, t2.due_date) fm_date, 
	isnull(t1.product_id_magento, t2.product_id_magento) product_id_magento, isnull(t1.product_family_name, t2.product_family_name) product_family_name, 
	isnull(t1.packsize, t2.packsize) packsize, isnull(t1.sku, t2.sku) sku, 
	t1.qty_sales_forecast, t1.qty_orders, t1.qty_stock_on_hand, 

	t2.due_date, t2.quantity, t2.fm_reference
from 
		(select warehouse_name, parameter, fm_date, product_id_magento, product_family_name, packsize, sku, 
			qty_sales_forecast, qty_orders, qty_stock_on_hand
		from Warehouse.fm.fact_fm_data_v
		where warehouse_name = 'York' and product_id_magento = '2317' and packsize = 30
			-- and sku = '02317B3D4CH0000000000301'
			and qty_orders <> 0) t1
	full join	
		(select warehouse_name, parameter, product_id_magento, product_family_name, packsize, sku, 
			fm_reference, release_date, due_date, quantity
		from Warehouse.fm.fact_planned_requirements_current_v
		where warehouse_name = 'York' and product_id_magento = '2317' and packsize = 30
			-- and sku = '02317B3D4CH0000000000301'
			) t2 on t1.warehouse_name = t2.warehouse_name and t1.SKU = t2.SKU and t1.fm_date = t2.due_date
where isnull(t1.fm_date, t2.due_date) > GETUTCDATE()
order by warehouse_name, fm_date, sku

-----------------------------------------------

select top 1000 warehouse_name, supplier_name, receipt_status_name, receipt_number, purchase_order_number, po_source_name, po_type_name, 
	created_date, stock_registered_date, due_date, sku, qty_ordered, qty_accepted
from Warehouse.rec.fact_receipt_line_v
where warehouse_name = 'Amsterdam' and sku = '01083B2D4AS0000000000301'
	-- and receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
order by due_date

		select warehouse_name, supplier_name, receipt_status_name, receipt_number, purchase_order_number, wh_shipment_type_name, po_source_name, po_type_name, 
			created_date, stock_registered_date, due_date, sku, qty_ordered, qty_accepted 
		from Warehouse.rec.fact_receipt_line_v
		where -- warehouse_name = 'York' and 
			product_id_magento = '2317' and packsize = 30
			and sku = '02317B3D4CF0000000000301'
			and receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
		order by warehouse_name, due_date

	select warehouse_name, convert(date, due_date) due_date, sku, sum(qty_ordered) qty_ordered
	from Warehouse.rec.fact_receipt_line_v
	where warehouse_name = 'Amsterdam' and sku = '01083B2D4AS0000000000301'
		and receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
	group by warehouse_name, convert(date, due_date), sku
	order by due_date

	select count(*)
	from Warehouse.rec.fact_receipt_line_v
	where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
		
	select convert(date, due_date) due_date, count(*)
	from Warehouse.rec.fact_receipt_line_v
	where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
	group by convert(date, due_date) 
	order by convert(date, due_date) 

	select wh_shipment_type_name, po_source_name, count(*)
	from Warehouse.rec.fact_receipt_line_v
	where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
	group by wh_shipment_type_name, po_source_name
	order by wh_shipment_type_name, po_source_name

	select wh_shipment_type_name, po_source_name, po_type_name, count(*)
	from Warehouse.rec.fact_receipt_line_v
	where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
	group by wh_shipment_type_name, po_source_name, po_type_name
	order by wh_shipment_type_name, po_source_name, po_type_name

		select warehouse_name, supplier_name, receipt_status_name, receipt_number, purchase_order_number, po_source_name, po_type_name, 
			created_date, stock_registered_date, due_date, sku, qty_ordered, qty_accepted
		from Warehouse.rec.fact_receipt_line_v
		where receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated')
			and wh_shipment_type_name = 'Stock' and  po_source_name = 'Back_To_Back' and  po_type_name  = 'Supply' 
		order by due_date


select isnull(t1.warehouse_name, t3.warehouse_name) warehouse_name, isnull(t1.fm_date, t3.due_date) fm_date, 
	t1.product_id_magento, t1.product_family_name, 
	t1.packsize, isnull(t1.sku, t3.sku) sku,  
	
	isnull(t1.qty_orders, 0) qty_orders, 
	isnull(t1.quantity, 0) qty_orders_plan_req, 
	isnull(t3.qty_ordered, 0) qty_orders_sch_rec, 
	isnull(t1.quantity, 0) + isnull(t3.qty_ordered, 0) qty_orders_calc, 
	t1.fm_reference
into #fm_qty_orders_check
from
		(select isnull(t1.warehouse_name, t2.warehouse_name) warehouse_name, isnull(t1.fm_date, t2.due_date) fm_date, 
			isnull(t1.product_id_magento, t2.product_id_magento) product_id_magento, isnull(t1.product_family_name, t2.product_family_name) product_family_name, 
			isnull(t1.packsize, t2.packsize) packsize, isnull(t1.sku, t2.sku) sku, 
			t1.qty_sales_forecast, t1.qty_orders, t1.qty_stock_on_hand, 

			t2.due_date, t2.quantity, t2.fm_reference
		from 
				(select warehouse_name, parameter, fm_date, product_id_magento, product_family_name, packsize, sku, 
					qty_sales_forecast, qty_orders, qty_stock_on_hand
				from Warehouse.fm.fact_fm_data_v
				where -- warehouse_name = 'York' and 
					product_id_magento = product_id_magento -- and packsize = 30
					-- and sku = '02317B3D4CH0000000000301'
					and qty_orders <> 0) t1
			full join	
				(select warehouse_name, product_id_magento, product_family_name, packsize, sku, due_date, min(fm_reference) fm_reference, sum(quantity) quantity
				from Warehouse.fm.fact_planned_requirements_current_v ------------- NEED TO GROUP BY warehouse_name, sku, due_date
				where -- warehouse_name = 'York' and 
					product_id_magento = product_id_magento -- and packsize = 30
					-- and sku = '02317B3D4CH0000000000301'
				group by warehouse_name, product_id_magento, product_family_name, packsize, sku, due_date) t2 
					on t1.warehouse_name = t2.warehouse_name and t1.SKU = t2.SKU and t1.fm_date = t2.due_date) t1
	full join
		(select warehouse_name, convert(date, due_date) due_date, sku, sum(qty_ordered) qty_ordered
		from Warehouse.rec.fact_receipt_line_v
		where -- warehouse_name = 'York' and 
			product_id_magento = product_id_magento -- and packsize = 30
			-- and sku = '02317B3D4CH0000000000301'
			and receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated') 
				-- and (po_type_name <> 'Supply' or (po_type_name = 'Supply' and wh_shipment_type_name = 'Stock'))
				and wh_shipment_type_name <> 'Back_To_Back'
		group by warehouse_name, convert(date, due_date), sku) t3 on t1.warehouse_name = t3.warehouse_name and t1.sku = t3.sku and t1.fm_date = t3.due_date
where isnull(t1.fm_date, t3.due_date) between GETUTCDATE() and dateadd(mm, 6, getutcdate())
	-- and isnull(t1.qty_orders, 0) <> isnull(t1.quantity, 0) + isnull(t3.qty_ordered, 0)
	-- and isnull(t1.sku, t3.sku) = '02317B3D4CF0000000000301'
order by warehouse_name, fm_date, sku

	select count(*)
	from #fm_qty_orders_check
	where qty_orders <> qty_orders_calc

	select top 1000 count(*) over (partition by warehouse_name, sku, fm_date) num_rep, *
	from #fm_qty_orders_check
	order by num_rep desc, warehouse_name, sku, fm_date

	select top 1000 *
	from #fm_qty_orders_check
	where warehouse_name = 'York'and sku = '02341B5D6CEC20000AJ00301'
	order by warehouse_name, sku, fm_date

	select count(*) over (), *, abs(qty_orders - qty_orders_calc)
	from #fm_qty_orders_check
	where qty_orders <> qty_orders_calc 
		-- and abs(qty_orders - qty_orders_calc) > 1
		-- and qty_orders_sch_rec <> 0
		-- and qty_orders_plan_req <> 0
		-- and qty_orders_sch_rec = 0 and qty_orders_plan_req = 0
	order by 
		abs(qty_orders - qty_orders_calc) desc, 
		sku, warehouse_name, fm_date

	select top 1000 substring(sku, 2, 4), count(*) 
	from #fm_qty_orders_check
	where qty_orders <> qty_orders_calc
	group by substring(sku, 2, 4)
	order by substring(sku, 2, 4)

	drop table #fm_qty_orders_check
