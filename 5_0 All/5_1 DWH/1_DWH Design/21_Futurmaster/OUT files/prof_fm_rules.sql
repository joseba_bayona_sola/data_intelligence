
select *
from Warehouse.fm.fact_fm_data_v
where product_id_magento = '1083' and parameter = '01083B2D4AS0000000000'
order by warehouse_name, parameter, fm_date, sku

select count(*) over (partition by product_id_magento) num_rep, 
	rank() over (partition by product_id_magento order by packsize) ord_rep, 
	product_id_magento, product_family_name, packsize, count(*) num_rows
into #fm_product_family
from Warehouse.fm.fact_fm_data_v
-- where product_id_magento = '1083' and parameter = '01083B2D4AS0000000000'
group by product_id_magento, product_family_name, packsize
order by num_rep desc, product_id_magento, product_family_name, packsize

select warehouse_name, product_id_magento, product_family_name, packsize, count(*)
from Warehouse.fm.fact_fm_data_v
-- where product_id_magento = '1083' and parameter = '01083B2D4AS0000000000'
group by warehouse_name, product_id_magento, product_family_name, packsize
order by warehouse_name, product_id_magento, product_family_name, packsize

select warehouse_name, product_id_magento, parameter, packsize, count(*)
from Warehouse.fm.fact_fm_data_v
where product_id_magento = '1083' and parameter = '01083B2D4AS0000000000'
group by warehouse_name, product_id_magento, parameter, packsize
order by warehouse_name, product_id_magento, parameter, packsize


-- SF, GR, SUPP
select top 1000 count(*) over (),
	warehouse_name, product_id_magento, product_family_name, packsize, parameter, sku, 
	fm_date, 
	qty_sales_forecast, qty_gross_req, qty_suppressions, 
	abs(qty_gross_req - qty_sales_forecast - qty_suppressions) qty_sales_forecast_diff
from Warehouse.fm.fact_fm_data_v
where -- product_id_magento = '1083' and
	-- qty_gross_req <> qty_sales_forecast + qty_suppressions -- GR = SF + SUPP
	-- qty_suppressions <> qty_gross_req - qty_sales_forecast -- SUPP = GR - SF 
	qty_suppressions <> 0
	-- 0 = 0
-- order by qty_sales_forecast_diff desc, warehouse_name, parameter, fm_date, sku
order by qty_sales_forecast_diff desc, warehouse_name, parameter, fm_date, sku

-- SUPP
select top 1000 count(*) over (),
	warehouse_name, product_id_magento, product_family_name, packsize, parameter, sku, 
	fm_date, 
	qty_sales_forecast, qty_gross_req, qty_suppressions
from Warehouse.fm.fact_fm_data_v
where -- product_id_magento = '1083' and
	qty_suppressions <> 0
	-- 0 = 0
order by qty_suppressions, warehouse_name, parameter, fm_date, sku


select top 1000 
	count(*) over (),
	count(*) over (partition by fm1.warehouse_name, fm1.parameter, fm1.fm_date) num_rep,
	fm1.warehouse_name, fm1.product_id_magento, fm1.product_family_name, fm1.packsize, fm2.packsize, fm1.parameter, 
	fm1.fm_date, 
	fm1.sku, fm2.sku, 	
	fm1.qty_suppressions, fm2.qty_suppressions 
from
		(select fm.*
		from 
				Warehouse.fm.fact_fm_data_v fm
			inner join
				#fm_product_family pf on fm.product_id_magento = pf.product_id_magento			
		where pf.product_id_magento = '1083' and pf.ord_rep = 1) fm1
	full join
		(select fm.*
		from 
				Warehouse.fm.fact_fm_data_v fm
			inner join
				#fm_product_family pf on fm.product_id_magento = pf.product_id_magento			
		where pf.product_id_magento = '1083' and pf.ord_rep = 2) fm2 
			on fm1.warehouse_name = fm2.warehouse_name and fm1.product_id_magento = fm2.product_id_magento and fm1.parameter = fm2.parameter and fm1.fm_date = fm2.fm_date
				and fm1.packsize <> fm2.packsize
where (fm1.qty_suppressions <> 0 or fm2.qty_suppressions <> 0)
	and fm1.packsize < fm2.packsize
order by warehouse_name, parameter, fm_date, fm1.sku

select *
from #fm_product_family

-- ORD

select top 1000 count(*) over (),
	fm.warehouse_name, fm.product_id_magento, fm.product_family_name, fm.packsize, fm.parameter, fm.sku, 
	fm.fm_date, pr.due_date,
	fm.qty_orders, pr.quantity
from 
		Warehouse.fm.fact_fm_data_v fm
	full join
		(select warehouse_name, product_id_magento, product_family_name, packsize, parameter, sku, 
			due_date, quantity
		from Warehouse.fm.fact_planned_requirements_v
		where product_id_magento = '1083'
		) pr on fm.warehouse_name = pr.warehouse_name and fm.sku = pr.sku and fm.fm_date = pr.due_date
where fm.product_id_magento = '1083' and
	-- fm.qty_orders is null
	-- fm.qty_orders <> 0
	fm.qty_orders <> 0 and (pr.quantity is null or fm.qty_orders <> pr.quantity)
order by warehouse_name, parameter, fm_date, sku


-- SOH

select top 1000 count(*) over (), *, 
	qty_stock_on_hand - qty_gross_req + qty_orders,
	next_qty_stock_on_hand - ( qty_stock_on_hand - qty_gross_req + qty_orders) diff
from 
	(select 
		warehouse_name, product_id_magento, product_family_name, packsize, parameter, sku, 
		fm_date, 
		qty_sales_forecast, qty_gross_req, qty_suppressions, qty_orders, qty_stock_on_hand, lead(qty_stock_on_hand) over (order by warehouse_name, sku, fm_date) next_qty_stock_on_hand
	from Warehouse.fm.fact_fm_data_v
	where product_id_magento = '1083' -- and warehouse_name = 'York' and sku = '01083B2D4CF0000000000301'
	) fm
where product_id_magento = '1083' 
	and abs(next_qty_stock_on_hand - ( qty_stock_on_hand - qty_gross_req + qty_orders)) > 1
-- order by diff desc, warehouse_name, sku, fm_date
order by warehouse_name, sku, fm_date

select top 1000 count(*) over (), *, 
	qty_stock_on_hand - qty_gross_req + qty_orders,
	next_qty_stock_on_hand - ( qty_stock_on_hand - qty_gross_req + qty_orders) diff
from 
	(select 
		warehouse_name, product_id_magento, product_family_name, packsize, parameter, sku, 
		fm_date, 
		qty_sales_forecast, qty_gross_req, qty_suppressions, qty_orders, qty_shortages,
		qty_stock_on_hand, lead(qty_stock_on_hand) over (order by warehouse_name, sku, fm_date) next_qty_stock_on_hand
	from Warehouse.fm.fact_fm_data_v
	where product_id_magento = '1083' and warehouse_name = 'York' and sku = '01083B2D4BZ0000000000301'
	) fm
where product_id_magento = '1083' 
order by warehouse_name, sku, fm_date


-- SH