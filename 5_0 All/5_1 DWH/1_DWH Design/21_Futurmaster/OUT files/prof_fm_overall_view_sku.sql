
				select warehouse_name, sku, fm_date, 
					qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
				from Warehouse.fm.fact_fm_data_v
				where warehouse_name = 'York' 
					and sku = '01083B2D4AW0000000000301'
				order by fm_date

				select top 1000 warehouse_name, sku, idCalendarFMDate_sk_fk, 
					fmd.qty_sales_forecast, fmd.qty_gross_req, fmd.qty_orders, fmd.qty_stock_on_hand, fmd.qty_shortages, fmd.qty_safety_stock, fmd.qty_min_stock, fmd.qty_max_stock, fmd.qty_suppressions
				from 
						Warehouse.fm.fact_fm_data_wrk fmd
					inner join
						Warehouse.gen.dim_warehouse_v w on fmd.idWarehouse_sk_fk = w.idWarehouse_sk
					inner join
						Warehouse.prod.dim_stock_item_v si on fmd.idStockItem_sk_fk = si.idStockItem_sk
				where warehouse_name = 'York' 
					and sku = '01083B2D4AW0000000000301'
				order by idCalendarFMDate_sk_fk

				select top 1000 record_type, idFMData_sk, 
					warehouse_name, product_id_magento, product_family_name, packsize, sku, -- stock_item_description, 
					fm_date, convert(date, aud_dateTo),
					qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
					qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions
				from Warehouse.fm.fact_fm_data_aud_v
				where warehouse_name = 'York' 
					and sku = '01083B2D4AW0000000000301'
					and fm_date = '2019-05-15'
				order by fm_date, record_type, convert(date, aud_dateTo)
