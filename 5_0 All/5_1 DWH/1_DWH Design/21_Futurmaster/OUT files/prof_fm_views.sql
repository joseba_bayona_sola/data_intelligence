
	select top 1000 
		w.warehouse_name, si.product_id_magento, 
		si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
		si.product_family_name, si.packsize, 
		si.parameter, si.product_description,
		si.SKU, si.stock_item_description, 
		c.calendar_date fm_date, 
				
		fmd.sale_type, 
		fmd.qty_sales_forecast, fmd.qty_gross_req, fmd.qty_orders, fmd.qty_stock_on_hand, fmd.qty_shortages, 
		fmd.qty_safety_stock, fmd.qty_min_stock, fmd.qty_max_stock, fmd.qty_suppressions, 

		si.packsize / pfsv.size,

		pfsv.local_subtotal * si.packsize / pfsv.size local_subtotal, pfsv.local_total_exc_vat * si.packsize / pfsv.size local_total_exc_vat, pfsv.local_total_inc_vat * si.packsize / pfsv.size local_total_inc_vat,
		pfsv.global_subtotal * si.packsize / pfsv.size global_subtotal, pfsv.global_total_exc_vat * si.packsize / pfsv.size global_total_exc_vat, pfsv.global_total_inc_vat * si.packsize / pfsv.size global_total_inc_vat,
		pfcv.global_product_cost, pfcv.global_total_cost
	from 
			Warehouse.fm.fact_fm_data fmd
		inner join
			Warehouse.gen.dim_warehouse_v w on fmd.idWarehouse_sk_fk = w.idWarehouse_sk
		inner join
			Warehouse.prod.dim_stock_item_v si on fmd.idStockItem_sk_fk = si.idStockItem_sk
		inner join
			Warehouse.gen.dim_calendar c on fmd.idCalendarFMDate_sk_fk = c.idCalendar_sk
		left outer join 
			-- Warehouse.fm.dim_fm_product_family_sales_value 
			(select pfsv.*, pfps.size
			from 
					Warehouse.fm.dim_fm_product_family_sales_value pfsv
				inner join 
					(select idProductFamily_sk, product_id_magento, product_family_name, min(size) size
					from Warehouse.prod.dim_product_family_pack_size_v
					group by idProductFamily_sk, product_id_magento, product_family_name) pfps on pfsv.idProductFamily_sk_fk = pfps.idProductFamily_sk) pfsv on pfsv.idWarehouse_sk_fk = w.idWarehouse_sk and pfsv.idProductFamily_sk_fk = si.idProductFamily_sk 
		left outer join 
			Warehouse.fm.dim_fm_product_family_cost_value pfcv on pfcv.idWarehouse_sk_fk = w.idWarehouse_sk 
				and pfcv.idProductFamily_sk_fk = si.idProductFamily_sk 
				and si.packsize = pfcv.pack_size
	where product_id_magento = '1083' 
		and parameter = '01083B2D4CX0000000000' and warehouse_name = 'York' -- and c.calendar_date = '2019-07-18'
	order by c.calendar_date, packsize

--------------------------------------------------------
--------------------------------------------------------
--------------------------------------------------------

			select record_type, count(*) over (partition by idWHStockItemStaticData_sk) num_records,
				t.idWHStockItemStaticData_sk,
				w.warehouse_name, si.product_id_magento, 
				si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
				si.product_family_name, si.packsize, 
				si.parameter, si.product_description,
				si.SKU, si.stock_item_description, 

				t.sale_type, 
				t.planner, t.abc, t.stockable, t.stocked, t.final, 
				t.aud_type, t.aud_dateFrom, t.aud_dateTo
			from 
				(select 'N' record_type, 
					idWHStockItemStaticData_sk ,
					idWarehouse_sk_fk, idStockItem_sk_fk,
					idSupplier_sk_fk, sale_type, 
					planner, abc, stockable, stocked, final,
					idETLBatchRun_ins, ins_ts,
					null aud_type, null aud_dateFrom, null aud_dateTo 
				from Warehouse.fm.dim_wh_stock_item_static_data
				where idWHStockItemStaticData_sk in (select distinct idWHStockItemStaticData_sk_fk from Warehouse.fm.dim_wh_stock_item_static_data_aud)
				union
				select 'H' record_type, 
					idWHStockItemStaticData_sk_fk idWHStockItemStaticData_sk,
					idWarehouse_sk_fk, idStockItem_sk_fk,
					idSupplier_sk_fk, sale_type, 
					planner, abc, stockable, stocked, final,
					idETLBatchRun_ins, ins_ts,
					aud_type, aud_dateFrom, aud_dateTo 
				from Warehouse.fm.dim_wh_stock_item_static_data_aud) t

			inner join
				Warehouse.gen.dim_warehouse_v w on t.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk_fk = si.idStockItem_sk
		order by num_records desc, idWHStockItemStaticData_sk, record_type, t.aud_dateFrom

--------------------------------------------------------

		select top 1000 record_type, count(*) over (partition by idFMData_sk) num_records,
			t.idFMData_sk,
			
			w.warehouse_name, si.product_id_magento, 
			si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
			si.product_family_name, si.packsize, 
			si.parameter, si.product_description,
			si.SKU, si.stock_item_description, 
			c.calendar_date fm_date, 

			t.sale_type, 
			t.qty_sales_forecast, t.qty_gross_req, t.qty_orders, t.qty_stock_on_hand, t.qty_shortages, 
			t.qty_safety_stock, t.qty_min_stock, t.qty_max_stock, t.qty_suppressions,
			t.aud_type, t.aud_dateFrom, t.aud_dateTo
		from 
			(select 'N' record_type,
				idFMData_sk,
				idWarehouse_sk_fk, idStockItem_sk_fk, idCalendarFMDate_sk_fk,
				sale_type, 
				qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
				qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions,
				idETLBatchRun_ins, ins_ts, 
				null aud_type, null aud_dateFrom, null aud_dateTo 
			from Warehouse.fm.fact_fm_data
			where idFMData_sk in (select distinct idFMData_sk_fk from Warehouse.fm.fact_fm_data_aud)
			union		
			select 'H' record_type,
				idFMData_sk_fk idFMData_sk,
				idWarehouse_sk_fk, idStockItem_sk_fk, idCalendarFMDate_sk_fk,
				sale_type, 
				qty_sales_forecast, qty_gross_req, qty_orders, qty_stock_on_hand, qty_shortages, 
				qty_safety_stock, qty_min_stock, qty_max_stock, qty_suppressions,
				idETLBatchRun_ins, ins_ts, 
				aud_type, aud_dateFrom, aud_dateTo 
			from Warehouse.fm.fact_fm_data_aud) t
		inner join
			Warehouse.gen.dim_warehouse_v w on t.idWarehouse_sk_fk = w.idWarehouse_sk
		inner join
			Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk_fk = si.idStockItem_sk
		inner join
			Warehouse.gen.dim_calendar c on t.idCalendarFMDate_sk_fk = c.idCalendar_sk
	where product_id_magento = '1083' 
		and sku = '01083B2D4CX0000000000301' and warehouse_name = 'York' and c.calendar_date = '2019-07-18'
	order by num_records desc, idFMData_sk, record_type, t.aud_dateFrom

--------------------------------------------------------

	select top 1000 record_type, count(*) over (partition by idPlannedRequirements_sk) num_records,
		t.idPlannedRequirements_sk,

		w.warehouse_name, si.product_id_magento, 
		si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
		si.product_family_name, si.packsize, 
		si.parameter, si.product_description,
		si.SKU, si.stock_item_description, 
		s.supplier_type_name, s.supplier_name,
		t.fm_reference,
		c_rel.calendar_date release_date, c_due.calendar_date due_date, 

		t.planner, t.overdue_status, 
		t.pack_size, t.quantity, t.unit_cost, 
		t.aud_type, t.aud_dateFrom, t.aud_dateTo
	from 
			(select 'N' record_type, 
				idPlannedRequirements_sk, 
				idWarehouse_sk_fk, idStockItem_sk_fk, idSupplier_sk_fk, fm_reference,
				idWarehouseSource_sk_fk, idCalendarReleaseDate_sk_fk, idCalendarDueDate_sk_fk,
				planner, overdue_status, 
				pack_size, quantity, unit_cost,
				idETLBatchRun_ins, ins_ts, 
				null aud_type, null aud_dateFrom, null aud_dateTo 
			from Warehouse.fm.fact_planned_requirements
			where idPlannedRequirements_sk in (select distinct idPlannedRequirements_sk_fk from Warehouse.fm.fact_planned_requirements_aud)
			union
			select 'H' record_type, 
				idPlannedRequirements_sk_fk idPlannedRequirements_sk, 
				idWarehouse_sk_fk, idStockItem_sk_fk, idSupplier_sk_fk, fm_reference,
				idWarehouseSource_sk_fk, idCalendarReleaseDate_sk_fk, idCalendarDueDate_sk_fk,
				planner, overdue_status, 
				pack_size, quantity, unit_cost,
				idETLBatchRun_ins, ins_ts, 
				aud_type, aud_dateFrom, aud_dateTo 
			from Warehouse.fm.fact_planned_requirements_aud) t
		inner join
			Warehouse.gen.dim_warehouse_v w on t.idWarehouse_sk_fk = w.idWarehouse_sk
		inner join
			Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk_fk = si.idStockItem_sk
		inner join
			Warehouse.gen.dim_supplier_v s on t.idSupplier_sk_fk = s.idSupplier_sk
		left join
			Warehouse.gen.dim_calendar c_rel on t.idCalendarReleaseDate_sk_fk = c_rel.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar c_due on t.idCalendarDueDate_sk_fk = c_due.idCalendar_sk
	order by num_records desc, idPlannedRequirements_sk, record_type, t.aud_dateFrom