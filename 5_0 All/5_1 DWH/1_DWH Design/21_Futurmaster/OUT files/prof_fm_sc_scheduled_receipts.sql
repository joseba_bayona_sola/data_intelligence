
select count(*) over (partition by warehouse, po_number, po_source, receipt_creation_year, receipt_creation_month, receipt_creation_day, sku) num_rep, *
from DW_GetLenses_jbs.dbo.scheduled_receipts
order by num_rep desc, warehouse, po_number, po_source, receipt_creation_year, receipt_creation_month, receipt_creation_day, sku

select top 1000 
	count(*) over () num_tot, 
	count(*) over (partition by warehouse, po_number, po_source, receipt_creation_year, receipt_creation_month, receipt_creation_day, sku) num_rep,
	warehouse, 
	buy_from_supplier, 
	
	po_number, po_status, po_source, 
	po_creation_year, po_creation_month, po_creation_day, 
	
	receipt_id, receipt_status, 
	receipt_creation_year, receipt_creation_month, receipt_creation_day,
	arrived_year, arrived_month, arrived_day, registered_year, registered_month, registered_day, 
	received_year, received_month, received_day, expected_year, expected_month, expected_day,

	sku, 
	qty_received, qty_remaining_receive, 
	unit_cost, 
	received_value, open_value
from DW_GetLenses_jbs.dbo.scheduled_receipts
order by num_rep desc, warehouse, po_number, po_source, receipt_creation_year, receipt_creation_month, receipt_creation_day, sku

 

select 
	row_number() over (partition by sr.sku, sr.po_number, sr.warehouse order by sr.sku, sr.po_number, sr.warehouse) r_n,
	sr.* 
from 
	scheduled_receipts sr 
	inner join 
		(select sku, po_number, warehouse, receipt_creation_year, receipt_creation_month, receipt_creation_day, po_source 
		from scheduled_receipts
		group by sku, po_number, warehouse, receipt_creation_year, receipt_creation_month, receipt_creation_day, po_source 
		having count(1) > 1) v
	on sr.sku = v.sku and sr.PO_Number = v.PO_Number and sr.Warehouse = v.Warehouse
order by 2, 3, 4;