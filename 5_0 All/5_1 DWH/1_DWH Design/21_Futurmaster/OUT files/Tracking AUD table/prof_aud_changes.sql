
select top 1000 *
from Warehouse.fm.fact_fm_data_wrk
where idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 139
ORDER BY idCalendarFMDate_sk_fk

select top 1000 *
from Warehouse.fm.fact_fm_data
where idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 139
order by idCalendarFMDate_sk_fk

select top 1000 *
from Warehouse.fm.fact_fm_data_v
where idWarehouse_sk = 1 and idStockItem_sk = 139
order by fm_date 


-----------------------------------------------------------------

select top 1000 *
from Warehouse.gen.dim_warehouse

select top 1000 *
from Warehouse.prod.dim_product
where parameter = '01089B3D7CWC70000B200'

select top 1000 *
from Warehouse.prod.dim_stock_item
where sku like '01089B3D7BVC10000B200061'

-- 6 min
select top 1000 *
from Warehouse.fm.fact_fm_data_aud
where idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 139 and idCalendarFMDate_sk_fk = 20200401
order by ins_ts

select top 1000 *
from Warehouse.fm.fact_fm_data_aud
where idWarehouse_sk_fk = 4 and idStockItem_sk_fk = 9774 and idCalendarFMDate_sk_fk = 20200401
order by ins_ts

-----------------------------------------------------------------

select idCalendarFMDate_sk_fk, idWarehouse_sk_fk, idStockItem_sk_fk, count(*) num
into DW_GetLenses_jbs.dbo.fact_fm_data_aud_wsi
from Warehouse.fm.fact_fm_data_aud
group by idCalendarFMDate_sk_fk, idWarehouse_sk_fk, idStockItem_sk_fk

select top 1000 w.idWarehouse_sk, si.idStockItem_sk,
	w.warehouse_name, 
	si.product_id_magento, si.product_family_name, si.packsize,
	si.base_curve, si.diameter, si.power, si.cylinder, si.axis, si.addition, si.dominance, si.colour, 
	si.parameter, si.product_description,
	si.SKU, si.stock_item_description,
	t.idCalendarFMDate_sk_fk, t.num
from 
		DW_GetLenses_jbs.dbo.fact_fm_data_aud_wsi t
	inner join
		Warehouse.gen.dim_warehouse w on t.idWarehouse_sk_fk = w.idWarehouse_sk
	inner join
		Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk_fk = si.idStockItem_sk
-- where idWarehouse_sk_fk = 4 and idStockItem_sk_fk = 205968
where warehouse_name = 'Girona' and si.sku = '01089B3D7BVC10000B200061'
order by idCalendarFMDate_sk_fk

select w.warehouse_name, si.product_id_magento, si.product_family_name, count(*), sum(num)
from 
		DW_GetLenses_jbs.dbo.fact_fm_data_aud_wsi t
	inner join
		Warehouse.gen.dim_warehouse w on t.idWarehouse_sk_fk = w.idWarehouse_sk
	inner join
		Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk_fk = si.idStockItem_sk
group by w.warehouse_name, si.product_id_magento, si.product_family_name
order by si.product_id_magento, w.warehouse_name, si.product_family_name
