
				select warehouse_name, parameter, product_id_magento, product_family_name, packsize, sku, 
					fm_reference, planner, supplier_name, release_date, due_date, quantity, unit_cost
				select *
				from Warehouse.fm.fact_planned_requirements_current_v
				where warehouse_name = 'York' 
					and sku = '02414B3D4BY0000000000031'
				order by due_date

				select warehouse_name, supplier_name, receipt_status_name, receipt_number, purchase_order_number, po_source_name, po_type_name, 
					created_date, stock_registered_date, due_date, sku, qty_ordered, qty_accepted
				from Warehouse.rec.fact_receipt_line_v
				where warehouse_name = 'York' 
					and sku = '02414B3D4BY0000000000031'
					and receipt_status_name not in ('Cancelled', 'Stock_Registered', 'Aggregated') 
						-- and wh_shipment_type_name <> 'Back_To_Back'
				order by due_date
