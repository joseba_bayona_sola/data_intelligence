
drop table #product_family_sales_value

select top 1000 *
from #product_family_sales_value
order by idWarehouse_sk_fk, idProductFamily_sk_fk

select  
	ol.order_line_id_bk, ol.invoice_date, 
	ol.idWarehouse_sk_fk, ol.idProductFamily_sk_fk, ol.qty_unit, pfps.size, ol.qty_unit / pfps.size num_min_packs, 
	-- ol.local_subtotal, ol.local_total_exc_vat, ol.local_total_inc_vat, 
	ol.local_subtotal / (ol.qty_unit / pfps.size) local_subtotal, 
	ol.local_total_exc_vat / (ol.qty_unit / pfps.size) local_total_exc_vat, 
	ol.local_total_inc_vat / (ol.qty_unit / pfps.size) local_total_inc_vat, 

	(ol.local_subtotal / (ol.qty_unit / pfps.size)) * ol.local_to_global_rate global_subtotal, 
	(ol.local_total_exc_vat / (ol.qty_unit / pfps.size)) * ol.local_to_global_rate global_total_exc_vat, 
	(ol.local_total_inc_vat / (ol.qty_unit / pfps.size)) * ol.local_to_global_rate global_total_inc_vat,
	ol.order_currency_code, ol.local_to_global_rate
into #product_family_sales_value
from 
		Warehouse.sales.fact_order_line ol
	left join
		(select idProductFamily_sk, product_id_magento, product_family_name, min(size) size
		from Warehouse.prod.dim_product_family_pack_size_v
		group by idProductFamily_sk, product_id_magento, product_family_name) pfps on ol.idProductFamily_sk_fk = pfps.idProductFamily_sk
where invoice_date > getutcdate() - 60
	and ((idWarehouse_sk_fk in (2, 4) and order_currency_code = 'EUR') or idWarehouse_sk_fk = 1)
order by idWarehouse_sk_fk, idProductFamily_sk_fk

select idWarehouse_sk_fk, idProductFamily_sk_fk, 
	sum(local_subtotal * num_min_packs) / sum_num_min_packs local_subtotal, 
	sum(local_total_exc_vat * num_min_packs) / sum_num_min_packs local_total_exc_vat, 
	sum(local_total_inc_vat * num_min_packs) / sum_num_min_packs local_total_inc_vat, 
	sum(global_subtotal * num_min_packs) / sum_num_min_packs global_subtotal, 
	sum(global_total_exc_vat * num_min_packs) / sum_num_min_packs global_total_exc_vat, 
	sum(global_total_inc_vat * num_min_packs) / sum_num_min_packs global_total_inc_vat
from
	(select idWarehouse_sk_fk, idProductFamily_sk_fk, num_min_packs, sum(num_min_packs) over (partition by idWarehouse_sk_fk, idProductFamily_sk_fk) sum_num_min_packs,
		case when (idWarehouse_sk_fk = 1 and order_currency_code = 'EUR') then global_subtotal else local_subtotal end local_subtotal, 
		case when (idWarehouse_sk_fk = 1 and order_currency_code = 'EUR') then global_total_exc_vat else local_total_exc_vat end local_total_exc_vat, 
		case when (idWarehouse_sk_fk = 1 and order_currency_code = 'EUR') then global_total_inc_vat else local_total_inc_vat end local_total_inc_vat, 
		global_subtotal, global_total_exc_vat, global_total_inc_vat
	from #product_family_sales_value) pfsv
group by idWarehouse_sk_fk, idProductFamily_sk_fk, sum_num_min_packs
order by idWarehouse_sk_fk, idProductFamily_sk_fk


select top 1000 idWarehouse_sk_fk, order_currency_code, count(*)
from Warehouse.sales.fact_order_line
where invoice_date > getutcdate() - 30
	-- and ((idWarehouse_sk_fk in (2, 4) and order_currency_code = 'EUR') or idWarehouse_sk_fk = 1)
group by idWarehouse_sk_fk, order_currency_code
order by idWarehouse_sk_fk, order_currency_code
