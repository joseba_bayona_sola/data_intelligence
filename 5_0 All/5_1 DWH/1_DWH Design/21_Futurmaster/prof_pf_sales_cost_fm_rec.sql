
drop table #fm_wh_product

select top 1000 *
from Warehouse.fm.fact_fm_data_v

select warehouse_name, product_id_magento, product_family_name, packsize, count(*) num_rec
into #fm_wh_product
from Warehouse.fm.fact_fm_data_v
group by warehouse_name, product_id_magento, product_family_name, packsize
order by warehouse_name, product_id_magento, product_family_name, packsize

select *
from #fm_wh_product fm
order by fm.product_id_magento, fm.packsize, fm.warehouse_name

select fm.*, pfsv.local_total_exc_vat, pfsv.global_total_exc_vat, pfcv.local_product_cost, pfcv.global_product_cost
from 
		#fm_wh_product fm
	left join
		(select w.warehouse_name, pf.product_id, pf.product_family_name, pfsv.local_total_exc_vat, pfsv.global_total_exc_vat
		from 
				Warehouse.fm.dim_fm_product_family_sales_value pfsv
			inner join
				Warehouse.gen.dim_warehouse_v w on pfsv.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_product_family_v pf on pfsv.idProductFamily_sk_fk = pf.idProductFamily_sk) pfsv 
					on fm.warehouse_name = pfsv.warehouse_name and fm.product_id_magento = pfsv.product_id
	left join
		(select w.warehouse_name, pf.product_id, pfcv.pack_size, pfcv.local_product_cost, pfcv.global_product_cost
		from 
				Warehouse.fm.dim_fm_product_family_cost_value pfcv
			inner join
				Warehouse.gen.dim_warehouse_v w on pfcv.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_product_family_v pf on pfcv.idProductFamily_sk_fk = pf.idProductFamily_sk) pfcv 
					on fm.warehouse_name = pfcv.warehouse_name and fm.product_id_magento = pfcv.product_id and fm.packsize = pfcv.pack_size
-- where pfsv.warehouse_name is null -- 71 NULL on Sales Value
-- where pfcv.warehouse_name is null -- 102 NULL on Cost Value
-- where pfsv.warehouse_name is null or pfcv.warehouse_name is null 
order by fm.product_id_magento, fm.packsize, fm.warehouse_name


select fm.warehouse_name, fm.product_id_magento, fm.product_family_name, fm.packsize, pfsv.global_total_exc_vat, pfcv.global_product_cost
into #fm_wh_product_sales_cost
from 
		#fm_wh_product fm
	left join
		(select w.warehouse_name, pf.product_id, pf.product_family_name, pfsv.local_total_exc_vat, pfsv.global_total_exc_vat
		from 
				Warehouse.fm.dim_fm_product_family_sales_value pfsv
			inner join
				Warehouse.gen.dim_warehouse_v w on pfsv.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_product_family_v pf on pfsv.idProductFamily_sk_fk = pf.idProductFamily_sk) pfsv 
					on fm.warehouse_name = pfsv.warehouse_name and fm.product_id_magento = pfsv.product_id
	left join
		(select w.warehouse_name, pf.product_id, pfcv.pack_size, pfcv.local_product_cost, pfcv.global_product_cost
		from 
				Warehouse.fm.dim_fm_product_family_cost_value pfcv
			inner join
				Warehouse.gen.dim_warehouse_v w on pfcv.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_product_family_v pf on pfcv.idProductFamily_sk_fk = pf.idProductFamily_sk) pfcv 
					on fm.warehouse_name = pfcv.warehouse_name and fm.product_id_magento = pfcv.product_id and fm.packsize = pfcv.pack_size
order by fm.product_id_magento, fm.packsize, fm.warehouse_name

select *
from #fm_wh_product_sales_cost

select s.product_id_magento, s.product_family_name, s.packsize, 
	s.global_total_exc_vat_ams, c.global_product_cost_ams, 
	s.global_total_exc_vat_gir, c.global_product_cost_gir, 
	s.global_total_exc_vat_york, c.global_product_cost_york   
from
	(select product_id_magento, product_family_name, packsize, 
		Amsterdam global_total_exc_vat_ams, Girona global_total_exc_vat_gir, York global_total_exc_vat_york 
	from 
			(select warehouse_name, product_id_magento, product_family_name, packsize, global_total_exc_vat
			from #fm_wh_product_sales_cost) psc
		pivot
			(avg(global_total_exc_vat)
			for warehouse_name in (Amsterdam, Girona, York)) pvt) s
inner join
	(select product_id_magento, product_family_name, packsize, 
		Amsterdam global_product_cost_ams, Girona global_product_cost_gir, York global_product_cost_york 
	from 
			(select warehouse_name, product_id_magento, product_family_name, packsize, global_product_cost
			from #fm_wh_product_sales_cost) psc
		pivot
			(avg(global_product_cost)
			for warehouse_name in (Amsterdam, Girona, York)) pvt) c on s.product_id_magento = c.product_id_magento and s.product_family_name = c.product_family_name and s.packsize = c.packsize
where s.global_total_exc_vat_ams is null or c.global_product_cost_ams is null or s.global_total_exc_vat_gir is null or c.global_product_cost_gir is null or s.global_total_exc_vat_york is null or c.global_product_cost_york is null    
order by s.product_id_magento, s.product_family_name, s.packsize

------------------------------------------------

		select w.warehouse_name, pf.product_id, pf.product_family_name, pfcv.pack_size, pfcv.global_product_cost
		from 
				Warehouse.fm.dim_fm_product_family_cost_value pfcv
			inner join
				Warehouse.gen.dim_warehouse_v w on pfcv.idWarehouse_sk_fk = w.idWarehouse_sk
			inner join
				Warehouse.prod.dim_product_family_v pf on pfcv.idProductFamily_sk_fk = pf.idProductFamily_sk
		where warehouse_name = 'York'
		order by product_id, warehouse_name, pack_size
