
select top 1000 count(*) over (), *
from Landing.fm.fm_planned_requirements

	select top 1000 count(*) 
	from 
			Landing.fm.fm_planned_requirements pr
		inner join
			Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
		inner join
			(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
			from 
				(select si.stockitemid, si.sku, si.stockitemdescription, 
					isnull(wsi.num_records, 0) num_records,
					count(*) over (partition by si.sku) num_rep, 
					rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
				from 
						Landing.mend.gen_prod_stockitem_v si
					left join
						(select stockitemid, count(*) num_records
						from Landing.mend.gen_wh_warehousestockitem_v
						where wh_stock_item_batch_f = 'Y'
						group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
			where num_rep = ord_rep) si on pr.sku = si.sku
		inner join
			-- Landing.mend.gen_supp_supplier_v s on pr.supplier = s.supplierName
			(select s.supplier_id, s.supplierID, s.supplierName
			from
				(select supplier_id, supplierID, supplierName, 
					count(*) over (partition by supplierName) num_rep, 
					dense_rank() over (partition by supplierName order by supplier_id) ord_rep
				from Landing.mend.gen_supp_supplier_v) s
			where num_rep = ord_rep) s on pr.supplier = s.supplierName

select top 1000 count(*) over (), *
from Landing.fm.fm_planned_requirements
where destination_warehouse = 'York' and sku = '01083B2D4AS0000000000901' and supplier = 'J&J UK (Stock)' and fm_reference = '1083-YORK_RETAIL-90PASS120191204'

	select top 1000 count(*) over (partition by destination_warehouse, sku, supplier, fm_reference) num_rep, *
	from Landing.fm.fm_planned_requirements
	order by num_rep desc, destination_warehouse, sku, supplier, fm_reference

	select *
	from
		(select count(*) over (partition by destination_warehouse, sku, supplier, fm_reference) num_rep, *
		from Landing.fm.fm_planned_requirements) pr
	where pr.num_rep > 1
		-- and (product_family <> '1-DAY ACUVUE MOIST' and pack_size <> 90)
	order by destination_warehouse, product_family, pack_size, sku, supplier, fm_reference

---------------------------------------------------------------
	drop table #planned_requirements

	select w.warehouseid warehouseid_bk, si.stockitemid stockitemid_bk, s.supplier_id supplierid_bk, fm_reference,
		null warehousesourceid_bk, 
		-- CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, release_date), 112)) idCalendarReleaseDate, 
		-- CONVERT(INT, CONVERT(VARCHAR(8), CONVERT(DATE, due_date), 112)) idCalendarDueDate, 
		release_date, due_date,
		planner, overdue_status,
		pack_size packsize, quantity, unit_cost
	into #planned_requirements
	from 
			Landing.fm.fm_planned_requirements pr
		inner join
			Landing.mend.gen_wh_warehouse_v w on pr.destination_warehouse = w.name
		inner join
			(select stockitemid, sku, stockitemdescription, count(*) over (partition by sku) num_rep
			from 
				(select si.stockitemid, si.sku, si.stockitemdescription, 
					isnull(wsi.num_records, 0) num_records,
					count(*) over (partition by si.sku) num_rep, 
					rank() over (partition by si.sku order by isnull(wsi.num_records, 0), si.stockitemid) ord_rep
				from 
						Landing.mend.gen_prod_stockitem_v si
					left join
						(select stockitemid, count(*) num_records
						from Landing.mend.gen_wh_warehousestockitem_v
						where wh_stock_item_batch_f = 'Y'
						group by stockitemid) wsi on si.stockitemid = wsi.stockitemid) si
			where num_rep = ord_rep) si on pr.sku = si.sku
		inner join
			-- Landing.mend.gen_supp_supplier_v s on pr.supplier = s.supplierName
			(select s.supplier_id, s.supplierID, s.supplierName
			from
				(select supplier_id, supplierID, supplierName, 
					count(*) over (partition by supplierName) num_rep, 
					dense_rank() over (partition by supplierName order by supplier_id) ord_rep
				from Landing.mend.gen_supp_supplier_v) s
			where num_rep = ord_rep) s on pr.supplier = s.supplierName

	
	select top 1000 count(*) over (partition by warehouseid_bk, stockitemid_bk, supplierid_bk, fm_reference) num_rep, *
	from #planned_requirements
	order by num_rep desc, warehouseid_bk, stockitemid_bk, supplierid_bk, fm_reference

	select *
	from
		(select count(*) over (partition by warehouseid_bk, stockitemid_bk, supplierid_bk, fm_reference) num_rep, *
		from #planned_requirements) pr
	where pr.num_rep > 1
	order by warehouseid_bk, stockitemid_bk, supplierid_bk, fm_reference

	-------------------------------------

	select release_date, isdate(release_date),
		-- CONVERT(DATE, release_date), 
		count(*)
	from #planned_requirements
	-- where release_date = '01/01/2020'
	-- where isdate(release_date) = 1
	group by release_date
	order by release_date

	select due_date, CONVERT(DATE, due_date), count(*)
	from #planned_requirements
	group by due_date
	order by due_date


	select top 1000 release_date, count(*), sum(count(*)) over ()
	from Landing.fm.fm_planned_requirements
	group by release_date
	order by release_date

