
select * 
from dw_bis_source_map
order by channel, source_code

select channel, count(*)
from dw_bis_source_map
group by channel
order by channel

select source_code, count(*)
from dw_bis_source_map
group by source_code
order by count(*) desc