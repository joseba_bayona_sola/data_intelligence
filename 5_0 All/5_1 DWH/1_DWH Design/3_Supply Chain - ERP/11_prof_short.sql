
---------------------- NOTES ----------------------
	
	-- shortageheader
		-- Why WH Source and Dest (When DIFF = Intersite Transfer?)
		-- wholesale attribute meaning

	-- shortageheader
	select shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName
	from Landing.mend.gen_short_shortageheader_v 
	-- where warehouse_name_src <> warehouse_name_dest
	order by shortageheaderid desc

	select count(*)
	from Landing.mend.short_shortageheader_aud

	select count(*)
	from Landing.mend.gen_short_shortageheader_v

		select *, 
			count(*) over (partition by shortageheaderid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.short_shortageheader_sourcewarehouse
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by shortageheaderid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.short_shortageheader_destinationwarehouse
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by shortageheaderid) rep_father,
			count(*) over (partition by supplierid) rep_son
		from Landing.mend.short_shortageheader_supplier
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by shortageheaderid) rep_father,
			count(*) over (partition by packsizeid) rep_son
		from Landing.mend.short_shortageheader_packsize
		order by rep_father desc, rep_son desc

	select code_src, warehouse_name_src, code_dest, warehouse_name_dest, count(*)
	from Landing.mend.gen_short_shortageheader_v 
	group by code_src, warehouse_name_src, code_dest, warehouse_name_dest
	order by code_src, warehouse_name_src, code_dest, warehouse_name_dest

	select supplierID, supplierName, count(*)
	from Landing.mend.gen_short_shortageheader_v 
	group by supplierID, supplierName
	order by supplierID, supplierName

	select code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, count(*)
	from Landing.mend.gen_short_shortageheader_v 
	group by code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName
	order by code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName


---------------------- NOTES ----------------------

	-- shortage
		-- Shortage Header can be without Shortage
		-- Why a shortage is MANY to shortageHeader
		-- Why rel between shortage and Purchase OL is MANY to MANY
		-- Diff between purchaseordernumber - ordernumbers - Info of PO
		-- meaning of unitquantity - packquantity

	-- shortage
	select shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate, 
		count(*) over (partition by shortage_id) num_rep
	from Landing.mend.gen_short_shortage_v
	-- where shortage_id is null
	-- where shortage_id is not null
	-- where shortageheaderid = 98516241848741710 -- 98516241848741710 - 98516241848742712
	-- where shortage_id = 43628621391685388
	where shortage_id = 43628621391998313
	order by createdDate desc
	-- order by num_rep desc, shortage_id, shortageheaderid

	select count(*)
	from Landing.mend.short_shortage

	select count(*)
	from Landing.mend.short_shortage_aud

	select count(*)
	from Landing.mend.gen_short_shortage_v

		select *, 
			count(*) over (partition by shortageid) rep_father,
			count(*) over (partition by shortageheaderid) rep_son
		from Landing.mend.short_shortage_shortageheader_aud
		order by rep_father desc, rep_son desc, shortageheaderid, shortageid

		select *, 
			count(*) over (partition by shortageid) rep_father,
			count(*) over (partition by productid) rep_son
		from Landing.mend.short_shortage_product_aud
		order by rep_father desc, rep_son desc, shortageid, productid

		select *, 
			count(*) over (partition by shortageid) rep_father,
			count(*) over (partition by stockitemid) rep_son
		from Landing.mend.short_shortage_stockitem_aud
		order by rep_father desc, rep_son desc, shortageid, stockitemid

		select *, 
			count(*) over (partition by shortageid) rep_father,
			count(*) over (partition by warehousestockitemid) rep_son
		from Landing.mend.short_shortage_warehousestockitem_aud
		order by rep_father desc, rep_son desc, shortageid, warehousestockitemid

		select *, 
			count(*) over (partition by shortageid) rep_father,
			count(*) over (partition by purchaseorderlineid) rep_son
		from Landing.mend.short_shortage_purchaseorderline_aud
		-- order by rep_father desc, rep_son desc, shortageid, purchaseorderlineid
		order by rep_son desc, rep_father desc, purchaseorderlineid, shortageid

	-- 

	select shortageid, count(*)
	from Landing.mend.gen_short_shortage_v
	group by shortageid
	order by count(*) desc

	select status, count(distinct shortageheaderid), count(distinct shortage_id), count(*)
	from Landing.mend.gen_short_shortage_v
	group by status
	order by status

	select shortageheaderid, count(*), count(shortage_id)
	from Landing.mend.gen_short_shortage_v
	group by shortageheaderid
	order by count(*) desc


---------------------- NOTES ----------------------
	
	-- shortage OL
		-- Why do we have this table: Extra Attributes?? Rel to Order Line

		-- unitquantity, packquantity on SH == unitquantity, packquantity on OL SH (Diff on (SH with many OL SH))
		-- Orderline Shortage Type: Standard vs Child (SH with many OL SH)

	-- shortage OL
	select orderlineshortageid, shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, orderlineid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate,
		
		orderlineshortage_id,
		orderlineshortagetype,
		unitquantity_ol, packquantity_ol,
		shortageprogress, adviseddate, 
		createdDate_ol, 
		count(*) over (partition by shortage_id) orderlineshortageid,
		count(*) over (partition by shortage_id) num_rep_sh
	from Landing.mend.gen_short_orderlineshortage_v
	-- where orderlineshortageid is null
	-- where orderlineshortageid is not null
	where shortage_id = 43628621391998313
	order by createdDate desc
	-- order by num_rep desc, shortage_id, shortageheaderid

	select count(*)
	from Landing.mend.short_orderlineshortage

	select count(*)
	from Landing.mend.short_orderlineshortage_aud

	select count(*)
	from Landing.mend.gen_short_orderlineshortage_v

		select *, 
			count(*) over (partition by orderlineshortageid) rep_father,
			count(*) over (partition by shortageid) rep_son
		from Landing.mend.short_orderlineshortage_shortage_aud
		order by rep_father desc, rep_son desc, shortageid, orderlineshortageid

		select *, 
			count(*) over (partition by orderlineshortageid) rep_father,
			count(*) over (partition by orderlineid) rep_son
		from Landing.mend.short_orderlineshortage_orderline_aud
		order by rep_father desc, rep_son desc, orderlineid, orderlineshortageid
			
	select orderlineshortagetype, count(*)
	from Landing.mend.gen_short_orderlineshortage_v
	group by orderlineshortagetype
	order by orderlineshortagetype


------------------------------------------------------ 

	select sh.*, 
		pfp.magentoProductID_int, pfp.name, pfp.size
	from 
			Landing.mend.gen_short_shortageheader_v sh
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfp on sh.packsizeid = pfp.packsizeid
	order by magentoProductID_int, code_src, code_dest, supplierID 

	select count(*) over () num_tot,
		count(*) over (partition by s.shortage_id) num_rep,
		s.*, 
		pfp.magentoProductID_int, pfp.name, pfp.size, 
		si.SKU, si.description, 
		pol.po_lineID, pol.quantity, pol.quantityReceived, pol.lineprice
	from 
			Landing.mend.gen_short_shortage_v s
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfp on s.packsizeid = pfp.packsizeid
		left join
			Landing.mend.gen_prod_stockitem_v si on s.stockitemid = si.stockitemid
		left join
			Landing.mend.gen_purc_purchaseorderline_v pol on s.purchaseorderlineid = pol.purchaseorderlineid
	-- where pfp.magentoProductID_int is not null
		-- and s.shortageheaderid = 98516241848762661
		-- and pfp.magentoProductID_int = 2269
		-- and s.shortage_id is not null order by num_rep desc, s.shortageheaderid 
	order by magentoProductID_int, code_src, code_dest, supplierID, s.shortageheaderid 
