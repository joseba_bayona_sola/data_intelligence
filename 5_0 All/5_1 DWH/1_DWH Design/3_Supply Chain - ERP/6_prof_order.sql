
select top 1000 *
from Landing.mend.order_order_aud

select orderStatus, count(*)
from Landing.mend.order_order_aud
group by orderStatus
order by orderStatus

select _type, count(*)
from Landing.mend.order_order_aud
group by _type
order by _type

select shipmentStatus, count(*)
from Landing.mend.order_order_aud
group by shipmentStatus
order by shipmentStatus

select allocationStatus, count(*)
from Landing.mend.order_order_aud
group by allocationStatus
order by allocationStatus

select allocationStrategy, count(*)
from Landing.mend.order_order_aud
group by allocationStrategy
order by allocationStrategy

select labelRenderer, count(*)
from Landing.mend.order_order_aud
group by labelRenderer
order by labelRenderer

---------------------------------------

select top 1000 *
from Landing.mend.all_orderlinestockallocationtransaction_aud

select top 1000 allocationType, count(*)
from Landing.mend.all_orderlinestockallocationtransaction_aud
group by allocationType
order by allocationType

select top 1000 recordType, count(*)
from Landing.mend.all_orderlinestockallocationtransaction_aud
group by recordType
order by recordType
