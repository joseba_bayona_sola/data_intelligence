
select top 1000 *
from Landing.mend.order_orderlineabstract

select top 1000 orderlinetype, count(*)
from Landing.mend.order_orderlineabstract_aud
group by orderlinetype
order by orderlinetype

select top 1000 subMetaObjectName, count(*)
from Landing.mend.order_orderlineabstract_aud
group by subMetaObjectName
order by subMetaObjectName

select top 1000 subMetaObjectName, orderlinetype, count(*)
from Landing.mend.order_orderlineabstract_aud
group by subMetaObjectName, orderlinetype
order by subMetaObjectName, orderlinetype

	select top 1000 magentoItemID, count(*)
	from Landing.mend.order_orderlineabstract_aud
	where subMetaObjectName = 'OrderProcessing.OrderLine' 
	group by magentoItemID
	having count(*) > 1
	order by count(*) desc, magentoItemID

	select top 1000 magentoItemID, count(*)
	from Landing.mend.order_orderlineabstract_aud
	where subMetaObjectName = 'OrderProcessing.OrderLineMagento' 
	group by magentoItemID
	having count(*) > 1
	order by count(*) desc, magentoItemID

-----------------------------------------

select top 1000 *
from Landing.mend.order_orderlineabstract_aud
where orderlineType = 'SubProduct'

	select top 1000 sku, count(*)
	from Landing.mend.order_orderlineabstract_aud
	where orderlineType = 'SubProduct'
	group by sku
	order by sku

-----------------------------------------

select top 1000 *
from Landing.mend.order_orderlineabstract_aud
where orderlineType = 'Grouped'

	select top 1000 sku, count(*)
	from Landing.mend.order_orderlineabstract_aud
	where orderlineType = 'Grouped'
	group by sku
	order by sku

----------------------------------------------------------------

select *
from Landing.mend.gen_order_orderline_v
where magentoOrderID_int = 7281234


select top 1000 *
from Landing.mend.order_orderlineabstract
where id = 54324670575540916


-------------------------

select *
from Landing.mend.gen_order_orderlinemagento_v
where magentoOrderID_int = 7281234


select top 1000 *
from Landing.mend.order_orderlineabstract
where id in (1970324918538396, 1970324918538395)