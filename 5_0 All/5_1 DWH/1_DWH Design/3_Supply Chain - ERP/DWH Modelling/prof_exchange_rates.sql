
select currencycode_from, currencycode_to, effectivedate, nexteffectivedate, exchange_rate
from Landing.mend.gen_comp_currency_exchange_v 

	select effectivedate, nexteffectivedate, datediff(dd, effectivedate, nexteffectivedate) diff, count(*)
	from Landing.mend.gen_comp_currency_exchange_v 
	group by effectivedate, nexteffectivedate
	-- order by count(*) desc, effectivedate, nexteffectivedate
	order by diff desc, effectivedate, nexteffectivedate

	select currencycode_from, currencycode_to, count(*)
	from Landing.mend.gen_comp_currency_exchange_v 
	group by currencycode_from, currencycode_to
	order by currencycode_from, currencycode_to



select *
from Landing.mend.gen_comp_currency_v
where currencycode = 'EUR'
order by currencycode, effectivedate desc

	select currencycode, count(*)
	from Landing.mend.gen_comp_currency_v
	group by currencycode
	order by currencycode

	select effectivedate, nexteffectivedate, count(*)
	from Landing.mend.gen_comp_currency_v
	group by effectivedate, nexteffectivedate
	order by effectivedate, nexteffectivedate

	select effectivedate_d, nexteffectivedate_d, count(*)
	from Landing.mend.gen_comp_currency_v
	group by effectivedate_d, nexteffectivedate_d
	order by effectivedate_d, nexteffectivedate_d

	select convert(date, effectivedate) effectivedate, count(*)
	from Landing.mend.gen_comp_currency_v
	group by effectivedate
	order by count(*) desc, effectivedate


select *
from Landing.aux.sales_exchange_rate
order by exchange_rate_day desc

-----------------

select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, historicalrate, convert(decimal(12, 8), 1 / historicalrate), 
	10.25000000 * historicalrate, 
	10.25000000 * convert(decimal(12, 8), 1 / historicalrate)
from Landing.mend.gen_comp_currency_v
-- where currencycode = 'EUR'
-- where '2017-06-01 10:55:39' between effectivedate and nexteffectivedate
where '2017-06-02 11:52:26' between effectivedate and nexteffectivedate
order by currencycode, effectivedate desc

select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
from Landing.mend.gen_comp_currency_v
where '2018-01-01' between effectivedate and nexteffectivedate
union
select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
from Landing.mend.gen_comp_currency_v
where '2018-01-01' between effectivedate and nexteffectivedate
order by currencycode, effectivedate desc

------------------------ SOLUTION NEW --------------------------------

select currencycode_from, currencycode_to, effectivedate_d, nexteffectivedate_d, exchange_rate
into #erp_exchange_rates
from
	(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate_d, nexteffectivedate_d, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
	from Landing.mend.gen_comp_currency_v
	union
	select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, historicalrate exchange_rate
	from Landing.mend.gen_comp_currency_v
	union
	select c1.currencycode_from, c2.currencycode_to, c1.effectivedate_d, c1.nexteffectivedate_d,
		c1.exchange_rate * c2.exchange_rate exchange_rate
	from
			(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate_d, nexteffectivedate_d, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode not in ('GBP', 'EUR')) c1
		left join
			(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, historicalrate exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode = 'EUR') c2 on c1.currencycode_to = c2.currencycode_from and 
				c1.effectivedate_d = c2.effectivedate_d and c1.nexteffectivedate_d = c2.nexteffectivedate_d) c

select *
from #erp_exchange_rates
where currencycode_to = 'EUR' and currencycode_from <> 'GBP'
order by currencycode_from, currencycode_to, effectivedate_d desc

select currencycode_from, currencycode_to, count(*)
from #erp_exchange_rates
group by currencycode_from, currencycode_to
order by currencycode_from, currencycode_to

select t.*, er.*
from 
		(select GETUTCDATE()-1 order_date, -- convert(datetime, '2018-10-13 00:00:00') order_date, 
			40 sales_cost, 'USD' currency_code) t
	inner join
		#erp_exchange_rates er on -- t.order_date between er.effectivedate_d and er.nexteffectivedate_d and 
			t.order_date >= er.effectivedate_d and t.order_date < er.nexteffectivedate_d and
			t.currency_code = er.currencycode_from and er.currencycode_to = 'EUR'

------------------------ SOLUTION OLD --------------------------------

select *, 150 * exchange_rate, 150 * exchange_rate * 1.12711000, 
	exchange_rate * 1.12711000, 
	150 * (exchange_rate * 1.12711000)
from
	(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
	from Landing.mend.gen_comp_currency_v
	where '2018-01-01' between effectivedate and nexteffectivedate
	union
	select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
	from Landing.mend.gen_comp_currency_v
	where '2018-01-01' between effectivedate and nexteffectivedate) c
order by currencycode_from, effectivedate desc

select c1.currencycode_from, c2.currencycode_to, c1.effectivedate, c1.nexteffectivedate,
	-- c1.exchange_rate, c2.exchange_rate, 
	c1.exchange_rate * c2.exchange_rate exchange_rate
from
		(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
		from Landing.mend.gen_comp_currency_v
		where '2018-01-01' between effectivedate and nexteffectivedate and currencycode not in ('GBP', 'EUR')) c1
	inner join
		(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
		from Landing.mend.gen_comp_currency_v
		where '2018-01-01' between effectivedate and nexteffectivedate and currencycode = 'EUR') c2 on c1.currencycode_to = c2.currencycode_from and 
			c1.effectivedate = c2.effectivedate and c1.nexteffectivedate = c2.nexteffectivedate



select currencycode_from, currencycode_to, effectivedate, nexteffectivedate, exchange_rate
into #erp_exchange_rates
from
	(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
	from Landing.mend.gen_comp_currency_v
	union
	select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
	from Landing.mend.gen_comp_currency_v
	union
	select c1.currencycode_from, c2.currencycode_to, c1.effectivedate, c1.nexteffectivedate,
		c1.exchange_rate * c2.exchange_rate exchange_rate
	from
			(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode not in ('GBP', 'EUR')) c1
		left join
			(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode = 'EUR') c2 on c1.currencycode_to = c2.currencycode_from and 
				c1.effectivedate = c2.effectivedate and c1.nexteffectivedate = c2.nexteffectivedate) c

select *
from #erp_exchange_rates
order by currencycode_from, currencycode_to, effectivedate desc

select currencycode_from, currencycode_to, count(*)
from #erp_exchange_rates
group by currencycode_from, currencycode_to
order by currencycode_from, currencycode_to

	select distinct c1.currencycode_from, c2.currencycode_to, c1.effectivedate, c1.nexteffectivedate, 
		dateadd(ms, -datepart(ms, c1.effectivedate), c1.effectivedate) effectivedate, dateadd(ms, -datepart(ms, c1.nexteffectivedate), c1.nexteffectivedate) nexteffectivedate,
		c1.exchange_rate * c2.exchange_rate exchange_rate
	from
			(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate, nexteffectivedate, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode not in ('GBP', 'EUR')) c1
		left join
			(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate, nexteffectivedate, historicalrate exchange_rate
			from Landing.mend.gen_comp_currency_v
			where currencycode = 'EUR') c2 on c1.currencycode_to = c2.currencycode_from and 
				-- c1.effectivedate = c2.effectivedate and c1.nexteffectivedate = c2.nexteffectivedate
				dateadd(ms, -datepart(ms, c1.effectivedate), c1.effectivedate) = dateadd(ms, -datepart(ms, c2.effectivedate), c2.effectivedate) and 
				dateadd(ms, -datepart(ms, c1.nexteffectivedate), c1.nexteffectivedate) = dateadd(ms, -datepart(ms, c2.nexteffectivedate), c2.nexteffectivedate)
	order by currencycode_from, currencycode_to, c1.effectivedate desc


-----------------

select curr.currencycode, sr.effectivedate, sr.value
from 
		Landing.mend.comp_spotrate sr
	inner join 
		Landing.mend.comp_historicalspotrate_currency sr_curr on sr.id = sr_curr.spotrateid
	inner join
		Landing.mend.comp_currency curr on sr_curr.currencyid = curr.id
where curr.currencycode = 'EUR'
order by sr.effectivedate, curr.currencycode


	cm.currencycode AS currency,
	cur.spotrate AS day_rate,
	eur.spotrate AS eurate,

FROM 
		warehouseshipments$receipt rec
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'