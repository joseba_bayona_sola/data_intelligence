
-- Stock 

select top 1000 *
from Warehouse.stock.dim_stock_method_type

	select count(*) over (partition by product_id_magento, product_family_name) num_rep, 
		product_id_magento, product_family_name, stock_method_type_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by product_id_magento, product_family_name, stock_method_type_name
	order by num_rep desc, product_id_magento, product_family_name, stock_method_type_name

	select count(*) over (partition by warehouse_name, product_id_magento, product_family_name) num_rep, 
		warehouse_name, product_id_magento, product_family_name, stock_method_type_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by warehouse_name, product_id_magento, product_family_name, stock_method_type_name
	order by num_rep desc, warehouse_name, product_id_magento, product_family_name, stock_method_type_name



select top 1000 abc, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by abc
order by abc

select top 1000 stockable, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by stockable
order by stockable

select top 1000 stocked, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by stocked
order by stocked

select top 1000 final, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by final
order by final

select top 1000 abc, stockable, stocked, final, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by abc, stockable, stocked, final
order by abc, stockable, stocked, final

select top 1000 stockable, stocked, final, count(*)
from Warehouse.fm.dim_wh_stock_item_static_data_v
group by stockable, stocked, final
order by stockable, stocked, final

	select top 1000 *
	from Warehouse.fm.dim_wh_stock_item_static_data_v

	select count(*) over (partition by product_id_magento, product_family_name) num_rep, 
		product_id_magento, product_family_name, abc, count(*)
	from Warehouse.fm.dim_wh_stock_item_static_data_v
	group by product_id_magento, product_family_name, abc
	order by num_rep desc, product_id_magento, product_family_name, abc

	select count(*) over (partition by product_id_magento, product_family_name) num_rep, 
		product_id_magento, product_family_name, stockable, count(*)
	from Warehouse.fm.dim_wh_stock_item_static_data_v
	group by product_id_magento, product_family_name, stockable
	order by num_rep desc, product_id_magento, product_family_name, stockable

	select count(*) over (partition by product_id_magento, product_family_name) num_rep, 
		product_id_magento, product_family_name, stocked, count(*)
	from Warehouse.fm.dim_wh_stock_item_static_data_v
	group by product_id_magento, product_family_name, stocked
	order by num_rep desc, product_id_magento, product_family_name, stocked

	select count(*) over (partition by product_id_magento, product_family_name) num_rep, 
		product_id_magento, product_family_name, final, count(*)
	from Warehouse.fm.dim_wh_stock_item_static_data_v
	group by product_id_magento, product_family_name, final
	order by num_rep desc, product_id_magento, product_family_name, final


-- Stock Adjustments


select top 1000 *
from Warehouse.stock.dim_stock_adjustment_type

select top 1000 *
from Warehouse.stock.dim_stock_movement_type

select 'Stock Sync' movement_reason -- operator = 'SYSTEM'
union
select 'Manual Stock Movement'	-- batchstockmovementtype in ('Manual_Negative', 'Manual_Positive')
union
select 'Damages' -- reasonid = '01'
union 
select 'Stock Check' -- reasonid = '02'
union 
select 'Sales Sample' -- reasonid = '03'
union
select 'Warehouse Adjust Only' -- reasonid = '99'
union
select 'Audit' -- reasonid = 'AU'
union 
select 'Unknown'
	

	select top 1000 stock_adjustment_type_name, stock_movement_type_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by stock_adjustment_type_name, stock_movement_type_name
	order by stock_adjustment_type_name, stock_movement_type_name

	select top 1000 stock_adjustment_type_name, stock_movement_type_name, movement_reason, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by stock_adjustment_type_name, stock_movement_type_name, movement_reason
	order by stock_adjustment_type_name, stock_movement_type_name, movement_reason

-----------------------------------------------------------------
-----------------------------------------------------------------

select *
from Warehouse.gen.dim_supplier_type

select top 1000 *
from Warehouse.po.dim_po_source

select top 1000 *
from Warehouse.po.dim_po_type

select top 1000 *
from Warehouse.po.dim_po_status

	select top 1000 *
	from Warehouse.po.dim_purchase_order_v
	order by purchaseorderid_bk desc

	select top 1000 supplier_type_name, count(*)
	from Warehouse.po.dim_purchase_order_v
	group by supplier_type_name
	order by supplier_type_name

	select top 1000 po_source_name, count(*)
	from Warehouse.po.dim_purchase_order_v
	group by po_source_name
	order by po_source_name

	select top 1000 po_type_name, count(*)
	from Warehouse.po.dim_purchase_order_v
	group by po_type_name
	order by po_type_name

	select top 1000 po_status_name, count(*)
	from Warehouse.po.dim_purchase_order_v
	group by po_status_name
	order by po_status_name

	select top 1000 supplier_type_name, po_source_name, po_type_name, po_status_name, count(*)
	from Warehouse.po.dim_purchase_order_v
	group by supplier_type_name, po_source_name, po_type_name, po_status_name
	order by supplier_type_name, po_source_name, po_type_name, po_status_name

select top 1000 *
from Warehouse.po.dim_pol_status

select top 1000 *
from Warehouse.po.dim_pol_problems

	select top 1000 *
	from Warehouse.po.fact_purchase_order_line_v
	order by purchaseorderlineid_bk desc

	select top 1000 pol_status_name, count(*)
	from Warehouse.po.fact_purchase_order_line_v
	group by pol_status_name
	order by pol_status_name

	select top 1000 pol_problems_name, count(*)
	from Warehouse.po.fact_purchase_order_line_v
	group by pol_problems_name
	order by pol_problems_name

-----------------------------------------------------------------
-----------------------------------------------------------------

select top 1000 *
from Warehouse.rec.dim_wh_shipment_type

select top 1000 *
from Warehouse.rec.dim_receipt_status

	select top 1000 *
	from Warehouse.rec.dim_receipt_v
	order by receiptid_bk desc

	select top 1000 wh_shipment_type_name, count(*)
	from Warehouse.rec.dim_receipt_v
	group by wh_shipment_type_name
	order by wh_shipment_type_name

	select top 1000 receipt_status_name, count(*)
	from Warehouse.rec.dim_receipt_v
	group by receipt_status_name
	order by receipt_status_name

select top 1000 *
from Warehouse.rec.dim_receipt_line_status

select top 1000 *
from Warehouse.rec.dim_receipt_line_sync_status

	select top 1000 *
	from Warehouse.rec.fact_receipt_line_v
	order by receiptlineid_bk desc

	select top 1000 receipt_line_status_name, count(*)
	from Warehouse.rec.fact_receipt_line_v
	group by receipt_line_status_name
	order by receipt_line_status_name

	select top 1000 receipt_line_sync_status_name, count(*)
	from Warehouse.rec.fact_receipt_line_v
	group by receipt_line_sync_status_name
	order by receipt_line_sync_status_name


