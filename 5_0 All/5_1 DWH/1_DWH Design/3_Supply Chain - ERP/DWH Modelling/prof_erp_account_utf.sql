﻿
select id, fullname
from administration$account

select id, fullname, cast(fullname as varchar(200)) fullname2, regexp_matches(fullname, '\u0041')
from administration$account
order by fullname

select id, fullname, cast(fullname as varchar(200)) fullname2, regexp_matches(fullname, '\u200B')
from administration$account
order by fullname

select convert('PostgreSQL' using iso_8859_1_to_utf8)

select convert('text_in_utf8', 'UTF8', 'LATIN1'), convert_to(fullname, 'WIN1250'), fullname
from administration$account


-- https://utf8-chartable.de/unicode-utf8-table.pl?start=8192&number=128&utf8=0x
-- https://www.utf8-chartable.de/unicode-utf8-table.pl