

select isib.intransitstockitembatchid, isib.transferheaderid, isib.purchaseorderid_s, isib.purchaseorderid_t, isib.warehouseid, isib.stockitemid, isib.receiptid, isibt_rl.receiptlineid, isibt.id intransitbatchtransactionid,
	isib.transfernum, isib.allocatedstrategy, isib.createddate, isib.ordernumber_s, isib.ordernumber_t, 
	isib.issuedquantity, isib.remainingquantity, isib.lostquantity, isib.registeredquantity,
	isib.stockregistereddate, isib.createddate_isib, 
	isibt.transactionid, isibt.transactiontype, isibt.qtyactioned, isibt.transactiondate
from 
		Landing.mend.gen_inter_intransitstockitembatch_th_v isib
	inner join
		Landing.mend.inter_intransitbatchtransacti_intransitstockitembatc_aud isibt_isib on isib.intransitstockitembatchid = isibt_isib.intransitstockitembatchid
	inner join
		Landing.mend.inter_intransitbatchtransaction_aud isibt on isibt_isib.Intransitbatchtransactionid = isibt.id
	inner join
		Landing.mend.inter_intransitbatchtransaction_receiptline_aud isibt_rl on isibt.id = isibt_rl.Intransitbatchtransactionid
-- where isib.intransitstockitembatchid = 96827391988810691 
-- where isibt.transactiontype = 'MISSING'
-- where isib.remainingquantity <> 0
-- where isibt_rl.Receiptlineid = 74590868832287171
order by isib.createddate

	select count(*), min(createddate)
	from 
			Landing.mend.gen_inter_intransitstockitembatch_th_v isib
		inner join
			Landing.mend.inter_intransitbatchtransacti_intransitstockitembatc_aud isibt_isib on isib.intransitstockitembatchid = isibt_isib.intransitstockitembatchid



select id, transactionid, transactiontype, qtyactioned, transactiondate, createddate,
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.inter_intransitbatchtransaction_aud

	select count(*), min(transactiondate), max(transactiondate)
	from Landing.mend.inter_intransitbatchtransaction_aud

	select transactiontype, count(*), min(transactiondate), max(transactiondate)
	from Landing.mend.inter_intransitbatchtransaction_aud
	group by transactiontype
	order by transactiontype

------------

select Intransitbatchtransactionid, intransitstockitembatchid, 
	count(*) over (partition by Intransitbatchtransactionid) num_rep1, 
	count(*) over (partition by intransitstockitembatchid) num_rep2, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.inter_intransitbatchtransacti_intransitstockitembatc_aud
order by num_rep1 desc, num_rep2 desc, intransitstockitembatchid

select Intransitbatchtransactionid,Receiptlineid, 
	count(*) over (partition by Intransitbatchtransactionid) num_rep1, 
	count(*) over (partition by Receiptlineid) num_rep2, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.mend.inter_intransitbatchtransaction_receiptline_aud
order by num_rep1 desc, num_rep2 desc, Receiptlineid


--------------------------------------------
--------------------------------------------

		select *
		from
			(select isib.intransitstockitembatchid_bk, 
				isib.intransit_batch_created_date, 
				isib.qty_sent qty_received, isib.qty_remaining, isib.qty_registered, isib.qty_missing,
				isnull(isibt.qty_registered, 0) qty_registered_tr,
				isnull(isibt.qty_missing, 0) qty_missing_tr,
				isib.delete_f, 'F' update_type
			from
					(select intransitstockitembatchid 
					from Landing.mend.gen_inter_intransitstockitembatchtransaction_th_v
					union
					select intransitstockitembatchid 
					from Landing.mend.gen_inter_intransitstockitembatch_th_v
					where createddate_isib > '2019-01-01') isib_f
				inner join
					Landing.aux.stock_dim_intransit_stock_item_batch isib on isib_f.intransitstockitembatchid = isib.intransitstockitembatchid_bk 
				left join
					(select intransitstockitembatchid, sum(qty_registered) qty_registered, sum(qty_missing) qty_missing
					from
						(select intransitstockitembatchid, transactiontype, 
							case when transactiontype = 'REGISTER' then qtyactioned else 0 end qty_registered,
							case when transactiontype = 'MISSING' then qtyactioned else 0 end qty_missing
						from Landing.mend.gen_inter_intransitstockitembatchtransaction_th_v) t
					group by intransitstockitembatchid) isibt on isib_f.intransitstockitembatchid = isibt.intransitstockitembatchid) t
		where qty_registered <> qty_registered_tr -- 14
		-- where qty_missing <> qty_missing_tr -- 4
		-- where qty_remaining <> qty_received - qty_registered_tr - qty_missing_tr -- 239

		select top 1000 *
		from Landing.aux.stock_dim_intransit_stock_item_batch

					select intransitstockitembatchid, sum(qtyactioned) qty_transaction
					from Landing.mend.gen_inter_intransitstockitembatchtransaction_th_v
					group by intransitstockitembatchid

					select intransitstockitembatchid, sum(qty_registered) qty_registered, sum(qty_missing) qty_missing
					from
						(select intransitstockitembatchid, transactiontype, 
							case when transactiontype = 'REGISTER' then qtyactioned else 0 end qty_registered,
							case when transactiontype = 'MISSING' then qtyactioned else 0 end qty_missing
						from Landing.mend.gen_inter_intransitstockitembatchtransaction_th_v) t
					group by intransitstockitembatchid
