
-- reason: varchar(50)
-- comment: varchar(200)
-- operator: varchar(20)

select top 10000 *
from Landing.mend.gen_wh_batchstockmovement_v
-- where operator = 'SYSTEM'
-- where comment is not null
-- where comment is not null and operator is not null
-- where comment like '%Patch%62%'

-- where batchstockmovement_createdate is null
where stockmovement_createdate is null
-- where isnull(batchstockmovement_createdate, stockmovement_createdate) is null
order by batchstockmovementid desc

select min(batchstockmovement_createdate), max(batchstockmovement_createdate)
from Landing.mend.gen_wh_batchstockmovement_v

select min(stockmovement_createdate), max(stockmovement_createdate)
from Landing.mend.gen_wh_batchstockmovement_v
where batchstockmovement_createdate is null

select top 1000 operator, _class, reasonid, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
group by operator, _class, reasonid
order by operator, _class, reasonid

select top 1000 operator, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
group by operator 
order by count(*) desc, operator

select top 1000 _class, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
group by _class
order by _class

select top 1000 reasonid, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
group by reasonid
order by reasonid

select top 1000 _class, reasonid, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
where reasonid is null
group by _class, reasonid
order by _class, reasonid

select top 1000 _class, reasonid, operator, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
where reasonid is null
group by _class, reasonid, operator
order by _class, reasonid, operator

select top 1000 comment, count(*)
from Landing.mend.gen_wh_batchstockmovement_v
group by comment
order by count(*) desc, comment
