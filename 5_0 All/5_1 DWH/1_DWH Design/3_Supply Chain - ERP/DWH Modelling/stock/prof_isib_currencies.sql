

select top 10000 *
from Warehouse.stock.dim_intransit_stock_item_batch_v
order by batch_created_date desc


select top 1000 warehouse_name_from, warehouse_name_to, currency_code, count(*)
from Warehouse.stock.dim_intransit_stock_item_batch_v
group by warehouse_name_from, warehouse_name_to, currency_code
order by warehouse_name_from, warehouse_name_to, currency_code

select warehouse_name_from, warehouse_name_to, currency_code, local_to_global_rate, count(*)
from Warehouse.stock.dim_intransit_stock_item_batch_v
group by warehouse_name_from, warehouse_name_to, currency_code, local_to_global_rate
order by warehouse_name_from, warehouse_name_to, currency_code, local_to_global_rate


			select top 10000 intransitstockitembatchid intransitstockitembatchid_bk, 
				transferheaderid transferheaderid_bk, stockitemid stockitemid_bk, 
				issuedquantity qty_sent, -- remainingquantity qty_remaining, registeredquantity qty_registered, lostquantity qty_missing, 
	
				createddate_isib intransit_batch_created_date, 
				-- createddate_isib batch_created_date, arriveddate batch_arrived_date, confirmeddate batch_confirmed_date, stockregistereddate batch_stock_register_date, 

				productunitcost local_product_unit_cost_orig, 
				productunitcost * 1 / convert(decimal(12, 4), er.exchange_rate) local_product_unit_cost, 
				productunitcost * 1 / convert(decimal(12, 4), er2.exchange_rate) local_product_unit_cost_fix, 
				productunitcost * convert(decimal(12, 4), er2.exchange_rate) local_product_unit_cost_fix2, -- GOOD ONE
				-- carriageUnitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_carriage_unit_cost, totalunitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost, 
				-- 0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost_interco, 

				convert(decimal(12, 4), er.exchange_rate) local_to_global_rate, 
				convert(decimal(12, 4), er2.exchange_rate) local_to_global_rate_fix, 
				th.currency currency_receiving, w.currencycode currency_sending, 'GBP' currency_global,

				'N' wh_stock_item_batch_f, 'N' delete_f

			from 
					Landing.mend.gen_inter_intransitstockitembatch_th_v th
				inner join
					Landing.mend.gen_wh_warehouse_v w on th.warehouseid = w.warehouseid
				left join
					Landing.mend.gen_comp_currency_exchange_v er on th.stockregistereddate > er.effectivedate and th.stockregistereddate <= er.nexteffectivedate and
						w.currencycode = er.currencycode_from and er.currencycode_to = 'GBP'
				left join
					Landing.mend.gen_comp_currency_exchange_v er2 on th.stockregistereddate > er2.effectivedate and th.stockregistereddate <= er2.nexteffectivedate and
						er2.currencycode_from = th.currency and er2.currencycode_to = w.currencycode
			order by createddate_isib desc

select top 1000 *
from 
