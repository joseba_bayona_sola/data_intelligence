
----------------------------------------------

-- customershipments$pickline // customershipments$picklist_customershipment // customershipments$PickLine_OrderLineStockAllocationTransaction

-- intersitetransfer$intransitbatchheader_customershipment + shipment_no between customershipment, customershipmentinvoice
	--> Add idWholesaleOrderShipment_sk_fk in dim_intransit_stock_item_batch

----------------------------------------------


-- Intersite Transfer + Intransit Batches
select top 1000 count(*) over (), *
from Warehouse.po.dim_intersite_transfer_v
where order_no_erp = 'INT00000483'
order by idIntersiteTransfer_sk desc

select top 1000 count(*) over (), *
from Warehouse.stock.dim_intransit_stock_item_batch_v
where order_no_erp = 'INT00000483' and sku = '01373B3D2CE0000000000031'
-- where order_no_erp = 'INT00004475' and sku = '023610000000000000000011'

	select count(*) -- used
	from Landing.mend.gen_inter_intransitstockitembatch_th_v

	select count(*) -- not used
	from Landing.mend.gen_inter_intransitstockitembatch_ibh_v

	select count(*)
	from Warehouse.stock.dim_intransit_stock_item_batch_v



	select top 1000 order_no_erp, sku, count(*) 
	from Warehouse.stock.dim_intransit_stock_item_batch_v
	-- where order_no_erp = 'INT00000483'
	group by order_no_erp, sku
	order by count(*) desc

-- Wholesales Invoice
select customershipmentinvoiceid_bk, orderid_bk, invoice_no, order_no_erp, order_type_erp_f, invoice_date, shipment_no
from Warehouse.alloc.dim_wholesale_order_shipment_v
where order_no_erp = 'INT00000483'
order by shipment_no

select top 1000 *
from Warehouse.alloc.dim_wholesale_order_invoice_line_v
where order_no_erp = 'INT00000483' and sku = '01373B3D2CE0000000000031'
-- where order_no_erp = 'INT00004475' and sku = '023610000000000000000011'

	select orderid_bk, order_no_erp, count(*)
	from Warehouse.alloc.dim_wholesale_order_shipment_v
	where order_type_erp_f = 'I'
	group by orderid_bk, order_no_erp
	order by count(*) desc

	select top 1000 orderid_bk, order_no_erp, sku, count(*)
	from Warehouse.alloc.dim_wholesale_order_invoice_line_v
	where order_type_erp_f = 'I'
		-- and order_no_erp = 'INT00000483'
	group by orderid_bk, order_no_erp, sku
	order by count(*) desc, order_no_erp, sku

	select shipment_no, count(*)
	from Warehouse.alloc.dim_wholesale_order_shipment_v
	-- where order_type_erp_f = 'I'
	group by shipment_no
	order by count(*) desc

------------------------------------------------------

select top 1000 *
from Landing.mend.gen_ship_customershipment_v
where orderid = 48695170987114185

select top 1000 *
from Landing.mend.gen_ship_customershipment_olm_v
where orderid = 48695170987114185

------------------------------------------------------

select *
from Landing.mend.inter_intransitbatchheader_customershipment

select intransitbatchheaderid, count(*)
from Landing.mend.inter_intransitbatchheader_customershipment
group by intransitbatchheaderid
order by count(*) desc

select customershipmentid, count(*)
from Landing.mend.inter_intransitbatchheader_customershipment
group by customershipmentid
order by count(*) desc

------------------------------------------------------

	select top 1000 
		 id intransitstockitembatchid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, th.warehouseid, isibsi.stockitemid, isibwsh.receiptid, ibhcs.customershipmentid,
		-- ibh.intransitbatchheaderid, ibh.customershipmentid, 
		th.transfernum, th.allocatedstrategy, th.totalremaining, th.createddate, th.ordernumber_s, th.ordernumber_t, 
		-- ibh.createddate_ibh, ibh.magentoOrderID_int, ibh.incrementID,
		si.magentoProductID_int, si.magentoProductID,
		si.magentoSKU, si.name, si.SKU_product, si.description, si.packSize, si.SKU, si.stockitemdescription,
		isib.issuedquantity, isib.remainingquantity, isib.lostquantity, isib.registeredquantity,
		isib.used,
		isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
		isib.currency,
		isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
		isib.createddate createddate_isib
	-- select count(*)
	from
			Landing.mend.gen_inter_transferheader_v th 
		inner join
			Landing.mend.inter_intransitstockitembatch_transferheader isibth on th.transferheaderid = isibth.transferheaderid
		inner join
			Landing.mend.inter_intransitstockitembatch isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem isibsi on isib.id = isibsi.intransitstockitembatchid
		left join	
			Landing.mend.gen_prod_stockitem_v si on isibsi.stockitemid = si.stockitemid 
		left join
			Landing.mend.inter_intransitstockitembatch_receipt isibwsh on isib.id = isibwsh.intransitstockitembatchid
		
		inner join
			Landing.mend.inter_intransitstockitembatch_intransitbatchheader isibibh on isib.id = isibibh.intransitstockitembatchid
		inner join
			Landing.mend.inter_intransitbatchheader_customershipment ibhcs on isibibh.intransitbatchheaderid = ibhcs.intransitbatchheaderid
	order by intransitstockitembatchid desc

	drop table #t

	select id intransitstockitembatchid, ibhcs.customershipmentid, th.transfernum
	into #t	
	from
			Landing.mend.gen_inter_transferheader_v th 
		inner join
			Landing.mend.inter_intransitstockitembatch_transferheader isibth on th.transferheaderid = isibth.transferheaderid
		inner join
			Landing.mend.inter_intransitstockitembatch isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem isibsi on isib.id = isibsi.intransitstockitembatchid
		left join	
			Landing.mend.gen_prod_stockitem_v si on isibsi.stockitemid = si.stockitemid 
		left join
			Landing.mend.inter_intransitstockitembatch_receipt isibwsh on isib.id = isibwsh.intransitstockitembatchid
		
		left join
			Landing.mend.inter_intransitstockitembatch_intransitbatchheader isibibh on isib.id = isibibh.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitbatchheader_customershipment ibhcs on isibibh.intransitbatchheaderid = ibhcs.intransitbatchheaderid

	select t.*, cs.shipmentNumber, whos.customershipmentinvoiceid_bk
	from 
			#t t
		left join
			Landing.mend.gen_ship_customershipment_v cs on t.customershipmentid = cs.customershipmentid
		left join
			Warehouse.alloc.dim_wholesale_order_shipment_v whos on cs.shipmentNumber = whos.shipment_no
	where cs.shipmentNumber is null
	-- where whos.customershipmentinvoiceid_bk is null

	select top 1000 *
	from Landing.mend.gen_ship_customershipment_v
	where customershipmentid = 23080948097188707

	select top 1000 *
	from Warehouse.alloc.dim_wholesale_order_shipment_v
	where shipment_no = 'INT00004216-1'
