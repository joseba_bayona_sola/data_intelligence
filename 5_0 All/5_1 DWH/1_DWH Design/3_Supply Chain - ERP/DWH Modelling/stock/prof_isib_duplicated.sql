
select top 1000 count(*) over (partition by sku) num_rep,
	intransitstockitembatchid_bk, 
	idIntersiteTransfer_sk_fk, idStockItem_sk_fk, intersite_transfer_number, 

	product_id_magento, product_family_name, packsize, sku, 
	qty_sent, qty_remaining, qty_registered, qty_missing, 
	batch_stock_register_date
from Warehouse.stock.dim_intransit_stock_item_batch_v
where intersite_transfer_number = 'TR00004059' -- TR00004013 - TR00004059
	-- and qty_remaining = 0
order by sku

--------------------------------------------------------------

select top 1000 *
from Landing.mend.gen_inter_intransitstockitembatch_th_v
where intransitstockitembatchid in (96827391988813995, 96827391988813837)

select top 1000 *
from Landing.mend.inter_intransitstockitembatch_transferheader_aud
where intransitstockitembatchid in (96827391988813995, 96827391988813837)

select *
from Landing.mend.gen_inter_transferheader_v
where transferheaderid = 64457769666781334

-- 878
select *
from 
	(select count(*) over (partition by transferheaderid, stockitemid, stockregistereddate) num_rep, 
	
	*
	from Landing.mend.gen_inter_intransitstockitembatch_th_v) t
where num_rep > 1
	-- and ordernumber_t = 'P00051913'
order by num_rep desc, transferheaderid, stockitemid


select num_rep, intransitstockitembatchid_bk, transferheaderid_bk, stockitemid_bk, batch_stock_register_date, 
	qty_sent, qty_remaining, qty_registered, qty_missing, 
	batch_created_date, 
	delete_f
from
	(select count(*) over (partition by transferheaderid_bk, stockitemid_bk, batch_stock_register_date) num_rep, *
	from Landing.aux.stock_dim_intransit_stock_item_batch
	where delete_f = 'N'
	) t
where num_rep > 1
order by num_rep desc, transferheaderid_bk, stockitemid_bk, delete_f



--- Questions: 
	-- Duplicated Intransit Batches: Detecting per tranferheaderid, stockitemid, stockregistereddate
		-- Detecting it through a different view - run the same query and the data matches

	-- How can we identify the ISIS creation date - created_date_isib
		-- Yes, created_date_isib would be fine

	-- From Where we should have the ISIB records for Hist Report
		-- All ISIB where we have transactions (Kalyani made them based on Registered Receipts on 2019) + new ISIB on 2019

---------------------------------------------------------

-- Kaliany Query 

select ShipmentNumber, count(h/ID) as Number 
from 
	CustomerShipments.CustomerShipment cs
inner join 
	cs/IntersiteTransfer.InTransitBatchHeader_CustomerShipment/IntersiteTransfer.InTransitBatchHeader h
group by ShipmentNumber 
having count(h/ID) > 1
-- intersitetransfer$intransitbatchheader_customershipment
-- intersitetransfer$intransitbatchheader_transferheader

select cs.shipmentNumber, count(ibh_th.transferheaderid)
from 
		Landing.mend.inter_intransitbatchheader_customershipment ibh_cs
	inner join
		Landing.mend.inter_intransitbatchheader_transferheader ibh_th on ibh_cs.intransitbatchheaderid = ibh_th.intransitbatchheaderid
	inner join
		Landing.mend.ship_customershipment_aud cs on ibh_cs.customershipmentid = cs.id
group by cs.shipmentNumber
having count(ibh_th.transferheaderid) > 1
order by cs.shipmentNumber

select num_rep, id, shipmentNumber, orderIncrementID, status, createdDate, dateShippedUTC, intransitbatchheaderid, transferheaderid
from
	(select count(ibh_th.transferheaderid) over (partition by cs.shipmentNumber) num_rep, cs.*, ibh_th.intransitbatchheaderid, ibh_th.transferheaderid
	from 
			Landing.mend.inter_intransitbatchheader_customershipment ibh_cs
		inner join
			Landing.mend.inter_intransitbatchheader_transferheader ibh_th on ibh_cs.intransitbatchheaderid = ibh_th.intransitbatchheaderid
		inner join
			Landing.mend.ship_customershipment_aud cs on ibh_cs.customershipmentid = cs.id) t
-- where shipmentNumber = 'INT00000903-2'
where num_rep > 1
order by shipmentNumber, intransitbatchheaderid

-- 874 over 1098
select count(*) over (partition by t.transferheaderid, stockitemid, stockregistereddate) num_repn, 
	t.*, isib.intransitstockitembatchid, isib.stockitemid, isib.transfernum, isib.ordernumber_s, isib.ordernumber_t, 
	isib.issuedquantity, isib.remainingquantity, -- isib.lostquantity, isib.registeredquantity,
	isib.stockregistereddate, isib.createddate_isib
from
		(select num_rep, id, shipmentNumber, orderIncrementID, status, createdDate, dateShippedUTC, intransitbatchheaderid, transferheaderid
		from
			(select count(ibh_th.transferheaderid) over (partition by cs.shipmentNumber) num_rep, cs.*, ibh_th.intransitbatchheaderid, ibh_th.transferheaderid
			from 
					Landing.mend.inter_intransitbatchheader_customershipment ibh_cs
				inner join
					Landing.mend.inter_intransitbatchheader_transferheader ibh_th on ibh_cs.intransitbatchheaderid = ibh_th.intransitbatchheaderid
				inner join
					Landing.mend.ship_customershipment_aud cs on ibh_cs.customershipmentid = cs.id) t
		where num_rep > 1) t
	inner join
		-- Landing.mend.gen_inter_intransitstockitembatch_th_v isib on t.transferheaderid = isib.transferheaderid
		Landing.mend.gen_inter_intransitstockitembatch_ibh_v isib on t.intransitbatchheaderid = isib.intransitbatchheaderid
order by num_repn desc, shipmentNumber, stockitemid, intransitbatchheaderid



---------------------------------------------------------

-- Which ISIB to have as Historical - Ones created on 2019
select top 1000 *
from Landing.mend.gen_inter_intransitstockitembatch_th_v
where remainingquantity <> 0
	-- and year(createddate_isib) < 2019
order by createddate_isib

---------------------------------------------------------

-- When the ISIB is registered as WSIB
select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch
where receiptlineid_bk = 74590868831865442


select count(*) over (partition by isib.intransitstockitembatchid, isibt.transactionid) num_rep, 
	isib.intransitstockitembatchid, -- isib.transferheaderid, isib.purchaseorderid_s, isib.purchaseorderid_t, isib.warehouseid, isib.stockitemid, isib.receiptid, isibt.id intransitbatchtransactionid,
	isibt_rl.receiptlineid, 
	isib.transfernum, -- isib.allocatedstrategy, isib.createddate, 
	isib.ordernumber_s, isib.ordernumber_t, 
	isib.issuedquantity, isib.remainingquantity, isib.lostquantity, isib.registeredquantity,
	isib.stockregistereddate, isib.createddate_isib, 
	isibt.transactionid, isibt.transactiontype, isibt.qtyactioned, isibt.transactiondate, 
	wsib.batch_id, wsib.batch_stock_register_date
from 
		Landing.mend.gen_inter_intransitstockitembatch_th_v isib
	inner join
		Landing.mend.inter_intransitbatchtransacti_intransitstockitembatc_aud isibt_isib on isib.intransitstockitembatchid = isibt_isib.intransitstockitembatchid
	inner join
		Landing.mend.inter_intransitbatchtransaction_aud isibt on isibt_isib.Intransitbatchtransactionid = isibt.id
	inner join
		Landing.mend.inter_intransitbatchtransaction_receiptline_aud isibt_rl on isibt.id = isibt_rl.Intransitbatchtransactionid
	left join
		Landing.aux.stock_dim_wh_stock_item_batch wsib on isibt_rl.receiptlineid = wsib.receiptlineid_bk
-- where wsib.receiptlineid_bk = 74590868831865442
order by isib.createddate