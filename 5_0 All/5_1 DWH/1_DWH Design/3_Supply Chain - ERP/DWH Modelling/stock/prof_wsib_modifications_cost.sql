
	select top 1000
		wsib.id warehousestockitembatchid, wsibrl.receiptlineid,
		case when (bstwsib.warehousestockitembatchid is null) then 0 else 1 end batchstockmovement_f, 
		wsi.magentoProductID, wsi.name, wsi.id_string, 
		wsib.batch_id,
		wsib.receivedquantity, wsib.stockregisteredDate, wsib.receiptCreatedDate, wsib.createdDate,
		wsib.productUnitCost, wsib.totalUnitCost
		-- wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.inteCoCarriageUnitCost interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo
	from 
			Landing.mend.gen_wh_warehousestockitem_v wsi
		inner join
			Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsiwsib on wsi.warehousestockitemid = wsiwsib.warehousestockitemid
		inner join
			Landing.mend.wh_warehousestockitembatch_aud wsib on wsiwsib.warehousestockitembatchid = wsib.id
		left join
			Landing.mend.wh_warehousestockitembatch_full wsibf on wsib.id = wsibf.id
		left join
			Landing.mend.whship_warehousestockitembatch_receiptline wsibrl on wsib.id = wsibrl.warehousestockitembatchid
		left join
			Landing.mend.whship_receiptline rl on wsibrl.receiptlineid = rl.id
		left join
			(select warehousestockitembatchid, count(*) num
			from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
			group by warehousestockitembatchid) bstwsib on wsib.id = bstwsib.warehousestockitembatchid

	where wsibrl.receiptlineid = 74590868831787676




--------------------------------------------------------------------

	-- RL: invoiceUnitCost ???

	select top 1000 count(*) over () num_tot, count(*) over (partition by wsib.idWHStockItemBatch_sk) num_rep,
		wsib.idWHStockItemBatch_sk, wsib.warehousestockitembatchid_bk, wsib.idReceiptLine_sk, irl.idInvoiceReconciliationLine_sk,
		wsib.product_id_magento, wsib.product_family_name, 
		wsib.stock_batch_type_name,
		wsib.batch_id, rl.receipt_number, 

		wsib.qty_received, rl.qty_ordered, rl.qty_accepted, 

		wsib.batch_stock_register_date, rl.stock_registered_date receipt_stock_register_date,

		wsib.local_product_unit_cost, wsib.local_total_unit_cost, rl.local_unit_cost local_unit_cost_receipt, rl.global_unit_cost global_unit_cost_receipt,
		irl.unit_cost, irl.adjusted_unit_cost, irl.adjustment_applied,
		wsib.currency_code, rl.currency_code currency_code_receipt
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		left join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
		left join
			Warehouse.invrec.fact_invoice_reconciliation_line irl on rl.idReceiptLine_sk = irl.idReceiptLine_sk_fk
	where rl.idReceiptLine_sk is not null
	order by num_rep desc