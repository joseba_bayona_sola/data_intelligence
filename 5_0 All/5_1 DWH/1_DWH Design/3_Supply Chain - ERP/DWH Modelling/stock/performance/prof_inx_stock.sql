use Landing
go

drop INDEX [NCI_comp_currency_aud]  ON [mend].[comp_currency_aud] 
go

drop INDEX [nci_comp_spotrate_aud]  ON [mend].[comp_spotrate_aud] 
go

drop INDEX [NCI_catalog_product_entity_flat]  ON [mend].catalog_product_entity_flat 
go


drop INDEX [NCI_prod_productfamily_aud]  ON [mend].[prod_productfamily_aud] 
go

drop INDEX [NCI_prod_product_aud]  ON [mend].[prod_product_aud] 
go

drop INDEX [NCI_prod_contactlens_aud]  ON [mend].[prod_contactlens_aud] 
go



drop INDEX NCI_prod_stockitem_aud  ON [mend].prod_stockitem_aud 
go

drop INDEX [NCI_wh_warehousestockitem_aud]  ON [mend].[wh_warehousestockitem_aud] 
go

drop INDEX [NCI_whship_receiptline]  ON [mend].[whship_receiptline] 
go

drop INDEX [mend_wh_warehousestockitembatch_aud_stockRegisteredDate]  ON [mend].[wh_warehousestockitembatch_aud] 
go

----------------------------------------------------------------------------
----------------------------------------------------------------------------


CREATE NONCLUSTERED INDEX [NCI_comp_currency_aud] ON [mend].[comp_currency_aud] ([currencycode] ASC)
	INCLUDE ([currencyname],[standardrate],[spotrate],[laststandardupdate],[lastspotupdate],[createddate],[changeddate])
GO

CREATE NONCLUSTERED INDEX [nci_comp_spotrate_aud] ON [mend].[comp_spotrate_aud] ([datasource] ASC)
	INCLUDE ([value],[effectivedate])
GO


CREATE NONCLUSTERED INDEX [NCI_catalog_product_entity_flat] ON mag.catalog_product_entity_flat ([brand] ASC)
	INCLUDE ([manufacturer],[product_lifecycle],[visibility],[sku],[name],[status],[promotional_product],[telesales_only],[packsize])
GO

CREATE NONCLUSTERED INDEX [NCI_prod_productfamily_aud] ON [mend].[prod_productfamily_aud] ([magentoProductID] ASC)
	INCLUDE ([magentoSKU],[name])
GO

CREATE NONCLUSTERED INDEX [NCI_prod_product_aud] ON [mend].[prod_product_aud] ([SKU] ASC)
	INCLUDE ([valid],[oldSKU],[description])
GO

CREATE NONCLUSTERED INDEX [NCI_prod_contactlens_aud] ON [mend].[prod_contactlens_aud] ([id] ASC)
	INCLUDE ([bc],[di],[po],[cy],[ax],[ad],[_do],[co],[co_EDI])
GO



CREATE NONCLUSTERED INDEX NCI_prod_stockitem_aud ON mend.prod_stockitem_aud (SKU ASC)
	INCLUDE (packsize,stockitemdescription,oldSKU,manufacturerArticleID,manufacturerCodeNumber,SNAPUploadStatus)
GO

CREATE NONCLUSTERED INDEX [NCI_wh_warehousestockitem_aud] ON [mend].[wh_warehousestockitem_aud] ([id_string] ASC)
	INCLUDE ([availableqty], [actualstockqty], allocatedstockqty], [forwarddemand],[dueinqty],[stocked],[outstandingallocation], [onhold],[stockingmethod],[changed])
GO


CREATE NONCLUSTERED INDEX [NCI_whship_receiptline] ON [mend].[whship_receiptline] ([stockitem] ASC)
	INCLUDE ([status],[syncstatus],[syncresolution],[created], [processed],[quantityordered],[quantityaccepted], [quantityrejected],[quantityreturned],[unitcost])
GO

CREATE NONCLUSTERED INDEX [mend_wh_warehousestockitembatch_aud_stockRegisteredDate] ON [mend].[wh_warehousestockitembatch_aud] ([stockRegisteredDate] ASC)
	INCLUDE ([status], [arriveddate], [receivedquantity], [allocatedquantity], [issuedquantity], [onHoldQuantity], [disposedQuantity], [forwardDemand],
		[registeredQuantity], [confirmedDate], [productUnitCost], [carriageUnitCost], [dutyUnitCost], [totalUnitCost], [currency], [exchangeRate]) 
GO
