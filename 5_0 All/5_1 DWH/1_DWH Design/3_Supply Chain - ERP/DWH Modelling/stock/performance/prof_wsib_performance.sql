

-- mend_stock_warehousestockitembatch_v
-- lnd_stg_get_stock_intransit_stock_item_batch

-- lnd_stg_get_po_purchase_order
-- lnd_stg_get_rec_receipt
-- lnd_stg_get_invrec_invoice_reconciliation

-- lnd_stg_get_aux_alloc_order_line_erp_issue_measures
-- lnd_stg_get_aux_alloc_order_line_mag_erp_measures

-- tableau.stock_report_hist_ds_v

-----------------------------------------------
-----------------------------------------------

					drop table #t1

					select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, 
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						batchstockmovement_f
					into #t1
					from Landing.mend.gen_wh_warehousestockitembatch_v wsib

					drop table #t2 

					select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					into #t2
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid

					drop table #t3 

					select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					into #t3
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.map.sales_summer_time_period_aud stp on year(wsib.arrivedDate) = stp.year
						left join
							Landing.map.sales_summer_time_period_aud stp2 on year(wsib.confirmedDate) = stp2.year
						left join
							Landing.map.sales_summer_time_period_aud stp3 on year(wsib.stockregistereddate) = stp3.year
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid

			drop table #t4

			-- 4 min
			select wsib.warehousestockitembatchid, wsib.warehousestockitemid, wsib.receiptlineid, wsib.productfamilyid, 
				wsib.batch_id, wsib.fullyallocated, wsib.fullyissued, wsib.autoadjusted, 
				wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onholdquantity, wsib.registeredquantity, wsib.disposedquantity, 
				wsib.arrivedDate, wsib.confirmedDate, wsib.stockregistereddate, 
				wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, wsib.interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo, 
				case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end exchangeRate, wsib.currency,
				wsib.batchstockmovement_f
			into #t4
			from
					(select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.map.sales_summer_time_period_aud stp on year(wsib.arrivedDate) = stp.year
						left join
							Landing.map.sales_summer_time_period_aud stp2 on year(wsib.confirmedDate) = stp2.year
						left join
							Landing.map.sales_summer_time_period_aud stp3 on year(wsib.stockregistereddate) = stp3.year
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid) wsib
				inner join -- left
					Landing.mend.gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
						wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP'

	drop table #t5

	select wsib.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsib.warehousestockitemid warehousestockitemid_bk, 
		case 
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 0) then 'NO_RECEIPT_NO_MOV'
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 1) then 'NO_RECEIPT_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 0) then 'RECEIPT_NO_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 1) then 'RECEIPT_MOV'
		end stock_batch_type_bk,
		wsib.receiptlineid receiptlineid_bk,
		wsib.batch_id, wsib.fullyallocated fully_allocated_f, wsib.fullyissued fully_issued_f, wsib.autoadjusted auto_adjusted_f, 

		wsib.receivedquantity qty_received, wsib.receivedquantity + wsib.registeredQuantity - wsib.allocatedquantity - wsib.disposedQuantity - wsib.onHoldQuantity qty_available, 
		wsib.allocatedquantity - wsib.issuedquantity qty_outstanding_allocation, 
		wsib.allocatedquantity qty_allocated_stock, wsib.issuedquantity qty_issued_stock, 
		wsib.onholdquantity qty_on_hold, wsib.registeredquantity qty_registered, wsib.disposedquantity qty_disposed, 
		isnull(wsibsm.qty_registered, 0) qty_registered_sm, isnull(wsibsm.qty_disposed, 0) qty_disposed_sm, 

		convert(datetime, wsib.arrivedDate) batch_arrived_date, convert(datetime, wsib.confirmedDate) batch_confirmed_date, convert(datetime, wsib.stockregistereddate) batch_stock_register_date, 

		wsib.productUnitCost local_product_unit_cost, wsib.carriageUnitCost local_carriage_unit_cost, wsib.dutyUnitCost local_duty_unit_cost, wsib.totalUnitCost local_total_unit_cost, 
		wsib.interCoCarriageUnitCost local_interco_carriage_unit_cost, wsib.totalUnitCostIncInterCo local_total_unit_cost_interco, 
		wsib.exchangeRate local_to_global_rate, wsib.currency currency_code
		, case when (wsib2.id is not null) then 'N' else 'Y' end delete_f
	into #t5
	from 
			(select wsib.warehousestockitembatchid, wsib.warehousestockitemid, wsib.receiptlineid, wsib.productfamilyid, 
				wsib.batch_id, wsib.fullyallocated, wsib.fullyissued, wsib.autoadjusted, 
				wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onholdquantity, wsib.registeredquantity, wsib.disposedquantity, 
				wsib.arrivedDate, wsib.confirmedDate, wsib.stockregistereddate, 
				wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, wsib.interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo, 
				-- case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end exchangeRate, wsib.currency,
				wsib.exchangeRate exchangeRate, wsib.currency,
				wsib.batchstockmovement_f
			from
					(select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.map.sales_summer_time_period_aud stp on year(wsib.arrivedDate) = stp.year
						left join
							Landing.map.sales_summer_time_period_aud stp2 on year(wsib.confirmedDate) = stp2.year
						left join
							Landing.map.sales_summer_time_period_aud stp3 on year(wsib.stockregistereddate) = stp3.year
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid) wsib
				--left join
				--	Landing.mend.gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
				--		wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP'
						) wsib
		left join
			(select warehousestockitembatchid_bk, 
				sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed
			from Landing.aux.mend_stock_batchstockmovement_v
			where delete_f = 'N'
			group by warehousestockitembatchid_bk) wsibsm on wsib.warehousestockitembatchid = wsibsm.warehousestockitembatchid_bk
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.wh_warehousestockitembatch_full wsib2 on wsib.warehousestockitembatchid = wsib2.id
	
	drop table #t6

	select wsib.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsib.warehousestockitemid warehousestockitemid_bk, 
		case 
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 0) then 'NO_RECEIPT_NO_MOV'
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 1) then 'NO_RECEIPT_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 0) then 'RECEIPT_NO_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 1) then 'RECEIPT_MOV'
		end stock_batch_type_bk,
		wsib.receiptlineid receiptlineid_bk,
		wsib.batch_id, wsib.fullyallocated fully_allocated_f, wsib.fullyissued fully_issued_f, wsib.autoadjusted auto_adjusted_f, 

		wsib.receivedquantity qty_received, wsib.receivedquantity + wsib.registeredQuantity - wsib.allocatedquantity - wsib.disposedQuantity - wsib.onHoldQuantity qty_available, 
		wsib.allocatedquantity - wsib.issuedquantity qty_outstanding_allocation, 
		wsib.allocatedquantity qty_allocated_stock, wsib.issuedquantity qty_issued_stock, 
		wsib.onholdquantity qty_on_hold, wsib.registeredquantity qty_registered, wsib.disposedquantity qty_disposed, 
		isnull(wsibsm.qty_registered, 0) qty_registered_sm, isnull(wsibsm.qty_disposed, 0) qty_disposed_sm, 

		convert(datetime, wsib.arrivedDate) batch_arrived_date, convert(datetime, wsib.confirmedDate) batch_confirmed_date, convert(datetime, wsib.stockregistereddate) batch_stock_register_date, 

		wsib.productUnitCost local_product_unit_cost, wsib.carriageUnitCost local_carriage_unit_cost, wsib.dutyUnitCost local_duty_unit_cost, wsib.totalUnitCost local_total_unit_cost, 
		wsib.interCoCarriageUnitCost local_interco_carriage_unit_cost, wsib.totalUnitCostIncInterCo local_total_unit_cost_interco, 
		wsib.exchangeRate local_to_global_rate, wsib.currency currency_code
		, case when (wsib2.id is not null) then 'N' else 'Y' end delete_f
	into #t6
	from 
			(select wsib.warehousestockitembatchid, wsib.warehousestockitemid, wsib.receiptlineid, wsib.productfamilyid, 
				wsib.batch_id, wsib.fullyallocated, wsib.fullyissued, wsib.autoadjusted, 
				wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onholdquantity, wsib.registeredquantity, wsib.disposedquantity, 
				wsib.arrivedDate, wsib.confirmedDate, wsib.stockregistereddate, 
				wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, wsib.interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo, 
				case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end exchangeRate, wsib.currency,
				-- wsib.exchangeRate exchangeRate, wsib.currency,
				wsib.batchstockmovement_f
			from
					(select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.map.sales_summer_time_period_aud stp on year(wsib.arrivedDate) = stp.year
						left join
							Landing.map.sales_summer_time_period_aud stp2 on year(wsib.confirmedDate) = stp2.year
						left join
							Landing.map.sales_summer_time_period_aud stp3 on year(wsib.stockregistereddate) = stp3.year
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid) wsib
				left join
					Landing.mend.gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
						wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP'
						) wsib
		left join
			(select warehousestockitembatchid_bk, 
				sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed
			from Landing.aux.mend_stock_batchstockmovement_v
			where delete_f = 'N'
			group by warehousestockitembatchid_bk) wsibsm on wsib.warehousestockitembatchid = wsibsm.warehousestockitembatchid_bk
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.wh_warehousestockitembatch_full wsib2 on wsib.warehousestockitembatchid = wsib2.id

		drop table #t7

		select wsib.id warehousestockitembatchid, er.exchange_rate
		into #t7
		from 
				Landing.mend.wh_warehousestockitembatch_aud wsib
			inner join
				Landing.mend.gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
					wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP'

	select *
	into #gen_comp_currency_exchange_v
	from Landing.mend.gen_comp_currency_exchange_v

	select *
	from #gen_comp_currency_exchange_v
	where currencycode_to = 'GBP'

		drop table #t8

		set statistics profile on

		select wsib.id warehousestockitembatchid, er.exchange_rate
		into #t8
		from 
				Landing.mend.wh_warehousestockitembatch_aud wsib
			inner join
				#gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
					wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP'

		set statistics profile off

		select t.*, wsib.stockregistereddate, wsib.currency
		from 
				#t8 t
			inner join
				Landing.mend.wh_warehousestockitembatch_aud wsib on t.warehousestockitembatchid = wsib.id
		where exchange_rate is null
		order by wsib.stockregistereddate

		select 2141564- 2010551

		select 
		from Landing.mend.wh_warehousestockitembatch_aud
		where stockregistereddate

			select *
			from #gen_comp_currency_exchange_v
			where currencycode_to = 'GBP'
				and '2018-12-14 14:56:07.5470000' between effectivedate and nexteffectivedate

			s

-- 2010551
-- 2141564
select count(*)
from Landing.mend.wh_warehousestockitembatch_aud

------------------------------------------------------
------------------------------------------------------


			drop table #tt1

			select intransitstockitembatchid intransitstockitembatchid_bk, 
				transferheaderid transferheaderid_bk, stockitemid stockitemid_bk, 
				issuedquantity qty_sent, remainingquantity qty_remaining, registeredquantity qty_registered, lostquantity qty_missing, 
	
				createddate_isib intransit_batch_created_date, 
				createddate_isib batch_created_date, arriveddate batch_arrived_date, confirmeddate batch_confirmed_date, stockregistereddate batch_stock_register_date, 
				-- case when (wsib.createddate_isib between stp0.from_date and stp0.to_date) then dateadd(hour, 1, wsib.createddate_isib) else wsib.createddate_isib end batch_created_date,
				-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
				-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
				-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,	
				
				-- version 1
				-- productunitcost local_product_unit_cost, carriageUnitCost local_carriage_unit_cost, totalunitCost local_total_unit_cost, 
				-- 0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo local_total_unit_cost_interco, 
				
				-- version 2
				-- productunitcost * 1 / convert(decimal(12, 4), er.exchange_rate) local_product_unit_cost, carriageUnitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_carriage_unit_cost, 
				-- totalunitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost, 
				-- 0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost_interco, 

				--productunitcost * convert(decimal(12, 4), er2.exchange_rate) local_product_unit_cost, carriageUnitCost * convert(decimal(12, 4), er2.exchange_rate) local_carriage_unit_cost, 
				--totalunitCost * convert(decimal(12, 4), er2.exchange_rate) local_total_unit_cost, 
				--0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo * convert(decimal(12, 4), er2.exchange_rate) local_total_unit_cost_interco, 

				--convert(decimal(12, 4), er.exchange_rate) local_to_global_rate, w.currencycode currency_code, 

				'N' wh_stock_item_batch_f, 'N' delete_f
			into #tt1

			from 
					Landing.mend.gen_inter_intransitstockitembatch_th_v th
				left join
					Landing.map.sales_summer_time_period_aud stp0 on year(th.createddate_isib) = stp0.year
				left join
					Landing.map.sales_summer_time_period_aud stp on year(th.arrivedDate) = stp.year
				left join
					Landing.map.sales_summer_time_period_aud stp2 on year(th.confirmedDate) = stp2.year
				left join
					Landing.map.sales_summer_time_period_aud stp3 on year(th.stockregistereddate) = stp3.year
				inner join
					Landing.mend.gen_wh_warehouse_v w on th.warehouseid = w.warehouseid
				--left join
				--	Landing.mend.gen_comp_currency_exchange_v er on th.stockregistereddate > er.effectivedate and th.stockregistereddate <= er.nexteffectivedate and
				--		w.currencycode = er.currencycode_from and er.currencycode_to = 'GBP'
				--left join
				--	Landing.mend.gen_comp_currency_exchange_v er2 on th.stockregistereddate > er2.effectivedate and th.stockregistereddate <= er2.nexteffectivedate and
				--		er2.currencycode_from = th.currency and er2.currencycode_to = w.currencycode



			drop table #tt2

			select intransitstockitembatchid intransitstockitembatchid_bk, 
				transferheaderid transferheaderid_bk, stockitemid stockitemid_bk, 
				issuedquantity qty_sent, remainingquantity qty_remaining, registeredquantity qty_registered, lostquantity qty_missing, 
	
				createddate_isib intransit_batch_created_date, 
				createddate_isib batch_created_date, arriveddate batch_arrived_date, confirmeddate batch_confirmed_date, stockregistereddate batch_stock_register_date, 
				-- case when (wsib.createddate_isib between stp0.from_date and stp0.to_date) then dateadd(hour, 1, wsib.createddate_isib) else wsib.createddate_isib end batch_created_date,
				-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
				-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
				-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,	
				
				-- version 1
				-- productunitcost local_product_unit_cost, carriageUnitCost local_carriage_unit_cost, totalunitCost local_total_unit_cost, 
				-- 0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo local_total_unit_cost_interco, 
				
				-- version 2
				-- productunitcost * 1 / convert(decimal(12, 4), er.exchange_rate) local_product_unit_cost, carriageUnitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_carriage_unit_cost, 
				-- totalunitCost * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost, 
				-- 0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo * 1 / convert(decimal(12, 4), er.exchange_rate) local_total_unit_cost_interco, 

				productunitcost * convert(decimal(12, 4), er2.exchange_rate) local_product_unit_cost, carriageUnitCost * convert(decimal(12, 4), er2.exchange_rate) local_carriage_unit_cost, 
				totalunitCost * convert(decimal(12, 4), er2.exchange_rate) local_total_unit_cost, 
				0 local_interco_carriage_unit_cost, totalUnitCostIncInterCo * convert(decimal(12, 4), er2.exchange_rate) local_total_unit_cost_interco, 

				convert(decimal(12, 4), er.exchange_rate) local_to_global_rate, w.currencycode currency_code, 

				'N' wh_stock_item_batch_f, 'N' delete_f
			into #tt2

			from 
					Landing.mend.gen_inter_intransitstockitembatch_th_v th
				left join
					Landing.map.sales_summer_time_period_aud stp0 on year(th.createddate_isib) = stp0.year
				left join
					Landing.map.sales_summer_time_period_aud stp on year(th.arrivedDate) = stp.year
				left join
					Landing.map.sales_summer_time_period_aud stp2 on year(th.confirmedDate) = stp2.year
				left join
					Landing.map.sales_summer_time_period_aud stp3 on year(th.stockregistereddate) = stp3.year
				inner join
					Landing.mend.gen_wh_warehouse_v w on th.warehouseid = w.warehouseid
				left join
					Landing.mend.gen_comp_currency_exchange_v er on th.stockregistereddate > er.effectivedate and th.stockregistereddate <= er.nexteffectivedate and
						w.currencycode = er.currencycode_from and er.currencycode_to = 'GBP'
				left join
					Landing.mend.gen_comp_currency_exchange_v er2 on th.stockregistereddate > er2.effectivedate and th.stockregistereddate <= er2.nexteffectivedate and
						er2.currencycode_from = th.currency and er2.currencycode_to = w.currencycode

			drop table #tt3

		select isib.id intransitstockitembatchid, er.exchange_rate
		into #tt3
		from 
				Landing.mend.inter_intransitstockitembatch isib
			inner join
				#gen_comp_currency_exchange_v er on isib.stockregistereddate > er.effectivedate and isib.stockregistereddate <= er.nexteffectivedate and
					isib.currency = er.currencycode_from and er.currencycode_to = 'GBP'


------------------------------------------------------
------------------------------------------------------

		select rec.receiptid_bk, 
			rec.shipment_id_bk, rec.warehouseid_bk, rec.supplier_id_bk, rec.receipt_status_bk, 
			case when (rec.num_rep > 1) 
				then rec.receipt_number + '-'  + convert(varchar, rec.ord_rep) 
				else rec.receipt_number
			end receipt_number, 		
			rec.receipt_no, rec.supplier_advice_ref,
			convert(datetime, rec.created_date) created_date, convert(datetime, rec.arrived_date) arrived_date, convert(datetime, rec.confirmed_date) confirmed_date, convert(datetime, rec.stock_registered_date) stock_registered_date, 
			-- null due_date, 
			convert(datetime, rec.due_date) due_date,
			rec.local_total_cost, 
			convert(decimal(12, 4), er.exchange_rate) local_to_wh_rate, convert(decimal(12, 4), er2.exchange_rate) local_to_global_rate, rec.currency_code, rec.currency_code_wh,
			'N' delete_f
		from
				(select receiptid receiptid_bk, 
					shipment_id shipment_id_bk, rec.warehouseid warehouseid_bk, supplier_id supplier_id_bk, status receipt_status_bk, 
					receipt_id receipt_number, receiptNumber receipt_no, supplieradviceref supplier_advice_ref,
					createdDate created_date, arrivedDate arrived_date, confirmedDate confirmed_date, stockRegisteredDate stock_registered_date, dueDate due_date,
					-- case when (rec.createddate between stp.from_date and stp.to_date) then dateadd(hour, 1, rec.createddate) else rec.createddate end created_date,
					-- case when (rec.arrivedDate between stp2.from_date and stp2.to_date) then dateadd(hour, 1, rec.arrivedDate) else rec.arrivedDate end arrived_date,
					-- case when (rec.confirmedDate between stp3.from_date and stp3.to_date) then dateadd(hour, 1, rec.confirmedDate) else rec.confirmedDate end confirmed_date,
					-- case when (rec.stockRegisteredDate between stp4.from_date and stp4.to_date) then dateadd(hour, 1, rec.stockRegisteredDate) else rec.stockRegisteredDate end stock_registered_date,
					-- case when (rec.dueDate between stp5.from_date and stp5.to_date) then dateadd(hour, 1, rec.dueDate) else rec.dueDate end due_date,
					totalItemPrice local_total_cost, 
					0 local_to_global_rate, rec.currencyCode currency_code, w.currencycode currency_code_wh,
					count(*) over (partition by receipt_id) num_rep, 
					rank() over (partition by receipt_id order by receiptid) ord_rep
				from 
						Landing.mend.gen_whship_receipt_v rec
					left join
						Landing.map.sales_summer_time_period_aud stp on year(rec.createddate) = stp.year		
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(rec.arrivedDate) = stp2.year		
					left join
						Landing.map.sales_summer_time_period_aud stp3 on year(rec.confirmedDate) = stp3.year		
					left join
						Landing.map.sales_summer_time_period_aud stp4 on year(rec.stockRegisteredDate) = stp4.year		
					left join
						Landing.map.sales_summer_time_period_aud stp5 on year(rec.dueDate) = stp5.year	
					left join
						Landing.mend.gen_wh_warehouse_v w on rec.warehouseid = w.warehouseid			
				where shipment_ID is not null and rec.warehouseid is not null and supplier_id is not null) rec
			left join
				Landing.mend.gen_comp_currency_exchange_v er on isnull(rec.stock_registered_date, rec.created_date) > er.effectivedate and isnull(rec.stock_registered_date, rec.created_date) <= er.nexteffectivedate and
						er.currencycode_from = rec.currency_code and er.currencycode_to = rec.currency_code_wh	
			left join
				Landing.mend.gen_comp_currency_exchange_v er2 on isnull(rec.stock_registered_date, rec.created_date) > er2.effectivedate and isnull(rec.stock_registered_date, rec.created_date) <= er2.nexteffectivedate and
						er2.currencycode_from = rec.currency_code and er2.currencycode_to = 'GBP'	