select *
into #wsibm
from
	(select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, wsib.batch_id, wsib.receiptlineid,
		stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		wsib.receivedquantity qty_received, 
		qty_movement,
		case 
			when (stock_adjustment_type_bk in ('Manual_Positive')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_2')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement <> wsib.receivedquantity) then qty_movement 
			else 0 
		end qty_registered, 
		case when (stock_adjustment_type_bk in ('Negative', 'Manual_Negative')) then qty_movement else 0 end qty_disposed, 
		-- case when (bsm.batch_movement_date between stp.from_date and stp.to_date) then dateadd(hour, 1, bsm.batch_movement_date) else bsm.batch_movement_date end batch_movement_date,
		batch_movement_date
	from 
			(select batchstockmovementid batchstockmovementid_bk, 
				batchstockmovement_id batch_stock_move_id, 
				case 
					when (moveid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (moveid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else moveid
				end	move_id, 
				case 
					when (movelineid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (movelineid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else movelineid
				end	move_line_id, 	

				warehousestockitembatchid warehousestockitembatchid_bk, 
				case when (stockadjustment is null) then batchstockmovementtype else stockadjustment end stock_adjustment_type_bk, 
				case when (movetype is null) then 'MANUAL' else movetype end stock_movement_type_bk, 
				smp.stock_movement_period stock_movement_period_bk, 
	
				quantity qty_movement, 
				isnull(batchstockmovement_createdate, stockmovement_createdate) batch_movement_date	
			from 
					Landing.mend.gen_wh_batchstockmovement_v bsm
				inner join
					Landing.map.erp_stock_movement_period_aud smp on isnull(bsm.batchstockmovement_createdate, bsm.stockmovement_createdate) between smp.period_start and smp.period_finish
			) bsm
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.map.sales_summer_time_period_aud stp on year(bsm.batch_movement_date) = stp.year) t
where stock_adjustment_type_bk = 'Positive' and stock_movement_period_bk = 'SM_PERIOD_1' 
	and qty_registered = 0 and receiptlineid is not null and qty_movement <> 0
	-- and qty_registered <> 0 and receiptlineid is null 

select top 1000 *
from #wsibm

select top 1000 rl.delete_f, t1.*
from 
		#wsibm t1
	inner join
		Landing.aux.rec_fact_receipt_line rl on t1.receiptlineid = rl.receiptlineid_bk

---------------------------------------------------------------------------

select wsib.*, t1.batch_id, t1.qty_movement, rl.delete_f, rl.qty_accepted
from
		(select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, batch_stock_register_date,
			qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, 
			qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, 
			qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold) qty_diff, 
			(qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold)) * global_product_unit_cost cost_diff
		from Warehouse.stock.dim_wh_stock_item_batch_v
		where product_id_magento = product_id_magento
			and qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold) wsib
	right join
		#wsibm t1 on wsib.batch_id = t1.batch_id
	inner join
		Landing.aux.rec_fact_receipt_line rl on t1.receiptlineid = rl.receiptlineid_bk


------------------------------------------------------------------------------

drop table #wsibm_new

	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		case 
			when (stock_adjustment_type_bk in ('Manual_Positive')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_2')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement <> wsib.receivedquantity) then qty_movement 
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement = wsib.receivedquantity 
				and rl.quantityaccepted is not null and rl.quantityaccepted = wsib.receivedquantity) then qty_movement
			else 0 
		end qty_registered, 
		case when (stock_adjustment_type_bk in ('Negative', 'Manual_Negative')) then qty_movement else 0 end qty_disposed, 
		-- case when (bsm.batch_movement_date between stp.from_date and stp.to_date) then dateadd(hour, 1, bsm.batch_movement_date) else bsm.batch_movement_date end batch_movement_date,
		batch_movement_date
	into #wsibm_new
	from 
			(select batchstockmovementid batchstockmovementid_bk, 
				batchstockmovement_id batch_stock_move_id, 
				case 
					when (moveid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (moveid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else moveid
				end	move_id, 
				case 
					when (movelineid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (movelineid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else movelineid
				end	move_line_id, 	

				warehousestockitembatchid warehousestockitembatchid_bk, 
				case when (stockadjustment is null) then batchstockmovementtype else stockadjustment end stock_adjustment_type_bk, 
				case when (movetype is null) then 'MANUAL' else movetype end stock_movement_type_bk, 
				smp.stock_movement_period stock_movement_period_bk, 
	
				quantity qty_movement, 
				isnull(batchstockmovement_createdate, stockmovement_createdate) batch_movement_date	
			from 
					Landing.mend.gen_wh_batchstockmovement_v bsm
				inner join
					Landing.map.erp_stock_movement_period_aud smp on isnull(bsm.batchstockmovement_createdate, bsm.stockmovement_createdate) between smp.period_start and smp.period_finish
			) bsm
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
		left join
			Landing.mend.whship_receiptline rl on wsib.receiptlineid = rl.id
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.map.sales_summer_time_period_aud stp on year(bsm.batch_movement_date) = stp.year


select top 1000 *
from 
		#wsibm_new t1
	inner join
		Warehouse.stock.fact_wh_stock_item_batch_movement wsibm on t1.batchstockmovementid_bk = wsibm.batchstockmovementid_bk
where t1.qty_registered <> wsibm.qty_registered
-- where t1.qty_disposed <> wsibm.qty_disposed
-- where t1.qty_movement <> wsibm.qty_movement