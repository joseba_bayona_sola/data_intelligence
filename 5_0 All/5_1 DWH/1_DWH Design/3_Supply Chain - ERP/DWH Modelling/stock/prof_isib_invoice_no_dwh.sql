
select top 1000 *
from Warehouse.stock.dim_intransit_stock_item_batch_v
where idWholesaleOrderShipment_sk_fk is null
order by intransit_batch_created_date desc

select top 1000 year(intransit_batch_created_date), month(intransit_batch_created_date), count(*)
from Warehouse.stock.dim_intransit_stock_item_batch_v
where idWholesaleOrderShipment_sk_fk is null
group by year(intransit_batch_created_date), month(intransit_batch_created_date)
order by year(intransit_batch_created_date), month(intransit_batch_created_date)

-- 

select top 1000 *
from 
	Staging.stock.dim_intransit_stock_item_batch

select isib.intransitstockitembatchid_bk, csi.customershipmentinvoiceid,
	isib.warehouse_name_from, isib.warehouse_name_to, isib.intersite_transfer_number, isib.created_date, isib.intransit_batch_created_date,
	isib.purchase_order_number_t, isib.po_status_name_t, isib.purchase_order_number_s, isib.order_no_erp, isib.wholesale_customer_name, 
	csi.shipmentnumber, csi.ordernumber
from 
		Staging.stock.dim_intransit_stock_item_batch isib_s
	inner join
		Warehouse.stock.dim_intransit_stock_item_batch_v isib on isib_s.intransitstockitembatchid_bk = isib.intransitstockitembatchid_bk
	inner join
		Landing.mend.gen_whcust_customershipmentinvoice_v csi on isib_s.customershipmentinvoiceid_bk = csi.customershipmentinvoiceid
where isib_s.customershipmentinvoiceid_bk is not null and isib.idWholesaleOrderShipment_sk_fk is null
order by isib.intransit_batch_created_date
