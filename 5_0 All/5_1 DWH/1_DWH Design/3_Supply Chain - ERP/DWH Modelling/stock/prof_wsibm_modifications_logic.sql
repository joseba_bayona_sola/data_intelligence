
-- Stock Sync
-- Manual Stock Movement
-- Damages 
-- Stock Check
-- Sales Sample
-- Warehouse Adjust Only
-- Audit
-- Unknown

select top 1000 batchstockmovementid, batchstockmovement_createdate, stockmovement_createdate, warehousestockitembatch_createdate, 
	_class, reasonid, batchstockmovementtype, 
	case
		when (operator = 'SYSTEM') then 'Stock Sync'
		when (batchstockmovementtype in ('Manual_Negative', 'Manual_Positive')) then 'Manual Stock Movement'	
		when (reasonid = '01') then 'Damages'
		when (reasonid = '02') then 'Stock Check'
		when (reasonid = '03') then 'Sales Sample'
		when (reasonid = '99') then 'Warehouse Adjust Only'
		when (reasonid = 'AU') then 'Audit'
		else 'Unknown'
	end movement_reason,
	comment movement_comment, 
	operator, 
	case 
        when (comment like '%Patch%62%') then warehousestockitembatch_createdate 
		else coalesce(batchstockmovement_createdate, warehousestockitembatch_createdate) 
		-- else coalesce(batchstockmovement_createdate, stockmovement_createdate, warehousestockitembatch_createdate) 
    end batch_movement_date,
	isnull(batchstockmovement_createdate, stockmovement_createdate) batch_movement_create_date 
from Landing.mend.gen_wh_batchstockmovement_v
order by batchstockmovementid desc
