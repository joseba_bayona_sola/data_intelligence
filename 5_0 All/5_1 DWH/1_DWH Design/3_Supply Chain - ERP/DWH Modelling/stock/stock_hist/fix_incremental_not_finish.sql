
select top 1000 count(*)
from Landing.aux.stock_dim_wh_stock_item_batch_hist

select top 1000 idCalendarStockDate, count(*)
from Landing.aux.stock_dim_wh_stock_item_batch_hist
group by idCalendarStockDate
order by idCalendarStockDate

	-- 247991
	select convert(date, ins_ts), count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by convert(date, ins_ts)
	order by convert(date, ins_ts)

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
-- where warehousestockitembatchid_bk in (43910096369074086, 43910096368684047, 43910096369144459, 43910096369149220)
where warehousestockitembatchid_bk in (43910096369503071)

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist
-- where warehousestockitembatchid_bk in (43910096369074086, 43910096368684047, 43910096369144459, 43910096369149220)
where warehousestockitembatchid_bk in (43910096369503071)

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
-- where warehousestockitembatchid_bk in (43910096369074086, 43910096368684047, 43910096369144459, 43910096369149220)
where warehousestockitembatchid_bk in (43910096369503071)

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
-- where convert(date, ins_ts) = '2019-03-19'
where convert(date, ins_ts) = '2020-01-10'
order by warehousestockitembatchid_bk

-------------------------------------------

	select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	where convert(date, ins_ts) = '2019-07-08'
		and last_stock_date is null

select t2.warehousestockitembatchid_bk, t2.invoicelineid_bk, 
	t2.last_qty_on_hand, t1.last_qty_on_hand, t2.last_stock_date, t1.last_stock_date
from
		(select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
		where convert(date, ins_ts) = '2019-03-08') t1
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info t2 
		on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk 	



disable trigger aux.trg_stock_dim_wh_stock_item_batch_hist_info_hist_last on aux.stock_dim_wh_stock_item_batch_hist_info
go

update t2
set t2.last_qty_on_hand = t1.last_qty_on_hand, 
	t2.last_stock_date = t1.last_stock_date
from
		(select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
		where convert(date, ins_ts) = '2020-01-10'
			and last_stock_date is not null) t1
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info t2 
			on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk 	

update t2
set t2.last_qty_on_hand = t1.last_qty_on_hand, 
	t2.last_stock_date = t1.last_stock_date, 
	t2.update_type = 'F'
from
		(select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
		where convert(date, ins_ts) = '2020-01-10'
			and last_stock_date is null) t1
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info t2 
			on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk 	

enable trigger aux.trg_stock_dim_wh_stock_item_batch_hist_info_hist_last on aux.stock_dim_wh_stock_item_batch_hist_info
go


------------------------------------------------------------------------------

disable trigger aux.trg_stock_dim_wh_stock_item_batch_hist_info_hist_last on aux.stock_dim_wh_stock_item_batch_hist_info
go

update t2
set t2.last_qty_on_hand = t1.last_qty_on_hand, 
	t2.last_stock_date = t1.last_stock_date
from
		(select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
		where convert(date, ins_ts) = '2020-01-11'
			and last_stock_date is not null) t1
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info t2 
			on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk 	

update t2
set t2.last_qty_on_hand = t1.last_qty_on_hand, 
	t2.last_stock_date = t1.last_stock_date, 
	t2.update_type = 'F'
from
		(select warehousestockitembatchid_bk, invoicelineid_bk, last_qty_on_hand, last_stock_date
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
		where convert(date, ins_ts) = '2020-01-11'
			and last_stock_date is null) t1
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info t2 
			on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk 	

enable trigger aux.trg_stock_dim_wh_stock_item_batch_hist_info_hist_last on aux.stock_dim_wh_stock_item_batch_hist_info
go

	select max(cut_off_date)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
	where lnd_stg_f = 'Y'

	update Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
	set lnd_stg_f = 'N'
	where idETLBatchRun in (4540, 4534)