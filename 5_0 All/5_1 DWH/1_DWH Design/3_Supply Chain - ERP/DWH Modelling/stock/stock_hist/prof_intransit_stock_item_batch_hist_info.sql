
select top 1000 count(*) over (),
	intransitstockitembatchid_bk, 
	intransit_batch_created_date, 
	qty_received,
	delete_f, update_type, 
	last_qty_on_hand, last_stock_date,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info

	select idETLBatchRun_ins, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	select delete_f, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	group by delete_f
	order by delete_f

	select update_type, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	group by update_type
	order by update_type		

	select last_stock_date, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	-- where update_type = 'I'
	group by last_stock_date
	order by last_stock_date

	select convert(date, intransit_batch_created_date), count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info
	where update_type = 'F'
	group by convert(date, intransit_batch_created_date)
	order by convert(date, intransit_batch_created_date)

------------------------------------------------

select top 1000 num_rep, 
	isibh.intransitstockitembatchid_bk, 
	isibh.intransit_batch_created_date, 
	isibh.transfernum, isibh.magentoProductID_int, isibh.name,
	isibh.qty_received,
	isibh.delete_f, isibh.update_type, 
	isibh.last_qty_on_hand, isibh.last_stock_date,
	isibh.idETLBatchRun_ins, isibh.ins_ts, isibh.idETLBatchRun_upd, isibh.upd_ts
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_v isibh
where num_rep > 1 -- and wsibh.warehousestockitembatchid_bk = 43910096368992820
	-- and wsib.magentoProductID_int in (1083, 1084, 2298, 1100, 1089)
order by isibh.magentoProductID_int, isibh.name, isibh.intransitstockitembatchid_bk



------------------------------------------------

select top 1000 count(*) over (),
	isibh.intransitstockitembatchid_bk, 
	isibh.intransit_batch_created_date, 
	isibh.transfernum, isibh.magentoProductID_int, isibh.name,
	isibh.qty_received,
	isibh.delete_f, isibh.update_type, 
	isibh.last_qty_on_hand, isibh.last_stock_date,
	isibh.idETLBatchRun_ins, isibh.ins_ts, isibh.idETLBatchRun_upd, isibh.upd_ts
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_v isibh
-- where wsib.magentoProductID_int = 1083 and wsibh.last_stock_date = '2019-02-25'
where isibh.update_type = 'N' and isibh.last_stock_date is null
order by isibh.intransitstockitembatchid_bk desc

	select 
		isibh.magentoProductID_int, isibh.name, 
		count(*) num_tot, 
		count(isibh.last_stock_date) num_stock, count(*) - count(isibh.last_stock_date) num_diff,
		min(isibh.last_stock_date) min_last_stock_date, max(isibh.last_stock_date) max_last_stock_date, 
		count(case when (isibh.update_type = 'N') then 1 else null end) num_n, 
		count(case when (isibh.update_type = 'I') then 1 else null end) num_i, 
		count(case when (isibh.update_type = 'F') then 1 else null end) num_f, 
		count(case when (isibh.update_type = 'D') then 1 else null end) num_d, 
		count(case when (isibh.update_type = 'T') then 1 else null end) num_t, 
		min(isibh.intransit_batch_created_date) min_stock_register_date, max(isibh.intransit_batch_created_date) max_stock_register_date
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_v isibh
	-- where wsibh.update_type = 'I'
	-- where isibh.last_stock_date is null
	group by isibh.magentoProductID_int, isibh.name
	order by isibh.magentoProductID_int, isibh.name
	-- order by wsibh.name
	-- order by num_tot desc


------------------------------------------------

select *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist


select top 1000 *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_v
where record_type = 'H'

select count(*) over (partition by intransitstockitembatchid_bk) num_rep, *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_v
where num_records > 1
	and update_type <> 'T'
order by num_rep desc, intransitstockitembatchid_bk, aud_dateFrom


------------------------------------------------

select *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last

	select idETLBatchRun_ins, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select ins_ts, count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last
	group by ins_ts
	order by ins_ts

	select convert(date, ins_ts), count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last
	group by convert(date, ins_ts)
	order by convert(date, ins_ts)

select top 1000 *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last_v
where num_rep > 1
order by transfernum, ins_ts


------------------------------------------------

select *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_cut_off
order by idETLBatchRun desc

------------------------------------------------

select *
from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_v
where update_type in ('D', 'T')	
order by magentoProductID_int, name, intransitstockitembatchid_bk
