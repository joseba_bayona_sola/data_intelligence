
select top 1000 count(*) over (),
	warehousestockitembatchid_bk, invoicelineid_bk, 
	receiptlineid_bk, batchstockmovementid_bk, 
	batch_stock_register_date, invoice_posted_date,
	qty_received,
	delete_f, update_type, 
	last_qty_on_hand, last_stock_date,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
-- where idETLBatchRun_upd is not null
-- where invoicelineid_bk <> -1
where invoicelineid_bk <> -1 and day(invoice_posted_date) in (1, 2) and last_stock_date > invoice_posted_date

	select idETLBatchRun_ins, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	select delete_f, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by delete_f
	order by delete_f

	select update_type, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	-- where invoicelineid_bk <> -1
	group by update_type
	order by update_type		

	select last_stock_date, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where update_type = 'I'
	group by last_stock_date
	order by last_stock_date

	select convert(date, batch_stock_register_date), count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where update_type = 'F'
	group by convert(date, batch_stock_register_date)
	order by convert(date, batch_stock_register_date)

------------------------------------------------

select top 1000 num_rep, 
	wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, 
	wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
	wsibh.batch_stock_register_date, wsibh.invoice_posted_date,
	wsibh.batch_id, wsibh.magentoProductID_int, wsibh.name,
	wsibh.qty_received,
	wsibh.delete_f, wsibh.update_type, 
	wsibh.last_qty_on_hand, wsibh.last_stock_date,
	wsibh.idETLBatchRun_ins, wsibh.ins_ts, wsibh.idETLBatchRun_upd, wsibh.upd_ts
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
where num_rep > 1 -- and wsibh.warehousestockitembatchid_bk = 43910096368992820
	-- and wsib.magentoProductID_int in (1083, 1084, 2298, 1100, 1089)
order by wsibh.magentoProductID_int, wsibh.name, wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk


------------------------------------------------

select top 1000 count(*) over (),
	wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, 
	wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
	wsibh.batch_stock_register_date, wsibh.invoice_posted_date,
	wsibh.batch_id, wsibh.magentoProductID_int, wsibh.name,
	wsibh.qty_received,
	wsibh.delete_f, wsibh.update_type, 
	wsibh.last_qty_on_hand, wsibh.last_stock_date,
	wsibh.idETLBatchRun_ins, wsibh.ins_ts, wsibh.idETLBatchRun_upd, wsibh.upd_ts
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
-- where wsib.magentoProductID_int = 1083 and wsibh.last_stock_date = '2019-02-25'
where wsibh.update_type = 'N' and wsibh.last_stock_date is null
order by wsibh.warehousestockitembatchid_bk desc

	select p.update_num,
		wsibh.magentoProductID_int, wsibh.name, 
		-- case when (wsibh.last_stock_date is null
		count(*) num_tot, 
		count(wsibh.last_stock_date) num_stock, count(*) - count(wsibh.last_stock_date) num_diff,
		min(wsibh.last_stock_date) min_last_stock_date, max(wsibh.last_stock_date) max_last_stock_date, 
		count(case when (wsibh.update_type = 'N') then 1 else null end) num_n, 
		count(case when (wsibh.update_type = 'I') then 1 else null end) num_i, 
		count(case when (wsibh.update_type = 'F') then 1 else null end) num_f, 
		count(case when (wsibh.update_type = 'D') then 1 else null end) num_d, 
		count(case when (wsibh.update_type = 'T') then 1 else null end) num_t, 
		min(wsibh.batch_stock_register_date) min_stock_register_date, max(wsibh.batch_stock_register_date) max_stock_register_date
	from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
	left join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products p on wsibh.magentoProductID_int = p.product_id_magento
	-- where wsibh.update_type = 'I'
	group by p.update_num, wsibh.magentoProductID_int, wsibh.name
	order by p.update_num, wsibh.magentoProductID_int, wsibh.name
	-- order by wsibh.name
	-- order by num_tot desc

------------------------------------------------

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist


select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_v
where record_type = 'H'

select count(*) over (partition by warehousestockitembatchid_bk, invoicelineid_bk) num_rep, *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_v
where num_records > 1
	and update_type <> 'T'
order by num_rep desc, warehousestockitembatchid_bk, invoicelineid_bk, aud_dateFrom

------------------------------------------------

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last


	select idETLBatchRun_ins, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select ins_ts, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by ins_ts
	order by ins_ts

	select convert(date, ins_ts), count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by convert(date, ins_ts)
	order by convert(date, ins_ts)

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last_v
where num_rep > 1
order by batch_id, ins_ts

------------------------------------------------

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
order by idETLBatchRun desc

------------------------------------------------

select product_id_magento, name, num_tot, update_num
from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products
order by num_tot desc

------------------------------------------------

select *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v
where update_type in ('D') --, 'T')	
order by magentoProductID_int, name, warehousestockitembatchid_bk, invoicelineid_bk

------------------------------------------------

select *
from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud
order by issue_date desc