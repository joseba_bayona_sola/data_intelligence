

	select update_type, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	-- where invoicelineid_bk <> -1
	-- where last_stock_date = convert(date, getutcdate() -2)
	group by update_type
	order by update_type	

	select convert(date, batch_stock_register_date), count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where update_type = 'F'
	group by convert(date, batch_stock_register_date)
	order by convert(date, batch_stock_register_date)

	select last_stock_date, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where update_type = 'N'
	group by last_stock_date
	order by last_stock_date desc

	--------------------------------------
	--------------------------------------


	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full' --  lnd_stg_get_aux_stock_wh_stock_item_batch_hist_incr - lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full
	order by spm.finishTime desc, spm.messageTime

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_merge_stock_wh_stock_item_batch_hist' --  stg_dwh_get_stock_wh_stock_item_batch_hist_del - stg_dwh_get_stock_wh_stock_item_batch_hist - stg_dwh_merge_stock_wh_stock_item_batch_hist
	order by spm.finishTime desc, spm.messageTime

	--------------------------------------
	--------------------------------------

	-- Implications and Use of @prev_cut_off_date and @cut_off_date
		
		-- @prev_cut_off_date: 
			-- value: Run Date - 3: Is it right (What if we don't run for some days)
			-- Flagging: For setting I records = Equal to last_stock_date

		-- @cut_off_date: 
			-- value: Run Date - 2: For setting the cut off / 2 days so we allow full day (backup at 9 pm, we miss some issue records)
			-- In Incr, Full for setting the limit of days to calculate the stock 
			-- In Full for updating F records to N (when don't have a record as they were consumed in the same day)

	select *, 
		count(*) over (partition by cut_off_date) num_rep, 
		datediff(dd, lag(cut_off_date) over (order by idETLBatchRun), cut_off_date) diff_days
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
	order by idETLBatchRun desc

	--------------------------------------
	--------------------------------------

	-- Update with T flag batches: Physically Deleted IRL 
	select ir.invoice_ref, ir.net_suite_no, ir.invoice_date, irl.* 
	from 
			Landing.aux.invrec_dim_invoice_reconciliation ir
		inner join
			Landing.aux.invrec_fact_invoice_reconciliation_line irl on ir.invoiceid_bk = irl.invoiceid_bk
		left join
			Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on irl.invoicelineid_bk = wsibh.invoicelineid_bk
	where irl.delete_f = 'Y'
		and wsibh.invoicelineid_bk is not null -- Needs to be posted

	-- Update with D flag batches: WSIB from Physically Deleted IRL -- 956
	select distinct wsibh.warehousestockitembatchid_bk
	from 
			Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
		inner join
			(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk

	-- Update with D flag batches: issue sync was delayed			
	select *, datediff(dd, createddate, shippeddate) diff
	from Landing.mend.wh_batchstockissue_aud
	where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1
	order by id desc
		
		-- 2074
		select distinct wsib.warehousestockitembatchid
		from
				(select *, datediff(dd, createddate, shippeddate) diff
				from Landing.mend.wh_batchstockissue_aud
				where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1) bsi
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsib on bsi.id = wsib.batchstockissueid

	-- update I flag batches: Incremental Run
	select *
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
	order by idETLBatchRun desc

	select *
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info trg
	where trg.last_stock_date = '2019-08-04' and trg.update_type not in ('D', 'T')



	--------------------------------------
	--------------------------------------

	-- Incremental



	-- Full

		select trg.*
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info trg
			inner join
				(select warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, qty_on_hand
				from
					(select warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, qty_on_hand, 
						count(*) over (partition by warehousestockitembatchid_bk, invoicelineid_bk) num_rep, 
						dense_rank() over (partition by warehousestockitembatchid_bk, invoicelineid_bk order by idCalendarStockDate) ord_rep
					from Landing.aux.stock_dim_wh_stock_item_batch_hist) wsibh
				where num_rep = ord_rep) src on trg.warehousestockitembatchid_bk = src.warehousestockitembatchid_bk and trg.invoicelineid_bk = src.invoicelineid_bk
			inner join
				Landing.aux.dim_calendar c on src.idCalendarStockDate = c.idCalendar_sk	
		where trg.update_type = 'D'


	--------------------------------------
	--------------------------------------

	select count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist_del
