

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist_info

	-- #stock_dim_wh_stock_item_batch_hist_info: Create + Load
		-- WSIB - RL - NO INV REC
		-- WSIB - INV REC
		-- WSIB
		-- WSIB - WSIBM

	-- Merge to aux.stock_dim_wh_stock_item_batch_hist_info from #stock_dim_wh_stock_item_batch_hist_info

	-- Update aux.stock_dim_wh_stock_item_batch_hist_info
		-- Invoiced WSIB - RL: T flag 
		-- Physically Deleted IRL: T flag 

	-- Flags
		-- N: No load needed
		-- F: Full load needed
		-- I: Incremental load needed
		-- D: Partial batches where Full load is needed
		-- T: Truncated batches: They refer to batches that later an invoice has been posted, creating  a new batch - invoice entity for them. 
		-- S: Special batches where full just one full load is need to fix issues

-- lnd_stg_get_stock_wh_stock_item_batch_hist

	-- @prev_cut_off_date = max(cut_off_date) from aux.stock_dim_wh_stock_item_batch_hist_cut_off
	-- insert on aux.stock_dim_wh_stock_item_batch_hist_cut_off: cut_off_date = convert(date, getutcdate() - 2), prev_cut_off_date = @prev_cut_off_date

	-- Update update_type flag on aux.stock_dim_wh_stock_item_batch_hist_info
		-- WSIB from Physically Deleted IRL: D flag 
		-- Issue sync was delayed: D flag 
		-- Cancelled issues: D flag 
		-- Special batches on aux.stock_dim_wh_stock_item_batch_hist_info_del: S flag
		
		-- Update Type not in ('D', 'T', 'S') + last_stock_date = @prev_cut_off_date: I flag 

	-- Call to lnd_stg_get_aux_stock_wh_stock_item_batch_hist

	-- Return data from aux.stock_dim_wh_stock_item_batch_hist

	-- Update aux.stock_dim_wh_stock_item_batch_hist_cut_off: num_rows, num_batches, lnd_stg_f

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist
	
	-- Call to lnd_stg_get_aux_stock_wh_stock_item_batch_hist_incr
	-- Call to lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist_incr

	-- @cut_off_date = cut_off_date from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off

	-- #t0: distinct warehousestockitembatchid_bk from aux.stock_dim_wh_stock_item_batch_hist_info where wsibh.update_type = 'I'

	-- #t1: List of batches + days to consider (Landing.aux.dim_calendar where calendar_date <= @cut_off_date // on convert(date, wsibh.last_stock_date) < c.calendar_date

	-- #t2: +,- transactions from day to consider (min(calendar_date) min_calendar_date // oli.issue_date > wsib.min_calendar_date // wsibm.batch_movement_date > wsib.min_calendar_date)
	-- #t2b: same but for WSIB related to many invoices

	-- insert into aux.stock_dim_wh_stock_item_batch_hist: #t1 left join #t2 on wsib + date // qty_hist = wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib order by wsib.calendar_date) // where st.qty_hist <> 0

	-- Update aux.stock_dim_wh_stock_item_batch_hist_info
		-- last_qty_on_hand, last_stock_date, update_type: if wsib, irl in aux.stock_dim_wh_stock_item_batch_hist (batch still to be consumed)
		-- update_type = 'N': not in aux.stock_dim_wh_stock_item_batch_hist and update_type = 'I' (batch fully consumed)

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full

	-- @cut_off_date = cut_off_date from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off

	-- #t0: distinct warehousestockitembatchid_bk from aux.stock_dim_wh_stock_item_batch_hist_info where wsibh.update_type in ('F', 'D', 'S')

	-- #t1: List of batches + days to consider (Landing.aux.dim_calendar where calendar_date <= @cut_off_date // on convert(date, wsibh.batch_stock_register_date) < c.calendar_date

	-- #t2: +,- transactions from day to consider no filter on oli.issue_date, wsibm.batch_movement_date: all transactions taken
	-- #t2b: same but for WSIB related to many invoices

	-- insert into aux.stock_dim_wh_stock_item_batch_hist: #t1 left join #t2 on wsib + date // qty_hist = wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib order by wsib.calendar_date) // where st.qty_hist <> 0

	-- Update aux.stock_dim_wh_stock_item_batch_hist_info
		-- last_qty_on_hand, last_stock_date, update_type: if wsib, irl in aux.stock_dim_wh_stock_item_batch_hist (batch still to be consumed) and update_type in ('F', 'S')
		-- update_type = 'N': not in aux.stock_dim_wh_stock_item_batch_hist and update_type = 'I' (batch fully consumed) and update_type in ('F', 'S')

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

-- lnd_stg_get_stock_wh_stock_item_batch_hist_full

	-- @prev_cut_off_date = max(cut_off_date) from aux.stock_dim_wh_stock_item_batch_hist_cut_off_full
	-- insert on aux.stock_dim_wh_stock_item_batch_hist_cut_off_full: cut_off_date = convert(date, getutcdate() - 2), prev_cut_off_date = @prev_cut_off_date

	-- Call to lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full_load
	
	-- Update aux.stock_dim_wh_stock_item_batch_hist_cut_off_full: num_rows, num_batches, lnd_stg_f

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full_load

	-- truncate Warehouse.stock.dim_wh_stock_item_batch_hist_full

	-- Cursor for iterate on aux.stock_dim_wh_stock_item_batch_hist_initial_products
		-- Call to lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full_load_full

	-- insert on Warehouse.stock.dim_wh_stock_item_batch_hist_full

-- lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full_load_full

	-- @cut_off_date = cut_off_date from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off_full

	-- #t0: distinct warehousestockitembatchid_bk from aux.stock_dim_wh_stock_item_batch_hist_info where wsibh.update_type not in ('T')

	-- #t1: List of batches + days to consider (Landing.aux.dim_calendar where calendar_date <= @cut_off_date // on convert(date, wsibh.batch_stock_register_date) < c.calendar_date

	-- #t2: +,- transactions from day to consider no filter on oli.issue_date, wsibm.batch_movement_date: all transactions taken
	-- #t2b: same but for WSIB related to many invoices

	-- insert into aux.stock_dim_wh_stock_item_batch_hist_full: #t1 left join #t2 on wsib + date // qty_hist = wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib order by wsib.calendar_date) // where st.qty_hist <> 0

