
select top 1000 count(*) over (), 
	wsibm.idWHStockItemBatchMovement_sk, wsibm.batchstockmovementid_bk, 
	wsibm.batch_movement_date, wsib.batch_stock_register_date,
	wsibm.warehouse_name, wsibm.product_id_magento, wsibm.product_family_name, wsibm.packsize, wsibm.batch_id,
	wsibm.move_id, wsibm.move_line_id, wsibm.stock_adjustment_type_name, wsibm.stock_method_type_name, wsibm.qty_movement, wsibm.local_product_unit_cost
from 
		Warehouse.stock.fact_wh_stock_item_batch_movement_v wsibm
	inner join
		Warehouse.stock.dim_wh_stock_item_batch wsib on wsibm.idWHStockItemBatch_sk = wsib.idWHStockItemBatch_sk
where convert(date, wsibm.batch_movement_date) < convert(date, wsib.batch_stock_register_date)