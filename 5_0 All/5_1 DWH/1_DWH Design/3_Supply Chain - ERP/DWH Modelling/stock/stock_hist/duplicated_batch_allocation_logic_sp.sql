
drop table #t0
drop table #t1
drop table #t2
drop table #t2b

		select distinct wsibh.warehousestockitembatchid_bk
		into #t0
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
		where wsibh.magentoProductID_int = 1099

		select wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.batch_stock_register_date, 
			c.calendar_date,
			wsibh.qty_received, 
			wsibh.num_rep, wsibh.ord_rep
		into #t1
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
			inner join
				#t0 wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Landing.aux.dim_calendar
				where calendar_date <= getutcdate()-2) c on convert(date, wsibh.batch_stock_register_date) <= c.calendar_date -- we use batch_stock_register_date // -2 days cut off (restore DB at 9 pm)


		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2
		from
				(select wsib.warehousestockitembatchid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep = 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date



		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.invoicelineid_bk is not null) then i.invoicelineid_bk else wsibm.invoicelineid_bk end invoicelineid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2b
		from
				(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep <> 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk and oli.invoicelineid_bk = wsib.invoicelineid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep <> 1 and ord_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date


			select top 1000 
				st.warehousestockitembatchid_bk, st.invoicelineid_bk, CONVERT(INT, (CONVERT(VARCHAR(8), st.calendar_date, 112))) idCalendarStockDate, 
				wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
				st.qty_hist, wsibh.delete_f
			from
					(select st.warehousestockitembatchid_bk, st.invoicelineid_bk, st.calendar_date, st.qty_hist
					from
							(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk,
								wsib.calendar_date, 
								wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.warehousestockitembatchid_bk order by wsib.calendar_date) qty_hist
							from
								#t1 wsib
							left join
								#t2 t on wsib.warehousestockitembatchid_bk = t.warehousestockitembatchid_bk and wsib.calendar_date = t.trans_date
							where wsib.num_rep = 1) st
					where st.qty_hist <> 0) st
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on st.warehousestockitembatchid_bk = wsibh.warehousestockitembatchid_bk and 
						st.invoicelineid_bk = wsibh.invoicelineid_bk

			select 
				st.warehousestockitembatchid_bk, st.invoicelineid_bk, CONVERT(INT, (CONVERT(VARCHAR(8), st.calendar_date, 112))) idCalendarStockDate, 
				wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
				st.qty_hist, wsibh.delete_f
			from
					(select st.warehousestockitembatchid_bk, st.invoicelineid_bk, st.calendar_date, st.qty_hist
					from
							(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk,
								wsib.calendar_date, 
								wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk order by wsib.calendar_date) qty_hist
							from
								#t1 wsib
							left join
								#t2b t on wsib.warehousestockitembatchid_bk = t.warehousestockitembatchid_bk and wsib.invoicelineid_bk = t.invoicelineid_bk 
									and wsib.calendar_date = t.trans_date
							where wsib.num_rep <> 1) st
					where st.qty_hist <> 0) st
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on st.warehousestockitembatchid_bk = wsibh.warehousestockitembatchid_bk and 
						st.invoicelineid_bk = wsibh.invoicelineid_bk


select *
from #t2b
order by warehousestockitembatchid_bk, invoicelineid_bk, trans_date