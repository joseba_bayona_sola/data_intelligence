
-- 43910096367273985: Receipt NO IRL - Discount not applied (before application time) / No Conv change needed
-- 43910096368657033: Receipt NO IRL - Discount applied  / No Conv change needed
-- 43910096368791233: Receipt NO IRL - Discount applied  / Conv change needed
-- 43910096369010944: Receipt IRL (Posted Date Logic applied) - Discount not applied (No JJ) / Conv change needed
-- 43910096369235207: Receipt IRL (Posted Date Logic applied No needed) - Discount applied / Conv change needed
select top 1000 *
from Warehouse.tableau.stock_report_hist_ds_v
where warehousestockitembatchid_bk in (43910096369010944) -- 43910096367273985, 43910096368657033, 43910096369010944, 43910096368985042
order by warehousestockitembatchid_bk, stock_date

select top 1000 *
from Warehouse.tableau.stock_report_hist_ds_v
where warehousestockitembatchid_bk in (43910096369235207) -- 43910096367273985, 43910096368657033, 43910096369010944, 43910096368985042
order by warehousestockitembatchid_bk, stock_date
