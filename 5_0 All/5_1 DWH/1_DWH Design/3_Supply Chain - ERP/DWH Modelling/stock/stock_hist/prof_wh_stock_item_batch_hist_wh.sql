

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist
where idETLBatchRun_upd is not null
order by idWHStockItemBatch_sk_fk, idCalendarStockDate_sk_fk

	--119.972.495
	select top 1000 count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist

	select idETLBatchRun_ins, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	-----------------------------------------

	select idCalendarStockDate_sk_fk, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist
	where idETLBatchRun_upd = 3017
	group by idCalendarStockDate_sk_fk
	order by idCalendarStockDate_sk_fk

	select top 1000 *
	from Warehouse.stock.dim_wh_stock_item_batch_hist_v
	where idWHStockItemBatchHist_sk in 
		(select idWHStockItemBatchHist_sk
		from Warehouse.stock.dim_wh_stock_item_batch_hist
		where idETLBatchRun_upd = 3017)
	order by sku, stock_date

	select top 1000 *
	from Warehouse.stock.dim_wh_stock_item_batch_hist_v
	where warehousestockitembatchid_bk = 43910096368895396
	order by sku, stock_date

	--------------------------------------------

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
order by qty_on_hand desc

	select top 1000 product_id_magento, product_family_name, count(*), min(stock_date), max(stock_date)
	from Warehouse.stock.dim_wh_stock_item_batch_hist_v
	group by product_id_magento, product_family_name
	order by product_id_magento, product_family_name

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_del
order by idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk

	select top 1000 count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist_del

	select idETLBatchRun_ins, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist_del
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_hist_del
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	-----------------------------------------
