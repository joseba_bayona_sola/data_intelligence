
-- Warehouse
	-- Tables
		-- dim_wh_stock_item_batch_hist_full
		-- dim_wh_stock_item_batch_hist_full_wrk (NO)

		-- dim_intransit_stock_item_batch_hist_full
		-- dim_intransit_stock_item_batch_hist_full_wrk (NO)
		
	-- Merge SP: (NO) (No needed? Truncate Normal Table + Insert in Normal Table)

	-- Views
		-- dim_wh_stock_item_batch_hist_full_v
		-- dim_intransit_stock_item_batch_hist_full_v
		
	-- Tableau Views
		-- stock_report_ds_v
	
-- Staging

	-- Tables
		-- dim_wh_stock_item_batch_hist_full (NO)
		-- dim_intransit_stock_item_batch_hist_full (NO)
		
	-- Get SP (NO)
		
-- Landing

	-- Aux Tables (For returning the data)
		-- aux.stock_dim_wh_stock_item_batch_hist_full
		-- aux.stock_dim_intransit_stock_item_batch_hist_full
		
		-- aux.stock_dim_wh_stock_item_batch_hist_cut_off_full 
		-- aux.stock_dim_intransit_stock_item_batch_hist_cut_off_full 
		
	-- Get SP: 
		-- New SPs: lnd_stg_get_stock_wh_stock_item_batch_hist_full, lnd_stg_get_stock_intransit_stock_item_batch_hist_full
		-- New SP: lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full (similar to initial) - 
			-- Insert directly to Warehouse Table ?? (No need of Staging Tables, LND_STG, STG_DWH packages ??)
		-- New SP: lnd_stg_get_aux_stock_wh_stock_item_batch_hist_full_full
			-- Full Load per Product on join with stock_dim_wh_stock_item_batch_hist_initial_products (change per update_num than product)
			-- When inserting on stock_dim_wh_stock_item_batch_hist_full: Filter only on last days of the month
	
		-- Try SP logic for only inserting at EOM days with just one batch
		
-- SSIS	
	
	-- VisionDirectDataWarehouseLoadERPStockHist 
		-- (Existing package for calling:  I-Initial, N-Normal // Add F-Full )
		-- Calls Info SP
		-- In I-Initial: Calls initial_products table + Loop 
		
		-- New Flag=F - New container (Similar to I)
			-- Call to SP for update stock_dim_wh_stock_item_batch_hist_initial_products
			-- Call to get stock_dim_wh_stock_item_batch_hist_initial_products - update_num
			-- ForEach (No need?? For Each done in SP)
				-- Call to SP Instead of Package: Calculates per update_num + write to aux table + at then end insert in Warehouse
	
-- Running Way

	-- Some SP for updating stock_dim_wh_stock_item_batch_hist_initial_products (Code in DATA_1026 5_landing_aux_db.sql)
	
	
-- To DO: 
	-- Check on SQL the running times of previous initial load
	-- Check how was done for Intransit
	
	-- How info, alloc_invrec are used + Calls to SPs