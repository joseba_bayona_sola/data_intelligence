
		select distinct wsibh.warehousestockitembatchid_bk
		into #t0
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
		-- where wsibh.update_type in ('D') 
		where wsibh.warehousestockitembatchid_bk = 43910096369461770

		select wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.batch_stock_register_date, 
			c.calendar_date,
			wsibh.qty_received, 
			wsibh.num_rep, wsibh.ord_rep
		into #t1
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
			inner join
				#t0 wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Landing.aux.dim_calendar
				where calendar_date <= convert(date, getutcdate() -2)) c on convert(date, wsibh.batch_stock_register_date) <= c.calendar_date -- we use batch_stock_register_date // -2 days cut off (restore DB at 9 pm)
		-- where wsibh.update_type in ('D')
		where wsibh.update_type not in ('T')

		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2
		from
				(select wsib.warehousestockitembatchid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep = 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date

		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.invoicelineid_bk is not null) then i.invoicelineid_bk else wsibm.invoicelineid_bk end invoicelineid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2b
		from
				(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep <> 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk and oli.invoicelineid_bk = wsib.invoicelineid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep <> 1 and ord_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date

		select wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.idCalendarStockDate, 
			wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
			wsibh.qty_hist, wsibh.delete_f
		from
			(select 
				st.warehousestockitembatchid_bk, st.invoicelineid_bk, CONVERT(INT, (CONVERT(VARCHAR(8), st.calendar_date, 112))) idCalendarStockDate, 
				wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
				st.qty_hist, wsibh.delete_f
			from
					(select st.warehousestockitembatchid_bk, st.invoicelineid_bk, st.calendar_date, st.qty_hist
					from
							(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk,
								wsib.calendar_date, 
								wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.warehousestockitembatchid_bk order by wsib.calendar_date) qty_hist
							from
								#t1 wsib
							left join
								#t2 t on wsib.warehousestockitembatchid_bk = t.warehousestockitembatchid_bk and wsib.calendar_date = t.trans_date
							where wsib.num_rep = 1
								-- and wsib.warehousestockitembatchid_bk = 43910096368070673
								) st
					where st.qty_hist <> 0) st
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on st.warehousestockitembatchid_bk = wsibh.warehousestockitembatchid_bk and 
						st.invoicelineid_bk = wsibh.invoicelineid_bk) wsibh
		inner join
			(select *
			from
				(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
					count(*) over (partition by year_num, month_num) num_rep, 
					dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_rep
				from Landing.aux.dim_calendar
				) c
			where num_rep = ord_rep) c on wsibh.idCalendarStockDate = c.idCalendar_sk


select *, sum(qty) over ()
from #t2
where trans_date <= '2019-07-31'
order by trans_date

-------------------------------------------------
-------------------------------------------------

select batchstockissueid_bk, issue_id, issue_date, warehousestockitembatchid_bk, qty_stock, ins_ts, upd_ts
into #issue_cancelled
from Landing.aux.alloc_fact_order_line_erp_issue_aud
where cancelled_f = 'Y'

	select *, datediff(dd, issue_date, upd_ts)
	from #issue_cancelled
	order by issue_date desc

	select distinct warehousestockitembatchid_bk
	from Landing.aux.alloc_fact_order_line_erp_issue_aud
	where cancelled_f = 'Y' and year(issue_date) >= 2019


select batchstockissueid_bk, issue_id, issue_date, cancelled_f, warehousestockitembatchid_bk, qty_stock, ins_ts, upd_ts, 
	sum(qty_stock) over (partition by cancelled_f)
from Landing.aux.alloc_fact_order_line_erp_issue_aud
where warehousestockitembatchid_bk = 43910096369338958
	-- and convert(date, issue_date) <= convert(date, getutcdate()-2)
	and convert(date, issue_date) <= '2019-07-31'
order by issue_date

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v
where warehousestockitembatchid_bk = 43910096369338958

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
where warehousestockitembatchid_bk = 43910096369338958
order by stock_date desc

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_full
where warehousestockitembatchid_bk = 43910096369338958

------------------------------------------------

drop table #wsibh_1
drop table #wsibh_2

select wsib.warehousestockitembatchid_bk, isnull(irl.invoicelineid_bk, wsibh.idInvoiceReconciliationLine_sk_fk) invoicelineid_bk, 
	wsibh.idCalendarStockDate_sk_fk, wsibh.qty_on_hand, wsibh.delete_f
into #wsibh_1
from 
		Warehouse.stock.dim_wh_stock_item_batch_hist wsibh
	inner join
		Warehouse.stock.dim_wh_stock_item_batch wsib on wsibh.idWHStockItemBatch_sk_fk = wsib.idWHStockItemBatch_sk
	left join
		Warehouse.invrec.fact_invoice_reconciliation_line irl on wsibh.idInvoiceReconciliationLine_sk_fk = irl.idInvoiceReconciliationLine_sk
where wsibh.idCalendarStockDate_sk_fk = 20190731

select warehousestockitembatchid_bk, invoicelineid_bk, qty_on_hand, delete_f
into #wsibh_2
from Landing.aux.stock_dim_wh_stock_item_batch_hist_full
where idCalendarStockDate = 20190731

select top 1000 isnull(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) warehousestockitembatchid_bk, 
	isnull(t1.invoicelineid_bk, t2.invoicelineid_bk) invoicelineid_bk, wsib.batch_id,
	t1.qty_on_hand, t2.qty_on_hand, t1.delete_f, t2.delete_f
from 
		#wsibh_1 t1
	full join
		#wsibh_2 t2 on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk
	inner join
		Warehouse.stock.dim_wh_stock_item_batch wsib on isnull(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) = wsib.warehousestockitembatchid_bk
-- where t1.warehousestockitembatchid_bk is null -- 6 rows (all negative)
-- where t2.warehousestockitembatchid_bk is null -- 25 rows (most negative)
where t1.qty_on_hand <> t2.qty_on_hand -- 5 rows
