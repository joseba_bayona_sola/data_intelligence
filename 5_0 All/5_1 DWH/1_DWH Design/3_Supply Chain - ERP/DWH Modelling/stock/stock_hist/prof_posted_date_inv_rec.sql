
drop table #stock_report_inv_posted_date

	select 
		wsibh.warehousestockitembatchid_bk, 
		'Receipt' transaction_type,
		wsibh.warehouse_name, 
		wsibh.manufacturer_name, 
		wsibh.product_type_name, wsibh.category_name, wsibh.product_family_group_name, wsibh.product_id_magento, wsibh.product_family_name, wsibh.packsize, 
		wsibh.SKU, wsibh.stock_item_description, 
		wsibh.batch_id, wsibh.stock_date,
		rl.supplier_type_name, rl.supplier_name,
		rl.receipt_number, rl.wh_shipment_type_name, rl.receipt_status_name, 
		rl.purchase_order_number, rl.po_source_name, rl.po_type_name, rl.po_status_name, 
		irl.net_suite_no, irl.posted_date invoice_posted_date, 
		null move_id, null movement_reason, null movement_comment,
		null intersite_order_no,

		wsibh.qty_on_hand,  
		
		wsibh.batch_stock_register_date,

		case when (wsibh.stock_date >= convert(date, irl.posted_date) or c.calendar_date is not null) then irl.local_adjusted_unit_cost else isnull (rl.local_invoice_unit_cost, rl.local_unit_cost) end local_unit_cost,
		case when (wsibh.stock_date >= convert(date, irl.posted_date) or c.calendar_date is not null) then irl.global_adjusted_unit_cost else isnull(rl.global_invoice_unit_cost, rl.global_unit_cost) end global_unit_cost, 
		
		case when (wsibh.stock_date >= convert(date, irl.posted_date) or c.calendar_date is not null) then irl.local_adjusted_unit_cost * convert(decimal(12, 4), er.exchange_rate) 
			else isnull(rl.local_invoice_unit_cost, rl.local_unit_cost) * convert(decimal(12, 4), er.exchange_rate) end global_unit_cost_new, 

		wsibh.local_to_global_rate, wsibh.currency_code, convert(decimal(12, 4), er.exchange_rate) local_to_global_rate_new
	into #stock_report_inv_posted_date
	from 
			Warehouse.stock.dim_wh_stock_item_batch_hist_v wsibh
		inner join
			Warehouse.rec.fact_receipt_line_v rl on wsibh.idReceiptLine_sk_fk = rl.idReceiptLine_sk
		inner join
			Warehouse.invrec.fact_invoice_reconciliation_line_v irl on wsibh.idInvoiceReconciliationLine_sk_fk = irl.idInvoiceReconciliationLine_sk
		left join
			(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
				dateadd(dd, calendar_date_num * -1, calendar_date) calendar_date_apply_from
			from
				(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
					dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
				from Warehouse.gen.dim_calendar
				where year_num = 2019
					and calendar_date_week_name not in ('Saturday', 'Sunday')) c
			where c.ord_calendar_date_num in (1, 2)) c on convert(date, irl.posted_date) = c.calendar_date and wsibh.stock_date between c.calendar_date_apply_from and c.calendar_date
		inner join
			Landing.mend.gen_comp_currency_exchange_v er on wsibh.stock_date > er.effectivedate and wsibh.stock_date <= er.nexteffectivedate and
				er.currencycode_from = wsibh.currency_code and er.currencycode_to = 'GBP'			
	-- where irl.idInvoiceReconciliationLine_sk in (39397, 165624)
	where warehousestockitembatchid_bk = 43910096369010944
	order by warehousestockitembatchid_bk, stock_date

	select warehousestockitembatchid_bk, product_id_magento, product_family_name, packsize, sku, 
		batch_id, stock_date, net_suite_no, invoice_posted_date, qty_on_hand, local_unit_cost, global_unit_cost, global_unit_cost_new, 
		local_to_global_rate, local_to_global_rate_new
	from #stock_report_inv_posted_date
	order by stock_date

	select sr.warehousestockitembatchid_bk, sr.product_id_magento, sr.product_family_name, sr.packsize, sr.sku, 
		sr.batch_id, sr.stock_date, sr.net_suite_no, sr.invoice_posted_date, sr.qty_on_hand, sr.local_unit_cost, sr.global_unit_cost, 
		c.calendar_date_apply_from, c.calendar_date
	from 
		#stock_report_inv_posted_date sr
	left join
		(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
			dateadd(dd, calendar_date_num * -1, calendar_date) calendar_date_apply_from
		from
			(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
				dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
			from Warehouse.gen.dim_calendar
			where year_num = 2019
				and calendar_date_week_name not in ('Saturday', 'Sunday')) c
		where c.ord_calendar_date_num in (1, 2)) c on convert(date, sr.invoice_posted_date) = c.calendar_date and sr.stock_date between c.calendar_date_apply_from and c.calendar_date
	order by sr.stock_date


	--- TO DO: Why after posted date local is more expensive and global cheaper --> IRL Currencies for local, global

		-- 0.8812 (stock_register_date - 2019-01-21 13:22:10.243)
		select top 1000 *
		from Warehouse.rec.fact_receipt_line_v
		where idReceiptLine_sk = 2551455

		-- 0.8673 (invoice_date - 2019-01-31 00:00:00.000)
		select top 1000 *
		from Warehouse.invrec.fact_invoice_reconciliation_line_v
		where idReceiptLine_sk_fk = 2551455
		order by posted_date 

		SELECT 33.16000000 * 0.8812

		SELECT 33.49333333 * 0.8673
		SELECT 33.49333333 * 0.8812

		select top 1000 *
		from Warehouse.stock.dim_wh_stock_item_batch_hist_v
		where warehousestockitembatchid_bk = 43910096369010944 

		

----------------------------------------------

-- The second Working day per Month (to know days until invoice posted is possible) + the day where we wanted to apply
select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
	dateadd(dd, calendar_date_num * -1, calendar_date) calendar_date_apply_from
from
	(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
		dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
	from Warehouse.gen.dim_calendar
	where year_num > 2018
		and calendar_date_week_name not in ('Saturday', 'Sunday')) c
where c.ord_calendar_date_num in (2)


-- 2 attributes // how to join with calendar idCalendar vs date
	-- a1: possible days for allowing invoice
	-- a2: days to apply invoice posted_date if a1 ok

select c1.idCalendar_sk, c1.calendar_date, c1.calendar_date_num, c1.calendar_date_week_name, c1.month_name, c1.month_num, c1.year_num, 
	case when (c2.idCalendar_sk is not null) then dateadd(dd, c1.calendar_date_num * -1, c1.calendar_date) else null end,
	case when (c2.idCalendar_sk is not null) then 'Y' else 'N' end valid_for_invoice_posted
from 
	Warehouse.gen.dim_calendar c1
left join
		(select *
		from
			(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
				dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
			from Warehouse.gen.dim_calendar
			where year_num > 2018
				and calendar_date_week_name not in ('Saturday', 'Sunday')) c
		where c.ord_calendar_date_num in (1, 2)) c2 on c1.idCalendar_sk = c2.idCalendar_sk
where c1.year_num = 2019
order by c1.calendar_date
