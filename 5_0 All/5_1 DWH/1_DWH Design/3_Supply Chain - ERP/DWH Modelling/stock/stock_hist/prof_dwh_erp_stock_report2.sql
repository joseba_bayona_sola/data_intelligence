
-- Amsterdam: 
	select 'Stock On Hand', 394660 - 394536
	select 'Intransit', 42790 - 42618
	select 'Total', 437450 - 437154

-- Girona: 
	select 'Stock On Hand', 252965 - 252916
	select 'Intransit', 6102 - 5692
	select 'Total', 259067- 258608

-- York: 
	select 'Stock On Hand', 896784 - 896898
	select 'Intransit', 22653 - 22171
	select 'Total', 919437 - 919069

select 30 - 1 + 102 - 14

select 10 - 0 + 43 - 4

select 127 - 9 + 549 - 820


-------------------------------------------


alter table DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_v2_dwh add unit_qty_int int

update DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_v2_dwh
set unit_qty_int = convert(int, replace(unit_qty, ',', ''))

select top 1000 warehouse, product_id_magento, product_family_name, sku, unit_qty, unit_qty_int
from DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_v2_dwh
where sku = '01083B2D4AS0000000000301'


-------------------------------------------

select top 1000 
	count(*) over (), 
	sum(dwh.unit_qty_int) over (), 
	sum(erp.unit_qty_int) over (),

	dwh.warehouse, erp.warehouse, 
	dwh.product_id_magento, erp.product_id_magento, dwh.product_family_name, erp.product_family_name, dwh.sku, erp.sku, 
	dwh.unit_qty_int unit_qty_dwh, erp.unit_qty_int unit_qty_erp
from 
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_v2_dwh dwh
	full join
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp erp on dwh.warehouse = erp.warehouse and dwh.sku = erp.sku
where erp.warehouse = 'York' -- Amsterdam (1) - Girona (0) - York (9)
 	and dwh.warehouse is null 

-- where dwh.warehouse = 'York' -- Amsterdam (30) - Girona (10) - York (127)
-- 	and erp.warehouse is null 

order by dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku


-----------

select top 1000 
	count(*) over (), 

	sum(case when (dwh.unit_qty_int - erp.unit_qty_int) > 0 then dwh.unit_qty_int - erp.unit_qty_int else 0 end) over (), 
	sum(case when (erp.unit_qty_int - dwh.unit_qty_int) > 0 then erp.unit_qty_int - dwh.unit_qty_int else 0 end) over (),

	dwh.warehouse, erp.warehouse, 
	dwh.product_id_magento, erp.product_id_magento, dwh.product_family_name, erp.product_family_name, dwh.sku, erp.sku, 
	dwh.unit_qty_int unit_qty_dwh, erp.unit_qty_int unit_qty_erp, 
	case when (dwh.unit_qty_int - erp.unit_qty_int) > 0 then dwh.unit_qty_int - erp.unit_qty_int else 0 end diff_pos_dwh, 
	case when (erp.unit_qty_int - dwh.unit_qty_int) > 0 then erp.unit_qty_int - dwh.unit_qty_int else 0 end diff_pos_erp

from 
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_v2_dwh dwh
	full join
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp erp on dwh.warehouse = erp.warehouse and dwh.sku = erp.sku

where dwh.warehouse = 'York' -- -- Amsterdam (102 / 14) - Girona (43 / 4) - York (549 / 820)
	and dwh.warehouse is not null and erp.warehouse is not null and dwh.unit_qty_int <> erp.unit_qty_int

-- order by dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku
order by diff_pos_dwh desc, diff_pos_erp desc, dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku

---------------------------------------------------

select wsib.*
from
		(select *, datediff(dd, createddate, shippeddate) diff
		from Landing.mend.wh_batchstockissue_aud
		where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1) bsi
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsib on bsi.id = wsib.batchstockissueid
where wsib.batch_id = 2180243

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where batch_id = 2180243
order by issue_date