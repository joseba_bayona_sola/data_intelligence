
	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_info

	select top 1000 count(*) over (),
		warehousestockitembatchid_bk, invoicelineid_bk, 
		receiptlineid_bk, batchstockmovementid_bk, 
		batch_stock_register_date, invoice_posted_date,
		qty_received,
		delete_f, update_type, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where warehousestockitembatchid_bk in (43910096369503071, 43910096369550901) -- and invoicelineid_bk = -1
	order by warehousestockitembatchid_bk, invoicelineid_bk

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info

	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by warehousestockitembatchid_bk, invoicelineid_bk
	order by count(*) desc

	select idETLBatchRun_ins, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins desc

	select idETLBatchRun_upd, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	select delete_f, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by delete_f
	order by delete_f

	select update_type, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	-- where invoicelineid_bk <> -1
	group by update_type
	order by update_type		

	select last_stock_date, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	group by last_stock_date
	order by last_stock_date desc

	select last_stock_date, update_type, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where last_stock_date >= '2020-01-01'
	group by last_stock_date, update_type
	order by last_stock_date desc, update_type

	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_info_hist
	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, 
		receiptlineid_bk, batchstockmovementid_bk, 
		batch_stock_register_date, invoice_posted_date,
		qty_received,
		delete_f, update_type, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts, 
		aud_type, aud_dateFrom, aud_dateTo
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist
	where warehousestockitembatchid_bk in (43910096369503071, 43910096369550901) -- and invoicelineid_bk = -1 
	order by warehousestockitembatchid_bk, invoicelineid_bk, ins_ts

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist

	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist
	group by warehousestockitembatchid_bk, invoicelineid_bk
	order by count(*) desc


	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_info_hist_last
	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	where warehousestockitembatchid_bk in (43910096369503071, 43910096369550901)
	order by warehousestockitembatchid_bk, invoicelineid_bk, convert(date, ins_ts) desc

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last

	select convert(date, ins_ts), count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by convert(date, ins_ts)
	order by convert(date, ins_ts) desc

	select convert(date, ins_ts), last_stock_date, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
	group by convert(date, ins_ts), last_stock_date
	order by convert(date, ins_ts) desc, last_stock_date

	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist
	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, 
		receiptlineid_bk, batchstockmovementid_bk, 
		qty_on_hand, delete_f
	from Landing.aux.stock_dim_wh_stock_item_batch_hist
	where warehousestockitembatchid_bk = 43910096367583981 and invoicelineid_bk = -1
	order by idCalendarStockDate desc

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist

	select top 1000 warehousestockitembatchid_bk, invoicelineid_bk, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist
	group by warehousestockitembatchid_bk, invoicelineid_bk
	order by count(*) desc

	select top 1000 idCalendarStockDate, count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist
	group by idCalendarStockDate
	order by idCalendarStockDate desc
	 
	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_cut_off
	select idETLBatchRun, cut_off_date, prev_cut_off_date, num_rows, num_batches, lnd_stg_f, stg_dwh_f
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
	order by idETLBatchRun desc

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off


	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_initial_products
	select product_id_magento, name, num_tot, update_num
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products
	order by num_tot desc

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products


	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_info_del
	select warehousestockitembatchid_bk
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_del

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_del
	


	--------------------------------------------------
	-- stock_dim_wh_stock_item_batch_hist_cut_off_full
	select idETLBatchRun, cut_off_date, prev_cut_off_date, num_rows, num_batches, lnd_stg_f
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off_full
	order by idETLBatchRun desc

	select count(*)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off_full