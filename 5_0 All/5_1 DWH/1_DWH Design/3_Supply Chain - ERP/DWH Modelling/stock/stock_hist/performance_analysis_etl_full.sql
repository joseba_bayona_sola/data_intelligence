
drop table DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist

create table DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist(
	warehousestockitembatchid_bk		bigint NOT NULL,
	invoicelineid_bk					bigint NOT NULL, 
	idCalendarStockDate					int NOT NULL, 

	receiptlineid_bk					bigint,
	batchstockmovementid_bk				bigint,

	qty_on_hand							decimal(28, 8), 

	delete_f							char(1));
go

alter table DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist add constraint [PK_stock_stock_dim_wh_stock_item_batch_hist]
	primary key clustered (warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate);
go

-- 

create table DW_GetLenses_jbs.dbo.dim_wh_stock_item_batch_hist(
	idWHStockItemBatchHist_sk			bigint NOT NULL, 

	idWHStockItemBatch_sk_fk			int	NOT NULL, 
	idInvoiceReconciliationLine_sk_fk	int NOT NULL, 
	idCalendarStockDate_sk_fk			int NOT NULL, 
	
	idReceiptLine_sk_fk					int,
	idWHStockItemBatchMovement_sk_fk	int,

	qty_on_hand							decimal(28, 8), 

	delete_f							char(1));
go 

alter table DW_GetLenses_jbs.dbo.dim_wh_stock_item_batch_hist add constraint [PK_stock_dim_wh_stock_item_batch_hist]
	primary key clustered (idWHStockItemBatchHist_sk);
go

alter table DW_GetLenses_jbs.dbo.dim_wh_stock_item_batch_hist add constraint [UNIQ_stock_dim_wh_stock_item_batch_hist_comb]
	unique (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk);
go




	update trg
	set trg.update_type = 'S'
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info trg
	where trg.last_stock_date = convert(date, getutcdate() -2) and trg.update_type not in ('D', 'T')

		select distinct wsibh.warehousestockitembatchid_bk
		into #t0
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
		where wsibh.update_type in ('S') 

		-- 48.948.536 - 3 min
		select wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.batch_stock_register_date, 
			c.calendar_date,
			wsibh.qty_received, 
			wsibh.num_rep, wsibh.ord_rep
		into #t1
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
			inner join
				#t0 wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Landing.aux.dim_calendar
				where calendar_date <= convert(date, getutcdate() -2)) c on convert(date, wsibh.batch_stock_register_date) <= c.calendar_date -- we use batch_stock_register_date // -2 days cut off (restore DB at 9 pm)
		where wsibh.update_type in ('S')

			--select warehousestockitembatchid_bk, invoicelineid_bk, count(*), count(*) over ()
			--from #t1
			--group by warehousestockitembatchid_bk, invoicelineid_bk
			--order by warehousestockitembatchid_bk, invoicelineid_bk

		-- 180152 - 2 min
		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2
		from
				(select wsib.warehousestockitembatchid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep = 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date

		-- 75 rows
		select
			case when (i.warehousestockitembatchid_bk is not null) then i.warehousestockitembatchid_bk else wsibm.warehousestockitembatchid_bk end warehousestockitembatchid_bk,
			case when (i.invoicelineid_bk is not null) then i.invoicelineid_bk else wsibm.invoicelineid_bk end invoicelineid_bk,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2b
		from
				(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
				from 
						Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oli
					inner join
						(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
						from #t1
						where num_rep <> 1) wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk and oli.invoicelineid_bk = wsib.invoicelineid_bk
				where oli.cancelled_f ='N' 
				group by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, convert(date, issue_date)) i
			full join
				(select warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
				from
					(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk, wsibm.batch_movement_date, 
						case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
					from 
							Landing.aux.mend_stock_batchstockmovement_v wsibm
						inner join
							(select distinct warehousestockitembatchid_bk, invoicelineid_bk, num_rep, ord_rep 
							from #t1
							where num_rep <> 1 and ord_rep = 1) wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
					where (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)) wsibm
				group by warehousestockitembatchid_bk, invoicelineid_bk, convert(date, batch_movement_date)) wsibm on i.warehousestockitembatchid_bk = wsibm.warehousestockitembatchid_bk and i.trans_date = wsibm.trans_date


		-- 48925337 - 6 min
		insert into DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist(warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, 
			receiptlineid_bk, batchstockmovementid_bk, 
			qty_on_hand, delete_f)

			select 
				st.warehousestockitembatchid_bk, st.invoicelineid_bk, CONVERT(INT, (CONVERT(VARCHAR(8), st.calendar_date, 112))) idCalendarStockDate, 
				wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
				st.qty_hist, wsibh.delete_f
			from
					(select st.warehousestockitembatchid_bk, st.invoicelineid_bk, st.calendar_date, st.qty_hist
					from
							(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk,
								wsib.calendar_date, 
								wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.warehousestockitembatchid_bk order by wsib.calendar_date) qty_hist
							from
								#t1 wsib
							left join
								#t2 t on wsib.warehousestockitembatchid_bk = t.warehousestockitembatchid_bk and wsib.calendar_date = t.trans_date
							where wsib.num_rep = 1) st
					where st.qty_hist <> 0) st
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on st.warehousestockitembatchid_bk = wsibh.warehousestockitembatchid_bk and 
						st.invoicelineid_bk = wsibh.invoicelineid_bk


		insert into DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist(warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, 
			receiptlineid_bk, batchstockmovementid_bk, 
			qty_on_hand, delete_f)

			select 
				st.warehousestockitembatchid_bk, st.invoicelineid_bk, CONVERT(INT, (CONVERT(VARCHAR(8), st.calendar_date, 112))) idCalendarStockDate, 
				wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
				st.qty_hist, wsibh.delete_f
			from
					(select st.warehousestockitembatchid_bk, st.invoicelineid_bk, st.calendar_date, st.qty_hist
					from
							(select wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk,
								wsib.calendar_date, 
								wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.warehousestockitembatchid_bk, wsib.invoicelineid_bk order by wsib.calendar_date) qty_hist
							from
								#t1 wsib
							left join
								#t2b t on wsib.warehousestockitembatchid_bk = t.warehousestockitembatchid_bk and wsib.invoicelineid_bk = t.invoicelineid_bk 
									and wsib.calendar_date = t.trans_date
							where wsib.num_rep <> 1) st
					where st.qty_hist <> 0) st
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on st.warehousestockitembatchid_bk = wsibh.warehousestockitembatchid_bk and 
						st.invoicelineid_bk = wsibh.invoicelineid_bk

			drop table #t0
			drop table #t1
			drop table #t2
			drop table #t2b

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

select warehousestockitembatchid_bk, invoicelineid_bk, idCalendarStockDate, 
	receiptlineid_bk, batchstockmovementid_bk, 
	qty_on_hand, delete_f
into #stock_dim_wh_stock_item_batch_hist
from DW_GetLenses_jbs.dbo.stock_dim_wh_stock_item_batch_hist
where idCalendarStockDate = 20190807 

	-- Compare WSIB from Full to Incr + Take Differences
	select top 10000 count(*) over (), t3.update_type,
		coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) warehousestockitembatchid_bk,
		coalesce(t1.invoicelineid_bk, t2.invoicelineid_bk) invoicelineid_bk,
		coalesce(t1.idCalendarStockDate, t2.idCalendarStockDate) idCalendarStockDate,
		coalesce(t1.receiptlineid_bk, t2.receiptlineid_bk) receiptlineid_bk,
		coalesce(t1.batchstockmovementid_bk, t2.batchstockmovementid_bk) batchstockmovementid_bk,

		t1.qty_on_hand, t2.qty_on_hand, t1.qty_on_hand - t2.qty_on_hand diff_qty_on_hand, 
		sum(t2.qty_on_hand) over (),
		sum(t1.qty_on_hand - t2.qty_on_hand) over ()
	from 
			#stock_dim_wh_stock_item_batch_hist t1
		full join
			(select *
			from Staging.stock.dim_wh_stock_item_batch_hist
			where idCalendarSTockDate = 20190807) t2 on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk	
		inner join
			Landing.aux.stock_dim_wh_stock_item_batch_hist_info t3 on 
				coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) = t3.warehousestockitembatchid_bk and
				coalesce(t1.invoicelineid_bk, t2.invoicelineid_bk) = t3.invoicelineid_bk
	-- where t2.warehousestockitembatchid_bk is null
	where t1.warehousestockitembatchid_bk is null and t3.update_type = 'S' -- 1266 / 1001
	-- where t1.qty_on_hand <> t2.qty_on_hand -- 533 WSIB where we didn't process issues from the day ERP didn't restore
	order by diff_qty_on_hand

	select batchstockissueid_bk, issue_id, issue_date, warehousestockitembatchid_bk, qty_stock, 
		idETLBatchRun, ins_ts
	from Landing.aux.alloc_fact_order_line_erp_issue_aud
	where warehousestockitembatchid_bk = 43910096369526855
		and idETLBatchRun = 3731

	-- Reloaded Issues into TEMP
	select batchstockissueid_bk, issue_id, issue_date, warehousestockitembatchid_bk, qty_stock, 
		idETLBatchRun, ins_ts, upd_ts
	into #alloc_fact_order_line_erp_issue_aud
	from Landing.aux.alloc_fact_order_line_erp_issue_aud
	where idETLBatchRun = 3731

	-- Compare WSIB diff between Full and Incr vs Reloaded Issues
	select coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) warehousestockitembatchid_bk, 
		t1.qty, t2.qty
	from
			(select 
				coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) warehousestockitembatchid_bk,
				-- sum(abs(t1.qty_on_hand - t2.qty_on_hand)), sum(t2.qty_on_hand),
				coalesce(sum(abs(t1.qty_on_hand - t2.qty_on_hand)), sum(t2.qty_on_hand)) qty
			from 
					#stock_dim_wh_stock_item_batch_hist t1
				full join
					(select *
					from Staging.stock.dim_wh_stock_item_batch_hist
					where idCalendarSTockDate = 20190807) t2 on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk and t1.invoicelineid_bk = t2.invoicelineid_bk	
				inner join
					Landing.aux.stock_dim_wh_stock_item_batch_hist_info t3 on 
						coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk) = t3.warehousestockitembatchid_bk and
						coalesce(t1.invoicelineid_bk, t2.invoicelineid_bk) = t3.invoicelineid_bk
			where t1.qty_on_hand <> t2.qty_on_hand 
				or (t1.warehousestockitembatchid_bk is null and t3.update_type = 'S')
			group by coalesce(t1.warehousestockitembatchid_bk, t2.warehousestockitembatchid_bk)) t1
		full join
			(select warehousestockitembatchid_bk, sum(qty_stock) qty
			from #alloc_fact_order_line_erp_issue_aud
			group by warehousestockitembatchid_bk) t2 on t1.warehousestockitembatchid_bk = t2.warehousestockitembatchid_bk
	-- where t1.qty <> t2.qty
	-- where t1.warehousestockitembatchid_bk is null -- Issues reload but not affecting ??
	where t2.warehousestockitembatchid_bk is null -- Diff on Numbers between F and I but not Issue affected (Minuses, Cancellations)

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

	-- 30 min
	insert into DW_GetLenses_jbs.dbo.dim_wh_stock_item_batch_hist (idWHStockItemBatchHist_sk, 
		idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, 
		idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
		qty_on_hand, 
		delete_f)

		select idWHStockItemBatchHist_sk, 
			idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, 
			idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
			qty_on_hand, 
			delete_f
		from Warehouse.stock.dim_wh_stock_item_batch_hist