
	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist_info

	select top 1000 
		intransitstockitembatchid_bk, 
		intransit_batch_created_date, 
		qty_received,
		delete_f, update_type, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info

	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist_info_hist

	select intransitstockitembatchid_bk, 
		intransit_batch_created_date, 
		qty_received,
		delete_f, update_type, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts, 
		aud_type, aud_dateFrom, aud_dateTo
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist

	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist_info_hist_last

	select intransitstockitembatchid_bk, 
		last_qty_on_hand, last_stock_date,
		idETLBatchRun_ins, ins_ts
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_hist_last

	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist

	select intransitstockitembatchid_bk, idCalendarStockDate, 
		qty_on_hand, delete_f
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist

	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist_cut_off

	select idETLBatchRun, cut_off_date, prev_cut_off_date, num_rows, num_batches, lnd_stg_f, stg_dwh_f
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_cut_off

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_cut_off

	--------------------------------------------------
	-- stock_dim_intransit_stock_item_batch_hist_info_del

	select intransitstockitembatchid_bk
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_del

	select count(*)
	from Landing.aux.stock_dim_intransit_stock_item_batch_hist_info_del

