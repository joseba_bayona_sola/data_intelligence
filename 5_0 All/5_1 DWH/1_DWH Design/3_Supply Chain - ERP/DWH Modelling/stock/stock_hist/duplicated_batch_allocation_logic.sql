
drop table #wsib_invrec
go
drop table #wsib_issue
go


select ord_rep, warehousestockitembatchid_bk, invoicelineid_bk, batch_id,
	qty_received
into #wsib_invrec
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
where num_rep > 1

select *
from #wsib_invrec
where warehousestockitembatchid_bk = 43910096368992843

select wsibi.batchstockissueid batchstockissueid_bk, 
	wsibi.issueid issue_id, wsibi.issue_date, case when (wsibi.cancelled = 0) then 'N' else 'Y' end cancelled_f, 
	wsibi.warehousestockitembatchid warehousestockitembatchid_bk, 
	wsibi.issuedquantity_issue*-1 qty_stock
into #wsib_issue
from 
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
	inner join
		(select distinct warehousestockitembatchid_bk
		from #wsib_invrec) wsib_ir on wsibi.warehousestockitembatchid = wsib_ir.warehousestockitembatchid_bk
where wsibi.cancelled = 0

select *
from #wsib_issue
where warehousestockitembatchid_bk = 43910096368992843

select wsib_ir.ord_rep, wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_ir.batch_id, wsib_ir.qty_received, 
	wsib_i.batchstockissueid_bk, 
	wsib_i.issue_id, wsib_i.issue_date, wsib_i.cancelled_f, 
	wsib_i.warehousestockitembatchid_bk, 
	wsib_i.qty_stock, 

	wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_ir.invoicelineid_bk) qty_hist
from 
		#wsib_invrec wsib_ir
	left join
		#wsib_issue wsib_i on wsib_ir.warehousestockitembatchid_bk = wsib_i.warehousestockitembatchid_bk
-- where wsib_ir.warehousestockitembatchid_bk = 43910096368992843
-- where wsib_ir.warehousestockitembatchid_bk = 43910096368992820
order by wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_i.issue_date, wsib_ir.ord_rep

-- Temp Table

	create table #alloc_fact_order_line_erp_issue_invrec_aud(
		batchstockissueid_bk						bigint NOT NULL, 

		issue_id									bigint, 

		issue_date									datetime,

		cancelled_f									char(1) NOT NULL, 

		warehousestockitembatchid_bk				bigint NOT NULL, 
		invoicelineid_bk							bigint NOT NULL,

		qty_stock									decimal(28, 8)) 

	insert into #alloc_fact_order_line_erp_issue_invrec_aud (batchstockissueid_bk, 
		issue_id, issue_date, cancelled_f, 
		warehousestockitembatchid_bk, invoicelineid_bk, 
		qty_stock)

	select batchstockissueid_bk, 
		issue_id, issue_date, cancelled_f, 
		warehousestockitembatchid_bk, invoicelineid_bk, 
		case when (flag_sign  = 'P') then (qty_stock * -1) else qty_hist + (qty_stock * -1) end qty_stock
	from
		(select batchstockissueid_bk,
			issue_id, issue_date, cancelled_f,
			warehousestockitembatchid_bk, invoicelineid_bk, 
			qty_stock, qty_hist,
			flag_sign,
			dense_rank() over (partition by warehousestockitembatchid_bk, invoicelineid_bk, flag_sign order by issue_date) flag_sign_num
		from
			(select wsib_ir.ord_rep, wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_ir.batch_id, wsib_ir.qty_received, 
				wsib_i.batchstockissueid_bk, 
				wsib_i.issue_id, wsib_i.issue_date, wsib_i.cancelled_f, 
				wsib_i.qty_stock, 

				wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_ir.invoicelineid_bk) qty_hist, 
				case when (wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_ir.invoicelineid_bk)) < 0 then 'N' else 'P' end flag_sign 
			from 
					#wsib_invrec wsib_ir
				inner join
					-- #wsib_issue wsib_i 
					(select t1.batchstockissueid_bk, t1.issue_id, t1.issue_date, t1.cancelled_f, t1.warehousestockitembatchid_bk, 
						t1.qty_stock + isnull(t2.qty_stock, 0) qty_stock
					from 
							#wsib_issue t1
						left join
							(select batchstockissueid_bk, sum(qty_stock) qty_stock
							from #alloc_fact_order_line_erp_issue_invrec_aud
							group by batchstockissueid_bk) t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
					where t1.qty_stock + isnull(t2.qty_stock, 0) <> 0) wsib_i on wsib_ir.warehousestockitembatchid_bk = wsib_i.warehousestockitembatchid_bk
			where wsib_ir.warehousestockitembatchid_bk = 43910096368992843 and ord_rep = 2) t) t
	where flag_sign = 'P' or (flag_sign = 'N' and flag_sign_num = 1 and qty_hist + (qty_stock * -1) <> 0)


	select t1.batchstockissueid_bk, t1.issue_date, t1.cancelled_f, t1.warehousestockitembatchid_bk, 
		t1.qty_stock + isnull(t2.qty_stock, 0) qty_stock
	from 
			#wsib_issue t1
		left join
			(select batchstockissueid_bk, sum(qty_stock) qty_stock
			from #alloc_fact_order_line_erp_issue_invrec_aud
			group by batchstockissueid_bk) t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
	where t1.qty_stock + isnull(t2.qty_stock, 0) <> 0



	select *
	from #alloc_fact_order_line_erp_issue_invrec_aud



----------------------------------------------------------
----------------------------------------------------------

	select ord_rep, warehousestockitembatchid_bk, invoicelineid_bk, batch_id,
		qty_received
	into #wsib_invrec
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
	where num_rep > 1

	select wsibi.batchstockissueid batchstockissueid_bk, 
		wsibi.issueid issue_id, wsibi.issue_date, case when (wsibi.cancelled = 0) then 'N' else 'Y' end cancelled_f, 
		wsibi.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsibi.issuedquantity_issue*-1 qty_stock
	into #wsib_issue
	from 
			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
		inner join
			(select distinct warehousestockitembatchid_bk
			from #wsib_invrec) wsib_ir on wsibi.warehousestockitembatchid = wsib_ir.warehousestockitembatchid_bk
	where wsibi.cancelled = 0

	create table #alloc_fact_order_line_erp_issue_invrec_aud(
		batchstockissueid_bk						bigint NOT NULL, 

		issue_id									bigint, 

		issue_date									datetime,

		cancelled_f									char(1) NOT NULL, 

		warehousestockitembatchid_bk				bigint NOT NULL, 
		invoicelineid_bk							bigint NOT NULL,

		qty_stock									decimal(28, 8)) 

	declare @warehousestockitembatchid_bk bigint, @invoicelineid_bk bigint

	declare db_cursor cursor for
		select warehousestockitembatchid_bk, invoicelineid_bk
		from #wsib_invrec
		order by warehousestockitembatchid_bk, invoicelineid_bk

	open db_cursor
	fetch next from db_cursor into @warehousestockitembatchid_bk, @invoicelineid_bk

	while @@FETCH_STATUS = 0
	begin

		insert into #alloc_fact_order_line_erp_issue_invrec_aud (batchstockissueid_bk, 
			issue_id, issue_date, cancelled_f, 
			warehousestockitembatchid_bk, invoicelineid_bk, 
			qty_stock)

			select batchstockissueid_bk, 
				issue_id, issue_date, cancelled_f, 
				warehousestockitembatchid_bk, invoicelineid_bk, 
				case when (flag_sign  = 'P') then (qty_stock * -1) else qty_hist + (qty_stock * -1) end qty_stock
			from
				(select batchstockissueid_bk,
					issue_id, issue_date, cancelled_f,
					warehousestockitembatchid_bk, invoicelineid_bk, 
					qty_stock, qty_hist,
					flag_sign,
					dense_rank() over (partition by warehousestockitembatchid_bk, invoicelineid_bk, flag_sign order by issue_date) flag_sign_num
				from
					(select wsib_ir.ord_rep, wsib_ir.warehousestockitembatchid_bk, wsib_ir.invoicelineid_bk, wsib_ir.batch_id, wsib_ir.qty_received, 
						wsib_i.batchstockissueid_bk, 
						wsib_i.issue_id, wsib_i.issue_date, wsib_i.cancelled_f, 
						wsib_i.qty_stock, 

						wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_ir.invoicelineid_bk) qty_hist, 
						case when (wsib_ir.qty_received + sum(isnull(wsib_i.qty_stock, 0)) over (partition by wsib_ir.warehousestockitembatchid_bk order by wsib_i.issue_date, wsib_ir.invoicelineid_bk)) < 0 then 'N' else 'P' end flag_sign 
					from 
							#wsib_invrec wsib_ir
						inner join
							(select t1.batchstockissueid_bk, t1.issue_id, t1.issue_date, t1.cancelled_f, t1.warehousestockitembatchid_bk, 
								t1.qty_stock + isnull(t2.qty_stock, 0) qty_stock
							from 
									#wsib_issue t1
								left join
									(select batchstockissueid_bk, sum(qty_stock) qty_stock
									from #alloc_fact_order_line_erp_issue_invrec_aud
									group by batchstockissueid_bk) t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
							where t1.qty_stock + isnull(t2.qty_stock, 0) <> 0) wsib_i on wsib_ir.warehousestockitembatchid_bk = wsib_i.warehousestockitembatchid_bk
					where wsib_ir.warehousestockitembatchid_bk = @warehousestockitembatchid_bk and invoicelineid_bk = @invoicelineid_bk) t) t
			where flag_sign = 'P' or (flag_sign = 'N' and flag_sign_num = 1 and qty_hist + (qty_stock * -1) <> 0)

		fetch next from db_cursor into @warehousestockitembatchid_bk, @invoicelineid_bk
	end 

	close db_cursor
	deallocate db_cursor








-- Merge Statement

select batchstockissueid_bk, 
	issue_id, issue_date, cancelled_f, 
	warehousestockitembatchid_bk, invoicelineid_bk, 
	qty_stock, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud


