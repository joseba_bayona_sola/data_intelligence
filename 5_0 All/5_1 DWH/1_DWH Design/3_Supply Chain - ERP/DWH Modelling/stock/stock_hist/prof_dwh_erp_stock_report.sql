
-- Amsterdam: 
	select 'Stock On Hand', 394730 - 394536
	select 'Intransit', 42790 - 42618
	select 'Total', 437520 - 437154

-- Girona: 
	select 'Stock On Hand', 254447 - 252916
	select 'Intransit', 6102 - 5692
	select 'Total', 260549- 258608

-- York: 
	select 'Stock On Hand', 896805 - 896898
	select 'Intransit', 22653 - 22171
	select 'Total', 919458 - 919069


-------------------------------------------


alter table DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh add unit_qty_int int

update DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh
set unit_qty_int = convert(int, replace(unit_qty, ',', ''))

select top 1000 warehouse, product_id_magento, product_family_name, sku, unit_qty, unit_qty_int
from DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh
where sku = '01083B2D4AS0000000000301'

--------------------

alter table DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp add unit_qty_int int

update DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp
set unit_qty_int = convert(int, unit_qty)

select top 1000 warehouse, product_id_magento, product_family_name, sku, unit_qty, unit_qty_int, unit_qty_snap, num_exception
from DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp
where sku = '01083B2D4AS0000000000301'

-------------------------------------------

select top 1000 
	count(*) over (), 
	sum(dwh.unit_qty_int) over (), 
	sum(erp.unit_qty_int) over (),

	dwh.warehouse, erp.warehouse, 
	dwh.product_id_magento, erp.product_id_magento, dwh.product_family_name, erp.product_family_name, dwh.sku, erp.sku, 
	dwh.unit_qty_int unit_qty_dwh, erp.unit_qty_int unit_qty_erp
from 
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh dwh
	full join
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp erp on dwh.warehouse = erp.warehouse and dwh.sku = erp.sku
-- where erp.warehouse = 'Girona' -- Amsterdam (1) - Girona (0) - York (9)
-- 	and dwh.warehouse is null 

where dwh.warehouse = 'York' -- Amsterdam (30) - Girona (565) - York (127)
	and erp.warehouse is null 

order by dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku

-----------


select top 1000 
	count(*) over (), 

	sum(case when (dwh.unit_qty_int - erp.unit_qty_int) > 0 then dwh.unit_qty_int - erp.unit_qty_int else 0 end) over (), 
	sum(case when (erp.unit_qty_int - dwh.unit_qty_int) > 0 then erp.unit_qty_int - dwh.unit_qty_int else 0 end) over (),

	dwh.warehouse, erp.warehouse, 
	dwh.product_id_magento, erp.product_id_magento, dwh.product_family_name, erp.product_family_name, dwh.sku, erp.sku, 
	dwh.unit_qty_int unit_qty_dwh, erp.unit_qty_int unit_qty_erp, 
	case when (dwh.unit_qty_int - erp.unit_qty_int) > 0 then dwh.unit_qty_int - erp.unit_qty_int else 0 end diff_pos_dwh, 
	case when (erp.unit_qty_int - dwh.unit_qty_int) > 0 then erp.unit_qty_int - dwh.unit_qty_int else 0 end diff_pos_erp

from 
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh dwh
	full join
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp erp on dwh.warehouse = erp.warehouse and dwh.sku = erp.sku

where dwh.warehouse = 'Amsterdam' -- -- Amsterdam (172 / 14) - Girona (968 / 2) - York (570 / 820)
	and dwh.warehouse is not null and erp.warehouse is not null and dwh.unit_qty_int <> erp.unit_qty_int

-- order by dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku
order by diff_pos_dwh desc, diff_pos_erp desc, dwh.warehouse, dwh.product_id_magento, dwh.product_family_name, dwh.sku

---------------------------------------------------
---------------------------------------------------
---------------------------------------------------


select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where order_no_erp = 'INT00003898' and batch_id = 1688817

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue
where batchstockissueid_bk = 89509042603175216

select top 1000 *, datediff(dd, createddate, shippeddate)
from Landing.mend.wh_batchstockissue_aud
where id = 89509042603175216

select top 10000 *, datediff(dd, createddate, shippeddate)
from Landing.mend.wh_batchstockissue_aud
where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1
order by shippeddate desc




select top 1000 *
from Landing.mend.wh_batchstockissue_aud_hist
where id = 89509042603175216


select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v
where batch_id = 1688817

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
where batch_id = 1688817
order by stock_date

select top 1000 *
from Landing.aux.alloc_fact_order_line_erp_issue_aud
where warehousestockitembatchid_bk = 43910096368572618

---------------------------------------------------
---------------------------------------------------
---------------------------------------------------

select 
	dwh.warehouse warehouse_dwh, erp.warehouse warehouse_erp, 
	case when (dwh.product_id_magento is null) then erp.product_id_magento else dwh.product_id_magento end product_id_magento, 
	case when (dwh.product_family_name is null) then erp.product_family_name else dwh.product_family_name end product_family_name, 
	case when (dwh.sku is null) then erp.sku else dwh.sku end sku, 
	dwh.unit_qty_int unit_qty_dwh, erp.unit_qty_int unit_qty_erp, 
	case when (dwh.unit_qty_int - erp.unit_qty_int) > 0 then dwh.unit_qty_int - erp.unit_qty_int else 0 end diff_pos_dwh, 
	case when (erp.unit_qty_int - dwh.unit_qty_int) > 0 then erp.unit_qty_int - dwh.unit_qty_int else 0 end diff_pos_erp
into #stock_report_sku_comp
from 
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_dwh dwh
	full join
		DW_GetLenses_jbs.dbo.ex_stock_report_sku_201904_erp erp on dwh.warehouse = erp.warehouse and dwh.sku = erp.sku

select top 10000 id, createddate, shippeddate, datediff(dd, createddate, shippeddate) diff_days
from Landing.mend.wh_batchstockissue_aud
where year(createddate) > 2018 and datediff(dd, createddate, shippeddate) < -1
-- order by diff_days asc
order by id desc



select top 1000 *
from Landing.aux.alloc_fact_order_line_erp_issue_aud
where batchstockissueid_bk = 89509042603070011


select oli.batchstockissueid_bk, oli.issue_date, bsi.createddate, bsi.ins_ts, bsi.diff_days, datediff(dd, bsi.ins_ts, bsi.shippeddate) diff_days2, bsi.issuedquantity, 
	oli.warehouse_name, oli.product_id_magento, oli.product_family_name, oli.sku, oli.qty_stock, 
	sum(oli.qty_stock) over (partition by oli.warehouse_name, oli.sku) sum_qty_stock, 
	t.unit_qty_dwh, t.unit_qty_erp, t.diff_pos_dwh, t.diff_pos_erp
from 
		(select id, createddate, shippeddate, ins_ts, datediff(dd, createddate, shippeddate) diff_days, issuedquantity
		from Landing.mend.wh_batchstockissue_aud
		where year(createddate) > 2018 and datediff(dd, createddate, shippeddate) < 0) bsi
	inner join
		Warehouse.alloc.fact_order_line_erp_issue_v oli on bsi.id = oli.batchstockissueid_bk
	inner join
		#stock_report_sku_comp t on oli.warehouse_name = t.warehouse_dwh and oli.sku = t.sku
where bsi.diff_days = -1
	-- and (diff_pos_dwh is null or diff_pos_dwh <> 0)
order by oli.issue_date

select *
from #stock_report_sku_comp
where sku = '01083B2D4CE0000000000301'



---------------------------------------------------
---------------------------------------------------

select wsib.*
select distinct wsib.warehousestockitembatchid
from
		(select *, datediff(dd, createddate, shippeddate) diff
		from Landing.mend.wh_batchstockissue_aud
		where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1) bsi
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsib on bsi.id = wsib.batchstockissueid

select count(*) over (partition by wsibh.warehousestockitembatchid_bk) num_r, bsi.diff, bsi.shippeddate, wsib.orde wsibh.*
from
		(select *, datediff(dd, createddate, shippeddate) diff
		from Landing.mend.wh_batchstockissue_aud
		where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1) bsi
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsib on bsi.id = wsib.batchstockissueid
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh on wsib.warehousestockitembatchid = wsibh.warehousestockitembatchid_bk and update_type <> 'T'
order by num_r desc, bsi.diff, update_type, last_stock_date, warehousestockitembatchid_bk

select count(*) over (partition by wsibh.warehousestockitembatchid_bk) num_r, wsibh.*
from
		(select distinct wsib.warehousestockitembatchid
		from
				(select *, datediff(dd, createddate, shippeddate) diff
				from Landing.mend.wh_batchstockissue_aud
				where year(shippeddate) = 2019 and datediff(dd, createddate, shippeddate) < -1) bsi
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsib on bsi.id = wsib.batchstockissueid) wsib
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh on wsib.warehousestockitembatchid = wsibh.warehousestockitembatchid_bk and update_type <> 'T'
order by num_r desc, update_type, last_stock_date, warehousestockitembatchid_bk

---------------------------------------------------
---------------------------------------------------

select *
from Landing.mend.inter_intransitbatchtransaction
where year(createddate) > 2018 and datediff(dd, createddate, transactiondate) < 0
order by id desc
