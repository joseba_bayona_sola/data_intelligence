
-- Truncate Tables
truncate table Warehouse.stock.dim_intransit_stock_item_batch_hist_wrk
go
truncate table Warehouse.stock.dim_intransit_stock_item_batch_hist_del_wrk
go

-- Insert Wrk Tables (Look Up)

insert into Warehouse.stock.dim_intransit_stock_item_batch_hist_wrk (idIntransitStockItemBatch_sk_fk, idCalendarStockDate_sk_fk, 
	qty_on_hand, 
	delete_f, 
	idETLBatchRun_ins)

	select isib.idIntransitStockItemBatch_sk, isibh.idCalendarStockDate, 
		isibh.qty_on_hand, 
		isibh.delete_f,
		3697 
	from 
			Staging.stock.dim_intransit_stock_item_batch_hist isibh
		inner join
			Warehouse.stock.dim_intransit_stock_item_batch isib on isibh.intransitstockitembatchid_bk = isib.intransitstockitembatchid_bk

insert into Warehouse.stock.dim_intransit_stock_item_batch_hist_del_wrk (idIntransitStockItemBatch_sk_fk, 
	idETLBatchRun_ins)

	select isib.idIntransitStockItemBatch_sk, 
		3697  
	from 
			Staging.stock.dim_intransit_stock_item_batch_hist_del isibh
		inner join
			Warehouse.stock.dim_intransit_stock_item_batch isib on isibh.intransitstockitembatchid_bk = isib.intransitstockitembatchid_bk

-- Merge SP Code

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_intransit_stock_item_batch_hist_del with (tablock) as trg
	using Warehouse.stock.dim_intransit_stock_item_batch_hist_del_wrk src
		on (trg.idIntransitStockItemBatch_sk_fk = src.idIntransitStockItemBatch_sk_fk)

	when not matched
		then 
			insert (idIntransitStockItemBatch_sk_fk, 
				idETLBatchRun_ins)
				
				values (src.idIntransitStockItemBatch_sk_fk, 
					3697);


	-- DELETE STATEMENT 
	delete trg
	from 
			Warehouse.stock.dim_intransit_stock_item_batch_hist trg
		inner join
			Warehouse.stock.dim_intransit_stock_item_batch_hist_del src on trg.idIntransitStockItemBatch_sk_fk = src.idIntransitStockItemBatch_sk_fk
	
	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_intransit_stock_item_batch_hist with (tablock) as trg
	using Warehouse.stock.dim_intransit_stock_item_batch_hist_wrk src
		on (trg.idIntransitStockItemBatch_sk_fk = src.idIntransitStockItemBatch_sk_fk and trg.idCalendarStockDate_sk_fk = src.idCalendarStockDate_sk_fk)
	when matched and not exists 
		(select 
			isnull(trg.qty_on_hand, 0), 
			isnull(trg.delete_f, '') 
		intersect
		select 
			isnull(src.qty_on_hand, 0), 
			isnull(src.delete_f, '') )
		then 
			update set
				trg.qty_on_hand = src.qty_on_hand, 
				trg.delete_f = src.delete_f, 

				trg.idETLBatchRun_upd = 3697, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idIntransitStockItemBatch_sk_fk, idCalendarStockDate_sk_fk, 
				qty_on_hand, 
				delete_f, 
				idETLBatchRun_ins)
				
				values (src.idIntransitStockItemBatch_sk_fk, src.idCalendarStockDate_sk_fk, 
						src.qty_on_hand, 
						src.delete_f,
					3697);
