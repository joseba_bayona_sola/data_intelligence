
	drop table #wsib_num_batches

	select wsib.magentoProductID_int, wsib.name, 
		count(*) num_tot, 0 update_num
	into #wsib_num_batches
	from 
			Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
	group by wsib.magentoProductID_int, wsib.name
	order by wsib.magentoProductID_int, wsib.name

	select *, sum(num_tot) over (partition by update_num)
	from #wsib_num_batches
	order by num_tot desc

	declare @product_id_magento bigint 
	declare @name varchar(50)
	declare @num_tot int 
	declare @update_num int = 1
	declare @num_tot_ac int = 0

	declare db_cursor cursor for
		select magentoProductID_int, name, num_tot
		from #wsib_num_batches
		order by num_tot desc

	open db_cursor
	fetch next from db_cursor into @product_id_magento, @name, @num_tot

	while @@FETCH_STATUS = 0
	begin
		
		update #wsib_num_batches
		set update_num = @update_num
		where magentoProductID_int = @product_id_magento

		select @num_tot_ac = @num_tot_ac + @num_tot
		
		print(@num_tot)
		print(@num_tot_ac)

		if @num_tot_ac > 100000 
			begin
				select @num_tot_ac = @num_tot
				select @update_num = @update_num + 1
			end

		fetch next from db_cursor into @product_id_magento, @name, @num_tot
	end 

	close db_cursor
	deallocate db_cursor


-----------------------------------------------------------

select product_id_magento, name, num_tot, update_num
from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products
order by num_tot desc


select update_num, count(*) num_products
from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products
group by update_num
order by update_num


	declare @update_num int 

	declare db_cursor cursor for
		select update_num
		from Landing.aux.stock_dim_wh_stock_item_batch_hist_initial_products
		group by update_num
		order by update_num

	open db_cursor
	fetch next from db_cursor into @update_num

	while @@FETCH_STATUS = 0
	begin
			
		-- Call to SSIS
		print(@update_num)

		fetch next from db_cursor into @update_num
	end 

	close db_cursor
	deallocate db_cursor
