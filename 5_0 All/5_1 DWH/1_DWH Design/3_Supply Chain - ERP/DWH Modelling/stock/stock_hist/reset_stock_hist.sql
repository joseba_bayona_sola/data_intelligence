
-- Warehouse
truncate table Warehouse.stock.dim_wh_stock_item_batch_hist
go

truncate table Warehouse.stock.dim_wh_stock_item_batch_hist_del
go
truncate table Warehouse.stock.dim_wh_stock_item_batch_hist_del_wrk
go

-- Staging
truncate table Staging.stock.dim_wh_stock_item_batch_hist_del
go

-- Landing
truncate table Landing.aux.stock_dim_wh_stock_item_batch_hist_info
go

truncate table Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist
go

truncate table Landing.aux.stock_dim_wh_stock_item_batch_hist_info_hist_last
go

truncate table Landing.aux.stock_dim_wh_stock_item_batch_hist_cut_off
go
