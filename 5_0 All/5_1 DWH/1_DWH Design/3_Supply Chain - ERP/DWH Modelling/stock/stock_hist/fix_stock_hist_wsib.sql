
-- Truncate 

truncate table Warehouse.stock.dim_wh_stock_item_batch_hist_wrk
go
truncate table Warehouse.stock.dim_wh_stock_item_batch_hist_del_wrk
go
truncate table Warehouse.stock.dim_wh_stock_item_batch_hist_info_wrk
go

-- Insert Tables


insert into Warehouse.stock.dim_wh_stock_item_batch_hist_wrk (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, 
	idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
	qty_on_hand, 
	delete_f, 
	idETLBatchRun_ins)

	select wsib.idWHStockItemBatch_sk, isnull(irl.idInvoiceReconciliationLine_sk, -1), wsibh.idCalendarStockDate, 
		rl.idReceiptLine_sk, wsibm.idWHStockItemBatchMovement_sk, 
		wsibh.qty_on_hand, 
		wsibh.delete_f,
		3697
	from 
			Staging.stock.dim_wh_stock_item_batch_hist wsibh
		inner join
			Warehouse.stock.dim_wh_stock_item_batch wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
		left join
			Warehouse.invrec.fact_invoice_reconciliation_line irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk
		left join
			Warehouse.rec.fact_receipt_line rl on wsibh.receiptlineid_bk = rl.receiptlineid_bk
		left join
			Warehouse.stock.fact_wh_stock_item_batch_movement wsibm on wsibh.batchstockmovementid_bk = wsibm.batchstockmovementid_bk


insert into Warehouse.stock.dim_wh_stock_item_batch_hist_del_wrk (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, 
	idETLBatchRun_ins)

	select wsib.idWHStockItemBatch_sk, isnull(irl.idInvoiceReconciliationLine_sk, -1), 
		3697 
	from 
			Staging.stock.dim_wh_stock_item_batch_hist_del wsibh
		inner join
			Warehouse.stock.dim_wh_stock_item_batch wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
		left join
			Warehouse.invrec.fact_invoice_reconciliation_line irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk


insert into Warehouse.stock.dim_wh_stock_item_batch_hist_info_wrk (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, 
	idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
	delete_f, 
	idETLBatchRun_ins)

	select wsib.idWHStockItemBatch_sk, isnull(irl.idInvoiceReconciliationLine_sk, -1), 
		rl.idReceiptLine_sk, wsibm.idWHStockItemBatchMovement_sk, 
		wsibh.delete_f,
		3697  
	from 
			Staging.stock.dim_wh_stock_item_batch_hist_info wsibh
		inner join
			Warehouse.stock.dim_wh_stock_item_batch wsib on wsibh.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
		left join
			Warehouse.invrec.fact_invoice_reconciliation_line irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk
		left join
			Warehouse.rec.fact_receipt_line rl on wsibh.receiptlineid_bk = rl.receiptlineid_bk
		left join
			Warehouse.stock.fact_wh_stock_item_batch_movement wsibm on wsibh.batchstockmovementid_bk = wsibm.batchstockmovementid_bk


-- Merge SP

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_wh_stock_item_batch_hist_info with (tablock) as trg
	using Warehouse.stock.dim_wh_stock_item_batch_hist_info_wrk src
		on (trg.idWHStockItemBatch_sk_fk = src.idWHStockItemBatch_sk_fk and trg.idInvoiceReconciliationLine_sk_fk = src.idInvoiceReconciliationLine_sk_fk)
	when matched and not exists 
		(select 
			isnull(trg.idReceiptLine_sk_fk, 0), isnull(trg.idWHStockItemBatchMovement_sk_fk, 0), 
			isnull(trg.delete_f, '') 
		intersect
		select 
			isnull(src.idReceiptLine_sk_fk, 0), isnull(src.idWHStockItemBatchMovement_sk_fk, 0), 
			isnull(src.delete_f, '') )
		then 
			update set
				trg.idReceiptLine_sk_fk = src.idReceiptLine_sk_fk, trg.idWHStockItemBatchMovement_sk_fk = src.idWHStockItemBatchMovement_sk_fk, 
				trg.delete_f = src.delete_f, 

				trg.idETLBatchRun_upd = 3697, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, 
				idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
				delete_f, 
				idETLBatchRun_ins)
				
				values (src.idWHStockItemBatch_sk_fk, src.idInvoiceReconciliationLine_sk_fk, 
						src.idReceiptLine_sk_fk, src.idWHStockItemBatchMovement_sk_fk, 
						src.delete_f,
					3697);


	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_wh_stock_item_batch_hist_del with (tablock) as trg
	using Warehouse.stock.dim_wh_stock_item_batch_hist_del_wrk src
		on (trg.idWHStockItemBatch_sk_fk = src.idWHStockItemBatch_sk_fk and trg.idInvoiceReconciliationLine_sk_fk = src.idInvoiceReconciliationLine_sk_fk)

	when not matched
		then 
			insert (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk,
				idETLBatchRun_ins)
				
				values (src.idWHStockItemBatch_sk_fk, src.idInvoiceReconciliationLine_sk_fk, 
					3697);


	-- DELETE STATEMENT 
	delete trg
	from 
			Warehouse.stock.dim_wh_stock_item_batch_hist trg
		inner join
			Warehouse.stock.dim_wh_stock_item_batch_hist_del src on trg.idWHStockItemBatch_sk_fk = src.idWHStockItemBatch_sk_fk and trg.idInvoiceReconciliationLine_sk_fk = src.idInvoiceReconciliationLine_sk_fk

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_wh_stock_item_batch_hist with (tablock) as trg
	using Warehouse.stock.dim_wh_stock_item_batch_hist_wrk src
		on (trg.idWHStockItemBatch_sk_fk = src.idWHStockItemBatch_sk_fk and trg.idInvoiceReconciliationLine_sk_fk = src.idInvoiceReconciliationLine_sk_fk 
			and trg.idCalendarStockDate_sk_fk = src.idCalendarStockDate_sk_fk)
	when matched and not exists 
		(select 
			isnull(trg.idReceiptLine_sk_fk, 0), isnull(trg.idWHStockItemBatchMovement_sk_fk, 0), 
			isnull(trg.qty_on_hand, 0), 
			isnull(trg.delete_f, '') 
		intersect
		select 
			isnull(src.idReceiptLine_sk_fk, 0), isnull(src.idWHStockItemBatchMovement_sk_fk, 0), 
			isnull(src.qty_on_hand, 0), 
			isnull(src.delete_f, '') )
		then 
			update set
				trg.idReceiptLine_sk_fk = src.idReceiptLine_sk_fk, trg.idWHStockItemBatchMovement_sk_fk = src.idWHStockItemBatchMovement_sk_fk, 
				trg.qty_on_hand = src.qty_on_hand, 
				trg.delete_f = src.delete_f, 

				trg.idETLBatchRun_upd = 3697, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idWHStockItemBatch_sk_fk, idInvoiceReconciliationLine_sk_fk, idCalendarStockDate_sk_fk, 
				idReceiptLine_sk_fk, idWHStockItemBatchMovement_sk_fk, 
				qty_on_hand, 
				delete_f, 
				idETLBatchRun_ins)
				
				values (src.idWHStockItemBatch_sk_fk, src.idInvoiceReconciliationLine_sk_fk, src.idCalendarStockDate_sk_fk, 
						src.idReceiptLine_sk_fk, src.idWHStockItemBatchMovement_sk_fk, 
						src.qty_on_hand, 
						src.delete_f,
					3697);
