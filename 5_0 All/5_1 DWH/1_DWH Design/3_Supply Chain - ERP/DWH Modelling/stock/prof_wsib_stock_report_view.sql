
	select wsib.stock_batch_type_name, count(*), sum(count(*)) over ()
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		inner join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
	group by wsib.stock_batch_type_name
	order by wsib.stock_batch_type_name


	select wsib.stock_batch_type_name, 
		case when (wsib.qty_received > 0) then 1 else 0 end qty_received, 
		case when (wsib.qty_registered_sm > 0) then 1 else 0 end qty_registered, 
		count(*), min(batch_stock_register_date), max(batch_stock_register_date), sum(count(*)) over ()
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		left join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
	where rl.idReceiptLine_sk is null
	group by wsib.stock_batch_type_name, 
		case when (wsib.qty_received > 0) then 1 else 0 end, 
		case when (wsib.qty_registered_sm > 0) then 1 else 0 end
	order by qty_received, qty_registered, wsib.stock_batch_type_name

		select top 1000 count(*) over (), *
		from
			(select wsib.*, 
				case when (wsib.qty_received > 0) then 1 else 0 end qty_received_c, 
				case when (wsib.qty_registered_sm > 0) then 1 else 0 end qty_registered_c
			from 
					Warehouse.stock.dim_wh_stock_item_batch_v wsib
				left join
					Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
			where rl.idReceiptLine_sk is null) wsib
		where qty_received_c = 0 and qty_registered_c = 1
		order by batch_stock_register_date desc

	-- Receipt - No InvRec
	select top 1000 count(*) over (), count(*) over (partition by wsib.warehousestockitembatchid_bk) num_rep,
		wsib.warehousestockitembatchid_bk, 
		'Receipt' transaction_type,
		wsib.warehouse_name, -- wsib.stock_method_type_name, wsib.stock_batch_type_name,
		wsib.manufacturer_name, 
		wsib.product_type_name, wsib.category_name, wsib.product_family_group_name, wsib.product_id_magento, wsib.product_family_name, wsib.packsize, 
		wsib.SKU, wsib.stock_item_description, 
		wsib.batch_id, 
		rl.supplier_type_name, rl.supplier_name,
		rl.receipt_number, rl.wh_shipment_type_name, rl.receipt_status_name, 
		rl.purchase_order_number, rl.po_source_name, rl.po_type_name, rl.po_status_name, 
		null invoice_ref, null invoice_date, null posted_date, irl.quantity,
		case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end irl_perc_qty,
		wsib.qty_available *  case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end qty_available_irl_perc,
		round(wsib.qty_available *  case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end, 0) qty_available_irl_perc_r,

		-- wsib.qty_received, 
		wsib.qty_received - isnull(irl.quantity, 0) qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold, -- wsib.qty_allocated_stock, wsib.qty_issued_stock, 
		wsib.qty_registered_sm qty_registered, wsib.qty_disposed_sm qty_disposed, 

		-- wsib.batch_arrived_date, wsib.batch_confirmed_date, 
		wsib.batch_stock_register_date,

		wsib.local_product_unit_cost, wsib.global_product_unit_cost, -- wsib.local_total_unit_cost, wsib.global_total_unit_cost, 
		rl.local_unit_cost, rl.global_unit_cost,
		rl.local_invoice_unit_cost, rl.global_invoice_unit_cost,
		null adjusted_unit_cost,

		wsib.local_to_global_rate, wsib.currency_code
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		inner join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
		left join
			(select idReceiptLIne_sk_fk, sum(quantity) quantity
			from Warehouse.invrec.fact_invoice_reconciliation_line_v 
			where posted_date is not null
			group by idReceiptLIne_sk_fk) irl on rl.idReceiptLine_sk = irl.idReceiptLine_sk_fk
	where 
		(irl.idReceiptLine_sk_fk is not null and wsib.qty_received - irl.quantity > 0) 
		or irl.idReceiptLine_sk_fk is null 
	order by num_rep desc



	-- Receipt - Inv Rec
	select top 1000 count(*) over (), count(*) over (partition by wsib.warehousestockitembatchid_bk) num_rep,
		wsib.warehousestockitembatchid_bk, 
		'Receipt' transaction_type,
		wsib.warehouse_name, -- wsib.stock_method_type_name, wsib.stock_batch_type_name,
		wsib.manufacturer_name, 
		wsib.product_type_name, wsib.category_name, wsib.product_family_group_name, wsib.product_id_magento, wsib.product_family_name, wsib.packsize, 
		wsib.SKU, wsib.stock_item_description, 
		wsib.batch_id, 
		rl.supplier_type_name, rl.supplier_name,
		rl.receipt_number, rl.wh_shipment_type_name, rl.receipt_status_name, 
		rl.purchase_order_number, rl.po_source_name, rl.po_type_name, rl.po_status_name, 
		irl.invoice_ref, irl.invoice_date, irl.posted_date, irl.quantity,
		case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end irl_perc_qty,
		wsib.qty_available *  case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end qty_available_irl_perc,
		round(wsib.qty_available *  case when (irl.quantity is not null) then irl.quantity / wsib.qty_received else null end, 0) qty_available_irl_perc_r,

		wsib.qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold, -- wsib.qty_allocated_stock, wsib.qty_issued_stock, 
		wsib.qty_registered_sm qty_registered, wsib.qty_disposed_sm qty_disposed, 

		-- wsib.batch_arrived_date, wsib.batch_confirmed_date, 
		wsib.batch_stock_register_date,

		wsib.local_product_unit_cost, wsib.global_product_unit_cost, -- wsib.local_total_unit_cost, wsib.global_total_unit_cost, 
		rl.local_unit_cost, rl.global_unit_cost,
		rl.local_invoice_unit_cost, rl.global_invoice_unit_cost,
		irl.adjusted_unit_cost,

		wsib.local_to_global_rate, wsib.currency_code
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		inner join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
		left join
			(select *
			from Warehouse.invrec.fact_invoice_reconciliation_line_v 
			where posted_date is not null) irl on rl.idReceiptLine_sk = irl.idReceiptLine_sk_fk
	where irl.idReceiptLine_sk_fk is not null 
	-- where irl.idReceiptLine_sk_fk is not null and irl.quantity <> wsib.qty_received
	order by num_rep desc

	-- Stock
	select top 1000 count(*) over (), count(*) over (partition by wsib.warehousestockitembatchid_bk) num_rep,
		wsib.warehousestockitembatchid_bk, 
		'Stock' transaction_type,
		wsib.warehouse_name, -- wsib.stock_method_type_name, wsib.stock_batch_type_name,
		wsib.manufacturer_name, 
		wsib.product_type_name, wsib.category_name, wsib.product_family_group_name, wsib.product_id_magento, wsib.product_family_name, wsib.packsize, 
		wsib.SKU, wsib.stock_item_description, 
		wsib.batch_id, 
		rl.supplier_type_name, rl.supplier_name,
		rl.receipt_number, rl.wh_shipment_type_name, rl.receipt_status_name, 
		rl.purchase_order_number, rl.po_source_name, rl.po_type_name, rl.po_status_name, 
		null invoice_ref, null posted_date, null quantity,

		wsib.qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold, -- wsib.qty_allocated_stock, wsib.qty_issued_stock, 
		wsib.qty_registered_sm qty_registered, wsib.qty_disposed_sm qty_disposed, 

		-- wsib.batch_arrived_date, wsib.batch_confirmed_date, 
		wsib.batch_stock_register_date,

		wsib.local_product_unit_cost, wsib.global_product_unit_cost, -- wsib.local_total_unit_cost, wsib.global_total_unit_cost, 
		rl.local_unit_cost, rl.global_unit_cost,
		null adjusted_unit_cost,

		wsib.local_to_global_rate, wsib.currency_code
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		left join
			Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
	where rl.idReceiptLine_sk is null 
		-- and wsib.stock_batch_type_name = 'NO_RECEIPT_NO_MOV'
		and ((wsib.qty_registered_sm = 0) or (wsib.qty_registered_sm > 0 and wsib.qty_received > 0))
	order by wsib.batch_stock_register_date desc

	-- Adjustment
	select top 1000 count(*) over (), *
	from	 
		(select count(*) over (partition by wsib.warehousestockitembatchid_bk) num_rep, 
			dense_rank() over (partition by wsib.warehousestockitembatchid_bk order by wsibm.batch_movement_date, wsibm.batchstockmovementid_bk) ord_rep, 
			wsib.warehousestockitembatchid_bk, 
			'Adjustment' transaction_type,
			wsib.warehouse_name, -- wsib.stock_method_type_name, wsib.stock_batch_type_name,
			wsib.manufacturer_name, 
			wsib.product_type_name, wsib.category_name, wsib.product_family_group_name, wsib.product_id_magento, wsib.product_family_name, wsib.packsize, 
			wsib.SKU, wsib.stock_item_description, 
			wsib.batch_id, 
			rl.supplier_type_name, rl.supplier_name,
			rl.receipt_number, rl.wh_shipment_type_name, rl.receipt_status_name, 
			rl.purchase_order_number, rl.po_source_name, rl.po_type_name, rl.po_status_name, 
			null invoice_ref, null posted_date, null quantity,

			wsib.qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold, -- wsib.qty_allocated_stock, wsib.qty_issued_stock, 
			wsib.qty_registered_sm qty_registered, wsib.qty_disposed_sm qty_disposed, 
			-- wsibm.qty_registered, wsibm.batch_movement_date,
			wsibm.move_id, wsibm.movement_reason, wsibm.movement_comment, -- wsibm.batch_stock_move_id

			-- wsib.batch_arrived_date, wsib.batch_confirmed_date, 
			wsib.batch_stock_register_date,

			wsib.local_product_unit_cost, wsib.global_product_unit_cost, -- wsib.local_total_unit_cost, wsib.global_total_unit_cost, 
			rl.local_unit_cost, rl.global_unit_cost,
			null adjusted_unit_cost,

			wsib.local_to_global_rate, wsib.currency_code
		from 
				Warehouse.stock.dim_wh_stock_item_batch_v wsib
			left join
				Warehouse.rec.fact_receipt_line_v rl on wsib.idReceiptLine_sk = rl.idReceiptLine_sk
			left join
				Warehouse.stock.fact_wh_stock_item_batch_movement_v wsibm on wsib.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and wsibm.qty_registered > 0 -- and wsib.qty_registered_sm = wsibm.qty_registered
			-- left join
				-- Warehouse.stock.fact_wh_stock_item_batch_movement_v wsibm2 on wsib.idWHStockItemBatch_sk = wsibm2.idWHStockItemBatch_sk and wsibm.idWHStockItemBatch_sk is null
		where rl.idReceiptLine_sk is null 
			-- and wsib.stock_batch_type_name = 'NO_RECEIPT_MOV'
			-- and wsib.qty_registered_sm > 0 and wsib.qty_received <> 0
			and (wsib.qty_registered_sm > 0 and wsib.qty_received = 0)) wsibm
	where ord_rep = 1 
	-- where num_rep > 1 order by num_rep desc, batch_stock_register_date desc
	order by batch_stock_register_date desc

	-- Intransit
	select top 1000 count(*) over (), count(*) over (partition by isib.intransitstockitembatchid_bk) num_rep, 
		isib.intransitstockitembatchid_bk, 
		'Intransit' transaction_type, 
		isib.wholesale_customer_name warehouse_name, 
		isib.manufacturer_name, 
		isib.product_type_name, isib.category_name, isib.product_family_group_name, isib.product_id_magento, isib.product_family_name, isib.packsize, 
		isib.SKU, isib.stock_item_description, 
		null batch_id, 
		isib.order_no_erp, 

		isib.qty_remaining, 

		isib.batch_created_date, 

		isib.local_product_unit_cost, isib.global_product_unit_cost,

		isib.local_to_global_rate, isib.currency_code
	from Warehouse.stock.dim_intransit_stock_item_batch_v isib
	where qty_remaining > 0
