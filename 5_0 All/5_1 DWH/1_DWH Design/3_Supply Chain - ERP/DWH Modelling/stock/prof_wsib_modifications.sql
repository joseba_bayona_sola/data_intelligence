
select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_v

	select top 1000
		wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, wsib.id warehousestockitembatchid, wsi.warehouseid,
		wsibrl.receiptlineid,
		case when (bstwsib.warehousestockitembatchid is null) then 0 else 1 end batchstockmovement_f, 
		case when (rstwsib.warehousestockitembatchid is null) then 0 else 1 end batchrepair_f,
		wsi.magentoProductID_int, wsi.magentoProductID,
		wsi.name, wsi.stockitemdescription, wsi.packSize, -- wsi.magentoSKU, wsi.SKU_product, wsi.SKU
		-- wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.co_EDI, 
		wsi.code, wsi.warehouse_name, wsi.id_string, 
		-- wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		-- wsi.outstandingallocation, wsi.onhold,
		-- wsi.stockingmethod, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.autoadjusted, 
		wsib.receivedquantity, wsib.allocatedquantity, 
		case when (wsibf.issuedquantity is not null) then wsibf.issuedquantity else wsib.issuedquantity end issuedquantity,
		wsib.onHoldQuantity, wsib.disposedQuantity, wsib.forwardDemand, wsib.registeredQuantity,
		rl.quantityaccepted,
		wsib.groupFIFODate, wsib.arrivedDate, wsib.confirmedDate, wsib.stockregisteredDate, wsib.receiptCreatedDate, wsib.createdDate,
		wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, 
		wsib.inteCoCarriageUnitCost interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo,
		wsib.exchangeRate, wsib.currency
	from 
			Landing.mend.gen_wh_warehousestockitem_v wsi
		inner join
			Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsiwsib on wsi.warehousestockitemid = wsiwsib.warehousestockitemid
		inner join
			Landing.mend.wh_warehousestockitembatch_aud wsib on wsiwsib.warehousestockitembatchid = wsib.id
		left join
			Landing.mend.wh_warehousestockitembatch_full wsibf on wsib.id = wsibf.id
		left join
			Landing.mend.whship_warehousestockitembatch_receiptline wsibrl on wsib.id = wsibrl.warehousestockitembatchid
		left join
			Landing.mend.whship_receiptline rl on wsibrl.receiptlineid = rl.id
		left join
			(select warehousestockitembatchid, count(*) num
			from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
			group by warehousestockitembatchid) bstwsib on wsib.id = bstwsib.warehousestockitembatchid
		left join
			(select warehousestockitembatchid, count(*) num
			from Landing.mend.wh_repairstockbatchs_warehousestockitembatch_aud
			group by warehousestockitembatchid) rstwsib on wsib.id = rstwsib.warehousestockitembatchid

-- Stock 
	-- lnd_stg_get_stock_wh_stock_item_batch

	select top 1000 *
	from Landing.aux.mend_stock_warehousestockitembatch_v


create view aux.mend_stock_warehousestockitembatch_v as
	select wsib.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsib.warehousestockitemid warehousestockitemid_bk, 
		case 
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 0) then 'NO_RECEIPT_NO_MOV'
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 1) then 'NO_RECEIPT_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 0) then 'RECEIPT_NO_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 1) then 'RECEIPT_MOV'
		end stock_batch_type_bk,
		wsib.receiptlineid receiptlineid_bk,
		wsib.batch_id, wsib.fullyallocated fully_allocated_f, wsib.fullyissued fully_issued_f, wsib.autoadjusted auto_adjusted_f, 

		wsib.receivedquantity qty_received, wsib.receivedquantity + wsib.registeredQuantity - wsib.allocatedquantity - wsib.disposedQuantity - wsib.onHoldQuantity qty_available, 
		wsib.allocatedquantity - wsib.issuedquantity qty_outstanding_allocation, 
		wsib.allocatedquantity qty_allocated_stock, wsib.issuedquantity qty_issued_stock, 
		wsib.onholdquantity qty_on_hold, wsib.registeredquantity qty_registered, wsib.disposedquantity qty_disposed, 
		isnull(wsibsm.qty_registered, 0) qty_registered_sm, isnull(wsibsm.qty_disposed, 0) qty_disposed_sm, 

		convert(datetime, wsib.arrivedDate) batch_arrived_date, convert(datetime, wsib.confirmedDate) batch_confirmed_date, convert(datetime, wsib.stockregistereddate) batch_stock_register_date, 

		wsib.productUnitCost local_product_unit_cost, wsib.carriageUnitCost local_carriage_unit_cost, wsib.dutyUnitCost local_duty_unit_cost, wsib.totalUnitCost local_total_unit_cost, 
		wsib.interCoCarriageUnitCost local_interco_carriage_unit_cost, wsib.totalUnitCostIncInterCo local_total_unit_cost_interco, 
		wsib.exchangeRate local_to_global_rate, wsib.currency currency_code, 
		case when (wsib2.id is not null) then 'N' else 'Y' end delete_f
	from 
			(select wsib.warehousestockitembatchid, wsib.warehousestockitemid, wsib.receiptlineid, wsib.productfamilyid, 
				wsib.batch_id, wsib.fullyallocated, wsib.fullyissued, wsib.autoadjusted, 
				wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onholdquantity, wsib.registeredquantity, wsib.disposedquantity, 
				wsib.arrivedDate, wsib.confirmedDate, wsib.stockregistereddate, 
				wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, wsib.interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo, 
				case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end exchangeRate, wsib.currency,
				wsib.batchstockmovement_f
			from
					(select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						-- case when (wsib.arrivedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.arrivedDate) else wsib.arrivedDate end arrivedDate,
						-- case when (wsib.confirmedDate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.confirmedDate) else wsib.confirmedDate end confirmedDate,
						-- case when (wsib.stockregistereddate between stp.from_date and stp.to_date) then dateadd(hour, 1, wsib.stockregistereddate) else wsib.stockregistereddate end stockregistereddate,
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, isnull(currency, w.currencycode) currency,
						batchstockmovement_f
					from 
							Landing.mend.gen_wh_warehousestockitembatch_v wsib
						left join
							Landing.map.sales_summer_time_period_aud stp on year(wsib.arrivedDate) = stp.year
						left join
							Landing.map.sales_summer_time_period_aud stp2 on year(wsib.confirmedDate) = stp2.year
						left join
							Landing.map.sales_summer_time_period_aud stp3 on year(wsib.stockregistereddate) = stp3.year
						left join
							Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid) wsib
				left join
					Landing.mend.gen_comp_currency_exchange_v er on wsib.stockregistereddate > er.effectivedate and wsib.stockregistereddate <= er.nexteffectivedate and
						wsib.currency = er.currencycode_from and er.currencycode_to = 'GBP') wsib
		left join
			(select warehousestockitembatchid_bk, 
				sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed
			from Landing.aux.mend_stock_batchstockmovement_v
			where delete_f = 'N'
			group by warehousestockitembatchid_bk) wsibsm on wsib.warehousestockitembatchid = wsibsm.warehousestockitembatchid_bk
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
		left join
			Landing.mend.wh_warehousestockitembatch_full wsib2 on wsib.warehousestockitembatchid = wsib2.id

-- Allocation

	-- lnd_stg_get_aux_alloc_order_line_erp_issue_measures (gen_wh_warehousestockitembatch_v)

	select top 1000 *
	from Landing.aux.alloc_order_line_erp_issue_measures

	-- lnd_stg_get_aux_alloc_order_line_erp_issue_measures_rec

	select top 1000 *
	from Landing.aux.alloc_order_line_erp_issue_measures_rec