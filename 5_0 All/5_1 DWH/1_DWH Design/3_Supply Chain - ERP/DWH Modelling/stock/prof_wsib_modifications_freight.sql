
-------------------------------------
-- WH SI B: Carriage Costs (Carriage - Duty) - Very few records
-------------------------------------

select count(*) over (), *
from Warehouse.stock.dim_wh_stock_item_batch
where local_carriage_unit_cost <> 0 or local_duty_unit_cost <> 0 -- order by local_carriage_unit_cost desc
-- where local_interco_carriage_unit_cost <> 0 order by local_interco_carriage_unit_cost desc
order by warehousestockitembatchid_bk desc

	select year(batch_stock_register_date), month(batch_stock_register_date), count(*)
	from Warehouse.stock.dim_wh_stock_item_batch
	where local_carriage_unit_cost <> 0 or local_duty_unit_cost <> 0 
	-- where local_interco_carriage_unit_cost <> 0 
	group by year(batch_stock_register_date), month(batch_stock_register_date)
	order by year(batch_stock_register_date), month(batch_stock_register_date)

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v


-------------------------------------
-- Receipt: No Carriage Costs 
-------------------------------------

select top 1000 *
from Landing.mend.gen_whship_receiptline_1_v

select top 1000 *
from Warehouse.rec.fact_receipt_line

select top 1000 *
from Warehouse.rec.fact_receipt_line_v


-------------------------------------
-- Invoice Rec: Carriage values at Invoice level - Not translated to Invoice Line level
-------------------------------------

select top 1000 *
from Warehouse.invrec.dim_invoice_reconciliation_v
where invoice_carriage_amount <> 0 
-- where invoice_discount_amount <> 0 
-- where invoice_import_duty_amount <> 0

select top 1000 *
from Warehouse.invrec.fact_invoice_reconciliation_line_v
