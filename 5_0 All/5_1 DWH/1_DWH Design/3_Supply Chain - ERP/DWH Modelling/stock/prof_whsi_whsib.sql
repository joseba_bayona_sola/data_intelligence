
drop table #whsi
drop table #whsib
drop table #whsib_s

drop table #bsm
drop table #bsm_s

--------------------------------------------

select warehousestockitemid, 
	warehouseid, stockitemid, productfamilyid, stockingmethod, 
	stockitemdescription stock_item_description, warehouse_name, id_string wh_stock_item_description, 
	availableqty + allocatedstockqty qty_received, actualstockqty qty_actual_stock, availableqty qty_available, 
	outstandingallocation qty_outstanding_allocation, allocatedstockqty qty_allocated_stock, onhold qty_on_hold, dueinqty qty_due_in, 
	null qty_registered, null qty_disposed, 
	stocked stocked_f
into #whsi
from Landing.mend.gen_wh_warehousestockitem_v 
where magentoProductID_int = 1083
order by stockitemdescription, warehouse_name

	select *
	from #whsi
	-- order by stock_item_description, warehouse_name
	order by qty_received desc

--------------------------------------------

select warehousestockitembatchid,	
	warehousestockitemid, stockitemid, productfamilyid, 
	stockitemdescription stock_item_description, warehouse_name, id_string wh_stock_item_description, batch_id, 
	fullyallocated, fullyIssued, autoadjusted, 
	receivedquantity qty_received, receivedquantity + registeredQuantity - allocatedquantity - disposedQuantity - onHoldQuantity qty_available, 
	allocatedquantity - issuedquantity qty_outstanding_allocation, allocatedquantity qty_allocated_stock, issuedquantity qty_issued_stock, onHoldQuantity qty_on_hold, 
	registeredQuantity qty_registered, disposedQuantity qty_disposed, 
	arrivedDate batch_arrived_date, confirmedDate batch_confirmed_date, stockRegisteredDate batch_stock_register_date, 
	productUnitCost local_product_unit_cost, totalUnitCost local_total_unit_cost, currency, exchangeRate, 
	receiptlineid, batchstockmovement_f, batchrepair_f
into #whsib
from Landing.mend.gen_wh_warehousestockitembatch_v
where magentoProductID_int = 1083
order by stockitemdescription, warehouse_name, batch_id

	select *
	from #whsib
	order by stock_item_description, warehouse_name, batch_id

	select warehousestockitembatchid, wh_stock_item_description, batch_id, 
		qty_received, qty_allocated_stock, qty_registered, qty_disposed, 
		batch_stock_register_date, 
		local_product_unit_cost, currency, 
		receiptlineid, batchstockmovement_f, batchrepair_f
	from #whsib
	-- where receiptlineid is null and currency is null -- and qty_received = 0
	-- where qty_disposed <> 0
	order by stock_item_description, warehouse_name, batch_id

	select warehousestockitembatchid, wh_stock_item_description, batch_id, 
		qty_received, qty_allocated_stock, qty_registered, qty_disposed, 
		batch_stock_register_date, 
		local_product_unit_cost, currency, 
		receiptlineid, batchstockmovement_f, batchrepair_f
	from #whsib
	-- where receiptlineid is null 
	where batchstockmovement_f = 1
	-- where batchrepair_f = 1
	order by batch_stock_register_date, stock_item_description, warehouse_name, batch_id

--------------------------------------------

select warehousestockitemid, stock_item_description, warehouse_name, 
	sum(qty_received) qty_received, sum(qty_available) qty_available, 
	sum(qty_outstanding_allocation) qty_outstanding_allocation, sum(qty_allocated_stock) qty_allocated_stock, sum(qty_on_hold) qty_on_hold, 
	sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed, 
	count(*) num_rows
into #whsib_s
from #whsib
group by warehousestockitemid, stock_item_description, warehouse_name
order by stock_item_description, warehouse_name

	select *
	from #whsib_s
	order by stock_item_description, warehouse_name

--------------------------------------------

---------- IMPORTANT-------------------
select t1.warehousestockitemid, t1.stock_item_description, t1.warehouse_name, 
	t1.qty_received, t2.qty_received, t1.qty_received - t2.qty_registered + t2.qty_disposed,
	t1.qty_available, t2.qty_available, 
	t1.qty_outstanding_allocation, t2.qty_outstanding_allocation, t1.qty_allocated_stock, t2.qty_allocated_stock, -- t1.qty_on_hold, t2.qty_on_hold, 
	t2.qty_registered, t2.qty_disposed
from 
		#whsi t1
	left join
		#whsib_s t2 on t1.warehousestockitemid = t2.warehousestockitemid
-- where t2.warehousestockitemid is null -- and t1.qty_received <> 0
-- where t2.warehousestockitemid is not null 
-- where t1.qty_on_hold <> 0
-- where t2.warehousestockitemid is not null and t1.qty_allocated_stock <> t2.qty_allocated_stock
-- where t2.warehousestockitemid is not null and t1.qty_received <> t2.qty_received 
-- where t2.warehousestockitemid is not null and t2.qty_received <> (t1.qty_received - t2.qty_registered + t2.qty_disposed)
-- where t2.warehousestockitemid is not null and t1.qty_available <> t2.qty_available
where t2.warehousestockitemid is not null and t1.qty_outstanding_allocation <> t2.qty_outstanding_allocation
order by t1.stock_item_description, t1.warehouse_name

--------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------

select batchstockmovementid, stockmovementid, 
	warehousestockitembatchid, warehousestockitemid, stockitemid, productfamilyid, 
	moveid, movelineid, batchstockmovement_id, 
	stockitemdescription stock_item_description, warehouse_name, id_string wh_stock_item_description, batch_id, 
	movementtype, movetype, _class, stockadjustment, fromstate, tostate,
	batchstockmovementtype, 
	qtyactioned, quantity, sum(quantity) over (partition by stockmovementid) quantity_sum,
	stockmovement_createdate, batchstockmovement_createdate
into #bsm
from Landing.mend.gen_wh_batchstockmovement_v
where magentoProductID_int = 1083
order by stockitemdescription, warehouse_name, batch_id

	select batchstockmovementid, stockmovementid, warehousestockitembatchid, 
		stock_item_description, warehouse_name, batch_id, 
		stockadjustment, batchstockmovementtype, 
		case
			when (batchstockmovementtype = 'Manual_Positive') then 1
			when (batchstockmovementtype = 'Manual_Negative') then -1
			when (stockadjustment = 'Positive') then 1
			when (stockadjustment = 'Negative') then 1
			else 0
		end qty_multiplier,
		quantity, 
		stockmovement_createdate, batchstockmovement_createdate
	from #bsm
	order by stock_item_description, warehouse_name, batch_id

	select count(*), count(distinct warehousestockitembatchid), count(distinct batch_id), count(distinct moveid), count(distinct movelineid), count(distinct batchstockmovement_id)
	from #bsm

	select *
	from #bsm
	-- where qtyactioned <> quantity
	where qtyactioned <> quantity_sum
	order by stockmovementid, batchstockmovementid

	select *
	from #bsm
	-- where movementtype = 'ERPADJ'
	order by movetype

	select movetype, movementtype, batchstockmovementtype, count(*)
	from #bsm
	group by movetype, movementtype, batchstockmovementtype
	order by movetype, movementtype, batchstockmovementtype

	select movetype, movementtype, stockadjustment, fromstate, tostate, batchstockmovementtype, count(*)
	from #bsm
	group by movetype, movementtype, stockadjustment, fromstate, tostate, batchstockmovementtype
	order by movetype, movementtype, stockadjustment, fromstate, tostate, batchstockmovementtype

	select stockadjustment, fromstate, tostate, batchstockmovementtype, count(*)
	from #bsm
	group by stockadjustment, fromstate, tostate, batchstockmovementtype
	order by stockadjustment, fromstate, tostate, batchstockmovementtype

	select moveid, count(*), count(distinct stockmovementid)
	from #bsm
	group by moveid
	order by count(distinct stockmovementid) desc, count(*) desc, moveid

	select movelineid, count(*), count(distinct stockmovementid)
	from #bsm
	group by movelineid
	order by count(distinct stockmovementid) desc, count(*) desc, movelineid

----------------------------------------------------------------

select warehousestockitembatchid, batch_id, qty_multiplier, sum(qty_multiplier * quantity) quantity
into #bsm_s
from
	(select batchstockmovementid, stockmovementid, warehousestockitembatchid, 
		stock_item_description, warehouse_name, batch_id, 
		stockadjustment, batchstockmovementtype, 
		case
			when (batchstockmovementtype = 'Manual_Positive') then 1
			when (batchstockmovementtype = 'Manual_Negative') then -1
			when (stockadjustment = 'Positive') then 1
			when (stockadjustment = 'Negative') then -1
			else 0
		end qty_multiplier,
		quantity, 
		stockmovement_createdate, batchstockmovement_createdate
	from #bsm) bsm
-- where qty_multiplier <> 0
group by warehousestockitembatchid, batch_id, qty_multiplier
order by warehousestockitembatchid, batch_id, qty_multiplier

	select count(*), count(distinct batch_id)
	from #bsm_s

	select warehousestockitembatchid, batch_id, 
		isnull(qty_registered, 0) qty_registered, isnull(qty_disposed, 0) qty_disposed, isnull(qty_zero, 0) qty_zero
	from
		(select warehousestockitembatchid, batch_id, 
			[1] qty_registered, [-1] qty_disposed, [0] qty_zero
		from 
				#bsm_s
			pivot
				(min(quantity) for 
				qty_multiplier in ([1], [-1], [0])) pvt) t

	---------- IMPORTANT-------------------
	select t1.warehousestockitembatchid, t1.wh_stock_item_description, t1.batch_id, 
		t1.qty_received, t1.qty_allocated_stock, 
		t1.qty_registered, t2.qty_registered, t1.qty_disposed, t2.qty_disposed, t2.qty_zero,
		t1.batch_stock_register_date, 
		t1.receiptlineid, t1.batchstockmovement_f
	from 
			#whsib t1
		inner join
			(select warehousestockitembatchid, batch_id, 
				isnull(qty_registered, 0) qty_registered, isnull(qty_disposed, 0)*-1 qty_disposed, isnull(qty_zero, 0) qty_zero
			from
				(select warehousestockitembatchid, batch_id, 
					[1] qty_registered, [-1] qty_disposed, [0] qty_zero
				from 
						#bsm_s
					pivot
						(min(quantity) for 
						qty_multiplier in ([1], [-1], [0])) pvt) t) t2 on t1.warehousestockitembatchid = t2.warehousestockitembatchid
	where t1.qty_registered <> t2.qty_registered
	-- where t1.qty_registered <> t2.qty_registered and t1.receiptlineid is null and t1.qty_received <> t2.qty_registered
	-- where t1.qty_disposed <> t2.qty_disposed 
	-- where t1.qty_disposed <> t2.qty_disposed and t1.qty_disposed + t2.qty_registered <> t2.qty_disposed

----------------------------------------------------------------

	---------- IMPORTANT-------------------
	select t1.warehousestockitembatchid, t1.stock_item_description, t1.warehouse_name, t1.batch_id, 
		t1.qty_received, t1.qty_allocated_stock, 
		t1.qty_registered, t1.qty_disposed, 
		t1.batch_stock_register_date, 
		t1.receiptlineid, t1.batchstockmovement_f, 
		t2.moveid, t2.movelineid, t2.batchstockmovement_id, t2.movetype, t2.movementtype, t2.stockadjustment, t2.batchstockmovementtype, 
		t2.quantity, 
		t2.qty_multiplier
	from 
			#whsib t1
		inner join
			(select *, 
				case
					when (batchstockmovementtype = 'Manual_Positive') then 1
					when (batchstockmovementtype = 'Manual_Negative') then -1
					when (stockadjustment = 'Positive') then 1
					when (stockadjustment = 'Negative') then -1
					else 0
				end qty_multiplier
			from #bsm) t2 on t1.warehousestockitembatchid = t2.warehousestockitembatchid
	-- where t1.receiptlineid is not null and qty_multiplier = 1
	-- where t1.receiptlineid is null and t1.qty_registered = 0 and t1.qty_received = t2.quantity
	where t1.receiptlineid is null and t1.qty_registered <> 0 and t1.qty_registered = t2.quantity
	-- where qty_multiplier = -0
	-- where t1.batch_id = 1010513
	-- order by t1.stock_item_description, t1.warehouse_name, t1.batch_id
	order by t1.batch_stock_register_date desc

	select t1.warehousestockitembatchid, t1.stock_item_description, t1.warehouse_name, t1.batch_id, 
		t1.qty_received, t1.qty_allocated_stock, 
		t1.qty_registered, t1.qty_disposed, 
		t1.batch_stock_register_date, 
		t1.receiptlineid, t1.batchstockmovement_f, 
		t2.moveid, t2.movelineid, t2.batchstockmovement_id, t2.movetype, t2.movementtype, t2.stockadjustment, t2.batchstockmovementtype, 
		t2.quantity, 
		t2.qty_multiplier
	from 
			#whsib t1
		inner join
			(select *, 
				case
					when (batchstockmovementtype = 'Manual_Positive') then 1
					when (batchstockmovementtype = 'Manual_Negative') then -1
					when (stockadjustment = 'Positive') then 1
					when (stockadjustment = 'Negative') then -1
					else 0
				end qty_multiplier
			from #bsm) t2 on t1.warehousestockitembatchid = t2.warehousestockitembatchid
		inner join
			(select movelineid, count(*) n1, count(distinct stockmovementid) n2
			from #bsm
			where movelineid <> ''
			group by movelineid
			having count(distinct stockmovementid) > 1) t3 on t2.movelineid = t3.movelineid
	order by t1.stock_item_description, t1.warehouse_name, t1.batch_id
