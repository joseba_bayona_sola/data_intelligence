
			drop table #current_wsib

			select id, batch_id
			into #current_wsib
			from openquery(MENDIX_DB_RESTORE,'
				select id, batch_id
				from public."inventory$warehousestockitembatch"')

			select top 1000 *
			from #current_wsib

			select top 1000 count(*)
			from #current_wsib

			select top 1000 wsib2.*, wsib1.*
			from 
					Warehouse.stock.dim_wh_stock_item_batch_v wsib1
				left join
					#current_wsib wsib2 on wsib1.warehousestockitembatchid_bk = wsib2.id
			-- where wsib1.batch_id <> wsib2.batch_id
			where wsib2.batch_id is null -- and qty_issued_stock <> 0
			-- order by wsib1.batch_stock_register_date desc
			order by wsib1.product_id_magento, wsib1.product_family_name, wsib1.stock_item_description

			select top 1000 wsib2.*, wsib1.*
			from 
					Warehouse.stock.dim_wh_stock_item_batch_rec_v wsib1
				left join
					#current_wsib wsib2 on wsib1.warehousestockitembatchid_bk = wsib2.id
			-- where wsib1.batch_id <> wsib2.batch_id
			where wsib2.batch_id is null
			-- order by wsib1.batch_stock_register_date desc
			order by wsib1.product_id_magento, wsib1.product_family_name, wsib1.stock_item_description

			select top 1000 wsib2.*, wsibm.*
			from 
					Warehouse.stock.dim_wh_stock_item_batch_v wsib1
				inner join
					Warehouse.stock.fact_wh_stock_item_batch_movement_v wsibm on wsib1.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk
				left join
					#current_wsib wsib2 on wsib1.warehousestockitembatchid_bk = wsib2.id
			-- where wsib1.batch_id <> wsib2.batch_id
			where wsib2.batch_id is null
			-- order by wsib1.batch_stock_register_date desc
			order by wsibm.product_id_magento, wsibm.product_family_name, wsibm.stock_item_description

			-----------------------------------------

			select top 1000 wsib2.*, wsib1.*
			from 
					Landing.mend.gen_wh_warehousestockitembatch_v wsib1
				left join
					#current_wsib wsib2 on wsib1.warehousestockitembatchid = wsib2.id
			-- where wsib1.batch_id <> wsib2.batch_id
			where wsib2.batch_id is null -- and qty_issued_stock <> 0
			-- order by wsib1.batch_stock_register_date desc
			order by wsib1.product_id_magento, wsib1.product_family_name, wsib1.stock_item_description

				select count(*)
				from Landing.mend.gen_wh_warehousestockitembatch_v