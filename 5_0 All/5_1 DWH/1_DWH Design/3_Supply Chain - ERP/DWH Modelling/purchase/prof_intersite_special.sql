
select *
from Warehouse.po.dim_intersite_transfer_v
where wholesale_customer_name is null
order by created_date desc

	select warehouse_name_from, warehouse_name_to, wholesale_customer_name, count(*)
	from Warehouse.po.dim_intersite_transfer_v
	group by warehouse_name_from, warehouse_name_to, wholesale_customer_name
	order by warehouse_name_from, warehouse_name_to, wholesale_customer_name


select intransitstockitembatchid_bk, 
	warehouse_name_from, warehouse_name_to, intersite_transfer_number, created_date, 
	purchase_order_number_t, supplier_name_t_po, po_type_name_t, 
	purchase_order_number_s, supplier_name_s_po, po_type_name_s, 
	order_no_erp, order_date_sync, wholesale_customer_name, 
	product_id_magento, product_family_name, sku, 
	qty_sent, qty_remaining, qty_registered, 
	local_product_unit_cost, global_product_unit_cost, currency_code
from Warehouse.stock.dim_intransit_stock_item_batch_v isib
where qty_remaining > 0
	and warehouse_name_from is null
order by intersite_transfer_number

	select warehouse_name_from, warehouse_name_to, wholesale_customer_name, count(*)
	from Warehouse.stock.dim_intransit_stock_item_batch_v isib
	where qty_remaining > 0
	group by warehouse_name_from, warehouse_name_to, wholesale_customer_name
	order by warehouse_name_from, warehouse_name_to, wholesale_customer_name


select *
from Landing.mend.inter_transferheader
where transfernum = 'TR00004013'

select *
from Landing.mend.gen_inter_transferheader_v
where transfernum = 'TR00004013'

select *
from Warehouse.gen.dim_warehouse_v