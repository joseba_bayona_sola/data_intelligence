
-- erp_po_inter

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_inter
order by createddate desc
-- order by totalprice desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_inter
	where num_rep <> 1 -- 8
	-- where ordernumber_t is null -- 8
	-- where ordernumber_s is null -- 322
	-- where totalremaining <> 0 -- 903
	order by createddate desc

	select allocatedstrategy, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_inter
	group by allocatedstrategy
	order by allocatedstrategy

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_inter
	-- where allocatedstrategy = 'Manual' -- Manual - AutomaticBackToBack - AutomaticFromStock
	order by createddate desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_inter
	where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')
	order by createddate desc

-- erp_po_inter_bh

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_inter_bh
order by createddate desc
-- order by totalprice desc

	select *
	from DW_GetLenses_jbs.dbo.erp_po_inter_bh
	-- where intransitbatchheaderid is null -- 385
	-- where num_rep <> 1 -- 24
	-- where num_rep = 1 and num_rep2 <> 2044
	-- where ordernumber_t is null -- 8
	-- where ordernumber_s is null -- 350
	where createddate_ibh is null -- 1088
	order by createddate desc


	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_inter_bh
	where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')
	order by createddate desc

-- erp_po_inter_sib

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_inter_sib
order by createddate desc

	select *
	from DW_GetLenses_jbs.dbo.erp_po_inter_sib
	-- where intransitbatchheaderid is null -- 385
	-- where intransitstockitembatchid is null -- 385
	
	-- where num_rep <> 1 -- 24
	-- where num_rep = 1 and num_rep2 <> 2044
	-- where ordernumber_t is null -- 8
	-- where ordernumber_s is null -- 350
	where createddate_ibh is null -- 45586
	order by createddate desc


	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_inter_sib
	where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')
	order by createddate desc

		select top 1000 isib.*, si.magentoProductID, si.name, si.packSize, si.stockitemdescription
		from 
				DW_GetLenses_jbs.dbo.erp_po_inter_sib isib
			left join
				Landing.mend.gen_prod_stockitem_v si on isib.stockitemid = si.stockitemid
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')
		order by createddate desc