
-------------------------------- Purchase Order ----------------------------------

-- Physically Deleted Orders - PO
select po.*
from
		Landing.mend.purc_purchaseorder_aud po
	inner join
		(select id 
		from Landing.mend.purc_purchaseorder_aud
		except 
		select id 
		from Landing.mend.purc_purchaseorder) po2 on po.id = po2.id

select po.id purchaseorderid, w.warehouseid, s.supplier_id, 
	w.code, w.name warehouse_name, 
	s.supplierID, s.supplierName,
	po.ordernumber, po.source, po.status, po.potype,
	po.leadtime, 
	po.itemcost, po.totalprice, po.formattedprice, 
	po.createddate, po.duedate, po.receiveddate, 
	po.approveddate, po.approvedby, po.confirmeddate, po.confirmedby, po.submitteddate, po.submittedby, po.completeddate, po.completedby
from
		Landing.mend.purc_purchaseorder_aud po
	inner join
		(select id 
		from Landing.mend.purc_purchaseorder_aud
		except 
		select id 
		from Landing.mend.purc_purchaseorder) po2 on po.id = po2.id
	inner join
		Landing.mend.purc_purchaseorder_warehouse_aud pow on po.id = pow.purchaseorderid
	inner join	
		Landing.mend.gen_wh_warehouse_v w on pow.warehouseid = w.warehouseid
	left join
		Landing.mend.purc_purchaseorder_supplier_aud pos on po.id = pos.purchaseorderid
	left join
		Landing.mend.gen_supp_supplier_v s on pos.supplierid = s.supplier_id
order by po.createddate desc


-- Physically Deleted Orders - POL

select po.purchaseorderid, 
	po.code, po.warehouse_name, po.supplierID, po.supplierName,
	po.ordernumber, po.source, po.status, po.potype, po.createddate, po.totalprice, 
	pol.*, pol.lineprice / pol.quantity
from
		Landing.mend.purc_purchaseorderline_aud pol
	inner join
		(select id 
		from Landing.mend.purc_purchaseorderline_aud
		except 
		select id 
		from Landing.mend.purc_purchaseorderline) pol2 on pol.id = pol2.id
	inner join
		Landing.mend.purc_purchaseorderline_purchaseorderlineheader_aud polpolh on pol.id = polpolh.purchaseorderlineid 
	inner join
		Landing.mend.purc_purchaseorderlineheader_purchaseorder_aud polhpo on polpolh.purchaseorderlineheaderid = polhpo.purchaseorderlineheaderid
	inner join
		Landing.mend.gen_purc_purchaseorder_v po on polhpo.purchaseorderid = po.purchaseorderid
order by po.createddate desc

-- duplicated purchase_order_number
select *
from
	(select count(*) over (partition by ordernumber) num_rep, *
	from Landing.mend.gen_purc_purchaseorder_v) po
where num_rep > 1
order by createddate, ordernumber

select *
from Landing.mend.gen_purc_purchaseorder_v
where warehouseid is null or supplier_id is null


-------------------------------- Purchase Order Line ----------------------------------

-- Missing Child Records
	
	-- PO without POLH
	
	select po.purchaseorderid, 
		po.code, po.warehouse_name, 
		po.supplierID, po.supplierName,
		po.ordernumber, po.source, po.status, po.potype,
		po.leadtime, 
		po.itemcost, po.totalprice, po.formattedprice, 
		po.createddate
	from 
			Landing.mend.gen_purc_purchaseorder_v po
		left join
			(select distinct purchaseorderid
			from Landing.mend.gen_purc_purchaseorderlineheader_v) polh on po.purchaseorderid = polh.purchaseorderid
	where polh.purchaseorderid is null
		and totalprice <> 0
	order by po.createddate desc

		select po.status, count(*)
		from 
				Landing.mend.gen_purc_purchaseorder_v po
			left join
				(select distinct purchaseorderid
				from Landing.mend.gen_purc_purchaseorderlineheader_v) polh on po.purchaseorderid = polh.purchaseorderid
		where polh.purchaseorderid is null
		group by po.status
		order by po.status

	-- POLH without POL

	select polh.purchaseorderlineheaderid, polh.purchaseorderid, 
		polh.code, polh.warehouse_name, polh.supplierID, polh.supplierName, 
		polh.ordernumber, polh.source, polh.status, polh.potype, polh.createddate, polh.totalprice,
		polh.status_lh, polh.problems, 
		polh.lines, polh.items, polh.total_price_lh
	from
			Landing.mend.gen_purc_purchaseorderlineheader_v polh
		left join 
			(select distinct purchaseorderlineheaderid
			from Landing.mend.gen_purc_purchaseorderline_v) pol on polh.purchaseorderlineheaderid = pol.purchaseorderlineheaderid
	where pol.purchaseorderlineheaderid is null
	order by polh.createddate desc

-- Missing Parent Records

	-- POLH without PO
	select polh.*
	from 
			Landing.mend.purc_purchaseorderlineheader polh
		left join
			Landing.mend.purc_purchaseorderlineheader_purchaseorder polhpo on polh.id = polhpo.purchaseorderlineheaderid 
		left join
			Landing.mend.purc_purchaseorder po on polhpo.purchaseorderid = po.id
	where polhpo.purchaseorderlineheaderid is null or po.id is null

	-- POL without POLH
	select pol.*
	from 
			Landing.mend.purc_purchaseorderline pol
		left join
			Landing.mend.purc_purchaseorderline_purchaseorderlineheader polpolh on pol.id = polpolh.purchaseorderlineid 
		left join
			Landing.mend.purc_purchaseorderlineheader polh on polpolh.purchaseorderlineheaderid = polh.id
	where polpolh.purchaseorderlineid is null or polh.id is null


-- Missing Relations - Supplier Price

	select top 1000 purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	where supplierpriceid is null
		-- and totalprice <> 0
		and status_lh = 'VALID'
	order by createddate desc

		select status, count(*), count(distinct purchaseorderid)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		where supplierpriceid is null
		group by status
		order by status

		select status_lh, count(*), count(distinct purchaseorderid)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		where supplierpriceid is null
		group by status_lh
		order by status_lh

		select problems, count(*), count(distinct purchaseorderid)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		where supplierpriceid is null
		group by problems
		order by problems

	drop table #gen_purc_purchaseorderline_v

	select purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh, 
		quantity, lineprice
	into #gen_purc_purchaseorderline_v
	from Landing.mend.gen_purc_purchaseorderline_v pol
	where pol.supplierpriceid is null

	select rlpol.receiptlineid,
		pol.purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh, 
		quantity, lineprice
	from 
			#gen_purc_purchaseorderline_v pol
		left join
			Landing.mend.whship_receiptline_purchaseorderline rlpol on pol.purchaseorderlineid = rlpol.purchaseorderlineid
	where rlpol.receiptlineid is not null
	order by pol.createddate desc

-- Missing Relations - Stock Item

	select top 1000 purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh, 
		quantity, lineprice
	from Landing.mend.gen_purc_purchaseorderline_v
	where stockitemid is null
	order by createddate desc

		select status, count(*), count(distinct purchaseorderid)
		from Landing.mend.gen_purc_purchaseorderline_v
		where stockitemid is null
		group by status
		order by status

		select distinct ordernumber
		from Landing.mend.gen_purc_purchaseorderline_v
		where stockitemid is null

	drop table #gen_purc_purchaseorderline_v

	select purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh, 
		quantity, lineprice
	into #gen_purc_purchaseorderline_v
	from Landing.mend.gen_purc_purchaseorderline_v pol
	where pol.stockitemid is null

	select rlpol.receiptlineid,
		pol.purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, 
		code, warehouse_name, supplierID, supplierName, 
		ordernumber, source, status, potype, createddate, totalprice,
		status_lh, problems, 
		lines, items, total_price_lh, 
		quantity, lineprice
	from 
			#gen_purc_purchaseorderline_v pol
		left join
			Landing.mend.whship_receiptline_purchaseorderline rlpol on pol.purchaseorderlineid = rlpol.purchaseorderlineid
	where rlpol.receiptlineid is not null
	order by pol.createddate desc
