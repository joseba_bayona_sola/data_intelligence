
select count(*)
from Warehouse.po.dim_purchase_order

select *
from Warehouse.po.dim_purchase_order_v
where purchaseorderid_bk in 
	(select po.id
	from
			Landing.mend.purc_purchaseorder_aud po
		inner join
			(select id 
			from Landing.mend.purc_purchaseorder_aud
			except 
			select id 
			from Landing.mend.purc_purchaseorder) po2 on po.id = po2.id)
order by created_date

----------------------------------------------------------

select *
from Warehouse.po.fact_purchase_order_line_v 
where purchaseorderlineid_bk in 
	(select pol.id
	from
			Landing.mend.purc_purchaseorderline_aud pol
		inner join
			(select id 
			from Landing.mend.purc_purchaseorderline_aud
			except 
			select id 
			from Landing.mend.purc_purchaseorderline) pol2 on pol.id = pol2.id)
order by created_date