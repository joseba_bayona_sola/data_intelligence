
-- Check new attributes to add in plannedrequirement: Only futurmasterpass (not in all rows)
-- Understand the data between plannedrequirement and plannedrequirement_aud: 
	-- It seems that the right one is plannedrequirement where we have Completed, Pending, Cancelled requirements
	-- Check Completed rows in AUD not in normal
-- Understand the relationship between plannedrequirement and PO - POL: 
	-- 2 ways: preq_plannedrequirement.ordernumber / preq_plannedrequirement_purchaseorderline (better: to POL)
	-- A POL can have many plannedrequirement
	-- POL where source = 'FuturMaster' and not relation to plannedrequirement
	-- POL where source <> 'FuturMaster' and relation to plannedrequirement

-- New attributes from POL: 
	-- quantityordered numeric(28,8), quantityoverdelivery numeric(28,8),
	-- quantitycxd numeric(28,8), quantityreturned numeric(28,8),

select top 1000 *
from Warehouse.po.fact_purchase_order_line_v
where po_source_name = 'FuturMaster' and purchase_order_number = 'P00069140'
order by purchaseorderlineid_bk desc

SELECT top 1000 id,
	fmorderref, fmfilename, fmfiledate, fmyear, fmmonth, 
	releasedate, duedate, 
	stockitemdescription,
	ordernumber, planner, plannedstatus, futurmasterstatus, reserved, -- futurmasterpass
	orderedquantity, unitprice, 
	createddate, changeddate, system_changedby, 
	idETLBatchRun, ins_ts
FROM Landing.mend.preq_plannedrequirement
where ordernumber = 'P00052328'
order by id desc

select top 1000 pq.*
from 
		Warehouse.po.fact_purchase_order_line_v pol
	inner join
		Landing.mend.preq_plannedrequirement_purchaseorderline pqpol on pol.purchaseorderlineid_bk = pqpol.purchaseorderlineid
	inner join
		Landing.mend.preq_plannedrequirement pq on pqpol.plannedrequirementid = pq.id
where pol.po_source_name = 'FuturMaster' and pol.purchase_order_number = 'P00052328'

------------------------------

select top 1000 *
from Landing.mend.preq_plannedrequirement
order by id

	select top 1000 count(*)
	from Landing.mend.preq_plannedrequirement

	select top 1000 count(*), count(distinct plannedrequirementid), count(distinct purchaseorderlineid)
	from Landing.mend.preq_plannedrequirement_purchaseorderline

	select top 1000 fmyear, fmmonth, count(*)
	from Landing.mend.preq_plannedrequirement
	group by fmyear, fmmonth
	order by fmyear, fmmonth

	select top 1000 year(fmfiledate), month(fmfiledate), count(*)
	from Landing.mend.preq_plannedrequirement
	where plannedstatus = 'Completed' -- Completed - Pending - Cancelled
	group by year(fmfiledate), month(fmfiledate)
	order by year(fmfiledate), month(fmfiledate)

	select top 1000 plannedstatus, count(*)
	from Landing.mend.preq_plannedrequirement
	-- where id in (select distinct plannedrequirementid from Landing.mend.preq_plannedrequirement_purchaseorderline)
	group by plannedstatus
	order by plannedstatus

	select top 1000 futurmasterstatus, count(*)
	from Landing.mend.preq_plannedrequirement
	group by futurmasterstatus
	order by futurmasterstatus

		select top 1000 count(*) over (), pq1.*, pol.*
		from 
				Landing.mend.preq_plannedrequirement_aud pq1
			left join
				Landing.mend.preq_plannedrequirement pq2 on pq1.id = pq2.id
			inner join
				Landing.mend.preq_plannedrequirement_purchaseorderline_aud pqpol on pq1.id = pqpol.plannedrequirementid
			inner join
				Warehouse.po.fact_purchase_order_line_v pol on pqpol.purchaseorderlineid = pol.purchaseorderlineid_bk
		where pq1.plannedstatus = 'Completed' and pq2.id is null


------------------------------

select top 1000 count(*) over (), pol.*
from 
		Warehouse.po.fact_purchase_order_line_v pol
	inner join
		Landing.mend.preq_plannedrequirement_purchaseorderline pqpol on pol.purchaseorderlineid_bk = pqpol.purchaseorderlineid
order by pol.purchaseorderlineid_bk desc

	select top 1000 count(*), count(distinct purchaseorderlineid)
	from Landing.mend.preq_plannedrequirement_purchaseorderline

	select top 1000 purchaseorderlineid, count(*)
	from Landing.mend.preq_plannedrequirement_purchaseorderline
	group by purchaseorderlineid
	order by count(*) desc

select top 1000 pol.*, pq.*
from 
		Warehouse.po.fact_purchase_order_line_v pol
	inner join
		Landing.mend.preq_plannedrequirement_purchaseorderline pqpol on pol.purchaseorderlineid_bk = pqpol.purchaseorderlineid
	inner join
		Landing.mend.preq_plannedrequirement pq on pqpol.plannedrequirementid = pq.id
where pol.purchaseorderlineid_bk = 4503599629549473
order by pol.purchaseorderlineid_bk desc

-- 

select top 1000 count(*) over (), pol.*
from 
		Warehouse.po.fact_purchase_order_line_v pol
	left join
		(select distinct purchaseorderlineid from Landing.mend.preq_plannedrequirement_purchaseorderline) pqpol on pol.purchaseorderlineid_bk = pqpol.purchaseorderlineid
-- where pol.po_source_name <> 'FuturMaster' -- 1790
where pol.po_source_name = 'FuturMaster' and pqpol.purchaseorderlineid is null -- 36649
order by pol.purchaseorderlineid_bk desc

----------------------------------------------------------------


select top 1000 count(*) over (), count(*) over (partition by id) num_rep, 
	count(*) over (partition by preq_wh.warehouseid, preq_si.stockitemid, preq_supp.supplierid, preq.fmorderref, preq_pol.purchaseorderlineid) num_rep2, 
	*
from 
		Landing.mend.preq_plannedrequirement preq
	left join 
		Landing.mend.preq_plannedreq_warehouse preq_wh on preq.id = preq_wh.plannedrequirementid
	left join 
		Landing.mend.preq_plannedreq_stockitem preq_si on preq.id = preq_si.plannedrequirementid
	left join
		Landing.mend.preq_plannedreq_supplier preq_supp on preq.id = preq_supp.plannedrequirementid
	left join
		Landing.mend.preq_plannedrequirement_purchaseorderline preq_pol on preq.id = preq_pol.plannedrequirementid
where plannedstatus <> 'Cancelled'
order by num_rep2 desc

----------------------------------------------------------------

select top 1000  count(*) over (), id, 
	po_lineID, 
	quantity, quantityordered, quantityoverdelivery, 
	quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantitycxd, 
	quantityExWorks, quantityRejected, quantityreturned,
	createdDate, changedDate
from Landing.mend.purc_purchaseorderline
where quantity <> quantityordered + quantityoverdelivery -- 143816
-- where quantityCancelled <> quantitycxd -- 43629
-- where quantityreturned <> 0 -- 0
order by id desc

	select year(createdDate), month(createdDate), count(*)
	from Landing.mend.purc_purchaseorderline
	-- where quantity <> quantityordered + quantityoverdelivery -- 143816
	where quantityCancelled <> quantitycxd -- 43629
	group by year(createdDate), month(createdDate)
	order by year(createdDate), month(createdDate)
