
-- erp_po_po

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_po
order by createddate desc
-- order by totalprice desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_po
	-- where num_rep <> 1 -- 124
	-- where num_rep2 <> 1 -- 41
	where itemcost <> totalprice -- 0
	order by createddate desc

	select source, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_po
	group by source
	order by source

	select potype, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_po
	group by potype
	order by potype

		select source, potype, count(*)
		from DW_GetLenses_jbs.dbo.erp_po_po
		group by source, potype
		order by source, potype

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_po
	group by status
	order by status

	select approvedby, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_po
	group by approvedby
	order by approvedby


	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_po
	-- where source = 'Manual' -- Back_To_Back - Manual - OverDelivery
	where potype in ('Transfer', 'Supply') -- Retail - Wholesale - Transfer - Supply - Stock - Retail_Transfer - Retail_Supply 
	-- where status = 'Completed' -- Drafted - Approved - Submitted - Confirmed - Received - Backordered - Completed - Cancelled
	order by createddate desc

------------------------------------------------------------------------------

-- erp_po_polh

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_polh
order by createddate desc

	select count(*)
	from DW_GetLenses_jbs.dbo.erp_po_polh

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_polh
	-- where purchaseorderlineheaderid is null -- 262
	-- where purchaseorderlineheaderid is null and status not in ('Cancelled') -- 19
	
	where totalprice <> sum_total_price_lh -- 41
	order by createddate desc

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_polh
	where purchaseorderlineheaderid is null
	group by status
	order by status

	select status_lh, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_polh
	group by status_lh
	order by status_lh

		select status_lh, status, count(*)
		from DW_GetLenses_jbs.dbo.erp_po_polh
		group by status_lh, status
		order by status_lh, status

	select problems, count(*)
	from DW_GetLenses_jbs.dbo.erp_po_polh
	group by problems
	order by problems

		select status_lh, problems, count(*)
		from DW_GetLenses_jbs.dbo.erp_po_polh
		group by status_lh, problems
		order by status_lh, problems


	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_polh
	-- where source = 'Manual' -- Back_To_Back - Manual - OverDelivery
	-- where potype = 'Stock' -- Retail - Wholesale - Transfer - Supply - Stock - Retail_Transfer - Retail_Supply 
	where status = 'Completed' -- Drafted - Approved - Submitted - Confirmed - Received - Backordered - Completed - Cancelled
	order by createddate desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_polh
	-- where status_lh = 'HAS_WARNINGS' -- VALID - HAS_ERRORS - HAS_WARNINGS
	where problems = 'MOQ Violation' -- None - Lead Time Too Long - MOQ Violation - MOQ Violation, Lead Time Too Long - Missing Price - Expired Price - Expired Price Lead Time Too Long
	order by createddate desc

	-------------------------------------------------------

	select top 1000 polh.*, pfpssp.magentoProductID, pfpssp.name, pfpssp.size
	from 
			DW_GetLenses_jbs.dbo.erp_po_polh polh
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpssp on polh.supplierpriceid = pfpssp.supplierpriceid
	where pfpssp.supplierpriceid is null -- 585
	order by createddate desc

	select count(*)
	from 
			DW_GetLenses_jbs.dbo.erp_po_polh polh
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpssp on polh.supplierpriceid = pfpssp.supplierpriceid

------------------------------------------------------------------------------

-- erp_po_pol

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_po_pol
order by createddate desc

	select count(*)
	from DW_GetLenses_jbs.dbo.erp_po_pol

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_pol
	-- where purchaseorderlineheaderid is null -- 262
	-- where purchaseorderlineheaderid is null and status not in ('Cancelled') -- 19
	-- where purchaseorderlineid is null -- 273
	-- where purchaseorderlineid is null and purchaseorderlineheaderid is not null -- 11
	
	where lines <> sum_lines -- 7
	-- where total_price_lh <> sum_lineprice -- 221
	order by createddate desc

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_po_pol
	-- where source = 'Manual' -- Back_To_Back - Manual - OverDelivery
	-- where potype = 'Stock' -- Retail - Wholesale - Transfer - Supply - Stock - Retail_Transfer - Retail_Supply 
	where status = 'Completed' -- Drafted - Approved - Submitted - Confirmed - Received - Backordered - Completed - Cancelled
	order by createddate desc

	-------------------------------------------------------

	select top 1000 pol.*, si.magentoProductID, si.name, si.packSize, si.stockitemdescription
	from 
			DW_GetLenses_jbs.dbo.erp_po_pol pol
		left join
			Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	where si.stockitemid is null -- 516
	order by createddate desc

	select count(*)
	from 
			DW_GetLenses_jbs.dbo.erp_po_pol pol
		left join
			Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid