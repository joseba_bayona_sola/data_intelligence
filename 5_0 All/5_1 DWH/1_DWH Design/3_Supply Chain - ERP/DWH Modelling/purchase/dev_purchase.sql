
drop table DW_GetLenses_jbs.dbo.erp_po_po
drop table DW_GetLenses_jbs.dbo.erp_po_polh
drop table DW_GetLenses_jbs.dbo.erp_po_pol

-- DW_GetLenses_jbs.dbo.erp_po_po

select count(*) over (partition by ordernumber) num_rep, count(*) over (partition by ordernumber, supplier_id) num_rep2, -- duplicated po on ordernumber
	purchaseorderid, warehouseid, supplier_id, 
	code, warehouse_name, 
	supplierID, supplierName,
	ordernumber, source, status, potype,
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby
	-- missing currency (related with supplier / exchange rate)
into DW_GetLenses_jbs.dbo.erp_po_po
from Landing.mend.gen_purc_purchaseorder_v 

-- DW_GetLenses_jbs.dbo.erp_po_polh

select polh.purchaseorderlineheaderid, po.purchaseorderid, polh.productfamilyid, polh.packsizeid, polh.supplierpriceid,
	po.ordernumber, po.source, po.status, po.potype, po.createddate,
	po.totalprice,

	polh.status_lh, polh.problems, 
	polh.lines, polh.items, polh.total_price_lh, 
	sum(polh.total_price_lh) over (partition by polh.purchaseorderid) sum_total_price_lh

into DW_GetLenses_jbs.dbo.erp_po_polh
from
		DW_GetLenses_jbs.dbo.erp_po_po po
	left join
		Landing.mend.gen_purc_purchaseorderlineheader_v polh on po.purchaseorderid = polh.purchaseorderid

-- DW_GetLenses_jbs.dbo.erp_po_pol

select pol.purchaseorderlineid, polh.purchaseorderlineheaderid, polh.purchaseorderid, polh.supplierpriceid, pol.stockitemid, 
	polh.ordernumber, polh.source, polh.status, polh.potype, polh.createddate,
	polh.totalprice,

	polh.status_lh, polh.problems, 
	polh.lines, polh.items, polh.total_price_lh, 

	pol.po_lineID, 
	pol.quantity, pol.quantityReceived, pol.quantityInTransit, pol.quantityPlanned, pol.quantityOpen, pol.quantityUnallocated, pol.quantityCancelled, pol.quantityExWorks, pol.quantityRejected, -- meaning of quantities
	pol.lineprice, 
	pol.backtobackduedate,

	count(pol.purchaseorderlineid) over (partition by pol.purchaseorderlineheaderid) sum_lines,
	sum(pol.lineprice) over (partition by pol.purchaseorderlineheaderid) sum_lineprice
into DW_GetLenses_jbs.dbo.erp_po_pol
from 
		DW_GetLenses_jbs.dbo.erp_po_polh polh
	left join
		Landing.mend.gen_purc_purchaseorderline_v pol on polh.purchaseorderlineheaderid = pol.purchaseorderlineheaderid

