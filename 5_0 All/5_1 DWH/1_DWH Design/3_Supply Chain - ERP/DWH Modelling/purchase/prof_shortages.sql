
select *
from DW_GetLenses_jbs.dbo.erp_shh
order by num_rep desc, wholesale, code_src, code_dest, supplierID, supplierName, packsizeid

	select count(*)
	from DW_GetLenses_jbs.dbo.erp_shh

	select shh.*, pfps.magentoProductID, pfps.name, pfps.size
	from 
			DW_GetLenses_jbs.dbo.erp_shh shh
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on shh.packsizeid = pfps.packsizeid
	where wholesale = 0 and warehouse_name_dest = 'Girona' -- and magentoProductID = '1089'
	-- order by num_rep desc, wholesale, code_src, code_dest, supplierID, supplierName, pfps.magentoProductID
	order by pfps.magentoProductID, wholesale, code_src, code_dest, supplierID, supplierName, shortageheaderid


----------------------------------------------------

select *
from DW_GetLenses_jbs.dbo.erp_sh
order by shortage_id desc

select *
from DW_GetLenses_jbs.dbo.erp_sh
-- where num_rep2 > 1
where shortage_id is not null
order by num_rep2 desc, wholesale, code_src, code_dest, supplierID, supplierName, packsizeid, shortage_id

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_sh
	group by status
	order by status

	select sh.*, pfps.magentoProductID, pfps.name, pfps.size
	from 
			DW_GetLenses_jbs.dbo.erp_sh sh
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on sh.packsizeid = pfps.packsizeid
	where wholesale = 0 and warehouse_name_dest = 'Girona' and magentoProductID = '1089' 
	order by shortage_id desc


----------------------------------------------------

select *
from DW_GetLenses_jbs.dbo.erp_olsh
order by shortage_id desc

select *
from DW_GetLenses_jbs.dbo.erp_olsh
-- where num_rep2 > 1
-- where shortage_id is null
where orderlineshortageid is null and shortage_id is not null
order by num_rep2 desc, wholesale, code_src, code_dest, supplierID, supplierName, packsizeid, shortage_id

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_olsh
	group by status
	order by status

	select orderlineshortagetype, count(*)
	from DW_GetLenses_jbs.dbo.erp_olsh
	group by orderlineshortagetype
	order by orderlineshortagetype

	select sh.*, pfps.magentoProductID, pfps.name, pfps.size
	from 
			DW_GetLenses_jbs.dbo.erp_olsh sh
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfps on sh.packsizeid = pfps.packsizeid
	-- where wholesale = 0 and warehouse_name_dest = 'Girona' and magentoProductID = '1089' 
	where ordernumbers in ('17000415739', '17000415923')
	-- order by shortage_id desc
	order by ordernumbers desc
