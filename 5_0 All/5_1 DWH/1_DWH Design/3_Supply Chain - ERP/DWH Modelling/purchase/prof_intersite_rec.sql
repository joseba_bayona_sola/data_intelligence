
select top 1000 rl.receiptlineid, rl.receiptid, rl.warehousestockitembatchid,
	rl.shipment_ID, rl.shipmentType, rl.warehouse_name, rl.supplierName, 
	rl.receipt_ID, rl.status, rl.syncstatus, rl.orderNumber, rl.createdDate,
	pol.*
from 
		DW_GetLenses_jbs.dbo.erp_po_pol pol
	inner join
		(select distinct purchaseorderid_t
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_t
	left join
		DW_GetLenses_jbs.dbo.erp_rec_recl rl on pol.purchaseorderlineid = rl.purchaseorderlineid
order by pol.createddate desc


select top 1000 rl.receiptlineid, rl.receiptid, rl.warehousestockitembatchid,
	rl.shipment_ID, rl.shipmentType, rl.warehouse_name, rl.supplierName, 
	rl.receipt_ID, rl.status, rl.syncstatus, rl.orderNumber, rl.createdDate, rl.quantityreceived,
	pol.*
from 
		DW_GetLenses_jbs.dbo.erp_po_pol pol
	inner join
		(select distinct purchaseorderid_s
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_s
	left join
		DW_GetLenses_jbs.dbo.erp_rec_recl rl on pol.purchaseorderlineid = rl.purchaseorderlineid
order by pol.createddate desc, pol.purchaseorderlineid