
drop table DW_GetLenses_jbs.dbo.erp_po_inter
drop table DW_GetLenses_jbs.dbo.erp_po_inter_bh
drop table DW_GetLenses_jbs.dbo.erp_po_inter_sib


-- DW_GetLenses_jbs.dbo.erp_po_inter

select count(*) over (partition by transfernum) num_rep,
	transferheaderid, purchaseorderid_s, purchaseorderid_t, th.orderid,
	transfernum, allocatedstrategy, 
	totalremaining,
	th.createddate, 
	code_t, warehouse_name_t, supplierID_t, supplierName_t, ordernumber_t,
	code_s, warehouse_name_s, supplierID_s, supplierName_s, ordernumber_s, 
	o.incrementID, o.createdDate createddate_oh, o._type, o.orderStatus 
into DW_GetLenses_jbs.dbo.erp_po_inter
from 
		Landing.mend.gen_inter_transferheader_v th
	left join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid


-- DW_GetLenses_jbs.dbo.erp_po_inter_bh

select th.num_rep,
	count(*) over (partition by th.transfernum) num_rep2,
	thbh.intransitbatchheaderid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, th.orderid,
	th.transfernum, th.allocatedstrategy, 
	th.totalremaining,
	th.createddate, 
	th.code_t, th.warehouse_name_t, th.supplierID_t, th.supplierName_t, th.ordernumber_t,
	th.code_s, th.warehouse_name_s, th.supplierID_s, th.supplierName_s, th.ordernumber_s, 
	th.incrementID, th.createddate_oh, th._type, th.orderStatus, 
	thbh.createddate_ibh
into DW_GetLenses_jbs.dbo.erp_po_inter_bh
from 
		DW_GetLenses_jbs.dbo.erp_po_inter th
	left join
		Landing.mend.gen_inter_intransitbatchheader_v thbh on th.transferheaderid = thbh.transferheaderid

-- DW_GetLenses_jbs.dbo.erp_po_inter_sib

select num_rep, num_rep2,
	isib.intransitstockitembatchid, th.intransitbatchheaderid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, th.orderid,
	isib.stockitemid,
	th.transfernum, th.allocatedstrategy, 
	th.totalremaining,
	th.createddate, 
	th.code_t, th.warehouse_name_t, th.supplierID_t, th.supplierName_t, th.ordernumber_t,
	th.code_s, th.warehouse_name_s, th.supplierID_s, th.supplierName_s, th.ordernumber_s, 
	th.incrementID, th.createddate_oh, th._type, th.orderStatus, 
	th.createddate_ibh, 
	isib.issuedquantity, isib.remainingquantity, 
	isib.used,
	isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
	isib.currency,
	isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
	isib.createddate_isib
into DW_GetLenses_jbs.dbo.erp_po_inter_sib
from
		DW_GetLenses_jbs.dbo.erp_po_inter_bh th
	left join
		Landing.mend.gen_inter_intransitstockitembatch_ibh_v isib on th.intransitbatchheaderid = isib.intransitbatchheaderid

 