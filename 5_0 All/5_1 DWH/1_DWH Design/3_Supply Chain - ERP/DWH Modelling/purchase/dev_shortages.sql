
drop table DW_GetLenses_jbs.dbo.erp_shh
drop table DW_GetLenses_jbs.dbo.erp_sh
drop table DW_GetLenses_jbs.dbo.erp_olsh


select shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
	wholesale, 
	code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
	count(*) over (partition by wholesale, code_src, code_dest, supplierName, packsizeid) num_rep
into DW_GetLenses_jbs.dbo.erp_shh
from Landing.mend.gen_short_shortageheader_v


select sh.shortage_id, shh.shortageheaderid, shh.supplier_id, shh.warehouseid_src, shh.warehouse_dest, shh.packsizeid,
	sh.purchaseorderlineid, sh.productid, sh.stockitemid, sh.warehousestockitemid,
	shh.wholesale, 
	shh.code_src, shh.warehouse_name_src, shh.code_dest, shh.warehouse_name_dest, shh.supplierID, shh.supplierName, 
	shh.num_rep, 
	sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
	sh.status, 
	sh.unitquantity, sh.packquantity, 
	sh.requireddate, 
	sh.createdDate, 
	count(*) over (partition by sh.shortage_id) num_rep2
into DW_GetLenses_jbs.dbo.erp_sh
from 
		DW_GetLenses_jbs.dbo.erp_shh shh
	left join
		Landing.mend.gen_short_shortage_v sh on shh.shortageheaderid = sh.shortageheaderid

select olsh.orderlineshortageid, sh.shortage_id, sh.shortageheaderid, sh.supplier_id, sh.warehouseid_src, sh.warehouse_dest, sh.packsizeid,
	sh.purchaseorderlineid, sh.productid, sh.stockitemid, sh.warehousestockitemid,
	sh.wholesale, 
	sh.code_src, sh.warehouse_name_src, sh.code_dest, sh.warehouse_name_dest, sh.supplierID, sh.supplierName, 
	sh.num_rep, 
	sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
	sh.status, 
	sh.unitquantity, sh.packquantity, 
	sh.requireddate, 
	sh.createdDate, 
	sh.num_rep2,
	olsh.orderlineshortagetype,
	olsh.unitquantity_ol, olsh.packquantity_ol,
	olsh.shortageprogress, olsh.adviseddate, 
	olsh.createdDate_ol
into DW_GetLenses_jbs.dbo.erp_olsh
from 
		DW_GetLenses_jbs.dbo.erp_sh sh
	left join
		Landing.mend.gen_short_orderlineshortage_v olsh on sh.shortage_id = olsh.shortage_id

