
-------------------------------- Intersite Transfer ----------------------------------


-- Physically Deleted Intersite Transfers

select it.*
from 
		Landing.mend.inter_transferheader_aud it
	inner join
		(select id
		from Landing.mend.inter_transferheader_aud
		except
		select id
		from Landing.mend.inter_transferheader) it2 on it.id = it2.id
order by it.createddate desc

select th.id transferheaderid, thpo_s.purchaseorderid purchaseorderid_s, thpo_t.purchaseorderid purchaseorderid_t, oth.orderid,
	th.transfernum, th.allocatedstrategy, 
	th.totalremaining,
	th.createddate, -- th.changeddate		 
	po_t.code code_t, po_t.warehouse_name warehouse_name_t, po_t.supplierID supplierID_t, po_t.supplierName supplierName_t, po_t.ordernumber ordernumber_t, 
	po_s.code code_s, po_s.warehouse_name warehouse_name_s, po_s.supplierID supplierID_s, po_s.supplierName supplierName_s, po_s.ordernumber ordernumber_s
from 
		Landing.mend.inter_transferheader_aud th
	inner join
		(select id
		from Landing.mend.inter_transferheader_aud
		except
		select id
		from Landing.mend.inter_transferheader) it2 on th.id = it2.id
	left join
		Landing.mend.inter_supplypo_transferheader thpo_s on th.id = thpo_s.transferheaderid
	left join
		Landing.mend.gen_purc_purchaseorder_v po_s on thpo_s.purchaseorderid = po_s.purchaseorderid
	left join
		Landing.mend.inter_transferpo_transferheader thpo_t on th.id = thpo_t.transferheaderid
	left join
		Landing.mend.gen_purc_purchaseorder_v po_t on thpo_t.purchaseorderid = po_t.purchaseorderid
	left join
		Landing.mend.inter_order_transferheader oth on th.id = oth.transferheaderid

-- duplicated intersite_transfer_number

select *
from
	(select count(*) over (partition by transfernum) num_rep, *
	from Landing.mend.gen_inter_transferheader_v) th
where num_rep > 1
order by createddate, transfernum

-- Missing Relations - Transfer PO

select *
from Landing.mend.gen_inter_transferheader_v
where purchaseorderid_t is null
order by createddate desc

-- Missing Relations - Supply PO

select *
from Landing.mend.gen_inter_transferheader_v
where purchaseorderid_s is null
order by createddate desc

-- Missing Relations - Order

select *
from Landing.mend.gen_inter_transferheader_v
where orderid is null
order by createddate desc

select th.*, po.status
from 
		Landing.mend.gen_inter_transferheader_v th
	left join
		Landing.mend.gen_purc_purchaseorder_v po on th.purchaseorderid_t = po.purchaseorderid
where th.orderid is null
order by th.createddate desc

-------------------------------- Intransit Stock Item Batch ----------------------------------

-- Physically Deleted Intransit Stock Item Batch

select isib.*
from 
		Landing.mend.inter_intransitstockitembatch_aud isib
	inner join
		(select id
		from Landing.mend.inter_intransitstockitembatch_aud
		except
		select id
		from Landing.mend.inter_intransitstockitembatch) isib2 on isib.id = isib2.id

select th.id, th.transfernum, th.createddate,
	isib.*
from 
		Landing.mend.inter_intransitstockitembatch_aud isib
	inner join
		(select id
		from Landing.mend.inter_intransitstockitembatch_aud
		except
		select id
		from Landing.mend.inter_intransitstockitembatch) isib2 on isib.id = isib2.id
	inner join
		Landing.mend.inter_intransitstockitembatch_transferheader_aud isibth on isib.id = isibth.intransitstockitembatchid
	inner join
		Landing.mend.inter_transferheader_aud th on isibth.transferheaderid = th.id
	inner join
		Landing.mend.gen_inter_transferheader_v thv on th.id = thv.transferheaderid
order by th.createddate desc

	select distinct th.id, th.transfernum, th.createddate
	from 
			Landing.mend.inter_intransitstockitembatch_aud isib
		inner join
			(select id
			from Landing.mend.inter_intransitstockitembatch_aud
			except
			select id
			from Landing.mend.inter_intransitstockitembatch) isib2 on isib.id = isib2.id
		inner join
			Landing.mend.inter_intransitstockitembatch_transferheader_aud isibth on isib.id = isibth.intransitstockitembatchid
		inner join
			Landing.mend.inter_transferheader_aud th on isibth.transferheaderid = th.id
		inner join
			Landing.mend.gen_inter_transferheader_v thv on th.id = thv.transferheaderid
	order by th.createddate desc


-- Missing Child Records
	
	-- Intersite Transfer without Intransit Batch Header
	select th.*
	from 
			Landing.mend.gen_inter_transferheader_v th
		left join
			(select distinct transferheaderid
			from Landing.mend.gen_inter_intransitbatchheader_v) ibh on th.transferheaderid = ibh.transferheaderid
	where ibh.transferheaderid is null
	order by th.createddate desc

	-- Intersite Transfer without Intransit Stock Item Batch
	select th.*
	from 
			Landing.mend.gen_inter_transferheader_v th
		left join
			(select distinct transferheaderid
			from Landing.mend.gen_inter_intransitstockitembatch_th_v) isib on th.transferheaderid = isib.transferheaderid
	where isib.transferheaderid is null
	order by th.createddate desc

		select th.*, o.orderStatus
		from 
				Landing.mend.gen_inter_transferheader_v th
			left join
				(select distinct transferheaderid
				from Landing.mend.gen_inter_intransitstockitembatch_th_v) isib on th.transferheaderid = isib.transferheaderid
			left join
				Landing.mend.gen_order_order_v o on th.orderid = o.orderid
		where isib.transferheaderid is null
			and o.orderStatus not in ('Pending', 'Cancelled')
		order by th.createddate desc


	-- Intransit Batch Header without Intransit Stock Item Batch
	select ibh.*
	from 
			Landing.mend.gen_inter_intransitbatchheader_v ibh
		left join
			(select distinct intransitbatchheaderid
			from Landing.mend.gen_inter_intransitstockitembatch_ibh_v) isib on ibh.intransitbatchheaderid = isib.intransitbatchheaderid
	where isib.intransitbatchheaderid is null

-- Missing Parent Records

	-- Intransit Batch Header without Intersite Transfer
	select ibh.*
	from 
			Landing.mend.inter_intransitbatchheader ibh
		left join
			Landing.mend.inter_intransitbatchheader_transferheader ibhth on ibh.id = ibhth.intransitbatchheaderid 
		left join	
			Landing.mend.inter_transferheader th on ibhth.transferheaderid = th.id
	where ibhth.intransitbatchheaderid is null or th.id is null
	

	-- Intransit Stock Item Batch without Intersite Transfer
	select isib.*
	from 
			Landing.mend.inter_intransitstockitembatch isib 
		left join
			Landing.mend.inter_intransitstockitembatch_transferheader isibth on isib.id = isibth.intransitstockitembatchid
		left join
			Landing.mend.inter_transferheader th on isibth.transferheaderid = th.id
	where isibth.intransitstockitembatchid is null or th.id is null

	-- Intransit Stock Item Batch without Intransit Batch Header
	select isib.*
	from 
			Landing.mend.inter_intransitstockitembatch isib 
		left join
			Landing.mend.inter_intransitstockitembatch_intransitbatchheader isibth on isib.id = isibth.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitbatchheader ibh on isibth.intransitbatchheaderid = ibh.id
	where isibth.intransitstockitembatchid is null or ibh.id is null

-- Missing Relations - Stock Item
select *
from Landing.mend.gen_inter_intransitstockitembatch_th_v
where stockitemid is null
order by createddate desc
	
-- Missing Relations - Stock Item
select *
from Landing.mend.gen_inter_intransitstockitembatch_th_v
where receiptid is null
order by createddate desc
