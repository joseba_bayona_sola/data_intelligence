
-- Transfer PO

select top 1000 po.*
from 
		DW_GetLenses_jbs.dbo.erp_po_po po
	inner join
		(select distinct purchaseorderid_t
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on po.purchaseorderid = th.purchaseorderid_t
order by po.createddate desc

select top 1000 polh.*
from 
		DW_GetLenses_jbs.dbo.erp_po_polh polh
	inner join
		(select distinct purchaseorderid_t
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on polh.purchaseorderid = th.purchaseorderid_t
order by polh.createddate desc

	select top 1000 polh.*, pfpssp.magentoProductID, pfpssp.name, pfpssp.size
	from 
			DW_GetLenses_jbs.dbo.erp_po_polh polh
		inner join
			(select distinct purchaseorderid_t
			from DW_GetLenses_jbs.dbo.erp_po_inter
			where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on polh.purchaseorderid = th.purchaseorderid_t
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpssp on polh.supplierpriceid = pfpssp.supplierpriceid
	order by polh.createddate desc

select top 1000 pol.*
from 
		DW_GetLenses_jbs.dbo.erp_po_pol pol
	inner join
		(select distinct purchaseorderid_t
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_t
order by pol.createddate desc


	select top 1000 pol.*, si.magentoProductID, si.name, si.packSize, si.stockitemdescription
	from 
			DW_GetLenses_jbs.dbo.erp_po_pol pol
		inner join
			(select distinct purchaseorderid_t
			from DW_GetLenses_jbs.dbo.erp_po_inter
			where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_t
		left join
			Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	order by pol.createddate desc


----------------------------------------------------------------------

-- Supply PO

select top 1000 po.*
from 
		DW_GetLenses_jbs.dbo.erp_po_po po
	inner join
		(select distinct purchaseorderid_s
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on po.purchaseorderid = th.purchaseorderid_s
order by po.createddate desc

select top 1000 polh.*
from 
		DW_GetLenses_jbs.dbo.erp_po_polh polh
	inner join
		(select distinct purchaseorderid_s
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on polh.purchaseorderid = th.purchaseorderid_s
order by polh.createddate desc

	select top 1000 polh.*, pfpssp.magentoProductID, pfpssp.name, pfpssp.size
	from 
			DW_GetLenses_jbs.dbo.erp_po_polh polh
		inner join
			(select distinct purchaseorderid_s
			from DW_GetLenses_jbs.dbo.erp_po_inter
			where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on polh.purchaseorderid = th.purchaseorderid_s
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpssp on polh.supplierpriceid = pfpssp.supplierpriceid
	order by polh.createddate desc

select top 1000 pol.*
from 
		DW_GetLenses_jbs.dbo.erp_po_pol pol
	inner join
		(select distinct purchaseorderid_s
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_s
order by pol.createddate desc

	select top 1000 pol.*, si.magentoProductID, si.name, si.packSize, si.stockitemdescription
	from 
			DW_GetLenses_jbs.dbo.erp_po_pol pol
		inner join
			(select distinct purchaseorderid_s
			from DW_GetLenses_jbs.dbo.erp_po_inter
			where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th on pol.purchaseorderid = th.purchaseorderid_s
		left join
			Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	order by pol.createddate desc