
select o.orderid, 
	o.magentoOrderID_int, o.magentoOrderID, o.incrementID, 
	o.createdDate, 
	o._type, o.orderStatus, o.shipmentStatus, o.allocationStatus, o.allocationStrategy, 
	o.subtotal, o.grandTotal
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
order by o.createddate desc	

----------------------------------------------------------------------

select ol.orderid, ol.orderlineid, 
	o.magentoOrderID, o.incrementID, o.createdDate, 
	ol.magentoItemID,
	ol.orderLinesMagento, ol.orderLinesDeduped,
	ol.quantityOrdered, ol.quantityInvoiced, ol.quantityAllocated, ol.basePrice, ol.baseRowPrice, 
	ol.productid, ol.magentoProductID, ol.name, ol.sku
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
	inner join
		Landing.mend.gen_order_orderline_v ol on th.orderid = ol.orderid
order by o.createddate desc	

select olm.orderid, olm.orderlinemagentoid, 
	o.magentoOrderID, o.incrementID, o.createdDate, 
	olm.magentoItemID, 
	olm.orderLinesMagento, olm.orderLinesDeduped, olm.lineAdded, olm.deduplicate,
	olm.quantityOrdered, olm.quantityInvoiced, olm.quantityAllocated, olm.basePrice, olm.baseRowPrice, 
	olm.productid, olm.magentoProductID, olm.name, olm.sku 
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
	inner join
		Landing.mend.gen_order_orderlinemagento_v olm on th.orderid = olm.orderid
order by o.createddate desc	

----------------------------------------------------------------------

select ol.orderid, ol.orderlineid, 
	o.magentoOrderID, o.incrementID, o.createdDate, 
	sat.orderlinestockallocationtransactionid, sat.warehousestockitemid, -- sat.batchstockallocationid, sat.warehousestockitembatchid, 
	sat.wh_name, sat.allocationType, sat.recordType, sat.timestamp, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packsize
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
	inner join
		Landing.mend.gen_order_orderline_v ol on th.orderid = ol.orderid
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
order by o.createddate desc	


select ol.orderid, ol.orderlineid, 
	o.magentoOrderID, o.incrementID, o.createdDate, 
	sat.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, 
	wsiba.productfamilyid, wsiba.productid, wsiba.stockitemid, wsiba.warehousestockitemid, wsiba.warehousestockitembatchid, 
	wsiba.batch_id, wsiba.magentoProductID, wsiba.stockitemdescription,
	wsiba.fullyissued_alloc, wsiba.cancelled, wsiba.allocatedquantity_alloc, wsiba.allocatedunits, wsiba.issuedquantity_alloc, 
	wsiba.allocation_date
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
	inner join
		Landing.mend.gen_order_orderline_v ol on th.orderid = ol.orderid
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
	left join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on sat.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
order by o.createddate desc	


select ol.orderid, ol.orderlineid, 
	o.magentoOrderID, o.incrementID, o.createdDate, 
	sat.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, wsibi.batchstockissueid, 
	wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, 
	wsibi.batch_id, wsibi.magentoProductID, wsibi.stockitemdescription, 
	wsibi.issueid, wsibi.issuedquantity_issue, wsibi.issue_date
from
		(select distinct orderid
		from DW_GetLenses_jbs.dbo.erp_po_inter
		where transfernum in ('TR00002851', 'TR00002843', 'TR00001972', 'TR00002804')) th
	inner join
		Landing.mend.gen_order_order_v o on th.orderid = o.orderid
	inner join
		Landing.mend.gen_order_orderline_v ol on th.orderid = ol.orderid
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
	left join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on sat.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
	left join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on wsiba.batchstockallocationid = wsibi.batchstockallocationid
order by o.createddate desc	

----------------------------------------------------------------------


----------------------------------------------------------------------


----------------------------------------------------------------------