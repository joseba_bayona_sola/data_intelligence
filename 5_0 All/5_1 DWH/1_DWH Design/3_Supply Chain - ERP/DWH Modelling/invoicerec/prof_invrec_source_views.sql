
select invoiceid, accountid, warehouseid, supplier_id,
	code, warehouse_name, 
	supplierID, supplierName, currencyCode,
	invoiceref, netsuiteno, invoicedate, status, approveddate, posteddate, paymentduedate, 
	invoicegoodsamount, invoicecarriageamount, invoicecreditamount, invoicenetamount, -- invoicediscountamount, invoiceimportdutyamount, 
	invoicegrossamount, invoicevatamount, -- invoicereconciledamount,
	erpreservedgoodsamount, erpgoodsamount, erpcarriageamount, erpnetamount, erpgrossamount, -- erpdiscountamount, erpimportdutyamount, 
	erpvatamount, erpadjustedamount,
	netdifference, creditreceived, linetotaladjusted, reconciledcostsappliedtoreceiptlines
from Landing.mend.gen_invrec_invoice_v
where invoiceref = '28583'
order by invoicedate, invoiceid

	select count(*)
	from Landing.mend.gen_invrec_invoice_v

	select status, count(*)
	from Landing.mend.gen_invrec_invoice_v
	group by status
	order by status

select invoiceid, accountid, warehouseid, supplier_id, invoicelinepoheaderid, purchaseorderid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	index_int, adjustedtotalcost, totalquantity, validated, editable
from Landing.mend.gen_invrec_invoicelinepoheader_v 
where invoiceref = '28583'
order by invoicedate, invoiceid, index_int

	select count(*)
	from Landing.mend.gen_invrec_invoicelinepoheader_v 

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable
from Landing.mend.gen_invrec_invoicelineheader_v
where invoiceref = '28583'
order by invoicedate, invoiceid, name

	select count(*)
	from Landing.mend.gen_invrec_invoicelineheader_v

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied
from Landing.mend.gen_invrec_invoiceline_v
where invoiceref = '28583'
order by invoicedate, invoiceid, description, orderNumber, receipt_id

	select count(*)
	from Landing.mend.gen_invrec_invoiceline_v

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select count(*) over (partition by adjustmentid) num_rep,
	adjustmentid, adjustmenthistoryid, 
	adjustmenttype, submetaobjectname, amount,
	createddate, comment, user_name
from Landing.mend.gen_invrec_adjustmenthistory_v
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid

	select count(*)
	from Landing.mend.gen_invrec_adjustmenthistory_v

	select adjustmenttype, submetaobjectname, count(*)
	from Landing.mend.gen_invrec_adjustmenthistory_v
	group by adjustmenttype, submetaobjectname
	order by adjustmenttype, submetaobjectname

	select user_name, comment, count(*)
	from Landing.mend.gen_invrec_adjustmenthistory_v
	group by user_name, comment
	order by user_name, comment

select count(*) over (partition by adjustmentid) num_rep,
	invoiceid, accountid, warehouseid, supplier_id, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoiceadjustmenthistory_v
-- where invoiceref = '28583'
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid

	select count(*)
	from Landing.mend.gen_invrec_invoiceadjustmenthistory_v

	select adjustmenttype, submetaobjectname, count(*)
	from Landing.mend.gen_invrec_invoiceadjustmenthistory_v
	group by adjustmenttype, submetaobjectname
	order by adjustmenttype, submetaobjectname

select count(*) over (partition by adjustmentid) num_rep,
	invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v
-- where invoiceref = '28583'
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid, invoicedate, invoiceid, name

	select count(*)
	from Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v

	select adjustmenttype, submetaobjectname, count(*)
	from Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v
	group by adjustmenttype, submetaobjectname
	order by adjustmenttype, submetaobjectname

select -- count(*) over (partition by adjustmentid) num_rep,
	invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineadjustmenthistory_v
-- where invoiceref = '06411643'
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid, invoicedate, invoiceid, description, orderNumber, receipt_id

	select count(*)
	from Landing.mend.gen_invrec_invoicelineadjustmenthistory_v

	select adjustmenttype, submetaobjectname, count(*)
	from Landing.mend.gen_invrec_invoicelineadjustmenthistory_v
	group by adjustmenttype, submetaobjectname
	order by adjustmenttype, submetaobjectname

	select 
		il.invoiceid, il.invoicelineid, il.accountid, il.warehouseid, il.supplier_id, il.invoicelineheaderid, il.productfamilyid, il.packsizeid, 
		il.invoicelinepoheaderid, il.receiptlineid, ah.adjustmentid, ah.adjustmenthistoryid, -- il.stockitemid, 
		il.code, il.warehouse_name, 
		il.supplierID, il.supplierName,
		il.invoiceref, il.netsuiteno, il.invoicedate, il.status, 
		-- il.receipt_id, il.orderNumber, il.stockitem, il.description, 
		il.size,
		-- il.quantityAccepted, 
		il.quantity, 
		-- il.receiptUnitCost, il.invoiceUnitCost,
		il.unitcost, il.totalcost, il.adjustedunitcost, il.adjustedtotalcost, il.adjustmentapplied, 
		ah.adjustmenttype, ah.submetaobjectname, ah.amount,
		ah.createddate, 
		ah.comment, ah.user_name
	from 
			(select 
				ilh.invoiceid, ilh.invoicelineheaderid, il.id invoicelineid, ilh.accountid, ilh.warehouseid, ilh.supplier_id, ilh.productfamilyid, ilh.packsizeid, 
				ililpoh.invoicelinepoheaderid, ilrl.receiptlineid,
				ilh.code, ilh.warehouse_name, 
				ilh.supplierID, ilh.supplierName,
				ilh.invoiceref, ilh.netsuiteno, ilh.invoicedate, ilh.status, 
				ilh.size,
				il.quantity, 
				il.unitcost, il.totalcost, il.adjustedunitcost, il.adjustedtotalcost, il.adjustmentapplied
			from 
					Landing.mend.gen_invrec_invoicelineheader_v ilh
				inner join
					Landing.mend.invrec_invoiceline_invoicelineheader ililh on ilh.invoicelineheaderid = ililh.invoicelineheaderid
				inner join
					Landing.mend.invrec_invoiceline il on ililh.invoicelineid = il.id
				inner join
					Landing.mend.invrec_invoiceline_invoicelinepoheader ililpoh on il.id = ililpoh.invoicelineid
				inner join
					Landing.mend.invrec_invoiceline_receiptline ilrl on il.id = ilrl.invoicelineid) il
		inner join
			Landing.mend.invrec_adjustmenthistory_invoiceline ahil on il.invoicelineid = ahil.invoicelineid
		inner join
			Landing.mend.gen_invrec_adjustmenthistory_v ah on ahil.adjustmenthistoryid = ah.adjustmenthistoryid
	where invoiceref = '06596610'



--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------


select invoicelineheaderadjustmentid, 
	submetaobjectname, unitcost, adjustedunitcost, comment,
	posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from Landing.mend.gen_invrec_invoicelineheaderadjustment_v

	select count(*)
	from Landing.mend.gen_invrec_invoicelineheaderadjustment_v

	select submetaobjectname, count(*)
	from Landing.mend.gen_invrec_invoicelineheaderadjustment_v
	group by submetaobjectname
	order by submetaobjectname

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid, invoicelineheaderadjustmentid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	submetaobjectname, unitcost_ilha, adjustedunitcost_ilha, comment,
	posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from Landing.mend.gen_invrec_invoicelineheaderadjustment_il_v
where invoicelineheaderadjustmentid = 200410183417987177
order by invoicedate, invoiceid, description, orderNumber, receipt_id


--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select invoiceid, accountid, warehouseid, supplier_id, carriageid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived,
	carriagetype, cost, 
	comment, 
	createddate, changeddate
from Landing.mend.gen_invrec_invoicecarriage_v 

	select count(*)
	from Landing.mend.gen_invrec_invoicecarriage_v 

	select carriagetype, count(*)
	from Landing.mend.gen_invrec_invoicecarriage_v
	group by carriagetype
	order by carriagetype

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select creditnoteid, supplier_id, invoicecreditid, invoiceid,
	creditnoteno, reference, netsuiteno,
	totalamount, 
	supplierID, supplierName, 
	credit_note_status, 
	creditdate, createddate, processeddate, 
	goodscreditamount, carriagecreditamount, vatcreditamount, misccreditamount,
	invoiceref, status, invoicenetamount, erpnetamount, netdifference,
	goodscreditamount_i, carriagecreditamount_i, 
	comment
from Landing.mend.gen_invrec_invoicecredit_v 
where creditnoteno = '1033'

	select count(*)
	from Landing.mend.gen_invrec_invoicecredit_v 

	select credit_note_status, count(*)
	from Landing.mend.gen_invrec_invoicecredit_v 
	group by credit_note_status
	order by credit_note_status