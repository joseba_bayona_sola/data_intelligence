
select invoiceid, accountid, warehouseid, supplier_id,
	code, warehouse_name, 
	supplierID, supplierName, currencyCode,
	invoiceref, netsuiteno, invoicedate, status, approveddate, posteddate, paymentduedate, 
	invoicegoodsamount, invoicecarriageamount, invoicecreditamount, invoicenetamount, -- invoicediscountamount, invoiceimportdutyamount, 
	invoicegrossamount, invoicevatamount, -- invoicereconciledamount,
	erpreservedgoodsamount, erpgoodsamount, erpcarriageamount, erpnetamount, erpgrossamount, -- erpdiscountamount, erpimportdutyamount, 
	erpvatamount, erpadjustedamount,
	netdifference, creditreceived, linetotaladjusted, reconciledcostsappliedtoreceiptlines
from Landing.mend.gen_invrec_invoice_v
where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
-- where invoiceref in ('12924', '1054582460', '12927', '12930', '12932')
-- where invoiceref in ('9245450597', '9245467455', '1054628337b', '9245499324', '9245526568')
order by invoicedate, invoiceid

select invoiceid, accountid, warehouseid, supplier_id, invoicelinepoheaderid, purchaseorderid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	index_int, adjustedtotalcost, totalquantity, validated, editable
from Landing.mend.gen_invrec_invoicelinepoheader_v 
-- where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
-- where invoiceref in ('12924', '1054582460', '12927', '12930', '12932')
where invoiceref in ('9245450597', '9245467455', '1054628337b', '9245499324', '9245526568')
order by invoicedate, invoiceid, index_int

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied
from Landing.mend.gen_invrec_invoiceline_v
-- where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
-- where invoiceref in ('12924', '1054582460', '12927', '12930', '12932')
where invoiceref in ('9245450597', '9245467455', '1054628337b', '9245499324', '9245526568')
order by invoicedate, invoiceid, description, orderNumber, receipt_id

select count(*) over (partition by adjustmentid) num_rep,
	invoiceid, accountid, warehouseid, supplier_id, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoiceadjustmenthistory_v
where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid

select count(*) over (partition by adjustmentid) num_rep,
	invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v
where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
order by adjustmenttype, submetaobjectname, num_rep desc, adjustmentid, adjustmenthistoryid, invoicedate, invoiceid, name

select invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	invoicelinepoheaderid, receiptlineid, stockitemid, adjustmentid, adjustmenthistoryid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	adjustmenttype, submetaobjectname, amount,
	createddate
from Landing.mend.gen_invrec_invoicelineadjustmenthistory_v
where invoiceref in ('61338621', '61335846', '61328923', '61328922', '61326080', '13056')
order by invoicedate, invoiceid, description, orderNumber, receipt_id

select invoiceid, accountid, warehouseid, supplier_id, carriageid,
	code, warehouse_name, 
	supplierID, supplierName,
	invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived,
	carriagetype, cost, 
	comment, 
	createddate, changeddate
from Landing.mend.gen_invrec_invoicecarriage_v 
where invoiceref in ('12924', '1054582460', '12927', '12930', '12932')

select creditnoteid, supplier_id, invoicecreditid, invoiceid,
	creditnoteno, reference, netsuiteno,
	totalamount, 
	supplierID, supplierName, 
	credit_note_status, 
	creditdate, createddate, processeddate, 
	goodscreditamount, carriagecreditamount, vatcreditamount, misccreditamount,
	invoiceref, status, invoicenetamount, erpnetamount, netdifference,
	goodscreditamount_i, carriagecreditamount_i, 
	comment
from Landing.mend.gen_invrec_invoicecredit_v 
where invoiceref in ('9245450597', '9245467455', '1054628337b', '9245499324', '9245526568')
order by invoiceref

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Per ADJ TYPE: Percentage - LineTotal - Value
-- Per ADJ LEVEL: Invoice - Invoice Header - Invoice Line
	-- Percentage - Invoice: 61328923
	-- Value - Invoice: H18126, 61335846, 28583

	-- Value - Invoice Header: 61334502, 06496321

	-- LineTotal - Invoice Line: 5240324022
	-- Value - Invoice Line: 111 73664337

	-- Bulk Header - Invoice Header: NL30381242

	-- Adjustments can be done at 3 different levels: 
		-- Invoice: Can be percentage - value adjustment
		-- Invoice Header: Only value: Can be done in 2 different ways: selecting the header + receipt // Bulk Adjustment: selecting a price and register dates // Final values always at Invoice Line
		-- Invoice Line: Can be value - line total: Coming from Header (value) - Coming from Line (line total): they won't appear in Adjustment
			-- Value: Need to multiply amount * quantity
			-- LineTotal: Need to take only amount


select inv_type, invoiceref, dense_rank() over (partition by inv_type order by invoiceref) ord_rep
into #invoices
from
	(select '1_1' inv_type, '61328923' invoiceref
	union
	select '1_2', 'H18126'
	union
	select '1_2', '61335846'
	union
	select '1_2', '28583'
	union
	select '2_1', '61334502'
	union
	select '2_1', '06496321'
	union
	select '3_1', '5240324022'
	union
	select '3_2', '111 73664337'
	union
	select '4_1', 'NL30381242') t

select *
from #invoices

select inv_type,
	-- invoiceid, accountid, warehouseid, supplier_id,
	-- code, 
	warehouse_name, 
	-- supplierID, 
	supplierName, currencyCode,
	t1.invoiceref, netsuiteno, -- invoicedate, status, approveddate, posteddate, paymentduedate, 
	invoicegoodsamount, invoicecarriageamount, invoicecreditamount, invoicenetamount, -- invoicediscountamount, invoiceimportdutyamount, 
	invoicegrossamount, invoicevatamount, -- invoicereconciledamount,
	erpreservedgoodsamount, erpgoodsamount, erpcarriageamount, erpnetamount, erpgrossamount, -- erpdiscountamount, erpimportdutyamount, 
	erpvatamount, erpadjustedamount,
	netdifference, creditreceived, linetotaladjusted, reconciledcostsappliedtoreceiptlines
from 
		Landing.mend.gen_invrec_invoice_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, invoicedate, invoiceid

select inv_type,
	-- invoiceid, accountid, warehouseid, supplier_id, invoicelinepoheaderid, purchaseorderid,
	code, warehouse_name, 
	supplierID, supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	index_int, adjustedtotalcost, totalquantity, validated, editable
from 
		Landing.mend.gen_invrec_invoicelinepoheader_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, invoicedate, invoiceid, index_int

select inv_type, -- invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid,
	-- code, 
	warehouse_name, 
	-- supplierID, 
	supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable
from 
		Landing.mend.gen_invrec_invoicelineheader_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, invoicedate, invoiceid, name

select inv_type, -- invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	-- invoicelinepoheaderid, receiptlineid, stockitemid,
	-- code, 
	warehouse_name, 
	-- supplierID, 
	supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied
from 
		Landing.mend.gen_invrec_invoiceline_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, invoicedate, invoiceid, description, orderNumber, receipt_id

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select inv_type,
	-- invoiceid, accountid, warehouseid, supplier_id, adjustmentid, adjustmenthistoryid,
	-- code, 
	warehouse_name, 
	supplierID, supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	invoicegoodsamount, invoicecarriageamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, 
	erpgoodsamount, erpcarriageamount, erpnetamount, 
	erpadjustedamount,
	netdifference, creditreceived, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from 
		Landing.mend.gen_invrec_invoiceadjustmenthistory_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, adjustmenttype, submetaobjectname, adjustmentid, adjustmenthistoryid

select inv_type,
	-- invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, adjustmentid, adjustmenthistoryid,
	-- code, 
	warehouse_name, 
	-- supplierID, 
	supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	magentoProductID_int, magentoProductID, name, size, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable, 
	adjustmenttype, submetaobjectname, amount,
	createddate
from 
		Landing.mend.gen_invrec_invoicelineheaderadjustmenthistory_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, adjustmenttype, submetaobjectname, adjustmentid, adjustmenthistoryid, invoicedate, invoiceid, name

select inv_type,
	-- invoiceid, accountid, warehouseid, supplier_id, invoicelineheaderid, productfamilyid, packsizeid, 
	-- invoicelinepoheaderid, receiptlineid, stockitemid, adjustmentid, adjustmenthistoryid,
	-- code, 
	warehouse_name, 
	-- supplierID, 
	supplierName,
	t1.invoiceref, netsuiteno, invoicedate, status, 
	receipt_id, orderNumber, stockitem, description, size,
	quantityAccepted, quantity, 
	receiptUnitCost, invoiceUnitCost,
	unitcost, totalcost, adjustedunitcost, adjustedtotalcost, adjustmentapplied,
	adjustmenttype, submetaobjectname, amount,
	createddate
from 
		Landing.mend.gen_invrec_invoicelineadjustmenthistory_v t1
	inner join
		#invoices t2 on t1.invoiceref = t2.invoiceref
order by t2.inv_type, t2.invoiceref, adjustmenttype, submetaobjectname, adjustmentid, adjustmenthistoryid, invoicedate, invoiceid, description, orderNumber, receipt_id

-- Per CARRIAGE: 

	-- Carriage is splitted into invoice lines - receipt lines whenever the invoice is posted

-- Per CREDIT NOTE: 
