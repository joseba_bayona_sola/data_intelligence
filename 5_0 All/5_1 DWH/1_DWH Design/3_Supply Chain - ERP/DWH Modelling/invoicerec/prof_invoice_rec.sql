﻿
select id, invoiceref, netsuiteno, invoicedate, status, 
	paymentduedate, approveddate, posteddate, -- reconcileddate, 
	invoicegoodsamount, invoicecarriageamount, invoicediscountamount, invoiceimportdutyamount, invoicenetamount, 
	invoicegrossamount, invoicevatamount, invoicereconciledamount, -- invoicematchedamount, 

	erpgoodsamount, erpcarriageamount, erpdiscountamount, erpimportdutyamount, erpnetamount, 
	erpgrossamount, erpvatamount, 
	erpadjustedamount, -- erpregisteredamount, erpunregisteredamount, 

	-- invoiceqtyofgoods, invoicematchedqty, erpregisteredqty, erpunregisteredqty,

	netdifference, creditreceived, linetotaladjusted,
	comment, auditcomment, attachmentscount,
	createddate
from invoicereconciliation$invoice	
-- where invoiceref in ('NL30372035', '111 72950749', '12658')
order by id desc

select *
from invoicereconciliation$invoice
where status = 'Credit_Note_Requested'

	select status, count(*)
	from invoicereconciliation$invoice
	group by status
	order by status

	select *
	from invoicereconciliation$invoice_assignedto

	select *
	from invoicereconciliation$invoice_supplier

	select *
	from invoicereconciliation$invoice_warehouse

	-- payment term ??


select id, index, adjustedtotalcost, editable, createddate
from invoicereconciliation$invoicelinepoheader
order by id desc

	select *
	from invoicereconciliation$invoicelinepoheader_invoice

	select *
	from invoicereconciliation$invoicelinepoheader_purchaseorder
	
select id, 
	pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable
from invoicereconciliation$invoicelineheader
order by id desc

	select *
	from invoicereconciliation$invoicelineheader_invoice

	select *
	from invoicereconciliation$invoicelineheader_productfamily

	select *
	from invoicereconciliation$invoicelineheader_packsize
	

select id, quantity, 
	unitcost, totalcost,
	adjustedunitcost, adjustedtotalcost, adjustmentapplied, 
	createddate
from invoicereconciliation$invoiceline
order by id desc

	select *
	from invoicereconciliation$invoiceline_invoicelineheader

	select *
	from invoicereconciliation$invoiceline_invoicelinepoheader

	-- multiple: same stock register receipt line in different invoices
	select *
	from invoicereconciliation$invoiceline_receiptline


-- Us Wrong - Supplier OK
	-- Value Adjustment: 
		-- Different Levels: Invoice Line - Invoice Header - Invoice
		-- Future Adjustment on Invoice Line for Adjust the Total
		
	-- Percentage Adjustment: 
		-- Top Level Adjustment 
		
-- Us OK - Supplier Wrong
	-- Credit Note

select id, adjustmenttype, submetaobjectname, amount
from invoicereconciliation$adjustment
order by id desc

select id, user, comment, createddate
from invoicereconciliation$adjustmenthistory

	select *
	from invoicereconciliation$adjustmenthistory_adjustment

	select *
	from invoicereconciliation$adjustmenthistory_invoice

	select *
	from invoicereconciliation$adjustmenthistory_invoicelineheader

	select *
	from invoicereconciliation$adjustmenthistory_invoiceline

select id, submetaobjectname, unitcost, adjustedunitcost, comment
from invoicereconciliation$invoicelineheaderadjustment

	select *
	from invoicereconciliation$invoicelineheaderadjustment_invoiceline

select id, posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from invoicereconciliation$invoicelineheaderbulkadjustment	

select id, description, name
from invoicereconciliation$percentageadjustment

	select *
	from invoicereconciliation$supplieradjustment_percentageadjustment

select id, enabled, createddate
from invoicereconciliation$supplieradjustment

	select *
	from invoicereconciliation$supplieradjustment_supplier
	

-- Carriage
	-- Invoice Level
	-- Some discussions about how to apply at line level

select id, 
	carriagetype, cost, 
	comment, 
	createddate, changeddate
from invoicereconciliation$carriage

	select *
	from invoicereconciliation$carriage_invoice

-- Credit Notes
	-- For a Supplier - 1 or many invoices
	-- Specify Value
	-- Recalculate Invoice - ERP Net Amount

select id, 
	status, reference, creditnoteno, amount, 
	processeddate, comment,
	createddate
from invoicereconciliation$creditnote

	select *
	from invoicereconciliation$creditnote_supplier

select id, amount
from invoicereconciliation$invoicecredit

	select *
	from invoicereconciliation$invoicecredit_invoice

	select *
	from invoicereconciliation$invoicecredit_creditnote
	
-----------------------------------------------------

select *
from invoicereconciliation$paymentterm
order by id desc

select *
from invoicereconciliation$invoicedreceiptexport

select *
from invoicereconciliation$invoicelineexport

select *
from invoicereconciliation$invoicelineexportdoc

select *
from invoicereconciliation$invoiceupload

select *
from invoicereconciliation$usercomment