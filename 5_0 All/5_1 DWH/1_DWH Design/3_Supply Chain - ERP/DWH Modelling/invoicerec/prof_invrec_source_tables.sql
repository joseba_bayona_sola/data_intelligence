
select id, invoiceref, netsuiteno, invoicedate, status, paymentduedate, approveddate, posteddate,
	invoicegoodsamount, invoicecarriageamount, invoicediscountamount, invoicecreditamount, invoiceimportdutyamount, invoicenetamount,
	invoicegrossamount, invoicevatamount, invoicereconciledamount,
	erpreservedgoodsamount, erpgoodsamount, erpcarriageamount, erpdiscountamount, erpimportdutyamount, erpnetamount, erpgrossamount, erpvatamount, erpadjustedamount,
	netdifference, creditreceived, linetotaladjusted, reconciledcostsappliedtoreceiptlines, comment, auditcomment, attachmentscount, createddate
from Landing.mend.invrec_invoice

	select count(*)
	from Landing.mend.invrec_invoice

	select status, count(*)
	from Landing.mend.invrec_invoice
	group by status
	order by status

select id, index_int, adjustedtotalcost, editable, createddate, totalquantity, validated
from Landing.mend.invrec_invoicelinepoheader

	select count(*)
	from Landing.mend.invrec_invoicelinepoheader

select id, pricedifference, totalquantity, totalcost, adjustedtotalcost, adjustmentapplied, editable
from Landing.mend.invrec_invoicelineheader

	select count(*)
	from Landing.mend.invrec_invoicelineheader

select  id, quantity, 
	unitcost, totalcost,
	adjustedunitcost, adjustedtotalcost, adjustmentapplied, 
	createddate
from Landing.mend.invrec_invoiceline

	select count(*)
	from Landing.mend.invrec_invoiceline

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select id, adjustmenttype, submetaobjectname, amount
from Landing.mend.invrec_adjustment

	select count(*)
	from Landing.mend.invrec_adjustment

	select adjustmenttype, submetaobjectname, count(*)
	from Landing.mend.invrec_adjustment
	group by adjustmenttype, submetaobjectname
	order by adjustmenttype, submetaobjectname

select id, createddate, comment, user_name
from Landing.mend.invrec_adjustmenthistory

	select count(*)
	from Landing.mend.invrec_adjustmenthistory

	select user_name, comment, count(*)
	from Landing.mend.invrec_adjustmenthistory
	group by user_name, comment
	order by user_name, comment


select count(*)
from Landing.mend.invrec_adjustmenthistory_invoice

select count(*)
from Landing.mend.invrec_adjustmenthistory_invoicelineheader

select count(*)
from Landing.mend.invrec_adjustmenthistory_invoiceline

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select id, submetaobjectname, unitcost, adjustedunitcost, comment
from Landing.mend.invrec_invoicelineheaderadjustment

	select count(*)
	from Landing.mend.invrec_invoicelineheaderadjustment

	select submetaobjectname, count(*)
	from Landing.mend.invrec_invoicelineheaderadjustment
	group by submetaobjectname
	order by submetaobjectname

select id, posubmitteddatefrom, posubmitteddateto, stockregistereddatefrom, stockregistereddateto
from Landing.mend.invrec_invoicelineheaderbulkadjustment

	select count(*)
	from Landing.mend.invrec_invoicelineheaderbulkadjustment

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select id, "enabled", createddate
from Landing.mend.invrec_supplieradjustment

select id, description, name
from Landing.mend.invrec_percentageadjustment

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select id, 
	carriagetype, cost, 
	comment, 
	createddate, changeddate
from Landing.mend.invrec_carriage

	select count(*)
	from Landing.mend.invrec_carriage

	select carriagetype, count(*)
	from Landing.mend.invrec_carriage
	group by carriagetype
	order by carriagetype

--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------

select id, 
	status, reference, creditnoteno, netsuiteno, 
	processeddate, comment,
	createddate, totalamount, creditdate, vatcreditamount, goodscreditamount, carriagecreditamount, misccreditamount
from Landing.mend.invrec_creditnote;

	select count(*)
	from Landing.mend.invrec_creditnote;

	select status, count(*)
	from Landing.mend.invrec_creditnote
	group by status
	order by status;

select id, goodscreditamount, carriagecreditamount
from Landing.mend.invrec_invoicecredit

	select count(*)
	from Landing.mend.invrec_invoicecredit