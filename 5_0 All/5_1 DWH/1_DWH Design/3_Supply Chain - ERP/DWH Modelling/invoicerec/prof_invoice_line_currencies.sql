

		select top 1000 count(*) over (), 
			il.invoicelineid invoicelineid_bk, 
			il.invoiceid invoiceid_bk, il.stockitemid stockitemid_bk, il.receiptlineid receiptlineid_bk, 
			il.quantity quantity, 
			il.unitcost unit_cost, il.totalcost total_cost, il.adjustedunitcost adjusted_unit_cost, il.adjustedtotalcost adjusted_total_cost, il.adjustmentapplied adjustment_applied, 
			--i.local_to_wh_rate, i.local_to_global_rate, 
			rl.local_to_wh_rate, rl.local_to_global_rate, 
			i.currency_code, i.currency_code_wh, 
			'N' delete_f
		from 
				Landing.mend.gen_invrec_invoiceline_v il
			inner join
				(select invoiceid_bk invoiceid, local_to_wh_rate, local_to_global_rate, currency_code, currency_code_wh
				from Landing.aux.invrec_dim_invoice_reconciliation) i on il.invoiceid = i.invoiceid
			inner join
				Landing.aux.rec_fact_receipt_line rl on il.receiptlineid = rl.receiptlineid_bk
		-- where il.invoicelineid = 181832834955127303
		-- where i.local_to_wh_rate <> rl.local_to_wh_rate

		select *
		from Landing.aux.rec_fact_receipt_line
		where receiptlineid_bk = 74590868831857212
