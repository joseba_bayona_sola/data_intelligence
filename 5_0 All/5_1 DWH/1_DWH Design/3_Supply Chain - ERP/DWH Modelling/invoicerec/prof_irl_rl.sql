
drop table #irl

select idInvoiceReconciliationLine_sk, invoicelineid_bk, idReceiptLine_sk_fk,
	invrec_status_name, 
	invoice_ref, net_suite_no, receipt_number,
	reconciled_costs_applied_to_receipt_lines,
	invoice_date, posted_date, 
	SKU, quantity, 
	unit_cost, adjusted_unit_cost, adjustment_applied
into #irl
from Warehouse.invrec.fact_invoice_reconciliation_line_v

	select top 1000 -- count(*) over (), 
		irl.idInvoiceReconciliationLine_sk, irl.invoicelineid_bk, irl.idReceiptLine_sk_fk, rl.receiptlineid_bk,
		irl.invrec_status_name, 
		irl.invoice_ref, irl.net_suite_no, irl.receipt_number,
		irl.reconciled_costs_applied_to_receipt_lines,
		irl.invoice_date, irl.posted_date, 
		irl.SKU, 
		rl.qty_accepted, irl.quantity, 
		rl.purchase_unit_cost,
		irl.unit_cost, irl.adjusted_unit_cost, irl.adjustment_applied, 
		rl2.recinvoicecost, rl2.recinvoicecarriagecost, 
		rl2.costavailableforinvoice, 
		rl2.quantityoninvoice, rl2.quantitymanuallyinvoiced, rl2.quantityavailableforinvoice
	from 
			#irl irl
		inner join
			Warehouse.rec.fact_receipt_line_v rl on irl.idReceiptLine_sk_fk = rl.idReceiptLine_sk
		inner join
			Landing.mend.whship_receiptline rl2 on rl.receiptlineid_bk = rl2.id
	-- where irl.adjustment_applied <> 0
	where rl2.id in (74590868832776642, 74590868833653521, 74590868832776663)
	order by rl.receiptlineid_bk desc
	
select top 1000 count(*) over (),
	id, quantityordered, quantityaccepted, 
	recInvoiceCost, recInvoiceCarriageCost, actualFreightCost
from Landing.mend.whship_receiptline
where (recInvoiceCost <> 0 or recInvoiceCarriageCost <> 0 or actualFreightCost <> 0) and
	quantityaccepted <> 0 -- and quantityaccepted <> quantityordered
	