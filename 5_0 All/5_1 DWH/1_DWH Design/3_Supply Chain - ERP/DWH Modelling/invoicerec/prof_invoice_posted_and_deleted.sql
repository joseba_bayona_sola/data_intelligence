
select top 1000  *
from Landing.aux.invrec_dim_invoice_reconciliation
where delete_f = 'Y'

select *
from Landing.aux.invrec_fact_invoice_reconciliation_line
where delete_f = 'Y' 
	and invoicelineid_bk in (select distinct invoicelineid_bk from Landing.aux.stock_dim_wh_stock_item_batch_hist_info)

select wsibh.*
from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
	inner join
		(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk

select count(*) over (partition by wsibh2.warehousestockitembatchid_bk) num_r, irl.invoicelineid_bk, wsibh2.*
from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
	inner join
		(select distinct invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh2 on wsibh.warehousestockitembatchid_bk = wsibh2.warehousestockitembatchid_bk
where wsibh2.receiptlineid_bk in (74590868832572942, 74590868832677419)
order by num_r desc, wsibh2.warehousestockitembatchid_bk, wsibh.invoicelineid_bk

select top 1000  *
from Landing.aux.invrec_fact_invoice_reconciliation_line
where invoicelineid_bk = 181832834955357847

select top 1000 *
from Warehouse.invrec.dim_invoice_reconciliation
where delete_f = 'Y'


	select irl.idInvoiceReconciliationLine_sk, irl.invoicelineid_bk, ir.idInvoiceReconciliation_sk, irl.idReceiptLine_sk_fk,
		ir.warehouse_name, ir.supplier_type_name, ir.supplier_name, ir.invrec_status_name, ir.created_by,
		ir.invoice_ref, ir.net_suite_no, rl.receipt_number,
		ir.invoice_date, ir.approved_date, ir.posted_date, ir.payment_due_date, 
		si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
		si.product_id_magento, si.product_family_name, si.packsize, 
		si.parameter, si.product_description,
		si.SKU, si.stock_item_description,
		irl.quantity, 

		irl.unit_cost, irl.total_cost, irl.adjusted_unit_cost, irl.adjusted_total_cost, irl.adjustment_applied, 
		convert(decimal(28, 8), irl.unit_cost * irl.local_to_wh_rate) local_unit_cost, 
		convert(decimal(28, 8), irl.total_cost * irl.local_to_wh_rate) local_total_cost, 
		convert(decimal(28, 8), irl.adjusted_unit_cost * irl.local_to_wh_rate) local_adjusted_unit_cost, 
		convert(decimal(28, 8), irl.adjusted_total_cost * irl.local_to_wh_rate) local_adjusted_total_cost, 
		convert(decimal(28, 8), irl.adjustment_applied * irl.local_to_wh_rate) local_adjustment_applied, 

		convert(decimal(28, 8), irl.unit_cost * irl.local_to_global_rate) global_unit_cost, 
		convert(decimal(28, 8), irl.total_cost * irl.local_to_global_rate) global_total_cost, 
		convert(decimal(28, 8), irl.adjusted_unit_cost * irl.local_to_global_rate) global_adjusted_unit_cost, 
		convert(decimal(28, 8), irl.adjusted_total_cost * irl.local_to_global_rate) global_adjusted_total_cost, 
		convert(decimal(28, 8), irl.adjustment_applied * irl.local_to_global_rate) global_adjustment_applied, 
		
		irl.local_to_wh_rate, irl.local_to_global_rate, 
		irl.currency_code, irl.currency_code_wh
	from 
			Warehouse.invrec.fact_invoice_reconciliation_line irl
		inner join
			Warehouse.invrec.dim_invoice_reconciliation_v ir on irl.idInvoiceReconciliation_sk_fk = ir.idInvoiceReconciliation_sk
		left join
			Warehouse.prod.dim_stock_item_v si on irl.idStockItem_sk_fk = si.idStockItem_sk			
		inner join
			Warehouse.rec.fact_receipt_line_v rl on irl.idReceiptLine_sk_fk = rl.idReceiptLine_sk
	where irl.delete_f = 'Y' and posted_date is not null
		and invoicelineid_bk in (select distinct invoicelineid_bk from Landing.aux.stock_dim_wh_stock_item_batch_hist_info)
	order by invoice_date

	------------------------------------------


select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_hist_v
where warehousestockitembatchid_bk = 43910096369298655
order by stock_date

select top 1000 *
from Warehouse.tableau.stock_report_hist_ds_v
where warehousestockitembatchid_bk = 43910096369298655
order by stock_date

	------------------------------------------

select wsibh.*
from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
	inner join
		(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk

		-- Update with T flag batches: Physically Deleted IRL 
		update trg
		set trg.update_type = 'T'
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info trg
			inner join
				(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') src on trg.invoicelineid_bk = src.invoicelineid_bk

		-- Update with D flag batches: WSIB from Physically Deleted IRL 
		update trg
		set trg.update_type = 'D'
		from 
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info trg
			inner join
				(select distinct wsibh.warehousestockitembatchid_bk
				from 
						Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
					inner join
						(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk) src 
							on trg.warehousestockitembatchid_bk = src.warehousestockitembatchid_bk and trg.update_type not in ('T')


select count(*) over (partition by wsibh2.warehousestockitembatchid_bk) num_r, irl.invoicelineid_bk, wsibh2.*
from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
	inner join
		(select distinct invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh2 on wsibh.warehousestockitembatchid_bk = wsibh2.warehousestockitembatchid_bk
order by num_r desc, wsibh2.warehousestockitembatchid_bk, wsibh.invoicelineid_bk

select irl.delete_f, wsibh.update_type, wsibh.num_rep, rl.receipt_number, rl.sku, 
	wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, wsibh.receiptlineid_bk, wsibh.batch_stock_register_date, wsibh.invoice_posted_date, 
	wsibh.batch_id, wsibh.magentoProductID_int, 
	wsibh.qty_received, wsibh.last_qty_on_hand, wsibh.last_stock_date
from 
	Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
inner join
	Warehouse.rec.fact_receipt_line_v rl on wsibh.receiptlineid_bk = rl.receiptlineid_bk
left join
	Landing.aux.invrec_fact_invoice_reconciliation_line irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk
where rl.receipt_number in ('R00029981', 'R00029044') 
	-- and 
	-- irl.delete_f = 'Y'
	-- and sku = '01088B1D2CJ0000000000061'
	-- and wsibh.num_rep > 1 -- 1849
	 and wsibh.update_type <> 'T' -- and (irl.delete_f <> 'Y' or irl.delete_f is null) -- 931
	-- wsibh.batch_id in (2403105, 2403100, 2403101, 2403102, 2403103, 2403104, 2406933, 2406932, 2406936, 2406935, 2406934, 2418474, 2418509, 2415840, 2415842, 2415841, 2418496)
order by rl.receipt_number, rl.sku, wsibh.batch_id, irl.delete_f

select receiptlineid_bk, receipt_number, purchase_order_number, product_id_magento, product_family_name, sku, stock_item_description, qty_accepted
from Warehouse.rec.fact_receipt_line_v
where receipt_number in ('R00029981', 'R00029044') -- and sku = '01088B1D2CJ0000000000061'
order by receipt_number, purchase_order_number, sku

select *
from Warehouse.stock.dim_wh_stock_item_batch_rec_v
where receipt_number in ('R00029981', 'R00029044')

-----------------


select distinct wsibh.warehousestockitembatchid_bk
into #wsib
from 
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh
	inner join
		(select invoicelineid_bk from Landing.aux.invrec_fact_invoice_reconciliation_line where delete_f = 'Y') irl on wsibh.invoicelineid_bk = irl.invoicelineid_bk

			select wsib.warehousestockitembatchid_bk, -1 invoicelineid_bk, 
				wsib.receiptlineid_bk, null batchstockmovementid_bk, 
				wsib.batch_stock_register_date, null invoice_posted_date,
				wsib.qty_received - isnull(irl.quantity, 0) qty_received,
				wsib.delete_f, 'F' update_type
			from 
					Landing.aux.stock_dim_wh_stock_item_batch wsib
				inner join
					#wsib wsib2 on wsib.warehousestockitembatchid_bk = wsib2.warehousestockitembatchid_bk
				inner join
					Landing.aux.rec_fact_receipt_line rl on wsib.receiptlineid_bk = rl.receiptlineid_bk
				left join
					(select irl.receiptlineid_bk, sum(irl.quantity) quantity
					from 
							Landing.aux.invrec_fact_invoice_reconciliation_line irl
						inner join
							Landing.aux.invrec_dim_invoice_reconciliation ir on irl.invoiceid_bk = ir.invoiceid_bk
					where ir.posted_date is not null and irl.delete_f = 'N'
					group by irl.receiptlineid_bk) irl on rl.receiptlineid_bk = irl.receiptlineid_bk
			where (irl.receiptlineid_bk is not null and wsib.qty_received - irl.quantity > 0) or irl.receiptlineid_bk is null

			select wsib.warehousestockitembatchid_bk, irl.invoicelineid_bk, 
				wsib.receiptlineid_bk, null batchstockmovementid_bk, 
				wsib.batch_stock_register_date, irl.posted_date invoice_posted_date,
				irl.quantity qty_received,
				wsib.delete_f, 'F' update_type
			from 
					Landing.aux.stock_dim_wh_stock_item_batch wsib
				inner join
					#wsib wsib2 on wsib.warehousestockitembatchid_bk = wsib2.warehousestockitembatchid_bk
				inner join
					Landing.aux.rec_fact_receipt_line rl on wsib.receiptlineid_bk = rl.receiptlineid_bk
				inner join
					(select irl.invoicelineid_bk, irl.receiptlineid_bk, ir.posted_date, irl.quantity
					from 
							Landing.aux.invrec_fact_invoice_reconciliation_line irl
						inner join
							Landing.aux.invrec_dim_invoice_reconciliation ir on irl.invoiceid_bk = ir.invoiceid_bk
					where ir.posted_date is not null and irl.delete_f = 'N') irl on rl.receiptlineid_bk = irl.receiptlineid_bk