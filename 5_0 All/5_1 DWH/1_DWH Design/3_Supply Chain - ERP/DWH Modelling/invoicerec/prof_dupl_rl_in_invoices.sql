
select num_rep, 
	wsibh.warehousestockitembatchid_bk, wsibh.invoicelineid_bk, 
	wsibh.receiptlineid_bk, wsibh.batchstockmovementid_bk, 
	wsibh.batch_stock_register_date, wsibh.invoice_posted_date,
	wsibh.batch_id, wsibh.qty_received
into #duplicate_rl_irl
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v wsibh
where num_rep > 1 

select receiptlineid, count(*) num_rep
into #duplicate_rl_irl2
from Landing.mend.invrec_invoiceline_receiptline
group by receiptlineid
having count(*) > 1

select t1.*, t2.num_rep,  
	rl.unitcost, rl.quantityaccepted, rl.quantitymoved, rl.costavailableforinvoice, rl.recinvoicecost, rl.recinvoicecarriagecost
from 
		#duplicate_rl_irl t1
	left join
		#duplicate_rl_irl2 t2 on t1.receiptlineid_bk = t2.receiptlineid	
	left join
		Landing.mend.whship_receiptline rl on t1.receiptlineid_bk = rl.id
order by t1.num_rep desc, t1.receiptlineid_bk, t1.invoicelineid_bk
	
