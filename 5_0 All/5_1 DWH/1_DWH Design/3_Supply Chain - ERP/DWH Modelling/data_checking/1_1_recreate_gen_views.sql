﻿

drop view public.gen_wh_batchstockmovement_v;
drop view public.gen_wh_stockmovement_v;
drop view public.gen_wh_warehousestockitembatch_v;
drop view public.gen_wh_warehousestockitem_v;
drop view public.gen_prod_stockitem_v;
drop view public.gen_prod_product_v;

-------------------------------------------------------


-- gen_wh_warehouse_v

create view public.gen_wh_warehouse_v as
	select w.id warehouseid, 
		w.code, w.name, w.shortName, w.warehouseType, w.snapWarehouse, 
		w.snapCode, w.scurriCode, 
		w.active, w.wms_active, w.useHoldingLabels, w.stockcheckignore, 
		c.currencycode
	from 
			inventory$warehouse w
		inner join
			inventory$warehouse_country wc on w.id = wc.inventory$warehouseid
		inner join		
			countrymanagement$country_currency cc on wc.countrymanagement$countryid = cc.countrymanagement$countryid
		inner join
			countrymanagement$currency c on cc.countrymanagement$currencyid = c.id;

-- gen_comp_currency_v

-- gen_comp_currency_exchange_v

-- gen_prod_product_v

create view public.gen_prod_product_v as
	select pf.id productfamilyid, p.id productid,
		-- case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pf.id end magentoProductID_int,
		pf.magentoProductID,
		pf.magentoSKU, pf.name, 
		p.valid, p.SKU SKU_product, p.oldSKU, p.description, 
		cl.bc, cl.di, cl.po, cl.cy, cl.ax,  cl.ad, cl._do, cl.co, cl.co_EDI
	from 
			product$productfamily pf
		left join 
			product$product_productfamily pfp on pf.id = pfp.product$productfamilyid
		left join
			product$product p on pfp.product$productid = p.id
		left join	
			product$contactlens cl on p.id = cl.id;


	
-- gen_prod_stockitem_v

create view public.gen_prod_stockitem_v as
	select p.productfamilyid, p.productid, si.id stockitemid, 
		p.magentoProductID,
		p.magentoSKU, p.name, 
		p.valid, p.SKU_product, p.oldSKU, p.description, 
		si.packSize, si.SKU, si.stockitemdescription, -- si.oldSKU, 
		p.bc, p.di, p.po, p.cy, p.ax, p.ad, p._do, p.co, p.co_EDI, 
		si.manufacturerArticleID, si.manufacturerCodeNumber,
		si.SNAPUploadStatus 
	from 
			public.gen_prod_product_v p
		left join
			product$stockitem_product sip on p.productid = sip.product$productid
		left join
			product$stockitem si on sip.product$stockitemid = si.id;


		
-- gen_wh_warehousestockitem_v

create view public.gen_wh_warehousestockitem_v as
	select si.productfamilyid, si.productid, si.stockitemid, wsi.id warehousestockitemid,
		w.warehouseid,
		si.magentoProductID,
		si.magentoSKU, si.name, si.SKU_product, si.description, si.packSize, si.SKU, si.stockitemdescription,
		si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, si.co_EDI, 
		w.code, w.name warehouse_name, wsi.id_string, 
		wsi.actualstockqty, wsi.availableqty, wsi.outstandingallocation, wsi.allocatedstockqty, wsi.onhold, wsi.dueinqty, wsi.forwarddemand, 
		wsi.stocked, 		
		wsi.stockingmethod, 
		case when (wsibwsi.warehousestockitemid is not null) then 'Y' else 'N' end wh_stock_item_batch_f
	from
			public.gen_prod_stockitem_v si
		inner join	
			inventory$warehousestockitem_stockitem wsisi on si.stockitemid = wsisi.product$stockitemid
		inner join
			inventory$warehousestockitem wsi on wsisi.inventory$warehousestockitemid = wsi.id
		inner join
			inventory$located_at wsiw on wsi.id = wsiw.inventory$warehousestockitemid
		inner join
			public.gen_wh_warehouse_v w on wsiw.inventory$warehouseid = w.warehouseid
		left join
			(select distinct inventory$warehousestockitemid warehousestockitemid
			from inventory$warehousestockitembatch_warehousestockitem) wsibwsi on wsi.id = wsibwsi.warehousestockitemid;



-- gen_wh_warehousestockitembatch_v

create view public.gen_wh_warehousestockitembatch_v as
	select wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, wsib.id warehousestockitembatchid, wsi.warehouseid,
		wsibrl.warehouseshipments$receiptlineid receiptlineid,
		case when (bstwsib.warehousestockitembatchid is null) then 0 else 1 end batchstockmovement_f, 
		case when (rstwsib.warehousestockitembatchid is null) then 0 else 1 end batchrepair_f,
		wsi.magentoProductID,
		wsi.name, wsi.stockitemdescription, wsi.packSize, -- wsi.magentoSKU, wsi.SKU_product, wsi.SKU
		wsi.code, wsi.warehouse_name, wsi.id_string, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.autoadjusted, 
		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, wsib.forwardDemand, wsib.registeredQuantity,
		rl.quantityaccepted,
		wsib.groupFIFODate, wsib.arrivedDate, wsib.confirmedDate, wsib.stockregisteredDate, wsib.receiptCreatedDate,
		wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, 
		wsib.inteCoCarriageUnitCost interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo,
		wsib.exchangeRate, wsib.currency
	from
			public.gen_wh_warehousestockitem_v wsi
		inner join
			inventory$warehousestockitembatch_warehousestockitem wsiwsib on wsi.warehousestockitemid = wsiwsib.inventory$warehousestockitemid
		inner join
			inventory$warehousestockitembatch wsib on wsiwsib.inventory$warehousestockitembatchid = wsib.id
		left join
			inventory$warehousestockitembatch_receiptline wsibrl on wsib.id = wsibrl.inventory$warehousestockitembatchid
		left join
			warehouseshipments$receiptline rl on wsibrl.warehouseshipments$receiptlineid = rl.id
		left join
			(select inventory$warehousestockitembatchid warehousestockitembatchid, count(*) num
			from inventory$fixbatchstockmoves_warehousestockitembatch
			group by inventory$warehousestockitembatchid) bstwsib on wsib.id = bstwsib.warehousestockitembatchid
		left join
			(select inventory$warehousestockitembatchid warehousestockitembatchid, count(*) num
			from inventory$repairstockbatchs_warehousestockitembatch
			group by inventory$warehousestockitembatchid) rstwsib on wsib.id = rstwsib.warehousestockitembatchid;


-- gen_wh_stockmovement_v
create view public.gen_wh_stockmovement_v as
	select sm.id stockmovementid, wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, 
		sm.moveid, sm.movelineid, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.dateCreated stockmovement_createdate, sm.dateClosed stockmovement_closeddate,
		wsi.magentoProductID,
		wsi.name, wsi.stockitemdescription, 	
		wsi.code, wsi.warehouse_name, wsi.id_string
	from
			inventory$stockmovement sm
		inner join
			inventory$stockmovement_warehousestockitem smwsi on sm.id = smwsi.inventory$stockmovementid
		inner join
			public.gen_wh_warehousestockitem_v wsi on smwsi.inventory$warehousestockitemid = wsi.warehousestockitemid;


	
-- gen_wh_batchstockmovement_v
create view public.gen_wh_batchstockmovement_v as
	select bsm.id batchstockmovementid, sm.stockmovementid, wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid,
		sm.moveid, sm.movelineid, bsm.batchstockmovement_id, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.stockmovement_createdate, sm.stockmovement_closeddate,
		bsm.createdDate batchstockmovement_createdate, bsm.batchstockmovementtype, bsm.quantity, bsm.comment,
		wsib.magentoProductID,
		wsib.name, wsib.stockitemdescription, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		wsib.batch_id
	from
			public.gen_wh_stockmovement_v sm
		full join
			inventory$batchstockmovement_stockmovement bsmsm on sm.stockmovementid = bsmsm.inventory$stockmovementid
		full join
			inventory$batchstockmovement bsm on bsmsm.inventory$batchstockmovementid = bsm.id
		inner join
			inventory$batchstockmovement_warehousestockitembatch bsmwsi on bsm.id = bsmwsi.inventory$batchstockmovementid
		inner join
			public.gen_wh_warehousestockitembatch_v wsib on bsmwsi.inventory$warehousestockitembatchid = wsib.warehousestockitembatchid;


