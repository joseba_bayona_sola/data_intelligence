﻿
drop view public.mend_stock_batchstockmovement_v;
drop view public.mend_stock_warehousestockitembatch_v;

-- mend_stock_batchstockmovement_v

create view public.mend_stock_batchstockmovement_v as
	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		case 
			when (stock_adjustment_type_bk in ('Manual_Positive')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_2')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement <> wsib.receivedquantity) then qty_movement 
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement = wsib.receivedquantity 
				and wsib.quantityaccepted is not null and wsib.quantityaccepted = wsib.receivedquantity) then qty_movement
			else 0 
		end qty_registered, 
		case when (stock_adjustment_type_bk in ('Negative', 'Manual_Negative')) then qty_movement else 0 end qty_disposed, 
		-- case when (bsm.batch_movement_date between stp.from_date and stp.to_date) then dateadd(hour, 1, bsm.batch_movement_date) else bsm.batch_movement_date end batch_movement_date,
		batch_movement_date
	from	
			(select batchstockmovementid batchstockmovementid_bk, 
				batchstockmovement_id batch_stock_move_id, 
				case 
					when (moveid is null) then 'M' || batchstockmovement_id::varchar
					when (moveid = '') then 'SS' || batchstockmovement_id::varchar
					else moveid
				end	move_id, 
				case 
					when (movelineid is null) then 'M' || batchstockmovement_id::varchar
					when (movelineid = '') then 'SS' || batchstockmovement_id::varchar
					else movelineid
				end	move_line_id, 	

				warehousestockitembatchid warehousestockitembatchid_bk, 
				case when (stockadjustment is null) then batchstockmovementtype else stockadjustment end stock_adjustment_type_bk, 
				case when (movetype is null) then 'MANUAL' else movetype end stock_movement_type_bk, 
				case when (coalesce(batchstockmovement_createdate, stockmovement_createdate) < '2017-10-10') then 'SM_PERIOD_1' else 'SM_PERIOD_2' end stock_movement_period_bk, 
	
				quantity qty_movement, 
				coalesce(batchstockmovement_createdate, stockmovement_createdate) batch_movement_date	
			from 
					public.gen_wh_batchstockmovement_v bsm) bsm

		inner join
			public.gen_wh_warehousestockitembatch_v wsib on bsm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid;	



-- mend_stock_warehousestockitembatch_v

create view public.mend_stock_warehousestockitembatch_v as
	select wsib.warehousestockitembatchid warehousestockitembatchid_bk, 
		wsib.warehousestockitemid warehousestockitemid_bk, 
		case 
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 0) then 'NO_RECEIPT_NO_MOV'
			when (wsib.receiptlineid is null and wsib.batchstockmovement_f = 1) then 'NO_RECEIPT_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 0) then 'RECEIPT_NO_MOV'
			when (wsib.receiptlineid is not null and wsib.batchstockmovement_f = 1) then 'RECEIPT_MOV'
		end stock_batch_type_bk,
		wsib.receiptlineid receiptlineid_bk,
		wsib.batch_id, wsib.fullyallocated fully_allocated_f, wsib.fullyissued fully_issued_f, wsib.autoadjusted auto_adjusted_f, 

		wsib.receivedquantity qty_received, wsib.receivedquantity + wsib.registeredQuantity - wsib.allocatedquantity - wsib.disposedQuantity - wsib.onHoldQuantity qty_available, 
		wsib.allocatedquantity - wsib.issuedquantity qty_outstanding_allocation, 
		wsib.allocatedquantity qty_allocated_stock, wsib.issuedquantity qty_issued_stock, 
		wsib.onholdquantity qty_on_hold, wsib.registeredquantity qty_registered, wsib.disposedquantity qty_disposed, 
		coalesce(wsibsm.qty_registered, 0) qty_registered_sm, coalesce(wsibsm.qty_disposed, 0) qty_disposed_sm, 

		wsib.arrivedDate batch_arrived_date, wsib.confirmedDate batch_confirmed_date, wsib.stockregistereddate batch_stock_register_date, 

		wsib.productUnitCost local_product_unit_cost, wsib.carriageUnitCost local_carriage_unit_cost, wsib.dutyUnitCost local_duty_unit_cost, wsib.totalUnitCost local_total_unit_cost, 
		wsib.interCoCarriageUnitCost local_interco_carriage_unit_cost, wsib.totalUnitCostIncInterCo local_total_unit_cost_interco
		-- wsib.exchangeRate local_to_global_rate, wsib.currency currency_code
	from	
				(select wsib.warehousestockitembatchid, wsib.warehousestockitemid, wsib.receiptlineid, wsib.productfamilyid, 
					wsib.batch_id, wsib.fullyallocated, wsib.fullyissued, wsib.autoadjusted, 
					wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onholdquantity, wsib.registeredquantity, wsib.disposedquantity, 
					wsib.arrivedDate, wsib.confirmedDate, wsib.stockregistereddate, 
					wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, wsib.interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo, 
					-- case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end exchangeRate, wsib.currency,
					wsib.batchstockmovement_f
				from
					(select warehousestockitembatchid, warehousestockitemid, receiptlineid, productfamilyid, w.warehouseid,
						batch_id, fullyallocated, fullyissued, autoadjusted, 
						receivedquantity, allocatedquantity, issuedquantity, onholdquantity, registeredquantity, disposedquantity, 
						arrivedDate, confirmedDate, stockregistereddate, 
						productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, interCoCarriageUnitCost, totalUnitCostIncInterCo, 
						exchangeRate, coalesce(currency, w.currencycode) currency,
						batchstockmovement_f
					from 
							public.gen_wh_warehousestockitembatch_v wsib
						left join
							public.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid) wsib) wsib
							
		left join
			(select warehousestockitembatchid_bk, 
				sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed
			from public.mend_stock_batchstockmovement_v
			group by warehousestockitembatchid_bk) wsibsm on wsib.warehousestockitembatchid = wsibsm.warehousestockitembatchid_bk;

		

				
	
	