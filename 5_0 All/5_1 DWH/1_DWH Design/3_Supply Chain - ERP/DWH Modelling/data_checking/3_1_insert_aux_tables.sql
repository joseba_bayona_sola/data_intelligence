﻿
drop table public.dim_wh_stock_item;
drop table public.dim_wh_stock_item_batch;
drop table public.fact_wh_stock_item_batch_movement;


-- dim_wh_stock_item

	-- create table
	create table public.dim_wh_stock_item(
		warehousestockitemid_bk			bigint NOT NULL, 

		warehouseid_bk				bigint NOT NULL, 
		stockitemid_bk				bigint NOT NULL, 
		stock_method_type_bk			varchar(20) NOT NULL, 

		wh_stock_item_description		varchar(200),

		qty_received				decimal(28, 8), 
		qty_available				decimal(28, 8), 
		qty_outstanding_allocation		decimal(28, 8), 
		qty_allocated_stock			decimal(28, 8), 
		qty_issued_stock			decimal(28, 8), 
		qty_on_hold				decimal(28, 8), 
		qty_registered				decimal(28, 8), 
		qty_disposed				decimal(28, 8), 
		qty_due_in				decimal(28, 8), 

		stocked					boolean);


	-- insert statement
	insert into public.dim_wh_stock_item (warehousestockitemid_bk,
		warehouseid_bk, stockitemid_bk, stock_method_type_bk, 
		wh_stock_item_description, 

		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, qty_due_in,
		stocked)

		select wsi.warehousestockitemid warehousestockitemid_bk,
			wsi.warehouseid_bk, wsi.stockitemid_bk, wsi.stock_method_type_bk, 
			wsi.wh_stock_item_description, 

			coalesce(wsib.qty_received, 0) qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, coalesce(wsib.qty_issued_stock, 0) qty_issued_stock, 
			wsi.qty_on_hold, coalesce(wsib.qty_registered, 0) qty_registered, coalesce(wsib.qty_disposed, 0) qty_disposed, wsi.qty_due_in,
			wsi.stocked
		from
				(select warehousestockitemid, 
					warehouseid warehouseid_bk, stockitemid stockitemid_bk, stockingmethod stock_method_type_bk, 
					id_string wh_stock_item_description, 

					availableqty qty_available, outstandingallocation qty_outstanding_allocation, allocatedstockqty qty_allocated_stock, 
					onhold qty_on_hold, dueinqty qty_due_in,
					stocked stocked
				from 
						public.gen_wh_warehousestockitem_v wsi) wsi
			inner join
				(select warehousestockitemid_bk, 
					sum(qty_received) qty_received, sum(qty_issued_stock) qty_issued_stock, 
					sum(qty_registered_sm) qty_registered, sum(qty_disposed_sm) qty_disposed
				from public.mend_stock_warehousestockitembatch_v
				group by warehousestockitemid_bk) wsib on wsi.warehousestockitemid = wsib.warehousestockitemid_bk;

	

-- dim_wh_stock_item_batch

	-- create table
	create table public.dim_wh_stock_item_batch(
		warehousestockitembatchid_bk		bigint NOT NULL, 

		warehousestockitemid_bk			bigint NOT NULL, 
		stock_batch_type_bk			varchar(20) NOT NULL, 
		receiptlineid_bk			bigint,

		batch_id				bigint,

		fully_allocated_f			boolean, 
		fully_issued_f				boolean, 
		auto_adjusted_f				boolean, 
		
		qty_received				decimal(28, 8), 
		qty_available				decimal(28, 8), 
		qty_outstanding_allocation		decimal(28, 8), 
		qty_allocated_stock			decimal(28, 8), 
		qty_issued_stock			decimal(28, 8), 
		qty_on_hold				decimal(28, 8), 
		qty_registered				decimal(28, 8), 
		qty_disposed				decimal(28, 8), 

		qty_registered_sm			decimal(28, 8), 
		qty_disposed_sm				decimal(28, 8), 

		batch_arrived_date			timestamp without time zone,
		batch_confirmed_date			timestamp without time zone,
		batch_stock_register_date		timestamp without time zone,

		local_product_unit_cost			decimal(28, 8), 
		local_carriage_unit_cost		decimal(28, 8), 
		local_duty_unit_cost			decimal(28, 8), 
		local_total_unit_cost			decimal(28, 8), 

		local_interco_carriage_unit_cost	decimal(28, 8), 
		local_total_unit_cost_interco		decimal(28, 8), 

		local_to_global_rate			decimal(12, 4),
		currency_code				varchar(10));

	-- insert statement
	insert into public.dim_wh_stock_item_batch(warehousestockitembatchid_bk, warehousestockitemid_bk, 
		stock_batch_type_bk, receiptlineid_bk,
		batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 

		qty_received, qty_available, qty_outstanding_allocation, 
		qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, 
		qty_registered_sm, qty_disposed_sm, 

		batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 

		local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
		local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
		local_to_global_rate, currency_code)

		select warehousestockitembatchid_bk, warehousestockitemid_bk, 
			stock_batch_type_bk, receiptlineid_bk,
			batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 

			qty_received, qty_available, qty_outstanding_allocation, 
			qty_allocated_stock, qty_issued_stock, 
			qty_on_hold, qty_registered, qty_disposed, 
			qty_registered_sm, qty_disposed_sm, 

			batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 

			local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
			local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
			null local_to_global_rate, null currency_code
		from public.mend_stock_warehousestockitembatch_v;
		
-- fact_wh_stock_item_batch_movement

	-- create table
	create table public.fact_wh_stock_item_batch_movement(
		batchstockmovementid_bk			bigint NOT NULL, 

		batch_stock_move_id			bigint NOT NULL, 
		move_id					varchar(20) NOT NULL,
		move_line_id				varchar(20) NOT NULL,

		warehousestockitembatchid_bk		bigint NOT NULL, 
		stock_adjustment_type_bk		varchar(20) NOT NULL, 
		stock_movement_type_bk			varchar(20) NOT NULL, 
		stock_movement_period_bk		varchar(20), 

		qty_movement				decimal(28, 8), 
		qty_registered				decimal(28, 8), 
		qty_disposed				decimal(28, 8), 

		batch_movement_date			timestamp without time zone);
	
	-- insert statement
	insert into public.fact_wh_stock_item_batch_movement(batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		qty_registered, qty_disposed, 
		batch_movement_date) 

		select batchstockmovementid_bk, 
			batch_stock_move_id, move_id, move_line_id, 
			warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
			qty_movement, 
			qty_registered, qty_disposed, 
			batch_movement_date
		from public.mend_stock_batchstockmovement_v;	

	