﻿-- dim_wh_stock_item

create view public.dim_wh_stock_item_v as
	select wsi.warehousestockitemid_bk, 
		w.name warehouse_name, si.magentoProductID product_id_magento, wsi.stock_method_type_bk stock_method_type_name, 

		si.name product_family_name, si.packsize, 
		si.description product_description,
		si.SKU, si.stockitemdescription stock_item_description, wsi.wh_stock_item_description, 

		wsi.qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, wsi.qty_issued_stock, 
		wsi.qty_on_hold, wsi.qty_registered, wsi.qty_disposed, wsi.qty_due_in,
		wsi.stocked
	from 
			public.dim_wh_stock_item wsi
		inner join
			public.gen_prod_stockitem_v si on wsi.stockitemid_bk = si.stockitemid
		inner join
			public.gen_wh_warehouse_v w on wsi.warehouseid_bk = w.warehouseid;	

-- dim_wh_stock_item_batch

create view public.dim_wh_stock_item_batch_v as
	select wsib.warehousestockitembatchid_bk, wsi.warehousestockitemid_bk, wsib.receiptlineid_bk receiptlineid_bk,
		wsi.warehouse_name, wsi.product_id_magento, wsi.stock_method_type_name, wsib.stock_batch_type_bk stock_batch_type_name,

		wsi.product_family_name, wsi.packsize, 
		wsi.product_description,
		wsi.SKU, wsi.stock_item_description, wsi.wh_stock_item_description, wsib.batch_id, 

		wsib.qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_allocated_stock, wsib.qty_issued_stock, 
		wsib.qty_on_hold, wsib.qty_registered, wsib.qty_disposed, 
		wsib.qty_registered_sm, wsib.qty_disposed_sm, 

		wsib.batch_arrived_date, wsib.batch_confirmed_date, wsib.batch_stock_register_date,

		wsib.local_product_unit_cost, wsib.local_total_unit_cost, 
		wsib.local_to_global_rate, wsib.currency_code
	from 
			public.dim_wh_stock_item_batch wsib
		inner join
			public.dim_wh_stock_item_v wsi on wsib.warehousestockitemid_bk = wsi.warehousestockitemid_bk;
	
-- fact_wh_stock_item_batch_movement

create view public.fact_wh_stock_item_batch_movement_v as
	select wsibm.batchstockmovementid_bk, wsib.warehousestockitembatchid_bk, wsib.warehousestockitemid_bk,

		wsib.warehouse_name, wsib.product_id_magento, wsib.stock_method_type_name, wsib.stock_batch_type_name,

		wsib.product_family_name, wsib.packsize, 
		wsib.product_description,
		wsib.SKU, wsib.stock_item_description, wsib.wh_stock_item_description, wsib.batch_id,

		wsibm.batch_stock_move_id, wsibm.move_id, wsibm.move_line_id,
		wsibm.stock_adjustment_type_bk stock_adjustment_type_name, wsibm.stock_movement_type_bk stock_movement_type_name, wsibm.stock_movement_period_bk stock_movement_period_name, 
	
		wsibm.qty_movement, wsibm.qty_registered, wsibm.qty_disposed, 
		wsibm.batch_movement_date, 

		wsib.local_product_unit_cost, wsib.local_total_unit_cost, 
		wsib.local_to_global_rate, wsib.currency_code
	from 
			public.fact_wh_stock_item_batch_movement wsibm
		inner join
			public.dim_wh_stock_item_batch_v wsib on wsibm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk;		
