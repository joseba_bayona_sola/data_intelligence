﻿
-- 1.1 WH Stock Item 

	select warehousestockitemid_bk, warehouse_name, product_id_magento, stock_item_description, 
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_registered, qty_disposed, qty_on_hold,
		qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold) qty_diff, 
		case when (qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold) > 0) 
			then qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold) else 0 end qty_diff_pos,
		case when (qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold) < 0) 
			then abs(qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold)) else 0 end qty_diff_neg
	from public.dim_wh_stock_item_v
	where product_id_magento = product_id_magento
		-- and qty_available = 0 and qty_allocated_stock = 0 and qty_received = 0 -- 178
		and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 710 (2410) (709) (610)
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold and qty_registered = 0 and qty_disposed = 0 -- 122 (969) (123)
		-- and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock -- 0
	order by product_id_magento, stock_item_description, warehouse_name

-- 1.2 WH Stock Item Batch:
	select warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, batch_stock_register_date,
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, 
		qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, 
		qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold) qty_diff
	from public.dim_wh_stock_item_batch_v
	where product_id_magento = product_id_magento
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold
		-- and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock
		-- and qty_registered <> qty_registered_sm -- 1691 (1636)
		-- and qty_disposed <> qty_disposed_sm -- 2490 (2453)

		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 0
		and qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold -- 1104 (1124) (1104) (1007)
		-- and qty_registered - qty_disposed <> qty_registered_sm - qty_disposed_sm -- 1104 (1124) (1104) (1007)
	-- order by product_id_magento, stock_item_description, warehouse_name, batch_stock_register_date
	order by batch_stock_register_date desc, product_id_magento, stock_item_description, warehouse_name
	