
		select count(*)
		from Landing.mend.gen_prod_product_v;

		select count(*)
		from Landing.mend.gen_prod_stockitem_v;

		select count(*)
		from Landing.mend.gen_wh_warehousestockitem_v;

		select count(*)
		from Landing.mend.gen_wh_warehousestockitembatch_v;

		select count(*)
		from Landing.mend.gen_wh_stockmovement_v;

		select count(*)
		from Landing.mend.gen_wh_batchstockmovement_v;


		select 159846 - 159103 

		select 209696 - 208953

		select 663711 - 660735

		select 1654818 - 1654666

		select 126388 - 126388

		select 144855 - 144701

------------------------------------------

	select id
	into #product
	from openquery(MENDIX_DB_RESTORE,'
		select id
		from public."product$product"')

		select *
		from Landing.mend.prod_product_aud
		where id not in (
			select id
			from #product)

	drop table #product

	select id
	into #wsi
	from openquery(MENDIX_DB_RESTORE,'
		select id
			from public."inventory$warehousestockitem"')

		select *
		from Landing.mend.wh_warehousestockitem_aud
		where id not in (
			select id
			from #wsi)

			and (availableqty <> 0 or allocatedstockqty <> 0)
		
	drop table #wsi