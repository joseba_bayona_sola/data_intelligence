
-- Steps
	-- Add attributes in alloc_order_line_erp_issue_measures, alloc_fact_order_line_erp_issue_aud (issue_date, invoice_posted_date, local_unit_cost_wsib, local_unit_cost_rl, local_unit_cost_irl + discount) // OK
	-- In lnd_stg_get_aux_alloc_order_line_erp_issue_measures SP: Logic Change: Don't set local_total_unit_cost, local_total_cost + Set new attributes // OK
	-- In lnd_stg_get_aux_alloc_order_dim_fact SP: Set cost values (OK) + Special Update for alloc_fact_order_line_erp_issue_aud (No Needed) + Insert on fact_order_line_erp_issue_adj (OK) + Insert on fact_order_line_mag_erp_adj (OK)
		-- Invoice Posted after Issue + Diff Value: Adjustment needed (Insert in Adj + Keep RL value)
	-- In lnd_stg_get_alloc_order_line_erp_issue SP: Return data from alloc_fact_order_line_erp_issue or special alloc_fact_order_line_erp_issue_aud (No Needed - from alloc_fact_order_line_erp_issue)
	-- In lnd_stg_get_aux_alloc_order_header_erp_o: Add orders from just posted invoices (TO DO)


drop table #rl
drop table #irl
drop table #olim
drop table #olim_f

	-- rl
	select r.receiptid_bk, rl.receiptlineid_bk, 
		r.receipt_number,
		isnull (rl.local_invoice_unit_cost * rl.local_to_wh_rate, rl.local_unit_cost * rl.local_to_wh_rate) local_unit_cost,
		rl.local_unit_cost * rl.local_to_wh_rate local_unit_cost_orig, rl.local_invoice_unit_cost * rl.local_to_wh_rate local_invoice_unit_cost, rl.currency_code_wh,
		s.discount_amount, s.discount_date_from, s.discount_date_to
	into #rl
	from 
			Landing.aux.rec_dim_receipt r
		inner join
			(select s.supplier_id supplier_id_bk, s.supplierType supplier_type_bk, s.supplierName supplier_name, s.defaultLeadTime default_lead_time, s.defaultTransitLeadTime default_transit_lead_time, 
				sc.discount_amount, sc.discount_date_from, sc.discount_date_to 
			from 
					Landing.mend.gen_supp_supplier_v s
				left join
					Landing.map.gen_supplier_discount_aud sc on s.supplierName = sc.supplier_name) s on r.supplier_id_bk = s.supplier_id_bk		
		inner join
			Landing.aux.rec_fact_receipt_line rl on r.receiptid_bk = rl.receiptid_bk

	-- irl
	select rl.receiptid_bk, rl.receiptlineid_bk, ir.invoiceid_bk, irl.invoicelineid_bk,
		rl.receipt_number, ir.invoice_ref, ir.net_suite_no, ir.posted_date,
		irl.adjusted_unit_cost * irl.local_to_wh_rate local_unit_cost, irl.currency_code_wh,
		rl.discount_amount, rl.discount_date_from, rl.discount_date_to
	into #irl
	from 
			Landing.aux.invrec_fact_invoice_reconciliation_line irl
		inner join
			#rl rl on irl.receiptlineid_bk = rl.receiptlineid_bk
		inner join
			Landing.aux.invrec_dim_invoice_reconciliation ir on irl.invoiceid_bk = ir.invoiceid_bk

	-- olim
	select oli.batchstockissueid_bk, 
		wsibi.warehousestockitembatchid warehousestockitembatchid_bk, wsibh.receiptlineid_bk, wsibh.invoicelineid_bk,
		olid.issue_date,
		ol.qty_unit qty_unit_order_line, wsib.packSize * wsibi.issuedquantity_issue qty_unit, wsibi.issuedquantity_issue qty_stock, 
		case when (ol.bom_f = 'N') then (wsib.packSize * wsibi.issuedquantity_issue) / ol.qty_unit 
			else 1 / convert(decimal(28, 8), count(*) over (partition by ol.orderlineid_bk)) end qty_percentage, olim.qty_percentage qty_percentage_olim,

		wsib.stockregistereddate
	into #olim
	from 
			Landing.aux.alloc_order_line_erp_issue_oli oli
		inner join
			Landing.aux.alloc_order_line_erp_issue_dates olid on oli.batchstockissueid_bk = olid.batchstockissueid_bk
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
		left join
			Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oliir on oli.batchstockissueid_bk = oliir.batchstockissueid_bk
		inner join
			Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on wsibi.warehousestockitembatchid = wsibh.warehousestockitembatchid_bk and wsibh.update_type <> 'T'
				and case 
					when (oliir.batchstockissueid_bk is not null and oliir.invoicelineid_bk = wsibh.invoicelineid_bk) then 1
					when (oliir.batchstockissueid_bk is null) then 1
					else 0
					end = 1 
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibi.warehousestockitembatchid = wsib.warehousestockitembatchid 
		inner join
			(select olp.orderlineid_bk, qty_unit, olp.bom_f
			from 
					Landing.aux.alloc_order_line_erp_product olp
				inner join
					Landing.aux.alloc_order_line_erp_measures olm on olp.orderlineid_bk = olm.orderlineid_bk) ol on oli.orderlineid_bk = ol.orderlineid_bk
		left join
			Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid -- optional (wh currency, local_to_global_rate in aux wsib OK - Yes: From aux.mend_stock_warehousestockitembatch_v, lnd_stg_get_stock_wh_stock_item_batch)
		inner join
			Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk -- optional
	-- where oliir.batchstockissueid_bk is not null

		select olim.batchstockissueid_bk, 
			olim.warehousestockitembatchid_bk, olim.receiptlineid_bk, olim.invoicelineid_bk, null invoice_reconciled_f, 
				olim.issue_date, case when (c.calendar_date_apply_from is not null) then c.calendar_date_apply_from else irl.posted_date end invoice_posted_date, 
				irl.posted_date, c.calendar_date, c.calendar_date_apply_from, -- add issue_date, invoice_posted_date to Aux Table

			olim.qty_unit_order_line, olim.qty_unit, olim.qty_stock, 
			olim.qty_percentage, 

				wsib.local_product_unit_cost local_unit_cost_wsib, rl.local_unit_cost local_unit_cost_rl, irl.local_unit_cost local_unit_cost_irl, 
				wsib.local_product_unit_cost local_unit_cost_wsib_discount, 
				case when (rl.discount_amount is not null and olim.stockregistereddate between rl.discount_date_from and rl.discount_date_to) 
					then rl.local_unit_cost - (rl.local_unit_cost * rl.discount_amount)
					else rl.local_unit_cost
				end	local_unit_cost_rl_discount, 
				case when (irl.discount_amount is not null and olim.stockregistereddate between irl.discount_date_from and irl.discount_date_to) 
					then irl.local_unit_cost - (irl.local_unit_cost * irl.discount_amount)
					else irl.local_unit_cost
				end	local_unit_cost_irl_discount,

			null local_total_unit_cost, null local_total_cost, 
			null local_total_unit_cost_discount, null local_total_cost_discount, 

			null local_to_global_rate, null order_currency_code, 
			null date_exchange
		into #olim_f
		from 
				#olim olim
			left join
				#rl rl on olim.receiptlineid_bk = rl.receiptlineid_bk
			left join
				#irl irl on olim.invoicelineid_bk = irl.invoicelineid_bk
			inner join
				Landing.aux.stock_dim_wh_stock_item_batch wsib on olim.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk 
			left join
				(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
					dateadd(dd, calendar_date_num * -1, calendar_date) calendar_date_apply_from
				from
					(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
						dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
					from Landing.aux.dim_calendar
					where calendar_date_week_name not in ('Saturday', 'Sunday')) c
				where c.ord_calendar_date_num in (1, 2)) c on convert(date, irl.posted_date) = c.calendar_date -- and wsibm.batch_movement_date between c.calendar_date_apply_from and c.calendar_date
		order by irl.posted_date desc






	-- Final Checks
	select top 1000 count(*) over (), *
	from #rl
	where local_invoice_unit_cost is not null and local_invoice_unit_cost <> local_unit_cost

	select *
	from #olim
	where qty_percentage <> qty_percentage_olim

	select *
	from #olim_f
	-- where receiptlineid_bk is null
	-- where receiptlineid_bk is not null -- and abs(local_unit_cost_wsib - local_unit_cost_rl) > 1 order by abs(local_unit_cost_wsib - local_unit_cost_rl) desc
	where receiptlineid_bk is not null and invoicelineid_bk <> -1 and abs(local_unit_cost_irl - local_unit_cost_rl) > 1 order by abs(local_unit_cost_irl - local_unit_cost_rl) desc

	select *
	from #olim_f
	-- where convert(date, issue_date) <= convert(date, invoice_posted_date) -- Invoice Posted after Issue
		where convert(date, issue_date) <= convert(date, invoice_posted_date) and abs(local_unit_cost_irl - local_unit_cost_rl) > 0 -- Invoice Posted after Issue + Diff Value: Adjustment needed
	order by invoice_posted_date desc	

	select *
	from #olim_f
	-- where local_unit_cost_rl <> local_unit_cost_rl_discount
	-- where local_unit_cost_irl <> local_unit_cost_irl_discount
	order by invoice_posted_date desc	


---------------------------------------------------------------
---------------------------------------------------------------

select oli.*
from 
		Landing.aux.invrec_dim_invoice_reconciliation ir
	inner join
		Landing.aux.invrec_fact_invoice_reconciliation_line irl on ir.invoiceid_bk = irl.invoiceid_bk
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on irl.invoicelineid_bk = wsibh.invoicelineid_bk
	inner join
		Landing.aux.alloc_fact_order_line_erp_issue_aud oli on wsibh.warehousestockitembatchid_bk = oli.warehousestockitembatchid_bk
where ir.posted_date > GETUTCDATE() -2

select distinct ol.orderid_bk
from 
		Landing.aux.invrec_dim_invoice_reconciliation ir
	inner join
		Landing.aux.invrec_fact_invoice_reconciliation_line irl on ir.invoiceid_bk = irl.invoiceid_bk
	inner join
		Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on irl.invoicelineid_bk = wsibh.invoicelineid_bk
	inner join
		Landing.aux.alloc_fact_order_line_erp_issue_aud oli on wsibh.warehousestockitembatchid_bk = oli.warehousestockitembatchid_bk
	inner join
		Landing.aux.alloc_fact_order_line_erp_aud ol on oli.orderlineid_bk = ol.orderlineid_bk
where ir.posted_date > GETUTCDATE() -2