
-- batchstockissueid_bk
-- product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f,
-- product_id_size_bk, quantity_lens_size, optimum_type_size, pack_size_optimum_type_size, optimum_size_f,

-- Add pack_size_qty for 1,2 in Landing.aux.prod_product_family_pack_size_optimum (avoid joining to take the pack_size_qty) -- OK
-- Calculate pack_size_qty for olim (qty_unit/qty_stock) vs take using warehousestockitembatchid_bk -- On gen_wh_warehousestockitembatch_v

select oli.batchstockissueid_bk, oli.orderlineid_bk, oli.issue_id, dense_rank() over (partition by oli.orderlineid_bk order by oli.issue_id),
	ols.product_id_pref_bk, ols.quantity_lens_pref, ols.optimum_type_pref, pfpso1.packsizeid1_bk, pfpso1.quantity_packs_1, pfpso1.size_1, pfpso1.packsizeid2_bk, pfpso1.quantity_packs_2, pfpso1.size_2, 
	-- ols.product_id_size_bk, ols.quantity_lens_size, ols.optimum_type_size
	olim.qty_stock, olim.qty_unit, olim.warehousestockitembatchid_bk, wsib.packSize pack_size
from 
		Landing.aux.alloc_order_line_erp_issue_oli oli
	left join
		Landing.aux.alloc_order_line_erp_service ols on oli.orderlineid_bk = ols.orderlineid_bk 
	left join
		Landing.aux.prod_product_family_pack_size_optimum pfpso1 on ols.product_id_pref_bk = pfpso1.product_id_bk and ols.quantity_lens_pref = pfpso1.quantity_lens and ols.optimum_type_pref =  pfpso1.optimum_type 
	inner join
		Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on olim.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
-- where ols.product_id_pref_bk = 1083
-- order by oli.orderlineid_bk, oli.issue_id
order by ols.quantity_lens_pref, oli.orderlineid_bk, oli.issue_id


select oli.batchstockissueid_bk, oli.orderlineid_bk, ol.bom_f, oli.issue_id, 
	count(*) over (partition by oli.orderlineid_bk) num_rep,
	dense_rank() over (partition by oli.orderlineid_bk order by oli.issue_id) ord_rep,
	ols.product_id_pref_bk, ols.quantity_lens_pref, pfpso1.quantity_packs_1, pfpso1.size_1, pfpso1.quantity_packs_2, pfpso1.size_2, 
	olim.qty_stock, olim.qty_unit, olim.warehousestockitembatchid_bk, wsib.packSize pack_size
from 
		Landing.aux.alloc_order_line_erp_issue_oli oli
	left join
		Landing.aux.alloc_order_line_erp_service ols on oli.orderlineid_bk = ols.orderlineid_bk 
	left join
		Landing.aux.alloc_fact_order_line_erp ol on oli.orderlineid_bk = ol.orderlineid_bk 
	left join
		Landing.aux.prod_product_family_pack_size_optimum pfpso1 on ols.product_id_pref_bk = pfpso1.product_id_bk and ols.quantity_lens_pref = pfpso1.quantity_lens and ols.optimum_type_pref =  pfpso1.optimum_type 
	inner join
		Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on olim.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
where ols.product_id_pref_bk = 1100
	-- and oli.orderlineid_bk = 54324670627867539
-- where ol.bom_f = 'Y'
-- order by oli.orderlineid_bk, oli.issue_id
order by num_rep desc, ord_rep, ols.quantity_lens_pref, oli.orderlineid_bk, oli.issue_id


-------------------------------------------------------------
-------------------------------------------------------------

	-- Cursor with select OL: orderlineid_bk, opt data, size, packs (1) - size, packs (2)

		-- Cursor with select OLI on (1) quantities: batchstockissueid_bk, qty_stock, pack_size / order by size desc - pref 
			-- Compare size(1) with pack_size
			-- Insert in TEMP
			-- Reduce remaining qty

		-- Cursor with select OLI on (2) quantities: batchstockissueid_bk, qty_stock, pack_size / order by size desc - pref 
			-- Compare size(2) with pack_size
			-- Insert in TEMP
			-- Reduce remaining qty

	-- Insert Issue records if didn't go through process