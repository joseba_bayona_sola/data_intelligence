

		select olp.orderlineid_bk, 
			olp.productid_erp_bk, p.magentoProductID_int, p.magentoProductID, p.productfamilyid, olp.qty_unit,
			pfpso1.product_id_bk product_id_pref_bk, pfpso1.quantity_lens quantity_lens_pref, pfpso1.optimum_type optimum_type_pref, 
			pfpso2.product_id_bk product_id_size_bk, pfpso2.quantity_lens quantity_lens_size, pfpso2.optimum_type optimum_type_size, 
			olsl.sucess_f inventory_level_success_f, olsh.purchaseorderlineid_bk purchaseorderlineid_sh_bk
		from 
				Landing.aux.alloc_fact_order_line_erp_aud olp
			inner join
				Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
			left join
				Landing.aux.mag_prod_product_family_v pf on p.productfamilyid = pf.productfamilyid
			left join
				Landing.aux.prod_product_family_pack_size_optimum pfpso1 on pf.product_id_bk = pfpso1.product_id_bk and olp.qty_unit = pfpso1.quantity_lens and pfpso1.optimum_type = 'pref'
			left join
				Landing.aux.prod_product_family_pack_size_optimum pfpso2 on pf.product_id_bk = pfpso2.product_id_bk and olp.qty_unit = pfpso2.quantity_lens and pfpso2.optimum_type = 'size'
			left join
				Landing.aux.alloc_fact_order_line_erp_stock_level olsl on olp.orderlineid_bk = olsl.orderlineid_bk and olsl.preference_order = 1
			left join
				(select orderlineid_bk, max(isnull(purchaseorderlineid_bk, 0)) purchaseorderlineid_bk
				from Landing.aux.alloc_fact_order_line_erp_shortage
				group by orderlineid_bk) olsh on olp.orderlineid_bk = olsh.orderlineid_bk				
		where pfpso1.product_id_bk is null  and olp.qty_unit < 1000
			-- and p.magentoProductID_int = 1083
		order by p.magentoProductID_int, olp.qty_unit

		select distinct p.magentoProductID_int, p.magentoProductID, p.productfamilyid
		from 
				Landing.aux.alloc_fact_order_line_erp_aud olp
			inner join
				Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
			left join
				Landing.aux.mag_prod_product_family_v pf on p.productfamilyid = pf.productfamilyid
			left join
				Landing.aux.prod_product_family_pack_size_optimum pfpso1 on pf.product_id_bk = pfpso1.product_id_bk and olp.qty_unit = pfpso1.quantity_lens and pfpso1.optimum_type = 'pref'
		where pfpso1.product_id_bk is null	
			and olp.qty_unit < 1000
		order by p.magentoProductID_int

		select *
		from Landing.aux.mag_prod_product_family_v
		where productfamilyid in (52917295621604235, 52917295621604237, 52917295621604238, 52917295621606629)

