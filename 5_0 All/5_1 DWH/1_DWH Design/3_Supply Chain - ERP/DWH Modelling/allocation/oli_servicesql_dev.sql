
drop table #alloc_order_line_erp_issue_service

create table #alloc_order_line_erp_issue_service(
	batchstockissueid_bk						bigint NOT NULL, 

	product_id_pref_bk							int,
	quantity_lens_pref							int, 
	optimum_type_pref							varchar(20), 
	pack_size_optimum_type_pref					int,
	optimum_pref_f								char(1));


			select distinct orderlineid_bk, bom_f, product_id_pref_bk, quantity_lens_pref, 
				quantity_packs_1, size_1, quantity_packs_2, size_2, 
				quantity_lens_pref / convert(decimal, size_1)
			select *
			from #oli_service
			-- where orderlineid_bk in (54324670628720808, 54324670628693397)
			where product_id_pref_bk = 1100
			order by quantity_lens_pref, orderlineid_bk

select count(*) over (partition by oli1.batchstockissueid_bk) num_rep,
	oli1.batchstockissueid_bk, oli1.orderlineid_bk, oli1.issue_id, 
	oli1.product_id_pref_bk, oli1.quantity_lens_pref, oli1.quantity_packs_1, oli1.size_1, oli1.quantity_packs_2, oli1.size_2, 
	oli1.qty_stock, oli1.qty_unit, oli1.pack_size, 
		oli2.pack_size_optimum_type_pref, oli2.optimum_pref_f
from 
		#oli_service oli1
	inner join
		#alloc_order_line_erp_issue_service oli2 on oli1.batchstockissueid_bk = oli2.batchstockissueid_bk
-- where oli2.optimum_pref_f <> 'Y'
-- where oli1.orderlineid_bk in (54324670628966098, 54324670628864431, 54324670628789800, 54324670628980993, 54324670628998344, 54324670628976619, 54324670628778585)
order by num_rep desc, oli1.quantity_lens_pref, oli1.orderlineid_bk

	select product_id_pref_bk, count(*), count(case when optimum_pref_f = 'Y' then 1 else null end), 
		count(*), count(case when optimum_pref_f = 'Y' then 1 else null end) * 100 / convert(decimal(12, 4), count(*))
	from #alloc_order_line_erp_issue_service
	group by product_id_pref_bk
	order by count(case when optimum_pref_f = 'Y' then 1 else null end) * 100 / convert(decimal(12, 4), count(*)), product_id_pref_bk

----------------------------------------------------------------------------

select oli.batchstockissueid_bk, oli.orderlineid_bk, ol.bom_f, oli.issue_id, 
	count(*) over (partition by oli.orderlineid_bk) num_rep,
	dense_rank() over (partition by oli.orderlineid_bk order by oli.issue_id) ord_rep,
	ols.product_id_pref_bk, ols.quantity_lens_pref, pfpso1.quantity_packs_1, pfpso1.size_1, pfpso1.quantity_packs_2, pfpso1.size_2, 
	olim.qty_stock, olim.qty_unit, olim.warehousestockitembatchid_bk, wsib.packSize pack_size
into #oli_service
from 
		Landing.aux.alloc_order_line_erp_issue_oli oli
	left join
		Landing.aux.alloc_order_line_erp_service ols on oli.orderlineid_bk = ols.orderlineid_bk 
	left join
		Landing.aux.alloc_fact_order_line_erp ol on oli.orderlineid_bk = ol.orderlineid_bk 
	left join
		Landing.aux.prod_product_family_pack_size_optimum pfpso1 on ols.product_id_pref_bk = pfpso1.product_id_bk and ols.quantity_lens_pref = pfpso1.quantity_lens and ols.optimum_type_pref =  pfpso1.optimum_type 
	inner join
		Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on olim.warehousestockitembatchid_bk = wsib.warehousestockitembatchid


		declare @orderlineid_bk bigint, @batchstockissueid_bk bigint
		declare @bom_f char(1)
		declare @product_id_bk int, @quantity_lens int, @quantity_lens_size_1 int, @quantity_lens_size_2 int, @quantity_packs_1 int, @size_1 int, @quantity_packs_2 int, @size_2 int
		declare @qty_stock int, @pack_size int

		declare cb_ol cursor for 
			select distinct orderlineid_bk, bom_f, product_id_pref_bk product_id_bk, quantity_lens_pref quantity_lens, 
				quantity_packs_1 * size_1 quantity_lens_size_1, quantity_packs_2 * size_2 quantity_lens_size_2, 
				quantity_packs_1, size_1, quantity_packs_2, size_2
			from #oli_service
			where product_id_pref_bk = 1100
			order by quantity_lens_pref, orderlineid_bk

		open cb_ol
		fetch next from cb_ol into @orderlineid_bk, @bom_f, @product_id_bk, @quantity_lens, @quantity_lens_size_1, @quantity_lens_size_2,
			@quantity_packs_1, @size_1, @quantity_packs_2, @size_2

		while @@FETCH_STATUS = 0
		begin
			-- Try to allocate size_1 packs
			declare cb_oli_1 cursor for 
				select batchstockissueid_bk, qty_stock, pack_size
				from #oli_service
				where orderlineid_bk = @orderlineid_bk -- and quantity_packs_1 = @quantity_packs_1 and (@quantity_packs_1 <> 0 and @quantity_packs_1 is not null)
					and size_1 = @size_1 and (@quantity_packs_1 <> 0 and @quantity_packs_1 is not null)
				order by pack_size desc

			open cb_oli_1
			fetch next from cb_oli_1 into @batchstockissueid_bk, @qty_stock, @pack_size

			while @@FETCH_STATUS = 0
			begin
				if @size_1 = @pack_size and @quantity_lens_size_1 / convert(decimal, @size_1) >=1 
					begin
						insert into #alloc_order_line_erp_issue_service(batchstockissueid_bk, product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f) values
							(@batchstockissueid_bk, @product_id_bk, @quantity_lens, 'pref', @size_1, 'Y')
						
						set @quantity_lens_size_1 =  @quantity_lens_size_1 - (@size_1 * @qty_stock)
					end 
				fetch next from cb_oli_1 into @batchstockissueid_bk, @qty_stock, @pack_size
			end

			close cb_oli_1
			deallocate cb_oli_1

			-- Try to allocate size_2 packs
			declare cb_oli_2 cursor for 
				select oli1.batchstockissueid_bk, qty_stock, pack_size
				from 
						#oli_service oli1
					left join
						#alloc_order_line_erp_issue_service oli2 on oli1.batchstockissueid_bk = oli2.batchstockissueid_bk
				where oli1.orderlineid_bk = @orderlineid_bk and oli2.batchstockissueid_bk is null
					and size_2 = @size_2 and (@quantity_packs_2 <> 0 and @quantity_packs_2 is not null) 
				order by pack_size desc

			open cb_oli_2
			fetch next from cb_oli_2 into @batchstockissueid_bk, @qty_stock, @pack_size

			while @@FETCH_STATUS = 0
			begin
				---- print ('ID:' + convert(varchar, @batchstockissueid_bk) + ' // Question: ' + convert(varchar, @size_1) + ' equal to ' + convert(varchar, @pack_size))
				--if @size_2 = @pack_size and @quantity_lens_mod / convert(decimal, @size_1) >=1 
				--	begin
				--		insert into #alloc_order_line_erp_issue_service(batchstockissueid_bk, product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f) values
				--			(@batchstockissueid_bk, @product_id_bk, @quantity_lens, 'pref', @size_2, 'N')
				--		set @quantity_lens_mod =  @quantity_lens_mod - @size_2
				--	end 
				--if @size_2 = @pack_size and @quantity_lens_mod / convert(decimal, @size_1) < 1 
				--	begin
				--		insert into #alloc_order_line_erp_issue_service(batchstockissueid_bk, product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f) values
				--			(@batchstockissueid_bk, @product_id_bk, @quantity_lens, 'pref', @size_2, 'Y')
				--		set @quantity_lens_mod =  @quantity_lens_mod - @size_2
				--	end 

				if @size_2 = @pack_size and @quantity_lens_size_2 / convert(decimal, @size_2) >=1 
					begin
						insert into #alloc_order_line_erp_issue_service(batchstockissueid_bk, product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f) values
							(@batchstockissueid_bk, @product_id_bk, @quantity_lens, 'pref', @size_1, 'Y')
						
						set @quantity_lens_size_2 =  @quantity_lens_size_2 - (@size_2 * @qty_stock)
					end 

				fetch next from cb_oli_2 into @batchstockissueid_bk, @qty_stock, @pack_size
			end

			close cb_oli_2
			deallocate cb_oli_2

			fetch next from cb_ol into @orderlineid_bk, @bom_f, @product_id_bk, @quantity_lens, @quantity_lens_size_1, @quantity_lens_size_2,
				@quantity_packs_1, @size_1, @quantity_packs_2, @size_2
		end 

		close cb_ol
		deallocate cb_ol

		insert into #alloc_order_line_erp_issue_service(batchstockissueid_bk, product_id_pref_bk, quantity_lens_pref, optimum_type_pref, pack_size_optimum_type_pref, optimum_pref_f) 
			select oli1.batchstockissueid_bk, oli1.product_id_pref_bk, oli1.quantity_lens_pref, 'pref' optimum_type_pref, size_1 pack_size_optimum_type_pref, 'N' optimum_pref_f
			from 
					#oli_service oli1
				left join
					#alloc_order_line_erp_issue_service oli2 on oli1.batchstockissueid_bk = oli2.batchstockissueid_bk
			where oli2.batchstockissueid_bk is null
				-- and oli1.product_id_pref_bk = 1100

