
select count(*)
from Landing.mend.whcust_customerinvoice

select count(*)
from Landing.mend.whcust_customerinvoice_aud

------------------------------

select count(*)
from Landing.mend.whcust_customershipmentinvoice

select count(*)
from Landing.mend.whcust_customershipmentinvoice_aud

select count(*)
from Landing.mend.gen_whcust_customershipmentinvoice_v

select count(*)
from Warehouse.alloc.dim_wholesale_order_shipment_v

	select *
	from Landing.mend.whcust_customershipmentinvoice_aud
	where id not in (select id from Landing.mend.whcust_customershipmentinvoice)

	select top 1000 * 
	from Warehouse.alloc.dim_wholesale_order_shipment_v
	where customershipmentinvoiceid_bk in 
		(select id
		from Landing.mend.whcust_customershipmentinvoice_aud
		where id not in (select id from Landing.mend.whcust_customershipmentinvoice)) 

	select top 1000 *
	from Warehouse.alloc.dim_wholesale_order_invoice_line_v
	where customershipmentinvoiceid_bk in 
		(select id
		from Landing.mend.whcust_customershipmentinvoice_aud
		where id not in (select id from Landing.mend.whcust_customershipmentinvoice)) 

------------------------------

select count(*)
from Landing.mend.whcust_customershipmentinvoice_customerinvoice

	select *, count(*) over (partition by customershipmentinvoiceid) num_rep
	from Landing.mend.whcust_customershipmentinvoice_customerinvoice
	order by num_rep desc

select count(*)
from Landing.mend.whcust_customershipmentinvoice_customerinvoice_aud

	select *, count(*) over (partition by customershipmentinvoiceid) num_rep
	from Landing.mend.whcust_customershipmentinvoice_customerinvoice_aud
	order by num_rep desc


------------------------------

select count(*)
from Landing.mend.whcust_customerinvoiceline

select count(*)
from Landing.mend.whcust_customerinvoiceline_aud

	select *
	from Landing.mend.whcust_customerinvoiceline_aud
	where id not in (select id from Landing.mend.whcust_customerinvoiceline)

	select top 1000 *
	from Warehouse.alloc.dim_wholesale_order_invoice_line_v
	where customerinvoicelineid_bk in 
		(select id
		from Landing.mend.whcust_customerinvoiceline_aud
		where id not in (select id from Landing.mend.whcust_customerinvoiceline))

------------------------------

select count(*)
from Landing.mend.whcust_customerinvoiceline_customershipmentinvoice

	select *, count(*) over (partition by customerinvoicelineid) num_rep
	from Landing.mend.whcust_customerinvoiceline_customershipmentinvoice
	order by num_rep desc

select count(*)
from Landing.mend.whcust_customerinvoiceline_customershipmentinvoice_aud

	select *, count(*) over (partition by customerinvoicelineid) num_rep
	from Landing.mend.whcust_customerinvoiceline_customershipmentinvoice_aud
	order by num_rep desc

---------------------------------------------------------------

	select *
	from Landing.mend.gen_whcust_customershipmentinvoice_v
	where delete_f = 'Y'

	select count(*)
	from Landing.mend.gen_whcust_customershipmentinvoice_v

	select customershipmentinvoiceid, count(*)
	from Landing.mend.gen_whcust_customershipmentinvoice_v
	group by customershipmentinvoiceid
	order by count(*) desc

	select *
	from Landing.mend.gen_whcust_customerinvoiceline_v
	where delete_f = 'Y'

	select customerinvoicelineid, count(*)
	from Landing.mend.gen_whcust_customerinvoiceline_v
	group by customerinvoicelineid
	order by count(*) desc
