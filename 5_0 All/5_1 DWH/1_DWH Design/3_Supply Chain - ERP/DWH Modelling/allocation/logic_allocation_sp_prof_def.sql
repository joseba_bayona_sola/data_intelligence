
-- Landing.aux.alloc_dim_order_header_erp_aud
select top 1000 orderid_bk, 
	order_no_erp, order_id_bk, 

	idCalendarOrderDateSync, order_date_sync, 
	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	idCalendarAllocationDate, allocation_date, 
	idCalendarIssueDate, issue_date, 

	order_type_erp_f,
	order_type_erp_bk, order_status_erp_bk, shipment_status_erp_bk, allocation_status_bk, allocation_strategy_bk, 
	country_id_shipping_bk, warehouseid_pref_bk, 

	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_to_global_rate, order_currency_code, 

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.alloc_dim_order_header_erp_aud
-- where order_id_bk in (5327076, 6362328, 6472630)
where orderid_bk = 48695171009825367
order by order_date_sync

	select count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud

	select order_type_erp_f, order_type_erp_bk, count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud
	group by order_type_erp_f, order_type_erp_bk
	order by order_type_erp_f, order_type_erp_bk

	select shipment_status_erp_bk, count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud
	group by shipment_status_erp_bk
	order by shipment_status_erp_bk

	select country_id_shipping_bk, count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud
	group by country_id_shipping_bk
	order by country_id_shipping_bk

	select country_id_shipping_bk, warehouseid_pref_bk, count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud
	group by country_id_shipping_bk, warehouseid_pref_bk
	order by country_id_shipping_bk, warehouseid_pref_bk

	select warehouseid_pref_bk, count(*)
	from Landing.aux.alloc_dim_order_header_erp_aud
	group by warehouseid_pref_bk
	order by warehouseid_pref_bk

-- Landing.aux.alloc_fact_order_line_erp_aud
select top 1000 orderlineid_bk, order_line_id_bk, 
	orderid_bk, productid_erp_bk, 
	
	bom_f, 

	qty_unit, qty_unit_allocated, qty_unit_issued,	
	local_price_unit, local_subtotal, 
	local_to_global_rate, order_currency_code, 

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.alloc_fact_order_line_erp_aud

	select count(*)
	from Landing.aux.alloc_fact_order_line_erp_aud

	select top 1000 count(*) over (),
		oh.orderid_bk, oh.order_no_erp, oh.order_id_bk, oh.order_date_sync,
		ol.orderlineid_bk, ol.order_line_id_bk, 
		
		ol.productid_erp_bk, ol.bom_f, p.magentoProductID, p.name, p.SKU_product, p.description,

		ol.qty_unit, ol.qty_unit_allocated, ol.qty_unit_issued,		
		ol.local_price_unit, ol.local_subtotal, 
		ol.local_to_global_rate, ol.order_currency_code
	from 
			Landing.aux.alloc_dim_order_header_erp_aud oh
		inner join			
			Landing.aux.alloc_fact_order_line_erp_aud ol on oh.orderid_bk = ol.orderid_bk
		inner join
			Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
	-- where oh.order_id_bk in (5327076, 6362328, 6472630)
	-- where oh.orderid_bk = 48695171009825367
	-- where p.magentoProductID in ('2328', '1278') and ol.bom_f = 'Y' -- N - Y
	-- where p.magentoProductID in ('2303') and ol.bom_f = 'Y' -- N - Y	
	order by oh.order_date_sync

		select p.magentoProductID, p.name, count(*), count(distinct bom_f)
		from
				Landing.aux.alloc_fact_order_line_erp_aud ol 
			inner join
				Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
		group by p.magentoProductID, p.name
		order by count(distinct bom_f) desc, p.magentoProductID, p.name

		select ol.bom_f, p.magentoProductID, p.name, count(*), count(*) over (partition by p.magentoProductID) num_rep
		from
				Landing.aux.alloc_fact_order_line_erp_aud ol 
			inner join
				Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
		group by ol.bom_f, p.magentoProductID, p.name
		order by num_rep desc, p.magentoProductID, ol.bom_f, p.name

		select 
			oh.orderid_bk, oh.order_no_erp, ol.productid_erp_bk, p.sku_product, count(*)
		from 
				Landing.aux.alloc_dim_order_header_erp_aud oh
			inner join			
				Landing.aux.alloc_fact_order_line_erp_aud ol on oh.orderid_bk = ol.orderid_bk
			inner join
				Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
		group by oh.orderid_bk, oh.order_no_erp, ol.productid_erp_bk, p.sku_product
		having count(*) > 1
		order by order_no_erp, sku_product	

-- Landing.aux.alloc_fact_order_line_erp_issue_aud
select top 1000 batchstockissueid_bk, orderlinestockallocationtransactionid_bk, 
	issue_id, 
	orderlineid_bk, 

	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	idCalendarAllocationDate, allocation_date, 
	idCalendarIssueDate, issue_date, 

	allocation_type_bk, allocation_record_type_bk, cancelled_f, num_rep,
	warehousestockitembatchid_bk, receiptlineid_bk,

	qty_unit_order_line, qty_unit, qty_stock, 
	qty_percentage, 
	local_total_unit_cost, local_total_cost, 
	local_to_global_rate, order_currency_code, 

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.alloc_fact_order_line_erp_issue_aud

	select count(*)
	from Landing.aux.alloc_fact_order_line_erp_issue_aud

	select num_rep, count(*)
	from Landing.aux.alloc_fact_order_line_erp_issue_aud
	group by num_rep
	order by num_rep

	select cancelled_f, count(*)
	from Landing.aux.alloc_fact_order_line_erp_issue_aud
	group by cancelled_f
	order by cancelled_f

	select top 1000 count(*) over (),
		oh.orderid_bk, oh.order_no_erp, oh.order_id_bk, oh.order_date_sync,
		ol.orderlineid_bk, oli.batchstockissueid_bk, oli.issue_id, oli.warehousestockitembatchid_bk,
		ol.productid_erp_bk, ol.bom_f, p.magentoProductID, p.name, p.SKU_product, p.description, wsib.packSize,
		wsib.warehouse_name, wsib.batch_id, 
		oli.cancelled_f, oli.num_rep,
		ol.qty_unit, ol.qty_unit_allocated, ol.qty_unit_issued,	

		oli.qty_unit_order_line, oli.qty_unit, oli.qty_stock, 
		oli.qty_percentage, 
		oli.local_total_unit_cost, oli.local_total_cost, 
		oli.local_to_global_rate

	from 
			Landing.aux.alloc_dim_order_header_erp_aud oh
		inner join			
			Landing.aux.alloc_fact_order_line_erp_aud ol on oh.orderid_bk = ol.orderid_bk
		inner join
			Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
		inner join
			Landing.aux.alloc_fact_order_line_erp_issue_aud oli on ol.orderlineid_bk = oli.orderlineid_bk
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
	-- where oh.order_id_bk in (5327076, 6362328, 6472630)
	-- where wsib.batch_id = 1809247
	where oh.orderid_bk = 48695171009825367
	order by oh.order_date_sync, p.SKU_product

			select oh.order_type_erp_f, wsib.warehouse_name, oh.country_id_shipping_bk, count(*)
			from 
					Landing.aux.alloc_dim_order_header_erp_aud oh
				inner join			
					Landing.aux.alloc_fact_order_line_erp_aud ol on oh.orderid_bk = ol.orderid_bk
				inner join
					Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
				inner join
					Landing.aux.alloc_fact_order_line_erp_issue_aud oli on ol.orderlineid_bk = oli.orderlineid_bk
				inner join
					Landing.mend.gen_wh_warehousestockitembatch_v wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
			group by oh.order_type_erp_f, wsib.warehouse_name, oh.country_id_shipping_bk
			order by oh.order_type_erp_f, wsib.warehouse_name, oh.country_id_shipping_bk

			select p.magentoProductID, p.name, wsib.warehouse_name, wsib.packSize, count(*), sum(qty_stock), avg(local_total_unit_cost)
			from 
					Landing.aux.alloc_dim_order_header_erp_aud oh
				inner join			
					Landing.aux.alloc_fact_order_line_erp_aud ol on oh.orderid_bk = ol.orderid_bk
				inner join
					Landing.mend.gen_prod_product_v p on ol.productid_erp_bk = p.productid
				inner join
					Landing.aux.alloc_fact_order_line_erp_issue_aud oli on ol.orderlineid_bk = oli.orderlineid_bk
				inner join
					Landing.mend.gen_wh_warehousestockitembatch_v wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
			group by p.magentoProductID, p.name, wsib.warehouse_name, wsib.packSize
			order by p.magentoProductID, p.name, wsib.warehouse_name, wsib.packSize

			-- per WSIB
			select wsib.warehousestockitembatchid, 
				wsib.magentoProductID, wsib.name, wsib.packSize, wsib.warehouse_name, wsib.batch_id, 
				wsib.fullyissued, 
				wsib.receivedquantity, wsib.issuedquantity,
				oli.qty_issue, 
				wsib.productUnitCost
			from
					(select oli.warehousestockitembatchid_bk, sum(qty_stock) qty_issue
					from Landing.aux.alloc_fact_order_line_erp_issue_aud oli 
					where cancelled_f = 'N'
					group by oli.warehousestockitembatchid_bk) oli
				right join
					Landing.mend.gen_wh_warehousestockitembatch_v wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
			-- where oli.warehousestockitembatchid_bk is null and wsib.issuedquantity <> 0 --0
			-- where wsib.issuedquantity < oli.qty_issue -- 0
			-- where wsib.issuedquantity = oli.qty_issue
			where wsib.issuedquantity > oli.qty_issue -- 0
			-- where wsib.magentoProductID = 'W0008'
			order by wsib.magentoProductID, wsib.name, wsib.packSize, wsib.warehouse_name, wsib.batch_id


-- Landing.aux.alloc_fact_order_line_mag_erp_aud
select top 1000 order_line_id_bk, orderlineid_bk, orderlinemagentoid_bk, 

	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	deduplicate_f, 

	qty_unit_mag, qty_unit_erp, qty_percentage, 
	local_total_cost_mag, local_total_cost_erp, 
	local_to_global_rate, order_currency_code,

	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.alloc_fact_order_line_mag_erp_aud

	select count(*)
	from Landing.aux.alloc_fact_order_line_mag_erp_aud


	select top 1000 -- count(*) over (),
		olm.order_line_id_bk, -- olm.orderlineid_bk, olm.orderlinemagentoid_bk, 
		oh.order_id_bk, oh.order_no, oh.order_date,

		olm.snap_complete_date, olm.expected_shipment_date, -- olm.cage_shipment_date, 
		ol.shipment_date, 

		olm.deduplicate_f, ol.product_id_bk, -- pf.product_family_name,

		ol.qty_unit, ol.qty_pack, olm.qty_unit_mag, olm.qty_unit_erp, olm.qty_percentage, 
		ol.local_subtotal, ol.local_total_inc_vat, ol.local_total_exc_vat,
		olm.local_total_cost_mag, olm.local_total_cost_erp, olm.local_total_cost_mag / ol.qty_pack local_total_cost_mag_stock,
		ol.order_currency_code
	from 
			Landing.aux.alloc_fact_order_line_mag_erp_aud olm
		inner join
			Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
		inner join
			Landing.aux.sales_dim_order_header_aud oh on ol.order_id_bk = oh.order_id_bk
		-- inner join
			-- Landing.aux.mag_prod_product_family_v pf on ol.product_id_bk = pf.product_id_bk	
	where oh.order_id_bk in (6362328, 6472630)
	-- where ol.product_id_bk = 1083

		select pf.product_family_group_bk, ol.product_id_bk, pf.product_family_name, ol.order_currency_code, count(*) num_lines, 
			sum(ol.local_total_exc_vat) sum_sales, sum(ol.local_total_exc_vat - olm.local_total_cost_mag) sum_margin,
			sum(ol.local_total_exc_vat - olm.local_total_cost_mag) / sum(ol.local_total_exc_vat) margin_perc,
			avg(ol.local_total_exc_vat / ol.qty_pack) mean_sale_price, 
			avg(olm.local_total_cost_mag / ol.qty_pack) mean_cost, avg(olm.local_total_cost_mag / ol.qty_unit) mean_cost_unit, 
			avg(ol.local_total_exc_vat - olm.local_total_cost_mag) mean_margin
		from 
				Landing.aux.alloc_fact_order_line_mag_erp_aud olm
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
			inner join
				Landing.aux.mag_prod_product_family_v pf on ol.product_id_bk = pf.product_id_bk	
		where ol.order_currency_code = 'GBP' 
			-- and pf.product_id_bk in (1083, 2317)
			-- and pf.product_family_name like 'Everclear%'
		group by pf.product_family_group_bk, ol.product_id_bk, pf.product_family_name, ol.order_currency_code
		-- order by ol.product_id_bk, pf.product_family_name, ol.order_currency_code
		order by pf.product_family_group_bk, avg(ol.local_total_exc_vat - olm.local_total_cost_mag) desc

		select product_family_group_bk, product_id_bk, product_family_name, order_currency_code, num_lines, 
			sum_sales, sum_margin,
			sum_margin / sum_sales margin_perc,
			mean_sale_price, 
			mean_cost, mean_cost_unit, 
			mean_margin
		from DW_GetLenses_jbs.dbo.erp_allocation_margin_uk
		where sum_margin is null
		-- where sum_margin < 0
		order by product_family_group_bk, product_id_bk
