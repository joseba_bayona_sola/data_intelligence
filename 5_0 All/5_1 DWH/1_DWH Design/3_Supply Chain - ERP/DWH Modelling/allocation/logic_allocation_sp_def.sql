

exec mend.lnd_stg_get_aux_alloc_order_dim_fact @idETLBatchRun = 1, @idPackageRun = 1


	-- t_SPRunMessage_v
	select top 1000 spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where sp.package_name = 'lnd_stg_alloc_tables'
	order by spm.finishTime desc

	
-------------------------------------------------------------------------

select *
from Landing.aux.alloc_dim_order_header_erp

insert into Landing.aux.alloc_dim_order_header_erp (orderid_bk, 
	order_no_erp, order_id_bk, 

	idCalendarOrderDateSync, order_date_sync, 
	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	idCalendarAllocationDate, allocation_date, 
	idCalendarIssueDate, issue_date, 

	order_type_erp_f,
	order_type_erp_bk, order_status_erp_bk, shipment_status_erp_bk, allocation_status_bk, allocation_strategy_bk, 
	country_id_shipping_bk, warehouseid_pref_bk, 

	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_to_global_rate, order_currency_code, idETLBatchRun)

	select o.orderid_bk, 
		o.order_no_erp, o.order_id_bk, 

		oda.idCalendarOrderDateSync, oda.order_date_sync, 
		oda.idCalendarSNAPCompleteDate, oda.snap_complete_date, oda.idTimeSNAPCompleteDate,
		oda.idCalendarExpectedShipmentDate, oda.expected_shipment_date, 
		oda.idCalendarCageShipmentDate, oda.cage_shipment_date, oda.idTimeCageShipmentDate,

		oda.idCalendarAllocationDate, oda.allocation_date, 
		oda.idCalendarIssueDate, oda.issue_date, 

		od.order_type_erp_f,
		od.order_type_erp_bk, od.order_status_erp_bk, od.shipment_status_erp_bk, od.allocation_status_bk, od.allocation_strategy_bk, 
		od.country_id_shipping_bk, od.warehouseid_pref_bk, 

		om.local_subtotal, om.local_shipping, om.local_discount, om.local_store_credit_used, om.local_total_inc_vat, 
		om.local_to_global_rate, om.order_currency_code
	from 
			Landing.aux.alloc_order_header_erp_o o
		inner join
			Landing.aux.alloc_order_header_erp_dim_order od on o.orderid_bk = od.orderid_bk
		inner join
			Landing.aux.alloc_order_header_erp_dates oda on o.orderid_bk = oda.orderid_bk
		inner join
			Landing.aux.alloc_order_header_erp_measures om on o.orderid_bk = om.orderid_bk


insert into Landing.aux.alloc_fact_order_line_erp (orderlineid_bk, order_line_id_bk, 
	orderid_bk, productid_erp_bk, 
	
	bom_f, 

	qty_unit, qty_unit_allocated, 	
	local_price_unit, local_subtotal, 
	local_to_global_rate, order_currency_code, 

	idETLBatchRun)

	select ol.orderlineid_bk, ol.order_line_id_bk, 
		ol.orderid_bk, olp.productid_erp_bk, 
	
		olp.bom_f, 

		olm.qty_unit, olm.qty_unit_allocated, 	
		olm.local_price_unit, olm.local_subtotal, 
		olm.local_to_global_rate, olm.order_currency_code
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_line_erp_product olp on ol.orderlineid_bk = olp.orderlineid_bk
		inner join
			Landing.aux.alloc_order_line_erp_measures olm on ol.orderlineid_bk = olm.orderlineid_bk


insert into Landing.aux.alloc_fact_order_line_erp_issue (batchstockissueid_bk, orderlinestockallocationtransactionid_bk, 
	issue_id, 
	orderlineid_bk, 

	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	idCalendarAllocationDate, allocation_date, 
	idCalendarIssueDate, issue_date, 

	allocation_type_bk, allocation_record_type_bk, cancelled_f, 
	warehousestockitembatchid_bk, receiptlineid_bk,

	qty_unit_order_line, qty_unit, qty_stock, 
	qty_percentage, 
	local_total_unit_cost, local_total_cost, 
	local_to_global_rate, order_currency_code, 
	idETLBatchRun)

	select oli.batchstockissueid_bk, oli.orderlinestockallocationtransactionid_bk, 
		oli.issue_id, 
		oli.orderlineid_bk, 

		olid.idCalendarSNAPCompleteDate, olid.snap_complete_date, olid.idTimeSNAPCompleteDate,
		olid.idCalendarExpectedShipmentDate, olid.expected_shipment_date, 
		olid.idCalendarCageShipmentDate, olid.cage_shipment_date, olid.idTimeCageShipmentDate,

		olid.idCalendarAllocationDate, olid.allocation_date, 
		olid.idCalendarIssueDate, olid.issue_date, 

		oli.allocation_type_bk, oli.allocation_record_type_bk, oli.cancelled_f, 
		olim.warehousestockitembatchid_bk, olim.receiptlineid_bk,

		olim.qty_unit_order_line, olim.qty_unit, olim.qty_stock, 
		olim.qty_percentage, 
		olim.local_total_unit_cost, olim.local_total_cost, 
		olim.local_to_global_rate, olim.order_currency_code
	from 
			Landing.aux.alloc_order_line_erp_issue_oli oli
		inner join
			Landing.aux.alloc_order_line_erp_issue_dates olid on oli.batchstockissueid_bk = olid.batchstockissueid_bk
		inner join
			Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk


insert into Landing.aux.alloc_fact_order_line_mag_erp (order_line_id_bk, orderlineid_bk, orderlinemagentoid_bk, 

	idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
	idCalendarExpectedShipmentDate, expected_shipment_date, 
	idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

	deduplicate_f, 

	qty_unit_mag, qty_unit_erp, qty_percentage, 
	local_total_cost_mag, local_total_cost_erp, 
	local_to_global_rate, order_currency_code,

	idETLBatchRun)

	select olm.order_line_id_bk, olm.orderlineid_bk, olm.orderlinemagentoid_bk, 

		olmd.idCalendarSNAPCompleteDate, olmd.snap_complete_date, olmd.idTimeSNAPCompleteDate,
		olmd.idCalendarExpectedShipmentDate, olmd.expected_shipment_date, 
		olmd.idCalendarCageShipmentDate, olmd.cage_shipment_date, olmd.idTimeCageShipmentDate,

		olmm.deduplicate_f, 

		olmm.qty_unit_mag, olmm.qty_unit_erp, olmm.qty_percentage, 
		olmm.local_total_cost_mag, olmm.local_total_cost_erp, 
		olmm.local_to_global_rate, olmm.order_currency_code
	from 
			Landing.aux.alloc_order_line_mag_erp_olm olm
		inner join
			Landing.aux.alloc_order_line_mag_erp_dates olmd on olm.order_line_id_bk = olmd.order_line_id_bk
		inner join
			Landing.aux.alloc_order_line_mag_erp_measures olmm on olm.order_line_id_bk = olmm.order_line_id_bk

		
