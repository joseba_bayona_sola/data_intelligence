
select *
from Landing.mend.gen_order_wholesaleorder_v
where wholesaleorderheaderid is not null

---------------------------------------------------
---------------------------------------------------


select *
from Landing.mend.gen_whcust_customerinvoice_v
order by invoicedate desc, invoicenumber

	select status, count(*)
	from Landing.mend.gen_whcust_customerinvoice_v
	group by status
	order by status

----------------------------

select *
from Landing.mend.gen_whcust_customershipmentinvoice_v
where customerinvoiceid is null
order by invoicedate desc, invoicenumber, dispatchdate desc

select invoicenumber, invoicedate, status,
	count(*), count(distinct ordernumber), count(distinct shipmentnumber)
from Landing.mend.gen_whcust_customershipmentinvoice_v
where customerinvoiceid is not null
group by invoicenumber, invoicedate, status
-- having count(*) <> count(distinct shipmentnumber)
order by invoicedate desc, invoicenumber

----------------------------

select top 1000 *
from Landing.mend.gen_whcust_customerinvoiceline_v 
order by invoicedate desc, invoicenumber

select customershipmentinvoiceid, count(*), count(distinct customerinvoicelineid)
from Landing.mend.gen_whcust_customerinvoiceline_v 
group by customershipmentinvoiceid
order by customershipmentinvoiceid


----------------------------

select *
from Landing.mend.gen_whcust_customerinvoicecarriage_v
where netprice <> 0
order by invoicedate desc, invoicenumber

select invoicenumber, invoicedate, status,
	count(*), count(distinct customerinvoicelineid)
from Landing.mend.gen_whcust_customerinvoicecarriage_v
group by invoicenumber, invoicedate, status
-- having count(*) <> count(distinct customerinvoicelineid)
having count(*) <> 1
order by invoicedate desc, invoicenumber
