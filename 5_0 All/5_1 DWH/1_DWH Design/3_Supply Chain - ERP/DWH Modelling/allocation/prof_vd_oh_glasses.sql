
drop table DW_GetLenses_jbs.dbo.aux_vd_glasses_ol
go

select idOrderHeader_sk_fk, order_id order_id_bk, order_line_id_bk
into DW_GetLenses_jbs.dbo.aux_vd_glasses_ol
from Warehouse.sales.fact_order_line
where idGlassVisionType_sk_fk is not null
order by order_id

	select top 1000 *
	from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol

	select top 1000 count(*), count(distinct order_id_bk)
	from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol

	select oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date
	from
			(select distinct idOrderHeader_sk_fk
			from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol) ol
		inner join
			Warehouse.sales.dim_order_header oh on ol.idOrderHeader_sk_fk = oh.idOrderHeader_sk
	order by oh.order_id_bk

drop table DW_GetLenses_jbs.dbo.aux_vd_glasses_ol2
go

select oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date, oh.order_status_name, oh.order_status_magento_name,
	ol2.idOrderLIne_sk, ol2.order_line_id_bk, pf.product_id_magento, pf.category_name, pf.product_family_name, ol2.qty_unit
into DW_GetLenses_jbs.dbo.aux_vd_glasses_ol2
from
		(select distinct idOrderHeader_sk_fk
		from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol) ol
	inner join
		Warehouse.sales.dim_order_header_v oh on ol.idOrderHeader_sk_fk = oh.idOrderHeader_sk
	inner join
		Warehouse.sales.fact_order_line ol2 on ol.idOrderHeader_sk_fk = ol2.idOrderHeader_sk_fk
	inner join
		Warehouse.prod.dim_product_family_v pf on ol2.idProductFamily_sk_fk = pf.idProductFamily_sk
order by oh.order_id_bk, ol2.order_line_id_bk

------------------------------------------

drop table DW_GetLenses_jbs.dbo.aux_vd_glasses_ol3
go

select ol.order_id_bk, 
	ol3.item_id, ol3.product_id, ol3.name, ol3.qty_ordered
into DW_GetLenses_jbs.dbo.aux_vd_glasses_ol3
from
		(select distinct order_id_bk
		from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol) ol
	inner join
		Landing.mag.sales_flat_order_item_aud ol3 on ol.order_id_bk = ol3.order_id
order by ol.order_id_bk, ol3.item_id

	select ol2.idOrderHeader_sk, ol2.order_id_bk, ol2.order_line_id_bk, ol3.item_id, 
		ol2.order_no, ol2.order_date, ol2.order_status_name, ol2.order_status_magento_name,
		ol2.idOrderLIne_sk, ol2.product_id_magento, ol2.category_name, ol2.product_family_name, ol2.qty_unit, 
		ol3.product_id, ol3.name, ol3.qty_ordered
	from
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol2 ol2
		right join
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol3 ol3 on ol2.order_id_bk = ol3.order_id_bk and ol2.order_line_id_bk = ol3.item_id
	order by ol3.order_id_bk, ol3.item_id

------------------------------------------

drop table DW_GetLenses_jbs.dbo.aux_vd_glasses_ol4
go

select ol.order_id_bk, 
	ol4.magentoItemID, ol4.magentoProductID, ol4.name, ol4.quantityOrdered
into DW_GetLenses_jbs.dbo.aux_vd_glasses_ol4
from
		(select distinct order_id_bk
		from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol) ol
	inner join
		Landing.mend.gen_order_orderlinemagento_v ol4 on ol.order_id_bk = ol4.magentoOrderID_int
order by ol.order_id_bk, ol4.magentoItemID


	select ol2.idOrderHeader_sk, ol3.order_id_bk, ol2.order_line_id_bk, ol3.item_id, ol4.magentoItemID,
		ol2.order_no, ol2.order_date, ol2.order_status_name, ol2.order_status_magento_name,
		ol2.idOrderLIne_sk, ol2.product_id_magento, ol2.category_name, ol2.product_family_name, ol2.qty_unit, 
		ol3.product_id, ol3.name, ol3.qty_ordered, 
		ol4.magentoProductID, ol4.name, ol4.quantityOrdered
	from
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol2 ol2
		right join
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol3 ol3 on ol2.order_id_bk = ol3.order_id_bk and ol2.order_line_id_bk = ol3.item_id
		left join
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol4 ol4 on ol3.order_id_bk = ol4.order_id_bk and ol3.item_id = ol4.magentoItemID
	order by ol3.order_id_bk, ol3.item_id



drop table DW_GetLenses_jbs.dbo.aux_vd_glasses_ol5
go

select ol.order_id_bk, 
	ol5.magentoItemID, ol5.magentoProductID, ol5.name, ol5.quantityOrdered
into DW_GetLenses_jbs.dbo.aux_vd_glasses_ol5
from
		(select distinct order_id_bk
		from DW_GetLenses_jbs.dbo.aux_vd_glasses_ol) ol
	inner join
		Landing.mend.gen_order_orderline_v ol5 on ol.order_id_bk = ol5.magentoOrderID_int
order by ol.order_id_bk, ol5.magentoItemID


	select ol2.idOrderHeader_sk, ol3.order_id_bk, ol2.order_line_id_bk, ol3.item_id, ol5.magentoItemID,
		ol2.order_no, ol2.order_date, ol2.order_status_name, ol2.order_status_magento_name,
		ol2.idOrderLIne_sk, ol2.product_id_magento, ol2.category_name, ol2.product_family_name, ol2.qty_unit, 
		ol3.product_id, ol3.name, ol3.qty_ordered, 
		ol5.magentoProductID, ol5.name, ol5.quantityOrdered, 
		count(*) over (partition by ol3.item_id) num_rep
	from
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol2 ol2
		right join
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol3 ol3 on ol2.order_id_bk = ol3.order_id_bk and ol2.order_line_id_bk = ol3.item_id
		left join
			DW_GetLenses_jbs.dbo.aux_vd_glasses_ol5 ol5 on ol3.order_id_bk = ol5.order_id_bk and ol3.item_id = ol5.magentoItemID
	-- order by ol3.order_id_bk, ol3.item_id
	order by num_rep desc, ol3.order_id_bk, ol3.item_id


----------------------------------------------------------

select top 1000 count(*) over (partition by magentoItemID) num_rep, *
from Landing.mend.gen_order_orderlinemagento_v
where magentoItemID <> 0
order by num_rep desc

select top 1000 count(*) over (partition by magentoItemID) num_rep, 
	orderid, orderlineid, productid, magentoOrderID, magentoItemID, incrementID, magentoProductID, name, quantityOrdered
from Landing.mend.gen_order_orderline_v
where magentoItemID <> 0
order by num_rep desc, magentoOrderID


select *
from Landing.aux.sales_fact_order_line_aud
-- where order_line_id_bk = 6485561
where order_id_bk = 3267657

select *
from Landing.mend.gen_order_orderlinemagento_v
-- where magentoItemID = 6485561
where magentoOrderID_int = 3267657
order by magentoOrderID_int
