
		select olsh.orderlineshortageid orderlineshortageid_bk, 
			olsh.warehouse_dest warehouseid_bk, olsh.supplier_id supplier_id_bk, ol.orderlineid_bk, 
			olsh.purchaseorderlineid purchaseorderlineid_bk, 
	
			1 idCalendarShortageCreateDate, olsh.createdDate shortage_create_date, 1 idCalendarShortageRequiredDate, olsh.requireddate shortage_required_date, 
	
			olsh.shortageid shortage_id, olsh.status shortage_status, olsh.orderlineshortagetype shortage_type, olsh.shortageprogress shortage_progress, 
			case when (olsh.wholesale = 1) then 'Y' else 'N' end wholesale_f, olsh.delete_f

		from 
				Landing.mend.gen_short_orderlineshortage_v olsh
			inner join
				Landing.aux.alloc_order_line_erp_ol ol on olsh.orderlineid = ol.orderlineid_bk
		where olsh.ord_rep = 1


------------------------------------

drop table #alloc_fact_order_line_erp_shortage

create table #alloc_fact_order_line_erp_shortage(
	orderlineshortageid_bk					bigint NOT NULL,

	warehouseid_bk							bigint, 
	supplier_id_bk							bigint,
	orderlineid_bk							bigint NOT NULL, 

	purchaseorderlineid_bk					bigint,

	idCalendarShortageCreateDate			int, 
	shortage_create_date					datetime,
	idCalendarShortageRequiredDate			int, 
	shortage_required_date					datetime,

	shortage_id								bigint,
	shortage_status							varchar(20), 
	shortage_type							varchar(20),
	shortage_progress						varchar(250),

	wholesale_f								char(1),
	delete_f								char(1));
go 

alter table #alloc_fact_order_line_erp_shortage add constraint [PK_#_alloc_fact_order_line_erp_shortage]
	primary key clustered (orderlineshortageid_bk);
go

	insert into #alloc_fact_order_line_erp_shortage (orderlineshortageid_bk, 
		warehouseid_bk, supplier_id_bk, orderlineid_bk, 
		purchaseorderlineid_bk, 
	
		idCalendarShortageCreateDate, shortage_create_date, idCalendarShortageRequiredDate, shortage_required_date, 
	
		shortage_id, shortage_status, shortage_type, shortage_progress, 
		wholesale_f, delete_f)

		select olsh.orderlineshortageid orderlineshortageid_bk, 
			olsh.warehouse_dest warehouseid_bk, olsh.supplier_id supplier_id_bk, olsh.orderlineid orderlineid_bk, 
			olsh.purchaseorderlineid purchaseorderlineid_bk, 
	
			CONVERT(INT, (CONVERT(VARCHAR(8), olsh.createdDate, 112))) idCalendarShortageCreateDate, olsh.createdDate shortage_create_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), olsh.requireddate, 112))) idCalendarShortageRequiredDate, olsh.requireddate shortage_required_date, 
	
			olsh.shortageid shortage_id, olsh.status shortage_status, olsh.orderlineshortagetype shortage_type, olsh.shortageprogress shortage_progress, 
			case when (olsh.wholesale = 1) then 'Y' else 'N' end wholesale_f, olsh.delete_f

		from 
				Landing.mend.gen_short_orderlineshortage_v olsh
			inner join
				Landing.aux.alloc_order_line_erp_ol ol on olsh.orderlineid = ol.orderlineid_bk
		where olsh.ord_rep = 1
	go

	select top 1000 count(*) over (partition by orderlineid_bk) num_rep, *
	from #alloc_fact_order_line_erp_shortage
	order by num_rep desc, orderlineid_bk
