
drop table DW_GetLenses_jbs.dbo.erp_a_oh

drop table DW_GetLenses_jbs.dbo.erp_a_ol
drop table DW_GetLenses_jbs.dbo.erp_a_olm

drop table DW_GetLenses_jbs.dbo.erp_a_olm_all

drop table DW_GetLenses_jbs.dbo.erp_a_ol_all
drop table DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
drop table DW_GetLenses_jbs.dbo.erp_a_ol_all_bi

----------------------------------------------------------------------

select orderid, 
	magentoOrderID_int, magentoOrderID, incrementID, 
	createdDate, 
	_type, orderStatus, shipmentStatus, allocationStatus, allocationStrategy, 
	subtotal, shippingAmount, discountAmount, custBalanceAmount, adjustmentNegative, adjustmentPositive, grandTotal, 
	toOrderRate, toGlobalRate, currencyCode
into DW_GetLenses_jbs.dbo.erp_a_oh
from Landing.mend.gen_order_order_v
where createdDate between '2018-07-01' and '2018-08-01'

----------------------------------------------------------------------

select oh.orderid, ol.orderlineid, ol.magentoItemID,
	ol.orderLinesMagento, ol.orderLinesDeduped,
	ol.quantityOrdered, ol.quantityInvoiced, ol.quantityAllocated, ol.basePrice, ol.baseRowPrice, 
	ol.productid, ol.magentoProductID, ol.name, ol.sku
into DW_GetLenses_jbs.dbo.erp_a_ol
from 
		DW_GetLenses_jbs.dbo.erp_a_oh oh
	left join
		Landing.mend.gen_order_orderline_v ol on oh.orderid = ol.orderid

select oh.orderid, olm.orderlinemagentoid, olm.magentoItemID, 
	olm.orderLinesMagento, olm.orderLinesDeduped, olm.lineAdded, olm.deduplicate,
	olm.quantityOrdered, olm.quantityInvoiced, olm.quantityAllocated, olm.basePrice, olm.baseRowPrice, 
	olm.productid, olm.magentoProductID, olm.name, olm.sku 	
into DW_GetLenses_jbs.dbo.erp_a_olm
from 
		DW_GetLenses_jbs.dbo.erp_a_oh oh
	left join
		Landing.mend.gen_order_orderlinemagento_v olm on oh.orderid = olm.orderid

----------------------------------------------------------------------

select olm.orderid, olm.orderlinemagentoid,
	ma.magentoallocationid, ma.orderlinestockallocationtransactionid,
	ma.wh_name,
	ma.shipped, ma.cancelled_magentoall, 
	ma.quantity
into DW_GetLenses_jbs.dbo.erp_a_olm_all
from 
		DW_GetLenses_jbs.dbo.erp_a_olm olm
	left join
		Landing.mend.gen_all_magentoallocation_v ma on olm.orderlinemagentoid = ma.orderlinemagentoid
where olm.orderid is not null and olm.orderlinemagentoid is not null

----------------------------------------------------------------------

select ol.orderid, ol.orderlineid, 
	sat.orderlinestockallocationtransactionid, sat.warehousestockitemid, -- sat.batchstockallocationid, sat.warehousestockitembatchid, 
	sat.wh_name, sat.allocationType, sat.recordType, sat.timestamp, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packsize
into DW_GetLenses_jbs.dbo.erp_a_ol_all
from 
		DW_GetLenses_jbs.dbo.erp_a_ol ol
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
where ol.orderid is not null and ol.orderlineid is not null

select ola.orderid, ola.orderlineid, ola.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, 
	wsiba.productfamilyid, wsiba.productid, wsiba.stockitemid, wsiba.warehousestockitemid, wsiba.warehousestockitembatchid, 
	wsiba.batch_id, wsiba.magentoProductID, wsiba.stockitemdescription,
	wsiba.fullyissued_alloc, wsiba.cancelled, wsiba.allocatedquantity_alloc, wsiba.allocatedunits, wsiba.issuedquantity_alloc, 
	wsiba.allocation_date
into DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
from 
		DW_GetLenses_jbs.dbo.erp_a_ol_all ola
	left join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on ola.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
where ola.orderlinestockallocationtransactionid is not null

select olab.orderid, olab.orderlineid, olab.orderlinestockallocationtransactionid, olab.batchstockallocationid, wsibi.batchstockissueid, 
	wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, 
	wsibi.batch_id, wsibi.magentoProductID, wsibi.stockitemdescription, 
	wsibi.issueid, wsibi.issuedquantity_issue, wsibi.issue_date 
into DW_GetLenses_jbs.dbo.erp_a_ol_all_bi
from 
		DW_GetLenses_jbs.dbo.erp_a_ol_all_ba olab
	left join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on olab.batchstockallocationid = wsibi.batchstockallocationid
where olab.batchstockallocationid is not null
