
select *
from DW_GetLenses_jbs.dbo.aux_order_many_shipments
order by order_id_bk desc

select oh.order_id_bk, oh.order_no, oh.order_date, oh.website, oh.product_type_oh_name
from 
		DW_GetLenses_jbs.dbo.aux_order_many_shipments ms
	inner join
		Warehouse.sales.dim_order_header_v oh on ms.order_id_bk = oh.order_id_bk
order by oh.order_id_bk desc

select oh.order_id_bk, oh.order_no, oh.order_date, oh.website, oh.product_type_oh_name, 
	ohpt.daily_perc, ohpt.two_weeklies_perc, ohpt.monthlies_perc, ohpt.yearly_perc, ohpt.colour_perc, 
	ohpt.sunglasses_perc, ohpt.glasses_perc, ohpt.other_perc
from 
		DW_GetLenses_jbs.dbo.aux_order_many_shipments ms
	inner join
		Warehouse.sales.dim_order_header_v oh on ms.order_id_bk = oh.order_id_bk
	inner join
		Warehouse.sales.dim_order_header_product_type_v ohpt on oh.order_id_bk = ohpt.order_id_bk
where ohpt.sunglasses_perc is null and ohpt.glasses_perc is null
order by oh.order_id_bk desc

select oh.order_id_bk, ol.order_line_id_bk, oh.order_no, oh.order_date, oh.website,  
	ol.shipment_no, ol.shipment_date, ol.product_id_magento, ol.product_family_name, 
	dense_rank() over (order by oh.order_id_bk desc) num_oh
from 
		DW_GetLenses_jbs.dbo.aux_order_many_shipments ms
	inner join
		Warehouse.sales.dim_order_header_v oh on ms.order_id_bk = oh.order_id_bk
	inner join
		Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk
order by oh.order_id_bk desc, ol.order_line_id_bk

-----------------------------------------------------------------------


select oh.order_id_bk, oh.order_no, oh.order_date, oh.website, oh.product_type_oh_name, 
	sh.shipmentNumber, sh.dispatchDate, sh.expectedShippingDate, sh.snap_timestamp, sh.createdDate, 
	dense_rank() over (order by oh.order_id_bk desc) num_oh
from 
		DW_GetLenses_jbs.dbo.aux_order_many_shipments ms
	inner join
		Warehouse.sales.dim_order_header_v oh on ms.order_id_bk = oh.order_id_bk
	inner join
		Landing.mend.gen_ship_customershipment_v sh on oh.order_id_bk = sh.magentoOrderID_int
order by oh.order_id_bk desc


select sh.id, sh.shipmentID, sh.shipmentNumber, sh.orderIncrementID, sh.dispatchDate, sh.expectedShippingDate, sh.createdDate, 
	shl.id, shl.shipmentLineID, shl.productName, shl.quantityDescription, shl.itemQuantityOrdered, shl.itemQuantityIssued
from 
		Landing.mend.ship_customershipment_aud sh
	inner join
		Landing.mend.ship_customershipmentline_customershipment_aud shlsh on sh.id = shlsh.customershipmentid
	inner join
		Landing.mend.ship_customershipmentline_aud shl on shlsh.customershipmentlineid = shl.id
where sh.id in (23080948094523120, 23080948094507913)

-- customershipments$customershipmentline_orderlinemagento: Needed In Landing