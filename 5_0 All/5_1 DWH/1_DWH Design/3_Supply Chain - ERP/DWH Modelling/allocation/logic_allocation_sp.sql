

	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_header_erp_o

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.alloc_order_header_erp_o (orderid_bk, order_no_erp, order_id_bk)

		select ov.orderid orderid_bk, ov.incrementID order_no_erp, 
			case when (ov.magentoOrderID_int = ov.orderid) then null else ov.magentoOrderID_int end order_id_bk
		from 
				Landing.mend.gen_order_order_v ov
			inner join
				(select id
				from Landing.mend.order_order										-- New/Updated in Order
				union
				select shv.orderid
				from 
						Landing.mend.gen_ship_customershipment_v shv				-- New/Updated in Shipment
					inner join
						Landing.mend.ship_customershipment sh on shv.customershipmentid = sh.id
				union
				select satv.orderid
				from 
						Landing.mend.gen_all_stockallocationtransaction_v satv
					inner join
						(select id
						from Landing.mend.all_orderlinestockallocationtransaction	-- New/Updated in Stock Allocation Transaction
						union
						select bsav.orderlinestockallocationtransactionid id
						from
								Landing.mend.gen_wh_warehousestockitembatch_alloc_v bsav
							inner join
								(select id
								from Landing.mend.wh_batchstockallocation			-- New/Updated in Batch Stock Allocation
								union
								select bsiv.batchstockallocationid id
								from 
										Landing.mend.wh_batchstockissue bsi			-- New/Updated in Batch Stock Issue
									inner join
										Landing.mend.gen_wh_warehousestockitembatch_issue_v bsiv on bsi.id = bsiv.batchstockissueid) bsa 
											on bsav.batchstockallocationid = bsa.id) sat on satv.orderlinestockallocationtransactionid = sat.id) o on ov.orderid = o.id



	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_header_erp_dates

	-- INSERT STATEMENT: From XXXX
		-- Need to do it when Order Line ERP Issue - Order Line MAG ERP done
	insert into Landing.aux.alloc_order_header_erp_dates(orderid_bk, 
		idCalendarOrderDateSync, order_date_sync, 
		idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
		idCalendarExpectedShipmentDate, expected_shipment_date, 
		idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

		idCalendarAllocationDate, allocation_date, 
		idCalendarIssueDate, issue_date)

		select o.orderid_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), ov.createdDate, 112))) idCalendarOrderDateSync, ov.createdDate order_date_sync, 
			ol.idCalendarSNAPCompleteDate, ol.snap_complete_date, ol.idTimeSNAPCompleteDate,
			ol.idCalendarExpectedShipmentDate, ol.expected_shipment_date, 
			ol.idCalendarCageShipmentDate, ol.cage_shipment_date, ol.idTimeCageShipmentDate,

			ol.idCalendarAllocationDate, ol.allocation_date, 
			ol.idCalendarIssueDate, ol.issue_date
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				(select ov.orderid, ov.createdDate createdDate_old, 
					case when (ov.createdDate between stp.from_date and stp.to_date) then dateadd(hour, 1, ov.createdDate) else ov.createdDate end createdDate
				from 
						Landing.mend.gen_order_order_v ov
					left join
						Landing.map.sales_summer_time_period_aud stp on year(ov.createdDate) = stp.year) ov on o.orderid_bk = ov.orderid
			left join
				(select ol.orderid_bk, 
					max(olid.idCalendarSNAPCompleteDate) idCalendarSNAPCompleteDate, max(olid.snap_complete_date) snap_complete_date, 
					RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, max(olid.snap_complete_date))), 2) + ':' + case when (DATEPART(MINUTE, max(olid.snap_complete_date)) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
					max(olid.idCalendarExpectedShipmentDate) idCalendarExpectedShipmentDate, max(olid.expected_shipment_date) expected_shipment_date, 
					max(olid.idCalendarCageShipmentDate) idCalendarCageShipmentDate, max(olid.cage_shipment_date) cage_shipment_date,
					RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, max(olid.cage_shipment_date))), 2) + ':' + case when (DATEPART(MINUTE, max(olid.cage_shipment_date)) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate, 

					max(olid.idCalendarAllocationDate) idCalendarAllocationDate, max(allocation_date) allocation_date, 
					max(olid.idCalendarIssueDate) idCalendarIssueDate, max(issue_date) issue_date 
				from 
						Landing.aux.alloc_order_line_erp_issue_dates olid
					inner join
						Landing.aux.alloc_order_line_erp_issue_oli oli on olid.batchstockissueid_bk = oli.batchstockissueid_bk
					inner join
						Landing.aux.alloc_order_line_erp_ol ol on oli.orderlineid_bk = ol.orderlineid_bk
				group by ol.orderid_bk) ol on o.orderid_bk = ol.orderid_bk


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_header_erp_dim_order

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.alloc_order_header_erp_dim_order (orderid_bk, 
		order_type_erp_f,
		order_type_erp_bk, order_status_erp_bk, shipment_status_erp_bk, allocation_status_bk, allocation_strategy_bk, 
		country_id_shipping_bk, warehouseid_pref_bk)

		select o.orderid_bk, 
			case 
				when (left(o.order_no_erp, 1) = 'I') then 'I'
				when (left(o.order_no_erp, 1) = 'W') then 'W'
				else 'R'
			end order_type_erp_f,
			ov._type order_type_erp_bk, ov.orderStatus order_status_erp_bk, ov.shipmentStatus shipment_status_erp_bk, ov.allocationStatus allocation_status_bk, ov.allocationStrategy allocation_strategy_bk, 
			oav.country_code country_id_shipping_bk, oav.warehouseid warehouseid_pref_bk
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_order_v ov on o.orderid_bk = ov.orderid
			inner join
				(select orderid_bk, country_code, warehouseid
				from
					(select o.orderid_bk, o.order_no_erp, 
 						oav.orderaddressid, oav.country_code, oav.warehouseid, 
						count(*) over (partition by o.orderid_bk) num_rep, 
						rank() over (partition by o.orderid_bk order by oav.orderaddressid) rank_oav
					from 
							Landing.aux.alloc_order_header_erp_o o
						inner join
							Landing.mend.gen_order_orderaddress_v oav on o.orderid_bk = oav.orderid and oav.order_num = 1) t
				where t.rank_oav = 1) oav on o.orderid_bk = oav.orderid_bk

				select 
					oa.orderid, a.id orderaddressid, ac.countryid, -- whc.warehouseid,
					a.addresstype, a.countryid country_code_a, -- whc.country_code, whc.countryname, 
					-- whc.code, whc.name, whc.warehouseType, whc.order_num,
					count(*) over (partition by oa.orderid) num_rep_order, 
					dense_rank() over (partition by oa.orderid order by a.id) rank_rep_order,
					count(*) over (partition by oa.orderid, a.id) num_rep_orderaddress
					--, rank() over (partition by oa.orderid, a.id order by whc.order_num) rank_rep_orderaddress
				from 
						Landing.mend.order_orderaddress_aud a
					inner join
						Landing.mend.order_address_order_aud oa on a.id = oa.orderaddressid 
					inner join
						Landing.mend.order_orderaddress_country_aud ac on a.id = ac.orderaddressid -- many rows per orderaddressid
				where oa.orderid = 48695171018684817
				
	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_header_erp_measures

	-- INSERT STATEMENT: From XXXX
		-- Need to take local_to_global_rate for wholesale orders
	insert into Landing.aux.alloc_order_header_erp_measures (orderid_bk, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
		local_to_global_rate, order_currency_code)

		select o.orderid_bk, 
			ov.subtotal local_subtotal, ov.shippingAmount local_shipping, ov.discountAmount local_discount, ov.customerBalanceAmount local_store_credit_used, ov.grandTotal local_total_inc_vat, 
			ov.toGlobalRate local_to_global_rate, ov.currencyCode order_currency_code
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_order_v ov on o.orderid_bk = ov.orderid

------------------------------------------------------------


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_ol

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.alloc_order_line_erp_ol (orderlineid_bk, order_line_id_bk, orderid_bk)

		select olv.orderlineid orderlineid_bk, olv.magentoItemID order_line_id_bk, o.orderid_bk orderid_bk
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_orderline_v olv on o.orderid_bk = olv.orderid


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_product

	-- INSERT STATEMENT: From XXXX
		-- bom_f: Need to do it using Allocation Transactions: DONE

	select ol.orderlineid_bk, sta.allocationType
	into #alloc_order_line_erp_bom
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.mend.gen_all_stockallocationtransaction_v sta on ol.orderlineid_bk = sta.orderlineid
	where sta.allocationType = 'Parent'

	insert into Landing.aux.alloc_order_line_erp_product (orderlineid_bk, 
		productid_erp_bk, bom_f)

		select ol.orderlineid_bk, olv.productid productid_erp_bk,  
			case when (sta.orderlineid_bk is not null) then 'Y' else 'N' end bom_f
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join
				Landing.mend.gen_order_orderline_v olv on ol.orderlineid_bk = olv.orderlineid
			left join
				(select distinct orderlineid_bk 
				from #alloc_order_line_erp_bom) sta on ol.orderlineid_bk = sta.orderlineid_bk

	drop table #alloc_order_line_erp_bom

	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_measures

	-- INSERT STATEMENT: From XXXX
		-- local_price_unit, local_subtotal: On Intersite orders value is 0
	insert into Landing.aux.alloc_order_line_erp_measures (orderlineid_bk, 
		qty_unit, qty_unit_allocated, 	
		local_price_unit, local_subtotal, 
		local_to_global_rate, order_currency_code)

		select ol.orderlineid_bk, 
			olv.quantityOrdered qty_unit, olv.quantityAllocated qty_unit_allocated, 
			case when (basePrice = 0) then netPrice else basePrice end local_price_unit, 
			case when (baseRowPrice = 0) then netRowPrice else baseRowPrice end local_subtotal, 
			om.local_to_global_rate, om.order_currency_code
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join
				Landing.mend.gen_order_orderline_v olv on ol.orderlineid_bk = olv.orderlineid
			inner join
				Landing.aux.alloc_order_header_erp_measures om on olv.orderid = om.orderid_bk

------------------------------------------------------------

	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_issue_oli

	-- INSERT STATEMENT: From XXXX
		-- cancelled_f can be from sat - wsiba - wsibi: Take wsibi (Need to change view): DONE
	insert into Landing.aux.alloc_order_line_erp_issue_oli (batchstockissueid_bk, batchstockallocationid, orderlinestockallocationtransactionid_bk, 
		issue_id, 
		orderlineid_bk, 
		allocation_type_bk, allocation_record_type_bk, cancelled_f)

		select wsibi.batchstockissueid batchstockissueid_bk, wsiba.batchstockallocationid, sat.orderlinestockallocationtransactionid orderlinestockallocationtransactionid_bk, 
			wsibi.issueid, 
			ol.orderlineid_bk, 
			sat.allocationType allocation_type_bk, sat.recordType allocation_record_type_bk, 
			case when (wsibi.cancelled = 0) then 'N' else 'Y' end cancelled_f
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join
				Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderlineid_bk = sat.orderlineid
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on sat.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on wsiba.batchstockallocationid = wsibi.batchstockallocationid

	--select ol.orderlineid_bk, sta.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, wsibi.batchstockissueid
	--into #alloc_order_line_erp_issue_oli
	--from 
	--		Landing.aux.alloc_order_line_erp_ol ol
	--	inner join
	--		Landing.mend.gen_all_stockallocationtransaction_v sta on ol.orderlineid_bk = sta.orderlineid
	--	inner join
	--		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on sta.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
	--	inner join
	--		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on wsiba.batchstockallocationid = wsibi.batchstockallocationid

	--insert into Landing.aux.alloc_order_line_erp_issue_oli (batchstockissueid_bk, batchstockallocationid, orderlinestockallocationtransactionid_bk, 
	--	issue_id, 
	--	orderlineid_bk, 
	--	allocation_type_bk, allocation_record_type_bk, cancelled_f)

	--	select wsibi.batchstockissueid batchstockissueid_bk, wsiba.batchstockallocationid, oli.orderlinestockallocationtransactionid orderlinestockallocationtransactionid_bk, 
	--		wsibi.issueid, 
	--		oli.orderlineid_bk, 
	--		sat.allocationType allocation_type_bk, sat.recordType allocation_record_type_bk, 
	--		case when (wsibi.cancelled = 0) then 'N' else 'Y' end cancelled_f
	--	from 
	--			#alloc_order_line_erp_issue_oli oli
	--		inner join
	--			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid = wsibi.batchstockissueid
	--		inner join
	--			Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on oli.batchstockallocationid = wsiba.batchstockallocationid
	--		inner join
	--			Landing.mend.gen_all_stockallocationtransaction_v sat on oli.orderlinestockallocationtransactionid = sat.orderlinestockallocationtransactionid

	--drop table #alloc_order_line_erp_issue_oli



	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_issue_dates

		select oli.orderlineid_bk, max(slolm.snap_complete) snap_complete, max(slolm.expectedShippingDate) expectedShippingDate, max(slolm.snap_cage) snap_cage
		into #ol_ship
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_order_orderlinemagento_v olm on oli.orderlineid_bk = olm.orderlineid
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp.year) slolm on olm.orderlinemagentoid = slolm.orderlinemagentoid 
		group by oli.orderlineid_bk

	-- INSERT STATEMENT: From XXXX
		-- issue_date is shippeddate so need to change view: DONE
		-- Need to take Shipment related attributes: From Shipment Records (rel to OLM - OH)
	insert into Landing.aux.alloc_order_line_erp_issue_dates (batchstockissueid_bk, 
		idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
		idCalendarExpectedShipmentDate, expected_shipment_date, 
		idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate,

		idCalendarAllocationDate, allocation_date, 
		idCalendarIssueDate, issue_date)

		select wsibi.batchstockissueid batchstockissueid_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate,

			CONVERT(INT, (CONVERT(VARCHAR(8), wsiba.allocation_date, 112))) idCalendarAllocationDate, wsiba.allocation_date allocation_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), wsibi.issue_date, 112))) idCalendarIssueDate, wsibi.issue_date issue_date
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				(select batchstockissueid, wsibi.issue_date
					-- case when (wsibi.issue_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsibi.issue_date) else wsibi.issue_date end issue_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsibi.issue_date) = stp.year) wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				(select batchstockallocationid, 
					case when (wsiba.allocation_date between stp.from_date and stp.to_date) then dateadd(hour, 1, wsiba.allocation_date) else wsiba.allocation_date end allocation_date
				from 
						Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba
					left join
						Landing.map.sales_summer_time_period_aud stp on year(wsiba.allocation_date) = stp.year) wsiba on oli.batchstockallocationid = wsiba.batchstockallocationid
			left join
				#ol_ship slolm on oli.orderlineid_bk = slolm.orderlineid_bk

	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_erp_issue_measures

	-- INSERT STATEMENT: From XXXX
		-- qty_unit_order_line, qty_percentage from qty from OL (For BOM in a different way from STA): DONE (BOM done per num_lines instead of using qty)
		-- Exchange Rate + Currency on Batches
	insert into Landing.aux.alloc_order_line_erp_issue_measures (batchstockissueid_bk, 
		warehousestockitembatchid_bk, receiptlineid_bk,

		qty_unit_order_line, qty_unit, qty_stock, 
		qty_percentage, 
		local_total_unit_cost, local_total_cost, 
		local_to_global_rate, order_currency_code)

		select oli.batchstockissueid_bk, 
			wsibi.warehousestockitembatchid warehousestockitembatchid_bk, null receiptlineid_bk, 
			ol.qty_unit qty_unit_order_line, wsib.packSize * wsibi.issuedquantity_issue qty_unit, wsibi.issuedquantity_issue qty_stock, 
			(wsib.packSize * wsibi.issuedquantity_issue) / ol.qty_unit qty_percentage, 
			wsib.productUnitCost local_total_unit_cost, wsib.productUnitCost * wsibi.issuedquantity_issue local_total_cost, 
			wsib.exchangeRate local_to_global_rate, wsib.currency order_currency_code
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibi.warehousestockitembatchid = wsib.warehousestockitembatchid 
			inner join
				(select olp.orderlineid_bk, qty_unit
				from 
						Landing.aux.alloc_order_line_erp_product olp
					inner join
						Landing.aux.alloc_order_line_erp_measures olm on olp.orderlineid_bk = olm.orderlineid_bk
				where olp.bom_f = 'N') ol on oli.orderlineid_bk = ol.orderlineid_bk

	insert into Landing.aux.alloc_order_line_erp_issue_measures (batchstockissueid_bk, 
		warehousestockitembatchid_bk, receiptlineid_bk,

		qty_unit_order_line, qty_unit, qty_stock, 
		qty_percentage, 
		local_total_unit_cost, local_total_cost, 
		local_to_global_rate, order_currency_code)

		select oli.batchstockissueid_bk, 
			wsibi.warehousestockitembatchid warehousestockitembatchid_bk, null receiptlineid_bk, 
			ol.qty_unit qty_unit_order_line, wsib.packSize * wsibi.issuedquantity_issue qty_unit, wsibi.issuedquantity_issue qty_stock, 
			1 / convert(decimal(28, 8), count(*) over (partition by ol.orderlineid_bk)) qty_percentage, 
			wsib.productUnitCost local_total_unit_cost, wsib.productUnitCost * wsibi.issuedquantity_issue local_total_cost, 
			wsib.exchangeRate local_to_global_rate, wsib.currency order_currency_code
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibi.warehousestockitembatchid = wsib.warehousestockitembatchid 
			inner join
				(select olp.orderlineid_bk, qty_unit
				from 
						Landing.aux.alloc_order_line_erp_product olp
					inner join
						Landing.aux.alloc_order_line_erp_measures olm on olp.orderlineid_bk = olm.orderlineid_bk
				where olp.bom_f = 'Y') ol on oli.orderlineid_bk = ol.orderlineid_bk


------------------------------------------------------------


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_mag_erp_olm

	-- INSERT STATEMENT: From XXXX
		-- Make sure that order_line_id_bk is PK in gen_order_orderlinemagento_v (glasses)
	insert into Landing.aux.alloc_order_line_mag_erp_olm (order_line_id_bk, orderlineid_bk, orderlinemagentoid_bk)

		select ol.order_line_id_bk, olmv.orderlineid orderlineid_bk, olmv.orderlinemagentoid orderlinemagentoid_bk -- isnull(t.orderlineid_bk, 0)
		from 
				Landing.aux.alloc_order_header_erp_o o
			inner join
				Landing.mend.gen_order_orderlinemagento_v olmv on o.orderid_bk = olmv.orderid
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olmv.magentoItemID = ol.order_line_id_bk

	--select distinct olmv.orderlinemagentoid orderlinemagentoid_bk, olmv.orderlineid orderlineid_bk
	--into #olm_ol
	--from 
	--		Landing.aux.alloc_order_header_erp_o o
	--	inner join
	--		Landing.mend.gen_order_orderlinemagento_v olmv on o.orderid_bk = olmv.orderid
	--	-- left join
	--		-- Landing.mend.gen_all_magentoallocation_v ma on olmv.orderlinemagentoid = ma.orderlinemagentoid
	--	-- left join
	--		-- Landing.mend.gen_all_stockallocationtransaction_v sta on ma.orderlinestockallocationtransactionid = sta.orderlinestockallocationtransactionid


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_mag_erp_dates

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.alloc_order_line_mag_erp_dates (order_line_id_bk, 
		idCalendarSNAPCompleteDate, snap_complete_date, idTimeSNAPCompleteDate,
		idCalendarExpectedShipmentDate, expected_shipment_date, 
		idCalendarCageShipmentDate, cage_shipment_date, idTimeCageShipmentDate)

		select olm.order_line_id_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_complete, 112))) idCalendarSNAPCompleteDate, slolm.snap_complete snap_complete_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_complete)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_complete) between 1 and 29) then '00' else '30' end + ':00' idTimeSNAPCompleteDate,
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.expectedShippingDate, 112))) idCalendarExpectedShipmentDate, slolm.expectedShippingDate expected_shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), slolm.snap_cage, 112))) idCalendarCageShipmentDate, slolm.snap_cage cage_shipment_date, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, slolm.snap_cage)), 2) + ':' + case when (DATEPART(MINUTE, slolm.snap_cage) between 1 and 29) then '00' else '30' end + ':00' idTimeCageShipmentDate
		from 
				Landing.aux.alloc_order_line_mag_erp_olm olm
			left join 
				(select orderlinemagentoid, shipmentNumber, 
					case when (snap_complete between stp.from_date and stp.to_date) then dateadd(hour, 1, snap_complete) else snap_complete end snap_complete,
					expectedShippingDate, 
					case when (snap_cage between stp2.from_date and stp2.to_date) then dateadd(hour, 1, snap_cage) else snap_cage end snap_cage
				from
						(select orderlinemagentoid, max(shipmentNumber) shipmentNumber, 
							max(snap_complete) snap_complete, max(expectedShippingDate) expectedShippingDate, max(snap_cage) snap_cage
						from Landing.mend.gen_ship_customershipment_olm_v
						group by orderlinemagentoid) slolm
					left join
						Landing.map.sales_summer_time_period_aud stp on year(slolm.snap_complete) = stp.year
					left join
						Landing.map.sales_summer_time_period_aud stp2 on year(slolm.snap_cage) = stp.year) slolm on olm.orderlinemagentoid_bk = slolm.orderlinemagentoid 


	-- DELETE STATEMENT
	delete from Landing.aux.alloc_order_line_mag_erp_measures

	-- INSERT STATEMENT: From XXXX
		-- qty_unit_mag: ordered vs allocated
		-- local_to_global_rate, order_currency_code from OH, OL_Issue
		-- BOM products: qty_percentage needs to be in different way as more qty in Issue level than Line level
	insert into Landing.aux.alloc_order_line_mag_erp_measures (order_line_id_bk, 
		deduplicate_f, 

		qty_unit_mag, qty_unit_erp, qty_percentage, 
		local_total_cost_mag, local_total_cost_erp, 
		local_to_global_rate, order_currency_code)

		select olm.order_line_id_bk, 
			case when (ol.num_olm > 1) then 'Y' else 'N' end deduplicate_f, 
			
			-- olmv.quantityOrdered qty_unit_mag, 
			olmv.quantityAllocated qty_unit_mag, case when (olp.bom_f = 'N') then oli.sum_qty_unit else olmv.quantityAllocated end qty_unit_erp, 
			olmv.quantityAllocated / case when (olp.bom_f = 'N') then oli.sum_qty_unit else olmv.quantityAllocated end qty_percentage, 
			oli.sum_local_total_cost * (olmv.quantityAllocated / case when (olp.bom_f = 'N') then oli.sum_qty_unit else olmv.quantityAllocated end) local_total_cost_mag, 
			oli.sum_local_total_cost local_total_cost_erp, 
			o.local_to_global_rate, o.order_currency_code
		from 
				Landing.aux.alloc_order_line_mag_erp_olm olm
			inner join
				(select orderlineid_bk, count(*) num_olm
				from Landing.aux.alloc_order_line_mag_erp_olm
				group by orderlineid_bk) ol on olm.orderlineid_bk = ol.orderlineid_bk
			inner join
				Landing.mend.gen_order_orderlinemagento_v olmv on olm.orderlinemagentoid_bk = olmv.orderlinemagentoid
			left join
				(select oli.orderlineid_bk, sum(qty_unit) sum_qty_unit, sum(local_total_cost) sum_local_total_cost
				from 
						Landing.aux.alloc_order_line_erp_issue_oli oli
					inner join
						Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
				where oli.cancelled_f = 'N'
				group by oli.orderlineid_bk) oli on olm.orderlineid_bk = oli.orderlineid_bk
			inner join
				Landing.aux.alloc_order_line_erp_product olp on olm.orderlineid_bk = olp.orderlineid_bk			
			inner join
				Landing.aux.alloc_order_header_erp_measures o on olmv.orderid = o.orderid_bk	
	
	
	
	-- OLD
	--insert into Landing.aux.alloc_order_line_mag_erp_measures (order_line_id_bk, 
	--	deduplicate_f, 

	--	qty_unit_mag, qty_unit_erp, qty_percentage, 
	--	local_total_cost_mag, local_total_cost_erp, 
	--	local_to_global_rate, order_currency_code)

	--	select olm.order_line_id_bk, 
	--		case when (ol.num_olm > 1) then 'Y' else 'N' end deduplicate_f, 
			
	--		-- olmv.quantityOrdered qty_unit_mag, 
	--		olmv.quantityAllocated qty_unit_mag, oli.sum_qty_unit qty_unit_erp, 
	--		olmv.quantityAllocated / oli.sum_qty_unit qty_percentage, 
	--		oli.sum_local_total_cost * (olmv.quantityAllocated / oli.sum_qty_unit) local_total_cost_mag, oli.sum_local_total_cost local_total_cost_erp, 
	--		o.local_to_global_rate, o.order_currency_code
	--	from 
	--			Landing.aux.alloc_order_line_mag_erp_olm olm
	--		inner join
	--			(select orderlineid_bk, count(*) num_olm
	--			from Landing.aux.alloc_order_line_mag_erp_olm
	--			group by orderlineid_bk) ol on olm.orderlineid_bk = ol.orderlineid_bk
	--		inner join
	--			Landing.mend.gen_order_orderlinemagento_v olmv on olm.orderlinemagentoid_bk = olmv.orderlinemagentoid
	--		left join
	--			(select oli.orderlineid_bk, sum(qty_unit) sum_qty_unit, sum(local_total_cost) sum_local_total_cost
	--			from 
	--					Landing.aux.alloc_order_line_erp_issue_oli oli
	--				inner join
	--					Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
	--			where oli.cancelled_f = 'N'
	--			group by oli.orderlineid_bk) oli on olm.orderlineid_bk = oli.orderlineid_bk
	--		inner join
	--			Landing.aux.alloc_order_header_erp_measures o on olmv.orderid = o.orderid_bk

