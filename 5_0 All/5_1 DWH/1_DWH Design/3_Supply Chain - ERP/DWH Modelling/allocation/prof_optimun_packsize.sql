
drop table #ol

select order_line_id_bk, order_id_bk, order_no, order_date, invoice_date, website, customer_id, 
	product_id_magento, sku_magento, qty_unit
into #ol
from Warehouse.sales.fact_order_line_v
where order_date > '2019-06-01'

select 
	count(*) over (partition by order_id_bk, product_id_magento, sku_magento) num_rep, 
	sum(qty_unit) over (partition by order_id_bk, product_id_magento, sku_magento) sum_qty,
	*
from #ol
where product_id_magento = 1083
-- order by num_rep desc
order by website, sum_qty, order_no
