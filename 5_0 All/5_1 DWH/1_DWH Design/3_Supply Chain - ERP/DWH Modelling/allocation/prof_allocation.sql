
--------------------------------------------------------
-- erp_a_oh

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_oh
	where discountAmount <> 0
	order by createdDate

	select *
	from DW_GetLenses_jbs.dbo.erp_a_oh
	where _type = 'Wholesale_order'
	order by substring(incrementID, 1, 1), createdDate

	select _type, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_oh
	group by _type
	order by _type

	select orderStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_oh
	group by orderStatus
	order by orderStatus

	select shipmentStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_oh
	group by shipmentStatus
	order by shipmentStatus

	select allocationStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_oh
	group by allocationStatus
	order by allocationStatus

	select allocationStrategy, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_oh
	group by allocationStrategy
	order by allocationStrategy

	-- Check vs Magento Orders

	select top 1000 eoh.orderid, eoh.magentoOrderID_int, moh.order_id_bk, eoh.incrementID, moh.order_no, 
		eoh.createdDate, moh.order_date, moh.order_status_name_bk, moh.status_bk, eoh.orderStatus
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh eoh
		left join
			Landing.aux.sales_dim_order_header_aud moh on eoh.magentoOrderID_int = moh.order_id_bk
	where eoh._type = 'Sales_order'
		-- and moh.order_id_bk is null
		-- and moh.order_status_name_bk = 'CANCEL'
		and eoh.orderStatus = 'Cancelled' and order_status_name_bk <> 'REFUND'
		-- and eoh.orderStatus not in ('Completed', 'Cancelled')
	order by createdDate

	select moh.order_status_name_bk, count(*)
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh eoh
		left join
			Landing.aux.sales_dim_order_header_aud moh on eoh.magentoOrderID_int = moh.order_id_bk
	where eoh._type = 'Sales_order'
	group by moh.order_status_name_bk
	order by moh.order_status_name_bk

	select moh.status_bk, count(*)
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh eoh
		left join
			Landing.aux.sales_dim_order_header_aud moh on eoh.magentoOrderID_int = moh.order_id_bk
	where eoh._type = 'Sales_order'
	group by moh.status_bk
	order by moh.status_bk

-------------------------------------------------------- 
-- erp_a_ol

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_ol
	where orderLinesMagento <> orderLinesDeduped

	select count(*), count(distinct orderid), count(distinct orderlineid)
	from DW_GetLenses_jbs.dbo.erp_a_ol
	where orderLinesMagento <> orderLinesDeduped
	-- where orderlineid is not null
	-- where orderlineid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_a_ol
	where orderLinesMagento <> orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_a_ol
	group by magentoProductID, name
	order by magentoProductID, name


	select top 1000 ol.orderlineid, oh.orderid, oh.magentoOrderID_int, oh.incrementID, 
		oh.createdDate, 
		oh._type, oh.orderStatus, oh.allocationStatus, oh.allocationStrategy, 
		ol.magentoItemID,
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol ol on oh.orderid = ol.orderid
	-- where _type = 'Wholesale_order'
	-- where orderLinesMagento <> orderLinesDeduped
	where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID


-------------------------------------------------------- 
-- erp_a_olm

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_olm
	where orderLinesMagento <> orderLinesDeduped

	select count(*), count(distinct orderid), count(distinct orderlinemagentoid)
	from DW_GetLenses_jbs.dbo.erp_a_olm
	where orderLinesMagento <> orderLinesDeduped
	-- where orderlineid is not null
	-- where orderlineid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_a_olm
	where orderLinesMagento <> orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_a_olm
	group by magentoProductID, name
	order by magentoProductID, name


	select top 1000 ol.orderlinemagentoid, oh.orderid, oh.magentoOrderID_int, oh.incrementID, 
		oh.createdDate, 
		oh._type, oh.orderStatus, oh.allocationStatus, oh.allocationStrategy, 
		ol.magentoItemID,
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_olm ol on oh.orderid = ol.orderid
	-- where _type = 'Wholesale_order'
	-- where orderLinesMagento <> orderLinesDeduped
	where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID


-------------------------------------------------------- 
-- erp_a_olm_all

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_olm_all

	select count(*), count(distinct orderid), count(distinct orderlinemagentoid), count(distinct magentoallocationid), count(distinct orderlinestockallocationtransactionid)
	from DW_GetLenses_jbs.dbo.erp_a_olm_all

	select wh_name, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_olm_all
	group by wh_name
	order by wh_name

	select shipped, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_olm_all
	group by shipped
	order by shipped

	select top 10000 oh.orderid, ol.orderlinemagentoid, ola.magentoallocationid, ola.orderlinestockallocationtransactionid, 
		oh.magentoOrderID_int, ol.magentoItemID, oh.incrementID, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.createdDate, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, 
		ola.wh_name, ola.shipped, ola.cancelled_magentoall, ola.quantity, 
		t.num_rep, t.num_dist_wh, 
		count(*) over (partition by ola.orderlinestockallocationtransactionid) st_all_ded
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_olm ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_a_olm_all ola on oh.orderid = ola.orderid and ol.orderlinemagentoid = ola.orderlinemagentoid 
		left join
			(select orderlinemagentoid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_a_olm_all
			group by orderlinemagentoid
			having count(*) > 1) t on ol.orderlinemagentoid = t.orderlinemagentoid
	-- where _type = 'Wholesale_order' and t.orderlinemagentoid is not null
	-- where t.orderlinemagentoid is not null
	where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID

	select top 1000 *
	from
		(select *, 
			count(*) over (partition by orderlinestockallocationtransactionid) num_rep_ol_dedup, 
			count(*) over (partition by orderlinemagentoid) num_rep_olm_many
		from DW_GetLenses_jbs.dbo.erp_a_olm_all) t
	-- where num_rep_ol_dedup = 2
	where num_rep_olm_many = 4

	select num_rep_ol_dedup, num_rep_olm_many, count(*)
	from
		(select *, 
			count(*) over (partition by orderlinestockallocationtransactionid) num_rep_ol_dedup, 
			count(*) over (partition by orderlinemagentoid) num_rep_olm_many
		from DW_GetLenses_jbs.dbo.erp_a_olm_all) t
	group by num_rep_ol_dedup, num_rep_olm_many
	order by num_rep_ol_dedup, num_rep_olm_many

-------------------------------------------------------- 
-- erp_a_ol_all

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_ol_all

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all
	-- where orderlinestockallocationtransactionid is null
	where orderlinestockallocationtransactionid is not null

	select wh_name, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all
	group by wh_name
	order by wh_name

	select allocationType, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all
	-- where batchstockallocationid is null
	group by allocationType
	order by allocationType

	select recordType, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all
	group by recordType
	order by recordType

		select allocationType, recordType, count(*)
		from DW_GetLenses_jbs.dbo.erp_a_ol_all
		group by allocationType, recordType
		order by allocationType, recordType

	select top 10000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, 
		oh.magentoOrderID_int, ol.magentoItemID, oh.incrementID, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.createdDate, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		t.num_rep, t.num_dist_wh
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
	-- where _type = 'Wholesale_order' 
	-- where ola.allocationType = 'Child' -- Parent - Child - Standard
	-- where ola.recordType = 'Part' -- Allocation - Issue - Part - NULL
	-- where t.orderlineid is not null
	-- where ola.allocationType <> 'Parent' and ola.recordType = 'Allocation'
	where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID

		select ola.allocationType, ol.magentoProductID, ol.name, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_a_oh oh
			inner join
				DW_GetLenses_jbs.dbo.erp_a_ol ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_a_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			left join
				(select orderlineid, count(*) num_rep
				from DW_GetLenses_jbs.dbo.erp_a_ol_all
				group by orderlineid
				having count(*) > 1) t on ol.orderlineid = t.orderlineid
		where ola.orderlinestockallocationtransactionid is not null
		group by ola.allocationType, ol.magentoProductID, ol.name
		order by ola.allocationType, ol.magentoProductID, ol.name


-- erp_a_ol_all_ba

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
	where batchstockallocationid is not null and cancelled <> 1
  
	select fullyissued_alloc, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
	group by fullyissued_alloc
	order by fullyissued_alloc

	select cancelled, count(*)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
	group by cancelled
	order by cancelled

		select allocationType, recordType, cancelled, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_a_ol_all ola 
			left join	
				DW_GetLenses_jbs.dbo.erp_a_ol_all_ba olaba on ola.orderid = olaba.orderid and ola.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		group by allocationType, recordType, cancelled
		order by allocationType, recordType, cancelled

	select top 10000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid,
		oh.magentoOrderID_int, ol.magentoItemID, oh.incrementID, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.createdDate, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, olaba.stockitemdescription, olaba.batch_id, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		t.num_rep, t.num_dist_wh, t2.num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_a_ol_all
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
		left join
			DW_GetLenses_jbs.dbo.erp_a_ol_all_ba olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		left join
			(select orderlinestockallocationtransactionid, count(*) num_rep2
			from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
			group by orderlinestockallocationtransactionid
			having count(*) > 1) t2 on ola.orderlinestockallocationtransactionid = t2.orderlinestockallocationtransactionid
	-- where _type = 'Wholesale_order' 
	-- where ola.allocationType = 'Parent' -- Parent - Child - Standard
	-- where ola.recordType is null -- Allocation - Issue - Part - NULL
	-- where olaba.cancelled = 1
	-- where ola.recordType = 'Allocation' and olaba.cancelled = 0
	where ola.recordType = 'Issue' and olaba.cancelled = 1
	-- where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID

-- erp_a_ol_all_bi

	select top 1000 *
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_bi

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid), count(distinct batchstockissueid)
	from DW_GetLenses_jbs.dbo.erp_a_ol_all_bi
	where batchstockissueid is not null
	-- where batchstockissueid is null


		select allocationType, recordType, cancelled, 
			case when (olabi.batchstockissueid is not null) then 0 else 1 end f_bi,
			count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_a_ol_all ola 
			left join	
				DW_GetLenses_jbs.dbo.erp_a_ol_all_ba olaba on ola.orderid = olaba.orderid and ola.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_a_ol_all_bi olabi on ola.orderid = olabi.orderid and ola.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
		group by allocationType, recordType, cancelled, 
			case when (olabi.batchstockissueid is not null) then 0 else 1 end 
		order by allocationType, recordType, cancelled

	select top 10000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid, olabi.batchstockissueid,
		oh.magentoOrderID_int, ol.magentoItemID, oh.incrementID, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.createdDate, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, olaba.stockitemdescription, olaba.batch_id, olabi.stockitemdescription, olabi.batch_id, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		olabi.issueid, olabi.issuedquantity_issue, olabi.issue_date,
		t.num_rep, t.num_dist_wh, t2.num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_a_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_a_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_a_ol_all
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
		left join
			DW_GetLenses_jbs.dbo.erp_a_ol_all_ba olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		left join
			(select orderlinestockallocationtransactionid, count(*) num_rep2
			from DW_GetLenses_jbs.dbo.erp_a_ol_all_ba
			group by orderlinestockallocationtransactionid
			having count(*) > 1) t2 on ola.orderlinestockallocationtransactionid = t2.orderlinestockallocationtransactionid
		left join
			DW_GetLenses_jbs.dbo.erp_a_ol_all_bi olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
	-- where oh.orderid = 48695171011662659
	-- where _type = 'Wholesale_order' 
	-- where ola.allocationType = 'Parent' -- Parent - Child - Standard
	-- where olabi.batchstockallocationid is not null and oh.orderStatus <> 'Cancelled'  and ola.recordType = 'Allocation' -- Allocation - Issue - Part - NULL
	-- where olaba.cancelled = 1
	-- where olabi.batchstockissueid is not null and olaba.cancelled = 1 
	where oh.magentoOrderID_int in (7496923, 7496819, 7570952, 7565955)
	order by substring(incrementID, 1, 1), createdDate, magentoProductID
