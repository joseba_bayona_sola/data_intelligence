
select distinct oh.orderid_bk, oh.order_no_erp, oh.order_id_bk
into DW_GetLenses_jbs.dbo.alloc_oh_issue_date_null
from 
		Landing.aux.alloc_fact_order_line_erp_issue_aud oli
	inner join
		Landing.aux.alloc_fact_order_line_erp_aud ol on oli.orderlineid_bk = ol.orderlineid_bk
	inner join
		Landing.aux.alloc_dim_order_header_erp_aud oh on ol.orderid_bk = oh.orderid_bk
where oli.issue_date is null

select top 1000 orderid_bk, order_no_erp, order_id_bk, count(*) over (partition by orderid_bk) num_rep
from DW_GetLenses_jbs.dbo.alloc_oh_issue_date_null
order by num_rep desc

select orderid_bk, order_no_erp, order_id_bk
from DW_GetLenses_jbs.dbo.alloc_oh_issue_date_null
