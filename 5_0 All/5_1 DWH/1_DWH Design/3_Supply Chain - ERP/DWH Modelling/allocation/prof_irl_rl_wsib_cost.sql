
-- Review logic on stock_dim_wh_stock_item_batch_hist_info: Need to exclude those on T (They have been truncated)
select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
where invoicelineid_bk <> -1 

	select count(*), count(distinct warehousestockitembatchid_bk)
	from Landing.aux.stock_dim_wh_stock_item_batch_hist_info
	where update_type <> 'T'

select top 1000 *
from Landing.aux.stock_dim_wh_stock_item_batch_hist_info_v
where num_rep > 1

select *
from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud

	select count(distinct warehousestockitembatchid_bk)
	from Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud

	-- Receipt

	select top 1000 s.supplier_id supplier_id_bk, s.supplierType supplier_type_bk, s.supplierName supplier_name, s.defaultLeadTime default_lead_time, s.defaultTransitLeadTime default_transit_lead_time, 
		sc.discount_amount, sc.discount_date_from, sc.discount_date_to 
	from 
			Landing.mend.gen_supp_supplier_v s
		left join
			Landing.map.gen_supplier_discount_aud sc on s.supplierName = sc.supplier_name

	select top 1000 *
	from Landing.aux.rec_dim_receipt

	select top 1000 *
	from Landing.aux.rec_fact_receipt_line

	drop table #rl 

	select r.receiptid_bk, rl.receiptlineid_bk, 
		r.receipt_number,
		rl.local_unit_cost * rl.local_to_wh_rate local_unit_cost, rl.local_invoice_unit_cost * rl.local_to_wh_rate local_invoice_unit_cost, rl.currency_code_wh,
		s.discount_amount, s.discount_date_from, s.discount_date_to
	into #rl
	from 
			Landing.aux.rec_dim_receipt r
		inner join
			(select s.supplier_id supplier_id_bk, s.supplierType supplier_type_bk, s.supplierName supplier_name, s.defaultLeadTime default_lead_time, s.defaultTransitLeadTime default_transit_lead_time, 
				sc.discount_amount, sc.discount_date_from, sc.discount_date_to 
			from 
					Landing.mend.gen_supp_supplier_v s
				left join
					Landing.map.gen_supplier_discount_aud sc on s.supplierName = sc.supplier_name) s on r.supplier_id_bk = s.supplier_id_bk		
		inner join
			Landing.aux.rec_fact_receipt_line rl on r.receiptid_bk = rl.receiptid_bk

	-- Invoice Reconciliation Line

	select top 1000 *
	from Landing.aux.invrec_dim_invoice_reconciliation

	select top 1000 *
	from Landing.aux.invrec_fact_invoice_reconciliation_line

	select rl.receiptid_bk, rl.receiptlineid_bk, ir.invoiceid_bk, irl.invoicelineid_bk,
		rl.receipt_number, ir.invoice_ref, ir.net_suite_no, ir.posted_date,
		irl.adjusted_unit_cost * irl.local_to_wh_rate local_unit_cost, irl.currency_code_wh,
		rl.discount_amount, rl.discount_date_from, rl.discount_date_to
	into #irl
	from 
			Landing.aux.invrec_fact_invoice_reconciliation_line irl
		inner join
			#rl rl on irl.receiptlineid_bk = rl.receiptlineid_bk
		inner join
			Landing.aux.invrec_dim_invoice_reconciliation ir on irl.invoiceid_bk = ir.invoiceid_bk

	-- WSIB
	select top 1000 *
	from Landing.aux.stock_dim_wh_stock_item_batch

		select top 1000 count(*) -- 2235945
		from Landing.aux.stock_dim_wh_stock_item_batch


	-- Logic
		-- 25706
		select oli.batchstockissueid_bk, 
			wsibi.warehousestockitembatchid warehousestockitembatchid_bk, null receiptlineid_bk, 
			ol.qty_unit qty_unit_order_line, wsib.packSize * wsibi.issuedquantity_issue qty_unit, wsibi.issuedquantity_issue qty_stock, 
			(wsib.packSize * wsibi.issuedquantity_issue) / ol.qty_unit qty_percentage, 
			wsib.productUnitCost local_total_unit_cost, wsib.productUnitCost * wsibi.issuedquantity_issue local_total_cost, 
			-- case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end local_to_global_rate, 
			isnull(wsib.currency, w.currencycode) order_currency_code, 
			convert(datetime, wsib.stockregistereddate) date_exchange
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibi.warehousestockitembatchid = wsib.warehousestockitembatchid 
			inner join
				(select olp.orderlineid_bk, qty_unit
				from 
						Landing.aux.alloc_order_line_erp_product olp
					inner join
						Landing.aux.alloc_order_line_erp_measures olm on olp.orderlineid_bk = olm.orderlineid_bk
				where olp.bom_f = 'N') ol on oli.orderlineid_bk = ol.orderlineid_bk
			left join
				Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid



		select -- count(*) over (partition by oli.batchstockissueid_bk) num_rep,
			oli.batchstockissueid_bk, 
			wsibi.warehousestockitembatchid warehousestockitembatchid_bk, wsibh.receiptlineid_bk, wsibh.invoicelineid_bk,
			ol.qty_unit qty_unit_order_line, wsib.packSize * wsibi.issuedquantity_issue qty_unit, wsibi.issuedquantity_issue qty_stock, 
			(wsib.packSize * wsibi.issuedquantity_issue) / ol.qty_unit qty_percentage, 
			null local_total_unit_cost, null local_total_cost, 
			-- case when (wsib.exchangeRate = 0) then er.exchange_rate else wsib.exchangeRate end local_to_global_rate, 
			isnull(wsib.currency, w.currencycode) order_currency_code, 
			convert(datetime, wsib.stockregistereddate) date_exchange
			 -- oliir.warehousestockitembatchid_bk, oliir.invoicelineid_bk, wsibh.invoicelineid_bk, wsibh.receiptlineid_bk
		into #oli
		from 
				Landing.aux.alloc_order_line_erp_issue_oli oli
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on oli.batchstockissueid_bk = wsibi.batchstockissueid
			left join
				Landing.aux.alloc_fact_order_line_erp_issue_invrec_aud oliir on oli.batchstockissueid_bk = oliir.batchstockissueid_bk
			inner join
				Landing.aux.stock_dim_wh_stock_item_batch_hist_info wsibh on wsibi.warehousestockitembatchid = wsibh.warehousestockitembatchid_bk and wsibh.update_type <> 'T'
					and case 
						when (oliir.batchstockissueid_bk is not null and oliir.invoicelineid_bk = wsibh.invoicelineid_bk) then 1
						when (oliir.batchstockissueid_bk is null) then 1
						else 0
						end = 1 
			
			
			inner join
				Landing.mend.gen_wh_warehousestockitembatch_v wsib on wsibi.warehousestockitembatchid = wsib.warehousestockitembatchid 
			inner join
				(select olp.orderlineid_bk, qty_unit
				from 
						Landing.aux.alloc_order_line_erp_product olp
					inner join
						Landing.aux.alloc_order_line_erp_measures olm on olp.orderlineid_bk = olm.orderlineid_bk
				where olp.bom_f = 'N') ol on oli.orderlineid_bk = ol.orderlineid_bk
			left join
				Landing.mend.gen_wh_warehouse_v w on wsib.warehouseid = w.warehouseid

		-- where oliir.batchstockissueid_bk is not null

		select oli.*, wsib.local_product_unit_cost, rl.local_unit_cost, irl.local_unit_cost, irl.posted_date, c.calendar_date, c.calendar_date_apply_from -- issue_date? order_date? invoice_date?
			--case when oli.receiptlineid_bk is null then wsib.local_total_unit_cost end local_total_unit_cost
			-- case when oli.receiptlineid_bk is not null and oli.invoicelineid_bk = -1 then
		from 
			#oli oli
		left join
			#rl rl on oli.receiptlineid_bk = rl.receiptlineid_bk
		left join
			#irl irl on oli.invoicelineid_bk = irl.invoicelineid_bk
		inner join
			Landing.aux.stock_dim_wh_stock_item_batch wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk 

		left join
			(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
				dateadd(dd, calendar_date_num * -1, calendar_date) calendar_date_apply_from
			from
				(select idCalendar_sk, calendar_date, calendar_date_num, calendar_date_week_name, month_name, month_num, year_num, 
					dense_rank() over (partition by year_num, month_num order by calendar_date_num) ord_calendar_date_num
				from Landing.aux.dim_calendar
				where calendar_date_week_name not in ('Saturday', 'Sunday')) c
			where c.ord_calendar_date_num in (1, 2)) c on convert(date, irl.posted_date) = c.calendar_date -- and wsibm.batch_movement_date between c.calendar_date_apply_from and c.calendar_date

		order by irl.posted_date desc

		select *
		from Landing.aux.dim_calendar