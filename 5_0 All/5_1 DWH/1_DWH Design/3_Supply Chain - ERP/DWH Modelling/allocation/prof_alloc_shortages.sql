

select top 1000 count(*) over (), *
from Landing.mend.gen_short_shortageheader_v
-- where shortageheaderid = 98516241848922921 
-- where shortageheaderid is null -- 0
-- where shortageheaderid = 98516241848922552
where warehouse_name_src = 'Amsterdam' and warehouse_name_dest = 'Amsterdam' and supplierName = 'Alcon NL' and wholesale = 0 and packsizeid = 40532396646334818
-- where delete_f = 'Y'
order by shortageheaderid desc

select top 1000 count(*) over (), 
	count(*) over (partition by shortageheaderid) num_rep,
	count(*) over (partition by shortage_id) num_rep_sh, -- due to shortages with many suppliers
	*
from Landing.mend.gen_short_shortage_v
-- where shortageheaderid = 98516241848921820
-- where shortage_id is null -- 28417
-- where shortage_id is not null order by shortage_id
where warehouse_name_src = 'Amsterdam' and warehouse_name_dest = 'Amsterdam' and supplierName = 'Alcon NL' and wholesale = 0 and packsizeid = 40532396646334818
order by shortageheaderid desc


select top 1000 count(*) over (), 
	count(*) over (partition by shortageheaderid) num_rep,
	*
from Landing.mend.gen_short_orderlineshortage_v
where shortageheaderid = 98516241848921820
-- where shortage_id is null -- 28417
-- where orderlineshortageid is null -- 31039
-- where orderlineshortageid is null and shortage_id is not null order by createdDate desc -- 2622
-- where ordernumbers in ('8005310506', '5001978589')
-- where orderlineshortageid is not null and createdDate_ol < '2019-03-01' and wholesale <> 1 order by createdDate_ol desc -- 85
-- where orderlineshortageid is not null and createdDate_ol > '2019-03-01' and wholesale <> 1 order by createdDate_ol -- 85
-- where orderlineshortageid is not null order by createdDate_ol desc
--where warehouse_name_src = 'Amsterdam' and warehouse_name_dest = 'Amsterdam' and supplierName = 'Alcon NL' and wholesale = 0 and packsizeid = 40532396646334818

--------------------------------------------

select count(*)
from Landing.mend.gen_short_shortageheader_v

	select delete_f, count(*)
	from Landing.mend.gen_short_shortageheader_v
	group by delete_f
	order by delete_f

	select warehouse_name_src, warehouse_name_dest, count(*)
	from Landing.mend.gen_short_shortageheader_v
	group by warehouse_name_src, warehouse_name_dest
	order by warehouse_name_src, warehouse_name_dest

	select warehouse_name_src, warehouse_name_dest, supplierName, count(*)
	from Landing.mend.gen_short_shortageheader_v
	group by warehouse_name_src, warehouse_name_dest, supplierName
	order by warehouse_name_src, warehouse_name_dest, supplierName

	select warehouse_name_src, warehouse_name_dest, supplierName, wholesale, packsizeid, count(*)
	from Landing.mend.gen_short_shortageheader_v
	group by warehouse_name_src, warehouse_name_dest, supplierName, wholesale, packsizeid
	order by warehouse_name_src, warehouse_name_dest, supplierName, wholesale, packsizeid

select count(*)
from Landing.mend.short_shortageheader

select count(*)
from Landing.mend.short_shortageheader_aud

--------------------------------------------

select count(*)
from Landing.mend.gen_short_shortage_v

	select count(*), count(distinct shortage_id), count(distinct shortageid) -- solve on view shortage_id with multiple suppliers
	from Landing.mend.gen_short_shortage_v
	-- where shortageheaderid is not null and shortage_id is not null

	select status, count(*)
	from Landing.mend.gen_short_shortage_v
	group by status
	order by status

	select top 10000 status, case when (purchaseordernumber is not null) then 1 else null end po_number, 
		case when (purchaseorderlineid is not null) then 1 else null end purchaseorderlineid, 
		count(*)
	from Landing.mend.gen_short_shortage_v
	where shortageheaderid is not null and shortage_id is not null
	group by status, case when (purchaseordernumber is not null) then 1 else null end, case when (purchaseorderlineid is not null) then 1 else null end
	order by status, po_number, purchaseorderlineid

select count(*)
from Landing.mend.short_shortage

select count(*)
from Landing.mend.short_shortage_aud

	select count(*), count(distinct id), count(distinct shortageid)
	from Landing.mend.short_shortage_aud

	select status, count(*)
	from Landing.mend.short_shortage_aud
	group by status
	order by status

--------------------------------------------

select count(*)
from Landing.mend.gen_short_orderlineshortage_v

	select count(*), count(distinct orderlineshortageid), count(distinct shortage_id), count(distinct shortageid), count(distinct orderlineid)
	from Landing.mend.gen_short_orderlineshortage_v
	-- where shortageheaderid is not null and shortage_id is not null
	where ord_rep = 1

	select wholesale, count(*)
	from Landing.mend.gen_short_orderlineshortage_v
	group by wholesale
	order by wholesale

select count(*)
from Landing.mend.short_orderlineshortage

select count(*)
from Landing.mend.short_orderlineshortage_aud

	select orderlineshortagetype, count(*)
	from Landing.mend.short_orderlineshortage_aud
	group by orderlineshortagetype
	order by orderlineshortagetype

---------------------------------------------------------

	-- PK: On orderlineshortageid
		-- When Shortage (shortage_id) with many headers is duplicated (Shortages where Intersite Transfer is needed --> Many suppliers)
		-- xxx_new views without Shortage Header
	-- Unique on orderlineid
		-- No always: An orderline can be related to many orderlineshortageid (Why ???)

---------------------------------------------------------

	select count(*), count(distinct shortage_id), count(distinct shortageid) -- solve on view shortage_id with multiple suppliers
	from Landing.mend.gen_short_shortage_new_v

	select count(*), count(distinct orderlineshortageid), count(distinct shortage_id), count(distinct shortageid), count(distinct orderlineid)
	from Landing.mend.gen_short_orderlineshortage_new_v


select top 1000 count(*) over (partition by orderlineshortageid) num_rep, *
from Landing.mend.gen_short_orderlineshortage_new_v
order by num_rep desc, orderlineshortageid desc, shortage_id desc

select top 1000 count(*) over (partition by shortage_id) num_rep, *
from Landing.mend.gen_short_orderlineshortage_new_v
order by num_rep desc, orderlineshortageid desc, shortage_id desc, orderlineid

select top 1000 count(*) over (partition by orderlineid) num_rep, *
from Landing.mend.gen_short_orderlineshortage_new_v
-- where delete_f = 'N'
where purchaseorderlineid is not null
order by num_rep desc, orderlineid desc, orderlineshortageid desc, shortage_id desc


--------------------------------------------------------
-------------------------------------------------------- Duplicated OrderlineShortages: Due to Double Shortage Header (Double Supplier) / 
--------------------------------------------------------

-- With Shortage Header
	
	-- Identify Cases
	select *
	into #gen_short_orderlineshortage_v_dupl
	from
		(select count(*) over (partition by orderlineshortageid) num_rep, *
		from Landing.mend.gen_short_orderlineshortage_v) t
	where num_rep > 1

		select *
		from #gen_short_orderlineshortage_v_dupl
		order by num_rep desc, orderlineshortageid desc

	-- Solution
	select *
	into #gen_short_orderlineshortage_v_dupl
	from
		(select count(*) over (partition by orderlineshortageid) num_rep, 
			dense_rank() over (partition by orderlineshortageid order by abs(code_src - code_dest) desc, shortageheaderid, shortage_id) ord_rep,
			*
		from Landing.mend.gen_short_orderlineshortage_v) t
	where num_rep > 1 and ord_rep = 1

		select count(*) over (partition by orderlineshortageid) num_rep2, *
		from #gen_short_orderlineshortage_v_dupl
		order by num_rep2 desc, orderlineshortageid desc


-- 

-- With NO Shortage Header
select top 1000 count(*) over (partition by orderlineshortageid) num_rep, *
from Landing.mend.gen_short_orderlineshortage_new_v
order by num_rep desc
