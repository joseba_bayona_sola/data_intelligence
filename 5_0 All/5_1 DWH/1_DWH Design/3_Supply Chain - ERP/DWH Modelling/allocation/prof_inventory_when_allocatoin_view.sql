

select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog_aud
where reference like 'SC%'

select count(*)
from Landing.mend.gen_wh_warehousestockitemlog_v 

	select count(*), count(distinct warehousestockitemlogid)
	from Landing.mend.gen_wh_warehousestockitemlog_v 

	select warehousepreference, count(*)
	from Landing.mend.gen_wh_warehousestockitemlog_v 
	group by warehousepreference
	order by warehousepreference

select top 1000 
	-- count(*) over (partition by warehousestockitemlogid) num_rep, 
	-- count(*) over (partition by orderid, productid) num_rep, 
	count(*) over (partition by orderid, productid, warehouseid) num_rep, 
	*
from Landing.mend.gen_wh_warehousestockitemlog_v 
order by num_rep desc, orderid, productid, warehousepreference, packsizepreference, createddate

select top 1000 *
from Landing.mend.gen_wh_warehousestockitemlog_v 
where orderid = 48695171027036454
order by orderid, createddate, productid, warehousepreference, packsizepreference

select num_ol, count(*), count(*) * 100 / convert(decimal(12, 4), num_tot)
from
	(select orderid, productid, count(*) num_ol, count(*) over () num_tot
	from Landing.mend.gen_wh_warehousestockitemlog_v 
	group by orderid, productid) num
group by num_ol, num_tot
order by num_ol


