
select top 1000 *
from Landing.mend.gen_wh_warehousestockitemlog_v
where orderid in (48695171042176160, 48695171036833561, 48695171042280025)
order by orderid, productid

	select top 1000 count(*), count(distinct warehousestockitemlogid)
	from Landing.mend.gen_wh_warehousestockitemlog_v

	select top 1000 count(*) over (partition by orderid, productid, warehouseid, packsizepreference) num_rep, *
	from Landing.mend.gen_wh_warehousestockitemlog_v
	order by num_rep desc

	select top 1000 wsil.orderid, wsil.productid, count(*) num, count(distinct warehouseid) num_w
	from 
			Landing.mend.gen_wh_warehousestockitemlog_v wsil
		inner join
			Landing.aux.alloc_order_header_erp_o o on wsil.orderid = o.orderid_bk
	-- where orderid = 48695171042176160
	group by wsil.orderid, wsil.productid
	order by orderid, productid

----------------------------------------------


select count(*) over (partition by wsil.warehousestockitemlogid) num_rep,
	wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
	wsil.orderid, null orderlineid_bk, 
	wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, wsil.productid,
	
	wsil.createddate stock_check_date, 
	
	wsil.warehousepreference warehouse_preference, 
	wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
	wsil.success sucess_f, null preference_order
from 
		Landing.mend.gen_wh_warehousestockitemlog_v wsil
	inner join
		Landing.aux.alloc_order_header_erp_o o on wsil.orderid = o.orderid_bk
where wsil.orderid in (48695171042176160, 48695171036833561, 48695171042280025)
order by num_rep desc, wsil.orderid



select count(*) over (partition by wsil.warehousestockitemlogid) num_rep,
	count(*) over (partition by wsil.orderid, wsil.productid) num_rep2,
	wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
	wsil.orderid, ol.orderlineid_bk, 
	wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, wsil.productid,
	
	wsil.createddate stock_check_date, 
	
	wsil.warehousepreference warehouse_preference, 
	wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
	wsil.success sucess_f, null preference_order
from 
		Landing.mend.gen_wh_warehousestockitemlog_v wsil
	inner join
		Landing.aux.alloc_order_header_erp_o o on wsil.orderid = o.orderid_bk
	left join
		(select ol.orderlineid_bk, ol.orderid_bk, olp.productid_erp_bk, 
			count(*) over (partition by ol.orderid_bk, olp.productid_erp_bk) num_rep,
			dense_rank() over (partition by ol.orderid_bk, olp.productid_erp_bk order by ol.orderlineid_bk) ord_rep
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join	
				Landing.aux.alloc_order_line_erp_product olp on ol.orderlineid_bk = olp.orderlineid_bk) ol on o.orderid_bk = ol.orderid_bk and wsil.productid = ol.productid_erp_bk
where wsil.orderid in (48695171042176160, 48695171036833561, 48695171042280025) order by wsil.orderid, wsil.productid
-- where ol.ord_rep = 1 order by num_rep desc
where ol.num_rep <> 1 order by num_rep desc

--------------------

select ol.orderlineid_bk, ol.orderid_bk, olp.productid_erp_bk, 
	count(*) over (partition by ol.orderid_bk, olp.productid_erp_bk) num_rep,
	dense_rank() over (partition by ol.orderid_bk, olp.productid_erp_bk order by ol.orderlineid_bk) ord_rep
from 
		Landing.aux.alloc_order_line_erp_ol ol
	inner join	
		Landing.aux.alloc_order_line_erp_product olp on ol.orderlineid_bk = olp.orderlineid_bk
where ol.orderid_bk in (48695171042176160, 48695171036833561, 48695171042280025)
order by ol.orderid_bk, olp.productid_erp_bk

select top 1000 orderlineid_bk, orderid_bk, productid_erp_bk, 
	count(*) over (partition by orderid_bk, productid_erp_bk) num_rep,
	dense_rank() over (partition by orderid_bk, productid_erp_bk order by orderlineid_bk) ord_rep
from Landing.aux.alloc_fact_order_line_erp_aud
order by num_rep desc, orderid_bk, productid_erp_bk

--------------------
--------------------
--------------------

		drop table #ol

		-- Incremental
		select ol.orderlineid_bk, ol.orderid_bk, olp.productid_erp_bk, 
			count(*) over (partition by ol.orderid_bk, olp.productid_erp_bk) num_rep,
			dense_rank() over (partition by ol.orderid_bk, olp.productid_erp_bk order by ol.orderlineid_bk) ord_rep -- for OH where same productid in different OL
		into #ol
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join	
				Landing.aux.alloc_order_line_erp_product olp on ol.orderlineid_bk = olp.orderlineid_bk

		-- Full
		select orderlineid_bk, orderid_bk, productid_erp_bk, 
			count(*) over (partition by orderid_bk, productid_erp_bk) num_rep,
			dense_rank() over (partition by orderid_bk, productid_erp_bk order by orderlineid_bk) ord_rep
		into #ol
		from Landing.aux.alloc_fact_order_line_erp_aud


select top 1000 
	count(*) over (), -- 4474834
	count(*) over (partition by wsil.warehousestockitemlogid) num_rep,
	wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
	wsil.orderid, ol.orderlineid_bk, 
	wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, wsil.productid,
	
	wsil.createddate stock_check_date, 
	
	wsil.warehousepreference warehouse_preference, 
	wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
	wsil.success sucess_f, null preference_order
from 
		Landing.mend.gen_wh_warehousestockitemlog_v wsil
	inner join
		#ol ol on wsil.orderid = ol.orderid_bk and wsil.productid = ol.productid_erp_bk
where ol.num_rep = 1 
order by num_rep desc

select top 1000 
	count(*) over (), -- 4474834
	count(*) over (partition by wsil.warehousestockitemlogid) num_rep,
	wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
	wsil.orderid, ol.orderlineid_bk, 
	wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, wsil.productid,
	
	wsil.createddate stock_check_date, 
	
	wsil.warehousepreference warehouse_preference, 
	wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
	wsil.success sucess_f, null preference_order
from 
		(select dense_rank() over (partition by orderid, productid, warehouseid, packsizepreference order by warehousestockitemlogid) ord_rep, *
		from Landing.mend.gen_wh_warehousestockitemlog_v) wsil
	inner join
		#ol ol on wsil.orderid = ol.orderid_bk and wsil.productid = ol.productid_erp_bk and wsil.ord_rep = ol.ord_rep
where ol.num_rep <> 1 
order by num_rep desc


------------------

drop table #alloc_fact_order_line_erp_stock_level

create table #alloc_fact_order_line_erp_stock_level(
	warehousestockitemlogid_bk				bigint NOT NULL,

	orderlineid_bk							bigint NOT NULL, 

	warehouseid_bk							bigint, 
	stockitemid_bk							bigint,

	idCalendarStockCheckDate				int,
	stock_check_date						datetime,

	warehouse_preference					int, 

	packsize_preference						int, 
	required_quantity						int, 
	free_to_sell							int, 

	sucess_f								char(1),
	preference_order						int)

alter table #alloc_fact_order_line_erp_stock_level add constraint [PK_#_alloc_fact_order_line_erp_stock_level]
	primary key clustered (warehousestockitemlogid_bk);
go

insert into #alloc_fact_order_line_erp_stock_level (warehousestockitemlogid_bk, 
	orderlineid_bk, 
	warehouseid_bk, stockitemid_bk, 
	
	idCalendarStockCheckDate, stock_check_date, 

	warehouse_preference, 
	packsize_preference, required_quantity, free_to_sell, 
	sucess_f, preference_order)

	select 
		wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
		ol.orderlineid_bk, 
		wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, 
	
		CONVERT(INT, (CONVERT(VARCHAR(8), wsil.createddate, 112))) idCalendarStockCheckDate, wsil.createddate stock_check_date, 
	
		wsil.warehousepreference warehouse_preference, 
		wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
		wsil.success sucess_f, 
		dense_rank() over (partition by ol.orderlineid_bk order by wsil.warehousepreference, wsil.packsizepreference, wsil.createddate, wsil.warehousestockitemlogid) preference_order
	from 
			Landing.mend.gen_wh_warehousestockitemlog_v wsil
		inner join
			#ol ol on wsil.orderid = ol.orderid_bk and wsil.productid = ol.productid_erp_bk
	where ol.num_rep = 1 

insert into #alloc_fact_order_line_erp_stock_level (warehousestockitemlogid_bk, 
	orderlineid_bk, 
	warehouseid_bk, stockitemid_bk, 
	
	idCalendarStockCheckDate, stock_check_date, 

	warehouse_preference, 
	packsize_preference, required_quantity, free_to_sell, 
	sucess_f, preference_order)

	select 
		wsil.warehousestockitemlogid warehousestockitemlogid_bk, 
		ol.orderlineid_bk, 
		wsil.warehouseid warehouseid_bk, wsil.stockitemid stockitemid_bk, 
	
		CONVERT(INT, (CONVERT(VARCHAR(8), wsil.createddate, 112))) idCalendarStockCheckDate, wsil.createddate stock_check_date, 
	
		wsil.warehousepreference warehouse_preference, 
		wsil.packsizepreference packsize_preference, wsil.requiredquantity required_quantity, wsil.freetosell free_to_sell, 
		wsil.success sucess_f, 
		dense_rank() over (partition by ol.orderlineid_bk order by wsil.warehousepreference, wsil.packsizepreference, wsil.createddate, wsil.warehousestockitemlogid) preference_order
	from 
		(select dense_rank() over (partition by orderid, productid, warehouseid, packsizepreference order by warehousestockitemlogid) ord_rep, *
		from Landing.mend.gen_wh_warehousestockitemlog_v) wsil
		inner join
			#ol ol on wsil.orderid = ol.orderid_bk and wsil.productid = ol.productid_erp_bk and wsil.ord_rep = ol.ord_rep
where ol.num_rep <> 1 

------------------

select top 1000 count(*) over (partition by orderlineid_bk) num_rep, *
	-- , dense_rank() over (partition by orderlineid_bk order by warehouse_preference, packsize_preference, stock_check_date, warehousestockitemlogid_bk) preference_order
from #alloc_fact_order_line_erp_stock_level
-- where warehousestockitemlogid_bk in (194780683892277787, 194780683892277788, 194780683892277789, 194780683892277790)
order by num_rep desc, orderlineid_bk

	select orderlineid_bk, preference_order, count(*)
	from #alloc_fact_order_line_erp_stock_level
	group by orderlineid_bk, preference_order
	having count(*) > 1
	order by orderlineid_bk, preference_order

-- work on preference_order per orderlineid_bk: Based on warehouse_preference, packsize_preference, stock_check_date desc, warehousestockitemlogid_bk

-- work inventory_level_success_f in OL: preference_order vs success ??