---------------------------------------
7947611
8004619259
---------------------------------------


select count(*)
from Landing.aux.alloc_order_header_erp_o

select *
from Landing.aux.alloc_order_header_erp_dates

select count(*)
from Landing.aux.alloc_order_header_erp_dim_order

	select order_type_erp_f, count(*)
	from Landing.aux.alloc_order_header_erp_dim_order
	group by order_type_erp_f
	order by order_type_erp_f

	select t.country_id_shipping_bk, c.idCountry_sk, t.num
	from
			(select country_id_shipping_bk, count(*) num
			from Landing.aux.alloc_order_header_erp_dim_order
			group by country_id_shipping_bk) t
		left join
			Warehouse.gen.dim_country c on t.country_id_shipping_bk = c.country_id_bk
	where c.idCountry_sk is null
	order by country_id_shipping_bk

select *
from Landing.aux.alloc_order_header_erp_measures

	select o.orderid_bk, o.order_id_bk, o.order_no_erp, 
		om.local_subtotal, oh.local_subtotal, om.local_shipping, oh.local_shipping, om.local_discount, oh.local_discount, 
		om.local_store_credit_used, oh.local_store_credit_used, om.local_total_inc_vat, oh.local_total_inc_vat,
		om.local_to_global_rate, oh.local_to_global_rate, om.order_currency_code, oh.order_currency_code
	from 
			Landing.aux.alloc_order_header_erp_measures om
		inner join
			Landing.aux.alloc_order_header_erp_o o on om.orderid_bk = o.orderid_bk
		left join
			Landing.aux.sales_dim_order_header_aud oh on o.order_id_bk = oh.order_id_bk
	-- where o.order_id_bk is null
	-- where o.order_id_bk is not null and om.local_subtotal <> oh.local_subtotal
	-- where o.order_id_bk is not null and om.local_discount <> oh.local_discount
	-- where o.order_id_bk is not null and om.local_store_credit_used <> oh.local_store_credit_used
	-- where o.order_id_bk is not null and om.local_total_inc_vat <> oh.local_total_inc_vat
	-- where o.order_id_bk is not null and om.local_to_global_rate <> oh.local_to_global_rate
	where om.local_to_global_rate = 0
	order by o.order_no_erp



------------------------------------------------------------

select *
from Landing.aux.alloc_order_line_erp_ol

	select count(*), count(distinct orderid_bk)
	from Landing.aux.alloc_order_line_erp_ol

	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		ol.orderlineid_bk, ol.order_line_id_bk
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
	order by o.order_no_erp

select *
from Landing.aux.alloc_order_line_erp_product

	select count(*)
	from Landing.aux.alloc_order_line_erp_product

	select olp.*, p.magentoProductID, p.name, p.SKU_product, p.description
	from 
			Landing.aux.alloc_order_line_erp_product olp
		left join
			Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
	-- where p.productid is null
	where olp.bom_f = 'N' and p.magentoProductID = '2352'

	select p.magentoProductID, p.name, count(*), count(distinct bom_f)
	from 
			Landing.aux.alloc_order_line_erp_product olp
		left join
			Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
	group by p.magentoProductID, p.name
	order by count(distinct bom_f) desc, p.magentoProductID, p.name

	select olp.bom_f, p.magentoProductID, pf.product_id_bk, pf.erp_product, p.name, count(*)
	from 
			Landing.aux.alloc_order_line_erp_product olp
		left join
			Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
		left join
			Landing.aux.mag_prod_product_family_v pf on p.magentoProductID = pf.product_id_bk
	group by olp.bom_f, p.magentoProductID, pf.product_id_bk, pf.erp_product, p.name
	order by olp.bom_f desc, p.magentoProductID, p.name

select *
from Landing.aux.alloc_order_line_erp_measures

	select count(*)
	from Landing.aux.alloc_order_line_erp_measures

	-- Can't compare to Magento OL due to deduplication
	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		ol.orderlineid_bk, ol.order_line_id_bk, 
		olm.qty_unit, olm.qty_unit_allocated, 	
		olm.local_price_unit, olm.local_subtotal, 
		olm.local_to_global_rate, olm.order_currency_code
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
		inner join
			Landing.aux.alloc_order_line_erp_measures olm on ol.orderlineid_bk = olm.orderlineid_bk
	-- where olm.local_subtotal = 0
	order by o.order_no_erp


------------------------------------------------------------

	select *
	from Landing.mend.all_orderlinestockallocationtransaction_aud
	where id = 9570149298357908

	select *
	from Landing.mend.wh_batchstockallocation_aud
	where id = 78812993527345441

	select *
	from Landing.mend.wh_batchstockissue_aud
	where id = 89509042601173003


select *
from Landing.aux.alloc_order_line_erp_issue_oli

	select count(*), count(distinct orderlinestockallocationtransactionid_bk), count(distinct orderlineid_bk)
	from Landing.aux.alloc_order_line_erp_issue_oli

	select allocation_type_bk, count(*)
	from Landing.aux.alloc_order_line_erp_issue_oli
	group by allocation_type_bk
	order by allocation_type_bk

	select allocation_record_type_bk, count(*)
	from Landing.aux.alloc_order_line_erp_issue_oli
	group by allocation_record_type_bk
	order by allocation_record_type_bk

	select cancelled_f, count(*)
	from Landing.aux.alloc_order_line_erp_issue_oli
	group by cancelled_f
	order by cancelled_f

	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		ol.orderlineid_bk, ol.order_line_id_bk, 
		oli.batchstockissueid_bk, oli.issue_id, oli.allocation_type_bk, oli.allocation_record_type_bk, oli.cancelled_f
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_oli oli on ol.orderlineid_bk = oli.orderlineid_bk
	-- where oli.orderlineid_bk is null
	where oli.cancelled_f = 'Y'
	order by o.order_no_erp
		
		select count(*), count(distinct orderlinestockallocationtransactionid_bk), 
				count(distinct oli.orderlineid_bk), count(distinct ol.orderlineid_bk), 
				count(distinct ol.orderid_bk)
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join
				Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
			left join
				Landing.aux.alloc_order_line_erp_issue_oli oli on ol.orderlineid_bk = oli.orderlineid_bk

select *
from Landing.aux.alloc_order_line_erp_issue_dates
where idCalendarSNAPCompleteDate is null

	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		ol.orderlineid_bk, ol.order_line_id_bk, 
		oli.batchstockissueid_bk, oli.issue_id, oli.allocation_type_bk, oli.allocation_record_type_bk, oli.cancelled_f, 
		olid.allocation_date, olid.issue_date
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_oli oli on ol.orderlineid_bk = oli.orderlineid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_dates olid on oli.batchstockissueid_bk = olid.batchstockissueid_bk

	-- where oli.orderlineid_bk is null
	-- where oli.cancelled_f = 'Y'
	order by o.order_no_erp

select *
from Landing.aux.alloc_order_line_erp_issue_measures
where local_to_global_rate <> 0


	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		ol.orderlineid_bk, ol.order_line_id_bk, 
		oli.batchstockissueid_bk, oli.issue_id, wsib.magentoProductID, wsib.name,
		-- oli.allocation_type_bk, oli.allocation_record_type_bk, oli.cancelled_f, 
		-- olid.allocation_date, olid.issue_date, 
		olim.qty_unit_order_line, olim.qty_unit, olim.qty_stock, olim.qty_percentage, olim.local_total_unit_cost, olim.local_total_cost, olim.local_to_global_rate, olim.order_currency_code
	from 
			Landing.aux.alloc_order_line_erp_ol ol
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_oli oli on ol.orderlineid_bk = oli.orderlineid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_dates olid on oli.batchstockissueid_bk = olid.batchstockissueid_bk
		left join
			Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
		left join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on olim.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
	-- where o.order_no_erp = 'INT00002286'
	order by o.order_no_erp

		select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
			ol.orderlineid_bk, olp.bom_f,
			oli.batchstockissueid_bk, oli.issue_id, oli.cancelled_f,
			olim.qty_unit_order_line, olim.qty_unit, olim.qty_stock, olim.qty_percentage, -- olim.local_total_unit_cost, olim.local_total_cost, 
			sum(qty_percentage) over (partition by ol.orderlineid_bk) sum_qty_percentage, 
			count(*) over (partition by ol.orderlineid_bk) rep_ol
			-- olim.local_to_global_rate, olim.order_currency_code
		from 
				Landing.aux.alloc_order_line_erp_ol ol
			inner join
				Landing.aux.alloc_order_header_erp_o o on ol.orderid_bk = o.orderid_bk
			inner join
				Landing.aux.alloc_order_line_erp_product olp on ol.orderlineid_bk = olp.orderlineid_bk
			inner join
				Landing.aux.alloc_order_line_erp_issue_oli oli on ol.orderlineid_bk = oli.orderlineid_bk
			inner join
				Landing.aux.alloc_order_line_erp_issue_dates olid on oli.batchstockissueid_bk = olid.batchstockissueid_bk
			inner join
				Landing.aux.alloc_order_line_erp_issue_measures olim on oli.batchstockissueid_bk = olim.batchstockissueid_bk
		where olim.qty_unit_order_line is not null
			and olp.bom_f = 'N' 
			-- and oli.cancelled_f = 'Y'
		-- order by rep_ol desc, o.order_no_erp
		order by sum_qty_percentage desc, o.order_no_erp


------------------------------------------------------------

select *
from Landing.aux.alloc_order_line_mag_erp_olm
where orderlineid_bk = 0

	select count(*), count(distinct orderlineid_bk), count(distinct orderlinemagentoid_bk)
	from Landing.aux.alloc_order_line_mag_erp_olm

	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		olm.orderlineid_bk, olm.order_line_id_bk
	from 
			Landing.aux.alloc_order_line_mag_erp_olm olm
		inner join
			Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.order_id_bk = o.order_id_bk
	order by o.order_no_erp

		select count(*), count(distinct orderlineid_bk), count(distinct orderlinemagentoid_bk), count(distinct ol.order_id_bk)
		from 
				Landing.aux.alloc_order_line_mag_erp_olm olm
			inner join
				Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
			inner join
				Landing.aux.alloc_order_header_erp_o o on ol.order_id_bk = o.order_id_bk

select *
from Landing.aux.alloc_order_line_mag_erp_dates
where idCalendarSNAPCompleteDate <> idCalendarExpectedShipmentDate

select *
from Landing.aux.alloc_order_line_mag_erp_measures

	select o.orderid_bk, o.order_no_erp, o.order_id_bk, 
		olm.orderlineid_bk, olm.order_line_id_bk, 
		
		olp.bom_f, p.magentoProductID, p.name, olmm.deduplicate_f, 

		olmm.qty_unit_mag, olmm.qty_unit_erp, olmm.qty_percentage, sum(olmm.qty_percentage) over (partition by olm.orderlineid_bk) sum_qty_percentage,
		olmm.local_total_cost_mag, olmm.local_total_cost_erp, 
		olmm.local_to_global_rate, olmm.order_currency_code
	from 
			Landing.aux.alloc_order_line_mag_erp_olm olm
		inner join
			Landing.aux.sales_fact_order_line_aud ol on olm.order_line_id_bk = ol.order_line_id_bk
		inner join
			Landing.aux.alloc_order_header_erp_o o on ol.order_id_bk = o.order_id_bk
		inner join
			Landing.aux.alloc_order_line_mag_erp_measures olmm on olm.order_line_id_bk = olmm.order_line_id_bk
		inner join
			Landing.aux.alloc_order_line_erp_product olp on olm.orderlineid_bk = olp.orderlineid_bk
		left join
			Landing.mend.gen_prod_product_v p on olp.productid_erp_bk = p.productid
	-- where o.order_id_bk = 7947611
	-- where olmm.qty_unit_erp is null
	where olp.bom_f = 'N' -- wrong qty_percentage
	-- order by o.order_no_erp
	order by sum_qty_percentage desc, o.order_no_erp
