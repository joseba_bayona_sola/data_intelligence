
select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog

select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog_aud

	select top 1000 year(createddate), month(createddate), count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by year(createddate), month(createddate)
	order by year(createddate), month(createddate)

		select top 1000 year(createddate), month(createddate), substring(reference, 1, 2), count(*)
		from Landing.mend.wh_warehousestockitemlog_aud
		where reference like 'SC%'
		group by year(createddate), month(createddate), substring(reference, 1, 2)
		order by year(createddate), month(createddate), substring(reference, 1, 2)

	select top 1000 substring(reference, 1, 2), count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by substring(reference, 1, 2)
	order by substring(reference, 1, 2)

	select top 1000 substring(reference, 1, 5), count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by substring(reference, 1, 5)
	order by substring(reference, 1, 5)

	select top 1000 warehouse, count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by warehouse
	order by warehouse

	select top 1000 warehousepreference, count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	where reference like 'SC%'
	group by warehousepreference
	order by warehousepreference

	select top 1000 requiredquantity, count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by requiredquantity
	order by requiredquantity

	select top 1000 packsizepreference, count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by packsizepreference
	order by packsizepreference

	select top 1000 success, count(*)
	from Landing.mend.wh_warehousestockitemlog_aud
	group by success
	order by success


select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog_aud_v

-------------------------

select *
from Landing.mend.wh_warehousestockitemlog
order by reference

select top 1000 *
from Landing.mend.wh_warehousestockitemlog_aud
where reference like 'SC%'

	select top 1000 *
	from Landing.mend.wh_warehousestockitemlog_aud
	where reference like 'SC%'

select *
from Landing.mend.wh_warehousestockitemlog_aud
where createddate between '2018-06-01' and '2018-07-01' and reference not like 'SC%'
-- where reference like 'SC_AE%'
order by reference desc

--------------------------------------------

select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog_warehousestockitem

select top 1000 count(*)
from Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud


select top 1000 *
from Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud

--------------------------------------------

select wsil.id, wsil.reference, wsil.warehouse, wsil.warehousepreference, wsil.createddate,
	wsi.warehousestockitemid, wsi.warehouse_name, wsi.magentoProductID, wsi.name, wsi.description, wsi.productid, wsi.sku, wsi.stockitemdescription,
	wsil.requiredquantity, wsil.packsizepreference, wsil.freetosell, wsil.success
from 
		Landing.mend.wh_warehousestockitemlog_aud wsil
	inner join
		Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud wsilwsi on wsil.id = wsilwsi.warehousestockitemlogid
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on wsilwsi.warehousestockitemid = wsi.warehousestockitemid
where reference in ('26000172748', '5001783892', '8004987901', '8004992353', '5002019017', '8005432732', '8004679440') 
order by reference, warehousepreference, description, packsizepreference


--------------------------------------------

select top 1000 
	count(*) over (), 
	wsil.id, wsilwsi.warehousestockitemid,
	wsil.reference, wsil.warehouse, wsil.warehousepreference, wsil.createddate, wsil.requiredquantity, wsil.packsizepreference, wsil.freetosell, wsil.success
from 
		Landing.mend.wh_warehousestockitemlog_aud wsil
	inner join
		Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud wsilwsi on wsil.id = wsilwsi.warehousestockitemlogid
where reference not like 'SC%'

	-- PK per Order No - WH Stock Item ID
	select top 1000 wsil.reference, wsilwsi.warehousestockitemid, count(*), count(*) over ()
	from 
			Landing.mend.wh_warehousestockitemlog_aud wsil
		inner join
			Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud wsilwsi on wsil.id = wsilwsi.warehousestockitemlogid
	where reference not like 'SC%'
	group by wsil.reference, wsilwsi.warehousestockitemid
	having count(*) > 1
	order by count(*) desc, wsil.reference, wsilwsi.warehousestockitemid

	select top 1000 wsil.reference, wsilwsi.warehousestockitemid, wsil.warehouse, count(*), count(*) over ()
	from 
			Landing.mend.wh_warehousestockitemlog_aud wsil
		inner join
			Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud wsilwsi on wsil.id = wsilwsi.warehousestockitemlogid
	where reference not like 'SC%'
	group by wsil.reference, wsilwsi.warehousestockitemid, wsil.warehouse
	having count(*) > 1
	order by count(*) desc, wsil.reference, wsilwsi.warehousestockitemid, wsil.warehouse

--------------------------------------------

-- PK: reference, warehousepreference, warehouse, productid, packsizepreference
select top 20000 count(*) over (partition by wsil.reference, wsil.warehousepreference, wsil.warehouse, wsi.productid, wsil.packsizepreference) num_rep,
	wsil.id, wsil.reference, wsil.warehouse, wsil.warehousepreference, wsil.createddate,
	wsi.warehousestockitemid, wsi.warehouse_name, wsi.magentoProductID, wsi.name, wsi.description, wsi.productid, wsi.sku, wsi.stockitemdescription,
	wsil.requiredquantity, wsil.packsizepreference, wsil.freetosell, wsil.success
from 
		Landing.mend.wh_warehousestockitemlog_aud wsil
	inner join
		Landing.mend.wh_warehousestockitemlog_warehousestockitem_aud wsilwsi on wsil.id = wsilwsi.warehousestockitemlogid
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on wsilwsi.warehousestockitemid = wsi.warehousestockitemid
where reference not like 'SC%'
order by num_rep desc, wsil.reference, wsil.warehousepreference, wsil.warehouse, wsi.productid, wsil.packsizepreference


-- MODELLING: STOCK CHECKER data