---Initial script for data modeling

---DIM freight Packing Method
SELECT 
	id					AS FreightPackingMethodid_bk, 
	[name], [description], packagingtype, 
	dimensionuom, weightuom,width, height, depth, maxweight, totalvolume
FROM mend.frcons_freightpackingmethod
GO

---DIM Freight Rate
SELECT 
	fr.id							AS freightrateid_bk,
	frf.supplierid					AS Supplierid_ff_bk,
	frws.supplierid					AS Supplierid_frws_bk,
	frtw.warehouseid				AS Warehouseid_to_bk,
	frp.freightpackingmethodid		AS freightpackingmethid_bk,
	frpm.name,
	 fr._type AS Freight_type,fr.freightmethod,fr.freightratereference, 
	 fr.startDate , fr.endDate,
	 fr.minQtyUoM, fr.maxQtyUoM, fr.transitLeadTime, fr.price, fr.currency, fr.createdDate
FROM [mend].[frcons_freightrate] fr
INNER JOIN [mend].[frcons_freightrate_freightforwarder] frf			ON fr.id = frf.freightrateid
INNER JOIN [mend].[frcons_freightrate_freightpackingmethod] frp		ON fr.id = frf.freightrateid
INNER JOIN mend.frcons_freightpackingmethod 	 frpm					ON frp.freightpackingmethodid = frpm.id
INNER JOIN [mend].[frcons_freightrate_fromwarehousesupplier] frws	ON fr.id = frws.freightrateid
INNER JOIN [mend].[frcons_freightrate_towarehouse] frtw				ON fr.id = frtw.freightrateid
GO

---DIM Freight Consignment 
SELECT
	fc.id					AS	freightconsignmentid_bk, 
	fcsw.warehouseid		AS	SourceWarehouseid_bk,
	fcfr.freightrateid		AS	freightrateid_bk,
	fcc.Accountid			AS	WHOperatorAccountCreateid_bk,
	fc.consignmentref, fc.consignmentid, fc.consignmenttype, fc.invoice,
	fc.estimatedPackingQuantity, fc.actualPackingQuantity, fc.numberOfReceipts,
	fc.quantity,fc.totalWeight, fc.totalVolume,
	fc.totalvalue_suppliercurrency,ssv.currencyCode AS Currency_code_supplier ,fc.supplierspotrate,fc.totalvalue_freightcurrency, fr.currency AS Currency_Code_Freight, fc.freightspotrate,
	fc.estimatedcarriagecost, fc.actualcarriagecost,
	fc.approveddate, fc.planneddate, fc.collectiondate, fc.requesteddeliverydate, fc.delivereddate,preparationcompletedate,completeddate
FROM mend.frcons_freightconsignment fc
INNER JOIN [mend].[frcons_freightconsignment_sourcewarehouse] fcsw	ON fc.id = fcsw.FreightConsignmentid
INNER JOIN [mend].[frcons_freightconsignment_freightrate] fcfr		ON fc.id = fcfr.FreightConsignmentid
INNER JOIN [mend].[frcons_freightconsignment_createdby] fcc			ON fc.id = fcc.FreightConsignmentid
INNER JOIN [mend].[frcons_freightconsignment_collectionsource] fccs	ON fc.id = fccs.FreightConsignmentid
INNER JOIN [mend].[gen_supp_supplier_v] ssv							ON ssv.supplier_id = fccs.Supplierid
INNER JOIN [mend].[frcons_freightrate] fr							ON fr.id = fcfr.freightrateid
WHERE fc.id = 220113431787732994


---DIM Freight Consingment Line
SELECT
	fch.id								AS FreightConsingmentheaderid_bk,
	fchc.freightconsignmentheaderid		AS FreightConsignmentid_bk,
	fchpz.packsizeid					AS ProductFamilyPacksizeid_bk,
	fch.quantity,fch.totalweight, fch.totalvolume,fch.width,fch.height,fch.depth,fch.weight,
	fch.totalestcarriagecost, fch.totalactualcarriagecost
FROM [mend].[frcons_freightconsignmentheader] fch	
INNER JOIN [mend].[frcons_freightconsignmentheader_freightconsignment] fchc	ON fch.id = fchc.freightconsignmentheaderid
INNER JOIN [mend].[frcons_freightconsignmentheader_packsize] fchpz			ON fch.id = fchpz.freightconsignmentheaderid
