
-- Freight Consignment Line

	-- total_weight - total_volume per line well calculated
	
	select *
	from
		(select idFreightConsignmentLine_sk, freightconsignmentheaderid_bk, consignment_ref, 
			product_id_magento, product_family_name, packsize, 
			total_weight, weight * quantity total_weight_calc_orig, convert(decimal(12, 0), weight * quantity) total_weight_calc,
			total_volume, width * height * depth * quantity total_volume_calc,
			quantity, width, height, depth, weight
		from Warehouse.frcons.fact_freight_consignment_line_v) fcl
	where total_weight <> total_weight_calc_orig
	-- where total_volume <> total_volume_calc
	order by freightconsignmentheaderid_bk

	--  total_estimated_carriage_cost - total_actual_carriage_cost well calculated (weighted values from Freight Consignment)
	select idFreightConsignmentLine_sk, freightconsignmentheaderid_bk, consignment_ref, 
		product_id_magento, product_family_name, packsize, 
		total_estimated_freight_cost, total_actual_freight_cost
	from Warehouse.frcons.fact_freight_consignment_line_v
	where freightconsignmentheaderid_bk = 222083756624709439
	order by freightconsignmentheaderid_bk

-- Freight Consignment

	-- quantity, total_weight - total_volume per consignment well calculated
	-- estimated_carriage_cost, actual_carriage_cost per consignment well calculated
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status, fc.auto_consigned, fc.freight_cost_exempt,
		fc.currency_code_supplier, fc.supplier_spot_rate, fc.currency_code_freight, fc.freight_spot_rate,
		-- fc.quantity, fcl.quantity_fcl, fc.total_weight, fcl.total_weight_fcl, fc.total_volume, fcl.total_volume_fcl, 
		-- fc.estimated_freight_cost, fcl.estimated_freight_cost_fcl, 
		fc.actual_freight_cost, fc.grand_total_supplier, fcl.actual_freight_cost_fcl_supplier, abs(fc.grand_total_supplier - fcl.actual_freight_cost_fcl_supplier)
		--, fc.actual_freight_cost * fc.supplier_spot_rate, fc.grand_total * fc.supplier_spot_rate
	from
			(select idFreightConsignment_sk, freightconsignmentid_bk, consignment_ref, consignment_status,
				auto_consigned, freight_cost_exempt,
				currency_code_supplier, supplier_spot_rate, currency_code_freight, freight_spot_rate,
				quantity, total_weight, total_volume, 
				estimated_freight_cost_supplier, actual_freight_cost, grand_total_supplier
			from Warehouse.frcons.dim_freight_consignment_v) fc
		left join
			(select idFreightConsignment_sk, consignment_ref, 
				sum(quantity) quantity_fcl, sum(total_weight) total_weight_fcl, sum(total_volume) total_volume_fcl, 
				sum(total_estimated_freight_cost_supplier) estimated_freight_cost_fcl_supplier, sum(total_actual_freight_cost_supplier) actual_freight_cost_fcl_supplier
			from Warehouse.frcons.fact_freight_consignment_line_v
			group by idFreightConsignment_sk, consignment_ref) fcl on fc.idFreightConsignment_sk = fcl.idFreightConsignment_sk
	-- where fcl.idFreightConsignment_sk is null
	-- where fc.quantity <> fcl.quantity_fcl
	-- where fc.total_weight <> fcl.total_weight_fcl
	-- where fc.total_volume <> fcl.total_volume_fcl

	-- where fc.estimated_freight_cost <> fcl.estimated_freight_cost_fcl
	where fc.grand_total_supplier <> fcl.actual_freight_cost_fcl_supplier -- and fc.consignment_ref = 'FC00001752'
	order by abs(fc.grand_total_supplier - fcl.actual_freight_cost_fcl_supplier) desc, fc.consignment_ref desc

	-- estimated_packing_quantity per consignment well calculated (total_weight, total_volume and freight_rate, freight_packing_method)
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status,
		fc.estimated_packing_quantity, fc.quantity, fc.total_weight, fc.total_volume, 
		fr.freight_packing_method_name, fr.freight_packing_type, fr.width, fr.height, fr.depth, fr.max_weight, fr.total_volume, 
		fc.total_volume / fr.total_volume, fc.total_weight / fr.max_weight, 
		case when (fc.total_volume / fr.total_volume > fc.total_weight / fr.max_weight) then ceiling(fc.total_volume / fr.total_volume) else ceiling(fc.total_weight / fr.max_weight) end
	from 
			Warehouse.frcons.dim_freight_consignment fc
		inner join
			(select fr.idFreightRate_sk, fr.freightrateid_bk, 
				fpm.idFreightPackingMethod_sk, fr.freight_packing_method_name, fr.freight_packing_type, fpm.width, fpm.height, fpm.depth, fpm.max_weight, fpm.total_volume
			from 
					Warehouse.frcons.dim_freight_rate_v fr
				inner join
					Warehouse.frcons.dim_freight_packing_method fpm on fr.idFreightPackingMethod_sk = fpm.idFreightPackingMethod_sk) fr on fc.idFreightRate_sk_fk = fr.idFreightRate_sk
	where fc.estimated_packing_quantity <> case when (fc.total_volume / fr.total_volume > fc.total_weight / fr.max_weight) then ceiling(fc.total_volume / fr.total_volume) else ceiling(fc.total_weight / fr.max_weight) end
	order by fc.consignment_ref

	-- estimated_carriage_cost per consignment well calculated (estimated_packing_quantity and freight_rate price)
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status,
		fc.freight_packing_method_name, fc.freight_packing_type, fc.freight_rate_name, fc.price,
		fc.estimated_packing_quantity, fc.estimated_freight_cost, 
		fc.price * fc.estimated_packing_quantity
	from Warehouse.frcons.dim_freight_consignment_v fc
	where fc.estimated_freight_cost <> fc.price * fc.estimated_packing_quantity

	-- difference between Estimated and Actual values (completed freight consignments)
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status, 
		fc.freight_packing_method_name, fc.price, -- fc.freight_packing_type, fc.freight_rate_name, 
		fc.estimated_packing_quantity, fc.estimated_freight_cost, 
		fc.actual_packing_quantity, fc.estimated_freight_cost
	from Warehouse.frcons.dim_freight_consignment_v fc
	order by freightconsignmentid_bk desc

	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status, 
		fc.approved_date, fc.planned_date, fc.collection_date, fc.completed_date,
		fc.freight_packing_method_name, fc.price, -- fc.freight_packing_type, fc.freight_rate_name, 
		fc.estimated_packing_quantity, fc.estimated_freight_cost, 
		fc.actual_packing_quantity, fc.actual_freight_cost
	from Warehouse.frcons.dim_freight_consignment_v fc
	-- where fc.completed_date is not null
	-- where fc.actual_carriage_cost <> 0
	where fc.estimated_packing_quantity <> fc.actual_packing_quantity
	order by fc.consignment_status, fc.consignment_ref

	----

	-- net_freight_cost
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status, 
		fc.approved_date, fc.planned_date, fc.collection_date, fc.completed_date, 
		fc.net_freight_cost, fc.advancement_fees + fc.demurrage + fc.duty + fc.actual_freight_cost + fc.fuel_cost + fc.other_charges net_freight_cost_calc, 
		fc.advancement_fees, fc.demurrage, fc.duty, fc.actual_freight_cost, fc.fuel_cost, fc.other_charges 

	from Warehouse.frcons.dim_freight_consignment_v fc
	where net_freight_cost <> advancement_fees + demurrage + duty + actual_freight_cost + fuel_cost + other_charges
	order by consignment_ref

	-- grand_total
	select fc.idFreightConsignment_sk, fc.freightconsignmentid_bk, fc.consignment_ref, fc.consignment_status, 
		fc.approved_date, fc.planned_date, fc.collection_date, fc.completed_date, 
		fc.grand_total, fc.net_freight_cost + fc.vat grand_total_calc,
		fc.net_freight_cost, fc.vat
	from Warehouse.frcons.dim_freight_consignment_v fc
	where fc.grand_total <> fc.net_freight_cost + fc.vat
	order by consignment_ref


---------------------------------------------------------------
---------------------------------------------------------------

select rfc.freightconsignmentid, rfc.receiptid, rlr.receiptlineid, 
	rl.estimatedfreightcost, 
	rl.actualfreightcost, 
	sum(rl.actualfreightcost) over (partition by rfc.freightconsignmentid),
	sum(rl.actualfreightcost) over (partition by rfc.receiptid),
	
	rl.invoiceUnitCost,
	rl.costavailableforinvoice, rl.recinvoicecarriagecost, rl.recinvoicecost, 
	rl.quantitymoved, rl.movedline
from 
		Landing.mend.frcons_receipt_freightconsignment rfc
	inner join
		Landing.mend.whship_receiptline_receipt rlr on rfc.receiptid = rlr.receiptid
	inner join
		Landing.mend.whship_receiptline rl on rlr.receiptlineid = rl.id
where rfc.freightconsignmentid = 220113431787739401
order by rfc.freightconsignmentid, rfc.receiptid, rlr.receiptlineid