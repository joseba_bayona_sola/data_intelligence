
select idFreightPackingMethod_sk, freightpackingmethodid_bk,
	name, description, packing_type,
	dimension_uom, weight_uom, width, height, depth, max_weight, total_volume,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.frcons.dim_freight_packing_method

	select name, count(*)
	from Warehouse.frcons.dim_freight_packing_method
	group by name
	order by name

	select description, count(*)
	from Warehouse.frcons.dim_freight_packing_method
	group by description
	order by description

	select packing_type, count(*)
	from Warehouse.frcons.dim_freight_packing_method
	group by packing_type
	order by packing_type

select idFreightRate_sk, freightrateid_bk, idFreightPackingMethod_sk,
	supplier_freight_forwarder_name, supplier_type_name, supplier_name, warehouse_name_to, 
	freight_packing_method_name, freight_packing_type, -- fpm.dimension_uom, fpm.weight_uom, fpm.height, fpm.depth, fpm.max_weight, fpm.total_volume,
	freight_rate_name, freight_type, freight_method, freight_reference,
	start_date, end_date,
	min_qty_UOM, max_qty_UOM, transit_lead_time, price,	currency_code_freight
from Warehouse.frcons.dim_freight_rate_v
where freight_rate_name = '4Care to York'

	select supplier_freight_forwarder_name, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by supplier_freight_forwarder_name
	order by supplier_freight_forwarder_name

	select supplier_type_name, supplier_name, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by supplier_type_name, supplier_name
	order by supplier_type_name, supplier_name

	select warehouse_name_to, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by warehouse_name_to
	order by warehouse_name_to

	select freight_packing_method_name, count(*) -- from dim_freight_packing_method
	from Warehouse.frcons.dim_freight_rate_v
	group by freight_packing_method_name
	order by freight_packing_method_name

	select freight_packing_type, count(*) -- from dim_freight_packing_method
	from Warehouse.frcons.dim_freight_rate_v
	group by freight_packing_type
	order by freight_packing_type

	select freight_rate_name, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by freight_rate_name
	order by freight_rate_name

		select freight_rate_name, supplier_name, warehouse_name_to, freight_reference, count(*)
		from Warehouse.frcons.dim_freight_rate_v
		group by freight_rate_name, supplier_name, warehouse_name_to, freight_reference
		order by freight_rate_name, supplier_name, warehouse_name_to, freight_reference

	select freight_type, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by freight_type
	order by freight_type

	select freight_method, count(*)
	from Warehouse.frcons.dim_freight_rate_v
	group by freight_method
	order by freight_method

select idFreightConsignment_sk, freightconsignmentid_bk, 
	consignment_ref, supplier_freight_forwarder_name, supplier_type_name, supplier_name, warehouse_name_from, warehouse_name_to, created_by,
	freight_packing_method_name, freight_packing_type, freight_rate_name, freight_type, freight_method, freight_reference, transit_lead_time, price,

	consignment_id, consignment_type, consignment_status, invoice_no,
	auto_consigned, freight_cost_exempt,
	estimated_packing_quantity, actual_packing_quantity, number_of_receipts,
	quantity, total_weight, total_volume, 
	total_value_supplier_currency, currency_code_supplier, supplier_spot_rate, total_value_freight_currency, currency_code_freight, freight_spot_rate,
	estimated_freight_cost, 
	net_freight_cost, advancement_fees, demurrage, duty, actual_freight_cost, fuel_cost, other_charges, vat, grand_total,
	-- created_date, 
	approved_date, planned_date, collection_date, requested_delivery_date, delivered_date, preparation_complete_date, completed_date
from Warehouse.frcons.dim_freight_consignment_v
where supplier_spot_rate = 0
order by consignment_status, consignment_ref

	select warehouse_name_from, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by warehouse_name_from
	order by warehouse_name_from

	select created_by, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by created_by
	order by created_by

	select consignment_type, freight_type, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by consignment_type, freight_type
	order by consignment_type, freight_type

	select consignment_status, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by consignment_status
	order by consignment_status

	select auto_consigned, freight_cost_exempt, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by auto_consigned, freight_cost_exempt
	order by auto_consigned, freight_cost_exempt

	select invoice_no, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	group by invoice_no
	order by invoice_no

	select currency_code_supplier, currency_code_freight, count(*)
	from Warehouse.frcons.dim_freight_consignment_v
	where (currency_code_supplier <> 'GBP' and currency_code_freight <> 'GBP') and currency_code_supplier <> currency_code_freight
	group by currency_code_supplier, currency_code_freight
	order by currency_code_supplier, currency_code_freight

		select currency_code_supplier, supplier_spot_rate, count(*)
		from Warehouse.frcons.dim_freight_consignment_v
		where supplier_spot_rate = 0 or (supplier_spot_rate = 1 and currency_code_supplier <> 'GBP') or (supplier_spot_rate <> 1 and currency_code_supplier = 'GBP')
		-- where not(supplier_spot_rate = 0 or (supplier_spot_rate = 1 and currency_code_supplier <> 'GBP') or (supplier_spot_rate <> 1 and currency_code_supplier = 'GBP')) 
		group by currency_code_supplier, supplier_spot_rate
		order by currency_code_supplier, supplier_spot_rate

		select currency_code_freight, freight_spot_rate, count(*)
		from Warehouse.frcons.dim_freight_consignment_v
		where freight_spot_rate = 0 or (freight_spot_rate = 1 and currency_code_freight <> 'GBP') or (freight_spot_rate <> 1 and currency_code_freight = 'GBP')
		-- where not(freight_spot_rate = 0 or (freight_spot_rate = 1 and currency_code_freight <> 'GBP') or (freight_spot_rate <> 1 and currency_code_freight = 'GBP')) 
		group by currency_code_freight, freight_spot_rate
		order by currency_code_freight, freight_spot_rate

		-- FC00002178: select 1/1.31258000 * 355925.55000000 * 1.18346000
		select freightconsignmentid_bk, 
			consignment_ref, supplier_freight_forwarder_name, supplier_type_name, supplier_name, warehouse_name_from, warehouse_name_to,
			consignment_type, consignment_status,
			auto_consigned, freight_cost_exempt,
			total_value_supplier_currency, currency_code_supplier, supplier_spot_rate, total_value_freight_currency, currency_code_freight, freight_spot_rate
		from Warehouse.frcons.dim_freight_consignment_v
		-- where supplier_spot_rate = 0
		where (currency_code_supplier <> 'GBP' and currency_code_freight <> 'GBP') and currency_code_supplier <> currency_code_freight
		order by freightconsignmentid_bk desc

select idFreightConsignment_sk, idFreightConsignmentLine_sk, freightconsignmentheaderid_bk, 

	consignment_ref, supplier_freight_forwarder_name, supplier_type_name, supplier_name, warehouse_name_from, warehouse_name_to, created_by,
	freight_packing_method_name, freight_packing_type, freight_rate_name, freight_type, freight_method, freight_reference, transit_lead_time, price,

	consignment_id, consignment_type, consignment_status, invoice_no,
	estimated_packing_quantity, actual_packing_quantity, number_of_receipts,
	quantity_fc, total_weight_fc, total_volume_fc, 

	-- created_date, 
	approved_date, planned_date, collection_date, requested_delivery_date, delivered_date, preparation_complete_date, completed_date,

	manufacturer_name, product_type_name, category_name, product_type_oh_name, product_family_group_name,
	product_id_magento, product_family_name, packsize, 
	quantity, total_weight,	total_volume, 
	width, height, depth, weight,
	total_estimated_freight_cost, total_actual_freight_cost
from Warehouse.frcons.fact_freight_consignment_line_v

	select manufacturer_name, product_type_name, category_name, product_type_oh_name, product_family_group_name,
		product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.frcons.fact_freight_consignment_line_v
	group by manufacturer_name, product_type_name, category_name, product_type_oh_name, product_family_group_name,
		product_id_magento, product_family_name, packsize
	order by manufacturer_name, product_type_name, category_name, product_type_oh_name, product_family_group_name,
		product_id_magento, product_family_name, packsize
