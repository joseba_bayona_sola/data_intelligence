
drop table #gen_frcons_freightrate
drop table #gen_frcons_freightconsignment

-- gen_frcons_freightpackingmethod -- packagingtype
select id, 
	name, description, packagingtype, dimensionuom, weightuom,
	width, height, depth, maxweight, totalvolume,
	system_changedby, createddate, changeddate
from Landing.mend.frcons_freightpackingmethod
order by packagingtype, name

-- gen_frcons_freightrate -- freightmethod, _type
select fr.id freightrateid, frff.supplierid supplierid_ff, frfws.supplierid supplierid_whsupp, frw.warehouseid,
	fr.supplyroute, s.supplierName suppFreightFowarder, s2.supplierName suppFromWHSupp, w.name to_warehouse_name, fpm.name freightPackagingMethod, -- s.supplierID, s2.supplierID, w.code, 
	fr.freightmethod, fr.freightratereference, fr._type, -- freightrateid, 
	fr.minqtyuom, fr.maxqtyuom, fr.transitleadtime, fr.price, fr.currency, fr.startdate, fr.enddate, 
	fr.createddate
into #gen_frcons_freightrate
from 
		Landing.mend.frcons_freightrate fr
	inner join
		Landing.mend.frcons_freightrate_freightforwarder frff on fr.id = frff.freightrateid
	inner join
		Landing.mend.gen_supp_supplier_v s on frff.supplierid = s.supplier_id
	inner join	
		Landing.mend.frcons_freightrate_fromwarehousesupplier frfws on fr.id = frfws.freightrateid
	inner join
		Landing.mend.gen_supp_supplier_v s2 on frfws.supplierid = s2.supplier_id
	inner join
		Landing.mend.frcons_freightrate_towarehouse frw on fr.id = frw.freightrateid
	inner join
		Landing.mend.gen_wh_warehouse_v w on frw.warehouseid = w.warehouseid
	inner join
		Landing.mend.frcons_freightrate_freightpackingmethod frfpm on fr.id = frfpm.freightrateid
	inner join
		Landing.mend.frcons_freightpackingmethod fpm on frfpm.freightpackingmethodid = fpm.id

-- order by fr._type, fr.freightmethod, fr.supplyroute
order by fr.id

	-- mend.frcons_freightrate_freightpackingmethod
	-- mend.frcons_freightrate_createdby

	select *
	from #gen_frcons_freightrate
	where freightrateid = 219550481834313281

-- gen_frcons_freightconsignment -- consignmenttype, consignmentstatus

select fc.id freightconsignmentid, fr.freightrateid, fr.supplierid_ff, fr.supplierid_whsupp, fcsw.warehouseid warehouseid_from, fr.warehouseid warehouseid_to, fccb.accountid,
	fc.consignmentref, fr._type, fr.suppFreightFowarder, fr.suppFromWHSupp, w.name from_warehouse_name, fr.to_warehouse_name, 
	fc.consignmentid, fc.consignmenttype, fc.invoice,
	fr.supplyroute, fr.freightPackagingMethod, fr.freightmethod,
	fc.consignmentstatus, 
	fc.estimatedPackingQuantity, fc.actualPackingQuantity, fc.numberOfReceipts, 
	fc.quantity, fc.totalCartons, fc.totalWeight, fc.totalVolume,
	fc.totalvalue_suppliercurrency, fc.totalvalue_freightcurrency, fc.supplierspotrate, fc.freightspotrate, 
	fc.estimatedcarriagecost, fc.actualcarriagecost, 
	fc.approveddate, fc.planneddate, fc.collectiondate, fc.requesteddeliverydate, fc.delivereddate, fc.receiptduedate, 
	fc.preparationcompletedate, fc.processingcompletedate, fc.completeddate, 
	fc.freightforwardernote, fc.auditComment, fc.attachmentsCount, fc.system_owner, fc.statusChangeHandlerRunning, fc.documenttype,
	fc.createdDate
into #gen_frcons_freightconsignment
from 
		Landing.mend.frcons_freightconsignment fc
	inner join
		Landing.mend.frcons_freightconsignment_freightrate fcfr on fc.id = fcfr.freightconsignmentid
	inner join
		#gen_frcons_freightrate fr on fcfr.freightrateid = fr.freightrateid
	inner join
		Landing.mend.frcons_freightconsignment_sourcewarehouse fcsw on fc.id = fcsw.freightconsignmentid 
	inner join
		Landing.mend.gen_wh_warehouse_v w on fcsw.warehouseid = w.warehouseid
	inner join
		Landing.mend.frcons_freightconsignment_createdby fccb on fc.id = fccb.freightconsignmentid
-- where fc.consignmentref = 'FC00000322'
order by fc.id desc

	-- mend.frcons_freightconsignment_freightforwarder // fcfr.supplierid_ff
	-- mend.frcons_freightconsignment_collectionsource // fcfr.supplierid_whsupp
	-- mend.frcons_freightconsignment_destinationwarehouse // fcfr.warehouseid

	select *
	from #gen_frcons_freightconsignment
	where consignmentref = 'FC00000322'

	-- mend.frcons_receipt_freightconsignment
	select count(*), count(distinct receiptid), count(distinct freightconsignmentid)
	from Landing.mend.frcons_receipt_freightconsignment

	select r.*, fc.*
	from 
			Landing.mend.gen_whship_receipt_v r
		inner join
			Landing.mend.frcons_receipt_freightconsignment rfc on r.receiptid = rfc.receiptid
		inner join 
			#gen_frcons_freightconsignment fc on rfc.freightconsignmentid = fc.freightconsignmentid
	where fc.consignmentref = 'FC00000312'


-- gen_frcons_freightconsignmentheader

select fch.id freightconsignmentheaderid, fc.freightconsignmentid, fc.freightrateid, fc.supplierid_ff, fc.supplierid_whsupp, fc.warehouseid_from, fc.warehouseid_to, fc.accountid, pfps.packsizeid,
	
	fc.consignmentref, fc._type, fc.suppFreightFowarder, fc.suppFromWHSupp, fc.from_warehouse_name, fc.to_warehouse_name, 
	fc.consignmentid, fc.consignmenttype, fc.invoice,
	fc.supplyroute, fc.freightPackagingMethod, fc.freightmethod,
	fc.consignmentstatus, 
		
	pfps.magentoProductID, pfps.name, pfps.size,

	fch.quantity, 
	fch.totalValue, fch.totalEstCarriageCost, fch.totalActualCarriageCost,
	fch.totalWeight, fch.totalVolume, 	
	fch.height, fch.width, fch.depth, fch.weight
from 
		Landing.mend.frcons_freightconsignmentheader fch
	inner join
		Landing.mend.frcons_freightconsignmentheader_freightconsignment fchfc on fch.id = fchfc.freightconsignmentheaderid
	inner join 
		#gen_frcons_freightconsignment fc on fchfc.freightconsignmentid = fc.freightconsignmentid
	inner join
		Landing.mend.frcons_freightconsignmentheader_packsize fchps on fch.id = fchps.freightconsignmentheaderid
	inner join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on fchps.packsizeid = pfps.packsizeid
where fc.consignmentref = 'FC00000322'

	-- mend.frcons_freightconsignmentheader_productfamily
	-- mend.frcons_freightconsignmentheader_packsize

	select *
	from Landing.mend.gen_prod_productfamilypacksize_v
