-- Freight Consignment Line

	-- total_weight - total_volume per line well calculated
	select *
	from
		(select freightconsignmentheaderid, consignmentref, 
			magentoProductID, name, size, totalWeight total_weight, totalVolume total_volume, 
			quantity, width, height, depth, weight, 
			weight * quantity total_weight_calc_orig, convert(decimal(12, 0), weight * quantity) total_weight_calc,
			width * height * depth * quantity total_volume_calc		
		from Landing.mend.gen_frcons_freightconsignmentheader_v) fcl
	where total_volume <> total_volume_calc
	-- where total_weight <> total_weight_calc_orig	
	order by freightconsignmentheaderid

	--  total_estimated_carriage_cost - total_actual_carriage_cost well calculated (weighted values from Freight Consignment)
	select freightconsignmentheaderid, consignmentref, 
		magentoProductID, name, size,
		totalEstCarriageCost total_estimated_carriage_cost, totalActualCarriageCost total_actual_carriage_cost
	from Landing.mend.gen_frcons_freightconsignmentheader_v
	where freightconsignmentheaderid = 222083756624709439
	order by freightconsignmentheaderid

-- Freight Consignment

	-- quantity, total_weight - total_volume per consignment well calculated

	select fc.freightconsignmentid, fc.consignmentref, fc.consignmentstatus,
		fc.quantity, fcl.quantity_fcl, 
		fc.total_weight, fcl.total_weight_fcl,
		fc.total_volume, fcl.total_volume_fcl
	from
			(select freightconsignmentid, consignmentref, consignmentstatus,
				quantity, totalWeight total_weight, totalVolume total_volume
			from Landing.mend.gen_frcons_freightconsignment_v) fc
		left join
			(select freightconsignmentid, consignmentref, 
				sum(quantity) quantity_fcl, sum(totalWeight) total_weight_fcl, sum(totalVolume) total_volume_fcl
			from Landing.mend.gen_frcons_freightconsignmentheader_v
			group by freightconsignmentid, consignmentref) fcl on fc.freightconsignmentid = fcl.freightconsignmentid
	-- where fcl.freightconsignmentid is null
	-- where fc.quantity <> fcl.quantity_fcl
	-- where fc.total_weight <> fcl.total_weight_fcl
	where fc.total_volume <> fcl.total_volume_fcl
	order by fc.consignmentref

	-- difference between Estimated and Actual values (completed freight consignments)
	select freightconsignmentid, consignmentref, consignmentstatus,
		freightPackagingMethod, 
		estimatedPackingQuantity, estimatedcarriagecost, 
		actualPackingQuantity, actualcarriagecost
	from Landing.mend.gen_frcons_freightconsignment_v
	order by freightconsignmentid