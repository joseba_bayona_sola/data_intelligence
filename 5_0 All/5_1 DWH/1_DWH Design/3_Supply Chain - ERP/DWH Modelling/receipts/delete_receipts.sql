
select count(*)
from Warehouse.rec.dim_receipt

select *
from Warehouse.rec.dim_receipt_v
where receiptid_bk in 
	(select rec.id
	from
			Landing.mend.whship_receipt_aud rec
		inner join
			(select id 
			from Landing.mend.whship_receipt_aud
			except 
			select id 
			from Landing.mend.whship_receipt) rec2 on rec.id = rec2.id)
order by created_date

------------------------------------------------------------------

select count(*)
from Warehouse.rec.fact_receipt_line_v

select *
from Warehouse.rec.fact_receipt_line_v
where receiptlineid_bk in 
	(select recl.id
	from
			Landing.mend.whship_receiptline_aud recl
		inner join
			(select id 
			from Landing.mend.whship_receiptline_aud
			except 
			select id 
			from Landing.mend.whship_receiptline) recl2 on recl.id = recl2.id)

