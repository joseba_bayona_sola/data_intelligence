
select top 1000 count(*) over (),
	idReceiptLine_sk, receiptlineid_bk, receipt_number, supplier_name,
	consignment_ref, auto_consigned, freight_cost_exempt, consignment_status, supplier_freight_forwarder_name,
	sku, 
	purchase_order_number, 
	purchase_unit_cost,
	purchase_rec_invoice_carriage_unit_cost, 
	purchase_actual_freight_unit_cost
from Warehouse.rec.fact_receipt_line_v 
-- where isnull(purchase_rec_invoice_carriage_unit_cost, 0) <> 0 -- 35590
-- where isnull(purchase_actual_freight_unit_cost, 0) <> 0 -- 44396
where isnull(purchase_rec_invoice_carriage_unit_cost, 0) <> 0 and isnull(purchase_actual_freight_unit_cost, 0) <> 0
order by receiptlineid_bk desc