
select *
from DW_GetLenses_jbs.dbo.erp_rec_ship
order by shipment_ID

	select count(*), count(distinct shipment_ID)
	from DW_GetLenses_jbs.dbo.erp_rec_ship


	select *
	from DW_GetLenses_jbs.dbo.erp_rec_ship
	where num_rep > 1
	order by shipment_ID

	select shipmentType, count(*)
	from DW_GetLenses_jbs.dbo.erp_rec_ship
	group by shipmentType
	order by shipmentType

-----------------------------------------------

select *
from DW_GetLenses_jbs.dbo.erp_rec_rec
order by createddate desc

	select count(*), count(distinct shipment_ID), count(distinct receipt_id), count(distinct orderNumber) 
	from DW_GetLenses_jbs.dbo.erp_rec_rec

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_rec
	where num_rep > 1
	order by shipment_ID desc, createdDate desc

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_rec
	where num_rep2 > 1
	order by receipt_ID

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_rec_rec
	group by status
	order by status

	select orderNumber, count(*)
	from DW_GetLenses_jbs.dbo.erp_rec_rec
	group by orderNumber
	order by count(*) desc

	

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_rec
	where orderNumber = 'Empty' and status = 'Stock_Registered'
	-- where orderNumber = 'Multiple'
	-- where orderNumber not in ('Empty', 'Multiple')
	order by createddate desc

		select orderNumber, status, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_rec
		where orderNumber in ('Empty', 'Multiple')
		group by orderNumber, status
		order by orderNumber, status

-----------------------------------------------

select *
from DW_GetLenses_jbs.dbo.erp_rec_reclh
order by createddate desc

	select count(*), count(distinct receiptlineheaderid), count(distinct receiptid),
		count(distinct shipment_ID), count(distinct receipt_id), count(distinct orderNumber) 
	from DW_GetLenses_jbs.dbo.erp_rec_reclh
	where receiptlineheaderid is null

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_reclh
	where receiptlineheaderid is null
	order by createddate desc

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_reclh
	-- where totalItemPrice <> sum_totalcostordered
	where totalItemPrice <> sum_totalcostaccepted
	order by createddate desc


	select rlh.*, pfps.magentoProductID, pfps.name, pfps.size
	from 
		DW_GetLenses_jbs.dbo.erp_rec_reclh rlh
	left join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on rlh.productfamilyid = pfps.productfamilyid and rlh.packsizeid = pfps.packsizeid
	order by createddate desc


-----------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_rec_recl
-- where receiptlineid = 74590868830395632
order by createddate desc
 
	select count(*), count(distinct receiptlineid), count(distinct receiptid), count(distinct purchaseorderlineid), count(distinct warehousestockitembatchid),
		count(distinct shipment_ID), count(distinct receipt_id), count(distinct orderNumber) 
	from DW_GetLenses_jbs.dbo.erp_rec_recl
	where receiptlineid is not null

		select receiptlineid, count(*), count(distinct warehousestockitembatchid)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by receiptlineid
		having count(*) > 1
			-- and count(*) <> count(distinct warehousestockitembatchid)
		order by count(*) desc, receiptlineid

		select top 1000 *
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		where status = 'In_Transit' -- Locked - Open - Planned - In_Transit - Arrived - Stock_Registered - Cancelled
		order by createddate desc

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_recl
	where purchaseorderlineid is null
	order by createddate desc

		select top 1000 purchaseorderlineid, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by purchaseorderlineid
		order by count(*) desc, purchaseorderlineid

	select *
	from DW_GetLenses_jbs.dbo.erp_rec_recl
	where warehousestockitembatchid is null
	order by createddate desc

		select top 1000 warehousestockitembatchid, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by warehousestockitembatchid
		order by count(*) desc, warehousestockitembatchid


	select top 1000 count(*) over (), *
	from DW_GetLenses_jbs.dbo.erp_rec_recl
	where totalItemPrice <> sum_totalItemPrice_a
	order by createddate desc


		select top 1000 status_rl, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by status_rl
		order by status_rl

		select top 1000 syncstatus, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by syncstatus
		order by syncstatus

		select top 1000 syncresolution, count(*)
		from DW_GetLenses_jbs.dbo.erp_rec_recl
		group by syncresolution
		order by syncresolution

------------------------------------------------------------------------------------

----- Purchase Order

select top 1000 pol.purchaseorderid, pol.ordernumber, pol.source, pol.status, pol.potype, pol.createddate, pol.quantity,
	rl.*
from 
		DW_GetLenses_jbs.dbo.erp_rec_recl rl
	left join
		DW_GetLenses_jbs.dbo.erp_po_pol pol on rl.purchaseorderlineid = pol.purchaseorderlineid
where receiptlineid = 74590868830395632
-- where rl.orderNumber in ('Empty', 'Multiple')
-- where pol.purchaseorderlineid is null
order by rl.createddate desc

----- Warehouse Stock Item Batch

select top 1000 wsib.magentoProductID, wsib.stockitemdescription, wsib.warehouse_name, wsib.batch_id, wsib.stockregistereddate, wsib.receivedquantity, wsib.productUnitCost,
	rl.*
from 
		DW_GetLenses_jbs.dbo.erp_rec_recl rl
	left join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on rl.warehousestockitembatchid = wsib.warehousestockitembatchid
where rl.receiptlineid = 74590868830395632
order by rl.createddate desc

