
-------------------------------- WH Shipment ----------------------------------

-- duplicated wh_shipment_number

select *
from
	(select count(*) over (partition by shipment_id) num_rep,
		shipment_ID, warehouseid, supplier_id, 
		warehouse_name, supplierName
	from 
		(select distinct shipment_ID, warehouseid, supplier_id, 
			warehouse_name, supplierName
		from Landing.mend.gen_whship_receipt_v) wsh) wsh
where num_rep > 1
order by shipment_id

select rec.*
from
		(select distinct shipment_ID
		from
			(select count(*) over (partition by shipment_id) num_rep,
				shipment_ID, warehouseid, supplier_id, 
				warehouse_name, supplierName
			from 
				(select distinct shipment_ID, warehouseid, supplier_id, 
					warehouse_name, supplierName
				from Landing.mend.gen_whship_receipt_v) wsh) wsh
		where num_rep > 1) wsh
	inner join
		Landing.mend.gen_whship_receipt_v rec on wsh.shipment_id = rec.shipment_id
order by rec.shipment_id, rec.warehouseid, rec.supplier_id, rec.receipt_id



-------------------------------- Receipt ----------------------------------

-- Physically Deleted Receipts
select rec.*
from
		Landing.mend.whship_receipt_aud rec
	inner join
		(select id 
		from Landing.mend.whship_receipt_aud
		except 
		select id 
		from Landing.mend.whship_receipt) rec2 on rec.id = rec2.id
order by rec.createddate desc

	select rec.status, count(*)
	from
			Landing.mend.whship_receipt_aud rec
		inner join
			(select id 
			from Landing.mend.whship_receipt_aud
			except 
			select id 
			from Landing.mend.whship_receipt) rec2 on rec.id = rec2.id
	group by rec.status
	order by rec.status

select whs.id receiptid, w.warehouseid, s.supplier_id, -- wsib_whs.warehousestockitembatchid,
	whs.shipmentID shipment_ID, whs.receiptID receipt_ID, whs.receiptNumber, whs.orderNumber, whs.invoiceRef,
	w.code, w.name warehouse_name, s.supplierID, s.supplierName,
	whs.status, whs.shipmentType, 
	whs.createdDate,
	whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
	whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
	whs.lock,
	whs.auditComment
from
		Landing.mend.whship_receipt_aud whs
	inner join
		(select id 
		from Landing.mend.whship_receipt_aud
		except 
		select id 
		from Landing.mend.whship_receipt) rec2 on whs.id = rec2.id
	inner join
		Landing.mend.whship_receipt_warehouse_aud whsw on whs.id = whsw.receiptid
	inner join
		Landing.mend.gen_wh_warehouse_v w on whsw.warehouseid = w.warehouseid
	inner join
		Landing.mend.whship_receipt_supplier_aud whss on whs.id = whss.receiptid
	inner join
		Landing.mend.gen_supp_supplier_v s on whss.supplierid = s.supplier_id
order by whs.createddate desc

	select t2.id,
		t2.receiptid,	
		t2.shipmentID shipment_ID, t2.receiptID receipt_ID, t2.receiptNumber, t2.orderNumber, 
		t2.status, t2.shipmentType, 
		t2.createdDate, -- t2.dueDate, 
		t2.arrivedDate, t2.confirmedDate, t2.stockRegisteredDate, 
		t3.id 
	from
			(select distinct rec.receiptid
			from
					Landing.mend.whship_receipt_aud rec
				inner join
					(select id 
					from Landing.mend.whship_receipt_aud
					except 
					select id 
					from Landing.mend.whship_receipt) rec2 on rec.id = rec2.id) t1
		inner join
			Landing.mend.whship_receipt_aud t2 on t1.receiptid = t2.receiptid	
		left join
			Landing.mend.whship_receipt t3 on t2.id = t3.id
	order by t2.receiptID, t2.createdDate

select *
from Landing.mend.whship_receipt_aud
where receiptid = 'P00042299-1'


-- Physically Deleted Receipt Lines

select 
	rec.receiptid,	
	rec.shipment_ID, rec.receipt_ID, rec.receiptNumber, rec.orderNumber, 
	rec.status, rec.shipmentType, 
	rec.createdDate,
	recl.*
from
		Landing.mend.whship_receiptline_aud recl
	inner join
		(select id 
		from Landing.mend.whship_receiptline_aud
		except 
		select id 
		from Landing.mend.whship_receiptline) recl2 on recl.id = recl2.id
	inner join
		Landing.mend.whship_receiptline_receipt_aud rlr on recl.id = rlr.receiptlineid
	inner join
		Landing.mend.gen_whship_receipt_v rec on rlr.receiptid = rec.receiptid
order by rec.createddate desc


-- duplicated receipt_number
select *
from
	(select count(*) over (partition by receipt_ID) num_rep, *
	from Landing.mend.gen_whship_receipt_v) po
where num_rep > 1
order by receipt_ID







-------------------------------- Receipt Line  ----------------------------------

-- Missing Child Records

	-- Rec without RecLH / RecL

	select rec.*
	from
			Landing.mend.gen_whship_receipt_v rec
		left join
			(select distinct receiptid
			from Landing.mend.gen_whship_receiptlineheader_v) reclh on rec.receiptid = reclh.receiptid
	where reclh.receiptid is null
	order by rec.createddate

	select rec.*
	from
			Landing.mend.gen_whship_receipt_v rec
		left join
			(select distinct receiptid
			from Landing.mend.gen_whship_receiptline_1_v) reclh on rec.receiptid = reclh.receiptid
	where reclh.receiptid is null
		-- and rec.status <> 'Cancelled'
		and rec.totalItemPrice <> 0
	order by rec.createddate desc

		select rec.status, count(*), count(distinct rec.receiptid)
		from
				Landing.mend.gen_whship_receipt_v rec
			left join
				(select distinct receiptid
				from Landing.mend.gen_whship_receiptline_1_v) reclh on rec.receiptid = reclh.receiptid
		where reclh.receiptid is null
		group by rec.status
		order by rec.status

	-- RecLH without RecL (Receipts with many RecLH where most RecLH have RecL but some No)
	select reclh.*
	from
			Landing.mend.gen_whship_receiptlineheader_v reclh
		left join
			(select distinct receiptlineheaderid
			from Landing.mend.gen_whship_receiptline_2_v) recl on reclh.receiptlineheaderid = recl.receiptlineheaderid
	where recl.receiptlineheaderid is null
	order by reclh.createdDate desc

		select reclh.status, count(*), count(distinct reclh.receiptid)
		from
				Landing.mend.gen_whship_receiptlineheader_v reclh
			left join
				(select distinct receiptlineheaderid
				from Landing.mend.gen_whship_receiptline_2_v) recl on reclh.receiptlineheaderid = recl.receiptlineheaderid
		where recl.receiptlineheaderid is null
		group by reclh.status
		order by reclh.status

		select reclh.*
		from
				Landing.mend.gen_whship_receiptlineheader_v reclh
			left join
				(select distinct receiptlineheaderid
				from Landing.mend.gen_whship_receiptline_2_v) recl on reclh.receiptlineheaderid = recl.receiptlineheaderid
			left join
				(select distinct rec.receiptid, receipt_id
				from
						Landing.mend.gen_whship_receipt_v rec
					left join
						(select distinct receiptid
						from Landing.mend.gen_whship_receiptline_1_v) reclh on rec.receiptid = reclh.receiptid
				where reclh.receiptid is null) rec on reclh.receiptid = rec.receiptid
		where recl.receiptlineheaderid is null and rec.receiptid is null
		order by reclh.createdDate desc

-- Missing Parent Records

	-- RecLH without Rec

	select reclh.*
	from 
			Landing.mend.whship_receiptlineheader reclh
		left join
			Landing.mend.whship_receiptlineheader_receipt reclhrec on reclh.id = reclhrec.receiptlineheaderid
		left join
			Landing.mend.whship_receipt rec on reclhrec.receiptid = rec.id
	where reclhrec.receiptlineheaderid is null or rec.id is null

	-- RecL without RecLH, Rec

	select rl.*
	from 
			Landing.mend.whship_receiptline rl
		left join
			Landing.mend.whship_receiptline_receipt rlrec on rl.id = rlrec.receiptlineid
		left join
			Landing.mend.whship_receipt rec on rlrec.receiptid = rec.id
	where rlrec.receiptlineid is null or rec.id is null

	select rl.*
	from 
			Landing.mend.whship_receiptline rl
		left join
			Landing.mend.whship_receiptline_receiptlineheader rlreclh on rl.id = rlreclh.receiptlineid
		left join
			Landing.mend.whship_receiptlineheader reclh on rlreclh.receiptlineheaderid = reclh.id
	where rlreclh.receiptlineid is null or reclh.id is null

-- Missing Relations - Stock Item

	select top 1000 *
	from Landing.mend.gen_whship_receiptline_1_v
	where stockitem is null
	order by createddate desc

		select top 1000 rl.stockitem, si.stockitemid, si.description
		from
				(select stockitem
				from Landing.mend.gen_whship_receiptline_1_v
				group by stockitem) rl
			left join
				Landing.mend.gen_prod_stockitem_v si on rl.stockitem = si.SKU
		where si.SKU is null

		select top 1000 rl.*
		from 
				Landing.mend.gen_whship_receiptline_1_v rl
			inner join
				(select top 1000 rl.stockitem, si.stockitemid, si.description
				from
						(select stockitem
						from Landing.mend.gen_whship_receiptline_1_v
						group by stockitem) rl
					left join
						Landing.mend.gen_prod_stockitem_v si on rl.stockitem = si.SKU
				where si.SKU is null) si on rl.stockitem = si.stockitem
		order by rl.receipt_id

-- Missing Relations - Purchase Order Line

	select receiptlineid, receiptid, 
		code, warehouse_name, supplierID, supplierName, 
		shipment_id, receipt_id, receiptnumber, ordernumber, status, shipmentType, 
		createdDate, totalItemPrice, 
		line, stockitem, status_rl, syncstatus, unitCost, quantityordered
	from Landing.mend.gen_whship_receiptline_1_v
	where purchaseorderlineid is null
	order by createddate desc

		select status, count(*), count(distinct receiptid), count(distinct receiptlineid)
		from Landing.mend.gen_whship_receiptline_1_v
		where purchaseorderlineid is null
		group by status
		order by status

-- WH Stock Item Batch Flag

	select top 1000 receiptlineid, receiptid, 
		code, warehouse_name, supplierID, supplierName, 
		shipment_id, receipt_id, receiptnumber, ordernumber, status, shipmentType, 
		createdDate, totalItemPrice, 
		line, stockitem, status_rl, syncstatus, unitCost, quantityordered, quantityaccepted
	from Landing.mend.gen_whship_receiptline_1_v
	where warehousestockitembatchid is null
		and status = 'Stock_Registered' and syncstatus = 'OK'
	order by createddate desc

		select count(*), count(distinct receiptid), count(distinct receiptlineid)
		from Landing.mend.gen_whship_receiptline_1_v
		where warehousestockitembatchid is null

		select status, count(*), count(distinct receiptid), count(distinct receiptlineid)
		from Landing.mend.gen_whship_receiptline_1_v
		where warehousestockitembatchid is null
		group by status
		order by status

		select syncstatus, count(*), count(distinct receiptid), count(distinct receiptlineid)
		from Landing.mend.gen_whship_receiptline_1_v
		where warehousestockitembatchid is null
		group by syncstatus
		order by syncstatus

		select status, syncstatus, count(*), count(distinct receiptid), count(distinct receiptlineid)
		from Landing.mend.gen_whship_receiptline_1_v
		where warehousestockitembatchid is null
		group by status, syncstatus
		order by status, syncstatus
