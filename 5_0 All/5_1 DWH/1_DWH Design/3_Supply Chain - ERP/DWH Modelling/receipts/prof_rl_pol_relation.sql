
select top 1000 *
from Warehouse.po.fact_purchase_order_line_v
order by purchaseorderlineid_bk desc

	select count(*)
	from Warehouse.po.fact_purchase_order_line_v

select top 1000 *
from Warehouse.rec.fact_receipt_line_v
order by receiptlineid_bk desc

	select count(*)
	from Warehouse.rec.fact_receipt_line_v

	select purchaseorderlineid_bk, count(*)
	from Warehouse.rec.fact_receipt_line_v
	group by purchaseorderlineid_bk
	having count(*) > 1
	order by count(*) desc

select top 1000 *
from Warehouse.tableau.fact_receipt_line_v
order by receiptlineid_bk desc

	select count(*)
	from Warehouse.tableau.fact_receipt_line_v
	where receiptlineid_bk is not null

	select count(*), count(distinct receiptlineid_bk)
	from Warehouse.tableau.stock_receipt_ds
	where receiptlineid_bk is not null

		select num_pol_rep, count(*)
		from Warehouse.tableau.stock_receipt_ds
		where receiptlineid_bk is not null
		group by num_pol_rep
		order by num_pol_rep
select purchaseorderlineid_bk, count(*) 
from Warehouse.tableau.fact_receipt_line_v
where qty_accepted > 0
group by purchaseorderlineid_bk
order by count(*) desc

select top 1000 num_pol_rep, receiptlineid_bk, purchaseorderlineid_bk, 
	wh_shipment_type_name, receipt_status_name, receipt_number, 
	created_date, stock_registered_date, due_date, 

	po_source_name, po_status_name, purchase_order_number, 
	created_date_po, due_date_po, 

	product_id_magento, product_family_name, packsize, sku, 

	qty_ordered, qty_received, qty_accepted, qty_open
from 
	(select count(*) over (partition by purchaseorderlineid_bk) num_pol_rep, *
	from Warehouse.tableau.fact_receipt_line_v
	where qty_accepted > 0
	) rl
where num_pol_rep > 1
order by purchaseorderlineid_bk desc, receiptlineid_bk desc

select top 1000 receiptlineid_bk, purchaseorderlineid_bk, 
	wh_shipment_type_name, receipt_status_name, receipt_number, 
	created_date, stock_registered_date, due_date, 

	po_source_name, po_status_name, purchase_order_number, 
	created_date_po, due_date_po, 

	product_id_magento, product_family_name, packsize, sku, 

	qty_ordered, qty_received, qty_accepted, qty_open
from Warehouse.tableau.fact_receipt_line_v
where purchaseorderlineid_bk in (4503599628265083, 4503599635592265)
order by purchaseorderlineid_bk desc, receiptlineid_bk desc

select top 1000 *
from Warehouse.po.fact_purchase_order_line_v
where purchaseorderlineid_bk in (4503599628265083, 4503599635592265)
order by purchaseorderlineid_bk desc
