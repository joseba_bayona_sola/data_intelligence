
drop table DW_GetLenses_jbs.dbo.erp_rec_ship
drop table DW_GetLenses_jbs.dbo.erp_rec_rec
drop table DW_GetLenses_jbs.dbo.erp_rec_reclh
drop table DW_GetLenses_jbs.dbo.erp_rec_recl

select distinct shipment_ID, shipmentType, 
	code, warehouse_name, supplierName, 
	count(*) over (partition by shipment_ID) num_rep
into DW_GetLenses_jbs.dbo.erp_rec_ship
from 
	(select distinct shipment_ID, shipmentType,
		code, warehouse_name, supplierName 
	from Landing.mend.gen_whship_receipt_v) t

select receiptid, 
	r.shipment_ID, r.shipmentType, r.code, r.warehouse_name, r.supplierName, s.num_rep,
	r.receipt_ID, r.receiptNumber, r.status, count(*) over (partition by receipt_ID) num_rep2,
	r.orderNumber,
	r.createdDate, r.dueDate, r.arrivedDate, r.confirmedDate, r.stockRegisteredDate, 
	r.totalItemPrice
into DW_GetLenses_jbs.dbo.erp_rec_rec
from	
		Landing.mend.gen_whship_receipt_v r
	inner join
		(select distinct shipment_ID, count(*) over (partition by shipment_ID) num_rep
		from 
			(select distinct shipment_ID, shipmentType,
				code, warehouse_name, supplierName 
			from Landing.mend.gen_whship_receipt_v) t) s on r.shipment_ID = s.shipment_ID		


select rlh.receiptlineheaderid, r.receiptid, rlh.productfamilyid, rlh.packsizeid,
	r.shipment_ID, r.shipmentType, r.code, r.warehouse_name, r.supplierName, r.num_rep,
	r.receipt_ID, r.receiptNumber, r.status, r.num_rep2,
	r.orderNumber,
	r.createdDate, 
	r.totalItemPrice, 
	rlh.totalquantityordered, rlh.totalquantityaccepted, 
	rlh.unitcost, rlh.totalcostordered, rlh.totalcostaccepted, 
	sum(rlh.totalcostordered) over (partition by r.receiptid) sum_totalcostordered,
	sum(rlh.totalcostaccepted) over (partition by r.receiptid) sum_totalcostaccepted
into DW_GetLenses_jbs.dbo.erp_rec_reclh
from
		DW_GetLenses_jbs.dbo.erp_rec_rec r
	left join
		Landing.mend.gen_whship_receiptlineheader_v rlh on r.receiptid = rlh.receiptid


select rl.receiptlineid, r.receiptid, rl.purchaseorderlineid, rl.warehousestockitembatchid,
	r.shipment_ID, r.shipmentType, r.code, r.warehouse_name, r.supplierName, r.num_rep,
	r.receipt_ID, r.receiptNumber, r.status, r.num_rep2,
	r.orderNumber,
	r.createdDate, 
	r.totalItemPrice, 
	rl.line, rl.stockitem, rl.status_rl, rl.syncstatus, rl.syncresolution,
	rl.unitCost, rl.quantityordered, rl.quantityreceived, rl.quantityaccepted, rl.quantityreturned, rl.quantityrejected, 
	rl.created, 
	sum(unitCost * quantityordered) over (partition by rl.receiptid) sum_totalItemPrice_o,
	sum(unitCost * quantityaccepted) over (partition by rl.receiptid) sum_totalItemPrice_a
into DW_GetLenses_jbs.dbo.erp_rec_recl
from
		DW_GetLenses_jbs.dbo.erp_rec_rec r
	left join
		Landing.mend.gen_whship_receiptline_1_v rl on r.receiptid = rl.receiptid

