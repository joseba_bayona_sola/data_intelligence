
select top 1000 product_id_magento, size, effective_date, expiry_date, active, leadtime, unitprice, 

	warehouse_name, pass, supplier_pass_price_date, supplier_name 
-- select *
from Warehouse.fm.dim_product_family_price_pass_v
where product_id_magento = '1083'
	and size = 30
	-- and supplier_pass_price_date = (select max(supplier_pass_price_date) from Warehouse.fm.dim_product_family_price_pass_v)
	and supplier_pass_price_date = '2019-07-30'
order by product_id_magento, size, warehouse_name, pass, supplier_pass_price_date

------------------------------------

select top 1000 purchaseorderlineid_bk, 
	idProductFamilyPrice_sk_fk,
	warehouse_name, supplier_name, po_source_name, po_type_name, purchase_order_number, leadtime, created_date, 
	product_id_magento, packsize, sku, qty_ordered, purchase_unit_cost
from Warehouse.po.fact_purchase_order_line_v
where product_id_magento = '1083'
	and packsize = 30
	and po_source_name = 'FuturMaster'
order by purchaseorderlineid_bk desc

-----------------------------------------------------------
-----------------------------------------------------------
-----------------------------------------------------------

-- Add in fm.dim_product_family_price_pass_v
	-- idProductFamilyPrice_sk_fk: for joining later
	-- latest_f: flag for saying Y: latest day pass info
select top 1000 *
from Warehouse.fm.dim_product_family_price_pass_v

-- Add in prod.dim_product_family_price_v
	-- york_pass, ams_pass, gir_pass: Info about latest pass data (from fm.dim_product_family_price_pass_v)
select top 1000 *
from Warehouse.prod.dim_product_family_price_v

	select count(*) over (partition by pvt.idProductFamilyPrice_sk_fk) num_rep, 
		pvt.idProductFamilyPrice_sk_fk,
		pvt.York york_pass, pvt.Amsterdam ams_pass, pvt.Girona gir_pass
	from 
		(select pfpp.idProductFamilyPrice_sk_fk, pfpp.warehouse_name, pfpp.pass, pfpp.latest_f
		from Warehouse.fm.dim_product_family_price_pass_v pfpp
		where latest_f = 1) v -- and pfpp.idProductFamilyPrice_sk_fk = 4597 
	pivot 
		(max(pass) for warehouse_name in (York, Amsterdam, Girona)) as pvt
	order by num_rep desc, pvt.idProductFamilyPrice_sk_fk

-- Add in po.fact_purchase_order_line_v
	-- pass_num: Info about the pass used when creating the po (from fm.dim_product_family_price_pass_v with idProductFamilyPrice_sk_fk, warehosue, created_date)
select top 1000 *
from Warehouse.po.fact_purchase_order_line_v

-- Add in tableau.stock_receipt_ds
	-- york_pass, ams_pass, gir_pass: Info about latest pass data (prod.dim_product_family_price_v)
	-- pass_num: Info about the pass used when creating the po (po.fact_purchase_order_line_v + fm.fact_planned_requirements_current_v)
select top 1000 *
from Warehouse.tableau.stock_receipt_ds

-------------------

select product_id_magento, product_family_name, packsize, count(*)
from Warehouse.po.fact_purchase_order_line_v
where po_source_name = 'FuturMaster'
	and created_date > '2019-08-01'
	and pass_num is null
group by product_id_magento, product_family_name, packsize
order by product_id_magento, product_family_name, packsize