
-- select top 1000 *
-- from Warehouse.po.fact_purchase_order_line_v
-- order by purchaseorderlineid_bk desc

-- 2110989
select top 1000 count(*) over () num_tot,
	polv.purchaseorderlineid_bk, 
	polv.warehouse_name, polv.supplier_name, polv.po_source_name, polv.po_type_name, polv.po_status_name, 
	polv.purchase_order_number, polv.leadtime,
	polv.created_date, polv.due_date, -- dateadd(dd, polv.leadtime, polv.created_date), DATEDIFF(dd, polv.created_date, polv.due_date),
	polv.product_id_magento, polv.product_family_name, polv.packsize, polv.sku, 
	polv.qty_ordered, polv.purchase_unit_cost, polv.purchase_line_cost, polv.currency_code, 

	pfp.supplier_name, pfp.lead_time, pfp.product_id_magento, pfp.size, 
	pfp.price_type_pf_name, pfp.unit_price, pfp.currency_code, 
	pfp.min_qty, pfp.max_qty, pfp.effective_date, pfp.expiry_date
from 
		Warehouse.po.fact_purchase_order_line_v polv
	inner join
		Warehouse.po.fact_purchase_order_line pol on polv.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
	left join
		Warehouse.prod.dim_product_family_price_v pfp on pol.idProductFamilyPrice_sk_fk = pfp.idProductfamilyPrice_sk
-- where pfp.idProductfamilyPrice_sk is null -- 1961
-- where polv.supplier_name <> pfp.supplier_name -- 4602
-- where polv.leadtime <> pfp.lead_time -- 1583528
-- where polv.product_id_magento <> pfp.product_id_magento -- 0
-- where polv.product_id_magento = pfp.product_id_magento and polv.packsize <> pfp.size -- 0
where polv.purchase_unit_cost <> pfp.unit_price -- 283
-- where polv.currency_code <> pfp.currency_code -- 283

-- where polv.created_date < pfp.effective_date -- 25437
-- where polv.created_date > pfp.expiry_date -- 57309
order by polv.purchaseorderlineid_bk desc

--select *
--from Warehouse.tableau.fact_receipt_line_v
--where purchaseorderlineid_bk = 4503599635476573
