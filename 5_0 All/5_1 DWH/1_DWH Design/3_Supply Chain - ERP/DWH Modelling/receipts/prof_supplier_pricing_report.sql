
-- prod.dim_product_family_price_v: Changes to make
	-- Add price_status column // Logic for best (best price after converting)
	-- Convert from Supplier Price to Global (Pounds) / Local (Warehouse) ?? - Which currency to use: Current Day + Option to Input

	-- Last: highlight price changes: Prices that are expired and have a new one active (in last month)

	-- Spot Price: As expired - Have a Price Type column too 


select *
from Warehouse.prod.dim_product_family_price_v
where product_id_magento = '1092'
	and supplier_name = '4Care'
order by size, supplier_name, effective_date, expiry_date

	drop table #dim_product_family_price_v

	select pfp.idProductFamilyPrice_sk, pfp.supplierpriceid_bk,
		pfp.idSupplier_sk, pfp.supplier_type_name, pfp.supplier_name,
		pfp.idProductFamilyPackSize_sk, pfp.product_id_magento, 
		pfp.manufacturer_name, pfp.brand_name, pfp.product_type_name, pfp.category_name, pfp.product_type_oh_name, pfp.product_family_group_name,
		pfp.product_family_name, pfp.size,
		pfp.idPriceTypePF_sk, pfp.price_type_pf_name, pfp.price_type,
		case when (pfp.ord_gbp_unit_price = 1) then 'Best' else pfp.price_status end price_status,
		pfp.unit_price, pfp.currency_code, 
		pfp.gbp_unit_price, pfp.eur_unit_price, pfp.usd_unit_price,
		pfp.lead_time, pfp.min_qty, pfp.max_qty, pfp.package_no, 
		pfp.effective_date, pfp.expiry_date, pfp.active, pfp.last_updated, pfp.direct, pfp.auto_edi
	from
		(select pfp.idProductFamilyPrice_sk, pfp.supplierpriceid_bk,
			s.idSupplier_sk, s.supplier_type_name, s.supplier_name,
			pfps.idProductFamilyPackSize_sk, pfps.product_id_magento, 
			pfps.manufacturer_name, pfps.brand_name, pfps.product_type_name, pfps.category_name, pfps.product_type_oh_name, pfps.product_family_group_name,
			pfps.product_family_name, pfps.size,
			pt.idPriceTypePF_sk, pt.price_type_pf_name, 
				CASE 
					WHEN pt.price_type_pf_name = 'Suppliers.StandardPrice' THEN 'Standard' 
					WHEN pt.price_type_pf_name = 'Suppliers.SpotPrice' THEN 'Spot' 
				END AS price_type,
			case
				when pt.price_type_pf_name = 'Suppliers.StandardPrice' AND pfp.active = 1 THEN 'Active'
				when pt.price_type_pf_name = 'Suppliers.StandardPrice' AND pfp.active = 0 and pfp.expiry_date between getutcdate() - 30 and getutcdate() THEN 'Last'
				else 'Expired'
			end price_status,
			pfp.unit_price, pfp.currency_code, 
			pfp.unit_price * ce_gbp.exchange_rate gbp_unit_price,
			pfp.unit_price * ce_eur.exchange_rate eur_unit_price,
			pfp.unit_price * ce_usd.exchange_rate usd_unit_price,
			case when pt.price_type_pf_name = 'Suppliers.StandardPrice' AND pfp.active = 1 and pfp.unit_price > 0.1 then 
				dense_rank() over (partition by CASE WHEN pfp.active = 1 and pfp.unit_price > 0.1 THEN 1 END, product_id_magento, pfps.size order by pfp.unit_price * ce_gbp.exchange_rate asc) else null end ord_gbp_unit_price,
			pfp.lead_time, pfp.min_qty, pfp.max_qty, pfp.package_no, 
			pfp.effective_date, pfp.expiry_date, pfp.active, pfp.last_updated, pfp.direct, pfp.auto_edi
		-- into #dim_product_family_price_v
		from 
				Warehouse.prod.dim_product_family_price pfp
			inner join
				Warehouse.prod.dim_product_family_pack_size_v pfps on pfp.idProductFamilyPackSize_sk_fk = pfps.idProductFamilyPackSize_sk
			inner join
				Warehouse.gen.dim_supplier_v s on pfp.idSupplier_sk_fk = s.idSupplier_sk
			inner join
				Warehouse.prod.dim_price_type_pf pt on pfp.idPriceTypePF_sk_fk = pt.idPriceTypePF_sk
			left join
				Landing.mend.gen_comp_currency_exchange_v ce_gbp ON pfp.currency_code = ce_gbp.currencycode_from and ce_gbp.currencycode_to = 'GBP' and ce_gbp.nexteffectivedate = CAST(GETDATE() AS DATE)
			left join
				Landing.mend.gen_comp_currency_exchange_v ce_eur ON pfp.currency_code = ce_eur.currencycode_from and ce_eur.currencycode_to = 'EUR' and ce_eur.nexteffectivedate = CAST(GETDATE() AS DATE)
			left join
				Landing.mend.gen_comp_currency_exchange_v ce_usd ON pfp.currency_code = ce_usd.currencycode_from and ce_usd.currencycode_to = 'USD' and ce_usd.nexteffectivedate = CAST(GETDATE() AS DATE)
		) pfp

	order by supplier_name, size, active, expiry_date

	select supplier_name, product_id_magento, product_family_name, size, price_type, price_status, 
		unit_price, currency_code, gbp_unit_price, ord_gbp_unit_price,
		effective_date, expiry_date, active
	from #dim_product_family_price_v
	where price_type = 'Standard' 
		-- and price_status = 'Active'
		and price_status = 'Last'
		-- and ord_gbp_unit_price = 1
	-- order by supplier_name, size, active, expiry_date
	order by product_id_magento, product_family_name, size, gbp_unit_price
	-- order by gbp_unit_price, product_id_magento, product_family_name, size

select pfp.idProductFamilyPrice_sk,
	manufacturer_name, product_id_magento, product_family_name, size,
	supplier_type_name, supplier_name, lead_time, currency_code, -- Date to use for Exchange Rate to use
	unit_price, effective_date, expiry_date, active, price_type_pf_name, 
	min_qty, max_qty, 
	case 
		when (price_type_pf_name = 'Suppliers.SpotPrice') then 'Spot Price'
		when (active = 0) then 'Expired'
		when (active = 1) then 'Active'
		-- when (XXXXX) then 'Best' // define logic for best
	end price_status, 
	pol.num_po, pol.num_po_lines, pol.sum_po_lines_qty, pol.avg_po_lines_price, pol.sum_po_lines_price, 
	rl.num_rec, rl.num_rec_lines, rl.sum_rec_lines_qty, rl.avg_rec_lines_price, rl.sum_rec_lines_price

from 
		Warehouse.prod.dim_product_family_price_v pfp
	left join
		(select pol.idProductFamilyPrice_sk_fk idProductfamilyPrice_sk, 
			count(distinct idPurchaseOrder_sk) num_po, count(*) num_po_lines, sum(qty_ordered) sum_po_lines_qty, avg(purchase_unit_cost) avg_po_lines_price, sum(purchase_line_cost) sum_po_lines_price
		from 
				Warehouse.po.fact_purchase_order_line_v polv
			inner join
				Warehouse.po.fact_purchase_order_line pol on polv.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
		where polv.product_id_magento = '1092'
		group by pol.idProductFamilyPrice_sk_fk) pol on pfp.idProductFamilyPrice_sk = pol.idProductfamilyPrice_sk
	left join
		(select pol.idProductFamilyPrice_sk_fk idProductfamilyPrice_sk, 
			count(distinct idReceipt_sk) num_rec, count(*) num_rec_lines, sum(rl.qty_ordered) sum_rec_lines_qty, avg(rl.purchase_unit_cost) avg_rec_lines_price, sum(rl.purchase_total_cost) sum_rec_lines_price
		from 
				Warehouse.po.fact_purchase_order_line_v polv
			inner join
				Warehouse.po.fact_purchase_order_line pol on polv.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
			inner join
				Warehouse.rec.fact_receipt_line_v rl on polv.idPurchaseOrderLine_sk = rl.idPurchaseOrderLine_sk
		where rl.product_id_magento = '1092'
		group by pol.idProductFamilyPrice_sk_fk) rl on pfp.idProductFamilyPrice_sk = rl.idProductfamilyPrice_sk
where pfp.product_id_magento = '1092'
	-- and price_type_pf_name <> 'Suppliers.SpotPrice' // Ask what to do with Spot Prices (Add/exclude from report)
	-- and unit_price <> avg_po_lines_price
	-- and unit_price <> avg_rec_lines_price
order by price_status, size, supplier_name, effective_date, expiry_date


--------------------------------------------------
--------------------------------------------------

-- pol.fact_purchase_order_line_v: Changes to make
	-- Add idProductfamilyPrice_sk so we can use for join // Inside view join with dim_product_family_price_v + return wanted attributes

select count(*) over () num_tot,
	polv.purchaseorderlineid_bk, 
	polv.warehouse_name, polv.supplier_name, polv.po_source_name, polv.po_type_name, polv.po_status_name, 
	polv.purchase_order_number, polv.leadtime,
	polv.created_date, polv.due_date, -- dateadd(dd, polv.leadtime, polv.created_date), DATEDIFF(dd, polv.created_date, polv.due_date),
	polv.product_id_magento, polv.product_family_name, polv.packsize, polv.sku, 
	polv.qty_ordered, polv.purchase_unit_cost, polv.purchase_line_cost, polv.currency_code, 

	pfp.idProductfamilyPrice_sk,
	pfp.manufacturer_name, pfp.product_id_magento, pfp.product_family_name, pfp.size,
	pfp.supplier_type_name, pfp.supplier_name, pfp.lead_time, pfp.currency_code, 
	pfp.unit_price, pfp.effective_date, pfp.expiry_date, pfp.active, pfp.price_type_pf_name, 
	pfp.min_qty, pfp.max_qty, 
	pfp.price_status
from 
		Warehouse.po.fact_purchase_order_line_v polv
	inner join
		Warehouse.po.fact_purchase_order_line pol on polv.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
	left join
		(select *, 
			case 
				when (price_type_pf_name = 'Suppliers.SpotPrice') then 'Spot Price'
				when (active = 0) then 'Expired'
				when (active = 1) then 'Active'
				-- when (XXXXX) then 'Best' // define logic for best
			end price_status
		from Warehouse.prod.dim_product_family_price_v) pfp on pol.idProductFamilyPrice_sk_fk = pfp.idProductfamilyPrice_sk
where polv.product_id_magento = '1092'
order by polv.purchaseorderlineid_bk desc


--------------------------------------------------
--------------------------------------------------

-- rec.fact_receipt_line_v: Changes to make
	-- Add dim_product_family_price_v attributes from POL view
	-- Distinguish correctly between Received (or Accepted) vs Due (or Open): With current logic is OK (New attributes in ERP) // The problems is having the right qty_ordered
	-- In Report: What to do about diff prices between PFP and REC (Should not be there since 2019)

	-- Questions: 
		-- What about Still not RL (PO Not confirmed) // REC Status = Cancelled
		-- What RL prices: Only used for Revenue vs RL Cost vs RL Inv Cost vs Inv Rec Cost // Date to use for conversion

select count(*) over () num_tot, count(*) over (partition by polv.purchaseorderlineid_bk) num_rep,
	polv.purchaseorderlineid_bk, 
	polv.warehouse_name, polv.supplier_name, polv.po_source_name, polv.po_type_name, polv.po_status_name, 
	polv.purchase_order_number, polv.leadtime,
	polv.created_date, polv.due_date, -- dateadd(dd, polv.leadtime, polv.created_date), DATEDIFF(dd, polv.created_date, polv.due_date),
	polv.product_id_magento, polv.product_family_name, polv.packsize, polv.sku, 
	polv.qty_ordered, polv.qty_received, polv.purchase_unit_cost, polv.purchase_line_cost, polv.currency_code, 

	pfp.idProductfamilyPrice_sk,
	pfp.manufacturer_name, pfp.product_id_magento, pfp.product_family_name, pfp.size,
	pfp.supplier_type_name, pfp.supplier_name, pfp.lead_time, pfp.currency_code, 
	pfp.unit_price, pfp.effective_date, pfp.expiry_date, pfp.active, pfp.price_type_pf_name, 
	pfp.min_qty, pfp.max_qty, 
	pfp.price_status, 

	rl.receiptlineid_bk, rl.wh_shipment_type_name, rl.receipt_status_name, rl.receipt_number, 
	rl.created_date, rl.arrived_date, rl.confirmed_date, rl.stock_registered_date, rl.due_date,
	rl.receipt_line_status_name, rl.receipt_line_sync_status_name, 

	rl.qty_ordered, rl.qty_received, rl.qty_accepted, 
	case 
		when rl.receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then rl.qty_ordered
		when rl.receipt_status_name in ('Received') then rl.qty_received
		else 0 -- Stock_Registered
	end as qty_open,

	rl.purchase_unit_cost, rl.purchase_total_cost, rl.purchase_total_cost_accepted
from 
		Warehouse.po.fact_purchase_order_line_v polv
	inner join
		Warehouse.po.fact_purchase_order_line pol on polv.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
	left join
		(select *, 
			case 
				when (price_type_pf_name = 'Suppliers.SpotPrice') then 'Spot Price'
				when (active = 0) then 'Expired'
				when (active = 1) then 'Active'
				-- when (XXXXX) then 'Best' // define logic for best
			end price_status
		from Warehouse.prod.dim_product_family_price_v) pfp on pol.idProductFamilyPrice_sk_fk = pfp.idProductfamilyPrice_sk
	left join
		Warehouse.rec.fact_receipt_line_v rl on polv.idPurchaseOrderLine_sk = rl.idPurchaseOrderLine_sk
where polv.product_id_magento = '1092'
-- where rl.product_id_magento = '1092'
	-- and polv.qty_ordered <> rl.qty_ordered
-- order by polv.purchaseorderlineid_bk desc
order by num_rep desc, polv.purchaseorderlineid_bk desc, rl.receiptlineid_bk desc

