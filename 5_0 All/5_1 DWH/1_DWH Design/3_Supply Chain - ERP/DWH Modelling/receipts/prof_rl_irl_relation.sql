
select top 1000 receiptlineid_bk, receiptlineid_bk, receipt_number, product_id_magento, 
	qty_ordered, qty_received, qty_accepted
from Warehouse.rec.fact_receipt_line_v
where qty_received <> qty_accepted

select top 1000 receiptlineid_bk, receiptlineid_bk, receipt_number, product_id_magento, 
	qty_ordered, qty_received, qty_accepted, qty_open
from Warehouse.tableau.fact_receipt_line_v
where receiptlineid_bk = 74590868831837878

select *
into #irl
from 
	(select count(*) over (partition by idReceiptLine_sk_fk) num_rep, 
		invoicelineid_bk, idReceiptLine_sk_fk, 
		invrec_status_name, invoice_ref, net_suite_no, invoice_date, posted_date, 
		quantity, 
		adjusted_unit_cost, adjusted_total_cost, adjustment_applied
		-- , local_unit_cost, local_total_cost, global_unit_cost, global_total_cost
	from Warehouse.invrec.fact_invoice_reconciliation_line_v) irl
where num_rep > 1

select num_rep,
	rlv.receiptlineid_bk, rlv.receipt_number, rlv.product_id_magento, rlv.sku,
	rlv.qty_ordered, rlv.qty_received, rlv.qty_accepted, rlv.qty_open, 
	rlv.purchase_unit_cost, rlv.purchase_total_cost, rlv.purchase_total_cost_accepted, 

	irl.invoicelineid_bk, 
	irl.invrec_status_name, irl.invoice_ref, irl.net_suite_no, irl.invoice_date, irl.posted_date, 
	irl.quantity, 
	irl.adjusted_unit_cost, irl.adjusted_total_cost, irl.adjustment_applied
from 
		#irl irl
	inner join
		Warehouse.rec.fact_receipt_line rl on irl.idReceiptLine_sk_fk = rl.idReceiptLine_sk
	inner join
		Warehouse.tableau.fact_receipt_line_v rlv on rl.receiptlineid_bk = rlv.receiptlineid_bk
order by num_rep desc, rlv.receiptlineid_bk

---------------------------------------------------------

select top 10000 rlv.receiptlineid_bk, rlv.receipt_number, rlv.receipt_status_name, rlv.created_date,
	rlv.product_id_magento, rlv.sku,
	rlv.qty_ordered, rlv.qty_received, rlv.qty_accepted, rlv.qty_open, 
	rlv.purchase_unit_cost, rlv.purchase_total_cost, rlv.purchase_total_cost_accepted, 

	irl.invoicelineid_bk, 
	irl.invrec_status_name, irl.invoice_ref, irl.net_suite_no, irl.invoice_date, irl.posted_date, 
	irl.quantity, 
	irl.adjusted_unit_cost, irl.adjusted_total_cost, irl.adjustment_applied
from 
		Warehouse.tableau.fact_receipt_line_v rlv 
	inner join	
		Warehouse.rec.fact_receipt_line rl on rlv.receiptlineid_bk = rl.receiptlineid_bk
	left join
		(select count(*) over (partition by idReceiptLine_sk_fk) num_rep, 
			invoicelineid_bk, idReceiptLine_sk_fk, 
			invrec_status_name, invoice_ref, net_suite_no, invoice_date, posted_date, 
			quantity, 
			adjusted_unit_cost, adjusted_total_cost, adjustment_applied
			-- , local_unit_cost, local_total_cost, global_unit_cost, global_total_cost
		from Warehouse.invrec.fact_invoice_reconciliation_line_v) irl on rl.idReceiptLine_sk = irl.idReceiptLine_sk_fk
where irl.invoicelineid_bk is not null
order by rlv.receiptlineid_bk desc


---------------------------------------------------------

	select recl.idReceiptLine_sk,
		recl.receiptlineid_bk, pol.purchaseorderlineid_bk, irl.invoicelineid_bk,
		recl.warehouse_name, recl.supplier_type_name, recl.supplier_name, pfp.supplier_type_name supplier_type_name_price, pfp.supplier_name supplier_name_price, 
		recl.wh_shipment_type_name, recl.receipt_status_name, recl.shipment_number, recl.receipt_number, recl.supplier_advice_ref, 
		recl.created_date, recl.arrived_date, recl.confirmed_date, recl.stock_registered_date, recl.due_date, 

		pfp.price_type, pfp.price_status, pfp.effective_date, pfp.expiry_date, pfp.unit_price, pfp.currency_code AS Unit_price_Currency_code, pfp.gbp_unit_price, pfp.eur_unit_price, pfp.usd_unit_price,
		pfp.gbp_to_eur_rate, pfp.gbp_to_usd_rate, pfp.gbp_to_sek_rate, pfp.eur_to_usd_rate, pfp.eur_to_sek_rate, 

		pol.po_source_name, pol.po_type_name, pol.po_status_name, 
		pol.purchase_order_number, pol.supplier_reference, 
		pol.created_by, pol.assigned_to, 
		pol.created_date created_date_po, pol.approved_date approved_date_po, pol.confirmed_date confirmed_date_po, 
		pol.submitted_date submitted_date_po, pol.completed_date completed_date_po, pol.due_date due_date_po, -- pol.received_date, 
		it_spo.purchase_order_number_t, 
		it_tpo.order_no_erp order_no_iso_tpo, it_spo.order_no_erp order_no_iso_spo, it_tpo.wholesale_customer_name tpo_customer_name, it_spo.wholesale_customer_name spo_customer_name,
		irl.invrec_status_name, irl.created_by invoice_rec_by, irl.invoice_ref, irl.net_suite_no, 
		irl.invoice_date, irl.approved_date, irl.posted_date, irl.payment_due_date, 

		isnull(recl.manufacturer_name, pfp.manufacturer_name) manufacturer_name, isnull(recl.product_type_name, pfp.product_type_name) product_type_name, 
		isnull(recl.category_name, pfp.category_name) category_name, isnull(recl.product_family_group_name, pfp.product_family_group_name) product_family_group_name,
		isnull(recl.product_id_magento, pfp.product_id_magento) product_id_magento, isnull(recl.product_family_name, pfp.product_family_name) product_family_name, isnull(recl.packsize, pfp.size) packsize,
		recl.product_description, recl.stock_item_description, recl.SKU,

		recl.receipt_line_sync_status_name, 
		count(*) over (partition by pol.purchaseorderlineid_bk) num_pol_rep, dense_rank() over (partition by pol.purchaseorderlineid_bk order by recl.receiptlineid_bk, irl.invoicelineid_bk) ord_pol_rep, 
		pol.qty_ordered qty_ordered_po,
		-- recl.qty_ordered, 
		recl.qty_received, recl.qty_accepted, 
		case 
			when recl.receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then recl.qty_ordered
			when recl.receipt_status_name in ('Received') then recl.qty_received
			else 0 -- Stock_Registered
		end as qty_open,
		irl.quantity, 

		recl.purchase_unit_cost, -- recl.purchase_total_cost, recl.purchase_total_cost_accepted, 
		recl.local_unit_cost, -- recl.local_total_cost, recl.local_total_cost_accepted, 
		recl.global_unit_cost, -- recl.global_total_cost, recl.global_total_cost_accepted, 
		
		irl.adjusted_unit_cost, irl.local_adjusted_unit_cost, irl.global_adjusted_unit_cost, 
		irl.adjustment_applied, irl.local_adjustment_applied, irl.global_adjustment_applied, 

		recl.local_to_wh_rate, recl.local_to_global_rate, 
		recl.currency_code, recl.currency_code_wh
	from 
			Warehouse.rec.fact_receipt_line_v recl 
		left join
			Warehouse.po.fact_purchase_order_line_v pol on recl.idPurchaseOrderLine_sk = pol.idPurchaseOrderLine_sk
		full join
			Warehouse.prod.dim_product_family_price_v pfp ON pfp.idProductFamilyPrice_sk = recl.idproductfamilyprice_sk_fk
		left join
			Warehouse.po.dim_intersite_transfer_v it_tpo on pol.idPurchaseOrder_sk = it_tpo.idPurchaseOrderT_sk_fk
		left join
			Warehouse.po.dim_intersite_transfer_v it_spo on pol.idPurchaseOrder_sk = it_spo.idPurchaseOrderS_sk_fk
		left join
			(select count(*) over (partition by idReceiptLine_sk_fk) num_rl_rep, invoicelineid_bk, idReceiptLine_sk_fk, 
				invrec_status_name, created_by, invoice_ref, net_suite_no, 
				invoice_date, approved_date, posted_date, payment_due_date, 
	
				quantity, 

				adjusted_unit_cost, adjusted_total_cost, adjustment_applied/quantity adjustment_applied,
				local_adjusted_unit_cost, local_adjusted_total_cost, local_adjustment_applied/quantity local_adjustment_applied,
				global_adjusted_unit_cost, global_adjusted_total_cost, global_adjustment_applied/quantity global_adjustment_applied
			from Warehouse.invrec.fact_invoice_reconciliation_line_v) irl on recl.idReceiptLine_sk = irl.idReceiptLine_sk_fk

	-- where pol.purchaseorderlineid_bk in (4503599628265083, 4503599635592265)
	where recl.receiptlineid_bk = 74590868832776642
	order by purchaseorderlineid_bk desc, receiptlineid_bk desc
