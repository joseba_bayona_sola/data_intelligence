
drop table #pfps

select product_id_bk, 
	packsizeid_bk, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
	priority_packs, priority_packs_size,
	min(size) over (partition by product_id_bk) min_pack_size
into #pfps
from
	(select product_id_bk, 
		packsizeid_bk, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
		dense_rank() over (partition by product_id_bk order by allocation_preference_order) priority_packs, 
		dense_rank() over (partition by product_id_bk order by size desc) priority_packs_size 	
	from
		(select pfps.packsizeid packsizeid_bk, 
			pf.product_id_bk, convert(int, pfps.size) size, pfps.description product_family_packsize_name, 
			pfps.allocationPreferenceOrder allocation_preference_order, pfps.purchasingPreferenceOrder purchasing_preference_order
		from 
				Landing.mend.gen_prod_productfamilypacksize_v pfps
			inner join
				Landing.aux.mag_prod_product_family_v pf on pfps.productfamilyid = pf.productfamilyid
		where packsizeid is not null and packsizeid <> 40532396646335267
			-- and pf.product_id_bk in ('1122', '1136')
			) pfps) pfps
where priority_packs in (1, 2)


select product_id_bk, 
	packsizeid_bk, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
	priority_packs, min_pack_size
from
	(select product_id_bk, 
		packsizeid_bk, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
		priority_packs, -- priority_packs / priority_packs_size
		-- dense_rank() over (partition by product_id_bk order by size desc) priority_packs_size, 
		min(size) over (partition by product_id_bk) min_pack_size
	from #pfps) pfps
where priority_packs in (1, 2)


	select product_id_bk, [1] packsizeid1_bk, [2] packsizeid2_bk
	from
			(select product_id_bk, packsizeid_bk, priority_packs -- size,				
			from #pfps) pfps
		pivot
			(avg(packsizeid_bk)
			for priority_packs in ([1], [2])) pvt
	-- where product_id_magento in ('3159', '1405', '2318', '2334', '1083')
	order by product_id_bk

select p.product_id_bk, pfps1.min_pack_size,

	p.packsizeid1_bk, pfps1.size size1, 
	p.packsizeid2_bk, pfps2.size size2
from
		(select product_id_bk, [1] packsizeid1_bk, [2] packsizeid2_bk
		from
				(select product_id_bk, packsizeid_bk, priority_packs -- size,				
				from #pfps) pfps
			pivot
				(avg(packsizeid_bk)
				for priority_packs in ([1], [2])) pvt) p
	inner join
		#pfps pfps1 on p.product_id_bk = pfps1.product_id_bk and p.packsizeid1_bk = pfps1.packsizeid_bk
	left join
		#pfps pfps2 on p.product_id_bk = pfps2.product_id_bk and p.packsizeid2_bk = pfps2.packsizeid_bk
order by product_id_bk

select product_id_bk, t.num_months * min_pack_size quantity_lens, 'pref' optimun_type,
	packsizeid1_bk, t.num_months * min_pack_size / size1 quantity_packs_1, 
	packsizeid2_bk, (t.num_months * min_pack_size % size1) / size2 quantity_packs_2
from 
	(select p.product_id_bk, pfps1.min_pack_size,
		p.packsizeid1_bk, pfps1.size size1, 
		p.packsizeid2_bk, pfps2.size size2
	from
			(select product_id_bk, [1] packsizeid1_bk, [2] packsizeid2_bk
			from
					(select product_id_bk, packsizeid_bk, priority_packs -- size,				
					from #pfps) pfps
				pivot
					(avg(packsizeid_bk)
					for priority_packs in ([1], [2])) pvt) p
		inner join
			#pfps pfps1 on p.product_id_bk = pfps1.product_id_bk and p.packsizeid1_bk = pfps1.packsizeid_bk
		left join
			#pfps pfps2 on p.product_id_bk = pfps2.product_id_bk and p.packsizeid2_bk = pfps2.packsizeid_bk) pf, 
	(select top 100 dense_rank() over (order by packsizeid_bk) num_months 
	from #pfps) t
where pf.product_id_bk in ('1122')
order by product_id_bk, quantity_lens
