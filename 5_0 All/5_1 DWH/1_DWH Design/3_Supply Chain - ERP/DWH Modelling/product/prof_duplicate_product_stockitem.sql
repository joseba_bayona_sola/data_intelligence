
-- product

	-- Landing
	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Landing.mend.prod_product) t
	where num_rep > 1
	order by num_rep desc, sku

	-- Landing Views
	select *
	from
		(select count(*) over (partition by sku_product) num_rep, *
		from Landing.mend.gen_prod_product_v) t
	where num_rep > 1
	order by num_rep desc, sku_product

	-- Warehouse
	select *
	from
		(select count(*) over (partition by parameter) num_rep, *
		from Warehouse.prod.dim_product
		where delete_f = 'N'
		) t
	where num_rep > 1
	order by num_rep desc, parameter

	select *
	from
		(select count(*) over (partition by parameter) num_rep, *
		from Warehouse.prod.dim_product_v) t
	where num_rep > 1
	order by num_rep desc, parameter

		select product_id_magento, product_family_name, count(*)
		from
			(select count(*) over (partition by parameter) num_rep, *
			from Warehouse.prod.dim_product_v) t
		where num_rep > 1
		group by product_id_magento, product_family_name
		order by product_id_magento, product_family_name


	-- Used in: 
		-- gen_order_orderline_v
		-- gen_order_orderlinemagento_v

-- stock item

	-- Landing
	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Landing.mend.prod_stockitem) t
	where num_rep > 1
	order by num_rep desc, sku

	-- Landing Views
	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Landing.mend.gen_prod_stockitem_v) t
	where num_rep > 1
	order by num_rep desc, sku

	-- Warehouse
	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Warehouse.prod.dim_stock_item
		where delete_f = 'N') t
	where num_rep > 1
	order by num_rep desc, sku

	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Warehouse.prod.dim_stock_item_v) t
	where num_rep > 1
	order by num_rep desc, sku

		select product_id_magento, product_family_name, count(*)
		from
			(select count(*) over (partition by sku) num_rep, *
			from Warehouse.prod.dim_stock_item_v) t
		where num_rep > 1
		group by product_id_magento, product_family_name
		order by product_id_magento, product_family_name


	select *
	from
		(select count(*) over (partition by sku) num_rep, *
		from Warehouse.prod.dim_stock_item
		where idStockItem_sk in 
			(select distinct idStockItem_sk_fk
			from Warehouse.stock.dim_wh_stock_item)) t
	where num_rep > 1
	order by num_rep desc, sku

	select *
	from
		(select *
		from Warehouse.prod.dim_stock_item
		where delete_f = 'Y' and 
			idStockItem_sk in 
			(select distinct idStockItem_sk_fk
			from Warehouse.stock.dim_wh_stock_item)) t
	order by sku

	select count(*) -- 163556
	from Warehouse.stock.dim_wh_stock_item

	select count(*) -- 163545
	from Warehouse.stock.dim_wh_stock_item_v

	-- Used in: 
		-- gen_wh_warehousestockitem_v
		-- gen_inter_intransitstockitembatch_th_v
		-- gen_invrec_invoiceline_v

		-- gen_whship_receiptline_1_v (Logic per SKU)
		-- gen_whcust_customerinvoiceline_v (Logic per SKU)

		-- lnd_stg_get_fm_wh_stock_item_static_data
		-- lnd_stg_get_fm_fm_data
		-- lnd_stg_get_fm_planned_requirements