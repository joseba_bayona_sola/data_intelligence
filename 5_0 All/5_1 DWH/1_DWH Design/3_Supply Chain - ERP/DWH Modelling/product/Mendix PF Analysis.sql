
-- Product Family
-- select top 1000 mepf.productfamilyid, mepf.magentoProductID, mepf.name, mepf.status
select mepf.*
from 
		Landing.mend.gen_prod_productfamily_v mepf
	left join
		Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
where mpf.product_id_magento is null
order by magentoProductID

-- Product Family Pack Size
select p.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_prod_productfamilypacksize_v p on pf.productfamilyid = p.productfamilyid
order by p.magentoProductID

-- Product Family Pack Size - Price
select p.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_prod_productfamilypacksize_supplierprice_v p on pf.productfamilyid = p.productfamilyid
order by p.magentoProductID

-- Product
select p.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_prod_product_v p on pf.productfamilyid = p.productfamilyid
order by p.magentoProductID

-- Stock Item
select si.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_prod_stockitem_v si on pf.productfamilyid = si.productfamilyid
order by si.magentoProductID

-- Warehouse Stock Item
select wsi.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on pf.productfamilyid = wsi.productfamilyid
where wsi.availableqty <> 0 or wsi.allocatedstockqty <> 0
order by wsi.magentoProductID


select wsib.*
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on pf.productfamilyid = wsib.productfamilyid
order by wsib.magentoProductID, wsib.warehousestockitemid, wsib.batch_id

-----------------------------------------

select ol.magentoProductID, ol.name, count(*)
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_order_orderline_v ol on pf.productfamilyid = ol.productfamilyid
group by ol.magentoProductID, ol.name
order by ol.magentoProductID, ol.name

select ol.magentoProductID, ol.name, count(*)
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_order_orderlinemagento_v ol on pf.productfamilyid = ol.productfamilyid
group by ol.magentoProductID, ol.name
order by ol.magentoProductID, ol.name


select ol.magentoProductID, ol.name, count(*)
from
		(select top 1000 mepf.*
		from 
				Landing.mend.gen_prod_productfamily_v mepf
			left join
				Warehouse.prod.dim_product_family_v mpf on mepf.magentoProductID_int = mpf.product_id_magento
		where mpf.product_id_magento is null) pf
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v ol on pf.productfamilyid = ol.productfamilyid
group by ol.magentoProductID, ol.name
order by ol.magentoProductID, ol.name