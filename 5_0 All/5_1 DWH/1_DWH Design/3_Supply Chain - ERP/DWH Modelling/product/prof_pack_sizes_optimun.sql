
select *
from Warehouse.prod.dim_product_family_pack_size_v
where purchasing_preference_order not in (1, 2)

select idProductFamilyPacksize_sk, packsizeid_bk, product_id_magento, product_family_name, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
	count(*) over (partition by product_id_magento) num_packs, 
	dense_rank() over (partition by product_id_magento order by allocation_preference_order) priority_packs, 
	dense_rank() over (partition by product_id_magento order by size desc) priority_packs_size, 
	min(size) over (partition by product_id_magento) min_pack_size
from Warehouse.prod.dim_product_family_pack_size_v
-- where product_id_magento in ('1083')
-- where product_id_magento in ('1122', '1136')
-- where product_id_magento in ('3159', '1405', '2318', '2334')
-- where allocation_preference_order <> purchasing_preference_order
-- order by product_id_magento, allocation_preference_order
order by num_packs desc, product_id_magento, allocation_preference_order

	select idProductFamilyPacksize_sk, packsizeid_bk, product_id_magento, product_family_name, product_family_packsize_name, size, allocation_preference_order, purchasing_preference_order, 
		count(*) over (partition by product_id_magento) num_packs, 
		dense_rank() over (partition by product_id_magento order by allocation_preference_order) priority_packs, 
		min(size) over (partition by product_id_magento) min_pack_size
	from Warehouse.prod.dim_product_family_pack_size_v
	where product_id_magento in ('1083')

	select product_id_magento, product_family_name, [1], [2], [3]
	from
			(select product_id_magento, product_family_name, 
				dense_rank() over (partition by product_id_magento order by allocation_preference_order) priority_packs,
				-- dense_rank() over (partition by product_id_magento order by size desc) priority_packs,  -- priority_packs_size
				size
			from Warehouse.prod.dim_product_family_pack_size_v
			-- where product_id_magento in ('1083')
			) pfps
		pivot
			(avg(size)
			for priority_packs in ([1], [2], [3])) pvt
	where product_id_magento in ('3159', '1405', '2318', '2334', '1083')
	order by product_id_magento, product_family_name

select t.num_months,
	product_id_magento, product_family_name, 
	t.num_months * min_pack_size, 
	t.num_months * min_pack_size / 90, -- t.num_months * min_pack_size % 90,
	(t.num_months * min_pack_size % 90) / 30
from 
	(select product_id_magento, product_family_name, min(size) min_pack_size
	from Warehouse.prod.dim_product_family_pack_size_v
	group by product_id_magento, product_family_name) pf, 
	(select top 36 dense_rank() over (order by idProductFamilyPacksize_sk) num_months 
	from Warehouse.prod.dim_product_family_pack_size_v) t
where pf.product_id_magento in ('1083')
-- where product_id_magento in ('1122', '1136')
order by product_id_magento

