

-------------- Landing

select count(*), count(distinct stockitemid), count(distinct SKU)
from Landing.mend.gen_prod_stockitem_v

select top 1000 *
from Landing.mend.gen_prod_stockitem_v
where stockitemid is null

select top 1000 sku, count(*)
from Landing.mend.gen_prod_stockitem_v
group by sku
having count(*) > 1
order by count(*) desc

select top 1000 t.n, si.productfamilyid, si.productid, si.stockitemid, 
	si.magentoProductID, si.name, si.SKU_product, si.SKU, si.stockitemdescription
from 
		Landing.mend.gen_prod_stockitem_v si
	inner join
		(select top 1000 sku, count(*) n
		from Landing.mend.gen_prod_stockitem_v
		where sku is not null
		group by sku
		having count(*) > 1) t on si.sku = t.sku
order by si.sku, si.stockitemid

-------------- Warehouse

select *
from
	(select idStockItem_sk, stockitemid_bk, product_id_magento, stock_item_description, sku, 
		count(*) over (partition by SKU) num_rep
	from Warehouse.prod.dim_stock_item_v) si
where num_rep > 1
order by si.sku, si.stockitemid_bk

select si.idStockItem_sk, si.stockitemid_bk, si.product_id_magento, si.stock_item_description, si.sku, 
	si.num_rep, wsi.num_records, 
	si.ord_rep, 
	count(*) over (partition by SKU) num_rep2
from
		(select *, rank() over (partition by si.sku order by stockitemid_bk) ord_rep
		from
			(select idStockItem_sk, stockitemid_bk, product_id_magento, stock_item_description, sku, 
				count(*) over (partition by SKU) num_rep
			from Warehouse.prod.dim_stock_item_v) si
		where num_rep > 1) si
	left join
		(select idStockItem_sk_fk, count(*) num_records
		from Warehouse.stock.dim_wh_stock_item
		group by idStockItem_sk_fk) wsi on si.idStockItem_sk = wsi.idStockItem_sk_fk
-- where wsi.num_records is not null
order by num_rep2 desc, si.sku, si.stockitemid_bk





