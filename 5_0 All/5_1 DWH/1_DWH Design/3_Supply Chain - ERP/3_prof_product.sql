
---------------------- NOTES ----------------------

	-- productfamily
	select *
	from Landing.mend.prod_productfamily_aud
	order by magentoProductID

	select count(*)
	from Landing.mend.prod_productfamily_aud

	select status, count(*)
	from Landing.mend.prod_productfamily_aud
	group by status
	order by status

	select productFamilyType, count(*)
	from Landing.mend.prod_productfamily_aud
	group by productFamilyType
	order by productFamilyType

	select productfamilyid, 
		magentoProductID_int, 
		magentoProductID, magentoSKU, name, status
	from Landing.mend.gen_prod_productfamily_v
	order by magentoProductID

---------------------- NOTES ----------------------
	-- meaning of package size attributes: packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder

	-- productfamilypacksize
	select productfamilyid, packsizeid, 
		productfamilysizerank,
		magentoProductID_int,
		magentoProductID, magentoSKU, name, status,
		
		description, size, 
		packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
		weight, height, width, depth
	from Landing.mend.gen_prod_productfamilypacksize_v
	-- where packsizeid is null
	-- where productfamilyid = 52917295621603794
	order by magentoProductID

	select count(*)
	from Landing.mend.prod_packsize_productfamily

	select count(*)
	from Landing.mend.gen_prod_productfamilypacksize_v

		select *, 
			count(*) over (partition by packsizeid) rep_father,
			count(*) over (partition by productfamilyid) rep_son
		from Landing.mend.prod_packsize_productfamily
		order by rep_son desc, rep_father desc

	select magentoProductID_int, magentoSKU, name, count(*)
	from Landing.mend.gen_prod_productfamilypacksize_v
	group by magentoProductID_int, magentoSKU, name
	order by magentoProductID_int, magentoSKU, name

	select size, count(*)
	from Landing.mend.gen_prod_productfamilypacksize_v
	group by size
	order by size

---------------------- NOTES ----------------------
	-- productfamilypacksize_supplierprice
		-- PF - Size without a Supplier Price (Free - Glasses - XXX)
		-- Why repeated PF - Size - Supplier - Price combinations (Standard Attr?)

	-- productfamilypacksize_supplierprice
	select productfamilyid, packsizeid, supplier_id, supplierpriceid,
		magentoProductID_int, magentoSKU, name, status, productFamilySizeRank, size, 
		supplierID, supplierName, subMetaObjectName, unitPrice, currencyCode, leadTime
	from Landing.mend.gen_prod_productfamilypacksize_supplierprice_v
	-- where supplierpriceid is null
	where supplierpriceid is not null
	-- where magentoProductID_int = 1083
	-- where packsizeid = 40532396646334928
	order by magentoProductID_int, size, supplierID, subMetaObjectName, unitPrice

	select count(*)
	from Landing.mend.gen_supp_supplierprice_v
	where supplierpriceid is not null

	select count(*)
	from Landing.mend.gen_prod_productfamilypacksize_supplierprice_v
	
		select *, 
			count(*) over (partition by supplierpriceid) rep_father,
			count(*) over (partition by packsizeid) rep_son
		from Landing.mend.supp_supplierprices_packsize_aud
		order by rep_father desc, rep_son desc

---------------------- NOTES ----------------------
	-- product
		-- 2 PF without Product
		-- SKU_product is NOT UNIQUE (1235 Proclear Multifocal Toric)

	-- product
	select top 2000 productfamilyid, productid,
		magentoProductID_int,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI
	from Landing.mend.gen_prod_product_v
	-- where productid is null
	-- where magentoProductID_int in (1083, 1317)
	-- where productfamilyid = 52917295621603859
	-- where SKU_product in ('01235B1D6BZC1DD02AY00', '01235B1D6CNC7DN03A400')
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co

	select count(*)
	from Landing.mend.prod_product_aud

	select count(*)
	from Landing.mend.gen_prod_product_v

		select *, 
			count(*) over (partition by productid) rep_father,
			count(*) over (partition by productfamilyid) rep_son
		from Landing.mend.prod_product_productfamily_aud
		order by rep_father desc, rep_son desc

	select magentoProductID_int, magentoSKU, name, count(*)
	from Landing.mend.gen_prod_product_v	
	group by magentoProductID_int, magentoSKU, name
	order by magentoProductID_int, magentoSKU, name

	select top 2000 SKU_product, count(*)
	from Landing.mend.gen_prod_product_v
	group by SKU_product
	having count(*) > 1
	order by count(*) desc, SKU_product

---------------------- NOTES ----------------------
	-- stockitem
		-- 12 P without Stockitem (The ones where packSize is null)
		-- SKU_product is NOT UNIQUE (1235 Proclear Multifocal Toric)

	-- stockitem
	select top 2000 productfamilyid, productid, stockitemid, 
		magentoProductID_int,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		packSize, SKU, stockitemdescription, -- si.oldSKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		manufacturerArticleID, manufacturerCodeNumber,
		SNAPUploadStatus
	from Landing.mend.gen_prod_stockitem_v
	-- where stockitemid is null
	where magentoProductID_int in (1083, 1317)
	-- where SKU_product in ('01235B1D6BZC1DD02AY00', '01235B1D6CNC7DN03A400')
	-- where packSize is null
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packsize

	select count(*)
	from Landing.mend.prod_stockitem_aud

	select count(*)
	from Landing.mend.gen_prod_stockitem_v

		select *, 
			count(*) over (partition by stockitemid) rep_father,
			count(*) over (partition by productid) rep_son
		from Landing.mend.prod_stockitem_product_aud
		order by rep_father desc, rep_son desc

	select magentoProductID_int, magentoSKU, name, count(*)
	from Landing.mend.gen_prod_stockitem_v	
	group by magentoProductID_int, magentoSKU, name
	order by magentoProductID_int, magentoSKU, name

	select magentoProductID_int, magentoSKU, packSize, name, count(*)
	from Landing.mend.gen_prod_stockitem_v	
	group by magentoProductID_int, magentoSKU, packSize, name
	order by magentoProductID_int, magentoSKU, packSize, name

	select top 2000 SKU, count(*)
	from Landing.mend.gen_prod_stockitem_v
	group by SKU
	having count(*) > 1
	order by count(*) desc, SKU

	select SNAPUploadStatus, count(*)
	from Landing.mend.gen_prod_stockitem_v	
	group by SNAPUploadStatus
	order by SNAPUploadStatus
