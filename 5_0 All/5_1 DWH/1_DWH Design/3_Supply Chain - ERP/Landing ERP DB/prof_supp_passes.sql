﻿


-- Suppliers table.
select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, 
	vatCode, defaultLeadTime,
	email, telephone, fax,
					
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber, 
	defaulttransitleadtime, aggregatable, 

	bulkdiscount, isAutoEDIStock, shipToNumber, sortIsEnabled, freightRateRef, isFreightSupplier, validate
from public.suppliers$supplier

-- standardprice: GBPUnitPrice the value in GPB for all unitPrice to be able to compare them.
select id,
	minqty, startdate, enddate, maxqty, 
	minOrderValue, maxOrderValue
from public.suppliers$standardprice

-- Suppliers$SupplierPassPrice  Sent to FM later - Extracts from ERP the 3 supplierPrices per warehouse per ps for 3 passes each day.
select id, 
	salesTerritory, productFamily, packsize, aggregateNode, pass, passNumber, 
	supplierName, leadtime, transitleadtime, unitprice, gbpUnitPrice, currency, 

	order_day_monday, order_day_tuesday, order_day_wednesday, order_day_thursday, order_day_friday, order_day_saturday, order_day_sunday, 
	shipping_day_monday, shipping_day_tuesday, shipping_day_wednesday, shipping_day_thursday, shipping_day_friday, shipping_day_saturday, shipping_day_sunday, 

	createdDate
from public.suppliers$supplierpassprice
order by productFamily, packsize, salesTerritory, pass

	select date(createddate), count(*)
	from public.suppliers$supplierpassprice
	group by date(createddate)
	order by date(createddate)

	select salesTerritory, count(*)
	from public.suppliers$supplierpassprice
	group by salesTerritory
	order by salesTerritory

	select productFamily, count(*)
	from public.suppliers$supplierpassprice
	group by productFamily
	order by productFamily

	select productFamily, packsize, count(*)
	from public.suppliers$supplierpassprice
	group by productFamily, packsize
	order by productFamily, packsize


select suppliers$supplierpasspriceid, suppliers$supplierid
from suppliers$supplierpassprice_supplier

select suppliers$supplierpasspriceid, suppliers$standardpriceid
from suppliers$supplierpassprice_standardprice

select suppliers$supplierpasspriceid, inventory$warehouseid
from suppliers$supplierpassprice_warehouse

select suppliers$supplierpasspriceid, product$packsizeid
from suppliers$supplierpassprice_packsize


	