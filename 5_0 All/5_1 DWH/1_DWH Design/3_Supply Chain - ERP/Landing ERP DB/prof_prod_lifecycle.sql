﻿

-- Ticket URL: https://lenses.atlassian.net/browse/ERP-2281

select *
from product$stockitem
limit 1000

select lifecycle, count(*)
from product$stockitem
group by lifecycle
order by lifecycle

-- AC - Active
-- LS - Late Stage 
-- MR - Manual Replenishment
-- OB - Obsolete 
-- ST - Setup
-- PR - Promotion

select *
from product$lifecycleskuimport

-- 
Solution: 

We will create a new dimension table (prod.dim_stock_lifecycle) so we can have on it the different lifecycle values. The values will be loaded from a Map file. Then we will create a new attribute in prod.dim_stock_item being a FK to that new dimension table

-- 1. Landing of new attribute in product$stockitem
-- 2. Mapping file, table for stock_lifecycle
-- 3. New dimension table dim_stock_lifecycle
-- 4. Changes on dimension table prod.dim_stock_item
-- 5. Changes on views 


* Development Steps - Map File + Dimension Table

Control DB: 

Landing DB: 


* Development Steps: Dimension Table

Gliffy:

Control DB: 

Warehouse DB:

Staging DB:

Landing DB: 


* Development Steps - SI Table: 

Gliffy:

Control DB: 

Warehouse DB:

Staging DB:

Landing DB: 
