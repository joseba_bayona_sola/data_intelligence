﻿
-- Questions: 
	-- Diff between Value - Estimated Carriage Cost - Actual Carriage Cost
	-- Relation To Receipts

	-- Diff between Collection Warehouse - Destination Warehosue
	-- Diff between External - Internal

	-- What about Stock Suppliers where freight cost is already on product cost

	-- What about duty cost
-- suppliers$supplier

select *
from suppliers$supplier

select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, null isEDIEnabled,
	vatCode, defaultLeadTime,
	email, telephone, fax,
	null flatFileConfigurationToUse,
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber,
	defaulttransitleadtime, aggregatable, 
	bulkdiscount, isautoedistock, 
	isfreightsupplier, validate, isses, ismx, autoconsigned, freightcostexempt -- freightrateref, 	
from suppliers$supplier
where isfreightSupplier = true

-- freightconsignments$freightconsignment

select *
from freightconsignments$freightconsignment
where consignmentref = 'FC00001508'

select id, 
	consignmentref, 
	autoconsigned, freightcostexempt,
	consignmentid, consignmenttype, invoice,
	consignmentstatus, 
	estimatedPackingQuantity, actualPackingQuantity, numberOfReceipts, 

	quantity, totalCartons, totalWeight, totalVolume,

	totalvalue_suppliercurrency, totalvalue_freightcurrency, supplierspotrate, freightspotrate, 
	estimatedfreightcost, 
	netfreightcost, advancementfees, demurrage, duty, actualfreightcost, fuelcost, othercharges, 
	vat, grandtotal,
	-- estimatedcarriagecost, 
	
	approveddate, planneddate, collectiondate, requesteddeliverydate, delivereddate, receiptduedate, 
	preparationcompletedate, processingcompletedate, completeddate, 

	freightforwardernote, auditComment, attachmentsCount, system$owner, statusChangeHandlerRunning, documenttype,
	
	createdDate
from freightconsignments$freightconsignment
order by id desc

	-- freightconsignments$freightconsignment_freightforwarder
	select freightconsignments$freightconsignmentid, suppliers$supplierid
	from freightconsignments$freightconsignment_freightforwarder

	-- freightconsignments$freightconsignment_collectionsource
	select freightconsignments$freightconsignmentid, suppliers$supplierid
	from freightconsignments$freightconsignment_collectionsource

	-- freightconsignments$freightconsignment_sourcewarehouse
	select freightconsignments$freightconsignmentid, inventory$warehouseid
	from freightconsignments$freightconsignment_sourcewarehouse
	
	-- freightconsignments$freightconsignment_destinationwarehouse
	select freightconsignments$freightconsignmentid, inventory$warehouseid
	from freightconsignments$freightconsignment_destinationwarehouse

	-- freightconsignments$freightconsignment_createdby
	select freightconsignments$freightconsignmentid, administration$accountid
	from freightconsignments$freightconsignment_createdby
	
	-- freightconsignments$freightconsignment_freightrate
	select freightconsignments$freightconsignmentid, freightconsignments$freightrateid
	from freightconsignments$freightconsignment_freightrate
	
	-- freightconsignments$freightconsignment_collectionaddresses - NO NEEDED
	select freightconsignments$freightconsignmentid, suppliers$addressid
	from freightconsignments$freightconsignment_collectionaddresses

-- RELATION TO RECEIPT
	-- warehouseshipments$receipt_freightconsignment
	select *
	from warehouseshipments$receipt_freightconsignment

	select *
	from warehouseshipments$receiptline
	order by id desc
	limit 1000 
			
	select id, estimatedfreightcost, actualfreightcost, 
		costavailableforinvoice, recinvoicecarriagecost, recinvoicecost, 
		quantitymoved, movedline
	from warehouseshipments$receiptline
	order by id desc
	limit 1000 


	select *
	from invoicereconciliation$invoice
	where reconciledCostsAppliedToReceiptLines is true
	limit 1000 
	
	select *
	from invoicereconciliation$invoiceline
	limit 1000 
			
-- freightconsignments$freightconsignmentheader
select *
from freightconsignments$freightconsignmentheader

select id, quantity, 
	totalValue, -- totalEstCarriageCost, totalActualCarriageCost,
	totalEstFreightCost, totalActualFreightCost,
	totalWeight, totalVolume, 	
	height, width, depth, weight
from freightconsignments$freightconsignmentheader

	-- freightconsignments$freightconsignmentheader_freightconsignment
	select freightconsignments$freightconsignmentheaderid, freightconsignments$freightconsignmentid
	from freightconsignments$freightconsignmentheader_freightconsignment

	-- freightconsignments$freightconsignmentheader_productfamily
	select freightconsignments$freightconsignmentheaderid, product$productfamilyid
	from freightconsignments$freightconsignmentheader_productfamily

	-- freightconsignments$freightconsignmentheader_packsize
	select freightconsignments$freightconsignmentheaderid, product$packsizeid
	from freightconsignments$freightconsignmentheader_packsize	
	
-- freightconsignments$freightrate
select *
from freightconsignments$freightrate

select id, supplyroute, freightRateID, freightmethod, freightRateReference, _type, 
	minQtyUoM, maxQtyUoM, transitLeadTime, price, currency, startDate, endDate, 

	createdDate
from freightconsignments$freightrate	
order by id

	-- freightconsignments$freightrate_freightforwarder
	select freightconsignments$freightrateid, suppliers$supplierid
	from freightconsignments$freightrate_freightforwarder
	
	-- freightconsignments$freightrate_fromwarehousesupplier
	select freightconsignments$freightrateid, suppliers$supplierid
	from freightconsignments$freightrate_fromwarehousesupplier
	
	-- freightconsignments$freightrate_towarehouse
	select freightconsignments$freightrateid, inventory$warehouseid
	from freightconsignments$freightrate_towarehouse

	-- freightconsignments$freightrate_freightpackingmethod
	select freightconsignments$freightrateid, freightconsignments$freightpackingmethodid
	from freightconsignments$freightrate_freightpackingmethod

	-- freightconsignments$freightrate_createdby
	select freightconsignments$freightrateid, administration$accountid
	from freightconsignments$freightrate_createdby


	-- freightconsignments$freightrate_collectionaddress - NO NEEDED?
	select freightconsignments$freightrateid, suppliers$addressid
	from freightconsignments$freightrate_collectionaddress

	-- freightconsignments$freightrate_shippingterms - NO NEEDED?
	select freightconsignments$freightrateid, purchaseorders$shippingtermsid
	from freightconsignments$freightrate_shippingterms

-- freightconsignments$freightpackingmethod
select *
from freightconsignments$freightpackingmethod

select id, name, description, packagingtype, dimensionUoM, weightUoM, 
	width, height, depth, maxWeight, totalVolume, 
	system$changedby, createdDate, changedDate
from freightconsignments$freightpackingmethod	
