﻿
-- freightconsignments$freightconsignment
select id, 
	consignmentref, consignmentid, consignmenttype, 
	invoicereference, consignmentstatus, 
	estimatedPackingQuantity, actualPackingQuantity, numberOfReceipts, 

	depth, height, width, 

	quantity, totalUnits, totalCartons, totalWeight, totalVolume,

	totalvalue_rcurrency, totalvalue_fccurrency, actualCarriageCost, estimatedCarriageCost, 
	
	approveddate, plannedDate, collectiondate, requesteddeliverydate, delivereddate, 
	preparationcompletedate, processingcompletedate, completeddate, 

	freightforwardernote, auditComment, attachmentsCount, system$owner, statusChangeHandlerRunning, 
	
	createdDate
from freightconsignments$freightconsignment
order by id

select *
from freightconsignments$freightconsignment
where consignmentref = 'FC00000002'
order by id

	-- freightconsignments$freightconsignment_sourcewarehouse
	select freightconsignments$freightconsignmentid, inventory$warehouseid
	from freightconsignments$freightconsignment_sourcewarehouse
	
	-- freightconsignments$freightconsignment_destinationwarehouse
	select freightconsignments$freightconsignmentid, inventory$warehouseid
	from freightconsignments$freightconsignment_destinationwarehouse
	
	-- freightconsignments$freightconsignment_collectionsource
	select freightconsignments$freightconsignmentid, suppliers$supplierid
	from freightconsignments$freightconsignment_collectionsource
	
	-- freightconsignments$freightconsignment_freightforwarder
	select freightconsignments$freightconsignmentid, suppliers$supplierid
	from freightconsignments$freightconsignment_freightforwarder
	
	-- freightconsignments$freightconsignment_createdby
	select freightconsignments$freightconsignmentid, administration$accountid
	from freightconsignments$freightconsignment_createdby
	
	-- freightconsignments$freightconsignment_freightrate
	select freightconsignments$freightconsignmentid, freightconsignments$freightrateid
	from freightconsignments$freightconsignment_freightrate
	
	-- freightconsignments$freightconsignment_collectionaddresses
	select freightconsignments$freightconsignmentid, suppliers$addressid
	from freightconsignments$freightconsignment_collectionaddresses


	-- warehouseshipments$receipt_freightconsignment
	select *
	from warehouseshipments$receipt_freightconsignment

-- freightconsignments$freightconsignmentheader
select id, quantity, totalUnits, totalValue, totalWeight, totalVolume, totalCartons, totalCarriageCost, 
	height, width, depth, weight
from freightconsignments$freightconsignmentheader

	-- freightconsignments$freightconsignmentheader_freightconsignment
	select freightconsignments$freightconsignmentheaderid, freightconsignments$freightconsignmentid
	from freightconsignments$freightconsignmentheader_freightconsignment

	-- freightconsignments$freightconsignmentheader_productfamily
	select freightconsignments$freightconsignmentheaderid, product$productfamilyid
	from freightconsignments$freightconsignmentheader_productfamily

	-- freightconsignments$freightconsignmentheader_packsize
	select freightconsignments$freightconsignmentheaderid, product$packsizeid
	from freightconsignments$freightconsignmentheader_packsize		
	

-- freightconsignments$usercomment
select id, fullname, comment, createddate
from freightconsignments$usercomment

	-- freightconsignments$usercomment_freightconsignment
	select *
	from freightconsignments$usercomment_freightconsignment


-- freightconsignments$freightrate
select id, supplyroute, freightRateID, freightmethod, freightRateReference, _type, 
	minQtyUoM, maxQtyUoM, transitLeadTime, price, currency, startDate, endDate, 

	createdDate
from freightconsignments$freightrate	
order by id


	-- freightconsignments$freightrate_towarehouse
	select freightconsignments$freightrateid, inventory$warehouseid
	from freightconsignments$freightrate_towarehouse

	-- freightconsignments$freightrate_fromwarehousesupplier
	select freightconsignments$freightrateid, suppliers$supplierid
	from freightconsignments$freightrate_fromwarehousesupplier

	-- freightconsignments$freightrate_freightforwarder
	select freightconsignments$freightrateid, suppliers$supplierid
	from freightconsignments$freightrate_freightforwarder

	-- freightconsignments$freightrate_collectionaddress
	select freightconsignments$freightrateid, suppliers$addressid
	from freightconsignments$freightrate_collectionaddress

	-- freightconsignments$freightrate_createdby
	select freightconsignments$freightrateid, administration$accountid
	from freightconsignments$freightrate_createdby

	-- freightconsignments$freightrate_freightpackingmethod
	select freightconsignments$freightrateid, freightconsignments$freightpackingmethodid
	from freightconsignments$freightrate_freightpackingmethod

	-- freightconsignments$freightrate_shippingterms
	select freightconsignments$freightrateid, purchaseorders$shippingtermsid
	from freightconsignments$freightrate_shippingterms
	
-- freightconsignments$freightpackingmethod
select id, name, description, packagingtype, dimensionUoM, weightUoM, 
	height, depth, maxWeight, totalVolume, 
	system$changedby, createdDate, changedDate
from freightconsignments$freightpackingmethod	
