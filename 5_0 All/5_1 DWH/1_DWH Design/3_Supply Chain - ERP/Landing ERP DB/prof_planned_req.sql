﻿
select id, 
	fmorderref, fmfilename, fmfiledate, fmyear, fmmonth, 
	releasedate, duedate, 
	stockitemdescription,
	ordernumber, planner, plannedstatus, futurmasterstatus, reserved,
	orderedquantity, unitprice, 
	createddate, changeddate, system$changedby
from purchaseorders$plannedrequirement
-- where fmorderref = '1083-YORK_RETAIL-90PASS120190605'
order by id 
limit 1000

	-- 148525
	select count(*)
	from purchaseorders$plannedrequirement
	limit 1000

	select fmfilename, count(*)
	from purchaseorders$plannedrequirement
	group by fmfilename
	order by fmfilename
	limit 1000
	
	select plannedstatus, count(*)
	from purchaseorders$plannedrequirement
	group by plannedstatus
	order by plannedstatus
	limit 1000

	select futurmasterstatus, count(*)
	from purchaseorders$plannedrequirement
	group by futurmasterstatus
	order by futurmasterstatus
	limit 1000

	select reserved, count(*)
	from purchaseorders$plannedrequirement
	group by reserved
	order by reserved
	limit 1000

	select id, 
		fmorderref, fmfilename, fmfiledate, fmyear, fmmonth, 
		releasedate, duedate, 
		stockitemdescription,
		ordernumber, planner, plannedstatus, futurmasterstatus, reserved,
		orderedquantity, unitprice, 
		createddate, changeddate, system$changedby
	from 
		purchaseorders$plannedrequirement pr
	left join
		purchaseorders$plannedrequirement_purchaseorderline pr_pol on pr.id = pr_pol.purchaseorders$plannedrequirementid
	where pr_pol.purchaseorders$plannedrequirementid is null		
	order by id

select purchaseorders$plannedrequirementid, purchaseorders$plannedrequirementheaderid
from purchaseorders$plannedrequirement_plannedrequirementheader
limit 1000 

	-- 254324
	select count(*)
	from purchaseorders$plannedrequirement_plannedrequirementheader
	
	select purchaseorders$plannedrequirementheaderid, count(*)
	from purchaseorders$plannedrequirement_plannedrequirementheader
	group by purchaseorders$plannedrequirementheaderid
	order by purchaseorders$plannedrequirementheaderid
	limit 1000 


select purchaseorders$plannedrequirementid, purchaseorders$plannedpurchaseordersheaderid
from purchaseorders$plannedrequirement_plannedpurchaseordersheader
limit 1000 

select purchaseorders$plannedrequirementid, purchaseorders$purchaseorderlineid
from purchaseorders$plannedrequirement_purchaseorderline
limit 1000 

	-- 148001
	select count(*)
	from purchaseorders$plannedrequirement_purchaseorderline
	
select purchaseorders$plannedrequirementid, suppliers$supplierpriceid
from purchaseorders$plannedrequirement_supplierprice
limit 1000 

select purchaseorders$plannedrequirementid, administration$accountid
from purchaseorders$plannedrequirement_planner
limit 1000 



select purchaseorders$plannedrequirementid, inventory$warehouseid
from purchaseorders$plannedreq_warehouse
limit 1000 

	select inventory$warehouseid, count(*)
	from purchaseorders$plannedreq_warehouse
	group by inventory$warehouseid
	order by inventory$warehouseid
	limit 1000 

	select purchaseorders$plannedrequirementid, count(*)
	from purchaseorders$plannedreq_warehouse
	group by purchaseorders$plannedrequirementid
	order by count(*) desc, purchaseorders$plannedrequirementid
	limit 1000 

select purchaseorders$plannedrequirementid, inventory$warehouseid
from purchaseorders$plannedreq_warehouseforpurchasing
limit 1000 

select purchaseorders$plannedrequirementid, suppliers$supplierid
from purchaseorders$plannedreq_supplier
limit 1000 

select purchaseorders$plannedrequirementid, product$productfamilyid
from purchaseorders$plannedreq_productfamily
limit 1000 

	select purchaseorders$plannedrequirementid, count(*)
	from purchaseorders$plannedreq_productfamily
	group by purchaseorders$plannedrequirementid
	order by count(*) desc, purchaseorders$plannedrequirementid
	limit 1000 

select purchaseorders$plannedrequirementid, product$packsizeid
from purchaseorders$plannedreq_packsize
limit 1000 

select purchaseorders$plannedrequirementid, product$stockitemid
from purchaseorders$plannedreq_stockitem
limit 1000 


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


select id, fmfiledate, warehouse, warehouseforpurchasing, suppliername, status
from purchaseorders$plannedpurchaseordersheader
limit 1000

	select count(*)
	from purchaseorders$plannedpurchaseordersheader
	limit 1000


select purchaseorders$purchaseorderid, purchaseorders$plannedpurchaseordersheaderid
from purchaseorders$purchaseorder_plannedpurchaseordersheader

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

select id, fmfilename, fmfiledate, syntaxerror, createddate
from purchaseorders$plannedrequirementheader
limit 1000

	select count(*)
	from purchaseorders$plannedrequirementheader
	limit 1000

select purchaseorders$plannedrequirementheaderid, inventory$warehouseid
from purchaseorders$plannedreqheader_warehouse

select purchaseorders$plannedrequirementheaderid, suppliers$supplierid
from purchaseorders$plannedreqheader_supplier

select purchaseorders$plannedrequirementheaderid, product$productfamilyid
from purchaseorders$plannedreqheader_productfamily

select purchaseorders$plannedrequirementheaderid, product$packsizeid
from purchaseorders$plannedreqheader_packsize

