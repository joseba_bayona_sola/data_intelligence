
---------------------- NOTES ----------------------
	-- stockallocationtransaction
		-- Warehouse can be null - Packsize can be null (allocationType = Child)
		-- Stock Allocation Tr - Batch Allocation rel
			-- Stock Allocation Tr without Batch Allocation (97 k) (allocationType = Parent) // Many Batch Allocation
			-- Batch Allocation without Stock Allocation Tr (6 k) (Cancel - 0 Allocations)

		-- meaning of attributes
			-- allocationType - recordType - stockAllocated - cancelled
			-- allocatedStockQuantity vs issuedQuantity

	-- stockallocationtransaction
	select top 1000 orderlinestockallocationtransactionid, batchstockallocationid,
		warehouseid, orderid, orderlineid, warehousestockitemid, packsizeid, -- customershipmentlineid, 
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime, count(*) over ()
	from Landing.mend.gen_all_stockallocationtransaction_v
	-- where warehouseid is null and orderlinestockallocationtransactionid is not null
	-- where packsizeid is null and orderlinestockallocationtransactionid is not null
	-- where batchstockallocationid is null
	-- where orderlinestockallocationtransactionid is null
	-- where warehousestockitemid = 52635820644988201
	-- where orderlinestockallocationtransactionid = 9570149212522384
	order by timestamp desc

	select count(*)
	from Landing.mend.all_orderlinestockallocationtransaction_aud

	select count(*)
	from Landing.mend.gen_all_stockallocationtransaction_v

		select top 1000 *, 
			count(*) over (partition by batchstockallocationid) rep_father,
			count(*) over (partition by orderlinestockallocationtransactionid) rep_son
		from Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction_aud
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by orderlinestockallocationtransactionid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.all_at_warehouse
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by orderlinestockallocationtransactionid) rep_father,
			count(*) over (partition by orderid) rep_son
		from Landing.mend.all_at_order
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by orderlinestockallocationtransactionid) rep_father,
			count(*) over (partition by orderlineid) rep_son
		from Landing.mend.all_at_orderline
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by orderlinestockallocationtransactionid) rep_father,
			count(*) over (partition by warehousestockitemid) rep_son
		from Landing.mend.all_at_warehousestockitem
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by orderlinestockallocationtransactionid) rep_father,
			count(*) over (partition by packsizeid) rep_son
		from Landing.mend.all_at_packsize
		order by rep_father desc, rep_son desc

	select allocationType, count(*)
	from Landing.mend.gen_all_stockallocationtransaction_v
	group by allocationType
	order by allocationType

		select warehousestockitemid, count(*)
		from Landing.mend.gen_all_stockallocationtransaction_v
		where allocationType = 'Parent'
		group by warehousestockitemid
		order by warehousestockitemid
	

	select recordType, count(*)
	from Landing.mend.gen_all_stockallocationtransaction_v
	group by recordType
	order by recordType




---------------------- NOTES ----------------------
	-- magentoallocation
		-- Magento Allocation Stock Allocation Tr rel
			-- Stock Allocation Tr without Magento Allocation (227 k) (allocationType = Parent) // Many Magento Allocation per Stock Allocation Tr
			-- NO Magento Allocation without Stock Allocation Tr 

	-- magentoallocation
	select top 1000 magentoallocationid, orderlinestockallocationtransactionid, batchstockallocationid,
		shipped, cancelled_magentoall, 
		quantity,
		warehouseid, orderid, orderlineid, warehousestockitemid, packsizeid, -- customershipmentlineid, 
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime, 
		issuedDateTime, count(*) over (), count(*) over (partition by allocationType)
	from Landing.mend.gen_all_magentoallocation_v
	where magentoallocationid is null and orderlinestockallocationtransactionid is not null
	-- where magentoallocationid is not null and orderlinestockallocationtransactionid is null
	-- where warehousestockitemid = 52635820644988201 and quantity <> allocatedUnits
	order by timestamp desc

	select count(*)
	from Landing.mend.all_magentoallocation_aud

	select count(*)
	from Landing.mend.gen_all_magentoallocation_v

		select top 1000 *, 
			count(*) over (partition by magentoallocationid) rep_father,
			count(*) over (partition by orderlinestockallocationtransactionid) rep_son
		from Landing.mend.all_magentoallocatio_orderlinestockallocationtransac_aud
		order by rep_father desc, rep_son desc

-------------------------------------------------------------------------------------

select top 1000 *
from Landing.mend.order_order_aud
where id = 48695171003361936

select *
from Landing.mend.gen_all_stockallocationtransaction_v
where orderid = 48695171003361936

select *
from Landing.mend.gen_all_magentoallocation_v
where orderid = 48695171003361936

select *
from Landing.mend.gen_wh_warehousestockitem_v
where warehousestockitemid in (52635820645116474, 52635820645089808, 52635820645330782)

-----------------------------------------------------------------------------------------

	select top 1000 orderlinestockallocationtransactionid, batchstockallocationid,
		warehouseid, orderid, orderlineid, sat.warehousestockitemid, packsizeid, -- customershipmentlineid, 
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, sat.packSize, 
		stockAllocated, cancelled, 
		issuedDateTime, 
		wsi.magentoProductID_int, wsi.name, wsi.SKU, wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.warehouse_name, wsi.id_string
	from 
			Landing.mend.gen_all_stockallocationtransaction_v sat
		inner join
			Landing.mend.gen_wh_warehousestockitem_v wsi on sat.warehousestockitemid = wsi.warehousestockitemid
	order by timestamp desc

		select magentoProductID_int, name, count(*)
		from
			Landing.mend.gen_all_stockallocationtransaction_v sat
		inner join
			Landing.mend.gen_wh_warehousestockitem_v wsi on sat.warehousestockitemid = wsi.warehousestockitemid
		group by magentoProductID_int, name
		order by magentoProductID_int, name

		select magentoProductID_int, SKU, wsi.packSize, name, warehouse_name, count(*)
		from
			Landing.mend.gen_all_stockallocationtransaction_v sat
		inner join
			Landing.mend.gen_wh_warehousestockitem_v wsi on sat.warehousestockitemid = wsi.warehousestockitemid
		where allocationType = 'Parent' -- Parent - Child
		group by magentoProductID_int, SKU, wsi.packSize, name, warehouse_name
		order by magentoProductID_int, SKU, wsi.packSize, name, warehouse_name
