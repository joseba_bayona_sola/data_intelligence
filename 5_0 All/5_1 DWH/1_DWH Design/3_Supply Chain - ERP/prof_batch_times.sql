
-- WH Stock Item // Issue, Allocation: 13 min
-- Orders // OH: 17 min
-- Orders // OL: 40 min
-- Orders // OL Rel: 7 min
-- Orders // SH Group: 7 min
-- Allocation: 15 min

	select idETLBatchRun, idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where idETLBatchRun in (1291, 1292, 1293, 1294, 1295, 1298)
	order by idETLBatchRun, idPackageRun

	select pr.idETLBatchRun, pr.duration,
		-- idPackageRunMessage, prm.idPackageRun, prm.flow_name, 
		prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunMessage_v prm
		inner join
			ControlDB.logging.t_PackageRun_v pr on prm.idPackageRun = pr.idPackageRun
	where pr.idETLBatchRun in (1291, 1292, 1293, 1294, 1295, 1298)
		--and prm.package_name = 'lnd_stg_sales_fact_order' --  // stg_dwh_sales_fact_order
	order by pr.idETLBatchRun, prm.package_name, prm.message

