
---------- 01 Supplier -------------------------

select id
from Landing.mend.supp_supplier_aud
except
select id
from Landing.mend.supp_supplier


select *
from Landing.mend.supp_supplierprice_aud
where id in 
	(select id
	from Landing.mend.supp_supplierprice_aud
	except
	select id
	from Landing.mend.supp_supplierprice)

select *
from Landing.mend.supp_standardprice_aud
where id in 
	(select id
	from Landing.mend.supp_standardprice_aud
	except
	select id
	from Landing.mend.supp_standardprice)

select supplierpriceid, supplierid
from Landing.mend.supp_supplierprices_supplier_aud
except
select supplierpriceid, supplierid
from Landing.mend.supp_supplierprices_supplier

select supplierpriceid, packsizeid
from Landing.mend.supp_supplierprices_packsize_aud
except
select supplierpriceid, packsizeid
from Landing.mend.supp_supplierprices_packsize


---------- 02 Company -------------------------

select id
from Landing.mend.comp_company_aud
except
select id
from Landing.mend.comp_company

select id
from Landing.mend.comp_country_aud
except
select id
from Landing.mend.comp_country

select id
from Landing.mend.comp_currency_aud
except
select id
from Landing.mend.comp_currency


select id
from Landing.mend.comp_spotrate_aud
except
select id
from Landing.mend.comp_spotrate


---------- 03 Product -------------------------

select id
from Landing.mend.prod_productfamily_aud
except
select id
from Landing.mend.prod_productfamily

select id
from Landing.mend.prod_product_aud
except
select id
from Landing.mend.prod_product

select id
from Landing.mend.prod_stockitem_aud
except
select id
from Landing.mend.prod_stockitem

select id
from Landing.mend.prod_contactlens_aud
except
select id
from Landing.mend.prod_contactlens

select id
from Landing.mend.prod_packsize_aud
except
select id
from Landing.mend.prod_packsize


select packsizeid, stockitemid
from Landing.mend.prod_stockitem_packsize_aud
except
select packsizeid, stockitemid
from Landing.mend.prod_stockitem_packsize


---------- 04 Warehouse -------------------------

select id
from Landing.mend.wh_warehouse_aud
except
select id
from Landing.mend.wh_warehouse

select warehousesupplierid, supplierid
from Landing.mend.wh_warehousesupplier_supplier_aud
except
select warehousesupplierid, supplierid
from Landing.mend.wh_warehousesupplier_supplier

select id
from Landing.mend.wh_supplierroutine_aud
except
select id
from Landing.mend.wh_supplierroutine

---------- 05 Warehouse Stock -------------------------

select id
from Landing.mend.wh_warehousestockitem_aud
except
select id
from Landing.mend.wh_warehousestockitem

select warehousestockitemid, stockitemid
from Landing.mend.wh_warehousestockitem_stockitem_aud
except
select warehousestockitemid, stockitemid
from Landing.mend.wh_warehousestockitem_stockitem


select id
from Landing.mend.wh_warehousestockitembatch_aud
except
select id
from Landing.mend.wh_warehousestockitembatch

select warehousestockitembatchid, warehousestockitemid
from Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud
except
select warehousestockitembatchid, warehousestockitemid
from Landing.mend.wh_warehousestockitembatch_warehousestockitem



select id
from Landing.mend.wh_batchstockissue_aud
except
select id
from Landing.mend.wh_batchstockissue

select batchstockissueid, warehousestockitembatchid
from Landing.mend.wh_batchstockissue_warehousestockitembatch_aud
except
select batchstockissueid, warehousestockitembatchid
from Landing.mend.wh_batchstockissue_warehousestockitembatch


select id
from Landing.mend.wh_batchstockallocation_aud
except
select id
from Landing.mend.wh_batchstockallocation

select batchstockissueid, batchstockallocationid
from Landing.mend.wh_batchstockissue_batchstockallocation_aud
except
select batchstockissueid, batchstockallocationid
from Landing.mend.wh_batchstockissue_batchstockallocation

select batchstockallocationid, orderlinestockallocationtransactionid
from Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction_aud
except
select batchstockallocationid, orderlinestockallocationtransactionid
from Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction




select id
from Landing.mend.wh_repairstockbatchs_aud
except
select id
from Landing.mend.wh_repairstockbatchs

select id
from Landing.mend.wh_stockmovement_aud
except
select id
from Landing.mend.wh_stockmovement

select id
from Landing.mend.wh_batchstockmovement_aud
except
select id
from Landing.mend.wh_batchstockmovement

select batchstockmovementid, warehousestockitembatchid
from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
except
select batchstockmovementid, warehousestockitembatchid
from Landing.mend.wh_batchstockmovement_warehousestockitembatch


---------- 06 Order -------------------------

select id
from Landing.mend.order_order_aud
except
select id
from Landing.mend.order_order

select id
from Landing.mend.order_basevalues_aud
except
select id
from Landing.mend.order_basevalues


select id
from Landing.mend.order_orderline_aud
except
select id
from Landing.mend.order_orderline

select id
from Landing.mend.order_orderlinemagento_aud
except
select id
from Landing.mend.order_orderlinemagento

select id
from Landing.mend.order_orderlineabstract_aud
except
select id
from Landing.mend.order_orderlineabstract


select id
from Landing.mend.order_shippinggroup_aud
except
select id
from Landing.mend.order_shippinggroup


---------- 07 Calendar -------------------------

select id
from Landing.mend.cal_shippingmethod_aud
except
select id
from Landing.mend.cal_shippingmethod


select id
from Landing.mend.cal_defaultcalendar_aud
except
select id
from Landing.mend.cal_defaultcalendar

select id
from Landing.mend.cal_defaultwarehousesuppliercalender_aud
except
select id
from Landing.mend.cal_defaultwarehousesuppliercalender



---------- 08 Shipments -------------------------

select top 1000 id
from Landing.mend.ship_customershipment_aud
except
select id
from Landing.mend.ship_customershipment

select top 1000 id
from Landing.mend.ship_customershipmentline_aud
except
select id
from Landing.mend.ship_customershipmentline


select top 1000 id
from Landing.mend.ship_consignment_aud
except
select id
from Landing.mend.ship_consignment


---------- 09 Stock Allocation -------------------------

select id
from Landing.mend.all_orderlinestockallocationtransaction_aud
except
select id
from Landing.mend.all_orderlinestockallocationtransaction


select id
from Landing.mend.all_magentoallocation_aud
except
select id
from Landing.mend.all_magentoallocation

---------- 10 Purchases -------------------------

select *
from Landing.mend.purc_purchaseorder_aud
where id in 
	(select id
	from Landing.mend.purc_purchaseorder_aud
	except
	select id
	from Landing.mend.purc_purchaseorder)
order by createddate desc

select purchaseorderid, supplierid
from Landing.mend.purc_purchaseorder_supplier_aud
except
select purchaseorderid, supplierid
from Landing.mend.purc_purchaseorder_supplier
order by purchaseorderid, supplierid

select purchaseorderid, warehouseid
from Landing.mend.purc_purchaseorder_warehouse_aud
except
select purchaseorderid, warehouseid
from Landing.mend.purc_purchaseorder_warehouse
order by purchaseorderid, warehouseid


select *
from Landing.mend.purc_purchaseorderlineheader_aud
where id in 
	(select id
	from Landing.mend.purc_purchaseorderlineheader_aud
	except
	select id
	from Landing.mend.purc_purchaseorderlineheader)
order by createddate desc


select *
from Landing.mend.purc_purchaseorderline_aud
where id in 
	(select id
	from Landing.mend.purc_purchaseorderline_aud
	except
	select id
	from Landing.mend.purc_purchaseorderline)
order by createddate desc

---------- 11 Shortages -------------------------

select id
from Landing.mend.short_shortageheader_aud
except
select id
from Landing.mend.short_shortageheader

select *
from Landing.mend.short_shortage_aud
where id in
	(select id
	from Landing.mend.short_shortage_aud
	except
	select id
	from Landing.mend.short_shortage)
order by createdDate desc

select shortageid, purchaseorderlineid
from Landing.mend.short_shortage_purchaseorderline_aud
except
select shortageid, purchaseorderlineid
from Landing.mend.short_shortage_purchaseorderline
order by shortageid, purchaseorderlineid

select *
from Landing.mend.short_orderlineshortage_aud
where id in
	(select id
	from Landing.mend.short_orderlineshortage_aud
	except
	select id
	from Landing.mend.short_orderlineshortage)
order by createdDate desc

---------- 12 Warehouse Shipments -------------------------

select *
from Landing.mend.whship_shipment_aud
where id in 
	(select id
	from Landing.mend.whship_shipment_aud
	except
	select id
	from Landing.mend.whship_shipment)
order by createddate desc

select shipmentid, supplierid
from Landing.mend.whship_shipment_supplier_aud
except
select shipmentid, supplierid
from Landing.mend.whship_shipment_supplier
order by shipmentid, supplierid

select shipmentid, warehousestockitembatchid
from Landing.mend.whship_warehousestockitembatch_shipment_aud
except
select shipmentid, warehousestockitembatchid
from Landing.mend.whship_warehousestockitembatch_shipment
order by shipmentid, warehousestockitembatchid


select *
from Landing.mend.whship_shipmentorderline_aud
where id in 
	(select id
	from Landing.mend.whship_shipmentorderline_aud
	except
	select id
	from Landing.mend.whship_shipmentorderline)
order by createddate desc

select shipmentorderlineid, shipmentid
from Landing.mend.whship_shipmentorderline_shipment_aud
except
select shipmentorderlineid, shipmentid
from Landing.mend.whship_shipmentorderline_shipment
order by shipmentid, shipmentorderlineid

select shipmentorderlineid, warehousestockitembatchid
from Landing.mend.whship_warehousestockitembatch_shipmentorderline_aud
except
select shipmentorderlineid, warehousestockitembatchid
from Landing.mend.whship_warehousestockitembatch_shipmentorderline
order by warehousestockitembatchid, shipmentorderlineid

---------- 13 Intersite Transfers -------------------------

select *
from Landing.mend.inter_transferheader_aud
where id in
	(select id
	from Landing.mend.inter_transferheader_aud
	except
	select id
	from Landing.mend.inter_transferheader)

select transferheaderid, purchaseorderid
from Landing.mend.inter_transferpo_transferheader_aud
except
select transferheaderid, purchaseorderid
from Landing.mend.inter_transferpo_transferheader
order by transferheaderid, purchaseorderid

select *
from Landing.mend.inter_intransitbatchheader_aud
where id in
	(select id
	from Landing.mend.inter_intransitbatchheader_aud
	except
	select id
	from Landing.mend.inter_intransitbatchheader)

select *
from Landing.mend.inter_intransitstockitembatch_aud
where id in
	(select id
	from Landing.mend.inter_intransitstockitembatch_aud
	except
	select id
	from Landing.mend.inter_intransitstockitembatch)

---------- 14 SNAP Receipts -------------------------

select id
from Landing.mend.rec_snapreceipt_aud
except
select id
from Landing.mend.rec_snapreceipt

select id
from Landing.mend.rec_snapreceiptline_aud
except
select id
from Landing.mend.rec_snapreceiptline

---------- 15 VAT -------------------------

select id
from Landing.mend.vat_commodity_aud
except
select id
from Landing.mend.vat_commodity

select id
from Landing.mend.vat_dispensingservicesrate_aud
except
select id
from Landing.mend.vat_dispensingservicesrate

select id
from Landing.mend.vat_vatrating_aud
except
select id
from Landing.mend.vat_vatrating

select id
from Landing.mend.vat_vatrate_aud
except
select id
from Landing.mend.vat_vatrate

select id
from Landing.mend.vat_vatschedule_aud
except
select id
from Landing.mend.vat_vatschedule
