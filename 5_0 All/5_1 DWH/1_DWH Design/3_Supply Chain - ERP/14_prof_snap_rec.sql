
---------------------- NOTES ----------------------

---------------------------------------------------
-- Missing Rel to Supp - WH
---------------------------------------------------

	-- snapreceipt
		-- receiptID meaning on name (IP (??) - OD (??) - P (Back_To_Back) - R (Stock) - TP (Intersite))

		-- lines and lineqty meaning
		-- consignment, weight, volume important??
		-- Different Dates meaning

	-- snapreceipt
	select 
		snapreceiptid, shipmentid,
		receiptid, 
		lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail,  priority, 
		status, stockstatus, 
		ordertype, orderclass, 
		suppliername, consignmentID, 
		weight, actualWeight, volume, 
		dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
		datesfixed
	from Landing.mend.gen_rec_snapreceipt_v
	-- where shipmentid is null
	-- where dateCreated is null
	order by receiptid, dateCreated 

	select count(*)
	from Landing.mend.rec_snapreceipt_aud

	select count(*)
	from Landing.mend.gen_rec_snapreceipt_v


		select top 1000 *, 
			count(*) over (partition by snapreceiptid) rep_father,
			count(*) over (partition by shipmentid) rep_son
		from Landing.mend.rec_snapreceipt_shipment_aud
		order by rep_father desc, rep_son desc

		

		select receiptStatus, count(*)
		from Landing.mend.gen_rec_snapreceipt_v
		group by receiptStatus
		order by receiptStatus

		select receiptprocessStatus, count(*)
		from Landing.mend.gen_rec_snapreceipt_v
		group by receiptprocessStatus
		order by receiptprocessStatus

		select processDetail, count(*)
		from Landing.mend.gen_rec_snapreceipt_v
		group by processDetail
		order by processDetail

		select orderclass, count(*)
		from Landing.mend.gen_rec_snapreceipt_v
		group by orderclass
		order by orderclass



	-- snapreceiptline
	select top 1000 
		snapreceiptlineid, snapreceiptid, shipmentid, stockitemid,
		receiptid, lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail, ordertype, orderclass, dateCreated,
		
		line, skuid, 
		status, level, unitofmeasure, 
		qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected
	from Landing.mend.gen_rec_snapreceiptline_v
	-- where snapreceiptlineid is null
	-- where snapreceiptid is null
	-- where stockitemid is null
	-- where receiptid = 'P00028819-1'
	order by dateCreated desc

	select count(*)
	from Landing.mend.rec_snapreceiptline_aud

	select count(*)
	from Landing.mend.gen_rec_snapreceiptline_v


		select top 1000 *, 
			count(*) over (partition by snapreceiptlineid) rep_father,
			count(*) over (partition by snapreceiptid) rep_son
		from Landing.mend.rec_snapreceiptline_snapreceipt_aud
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by snapreceiptlineid) rep_father,
			count(*) over (partition by stockitemid) rep_son
		from Landing.mend.rec_snapreceiptline_stockitem_aud
		order by rep_father desc, rep_son desc

		select status, count(*)
		from Landing.mend.gen_rec_snapreceiptline_v
		group by status
		order by status

		select unitofmeasure, count(*)
		from Landing.mend.gen_rec_snapreceiptline_v
		group by unitofmeasure
		order by unitofmeasure

	--- 

		select receiptid, count(*)
		from Landing.mend.gen_rec_snapreceiptline_v
		group by receiptid
		order by receiptid

-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------

	-- snapreceipt (shipmentid)

	-- snapreceiptline (stockitemid)

