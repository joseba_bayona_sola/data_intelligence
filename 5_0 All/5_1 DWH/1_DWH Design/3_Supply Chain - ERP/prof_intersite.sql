
------------------------------------------------------
-- Transfer Order from AMS to YORK to CooperVision UK for Frequency 55 Aspheric

------------------------------------------------------

	select *
	from Landing.mend.gen_inter_transferheader_v
	where ordernumber_t = 'P00028987' and ordernumber_s = 'P00029314'

	select *
	from Landing.mend.gen_inter_intransitbatchheader_v
	where transfernum= 'TR00001660'
	order by createddate desc

	select top 1000 *
	from Landing.mend.gen_inter_intransitstockitembatch_th_v
	where transfernum= 'TR00001660'

------------------------------------------------------

	select *
	from Landing.mend.gen_purc_purchaseorder_v
	where ordernumber in ('P00028987', 'P00029314')
	order by ordernumber 

	select count(*) over (),
		polh.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime
	from 
			Landing.mend.gen_purc_purchaseorderlineheader_v polh
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on polh.supplierpriceid = pfpsp.supplierpriceid
	where ordernumber in ('P00028987', 'P00029314')	
	order by ordernumber, createddate desc, polh.purchaseorderlineheaderid

	select count(*) over (),
		pol.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime, si.SKU, si.description
	from		
				Landing.mend.gen_purc_purchaseorderline_v pol
			left join
				Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on pol.supplierpriceid = pfpsp.supplierpriceid
			left join
				Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	where ordernumber in ('P00028987', 'P00029314')
	order by ordernumber, createddate desc, pol.purchaseorderlineheaderid, SKU	

	----------------------------------

	select *
	from Landing.mend.gen_whship_shipment_v 
	where ordernumber in ('P00028987', 'P00029314')
	order by ordernumber, shipment_id, receiptID

	select 
		-- shipmentorderlineid, shipmentid, warehouseid, supplier_id, purchaseorderlineid, snapreceiptlineid, 
		wsib.warehousestockitembatchid,
		sol.shipment_ID, sol.orderNumber, sol.receiptID, sol.receiptNumber, 
		sol.warehouse_name, sol.supplierName, -- code, supplierID, 
		sol.status, sol.shipmentType, 
		sol.arrivedDate, sol.stockRegisteredDate, -- confirmedDate, dueDate, 
		sol.totalItemPrice, 
		sol.createdDate,
		-- line, stockItem, status_ol, 
		sol.syncStatus, sol.processed, 
		sol.quantityOrdered, sol.quantityReceived, sol.quantityAccepted, -- quantityRejected, quantityReturned, 
		sol.unitCost, 

		pol.magentoProductID_int, pol.name, pol.size, pol.unitPrice, pol.SKU, pol.description, 
		wsib.batch_id, wsib.receivedquantity, wsib.confirmedDate, wsib.productUnitCost, 
		srl.receiptStatus, srl.receiptprocessStatus, srl.status, srl.qtyReceived
	from 
			Landing.mend.gen_whship_shipmentorderline_v sol
		inner join
			(select 
				pol.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, si.SKU, si.description -- pfpsp.leadTime, 
			from		
						Landing.mend.gen_purc_purchaseorderline_v pol
					left join
						Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on pol.supplierpriceid = pfpsp.supplierpriceid
					left join
						Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid) pol on sol.purchaseorderlineid = pol.purchaseorderlineid
		left join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on sol.warehousestockitembatchid = wsib.warehousestockitembatchid
		left join
			Landing.mend.gen_rec_snapreceiptline_v srl on sol.snapreceiptlineid = srl.snapreceiptlineid

	where sol.ordernumber in ('P00028987', 'P00029314')
		-- and sol.syncStatus <> 'OK'
	order by sol.ordernumber, pol.purchaseorderlineheaderid, pol.SKU, sol.shipment_id, sol.receiptID, sol.line

	select *
	from Landing.mend.gen_wh_warehousestockitembatch_v
	where warehousestockitembatchid in (43910096368108534, 43910096368100125)
