
---------------------- NOTES ----------------------

	-- purchaseorder
		-- supplier_id can be NULL / can be MANY
		-- ordernumber not unique: many rows per ordernumber (PO to MANY suppliers, MANY warehouse - PO to MANY source - OTHER) - Look like wrong data (2016) - SHOULD BE 1 PO ROW PER PO NUMBER, SUPPLIER
		-- source meaning: Back_To_Back vs Manual
		-- meaning for itemcost - totalprice (always same)

	-- purchaseorder
	select count(*) over (partition by ordernumber) num_rep, count(*) over (partition by ordernumber, supplier_id) num_rep2,
		purchaseorderid, warehouseid, supplier_id, 
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype,
		leadtime, 
		itemcost, totalprice, formattedprice, 
		duedate, receiveddate, createddate, 
		approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby
	from Landing.mend.gen_purc_purchaseorder_v 
	-- where supplier_id is null
	-- where supplierID is null
	-- where source = 'Back_To_Back' -- Manual - Back_To_Back
	-- where status <> 'Cancelled' -- Drafted - Approved - Submitted - Confirmed - Completed - Backordered - Cancelled
	-- where ordernumber in ('P00006968', 'P00008215', 'P00002566', 'P00032196', 'P00032254', 'P00033005', 'P00008214', 'P00005039')
	-- where itemcost <> totalprice
	order by num_rep2 desc, num_rep desc, ordernumber desc, createddate desc
	-- order by itemcost desc, createddate

	-- MISSING ATTRIBUTE
	select top 1000 id, ordernumber -- , supplierreference
	from Landing.mend.purc_purchaseorder_aud

	select count(*)
	from Landing.mend.purc_purchaseorder

	select count(*)
	from Landing.mend.purc_purchaseorder_aud

	select count(*)
	from Landing.mend.gen_purc_purchaseorder_v

		select top 1000 count(*) over (partition by purchaseorderid) num_rep, *
		from Landing.mend.gen_purc_purchaseorder_v
		order by num_rep desc, purchaseorderid

		select top 1000 *, 
			count(*) over (partition by purchaseorderid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.purc_purchaseorder_warehouse_aud
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by purchaseorderid) rep_father,
			count(*) over (partition by supplierid) rep_son
		from Landing.mend.purc_purchaseorder_supplier_aud
		order by rep_father desc, rep_son desc


		select source, count(*)
		from Landing.mend.gen_purc_purchaseorder_v
		group by source
		order by source

		select status, count(*)
		from Landing.mend.purc_purchaseorder_aud
		group by status
		order by status

		select potype, count(*)
		from Landing.mend.purc_purchaseorder_aud
		group by potype
		order by potype

		-- 

		select code, warehouse_name, count(*), sum(itemcost)
		from Landing.mend.gen_purc_purchaseorder_v
		group by code, warehouse_name
		order by code, warehouse_name

		select supplierID, supplierName, count(*), sum(itemcost)
		from Landing.mend.gen_purc_purchaseorder_v
		group by supplierID, supplierName
		order by supplierID, supplierName

		select ordernumber, count(*), count(distinct supplier_id), count(distinct warehouseid), sum(itemcost)
		from Landing.mend.gen_purc_purchaseorder_v
		-- where status <> 'Cancelled'
		group by ordernumber
		having count(*) > 1
		order by count(distinct supplier_id) desc, count(distinct warehouseid) desc, ordernumber desc

		select ordernumber, count(*), sum(itemcost)
		from Landing.mend.purc_purchaseorder_aud
		where status <> 'Cancelled'
		group by ordernumber
		having count(*) > 1
		order by count(*) desc, ordernumber desc

		---
		select *
		from Landing.mend.purc_purchaseorder_aud_v
		where num_records <> 1
		order by ordernumber desc, createddate desc
		---

---------------------- NOTES ----------------------

	-- purchaseorderlineheader
		-- some purchase_order without purchase order LH
		-- supplierpriceid can be NULL -- Line Headers with HAS_ERRORS - Missing Price
		-- po totalprice == sum_total_price_lh ??
		-- meaning lines (sum of PO L) - items (sum of qty in PO L)


	-- purchaseorderlineheader
	select 
		purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype, createddate,
		leadtime, 
		itemcost, totalprice, 

		status_lh, problems, 
		lines, items, total_price_lh, 
		sum(total_price_lh) over (partition by purchaseorderid) sum_total_price_lh
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	-- where purchaseorderlineheaderid is null
	-- where supplierpriceid is null
	-- where ordernumber in ('P00006968', 'P00008215', 'P00002566', 'P00032196', 'P00032254', 'P00033005', 'P00008214', 'P00005039')
	order by ordernumber desc, createddate desc, code, supplierID

	select top 1000 id
	from Landing.mend.purc_purchaseorderlineheader

	select count(*)
	from Landing.mend.purc_purchaseorderlineheader

	select count(*)
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	where supplierpriceid is null

		select top 1000 *, 
			count(*) over (partition by purchaseorderlineheaderid) rep_father,
			count(*) over (partition by purchaseorderid) rep_son
		from Landing.mend.purc_purchaseorderlineheader_purchaseorder_aud
		order by rep_father desc, rep_son desc

	select count(purchaseorderlineheaderid), count(purchaseorderid), 
		count(productfamilyid), count(packsizeid), count(supplierpriceid)
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	

	select ordernumber, createddate, count(*)
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	group by ordernumber, createddate
	order by createddate desc


		select status, count(distinct purchaseorderid), count(distinct purchaseorderlineheaderid), count(*)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		group by status
		order by status

		-- 

		select status_lh, count(distinct purchaseorderid), count(distinct purchaseorderlineheaderid), count(*)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		group by status_lh
		order by status_lh

		select problems, count(distinct purchaseorderid), count(distinct purchaseorderlineheaderid), count(*)
		from Landing.mend.gen_purc_purchaseorderlineheader_v
		group by problems
		order by problems


		---
		select *
		from Landing.mend.purc_purchaseorderlineheader_aud_v
		where num_records <> 1
		order by id desc, createddate desc, ins_ts
		---


---------------------- NOTES ----------------------

	-- purchaseorderline
		-- some PO HL without PO L - the PO HL have 0 lines and 0 total_price_lh
		-- some PO L witouth PO HL -- seems like wron PO L records from 2016
		-- stockitemid can be NULL -- WHY???
		-- pohl total_price_lh == sum lineprice ??
		-- meaning different qty values

	-- purchaseorderline
	select top 1000 purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid, stockitemid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype, createddate,
		leadtime, 
		itemcost, totalprice, 
		status_lh, problems, 
		lines, items, total_price_lh, 
		
		po_lineID, 
		quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
		lineprice, 
		backtobackduedate
	from Landing.mend.gen_purc_purchaseorderline_v
	-- where purchaseorderlineid is null and purchaseorderlineheaderid is not null
	-- where purchaseorderlineid is not null and purchaseorderlineheaderid is null
	-- where purchaseorderlineheaderid is null
	-- where stockitemid is null and purchaseorderlineid is not null
	-- where ordernumber in ('P00006968', 'P00008215', 'P00002566', 'P00032196', 'P00032254', 'P00033005', 'P00008214', 'P00005039')
	order by ordernumber desc, createddate desc, code, supplierID, purchaseorderlineheaderid, purchaseorderlineid desc

	select top 1000 id
	from Landing.mend.purc_purchaseorderline_aud
	
	select count(*)
	from Landing.mend.purc_purchaseorderline

	select count(*)
	from Landing.mend.gen_purc_purchaseorderline_v

		select count(distinct purchaseorderid), count(distinct purchaseorderlineheaderid), count(purchaseorderlineid), count(*)
		from Landing.mend.gen_purc_purchaseorderline_v
		-- where purchaseorderlineheaderid is null
		-- where purchaseorderlineid is null
		where stockitemid is null

		select top 1000 *, 
			count(*) over (partition by purchaseorderlineid) rep_father,
			count(*) over (partition by purchaseorderlineheaderid) rep_son
		from Landing.mend.purc_purchaseorderline_purchaseorderlineheader
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by purchaseorderlineid) rep_father,
			count(*) over (partition by stockitemid) rep_son
		from Landing.mend.purc_purchaseorderline_stockitem_aud
		order by rep_father desc, rep_son desc	

	select ordernumber, createddate, count(*)
	from Landing.mend.gen_purc_purchaseorderline_v
	group by ordernumber, createddate
	order by createddate desc

		select status, count(distinct purchaseorderid), count(distinct purchaseorderlineheaderid), count(purchaseorderlineid), count(*)
		from Landing.mend.gen_purc_purchaseorderline_v
		group by status
		order by status

		select status_lh, count(*)
		from Landing.mend.gen_purc_purchaseorderline_v
		group by status_lh
		order by status_lh

		select problems, count(*)
		from Landing.mend.gen_purc_purchaseorderline_v
		group by problems
		order by problems

		---
		select top 1000 *
		from Landing.mend.purc_purchaseorderline_aud_v
		where num_records <> 1 and num_records > 2
		order by id desc, createddate desc, ins_ts
		---

-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------

	-- purchaseorderlineheader (productfamilyid, packsizeid, supplierpriceid)
	select count(*) over (),
		polh.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime
	from 
			Landing.mend.gen_purc_purchaseorderlineheader_v polh
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on polh.supplierpriceid = pfpsp.supplierpriceid
	-- where magentoProductID_int in (1083, 1317)
	-- where ordernumber in ('P00006968', 'P00008215', 'P00002566', 'P00032196', 'P00032254', 'P00033005', 'P00008214', 'P00005039')
	order by ordernumber desc, createddate desc, polh.purchaseorderlineheaderid
		

	-- purchaseorderline (stockitemid)
	select top 1000 count(*) over (),
		pol.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime, si.SKU, si.stockitemdescription
	from		
				Landing.mend.gen_purc_purchaseorderline_v pol
			left join
				Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on pol.supplierpriceid = pfpsp.supplierpriceid
			left join
				Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	-- where pfpsp.magentoProductID_int in (1083, 1317) and pol.stockitemid= 32088147345065054
	-- where ordernumber in ('P00006968', 'P00008215', 'P00002566', 'P00032196', 'P00032254', 'P00033005', 'P00008214', 'P00005039')
	order by ordernumber desc, createddate desc, pol.purchaseorderlineheaderid, si.stockitemdescription

-------------------------------------------------------------------------------------

	select *
	from Landing.mend.purc_purchaseorder_aud_v
	where id = 41376821576531329
	order by ordernumber desc, createddate desc
				
	select *
	from Landing.mend.purc_purchaseorder_supplier_aud
	where purchaseorderid = 41376821576531329

	select *
	from Landing.mend.purc_purchaseorderlineheader_purchaseorder_aud
	where purchaseorderid = 41376821576531329
