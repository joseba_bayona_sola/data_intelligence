

	select warehouseid, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore
	from Landing.mend.gen_wh_warehouse_v
	order by code



---------------------- NOTES ----------------------
	-- warehousestockitem
		-- 12 SI without WH SI (the ones from StockItem) 
		-- id_string is NOT UNIQUE NULL values on 1235, ... + Same id_string for WH SI in diff WH (York - Girona)

		-- Meaning of attributes: 
			-- availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty
			-- stocked - outstandingallocation - onhold - stockingmethod (STOCKED vs BACKTOBACK)

	-- warehousestockitem
	select top 2000 productfamilyid, productid, stockitemid, warehousestockitemid,
		magentoProductID_int, magentoProductID,
		magentoSKU, name, SKU_product, packSize, SKU, stockitemdescription,
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		actualstockqty, availableqty, outstandingallocation, allocatedstockqty, onhold, dueinqty, forwarddemand, 
		stocked,
		stockingmethod
	from Landing.mend.gen_wh_warehousestockitem_v
	where warehousestockitemid is null
	-- where magentoProductID_int in (1083, 1317)
	-- where stockitemid = 32088147345256194
	-- where id_string is null
	-- where id_string = '01093B3D4BJ0000110000031York'
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packsize, code

	select count(*)
	from Landing.mend.wh_warehousestockitem_aud

	select count(*)
	from Landing.mend.gen_wh_warehousestockitem_v

		select top 1000 *, 
			count(*) over (partition by warehousestockitemid) rep_father,
			count(*) over (partition by stockitemid) rep_son
		from Landing.mend.wh_warehousestockitem_stockitem_aud
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by warehousestockitemid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.wh_located_at_aud
		order by rep_father desc, rep_son desc

	select stockingmethod, count(*)
	from Landing.mend.gen_wh_warehousestockitem_v
	group by stockingmethod
	order by stockingmethod

	select magentoProductID_int, magentoSKU, name, count(*)
	from Landing.mend.gen_wh_warehousestockitem_v
	group by magentoProductID_int, magentoSKU, name
	order by magentoProductID_int, magentoSKU, name

	select magentoProductID_int, magentoSKU, packSize, name, count(*)
	from Landing.mend.gen_wh_warehousestockitem_v
	group by magentoProductID_int, magentoSKU, packSize, name
	order by magentoProductID_int, magentoSKU, packSize, name

	select warehouse_name, count(*)
	from Landing.mend.gen_wh_warehousestockitem_v
	group by warehouse_name
	order by warehouse_name

	select top 2000 id_string, count(*), count(*) over ()
	from Landing.mend.gen_wh_warehousestockitem_v
	group by id_string
	having count(*) > 1
	order by count(*) desc, id_string

		select magentoProductID_int, magentoSKU, packSize, name, count(*)
		from Landing.mend.gen_wh_warehousestockitem_v
		where id_string is null
		group by magentoProductID_int, magentoSKU, packSize, name
		order by magentoProductID_int, magentoSKU, packSize, name

	--- 

			select *
			from Landing.mend.wh_warehousestockitem_aud_hist
			where id = 52635820644988201
			order by ins_ts

			select *
			from Landing.mend.wh_warehousestockitem_aud_v
			where id = 52635820644988201
			order by ins_ts
	
---------------------- NOTES ----------------------

	-- warehousestockitembatch
		-- XXX WH SI without WH SI B (those who never get required / needed (Girona + Not Usual) 
		-- batch_id is UNIQUE 

		-- Meaning of attributes: 
				-- fully_allocated, fullyissued
				-- receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity
				-- productUnitCost, totalUnitCost

	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		receiptlineid, batchstockmovement_f, batchrepair_f,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string, 
		
		batch_id,
		fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, -- wsib.forwardDemand, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency
	from Landing.mend.gen_wh_warehousestockitembatch_v
	-- where warehousestockitembatchid is null
	where magentoProductID_int in (1083, 1317) and warehousestockitemid = 52635820644988201
	-- where code = 9 and warehousestockitembatchid is not null
	-- where stockitemid = 32088147345256194
	order by magentoProductID_int, stockitemdescription, batch_id


	select count(*)
	from Landing.mend.wh_warehousestockitembatch_aud

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v

		select top 1000 *, 
			count(*) over (partition by warehousestockitembatchid) rep_father,
			count(*) over (partition by warehousestockitemid) rep_son
		from Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud
		order by rep_father desc, rep_son desc


	select status, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by status
	order by status

	select exchangeRate, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by exchangeRate
	order by exchangeRate

	select currency, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by currency
	order by currency

	select magentoProductID_int, magentoSKU, name, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by magentoProductID_int, magentoSKU, name
	order by magentoProductID_int, magentoSKU, name

	select magentoProductID_int, magentoSKU, packSize, name, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by magentoProductID_int, magentoSKU, packSize, name
	order by magentoProductID_int, magentoSKU, packSize, name

	select top 2000 batch_id, count(*), count(*) over ()
	from Landing.mend.gen_wh_warehousestockitembatch_v
	group by batch_id
	having count(*) > 1
	order by count(*) desc, batch_id

		---  
			select top 1000 *
			from Landing.mend.wh_warehousestockitembatch_aud_hist
			where id in 
				(select warehousestockitembatchid
				from Landing.mend.gen_wh_warehousestockitembatch_v
				where warehousestockitemid = 52635820644988201)
			order by id, ins_ts

			select *
			from Landing.mend.wh_warehousestockitembatch_aud_v
			where id in 
				(select warehousestockitembatchid
				from Landing.mend.gen_wh_warehousestockitembatch_v
				where warehousestockitemid = 52635820644988201)
			order by id, ins_ts






---------------------- NOTES ----------------------

	-- repairstockbatch
		-- STILL IN USE: No new records from 2017-09

		-- meaning of attributes:
			-- reference: UNIQUE
			-- batchtransaction_id = batch_id ??
			-- oldallocation - new allocation and oldissue - new issue

	-- repairstockbatch
	select repairstockbatchsid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, 
		batchtransaction_id, reference, date,
		oldallocation, newallocation, oldissue, newissue, processed, 

		magentoProductID_int, magentoProductID,
		name, stockitemdescription, 
		code, warehouse_name, id_string, 
		batch_id
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	where magentoProductID_int in (1083, 1317) -- and warehousestockitemid = 52635820644988201
	-- where batchtransaction_id = 778136
	order by magentoProductID_int, stockitemdescription, batch_id, date desc
	-- order by date desc

	select count(*)
	from Landing.mend.wh_repairstockbatchs_aud

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v

		select top 1000 *, 
			count(*) over (partition by repairstockbatchsid) rep_father,
			count(*) over (partition by warehousestockitembatchid) rep_son
		from Landing.mend.wh_repairstockbatchs_warehousestockitembatch_aud
		order by rep_father desc, rep_son desc

	select reference, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	group by reference
	order by count(*) desc, reference

	select batchtransaction_id, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	group by batchtransaction_id
	order by count(*) desc, batchtransaction_id

	select batchtransaction_id, count(*), count(distinct stockitemid), count(distinct warehousestockitemid), count(distinct warehousestockitembatchid)
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	group by batchtransaction_id
	order by count(distinct warehousestockitembatchid) desc, count(distinct warehousestockitemid) desc, count(distinct stockitemid) desc, count(*) desc, batchtransaction_id


---------------------- NOTES ----------------------

	-- stockmovement
		-- Why some Stock Movement without Warehouse Stock Item

		-- meaning of attributes
			-- moveid - movelineid
			-- stockadjustment
			-- movementtype - movetype - class
			-- reasonid
			-- status
			-- qtyactioned

	-- stockmovement
	select stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		stockmovement_createdate, stockmovement_closeddate,

		magentoProductID_int, magentoProductID,
		name, stockitemdescription, 
		code, warehouse_name, id_string
	
	from Landing.mend.gen_wh_stockmovement_v
	-- where warehousestockitemid is null
	-- where magentoProductID_int in (1083, 1317) -- and warehousestockitemid = 52635820644988201
	-- where moveid in ('SHOL000158', 'DIS000116')
	where moveid = ''
	-- where _class is null
	order by magentoProductID_int, stockitemdescription, code, moveid, stockmovement_createdate desc
	-- order by dateCreated
	-- order by moveid, movelineid

	select count(*)
	from Landing.mend.wh_stockmovement_aud

	select count(*), count(distinct moveid), count(distinct movelineid)
	from Landing.mend.gen_wh_stockmovement_v

	select movementtype, count(*), min(stockmovement_createdate), max(stockmovement_createdate)
	from Landing.mend.gen_wh_stockmovement_v
	where moveid = ''
	group by movementtype
	order by movementtype

		select top 1000 *, 
			count(*) over (partition by stockmovementid) rep_father,
			count(*) over (partition by warehousestockitemid) rep_son
		from Landing.mend.wh_stockmovement_warehousestockitem_aud
		order by rep_father desc, rep_son desc

	select moveid, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	where magentoProductID_int = 1083
	group by moveid
	having count(*) > 1
	order by count(*) desc, moveid

	select movelineid, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	-- where magentoProductID_int = 1083
	group by movelineid
	having count(*) > 1
	order by count(*) desc, movelineid

	select stockadjustment, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by stockadjustment
	order by stockadjustment

	select movetype, _class, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movetype, _class
	order by movetype, _class

	select movetype, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movetype
	order by movetype

	select movementtype, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movementtype
	order by movementtype

	select reasonid, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by reasonid
	order by reasonid

	select status, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by status
	order by status

	select fromstate, tostate, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by fromstate, tostate
	order by fromstate, tostate

	select movetype, movementtype, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movetype, movementtype
	order by movetype, movementtype

	select movetype, movementtype, stockadjustment, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movetype, movementtype, stockadjustment
	order by movetype, movementtype, stockadjustment

	select movetype, movementtype, fromstate, tostate, count(*)
	from Landing.mend.gen_wh_stockmovement_v
	group by movetype, movementtype, fromstate, tostate
	order by movetype, movementtype, fromstate, tostate


---------------------- NOTES ----------------------

	-- batchstockmovement
		-- Why Stock Movement without Batch Stock Movement
			-- Some without Warehouse Stock Item
		-- Why Batch Stock Movement without Stock Movement: Over Allocated Batch: Auto Fix / Auto Offset

		-- Stock Movement to Many Batch Stock Movement rel: A SM can be touching different Batches 

		-- meaning of attributes
			-- batchstockmovement_id: UNIQUE

	-- batchstockmovement
	select count(*) over (partition by stockmovementid) num_rep,
		batchstockmovementid, stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		moveid, movelineid, batchstockmovement_id, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		stockmovement_createdate, stockmovement_closeddate,
		batchstockmovement_createdate, batchstockmovementtype, quantity, comment,
		magentoProductID_int, magentoProductID, 
		name, stockitemdescription, 
		code, warehouse_name, id_string, 
		batch_id

	from Landing.mend.gen_wh_batchstockmovement_v
	-- where batchstockmovementid is null
	-- where stockmovementid is null
	-- where magentoProductID_int in (1083, 1317) --  and warehousestockitemid = 52635820644988201
	-- where moveid in ('SHOL000158', 'DIS000116')
	order by num_rep desc, magentoProductID_int, stockitemdescription, code, moveid, stockmovement_createdate desc
	-- order by dateCreated
	-- order by moveid, movelineid


	select count(*)
	from Landing.mend.wh_batchstockmovement_aud

	select count(*), count(distinct moveid), count(distinct movelineid)
	from Landing.mend.gen_wh_batchstockmovement_v
	where moveid <> '' and moveid is not null
	-- where batchstockmovementid is null
	where stockmovementid is null

		select top 1000 *, 
			count(*) over (partition by batchstockmovementid) rep_father,
			count(*) over (partition by stockmovementid) rep_son
		from Landing.mend.wh_batchstockmovement_stockmovement_aud
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by batchstockmovementid) rep_father,
			count(*) over (partition by warehousestockitembatchid) rep_son
		from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
		order by rep_father desc, rep_son desc

	select moveid, count(*), count(distinct stockmovementid)
	from Landing.mend.gen_wh_batchstockmovement_v
	where magentoProductID_int = 1083
	group by moveid
	having count(*) > 1
	order by count(distinct stockmovementid) desc, count(*) desc, moveid

	select movelineid, count(*), count(distinct stockmovementid)
	from Landing.mend.gen_wh_batchstockmovement_v
	where magentoProductID_int = 1083
	group by movelineid
	having count(*) > 1
	order by count(distinct stockmovementid) desc, count(*) desc, movelineid

	select batchstockmovement_id, count(*)
	from Landing.mend.gen_wh_batchstockmovement_v
	group by batchstockmovement_id
	having count(*) > 1
	order by count(*) desc, batchstockmovement_id

	select batchstockmovementtype, count(*)
	from Landing.mend.gen_wh_batchstockmovement_v
	group by batchstockmovementtype
	order by batchstockmovementtype

	select magentoProductID, name, count(*)
	from Landing.mend.gen_wh_batchstockmovement_v
	group by magentoProductID, name
	order by count(*) desc, magentoProductID, name


----------------------------------------------------------------------

	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid,
		magentoProductID_int, magentoProductID,
		magentoSKU, name, SKU_product, description, packSize, SKU, stockitemdescription,
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod
	from Landing.mend.gen_wh_warehousestockitem_v 
	where stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO-9.00 PS30'
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string, 
		batch_id,
		fullyallocated, fullyIssued, status, autoadjusted, 
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, registeredQuantity,
		groupFIFODate, arriveddate, confirmedDate, stockregisteredDate, receiptCreatedDate,
		productUnitCost, totalUnitCost, exchangeRate, currency
	from Landing.mend.gen_wh_warehousestockitembatch_v 
	where magentoProductID_int = 1083
		and stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO-9.00 PS30'
		and	warehouse_name = 'York'
	order by magentoProductID_int, stockitemdescription, code, batch_id DESC

	select top 1000 *
	from Landing.mend.wh_warehousestockitembatch_aud
	where batch_id in (1485789, 1365287)

	select top 1000 *
	from Landing.mend.gen_wh_warehousestockitembatch_v 
	where currency = 'EUR'

	select stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		stockmovement_createdate, stockmovement_closeddate,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string

	from Landing.mend.gen_wh_stockmovement_v
	where magentoProductID_int = 1083
		and stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO-9.00 PS30'
		and	warehouse_name = 'York'
	order by stockmovement_createdate desc



--------------------------------------------------------------------------------

---------------------- NOTES ----------------------

	-- batchstockissue
	-- XXX WH SI B that don't have BS ISSUE (those who still haven't been allocated) 

	-- batchstockissue
	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, batchstockissueid,
		magentoProductID_int, magentoProductID, 
		name, stockitemdescription,
		code, warehouse_name, id_string, 
		batch_id,
		
		issueid, issuedquantity_issue, issue_date 
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v
	-- where batchstockissueid is null
	where magentoProductID_int in (1083, 1317) and warehousestockitemid = 52635820644988201
	-- where warehousestockitembatchid = 43910096367646559
	order by magentoProductID_int, stockitemdescription, batch_id, issue_date

	select count(*)
	from Landing.mend.wh_batchstockissue_aud

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v

		select top 1000 *, 
			count(*) over (partition by batchstockissueid) rep_father,
			count(*) over (partition by warehousestockitembatchid) rep_son
		from Landing.mend.wh_batchstockissue_warehousestockitembatch_aud
		order by rep_father desc, rep_son desc


	select magentoProductID_int, name, count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v
	group by magentoProductID_int, name
	order by magentoProductID_int, name

---------------------- NOTES ----------------------

	-- batchstockallocation
		-- allocation - issue relation: 
			-- Allocation with no issues
			-- Allocation with many issues
			-- Issue always 1 to 1

		-- meaning of attirbutes
			-- fullyissued - cancelled

	-- batchstockallocation

	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, batchstockissueid, batchstockallocationid,
		magentoProductID_int,
		magentoSKU, name, SKU_product, packSize, SKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		batch_id,
		fullyallocated, fullyIssued, status, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, 
		confirmedDate, 
		productUnitCost, totalUnitCost, exchangeRate, currency, 
		issueid, createddate, issuedquantity_issue, 
		
		fullyissued_alloc, cancelled, allocatedquantity_alloc, allocatedunits, issuedquantity_alloc, count(*) over ()
	from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v
	-- where batchstockallocationid is null -- and batchstockissueid is not null
	-- where batchstockissueid is null and batchstockallocationid is not null
	where magentoProductID_int in (1083, 1317) and warehousestockitemid = 52635820644988201
	-- where batchstockallocationid = 78812993505926713
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packsize, code, batch_id, createddate

	select *
	from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v
	where batchstockallocationid in (78812993514496002, 78812993524723119)

	select count(*)
	from Landing.mend.wh_batchstockallocation_aud

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v

		select top 1000 *, 
			count(*) over (partition by batchstockallocationid) rep_father,
			count(*) over (partition by batchstockissueid) rep_son
		from Landing.mend.wh_batchstockissue_batchstockallocation_aud
		order by rep_son desc, rep_father desc, batchstockissueid
