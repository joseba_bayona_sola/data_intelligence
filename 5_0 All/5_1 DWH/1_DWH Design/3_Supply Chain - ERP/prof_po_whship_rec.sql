	
	select *
	from Landing.mend.gen_purc_purchaseorder_v
	where ordernumber in ('P00033055', 'P00032773', 'P00030609', 'P00009721')
	order by ordernumber desc
	 
	select count(*) over (),
		polh.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime
	from 
			Landing.mend.gen_purc_purchaseorderlineheader_v polh
		left join
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on polh.supplierpriceid = pfpsp.supplierpriceid
	where ordernumber in ('P00033055', 'P00032773', 'P00030609', 'P00009721')	
	order by ordernumber desc, createddate desc, polh.purchaseorderlineheaderid

	select count(*) over (),
		pol.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, pfpsp.leadTime, si.SKU, si.description
	from		
				Landing.mend.gen_purc_purchaseorderline_v pol
			left join
				Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on pol.supplierpriceid = pfpsp.supplierpriceid
			left join
				Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid
	where ordernumber in ('P00033055', 'P00032773', 'P00030609', 'P00009721')
	order by ordernumber desc, createddate desc, pol.purchaseorderlineheaderid, pol.purchaseorderlineid	

	----------------------------------

	select *
	from Landing.mend.gen_whship_shipment_v 
	where ordernumber in ('P00033055', 'P00009721') or shipment_id in ('IP00030609', 'R00002542') 
	order by ordernumber desc, shipment_id, receiptID

	select 
		-- shipmentorderlineid, shipmentid, warehouseid, supplier_id, purchaseorderlineid, snapreceiptlineid, warehousestockitembatchid,
		sol.shipment_ID, sol.orderNumber, sol.receiptID, sol.receiptNumber, 
		sol.warehouse_name, sol.supplierName, -- code, supplierID, 
		sol.status, sol.shipmentType, 
		sol.arrivedDate, sol.stockRegisteredDate, -- confirmedDate, dueDate, 
		sol.totalItemPrice, 
		sol.createdDate,
		-- line, stockItem, 
		sol.status_ol, sol.syncStatus, sol.processed, 
		sol.quantityOrdered, sol.quantityReceived, sol.quantityAccepted, -- quantityRejected, quantityReturned, 
		sol.unitCost, 

		pol.magentoProductID_int, pol.name, pol.size, pol.unitPrice, pol.SKU, pol.description, 
		wsib.batch_id, wsib.receivedquantity, wsib.confirmedDate, wsib.productUnitCost, 
		srl.receiptStatus, srl.receiptprocessStatus, srl.status, srl.qtyReceived
	from 
			Landing.mend.gen_whship_shipmentorderline_v sol
		inner join
			(select 
				pol.*, pfpsp.magentoProductID_int, pfpsp.name, pfpsp.size, pfpsp.unitPrice, pfpsp.currencyCode, si.SKU, si.description -- pfpsp.leadTime, 
			from		
						Landing.mend.gen_purc_purchaseorderline_v pol
					left join
						Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpsp on pol.supplierpriceid = pfpsp.supplierpriceid
					left join
						Landing.mend.gen_prod_stockitem_v si on pol.stockitemid = si.stockitemid) pol on sol.purchaseorderlineid = pol.purchaseorderlineid
		left join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on sol.warehousestockitembatchid = wsib.warehousestockitembatchid
		left join
			Landing.mend.gen_rec_snapreceiptline_v srl on sol.snapreceiptlineid = srl.snapreceiptlineid

	where (sol.ordernumber in ('P00033055', 'P00009721') or shipment_id in ('IP00030609', 'R00002542'))
		-- and sol.syncStatus <> 'OK'
	order by sol.ordernumber desc, pol.purchaseorderlineheaderid, sol.purchaseorderlineid, sol.shipment_id, sol.receiptID, sol.line
	-- order by ordernumber desc, shipment_id, receiptID, line


	----------------------------------

	select whs.shipment_ID, whs.orderNumber, whs.status,
		sr.*
	from 
			Landing.mend.gen_rec_snapreceipt_v sr
		inner join
			(select shipmentid, shipment_ID, orderNumber, status
			from Landing.mend.gen_whship_shipment_v 
			where ordernumber in ('P00033055', 'P00009721') or shipment_id in ('IP00030609', 'R00002542')) whs on sr.shipmentid = whs.shipmentid 
	order by whs.orderNumber desc, whs.shipment_id, receiptID


	select whs.shipment_ID, whs.orderNumber, whs.status,
		srl.*
	from 
			Landing.mend.gen_rec_snapreceiptline_v srl
		inner join
			(select shipmentid, shipment_ID, orderNumber, status
			from Landing.mend.gen_whship_shipment_v 
			where ordernumber in ('P00033055', 'P00009721') or shipment_id in ('IP00030609', 'R00002542')) whs on srl.shipmentid = whs.shipmentid 
	order by whs.orderNumber desc, whs.shipment_id, receiptID
