
			select id, 
				incrementID, magentoOrderID, orderStatus, 
				createdDate, changedDate
			into DW_GetLenses_jbs.dbo.erp_oh
			from openquery(MENDIX_DB_RESTORE,'
					select id, 
						incrementID, magentoOrderID, orderStatus, 
						createdDate, changedDate  
					from public."orderprocessing$order"')

-------------------------------------------------

select oh1.*
from 
		DW_GetLenses_jbs.dbo.erp_oh oh1
	left join
		Landing.mend.order_order_aud oh2 on oh1.id = oh2.id
where oh2.id is null
order by oh1.id

select year(oh1.createdDate), month(oh1.createdDate), day(oh1.createdDate), count(*)
from 
		DW_GetLenses_jbs.dbo.erp_oh oh1
	left join
		Landing.mend.order_order_aud oh2 on oh1.id = oh2.id
where oh2.id is null
group by year(oh1.createdDate), month(oh1.createdDate), day(oh1.createdDate)
order by year(oh1.createdDate), month(oh1.createdDate), day(oh1.createdDate)

-------------------------------------------------

			select id, status, packsOrdered, packsAllocated, packsShipped
			into DW_GetLenses_jbs.dbo.erp_ol
			from openquery(MENDIX_DB_RESTORE,'
					select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped
					from public."orderprocessing$orderline" ol')

			select orderprocessing$orderlineid orderlineid, orderprocessing$orderid orderid
			into DW_GetLenses_jbs.dbo.erp_ol_oh
			from openquery(MENDIX_DB_RESTORE,'
					select olo."orderprocessing$orderlineid", olo."orderprocessing$orderid"
					from public."orderprocessing$orderline_order" olo')

select 
	ol1.*
	-- ol2.*
from 
		DW_GetLenses_jbs.dbo.erp_ol ol1
	full join
		Landing.mend.order_orderline_aud ol2 on ol1.id = ol2.id
where ol2.id is null
-- where ol1.id is null
order by ol1.id, ol2.id

select ol_oh.*, oh.*, oh2.id, oh2.idETLBatchRun, oh2.ins_ts
from
		(select ol1.id
		from 
				DW_GetLenses_jbs.dbo.erp_ol ol1
			full join
				Landing.mend.order_orderline_aud ol2 on ol1.id = ol2.id
		where ol2.id is null) ol 
	inner join
		DW_GetLenses_jbs.dbo.erp_ol_oh ol_oh on ol.id = ol_oh.orderlineid
	inner join
		DW_GetLenses_jbs.dbo.erp_oh oh on ol_oh.orderid = oh.id
	left join
		Landing.mend.order_order_aud oh2 on ol_oh.orderid = oh2.id
--where oh2.id is null
order by ol_oh.orderlineid

select year(oh.createdDate), month(oh.createdDate), day(oh.createdDate), count(*), count(distinct oh.id)
from
		(select ol1.id
		from 
				DW_GetLenses_jbs.dbo.erp_ol ol1
			full join
				Landing.mend.order_orderline_aud ol2 on ol1.id = ol2.id
		where ol2.id is null) ol 
	inner join
		DW_GetLenses_jbs.dbo.erp_ol_oh ol_oh on ol.id = ol_oh.orderlineid
	inner join
		DW_GetLenses_jbs.dbo.erp_oh oh on ol_oh.orderid = oh.id
	left join
		Landing.mend.order_order_aud oh2 on ol_oh.orderid = oh2.id
-- where oh2.id is not null
group by year(oh.createdDate), month(oh.createdDate), day(oh.createdDate)
order by year(oh.createdDate), month(oh.createdDate), day(oh.createdDate)

