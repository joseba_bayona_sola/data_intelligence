
-- erp_all_oh
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_oh_2
-- where entity_id is null
where magentoOrderID_int is null
-- where entity_id is not null and magentoOrderID_int is not null
-- where orderStatus = '' -- Completed - Pending - Cancelled
-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
-- where allocationStatus = 'Full' -- Full - Partial - None
-- where allocationStrategy = '' -- AutomaticFromStock - Manual
order by invoice_date, erp_sync_date

	select count(*), count(distinct order_id_bk), count(distinct orderid),
		min(invoice_date), max(invoice_date), 
		min(erp_sync_date), max(erp_sync_date)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by status
	order by status

	select orderStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by orderStatus
	order by orderStatus

	select _type, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by _type
	order by _type

	select allocationStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by allocationStatus
	order by allocationStatus

	select allocationStrategy, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by allocationStrategy
	order by allocationStrategy

	select shipmentStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	group by shipmentStatus
	order by shipmentStatus

	select top 1000 count(*) over (partition by orderid) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	where orderid is not null
	order by num_rep desc, invoice_date

	select top 1000 count(*) over (partition by order_id_bk) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_oh_2
	where order_id_bk is not null
	order by num_rep desc, invoice_date

---------------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_dwh_2
where invoice_date is null
order by order_date

	select count(*), count(distinct order_id_bk)
	from DW_GetLenses_jbs.dbo.erp_all_ol_dwh_2

------------------------------------------------------------------------

-- erp_all_ship
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ship_2
where customershipmentid is null

	select count(*), count(distinct orderid), count(distinct customershipmentid)
	from DW_GetLenses_jbs.dbo.erp_all_ship_2
	-- where customershipmentid is not null	
	where customershipmentid is null

	select shippingDescription, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ship_2
	group by shippingDescription
	order by shippingDescription

	select top 1000 oh.orderid, oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		sh.shipmentnumber, sh.shippingDescription, sh.shippingTotal, sh.dispatchDate, oh.shipment_date,
		count(*) over (partition by oh.orderid) num_ship_rep
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ship_2 sh on oh.orderid = sh.orderid
	-- where sh.customershipmentid is null	
	order by num_ship_rep desc, oh.erp_sync_date 

-------------------------------------------------------

-- erp_all_ol
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_2
where orderlineid is null
-- where orderLinesMagento <> orderLinesDeduped
where quantityInvoiced <> quantityAllocated
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_2
	-- where orderLinesMagento <> orderLinesDeduped
	where orderlineid is not null
	-- where orderlineid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_2
	where orderLinesMagento = orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_2
	group by magentoProductID, name
	order by magentoProductID, name

	select oh.orderid, oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.magentoItemID,
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
	-- where ol.orderlineid is null
	-- where quantityInvoiced <> quantityAllocated
	-- where quantityAllocated = 0
	-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
	-- where allocationStatus = 'None' -- Full - Partial - None
	-- where allocationStrategy = '' -- AutomaticFromStock - Manual
	-- where ol.magentoItemID = 0
	order by oh.erp_sync_date 


-- erp_all_olm
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_olm_2
-- where orderlineid is null
-- where orderLinesMagento <> orderLinesDeduped
-- where quantityInvoiced <> quantityAllocated
order by orderid, orderlinemagentoid

	select count(*), count(distinct orderid), count(distinct orderlinemagentoid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_2
	-- where orderlinemagentoid is not null
	where orderlinemagentoid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_2
	where orderLinesMagento = orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_2
	group by magentoProductID, name
	order by magentoProductID, name

	select lineAdded, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_2
	group by lineAdded
	order by lineAdded

	select deduplicate, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_2
	group by deduplicate
	order by deduplicate

	select top 1000 oh.orderid, olm.orderlinemagentoid,
		oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		olm.magentoItemID,
		olm.orderLinesMagento, olm.orderLinesDeduped, 
		olm.quantityInvoiced, olm.quantityAllocated, 
		olm.productid, olm.magentoProductID, olm.name, olm.sku
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_olm_2 olm on oh.orderid = olm.orderid
	-- where olm.orderlinemagentoid is null
	-- where quantityInvoiced <> quantityAllocated
	-- where quantityAllocated = 0
	-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
	-- where allocationStatus = 'None' -- Full - Partial - None
	-- where allocationStrategy = '' -- AutomaticFromStock - Manual
	-- where magentoItemID = 0
	order by oh.erp_sync_date 



------------------------------------------------------------------------

-- erp_all_olm_all
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_olm_all_2
-- where orderlinestockallocationtransactionid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlinemagentoid), count(distinct magentoallocationid), count(distinct orderlinestockallocationtransactionid)
	from DW_GetLenses_jbs.dbo.erp_all_olm_all_2

	select wh_name, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_olm_all_2
	group by wh_name
	order by wh_name

	select shipped, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_olm_all_2
	group by shipped
	order by shipped

	select cancelled_magentoall, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_olm_all_2
	group by cancelled_magentoall
	order by cancelled_magentoall

	select top 1000 oh.orderid, ol.orderlinemagentoid, ola.magentoallocationid, ola.orderlinestockallocationtransactionid, 
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, 
		ola.wh_name, ola.shipped, ola.cancelled_magentoall, ola.quantity, 
		t.num_rep, t.num_dist_wh, 
		count(*) over (partition by ola.orderlinestockallocationtransactionid) st_all_ded
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_olm_2 ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_olm_all_2 ola on oh.orderid = ola.orderid and ol.orderlinemagentoid = ola.orderlinemagentoid 
		left join
			(select orderlinemagentoid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_all_olm_all_2
			group by orderlinemagentoid
			having count(*) > 1) t on ol.orderlinemagentoid = t.orderlinemagentoid
	order by oh.erp_sync_date

------------------------------------------------------------------------

-- erp_all_ol_all
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
-- where orderlinestockallocationtransactionid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
	where orderlinestockallocationtransactionid is null
	-- where orderlinestockallocationtransactionid is not null
	-- where batchstockallocationid is not null

	select wh_name, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
	group by wh_name
	order by wh_name

	select allocationType, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
	-- where batchstockallocationid is null
	group by allocationType
	order by allocationType

	select recordType, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
	group by recordType
	order by recordType

		select allocationType, recordType, count(*)
		from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
		group by allocationType, recordType
		order by allocationType, recordType

	select oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, 
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		t.num_rep, t.num_dist_wh
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	-- where ol.name = 'Acuvue Oasys for Astigmatism'
	-- where oh.orderid in (48695171008613892)
	-- where t.num_rep is not null and ola.allocationType not in ('Parent', 'Child')
	-- where t.num_dist_wh > 1
	-- where ol.magentoProductID = '2352'
	order by num_rep desc, oh.erp_sync_date 

		select ola.allocationType, ol.magentoProductID, ol.name, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			inner join
				(select orderlineid, count(*) num_rep
				from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
				group by orderlineid
				having count(*) > 1) t on ol.orderlineid = t.orderlineid
		group by ola.allocationType, ol.magentoProductID, ol.name
		order by ola.allocationType, ol.magentoProductID, ol.name


-- erp_all_ol_all_ba
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
	where batchstockallocationid is not null and cancelled <> 1
  
	select fullyissued_alloc, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
	group by fullyissued_alloc
	order by fullyissued_alloc

	select cancelled, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
	group by cancelled
	order by cancelled

		select allocationType, recordType, cancelled, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola 
			left join	
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on ola.orderid = olaba.orderid and ola.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		group by allocationType, recordType, cancelled
		order by allocationType, recordType, cancelled

	select oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid,
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, olaba.stockitemdescription, olaba.batch_id, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		t.num_rep, t.num_dist_wh, t2.num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		left join
			(select orderlinestockallocationtransactionid, count(*) num_rep2
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
			group by orderlinestockallocationtransactionid
			having count(*) > 1) t2 on ola.orderlinestockallocationtransactionid = t2.orderlinestockallocationtransactionid
	-- where olaba.batchstockallocationid is null and ola.allocationType not in ('Parent')
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	-- where ol.name = 'Acuvue Oasys for Astigmatism'
	-- where oh.orderid in (48695171008613892, 48695171009662452, 48695171009844252)
	-- where t.num_rep is not null and ola.allocationType not in ('Parent', 'Child')
	-- where t.num_dist_wh > 1
	-- where ol.magentoProductID = '2352'
	-- where olaba.cancelled = 1 and t.num_dist_wh = 1
	order by num_rep desc, oh.erp_sync_date, olaba.allocation_date 


-- erp_all_ol_all_bi
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2
--where batchstockissueid is not null
where batchstockissueid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid), count(distinct batchstockissueid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2
	where batchstockissueid is not null
	-- where batchstockissueid is null

	select top 1000 count(*) over (partition by batchstockallocationid) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
	order by num_rep desc, orderid, orderlineid


		select allocationType, recordType, cancelled, 
			case when (olabi.batchstockissueid is not null) then 0 else 1 end f_bi,
			count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola 
			left join	
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on ola.orderid = olaba.orderid and ola.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on ola.orderid = olabi.orderid and ola.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
		group by allocationType, recordType, cancelled, 
			case when (olabi.batchstockissueid is not null) then 0 else 1 end 
		order by allocationType, recordType, cancelled

	select oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid, olabi.batchstockissueid,
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, olaba.stockitemdescription, olaba.batch_id, olabi.stockitemdescription, olabi.batch_id, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		olabi.issueid, olabi.issuedquantity_issue, olabi.issue_date,
		t.num_rep, t.num_dist_wh, t2.num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			(select orderlineid, count(*) num_rep, count(distinct wh_name) num_dist_wh
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_2
			group by orderlineid
			having count(*) > 1) t on ol.orderlineid = t.orderlineid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		left join
			(select orderlinestockallocationtransactionid, count(*) num_rep2
			from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
			group by orderlinestockallocationtransactionid
			having count(*) > 1) t2 on ola.orderlinestockallocationtransactionid = t2.orderlinestockallocationtransactionid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
	-- where olaba.batchstockallocationid is null and ola.allocationType not in ('Parent')
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	-- where ol.name = 'Acuvue Oasys for Astigmatism'
	where oh.orderid in (48695171008613892, 48695171009662452, 48695171009844252)
	-- where oh.orderid in (48695171010072542)
	-- where t.num_rep is not null and ola.allocationType not in ('Parent', 'Child')
	-- where t.num_dist_wh > 1
	-- where ol.magentoProductID = '2352'
	-- where olaba.cancelled = 1 and t.num_dist_wh = 1
	order by num_rep desc, oh.erp_sync_date, olaba.allocation_date 
