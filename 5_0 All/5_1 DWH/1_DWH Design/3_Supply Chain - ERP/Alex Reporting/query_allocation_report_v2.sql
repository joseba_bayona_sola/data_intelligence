
drop table #magento_oh
drop table DW_GetLenses_jbs.dbo.erp_all_oh_2
drop table DW_GetLenses_jbs.dbo.erp_all_ol_dwh_2

drop table DW_GetLenses_jbs.dbo.erp_all_ol_2
drop table DW_GetLenses_jbs.dbo.erp_all_olm_2
drop table DW_GetLenses_jbs.dbo.erp_all_ship_2

drop table DW_GetLenses_jbs.dbo.erp_all_olm_all_2

drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_2
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2

drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2

select idOrderHeader_sk, order_id_bk, order_no, order_status_magento_name status, payment_method_name payment_method,
	order_date, invoice_date, shipment_date, country_code_ship,
	global_total_exc_vat, local_total_inc_vat
into #magento_oh
from Warehouse.sales.dim_order_header_v
where shipment_date between '2018-05-01' and '2018-06-01'

-- DW_GetLenses_jbs.dbo.erp_all_oh_2
select moh.order_id_bk, eoh.magentoOrderID_int, eoh.orderid, moh.order_no, eoh.incrementID order_no_erp, moh.status, moh.payment_method, 
	eoh.orderStatus, eoh._type, 
	eoh.allocationStatus, eoh.allocationStrategy, eoh.shipmentStatus, 
	moh.order_date, moh.invoice_date, moh.shipment_date, 
	eoh.createdDate erp_sync_date, eoh.promisedshippingdate, eoh.promiseddeliverydate, -- eoh.allocatedDate, 
	moh.country_code_ship,
	moh.global_total_exc_vat, moh.local_total_inc_vat
into DW_GetLenses_jbs.dbo.erp_all_oh_2
from 
		#magento_oh moh
	left join
		Landing.mend.gen_order_order_v eoh on moh.order_id_bk = eoh.magentoOrderID_int 

-------------------

select idOrderHeader_sk, idOrderLine_sk, 
	order_id_bk, order_line_id_bk, order_no,
	order_date, invoice_date, shipment_date, 
	global_total_exc_vat
into DW_GetLenses_jbs.dbo.erp_all_ol_dwh_2
from Warehouse.sales.fact_order_line_v
where shipment_date between '2018-05-01' and '2018-06-01'

------------------------

-- DW_GetLenses_jbs.dbo.erp_all_ship
select oh.orderid, sh.customershipmentid, 
	sh.shipmentnumber, sh.shippingDescription, sh.shippingTotal, sh.dispatchDate
into DW_GetLenses_jbs.dbo.erp_all_ship_2
from 
		DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
	left join
		Landing.mend.gen_ship_customershipment_v sh on oh.orderid = sh.orderid
where oh.orderid is not null

------------------------


-- DW_GetLenses_jbs.dbo.erp_all_ol
select oh.orderid, ol.orderlineid, ol.magentoItemID,
	ol.orderLinesMagento, ol.orderLinesDeduped,
	ol.quantityOrdered, ol.quantityInvoiced, ol.quantityAllocated, ol.basePrice, ol.baseRowPrice, 
	ol.productid, ol.magentoProductID, ol.name, ol.sku 	
into DW_GetLenses_jbs.dbo.erp_all_ol_2
from 
		DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
	left join
		Landing.mend.gen_order_orderline_v ol on oh.orderid = ol.orderid
where oh.orderid is not null

-- DW_GetLenses_jbs.dbo.erp_all_olm
select oh.orderid, olm.orderlinemagentoid, olm.magentoItemID, 
	olm.orderLinesMagento, olm.orderLinesDeduped, olm.lineAdded, olm.deduplicate,
	olm.quantityOrdered, olm.quantityInvoiced, olm.quantityAllocated, olm.basePrice, olm.baseRowPrice, 
	olm.productid, olm.magentoProductID, olm.name, olm.sku 	
into DW_GetLenses_jbs.dbo.erp_all_olm_2
from 
		DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
	left join
		Landing.mend.gen_order_orderlinemagento_v olm on oh.orderid = olm.orderid
where oh.orderid is not null

------------------------

-- DW_GetLenses_jbs.dbo.erp_all_olm_all
select olm.orderid, olm.orderlinemagentoid,
	ma.magentoallocationid, ma.orderlinestockallocationtransactionid,
	ma.wh_name,
	ma.shipped, ma.cancelled_magentoall, 
	ma.quantity
into DW_GetLenses_jbs.dbo.erp_all_olm_all_2
from 
		DW_GetLenses_jbs.dbo.erp_all_olm_2 olm
	left join
		Landing.mend.gen_all_magentoallocation_v ma on olm.orderlinemagentoid = ma.orderlinemagentoid
where olm.orderid is not null and olm.orderlinemagentoid is not null

------------------------

-- DW_GetLenses_jbs.dbo.erp_all_ol_all
select ol.orderid, ol.orderlineid, 
	sat.orderlinestockallocationtransactionid, sat.warehousestockitemid, -- sat.batchstockallocationid, sat.warehousestockitembatchid, 
	sat.wh_name, sat.allocationType, sat.recordType, sat.timestamp, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packsize
into DW_GetLenses_jbs.dbo.erp_all_ol_all_2
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_2 ol
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
where ol.orderid is not null and ol.orderlineid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
select ola.orderid, ola.orderlineid, ola.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, 
	wsiba.productfamilyid, wsiba.productid, wsiba.stockitemid, wsiba.warehousestockitemid, wsiba.warehousestockitembatchid, 
	wsiba.batch_id, wsiba.magentoProductID, wsiba.stockitemdescription,
	wsiba.fullyissued_alloc, wsiba.cancelled, wsiba.allocatedquantity_alloc, wsiba.allocatedunits, wsiba.issuedquantity_alloc, 
	wsiba.allocation_date
into DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola
	left join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on ola.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
where ola.orderlinestockallocationtransactionid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
select olab.orderid, olab.orderlineid, olab.orderlinestockallocationtransactionid, olab.batchstockallocationid, wsibi.batchstockissueid, 
	wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, 
	wsibi.batch_id, wsibi.magentoProductID, wsibi.stockitemdescription, 
	wsibi.issueid, wsibi.issuedquantity_issue, wsibi.issue_date 
into DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olab
	left join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on olab.batchstockallocationid = wsibi.batchstockallocationid
where olab.batchstockallocationid is not null

-- erp_all_ol_all_batch_2
select olabi.orderid, olabi.orderlineid, olabi.orderlinestockallocationtransactionid, olabi.batchstockallocationid, olabi.batchstockissueid, 
	olabi.productfamilyid, olabi.productid, olabi.stockitemid, olabi.warehousestockitemid, olabi.warehousestockitembatchid,
	wsib.packsize, wsib.productUnitCost, wsib.arrivedDate, wsib.confirmedDate, wsib.stockregisteredDate
into DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on olabi.warehousestockitembatchid = wsib.warehousestockitembatchid
where olabi.batchstockissueid is not null

