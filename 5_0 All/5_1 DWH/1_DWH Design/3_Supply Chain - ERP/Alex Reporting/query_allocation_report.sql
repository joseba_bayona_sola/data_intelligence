
drop table #magento_oh
drop table DW_GetLenses_jbs.dbo.erp_all_oh
drop table DW_GetLenses_jbs.dbo.erp_all_ol
drop table DW_GetLenses_jbs.dbo.erp_all_olm
drop table DW_GetLenses_jbs.dbo.erp_all_ship
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_bi

drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_rec -- to do
drop table DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec

drop table DW_GetLenses_jbs.dbo.erp_all_olm_all


----------------------------------------

-- #magento_oh
select *
into #magento_oh
from
	(select oh.entity_id, oh.increment_id, oh.status, ph.method payment_method, -- store_id, customer_id, base_grand_total, 
		oh.created_at order_date, ih.created_at invoice_date, oh.warehouse_approved_time, -- presc_verification_method, prescription_verification_type, 
		rank() over (partition by ih.order_id order by ih.entity_id) rank_inv
	from 
			Landing.mag.sales_flat_order_aud oh
		inner join
			Landing.mag.sales_flat_invoice_aud ih on oh.entity_id = ih.order_id
		left join
			Landing.mag.sales_flat_order_payment_aud ph on oh.entity_id = ph.parent_id
	where ih.created_at > '2018-05-01') t
where rank_inv = 1
order by order_date

-- DW_GetLenses_jbs.dbo.erp_all_oh
select moh.entity_id, eoh.magentoOrderID_int, eoh.orderid, moh.increment_id order_no, eoh.incrementID order_no_erp, moh.status, moh.payment_method, 
	eoh.orderStatus, eoh._type, 
	eoh.allocationStatus, eoh.allocationStrategy, eoh.shipmentStatus, 
	moh.order_date, moh.invoice_date, moh.warehouse_approved_time, 
	eoh.createdDate erp_sync_date, eoh.promisedshippingdate, eoh.promiseddeliverydate -- eoh.allocatedDate, 
into DW_GetLenses_jbs.dbo.erp_all_oh
from 
		#magento_oh moh
	full join
		(select *
		from Landing.mend.gen_order_order_v 
		where createdDate > '2018-05-01') eoh on moh.entity_id = eoh.magentoOrderID_int 

-- DW_GetLenses_jbs.dbo.erp_all_ol
select oh.orderid, ol.orderlineid, ol.magentoItemID,
	ol.orderLinesMagento, ol.orderLinesDeduped,
	ol.quantityOrdered, ol.quantityInvoiced, ol.quantityAllocated, ol.basePrice, ol.baseRowPrice, 
	ol.productid, ol.magentoProductID, ol.name, ol.sku 	
into DW_GetLenses_jbs.dbo.erp_all_ol
from 
		DW_GetLenses_jbs.dbo.erp_all_oh oh
	left join
		Landing.mend.gen_order_orderline2_v ol on oh.orderid = ol.orderid
where oh.orderid is not null

-- DW_GetLenses_jbs.dbo.erp_all_olm
select oh.orderid, olm.orderlinemagentoid, olm.magentoItemID, 
	olm.orderLinesMagento, olm.orderLinesDeduped, olm.lineAdded, olm.deduplicate,
	olm.quantityOrdered, olm.quantityInvoiced, olm.quantityAllocated, olm.basePrice, olm.baseRowPrice, 
	olm.productid, olm.magentoProductID, olm.name, olm.sku 	
into DW_GetLenses_jbs.dbo.erp_all_olm
from 
		DW_GetLenses_jbs.dbo.erp_all_oh oh
	left join
		Landing.mend.gen_order_orderlinemagento2_v olm on oh.orderid = olm.orderid
where oh.orderid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ship
select oh.orderid, sh.customershipmentid, 
	sh.shipmentnumber, sh.shippingDescription, sh.shippingTotal, sh.dispatchDate
into DW_GetLenses_jbs.dbo.erp_all_ship
from 
		DW_GetLenses_jbs.dbo.erp_all_oh oh
	left join
		Landing.mend.gen_ship_customershipment_v sh on oh.orderid = sh.orderid
where oh.orderid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all
select ol.orderid, ol.orderlineid, 
	sat.orderlinestockallocationtransactionid, sat.warehousestockitemid, -- sat.batchstockallocationid, sat.warehousestockitembatchid, 
	sat.wh_name, sat.allocationType, sat.recordType, sat.timestamp, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packsize
into DW_GetLenses_jbs.dbo.erp_all_ol_all
from 
		DW_GetLenses_jbs.dbo.erp_all_ol ol
	left join
		Landing.mend.gen_all_stockallocationtransaction_v sat on ol.orderid = sat.orderid and ol.orderlineid = sat.orderlineid
where ol.orderid is not null and ol.orderlineid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
select ola.orderid, ola.orderlineid, ola.orderlinestockallocationtransactionid, wsiba.batchstockallocationid, 
	wsiba.productfamilyid, wsiba.productid, wsiba.stockitemid, wsiba.warehousestockitemid, wsiba.warehousestockitembatchid, 
	wsiba.batch_id, wsiba.magentoProductID, wsiba.stockitemdescription,
	wsiba.fullyissued_alloc, wsiba.cancelled, wsiba.allocatedquantity_alloc, wsiba.allocatedunits, wsiba.issuedquantity_alloc, 
	wsiba.allocation_date
into DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all ola
	left join
		Landing.mend.gen_wh_warehousestockitembatch_alloc_v wsiba on ola.orderlinestockallocationtransactionid = wsiba.orderlinestockallocationtransactionid
where ola.orderlinestockallocationtransactionid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
select olab.orderid, olab.orderlineid, olab.orderlinestockallocationtransactionid, olab.batchstockallocationid, wsibi.batchstockissueid, 
	wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, 
	wsibi.batch_id, wsibi.magentoProductID, wsibi.stockitemdescription, 
	wsibi.issueid, wsibi.issuedquantity_issue, wsibi.issue_date 
into DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all_ba olab
	left join
		Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi on olab.batchstockallocationid = wsibi.batchstockallocationid
where olab.batchstockallocationid is not null

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_rec

-- DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec
select olabi.orderid, olabi.orderlineid, olabi.orderlinestockallocationtransactionid, olabi.batchstockallocationid, olabi.batchstockissueid, 
		olabi.productfamilyid, olabi.productid, olabi.stockitemid, olabi.warehousestockitemid, olabi.warehousestockitembatchid, 
		rl.receiptlineid, rl.receiptid, rl.supplier_id, rl.purchaseorderlineid, 
		rl.receipt_ID, rl.orderNumber, rl.unitcost
into DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec
from 
		DW_GetLenses_jbs.dbo.erp_all_ol_all_bi olabi
	left join
		Landing.mend.gen_whship_receiptline_v rl on olabi.warehousestockitembatchid = rl.warehousestockitembatchid
where olabi.batchstockissueid is not null

