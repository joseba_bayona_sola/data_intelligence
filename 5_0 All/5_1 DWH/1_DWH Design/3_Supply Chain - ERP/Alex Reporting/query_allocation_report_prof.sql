
-- erp_all_oh
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_oh
-- where entity_id is null
where magentoOrderID_int is null
-- where entity_id is not null and magentoOrderID_int is not null
-- where orderStatus = '' -- Completed - Pending - Cancelled
-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
-- where allocationStatus = 'Full' -- Full - Partial - None
-- where allocationStrategy = '' -- AutomaticFromStock - Manual
order by invoice_date, erp_sync_date

	select count(*), count(distinct entity_id), count(distinct orderid),
		min(invoice_date), max(invoice_date), 
		min(erp_sync_date), max(erp_sync_date)
	from DW_GetLenses_jbs.dbo.erp_all_oh

	select status, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by status
	order by status

	select orderStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by orderStatus
	order by orderStatus

	select _type, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by _type
	order by _type

	select allocationStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by allocationStatus
	order by allocationStatus

	select allocationStrategy, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by allocationStrategy
	order by allocationStrategy

	select shipmentStatus, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_oh
	group by shipmentStatus
	order by shipmentStatus

	select top 1000 count(*) over (partition by orderid) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_oh
	where orderid is not null
	order by num_rep desc, invoice_date

	select top 1000 count(*) over (partition by entity_id) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_oh
	where entity_id is not null
	order by num_rep desc, invoice_date

------------------------------------------------------------------------

-- erp_all_ol
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol
where orderlineid is null
-- where orderLinesMagento <> orderLinesDeduped
where quantityInvoiced <> quantityAllocated
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid)
	from DW_GetLenses_jbs.dbo.erp_all_ol
	-- where orderLinesMagento <> orderLinesDeduped
	where orderlineid is not null
	-- where orderlineid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_ol
	where orderLinesMagento = orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_ol
	group by magentoProductID, name
	order by magentoProductID, name

	select oh.orderid, oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.magentoItemID,
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
	-- where ol.orderlineid is null
	-- where quantityInvoiced <> quantityAllocated
	-- where quantityAllocated = 0
	-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
	-- where allocationStatus = 'None' -- Full - Partial - None
	-- where allocationStrategy = '' -- AutomaticFromStock - Manual
	-- where ol.magentoItemID = 0
	order by oh.erp_sync_date 

-- erp_all_olm
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_olm
-- where orderlineid is null
-- where orderLinesMagento <> orderLinesDeduped
where quantityInvoiced <> quantityAllocated
order by orderid, orderlinemagentoid

	select count(*), count(distinct orderid), count(distinct orderlinemagentoid)
	from DW_GetLenses_jbs.dbo.erp_all_olm
	-- where orderlinemagentoid is not null
	where orderlinemagentoid is null

	select count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm
	where orderLinesMagento = orderLinesDeduped -- =, <>

	select magentoProductID, name, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm
	group by magentoProductID, name
	order by magentoProductID, name

	select lineAdded, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm
	group by lineAdded
	order by lineAdded

	select deduplicate, count(*), count(distinct orderid)
	from DW_GetLenses_jbs.dbo.erp_all_olm
	group by deduplicate
	order by deduplicate

	select oh.orderid, oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		olm.magentoItemID,
		olm.orderLinesMagento, olm.orderLinesDeduped, 
		olm.quantityInvoiced, olm.quantityAllocated, 
		olm.productid, olm.magentoProductID, olm.name, olm.sku
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_olm olm on oh.orderid = olm.orderid
	-- where olm.orderlinemagentoid is null
	-- where quantityInvoiced <> quantityAllocated
	-- where quantityAllocated = 0
	-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
	-- where allocationStatus = 'None' -- Full - Partial - None
	-- where allocationStrategy = '' -- AutomaticFromStock - Manual
	where magentoItemID = 0
	order by oh.erp_sync_date 

------------------------------------------------------------------------

-- erp_all_ship
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ship
where customershipmentid is null

	select count(*), count(distinct orderid), count(distinct customershipmentid)
	from DW_GetLenses_jbs.dbo.erp_all_ship
	-- where customershipmentid is not null	
	where customershipmentid is null

	select shippingDescription, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ship
	group by shippingDescription
	order by shippingDescription

	select top 1000 oh.orderid, oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		sh.shipmentnumber, sh.shippingDescription, sh.shippingTotal, sh.dispatchDate,
		count(*) over (partition by oh.orderid) num_ship_rep
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ship sh on oh.orderid = sh.orderid
	-- where sh.customershipmentid is null	
	order by num_ship_rep desc, oh.erp_sync_date 

------------------------------------------------------------------------

-- erp_all_ol_all
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all
-- where orderlinestockallocationtransactionid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all
	-- where orderlinestockallocationtransactionid is null
	where orderlinestockallocationtransactionid is not null
	-- where batchstockallocationid is not null

	select wh_name, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all
	group by wh_name
	order by wh_name

	select allocationType, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all
	-- where batchstockallocationid is null
	group by allocationType
	order by allocationType

	select recordType, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all
	group by recordType
	order by recordType


	select top 1000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, 
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		count(*) over (partition by ola.orderlinestockallocationtransactionid) num_rep
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
	-- where orderlinestockallocationtransactionid is null
	where orderlinestockallocationtransactionid is not null
	-- where quantityInvoiced <> quantityAllocated
	-- where quantityAllocated = 0
	-- where _type = 'Wholesale_order' -- Sales_order - Wholesale_order
	-- where allocationStatus = 'None' -- Full - Partial - None
	-- where allocationStrategy = '' -- AutomaticFromStock - Manual
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	-- where ol.name = 'Acuvue Oasys for Astigmatism'
	order by num_rep desc, oh.erp_sync_date 


		select ol.magentoProductID, ol.name, count(*), count(distinct oh.orderid)
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		where orderlinestockallocationtransactionid is null and oh.orderStatus = 'Pending'
		group by ol.magentoProductID, ol.name
		order by count(*) desc, ol.magentoProductID, ol.name

-- erp_all_ol_all_ba
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
	where batchstockallocationid is not null

	select fullyissued_alloc, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
	group by fullyissued_alloc
	order by fullyissued_alloc

	select cancelled, count(*)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_ba
	group by cancelled
	order by cancelled

	select top 1000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid,
		oh.magentoOrderID_int, ol.magentoItemID, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.magentoProductID, ol.name, ol.sku, olaba.stockitemdescription, olaba.batch_id, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		count(*) over (partition by ola.orderlinestockallocationtransactionid) num_rep, 
		count(*) over (partition by olaba.batchstockallocationid) num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_ba olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	where ola.orderlinestockallocationtransactionid is not null
	order by num_rep desc, oh.erp_sync_date, olaba.allocation_date 

-- erp_all_ol_all_bi
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
--where batchstockissueid is not null
where batchstockissueid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid), count(distinct batchstockissueid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
	where batchstockissueid is not null
	-- where batchstockissueid is null

	select top 1000 count(*) over (partition by batchstockallocationid) num_rep, *
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi
	order by num_rep desc, orderid, orderlineid

	select top 1000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, olaba.batchstockallocationid,
		oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.name, ol.sku, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.allocation_date,
		olabi.productfamilyid, olabi.productid, olabi.stockitemid, olabi.warehousestockitemid, olabi.warehousestockitembatchid, 
		olabi.batch_id, olabi.magentoProductID, 
		olabi.issueid, olabi.issuedquantity_issue, olabi.issue_date, 
		count(*) over (partition by ola.orderlinestockallocationtransactionid) num_rep, 
		count(*) over (partition by olaba.batchstockallocationid) num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_ba olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_bi olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
	where oh.orderid in (48695171011222244, 48695171011241863)
	-- where ola.batchstockallocationid is not null
	-- order by num_rep desc, oh.erp_sync_date, olaba.createdDate 
	order by oh.erp_sync_date, olaba.allocation_date 

-------------------------------------------------------------------

-- erp_all_ol_all_ba_rec

-- erp_all_ol_all_bi_rec
select top 1000 *
from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec
--where receiptlineid is not null
where receiptlineid is null
order by orderid, orderlineid

	select count(*), count(distinct orderid), count(distinct orderlineid), count(distinct orderlinestockallocationtransactionid), count(distinct batchstockallocationid), count(distinct batchstockissueid)
	from DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec
	-- where receiptlineid is not null
	where receiptlineid is null


	select top 10000 oh.orderid, ol.orderlineid, ola.orderlinestockallocationtransactionid, ola.batchstockallocationid,
		oh.magentoOrderID_int, oh.order_no_erp, 
		oh.orderStatus, oh._type, oh.allocationStatus, oh.allocationStrategy, 
		oh.erp_sync_date, 
		ol.orderLinesMagento, ol.orderLinesDeduped, 
		ol.quantityInvoiced, ol.quantityAllocated, 
		ol.productid, ol.name, ol.sku, 
		ola.wh_name, ola.allocationType, ola.recordType, ola.timestamp, 
		ola.allocatedUnits, ola.allocatedStockQuantity, ola.issuedQuantity, ola.packsize, 
		olaba.fullyissued_alloc, olaba.cancelled, olaba.allocatedquantity_alloc, olaba.allocatedunits, olaba.issuedquantity_alloc, 
		olaba.createdDate,
		olabi.productfamilyid, olabi.productid, olabi.stockitemid, olabi.warehousestockitemid, olabi.warehousestockitembatchid, 
		olabi.batch_id, olabi.magentoProductID_int, olabi.name, olabi.packSize, olabi.SKU, 
		olabi.productUnitCost, olabi.totalUnitCost, olabi.currency, 
		olabi.confirmedDate, 
		olabi.issueid, olabi.createddate, olabi.issuedquantity_issue, 
		olabir.receipt_ID, olabir.orderNumber, olabir.unitcost,
		count(*) over (partition by ola.orderlinestockallocationtransactionid) num_rep, 
		count(*) over (partition by ola.batchstockallocationid) num_rep2
	from 
			DW_GetLenses_jbs.dbo.erp_all_oh oh
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol ol on oh.orderid = ol.orderid
		inner join
			DW_GetLenses_jbs.dbo.erp_all_ol_all ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_ba olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.batchstockallocationid = olaba.batchstockallocationid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_bi olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
		left join
			DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_rec olabir on oh.orderid = olabir.orderid and ol.orderlineid = olabir.orderlineid and olaba.batchstockallocationid = olabir.batchstockallocationid and
				olabi.batchstockissueid = olabir.batchstockissueid
	-- where oh.orderid in (48695171011222244, 48695171011241863)
	where olabir.batchstockissueid is not null and olabir.receiptlineid is null order by olabi.confirmedDate
	-- order by num_rep desc, oh.erp_sync_date, olaba.createdDate 
	order by oh.erp_sync_date, olaba.createdDate
