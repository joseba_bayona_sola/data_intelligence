
		select top 1000 
			oh.magentoOrderID_int order_id_bk, ol.magentoItemID order_line_id_bk, 
			oh.order_no_erp order_no, sh.shipmentnumber shipment_number, 
			oh.global_total_exc_vat, oh.local_total_inc_vat, ol.baseRowPrice,
			oh.order_date, oh.invoice_date, oh.erp_sync_date, sh.dispatchDate, oh.shipment_date, 
			olaba.allocation_date, olabi.issue_date, 
			-- datediff(dd, oh.invoice_date, oh.erp_sync_date) diff_sync, 
			-- datediff(dd, oh.erp_sync_date, olabi.issue_date) diff_issue, 
			oh.country_code_ship,

			ol.magentoProductID, ol.name, ol.sku, 
			ol.quantityOrdered,
			ola.wh_name, olabi.magentoProductID, olabi.stockitemdescription, -- olabi.batch_id, olabi.warehousestockitembatchid,
			olabatch.packsize, olabi.issuedquantity_issue,
			olabatch.productUnitCost, olabi.issuedquantity_issue * olabatch.productUnitCost rowCost,
			olabatch.arrivedDate, olabatch.confirmedDate, olabatch.stockregisteredDate
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ship_2 sh on oh.orderid = sh.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2 olabatch on olabi.batchstockissueid = olabatch.batchstockissueid
		where olabi.batchstockissueid is not null
			-- and datediff(dd, oh.erp_sync_date, olabi.issue_date) > 4
			-- and oh.order_id_bk = 7173164
		order by oh.erp_sync_date, olaba.allocation_date 

		select count(*), count(distinct oh.orderid), count(distinct ol.orderlineid), 
			count(distinct ola.orderlinestockallocationtransactionid), count(distinct olaba.batchstockallocationid), count(distinct olabi.batchstockissueid)
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ship_2 sh on oh.orderid = sh.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2 olabatch on olabi.batchstockissueid = olabatch.batchstockissueid
		where olabi.batchstockissueid is not null

		select ol.magentoProductID, ol.name, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ship_2 sh on oh.orderid = sh.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2 olabatch on olabi.batchstockissueid = olabatch.batchstockissueid	
		where olabi.batchstockissueid is not null
			-- and datediff(dd, oh.erp_sync_date, olabi.issue_date) > 4
		group by ol.magentoProductID, ol.name
		-- order by count(*) desc, ol.magentoProductID, ol.name
		order by ol.magentoProductID, ol.name


		select olabi.magentoProductID, count(*)
		from 
				DW_GetLenses_jbs.dbo.erp_all_oh_2 oh
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ship_2 sh on oh.orderid = sh.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_2 ol on oh.orderid = ol.orderid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_2 ola on oh.orderid = ola.orderid and ol.orderlineid = ola.orderlineid 
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_ba_2 olaba on oh.orderid = olaba.orderid and ol.orderlineid = olaba.orderlineid and ola.orderlinestockallocationtransactionid = olaba.orderlinestockallocationtransactionid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_bi_2 olabi on oh.orderid = olabi.orderid and ol.orderlineid = olabi.orderlineid and olaba.batchstockallocationid = olabi.batchstockallocationid
			inner join
				DW_GetLenses_jbs.dbo.erp_all_ol_all_batch_2 olabatch on olabi.batchstockissueid = olabatch.batchstockissueid	
		where olabi.batchstockissueid is not null
			-- and datediff(dd, oh.erp_sync_date, olabi.issue_date) > 4
		group by olabi.magentoProductID
		-- order by count(*) desc, ol.magentoProductID, ol.name
		order by olabi.magentoProductID
