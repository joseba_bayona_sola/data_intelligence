
-- Warehouse - Warehouse Preference - Country 
	-- Missing: Country_Warehouse relation - Order_Country relation

select *
from Landing.mend.wh_warehouse

select *
from Landing.mend.gen_wh_warehouse_v

select *
from Landing.mend.comp_country

-- Product Family - Stock Item - 
	-- Missing: stockitem.stockitemdescription
	-- Different values from PF Manufacturer, Product Type, Category
select *
from Landing.mend.prod_productfamily

select *
from Landing.mend.gen_prod_productfamilypacksize_v

select top 1000 count(*) over (), *
from Landing.mend.prod_stockitem

select top 1000 *
from Landing.mend.gen_prod_stockitem_v

select count(*)
from Landing.mend.gen_prod_stockitem_v

-- Warehouse Stock Item Batch
	-- Views: Cardinalities in Warehouse Stock Item Batch

select top 1000 *
from Landing.mend.wh_warehousestockitembatch_aud

	select count(*)
	from Landing.mend.wh_warehousestockitembatch_aud

select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_v

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_v

-- Order - Shipment
	-- Missing: customershipment.currencycode (OK in Order)
	-- Views: Cardinalities in Order, Customer Shipment

select top 1000 *
from Landing.mend.order_order

	select top 1000 count(*)
	from Landing.mend.order_order_aud

select top 1000 *
from Landing.mend.gen_order_order_v

	select top 1000 count(*)
	from Landing.mend.gen_order_order_v

select top 1000 *
from Landing.mend.ship_customershipment_aud

	select count(*)
	from Landing.mend.ship_customershipment_aud

select top 1000 *
from Landing.mend.gen_ship_customershipment_v

	select count(*)
	from Landing.mend.gen_ship_customershipment_v

-- OL 
	-- OL: Magento OL vs OL: What to take
	-- Views: Performance (Indexes)

select top 1000 *
from Landing.mend.order_orderlinemagento_aud

	select top 1000 *
	from Landing.mend.gen_order_orderlinemagento_v

select top 1000 *
from Landing.mend.order_orderline_aud

	select top 1000 *
	from Landing.mend.gen_order_orderline_v


select top 1000 *
from Landing.mend.order_orderlineabstract_aud

	select count(*)
	from Landing.mend.order_orderlineabstract_aud

-- Allocation 
	-- Allocation: Magento Alloc vs Alloc: What to take
	-- Views: Performance (Indexes) + Cardinalities

select top 1000 *
from Landing.mend.all_orderlinestockallocationtransaction_aud

	select top 1000 count(*)
	from Landing.mend.all_orderlinestockallocationtransaction_aud

select top 1000 *
from Landing.mend.gen_all_stockallocationtransaction_v
	
	select count(*)
	from Landing.mend.gen_all_stockallocationtransaction_v


select top 1000 *
from Landing.mend.all_magentoallocation_aud

	select top 1000 count(*)
	from Landing.mend.all_magentoallocation_aud

select top 1000 *
from Landing.mend.gen_all_magentoallocation_v

	select top 1000 count(*)
	from Landing.mend.gen_all_magentoallocation_v

-- Allocation Batch
	-- Cardinality between Allocation - Issue (1:N)
	-- Views: Performance (Indexes) + Cardinalities

select top 1000 *
from Landing.mend.wh_batchstockallocation_aud

	select count(*)
	from Landing.mend.wh_batchstockallocation_aud	

select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v


select top 1000 *
from Landing.mend.wh_batchstockissue_aud

	select count(*)
	from Landing.mend.wh_batchstockissue_aud	

select top 1000 *
from Landing.mend.gen_wh_warehousestockitembatch_issue_v

	select count(*)
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v

-- Purchase Order - Receipt
	-- PO: Missing Attributes
	-- Views: Cardinalities

	-- Receipt Views: Redefine after Table Renaming from Shipment to Receipt

select top 1000 *
from Landing.mend.purc_purchaseorder
order by id desc

	select count(*)
	from Landing.mend.purc_purchaseorder

select top 1000 *
from Landing.mend.gen_purc_purchaseorder_v

	select count(*)
	from Landing.mend.gen_purc_purchaseorder_v


select top 1000 *
from Landing.mend.whship_receipt

	select count(*)
	from Landing.mend.whship_receipt

select top 1000 *
from Landing.mend.gen_whship_shipment_v


select top 1000 *
from Landing.mend.whship_receiptline

	select count(*)
	from Landing.mend.whship_receiptline

select top 1000 *
from Landing.mend.gen_whship_shipmentorderline_v
