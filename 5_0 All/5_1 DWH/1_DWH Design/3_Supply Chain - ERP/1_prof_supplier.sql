
---------------------- NOTES ----------------------
	-- suplier
		-- Why some Suppliers with NULL on supplierID

	-- suplier
	select supplier_id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		defaultLeadTime,
		flatFileConfigurationToUse,		
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime
	from Landing.mend.gen_supp_supplier_v
	-- where supplierID is null
	order by supplierType, supplierName

	select count(*)
	from Landing.mend.supp_supplier_aud

	select count(*)
	from Landing.mend.gen_supp_supplier_v


		select supplierType, count(*)
		from Landing.mend.gen_supp_supplier_v
		group by supplierType
		order by supplierType

		select flatFileConfigurationToUse, count(*)
		from Landing.mend.gen_supp_supplier_v
		group by flatFileConfigurationToUse
		order by flatFileConfigurationToUse


---------------------- NOTES ----------------------
	-- suplierprice
		-- Some Suppliers without Prices
		-- Diff between StandardPrive vs SpotPrice
		-- Standard Price Atr: Dates + Min, Max Qty
		-- Important Atr: unitPrice (per pkg) - leadTime - orderMultiple - Rest ???

	-- suplierprice
	select supplier_id, supplierpriceid,
		supplierID, supplierName, currencyCode, supplierType,
		subMetaObjectName,
		unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
		directPrice, expiredPrice,
		totallt, shiplt, transferlt,
		comments,
		lastUpdated,
		createdDate, changedDate, 
		moq, effectiveDate, expiryDate, maxqty
	from Landing.mend.gen_supp_supplierprice_v
	-- where supplierpriceid is null
	-- where supplierName = 'J&J UK'
	order by supplierType, supplierName, subMetaObjectName

	select count(*)
	from Landing.mend.supp_supplierprice_aud

	select count(*)
	from Landing.mend.supp_standardprice_aud

	select count(*)
	from Landing.mend.gen_supp_supplierprice_v

		select *, 
			count(*) over (partition by supplierpriceid) rep_father,
			count(*) over (partition by supplierid) rep_son
		from Landing.mend.supp_supplierprices_supplier_aud
		order by rep_father desc, rep_son desc

	select supplierID, supplierName, count(*)
	from Landing.mend.gen_supp_supplierprice_v
	group by supplierID, supplierName
	order by supplierID, supplierName
