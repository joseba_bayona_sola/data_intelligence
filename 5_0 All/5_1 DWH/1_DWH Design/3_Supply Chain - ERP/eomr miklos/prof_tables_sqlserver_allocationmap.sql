
select top 1000 *
from DW_GetLenses_jbs.dbo.eomr_allocationmap
-- where issue_id is null
-- where batch_id is null
where issue_id = 6705116
order by issue_id
-- order by batch_id

	select top 1000 count(*), 
		count(distinct issue_id), min(issue_id), max(issue_id), 
		count(distinct batch_id), min(batch_id), max(batch_id) 
	from DW_GetLenses_jbs.dbo.eomr_allocationmap

	select top 1000 warehouse, count(*)
	from DW_GetLenses_jbs.dbo.eomr_allocationmap
	group by warehouse
	order by warehouse

-- why NULL batch_id
select top 1000 batch_id, warehouse, count(*) num_records, sum(units) units
from DW_GetLenses_jbs.dbo.eomr_allocationmap
group by batch_id, warehouse
order by batch_id, warehouse


-----------------------------------------------

-- ISSUE TRANSACTIONS - 1 Day Acuvue Moist BC8.5/DI14.2/PO-6.50
select t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.description, t.units,
	am.issue_id, am.batch_id, am.units, am.warehouse
from
		(select t.id, t.trans, t.datetime, t.warehouse, 
			t.productid, t.sku, 
			-- si.magentoProductID_int, si.magentoSKU, si.name, 
			si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
			t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
			t.document_number, t.order_status
		from 
				DW_GetLenses_jbs.dbo.eomr_alltransactions t
			inner join
				Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
		where t.productid = '1083' and t.sku = '01083B2D4BN0000000000301'
			and trans = 'issue') t  
	left join
		DW_GetLenses_jbs.dbo.eomr_allocationmap am on t.id = am.issue_id
-- where am.issue_id is null
where am.issue_id is not null -- and t.units <> am.units
order by t.warehouse, t.datetime desc

-----------------------------------------------

-- STOCK - RECEIPT TRANSACTIONS - 1 Day Acuvue Moist BC8.5/DI14.2/PO-6.50
select t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.description, t.units,
	am.issue_id, am.batch_id, am.units, am.warehouse
from
		(select t.id, t.trans, t.datetime, t.warehouse, 
			t.productid, t.sku, 
			-- si.magentoProductID_int, si.magentoSKU, si.name, 
			si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
			t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
			t.document_number, t.order_status
		from 
				DW_GetLenses_jbs.dbo.eomr_alltransactions t
			inner join
				Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
		where t.productid = '1083' and t.sku = '01083B2D4BN0000000000301'
			and trans <> 'issue') t  
	left join
		DW_GetLenses_jbs.dbo.eomr_allocationmap am on t.id = am.batch_id
-- where am.issue_id is null
where am.batch_id is not null -- and t.units <> am.units
order by t.warehouse, t.datetime desc

-- Why Issued units sometimes more than stock, receipt
select t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.description, t.units,
	t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype,
	am.batch_id, am.units, am.warehouse
from
		(select t.id, t.trans, t.datetime, t.warehouse, 
			t.productid, t.sku, 
			-- si.magentoProductID_int, si.magentoSKU, si.name, 
			si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
			t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
			t.document_number, t.order_status
		from 
				DW_GetLenses_jbs.dbo.eomr_alltransactions t
			inner join
				Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
		where t.productid = '1083' and t.sku = '01083B2D4BN0000000000301'
			and trans not in ('issue', 'adjustment')) t  
	left join
		(select batch_id, warehouse, count(*) num_records, sum(units) units
		from DW_GetLenses_jbs.dbo.eomr_allocationmap
		group by batch_id, warehouse) am on t.id = am.batch_id
-- where am.batch_id is null
where am.batch_id is not null -- and t.units = am.units -- = - > - <
order by t.warehouse, t.datetime desc


-----------------------------------------------

-- ISSUE TRANSACTIONS - ALL

-- NULL on eomr_allocationmap: May Issues - 0 unit issues
-- NULL on eomr_alltransactions: ???
select top 1000 count(*) over () num_tot,
	t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.sku, t.description, t.units,
	am.issue_id, am.batch_id, am.units, am.warehouse
from
		(select t.id, t.trans, t.datetime, t.warehouse, 
			t.productid, t.sku, 
			-- si.magentoProductID_int, si.magentoSKU, si.name, 
			si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
			t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
			t.document_number, t.order_status
		from 
				DW_GetLenses_jbs.dbo.eomr_alltransactions t
			inner join
				Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
		where trans = 'issue') t  
	full join
		DW_GetLenses_jbs.dbo.eomr_allocationmap am on t.id = am.issue_id
-- where am.issue_id is null and t.datetime < '2018-05-01'
where t.id is null 
order by t.warehouse, t.productid, t.sku, t.datetime desc, 
	am.warehouse, am.issue_id


-- BATCH TRANSACTIONS - ALL

select top 1000	count(*) over () num_tot,	
	t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.sku, t.description, t.units,
	t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype,
	am.batch_id, am.units, am.warehouse
from
		(select t.id, t.trans, t.datetime, t.warehouse, 
			t.productid, t.sku, 
			-- si.magentoProductID_int, si.magentoSKU, si.name, 
			si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
			t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
			t.document_number, t.order_status
		from 
				DW_GetLenses_jbs.dbo.eomr_alltransactions t
			inner join
				Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
		where trans not in ('issue', 'adjustment')) t  
	full join
		(select batch_id, warehouse, count(*) num_records, sum(units) units
		from DW_GetLenses_jbs.dbo.eomr_allocationmap
		group by batch_id, warehouse) am on t.id = am.batch_id
-- where am.batch_id is null -- and t.datetime < '2018-05-01'
where t.id is null 
order by t.warehouse, t.productid, t.sku, t.datetime desc, 
	am.warehouse, am.batch_id