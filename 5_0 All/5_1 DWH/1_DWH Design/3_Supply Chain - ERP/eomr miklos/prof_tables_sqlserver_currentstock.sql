
select top 1000 warehouse, sku, units, snapunits, id
from DW_GetLenses_jbs.dbo.eomr_currentstock
-- where sku = '01083B2D4BN0000000000301'
where id = 6705116
order by id

	select top 1000 count(*), count(distinct id)
	from DW_GetLenses_jbs.dbo.eomr_currentstock

	-- Why -1 id / duplicated ids
	select top 1000 id, count(*), count(distinct warehouse)
	from DW_GetLenses_jbs.dbo.eomr_currentstock
	group by id
	order by count(distinct warehouse) desc, count(*) desc, id

	select id, warehouse, count(*)
	from DW_GetLenses_jbs.dbo.eomr_currentstock
	group by id, warehouse
	having count(*) > 1
	order by count(*) desc, id


	select top 1000 warehouse, count(*)
	from DW_GetLenses_jbs.dbo.eomr_currentstock
	group by warehouse
	order by warehouse

	select sku, count(*) num_rec
	from DW_GetLenses_jbs.dbo.eomr_currentstock
	group by sku
	order by sku

	select t.sku, 
		si.magentoProductID_int, si.magentoSKU, si.name, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
		t.num_rec
	from
			(select sku, count(*) num_rec
			from DW_GetLenses_jbs.dbo.eomr_currentstock
			group by sku) t
		full join
			Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
	where si.sku is not null and t.sku is not null
	-- where si.sku is null
	-- where t.sku is null
	order by si.magentoProductID_int, t.sku, si.magentoSKU

-------------------------------------------

select top 1000 cs.warehouse, cs.sku, 
	si.magentoProductID_int, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, 
	cs.units, cs.snapunits, cs.id
from 
		DW_GetLenses_jbs.dbo.eomr_currentstock cs
	inner join
		Landing.mend.gen_prod_stockitem_v si on cs.sku = si.sku
where cs.id = 6705116
order by si.magentoProductID_int, cs.sku, si.magentoSKU, cs.warehouse, cs.id

-------------------------------------------

select top 1000 cs.warehouse, cs.sku, 
	si.magentoProductID_int, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, 
	cs.units, cs.snapunits, cs.id
from 
		DW_GetLenses_jbs.dbo.eomr_currentstock cs
	inner join
		Landing.mend.gen_prod_stockitem_v si on cs.sku = si.sku
where cs.sku = '01083B2D4BN0000000000301'
order by si.magentoProductID_int, cs.sku, si.magentoSKU, cs.warehouse, cs.id

select top 1000 cs.warehouse, cs.sku, 
	si.magentoProductID_int, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, 
	cs.units, cs.snapunits, cs.id, 
	t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, t.document_number,
	am.units
from 
		DW_GetLenses_jbs.dbo.eomr_currentstock cs
	inner join
		Landing.mend.gen_prod_stockitem_v si on cs.sku = si.sku
	left join
		DW_GetLenses_jbs.dbo.eomr_alltransactions t on cs.id = t.id
	left join
		(select batch_id, warehouse, count(*) num_records, sum(units) units
		from DW_GetLenses_jbs.dbo.eomr_allocationmap
		group by batch_id, warehouse) am on t.id = am.batch_id
where cs.sku = '01083B2D4BN0000000000301'
order by si.magentoProductID_int, cs.sku, si.magentoSKU, cs.warehouse, cs.id

-------------------------------

select top 1000 cs.warehouse, cs.sku, 
	si.magentoProductID_int, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, 
	cs.units, cs.snapunits, cs.id, 
	t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, t.document_number,
	am.units
from 
		DW_GetLenses_jbs.dbo.eomr_currentstock cs
	inner join
		Landing.mend.gen_prod_stockitem_v si on cs.sku = si.sku
	left join
		DW_GetLenses_jbs.dbo.eomr_alltransactions t on cs.id = t.id
	left join
		(select batch_id, warehouse, count(*) num_records, sum(units) units
		from DW_GetLenses_jbs.dbo.eomr_allocationmap
		group by batch_id, warehouse) am on t.id = am.batch_id
where t.document_number = 'R00013725'
order by si.magentoProductID_int, cs.sku, si.magentoSKU, cs.warehouse, cs.id
