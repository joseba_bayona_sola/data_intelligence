
-- stock
SELECT
	'stock'          AS transaction,
	wsib.createddate      AS datetime,
	wh.shortname        AS warehouse,
	si.sku              AS sku,

	-- adjustment can't be tracked (positive adjustment added to receivedquantity and some of the adjustments have no timestamp)
	wsib.receivedquantity - wsib.disposedquantity AS units,
	wsib.productunitcost  AS unitcost,

	NULL :: VARCHAR  AS currency,
	NULL :: FLOAT    AS day_rate,
	NULL :: FLOAT    AS eurate,

	NULL :: VARCHAR  AS supplier,
	NULL :: VARCHAR  AS suppliertype,

	NULL :: VARCHAR  AS document_number,
	NULL :: VARCHAR  AS order_status

FROM 
		inventory$warehousestockitembatch wsib
	LEFT OUTER JOIN 
		inventory$warehousestockitembatch_receipt wsib_rec ON wsib_rec.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem  wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id

-- only those that don't have receipts( shipment) attached
WHERE wsib_rec.warehouseshipments$receiptid IS NULL
ORDER BY wsib.createddate desc 
limit 1000


-- issue
SELECT
	'issue'                                  AS transaction,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN bsi.createddate + interval '1 hour' ELSE bsi.createddate END AS datetime,
	wh.shortname                                AS warehouse,
	si.sku                                      AS sku,

	bsi.issuedquantity AS units,
	NULL :: FLOAT                            AS unitcost,

	NULL :: VARCHAR                          AS currency,
	NULL :: FLOAT                            AS day_rate,
	NULL :: FLOAT                            AS eurate,

	NULL :: VARCHAR                          AS supplier,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN 'Company' ELSE NULL :: VARCHAR END AS suppliertype,

	incrementid AS document_number,
	orderstatus AS order_status
FROM 
		inventory$batchstockissue bsi
	JOIN 
		inventory$batchstockissue_warehousestockitembatch bsi_wsib ON bsi_wsib.inventory$batchstockissueid = bsi.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = bsi_wsib.inventory$warehousestockitembatchid
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsi_si.inventory$warehousestockitemid
	JOIN 	
		inventory$warehouse wh ON wh.id = wsi_wh.inventory$warehouseid

	-- inventory$batchstockallocation
	LEFT OUTER JOIN 
		inventory$batchstockissue_batchstockallocation bsi_bsa ON bsi_bsa.inventory$batchstockissueid = bsi.id
	LEFT OUTER JOIN 
		inventory$batchstockallocation bsa ON bsa.id = bsi_bsa.inventory$batchstockallocationid

	-- stockallocation$orderlinestockallocationtransaction
	LEFT OUTER JOIN 
		inventory$batchtransaction_orderlinestockallocationtransaction tr_bsa ON tr_bsa.inventory$batchstockallocationid = bsi_bsa.inventory$batchstockallocationid

	LEFT OUTER JOIN 
		stockallocation$at_order tr_oh ON tr_oh.stockallocation$orderlinestockallocationtransactionid = tr_bsa.stockallocation$orderlinestockallocationtransactionid

	-- orderprocessing$order
	LEFT OUTER JOIN 
		orderprocessing$order oh ON oh.id = tr_oh.orderprocessing$orderid

	-- orderprocessing$order_transferheader
	LEFT OUTER JOIN 
		orderprocessing$order_transferheader oh_th ON oh_th.orderprocessing$orderid = oh.id

WHERE bsi.cancelled = FALSE
limit 1000    


-- receipt
SELECT
	'receipt'  AS transaction,
	-- intersite receipt can't be before transfer order issue
	CASE 
		WHEN supp.suppliertype = 'Company' AND rec.stockregistereddate - rec.arriveddate > interval '6 day' THEN coalesce(coalesce(rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		ELSE coalesce(coalesce(coalesce(rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
	END AS datetime,
	wh.shortname AS warehouse,
	si.sku AS sku,

	recl.quantityreceived AS units,
	recl.unitcost AS unitcost,

	cm.currencycode AS currency,
	cur.spotrate AS day_rate,
	eur.spotrate AS eurate,

	supp.suppliername AS supplier,
	supp.suppliertype AS suppliertype,

	rec.receiptid AS document_number,
	NULL :: VARCHAR AS order_status

FROM 
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		product$stockitem si ON si.id = pol_si.product$stockitemid

 WHERE rec.status = 'Stock_Registered' AND recl.quantityreceived > 0
 limit 1000    
 
 
 -- adjustment
 SELECT
	'adjustment' AS transaction,
	coalesce(bsm.createddate, wsib.createddate) + interval '1 hour' AS datetime,
	wh.shortname AS warehouse,
	si.sku AS sku,

	CASE 
		WHEN bsm.batchstockmovementtype = 'Manual_Negative' THEN -1
		WHEN bsm.batchstockmovementtype = 'Manual_Positive' THEN 1

		WHEN sm.movetype = 'SREG' AND sm.movementtype =	'SREG' AND sm.fromstate = 'Z' and sm.tostate = 'W' THEN  1

		WHEN sm.movetype = 'SADJ' AND sm.movementtype = 'ERPADJ' AND sm.fromstate = 'Z' and sm.tostate = 'W' THEN  1
		WHEN sm.movetype = 'SADJ' AND sm.movementtype =	'ERPADJ' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1
		WHEN sm.movetype = 'SADJ' AND sm.movementtype = 'SADJ' AND sm.fromstate = 'Z' and sm.tostate = 'W' THEN 1
		WHEN sm.movetype = 'SADJ' AND sm.movementtype = 'SADJ' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1

		WHEN sm.movetype = 'SDISS' AND sm.movementtype = 'SDISS' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1
		WHEN sm.movetype = 'SDISS' AND sm.movementtype = 'ERPADJ' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1

		WHEN sm.movetype = 'SDISI' AND sm.movementtype = 'SDISI' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1
		WHEN sm.movetype = 'SDISI' AND sm.movementtype = 'ERPADJ' AND sm.fromstate = 'W' and sm.tostate = 'Z' THEN -1

		WHEN sm.movetype = 'SREL' AND sm.movementtype = 'SREL' AND sm.fromstate = 'W' and sm.tostate = 'W' THEN 0
		WHEN sm.movetype = 'SHOL' AND sm.movementtype = 'ERPADJ' AND sm.fromstate = 'W' and sm.tostate = 'W' THEN 0
		WHEN sm.movetype = 'SHOL' AND sm.movementtype = 'SHOL' AND sm.fromstate = 'W' and sm.tostate = 'W' THEN 0
		WHEN sm.movetype = 'ERP Batch' AND sm.movementtype = 'ERPADJ' AND sm.fromstate = 'W' and sm.tostate = 'W' THEN 0

		ELSE 0
	END * bsm.quantity AS units,
	NULL AS unitcost,

	NULL AS currency,
	NULL AS day_rate,
	NULL AS eurate,

	NULL AS supplier,
	NULL AS suppliertype,

	NULL :: VARCHAR AS document_number,
	NULL :: VARCHAR AS order_status

FROM 
		inventory$batchstockmovement bsm
	JOIN 
		inventory$batchstockmovement_warehousestockitembatch bsm_wsib ON bsm_wsib.inventory$batchstockmovementid = bsm.id
	JOIN 
		inventory$warehousestockitembatch wsib ON bsm_wsib.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		inventory$batchstockmovement_stockmovement bsm_sm ON bsm_sm.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN 
		inventory$stockmovement sm ON bsm_sm.inventory$stockmovementid = sm.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id
	LEFT OUTER JOIN 
		inventory$warehousestockitembatch_receipt wsib_rec ON wsib_rec.inventory$warehousestockitembatchid = wsib.id

WHERE sm.movementtype NOT IN ('SHOL', 'SHREL')

      -- ignore movements to stock batches (unfortunately it can't be re-traced)
      AND (wsib_rec.warehouseshipments$receiptid IS NOT NULL or 
	coalesce(bsm.createddate, wsib.createddate) > '2018-01-01')