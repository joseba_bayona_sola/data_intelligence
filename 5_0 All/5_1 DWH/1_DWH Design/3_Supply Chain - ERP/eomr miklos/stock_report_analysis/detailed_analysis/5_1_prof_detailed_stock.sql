﻿-- tmp_detailed_stock

SELECT
	case when warehouse = 'GIR' then 'Girona'  else warehouse end warehouse, 
	datetime registered, warehouseshipments$receipt.arriveddate arrived,
	coalesce(name,productid) product, pack packsize, 
	x.supplier, document_number receipt, transaction, string_agg(DISTINCT purchaseorders$purchaseorder.ordernumber, ',') pos,
	x.unitcost erp_unitcost,
	--string_agg(DISTINCT TEXT(receipt_invoiceline.unitcost),',')
	-- 0  invoiceline_cost,
	--string_agg(DISTINCT TEXT(receipt_invoiceline.units),',')
	invoiceunitcost,
	currency, day_rate, eurate,
	rec_units received_units, case when x.units < 0 then 0 else x.units end remaining_units, x.snapunits snapunits, x.excep_units excep_units
INTO tmp_detailed_stock
FROM 
	(SELECT DISTINCT supplier, datetime, transaction, substring(currentStock.sku FROM 22 FOR 2) pack,
		AllTransactions.unitcost,
		currentStock.warehouse,
		document_number,
		productid as productidx, substring(currentStock.sku FROM 2 FOR 4) as productid,
		coalesce(currency, CASE WHEN currentStock.warehouse = 'York' THEN 'GBP' ELSE 'EUR' END) currency,
		day_rate, eurate,
		sum(currentStock.units) units, sum(snapunits) snapunits, sum(AllTransactions.units) rec_units, sum(case when currentStock.excep_batch =1  then currentStock.units else 0 end) excep_units
	FROM 
		currentStock
        LEFT OUTER JOIN 
		alltransactions USING (id)
        WHERE currentStock.warehouse in ('Amsterdam', 'York', 'GIR')
		and currentStock.id <> -1
		and transaction <> 'adjustment'
	group by supplier, datetime, transaction, substring(currentStock.sku FROM 22 FOR 2), 
		AllTransactions.unitcost, substring(currentStock.sku FROM 2 FOR 4), 
		currentStock.warehouse, document_number, 
		productid, 
		currency, day_rate, eurate) AS x
 LEFT OUTER JOIN 
	product$productfamily ON magentoproductid = productid
--LEFT OUTER JOIN receipt_invoiceline ON document_number = receipt_number
--LEFT OUTER JOIN invoiceline ON invoiceline_id = invoiceline.id
--LEFT OUTER JOIN invoice ON invoice.id = invoice_id
LEFT OUTER JOIN
	warehouseshipments$receipt ON receiptid = document_number
LEFT OUTER JOIN 
	warehouseshipments$receiptline_receipt ON warehouseshipments$receiptline_receipt.warehouseshipments$receiptid = warehouseshipments$receipt.id
LEFT OUTER JOIN 
	warehouseshipments$receiptline ON warehouseshipments$receiptline_receipt.warehouseshipments$receiptlineid = warehouseshipments$receiptline.id
LEFT OUTER JOIN 
	warehouseshipments$receiptline_purchaseorderline ON warehouseshipments$receiptline_purchaseorderline.warehouseshipments$receiptlineid = warehouseshipments$receiptline.id
LEFT OUTER JOIN 
	purchaseorders$purchaseorderline_purchaseorderlineheader ON purchaseorders$purchaseorderline_purchaseorderlineheader.purchaseorders$purchaseorderlineid = warehouseshipments$receiptline_purchaseorderline.purchaseorders$purchaseorderlineid
LEFT OUTER JOIN 
	purchaseorders$purchaseorderlineheader_purchaseorder ON purchaseorders$purchaseorderlineheader_purchaseorder.purchaseorders$purchaseorderlineheaderid = purchaseorders$purchaseorderline_purchaseorderlineheader.purchaseorders$purchaseorderlineheaderid
LEFT OUTER JOIN 
	purchaseorders$purchaseorder ON purchaseorders$purchaseorderlineheader_purchaseorder.purchaseorders$purchaseorderid = purchaseorders$purchaseorder.id
where transaction != 'adjustment'
GROUP BY x.warehouse, x.datetime, warehouseshipments$receipt.arriveddate, 
	name, x.productid,productidx,  x.pack, 
	x.supplier, document_number, x.transaction, invoiceunitcost, 
	x.currency, x.day_rate, x.eurate, 
	x.unitcost, x.rec_units, x.units, x.snapunits, x.excep_units
ORDER BY 1, 4, 2, 5, 6;


	select warehouse, registered, arrived, 
		product AS Product_Family, packsize AS Pack_Size, 
		supplier, receipt, transaction, pos, 
		erp_unitcost, invoiceunitcost, 
		currency, day_rate, eurate, 
		received_units, remaining_units, snapunits
	from tmp_detailed_stock
	order by registered desc
	
select *
from tmp_detailed_stock
-- where remaining_units <> snapunits
where snapunits = 0
order by registered desc

	select warehouse, count(*)
	from tmp_detailed_stock
	group by warehouse
	order by warehouse

	select transaction, count(*)
	from tmp_detailed_stock
	group by transaction
	order by transaction
	