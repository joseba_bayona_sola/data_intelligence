﻿
-- invoice
select id, invoice_date, invoice_number, supplier, verified
from invoice
order by invoice_date desc
limit 1000

	select count(*)
	from invoice

	select supplier, count(*)
	from invoice
	group by supplier
	order by supplier

	select verified, count(*)
	from invoice
	group by verified
	order by verified

-- invoiceline
select id, invoice_id, units, unitcost, admin, verified
from invoiceline
order by invoice_id desc
limit 1000

	select count(*)
	from invoiceline

	select invoice_id, count(*)
	from invoiceline
	group by invoice_id
	order by invoice_id

	select admin, count(*)
	from invoiceline
	group by admin
	order by admin

	select verified, count(*)
	from invoiceline
	group by verified
	order by verified		

-- purchaseorder_invoiceline
select invoiceline_id, po_number, verified
from purchaseorder_invoiceline
limit 1000

	select count(*)
	from purchaseorder_invoiceline

	select po_number, count(*)
	from purchaseorder_invoiceline
	group by po_number
	order by po_number

-- receipt_invoiceline
select invoiceline_id, receipt_number, 
	units, unitcost, arrived, registerd, 
	verified
from receipt_invoiceline
limit 1000

	select count(*)
	from receipt_invoiceline
	
	select receipt_number, count(*)
	from receipt_invoiceline
	group by receipt_number
	order by receipt_number

	