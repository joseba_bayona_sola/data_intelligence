﻿
select warehouse, sku, substring(sku from 2 for 4),
	units, snapunits, 
	id, excep_batch, 
	unitcost, 
	documnent_number
from currentstock
-- where sku = '01083B2D4AS0000000000301'
where substring(sku from 2 for 4) = '1083'
	and units < 0

-- where id = -1 -- Not processed stock - ID referes to the transaction id
-- where id <> -1 and units <> snapunits

-- where excep_batch = 1 -- Exception Batches when not stock available for issue

order by sku, warehouse
-- limit 1000

	-- 106052
	select count(*), count (distinct id), count(distinct sku)
	from currentstock
	limit 1000

	select warehouse, count(*)
	from currentstock
	group by warehouse
	order by warehouse
	limit 1000

	select id, count(*)
	from currentstock
	group by id
	order by id
	limit 1000	

	select sku, count(*)
	from currentstock
	group by sku
	order by sku
	limit 1000	

	select substring(sku from 2 for 4), id, count(*)
	from currentstock
	group by substring(sku from 2 for 4), id
	order by substring(sku from 2 for 4), id	
	limit 10000	

	select excep_batch, count(*)
	from currentstock
	group by excep_batch
	order by excep_batch
	limit 1000	
	
	select documnent_number, count(*)
	from currentstock
	group by documnent_number
	order by count(*) desc, documnent_number

-------------------------------------------

-- Check Currency Conversion

select cs.warehouse, cs.sku, cs.units, cs.snapunits, 
	cs.id, cs.excep_batch, 
	cs.unitcost, 
	cs.documnent_number, 
	at.transaction, at.datetime, at.sku, at.units, at.unitcost, at.currency, at.supplier, at.suppliertype, at.document_number
from 
		currentstock cs
	inner join
		alltransactions at on cs.id = at.id
where cs.id <> -1
	-- and cs.warehouse = 'York' and at.currency is null
	-- and at.transaction = 'adjustment' -- receipt - adjustment - issue
	and cs.units <> cs.snapunits and cs.warehouse in ('Amsterdam', 'GIR', 'York') and cs.excep_batch = 0
order by cs.warehouse, cs.sku

	select at.transaction, count(*)
	from 
			currentstock cs
		inner join
			alltransactions at on cs.id = at.id
	where cs.id <> -1
	group by at.transaction
	order by at.transaction
	
	select cs.warehouse, at.transaction, count(*)
	from 
			currentstock cs
		inner join
			alltransactions at on cs.id = at.id
	where cs.id <> -1
	group by cs.warehouse, at.transaction
	order by cs.warehouse, at.transaction

	select cs.warehouse, at.currency, count(*)
	from 
			currentstock cs
		inner join
			alltransactions at on cs.id = at.id
	where cs.id <> -1
	group by cs.warehouse, at.currency
	order by cs.warehouse, at.currency

	select cs.warehouse, at.currency, at.transaction, count(*)
	from 
			currentstock cs
		inner join
			alltransactions at on cs.id = at.id
	where cs.id <> -1
	group by cs.warehouse, at.currency, at.transaction
	order by cs.warehouse, at.currency, at.transaction	
	