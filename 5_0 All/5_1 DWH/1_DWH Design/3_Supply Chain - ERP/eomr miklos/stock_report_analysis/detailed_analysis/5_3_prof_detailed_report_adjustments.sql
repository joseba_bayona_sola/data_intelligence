﻿
-- tmp_detailed_report_adjustments


SELECT DISTINCT
        datetime, currentStock.sku, substring(currentStock.sku FROM 22 FOR 2) pack,

        -- (select unitcost from alltransactions ot where ot.transaction in ('receipt','stock') and currentStock.sku = ot.sku and ot.warehouse = currentstock.warehouse order by ot.datetime limit 1),

        case when currentStock.warehouse = 'GIR' then 'Girona' else currentStock.warehouse end as warehouse,
        document_number,
        productid as productidx,
        substring(currentStock.sku FROM 2 FOR 4) as productid,

        -- coalesce(
           -- (select ot.currency from alltransactions ot where ot.transaction in ('receipt','stock') and currentStock.sku = ot.sku and ot.warehouse = currentstock.warehouse order by ot.datetime limit 1),
            --CASE when currentStock.warehouse in ('Amsterdam','GIR') then 'EUR'else 'GBP' END
        --),
        --(select ot.day_rate from alltransactions ot where ot.transaction in ('receipt','stock') and currentStock.sku = ot.sku and ot.warehouse = currentstock.warehouse order by ot.datetime limit 1),
        --(select ot.eurate from alltransactions ot where ot.transaction in ('receipt','stock') and currentStock.sku = ot.sku and ot.warehouse = currentstock.warehouse order by ot.datetime limit 1),
        currentStock.units
INTO tmp_detailed_report_adjustments
FROM 
		currentstock
        LEFT OUTER JOIN 
		alltransactions USING (id)
WHERE currentstock.id <> -1
	and currentStock.warehouse in ('Amsterdam', 'York', 'GIR') and transaction = 'adjustment';

	select datetime, sku, unitcost, currency, day_rate, eurate
	from alltransactions ot 
	where ot.transaction in ('receipt','stock') 
		and sku = '01083B2D4BR0000000000901' and ot.warehouse = 'Amsterdam' 
	order by ot.datetime 
	limit 1


	select datetime, sku, pack, unitcost, warehouse, productidx, coalesce, day_rate, eurate, units 
	from tmp_detailed_report_adjustments

select *
from tmp_detailed_report_adjustments
order by sku
limit 1000 


