﻿
select issue_id, batch_id, units, warehouse
from allocationmap
-- where batch_id is null
order by issue_id desc
limit 1000

	-- 697188
	select count(*), count(distinct issue_id)
	from allocationmap
	limit 1000

	select warehouse, count(*)
	from allocationmap
	group by warehouse
	order by warehouse
	limit 1000

------------------------------------------------

select am.issue_id, at1.transaction, at1.datetime, at1.sku, at1.units, at1.document_number, at1.order_status,
	am.batch_id, at2.transaction, at2.datetime, at2.sku, at2.units, at2.unitcost, at2.currency, at2.supplier, at2.suppliertype, at2.document_number,
	am.units, 
	am.warehouse
from 
		allocationmap am
	inner join
		alltransactions at1 on am.issue_id = at1.id
	left join
		alltransactions at2 on am.batch_id = at2.id
where at2.id is null -- issues that could not be related to rec,stock: exception batches	

-- where at1.transaction = 'receipt' -- adjustment - receipt
-- where at2.transaction = 'issue' -- adjustment - receipt
order by issue_id desc	
limit 1000

	select at1.transaction, count(*)
	from 
			allocationmap am
		inner join
			alltransactions at1 on am.issue_id = at1.id
		left join
			alltransactions at2 on am.batch_id = at2.id
	-- where at2.id is null -- issues that could not be related to rec,stock: exception batches			
	group by at1.transaction
	order by at1.transaction

	select at1.transaction issue_tr, at2.transaction batch_tr, count(*)
	from 
			allocationmap am
		inner join
			alltransactions at1 on am.issue_id = at1.id
		left join
			alltransactions at2 on am.batch_id = at2.id
	group by at1.transaction, at2.transaction


------------------------------------------------

select at.*
from 
		alltransactions at
	left join
		allocationmap am on at.id = am.issue_id
where at.productid = '1083'
	and at.transaction = 'issue'
	and am.issue_id is null -- transactional issues that were not processed
order by at.datetime

select at.*
from 
		alltransactions at
	left join
		allocationmap am on at.id = am.issue_id
where at.productid = '1083'
	and at.transaction = 'adjustment' and at.units < 0
	and am.issue_id is null -- transactional negative adjustments that were not processed
order by at.datetime
limit 1000	