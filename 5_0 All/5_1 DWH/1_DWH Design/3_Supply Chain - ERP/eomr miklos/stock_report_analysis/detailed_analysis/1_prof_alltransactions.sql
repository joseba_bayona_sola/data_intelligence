﻿
select transaction, datetime, warehouse, sku, substring(sku from 22 for 1) packsize,
	units, unitcost, 
	currency, day_rate, eurate, 
	supplier, suppliertype, 
	document_number, order_status, receipt_units, 
	adjustment_reason, adjustment_comment, id, productid
from alltransactions
where productid = '1083'
	-- and transaction = 'stock' 
	and transaction = 'issue'
	-- and transaction = 'receipt'
	-- and transaction = 'adjustment'

	-- and units <> receipt_units
	-- and currency  = 'EUR' -- GBP - EUR - SEK - USD

	-- and transaction = 'issue' and suppliertype = 'Company'
order by datetime desc
limit 1000

	select transaction, datetime, warehouse, sku, substring(sku from 22 for 1) packsize,
		units, unitcost, 
		currency, day_rate, eurate, 
		supplier, suppliertype, 
		document_number, order_status, receipt_units, 
		adjustment_reason, adjustment_comment, id, productid
	from alltransactions
	-- where sku = '01099B3D2CG0000000000061'
	-- where sku = '01317B2D7CEC10000B200301'
	order by units desc
	limit 1000
	
	select count(*), count(distinct id)
	from alltransactions
	limit 1000

	select transaction, supplier, count(*)
	from alltransactions
	where productid = '1083'
		and suppliertype = 'Company'
	group by transaction, supplier
	order by transaction, supplier
	limit 1000

	select productid, count(*)
	from alltransactions
	group by productid
	order by productid
	limit 1000

	select datetime::date, count(*)
	from alltransactions
	group by datetime::date
	order by datetime::date desc
	limit 1000

	select length(sku), count(*)
	from alltransactions
	group by length(sku)
	order by length(sku)
	limit 1000

	select transaction, substring(sku from 22 for 1), count(*)
	from alltransactions
	where productid = '1083'
	group by transaction, substring(sku from 22 for 1)
	order by transaction, substring(sku from 22 for 1)
	limit 1000

	select transaction, adjustment_reason, adjustment_comment, count(*)
	from alltransactions
	where productid = '1083'
	group by transaction, adjustment_reason, adjustment_comment
	order by transaction, adjustment_reason, adjustment_comment
	limit 1000

	select transaction, warehouse, count(*)
	from alltransactions
	where productid = '1083'
	group by transaction, warehouse
	order by transaction, warehouse
	limit 1000
	
	select transaction, currency, count(*)
	from alltransactions
	where productid = '1083'
	group by transaction, currency
	order by transaction, currency
	limit 1000

	select transaction, suppliertype, supplier, count(*)
	from alltransactions
	where productid = '1083'
	group by transaction, suppliertype, supplier
	order by transaction, suppliertype, supplier
	limit 1000

	select count(*)
	from alltransactions
	where productid = '1083'
		and transaction = 'adjustment'
		and units < 0
		
--------------------------------------

select invoice_supplier, receipt_supplier, cost_name
from suppliermap
