﻿


-- tmp_detailed_stock_erp_batches

-- tmp_detailed_stock_erp_in_transit

SELECT sum(remainingquantity) TotalUnits, sum("batch".totalunitcostincinterco * remainingquantity) "TotalValue",
	"sendingWarehouse"."name" AS "Origin", "receivingWarehouse"."name"
into tmp_detailed_stock_erp_in_transit

FROM 
	"intersitetransfer$intransitstockitembatch" "batch"
INNER JOIN 
	"intersitetransfer$intransitstockitembatch_stockitem" "a1intersitetransfer$intransitstockitembatch_stockitem" ON "a1intersitetransfer$intransitstockitembatch_stockitem"."intersitetransfer$intransitstockitembatchid" = "batch"."id"
INNER JOIN 
	"product$stockitem" "stockitem" ON "stockitem"."id" = "a1intersitetransfer$intransitstockitembatch_stockitem"."product$stockitemid"
INNER JOIN 
	"intersitetransfer$intransitstockitembatch_transferheader" "a2intersitetransfer$intransitstockitembatch_transferheader" ON "a2intersitetransfer$intransitstockitembatch_transferheader"."intersitetransfer$intransitstockitembatchid" = "batch"."id"
INNER JOIN 
	"orderprocessing$order_transferheader" "a3orderprocessing$order_transferheader" ON "a3orderprocessing$order_transferheader"."intersitetransfer$transferheaderid" = "a2intersitetransfer$intransitstockitembatch_transferheader"."intersitetransfer$transferheaderid"
INNER JOIN 
	"orderprocessing$wholesalecustomerorders" "a4orderprocessing$wholesalecustomerorders" ON "a4orderprocessing$wholesalecustomerorders"."orderprocessing$orderid" = "a3orderprocessing$order_transferheader"."orderprocessing$orderid"
INNER JOIN 
	"intersitetransfer$intransitstockitembatch_receipt" "a5intersitetransfer$intransitstockitembatch_receipt" ON "a5intersitetransfer$intransitstockitembatch_receipt"."intersitetransfer$intransitstockitembatchid" = "batch"."id"
INNER JOIN 
	"warehouseshipments$receipt" "receipt" ON "receipt"."id" = "a5intersitetransfer$intransitstockitembatch_receipt"."warehouseshipments$receiptid"
INNER JOIN 
	"customer$wholesalecustomer_supplyingwarehouse" "a6customer$wholesalecustomer_supplyingwarehouse" ON "a6customer$wholesalecustomer_supplyingwarehouse"."customer$wholesalecustomerid" = "a4orderprocessing$wholesalecustomerorders"."customer$wholesalecustomerid"
INNER JOIN 
	"inventory$warehouse" "sendingWarehouse" ON "sendingWarehouse"."id" = "a6customer$wholesalecustomer_supplyingwarehouse"."inventory$warehouseid"
INNER JOIN 
	"customer$wholesalecustomer_warehousecustomer" "a7customer$wholesalecustomer_warehousecustomer" ON "a7customer$wholesalecustomer_warehousecustomer"."customer$wholesalecustomerid" = "a4orderprocessing$wholesalecustomerorders"."customer$wholesalecustomerid"
INNER JOIN 
	"inventory$warehouse" "receivingWarehouse" ON "receivingWarehouse"."id" = "a7customer$wholesalecustomer_warehousecustomer"."inventory$warehouseid"
INNER JOIN 
	"product$stockitem_packsize" "a8product$stockitem_packsize" ON "a8product$stockitem_packsize"."product$stockitemid" = "stockitem"."id"
INNER JOIN 
	"product$packsize" "packsize" ON "packsize"."id" = "a8product$stockitem_packsize"."product$packsizeid"
INNER JOIN 
	"product$packsize_productfamily" "a9product$packsize_productfamily" ON "a9product$packsize_productfamily"."product$packsizeid" = "packsize"."id"
INNER JOIN 
	"product$productfamily" "productfamily" ON "productfamily"."id" = "a9product$packsize_productfamily"."product$productfamilyid" 

where "productfamily".magentoproductid = '1083'
group by "sendingWarehouse"."name","receivingWarehouse"."name"


select *
from tmp_detailed_stock_erp_in_transit


-- tmp_detailed_intransit_cost