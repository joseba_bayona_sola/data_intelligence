
select top 1000 *
from Warehouse.stock.dim_wh_stock_item_v
where product_id_magento = 1083 and idWHStockItem_sk = 9

select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v
where idWHStockItem_sk = 9
	-- and idReceiptLine_sk is null
order by batch_arrived_date

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where idWHStockItemBatch_sk in 
	(select idWHStockItemBatch_sk
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where idWHStockItem_sk = 9)


select top 1000 *
from Warehouse.rec.fact_receipt_line_v
where warehouse_name = 'York' and stock_item_description = '1-Day Acuvue Moist BC8.5 DI14.2 PO+1.00 PS30'
	and receipt_status_name = 'Stock_Registered'
	and qty_received <> 0

select top 1000 *
from Warehouse.stock.fact_wh_stock_item_batch_movement_v
where idWHStockItem_sk = 9
