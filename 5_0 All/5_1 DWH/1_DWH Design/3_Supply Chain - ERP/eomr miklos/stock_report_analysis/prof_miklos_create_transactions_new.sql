﻿        -- STOCK`
select
    case when  wsib_rec.warehouseshipments$receiptid IS null then 'stock' else 'receipt' end AS transaction,
      wsib.createddate      AS datetime,
      shortname        AS warehouse,
      si.sku              AS sku,

      -- adjustment can't be tracked (positive adjustment added to receivedquantity and some of the adjustments have no timestamp)
      receivedquantity - wsib.disposedquantity +registeredquantity  AS units,
      productunitcost  AS unitcost,

      case when  wsib_rec.warehouseshipments$receiptid IS null then null ::varchar else cm.currencycode end AS currency,
      case when  wsib_rec.warehouseshipments$receiptid IS null then null ::FLOAT else cur.spotrate end AS day_rate,
      case when  wsib_rec.warehouseshipments$receiptid IS null then null ::FLOAT else eur.spotrate end AS eurate,

      suppliername AS supplier,
      suppliertype AS suppliertype,
      
      rec.receiptid AS document_number,
      NULL :: VARCHAR  AS order_status, 
      
      receivedquantity - wsib.disposedquantity +registeredquantity as receipt_units

-- select  case when  wsib_rec.warehouseshipments$receiptid IS null then 'stock' else 'receipt' end AS transaction, count(*)
FROM 
		inventory$warehousestockitembatch wsib
	LEFT OUTER JOIN 
		inventory$warehousestockitembatch_receipt wsib_rec ON wsib_rec.inventory$warehousestockitembatchid = wsib.id
	left outer join 
		warehouseshipments$receipt rec on wsib_rec.warehouseshipments$receiptid = rec.id
	left outer join 
		warehouseshipments$receipt_supplier rec_supp on rec_supp.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
		
	join 
		inventory$warehouse_companylocation wh_loc on wh_loc.inventory$warehouseid = wh.id
	join 
		companyorganisation$companylocation loc on loc.id = wh_loc.companyorganisation$companylocationid
	join 
		companyorganisation$companylocation_currency loc_curr on loc_curr.companyorganisation$companylocationid = loc.id
	join 
		countrymanagement$currency cm on loc_curr.countrymanagement$currencyid = cm.id
	left outer join 
		suppliers$supplier supp on rec_supp.suppliers$supplierid = supp.id
	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate
		JOIN 
			countrymanagement$historicalspotrate_currency ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day',  wsib.createddate  ) AND cur.currencycode = cm.currencycode
      LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate
		JOIN 
			countrymanagement$historicalspotrate_currency ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', wsib.createddate) AND eur.currencycode = 'EUR'
                                   
    -- only those that don't have receipts( shipment) attached
WHERE 
	((wsib_rec.warehouseshipments$receiptid IS null and cast(wsib.createddate  as date) < '2017.01.01') or cast(wsib.createddate  as date) < '2017.01.01')

	-- and wsib_wsi.inventory$warehousestockitemid = 52635820644988209
	and pf.magentoproductid = '1083'
-- group by  case when  wsib_rec.warehouseshipments$receiptid IS null then 'stock' else 'receipt' end 
order by datetime
    

-----------------------------------------------------------------------------

   -- ISSUES

SELECT
	'issue'                                  AS transaction,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN bsi.createddate + interval '1 hour' ELSE bsi.createddate END AS datetime,
	shortname                                AS warehouse,
	si.sku                                      AS sku,
	
	bsi.issuedquantity AS units,
	NULL :: FLOAT                            AS unitcost,

	NULL :: VARCHAR                          AS currency,
	NULL :: FLOAT                            AS day_rate,
	NULL :: FLOAT                            AS eurate,
	
	NULL :: VARCHAR                          AS supplier,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN 'Company' ELSE NULL :: VARCHAR END AS suppliertype,

	incrementid AS document_number,
	orderstatus AS order_status,
	bsi.issuedquantity AS receipt_units

-- select count(*)
FROM 
		inventory$batchstockissue bsi
	JOIN 
		inventory$batchstockissue_warehousestockitembatch bsi_wsib ON bsi_wsib.inventory$batchstockissueid = bsi.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = bsi_wsib.inventory$warehousestockitembatchid
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id 


	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id

		
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsi_si.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wh.id = wsi_wh.inventory$warehouseid

	-- inventory$batchstockallocation
	LEFT OUTER JOIN 
		inventory$batchstockissue_batchstockallocation bsi_bsa ON bsi_bsa.inventory$batchstockissueid = bsi.id
	LEFT OUTER JOIN 
		inventory$batchstockallocation bsa ON bsa.id = bsi_bsa.inventory$batchstockallocationid

	-- stockallocation$orderlinestockallocationtransaction
	LEFT OUTER JOIN 
		inventory$batchtransaction_orderlinestockallocationtransaction tr_bsa ON tr_bsa.inventory$batchstockallocationid = bsi_bsa.inventory$batchstockallocationid

	LEFT OUTER JOIN 
		stockallocation$at_order tr_oh ON tr_oh.stockallocation$orderlinestockallocationtransactionid = tr_bsa.stockallocation$orderlinestockallocationtransactionid

	-- orderprocessing$order
	LEFT OUTER JOIN 
		orderprocessing$order oh ON oh.id = tr_oh.orderprocessing$orderid

	-- orderprocessing$order_transferheader
	LEFT OUTER JOIN 
		orderprocessing$order_transferheader oh_th ON oh_th.orderprocessing$orderid = oh.id

WHERE bsi.cancelled = FALSE
	-- and  wsi_wh.inventory$warehousestockitemid = 52635820644988209
	and pf.magentoproductid = '1083'
	
-----------------------------------------------------------------------------

	
   -- RECEIVED
SELECT
	'receipt'  AS transaction,
	-- intersite receipt can't be before transfer order issue
	CASE 
		WHEN suppliertype = 'Company' AND rec.stockregistereddate - rec.arriveddate > interval '6 day' THEN coalesce(coalesce(rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		ELSE coalesce(coalesce(coalesce(wsib.stockregistereddate, rec.stockregistereddate, rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
	END AS datetime,
	shortname AS warehouse,
	si.sku AS sku,

	wsib.receivedquantity                                      AS units,
	unitcost                                                                                AS unitcost,

	cm.currencycode                                                                         AS currency,
	cur.spotrate                                                                            AS day_rate,
	eur.spotrate                                                                            AS eurate,

	suppliername                                                                            AS supplier,
	suppliertype                                                                            AS suppliertype,

	rec.receiptid                                                   AS document_number,
	NULL :: VARCHAR                                                                         AS order_status,

	recl.quantityreceived                                         AS receipt_quantity

-- select count(*)
FROM 
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency AS cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		product$stockitem si ON si.id = pol_si.product$stockitemid

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
		
	join 
		inventory$warehousestockitembatch_receiptline wsib_recl on wsib_recl.warehouseshipments$receiptlineid = recl.id
	join 
		inventory$warehousestockitembatch wsib on wsib.id = wsib_recl.inventory$warehousestockitembatchid

WHERE rec.status = 'Stock_Registered' AND recl.quantityreceived > 0 
	and cast(wsib.createddate as date)  >= '2017.01.01'    
	-- and pol_si.product$stockitemid = 32088147345062653 and rec_wh.inventory$warehouseid = 46161896180547586
	and pf.magentoproductid = '1083'


-- RECEIVED where purchase order are for some reason stock items this is a tad dangerous as we are grabbing the sku from a text field on 
-- the receipt rather than where is should properly be
SELECT
	'receipt'  AS transaction,
	-- intersite receipt can't be before transfer order issue
	CASE 
		WHEN suppliertype = 'Company' AND rec.stockregistereddate - rec.arriveddate > interval '6 day' THEN coalesce(coalesce(rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		ELSE coalesce(coalesce(coalesce(wsib.stockregistereddate, rec.stockregistereddate, rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
	END AS datetime,
	shortname                                                                               AS warehouse,
	stockitem                                                                                   AS sku,

	wsib.receivedquantity                                         AS units,
	unitcost                                                                                AS unitcost,

	cm.currencycode                                                                         AS currency,
	cur.spotrate                                                                            AS day_rate,
	eur.spotrate                                                                            AS eurate,

	suppliername                                                                            AS supplier,
	suppliertype                                                                            AS suppliertype,

	rec.receiptid                                                   AS document_number,
	NULL :: VARCHAR                                                                         AS order_status,
	recl.quantityreceived                                         AS receipt_quantity
FROM 
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency AS cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	left JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id

	join 
		inventory$warehousestockitembatch_receiptline wsib_recl on wsib_recl.warehouseshipments$receiptlineid = recl.id
	join 
		inventory$warehousestockitembatch wsib on wsib.id = wsib_recl.inventory$warehousestockitembatchid

WHERE rec.status = 'Stock_Registered' AND recl.quantityreceived > 0 
	and cast(wsib.createddate as date)  >= '2017.01.01'
        and pol_si.purchaseorders$purchaseorderlineid is null	
        and pol_si.product$stockitemid = 32088147345062653 and rec_wh.inventory$warehouseid = 46161896180547586


 -----------------------------------------------------------------------------


-- ADJUST
SELECT
	'adjustment'                                            AS transaction,
	coalesce(bsm.createddate, wsib.createddate) + interval '1 hour' AS datetime,
	shortname                                               AS warehouse,
	si.sku                                                     AS sku,

	CASE
		WHEN batchstockmovementtype = 'Manual_Negative' THEN -1
		WHEN batchstockmovementtype = 'Manual_Positive' THEN 1

		--- movetype	movementtype	fromstate	tos`tate	stockadjustment
	
		WHEN movetype = 'SREG' AND movementtype =	'SREG' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1

		WHEN movetype = 'SADJ' AND movementtype = 'ERPADJ'  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype =	'ERPADJ'  AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISS' AND movementtype = 'SDISS' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISS' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISI' AND movementtype = 'SDISI' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISI' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SREL' AND movementtype = 'SREL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'ERPADJ'  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'SHOL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'ERP Batch' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'W'	THEN 0

		ELSE 0
      END * quantity AS units,
      NULL                                                    AS unitcost,
      
      NULL                                                    AS currency,
      NULL                                                    AS day_rate,
      NULL                                                    AS eurate,
      
      NULL                                                    AS supplier,
      NULL                                                    AS suppliertype,
      
      NULL :: VARCHAR                                         AS document_number,
      NULL :: VARCHAR                                         AS order_status,

	CASE
		WHEN batchstockmovementtype = 'Manual_Negative' THEN -1
		WHEN batchstockmovementtype = 'Manual_Positive' THEN 1

		--- movetype	movementtype	fromstate	tos`tate	stockadjustment
		WHEN movetype = 'SREG' AND movementtype =	'SREG' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1

		WHEN movetype = 'SADJ' AND movementtype = 'ERPADJ'  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype =	'ERPADJ'  AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISS' AND movementtype = 'SDISS' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISS' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISI' AND movementtype = 'SDISI' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISI' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SREL' AND movementtype = 'SREL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'ERPADJ'  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'SHOL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'ERP Batch' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'W'	THEN 0

		ELSE 0
      END * quantity AS receipt_quantity

-- select count(*)
FROM 
		inventory$batchstockmovement bsm
	JOIN 
		inventory$batchstockmovement_warehousestockitembatch bsm_wsib ON bsm_wsib.inventory$batchstockmovementid = bsm.id
	JOIN 
		inventory$warehousestockitembatch wsib ON bsm_wsib.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		inventory$batchstockmovement_stockmovement bsm_sm ON bsm_sm.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN 
		inventory$stockmovement sm ON bsm_sm.inventory$stockmovementid = sm.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id


	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
				
	LEFT OUTER JOIN 
		inventory$warehousestockitembatch_receipt wsib_rec ON wsib_rec.inventory$warehousestockitembatchid = wsib.id

    WHERE coalesce(movementtype,'') NOT IN ('SHOL', 'SREL')
          -- ignore movements to stock batches (unfortunately it can't be re-traced)
          -- ( -- adjustments to non-receipt related batches can not be re-played as we don't know initial number of units
        and cast(wsib.createddate  as date) >= '2017.01.01'

	-- and wsib.id = 52635820644988209		
	and pf.magentoproductid = '1083'

	
	