﻿
-- tmp_invoice_cut_off

create table if not exists tmp_invoice_cut_off (cut_off date default current_date );

insert into tmp_invoice_cut_off
	select current_date
	where not exists (
	    select *
	    from tmp_invoice_cut_off);

-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- tmp_fix_receipt_date

select rec.id, rec.receiptid, rec.stockregistereddate stockregistereddate_receipt, min(wsib.stockregistereddate) stockregistereddate_batch 
into tmp_fix_receipt_date
from
		warehouseshipments$receipt rec
	join 
		warehouseshipments$receiptline_receipt rl_rec on rl_rec.warehouseshipments$receiptid = rec.id
	join 
		warehouseshipments$receiptline rl on rl_rec.warehouseshipments$receiptlineid = rl.id
	join 
		inventory$warehousestockitembatch_receiptline wsib_rl on wsib_rl.warehouseshipments$receiptlineid = rl.id
	join 
		inventory$warehousestockitembatch wsib on wsib.id = wsib_rl.inventory$warehousestockitembatchid
group by rec.id, rec.stockregistereddate
having
    rec.stockregistereddate> min(wsib.stockregistereddate) and (rec.stockregistereddate - min(wsib.stockregistereddate)) > interval '1 hour';

create index tmp_fix_receipt_date_idx_receipt_id on tmp_fix_receipt_date (id);

-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- tmp_ignore_receipt_lines

select distinct rec.receiptid, rl.id receiptline_id, rl.stockitem, rec.stockregistereddate 
into tmp_ignore_receipt_lines
from
		warehouseshipments$receipt rec
	left join 
		warehouseshipments$receiptline_receipt rl_rec on rl_rec.warehouseshipments$receiptid = rec.id
	left join 
		warehouseshipments$receiptline rl on rl_rec.warehouseshipments$receiptlineid = rl.id
	left join 
		inventory$warehousestockitembatch_receiptline wsib_rl on wsib_rl."warehouseshipments$receiptlineid" = rl.id
where rl.quantityaccepted > 0 and wsib_rl."inventory$warehousestockitembatchid" is null
    and rec.stockregistereddate > '2017.01.01' and rec.shipmenttype != 'Aggregated'
order by rec.stockregistereddate desc;

create index tmp_ignore_receipt_lines_idx_receipt_line_id on tmp_ignore_receipt_lines (receiptline_id);

-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- tmp_receipt_line_invoiced_cutoff

select rl.id as receiptline_id,
	max(rl.stockitem) stockitem, 
	sum(il.quantity) invoiced_cut_off, 
	max(rl.quantityoninvoice) quantityoninvoice, max(rl.quantityaccepted) accepted, 
	max(i.posteddate) last_posteddate, max(i.invoiceref) invoiceref 
into tmp_receipt_line_invoiced_cutoff
from
		"invoicereconciliation$invoiceline" il
	inner join 
		"invoicereconciliation$invoiceline_invoicelineheader" il_ilh on il_ilh."invoicereconciliation$invoicelineid" = il.id
	inner join 
		invoicereconciliation$invoicelineheader ilh on il_ilh."invoicereconciliation$invoicelineheaderid" = ilh.id
	inner join 
		"invoicereconciliation$invoicelineheader_invoice" ilh_i on ilh_i."invoicereconciliation$invoicelineheaderid" = ilh.id
	inner join 
		invoicereconciliation$invoice i on ilh_i."invoicereconciliation$invoiceid" = i.id
	inner join 
		"invoicereconciliation$invoiceline_receiptline" il_rl on il_rl."invoicereconciliation$invoicelineid" = il.id
	inner join 
		"warehouseshipments$receiptline" rl on il_rl."warehouseshipments$receiptlineid" = rl.id
where
        cast(i.posteddate as date) < (select cut_off from tmp_invoice_cut_off) and i.status = 'Posted'
group by rl.id ; 


-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- STOCK
-- Represent opening stock batches these are batches with no receipts

	-- Changes: 
		-- Filter: Before by date (Before 2017.01.01) - (Now NO Adjustments: (wsib.registeredquantity + wsib.disposedquantity) = 0)
		-- Logic for transaction - currency - suppliertype - document_number

select
	case when wsib_rec.warehouseshipments$receiptid is null and wsib.createddate <= cast('2017.01.01' as date) then 'stock' else 'receipt' end as transaction,
        wsib.createddate as datetime,
        wh.shortname as warehouse,
        si.sku as sku,
        
        -- potentially this will not be a problem if patches ru
        -- adjustment can't be tracked (positive adjustment added to receivedquantity and some of the adjustments have no timestamp)
	wsib.receivedquantity - wsib.disposedquantity + wsib.registeredquantity as units,
        wsib.productunitcost as unitcost,
        null ::float as invoiceunitcost,

        case when wsib_rec.warehouseshipments$receiptid is null then 
		case when wh.shortname in ('GIR','Amsterdam') then 'EUR' else 'GBP' end else cm.currencycode
        end as currency,
        case when wsib_rec.warehouseshipments$receiptid is null then null ::float else cur.spotrate end as day_rate,
        case when wsib_rec.warehouseshipments$receiptid is null then null ::float else eur.spotrate end as eurate,

        supp.suppliername as supplier,
        case when supp.suppliertype = 'Company' and rec.shipmenttype != 'Intersite' then null else suppliertype end as suppliertype,

        coalesce(rec.receiptid ::varchar, wsib.batch_id ::varchar) as document_number,
        null :: varchar as order_status,

        wsib.receivedquantity - wsib.disposedquantity + wsib.registeredquantity as receipt_units,

        null ::varchar as adjustment_reason,
        null ::varchar as adjustment_comment,

        null ::varchar as intersite_id,
        null ::varchar as invoice_ref,
        null ::date as invoice_posted_date

from
		inventory$warehousestockitembatch wsib
	LEFT OUTER JOIN 
		inventory$warehousestockitembatch_receipt wsib_rec ON wsib_rec.inventory$warehousestockitembatchid = wsib.id
	left outer join 
		warehouseshipments$receipt rec on wsib_rec.warehouseshipments$receiptid = rec.id
	left outer join 
		warehouseshipments$receipt_supplier rec_supp on rec_supp.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
		
	join 
		inventory$warehouse_companylocation wh_loc on wh_loc.inventory$warehouseid = wh.id
	join 
		companyorganisation$companylocation loc on loc.id = wh_loc.companyorganisation$companylocationid
	join 
		companyorganisation$companylocation_currency loc_curr on loc_curr.companyorganisation$companylocationid = loc.id
	join 
		countrymanagement$currency cm on loc_curr.countrymanagement$currencyid = cm.id
	left outer join 
		suppliers$supplier supp on rec_supp.suppliers$supplierid = supp.id
	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate
		JOIN 
			countrymanagement$historicalspotrate_currency ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day',  wsib.createddate  ) AND cur.currencycode = cm.currencycode
      LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate
		JOIN 
			countrymanagement$historicalspotrate_currency ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', wsib.createddate) AND eur.currencycode = 'EUR'


        -- only those that don't have receipts( shipment) attached        

where wsib_rec.warehouseshipments$receiptid is null and (wsib.registeredquantity + wsib.disposedquantity) = 0     
	and pf.magentoproductid = '1083'

-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- ISSUES	

select 
	'issue'                                  AS transaction,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN bsi.createddate + interval '1 hour' ELSE bsi.createddate END AS datetime,
	shortname                                AS warehouse,
	si.sku                                      AS sku,
	
	bsi.issuedquantity AS units,
	NULL :: FLOAT                            AS unitcost,

	NULL :: VARCHAR                          AS currency,
	NULL :: FLOAT                            AS day_rate,
	NULL :: FLOAT                            AS eurate,
	
	NULL :: VARCHAR                          AS supplier,
	CASE WHEN oh_th.intersitetransfer$transferheaderid IS NOT NULL THEN 'Company' ELSE NULL :: VARCHAR END AS suppliertype,

	incrementid AS document_number,
	orderstatus AS order_status,
	bsi.issuedquantity AS receipt_units

-- select count(*)
FROM 
		inventory$batchstockissue bsi
	JOIN 
		inventory$batchstockissue_warehousestockitembatch bsi_wsib ON bsi_wsib.inventory$batchstockissueid = bsi.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = bsi_wsib.inventory$warehousestockitembatchid
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id 


	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id

		
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsi_si.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wh.id = wsi_wh.inventory$warehouseid

	-- inventory$batchstockallocation
	LEFT OUTER JOIN 
		inventory$batchstockissue_batchstockallocation bsi_bsa ON bsi_bsa.inventory$batchstockissueid = bsi.id
	LEFT OUTER JOIN 
		inventory$batchstockallocation bsa ON bsa.id = bsi_bsa.inventory$batchstockallocationid

	-- stockallocation$orderlinestockallocationtransaction
	LEFT OUTER JOIN 
		inventory$batchtransaction_orderlinestockallocationtransaction tr_bsa ON tr_bsa.inventory$batchstockallocationid = bsi_bsa.inventory$batchstockallocationid

	LEFT OUTER JOIN 
		stockallocation$at_order tr_oh ON tr_oh.stockallocation$orderlinestockallocationtransactionid = tr_bsa.stockallocation$orderlinestockallocationtransactionid

	-- orderprocessing$order
	LEFT OUTER JOIN 
		orderprocessing$order oh ON oh.id = tr_oh.orderprocessing$orderid

	-- orderprocessing$order_transferheader
	LEFT OUTER JOIN 
		orderprocessing$order_transferheader oh_th ON oh_th.orderprocessing$orderid = oh.id

WHERE bsi.cancelled = FALSE
	-- and  wsi_wh.inventory$warehousestockitemid = 52635820644988209
	and pf.magentoproductid = '1083'

-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- RECEIVED_STOCK_ITEM
      -- Info from receipts

      -- Changes: 
		-- Filter: Not Status - Created Date use // quantityaccepted instead of quantityreceived // Aggregated excluded // Ingore RL
		-- Attributes: Not WSIB used: Stock Register Date, Received Quantity // invoiceunitcost // suppliertype

select 'receipt' as transaction,
        -- intersite receipt can't be before transfer order issue
	case
		when suppliertype = 'Company' and coalesce(rec.stockregistereddate, stockregistereddate_batch) - rec.arriveddate > interval '6 day' then coalesce(coalesce(stockregistereddate_batch, rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		else coalesce(coalesce(coalesce(stockregistereddate_batch, rec.stockregistereddate, rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
        end as datetime,
        shortname as warehouse,
        si.sku as sku,

        -- only take units that are not invoices at this cost from the receipt
        recl.quantityaccepted - coalesce(t2.invoiced_cut_off,0) as units,
        unitcost as unitcost,
        recl.invoiceunitcost,
        
        cm.currencycode as currency,
        cur.spotrate as day_rate,
        eur.spotrate as eurate,
        
        suppliername as supplier,
        case when suppliertype = 'Company' and shipmenttype != 'Intersite' then null else suppliertype end as suppliertype,
        
        rec.receiptid as document_number,
        null :: varchar as order_status,
        
        recl.quantityreceived as receipt_quantity,

        null ::varchar as adjustment_reason,
        null ::varchar as adjustment_comment,

        o.incrementid as intersite_id,
        null ::varchar as invoice_ref,
        null ::date as invoice_posted_date

FROM 
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency AS cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		product$stockitem si ON si.id = pol_si.product$stockitemid

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id		

	left join 
		"purchaseorders$purchaseorderline_purchaseorderlineheader" pol_polh on pol_polh."purchaseorders$purchaseorderlineid" = pol.id
	left join 
		purchaseorders$purchaseorderlineheader polh on polh.id = pol_polh.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorder po on polh_po.purchaseorders$purchaseorderid = po.id
	left join 
		purchaseorders$transferpo_transferheader tpo_th on po.id = tpo_th.purchaseorders$purchaseorderid
	left join 
		intersitetransfer$transferheader th on tpo_th.intersitetransfer$transferheaderid = th.id
	left join 
		orderprocessing$order_transferheader o_th on th.id = o_th.intersitetransfer$transferheaderid
	left join 
		orderprocessing$order o on o_th.orderprocessing$orderid = o.id

	left join 
		tmp_fix_receipt_date t1 on t1.id = rec.id
	left join 
		tmp_receipt_line_invoiced_cutoff t2 on t2.receiptline_id = recl.id
where
        recl.quantityaccepted > 0 and pol_si.purchaseorders$purchaseorderlineid is not null
        and shipmenttype != 'Aggregated'
       -- and warehouseshipments$receipt.status != 'Cancelled'
        and recl.id not in (select receiptline_id from tmp_ignore_receipt_lines)
        and (recl.quantityaccepted - coalesce(t2.invoiced_cut_off,0)) > 0
	and pf.magentoproductid = '1083'
		
        
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- RECEIVED_NO_STOCK_ITEM
      -- Some receipts exist where purchase order is missing an association to a stock items 
      -- the fix is a tad dangerous as we are grabbing the sku from a text field on the receipt rather than where is should properly be

 select 'receipt' as transaction,
        -- intersite receipt can't be before transfer order issue
	CASE 
		WHEN suppliertype = 'Company' AND rec.stockregistereddate - rec.arriveddate > interval '6 day' THEN coalesce(coalesce(rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		ELSE coalesce(coalesce(coalesce(rec.stockregistereddate, rec.stockregistereddate, rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
	END AS datetime,

	shortname                                                                               AS warehouse,
	recl.stockitem                                                                                   AS sku,

        -- only take units that are not invoices at this cost from the receipt
        recl.quantityaccepted - coalesce(t2.invoiced_cut_off,0) as units,
        unitcost as unitcost,
        recl.invoiceunitcost,

        cm.currencycode as currency,
        cur.spotrate as day_rate,
        eur.spotrate as eurate,
        
        suppliername as supplier,
        case when suppliertype = 'Company' and shipmenttype != 'Intersite' then null else suppliertype end as suppliertype,
        
        rec.receiptid as document_number,
        null :: varchar as order_status,
        
        recl.quantityreceived as receipt_quantity,
        
        null ::varchar as adjustment_reason,
        null ::varchar as adjustment_comment,
        
        o.incrementid as intersite_id,
        null ::varchar as invoice_ref,
        null ::date as invoice_posted_date

from
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency AS cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	left JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id


	left join 
		"purchaseorders$purchaseorderline_purchaseorderlineheader" pol_polh on pol_polh."purchaseorders$purchaseorderlineid" = pol.id
	left join 
		purchaseorders$purchaseorderlineheader polh on polh.id = pol_polh.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorder po on polh_po.purchaseorders$purchaseorderid = po.id
	left join 
		purchaseorders$transferpo_transferheader tpo_th on po.id = tpo_th.purchaseorders$purchaseorderid
	left join 
		intersitetransfer$transferheader th on tpo_th.intersitetransfer$transferheaderid = th.id
	left join 
		orderprocessing$order_transferheader o_th on th.id = o_th.intersitetransfer$transferheaderid
	left join 
		orderprocessing$order o on o_th.orderprocessing$orderid = o.id
	left join 
		tmp_fix_receipt_date t1 on t1.id = rec.id
	left join 
		tmp_receipt_line_invoiced_cutoff t2 on t2.receiptline_id = recl.id

 where
	recl.quantityaccepted > 0 
	and pol_si.purchaseorders$purchaseorderlineid is null
        and shipmenttype != 'Aggregated'
     --   and warehouseshipments$receipt.status != 'Cancelled'
        and recl.id not in (select receiptline_id from tmp_ignore_receipt_lines)
        and  (recl.quantityaccepted - coalesce(t2.invoiced_cut_off,0)) > 0
	and pol_si.product$stockitemid = 32088147345062653 and rec_wh.inventory$warehouseid = 46161896180547586	


-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- SUB_END RECEIVED_NO_STOCK_ITEM filter and stockitem = ''


select
	'receipt' as transaction,
	-- intersite receipt can't be before transfer order issue
	case
		when suppliertype = 'Company' and coalesce(rec.stockregistereddate, stockregistereddate_batch) - rec.arriveddate > interval '6 day' then coalesce(coalesce(stockregistereddate_batch, rec.stockregistereddate, rec.confirmeddate), rec.arriveddate)
		else coalesce(coalesce(coalesce(stockregistereddate_batch, rec.stockregistereddate, rec.stockregistereddate, rec.confirmeddate), rec.createddate), rec.arriveddate)
        end as datetime,
        shortname as warehouse,
        si.sku as sku,
    
	il.quantity as units,
	recl.unitcost as unitcost,
	il.adjustedunitcost as invoiceunitcost,
	
	cm.currencycode as currency,
	cur.spotrate as day_rate,
	eur.spotrate as eurate,

	suppliername as supplier,
	case when suppliertype = 'Company' and shipmenttype != 'Intersite' then null else suppliertype end as suppliertype,

	rec.receiptid as document_number,
	null :: varchar as order_status,

	recl.quantityreceived as receipt_quantity,

	null ::varchar as adjustment_reason,
	null ::varchar as adjustment_comment,

	o.incrementid as intersite_id,
	i.invoiceref as invoice_ref,
	i.posteddate as invoice_posted_date

FROM 
		warehouseshipments$receipt rec
	JOIN 
		warehouseshipments$receipt_warehouse rec_wh ON rec_wh.warehouseshipments$receiptid = rec.id
	JOIN 
		inventory$warehouse wh ON rec_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		warehouseshipments$receipt_supplier rec_supp ON rec_supp.warehouseshipments$receiptid = rec.id
	LEFT OUTER JOIN 
		suppliers$supplier supp ON supp.id = rec_supp.suppliers$supplierid
	LEFT OUTER JOIN 
		suppliers$supplier_currency supp_curr ON rec_supp.suppliers$supplierid = supp_curr.suppliers$supplierid
	LEFT OUTER JOIN 
		countrymanagement$currency AS cm ON cm.id = supp_curr.countrymanagement$currencyid

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND cur.currencycode = cm.currencycode

	LEFT OUTER JOIN
		(SELECT date_trunc('day', effectivedate) AS ed2, currencycode, value AS spotrate
		FROM 
			countrymanagement$spotrate sr
		JOIN 
			countrymanagement$historicalspotrate_currency sr_curr ON sr.id = sr_curr.countrymanagement$spotrateid
		JOIN 
			countrymanagement$currency curr ON curr.id = sr_curr.countrymanagement$currencyid
		GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(rec.arriveddate, rec.confirmeddate), rec.createddate)) AND eur.currencycode = 'EUR'

	JOIN 
		warehouseshipments$receiptline_receipt recl_rec ON recl_rec.warehouseshipments$receiptid = rec.id
	JOIN 
		warehouseshipments$receiptline recl ON recl_rec.warehouseshipments$receiptlineid = recl.id
	JOIN 
		warehouseshipments$receiptline_purchaseorderline recl_pol ON recl_pol.warehouseshipments$receiptlineid = recl.id
	JOIN 
		purchaseorders$purchaseorderline pol ON recl_pol.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		purchaseorders$purchaseorderline_stockitem pol_si ON pol_si.purchaseorders$purchaseorderlineid = pol.id
	JOIN 
		product$stockitem si ON si.id = pol_si.product$stockitemid

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id		

	left join 
		"purchaseorders$purchaseorderline_purchaseorderlineheader" pol_polh on pol_polh."purchaseorders$purchaseorderlineid" = pol.id
	left join 
		purchaseorders$purchaseorderlineheader polh on polh.id = pol_polh.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorderlineheader_purchaseorder polh_po on polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
	left join 
		purchaseorders$purchaseorder po on polh_po.purchaseorders$purchaseorderid = po.id
	left join 
		purchaseorders$transferpo_transferheader tpo_th on po.id = tpo_th.purchaseorders$purchaseorderid
	left join 
		intersitetransfer$transferheader th on tpo_th.intersitetransfer$transferheaderid = th.id
	left join 
		orderprocessing$order_transferheader o_th on th.id = o_th.intersitetransfer$transferheaderid
	left join 
		orderprocessing$order o on o_th.orderprocessing$orderid = o.id

	left join 
		tmp_fix_receipt_date t1 on t1.id = rec.id

	inner join 
		"invoicereconciliation$invoiceline_receiptline" il_rl on il_rl."warehouseshipments$receiptlineid" = recl.id
	inner join 
		"invoicereconciliation$invoiceline" il on il_rl."invoicereconciliation$invoicelineid" = il.id
	inner join 
		"invoicereconciliation$invoiceline_invoicelineheader" il_ilh on il_ilh."invoicereconciliation$invoicelineid" = il.id
	inner join 
		invoicereconciliation$invoicelineheader ilh on il_ilh."invoicereconciliation$invoicelineheaderid" = ilh.id
	inner join 
		"invoicereconciliation$invoicelineheader_invoice" ilh_i on ilh_i."invoicereconciliation$invoicelineheaderid" = ilh.id
	inner join 
		invoicereconciliation$invoice i on ilh_i."invoicereconciliation$invoiceid" = i.id

 where
	recl.quantityaccepted > 0 and pol_si.purchaseorders$purchaseorderlineid is not null
	and shipmenttype != 'Aggregated'
	--and warehouseshipments$receipt.status != 'Cancelled'
	and recl.id not in (select receiptline_id from tmp_ignore_receipt_lines)
	and cast(i.posteddate as date) < (select cut_off from tmp_invoice_cut_off)	
	and pf.magentoproductid = '1083'


-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------


-- ADJUST
      -- Patch 62 atempts to fix fugures on batches with adjustments where the adjumenst are at the batch level but no supporting adjustments exists
      -- the hour interval is added to try make sure that these adjustments get dealt with in the correct order

select 
	'adjustment' as transaction,
        case 
          when comment like '%Patch%62%' then wsib.createddate + interval '1 hour'
          else coalesce(bsm.createddate, wsib.createddate) + interval '1 hour'
        end as datetime,
        shortname as warehouse,
        si.sku as sku,

        case
		when batchstockmovementtype = 'Manual_Negative' then -1
		when batchstockmovementtype = 'Manual_Positive' then 1
          
		when stockadjustment = 'Negative' then -1
		when stockadjustment = 'Positive' then 1
          
		WHEN movetype = 'SREG' AND movementtype =	'SREG' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1

		WHEN movetype = 'SADJ' AND movementtype = 'ERPADJ'  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype =	'ERPADJ'  AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISS' AND movementtype = 'SDISS' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISS' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISI' AND movementtype = 'SDISI' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISI' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SREL' AND movementtype = 'SREL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'ERPADJ'  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'SHOL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'ERP Batch' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'W'	THEN 0

		ELSE 0
	END * quantity AS units,
	null as unitcost,
	null ::float as invoiceunitcost,

	null as currency,
	null as day_rate,
	null as eurate,

	null as supplier,
	null as suppliertype,

	batchstockmovement_id ::varchar as document_number,
	null :: varchar as order_status,
	
	case
		when batchstockmovementtype = 'Manual_Negative' then -1
		when batchstockmovementtype = 'Manual_Positive' then 1
          
		when stockadjustment = 'Negative' then -1
		when stockadjustment = 'Positive' then 1
          
		WHEN movetype = 'SREG' AND movementtype =	'SREG' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1

		WHEN movetype = 'SADJ' AND movementtype = 'ERPADJ'  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype =	'ERPADJ'  AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1
		WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISS' AND movementtype = 'SDISS' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISS' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SDISI' AND movementtype = 'SDISI' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
		WHEN movetype = 'SDISI' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

		WHEN movetype = 'SREL' AND movementtype = 'SREL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'ERPADJ'  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'SHOL' AND movementtype = 'SHOL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
		WHEN movetype = 'ERP Batch' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'W'	THEN 0

		ELSE 0
        end * quantity as receipt_quantity,

	case
		when operator = 'SYSTEM' then 'Stock Sync'
		when batchstockmovementtype in ('Manual_Negative', 'Manual_Positive') then 'Manual Stock Movement'
		when _class in ('STOCK_DISPOSE_STU', 'STOCK_DISPOSE_ITEM') then
			case
				when reasonid = '01' then 'Damages'
				when reasonid = '02' then 'Stock Check'
				when reasonid = '03' then 'Sales Sample'
				when reasonid = '99' then 'Warehouse Adjust Only'
				when reasonid = 'AU' then 'Audit'
				else 'Unknown Disposal - ' || coalesce(_class, '') || ' - Reason ID - ' || coalesce(reasonid, '')
			end
		when _class in ('STOCK_ADJUST') then
			case
				when reasonid = '01' then 'Damages'
				when reasonid = '02' then 'Stock Check'
				when reasonid = '03' then 'Sales Sample'
				when reasonid = '99' then 'Warehouse Adjust Only'
				when reasonid = 'AU' then 'Audit'
				else 'Unknown Adjustment - ' || coalesce(_class, '') || ' - Reason ID - ' || coalesce(reasonid, '')
			end
		when _class in ('STOCK_REGISTER') then
			case
				when reasonid = '01' then 'Damages Adjustment'
				when reasonid = '02' then 'Stock Check Adjustment'
				when reasonid = '99' then 'Warehouse Adjust Only'
				when reasonid = 'AU' then 'Audit'
				else 'Unknown Stock Register - ' || coalesce(_class, '') || ' - Reason ID - ' || coalesce(reasonid, '')
			end
		when _class in ('STOCK_REGISTER') then
			case
				when reasonid = '01' then 'Damages AdjuZZstment'
				when reasonid = '02' then 'Stock Check Adjustment'
				when reasonid = '99' then 'Warehouse Adjust Only'
				when reasonid = 'AU' then 'Audit'
				else 'Unknown Stock Register - ' || coalesce(_class, '') || ' - Reason ID - ' || coalesce(reasonid, '')
			end
		else 'Unknown Stock Movement'
        end adjustment_reason,
        regexp_replace(comment, '[",]', ' ') as adjustment_comment,        

        null ::varchar as intersite_id,
        null ::varchar as invoice_ref,
        null ::date as invoice_posted_date

from
		inventory$batchstockmovement bsm
	JOIN 
		inventory$batchstockmovement_warehousestockitembatch bsm_wsib ON bsm_wsib.inventory$batchstockmovementid = bsm.id
	JOIN 
		inventory$warehousestockitembatch wsib ON bsm_wsib.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$warehousestockitembatch_warehousestockitem wsib_wsi ON wsib_wsi.inventory$warehousestockitembatchid = wsib.id
	JOIN 
		inventory$located_at wsi_wh ON wsi_wh.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		inventory$warehouse wh ON wsi_wh.inventory$warehouseid = wh.id
	LEFT OUTER JOIN 
		inventory$batchstockmovement_stockmovement bsm_sm ON bsm_sm.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN 
		inventory$stockmovement sm ON bsm_sm.inventory$stockmovementid = sm.id
	JOIN 
		inventory$warehousestockitem_stockitem wsi_si ON wsi_si.inventory$warehousestockitemid = wsib_wsi.inventory$warehousestockitemid
	JOIN 
		product$stockitem si ON wsi_si.product$stockitemid = si.id

	inner join
		product$stockitem_product si_p on si.id = si_p.product$stockitemid
	inner join	
		product$product p on si_p.product$productid = p.id
	inner join	
		product$product_productfamily p_pf on p.id = p_pf.product$productid
	inner join	
		product$productfamily pf on p_pf.product$productfamilyid = pf.id
		
where coalesce(movementtype, '') not in ('SHOL', 'SREL')
	and pf.magentoproductid = '1083'
        
        
-----------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------

-- CANCEL
-- A record only for in transit batches to deal with cases where the demand for an item gets canceled

select 
	'cancel' as transaction,
        batch.createddate datetime,
        rw."name" as warehouse,
        sku,

        lostquantity as units,
        batch.productunitcost unitcost,
        null ::float as invoiceunitcost,

        Currency as currency,
        cur.spotrate as day_rate,
        eur.spotrate as eurate,

        concat('VD ', sw.name) as supplier,
        'Company' as suppliertype,

        coalesce(r.receiptid, 'UNKNOWN-RECIEPT') as document_number,
        null :: varchar as order_status,

        lostquantity as receipt_quantity,

        null ::varchar as adjustment_reason,
        null ::varchar as adjustment_comment,

        o.incrementid as intersite_id,
        null ::varchar as invoice_ref,
        null ::date as invoice_posted_date
from
		"intersitetransfer$intransitstockitembatch" "batch"
	inner join 
		"intersitetransfer$intransitstockitembatch_stockitem" isib_si on isib_si."intersitetransfer$intransitstockitembatchid" = "batch"."id"
	inner join 
		"product$stockitem" si on si."id" = isib_si."product$stockitemid"
	inner join 
		"intersitetransfer$intransitstockitembatch_transferheader" isib_th on isib_th."intersitetransfer$intransitstockitembatchid" = "batch"."id"
	inner join 
		"orderprocessing$order_transferheader" o_th on o_th."intersitetransfer$transferheaderid" = isib_th."intersitetransfer$transferheaderid"
	left join 
		"orderprocessing$wholesalecustomerorders" who on who."orderprocessing$orderid" = o_th."orderprocessing$orderid"
	left join 
		"intersitetransfer$intransitstockitembatch_receipt" isib_r on isib_r."intersitetransfer$intransitstockitembatchid" = "batch"."id"
	left join 
		"warehouseshipments$receipt" r on r."id" = isib_r."warehouseshipments$receiptid"
	left join 
		"customer$wholesalecustomer_supplyingwarehouse" wcsw on wcsw."customer$wholesalecustomerid" = who."customer$wholesalecustomerid"
	left join 
		"inventory$warehouse" sw on sw."id" = wcsw."inventory$warehouseid"
	left join 
		"customer$wholesalecustomer_warehousecustomer" wcrw on wcrw."customer$wholesalecustomerid" = who."customer$wholesalecustomerid"
	left join 
		"inventory$warehouse" rw on rw."id" = wcrw."inventory$warehouseid"
	left join 
		"product$stockitem_packsize" si_ps on si_ps."product$stockitemid" = si."id"
	left join 
		"product$packsize" ps on ps."id" = si_ps."product$packsizeid"
	left join 
		"product$packsize_productfamily" ps_pf on ps_pf."product$packsizeid" = ps."id"
	left join 
		"product$productfamily" pf on pf."id" = ps_pf."product$productfamilyid"
	left outer join 
		(select date_trunc('day', effectivedate) as ed, currencycode, value as spotrate
		from
			countrymanagement$spotrate
		join 
			countrymanagement$historicalspotrate_currency on countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		join 
			countrymanagement$currency on countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		group by 1, 2, 3) as cur on ed = date_trunc('day', batch.createddate) and cur.currencycode = Currency
      left outer join 
		(select date_trunc('day', effectivedate) as ed2, currencycode, value as spotrate
		from
			countrymanagement$spotrate
		join 
			countrymanagement$historicalspotrate_currency on countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
		join 
			countrymanagement$currency on countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
		group by 1, 2, 3) as eur on ed2 = date_trunc('day', batch.createddate) and eur.currencycode = 'EUR'
      left join 
	orderprocessing$order o on o_th.orderprocessing$orderid = o.id
where lostquantity> 0


     