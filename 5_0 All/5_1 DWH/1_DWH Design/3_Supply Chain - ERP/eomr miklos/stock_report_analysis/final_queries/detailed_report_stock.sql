﻿create index if not exists currentstock_idx_document_number on currentStock(documnent_number);

drop table if exists tmp_detailed_stock;

select
  case when currentStock.warehouse = 'GIR' then 'Girona' else currentStock.warehouse end warehouse,
  datetime registered, null ::date arrived,
  productid, currentStock.sku, coalesce(name, productid) product, substring(currentStock.sku from 22 for 2) packsize,
  supplier, document_number document_number, transaction, null ::text pos,
  currentstock.unitcost erp_unitcost, 
  invoiceunitcost,
  currency, day_rate, eurate,
  AllTransactions.units received_units, currentStock.units remaining_units, snapunits snapunits,
  case when currentStock.excep_batch = 1 then currentStock.units else 0 end excep_units,

  invoice_ref, invoice_posted_date

into tmp_detailed_stock
from
	currentStock
  left outer join 
	alltransactions using (id)
  left outer join 
	product$productfamily on magentoproductid = productid
where
  currentStock.warehouse in ('Amsterdam', 'York', 'GIR') and transaction != 'adjustment'
order by currentStock.warehouse,datetime DESC;

create index tmp_detailed_stock_idx_document_number on tmp_detailed_stock(document_number);

drop table if exists tmp_detailed_stock_receipt_info;

select document_number, max(warehouseshipments$receipt.arriveddate) arrived, string_agg(distinct purchaseorders$purchaseorder.ordernumber, ',') pos 
into tmp_detailed_stock_receipt_info
from
	tmp_detailed_stock
  left outer join 
	warehouseshipments$receipt on receiptid = document_number
  left outer join 
	warehouseshipments$receiptline_receipt on warehouseshipments$receiptline_receipt.warehouseshipments$receiptid = warehouseshipments$receipt.id
  left outer join 
	warehouseshipments$receiptline on warehouseshipments$receiptline_receipt.warehouseshipments$receiptlineid = warehouseshipments$receiptline.id
  left outer join 
	warehouseshipments$receiptline_purchaseorderline on warehouseshipments$receiptline_purchaseorderline.warehouseshipments$receiptlineid = warehouseshipments$receiptline.id
  left outer join 
	purchaseorders$purchaseorderline_purchaseorderlineheader on purchaseorders$purchaseorderline_purchaseorderlineheader.purchaseorders$purchaseorderlineid = warehouseshipments$receiptline_purchaseorderline.purchaseorders$purchaseorderlineid
  left outer join 
	purchaseorders$purchaseorderlineheader_purchaseorder on purchaseorders$purchaseorderlineheader_purchaseorder.purchaseorders$purchaseorderlineheaderid = purchaseorders$purchaseorderline_purchaseorderlineheader.purchaseorders$purchaseorderlineheaderid
  left outer join 
	purchaseorders$purchaseorder on purchaseorders$purchaseorderlineheader_purchaseorder.purchaseorders$purchaseorderid = purchaseorders$purchaseorder.id
group by document_number;

update tmp_detailed_stock 
set 
	arrived = tmp_detailed_stock_receipt_info.arrived,
	pos = tmp_detailed_stock_receipt_info.pos
from tmp_detailed_stock_receipt_info
where tmp_detailed_stock.document_number = tmp_detailed_stock_receipt_info.document_number;
