﻿
DROP TABLE IF EXISTS tmp_detailed_stock_by_product;

select
  currentStock.sku as SKU,
  case when currentStock.warehouse = 'GIR' then 'Girona' ELSE currentStock.warehouse END  as Warehouse,

  coalesce(name, productid) as Product, productid as "Product ID",

  sum(currentStock.units) as "Remaining Units", sum(snapunits) as "Snap Units", sum(excep_batch) as "Number of Exception Batches"
into  tmp_detailed_stock_by_product
from
	currentStock
left outer join 
	alltransactions using (id)
left outer join 
	product$productfamily on magentoproductid = productid
where currentStock.warehouse not like '%Transit'
group by 
	currentStock.sku,
	currentStock.warehouse, coalesce(name, productid),   	
	productid
order by currentStock.warehouse, currentStock.sku;

