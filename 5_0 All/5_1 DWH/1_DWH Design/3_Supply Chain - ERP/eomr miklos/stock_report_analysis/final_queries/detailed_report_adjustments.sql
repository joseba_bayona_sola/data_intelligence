﻿
  
  select datetime, currentStock.sku, coalesce(name, productid) product, substring(currentStock.sku from 22 for 2) packsize,

    currentStock.unitcost,

    case when currentStock.warehouse = 'GIR' then 'Girona' else currentStock.warehouse end as warehouse,
    document_number,
    productid as productidx,
    substring(currentStock.sku from 2 for 4) as productid,
    
    coalesce( currency, case when currentStock.warehouse in ('Amsterdam', 'GIR') then 'EUR' else 'GBP' end ) currency,    
    day_rate, eurate, 

    currentStock.units,
    
    adjustment_reason, adjustment_comment 
    -- into tmp_detailed_report_adjustments
from
		currentstock
	left outer join 
		alltransactions using (id)
	left outer join 
		product$productfamily on magentoproductid = productid
where currentStock.warehouse in ('Amsterdam', 'York', 'GIR') and 
	transaction = 'adjustment'
order by currentStock.warehouse,datetime desc;
