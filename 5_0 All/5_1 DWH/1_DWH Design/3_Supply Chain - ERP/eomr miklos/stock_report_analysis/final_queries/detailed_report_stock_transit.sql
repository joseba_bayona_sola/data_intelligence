﻿drop table if exists tmp_detailed_stock_transit;

select 
	case currentStock.warehouse 
		when 'AmsTransit' then 'In Transit from Amsterdam' 
		when 'YorkTransit' then 'In Transit from York'
		when 'GirTransit' then 'In Transit from Girona'
		else currentStock.warehouse
	end as "Warehouse",
	datetime as "Shipped Date",
	currentStock.sku as "SKU", productid as "Product ID", coalesce(name, productid) as "Product Family", substring(currentStock.sku from 22 for 2) as "Pack size",
	supplier as "Supplier", document_number as "Document Number", transaction as "Transaction", 
	currentstock.unitcost as "ERP Unit cost",
	coalesce(currency, case when currentStock.warehouse = 'YorkTransit' then 'GBP' else 'EUR' end) as "Currency",

	currentStock.units as Units,
	case when currentStock.excep_batch = 1 then currentStock.units else 0 end as "Exception Units" 
into tmp_detailed_stock_transit
from
		currentStock currentStock
	left outer join 
		alltransactions using (id)
	left outer join 
		product$productfamily on magentoproductid = productid
where currentStock.warehouse like '%Transit'
order by currentStock.warehouse, datetime desc;
