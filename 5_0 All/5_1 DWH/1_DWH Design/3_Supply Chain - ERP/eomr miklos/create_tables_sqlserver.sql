
drop table DW_GetLenses_jbs.dbo.eomr_alltransactions
go

select id, trans, datetime, 
	warehouse, productid, sku, 
	units, unitcost, 
	currency, day_rate, eurate, 
	supplier, suppliertype, 
	document_number, order_status
into DW_GetLenses_jbs.dbo.eomr_alltransactions
from openquery(MENDIX_DB_RESTORE, '
	select id, cast(transaction as varchar(20)) as trans, datetime, 
		warehouse, productid, sku, 
		units, unitcost, 
		currency, day_rate, eurate, 
		supplier, suppliertype, 
		document_number, order_status
	from public.alltransactions')


-------------------------------------------

drop table DW_GetLenses_jbs.dbo.eomr_allocationmap
go

select issue_id, batch_id, units, warehouse
into DW_GetLenses_jbs.dbo.eomr_allocationmap
from openquery(MENDIX_DB_RESTORE, '
	select issue_id, batch_id, units, warehouse
	from public.allocationmap')

-------------------------------------------

drop table DW_GetLenses_jbs.dbo.eomr_currentstock
go

select warehouse, sku, units, snapunits, id
into DW_GetLenses_jbs.dbo.eomr_currentstock
from openquery(MENDIX_DB_RESTORE, '
	select warehouse, sku, units, snapunits, id
	from public.currentstock')
