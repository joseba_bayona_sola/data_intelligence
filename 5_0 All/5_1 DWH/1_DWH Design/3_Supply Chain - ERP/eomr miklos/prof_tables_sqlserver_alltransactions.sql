
select top 1000 *
from DW_GetLenses_jbs.dbo.eomr_alltransactions
-- where productid = '0004'
order by id

	select top 1000 count(*)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions

	select top 1000 trans, count(*), min(datetime), max(datetime), min(id), max(id)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans
	order by trans

	select top 1000 warehouse, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by warehouse
	order by warehouse

	select top 1000 trans, currency, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans, currency
	order by trans, currency

	select top 1000 trans, currency, day_rate, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans, currency, day_rate
	order by trans, currency, day_rate

	select top 1000 trans, currency, eurate, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans, currency, eurate
	order by trans, currency, eurate

	select top 1000 trans, supplier, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans, supplier
	order by trans, supplier

	select top 1000 trans, suppliertype, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by trans, suppliertype
	order by trans, suppliertype

	select top 1000 productid, count(*), min(datetime), max(datetime)
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by productid
	order by productid

	select productid, sku, count(*) num_rec, min(datetime) min_date, max(datetime) max_date
	from DW_GetLenses_jbs.dbo.eomr_alltransactions
	group by productid, sku
	order by productid, sku

	select count(*)
	from Landing.mend.gen_prod_stockitem_v

	select t.productid, t.sku, 
		si.magentoProductID_int, si.magentoSKU, si.name, si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
		t.num_rec, t.min_date, t.max_date, t.num_dist_trans
	from
			(select productid, sku, count(*) num_rec, min(datetime) min_date, max(datetime) max_date, count(distinct trans) num_dist_trans
			from DW_GetLenses_jbs.dbo.eomr_alltransactions
			group by productid, sku) t
		full join
			Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
	where si.sku is not null and t.sku is not null
	-- where si.sku is null
	-- where t.sku is null
	order by t.productid, t.sku, si.magentoProductID_int, si.magentoSKU

		select -- *
			magentoProductID_int, magentoSKU, name, SKU_product, description, packsize, 
				bc, di, po, cy, ax, ad, _do, co
		from Landing.mend.gen_prod_stockitem_v
		where sku = '01083B7D4DQ0000000000301'

------------------------------------------------------------

select top 1000 t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.sku, 
	-- si.magentoProductID_int, si.magentoSKU, si.name, 
	si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
	t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
	t.document_number, t.order_status
from 
		DW_GetLenses_jbs.dbo.eomr_alltransactions t
	inner join
		Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
-- where t.id = 373435
where trans = 'receipt' -- stock - issue - receipt - adjustment

order by warehouse, datetime desc


------------------------------------------------------------

select t.id, t.trans, t.datetime, t.warehouse, 
	t.productid, t.sku, 
	-- si.magentoProductID_int, si.magentoSKU, si.name, 
	si.description, si.packsize, si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co,
	t.units, t.unitcost, t.currency, t.day_rate, t.eurate, t.supplier, t.suppliertype, 
	t.document_number, t.order_status
from 
		DW_GetLenses_jbs.dbo.eomr_alltransactions t
	inner join
		Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
where t.productid = '1083' and t.sku = '01083B2D4BN0000000000301'
	and trans = 'issue' -- stock - issue - receipt - adjustment
order by warehouse, datetime desc

	select top 1000 trans, count(*), min(datetime), max(datetime), min(id), max(id), sum(units)
	from 
			DW_GetLenses_jbs.dbo.eomr_alltransactions t
		inner join
			Landing.mend.gen_prod_stockitem_v si on t.sku = si.sku
	where t.productid = '1083' and t.sku = '01083B2D4BN0000000000301'
	group by trans
	order by trans


