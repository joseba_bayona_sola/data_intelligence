
---------------------- NOTES ----------------------

	-- transferheader
		-- Diff between PO Supply and PO Transfer
		-- Why PO Supply can be NULL
		-- Why PO Transfer can be NULL
		-- meaning of Allocatedstrategy and totalremaining

	-- transferheader

	select transferheaderid, purchaseorderid_s, purchaseorderid_t, 
		transfernum, allocatedstrategy, 
		totalremaining,
		createddate, 
		code_t, warehouse_name_t, supplierID_t, supplierName_t, ordernumber_t,
		code_s, warehouse_name_s, supplierID_s, supplierName_s, ordernumber_s 
	from Landing.mend.gen_inter_transferheader_v
	-- where purchaseorderid_s is null 
	-- where purchaseorderid_t is null 
	-- where transferheaderid in (64457769666764552, 64457769666751632)
	order by createddate desc

	select count(*)
	from Landing.mend.inter_transferheader

	select count(*)
	from Landing.mend.inter_transferheader_aud

	select count(*)
	from Landing.mend.gen_inter_transferheader_v

		select *, 
			count(*) over (partition by transferheaderid) rep_father,
			count(*) over (partition by purchaseorderid) rep_son
		from Landing.mend.inter_supplypo_transferheader_aud
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by transferheaderid) rep_father,
			count(*) over (partition by purchaseorderid) rep_son
		from Landing.mend.inter_transferpo_transferheader_aud
		order by rep_father desc, rep_son desc

	select transfernum, count(*), count(distinct transferheaderid)
	from Landing.mend.gen_inter_transferheader_v
	group by transfernum
	order by count(distinct transferheaderid) desc

	select allocatedStrategy, count(*)
	from Landing.mend.gen_inter_transferheader_v
	group by allocatedStrategy
	order by allocatedStrategy

---------------------- NOTES ----------------------
	-- Why some Transfer Header without Transit Batch Header
	-- Transfer Header MANY Transit Batch Header
	-- Why this table - Relation to Customer shipment?? 

	-- intransitbatchheader

	select intransitbatchheaderid, transferheaderid, purchaseorderid_s, purchaseorderid_t, customershipmentid,
		transfernum, allocatedstrategy, totalremaining, createddate, ordernumber_t, ordernumber_s, 
		createddate_ibh
	from Landing.mend.gen_inter_intransitbatchheader_v
	-- where intransitbatchheaderid  is null
	-- where intransitbatchheaderid  is not null
	-- where transferheaderid in (64457769666764552, 64457769666751632)
	order by createddate desc

	select count(*)
	from Landing.mend.inter_intransitbatchheader

	select count(*)
	from Landing.mend.inter_intransitbatchheader_aud

	select count(*)
	from Landing.mend.gen_inter_intransitbatchheader_v

		select *, 
			count(*) over (partition by intransitbatchheaderid) rep_father,
			count(*) over (partition by transferheaderid) rep_son
		from Landing.mend.inter_intransitbatchheader_transferheader_aud
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by intransitbatchheaderid) rep_father,
			count(*) over (partition by customershipmentid) rep_son
		from Landing.mend.inter_intransitbatchheader_customershipment_aud
		order by rep_father desc, rep_son desc


---------------------- NOTES ----------------------
	-- intransitstockitembatch
		-- Why relation from Transfer Header and Transfer Batch Header
		-- meaning of issuedquantity - remainingquantity - issued
		-- meaning of productunitcost - carriageUnitCost - totalUnitCost - totalUnitCostInterCo
		-- Relation to WH Shipment

	-- intransitstockitembatch
	select top 1000 *
	from Landing.mend.gen_inter_intransitstockitembatch_th_v
	-- from Landing.mend.gen_inter_intransitstockitembatch_ibh_v
	-- where intransitbatchheaderid is null
	-- where intransitstockitembatchid is null
	-- where transferheaderid in (64457769666764552, 64457769666751632)
	order by createddate desc

	select count(*)
	from Landing.mend.inter_intransitstockitembatch

	select count(*)
	from Landing.mend.inter_intransitstockitembatch_aud

	select count(*)
	from Landing.mend.gen_inter_intransitstockitembatch_th_v

	select count(*)
	from Landing.mend.gen_inter_intransitstockitembatch_ibh_v

		select *, 
			count(*) over (partition by intransitstockitembatchid) rep_father,
			count(*) over (partition by transferheaderid) rep_son
		from Landing.mend.inter_intransitstockitembatch_transferheader_aud
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by intransitstockitembatchid) rep_father,
			count(*) over (partition by intransitbatchheaderid) rep_son
		from Landing.mend.inter_intransitstockitembatch_intransitbatchheader_aud
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by intransitstockitembatchid) rep_father,
			count(*) over (partition by stockitemid) rep_son
		from Landing.mend.inter_intransitstockitembatch_stockitem_aud
		order by rep_father desc, rep_son desc

		select *, 
			count(*) over (partition by intransitstockitembatchid) rep_father,
			count(*) over (partition by shipmentid) rep_son
		from Landing.mend.inter_intransitstockitembatch_shipment_aud
		order by rep_father desc, rep_son desc