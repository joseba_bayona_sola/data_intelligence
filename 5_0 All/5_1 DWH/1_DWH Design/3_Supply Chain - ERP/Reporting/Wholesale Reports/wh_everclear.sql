select oli.product_family_name, oli.packsize, SUM(oli.local_total_unit_cost * oli.qty_stock) / SUM(oli.qty_stock) weighted_avg
from Warehouse.alloc.fact_order_line_erp_issue_v oli
	right join Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
where 
	wholesale_customer_name = 'Lensway Group AB' 
	and invoice_date_c between '2019-07-01' and '2019-07-31' 
	and oli.product_family_name in ('everclear ELITE','everclear ELITE (5 pack)')
group by oli.product_family_name, oli.packsize;


select whoil.invoice_no, whoil.order_no_erp, whoil.wholesale_customer_name, 
	 oli.batchstockissueid_bk, oli.orderid_bk, oli.warehouse_name, oli.product_family_name, oli.packsize, whoil.product_description, oli.sku, oli.qty_stock, oli.local_total_unit_cost, oli.order_currency_code,
	 oli.wh_shipment_type_name, oli.receipt_number, oli.purchase_order_number
from Warehouse.alloc.fact_order_line_erp_issue_v oli
	right join Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
where 
	wholesale_customer_name = 'Lensway Group AB' 
	and invoice_date_c between '2019-07-01' and '2019-07-31' 
	and oli.product_family_name in ('everclear ELITE','everclear ELITE (5 pack)')
order by whoil.invoice_no, whoil.order_no_erp, oli.product_family_name, oli.wh_shipment_type_name, oli.receipt_number, oli.purchase_order_number, oli.sku


----------------------------------------------------

select top 1000 *
from warehouse.alloc.dim_wholesale_order_invoice_v
where invoice_no in ('GIR00159-030719')
order by idWholesaleOrderInvoice_sk desc

select top 1000 *
from warehouse.alloc.dim_wholesale_order_shipment_v
where invoice_no in ('GIR00159-030719')
-- where order_type_erp_f = 'W'
order by order_no_erp desc

select top 1000 *
from warehouse.alloc.dim_wholesale_order_invoice_line_v
where invoice_no in ('GIR00159-030719') 
	-- and orderid_bk = 48695171029600169
-- where orderid_bk = 48695171036044179
order by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize

select top 1000 invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent, 
	sum(qty_pack) quantity, count(*) lines, sum(local_total_exc_vat) net_price, sum(local_total_inc_vat) grossamount, sum(local_total_vat) vatamount
from warehouse.alloc.dim_wholesale_order_invoice_line_v
where invoice_no in ('GIR00159-030719')
group by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent
order by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent

-- 

select batchstockissueid_bk,
	idOrderHeaderERP_sk, order_no_erp, product_id_magento, product_family_name, packsize, product_description, sku, order_currency_code, warehouse_name, receipt_number,
	batch_id, qty_stock, local_total_unit_cost, global_total_unit_cost
from Warehouse.alloc.fact_order_line_erp_issue_v
where orderid_bk = 48695171039099926

select top 1000 idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code, 
	sum(qty_stock) sum_qty_stock, sum(local_total_unit_cost * qty_stock) sum_local_total_cost, 
	sum(local_total_unit_cost * qty_stock) / sum(qty_stock) weighted_avg_cost
from Warehouse.alloc.fact_order_line_erp_issue_v
-- where idOrderHeaderERP_sk = 4823406
where orderid_bk = 48695171039099926
group by idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code
order by idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code

-- 

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue
where batchstockissueid_bk in (89509042604367394, 89509042604367406)

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where batchstockissueid_bk in (89509042604367394, 89509042604367406)

select top 1000 *
from Warehouse.po.fact_purchase_order_line_v
where purchase_order_number = 'P00045672'

select top 1000 *
from Warehouse.rec.fact_receipt_line_v
where receipt_number = 'TP00045672-1'