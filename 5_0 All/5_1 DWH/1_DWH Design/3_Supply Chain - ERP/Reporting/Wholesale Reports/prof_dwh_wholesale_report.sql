
select top 1000 *
from warehouse.alloc.dim_wholesale_order_header_erp_v
where order_type_erp_f = 'W'
order by idOrderHeaderERP_sk_fk desc

	select top 1000 order_type_erp_f, count(*)
	from warehouse.alloc.dim_wholesale_order_header_erp_v
	group by order_type_erp_f
	order by order_type_erp_f

select top 1000 *
from warehouse.alloc.dim_wholesale_order_invoice_v
where invoice_no in ('VDA00921-030419')
order by idWholesaleOrderInvoice_sk desc

	select top 1000 count(*)
	from warehouse.alloc.dim_wholesale_order_invoice_v

select top 1000 *
from warehouse.alloc.dim_wholesale_order_shipment_v
where invoice_no in ('GIR00137-240419')
-- where order_type_erp_f = 'W'
order by order_no_erp desc

	select top 1000 count(*) over (partition by order_no_erp), count(*) over (partition by order_no_erp, invoice_no),
		customershipmentinvoiceid_bk, orderid_bk, invoice_no, order_no_erp, order_type_erp_f, wholesale_customer_name, warehouse_name, shipment_no, wholesale_order_invoice_status_name
	from warehouse.alloc.dim_wholesale_order_shipment_v
	where order_type_erp_f = 'W'
	order by order_no_erp desc

	select top 1000 count(*)
	from warehouse.alloc.dim_wholesale_order_shipment_v

	select invoice_no, count(*), count(distinct wholesale_customer_name)
	from warehouse.alloc.dim_wholesale_order_shipment_v
	group by invoice_no
	order by count(distinct wholesale_customer_name) desc, invoice_no

select top 1000 *
from warehouse.alloc.dim_wholesale_order_invoice_line_v
where invoice_no in ('VDA00884-050219', 'VDE00337-160119') 
	-- and orderid_bk = 48695171029600169
-- where orderid_bk = 48695171036044179
order by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize

	select top 1000 count(*)
	from warehouse.alloc.dim_wholesale_order_invoice_line_v

	select top 1000 order_type_erp_f, count(*)
	from warehouse.alloc.dim_wholesale_order_invoice_line_v
	group by order_type_erp_f
	order by order_type_erp_f

select top 1000 invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent, 
	sum(qty_pack) quantity, count(*) lines, sum(local_total_exc_vat) net_price, sum(local_total_inc_vat) grossamount, sum(local_total_vat) vatamount
from warehouse.alloc.dim_wholesale_order_invoice_line_v
where invoice_no in ('VDA00884-050219', 'VDE00337-160119')
group by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent
order by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent

-----------------------------------------------

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where order_type_erp_f = 'W'
order by orderid_bk desc

select top 1000 *
from Warehouse.tableau.fact_order_line_erp_issue_v
where order_type_erp_f = 'I'
order by orderid_bk desc

select idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code, 
	batch_id, qty_stock, local_total_unit_cost, global_total_unit_cost
from Warehouse.alloc.fact_order_line_erp_issue_v
where orderid_bk = 48695171032963126

select top 1000 idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code, 
	sum(qty_stock) sum_qty_stock, sum(local_total_unit_cost * qty_stock) sum_local_total_cost, 
	sum(local_total_unit_cost * qty_stock) / sum(qty_stock) weighted_avg_cost
from Warehouse.alloc.fact_order_line_erp_issue_v
-- where idOrderHeaderERP_sk = 4823406
where orderid_bk = 48695171032963126
group by idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code
order by idOrderHeaderERP_sk, product_id_magento, product_family_name, packsize, order_currency_code

select top 1000 count(*) over (), count(*) over (partition by idWholesaleOrderInvoiceLine_sk) num_rep,
	-- oli.sum_qty_stock, 
	oli.weighted_avg_cost, oli.order_currency_code, 
	whoil.*
from 
		Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
	left join
		(select idOrderHeaderERP_sk, orderid_bk, product_id_magento, product_family_name, packsize, order_currency_code, 
			sum(qty_stock) sum_qty_stock, sum(local_total_unit_cost * qty_stock) sum_local_total_cost
			, sum(local_total_unit_cost * qty_stock) / sum(qty_stock) weighted_avg_cost
		from Warehouse.alloc.fact_order_line_erp_issue_v
		-- where orderid_bk = 48695171035671441
		group by idOrderHeaderERP_sk, orderid_bk, product_id_magento, product_family_name, packsize, order_currency_code) oli 
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
-- where oli.weighted_avg_cost is null
-- where oli.order_currency_code <> whoil.order_currency_code -- and invoice_no = 'VDE00337-160119'
-- where whoil.orderid_bk = 48695171037248895
-- where oli.sum_qty_stock = 0
order by num_rep desc, order_no_erp desc

-----------------------------------------------

drop table #wholesale_report

select -- top 1000 count(*) over (), 
	
	whoil.customerinvoicelineid_bk, whoil.customershipmentinvoiceid_bk, whoil.customerinvoiceid_bk, whoil.orderid_bk,
	whoil.invoice_no, 
	whoil.order_no_erp, whoil.order_type_erp_f, whoil.order_type_erp_name, whoil.order_status_erp_name, whoil.shipment_status_erp_name, whoil.allocation_status_name, 
	whoil.country_code_ship, whoil.wholesale_customer_name, whoil.customer_ref, whoil.due_date,
	whoil.warehouse_name,
	whoil.shipment_no, 
	whoil.invoice_date, 
	whoil.wholesale_order_invoice_status_name,	

	whoil.manufacturer_name, whoil.product_type_name, whoil.category_name, whoil.product_type_oh_name, whoil.product_family_group_name,
	whoil.product_id_magento, whoil.product_family_name, whoil.packsize, 
	whoil.parameter, whoil.product_description,
	whoil.SKU, whoil.stock_item_description,
	whoil.qty_pack, whoil.price_pack,

	-- oli.weighted_avg_cost local_weighted_avg_cost, ce.exchange_rate,
	isnull(oli.weighted_avg_cost, 0) * ce.exchange_rate local_weighted_avg_cost, 
	isnull(oli.weighted_avg_cost, 0) * ce2.exchange_rate global_weighted_avg_cost, 

	whoil.local_subtotal, whoil.local_total_inc_vat, whoil.local_total_exc_vat, whoil.local_total_vat, whoil.local_total_prof_fee, 
	whoil.global_subtotal, whoil.global_total_inc_vat, whoil.global_total_exc_vat, whoil.global_total_vat, whoil.global_total_prof_fee, 

	whoil.local_to_global_rate, whoil.order_currency_code, 
	whoil.vat_percent
	
	-- , oli.order_currency_code curreny_code_allocation
into #wholesale_report		
from 
		Warehouse.alloc.dim_wholesale_order_invoice_line_v whoil
	left join
		(select idOrderHeaderERP_sk, orderid_bk, product_id_magento, product_family_name, packsize, order_currency_code, 
			sum(qty_stock) sum_qty_stock, sum(local_total_unit_cost * qty_stock) sum_local_total_cost, sum(local_total_unit_cost * qty_stock) / sum(qty_stock) weighted_avg_cost
		from Warehouse.alloc.fact_order_line_erp_issue_v
		group by idOrderHeaderERP_sk, orderid_bk, product_id_magento, product_family_name, packsize, order_currency_code) oli 
			on whoil.orderid_bk = oli.orderid_bk and whoil.product_id_magento = oli.product_id_magento and whoil.packsize = oli.packsize
	left join
		Landing.mend.gen_comp_currency_exchange_v ce 
			on oli.order_currency_code = ce.currencycode_from and whoil.order_currency_code = ce.currencycode_to and whoil.invoice_date > ce.effectivedate and whoil.invoice_date <= ce.nexteffectivedate
	left join
		Landing.mend.gen_comp_currency_exchange_v ce2 
			on oli.order_currency_code = ce2.currencycode_from and 'GBP' = ce2.currencycode_to and whoil.invoice_date > ce2.effectivedate and whoil.invoice_date <= ce2.nexteffectivedate
-- where whoil.orderid_bk = 48695171037248895
-- where whoil.invoice_no = 'VDE00337-160119'
union
select 
	null customerinvoicelineid_bk, null customershipmentinvoiceid_bk, woi.customerinvoiceid_bk, null orderid_bk,
	woi.invoice_no, 
	null order_no_erp, null order_type_erp_f, null order_type_erp_name, null order_status_erp_name, null shipment_status_erp_name, null allocation_status_name, 
	null country_code_ship, wos.wholesale_customer_name, null customer_ref, null due_date,
	wos.warehouse_name,
	null shipment_no, 
	woi.invoice_date, 
	woi.wholesale_order_invoice_status_name,	

	null manufacturer_name, null product_type_name, null category_name, null product_type_oh_name, null product_family_group_name,
	null product_id_magento, 'Carriage' product_family_name, null packsize, 
	null parameter, null product_description,
	null SKU, null stock_item_description,
	null qty_pack, null price_pack,

	null local_weighted_avg_cost, null global_weighted_avg_cost, 
	 
	woi.local_shipping local_subtotal, woi.local_shipping local_total_inc_vat, woi.local_shipping local_total_exc_vat, 0 local_total_vat, 0 local_total_prof_fee,
	woi.global_shipping global_subtotal, woi.global_shipping global_total_inc_vat, woi.local_shipping global_total_exc_vat, 0 global_total_vat, 0 global_total_prof_fee,

	woi.local_to_global_rate, woi.order_currency_code, 
	0 vat_percent
from 
		Warehouse.alloc.dim_wholesale_order_invoice_v woi
	inner join
		(select distinct customerinvoiceid_bk, wholesale_customer_name, warehouse_name
		from warehouse.alloc.dim_wholesale_order_shipment_v) wos on woi.customerinvoiceid_bk = wos.customerinvoiceid_bk

select top 1000 *
from #wholesale_report
where invoice_date between '2019-05-01' and '2019-06-01'

select top 1000 count(*) over (partition by invoice_no, product_family_name, packsize) num_rep,
	invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent, 
	local_weighted_avg_cost,
	-- avg(local_weighted_avg_cost) local_weighted_avg_cost,
	sum(qty_pack) quantity, count(*) lines, sum(local_total_exc_vat) net_price, sum(local_total_inc_vat) grossamount, sum(local_total_vat) vatamount
from #wholesale_report
where invoice_date between '2019-05-01' and '2019-06-01'
	-- and product_family_name = 'Carriage' -- and local_total_exc_vat <> 0
	and invoice_no = 'VDA00946-020519'
group by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent
	, local_weighted_avg_cost
order by num_rep desc, invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent


select top 1000 
	invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent, 
	avg(local_weighted_avg_cost) local_weighted_avg_cost,
	sum(qty_pack) quantity, count(*) lines, sum(local_total_exc_vat) net_price, sum(local_total_inc_vat) grossamount, sum(local_total_vat) vatamount
from #wholesale_report
where invoice_no = 'VDE00337-160119'
group by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent
order by invoice_no, invoice_date, wholesale_order_invoice_status_name, wholesale_customer_name, warehouse_name, product_family_name, packsize, order_currency_code, vat_percent

------------------------------------------------------
------------------------------------------------------

select top 1000 *
from Warehouse.alloc.fact_order_line_erp_issue_v
where orderid_bk = 48695171032963126

select top 1000 orderid_bk, order_no,  product_id_magento, product_family_name, packsize, sku, 
	receipt_number, purchase_order_number, supplier_name, batch_id, 
	qty_stock, local_total_unit_cost, global_total_unit_cost, 
	supplier_currency_code, warehouse_currency_code
from Warehouse.tableau.fact_order_line_erp_issue_v
where orderid_bk = 48695171032963126
	and sku = '02317B3D4DA0000000000301'

select top 1000 *
from Warehouse.rec.fact_receipt_line_v
where receipt_number = 'TP00045766-1'
	and sku = '02317B3D4DA0000000000301'

