-------------------------------------begin tests-------------------------------------------------------------------------------------------------------------------------------------------------------------
SELECT DISTINCT orderid_bk, product_family_name, local_total_unit_cost, global_total_unit_cost, order_currency_code, AVG(local_total_unit_cost) OVER(), AVG(global_total_unit_cost) OVER()
FROM  [alloc].[fact_order_line_erp_issue_v]
WHERE 
orderid_bk = '48695171021007049'
and 
product_family_name = 'Air Optix Aqua'
--and 
--order_currency_code != 'GBP' 
--and order_type_erp_f = 'w'
GROUP BY orderid_bk, product_family_name, local_total_unit_cost, global_total_unit_cost, order_currency_code


SELECT * FROM [Warehouse].[alloc].[dim_wholesale_order_invoice_line_v]
WHERE 
orderid_bk = '48695171021007049'
and 
product_family_name = 'Air Optix Aqua'
--and order_currency_code != 'GBP'


SELECT *
FROM [alloc].[fact_order_line_erp_issue_v]
WHERE orderid_bk = '48695171020269074'

----------------------------------------------end tests -------------------------------------------------------------------------------
-----------------------------------------------main dataset----------------------------------------------------------------------------

  SELECT DISTINCT
	n.orderid_bk,
	v.invoice_no,
	v.invoice_date,
	v.wholesale_customer_name								AS Customer_Name,
	v.warehouse_name,
	v.product_family_name									AS Product_Family,
	v.Packsize,
	v.Shipment_no,
	v.qty_pack												AS qty,
	v.price_pack											AS Local_Unit_Price,
	v.price_pack	* v.local_to_global_rate				AS Global_Unit_Price,
	v.local_to_global_rate,
	v.Order_currency_Code,
	v.vat_percent											AS VAT_Rate,
	v.Local_Total_VAT,
	AVG(CASE WHEN wholesale_customer_name NOT IN ('Amsterdam (from York)', 'Amsterdam (from Girona)','Girona (from York)', 'Girona (from Amsterdam)', 'York (from Amsterdam)','York (from Girona)') 
			THEN local_total_unit_cost	ELSE 0 END)	OVER(Partition by n.orderid_bk,v.product_family_name) AS AVG_local_unit_cost,
	AVG(CASE WHEN wholesale_customer_name NOT IN ('Amsterdam (from York)', 'Amsterdam (from Girona)','Girona (from York)', 'Girona (from Amsterdam)', 'York (from Amsterdam)','York (from Girona)') 
			THEN global_total_unit_cost	ELSE 0 END)	OVER(Partition by n.orderid_bk,v.product_family_name) AS AVG_global_total_unit_cost,
	n.order_currency_code									AS Purchase_currency_Code, 
	v.local_total_exc_vat									AS Gross_Amount,
	v.local_total_inc_vat									AS Net_Price
	FROM [Warehouse].[alloc].[dim_wholesale_order_invoice_line_v] V
	LEFT JOIN [Warehouse].[alloc].[fact_order_line_erp_issue_v] N 
		ON N.orderid_bk = V.orderid_bk
		AND V.product_family_name = N.product_family_name
	WHERE 
	n.orderid_bk = 48695171037248895
	--AND 
	--v.wholesale_customer_name = 'Amsterdam (from York)'

select count(*)
from
  (SELECT DISTINCT
	n.orderid_bk,
	v.invoice_no,
	v.invoice_date,
	v.wholesale_customer_name								AS Customer_Name,
	v.warehouse_name,
	v.product_family_name									AS Product_Family,
	v.Packsize,
	v.Shipment_no,
	v.qty_pack												AS qty,
	v.price_pack											AS Local_Unit_Price,
	v.price_pack	* v.local_to_global_rate				AS Global_Unit_Price,
	v.local_to_global_rate,
	v.Order_currency_Code,
	v.vat_percent											AS VAT_Rate,
	v.Local_Total_VAT,
	AVG(CASE WHEN wholesale_customer_name NOT IN ('Amsterdam (from York)', 'Amsterdam (from Girona)','Girona (from York)', 'Girona (from Amsterdam)', 'York (from Amsterdam)','York (from Girona)') 
			THEN local_total_unit_cost	ELSE 0 END)	OVER(Partition by n.orderid_bk,v.product_family_name) AS AVG_local_unit_cost,
	AVG(CASE WHEN wholesale_customer_name NOT IN ('Amsterdam (from York)', 'Amsterdam (from Girona)','Girona (from York)', 'Girona (from Amsterdam)', 'York (from Amsterdam)','York (from Girona)') 
			THEN global_total_unit_cost	ELSE 0 END)	OVER(Partition by n.orderid_bk,v.product_family_name) AS AVG_global_total_unit_cost,
	n.order_currency_code									AS Purchase_currency_Code, 
	v.local_total_exc_vat									AS Gross_Amount,
	v.local_total_inc_vat									AS Net_Price
	FROM [Warehouse].[alloc].[dim_wholesale_order_invoice_line_v] V
	LEFT JOIN [Warehouse].[alloc].[fact_order_line_erp_issue_v] N 
		ON N.orderid_bk = V.orderid_bk
		AND V.product_family_name = N.product_family_name) t