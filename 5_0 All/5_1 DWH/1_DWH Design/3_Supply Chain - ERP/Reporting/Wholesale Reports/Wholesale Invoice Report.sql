﻿--
---------------------------------------------------
-- Set reporting dates
---------------------------------------------------
drop table if exists tmp_rundate;
create temp table tmp_rundate
as
select 
(TIMESTAMP '2016-12-01 00:00:00.000') as startdate,
(TIMESTAMP '2017-01-01 00:00:00.000') as enddate;
select * from tmp_rundate;
--
-----------------------------------------------------
-- Create temp table for stock allocations
-- by invoice, product family, packsize and unit cost
-----------------------------------------------------
drop table if exists tmp_invoice_product_price;
create temp table tmp_invoice_product_price
as
--
SELECT
	ci.invoicenumber,
	prf.name,
	olsat.packsize,
	whsib.productunitcost,
	sum(bsa.issuedquantity) qtyissued,
	sum(bsa.issuedquantity * whsib.productunitcost) totalcost
from
	customershipments$customershipment csh
-----------------------------------
-- JOIN to customer shipmentinvoice
-----------------------------------
left outer join
	customerinvoicing$customershipmentinvoice csi
on
	csh.shipmentnumber = csi.shipmentnumber 
---------------------------
-- JOIN to customer invoice
---------------------------
left outer join
	customerinvoicing$customershipmentinvoice_customerinvoice csi_ci
on
	csi.id = csi_ci.customerinvoicing$customershipmentinvoiceid
left outer join
	customerinvoicing$customerinvoice ci
on
	csi_ci.customerinvoicing$customerinvoiceid = ci.id
------------------------------------------
-- JOIN to PickLine
------------------------------------------
left outer join
	customershipments$picklist_customershipment pl_csh
on
	csh.id = pl_csh.customershipments$customershipmentid
left outer join
	customershipments$pickline pl
on
	pl_csh.customershipments$picklineid = pl.id
------------------------------------------
-- JOIN to stock allocation transaction
------------------------------------------
left outer join
	customershipments$PickLine_OrderLineStockAllocationTransaction pl_olsat
on
	pl.id = pl_olsat.customershipments$picklineid
left outer join
	stockallocation$OrderLineStockAllocationTransaction olsat
on
	pl_olsat.stockallocation$OrderLineStockAllocationTransactionid = olsat.id
---------------------------------------------
-- JOIN to batch stock allocation transaction
---------------------------------------------
left outer join
	inventory$BatchTransaction_OrderLineStockAllocationTransaction bt_olsat
on
	olsat.id = bt_olsat.stockallocation$OrderLineStockAllocationTransactionid
left outer join
	inventory$BatchStockAllocation bsa
on
	bt_olsat.inventory$BatchStockAllocationid = bsa.id
---------------------------------------------
-- JOIN to warehouse stock item batch 
---------------------------------------------
left outer join
	inventory$BatchTransaction_WarehouseStockItemBatch bt_whsib
on
	bsa.id = bt_whsib.inventory$BatchStockAllocationid
left outer join
	inventory$WarehouseStockItemBatch whsib
on
	bt_whsib.inventory$WarehouseStockItemBatchid = whsib.id
---------------------
-- JOIN to Stock Item
---------------------
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
left outer join
	inventory$warehousestockitem_stockitem whsi_si
on
	whsi.id = whsi_si.inventory$warehousestockitemid
left outer join
	product$stockitem si
on
	whsi_si.product$stockitemid = si.id
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
-------------------------
-- End of JOINs
-------------------------
where
	csh.shipmentnumber like 'WS%'
and
	bsa.cancelled = false
group by
	ci.invoicenumber,
	prf.name,
	olsat.packsize,
	whsib.productunitcost
having not
	sum(bsa.issuedquantity) = 0
order by
	ci.invoicenumber,
	prf.name,
	olsat.packsize,
	whsib.productunitcost;
--
-----------------------------------------------------
-- Create temp table for weighted average cost
-- by invoice, product family and packsize
-----------------------------------------------------
drop table if exists tmp_invoice_product;
create temp table tmp_invoice_product
as
--
select 
	invoicenumber,
	name,
	packsize,
	sum(totalcost) / sum(qtyissued) avecost
from
	tmp_invoice_product_price
group by
	invoicenumber,
	name,
	packsize
order by
	invoicenumber,
	name,
	packsize;
--
--****************************************************************************************************************************************************************************************
--                                                                     Run wholesale invoice product lines report
--****************************************************************************************************************************************************************************************
select
 *
from
(
SELECT 
	ci.invoicenumber,
	ci.invoicedate,
	ci.status,
	wsc.name as customer,
	csi.warehouse,
	prf.name as product,
	si.packsize,
	sum(cil.packquantity)as Quantity,
	count(cil.id) as Lines,
	tmp_pr.avecost as AveCost,
	ci.currency as Currency,
	sum(netprice) as NetPrice,
	max(vatrate) as VATRate,
	sum(vatamount) as VATAmount,
	sum(totalprice) as GrossAmount
FROM
	customerinvoicing$customerinvoiceline cil
------------------------------------
-- JOIN to customer shipment invoice
------------------------------------
left outer join
	customerinvoicing$customerinvoiceline_customershipmentinvoice cil_csi
on
	cil.id = cil_csi.customerinvoicing$customerinvoicelineid
left outer join
	customerinvoicing$customershipmentinvoice csi
on
	cil_csi.customerinvoicing$customershipmentinvoiceid = csi.id
---------------------------
-- JOIN to customer invoice
---------------------------
left outer join
	customerinvoicing$customershipmentinvoice_customerinvoice csi_ci
on
	csi.id = csi_ci.customerinvoicing$customershipmentinvoiceid
left outer join
	customerinvoicing$customerinvoice ci
on
	csi_ci.customerinvoicing$customerinvoiceid = ci.id
---------------------------
-- JOIN to customer
---------------------------
left outer join
	customerinvoicing$customershipmentinvoice_customerentity csi_ce
on
	csi.id = csi_ce.customerinvoicing$customershipmentinvoiceid
left outer join
	customer$customerentity ce
on
	csi_ce.customer$customerentityid = ce.id
left outer join
	customer$wholesalecustomer wsc
on
	csi_ce.customer$customerentityid = wsc.id
----------------------------
-- JOIN to customer shipment
----------------------------
left outer join
	customershipments$customershipment csh
on
	csi.shipmentnumber = csh.shipmentnumber
---------------------------
-- JOIN to stockitem family
---------------------------
left outer join
	product$stockitem si
on
	cil.sku = si.sku
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
------------------------------------------------------
-- JOIN to temp tabel for weighted average cost 
------------------------------------------------------
left outer join
	tmp_invoice_product tmp_pr
on
	ci.invoicenumber = tmp_pr.invoicenumber
and
	prf.name = tmp_pr.name
and
	si.packsize = tmp_pr.packsize
-------------------------
-- End of JOINs
-------------------------
where
	ci.invoicenumber is not null
and
	ci.invoicedate >= (select max(startdate) from tmp_rundate)
and
	ci.invoicedate < (select max(enddate) from tmp_rundate)
group by
	ci.invoicenumber,
	ci.invoicedate,
	ci.status,
	ci.currency,
	wsc.name,
	csi.warehouse,
	prf.name,
	si.packsize,
	tmp_pr.avecost
-- order by
-- 	ci.invoicenumber,
-- 	prf.name,
-- 	si.packsize
UNION ALL
SELECT 
	ci.invoicenumber,
	ci.invoicedate,
	ci.status,
	wsc.name,
	wh.warehouse as warehouse,
	cil.description as name,
	NULL as packsize,
	NULL as Quantity,
	count(cil.id) as Lines,
	NULL as AveCost,
	ci.currency as Currency,
	sum(cil.netprice) as NetPrice,
	max(cil.vatrate) as VATRate,
	sum(cil.vatamount) as VATAmount,
	sum(cil.totalprice) as GrossAmount
FROM
	customerinvoicing$customerinvoice ci
-----------------------------------------------
-- JOIN to customer invoice line (for carriage)
-----------------------------------------------
left outer join
	customerinvoicing$customerinvoice_customerinvoicecarriage ci_cic
on
	ci.id = ci_cic.customerinvoicing$customerinvoiceid
left outer join
	customerinvoicing$customerinvoiceline cil
on
	ci_cic.customerinvoicing$customerinvoicelineid = cil.id
---------------------------
-- JOIN to customer
---------------------------
left outer join
	customerinvoicing$customerinvoice_customerentity ci_ce
on
	ci.id = ci_ce.customerinvoicing$customerinvoiceid
left outer join
	customer$customerentity ce
on
	ci_ce.customer$customerentityid = ce.id
left outer join
	customer$wholesalecustomer wsc
on
	ci_ce.customer$customerentityid = wsc.id
------------------------------------
-- JOIN to customer shipment invoice
------------------------------------
left outer join
(
select
	ci.invoicenumber,
	max(warehouse) as warehouse
from 
	customerinvoicing$customerinvoice ci
left outer join
	customerinvoicing$CustomerShipmentInvoice_CustomerInvoice csi_ci
on
	ci.id = csi_ci.customerinvoicing$customerinvoiceid
left outer join
	customerinvoicing$customershipmentinvoice csi
on
	csi_ci.customerinvoicing$customershipmentinvoiceid = csi.id
group by
	ci.invoicenumber
order by
	ci.invoicenumber
) wh
on
	ci.invoicenumber = wh.invoicenumber
-------------------------
-- End of JOINs
-------------------------
where
	ci.invoicenumber is not null
and
	ci.invoicedate >= (select max(startdate) from tmp_rundate)
and
	ci.invoicedate < (select max(enddate) from tmp_rundate)
group by
	ci.invoicenumber,
	ci.invoicedate,
	ci.status,
	ci.currency,
	wsc.name,
	wh.warehouse,
	cil.description
-- order by
-- 	ci.invoicenumber
-- --	prf.name,
-- --	si.packsize
)as wsInvoice
order by
	invoicenumber,
	CASE
		WHEN wsInvoice.product = 'Carriage'
		THEN 999
		ELSE 1
	END
