
-- Adjustments

select top 1000 sum(qty_registered) over (), sum(qty_disposed) over (), *
from Warehouse.stock.fact_wh_stock_item_batch_movement_v
where product_id_magento = '1083'
	and batch_movement_date between '2019-12-01' and '2020-01-01'

-- Issues

drop table #oli
drop table #oli2
drop table #oli3
drop table #oli4


select oli.batchstockissueid_bk, oli.issue_id, oli.issue_date, oli.warehousestockitembatchid_bk, oli.qty_stock, wsib.warehouse_name
into #oli
from 
		Landing.aux.alloc_fact_order_line_erp_issue_aud oli
	inner join
		Warehouse.stock.dim_wh_stock_item_batch_v wsib on oli.warehousestockitembatchid_bk = wsib.warehousestockitembatchid_bk
where oli.cancelled_f ='N' 
	and oli.issue_date between '2019-12-01' and '2020-01-01'
	and wsib.product_id_magento = '1096'

select sum(qty_stock) over (), *
from #oli

	select warehouse_name, sum(qty_stock), count(distinct batchstockissueid_bk), count(*)
	from #oli
	group by warehouse_name
	order by warehouse_name

select batchstockissueid_bk, order_no_erp, order_type_erp_f, warehouse_name, qty_stock
into #oli2
from Warehouse.alloc.fact_order_line_erp_issue_v
where issue_date between '2019-12-01' and '2020-01-01'
	and product_id_magento = '1096'
	and cancelled_f = 'N'

select sum(qty_stock) over (), *
from #oli2

	select warehouse_name, sum(qty_stock), count(distinct batchstockissueid_bk), count(*)
	from #oli2
	group by warehouse_name
	order by warehouse_name

select batchstockissueid_bk, order_no_erp, order_type_erp_f, warehouse_name, qty_stock
into #oli3
from Warehouse.alloc.fact_order_line_erp_issue_lj_v
where issue_date between '2019-12-01' and '2020-01-01'
	and product_id_magento = '1096'
	and cancelled_f = 'N'

select sum(qty_stock) over (), *
from #oli3

	select warehouse_name, sum(qty_stock), count(distinct batchstockissueid_bk), count(*)
	from #oli3
	group by warehouse_name
	order by warehouse_name


select batchstockissueid_bk, transaction_type, order_no order_no_erp, order_type_erp_f, warehouse_name, qty_pack_erp_m qty_stock
into #oli4
from Warehouse.alloc.fact_order_line_erp_issue_all_v
where issue_date between '2019-12-01' and '2020-01-01'
	and product_id_magento = '1096'
	and cancelled_f = 'N'

select sum(qty_stock) over (), count(*) over (partition by batchstockissueid_bk) num_rep,
	*
from #oli4
order by num_rep desc

	select warehouse_name, sum(qty_stock), count(distinct batchstockissueid_bk), count(*)
	from #oli4
	group by warehouse_name
	order by warehouse_name


select t1.*, sum(t1.qty_stock) over ()
from 
		#oli3 t1
	left join
		#oli4 t2 on t1.batchstockissueid_bk = t2.batchstockissueid_bk
where t2.batchstockissueid_bk is null
order by t1.order_no_erp, t1.batchstockissueid_bk

-- Purchases

select top 1000 stock_batch_type_name, sum(qty_received), sum(sum(qty_received)) over ()
from Warehouse.stock.dim_wh_stock_item_batch_v
where product_id_magento = '1096'
	and batch_stock_register_date between '2019-12-01' and '2020-01-01'
group by stock_batch_type_name
order by stock_batch_type_name

select sum(qty_accepted) over (), 
	receiptlineid_bk, receipt_number, wh_shipment_type_name, receipt_status_name, purchase_order_number, stock_registered_date, qty_ordered, qty_accepted, wh_stock_item_batch_f
from Warehouse.rec.fact_receipt_line_v
where product_id_magento = '1096'
	and stock_registered_date between '2019-12-01' and '2020-01-01'
	and qty_accepted <> 0
	and wh_shipment_type_name <> 'Aggregated'
order by wh_stock_item_batch_f, stock_registered_date