
		select idWHStockItemBatch_sk, warehousestockitembatchid_bk, idWHStockItem_sk,
			warehouse_name, product_id_magento, product_family_name, packsize, stock_item_description, batch_id,
			qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, qty_registered_sm, qty_disposed_sm, 
			batch_stock_register_date, 
			local_total_unit_cost
		into #t0
		from Warehouse.stock.dim_wh_stock_item_batch_v
		where product_id_magento = 1089

		select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
			c.calendar_date, c.calendar_date_week_name,
			qty_received, -- qty_received + qty_registered_sm - qty_disposed_sm
			qty_issued_stock,
			local_total_unit_cost
		into #t1
		from 
				Warehouse.stock.dim_wh_stock_item_batch_v wsib
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Warehouse.gen.dim_calendar
				where calendar_date <= getutcdate()) c on convert(date, wsib.batch_stock_register_date) <= c.calendar_date
		where idWHStockItemBatch_sk in 
			(select idWHStockItemBatch_sk
			from Warehouse.stock.dim_wh_stock_item_batch_v
			where product_id_magento = 1089)

		select 
			case when (i.idWHStockItemBatch_sk is not null) then i.idWHStockItemBatch_sk else wsibm.idWHStockItemBatch_sk end idWHStockItemBatch_sk,
			case when (i.batch_id is not null) then i.batch_id else wsibm.batch_id end batch_id,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		into #t2
		from
			(select idWHStockItemBatch_sk, batch_id, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
			from Warehouse.alloc.fact_order_line_erp_issue_v
			where idWHStockItemBatch_sk in 
				(select idWHStockItemBatch_sk
				from Warehouse.stock.dim_wh_stock_item_batch_v
				where product_id_magento = 1089)			
				and cancelled_f = 'N'
			group by idWHStockItemBatch_sk, batch_id, convert(date, issue_date)) i
		full join
			(select idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
			from
				(select idWHStockItemBatch_sk, batch_id, 
					batch_movement_date, 
					-- batch_movement_create_date batch_movement_date,
					stock_adjustment_type_name, 
					case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
				from Warehouse.stock.fact_wh_stock_item_batch_movement_v
				where (qty_registered <> 0 or qty_disposed <> 0) and 
					idWHStockItemBatch_sk in 
						(select idWHStockItemBatch_sk
						from Warehouse.stock.dim_wh_stock_item_batch_v
						where product_id_magento = 1089)) wsibm
			group by idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date)) wsibm on i.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and i.trans_date = wsibm.trans_date

select top 1000 *
from #t0
order by idWHStockItemBatch_sk

	select top 1000 count(*), count(distinct idWHStockItemBatch_sk), count(distinct idWHStockItem_sk)
	from #t0

select top 1000 *
from #t1
order by idWHStockItemBatch_sk, calendar_date

	select top 1000 count(*), count(distinct idWHStockItemBatch_sk), count(*) / count(distinct idWHStockItemBatch_sk)
	from #t1

select top 1000 *
from #t2
order by idWHStockItemBatch_sk, trans_date

	select top 1000 count(*), count(distinct idWHStockItemBatch_sk)
	from #t2

-- Total Rows = 8.116.310 // 60.786.773
-- Distinct Batches = 127976
-- Duration = 2.36 min
select -- top 100000 count(*) over (), dense_rank() over (order by idWHStockItemBatch_sk),
	idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
	calendar_date, calendar_date_week_name,
	qty_hist, local_total_unit_cost
into #t3
from
	(select wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.batch_stock_register_date, 
		wsib.calendar_date, wsib.calendar_date_week_name,
		wsib.qty_received, isnull(t.qty, 0) qty, 
		-- sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date),
		wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date) qty_hist, 
		wsib.local_total_unit_cost
	from
		#t1 wsib
	left join
		#t2 t on wsib.idWHStockItemBatch_sk = t.idWHStockItemBatch_sk and wsib.calendar_date = t.trans_date) st
where qty_hist <> 0	
order by idWHStockItemBatch_sk desc, calendar_date

select *, count(*) over (), count(*) over (partition by calendar_date)
from #t3
where qty_hist < 0
-- order by idWHStockItemBatch_sk, calendar_date
order by calendar_date desc, idWHStockItemBatch_sk

	-- 230 vs 204
	select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, min(calendar_date) min_trans_date, count(distinct calendar_date) num_trans_date, sum(qty_hist) sum_qty_hist
	from #t3
	where qty_hist < 0
	group by idWHStockItemBatch_sk, batch_id, batch_stock_register_date
	order by min(calendar_date), idWHStockItemBatch_sk, batch_id, batch_stock_register_date

	select wsib.min_trans_date, wsib.num_trans_date, dateadd(dd, wsib.num_trans_date, wsib.min_trans_date), wsib.sum_qty_hist, wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.batch_stock_register_date, 
		wsibm.stock_adjustment_type_name, wsibm.qty_movement, wsibm.batch_movement_date, wsibm.movement_reason, wsibm.movement_comment, wsibm.operator, 
		dense_rank() over (partition by wsib.idWHStockItemBatch_sk order by wsibm.batch_movement_date, wsibm.batchstockmovementid_bk) 	 
	from
			(select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, min(calendar_date) min_trans_date, count(distinct calendar_date) num_trans_date, sum(qty_hist) sum_qty_hist
			from #t3
			where qty_hist < 0
			group by idWHStockItemBatch_sk, batch_id, batch_stock_register_date) wsib
		left join
			Warehouse.stock.fact_wh_stock_item_batch_movement_v wsibm on wsib.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and (wsibm.qty_registered <> 0 or wsibm.qty_disposed <> 0)	
	order by wsib.min_trans_date, wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.batch_stock_register_date


	select *
	from #t3
	where batch_id in (395807, 2053009)
	order by idWHStockItemBatch_sk, calendar_date

	select *
	from #t3
	where batch_id in (426798)
	order by idWHStockItemBatch_sk, calendar_date

	select top 1000 count(*), count(distinct idWHStockItemBatch_sk)
	from #t3

	-- Historical Stock Value for 1089

	select calendar_date, sum(qty_hist), sum(qty_hist * local_total_unit_cost)
	from #t3
	group by calendar_date
	order by calendar_date desc

----------------------------------------------
-- Comparing Historic with current values
	-- qty_hist = qty_available + qty_outstanding_allocation + qty_on_hold
select t.idWHStockItemBatch_sk, wsib.batch_id, t.calendar_date, 
	t.qty_hist, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold
from
		(select idWHStockItemBatch_sk, calendar_date, qty_hist
		from #t3
		where calendar_date = '2019-02-20') t
	left join
		Warehouse.stock.dim_wh_stock_item_batch_v wsib on t.idWHStockItemBatch_sk = wsib.idWHStockItemBatch_sk
where t.qty_hist <> wsib.qty_available + wsib.qty_outstanding_allocation + wsib.qty_on_hold
order by t.idWHStockItemBatch_sk

select t.idWHStockItemBatch_sk, wsib.batch_id, t.calendar_date, 
	t.qty_hist, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold
from
		(select idWHStockItemBatch_sk, batch_id, calendar_date, qty_hist
		from #t3
		where calendar_date = '2019-02-20') t
	full join
		(select idWHStockItemBatch_sk, batch_id, qty_available, qty_outstanding_allocation, qty_on_hold
		from Warehouse.stock.dim_wh_stock_item_batch_v
		where product_id_magento = 1089 
			and (qty_available > 0 or qty_outstanding_allocation > 0 or qty_on_hold > 0)) wsib on t.idWHStockItemBatch_sk = wsib.idWHStockItemBatch_sk
-- where t.qty_hist <> wsib.qty_available + wsib.qty_outstanding_allocation + wsib.qty_on_hold
where t.idWHStockItemBatch_sk is null or wsib.idWHStockItemBatch_sk is null
order by t.idWHStockItemBatch_sk


select *
from #t1
where batch_id = 2086408
order by calendar_date desc

select *
from #t2
where batch_id = 2086408

select *
from #t3
where batch_id = 2086408

--------------------------

-- Batches with no Value at any day: same day create and consume
select t0.*
from 
		#t0 t0
	left join
		#t3 t3 on t0.idWHStockItemBatch_sk = t3.idWHStockItemBatch_sk
where t3.idWHStockItemBatch_sk is null
order by idWHStockItemBatch_sk

-- Batches with no Value at any day + No Issue, Adj transaction
select t0.*
from 
		#t0 t0
	left join
		#t3 t3 on t0.idWHStockItemBatch_sk = t3.idWHStockItemBatch_sk
	left join
		#t2 t2 on t0.idWHStockItemBatch_sk = t2.idWHStockItemBatch_sk
where t3.idWHStockItemBatch_sk is null and t2.idWHStockItemBatch_sk is null
order by idWHStockItemBatch_sk

-- Batches with no Value at any day + Issue, Adj transaction: same day create and consume
select t2.*
from 
		#t2 t2
	left join
		#t3 t3 on t2.idWHStockItemBatch_sk = t3.idWHStockItemBatch_sk
where t3.idWHStockItemBatch_sk is null
order by idWHStockItemBatch_sk

drop table #t0
drop table #t1
drop table #t2
drop table #t3

-------------------------------------------------------------------------------


select top 1000
	idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
	calendar_date, calendar_date_week_name,
	qty_hist, local_total_unit_cost
from
	(select wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.batch_stock_register_date, 
		wsib.calendar_date, wsib.calendar_date_week_name,
		wsib.qty_received, isnull(t.qty, 0) qty, 
		-- sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date),
		wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date) qty_hist, 
		wsib.local_total_unit_cost
	from
		(select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
			c.calendar_date, c.calendar_date_week_name,
			qty_received, -- qty_received + qty_registered_sm - qty_disposed_sm
			qty_issued_stock,
			local_total_unit_cost
		from 
				Warehouse.stock.dim_wh_stock_item_batch_v wsib
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Warehouse.gen.dim_calendar
				where calendar_date <= getutcdate()) c on convert(date, wsib.batch_stock_register_date) <= c.calendar_date
		where idWHStockItemBatch_sk in 
			(select idWHStockItemBatch_sk
			from Warehouse.stock.dim_wh_stock_item_batch_v
			where product_id_magento = 1083)) wsib
	left join
		(select 
			case when (i.idWHStockItemBatch_sk is not null) then i.idWHStockItemBatch_sk else wsibm.idWHStockItemBatch_sk end idWHStockItemBatch_sk,
			case when (i.batch_id is not null) then i.batch_id else wsibm.batch_id end batch_id,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		from
			(select idWHStockItemBatch_sk, batch_id, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
			from Warehouse.alloc.fact_order_line_erp_issue_v
			where idWHStockItemBatch_sk in 
				(select idWHStockItemBatch_sk
				from Warehouse.stock.dim_wh_stock_item_batch_v
				where product_id_magento = 1083)			
				and cancelled_f = 'N'
			group by idWHStockItemBatch_sk, batch_id, convert(date, issue_date)) i
		full join
			(select idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
			from
				(select idWHStockItemBatch_sk, batch_id, batch_movement_date,
					stock_adjustment_type_name, 
					case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
				from Warehouse.stock.fact_wh_stock_item_batch_movement_v
				where (qty_registered <> 0 or qty_disposed <> 0) and 
					idWHStockItemBatch_sk in 
						(select idWHStockItemBatch_sk
						from Warehouse.stock.dim_wh_stock_item_batch_v
						where product_id_magento = 1083)) wsibm
			group by idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date)) wsibm on i.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and i.trans_date = wsibm.trans_date) t 
				on wsib.idWHStockItemBatch_sk = t.idWHStockItemBatch_sk and wsib.calendar_date = t.trans_date) st
where qty_hist <> 0	
order by idWHStockItemBatch_sk, calendar_date

