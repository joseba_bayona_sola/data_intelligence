
use Landing
go

-- "po"."supplierreference": Most null, "po"."receiveddate": All NULL (Not used since 2016)
--  "product"."oldsku" AS "ProductCode", 
-- "supp_price".directPrice: All 0

SELECT top 1000 
    "warehouse"."name" AS "Warehouse", "supp"."suppliername" AS "BuyFromVendor", 

	"po"."source" AS "Source", "po"."potype" AS "POType", "po"."status" AS "Status", "po"."ordernumber" AS "OrderID", "po"."supplierreference" AS "SuplierOrderID", 
	"po"."receiveddate" AS "Received_Date", "po"."duedate" AS "PromiseDate", 
	
    "prod_family"."magentosku" AS "SKU", "prod_family"."name" AS "ItemName", "packsize"."size" AS "PackSize",  
    "product"."oldsku" AS "ProductCode", "stockitem"."stockitemdescription" AS "Product_Description",

	po_line.po_lineID, "po_line"."quantity" AS "OrderedQTY", "po_line"."quantityopen" AS "Quantity_Open", "po_line"."lineprice" AS "Total_Line_Amount", "po_line"."createddate" AS "createdDate", 
	
	"supp_price"."unitprice" AS "Unit_Cost", "supp_price".directPrice AS "Direct", "supp"."currencycode" AS "CurrencyCode"
    
FROM 

		[mend].[purc_purchaseorderline] "po_line"
	LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_purchaseorderlineheader] "a1purchaseorders$purchaseorderline_purchaseorderlineheader" ON "a1purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineid = "po_line"."id"
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_productfamily] "a2purchaseorders$purchaseorderlineheader_productfamily" ON "a2purchaseorders$purchaseorderlineheader_productfamily".purchaseorderlineheaderid = "a1purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
    LEFT OUTER JOIN 
		[mend].[prod_productfamily] "prod_family" ON "prod_family"."id" = "a2purchaseorders$purchaseorderlineheader_productfamily".productfamilyid
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_purchaseorder]"a3purchaseorders$purchaseorderlineheader_purchaseorder" ON "a3purchaseorders$purchaseorderlineheader_purchaseorder".purchaseorderlineheaderid = "a1purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorder]"po" ON "po"."id" = "a3purchaseorders$purchaseorderlineheader_purchaseorder".purchaseorderid
   
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorder_warehouse]"a4purchaseorders$purchaseorder_warehouse" ON "a4purchaseorders$purchaseorder_warehouse".purchaseorderid = "po"."id"
    LEFT OUTER JOIN 
		[mend].[wh_warehouse] "warehouse" ON "warehouse".id = "a4purchaseorders$purchaseorder_warehouse".warehouseid

    LEFT OUTER JOIN 
		[mend].[purc_purchaseorder_supplier] "a5purchaseorders$purchaseorder_supplier" ON "a5purchaseorders$purchaseorder_supplier".purchaseorderid = "po"."id"
    LEFT OUTER JOIN 
		[mend].[supp_supplier] "supp" ON "supp"."id" = "a5purchaseorders$purchaseorder_supplier".supplierid

    LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_packsize] "a6purchaseorders$purchaseorderlineheader_packsize" ON "a6purchaseorders$purchaseorderlineheader_packsize".purchaseorderlineheaderid = "a1purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
    LEFT OUTER JOIN 
		[mend].[prod_packsize] "packsize" ON "packsize"."id" = "a6purchaseorders$purchaseorderlineheader_packsize".packsizeid
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_supplierprices] "a7purchaseorders$purchaseorderlineheader_supplierprices" ON "a7purchaseorders$purchaseorderlineheader_supplierprices".purchaseorderlineheaderid = "a1purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
    LEFT OUTER JOIN 
		[mend].[supp_supplierprice] "supp_price" ON "supp_price"."id" = "a7purchaseorders$purchaseorderlineheader_supplierprices".supplierpriceid
    LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_stockitem] "a8purchaseorders$purchaseorderline_stockitem" ON "a8purchaseorders$purchaseorderline_stockitem".purchaseorderlineid = "po_line"."id"
    LEFT OUTER JOIN 
		[mend].[prod_stockitem] "stockitem" ON "stockitem"."id" = "a8purchaseorders$purchaseorderline_stockitem".stockitemid
    LEFT OUTER JOIN 
		[mend].[prod_stockitem_product] "a9product$stockitem_product" ON "a9product$stockitem_product".stockitemid = "stockitem"."id"
    LEFT OUTER JOIN 
		[mend].[prod_product] "product" ON "product"."id" = "a9product$stockitem_product".productid
WHERE po.status not in ('Cancelled','Completed') and po.status is not null
	and "po"."ordernumber" = 'P00022548'