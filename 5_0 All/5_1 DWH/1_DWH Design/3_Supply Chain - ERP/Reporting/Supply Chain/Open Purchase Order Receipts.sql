use Landing
go

-- What about PO Lines where Receipt still not created

-- "receipt"."duedate" -- "receipt"."supplieradviceref"
-- "product"."oldsku" AS "ProductCode",

SELECT top 1000 
	"warehouse"."name" AS "Warehouse", "supp"."suppliername" AS "Buy_From_Supplier", 

	CAST("receipt"."status" AS varchar)  AS "Receipt_Status", "receipt"."receiptid" AS "ReceiptID", "receipt"."supplieradviceref" AS "Delivery_Note",
    "receipt"."createddate" AS "Receipt_Created", "receipt"."arriveddate" AS "Arrived_Date", 
	"receipt"."confirmeddate" AS "Received_Date", "receipt"."stockregistereddate" AS "Registered_Date", "receipt"."duedate" AS "Expected_Date", 

	CASE 
		WHEN receipt.status ='Open' OR receipt.status ='In_Transiten' OR receipt.status ='Locked' OR receipt.status ='Arrived' OR receipt.status ='Planned' THEN "receiptline"."quantityordered" 
		WHEN receipt.status = 'Received' THEN "receiptline"."quantityreceived" 
		ELSE 0 
	END AS "QuantityOpen",
	"receiptline"."unitcost" AS "Unit_Cost",

	CAST("po"."source" AS varchar) AS "PO_Source", CAST("po"."status" AS varchar) AS "PO_Status", CAST("po"."potype" AS varchar) AS "PO_Type", "po"."ordernumber" AS "PO_Number", "po"."supplierreference" AS "Supplier_Ref",
	"po"."createddate" AS "PO_Created", "po"."approveddate" AS "Approved_Date", "po"."confirmeddate" AS "Confirmed_Date", "po"."submitteddate" AS "Submitted_Date", "po"."duedate" AS "Due_Date", 

    "transfer_po"."ordernumber" AS "Transfer_PO", "iso_tpo"."incrementid" AS "ISO_TPO", "iso_spo"."incrementid" AS "ISO_SPO",
    
    "transfer_customer"."name" AS "TPO_Customer_Name", "supply_customer"."name" AS "SPO_Customer_Name",

    "product_cat"."name" AS "Product_Category",
    "product_fam"."magentoproductid" AS "MagentoID", "product_fam"."name" AS "Product_Name", "packsize"."size" AS "Pack_Size",
    "product"."oldsku" AS "ProductCode",
    "stockitem"."sku" AS "SKU", "stockitem"."stockitemdescription" AS "Description",

    "supp"."currencycode" AS "Currency"
    
    
    --"assigned_account"."fullname" AS "PO_Assigned_To",
    --"created_account"."fullname" AS "PO_Created_By",
    --"manufacturer"."manufacturername" AS "Manufacturer",

FROM 
		[mend].[whship_receiptline] "receiptline"
     LEFT OUTER JOIN 
		[mend].[whship_receiptline_receipt] "a1warehouseshipments$receiptline_receipt" ON "a1warehouseshipments$receiptline_receipt".receiptlineid = "receiptline"."id"
     LEFT OUTER JOIN 
		[mend].[whship_receipt] "receipt" ON "receipt"."id" = "a1warehouseshipments$receiptline_receipt".receiptid
     LEFT OUTER JOIN 
		[mend].[whship_receiptline_purchaseorderline] "a2warehouseshipments$receiptline_purchaseorderline" ON "a2warehouseshipments$receiptline_purchaseorderline".receiptlineid = "receiptline"."id"
     LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_purchaseorderlineheader] "a3purchaseorders$purchaseorderline_purchaseorderlineheader" ON "a3purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineid = "a2warehouseshipments$receiptline_purchaseorderline".purchaseorderlineid
     LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_purchaseorder] "a4purchaseorders$purchaseorderlineheader_purchaseorder" ON "a4purchaseorders$purchaseorderlineheader_purchaseorder".purchaseorderlineheaderid = "a3purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
     LEFT OUTER JOIN 
		[mend].[purc_purchaseorder] "po" ON "po"."id" = "a4purchaseorders$purchaseorderlineheader_purchaseorder".purchaseorderid
     LEFT OUTER JOIN 
		[mend].[whship_receiptline_purchaseorderline] "a5warehouseshipments$receiptline_purchaseorderline" ON "a5warehouseshipments$receiptline_purchaseorderline".receiptlineid = "receiptline"."id"

     LEFT OUTER JOIN 
		[mend].[inter_supplypo_transferheader] "a6purchaseorders$supplypo_transferheader" ON "a6purchaseorders$supplypo_transferheader".purchaseorderid = "po"."id"
     LEFT OUTER JOIN 
		[mend].[inter_transferpo_transferheader] "a7purchaseorders$transferpo_transferheader" ON "a7purchaseorders$transferpo_transferheader".purchaseorderid = "po"."id"

     LEFT OUTER JOIN 
		[mend].[inter_transferpo_transferheader] "a8purchaseorders$transferpo_transferheader" ON "a8purchaseorders$transferpo_transferheader".transferheaderid = "a6purchaseorders$supplypo_transferheader".transferheaderid
     LEFT OUTER JOIN 
		[mend].[purc_purchaseorder] "transfer_po" ON "transfer_po"."id" = "a8purchaseorders$transferpo_transferheader".purchaseorderid
     LEFT OUTER JOIN 
		[mend].[inter_transferpo_transferheader] "a9purchaseorders$transferpo_transferheader" ON "a9purchaseorders$transferpo_transferheader".purchaseorderid = "po"."id"
     LEFT OUTER JOIN 
		[mend].[inter_order_transferheader] "a10orderprocessing$order_transferheader" ON "a10orderprocessing$order_transferheader".transferheaderid = "a9purchaseorders$transferpo_transferheader".transferheaderid
     LEFT OUTER JOIN 
		[mend].[order_order_aud] "iso_tpo" ON "iso_tpo"."id" = "a10orderprocessing$order_transferheader".orderid

     LEFT OUTER JOIN 
		[mend].[inter_supplypo_transferheader] "a11purchaseorders$supplypo_transferheader" ON "a11purchaseorders$supplypo_transferheader".purchaseorderid = "po"."id"
     LEFT OUTER JOIN 
		[mend].[inter_order_transferheader] "a12orderprocessing$order_transferheader" ON "a12orderprocessing$order_transferheader".transferheaderid = "a11purchaseorders$supplypo_transferheader".transferheaderid
     LEFT OUTER JOIN 
		[mend].[order_order_aud] "iso_spo" ON "iso_spo"."id" = "a12orderprocessing$order_transferheader".orderid

     LEFT OUTER JOIN 
		[mend].[whship_receipt_warehouse] "a13warehouseshipments$receipt_warehouse" ON "a13warehouseshipments$receipt_warehouse".receiptid = "receipt"."id"
     LEFT OUTER JOIN 
		[mend].[wh_warehouse] "warehouse" ON "warehouse"."id" = "a13warehouseshipments$receipt_warehouse".warehouseid

     LEFT OUTER JOIN 
		[mend].[whship_receiptline_supplier] "a14warehouseshipments$receiptline_supplier" ON "a14warehouseshipments$receiptline_supplier".receiptlineid = "receiptline"."id"
     LEFT OUTER JOIN 
		[mend].[supp_supplier] "supp" ON "supp"."id" = "a14warehouseshipments$receiptline_supplier".supplierid

     LEFT OUTER JOIN 
		[mend].[inter_order_transferheader] "a15orderprocessing$order_transferheader" ON "a15orderprocessing$order_transferheader".transferheaderid = "a6purchaseorders$supplypo_transferheader".transferheaderid
     LEFT OUTER JOIN 
		[mend].[order_wholesalecustomerorders] "a16orderprocessing$wholesalecustomerorders" ON "a16orderprocessing$wholesalecustomerorders".orderid = "a15orderprocessing$order_transferheader".orderid
     LEFT OUTER JOIN 
		[mend].[whcust_wholesalecustomer] "supply_customer" ON "supply_customer"."id" = "a16orderprocessing$wholesalecustomerorders".wholesalecustomerid
     LEFT OUTER JOIN 
		[mend].[inter_order_transferheader] "a17orderprocessing$order_transferheader" ON "a17orderprocessing$order_transferheader".transferheaderid = "a7purchaseorders$transferpo_transferheader".transferheaderid
     LEFT OUTER JOIN 
		[mend].[order_wholesalecustomerorders] "a18orderprocessing$wholesalecustomerorders" ON "a18orderprocessing$wholesalecustomerorders".orderid = "a17orderprocessing$order_transferheader".orderid
     LEFT OUTER JOIN 
		[mend].[whcust_wholesalecustomer] "transfer_customer" ON "transfer_customer"."id" = "a18orderprocessing$wholesalecustomerorders".wholesalecustomerid

     LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_stockitem] "a19purchaseorders$purchaseorderline_stockitem" ON "a19purchaseorders$purchaseorderline_stockitem".purchaseorderlineid = "a5warehouseshipments$receiptline_purchaseorderline".purchaseorderlineid
     LEFT OUTER JOIN 
		[mend].[prod_stockitem] "stockitem" ON "stockitem"."id" = "a19purchaseorders$purchaseorderline_stockitem".stockitemid
     LEFT OUTER JOIN 
		[mend].[prod_stockitem_product] "a20product$stockitem_product" ON "a20product$stockitem_product".stockitemid = "stockitem"."id"
     LEFT OUTER JOIN 
		[mend].[prod_product] "product" ON "product"."id" = "a20product$stockitem_product".productid
     LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_productfamily] "a21purchaseorders$purchaseorderlineheader_productfamily" ON "a21purchaseorders$purchaseorderlineheader_productfamily".purchaseorderlineheaderid = "a3purchaseorders$purchaseorderline_purchaseorderlineheader".purchaseorderlineheaderid
     LEFT OUTER JOIN 
		[mend].[prod_productfamily] "product_fam" ON "product_fam"."id" = "a21purchaseorders$purchaseorderlineheader_productfamily".productfamilyid
     LEFT OUTER JOIN 
		[mend].[prod_productfamily_productcategory] "a22product$productfamily_productcategory" ON "a22product$productfamily_productcategory".productfamilyid = "product_fam"."id"
     LEFT OUTER JOIN 
		[mend].[prod_productcategory] "product_cat" ON "product_cat"."id" = "a22product$productfamily_productcategory".productcategoryid
     LEFT OUTER JOIN 
		[mend].[prod_stockitem_packsize] "a23product$stockitem_packsize" ON "a23product$stockitem_packsize".stockitemid = "stockitem"."id"
     LEFT OUTER JOIN 
		[mend].[prod_packsize] "packsize" ON "packsize"."id" = "a23product$stockitem_packsize".packsizeid
    -- LEFT OUTER JOIN "purchaseorders$purchaseorder_assignedto" "a24purchaseorders$purchaseorder_assignedto" ON "a24purchaseorders$purchaseorder_assignedto"."purchaseorders$purchaseorderid" = "po"."id"
    -- LEFT OUTER JOIN "administration$account" "assigned_account" ON "assigned_account"."id" = "a24purchaseorders$purchaseorder_assignedto"."administration$accountid"
    -- LEFT OUTER JOIN "purchaseorders$purchaseorder_createdby" "a25purchaseorders$purchaseorder_createdby" ON "a25purchaseorders$purchaseorder_createdby"."purchaseorders$purchaseorderid" = "po"."id"
    -- LEFT OUTER JOIN "administration$account" "created_account" ON "created_account"."id" = "a25purchaseorders$purchaseorder_createdby"."administration$accountid"
    -- LEFT OUTER JOIN "product$productfamily_defaultmanufacturer" "a26product$productfamily_defaultmanufacturer" ON "a26product$productfamily_defaultmanufacturer"."product$productfamilyid" = "product_fam"."id"
    -- LEFT OUTER JOIN [mend].[supp_manufacturer] "manufacturer" ON "manufacturer"."id" = "a26product$productfamily_defaultmanufacturer".manufacturerid
WHERE -- po.[status] not in ( 'Cancelled', 'Completed' ) and 
	po.[status] is not null
	-- and "po"."ordernumber" in ('P00030724', 'P00031041')
		and "po"."ordernumber" in ('P00038028', 'P00038034')
	-- and "iso_spo"."incrementid" is not null
order by "po"."ordernumber"