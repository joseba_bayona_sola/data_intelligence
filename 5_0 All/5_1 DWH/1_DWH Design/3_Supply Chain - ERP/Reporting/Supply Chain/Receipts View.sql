use Landing
go

-- Understand Filtering and Logic

-- SupplierAdviceRef AS Delivery_note

SELECT top 1000 
	warehouse.Name AS Warehouse,

	receipt.ReceiptId AS Receipt, receipt.SupplierAdviceRef AS Delivery_note, receipt.createdDate AS receipt_created_date,

	receipt.ArrivedDate AS Arrived_date, receipt.ConfirmedDate AS Confirmed_Date, receipt.StockRegisteredDate AS Stock_registered_date,
	receiptline.StockItem AS StockItem, receiptline.QuantityReceived AS Quantity_received, receiptline.QuantityAccepted AS Quantity_accepted,
	receiptline.UnitCost AS Unit_cost,

	CASE
		WHEN receiptline.InvoiceUnitCost IS NOT NULL
		THEN
		CASE
			WHEN receiptline.QuantityAccepted IS NOT NULL
			THEN receiptline.InvoiceUnitCost * receiptline.QuantityAccepted
			ELSE
			receiptline.InvoiceUnitCost * receiptline.QuantityReceived
		END
		ELSE
		CASE
			WHEN receiptline.QuantityAccepted IS NOT NULL
			THEN receiptline.UnitCost * receiptline.QuantityAccepted
			ELSE
			receiptline.UnitCost * receiptline.QuantityReceived
		END
	END as LineTotalValue,

	po.OrderNumber  AS PO_number, po.createdDate AS PO_created, po.SupplierReference AS Supplier_order_ref,
	supp.SupplierName AS Supplier,


	product_fam.Name AS Product_family, stockitem.PackSize AS Pack_size,
	stockitem.StockItemDescription AS Stock_Item_Description, 
	
	supp.CurrencyCode AS Currency
	
FROM 
		[mend].[whship_receiptline] receiptline
	LEFT OUTER JOIN 
		[mend].[whship_receiptline_receipt] a1 ON a1.[receiptlineid] = receiptline.id
	LEFT OUTER JOIN 
		[mend].[whship_receipt] receipt ON receipt.id = a1.receiptid
	LEFT OUTER JOIN 
		[mend].[whship_receiptline_purchaseorderline] a2 ON a2.receiptlineid = receiptline.id
	LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_purchaseorderlineheader] a3  ON a3.purchaseorderlineid = a2.purchaseorderlineid
	LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_purchaseorder] a4 ON a4.purchaseorderlineheaderid = a3.purchaseorderlineheaderid
	LEFT OUTER JOIN 
		[mend].[purc_purchaseorder] po ON po.id = a4.purchaseorderid
	LEFT OUTER JOIN 
		[mend].[whship_receiptline_purchaseorderline] a5 ON a5.receiptlineid = receiptline.id

	LEFT OUTER JOIN 
		[mend].[whship_receiptline_supplier] a6 ON a6.receiptlineid = receiptline.id
	LEFT OUTER JOIN
		[mend].[supp_supplier] supp ON supp.id = a6.supplierid

	LEFT OUTER JOIN 
		[mend].[purc_purchaseorderline_stockitem] a7 ON a7.purchaseorderlineid = a5.purchaseorderlineid
	LEFT OUTER JOIN 
		[mend].[prod_stockitem] stockitem ON "stockitem"."id" = a7.stockitemid
	LEFT OUTER JOIN 
		[mend].[purc_purchaseorderlineheader_productfamily] a8 ON "a8"."purchaseorderlineheaderid" = a3.purchaseorderlineheaderid
	LEFT OUTER JOIN 
		[mend].[prod_productfamily] "product_fam" ON "product_fam"."id" = "a8"."productfamilyid"

	LEFT OUTER JOIN 
		[mend].[whship_receipt_warehouse] a9 ON a9.receiptid = "receipt"."id"
	LEFT OUTER JOIN 
		[mend].[wh_warehouse] "warehouse"  ON "warehouse"."id" = a9.warehouseid

-- WHERE (DATEPART(YEAR,receipt.createdDate) = year(getdate()) or receipt.createdDate IS NULL)
where po.OrderNumber = 'P00022548' -- and supp.SupplierName = 'Ta-To Contacts Co. Ltd'
order by stock_item_description, receipt