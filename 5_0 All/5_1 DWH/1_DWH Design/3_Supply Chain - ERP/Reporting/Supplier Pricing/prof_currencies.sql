
select top 1000 *
from Landing.mend.gen_comp_currency_v
where effectivedate_d = '2019-05-20'

select *
from Landing.mend.gen_comp_currency_exchange_v
where effectivedate = '2019-05-20'
order by currencycode_from, currencycode_to

		select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate_d, nexteffectivedate_d, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
		from Landing.mend.gen_comp_currency_v
		where effectivedate_d = '2019-05-20'
		union
		select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, historicalrate exchange_rate
		from Landing.mend.gen_comp_currency_v
		where effectivedate_d = '2019-05-20'
		union
		select c1.currencycode_from, c2.currencycode_to, c1.effectivedate_d, c1.nexteffectivedate_d,
			c1.exchange_rate * c2.exchange_rate exchange_rate
		from
				(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate_d, nexteffectivedate_d, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
				from Landing.mend.gen_comp_currency_v
				where currencycode not in ('GBP', 'EUR')) c1
			left join
				(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, historicalrate exchange_rate
				from Landing.mend.gen_comp_currency_v
				where currencycode = 'EUR') c2 on c1.currencycode_to = c2.currencycode_from and 
					c1.effectivedate_d = c2.effectivedate_d and c1.nexteffectivedate_d = c2.nexteffectivedate_d
		where c1.effectivedate_d = '2019-05-20'
		union
		select 'EUR' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, 1 exchange_rate
		from Landing.mend.gen_comp_currency_v
		where currencycode = 'EUR'
				AND effectivedate_d = '2019-05-20'
		union
		select c1.currencycode_from, c2.currencycode_to, c1.effectivedate_d, c1.nexteffectivedate_d,
			c1.exchange_rate * c2.exchange_rate exchange_rate
		from
				(select currencycode currencycode_from, 'GBP' currencycode_to, effectivedate_d, nexteffectivedate_d, convert(decimal(12, 8), 1 / historicalrate) exchange_rate
				from Landing.mend.gen_comp_currency_v
				where currencycode not in ('GBP', 'USD')) c1
			left join
				(select 'GBP' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, historicalrate exchange_rate
				from Landing.mend.gen_comp_currency_v
				where currencycode = 'USD') c2 on c1.currencycode_to = c2.currencycode_from and 
					c1.effectivedate_d = c2.effectivedate_d and c1.nexteffectivedate_d = c2.nexteffectivedate_d
		where c1.effectivedate_d = '2019-05-20'
		union
		select 'USD' currencycode_from, currencycode currencycode_to, effectivedate_d, nexteffectivedate_d, 1 exchange_rate
		from Landing.mend.gen_comp_currency_v
		where currencycode = 'USD'
				AND effectivedate_d = '2019-05-20'
