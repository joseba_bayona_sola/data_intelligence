
select idWHStockItemBatch_sk, warehousestockitembatchid_bk, 
	warehouse_name, product_id_magento, product_family_name, packsize, stock_item_description, batch_id,
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, qty_registered_sm, qty_disposed_sm, 
	batch_stock_register_date, 
	local_total_unit_cost
from Warehouse.stock.dim_wh_stock_item_batch_v
where product_id_magento = 1083
	-- and qty_available <> 0 and qty_issued_stock <> 0 and (qty_registered_sm <> 0 or qty_disposed_sm <> 0)
	and idWHStockItemBatch_sk = 1482072

select orderid_bk, orderlineid_bk, order_no_erp, order_id_bk, order_line_id_bk, order_date_sync, 
	issue_date, issue_id, batch_id, qty_unit, qty_stock, local_total_unit_cost, sum(qty_stock) over ()
from Warehouse.alloc.fact_order_line_erp_issue_v
where idWHStockItemBatch_sk = 1482072
	and cancelled_f = 'N'
order by issue_date

select top 1000 batchstockmovementid_bk, batch_id, batch_stock_move_id, move_id, move_line_id, 
	batch_movement_date,
	stock_adjustment_type_name, stock_movement_type_name, 
	qty_movement, qty_registered, qty_disposed
from Warehouse.stock.fact_wh_stock_item_batch_movement_v
where idWHStockItemBatch_sk = 1482072

--------------------------------------------------------------------------

	select idWHStockItemBatch_sk, batch_id, batch_stock_register_date,
		qty_received, -- qty_received + qty_registered_sm - qty_disposed_sm
		qty_issued_stock,
		local_total_unit_cost
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where idWHStockItemBatch_sk in (1482072, 1600979)

	select idWHStockItemBatch_sk, batch_id, convert(date, issue_date) issue_date, sum(qty_stock) * -1
	from Warehouse.alloc.fact_order_line_erp_issue_v
	where idWHStockItemBatch_sk in (1482072, 1600979)
		and cancelled_f = 'N'
	group by idWHStockItemBatch_sk, batch_id, convert(date, issue_date)
	order by issue_date

	select idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) batch_movement_date, sum(qty_movement)
	from
		(select idWHStockItemBatch_sk, batch_id, batch_movement_date,
			stock_adjustment_type_name, 
			case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
		from Warehouse.stock.fact_wh_stock_item_batch_movement_v
		where (qty_registered <> 0 or qty_disposed <> 0) and 
			idWHStockItemBatch_sk in (1482072, 1600979)) wsibm
	group by idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) 		

--------------------------------------------------------------------------

	select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
		c.calendar_date, c.calendar_date_week_name,
		qty_received, -- qty_received + qty_registered_sm - qty_disposed_sm
		qty_issued_stock,
		local_total_unit_cost
	from 
			Warehouse.stock.dim_wh_stock_item_batch_v wsib
		inner join
			(select idCalendar_sk, calendar_date, calendar_date_week_name
			from Warehouse.gen.dim_calendar
			where calendar_date <= getutcdate()) c on convert(date, wsib.batch_stock_register_date) <= c.calendar_date
	where idWHStockItemBatch_sk in (1482072)
	order by wsib.idWHStockItemBatch_sk, c.calendar_date

	select 
		case when (i.idWHStockItemBatch_sk is not null) then i.idWHStockItemBatch_sk else wsibm.idWHStockItemBatch_sk end idWHStockItemBatch_sk,
		case when (i.batch_id is not null) then i.batch_id else wsibm.batch_id end batch_id,
		case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
		isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
	from
		(select idWHStockItemBatch_sk, batch_id, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
		from Warehouse.alloc.fact_order_line_erp_issue_v
		where idWHStockItemBatch_sk in (1482072)
			and cancelled_f = 'N'
		group by idWHStockItemBatch_sk, batch_id, convert(date, issue_date)) i
	full join
		(select idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
		from
			(select idWHStockItemBatch_sk, batch_id, batch_movement_date,
				stock_adjustment_type_name, 
				case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
			from Warehouse.stock.fact_wh_stock_item_batch_movement_v
			where (qty_registered <> 0 or qty_disposed <> 0) and 
				idWHStockItemBatch_sk in (1482072)) wsibm
		group by idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date)) wsibm on i.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and i.trans_date = wsibm.trans_date 	
	order by idWHStockItemBatch_sk, trans_date 

-------------------------------------------------------------------------------

select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
	calendar_date, calendar_date_week_name,
	qty_hist, local_total_unit_cost
from
	(select wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.batch_stock_register_date, 
		wsib.calendar_date, wsib.calendar_date_week_name,
		wsib.qty_received, isnull(t.qty, 0) qty, 
		-- sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date),
		wsib.qty_received + sum(isnull(t.qty, 0)) over (partition by wsib.idWHStockItemBatch_sk order by wsib.calendar_date) qty_hist, 
		wsib.local_total_unit_cost
	from
		(select idWHStockItemBatch_sk, batch_id, batch_stock_register_date, 
			c.calendar_date, c.calendar_date_week_name,
			qty_received, -- qty_received + qty_registered_sm - qty_disposed_sm
			qty_issued_stock,
			local_total_unit_cost
		from 
				Warehouse.stock.dim_wh_stock_item_batch_v wsib
			inner join
				(select idCalendar_sk, calendar_date, calendar_date_week_name
				from Warehouse.gen.dim_calendar
				where calendar_date <= getutcdate()) c on convert(date, wsib.batch_stock_register_date) <= c.calendar_date
		where idWHStockItemBatch_sk in (11512, 1482072)) wsib
	left join
		(select 
			case when (i.idWHStockItemBatch_sk is not null) then i.idWHStockItemBatch_sk else wsibm.idWHStockItemBatch_sk end idWHStockItemBatch_sk,
			case when (i.batch_id is not null) then i.batch_id else wsibm.batch_id end batch_id,
			case when (i.trans_date is not null) then i.trans_date else wsibm.trans_date end trans_date,
			isnull(i.qty, 0) + isnull(wsibm.qty, 0) qty
		from
			(select idWHStockItemBatch_sk, batch_id, convert(date, issue_date) trans_date, sum(qty_stock) * -1 qty
			from Warehouse.alloc.fact_order_line_erp_issue_v
			where idWHStockItemBatch_sk in (11512, 1482072)
				and cancelled_f = 'N'
			group by idWHStockItemBatch_sk, batch_id, convert(date, issue_date)) i
		full join
			(select idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date) trans_date, sum(qty_movement) qty
			from
				(select idWHStockItemBatch_sk, batch_id, batch_movement_date,
					stock_adjustment_type_name, 
					case when (qty_registered <> 0) then qty_registered * 1 else qty_disposed * -1 end qty_movement
				from Warehouse.stock.fact_wh_stock_item_batch_movement_v
				where (qty_registered <> 0 or qty_disposed <> 0) and 
					idWHStockItemBatch_sk in (11512, 1482072)) wsibm
			group by idWHStockItemBatch_sk, batch_id, convert(date, batch_movement_date)) wsibm on i.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk and i.trans_date = wsibm.trans_date) t 
				on wsib.idWHStockItemBatch_sk = t.idWHStockItemBatch_sk and wsib.calendar_date = t.trans_date) st
where qty_hist <> 0	
order by idWHStockItemBatch_sk, calendar_date

