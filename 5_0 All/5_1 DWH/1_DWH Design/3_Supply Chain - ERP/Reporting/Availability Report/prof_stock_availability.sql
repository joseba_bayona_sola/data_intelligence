
select top 1000 *
from Warehouse.stock.dim_wh_stock_item_v
where product_id_magento = '2317'

-- qty_on_hand
	select wsib.idWHStockItemBatch_sk, wsib.batch_id, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_on_hold
	from 
			Warehouse.stock.dim_wh_stock_item_batch wsib
		inner join
			Warehouse.stock.dim_wh_stock_item_v wsi on wsib.idWHStockItem_sk_fk = wsi.idWHStockItem_sk
	where wsib.qty_available <> 0 or wsib.qty_outstanding_allocation <> 0 or wsib.qty_on_hold <> 0
		and wsi.product_id_magento = '2317'

-- qty_on_transit
	select top 1000 idIntransitStockItemBatch_sk, qty_remaining
	-- select *
	from Warehouse.stock.dim_intransit_stock_item_batch_v
	where qty_remaining > 0
		and product_id_magento = '2317'

-- qty_open
	select top 1000 idReceiptLine_sk, idStockItem_sk, warehouse_name, due_date, receipt_status_name,
		case 
			when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
			when receipt_status_name in ('Received') then qty_received
			else 0 -- Stock_Registered
		end as qty_open
	-- select *
	from Warehouse.rec.fact_receipt_line_v
	where product_id_magento = '2317'
		and receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
		and supplier_type_name = 'Company'
	order by due_date

	select warehouse_name, supplier_name, receipt_number, receipt_status_name, 
		purchase_order_number, po_source_name, po_type_name, po_status_name, 
		created_date, due_date, 
		product_id_magento, packsize, sku, stock_item_description, 
		case 
			when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
			when receipt_status_name in ('Received') then qty_received
			else 0 -- Stock_Registered
		end as qty_open
	from Warehouse.rec.fact_receipt_line_v
	where product_id_magento = '2317'
		and receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
		and supplier_type_name = 'Company'
	order by due_date

-- qty_planned

	select top 1000 idStockItem_sk, warehouse_name, due_date, quantity
	from Warehouse.fm.fact_planned_requirements_current_v
	where product_id_magento = '2317'
	order by due_date

-- qty_sales_forecast

	select top 10000 idStockItem_sk, warehouse_name, qty_sales_forecast, fm_date
	from Warehouse.fm.fact_fm_data_v
	where fm_date between convert(date, getutcdate()) and dateadd(dd, 30, getutcdate())
		and product_id_magento = '2317'
	order by idStockItem_sk, warehouse_name, fm_date

-- qty_issued

select issue_date, order_type_erp_f, warehouse_name product_id_magento, qty_stock
from Warehouse.alloc.fact_order_line_erp_issue_v
where product_id_magento = '2317'
	and issue_date between dateadd(dd, -30, getutcdate()) and convert(date, getutcdate())
order by order_type_erp_f, issue_date
