
					select top 1000 *
					from 
							Warehouse.stock.dim_intransit_stock_item_batch_v isib
					where qty_remaining > 0
						and isib.idStockItem_sk_fk = 66918

					select top 1000 idReceiptLine_sk, idStockItem_sk, warehouse_name, supplier_type_name, supplier_name, 
						receipt_number, purchase_order_number, po_source_name, po_type_name,
						due_date, receipt_status_name,
						case 
							when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
							when receipt_status_name in ('Received') then qty_received
							else 0 -- Stock_Registered
						end as qty_open
					from Warehouse.rec.fact_receipt_line_v
					where idStockItem_sk = 66918
						and receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
						-- and supplier_type_name = 'Company'
					order by due_date
