
select top 1000 count(*)
from Warehouse.stock.dim_wh_stock_item_v

	-- select top 1000 *
	-- from Warehouse.stock.dim_wh_stock_item

-- qty_on_hand
	-- FROM dim_wh_stock_item
	select top 1000 count(*) over (),
		idWHStockItem_sk, idWarehouse_sk_fk idWarehouse_sk, idStockItem_sk_fk idStockItem_sk, 
		qty_available, qty_outstanding_allocation, qty_on_hold, 
		qty_available + qty_outstanding_allocation + qty_on_hold qty_on_hand
	from Warehouse.stock.dim_wh_stock_item
	where qty_available <> 0 or qty_outstanding_allocation <> 0 or qty_on_hold <> 0

	select top 1000 count(*) over (), 
		wsib.idWHStockItem_sk_fk idWHStockItem_sk, 
		wsi.idWarehouse_sk_fk idWarehouse_sk, wsi.idStockItem_sk_fk idStockItem_sk, 
		-- qty_available, qty_outstanding_allocation, qty_on_hold, 
		sum(wsib.qty_available + wsib.qty_outstanding_allocation + wsib.qty_on_hold) qty_on_hand
	from 
			Warehouse.stock.dim_wh_stock_item_batch wsib
		inner join	
			Warehouse.stock.dim_wh_stock_item wsi on wsib.idWHStockItem_sk_fk = wsi.idWHStockItem_sk
	where wsib.qty_available <> 0 or wsib.qty_outstanding_allocation <> 0 or wsib.qty_on_hold <> 0
	group by idWHStockItem_sk_fk, wsi.idWarehouse_sk_fk, wsi.idStockItem_sk_fk

	select top 1000 count(*) over (),
		wsi.idWHStockItem_sk, wsib.idWHStockItem_sk, 
		wsi.idWarehouse_sk, wsi.idStockItem_sk, 
		wsi.qty_on_hand, wsib.qty_on_hand
	from 
			(select 
				idWHStockItem_sk, idWarehouse_sk_fk idWarehouse_sk, idStockItem_sk_fk idStockItem_sk, 				
				qty_available + qty_outstanding_allocation + qty_on_hold qty_on_hand
			from Warehouse.stock.dim_wh_stock_item wsi
			where qty_available <> 0 or qty_outstanding_allocation <> 0 or qty_on_hold <> 0) wsi
		full join
			(select 
				idWHStockItem_sk_fk idWHStockItem_sk, 
				sum(qty_available + qty_outstanding_allocation + qty_on_hold) qty_on_hand
			from Warehouse.stock.dim_wh_stock_item_batch
			where qty_available <> 0 or qty_outstanding_allocation <> 0 or qty_on_hold <> 0
			group by idWHStockItem_sk_fk
			having sum(qty_available + qty_outstanding_allocation + qty_on_hold) <> 0) wsib on wsi.idWHStockItem_sk = wsib.idWHStockItem_sk
	where wsi.idWHStockItem_sk is null or wsib.idWHStockItem_sk is null or wsi.qty_on_hand <> wsib.qty_on_hand

-- qty_on_transit

	select top 1000 count(*) over (), idIntransitStockItemBatch_sk, 
		po_t.idWarehouse_sk_fk idWarehouse_sk, isib.idStockItem_sk_fk idStockItem_sk,
		isib.qty_remaining
	-- select *
	from 
			Warehouse.stock.dim_intransit_stock_item_batch isib
		inner join
			Warehouse.po.dim_intersite_transfer it on isib.idIntersiteTransfer_sk_fk = it.idIntersiteTransfer_sk
		inner join
			Warehouse.po.dim_purchase_order po_t on it.idPurchaseOrderT_sk_fk = po_t.idPurchaseOrder_sk
	where qty_remaining > 0

-- qty_open: all (not just 3 months)

	select top 1000 rec.idWarehouse_sk, rl.idStockItem_sk_fk idStockItem_sk, rec.due_date, receipt_status_name, 
		case 
			when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
			when receipt_status_name in ('Received') then qty_received
			else 0 -- Stock_Registered
		end as qty_open
	from
			Warehouse.rec.fact_receipt_line rl
		inner join
			Warehouse.rec.dim_receipt_v	rec on rl.idReceipt_sk_fk = rec.idReceipt_sk
	where receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
		-- and rec.due_date = XXX

	select top 1000 count(*) over (), 
		rec.idWarehouse_sk, rl.idStockItem_sk_fk idStockItem_sk, -- rec.due_date, receipt_status_name, 
		sum(case 
			when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
			when receipt_status_name in ('Received') then qty_received
			else 0 -- Stock_Registered
		end) as qty_open
	from
			Warehouse.rec.fact_receipt_line rl
		inner join
			Warehouse.rec.dim_receipt_v	rec on rl.idReceipt_sk_fk = rec.idReceipt_sk
	where receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
		-- and rec.due_date = XXX
	group by rec.idWarehouse_sk, rl.idStockItem_sk_fk

-- qty_planned: all (not just 3 months)

	select pr.idWarehouse_sk_fk, pr.idStockItem_sk_fk, pr.quantity, c_due.calendar_date
	from
			Warehouse.fm.fact_planned_requirements pr
		inner join
			Warehouse.fm.fact_planned_requirements_wrk prw 
				on pr.idWarehouse_sk_fk = prw.idWarehouse_sk_fk and pr.idStockItem_sk_fk = prw.idStockItem_sk_fk and
					pr.idSupplier_sk_fk = prw.idSupplier_sk_fk and pr.fm_reference = prw.fm_reference 
		left join
			Warehouse.gen.dim_calendar c_due on pr.idCalendarDueDate_sk_fk = c_due.idCalendar_sk
	-- where c_due.calendar_date = XXX

	select pr.idWarehouse_sk_fk idWarehouse_sk, pr.idStockItem_sk_fk idStockItem_sk, sum(pr.quantity) qty_planned
	from
			Warehouse.fm.fact_planned_requirements pr
		inner join
			Warehouse.fm.fact_planned_requirements_wrk prw 
				on pr.idWarehouse_sk_fk = prw.idWarehouse_sk_fk and pr.idStockItem_sk_fk = prw.idStockItem_sk_fk and
					pr.idSupplier_sk_fk = prw.idSupplier_sk_fk and pr.fm_reference = prw.fm_reference 
		left join
			Warehouse.gen.dim_calendar c_due on pr.idCalendarDueDate_sk_fk = c_due.idCalendar_sk
	-- where c_due.calendar_date = XXX
	group by pr.idWarehouse_sk_fk, pr.idStockItem_sk_fk

-- qty_sales_forecast (3 months sales + average per day for days cover)

	select top 1000 fmd.idWarehouse_sk_fk idWarehouse_sk, fmd.idStockItem_sk_fk idStockItem_sk, fmd.qty_sales_forecast, c.calendar_date
	from 
			Warehouse.fm.fact_fm_data fmd
		inner join
			Warehouse.gen.dim_calendar c on fmd.idCalendarFMDate_sk_fk = c.idCalendar_sk
	where c.calendar_date between getutcdate() and dateadd(mm, 3, getutcdate())
	order by idWarehouse_sk, idStockItem_sk, c.calendar_date

	select top 1000 count(*) over (),
		fmd.idWarehouse_sk_fk idWarehouse_sk, fmd.idStockItem_sk_fk idStockItem_sk, sum(fmd.qty_sales_forecast) qty_sales_forecast, 
		datediff(dd, getutcdate(), dateadd(mm, 3, getutcdate())) num_days
	from 
			Warehouse.fm.fact_fm_data fmd
		inner join
			Warehouse.gen.dim_calendar c on fmd.idCalendarFMDate_sk_fk = c.idCalendar_sk
	where c.calendar_date between getutcdate() and dateadd(mm, 3, getutcdate())
		and idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 27503
	group by fmd.idWarehouse_sk_fk, fmd.idStockItem_sk_fk
	order by idWarehouse_sk, idStockItem_sk

	-- 

	select CONVERT(INT, (CONVERT(VARCHAR(8), getutcdate(), 112)))
	select CONVERT(INT, (CONVERT(VARCHAR(8), dateadd(mm, 3, getutcdate()), 112)))
	select CONVERT(INT, (CONVERT(VARCHAR(8), dateadd(mm, 3, getutcdate()), 112))) - CONVERT(INT, (CONVERT(VARCHAR(8), getutcdate(), 112)))

	select top 1000 count(*) over (),
		fmd.idWarehouse_sk_fk idWarehouse_sk, fmd.idStockItem_sk_fk idStockItem_sk, sum(fmd.qty_sales_forecast) qty_sales_forecast, 
		datediff(dd, getutcdate(), dateadd(mm, 3, getutcdate())) num_days
	from Warehouse.fm.fact_fm_data fmd
	where idCalendarFMDate_sk_fk > CONVERT(INT, (CONVERT(VARCHAR(8), getutcdate(), 112))) and idCalendarFMDate_sk_fk <= CONVERT(INT, (CONVERT(VARCHAR(8), dateadd(mm, 3, getutcdate()), 112)))
		-- and idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 27503
	group by fmd.idWarehouse_sk_fk, fmd.idStockItem_sk_fk
	order by idWarehouse_sk, idStockItem_sk

	select top 1000 count(*) over (),
		fmd.idWarehouse_sk_fk idWarehouse_sk, fmd.idStockItem_sk_fk idStockItem_sk, fmd.qty_sales_forecast, idCalendarFMDate_sk_fk
	from Warehouse.fm.fact_fm_data fmd
	where idCalendarFMDate_sk_fk between CONVERT(INT, (CONVERT(VARCHAR(8), getutcdate(), 112))) and CONVERT(INT, (CONVERT(VARCHAR(8), dateadd(mm, 3, getutcdate()), 112)))
		and idWarehouse_sk_fk = 1 and idStockItem_sk_fk = 27503
	order by idWarehouse_sk, idStockItem_sk, idCalendarFMDate_sk_fk

-- qty_issued (3 months issues + average per day for days cover)

	select top 1000 
		oli.idWarehouse_sk_fk idWarehouse_sk, oli.idStockItem_sk_fk idStockItem_sk, oli.qty_stock, cal_i.calendar_date, ole.order_type_erp_f
	from 
			Warehouse.alloc.fact_order_line_erp_issue oli
		left join
			Warehouse.gen.dim_calendar cal_i on oli.idCalendarIssueDate_sk_fk = cal_i.idCalendar_sk
		inner join
			Warehouse.alloc.fact_order_line_erp_v ole on oli.idOrderLineERP_sk_fk = ole.idOrderLineERP_sk 
	where ole.order_type_erp_f = 'R' and oli.cancelled_f = 'N'
		and cal_i.calendar_date between dateadd(dd, -30, getutcdate()) and getutcdate() 
	
	select top 1000 
		oli.idWarehouse_sk_fk idWarehouse_sk, oli.idStockItem_sk_fk idStockItem_sk, sum(oli.qty_stock) qty_issued, 
		datediff(dd, getutcdate(), dateadd(mm, 3, getutcdate())) num_days
	from 
			Warehouse.alloc.fact_order_line_erp_issue oli
		left join
			Warehouse.gen.dim_calendar cal_i on oli.idCalendarIssueDate_sk_fk = cal_i.idCalendar_sk
		inner join
			Warehouse.alloc.fact_order_line_erp_v ole on oli.idOrderLineERP_sk_fk = ole.idOrderLineERP_sk 
	where ole.order_type_erp_f = 'R' and oli.cancelled_f = 'N'
		and cal_i.calendar_date between dateadd(dd, -30, getutcdate()) and getutcdate() 
		and oli.idWarehouse_sk_fk = 1 and oli.idStockItem_sk_fk = 27503
	group by oli.idWarehouse_sk_fk, oli.idStockItem_sk_fk

-----------------------------------------------------
-----------------------------------------------------


select top 1000 count(*) over (), t.idWarehouse_sk, t.idStockItem_sk, 
	ssd.abc, ssd.stockable, ssd.stocked, ssd.final,
	w.warehouse_name, 
	si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
	si.product_id_magento, si.product_family_name, si.packsize, 
	si.base_curve, si.diameter, si.power, si.cylinder, si.axis, si.addition, si.dominance, si.colour,
	si.parameter, si.product_description, si.SKU, si.stock_item_description,

	t.qty_on_hand, t.qty_on_transit, t.qty_open, t.qty_planned, 
	t.qty_sales_forecast, t.num_days_forecast, 
	t.qty_issued, t.num_days_issue
from
		(select wsib.idWarehouse_sk, wsib.idStockItem_sk, 
			wsib.qty_on_hand, isnull(isib.qty_on_transit, 0) qty_on_transit, isnull(recl.qty_open, 0) qty_open, isnull(preq.qty_planned, 0) qty_planned, 
			isnull(fm.qty_sales_forecast, 0) qty_sales_forecast, isnull(fm.num_days_forecast, 0) num_days_forecast, 
			isnull(oli.qty_issued, 0) qty_issued, isnull(oli.num_days_issue, 0) num_days_issue
		from
				(select 
					wsib.idWHStockItem_sk_fk idWHStockItem_sk, 
					wsi.idWarehouse_sk_fk idWarehouse_sk, wsi.idStockItem_sk_fk idStockItem_sk, 
					sum(wsib.qty_available + wsib.qty_outstanding_allocation + wsib.qty_on_hold) qty_on_hand
				from 
						Warehouse.stock.dim_wh_stock_item_batch wsib
					inner join	
						Warehouse.stock.dim_wh_stock_item wsi on wsib.idWHStockItem_sk_fk = wsi.idWHStockItem_sk
				where wsib.qty_available <> 0 or wsib.qty_outstanding_allocation <> 0 or wsib.qty_on_hold <> 0
				group by idWHStockItem_sk_fk, wsi.idWarehouse_sk_fk, wsi.idStockItem_sk_fk) wsib
			left join
				(select 
					po_t.idWarehouse_sk_fk idWarehouse_sk, isib.idStockItem_sk_fk idStockItem_sk,
					sum(isib.qty_remaining) qty_on_transit
				from 
						Warehouse.stock.dim_intransit_stock_item_batch isib
					inner join
						Warehouse.po.dim_intersite_transfer it on isib.idIntersiteTransfer_sk_fk = it.idIntersiteTransfer_sk
					inner join
						Warehouse.po.dim_purchase_order po_t on it.idPurchaseOrderT_sk_fk = po_t.idPurchaseOrder_sk
				where qty_remaining > 0
				group by po_t.idWarehouse_sk_fk, isib.idStockItem_sk_fk) isib on wsib.idWarehouse_sk = isib.idWarehouse_sk and wsib.idStockItem_sk = isib.idStockItem_sk
			left join
				(select 
					rec.idWarehouse_sk, rl.idStockItem_sk_fk idStockItem_sk, 
					sum(case 
						when receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned') then qty_ordered
						when receipt_status_name in ('Received') then qty_received
						else 0 -- Stock_Registered
					end) as qty_open
				from
						Warehouse.rec.fact_receipt_line rl
					inner join
						Warehouse.rec.dim_receipt_v	rec on rl.idReceipt_sk_fk = rec.idReceipt_sk
				where receipt_status_name in ('Open', 'In_Transit', 'Locked', 'Arrived', 'Planned', 'Received') -- Needs to exclude Intransit (as they will apear in Intransit) - What about Locked
					-- and rec.due_date = XXX
				group by rec.idWarehouse_sk, rl.idStockItem_sk_fk) recl on wsib.idWarehouse_sk = recl.idWarehouse_sk and wsib.idStockItem_sk = recl.idStockItem_sk
			left join
				(select pr.idWarehouse_sk_fk idWarehouse_sk, pr.idStockItem_sk_fk idStockItem_sk, sum(pr.quantity) qty_planned
				from
						Warehouse.fm.fact_planned_requirements pr
					inner join
						Warehouse.fm.fact_planned_requirements_wrk prw 
							on pr.idWarehouse_sk_fk = prw.idWarehouse_sk_fk and pr.idStockItem_sk_fk = prw.idStockItem_sk_fk and
								pr.idSupplier_sk_fk = prw.idSupplier_sk_fk and pr.fm_reference = prw.fm_reference 
					left join
						Warehouse.gen.dim_calendar c_due on pr.idCalendarDueDate_sk_fk = c_due.idCalendar_sk
				-- where c_due.calendar_date = XXX
				group by pr.idWarehouse_sk_fk, pr.idStockItem_sk_fk) preq on wsib.idWarehouse_sk = preq.idWarehouse_sk and wsib.idStockItem_sk = preq.idStockItem_sk
			left join
				(select 
					fmd.idWarehouse_sk_fk idWarehouse_sk, fmd.idStockItem_sk_fk idStockItem_sk, sum(fmd.qty_sales_forecast) qty_sales_forecast, 
					datediff(dd, getutcdate(), dateadd(mm, 3, getutcdate())) num_days_forecast
				from 
						Warehouse.fm.fact_fm_data fmd
					inner join
						Warehouse.gen.dim_calendar c on fmd.idCalendarFMDate_sk_fk = c.idCalendar_sk
				where c.calendar_date between getutcdate() and dateadd(mm, 3, getutcdate())
				group by fmd.idWarehouse_sk_fk, fmd.idStockItem_sk_fk) fm on wsib.idWarehouse_sk = fm.idWarehouse_sk and wsib.idStockItem_sk = fm.idStockItem_sk
			left join
				(select 
					oli.idWarehouse_sk_fk idWarehouse_sk, oli.idStockItem_sk_fk idStockItem_sk, sum(oli.qty_stock) qty_issued, 
					datediff(dd, getutcdate(), dateadd(mm, 3, getutcdate())) num_days_issue
				from 
						Warehouse.alloc.fact_order_line_erp_issue oli
					left join
						Warehouse.gen.dim_calendar cal_i on oli.idCalendarIssueDate_sk_fk = cal_i.idCalendar_sk
					inner join
						Warehouse.alloc.fact_order_line_erp_v ole on oli.idOrderLineERP_sk_fk = ole.idOrderLineERP_sk 
				where ole.order_type_erp_f = 'R' and oli.cancelled_f = 'N'
					and cal_i.calendar_date between dateadd(dd, -30, getutcdate()) and getutcdate() 
				group by oli.idWarehouse_sk_fk, oli.idStockItem_sk_fk) oli on wsib.idWarehouse_sk = oli.idWarehouse_sk and wsib.idStockItem_sk = oli.idStockItem_sk) t
	inner join
		Warehouse.gen.dim_warehouse_v w on t.idWarehouse_sk = w.idWarehouse_sk
	inner join
		Warehouse.prod.dim_stock_item_v si on t.idStockItem_sk = si.idStockItem_sk
	left join
		Warehouse.fm.dim_wh_stock_item_static_data_v ssd on t.idWarehouse_sk = ssd.idWarehouse_sk and t.idStockItem_sk = ssd.idStockItem_sk

