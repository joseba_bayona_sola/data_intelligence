
select oh.order_id_bk, oh.order_no, 
	oh.order_date, ohm.createdDate, -- invoice_date, 
	shipment_date, shipping_days,
	oh.website,  oh.customer_id, oh.country_code_ship, 
	oh.order_status_name, oh.order_status_magento_name, oh.order_stage_name,
	-- local_subtotal, local_shipping, local_discount, local_store_credit_used,
	oh.local_total_inc_vat, oh.num_lines, 
	
	ohm._type, ohm.orderStatus, ohm.allocationStrategy, ohm.allocationStatus, 
	ohm.orderLinesMagento, ohm.orderLinesDeduped, ohm.idETLBatchRun
from 
		Warehouse.sales.dim_order_header_v oh
	left join 
		Landing.mend.order_order_aud ohm on oh.order_id_bk = ohm.magentoOrderID
where oh.order_date between '2018-02-20' and '2018-02-21'
	--and ohm.magentoOrderID is null -- and (order_status_name <> 'CANCEL' and order_status_magento_name not in ('canceled', 'pending_payment') and order_stage_name <> 'ORDER')
	and ohm.magentoOrderID is not null
	-- and order_id_bk in (6838235, 6838236, 6838237, 6838238)
order by oh.order_date

select oh.order_id_bk, oh.order_no, 
	oh.order_date, s.createdDate_order, -- invoice_date, 
	shipment_date, shipping_days, s.expectedShippingDate, s.snap_timestamp,
	
	oh.website,  oh.customer_id, oh.country_code_ship, 
	oh.order_status_name, oh.order_status_magento_name, oh.order_stage_name,
	-- local_subtotal, local_shipping, local_discount, local_store_credit_used,
	oh.local_total_inc_vat, oh.num_lines
from 
		Warehouse.sales.dim_order_header_v oh
	left join 
		Landing.mend.gen_ship_customershipment_v s on oh.order_id_bk = s.magentoOrderID_int
where oh.order_date between '2018-04-02' and '2018-04-09'
	-- and order_id_bk in (6838235, 6838236, 6838237, 6838238, 6838269)
	and convert(date, shipment_date) <> convert(date, snap_timestamp)
	-- and convert(date, shipment_date) <> convert(date, expectedShippingDate)
-- order by oh.order_date
order by s.snap_timestamp

------------------------------------------------------------


select order_line_id_bk, order_id_bk, order_no, 
	order_date, 
	product_id_magento, product_family_name, sku_magento, qty_unit
from Warehouse.sales.fact_order_line_v 
where order_id_bk in (6838235, 6838236, 6838237, 6838238, 6838269)
order by order_date

select orderid, magentoOrderID_int, incrementID, createdDate, 
	_type, orderStatus, allocationStrategy, allocationStatus, 
	orderLinesMagento, orderLinesDeduped, 
	subtotal, grandTotal
from Landing.mend.gen_order_order_v
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int

select orderid, orderlinemagentoid, productid,
	magentoOrderID_int, incrementID, createdDate, 
	lineAdded, deduplicate, fulfilled,
	magentoProductID_int, name, sku, 
	quantityOrdered, quantityAllocated, quantityIssued, 
	basePrice, baseRowPrice, 
	allocated
from Landing.mend.gen_order_orderlinemagento_v
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int

select top 1000 ma.orderid, ma.orderlineid, ma.orderlinestockallocationtransactionid, ma.batchstockallocationid, ma.magentoallocationid,
	ma.warehousestockitemid, ma.warehousestockitembatchid,
	ma.magentoOrderID_int, ma.incrementID, ma.wh_name,
		wsi.magentoProductID_int, wsi.name, wsi.sku, wsib.batch_id, wsib.productUnitCost,
	ma.quantity,
	ma.transactionID, ma.timestamp, -- ma.allocationType, ma.recordType, 
	ma.allocatedUnits, ma.allocatedStockQuantity, ma.issuedQuantity, ma.packSize 
	-- ma.stockAllocated, ma.cancelled, ma.issuedDateTime
from 
		Landing.mend.gen_all_magentoallocation_v ma
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on ma.warehousestockitemid = wsi.warehousestockitemid
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on ma.warehousestockitembatchid = wsib.warehousestockitembatchid
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int



select orderid, orderlineid, productid, 
	magentoOrderID_int, incrementID, createdDate, 
	status, packsOrdered, packsAllocated, packsShipped, 
	magentoProductID_int, name, sku, 
	quantityOrdered, quantityAllocated, quantityIssued, 
	basePrice, baseRowPrice, 
	allocated
from Landing.mend.gen_order_orderline_v
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int

select top 1000 sat.orderid, sat.orderlineid, sat.orderlinestockallocationtransactionid, sat.batchstockallocationid,
	sat.warehousestockitemid, sat.warehousestockitembatchid,
	sat.magentoOrderID_int, sat.incrementID, sat.wh_name,
		wsi.magentoProductID_int, wsi.name, wsi.sku, wsib.batch_id, wsib.productUnitCost,
	sat.transactionID, sat.timestamp, sat.allocationType, sat.recordType, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
	sat.stockAllocated, sat.cancelled, sat.issuedDateTime
from 
		Landing.mend.gen_all_stockallocationtransaction_v sat
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on sat.warehousestockitemid = wsi.warehousestockitemid
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on sat.warehousestockitembatchid = wsib.warehousestockitembatchid
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int


select top 1000 sat.orderid, sat.orderlineid, sat.orderlinestockallocationtransactionid, sat.batchstockallocationid,
	sat.warehousestockitemid, sat.warehousestockitembatchid,
	sat.magentoOrderID_int, sat.incrementID, sat.wh_name,
		wsi.magentoProductID_int, wsi.name, wsi.sku, wsib.batch_id, wsib.productUnitCost,
	sat.transactionID, sat.timestamp, sat.allocationType, sat.recordType, 
	sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
	sat.stockAllocated, sat.cancelled, sat.issuedDateTime, 

	bsa.issueid, bsa.issuedquantity_issue, 
	bsa.fullyissued_alloc, bsa.cancelled, bsa.allocatedquantity_alloc, bsa.allocatedunits, bsa.issuedquantity_alloc
from 
		Landing.mend.gen_all_stockallocationtransaction_v sat
	inner join
		Landing.mend.gen_wh_warehousestockitem_v wsi on sat.warehousestockitemid = wsi.warehousestockitemid
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_v wsib on sat.warehousestockitembatchid = wsib.warehousestockitembatchid
	inner join
		Landing.mend.gen_wh_warehousestockitembatch_issue_alloc_v bsa on sat.batchstockallocationid = bsa.batchstockallocationid

where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int


----------------------------------------

select customershipmentid, orderid, 
	magentoOrderID_int, incrementID, createdDate_order, 
	shipmentNumber, status, notify, fullyShipped, syncedToMagento, 
	dispatchDate, expectedShippingDate, expectedDeliveryDate, 
	snap_timestamp,
	createdDate, changedDate
from Landing.mend.gen_ship_customershipment_v
where magentoOrderID_int in (6838235, 6838236, 6838237, 6838238, 6838269)
order by magentoOrderID_int

	select s.*
	from 
			Landing.mend.ship_customershipment_aud s
		inner join
			Landing.mend.ship_customershipment_order_aud so on s.id = so.customershipmentid
		inner join
			Landing.mend.gen_order_order_v o on so.orderid = o.orderid
	where o.magentoOrderID_int in (6838235, 6838236, 6838237, 6838238)
