
---------------------- NOTES ----------------------

	-- whship_shipment
		-- supplier_id can be NULL / can be MANY
		-- Shipmet_ID meaning on name (IP (Back_To_Back) - P (Back_To_Back) - R (Stock) - TP (Intersite))
		-- MIX of WHSHIP with PO (only through orderNumber) and with REC (FK + receiptID) being multiple REL
		-- shipment_ID not unique: Shipment with MANY Receipt (receiptID) - MANY PO (orderNumber) - MANY suppliers ?? - MANY warehouse
		-- receiptID not unique:
		-- shipmentType + receiptNumber meaning
		-- Different Dates meaning
		-- totalItemPrice + Rest of Price meaning

		-- WHY REL to WH SI Batch - Redundant at also OL level?

	-- whship_shipment
	select count(*) over (partition by receipt_ID) num_rep,
		receiptid, warehouseid, supplier_id, -- warehousestockitembatchid,
		shipment_ID, orderNumber, receipt_ID, receiptNumber, invoiceRef,
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		auditComment,
		createdDate
	from Landing.mend.gen_whship_receipt_v 
	-- where supplier_id is null
	-- where supplierID is null
	-- where shipment_id in ('P00027841', 'P00032210', 'IP00018777', 'IP00030609')
	-- where receiptID = 'P00005008-1'
	-- where orderNumber = 'P00019985'
	-- where status = 'Stock_Registered' -- Open - Planned - In_Transit - Delivered - Arrived - Stock_Registered // Processing - Locked - Cancelled
	order by num_rep desc, shipment_ID, receipt_ID, createddate desc
	

	select top 1000 id, orderNumber, receiptid, ShipmentType, arrivedDate, confirmedDate, stockRegisteredDate, createdDate --, supplieradviceref
	from Landing.mend.whship_receipt_aud
	order by createddate desc

	select count(*)
	from Landing.mend.whship_receipt

	select count(*), count(distinct receipt_ID), count(distinct shipment_ID), count(distinct orderNumber)
	from Landing.mend.gen_whship_receipt_v


		select top 1000 *, 
			count(*) over (partition by shipmentid) rep_father,
			count(*) over (partition by warehouseid) rep_son
		from Landing.mend.whship_shipment_warehouse_aud
		order by rep_son desc, rep_father desc

		select top 1000 *, 
			count(*) over (partition by shipmentid) rep_father,
			count(*) over (partition by supplierid) rep_son
		from Landing.mend.whship_shipment_supplier_aud
		order by rep_son desc, rep_father desc



		select status, count(*)
		from Landing.mend.gen_whship_receipt_v
		-- where stockRegisteredDate is null
		group by status
		order by status

		select shipmentType, count(*)
		from Landing.mend.gen_whship_receipt_v
		group by shipmentType
		order by shipmentType

		-- 

		select code, warehouse_name, count(*), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		group by code, warehouse_name
		order by code, warehouse_name

		select supplierID, supplierName, count(*), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		group by supplierID, supplierName
		order by supplierID, supplierName


		select shipment_ID, count(*), count(distinct supplier_id), count(distinct warehouseid), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		-- where status <> 'Cancelled'
		group by shipment_ID
		order by count(distinct supplier_id) desc, count(distinct warehouseid) desc, shipment_ID desc

		select shipment_ID, count(*), count(distinct receiptID), count(distinct orderNumber), count(distinct supplier_id), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		-- where status <> 'Cancelled'
		-- where orderNumber not in ('Empty', 'Multiple')
		group by shipment_ID
		-- having count(*) > 1
		order by count(distinct supplier_id) desc, count(distinct receiptID) desc, count(distinct orderNumber) desc

			select shipmentID, count(*), count(distinct orderNumber), count(distinct receiptID), sum(totalItemPrice)
			from Landing.mend.gen_whship_receipt_v
			-- where status <> 'Cancelled'
			group by shipmentID
			having count(*) > 1
			order by count(*) desc, count(distinct orderNumber) desc, count(distinct receiptID) desc

		select receipt_ID, count(*), count(distinct shipment_ID), count(distinct ordernumber), count(distinct supplier_id), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		-- where status <> 'Cancelled'
		group by receipt_ID
		order by count(distinct shipment_ID) desc, count(*) desc, count(distinct supplier_id) desc, count(distinct ordernumber) desc, receipt_ID

		select orderNumber, count(*), count(distinct shipment_ID), sum(totalItemPrice)
		from Landing.mend.gen_whship_receipt_v
		-- where status <> 'Cancelled'
		group by orderNumber
		order by count(distinct shipment_ID) desc, count(*) desc


---------------------- NOTES ----------------------
	-- Receipt LIne Headers without Receipt

	select receiptlineheaderid, receiptid, warehouseid, supplier_id, productfamilyid, packsizeid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, totalItemPrice, 
		createdDate,
		totalquantityordered, totalquantityaccepted, 
		unitcost, totalcostordered, totalcostaccepted
	from Landing.mend.gen_whship_receiptlineheader_v
	-- where productfamilyid is null and (status <> 'Cancelled' or totalItemPrice <> 0)
	order by createdDate desc


	select count(*)
	from Landing.mend.whship_receiptlineheader

	select count(*), count(distinct shipment_ID), count(distinct receipt_ID), count(distinct orderNumber)
	from Landing.mend.gen_whship_receiptlineheader_v
	where productfamilyid is null 



---------------------- NOTES ----------------------

	-- whship_shipmentorderline

	-- whship_shipmentorderline

	select top 1000 shipmentorderlineid, shipmentid, warehouseid, supplier_id, purchaseorderlineid, snapreceiptlineid, warehousestockitembatchid,
		shipment_ID, orderNumber, receiptID, receiptNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, 
		createdDate,
		
		line, stockItem,
		status_ol, syncStatus, processed, 
		quantityOrdered, quantityReceived, quantityAccepted, quantityRejected, quantityReturned, 
		unitCost, 
		quantityOrdered * unitCost, 
		sum(quantityOrdered * unitCost) over (partition by shipmentid)
	from Landing.mend.gen_whship_shipmentorderline_v
	-- where shipmentid is null
	-- where shipmentorderlineid is null
	-- where purchaseorderlineid is null
	-- where snapreceiptlineid is null
	-- where warehousestockitembatchid is null
	-- where shipment_id in ('P00027841', 'P00032210', 'IP00018777')
	order by createddate desc, line

	select top 1000 receiptlineid, receiptid, warehouseid, supplier_id, purchaseorderlineid, warehousestockitembatchid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, totalItemPrice, 
		createdDate, 
		line, stockitem, status_rl, syncstatus, syncresolution,
		unitCost, quantityordered, quantityreceived, quantityaccepted, quantityreturned, quantityrejected, 
		created
	from Landing.mend.gen_whship_receiptline_1_v 
	-- where purchaseorderlineid is null
	where warehousestockitembatchid is not null
	order by createdDate desc, line

	select count(*)
	from Landing.mend.whship_receiptline

	select count(*)
	from Landing.mend.whship_receiptline_aud

	select count(*), count(distinct receiptlineid)
	from Landing.mend.gen_whship_receiptline_1_v
	-- where purchaseorderlineid is null
	-- where warehousestockitembatchid is null

	select count(*), count(distinct receiptlineid)
	from Landing.mend.gen_whship_receiptline_2_v
	-- where purchaseorderlineid is null
	-- where warehousestockitembatchid is null


			select top 10000 status, status_rl, syncstatus, syncresolution, count(*)
			from Landing.mend.gen_whship_receiptline_1_v
			where warehousestockitembatchid is null
			group by status, status_rl, syncstatus, syncresolution
			order by count(*) desc, status, status_rl, syncstatus, syncresolution

		select top 1000 *, 
			count(*) over (partition by receiptlineid) rep_father,
			count(*) over (partition by receiptid) rep_son
		from Landing.mend.whship_receiptline_receipt
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by receiptlineid) rep_father,
			count(*) over (partition by purchaseorderlineid) rep_son
		from Landing.mend.whship_receiptline_purchaseorderline
		order by rep_father desc, rep_son desc

		select top 1000 *, 
			count(*) over (partition by receiptlineid) rep_father,
			count(*) over (partition by warehousestockitembatchid) rep_son
		from Landing.mend.whship_warehousestockitembatch_receiptline
		order by rep_father desc, rep_son desc, shipmentorderlineid, warehousestockitembatchid

		select status_rl, count(*)
		from Landing.mend.gen_whship_receiptline_1_v
		group by status_rl
		order by status_rl

		select syncStatus, count(*)
		from Landing.mend.gen_whship_receiptline_1_v
		group by syncStatus
		order by syncStatus

		select syncResolution, count(*)
		from Landing.mend.gen_whship_receiptline_1_v
		group by syncResolution
		order by syncResolution

		select processed, count(*)
		from Landing.mend.gen_whship_receiptline_1_v
		group by processed
		order by processed


-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------

	-- gen_whship_receiptlineheader_v (PF - Size)
	select top 1000 count(*) over (),
		rlh.*, pfp.magentoProductID, pfp.description, pfp.size
	from 
			Landing.mend.gen_whship_receiptlineheader_v rlh
		left join
			Landing.mend.gen_prod_productfamilypacksize_v pfp on rlh.packsizeid = pfp.packsizeid
	order by createdDate desc

	-- gen_whship_receiptline_1_v (purchaseorderlineid - warehousestockitembatchid)
	select top 1000 count(*) over (), 
		rl.*, wsib.magentoProductID, wsib.stockitemdescription, wsib.id_string, wsib.batch_id, wsib.receivedquantity, wsib.arrivedDate, wsib.stockRegisteredDate, wsib.productUnitCost
	from 
			Landing.mend.gen_whship_receiptline_1_v rl
		left join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on rl.warehousestockitembatchid = wsib.warehousestockitembatchid
	where rl.warehousestockitembatchid is not null
	order by createdDate desc, line

	select top 1000 *
	from Landing.mend.gen_wh_warehousestockitembatch_v

---------------------------------------------------------------------------------------------

select rl.*
from
		(select receiptlineid
		from Landing.mend.whship_receiptline_receipt
		except
		select receiptlineid
		from Landing.mend.whship_receiptline_receiptlineheader) t
	inner join
		Landing.mend.gen_whship_receiptline_1_v rl on t.receiptlineid = rl.receiptlineid
order by createdDate desc

select distinct rl.receiptlineid, rl.receiptid, rl.receipt_id
from
		(select receiptlineid
		from Landing.mend.whship_receiptline_receipt
		except
		select receiptlineid
		from Landing.mend.whship_receiptline_receiptlineheader) t
	inner join
		Landing.mend.gen_whship_receiptline_1_v rl on t.receiptlineid = rl.receiptlineid

select *
from Landing.mend.gen_whship_receipt_v
where receipt_id = 'TP00037008-1'

select *
from Landing.mend.gen_whship_receiptlineheader_v
where receipt_id = 'TP00037008-1'

select *
from Landing.mend.gen_whship_receiptline_1_v
where receipt_id = 'TP00037008-1'

select *
from Landing.mend.gen_whship_receiptline_2_v
where receipt_id = 'TP00037008-1'


select rl.*
from
		(select receiptlineid
		from Landing.mend.whship_receiptline_receipt
		except
		select receiptlineid
		from Landing.mend.whship_receiptline_receiptlineheader) t
	inner join
		Landing.mend.whship_receiptline_receiptlineheader_aud rl on t.receiptlineid = rl.receiptlineid