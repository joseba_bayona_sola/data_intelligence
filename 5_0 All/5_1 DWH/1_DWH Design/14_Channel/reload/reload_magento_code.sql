
select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where order_source = 'M' 

select channel_name_bk, count(*)
from #oh_channel
group by channel_name_bk
order by channel_name_bk	

select marketing_channel_name_bk, count(*)
from #oh_marketing_channel
group by marketing_channel_name_bk
order by marketing_channel_name_bk	

	select oh.order_id_bk, 
		oh.channel_name_bk, r.channel_name_bk, oh.marketing_channel_name_bk, r.marketing_channel_name_bk 
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			#oh_reload_magento r on oh.order_id_bk = r.order_id_bk
	where (oh.channel_name_bk is not null and oh.marketing_channel_name_bk is not null)
		-- and oh.channel_name_bk <> r.channel_name_bk
		and oh.marketing_channel_name_bk <> r.marketing_channel_name_bk

----------------------------------------------------------------------------------

	create table #oh_reload_magento(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		marketing_channel_name_bk		varchar(50))

	insert into #oh_reload_magento(order_id_bk, channel_name_bk, marketing_channel_name_bk)

		select order_id_bk, channel_name_bk, marketing_channel_name_bk
		from Landing.aux.sales_dim_order_header_aud
		where order_source = 'M' -- and channel_name_bk is null and marketing_channel_name_bk is null

	create table #oh_couponcode(
		order_id_bk						bigint NOT NULL, 
		coupon_id_bk					int,
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255), 
		channel							varchar(255))

		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							#oh_reload_magento oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) = 0 -- and len(o.applied_rule_ids) < 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
			order by o.applied_rule_ids, o.coupon_code

		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							#oh_reload_magento oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) <> 0 -- and len(o.applied_rule_ids) >= 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code
			order by o.applied_rule_ids, o.coupon_code


	create table #oh_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		ga_channel						varchar(100),
		telesales_f						char(1))

	insert into #oh_channel(order_id_bk, channel_name_bk, 
		ga_medium, ga_source, ga_channel, telesales_f)

		select order_id_bk, 
			case 
				when (ga_medium is null and telesales_f = 'N') then 'Non-GA'
				when (ga_medium is null and telesales_f = 'Y') then 'Non-GA Telesales'
				when (channel_name is not null) then channel_name
				else null
			end channel_name_bk,
			ga_medium, ga_source, null ga_channel, telesales_f
		from
			(select oh.order_id_bk, mc.channel_name,
				ga.medium ga_medium, ga.source ga_source, o.affilCode, 
				case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f 
			from 
					#oh_reload_magento oh
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
				left join
					Landing.mag.ga_entity_transaction_data_aud ga on o.increment_id = ga.transactionId
				left join
					Landing.map.sales_ch_ga_medium_ch mc on ga.medium = mc.ga_medium) t
		order by ga_medium, telesales_f, ga_source

	create table #oh_marketing_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		marketing_channel_name_bk		varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		affilCode						varchar(255),
		cc_channel						varchar(255),
		telesales_f						char(1), 
		sms_auto_f						char(1),
		mutuelles_f						char(1))

	insert into #oh_marketing_channel(order_id_bk, channel_name_bk,	
		ga_medium, ga_source, affilCode, cc_channel, telesales_f, sms_auto_f, mutuelles_f)

		select oh.order_id_bk, c.channel_name_bk, 
			c.ga_medium, c.ga_source, o.affilCode, 
			cc.channel cc_channel,
			case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f, 
			null sms_auto_f, 
			case when (mut.order_id_bk is not null) then 'A' else 'N' end mutuelles_f 
		from 
				#oh_reload_magento oh
			inner join
				#oh_channel c on oh.order_id_bk = c.order_id_bk
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				(select *
				from #oh_couponcode
				where channel in 
					(select cc_channel
					from Landing.map.sales_ch_cc_channel_mch)) cc on oh.order_id_bk = cc.order_id_bk
			left join
				(select oh.entity_id order_id_bk, 
					oh.mutual_amount, oh.mutual_quotation_id, 
					mq.mutual_id, m.code, mq.type_id, mq.is_online, mq.status_id, mq.amount
				from 
						Landing.mag.sales_flat_order_aud oh
					inner join
						Landing.mag.mutual_quotation_aud mq on oh.mutual_quotation_id = mq.quotation_id
					inner join 
						Landing.mag.mutual_aud m on mq.mutual_id = m.mutual_id
				where mutual_amount is not null) mut on oh.order_id_bk = mut.order_id_bk


		-- Mutuelles
		update #oh_marketing_channel
		set marketing_channel_name_bk = 'Mututelles Auto'
		where marketing_channel_name_bk is null and mutuelles_f = 'A'

		-- SMS Automatic

		-- Coupon Code
		update mc
		set 
			mc.marketing_channel_name_bk = ccmch.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_cc_channel_mch ccmch on mc.cc_channel = ccmch.cc_channel
		where mc.marketing_channel_name_bk is null

		-- Customer Service
		update #oh_marketing_channel
		set marketing_channel_name_bk = 'Customer Service'
		where marketing_channel_name_bk is null and telesales_f = 'Y'

		-- BSC: Exact affilCode Match
		update mc
		set 
			mc.marketing_channel_name_bk = ac.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_mch ac on mc.affilCode = ac.affil_code
		where mc.marketing_channel_name_bk is null

		-- BSC: Like affilCode Match - adwords-bk
		update mc
		set 
			mc.marketing_channel_name_bk = acl.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
		where mc.marketing_channel_name_bk is null

		-- BSC: Like affilCode Match - rest
		update mc
		set 
			mc.marketing_channel_name_bk = acl.marketing_channel_name
		from
				#oh_marketing_channel mc
			inner join
				Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like <> 'adwords-br'
		where mc.marketing_channel_name_bk is null

		-- GA: Email
		update mc
		set 
			mc.marketing_channel_name_bk = isnull(gas.marketing_channel_name, 'Email Marketing')
		from
				#oh_marketing_channel mc
			left join
				Landing.map.sales_ch_ga_source_mch gas on mc.ga_source = gas.ga_source
		where mc.marketing_channel_name_bk is null and mc.channel_name_bk = 'Email'

		-- GA
		update #oh_marketing_channel
		set 
			marketing_channel_name_bk = case 
					when (channel_name_bk = 'Paid Search') then 'Paid Search Non Branded'
					when (channel_name_bk = 'SMS') then 'SMS Url'
					else channel_name_bk
				end
		where marketing_channel_name_bk is null and channel_name_bk is not null

	merge #oh_reload_magento trg
	using #oh_channel src
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.channel_name_bk = src.channel_name_bk;

	merge #oh_reload_magento trg
	using #oh_marketing_channel src
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.marketing_channel_name_bk = src.marketing_channel_name_bk;



	-- UPDATE sales_dim_order_header_aud
	merge Landing.aux.sales_dim_order_header_aud trg
	using #oh_reload_magento src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.channel_name_bk = src.channel_name_bk, trg.marketing_channel_name_bk = src.marketing_channel_name_bk;
	
	-- UPDATE dim_order_header
	merge Warehouse.sales.dim_order_header trg
	using
		(select oh.order_id_bk, 
			isnull(ch.idChannel_sk, -1) idChannel_sk, oh.channel_name_bk, 
			isnull(mch.idMarketingChannel_sk, -1) idMarketingChannel_sk, oh.marketing_channel_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			left join
				Warehouse.sales.dim_channel ch on oh.channel_name_bk = ch.channel_name_bk
			left join
				Warehouse.sales.dim_marketing_channel mch on oh.marketing_channel_name_bk = mch.marketing_channel_name_bk
		where order_source = 'M') src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.idChannel_sk_fk = src.idChannel_sk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk;

	-- UPDATE fact_activity_sales
	merge Warehouse.act.fact_activity_sales trg
	using
		(select oh.order_id_bk, 
			isnull(ch.idChannel_sk, -1) idChannel_sk, oh.channel_name_bk, 
			isnull(mch.idMarketingChannel_sk, -1) idMarketingChannel_sk, oh.marketing_channel_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			left join
				Warehouse.sales.dim_channel ch on oh.channel_name_bk = ch.channel_name_bk
			left join
				Warehouse.sales.dim_marketing_channel mch on oh.marketing_channel_name_bk = mch.marketing_channel_name_bk
		where order_source = 'M') src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.idChannel_sk_fk = src.idChannel_sk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk;	
		

	drop table #oh_reload_magento

	drop table #oh_couponcode

	drop table #oh_channel

	drop table #oh_marketing_channel