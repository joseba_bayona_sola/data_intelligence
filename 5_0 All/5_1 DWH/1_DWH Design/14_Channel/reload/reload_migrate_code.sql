
select top 1000 *
from Landing.aux.sales_dim_order_header_aud
where order_source = 'P' 

	
	create table #oh_reload_migrate(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		marketing_channel_name_bk		varchar(50))

	insert into #oh_reload_migrate(order_id_bk, channel_name_bk, marketing_channel_name_bk)

		select order_id_bk, 'Migration CH' channel_name_bk, isnull(mc.marketing_channel_name, 'Migration CH') marketing_channel_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				Landing.migra.sales_flat_order_migrate_aud o on oh.order_id_bk = o.entity_id
			left join
				Landing.map.sales_ch_migrate_channel_mch mc on o.channel = mc.channel		
		where oh.order_source = 'P' 

		select top 1000 channel_name_bk, marketing_channel_name_bk, count(*)
		from #oh_reload_migrate
		group by channel_name_bk, marketing_channel_name_bk
		order by channel_name_bk, marketing_channel_name_bk


	-- UPDATE sales_dim_order_header_aud
	merge Landing.aux.sales_dim_order_header_aud trg
	using #oh_reload_migrate src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.channel_name_bk = src.channel_name_bk, trg.marketing_channel_name_bk = src.marketing_channel_name_bk;
	
	-- UPDATE dim_order_header
	merge Warehouse.sales.dim_order_header trg
	using
		(select oh.order_id_bk, ch.idChannel_sk, oh.channel_name_bk, mch.idMarketingChannel_sk, oh.marketing_channel_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				Warehouse.sales.dim_channel ch on oh.channel_name_bk = ch.channel_name_bk
			inner join
				Warehouse.sales.dim_marketing_channel mch on oh.marketing_channel_name_bk = mch.marketing_channel_name_bk
		where order_source = 'P') src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.idChannel_sk_fk = src.idChannel_sk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk;

	-- UPDATE fact_activity_sales
	merge Warehouse.act.fact_activity_sales trg
	using
		(select oh.order_id_bk, ch.idChannel_sk, oh.channel_name_bk, mch.idMarketingChannel_sk, oh.marketing_channel_name_bk
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				Warehouse.sales.dim_channel ch on oh.channel_name_bk = ch.channel_name_bk
			inner join
				Warehouse.sales.dim_marketing_channel mch on oh.marketing_channel_name_bk = mch.marketing_channel_name_bk
		where order_source = 'P') src 
	on trg.order_id_bk = src.order_id_bk
	when matched then
		update set
			trg.idChannel_sk_fk = src.idChannel_sk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk;	
		

	drop table #oh_reload_migrate
