
select *
from Warehouse.sales.dim_channel

select *
from Warehouse.sales.dim_marketing_channel

select top 1000 channel_name_bk, marketing_channel_name_bk, count(*)
from Landing.aux.sales_dim_order_header_aud
where order_source = 'M' -- H - P - M
group by channel_name_bk, marketing_channel_name_bk
order by channel_name_bk, marketing_channel_name_bk

select top 1000 idChannel_sk_fk, idMarketingChannel_sk_fk, count(*)
from Warehouse.sales.dim_order_header
where order_source = 'M' -- H - P - M
group by idChannel_sk_fk, idMarketingChannel_sk_fk
order by idChannel_sk_fk, idMarketingChannel_sk_fk

select top 1000 idChannel_sk_fk, idMarketingChannel_sk_fk, count(*)
from Warehouse.act.fact_activity_sales
group by idChannel_sk_fk, idMarketingChannel_sk_fk
order by idChannel_sk_fk, idMarketingChannel_sk_fk


select top 1000 *
from Warehouse.sales.dim_order_header

select top 1000 *
from Warehouse.act.fact_activity_sales