
		select channel_name_bk, count(*)
		from DW_GetLenses_jbs.dbo.oh_marketing_channel
		group by channel_name_bk
		order by channel_name_bk

		select marketing_channel_name_bk, count(*)
		from DW_GetLenses_jbs.dbo.oh_marketing_channel
		group by marketing_channel_name_bk
		order by marketing_channel_name_bk

		select channel_name_bk, marketing_channel_name_bk, count(*)
		from DW_GetLenses_jbs.dbo.oh_marketing_channel
		group by channel_name_bk, marketing_channel_name_bk
		order by channel_name_bk, marketing_channel_name_bk

	select *
	from DW_GetLenses_jbs.dbo.oh_marketing_channel
	where channel_name_bk = 'Paid Search' and marketing_channel_name_bk not in ('Adwords', 'Adwords-branded', 'Google Shopping')
	-- where channel_name_bk = 'Affiliates' and marketing_channel_name_bk not in ('Affiliates')
	-- where channel_name_bk = 'Referral' and marketing_channel_name_bk not in ('Referral')
	-- where channel_name_bk = 'Social Media'
	-- where channel_name_bk = 'Email' and marketing_channel_name_bk not in ('Email', 'Email Reminder', 'Email Transactional')
	-- where channel_name_bk = 'Direct Entry' and marketing_channel_name_bk not in ('Direct Entry')
	-- where channel_name_bk = 'Organic Search' and marketing_channel_name_bk not in ('Organic Search')
	-- where channel_name_bk = 'Non-GA' and marketing_channel_name_bk not in ('Non-GA')
	order by marketing_channel_name_bk, telesales_f, cc_channel, affilCode, ga_medium, ga_source

	select *
	from DW_GetLenses_jbs.dbo.oh_marketing_channel
	where marketing_channel_name_bk = 'Redirect'
	order by marketing_channel_name_bk, telesales_f, cc_channel, affilCode, ga_medium, ga_source
