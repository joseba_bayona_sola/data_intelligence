
	-- DELETE STATEMENT
	delete from Landing.aux.sales_order_header_o

	-- INSERT STATEMENT: From XXXX
	insert into Landing.aux.sales_order_header_o(order_id_bk)
		select entity_id order_id_bk
		from Landing.mag.sales_flat_order
		where status <> 'archived' and base_subtotal <> 0 and customer_id is not null
			and store_id is not null
		union
		select order_id
		from Landing.mag.sales_flat_invoice
		union
		select order_id
		from Landing.mag.sales_flat_shipment
		union
		select order_id
		from Landing.mag.sales_flat_creditmemo
		union
		select oh.entity_id order_id
		from
				(select transactionId, transaction_date, source, medium
				from Landing.mag.ga_entity_transaction_data_aud
				where idETLBatchRun = 1226) ga
			inner join
				Landing.mag.sales_flat_order_aud oh on ga.transactionId = oh.increment_id

	-- Insert into Landing.aux.sales_order_header_o_i_s_cr

------------------------------------

	create table #oh_couponcode(
		order_id_bk						bigint NOT NULL, 
		coupon_id_bk					int,
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255), 
		channel							varchar(255))

		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) = 0 -- and len(o.applied_rule_ids) < 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.applied_rule_ids = cc.rule_id
			order by o.applied_rule_ids, o.coupon_code


		insert into #oh_couponcode (order_id_bk, coupon_id_bk, coupon_code, applied_rule_ids, channel)

			select o.order_id_bk, cc.coupon_id_bk, o.coupon_code, o.applied_rule_ids, cc.channel
			from
					(select o.entity_id order_id_bk, o.coupon_code, o.applied_rule_ids, len(o.applied_rule_ids) l
					from 
							Landing.aux.sales_order_header_o_i_s_cr oh
						inner join
							Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
					where (o.coupon_code is not null or o.applied_rule_ids is not null)
						and charindex(',', applied_rule_ids) <> 0 -- and len(o.applied_rule_ids) >= 5
						) o
				left join
					(select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id) cc on o.coupon_code = cc.coupon_code
			order by o.applied_rule_ids, o.coupon_code

------------------------------------

	create table #oh_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		ga_channel						varchar(100),
		telesales_f						char(1))

	insert into #oh_channel(order_id_bk, channel_name_bk, 
		ga_medium, ga_source, ga_channel, telesales_f)

		select order_id_bk, 
			case 
				when (ga_medium is null and telesales_f = 'N') then 'Non-GA'
				when (ga_medium is null and telesales_f = 'Y') then 'Non-GA Telesales'
				when (channel_name is not null) then channel_name
				else null
			end channel_name_bk,
			ga_medium, ga_source, null ga_channel, telesales_f
		from
			(select oh.order_id_bk, mc.channel_name,
				ga.medium ga_medium, ga.source ga_source, o.affilCode, 
				case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f 
			from 
					Landing.aux.sales_order_header_o_i_s_cr oh
				left join
					Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
				inner join
					Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
				left join
					Landing.map.sales_ch_ga_medium_ch mc on ga.medium = mc.ga_medium
			-- where oh.order_date < '2017-11-23'
			) t
		order by ga_medium, telesales_f, ga_source

		select channel_name_bk, count(*)
		from #oh_channel
		group by channel_name_bk
		order by channel_name_bk

		select t.channel_name_bk, t.num, c.idChannel_sk
		from
				(select channel_name_bk, count(*) num
				from #oh_channel
				group by channel_name_bk) t
			left join
				Warehouse.sales.dim_channel c on t.channel_name_bk = c.channel_name_bk
		order by t.channel_name_bk


	create table #oh_marketing_channel(
		order_id_bk						bigint NOT NULL, 
		channel_name_bk					varchar(50), 
		marketing_channel_name_bk		varchar(50), 
		ga_medium						varchar(100),
		ga_source						varchar(100),
		affilCode						varchar(255),
		cc_channel						varchar(255),
		telesales_f						char(1), 
		sms_auto_f						char(1),
		mutuelles_f						char(1))

		select marketing_channel_name_bk, count(*)
		from #oh_marketing_channel
		group by marketing_channel_name_bk
		order by marketing_channel_name_bk

		select t.marketing_channel_name_bk, t.num, mc.idMarketingChannel_sk
		from 
				(select marketing_channel_name_bk, count(*) num
				from #oh_marketing_channel
				group by marketing_channel_name_bk) t
			left join
				Warehouse.sales.dim_marketing_channel mc on t.marketing_channel_name_bk = mc.marketing_channel_name_bk
		order by t.marketing_channel_name_bk


		select channel_name_bk, count(*)
		from #oh_marketing_channel
		where marketing_channel_name_bk is null
		group by channel_name_bk
		order by channel_name_bk

		select channel_name_bk, marketing_channel_name_bk, count(*)
		from #oh_marketing_channel
		group by channel_name_bk, marketing_channel_name_bk
		order by channel_name_bk, marketing_channel_name_bk

	insert into #oh_marketing_channel(order_id_bk, channel_name_bk,	
		ga_medium, ga_source, affilCode, cc_channel, telesales_f, sms_auto_f, mutuelles_f)

		select oh.order_id_bk, c.channel_name_bk, 
			c.ga_medium, c.ga_source, o.affilCode, 
			cc.channel cc_channel,
			case when (o.telesales_admin_username is not null) then 'Y' else 'N' end telesales_f, 
			null sms_auto_f, null mutuelles_f 
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				#oh_channel c on oh.order_id_bk = c.order_id_bk
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				(select *
				from #oh_couponcode
				where channel in 
					(select cc_channel
					from Landing.map.sales_ch_cc_channel_mch)) cc on oh.order_id_bk = cc.order_id_bk
		-- where oh.order_date < '2017-11-23'
			-- and cc.channel is not null
		order by c.ga_medium, o.affilCode, o.telesales_admin_username, c.ga_source, o.store_id, oh.order_date

	-- SMS Automatic

	-- Coupon Code
	update mc
	set 
		mc.marketing_channel_name_bk = ccmch.marketing_channel_name
	from
			#oh_marketing_channel mc
		inner join
			Landing.map.sales_ch_cc_channel_mch ccmch on mc.cc_channel = ccmch.cc_channel
	where mc.marketing_channel_name_bk is null

	-- Customer Service
	update #oh_marketing_channel
	set marketing_channel_name_bk = 'Customer Service'
	where marketing_channel_name_bk is null and telesales_f = 'Y'

	-- BSC
	update mc
	set 
		mc.marketing_channel_name_bk = ac.marketing_channel_name
	from
			#oh_marketing_channel mc
		inner join
			Landing.map.sales_ch_affil_code_mch ac on mc.affilCode = ac.affil_code
	where mc.marketing_channel_name_bk is null

	update mc
	set 
		mc.marketing_channel_name_bk = acl.marketing_channel_name
	from
			#oh_marketing_channel mc
		inner join
			Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like = 'adwords-br'
	where mc.marketing_channel_name_bk is null

	update mc
	set 
		mc.marketing_channel_name_bk = acl.marketing_channel_name
	from
			#oh_marketing_channel mc
		inner join
			Landing.map.sales_ch_affil_code_like_mch acl on mc.affilCode like acl.affil_code_like + '%' and acl.affil_code_like <> 'adwords-br'
	where mc.marketing_channel_name_bk is null

	-- GA: Email
	update mc
	set 
		mc.marketing_channel_name_bk = isnull(gas.marketing_channel_name, 'Email Marketing')
	from
			#oh_marketing_channel mc
		left join
			Landing.map.sales_ch_ga_source_mch gas on mc.ga_source = gas.ga_source
	where mc.marketing_channel_name_bk is null and mc.channel_name_bk = 'Email'

	-- GA
	update #oh_marketing_channel
	set 
		marketing_channel_name_bk = case 
				when (channel_name_bk = 'Paid Search') then 'Paid Search Non Branded'
				when (channel_name_bk = 'SMS') then 'SMS Url'
				else channel_name_bk
			end
	where marketing_channel_name_bk is null and channel_name_bk is not null

---------------------------------------------------------

		select oh.order_id_bk, 
			c.channel_name_bk, mc.marketing_channel_name_bk, null publisher_id_bk, 
			mc.affilCode, c.ga_medium, c.ga_source, null ga_channel, mc.telesales_f, mc.sms_auto_f, mc.mutuelles_f,
			cc.coupon_id_bk, cc.coupon_code, cc.applied_rule_ids,
			null device_model_name_bk, null device_browser_name_bk, null device_os_name_bk, 
			null ga_device_model, null ga_device_browser, null ga_device_os
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
			left join
				#oh_couponcode cc on oh.order_id_bk = cc.order_id_bk
			inner join
				#oh_channel c on oh.order_id_bk = c.order_id_bk
			inner join
				#oh_marketing_channel mc on oh.order_id_bk = mc.order_id_bk
		order by oh.order_id_bk

---------------------------------------------------------

	select count(*)
	from #oh_marketing_channel
	-- where ga_medium is null and affilCode is null and telesales_f = 'N'
	-- where ga_medium is null and affilCode is not null 
	-- where ga_medium in ('cpc', 'ppc') and affilCode is null
	-- where ga_medium like 'affil%' and affilCode is not null
	-- where ga_medium = 'referral' and affilCode is null and telesales_f = 'N'
	-- where ga_medium like 'social%'
	-- where ga_medium = 'email' and affilCode is null
	-- where ga_medium = '(none)' and affilCode is not null
	where ga_medium = 'organic' and affilCode is null


	select oh.order_id_bk, oh.order_no, oh.order_date, o.store_id,
		ga.transaction_date, ga.medium, ga.source, o.affilCode, cc.channel cc_channel, o.telesales_admin_username 
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		left join
			Landing.mag.ga_entity_transaction_data_aud ga on oh.order_no = ga.transactionId
		inner join
			Landing.mag.sales_flat_order_aud o on oh.order_id_bk = o.entity_id
		left join
			#oh_couponcode cc on oh.order_id_bk = cc.order_id_bk
	-- where oh.order_date < '2017-11-23'
	order by cc_channel, ga.medium, o.affilCode, o.telesales_admin_username, ga.source, o.store_id, oh.order_date

select t1.affilCode, t2.affilCode, t1.num
from
		(select affilCode, count(*) num
		from #oh_marketing_channel
		group by affilCode) t1
	left join
		(select 'adwords' affilCode
		union 
		select 'affil' affilCode) t2 on t1.affilCode like t2.affilCode +'%'
order by t1.affilCode


