
select top 1000 channel_name, count(*)
from Warehouse.sales.dim_order_header_v
group by channel_name
order by channel_name

select top 1000 channel_name_bk, count(*)
from Landing.aux.sales_dim_order_header_aud
group by channel_name_bk
order by channel_name_bk

select top 1000 store_id_bk, count(*)
from Landing.aux.sales_dim_order_header_aud
where channel_name_bk is not null
group by store_id_bk
order by store_id_bk