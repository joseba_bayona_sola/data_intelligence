
select *
from Landing.mag.ga_entity_transaction_data
order by transaction_date desc

select oh.entity_id, oh.increment_id, oh.created_at, oh.store_id,
	ga.transaction_date, ga.medium, ga.source, 
	oh.affilCode, 
	oh.telesales_admin_username
from
		(select transactionId, transaction_date, source, medium
		from Landing.mag.ga_entity_transaction_data_aud
		where idETLBatchRun = 1205) ga
	inner join
		Landing.mag.sales_flat_order_aud oh on ga.transactionId = oh.increment_id
-- order by oh.created_at
order by ga.medium, oh.affilCode, ga.source, oh.created_at

--------------------------------------------------

					select isnull(src.coupon_id, sr.rule_id * -1) coupon_id_bk, sr.rule_id, 
						isnull(src.code, 'RULE ' + convert(varchar, sr.rule_id)) coupon_code, 
						sr.channel
					from 
							Landing.mag.salesrule_coupon_aud src
						right join
							Landing.mag.salesrule_aud sr on src.rule_id = sr.rule_id