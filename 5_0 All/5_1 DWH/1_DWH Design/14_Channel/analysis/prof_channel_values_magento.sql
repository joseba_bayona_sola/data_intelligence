
-- CHANNEL

select affilCode, count(*)
from Landing.mag.sales_flat_order_aud
group by affilCode
order by affilCode

select o.affilCode, sm.source_code, o.num
from
		(select affilCode, count(*) num
		from Landing.mag.sales_flat_order_aud
		group by affilCode) o
	left join
		openquery(MAGENTO, 'select * from dw_flat.dw_bis_source_map') sm on o.affilCode = sm.source_code
--where sm.source_code is null
order by o.affilCode


-----------------------------------------------------------

-- Google Analytics
	-- Medium: cpc - email - referral - ppc - organic

select top 1000 *
from Landing.mag.ga_entity_transaction_data_aud

select top 1000 medium, count(*)
from Landing.mag.ga_entity_transaction_data_aud
group by medium
order by medium

-----------------------------------------------------
---- profiling_medium_code
-----------------------------------------------------
select top 1000 medium, channel, min(transaction_date),  max(transaction_date), count(*) num
from Landing.mag.ga_entity_transaction_data_aud
-- where transaction_date > 20170000
group by medium, channel
order by medium, channel


-- referral - email
select source, min(transaction_date) min_date,  max(transaction_date) max_date, count(*) num
from Landing.mag.ga_entity_transaction_data_aud
where medium like '(none)' -- cpc / ppc / google shopping / affil // referral // social // email // organic // (none) // newsletter // sms // feed
group by source
order by count(*) desc


select source, min(transaction_date) min_date,  max(transaction_date) max_date, count(*) num
from Landing.mag.ga_entity_transaction_data_aud
where medium like 'affil%' and (channel <> 'Affiliates' and channel is not null)
group by source
order by count(*) desc

select source, min(transaction_date),  max(transaction_date), count(*) num 
from Landing.mag.ga_entity_transaction_data_aud
-- where medium like 'referral' and channel = 'Other'
where medium like 'referral' and channel = 'Direct'
group by source
order by count(*) desc
