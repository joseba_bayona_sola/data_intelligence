
select count(id)
from Order

select EON_PSP_Payment_Method__c, count(id)
from Order
group by EON_PSP_Payment_Method__c
order by EON_PSP_Payment_Method__c

select EON_Site__c, EON_Locale__c, EON_Currency__c, count(id)
from Order
group by EON_Site__c, EON_Locale__c, EON_Currency__c
order by EON_Site__c, EON_Locale__c, EON_Currency__c

select EON_Order_Method__c, count(id)
from Order
group by EON_Order_Method__c
order by EON_Order_Method__c

select EON_Customer_Order_Status__c, Status, SFCC_Payment_Status__c, EON_Export_Status__c, EON_Confirmation_Status__c, Shipment_Status__c, EON_Return_Status__c, count(id)
from Order
group by EON_Customer_Order_Status__c, Status, SFCC_Payment_Status__c, EON_Export_Status__c, EON_Confirmation_Status__c, Shipment_Status__c, EON_Return_Status__c
order by EON_Customer_Order_Status__c, Status, SFCC_Payment_Status__c, EON_Export_Status__c, EON_Confirmation_Status__c, Shipment_Status__c, EON_Return_Status__c

select EON_Customer_Order_Status__c, count(id)
from Order
group by EON_Customer_Order_Status__c
order by EON_Customer_Order_Status__c

select Status, count(id)
from Order
group by Status
order by Status

select SFCC_Payment_Status__c, count(id)
from Order
group by SFCC_Payment_Status__c
order by SFCC_Payment_Status__c

select EON_Export_Status__c, count(id)
from Order
group by EON_Export_Status__c
order by EON_Export_Status__c

select EON_Confirmation_Status__c, count(id)
from Order
group by EON_Confirmation_Status__c
order by EON_Confirmation_Status__c

select Shipment_Status__c, count(id)
from Order
group by Shipment_Status__c
order by Shipment_Status__c

select EON_Return_Status__c, count(id)
from Order
group by EON_Return_Status__c
order by EON_Return_Status__c

select EON_Applied_Promotions__c, count(id)
from Order
group by EON_Applied_Promotions__c
order by EON_Applied_Promotions__c
