
-- RecordType
select Id, name, 
	SobjectType, Description, IsPersonType, DeveloperName, 
	IsActive, -- isDeleted,
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from RecordType 
order by SobjectType, Name 

-- Profile 
select Id, name, 
	UserType, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Profile 

-- User
select Id, name, 
	ProfileId, 
	UserType, Username, FirstName, LastName, Email, 
	EON_GOC_number__c, 
	LastLoginDate, IsActive, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from User
