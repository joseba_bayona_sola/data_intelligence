
-- SOQL queries

	-- RecordType
	select Id, name, 
		SobjectType, Description, IsPersonType, DeveloperName, 
		IsActive, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from RecordType 
	order by SobjectType, Name 

	-- Profile 
	select Id, name, 
		UserType, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from Profile 

	-- User
	select Id, name, 
		ProfileId, 
		UserType, Username, FirstName, LastName, Email, 
		EON_GOC_number__c, 
		LastLoginDate, IsActive, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from User

	select Id, PersonContactId, RecordTypeId, Name, 
		EON_Preferred_Site__c, PersonEmail, 
		EON_Number_of_Orders__c, EON_Total_Order_Value__c, 
		IsDeleted, OwnerId,
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById
	from Account 
	limit 100

	select Id, Name, 
		Account__c, EON_Active__c, 
		Address_Line_1__c, City__c, State__c, Postal_Code__c, Country__c,
		IsDeleted, OwnerId,
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById
	from Address__c 
	limit 100

	select Id, Name, 
		EON_Account__c, 
		EON_Method__c, EON_Frequency__c, EON_Opt_in__c, 
		EON_Legacy_Preference_Id__c,
		IsDeleted, OwnerId,
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById
	from EON_Preferences__c 
	limit 100

-- Landing DB Tables

	-- RecordType
	create table Landing.sf_sc_poc.RecordType(
		Id							varchar(18) NOT NULL,
		Name						varchar(255) NOT NULL, 
		
		SobjectType					varchar(40) NOT NULL, 
		Description					varchar(255), 
		IsPersonType				bit NOT NULL, 
		DeveloperName				varchar(80) NOT NULL,	 
		IsActive					bit NOT NULL, 
		-- isDeleted					bit NOT NULL,

		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.RecordType add constraint [PK_sf_sc_poc_RecordType]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.RecordType add constraint [DF_sf_sc_poc_RecordType_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 
	
	-- Profile
	create table Landing.sf_sc_poc.Profile(
		Id							varchar(18) NOT NULL,
		Name						varchar(255) NOT NULL, 
		UserType					varchar(40), 

		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.Profile add constraint [PK_sf_sc_poc_Profile]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.Profile add constraint [DF_sf_sc_poc_Profile_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 
	
	-- t_User
	create table Landing.sf_sc_poc.t_User(
		Id							varchar(18) NOT NULL,
		Name						varchar(255) NOT NULL, 
		
		ProfileId					varchar(18), 
		UserType					varchar(40), 
		Username					varchar(80) NOT NULL, 
		FirstName					varchar(40), 
		LastName					varchar(80), 
		Email						varchar(128) NOT NULL, 
		EON_GOC_number__c			varchar(255), 
		LastLoginDate				varchar(255), 
		IsActive					bit NOT NULL, 

		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.t_User add constraint [PK_sf_sc_poc_t_User]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.t_User add constraint [DF_sf_sc_poc_t_User_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 
	
	-- Account
	create table Landing.sf_sc_poc.Account(
		Id							varchar(18) NOT NULL,
		PersonContactId				varchar(18) NOT NULL,	 
		RecordTypeId				varchar(18) NOT NULL,	 
		Name						varchar(255), 
		
		EON_Preferred_Site__c		varchar(255),
		PersonEmail					varchar(80),

		EON_Number_of_Orders__c		decimal(18, 4), 
		EON_Total_Order_Value__c	decimal(18, 4), 

		IsDeleted					bit NOT NULL, 
		OwnerId						varchar(18), 
		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.Account add constraint [PK_sf_sc_poc_Account]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.Account add constraint [DF_sf_sc_poc_Account_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 
	
	-- Address__c
	create table Landing.sf_sc_poc.Address__c(
		Id							varchar(18) NOT NULL,
		Name						varchar(255) NOT NULL, 
		
		Account__c					varchar(18) NOT NULL,
		EON_Active__c				bit NOT NULL,

		Address_Line_1__c			varchar(255),
		City__c						varchar(255), 
		State__c					varchar(255), 
		Postal_Code__c				varchar(255), 
		Country__c					varchar(255),

		IsDeleted					bit NOT NULL, 
		OwnerId						varchar(18), 
		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.Address__c add constraint [PK_sf_sc_poc_Address__c]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.Address__c add constraint [DF_sf_sc_poc_Address__c_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 	
	
	-- EON_Preferences__c
	create table Landing.sf_sc_poc.EON_Preferences__c(
		Id							varchar(18) NOT NULL,
		Name						varchar(80) NOT NULL, 
		
		EON_Account__c				varchar(18) NOT NULL,
		EON_Method__c				varchar(255) NOT NULL, 
		EON_Frequency__c			varchar(255), 
		EON_Opt_in__c				bit NOT NULL, 
		EON_Legacy_Preference_Id__c	varchar(255), 			

		IsDeleted					bit NOT NULL, 
		OwnerId						varchar(18), 
		CreatedDate					varchar(255) NOT NULL, 
		LastModifiedDate			varchar(255) NOT NULL,
		CreatedById					varchar(18), 
		LastModifiedById			varchar(18),

		idBatchRun					bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table Landing.sf_sc_poc.EON_Preferences__c add constraint [PK_sf_sc_poc_EON_Preferences__c]
		primary key clustered (Id);
	go
	alter table Landing.sf_sc_poc.EON_Preferences__c add constraint [DF_sf_sc_poc_EON_Preferences__c_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 	