
-- EON_Prescriptions__c
select id, RecordTypeId, Name, 
	EON_Customer_Record_Id__c, 
	EON_Wearer__c, EON_Is_Active__c, EON_Issue_Date__c, EON_Recall__c, EON_Prescription_Notes__c, EON_Expiration_Date__c, 
	EON_Left_Base_Curve__c, EON_Left_Diameter__c, EON_Left_Power__c, EON_Left_Cylinder__c, EON_Left_Axis__c, EON_Left_Dominant__c, EON_Left_Addition__c, 
	EON_Right_Base_Curve__c, EON_Right_Diameter__c, EON_Right_Power__c, EON_Right_Cylinder__c, EON_Right_Axis__c, EON_Right_Dominant__c, EON_Right_Addition__c, 
	
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Prescriptions__c 
order by Id desc


-- EON_Re_Orders__c -- EON_Mass_Phone_Update__c, EON_Sent_to_CC_Successfully__c 
select id, RecordTypeId, Name, 
	EON_Customer_Name__c, EON_Order_ID__c, EON_Customer_Order_Number__c, EON_Re_Order_Date__c,
	EON_Frequency_in_Days__c, 
	EON_Is_Active__c, EON_Site__c, EON_Communication_Channel__c, 
	EON_Email__c, EON_Mobile_Phone__c, EON_Mobile__c, EON_Phone_Type__c, 
	EON_Reminder_Sent__c, 
	EON_Not_Received_By_CC__c,
	
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Re_Orders__c 
order by id desc

-- EON_Store_Credit__c -- EON_Customer_Notification__c
select id, RecordTypeId, Name, 
	EON_Customer_Name__c, EON_Site__c, EON_Order_ID__c, EON_Customer_Order_Number__c, 
	EON_Type__c, 
	EON_Amount__c, EON_Currency__c, 
	EON_Category__c, EON_Reason__c, 
	EON_Customer_Notification__c,
	
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Store_Credit__c 
order by id desc



-- Order -- ShippingAddress - BillingAddress
	-- where SFCC_Order_Number__c = 'dev19000006030011'
select id, RecordTypeId, OrderNumber, AccountId, 
	SFCC_Order_Number__c, EON_Original_Order_Number__c, EON_Original_Order_ID__c, EON_Replaced_Order_Number__c, EON_Replaced_Order_Id__c, EON_Replacement_Order_Number__c, EON_Replacement_Order_Id__c, 
	EON_PSP_Payment_Method__c, EON_Site__c, EON_Locale__c, EON_Order_Method__c, 
	EON_Order_Date__c, EON_Original_Order_Date__c, 
	EON_Customer_Order_Status__c, Status, SFCC_Payment_Status__c, EON_Export_Status__c, EON_Confirmation_Status__c, Shipment_Status__c, EON_Return_Status__c, 
	EON_Fraud_Score__c, EON_Chargeback__c, 
	EON_Currency__c, EON_Costs_of_Goods__c, EON_Applied_Promotions__c, EON_Subtotal_Before_Deduction__c, EON_Promotion_Discount_Amount__c, EON_Subtotal_before_Tax__c, EON_VAT_Tax_Amount__c, EON_Dispensing_Services__c, EON_Delivery_Charge__c, SFCC_Order_Total__c, EON_Total_Refunded__c, 
	EON_Rx_Check_Required__c, EON_Prescription__c, 
	EON_Estimated_Shipping_Date__c, EON_Actual_Shipping_Date__c, EON_Estimated_Delivery_Date__c, EON_Actual_Delivery_Date__c, 
	EON_Ordered_Shipping_Method_Id__c, EON_Ordered_Shipping_Method__c,
	EON_Shipping_Address_Name__c, EON_Shipping_Address_Phone_Number__c, ShippingAddress, ShippingCity, ShippingState, ShippingCountry, ShippingPostalCode, 
	EON_Billing_Address_Name__c, EON_Billing_Address_Phone_Number__c, BillingAddress, BillingCity, BillingState, BillingCountry, BillingPostalCode, 
	EON_Invoice_URL__c, 
	EON_Ordered_on_Behalf_By__c, EON_Order_Token__c, 
	EON_Delayed_Shipment__c, EON_HT_Late_Return__c, EON_New_Return_Identified__c, EffectiveDate, EON_ReOrder_Reminder__c, 
	
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Order
order by CreatedDate desc

-- EON_Order_Line__c
select id, RecordTypeId, Name, 
	EON_Order__c, EON_Customer_Order_Number__c, EON_Shipment__c, EON_CC_Shipment_Number__c, 
	EON_Product_Name__c, EON_SKU__c, EON_Quantity__c, EON_Pack_Size__c, 
	EON_Fulfilment_Status__c, EON_Allocated_Lenses__c, EON_Allocated_Boxes_Pack_Size__c, 
	EON_Unit_Price__c, EON_Price_Per_Lens__c, EON_Discount_Amount__c, EON_Applied_Promotions__c, EON_Pricebook__c, EON_Order_Line_Total__c, 
	EON_Eye__c, EON_Colour__c, EON_Base_Curve__c, EON_Diameter__c, EON_Power__c, EON_Cylinder__c, EON_Axis__c, EON_Addition__c, EON_Dominant__c, 
	
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Order_Line__c

-- Order Amendment

-- EON_Payment_Transaction__c
	-- EON_PCI__c
	-- EON_Bank_Account_Number__c, EON_Bank_Name__c, EON_City__c, EON_IBAN__c, EON_Swift_Code__c
	-- EON_Multibanco_Reference__c, EON_MultiBanco_Entity__c
select id, Name, EON_Payment_Transaction_Number__c, 
	EON_Order_Id__c, EON_Customer_Order_Number__c, 
	EON_Transaction_Status__c, EON_Payment_Method__c, EON_Transaction_Amount__c, 
	EON_Credit_Card_Type__c, EON_Card_Number__c, EON_Card_Expiration_Year__c, EON_Card_Expiration_Month__c, 
	EON_PayPal_Payer_Id__c, EON_PayPal_Email__c, EON_PayPal_Payment_Status__c, 

	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Payment_Transaction__c
order by id desc

-- EON_Shipment__c
select id,  Name, 
	EON_Order__c, EON_Customer_Order_Number__c, EON_Shipping_Reference_Number__c, 
	EON_Actual_Shipping_Method_Id__c, EON_Actual_Shipping_Method__c, EON_Actual_Shipment_Date__c,
	EON_Late_Delivery__c, EON_Delivery_Status__c, 
	EON_Estimated_Delivery_Date__c, EON_Actual_Delivery_Date__c, 
	EON_Shipping_Method__c, EON_Carrier__c, EON_Tracking_Number__c, EON_Tracking_Link__c,
	EON_CC_Shipment_Number__c,
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Shipment__c
order by CreatedDate desc
