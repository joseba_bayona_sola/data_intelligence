

-- EON_Prescriptions__c
select Id, Name, 
	EON_Customer_Record_Id__c, EON_Wearer__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Prescriptions__c 

-- EON_Manufacturing_Data_History__c
select Id, EON_Wearer__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Manufacturing_Data_History__c 

-- EON_Re_Orders__c
select Id, Name, RecordTypeId, EON_Customer_Name__c, EON_Order_ID__c, 
	EON_Site__c, EON_Customer_Order_Number__c, EON_Email__c, 
	EON_Reorder_URL__c, EON_Re_Order_Date__c, EON_Mobile__c, EON_Mobile_Phone__c, EON_Communication_Channel__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById
from EON_Re_Orders__c 

-- EON_Store_Credit__c
select Id, Name, RecordTypeId, EON_Customer_Name__c, EON_Order_ID__c, 
	EON_Customer_Order_Number__c, EON_Site__c, EON_Type__c, 
	EON_Reason_Category__c, EON_Reason__c, EON_Category__c, EON_Amount__c, EON_Currency__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Store_Credit__c 
order by RecordTypeId, EON_Type__c 

-- Mutuelle

-- Payment Info

-- EON_Payment_Transaction__c
select Id, Name, EON_Order_Id__c, 
	EON_Customer_Order_Number__c, 
	EON_Transaction_Status__c, EON_Payment_Method__c, 
	EON_Credit_Card_Type__c, EON_Card_Number__c, EON_Card_Expiration_Year__c, EON_Card_Expiration_Month__c, 
	EON_Transaction_Amount__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Payment_Transaction__c 

-- Order
select Id, SFCC_Order_Number__c, RecordTypeId, AccountId, 
	EON_Created_Date_Time__c, EON_Site__c, 
	EON_Email__c, EON_Account_Name__c, EON_Account_Full_Name__c, 
	Status, StatusCode, Order_SCCSync_Status__c, SFCC_Payment_Status__c, EON_Confirmation_Status__c, 
	EON_Line_item_price_before_discounts__c, EON_Line_item_quantity__c, 
	EON_Costs_of_Goods__c, EON_Subtotal_Before_Deduction__c, EON_Subtotal_before_Tax__c, 
	EON_Delivery_Charge__c, EON_Promotion_Discount_Amount__c, 
	SFCC_Order_Total__c, TotalAmount, EON_VAT_Tax_Amount__c, 
	EON_Total_Refunded__c, 
	EON_Currency__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from Order 
order by EON_Created_Date_Time__c desc

-- EON_Order_Line__c
select Id, Name, RecordTypeId, EON_Order__c, EON_Shipment__c, EON_Wearer__c, EON_Prescription__c, 
	EON_Customer_Order_Number__c, 
	EON_Product_Name__c, EON_SKU__c, 
	EON_Quantity__c, EON_Unit_Price__c, EON_Pricebook__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Order_Line__c 

-- Order Amendment

-- EON_Shipment__c
select Id, Name, Shipment_No__c, EON_Order__c, 
	EON_Customer_Order_Number__c, Shipping_Address__c, 
	EON_Shipping_Method__c, EON_Carrier__c, 
	EON_Tracking_Number__c, 
	EON_Estimated_Delivery_Date__c, 
	EON_Actual_Shipment_Date__c, EON_Actual_Delivery_Date__c, 
	Shipment_Total__c, Shipping_Total__c, Shipping_Total_Tax__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from EON_Shipment__c 
order by CreatedDate desc