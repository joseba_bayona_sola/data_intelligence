
-- General Case
-- Case Details
-- Case Description
	-- Service: Payment Investigation - Refunds - GDPR - Fulfilment Actions
	-- Returns: Returns
	-- RX Auth
	-- TrustPilot: TrustPilot
	-- Payment Dispute: Payment Dispute
	-- Mutuelle: Mutuelle
-- Case Resolution
-- System Information

-- Case -- Description
select id, RecordTypeId, CaseNumber, 
	Status, Origin, ParentId, EON_Case_Category__c, EON_Case_Reason__c, 
	AccountId, Order__c, EON_Customer_Order_Number__c, EON_Order_Status__c, EON_Site__c, EON_Legacy_Order_Number__c, CL_Legacy_Site__c, 
	EON_Wearer__c, EON_Case_Contact__c, 
	Subject, 
	EON_Reported_Order_Status__c, EON_Reported_Value__c, EON_Action_Requested__c, EON_OM_Actions_Taken__c, EON_Payment_Portal__c, EON_Payment_Action_Notes__c, EON_Investigation_Status__c, 
	EON_Items_Refunded__c, EON_Refund_Reason__c, EON_Refund_Value__c, EON_Refund_Action_Notes__c, EON_Refund_Status__c, 
	EON_GDPR_Language__c, EON_GDPR_Date_Actioned__c, EON_GDPR_Action_Notes__c, EON_Customer_Complaint_Made__c, EON_Decision__c,
	EON_Fulfilment_Action__c, 
	EON_Warehouse__c, EON_Returned_Order_Line_s__c, EON_Returns_Status__c, EON_Quantity__c, EON_Condition__c, EON_Customer_Request__c, EON_Reason_Code__c, EON_Alternative_Exchange__c, EON_Returns_Value__c, 
	EON_Review_Date__c, CL_Review_Type__c, EON_Star_Rating__c, EON_Product_Type__c, EON_Product_Name__c, EON_Published_Response__c, EON_Reported__c, 
	EON_Tracking_Delivery_Date__c, EON_Customer_Delivery_Date__c, EON_Estimated_Dispatch__c, EON_Actual_Dispatch__c, EON_Estimated_Delivery__c, EON_Actual_Delivery__c, 
	EON_Delivery_Method__c, EON_What_Happened_Factual__c, EON_CSC_Contact__c, EON_CSC_Issue__c, EON_Goodwill_Issued__c, EON_Revised_Star_Rating__c, EON_Removed__c, 
	EON_Dispute_Raised_Date__c, EON_Response_Due_Date__c, EON_Dispute_ID__c, EON_Address_with_Paypal__c, EON_Email_Address_with_Paypal__c, EON_Transaction_ID__c, EON_Dispute_Amount__c, EON_Customer_Comments__c, EON_Response_Date__c, 
	EON_Case_Closure_Reason__c, EON_Resolution_Details__c, 
	ClosedDate, SuppliedName, SuppliedEmail, SuppliedPhone, ContactId, 
	EON_Fraud_Case__c, EON_Rx_Check_Case__c, 
	
	EON_Hour_Created_GMT__c, EON_Quarter_Hour_Created_GMT__c, EON_Hour_Closed_GMT__c, EON_Quarter_Hour_Closed_GMT__c, 
	EON_First_Assigned_Date__c, EON_Hour_First_Assigned_GMT__c, EON_Quarter_Hour_First_Assigned_GMT__c, 
	EON_First_Response_Date__c, EON_Time_to_First_Response_hours__c, 
	EON_First_Contact_Date__c, EON_First_Contact_Resolution__c, 
	EON_Resolution_Time_hours__c, 
	EON_Handling_Time_minutes__c, EON_Time_Entered_Handling__c, 
	EON_Original_Owner__c, EON_Is_Reassigned__c, EON_Original_Owner_Is_Viewing__c, EON_Count_of_Owners__c,
	
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Case 
order by EON_Case_Category__c, EON_Case_Reason__c, Origin, CreatedDate desc
limit 10

select id, RecordTypeId, CaseNumber, 
	Status, Origin, ParentId, EON_Case_Category__c, EON_Case_Reason__c, 
	AccountId, EON_Order_Status__c, EON_Legacy_Order_Number__c, 
	EON_Wearer__c, EON_Case_Contact__c, 
	Subject, 
	EON_Reported_Order_Status__c, EON_Reported_Value__c, EON_Action_Requested__c, EON_OM_Actions_Taken__c, EON_Payment_Portal__c, EON_Payment_Action_Notes__c, EON_Investigation_Status__c, 
	EON_Items_Refunded__c, EON_Refund_Reason__c, EON_Refund_Value__c, EON_Refund_Action_Notes__c, EON_Refund_Status__c, 
	EON_GDPR_Language__c, EON_GDPR_Date_Actioned__c, EON_GDPR_Action_Notes__c, EON_Customer_Complaint_Made__c, 
	EON_Fulfilment_Action__c, 
	EON_Warehouse__c, EON_Returned_Order_Line_s__c, EON_Returns_Status__c, EON_Quantity__c, EON_Condition__c, EON_Customer_Request__c, EON_Reason_Code__c, EON_Alternative_Exchange__c, EON_Returns_Value__c, 
	EON_Star_Rating__c, EON_Product_Type__c, EON_Product_Name__c, EON_Published_Response__c, EON_Reported__c, 
	EON_Tracking_Delivery_Date__c, EON_Customer_Delivery_Date__c, EON_Estimated_Dispatch__c, EON_Actual_Dispatch__c, EON_Estimated_Delivery__c, EON_Actual_Delivery__c, 
	EON_Delivery_Method__c, EON_What_Happened_Factual__c, EON_CSC_Contact__c, EON_CSC_Issue__c, EON_Goodwill_Issued__c, EON_Revised_Star_Rating__c, EON_Removed__c, 
	EON_Dispute_Raised_Date__c, EON_Response_Due_Date__c, EON_Dispute_ID__c, EON_Address_with_Paypal__c, EON_Email_Address_with_Paypal__c, EON_Transaction_ID__c, EON_Dispute_Amount__c, EON_Customer_Comments__c, EON_Response_Date__c, 
	EON_Case_Closure_Reason__c, EON_Resolution_Details__c, 
	ClosedDate, SuppliedName, SuppliedEmail, SuppliedPhone, ContactId, 
	EON_Fraud_Case__c, EON_Rx_Check_Case__c, 
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Case 
order by EON_Case_Category__c, EON_Case_Reason__c, Origin, CreatedDate desc


select id, RecordTypeId, CaseNumber, 
	Status, Origin, ParentId, EON_Case_Category__c, EON_Case_Reason__c, 
	AccountId, EON_Order_Status__c, EON_Legacy_Order_Number__c, 
	EON_Wearer__c, EON_Case_Contact__c, 
	Subject, 
	EON_Reported_Order_Status__c, EON_Reported_Value__c, EON_Action_Requested__c, EON_OM_Actions_Taken__c, EON_Payment_Portal__c, EON_Payment_Action_Notes__c, EON_Investigation_Status__c, 
	EON_Items_Refunded__c, EON_Refund_Reason__c, EON_Refund_Value__c, EON_Refund_Action_Notes__c, EON_Refund_Status__c, 
	EON_GDPR_Language__c, EON_GDPR_Date_Actioned__c, EON_GDPR_Action_Notes__c, EON_Customer_Complaint_Made__c, 
	EON_Fulfilment_Action__c, 
	EON_Warehouse__c, EON_Returned_Order_Line_s__c, EON_Quantity__c, EON_Condition__c, EON_Customer_Request__c, EON_Reason_Code__c, EON_Alternative_Exchange__c, EON_Returns_Value__c, 
	EON_Star_Rating__c, EON_Product_Type__c, EON_Product_Name__c, EON_Published_Response__c, EON_Reported__c, 
	EON_Tracking_Delivery_Date__c, EON_Customer_Delivery_Date__c, EON_Estimated_Dispatch__c, EON_Actual_Dispatch__c, EON_Estimated_Delivery__c, EON_Actual_Delivery__c, 
	EON_Delivery_Method__c, EON_What_Happened_Factual__c, EON_CSC_Contact__c, EON_CSC_Issue__c, EON_Goodwill_Issued__c, EON_Revised_Star_Rating__c, EON_Removed__c, 
	EON_Dispute_Raised_Date__c, EON_Response_Due_Date__c, EON_Dispute_ID__c, EON_Address_with_Paypal__c, EON_Email_Address_with_Paypal__c, EON_Transaction_ID__c, EON_Dispute_Amount__c, EON_Customer_Comments__c, EON_Response_Date__c, 
	EON_Case_Closure_Reason__c, EON_Resolution_Details__c, 
	ClosedDate, SuppliedName, SuppliedEmail, SuppliedPhone, ContactId, 
	EON_Fraud_Case__c, EON_Rx_Check_Case__c, 
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Case 
order by EON_Case_Category__c, EON_Case_Reason__c, Origin, CreatedDate desc
limit 10000