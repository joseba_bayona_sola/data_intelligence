
-- Account
select id, PersonContactId, RecordTypeId, Name, 
	SCCActive__c, 
	Salutation, FirstName, LastName, 
	EON_Customer_Value_Tier__c, EON_Service_Recovery__c, EON_High_Risk__c, EON_Has_Chargebacks__c, 
	EON_Preferred_Locale__c, EON_Preferred_Site__c, 
	EON_Terms_Conditions_Date__c, 
	EON_Last_Order_Date__c, EON_Months_Since_Last_Order__c, 
	PersonEmail, EON_Alternative_Email__c, EON_Email__c, 
	EON_Phone_Type__c, Phone, EON_Alt_Phone_1_Type__c, EON_Alternative_Phone_1__c, EON_Alt_Phone_2_Type__c, EON_Alternative_Phone_2__c, EON_Mobile__c, 
	EON_Default_Shipping_Address__c, EON_Default_Billing_Address__c, 
	EON_Date_of_Birth__c, EON_Tax_Number__c, EON_Authorised_User_1__c, EON_Authorised_User_2__c, EON_Authorised_User_3__c, EON_Prescription_Check_Required__c, 
	SFCC_Last_Visit_Time__c, SFCC_Last_Login_Time__c, EON_Last_Password_Request_Date__c, 
	EON_Number_of_Orders__c, EON_Number_of_Refunds__c, EON_Total_Order_Value__c, EON_Total_Refund_Value__c, 
	EON_Reminder_Email_Setting__c, EON_Reminder_SMS_Setting__c, EON_Newsletter_Signed_Up__c, 
	SFCC_Customer_Number__pc, EON_Legacy_Customer_Id__c, SFCC_Customer_Id__pc, 
	EON_CC_Default_Shipping_Address_Id__c, EON_CC_Default_Billing_Address_Id__c, 
	EON_Requires_Password_Migration__c, EON_Migrated_From_Site__c, EON_Registration_Date__c, 
	EON_Customer_Name__c, EON_National_Identification_Number__c, EON_Birth_Rank__c, 
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Account 
limit 10
where Id = '0013O00000D0UDUQA3' 

	select Id, EON_Legacy_Customer_Id__c, Name
	from Account
	where EON_Legacy_Customer_Id__c <> null 
	order by Id
	limit 100
	offset 10
	
-- Address__c
select id, Name, 
	Account__c, EON_Active__c, 
	EON_Default_Shipping_Address__c, EON_Default_Billing_Address__c, First_Name__c, Last_Name__c, 
	EON_Phone_Type__c, Phone__c, EON_Mobile__c, EON_Alt_Phone__c, EON_Alt_Phone_Type__c, 
	Address_Line_1__c, Address_Line_2__c, City__c, State__c, Postal_Code__c, Country__c, 
	EON_CC_Address_Id__c, EON_Legacy_Address_Id__c, 
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Address__c 
limit 100
where Account__c = '0013O00000D0UDUQA3'
	
-- EON_Preferences__c
select id, Name, 
	EON_Account__c, 
	EON_Method__c, EON_Frequency__c, EON_Opt_in__c, 
	EON_Legacy_Preference_Id__c, 
	IsDeleted, OwnerId, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from EON_Preferences__c 
limit 10
where EON_Account__c = '0013O00000D0UDUQA3'

-------------------------------------------------
-------------------------------------------------
select Id, PersonContactId, RecordTypeId, Name, 
	EON_Preferred_Site__c, PersonEmail, 
	EON_Number_of_Orders__c, EON_Total_Order_Value__c, 
	IsDeleted, OwnerId,
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById
from Account 
limit 100

select Id, Name, 
	Account__c, EON_Active__c, 
	Address_Line_1__c, City__c, State__c, Postal_Code__c, Country__c,
	IsDeleted, OwnerId,
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById
from Address__c 
limit 100

select Id, Name, 
	EON_Account__c, 
	EON_Method__c, EON_Frequency__c, EON_Opt_in__c, 
	EON_Legacy_Preference_Id__c,
	IsDeleted, OwnerId,
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById
from EON_Preferences__c 
limit 100


