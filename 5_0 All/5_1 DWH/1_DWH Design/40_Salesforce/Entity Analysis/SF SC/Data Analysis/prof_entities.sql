
-- Account (} AccountHistory, AccountChangeEvent, AccountFeed)
select Id, PersonContactId, EON_Legacy_Customer_Id__c, 
	EON_Email__c, PersonEmail, EON_Preferred_Site__c, EON_Preferred_Locale__c, Name, FirstName, LastName, 
	EON_Default_Billing_Address__c, EON_Default_Shipping_Address__c, 
	EON_Registration_Date__c, EON_Migrated_From_Site__c, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById

from Account 
where Id = '0013O00000D0UDUQA3' 
where createdDate > 2020-04-24T00:00:00Z and createdDate < 2020-04-25T00:00:00Z
	-- '0011i00000TyAWmAAN'

	select count(Id)
	from Account 
	where (createdDate > 2020-04-24T00:00:00Z and createdDate < 2020-04-25T00:00:00Z) or (LastModifiedDate > 2020-04-24T00:00:00Z and LastModifiedDate < 2020-04-25T00:00:00Z)
	
-- Contact
select Id, AccountId, EON_Person_Account_ID__c, EON_Customer_ID__c, 
	Email, Name,
	SFCC_Customer_Id__c, SFCC_Customer_Number__c, 	
	OwnerId, CreatedById, LastModifiedById
from Contact
where Id = '0033O00000EGv7sQAD' 
	-- '0031i00000TC84MAAT'
	-- EON_Number_of_Orders__c, 
	
-- RecordType
select Id, name, Description, SobjectType, IsPersonType, DeveloperName, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from RecordType 
order by SobjectType, Name 

-- Address__c 
select id, Account__c, EON_Legacy_Address_Id__c, 
	First_Name__c, Last_Name__c, Full_Name__c, 
	Address_Line_1__c, Address_Line_2__c, State__c, City__c, Country__c, 
	EON_Address_Field__c, 
	EON_Default_Billing_Address__c, EON_Default_Shipping_Address__c, 
	EON_Phone_Type__c, Phone__c, EON_Mobile__c,  
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById 
from Address__c 
where Account__c = '0013O00000D0UDUQA3'
	-- '0011i00000TyAWmAAN'

-- EON_Preferences__c (+ EON_Preferences__History, EON_Preferences__ChangeEvent, EON_Preferences__Feed)
select Id, EON_Account__c, name,
	EON_Method__c, EON_Frequency__c, EON_Opt_in__c, 
	EON_Legacy_Preference_Id__c, 
	IsDeleted, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById 
from EON_Preferences__c 
where EON_Account__c = '0013O00000D0UDUQA3'
	-- '0011i00000TyAWmAAN'

-- Case
select Id, AccountId, EON_Person_Account__c, ContactId, RecordTypeId, 
	CaseNumber, Status, Priority, Origin, 
	EON_Case_Category__c, EON_Case_Reason__c, EON_Case_Contact__c, EON_Case_Closure_Reason__c, 
	Subject, Description, 
	SuppliedEmail, SuppliedName, ContactEmail, ContactPhone, 
	EON_Original_Owner__c, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById
from Case

	-- where Id = '5001i00000MRAzYAAX'

-- Task -- Description
select Id, AccountId, WhoId, RecordTypeId, EON_Case_ID__c, 
	TaskSubtype, ACD_queue__c, 
	CallDisposition, CallType, CallObject, CallDurationInSeconds, 
	Status, 
	CreatedDate, LastModifiedDate, 
	OwnerId, CreatedById, LastModifiedById			
from Task
	-- where EON_Case_ID__c = '5001i00000MS1hAAAT'
	-- EON_Interaction_URL__c
	
-- EmailMessage
select count(Id)
from EmailMessage

-----------------------------------------------------
-----------------------------------------------------

-- Profile
select Id, Name, UserLicenseId, UserType, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from Profile 

-- User
select Id, ProfileId, 
	email, Username, 
	FirstName, LastName, Name, 
	LastLoginDate, 
	CreatedDate, LastModifiedDate, 
	CreatedById, LastModifiedById 
from User


-----------------------------------------------------
-----------------------------------------------------
-- purecloud__PureCloud_Log__c 
select count(Id)
from purecloud__PureCloud_Log__c 

select Id, Name, purecloud__Level__c, purecloud__Message__c, purecloud__Timestamp__c
from purecloud__PureCloud_Log__c 

-----------------------------------------------------
-----------------------------------------------------

-- Asset
select count(Id)
from Asset

-- Lead
select count(Id)
from Lead

-- Opportunity
select count(Id)
from Opportunity


-- Order
select count(Id)
from Order

-- Quote
select count(Id)
from Quote

-- Contract
select count(Id)
from Contract

-- Campaign
select count(Id)
from Campaign

















