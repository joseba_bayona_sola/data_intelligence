
	-- RecordType
	select Id, name, 
		SobjectType, Description, IsPersonType, DeveloperName, 
		IsActive, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from RecordType 
	order by SobjectType, Name 

	-- Profile 
	select Id, name, 
		UserType, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from Profile 

	-- User
	select Id, name, 
		ProfileId, 
		UserType, Username, FirstName, LastName, Email, 
		EON_GOC_number__c, 
		LastLoginDate, IsActive, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from User
	
	-- Account
		-- ?? EON_Number_of_Orders__c --> EON_Total_Refund_Value__c, EON_Total_Order_Value__c --> EON_TotalOrderValue__c
		-- Add: EON_Store_Credit_Balance__c - EON_Text_Local_Email__c - EON_Phone_Type__c - EON_Alt_Phone_1_Type__c - EON_Alt_Phone_2_Type__c
			-- EON_Sign_Up_Form_Source__c - EON_Preferred_Lenses__c
	select id, PersonContactId, RecordTypeId, Name,  
		SCCActive__c, 
		Salutation, FirstName, LastName, 
		EON_Customer_Value_Tier__c, EON_Service_Recovery__c, EON_High_Risk__c, EON_Has_Chargebacks__c, 
		EON_Preferred_Locale__c, EON_Preferred_Site__c, 
		EON_Terms_Conditions_Date__c, 
		EON_Last_Order_Date__c, EON_Months_Since_Last_Order__c, 
		PersonEmail, EON_Alternative_Email__c, EON_Email__c, 
		Phone, EON_Alternative_Phone_1__c, EON_Alternative_Phone_2__c, EON_Mobile__c, 
		EON_Default_Shipping_Address__c, EON_Default_Billing_Address__c, 
		EON_Date_of_Birth__c, EON_Tax_Number__c, EON_Authorised_User_1__c, EON_Authorised_User_2__c, EON_Authorised_User_3__c, EON_Prescription_Check_Required__c, 
		SFCC_Last_Visit_Time__c, SFCC_Last_Login_Time__c, EON_Last_Password_Request_Date__c, 
		EON_Number_of_Orders__c, EON_Number_of_Refunds__c, EON_Total_Order_Value__c, EON_Total_Refund_Value__c, 
		EON_Reminder_Email_Setting__c, EON_Reminder_SMS_Setting__c, EON_Newsletter_Signed_Up__c, 
		SFCC_Customer_Number__pc, EON_Legacy_Customer_Id__c, SFCC_Customer_Id__pc, 
		EON_CC_Default_Shipping_Address_Id__c, EON_CC_Default_Billing_Address_Id__c, 
		EON_Requires_Password_Migration__c, EON_Migrated_From_Site__c, EON_Registration_Date__c, 
		EON_Customer_Name__c, EON_National_Identification_Number__c, EON_Birth_Rank__c, 
		IsDeleted, OwnerId, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from Account 
	limit 10

	-- Address__c
		-- Add: EON_Tax_Number__c
	select id, Name, 
		Account__c, EON_Active__c, 
		EON_Default_Shipping_Address__c, EON_Default_Billing_Address__c, First_Name__c, Last_Name__c, 		
		EON_Phone_Type__c, Phone__c, EON_Mobile__c, EON_Alt_Phone__c, EON_Alt_Phone_Type__c, 
		Address_Line_1__c, Address_Line_2__c, City__c, State__c, Postal_Code__c, Country__c, 
		EON_CC_Address_Id__c, EON_Legacy_Address_Id__c, 
		IsDeleted, OwnerId, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from Address__c 
	limit 10

	-- EON_Preferences__c
	select id, Name, 
		EON_Account__c, 
		EON_Method__c, EON_Frequency__c, EON_Opt_in__c, 
		EON_Legacy_Preference_Id__c, 
		IsDeleted, OwnerId, 
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from EON_Preferences__c 
	limit 10

	-- Case
		-- Add: Order__c - EON_Customer_Order_Number__c - EON_Site__c - EON_LOT_Batch_Number__c - EON_Product_Family__c - EON_Product_Name__c
			-- EON_Browser__c - EON_Device_Type__c
			-- Returns: EON_Returns_Status__c
			-- Reviews: EON_Review_Type__c
	select id, RecordTypeId, CaseNumber, 
		Status, Origin, ParentId, EON_Case_Category__c, EON_Case_Reason__c, 
		AccountId, EON_Order_Status__c, EON_Legacy_Order_Number__c, CL_Legacy_Site__c, 
		EON_Wearer__c, EON_Case_Contact__c, 
		Subject, Description,
		EON_Reported_Order_Status__c, EON_Reported_Value__c, EON_Action_Requested__c, EON_OM_Actions_Taken__c, EON_Payment_Portal__c, EON_Payment_Action_Notes__c, EON_Investigation_Status__c, 
		EON_Items_Refunded__c, EON_Refund_Reason__c, EON_Refund_Value__c, EON_Refund_Action_Notes__c, EON_Refund_Status__c, 
		EON_GDPR_Language__c, EON_GDPR_Date_Actioned__c, EON_GDPR_Action_Notes__c, EON_Customer_Complaint_Made__c,  
		EON_Fulfilment_Action__c, 
		EON_Warehouse__c, EON_Returned_Order_Line_s__c, EON_Quantity__c, EON_Condition__c, EON_Customer_Request__c, EON_Reason_Code__c, EON_Alternative_Exchange__c, EON_Returns_Value__c, 
		EON_Review_Date__c, EON_Star_Rating__c, EON_Product_Type__c, EON_Product_Name__c, EON_Published_Response__c, EON_Reported__c, 
		EON_Tracking_Delivery_Date__c, EON_Customer_Delivery_Date__c, EON_Estimated_Dispatch__c, EON_Actual_Dispatch__c, EON_Estimated_Delivery__c, EON_Actual_Delivery__c, 
		EON_Delivery_Method__c, EON_What_Happened_Factual__c, EON_CSC_Contact__c, EON_CSC_Issue__c, EON_Goodwill_Issued__c, EON_Revised_Star_Rating__c, EON_Removed__c, 
		EON_Dispute_Raised_Date__c, EON_Response_Due_Date__c, EON_Dispute_ID__c, EON_Address_with_Paypal__c, EON_Email_Address_with_Paypal__c, EON_Transaction_ID__c, EON_Dispute_Amount__c, EON_Customer_Comments__c, EON_Response_Date__c, 
		EON_Case_Closure_Reason__c, EON_Resolution_Details__c, 
		ClosedDate, SuppliedName, SuppliedEmail, SuppliedPhone, ContactId, 
		EON_Fraud_Case__c, EON_Rx_Check_Case__c, 
		EON_Hour_Created_GMT__c, EON_Quarter_Hour_Created_GMT__c, EON_Hour_Closed_GMT__c, EON_Quarter_Hour_Closed_GMT__c, 
		EON_First_Assigned_Date__c, EON_Hour_First_Assigned_GMT__c, EON_Quarter_Hour_First_Assigned_GMT__c, 
		EON_First_Response_Date__c, EON_Time_to_First_Response_hours__c, 
		EON_First_Contact_Date__c, EON_First_Contact_Resolution__c, 
		EON_Resolution_Time_hours__c, 
		EON_Handling_Time_minutes__c, EON_Time_Entered_Handling__c, 
		EON_Original_Owner__c, EON_Is_Reassigned__c, EON_Original_Owner_Is_Viewing__c, EON_Count_of_Owners__c,
		IsDeleted, OwnerId,  
		CreatedDate, LastModifiedDate, 
		CreatedById, LastModifiedById 
	from Case 
	limit 10
