
select top 1000 *
from Landing.sf_sc.Account

	select top 1000 count(*)
	from Landing.sf_sc.Account_aud
	
	select convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*)
	from Landing.sf_sc.Account_aud
	group by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
	order by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)

	select substring(EON_Legacy_Customer_Id__c, 1, 1), count(*)
	from Landing.sf_sc.Account_aud
	group by substring(EON_Legacy_Customer_Id__c, 1, 1)
	order by substring(EON_Legacy_Customer_Id__c, 1, 1)



select top 1000 *
from Landing.sf_sc.Address__c

	select top 1000 count(*)
	from Landing.sf_sc.Address__c_aud
	
	select convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*)
	from Landing.sf_sc.Address__c_aud
	group by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
	order by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)

	select substring(EON_Legacy_Address_Id__c, 1, 1), count(*)
	from Landing.sf_sc.Address__c_aud
	group by substring(EON_Legacy_Address_Id__c, 1, 1)
	order by substring(EON_Legacy_Address_Id__c, 1, 1)


select top 1000 *
from Landing.sf_sc.EON_Preferences__c

	select top 1000 count(*)
	from Landing.sf_sc.EON_Preferences__c_aud
	
	select convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*)
	from Landing.sf_sc.EON_Preferences__c_aud
	group by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
	order by convert(date, ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)

	select substring(EON_Legacy_Preference_Id__c, 1, 1), count(*)
	from Landing.sf_sc.EON_Preferences__c_aud
	group by substring(EON_Legacy_Preference_Id__c, 1, 1)
	order by substring(EON_Legacy_Preference_Id__c, 1, 1)


