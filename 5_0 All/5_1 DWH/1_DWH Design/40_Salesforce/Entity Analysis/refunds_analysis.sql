
	drop table #sales_flat_creditmemo_aud

	select entity_id, order_id
	into #sales_flat_creditmemo_aud
	from Landing.mag.sales_flat_creditmemo_aud
	where order_id in (757758, 1832853, 11930854, 11994484, 11251050)



select top 1000 count(*) over (), entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
	base_grand_total, 
	base_adjustment, base_adjustment_positive, base_adjustment_negative, mutual_amount,
	base_to_global_rate, base_to_order_rate, order_currency_code
from Landing.mag.sales_flat_creditmemo_aud
-- where entity_id = 1366227
where order_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id, entity_id 

	select count(*), count(distinct entity_id), count(distinct order_id), count(distinct invoice_id)
	from Landing.mag.sales_flat_creditmemo_aud


select top 1000 count(*) over (), entity_id, order_item_id, parent_id,
	product_id, sku, name, description, 
	qty, 
	base_price, price, base_price_incl_tax, 
	base_cost, 
	base_row_total, base_discount_amount
from Landing.mag.sales_flat_creditmemo_item_aud
where parent_id in (select distinct entity_id from #sales_flat_creditmemo_aud)
order by parent_id, entity_id 

	select count(*), count(distinct entity_id), count(distinct parent_id), count(distinct order_item_id)
	from Landing.mag.sales_flat_creditmemo_item_aud

----------------------------

-- aux.mag_sales_flat_creditmemo used for: 
	-- set creditmemo_id, refund_date // order_status_name_bk, adjustment_order // total values xx_refund
select top 1000 
	count(*) over (), count(*) over (partition by order_id) num_rep,
	order_id, num_order, 
	entity_id, increment_id, created_at,
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
from Landing.aux.mag_sales_flat_creditmemo
-- where entity_id = 1366227
where order_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by num_rep desc, order_id, entity_id 

	select count(*), count(distinct entity_id), count(distinct order_id)
	from Landing.aux.mag_sales_flat_creditmemo
	where num_order = 1

	select num_order, num_rep_rows, count(*), count(distinct entity_id), count(distinct order_id)
	from Landing.aux.mag_sales_flat_creditmemo
	group by num_order, num_rep_rows 
	order by num_order, num_rep_rows

	select num_rep, num_order, num_rep_rows, count(*), count(distinct entity_id), count(distinct order_id)
	from 
		(select count(*) over (partition by order_id) num_rep,
			num_order, num_rep_rows, 
			order_id, entity_id
		from Landing.aux.mag_sales_flat_creditmemo) t
	group by num_rep, num_order, num_rep_rows 
	order by num_rep, num_order, num_rep_rows

-- aux.mag_sales_flat_creditmemo used for: 
	-- set creditmemo_id, creditmemo_no, refund_date // local_discount_refund
select top 1000 count(*) over (), order_id, order_item_id, 
	entity_id, increment_id, created_at, item_id,
	num_rep_rows, 
	sum_qty, sum_base_row_total, sum_base_discount_amount
from Landing.aux.mag_sales_flat_creditmemo_item
where order_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id, entity_id 

	select count(*), count(distinct entity_id), count(distinct order_id), count(distinct order_item_id)
	from Landing.aux.mag_sales_flat_creditmemo_item


----------------------------
----------------------------
----------------------------

select entity_id, increment_id, created_at, updated_at, status,
	base_customer_balance_refunded, base_total_refunded, bs_customer_bal_total_refunded

	--base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	--base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	--base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	--base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	--base_grand_total, 
	--base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, 
	--base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due
from Landing.mag.sales_flat_order_aud
where entity_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by entity_id

select top 1000 item_id, order_id, created_at, updated_at, 
	product_id, sku, name, 
	qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
	base_row_total, 
	base_discount_amount, 
	base_amount_refunded
from Landing.mag.sales_flat_order_item_aud
where order_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id, item_id

select *
from Landing.mag.sales_flat_order_payment_aud
where parent_id in (select distinct order_id from #sales_flat_creditmemo_aud)
order by parent_id

	select count(*), count(distinct entity_id), count(distinct parent_id)
	from Landing.mag.sales_flat_order_payment_aud

----------------------------
----------------------------
----------------------------

select top 1000 order_id_bk, invoice_id, shipment_id, creditmemo_id,
	order_no, invoice_no, shipment_no, creditmemo_no, 
	order_date, invoice_date, shipment_date, refund_date, 
	total_qty_unit, total_qty_unit_refunded, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat,
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_payment_online, local_payment_refund_online
from Landing.aux.sales_dim_order_header_aud
where order_id_bk in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id_bk

select order_line_id_bk, order_id_bk, invoice_id, shipment_id, creditmemo_id,
	order_no, invoice_no, shipment_no, creditmemo_no, 
	invoice_date, shipment_date, refund_date, 
	qty_unit, qty_unit_refunded,
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat,
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_payment_online, local_payment_refund_online
from Landing.aux.sales_fact_order_line_aud
where order_id_bk in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id_bk, order_line_id_bk


-----

select top 1000 order_id_bk, invoice_id, shipment_id, creditmemo_id,
	order_no, invoice_no, shipment_no, creditmemo_no, 
	order_date, invoice_date, shipment_date, refund_date, 
	total_qty_unit, total_qty_unit_refunded, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat,
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_payment_online, local_payment_refund_online
from Warehouse.sales.dim_order_header_v
where order_id_bk in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id_bk

select order_line_id_bk, order_id_bk, invoice_id, shipment_id, creditmemo_id,
	order_no, invoice_no, shipment_no, creditmemo_no, 
	invoice_date, shipment_date, refund_date, 
	qty_unit, qty_unit_refunded,
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat,
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_payment_online, local_payment_refund_online
from Warehouse.sales.fact_order_line_v
where order_id_bk in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id_bk

-- Warehouse.sales.fact_order_line_all

select top 1000 *
from Warehouse.sales.fact_order_line_all

-- Warehouse.sales.fact_order_line_all_v

select top 1000 *
from Warehouse.sales.fact_order_line_all_v
where order_id_bk in (select distinct order_id from #sales_flat_creditmemo_aud)
order by order_id_bk