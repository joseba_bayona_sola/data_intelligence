
select 
	pim_attribute_name, pim_attribute_area, pim_attribute_group, pim_attribute_id, 
	cc_attribute_name, cc_attribute_id, cc_data_type, cc_attribute_settings, 
	field_description
from DW_GetLenses_jbs.dbo.product_DM

	select pim_attribute_area, count(*)
	from DW_GetLenses_jbs.dbo.product_DM
	group by pim_attribute_area
	order by pim_attribute_area

	select pim_attribute_area, pim_attribute_group, count(*)
	from DW_GetLenses_jbs.dbo.product_DM
	group by pim_attribute_area, pim_attribute_group 
	order by pim_attribute_area, pim_attribute_group 

	select pim_attribute_group, count(*), count(distinct pim_attribute_area)
	from DW_GetLenses_jbs.dbo.product_DM
	group by pim_attribute_group 
	order by pim_attribute_group 

-----------------------------------------------

select 
	pim_attribute_name, pim_attribute_area, pim_attribute_group, pim_attribute_id, 
	cc_attribute_name, cc_attribute_id, cc_data_type, cc_attribute_settings, 
	field_description
from DW_GetLenses_jbs.dbo.product_DM
-- where pim_attribute_area = 'Product - Non-Prescription Eyewear' -- Product / System / Lifecycle / Product - Contact Lenses / Product - Prescription Glasses / Product - Lens Package Flow / Product - Non-Prescription Eyewear
where pim_attribute_area = ''
order by pim_attribute_area, pim_attribute_group 
