
select hla_group_id, hla_functional_area, group_n, from_n, to_n, 
	num, id, logical_arch_name, hla_name, gd_vd, vd_ireland, description
from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
where vd_ireland = 'Yes'
order by convert(int, hla_group_id), hla_functional_area, group_n, from_n, to_n, logical_arch_name, hla_name

	select count(*), count(distinct id), count(distinct hla_group_id), count(distinct hla_functional_area), count(distinct group_n), count(distinct from_n), count(distinct to_n), 
		count(distinct logical_arch_name), count(distinct hla_name)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1

	select hla_group_id, hla_functional_area, count(*), 
		count(case when (vd_ireland = 'Yes') then 1 else null end) num_ie_yes, 
		count(case when (vd_ireland <> 'Yes') then 1 else null end) num_ie_yes
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by hla_group_id, hla_functional_area
	order by convert(int, hla_group_id), hla_functional_area

	select group_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by group_n
	order by group_n

	select from_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by from_n
	order by from_n

	select to_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by to_n
	order by to_n

	select gd_vd, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by gd_vd
	order by gd_vd

	select vd_ireland, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by vd_ireland
	order by vd_ireland

	select id, charindex('-', id), 
		case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end,		
		count(*)
	from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
	group by id
	order by id

---------------------------------------------------------

select hla_group_id, hla_functional_area, group_n, from_n, to_n, 
	num, case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end new_id, id, logical_arch_name, hla_name, gd_vd, vd_ireland, description
from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
-- where from_n = ''
where to_n = 'SFCC'
order by convert(int, hla_group_id), hla_functional_area, group_n, from_n, to_n, logical_arch_name, hla_name