
select type_n, group_n, from_n, to_n, id, name, 
	count(*) over (partition by id) num_rep_id, 
	count(*) over (partition by name) num_rep_name
from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
order by type_n, group_n, from_n, to_n, id

	select count(*), count(distinct id), count(distinct name), count(distinct group_n), count(distinct from_n), count(distinct to_n), count(distinct type_n)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture

	select type_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by type_n
	order by type_n

	select type_n, group_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by type_n, group_n
	order by type_n, group_n

	select group_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by group_n
	order by group_n

	select from_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by from_n
	order by from_n

	select to_n, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by to_n
	order by to_n

	select id, count(*)
	from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
	group by id
	order by id

---------------------------------------------------

select type_n, group_n, from_n, to_n, id, name, 
	count(*) over (partition by id) num_rep_id, 
	count(*) over (partition by name) num_rep_name
from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
-- where from_n = 'SFMC' -- SFCC - SFCC (Browser) - SFMC - SFSC - Order Services - Availability Services - Product Services - Accounting Services - BU Systems
where to_n = 'SFCC' -- SFCC - SFMC - SFSC - Datawarehouse - Order Services - Availability Services - Product Services - Accounting Services - BU Systems
order by type_n, group_n, from_n, to_n, id

---------------------------------------------------

select t1.id, t2.id_new, t1.num1, t2.num2
from
		(select case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end id, count(*) num1
		from DW_GetLenses_jbs.dbo.sf_al_logical_architecture
		group by case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end) t1
	full join
		(select case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end id_new,	count(*) num2
		from DW_GetLenses_jbs.dbo.sf_al_interfaces_1
		group by case when (charindex('-', id) <> 0) then substring(id, 1, charindex('-', id) -1) else id end) t2 on t1.id = t2.id_new
order by t1.id, t2.id_new


select t2.vd_ireland, 
	t2.hla_group_id, t2.hla_functional_area, t1.type_n, 
	t1.group_n, t1.from_n, t1.to_n, 
	t1.id, t2.id, t1.name, t2.logical_arch_name, t2.hla_name, t2.gd_vd, t2.description
from 
	DW_GetLenses_jbs.dbo.sf_al_logical_architecture t1
full join
	DW_GetLenses_jbs.dbo.sf_al_interfaces_1 t2 on 
		case when (charindex('-', t1.id) <> 0) then substring(t1.id, 1, charindex('-', t1.id) -1) else t1.id end = case when (charindex('-', t2.id) <> 0) then substring(t2.id, 1, charindex('-', t2.id) -1) else t2.id end
-- where from_n = 'SFMC' -- SFCC - SFCC (Browser) - SFMC - SFSC - Order Services - Availability Services - Product Services - Accounting Services - BU Systems
-- where t1.to_n = 'SFCC' -- SFCC - SFMC - SFSC - Datawarehouse - Order Services - Availability Services - Product Services - Accounting Services - BU Systems
order by convert(int, t2.hla_group_id), t2.hla_functional_area, t1.type_n, t1.group_n, t1.from_n, t1.to_n, t1.id