
---------------------------------------------------------------------------------
-------------------------------- Suppliers --------------------------------------
---------------------------------------------------------------------------------

create table Staging.gen.dim_supplier(
	supplier_id_bk				bigint NOT NULL,
	supplier_type_bk			varchar(50) NOT NULL,
	supplier_name				varchar(200), 
	default_lead_time			int,
	default_transit_lead_time	int,
	discount_amount				decimal(12, 4),
	discount_date_from			date, 
	discount_date_to			date, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

---------------------------------------------------------------------------------
-------------------------------- Warehouse --------------------------------------
---------------------------------------------------------------------------------

create table Staging.gen.dim_warehouse(
	warehouseid_bk				bigint NOT NULL,
	warehouse_name				varchar(200), 
	wh_short_name				varchar(200), 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go


---------------------------------------------------------------------------------
-------------------------------- Product // Stock -------------------------------
---------------------------------------------------------------------------------

create table Staging.prod.dim_product_family_pack_size(
	packsizeid_bk					bigint NOT NULL,
	product_id_bk					int,
	size							int,
	product_family_packsize_name	varchar(200),
	allocation_preference_order		int,
	purchasing_preference_order		int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.prod.dim_product_family_price(
	supplierpriceid_bk				bigint NOT NULL,
	
	supplier_id_bk					bigint NOT NULL, 
	packsizeid_bk					bigint NOT NULL,
	price_type_pf_bk				varchar(50) NOT NULL,

	unit_price						decimal(12, 4), 
	currency_code					varchar(4),
	lead_time						int,
	min_qty							int,
	max_qty							int,
	package_no						int,
	
	effective_date					date,
	expiry_date						date,
	active							char(1),
	last_update						datetime2,

	direct							BIT,
	auto_edi						char(1),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go


create table Staging.prod.dim_product(
	productid_erp_bk				bigint NOT NULL,
	
	product_id_bk					int,
	base_curve_bk					char(3), 
	diameter_bk						char(4), 
	power_bk						char(6), 
	cylinder_bk						char(5), 
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1),
	colour_bk						varchar(50),

	parameter						varchar(200),
	product_description				varchar(200),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.prod.dim_stock_item(
	stockitemid_bk					bigint NOT NULL,
	
	productid_erp_bk				bigint,
	packsize						int, 

	SKU								varchar(200),
	stock_item_description			varchar(200),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go


create table Staging.stock.dim_wh_stock_item(
	warehousestockitemid_bk			bigint NOT NULL, 

	warehouseid_bk					bigint NOT NULL, 
	stockitemid_bk					bigint NOT NULL, 
	stock_method_type_bk			varchar(50) NOT NULL, 

	wh_stock_item_description		varchar(200),

	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 
	qty_due_in						decimal(28, 8), 

	stocked							int,

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.stock.dim_wh_stock_item_batch(
	warehousestockitembatchid_bk	bigint NOT NULL, 

	warehousestockitemid_bk			bigint NOT NULL,
	stock_batch_type_bk				varchar(50) NOT NULL, 
	receiptlineid_bk				bigint,

	batch_id						bigint,

	fully_allocated_f				int, 
	fully_issued_f					int, 
	auto_adjusted_f					int, 
	
	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	qty_registered_sm				decimal(28, 8), 
	qty_disposed_sm					decimal(28, 8), 

	batch_arrived_date				datetime,
	batch_confirmed_date			datetime,
	batch_stock_register_date		datetime,

	local_product_unit_cost			decimal(28, 8), 
	local_carriage_unit_cost		decimal(28, 8), 
	local_duty_unit_cost			decimal(28, 8), 
	local_total_unit_cost			decimal(28, 8), 

	local_interco_carriage_unit_cost	decimal(28, 8), 
	local_total_unit_cost_interco		decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	delete_f						char(1), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.stock.fact_wh_stock_item_batch_movement(
	batchstockmovementid_bk			bigint NOT NULL, 

	batch_stock_move_id				bigint NOT NULL, 
	move_id							varchar(20) NOT NULL,
	move_line_id					varchar(20) NOT NULL,

	warehousestockitembatchid_bk	bigint NOT NULL,
	stock_adjustment_type_bk		varchar(50) NOT NULL, 
	stock_movement_type_bk			varchar(50) NOT NULL, 
	stock_movement_period_bk		varchar(50), 

	qty_movement					decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	batch_movement_date				datetime,
	batch_movement_create_date		datetime,

	movement_reason					varchar(50), 
	movement_comment				varchar(200), 
	operator						varchar(20), 

	delete_f						char(1), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.stock.dim_intransit_stock_item_batch(
	intransitstockitembatchid_bk	bigint NOT NULL, 

	transferheaderid_bk				bigint NOT NULL, 
	stockitemid_bk					bigint NOT NULL, 

	qty_sent						decimal(28, 8), 
	qty_remaining					decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_missing						decimal(28, 8), 

	intransit_batch_created_date	datetime,
	batch_created_date				datetime,
	batch_arrived_date				datetime,
	batch_confirmed_date			datetime,
	batch_stock_register_date		datetime,

	local_product_unit_cost			decimal(28, 8), 
	local_carriage_unit_cost		decimal(28, 8), 
	local_total_unit_cost			decimal(28, 8), 

	local_interco_carriage_unit_cost	decimal(28, 8), 
	local_total_unit_cost_interco		decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	wh_stock_item_batch_f			char(1),
	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

---------------------------------------------------------------------------------
-------------------------------- Purchase Orders --------------------------------
---------------------------------------------------------------------------------

create table Staging.po.dim_wh_operator(
	wh_operator_bk					varchar(200) NOT NULL,
	wh_operator_name				varchar(200) NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.po.dim_wh_operator_account(
	accountid_bk					bigint NOT NULL,
	wh_operator_account_name		varchar(200) NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.po.dim_purchase_order(
	purchaseorderid_bk				bigint NOT NULL,

	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	po_source_bk					varchar(50), 
	po_type_bk						varchar(50),
	po_status_bk					varchar(50),
	wh_operator_cr_bk				varchar(200), 
	wh_operator_as_bk				varchar(200), 
	accountid_cr_bk					bigint, 
	accountid_as_bk					bigint, 

	purchase_order_number			varchar(20), 
	leadtime						int, 
	supplier_reference				varchar(200),

	created_date					datetime,
	approved_date					datetime,
	confirmed_date					datetime,

	submitted_date					datetime,
	completed_date					datetime,
	due_date						datetime,
	received_date					datetime, 

	local_total_cost				decimal(28, 8), 

	local_to_wh_rate				decimal(12, 4),
	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.po.fact_purchase_order_line(
	purchaseorderlineid_bk			bigint NOT NULL,

	purchaseorderid_bk				bigint NOT NULL,
	pol_status_bk					varchar(50),
	pol_problems_bk					varchar(50), 
	stockitemid_bk					bigint,
	supplierpriceid_bk				bigint,

	purchase_order_line_id			bigint,

	quantity_ordered				decimal(28, 8), 
	quantity_received				decimal(28, 8), 
	quantity_planned				decimal(28, 8), 
	quantity_open					decimal(28, 8), 
	quantity_cancelled				decimal(28, 8), 

	local_unit_cost					decimal(28, 8),
	local_line_cost					decimal(28, 8),

	local_to_wh_rate				decimal(12, 4),
	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.po.dim_intersite_transfer(
	transferheaderid_bk				bigint NOT NULL,

	purchaseorderid_t_bk			bigint NOT NULL, 
	purchaseorderid_s_bk			bigint, 
	orderid_bk						bigint, 
	it_allocation_strategy_bk		varchar(50), 
	warehouseid_from_bk				bigint,

	intersite_transfer_number		varchar(20), 

	created_date					datetime,

	delete_f						char(1),
	
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

---------------------------------------------------------------------------------
-------------------------------- Receipts ---------------------------------------
---------------------------------------------------------------------------------


create table Staging.rec.dim_wh_shipment(
	shipment_id_bk					varchar(20) NOT Null, 

	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	wh_shipment_type_bk				varchar(50), 

	shipment_number					varchar(20), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.rec.dim_receipt(
	receiptid_bk					bigint NOT NULL, 

	shipment_id_bk					varchar(20) NOT Null, 
	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	receipt_status_bk				varchar(50), 

	receipt_number					varchar(20), 
	receipt_no						int, 
	supplier_advice_ref				varchar(15),

	created_date					datetime, 
	arrived_date					datetime,
	confirmed_date					datetime, 
	stock_registered_date			datetime, 
	due_date						datetime,

	local_total_cost				decimal(28, 8), 

	local_to_wh_rate				decimal(12, 4), 
	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.rec.fact_receipt_line(
	receiptlineid_bk				bigint NOT NULL, 

	receiptid_bk					bigint NOT NULL,
	receipt_line_status_bk			varchar(50), 
	receipt_line_sync_status_bk		varchar(50), 
	purchaseorderlineid_bk			bigint, 
	stockitemid_bk					bigint, 

	created_date					datetime, 

	qty_ordered						decimal(28, 8), 
	qty_received					decimal(28, 8), 
	qty_accepted					decimal(28, 8), 
	qty_returned					decimal(28, 8), 
	qty_rejected					decimal(28, 8), 

	local_unit_cost					decimal(28, 8), 
	local_total_cost				decimal(28, 8), 

	local_invoice_unit_cost			decimal(28, 8), 
	local_invoice_total_cost		decimal(28, 8),

	local_to_wh_rate				decimal(12, 4), 
	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	wh_stock_item_batch_f			char(1),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

---------------------------------------------------------------------------------
-------------------------------- Inv Reconciliation -----------------------------
---------------------------------------------------------------------------------


create table Staging.invrec.dim_invoice_reconciliation(
	invoiceid_bk					bigint NOT NULL, 

	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	invrec_status_bk				varchar(50),
	accountid_cr_bk					bigint, 

	invoice_ref						varchar(200),
	net_suite_no					varchar(200),

	invoice_date					datetime, 
	approved_date					datetime, 
	posted_date						datetime, 
	payment_due_date				datetime, 

	invoice_goods_amount			decimal(28,8),
	invoice_carriage_amount			decimal(28,8),
	invoice_discount_amount			decimal(28,8),
	invoice_import_duty_amount		decimal(28,8),
	invoice_net_amount				decimal(28,8),
	invoice_gross_amount			decimal(28,8),
	invoice_vat_amount				decimal(28,8),
	invoice_reconciled_amount		decimal(28,8),

	erp_goods_amount				decimal(28,8),
	erp_carriage_amount				decimal(28,8),
	erp_discount_amount				decimal(28,8),
	erp_import_duty_amount			decimal(28,8),
	erp_net_amount					decimal(28,8),
	erp_gross_amount				decimal(28,8),
	erp_vat_amount					decimal(28,8),
	erp_adjusted_amount				decimal(28,8),

	net_difference					decimal(28,8),
	credit_received					decimal(28,8),

	local_to_wh_rate				decimal(12, 4),
	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 


create table Staging.invrec.fact_invoice_reconciliation_line(
	invoicelineid_bk				bigint NOT NULL, 

	invoiceid_bk					bigint NOT NULL, 
	stockitemid_bk					bigint, 
	receiptlineid_bk				bigint,

	quantity						numeric(28,8),

	unit_cost						numeric(28,8),
	total_cost						decimal(28,8),

	adjusted_unit_cost				decimal(28,8),
	adjusted_total_cost				decimal(28,8),
	adjustment_applied				decimal(28,8),

	local_to_wh_rate				decimal(12, 4),
	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),
	currency_code_wh				varchar(10),

	delete_f						char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

---------------------------------------------------------------------------------
-------------------------------- Allocation // WH Invoicing ---------------------
---------------------------------------------------------------------------------


create table Staging.alloc.dim_order_header_erp(
	orderid_bk								bigint NOT NULL, 

	order_no_erp							varchar(30),
	order_id_bk								bigint, 

	idCalendarOrderDateSync					int NOT NULL, 
	order_date_sync							datetime NOT NULL,

	idCalendarSNAPCompleteDate				int, 
	snap_complete_date						datetime,
	idTimeSNAPCompleteDate					varchar(10),
	idCalendarExpectedShipmentDate			int, 
	expected_shipment_date					datetime,
	idCalendarCageShipmentDate				int, 
	cage_shipment_date						datetime,
	idTimeCageShipmentDate					varchar(10),

	idCalendarAllocationDate				int, 
	allocation_date							datetime,
	idCalendarIssueDate						int, 
	issue_date								datetime,

	order_type_erp_f						char(1) NOT NULL, 
	order_type_erp_bk						varchar(50) NOT NULL,
	order_status_erp_bk						varchar(50) NOT NULL,
	shipment_status_erp_bk					varchar(50) NOT NULL,
	allocation_status_bk					varchar(50) NOT NULL,
	allocation_strategy_bk					varchar(50) NOT NULL,

	country_id_shipping_bk					char(2),
	warehouseid_pref_bk						bigint,

	local_subtotal							decimal(28, 8),
	local_shipping							decimal(28, 8),
	local_discount							decimal(28, 8),
	local_store_credit_used					decimal(28, 8),
	local_total_inc_vat						decimal(28, 8),

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.fact_order_line_erp(
	orderlineid_bk							bigint NOT NULL, 

	order_line_id_bk						bigint, 

	orderid_bk								bigint NOT NULL,
	productid_erp_bk						bigint NOT NULL,

	bom_f									char(1) NOT NULL, 

	qty_unit								decimal(28, 8), 
	qty_unit_allocated						decimal(28, 8), 

	local_price_unit						decimal(28, 8), 
	local_subtotal							decimal(28, 8), 

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.fact_order_line_mag_erp(
	order_line_id_bk						bigint NOT NULL,
	orderlineid_bk							bigint NOT NULL, 

	orderlinemagentoid_bk					bigint NOT NULL, 

	idCalendarSNAPCompleteDate				int, 
	snap_complete_date						datetime,
	idTimeSNAPCompleteDate					varchar(10),
	idCalendarExpectedShipmentDate			int, 
	expected_shipment_date					datetime,
	idCalendarCageShipmentDate				int, 
	cage_shipment_date						datetime,
	idTimeCageShipmentDate					varchar(10),

	deduplicate_f							char(1), 

	qty_unit_mag							decimal(28, 8), 
	qty_unit_erp							decimal(28, 8), 
	qty_percentage							decimal(28, 8), 

	local_total_cost_mag					decimal(28, 8), 
	local_total_cost_erp					decimal(28, 8), 

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.fact_order_line_erp_issue(
	batchstockissueid_bk						bigint NOT NULL, 
	orderlinestockallocationtransactionid_bk	bigint NOT NULL,

	issue_id									bigint, 

	orderlineid_bk								bigint NOT NULL, 

	idCalendarSNAPCompleteDate					int, 
	snap_complete_date							datetime,
	idTimeSNAPCompleteDate						varchar(10),
	idCalendarExpectedShipmentDate				int, 
	expected_shipment_date						datetime,
	idCalendarCageShipmentDate					int, 
	cage_shipment_date							datetime,
	idTimeCageShipmentDate						varchar(10),

	idCalendarAllocationDate					int, 
	allocation_date								datetime,
	idCalendarIssueDate							int, 
	issue_date									datetime,

	allocation_type_bk							varchar(50) NOT NULL,
	allocation_record_type_bk					varchar(50) NOT NULL,
	cancelled_f									char(1) NOT NULL, 

	warehouseid_bk								bigint, 
	stockitemid_bk								bigint,
	 
	warehousestockitembatchid_bk				bigint NOT NULL, 
	receiptlineid_bk							bigint,
	warehousestockitembatchid_rec_bk			bigint,
	batchstockmovementid_bk						bigint, 

	qty_unit_order_line							decimal(28, 8), 
	qty_unit									decimal(28, 8), 
	qty_stock									decimal(28, 8), 

	qty_percentage								decimal(28, 8),

	local_total_unit_cost						decimal(28, 8), 
	local_total_cost							decimal(28, 8), 

	local_total_unit_cost_rec					decimal(28, 8), 
	local_total_cost_rec						decimal(28, 8), 

	local_to_global_rate						decimal(12, 4),
	local_to_global_rate_rec					decimal(12, 4),
	order_currency_code							varchar(255),

	idETLBatchRun								bigint NOT NULL, 
	ins_ts										datetime NOT NULL);
go



create table Staging.gen.dim_wholesale_customer(
	wholesale_customer_id_bk		bigint NOT NULL,

	customer_type_name_bk			varchar(50) NOT NULL,
	wholesale_customer_type_name_bk	varchar(50) NOT NULL,
	warehouseid_bk					bigint NOT NULL,

	name							varchar(100),
	order_currency_code				varchar(255),
	 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

create table Staging.alloc.dim_wholesale_order_header_erp(
	orderid_bk								bigint NOT NULL,  

	wholesale_customer_id_bk				bigint NOT NULL, 

	customer_ref							varchar(100),
	due_date								datetime,

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.dim_wholesale_order_invoice(
	customerinvoiceid_bk					bigint NOT NULL, 

	invoice_no								varchar(200), 

	idCalendarInvoiceDate					int, 
	invoice_date							datetime,

	wholesale_order_invoice_status_bk		varchar(50),

	qty_pack								decimal(28, 8), 

	local_subtotal							decimal(28, 8), 
	local_shipping							decimal(28, 8), 
	local_total_inc_vat						decimal(28, 8), 
	local_total_exc_vat						decimal(28, 8), 
	local_total_vat							decimal(28, 8), 
	local_total_prof_fee					decimal(28, 8), 

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	delete_f								char(1), 

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.dim_wholesale_order_shipment(
	customershipmentinvoiceid_bk			bigint NOT NULL, 

	shipment_no								varchar(20), 

	customerinvoiceid_bk					bigint NOT NULL, 
	orderid_bk								bigint NOT NULL, 
	wholesale_customer_id_bk				bigint NOT NULL, 

	warehouseid_bk							bigint NOT NULL, 

	qty_pack								decimal(28, 8), 

	local_subtotal							decimal(28, 8), 
	local_shipping							decimal(28, 8), 
	local_total_inc_vat						decimal(28, 8), 
	local_total_exc_vat						decimal(28, 8), 
	local_total_vat							decimal(28, 8), 
	local_total_prof_fee					decimal(28, 8), 

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	delete_f								char(1), 

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go

create table Staging.alloc.dim_wholesale_order_invoice_line(
	customerinvoicelineid_bk				bigint NOT NULL, 

	customershipmentinvoiceid_bk			bigint NOT NULL, 
	stockitemid_bk							bigint NOT NULL, 

	qty_pack								decimal(28, 8), 
	price_pack								decimal(28, 8), 

	local_subtotal							decimal(28, 8), 
	local_shipping							decimal(28, 8), 
	local_total_inc_vat						decimal(28, 8), 
	local_total_exc_vat						decimal(28, 8), 
	local_total_vat							decimal(28, 8), 
	local_total_prof_fee					decimal(28, 8), 

	local_to_global_rate					decimal(12, 4),
	order_currency_code						varchar(255),

	vat_percent								decimal(28, 8), 

	delete_f								char(1), 

	idETLBatchRun							bigint NOT NULL, 
	ins_ts									datetime NOT NULL);
go