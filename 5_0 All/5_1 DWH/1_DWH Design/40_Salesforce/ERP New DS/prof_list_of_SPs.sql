
select *
from ControlDB.config.t_SP
where sp_name like 'srcmend%'
order by idSP desc

select *
from ControlDB.config.t_SP
where sp_name like 'stg_dwh_get_prod%' or sp_name like 'stg_dwh_get_stock%' or sp_name like 'stg_dwh_get_po%' or sp_name like 'stg_dwh_get_rec%' or sp_name like 'stg_dwh_get_alloc%' or sp_name like 'stg_dwh_get_invrec%' or sp_name like 'stg_dwh_get_frcons%'
order by sp_name

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, datediff(minute, spm.startTime, spm.finishTime) duration,
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_merge_prod_stock_item'
	order by spm.finishTime desc, spm.messageTime