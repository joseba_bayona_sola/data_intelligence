
select *
from wearer_order_item
order by id desc
limit 1000

-- customercomments
select comment_id, customer_id, author_id, comment, created_at
from customercomments
where customer_id = 2383848
order by created_at desc

  select count(*)
  from customercomments

-- sales_flat_order_status_history  
select status, label
from sales_order_status

select entity_id, parent_id, created_at, status, admin_username, entity_name, 
  is_customer_notified, is_visible_on_front, comment
from sales_flat_order_status_history
where parent_id = 11179079
order by entity_id desc
limit 1000
    
  select count(*), min(created_at), max(created_at)
  from sales_flat_order_status_history
  where created_at is not null
  
-- Global Collect
select token_id, payment_product_id, token, 
  customer_id, customer_name, 
  card_number, card_expiry_month, card_expiry_year, 
  initial_scheme_transaction_id, scheme_transaction_id, 
  created_at, updated_at
from globalcollect_token
order by token_id desc
limit 1000
  
  select payment_product_id, count(*)
  from globalcollect_token
  group by payment_product_id
  order by payment_product_id

select payment_method_id, code, name
from globalcollect_payment_method

select payment_product_id, parent_id, code, name, payment_method_id, is_standalone, is_selectable
from globalcollect_payment_product

select *
from globalcollect_token_address
order by token_id desc
limit 1000

select *
from globalcollect_payment_product_tokendata
order by id desc
limit 1000 

select *
from globalcollect_token_multiple_customer_id
limit 1000

select *
from globalcollect_token_tokendata
order by token_id desc
limit 1000

-- Cyber Source

select id, subscription_id, store_id, 
  customer_id, cname, address_id, order_address_id, addr_version, 
  active, ctype, clogo, cnumbermasked, cexpiry, 
  cc_request_id
  created_at, updated_at
from cybersourcestored_token
order by id desc
limit 1000

  select ctype, clogo, count(*)
  from cybersourcestored_token
  group by ctype, clogo
  order by ctype, clogo
  
select *
from cybersourcestored_token_tokendata
order by id desc
limit 1000 

-- Paypal

-- Ideal
  
-- Klarna

-- BCMC

-- Bank Transfer

-- COD

-- Cheque