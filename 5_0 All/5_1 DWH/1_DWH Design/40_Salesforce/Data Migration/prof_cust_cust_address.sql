
-- customer duplicates
select entity_id, email, email_dupl, created_at, 
	website_group_id, website_id, store_id,		
	num_rep, last_order_date,
	EON_Account__Record_Id__c, PersonContactId,
	migration_f, migration_f_final, delete_f,
	idETLBatchRun, ins_ts
from Landing.mag.customer_entity_duplicates
order by ins_ts desc;

-- customer deleted
select count(*) over (), c1.entity_id, c1.email, c1.website_group_id, c1.store_id, c1.created_at, c1.updated_at, 
	c3.customer_id, c3.registered_at, c3.deleted_at, c3.comment
from 
		Landing.mag.customer_entity_flat_aud c1
	left join
		Landing.mag.customer_entity_all c2 on c1.entity_id = c2.entity_id
	left join
		Landing.mag.customer_deleted_entity c3 on c1.entity_id = c3.customer_id
where c2.entity_id is null
	-- and c1.email not like 'XXXXX%'
order by c1.entity_id desc	

-- customer address deleted
select top 1000 count(*) over (), c1.entity_id, c1.parent_id, c1.firstname, c1.street, c1.city, c1.is_active, c1.created_at, c1.updated_at, 
	c3.customer_id, c3.registered_at, c3.deleted_at, c3.comment
from 
		Landing.mag.customer_address_entity_flat_aud c1
	left join
		Landing.mag.customer_address_entity_all c2 on c1.entity_id = c2.entity_id
	left join
		Landing.mag.customer_deleted_entity c3 on c1.parent_id = c3.customer_id
where c2.entity_id is null
	and c1.street not like 'XXXXX%'
order by c1.entity_id desc	
