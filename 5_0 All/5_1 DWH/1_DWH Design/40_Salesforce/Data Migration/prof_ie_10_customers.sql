
--select top 10 idCustomer_sk_fk, customer_id, customer_email, last_order_date,
--	num_tot_orders, num_tot_cancel, num_tot_refund, num_tot_store_credit_orders
--into DW_GetLenses_jbs.dbo.sfcc_customers_ie
--from Warehouse.act.fact_customer_signature_v
--where store_register = 'visiondirect.ie'
--	and num_tot_cancel <> 0 and num_tot_refund <> 0 and num_tot_store_credit_orders <> 0
--order by num_tot_orders desc

select *
from DW_GetLenses_jbs.dbo.sfcc_customers_ie

	-- Customer
	select ie.*, c.*
	from 
		DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
	inner join
		Landing.mag.customer_entity_flat_aud c on ie.customer_id = c.entity_id

	-- Customer Address
	select ie.*, c.*
	from 
		DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
	inner join
		Landing.mag.customer_address_entity_flat_aud c on ie.customer_id = c.parent_id
	order by ie.num_tot_orders desc, ie.customer_id, c.entity_id

	-- Customer Payment Instrument

	-- Store Credit

	select top 1000 ie.*,
		ecb.balance_id balance_id_bk, 
		ecb.customer_id customer_id_bk, cs.store_id store_id_bk, 
		ecbh.first_transaction_date, ecbh.last_transaction_date,
		ecb.amount customer_balance, ecb.base_currency_code currency_code
	from 
			DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
		inner join
			Landing.mag.enterprise_customerbalance_aud ecb on ie.customer_id = ecb.customer_id
		inner join
			(select website_id, min(store_id) store_id
			from Landing.mag.core_store
			group by website_id) cs on ecb.website_id = cs.website_id
		left join -- inner
			(select balance_id, min(updated_at) first_transaction_date, max(updated_at) last_transaction_date
			from Landing.mag.enterprise_customerbalance_history_aud
			group by balance_id) ecbh on ecb.balance_id = ecbh.balance_id
	order by ie.num_tot_orders desc, ie.customer_id, ecb.balance_id
	
	select ecbh.customer_id, ecbh.customer_email, ecbh.last_order_date, ecbh.num_tot_orders,
		ecbh.history_id_bk, ecbh.balance_id_bk, ecbh.action_bk, 
		oh.entity_id order_id_bk, rtrim(ltrim(replace(ecbh.order_no,'.',''))) order_no, 
		ecbh.customer_service_agent,
		ecbh.balance_amount, ecbh.transaction_amount, 
		ecbh.transaction_date, 
		ecbh.additional_info, ecbh.frontend_comment, ecbh.is_customer_notified
	from
				(select ecb.customer_id, ecb.customer_email, ecb.last_order_date, ecb.num_tot_orders,
					ecbh.history_id history_id_bk, 
					ecbh.balance_id balance_id_bk, ecbh.action action_bk, 
					case 
						when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, len(additional_info)) 
						when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) = 0) then substring(additional_info, charindex(' ', additional_info, 1) + 1, len(additional_info)) 
						when (ecbh.action in (4) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, charindex(',', additional_info, 1) - charindex('#', additional_info, 1) -1) 
						else null 
					end order_no, 
					case 
						when (ecbh.action in (1, 2)) then substring(additional_info, charindex('By admin:', additional_info, 1) + len('By admin:'), charindex('.', additional_info, 1) - (charindex('By admin:', additional_info, 1) + len('By admin:'))) 
						else null 
					end customer_service_agent,
					ecbh.balance_amount balance_amount, ecbh.balance_delta transaction_amount, 
					ecbh.updated_at transaction_date, 
					ecbh.additional_info, ecbh.frontend_comment, ecbh.is_customer_notified
				from 
						Landing.mag.enterprise_customerbalance_history_aud ecbh --_aud
					inner join
						(select ecb.*, ie.customer_email, ie.last_order_date, ie.num_tot_orders
						from 
								DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
							inner join
								Landing.mag.enterprise_customerbalance_aud ecb on ie.customer_id = ecb.customer_id
							inner join
								(select website_id, min(store_id) store_id
								from Landing.mag.core_store
								group by website_id) cs on ecb.website_id = cs.website_id) ecb on ecbh.balance_id = ecb.balance_id
				where ecbh.action is not null) ecbh
			left join
				Landing.mag.sales_flat_order_aud oh on rtrim(ltrim(replace(ecbh.order_no,'.',''))) = oh.increment_id
		order by num_tot_orders desc, customer_id, balance_id_bk, ecbh.transaction_date

	-- ReOrder - Reminder

	select ie.customer_email, ie.last_order_date, ie.num_tot_orders,
		entity_id, increment_id, created_at, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder
	from 
			DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
		inner join
			Landing.mag.sales_flat_order_aud oh on ie.customer_id = oh.customer_id
	order by ie.num_tot_orders desc, ie.customer_id, oh.created_at

	select ie.customer_email, ie.last_order_date, ie.num_tot_orders,
		id, created_at, updated_at, rp.customer_id, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile
	from 
			DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
		inner join	
			Landing.mag.po_reorder_profile_aud rp on ie.customer_id = rp.customer_id
	where enddate > startdate -- and enddate > getutcdate()
	order by ie.num_tot_orders desc, ie.customer_id, rp.created_at

	-- Orders
	select ie.customer_email, ie.last_order_date, ie.num_tot_orders,
		oh.*
	from 
			DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
		inner join
			Landing.aux.sales_dim_order_header_aud oh on ie.customer_id = oh.customer_id_bk
	order by ie.num_tot_orders desc, ie.customer_id, oh.order_date

		-- order_stage_name_bk
		-- order_status_name_bk
		-- adjustment_order
		-- order_tpe_name_bk
		-- status_bk
		-- payment_method_name_bk - payment_method code
		-- cc_type_name_bk
		-- shipping_description_bk - shipping_description_code
		-- reminder_type_name_bk - reminder_period_bk - reminder_date - reminder_sent 
		-- reorder_f - reorder_date - reorder_profile_id - automatic_reorder
		-- order_source - platform
		-- coupon_id_bk - coupon_code
		-- price_type_name_bk - discount_f
		-- order_currency_code
		-- countries_registered_code

		select oh.store_id_bk, count(*)
		from 
				DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
			inner join
				Landing.aux.sales_dim_order_header_aud oh on ie.customer_id = oh.customer_id_bk
		group by oh.store_id_bk
		order by oh.store_id_bk

	-- Order Item
	select ie.customer_email, ie.last_order_date, ie.num_tot_orders,
		ol.*
	from 
			DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
		inner join
			Landing.aux.sales_dim_order_header_aud oh on ie.customer_id = oh.customer_id_bk
		inner join
			Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
	order by ie.num_tot_orders desc, ie.customer_id, oh.order_date, ol.order_line_id_bk

		-- product_id_bk - pack_size
		-- eye
		-- base_curve_bk - diameter_bk - power_bk - cylinder_bk - axis_bk - addition_bk - dominance_bk - colour_bk
		-- sku_erp
		-- price_type_name_bk - discount_f
		-- qty_unit - qty_pack
		-- order_currency_code
		-- countries_registered_code
		-- product_type_vat
		
		select ol.product_id_bk, count(*)
		from 
				DW_GetLenses_jbs.dbo.sfcc_customers_ie ie
			inner join
				Landing.aux.sales_dim_order_header_aud oh on ie.customer_id = oh.customer_id_bk
			inner join
				Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
		group by ol.product_id_bk
		order by ol.product_id_bk