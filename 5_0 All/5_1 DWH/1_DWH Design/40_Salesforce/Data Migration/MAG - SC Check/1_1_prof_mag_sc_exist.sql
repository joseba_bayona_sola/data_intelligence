
-- Account

	-- 1.1 MAG customers not in SC
		-- VDDM: Most customers not in SC with wrong email // Wrong ID (really inserted in PROD) in 2020-02-26 // Not inserted in 2020-02-26 to 2020-03-01 and 2020-03-05 to 2020-03-08
	select count(*) over (), t1.migration_f_final, t1.migration_f, t1.Id, t1.PersonContactId, t1.entity_id, t1.email, t1.created_at, t1.ins_ts
	from 
			Landing.sf_mig.customer_data3_v t1
		left join
			Landing.sf_sc.Account_aud t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where t2.Id is null -- VDDM = 113918 / PROD = XX
		and t1.email not like '%@@%' -- VDDM = 2673 / PROD = XX
		and t1.Id is null -- VDDM = 2406 / PROD = XX
		and convert(date, t1.created_at) not in ('2020-02-26', '2020-02-27', '2020-02-28', '2020-02-29', '2020-03-01', '2020-03-05', '2020-03-06', '2020-03-07', '2020-03-08') -- VDDM = 852 / PROD = XX
		-- and t1.email LIKE '%_@__%.__%' -- VDDM = 675 / PROD = XX
	order by t1.entity_id

	-- 1.2 SC customers not in MAG
		-- VDDM: Guest customers inserted / GDPR deleted customers / Current No Active
	select count(*) over (), t2.Id, t2.PersonContactId, t2.RecordTypeName, 
		t2.Name, t2.PersonEmail, t2.EON_Legacy_Customer_Id__c, 
		-- t2.EON_Sign_Up_Form_Source__c,
		t2.EON_Registration_Date__c, t2.CreatedDate, t2.LastModifiedDate, 
		t3.email
	from 
			Landing.sf_mig.customer_data3_v t1
		right join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
		left join
			Landing.aux.gdpr_customer_list t3 on convert(int, t2.EON_Legacy_Customer_Id__c) = t3.customer_id
	where t1.entity_id is null -- VDDM = 26291 / PROD = XX
		and t2.RecordTypeName <> 'Guest' and t2.CreatedDate < getutcdate() -1 -- VDDM = 144 / PROD = XX
		and t3.email is null -- VDDM = 5 / PROD = XX
	order by convert(int, t2.EON_Legacy_Customer_Id__c)

-----
-- TEMP table 

	select t1.entity_id, t1.email, t1.created_at, t1.ins_ts
	into #customer_not_in_sc
	from 
			Landing.sf_mig.customer_data3_v t1
		left join
			Landing.sf_sc.Account_aud t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where t2.Id is null 

-- Address


	-- 2.1 MAG addresses not in SC
		-- VDDM: Most from Customers that are neither in SC / 662 from before 2020-02-21 (some value not like it) / 10 k without reason

	select count(*) over (), t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.street, t1.city, t1.postcode, t1.region, t1.country_id
	from 
			Landing.sf_mig.customer_address3_v t1
		left join
			Landing.sf_sc.Address__c_aud t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
		left join
			#customer_not_in_sc c on t1.entity_id = c.entity_id
	where t2.Id is null -- VDDM = 145959 / PROD = XX
		and c.entity_id is null -- VDDM = 11298 / PROD = XX
		and convert(date, t1.created_at) < '2020-02-21' -- VDDM = 662 / PROD = XX
	order by t1.entity_id_address

		select top 1000 *
		from Landing.mag.customer_entity_flat_aud
		where entity_id = 3215958

		select top 1000 *
		from Landing.mag.customer_address_entity_flat_aud
		where parent_id = 3215958

		select *
		from Landing.sf_sc.dm_customer_aud
		where convert(int, SFCC_Customer_Id__pc) = 3215958 

	-- 2.2 SC addresses not in MAG
		-- VDDM: Most addresses from 2020-02-21 (without filtering those ones phisically deleted)
	select count(*) over (), t2.Id, t2.Name, t2.Account__c, t2.EON_Active__c, 
		t2.First_Name__c, t2.Last_Name__c, t2.Address_Line_1__c, t2.Address_Line_2__c, t2.City__c,
		convert(int, t2.EON_Legacy_Address_Id__c) EON_Legacy_Address_Id__c, t2.CreatedDate
	from 
			Landing.sf_mig.customer_address3_v t1
		right join
			Landing.sf_sc.Address__c_aud t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
		left join
			Landing.mag.customer_address_entity_all t3 on convert(int, t2.EON_Legacy_Address_Id__c) = t3.entity_id
	where t1.entity_id_address is null -- VDDM = 127563 / PROD = XX
		and t3.entity_id is not null -- VDDM = 3 / PROD = XX

-- Preferences

	-- 3.1 MAG preferences not in SC
	select top 100000 count(*) over (), t1.entity_id, t1.email, t1.entity_sfcc_exported, -- t1.EON_Method__c, t1.EON_Frequency__c, 
		t1.EON_Opt_in__c, t1.created_at
	from 
			Landing.sf_mig.customer3_v t1
		left join
			Landing.sf_sc.EON_Preferences__c_aud_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c
		left join
			#customer_not_in_sc c on t1.entity_id = c.entity_id
	where t2.Id is null -- VDDM = 1032185 / PROD = XX
		and c.entity_id is null -- VDDM = 918270 / PROD = XX
		and t1.EON_Opt_in__c = 1 -- VDDM = 14571 / PROD = XX
	order by t1.EON_Opt_in__c desc, t1.created_at

	-- 3.2 SC preferences not in MAG

	select top 1000 count(*) over (), t2.Id, t2.Name, t2.RecordTypeName, t2.AccountName, t2.PersonEmail, t2.EON_Legacy_Customer_Id__c, t2.EON_Method__c, t2.EON_Frequency__c, t2.EON_Opt_in__c, t2.CreatedDate, t3.email
	from 
			Landing.sf_mig.customer3_v t1
		right join
			Landing.sf_sc.gen_EON_Preferences__c_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c 
		left join
			Landing.aux.gdpr_customer_list t3 on convert(int, t2.EON_Legacy_Customer_Id__c) = t3.customer_id
	where t2.EON_Account__c is not null and t1.entity_sfcc_exported is null -- VDDM = 11794 / PROD = XX
		and t2.RecordTypeName <> 'Guest' and t2.CreatedDate < getutcdate() -1 -- VDDM = 101 / PROD = XX
