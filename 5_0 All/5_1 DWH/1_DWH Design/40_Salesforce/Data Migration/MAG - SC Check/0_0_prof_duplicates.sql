
	select top 1000 count(*) over (), *
	from Landing.mag.customer_entity_flat_aud
	where website_group_id = 2

	select entity_id, email, email_dupl, created_at, 
		website_group_id, website_id, store_id,		
		num_rep, last_order_date,
		EON_Account__Record_Id__c, PersonContactId,
		migration_f, migration_f_final, delete_f,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.customer_entity_duplicates
	-- where email <> email_dupl
	-- where migration_f_final = 'Y' and EON_Account__Record_Id__c is null
	-- where migration_f_final = 'N' and EON_Account__Record_Id__c is not null
	order by ins_ts desc;

	-- email: current email
	-- email_dupl: old email - was duplicated
	select count(*), count(distinct entity_id), count(distinct email), count(distinct email_dupl)
	from Landing.mag.customer_entity_duplicates

	select migration_f, count(*), count(distinct entity_id), count(distinct email), count(distinct email_dupl)  
	from Landing.mag.customer_entity_duplicates
	group by migration_f
	order by migration_f

	select migration_f_final, count(*), count(distinct entity_id), count(distinct email), count(distinct email_dupl) 
	from Landing.mag.customer_entity_duplicates
	group by migration_f_final
	order by migration_f_final

	select migration_f, migration_f_final, count(*) 
	from Landing.mag.customer_entity_duplicates
	group by migration_f, migration_f_final
	order by migration_f, migration_f_final

	select count(*) over (partition by email) num_rep2, entity_id, email, email_dupl, created_at, 
		website_group_id, website_id, store_id,		
		num_rep, last_order_date,
		EON_Account__Record_Id__c, PersonContactId,
		migration_f, migration_f_final, delete_f,
		idETLBatchRun, ins_ts, upd_ts
	from Landing.mag.customer_entity_duplicates
	where migration_f_final = 'Y'
	order by num_rep2 desc, email

	select delete_f, count(*), count(distinct entity_id), count(distinct email), count(distinct email_dupl) 
	from Landing.mag.customer_entity_duplicates
	group by delete_f
	order by delete_f

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	

	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmag_lnd_merge_customer_entity_duplicates'
	order by spm.package_name, spm.sp_name, spm.messageTime desc

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	

	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmag_lndrt_merge_customer_entity_duplicates'
	order by spm.package_name, spm.sp_name, spm.messageTime desc