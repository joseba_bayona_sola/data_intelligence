
select count(*) over () num, migration_f_final, migration_f, Id, PersonContactId, 
	entity_id, email, store_id
into #c1
from Landing.sf_mig.customer_data3_v

select count(*) over () num, entity_sfcc_exported, entity_id, email, store_id
into #c2
from Landing.sf_mig.customer_v

	select count(*), count(distinct entity_id), count(distinct email)
	from #c1

	select count(*), count(distinct entity_id), count(distinct email)
	from #c2

	select top 1000 c1.migration_f_final, c1.migration_f, c1.Id, c1.PersonContactId, c2.entity_sfcc_exported,
		c1.entity_id, c2.entity_id, c1.email, c2.email, c1.store_id, c2.store_id
	from 
			#c1 c1
		full join
			#c2 c2 on c1.entity_id = c2.entity_id
	-- where c1.entity_id is null or c2.entity_id is null
	where (c1.entity_id is not null and c2.entity_id is not null) and 
		(isnull(c1.Id, '') <> isnull(c2.entity_sfcc_exported, '') or isnull(c1.email, '') <> isnull(c2.email, ''))
