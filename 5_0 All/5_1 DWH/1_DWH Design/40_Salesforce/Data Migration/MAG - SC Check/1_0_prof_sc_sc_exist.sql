
-- Account

	-- 1.1 SC DM Customers not in SC
		-- VDDM: Customers that were wrongly inserted in SC DM as they are not in SC: They are from PROD SC environment - Days: '2020-02-26'
	select top 1000 count(*) over (), t1.*
	from 
			Landing.sf_sc.dm_customer_aud t1
		left join
			Landing.sf_sc.Account_aud t2 on t1.Id = t2.Id
	where t2.Id is null and t1.ins_ts < getutcdate() -1 -- VDDM = 259 / PROD = XX
		-- and convert(date, t1.ins_ts) not in ('2020-02-26') -- VDDM = 0 / PROD = XX
	order by convert(int, t1.SFCC_Customer_Id__pc)

	-- 1.2 SC Customers not in SC DM
		-- VDDM: For some reason they were inserted in SC but not back in SC DM - Days: '2020-02-27', '2020-02-28', '2020-03-27', '2020-03-28'
	select count(*) over (), t2.Id, t2.PersonContactId, t2.RecordTypeName, 
		t2.Name, t2.PersonEmail, t2.EON_Legacy_Customer_Id__c, 
		-- t2.EON_Sign_Up_Form_Source__c,
		t2.EON_Registration_Date__c, t2.CreatedDate, t2.LastModifiedDate
	from 
			Landing.sf_sc.dm_customer_aud t1
		right join
			Landing.sf_sc.gen_Account_v t2 on t1.Id = t2.Id
	where t1.Id is null
		and t2.RecordTypeName <> 'Guest' -- VDDM = 1019 / PROD = XX
		and convert(date, t2.CreatedDate) not in ('2020-02-27', '2020-02-28', '2020-03-27', '2020-03-28') -- VDDM = 12 / PROD = XX
	order by convert(int, t2.EON_Legacy_Customer_Id__c), t2.CreatedDate

	-- 1.3 Check Differences
		-- Differences all in email
	select top 1000 count(*) over (), t1.Id, t1.PersonContactID, t2.PersonContactID, t1.SFCC_Customer_Id__pc, t2.EON_Legacy_Customer_Id__c, t1.PersonEmail, t2.PersonEmail
	from 
			Landing.sf_sc.dm_customer_aud t1
		inner join
			Landing.sf_sc.Account_aud t2 on t1.Id = t2.Id
	where t1.PersonContactID <> t2.PersonContactID or t1.SFCC_Customer_Id__pc <> t2.EON_Legacy_Customer_Id__c or t1.PersonEmail <> t2.PersonEmail -- VDDM = 2123 / PROD = XX
	order by convert(int, t1.SFCC_Customer_Id__pc)

-- Address - NOT POSSIBLE (NO SC DM data for Address)

	-- 2.1 SC DM addresses not in SC

	-- 2.2 SC addresses not in SC DM

-- Preferences

	-- 3.1 SC DM preferences not in SC

	select top 1000 count(*) over (), t1.*
	from 
			Landing.sf_sc.dm_preferences_aud t1
		left join
			Landing.sf_sc.EON_Preferences__c_aud t2 on t1.EON_Preference_Record_Id__c = t2.Id
	where t2.Id is null -- VDDM = 4 / PROD = XX
	order by t1.EON_Legacy_Preference_Id__c

	-- 3.2 SC preferences not in SC DM

	select top 1000 count(*) over (), t2.*
	from 
			Landing.sf_sc.dm_preferences_aud t1
		right join
			Landing.sf_sc.EON_Preferences__c_aud t2 on t1.EON_Preference_Record_Id__c = t2.Id
	where t1.EON_Preference_Record_Id__c is null -- VDDM = 1826052 / PROD = XX 
	order by t2.EON_Legacy_Preference_Id__c

	-- 1.3 Check Differences

	select top 1000 count(*) over (), t1.EON_Preference_Record_Id__c, 
		t1.EON_Account__Record_Id__c, t2.EON_Account__c, t1.EON_Method__c, t2.EON_Method__c, t1.EON_Opt_in__c, t2.EON_Opt_in__c, t1.EON_Legacy_Preference_Id__c, t2.EON_Legacy_Preference_Id__c 
	from 
			Landing.sf_sc.dm_preferences_aud t1
		inner join
			Landing.sf_sc.EON_Preferences__c_aud t2 on t1.EON_Preference_Record_Id__c = t2.Id
	where t1.EON_Account__Record_Id__c <> t2.EON_Account__c or t1.EON_Method__c <> t2.EON_Method__c or t1.EON_Opt_in__c <> t2.EON_Opt_in__c or t1.EON_Legacy_Preference_Id__c <> t2.EON_Legacy_Preference_Id__c 
		-- VDDM = 1176 / PROD = XX
	order by t1.EON_Legacy_Preference_Id__c
