
-- 

select top 1000 count(*) over (), 
	t1.migration_f_final, t1.migration_f, t1.entity_id, t1.email, t1.created_at, t1.ins_ts,
	t2.Id, t2.PersonContactId, t2.RecordTypeName, 
	t2.Name, t2.PersonEmail, t2.EON_Legacy_Customer_Id__c, t2.EON_Registration_Date__c, t2.CreatedDate, t2.LastModifiedDate
from 
		Landing.sf_mig.customer_data3_v t1
	inner join
		Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
where t1.email = 'jibs1284@gmail.com'



	-- PersonEmail
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate
	from 
			Landing.sf_mig.customer_data3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
	 	t1.email <> t2.PersonEmail -- VDDM = 220 / PROD = XX
	order by t1.entity_id



	-- Salutation, FirstName, LastName, Name
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.prefix, t1.firstname, t2.lastname, t2.Salutation, t2.FirstName, t2.LastName, t2.Name
	from 
			Landing.sf_mig.customer_data3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
	 	t1.prefix <> t2.Salutation or t1.firstname <> t2.FirstName or t1.lastname <> t2.LastName -- VDDM = 327 / PROD = XX
	order by t1.entity_id




	-- EON_Preferred_Site__c, EON_Preferred_Locale__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_Preferred_Site__c, t2.EON_Preferred_Site__c, t1.EON_Preferred_Locale__c, t2.EON_Preferred_Locale__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		t1.EON_Preferred_Site__c <> t2.EON_Preferred_Site__c or t1.EON_Preferred_Locale__c <> t2.EON_Preferred_Locale__c -- VDDM = 312 / PROD = XX




	-- EON_Terms_Conditions_Date__c ??
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.tc_accepted_date, t2.EON_Terms_Conditions_Date__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.tc_accepted_date, '') <> isnull(t2.EON_Terms_Conditions_Date__c, '') -- VDDM = 21631 / PROD = XX
	order by t1.tc_accepted_date




	-- EON_Last_Order_Date__c - EON_Months_Since_Last_Order__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_Last_Order_Date__c, t2.EON_Last_Order_Date__c, t1.EON_Months_Since_Last_Order__c, t2.EON_Months_Since_Last_Order__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.EON_Last_Order_Date__c, '') <> isnull(t2.EON_Last_Order_Date__c, '') or isnull(t1.EON_Months_Since_Last_Order__c, 0) <> isnull(t2.EON_Months_Since_Last_Order__c, 0) -- VDDM = 2306120 / PROD = XX

		select count(*) from Landing.sf_sc.gen_Account_v where EON_Last_Order_Date__c is not null




	-- Phone - EON_Mobile__c
		-- Mulesoft Formatting done
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.cus_phone_country_id, t1.cus_phone, t2.Phone, t1.cus_phone, t2.EON_Mobile__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where t1.email = 'jibs1284@gmail.com' and
		isnull(t1.cus_phone, '') <> isnull(t2.Phone, '') or isnull(t1.cus_phone, '') <> isnull(t2.EON_Mobile__c, '') -- VDDM = XX / PROD = XX




	-- EON_Default_Shipping_Address__c - EON_Default_Billing_Address__c
		-- Mulesoft Formatting done
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		isnull(t1.street1_shipping, '') + ' ' + isnull(t1.street2_shipping, '') + ' ' + isnull(t1.city_shipping, '') + ' ' + isnull(t1.region_shipping, '') + ' ' + isnull(t1.country_id_shipping, '') + ' ' + isnull(t1.postcode_shipping, '') EON_Default_Shipping_Address__c, 
		t2.EON_Default_Shipping_Address__c, 
		isnull(t1.street1_billing, '') + ' ' + isnull(t1.street2_billing, '') + ' ' + isnull(t1.city_billing, '') + ' ' + isnull(t1.region_billing, '') + ' ' + isnull(t1.country_id_billing, '') + ' ' + isnull(t1.postcode_billing, '') EON_Default_Billing_Address__c, 
		t2.EON_Default_Billing_Address__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where t1.email = 'jibs1284@gmail.com' and
		t1.EON_Default_Shipping_Address__c <> t2.EON_Default_Shipping_Address__c or t1.EON_Default_Billing_Address__c <> t2.EON_Default_Billing_Address__c -- VDDM = XX / PROD = XX

			select count(*) from Landing.sf_sc.gen_Account_v where EON_Default_Shipping_Address__c is not null and EON_Default_Billing_Address__c is not null
			select count(*) from Landing.sf_sc.gen_Account_v where EON_Default_Shipping_Address__c is null or EON_Default_Billing_Address__c is null




	-- EON_Tax_Number__c - EON_Prescription_Check_Required__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_IVA_Number__c, t2.EON_Tax_Number__c, t1.EON_Prescription_Check_Required__c, t2.EON_Prescription_Check_Required__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.EON_IVA_Number__c, '') <> isnull(t2.EON_Tax_Number__c, '') or isnull(t2.EON_Prescription_Check_Required__c, 0) <> isnull(t2.EON_Prescription_Check_Required__c, 0) -- VDDM = 144 / PROD = XX




	-- SFCC_Last_Visit_Time__c - SFCC_Last_Login_Time__c - EON_Last_Password_Request_Date__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.SFCC_Last_Visit_Time__c, t2.SFCC_Last_Visit_Time__c, t1.SFCC_Last_Login_Time__c, t2.SFCC_Last_Login_Time__c, t1.EON_Last_Password_Request_Date__c, t2.EON_Last_Password_Request_Date__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.SFCC_Last_Visit_Time__c, '') <> isnull(t2.SFCC_Last_Visit_Time__c, '') or isnull(t1.SFCC_Last_Login_Time__c, '') <> isnull(t2.SFCC_Last_Login_Time__c, '') or 
			isnull(t1.EON_Last_Password_Request_Date__c, '') = isnull(t2.EON_Last_Password_Request_Date__c, '') -- VDDM = 312 / PROD = XX

		select count(*) from Landing.sf_sc.gen_Account_v where SFCC_Last_Visit_Time__c is not null or SFCC_Last_Login_Time__c is not null or EON_Last_Password_Request_Date__c is not null 




	-- EON_Number_of_Orders__pc - EON_Number_of_Refunds__c - EON_Total_Order_Value__pc - EON_Total_Refund_Value__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_Number_of_Orders__pc, t2.EON_Number_of_Orders__c, t1.EON_Number_of_Refunds__c, t2.EON_Number_of_Refunds__c, 
		t1.EON_Total_Order_Value__pc, t2.EON_Total_Order_Value__c, t1.EON_Total_Refund_Value__c, t2.EON_Total_Refund_Value__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.EON_Number_of_Orders__pc, 0) <> isnull(t2.EON_Number_of_Orders__c, 0) or isnull(t1.EON_Number_of_Refunds__c, 0) <> isnull(t2.EON_Number_of_Refunds__c, 0) or
		isnull(t1.EON_Total_Order_Value__pc, 0) <> isnull(t2.EON_Total_Order_Value__c, 0) or isnull(t1.EON_Total_Refund_Value__c, 0) <> isnull(t2.EON_Total_Refund_Value__c, 0) -- VDDM = 312 / PROD = XX




	-- EON_Reminder_Email_Setting__c - EON_Reminder_SMS_Setting__c - EON_Newsletter_Signed_Up__c
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_Reminder_Email_Setting__c, t2.EON_Reminder_Email_Setting__c, t1.EON_Reminder_SMS_Setting__c, t2.EON_Reminder_SMS_Setting__c, t1.EON_Newsletter_Signed_Up__c, t2.EON_Newsletter_Signed_Up__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where t1.email = 'jibs1284@gmail.com' and
		t1.EON_Reminder_Email_Setting__c <> t2.EON_Reminder_Email_Setting__c or t2.EON_Reminder_SMS_Setting__c <> t1.EON_Reminder_SMS_Setting__c -- VDDM = 312 / PROD = XX

		select t1.EON_Reminder_Email_Setting__c, t2.EON_Reminder_Email_Setting__c, count(*)
		from
				Landing.sf_mig.customer3_v t1
			inner join
				Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
		group by t1.EON_Reminder_Email_Setting__c, t2.EON_Reminder_Email_Setting__c
		order by t1.EON_Reminder_Email_Setting__c, t2.EON_Reminder_Email_Setting__c

		select t1.EON_Reminder_SMS_Setting__c, t2.EON_Reminder_SMS_Setting__c, count(*)
		from
				Landing.sf_mig.customer3_v t1
			inner join
				Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
		group by t1.EON_Reminder_SMS_Setting__c, t2.EON_Reminder_SMS_Setting__c
		order by t1.EON_Reminder_SMS_Setting__c, t2.EON_Reminder_SMS_Setting__c

		select t1.EON_Newsletter_Signed_Up__c, t2.EON_Newsletter_Signed_Up__c, count(*)
		from
				Landing.sf_mig.customer3_v t1
			inner join
				Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
		group by t1.EON_Newsletter_Signed_Up__c, t2.EON_Newsletter_Signed_Up__c
		order by t1.EON_Newsletter_Signed_Up__c, t2.EON_Newsletter_Signed_Up__c




	-- EON_Requires_Password_Migration__c - EON_Migrated_From_Site__c - EON_Registration_Date__c 
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t1.EON_Requires_Password_Migration__c, t2.EON_Requires_Password_Migration__c, t1.EON_Migrated_From_Site__c, t2.EON_Migrated_From_Site__c, t1.EON_Registration_Date__c, t2.EON_Registration_Date__c
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		isnull(t1.EON_Migrated_From_Site__c, '') <> isnull(t2.EON_Migrated_From_Site__c, '') or isnull(t1.EON_Registration_Date__c, '') <> isnull(t2.EON_Registration_Date__c, '') -- VDDM = 4053 / PROD = XX

		select t1.EON_Requires_Password_Migration__c, t2.EON_Requires_Password_Migration__c, count(*)
		from
				Landing.sf_mig.customer3_v t1
			inner join
				Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
		group by t1.EON_Requires_Password_Migration__c, t2.EON_Requires_Password_Migration__c
		order by t1.EON_Requires_Password_Migration__c, t2.EON_Requires_Password_Migration__c



	-- IsDeleted
	select top 1000 count(*) over (), t1.entity_id, t1.email, t2.PersonEmail, t2.Id, t2.PersonContactId, t2.RecordTypeName, t1.created_at, t1.updated_at, t2.CreatedDate, t2.LastModifiedDate,
		
		t2.IsDeleted
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_Account_v t2 on t1.entity_id = convert(int, t2.EON_Legacy_Customer_Id__c)
	where -- t1.email = 'jibs1284@gmail.com' and
		t2.IsDeleted <> 0 -- VDDM = XX / PROD = XX

		select IsDeleted, count(*)
		from Landing.sf_sc.gen_Account_v 
		group by IsDeleted
		order by IsDeleted