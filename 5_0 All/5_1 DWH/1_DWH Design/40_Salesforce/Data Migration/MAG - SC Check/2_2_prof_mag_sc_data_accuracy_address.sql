-- 

	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.street, t1.city, t1.postcode, t1.region, t1.country_id, 
		t2.Id, t2.Name, t2.Account__c, t2.EON_Active__c, t2.EON_Default_Shipping_Address__c, t2.EON_Default_Billing_Address__c,
		t2.First_Name__c, t2.Last_Name__c, t2.Address_Line_1__c, t2.Address_Line_2__c, t2.City__c,
		convert(int, t2.EON_Legacy_Address_Id__c) EON_Legacy_Address_Id__c, t2.CreatedDate
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com')
	order by entity_id

	-- EON_Active__c - EON_Default_Shipping_Address__c - EON_Default_Billing_Address__c
	
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.is_active, t2.EON_Active__c, t1.shipping_f, t2.EON_Default_Shipping_Address__c, t1.billing_f, t2.EON_Default_Billing_Address__c
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where -- t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		(t1.is_active <> t2.EON_Active__c or t1.shipping_f <> t2.EON_Default_Shipping_Address__c or t1.billing_f <> t2.EON_Default_Billing_Address__c) -- VDDM = 5621 / PROD = XX

		select top 1000 count(*) over (), 
			Account__c, PersonEmail, sum(convert(int, EON_Default_Shipping_Address__c)) EON_Default_Shipping_Address__c, sum(convert(int, EON_Default_Billing_Address__c)) EON_Default_Billing_Address__c
		from Landing.sf_sc.gen_Address__c_v t2 
		-- where PersonEmail = 'jibs1284@gmail.com'
		group by Account__c, PersonEmail
		having sum(convert(int, EON_Default_Shipping_Address__c)) <> 1 or sum(convert(int, EON_Default_Billing_Address__c)) <> 1 -- 18545




	-- First_Name__c, Last_Name__c
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.firstname, t2.First_Name__c, t1.lastname, t2.Last_Name__c
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where -- t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		(isnull(t1.firstname, '') <> isnull(t2.First_Name__c, '') or isnull(t1.lastname, '') <> isnull(t2.Last_Name__c, '')) -- VDDM = 97 / PROD = XX




	-- EON_Phone_Type__c - Phone__c - EON_Mobile__c - EON_Alt_Phone__c - EON_Alt_Phone_Type__c
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.telephone, t2.Phone__c, t1.telephone, t2.EON_Mobile__c
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		(isnull(t1.telephone, '') <> isnull(t2.Phone__c, '') or isnull(t1.telephone, '') <> isnull(t2.EON_Mobile__c, '')) -- VDDM = XX / PROD = XX




	-- Address_Line_1__c - Address_Line_2__c
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.street1, t2.Address_Line_1__c, t1.street2, t2.Address_Line_2__c
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where -- t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		(isnull(t1.street1, '') <> isnull(t2.Address_Line_1__c, '') or isnull(t1.street2, '') <> isnull(t2.Address_Line_2__c, '')) -- VDDM = 1579 / PROD = XX




	-- City__c - State__c - Postal_Code__c - Country__c
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.city, t2.City__c, t1.region, t2.State__c, t1.postcode, t2.Postal_Code__c, t1.country_id, t2.Country__c
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where -- t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		(isnull(t1.city, '') <> isnull(t2.City__c, '') or isnull(t1.region, '') <> isnull(t2.State__c, '') or 
		isnull(t1.postcode, '') <> isnull(t2.Postal_Code__c, '') or isnull(t1.country_id, '') <> isnull(t2.Country__c, '')) -- VDDM = 1373 / PROD = XX




	-- IsDeleted
	select top 1000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_id_address, t1.created_at, t1.updated_at, t2.Id, t2.Name, t2.Account__c, t2.CreatedDate, t2.LastModifiedDate,

		t1.IsDeleted
	from 
			Landing.sf_mig.customer_address3_v t1
		inner join
			Landing.sf_sc.gen_Address__c_v t2 on t1.entity_id_address = convert(int, t2.EON_Legacy_Address_Id__c)
	where -- t1.email in ('jibs1284@gmail.com', 'jonselnes@yahoo.com', 'oihana83@kaixo.com') and
		t2.IsDeleted <> 0 -- VDDM = 97 / PROD = XX

		select IsDeleted, count(*)
		from Landing.sf_sc.gen_Address__c_v 
		group by IsDeleted
		order by IsDeleted