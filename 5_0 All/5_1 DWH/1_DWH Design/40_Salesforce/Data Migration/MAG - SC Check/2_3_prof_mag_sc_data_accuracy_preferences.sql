-- 

	select top 100000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_sfcc_exported, t1.EON_Opt_in__c, t1.created_at, 
		t2.Id, t2.Name, t2.RecordTypeName, t2.AccountName, t2.PersonEmail, t2.EON_Legacy_Customer_Id__c, t2.EON_Method__c, t2.EON_Frequency__c, t2.EON_Opt_in__c, t2.CreatedDate
	from 
			Landing.sf_mig.customer3_v t1
		inner join
			Landing.sf_sc.gen_EON_Preferences__c_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c
	where t1.email = 'jibs1284@gmail.com'

	-- EON_Method__c 
	select top 100000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_sfcc_exported, t2.Id, t2.Name, t2.EON_Account__c, t2.CreatedDate, t2.LastModifiedDate, 
		t1.EON_Method__c, t2.EON_Method__c
	from 
			Landing.sf_mig.customer_preferences3_v t1
		inner join
			Landing.sf_sc.gen_EON_Preferences__c_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c
	where -- t1.email = 'jibs1284@gmail.com'
		isnull(t1.EON_Method__c, '') <> isnull(t2.EON_Method__c, '') -- VDDM = 1 / PROD = XX

		select EON_Method__c, count(*)
		from Landing.sf_sc.gen_EON_Preferences__c_v
		group by EON_Method__c
		order by EON_Method__c

	-- EON_Frequency__c
	select top 100000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_sfcc_exported, t2.Id, t2.Name, t2.EON_Account__c, t2.CreatedDate, t2.LastModifiedDate, 
		t1.EON_Frequency__c, t2.EON_Frequency__c
	from 
			Landing.sf_mig.customer_preferences3_v t1
		inner join
			Landing.sf_sc.gen_EON_Preferences__c_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c
	where -- t1.email = 'jibs1284@gmail.com'
		isnull(t1.EON_Frequency__c, '') <> isnull(t2.EON_Frequency__c, '') -- VDDM = 1 / PROD = XX

		select EON_Frequency__c, count(*)
		from Landing.sf_sc.gen_EON_Preferences__c_v
		group by EON_Frequency__c
		order by EON_Frequency__c

	-- EON_Opt_In__c
	select top 100000 count(*) over (), 
		t1.entity_id, t1.email, t1.entity_sfcc_exported, t2.Id, t2.Name, t2.EON_Account__c, t2.CreatedDate, t2.LastModifiedDate, 
		t1.EON_Opt_In__c, t2.EON_Opt_In__c
	from 
			Landing.sf_mig.customer_preferences3_v t1
		inner join
			Landing.sf_sc.gen_EON_Preferences__c_v t2 on t1.entity_sfcc_exported = t2.EON_Account__c
	where -- t1.email = 'jibs1284@gmail.com'
		isnull(t1.EON_Opt_In__c, 0) <> isnull(t2.EON_Opt_In__c, 0) -- VDDM = 3811 / PROD = XX

		select EON_Opt_In__c, count(*)
		from Landing.sf_sc.gen_EON_Preferences__c_v
		group by EON_Opt_In__c
		order by EON_Opt_In__c

	-- IsDeleted