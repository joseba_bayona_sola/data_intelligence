
-- WEARERS + PRESCRIPTIONS

	-- wearer
	select top 1000 count(*) over (), count(*) over (partition by customer_id) num_rep,
		*
	from Landing.mag.wearer_aud
	order by num_rep desc, customer_id, id

	-- wearer_prescription
	select top 1000 count(*) over (), count(*) over (partition by wearer_id) num_rep,
		*
	from Landing.mag.wearer_prescription_aud
	where (date_start_prescription is null and date_last_prescription is null) 
		or GETUTCDATE() between date_start_prescription and date_last_prescription
		-- or date_start_prescription <= date_last_prescription
	order by num_rep desc, wearer_id, id

	-- wearer_prescription_method
	select top 1000 count(*) over (), *
	from Landing.mag.wearer_prescription_method_aud

	select count(*) over (), 
		w.id wearer_id, w.customer_id, c.firstname, c.lastname, w.name, w.dob, 
		wp.id prescription_id, wpm.method, wpm.code, 
		wp.date_last_prescription, wp.date_start_prescription, 
		wp.optician_name, wp.optician_address1, wp.optician_address2, wp.optician_city, wp.optician_state, wp.optician_country, wp.optician_postcode, wp.optician_phone, wp.optician_lookup_id, 
		wp.reminder_sent

	from 
			Landing.mag.wearer_aud w
		left join
			Landing.mag.customer_entity_flat_aud c on w.customer_id = c.entity_id
		left join
			Landing.mag.wearer_prescription_aud wp on w.id = wp.wearer_id
		left join
			Landing.mag.wearer_prescription_method_aud wpm on wp.method_id = wpm.id
	-- where w.customer_id = 133223 or w.id = 3782 or wp.id in (5844, 29766, 7104, 129648)
	where (date_start_prescription is null and date_last_prescription is null) or GETUTCDATE() between date_start_prescription and date_last_prescription
	-- order by w.customer_id, w.id, wp.id
	order by wp.id desc

	-- wearer_order_item -- missing item_data attribute
	select top 1000 *
	from Landing.mag.wearer_order_item_aud	
	where wearer_prescription_id in (5844, 29766, 7104, 129648)

			select id, 
				order_id, item_id, quote_item_id, 
				prescription_type, 
				wearer_prescription_id, prescription_flag, item_data
			from openquery(MAGENTO,
				'select id, 
					order_id, item_id, quote_item_id, 
					prescription_type, 
					wearer_prescription_id, prescription_flag, item_data 
				from magento01.wearer_order_item
				order by id desc
				limit 10')

-- STORE CREDIT

	select top 1000 
		ecb.balance_id balance_id_bk, 
		ecb.customer_id customer_id_bk, cs.store_id store_id_bk, 
		ecbh.first_transaction_date, ecbh.last_transaction_date,
		ecb.amount customer_balance, ecb.base_currency_code currency_code
	from 
			Landing.mag.enterprise_customerbalance_aud ecb
		inner join
			(select website_id, min(store_id) store_id
			from Landing.mag.core_store
			group by website_id) cs on ecb.website_id = cs.website_id
		left join -- inner
			(select balance_id, min(updated_at) first_transaction_date, max(updated_at) last_transaction_date
			from Landing.mag.enterprise_customerbalance_history_aud
			group by balance_id) ecbh on ecb.balance_id = ecbh.balance_id
	where ecb.customer_id = 70200

	select 
		ecbh.history_id_bk, ecbh.balance_id_bk, ecbh.action_bk, 
		oh.entity_id order_id_bk, rtrim(ltrim(replace(ecbh.order_no,'.',''))) order_no, 
		ecbh.customer_service_agent,
		ecbh.balance_amount, ecbh.transaction_amount, 
		ecbh.transaction_date, 
		ecbh.additional_info, ecbh.frontend_comment, ecbh.is_customer_notified
	from
			(select 
				ecbh.history_id history_id_bk, 
				ecbh.balance_id balance_id_bk, ecbh.action action_bk, 
				case 
					when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, len(additional_info)) 
					when (ecbh.action in (3, 5) and charindex('#', additional_info, 1) = 0) then substring(additional_info, charindex(' ', additional_info, 1) + 1, len(additional_info)) 
					when (ecbh.action in (4) and charindex('#', additional_info, 1) > 0) then substring(additional_info, charindex('#', additional_info, 1) + 1, charindex(',', additional_info, 1) - charindex('#', additional_info, 1) -1) 
					else null 
				end order_no, 
				case 
					when (ecbh.action in (1, 2)) then substring(additional_info, charindex('By admin:', additional_info, 1) + len('By admin:'), charindex('.', additional_info, 1) - (charindex('By admin:', additional_info, 1) + len('By admin:'))) 
					else null 
				end customer_service_agent,
				ecbh.balance_amount balance_amount, ecbh.balance_delta transaction_amount, 
				ecbh.updated_at transaction_date, 
				ecbh.additional_info, ecbh.frontend_comment, ecbh.is_customer_notified
			from 
					Landing.mag.enterprise_customerbalance_history_aud ecbh --_aud
				inner join
					(select ecb.*
					from 
							Landing.mag.enterprise_customerbalance_aud ecb
						inner join
							(select website_id, min(store_id) store_id
							from Landing.mag.core_store
							group by website_id) cs on ecb.website_id = cs.website_id) ecb on ecbh.balance_id = ecb.balance_id
			where ecbh.action is not null) ecbh
		left join
			Landing.mag.sales_flat_order_aud oh on rtrim(ltrim(replace(ecbh.order_no,'.',''))) = oh.increment_id
	where ecbh.balance_id_bk in (16, 17)

-- REORDER / REMINDER

	select reminder_type reminder_type_name_bk, UPPER(reminder_type) reminder_type_name
	from
		(select reminder_type, count(*) num
		from Landing.mag.sales_flat_order_aud
		where status <> 'archived'
		group by reminder_type) t
	where reminder_type in ('both', 'email', 'sms', 'none', 'reorder')
	order by reminder_type

select top 1000 entity_id, increment_id, created_at, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder
from Landing.mag.sales_flat_order
order by entity_id desc

select count(*) over (), id, created_at, updated_at, customer_id, 
	startdate, enddate, interval_days, next_order_date, 
	reorder_quote_id, coupon_rule, completed_profile
from Landing.mag.po_reorder_profile_aud
where enddate > startdate and enddate > getutcdate()
order by next_order_date 

select order_id_bk, order_no, order_date, 
	reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
	reorder_f, reorder_date, reorder_profile_id, automatic_reorder
from Landing.aux.sales_dim_order_header
-- where reorder_f <> 'N'
-- where automatic_reorder <> 0
order by order_id_bk desc


-- PAYMENT INSTRUMENT

select entity_id, parent_id, method, cc_type, amount_authorized, base_amount_ordered
from Landing.mag.sales_flat_order_payment
order by entity_id desc

		select op.method payment_method_name_bk, op.method payment_method_name, 'Other' payment_method_name_nod, 'No Desc.' description, 
			pm.payment_method_code, pm.payment_method_name
		from
				(select distinct method
				from Landing.mag.sales_flat_order_payment_aud
				where method not in ('storecredit')) op
			left join
				Landing.map.sales_payment_method_aud pm on op.method = pm.payment_method_code
		order by op.method