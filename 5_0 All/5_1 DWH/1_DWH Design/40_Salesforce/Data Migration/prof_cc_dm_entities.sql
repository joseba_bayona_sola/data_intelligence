
-- order
select null order_no

-- order
select null order_no, null original_order_no, null current_order_no, null invoice_no, 
	null order_date, null site, null customer_locale, 
	null created_by, null channel_type,  null order_method,
	null order_status, null shipping_status, null confirmation_status, null payment_status, null export_status, null customer_facing_status,
	0 merchandize_total_net_price, 0 merchandize_total_tax, 0 merchandize_total_gross_price, 
	0 adjusted_merchandize_total_net_price, 0 adjusted_merchandize_total_tax, 0 adjusted_merchandize_total_gross_price, 
	0 shipping_total_net_price, 0 shipping_total_tax, 0 shipping_total_gross_price, 
	0 adjusted_shipping_total_net_price, 0 adjusted_shipping_total_tax, 0 adjusted_shipping_total_gross_price, 
	0 total_net_price, 0 total_tax, 0 total_gross_price, 
	null currency, null taxation

-- order_product_lineitem
select null order_no, null position, 
	null product_id, null product_name, null line_item_text, 
	null pack_size, null variantSKU, null externalVariantID, null enteredManufacturingData,
	null base_curve, null diameter, null power, null cylinder, null axis, null addition, null dominant, null colour,
	null contact_lens_eye,
	null line_item_group_id, 
	null shipment_id, null gift,

	0 quantity, 0 total_lenses, 0 base_price, 
	0 tax_basis, null tax_class_id,  0 tax_rate,
	0 net_price, 0 tax, 0 gross_price 

-- order_customer
select null order_no, null customer_id, null customer_no, 
	null customer_name, null customer_email 

-- order_billing_address
select null order_no, null customer_id, null customer_no, 
	null first_name, null first_name, 
	null building_number, null street, 
	null address1, null address2, null city, null postal_code, null country_code

-- order_shipments
select null order_no, null shipment_id, null shipment_no,
	null shipping_status, 
	null shipping_method_id, null shipping_method, 
	null first_name, null first_name, 
	null building_number, null street, 
	null address1, null address2, null city, null postal_code, null country_code, null phone, 
	null gift,
	0 merchandize_total_net_price, 0 merchandize_total_tax, 0 merchandize_total_gross_price, 
	0 adjusted_merchandize_total_net_price, 0 adjusted_merchandize_total_tax, 0 adjusted_merchandize_total_gross_price, 
	0 shipping_total_net_price, 0 shipping_total_tax, 0 shipping_total_gross_price, 
	0 adjusted_shipping_total_net_price, 0 adjusted_shipping_total_tax, 0 adjusted_shipping_total_gross_price, 
	0 shipment_total_net_price, 0 shipment_total_tax, 0 shipment_total_gross_price

-- order_shipping_lineitem
select null order_no, null shipment_id, null shipment_no,
	null line_item_text, 
	0 base_price, 
	0 tax_basis, null tax_class_id,  0 tax_rate,
	0 net_price, 0 tax, 0 gross_price

-- order_payments
select null order_no, null transaction_id, 
	null processor_id, null payment_method_id, null payment_method_name, 
	0 amount


