
-- dim_subscribers

select top 1000 count(*) over (partition by idCustomer_sk_fk) num_rep, *
from Warehouse.email.dim_subscribers
-- where idCustomer_sk_fk is null
order by num_rep desc, idCustomer_sk_fk, email

	select top 1000 count(*), count(distinct idCustomer_sk_fk)
	from Warehouse.email.dim_subscribers
	
-- dim_campaign

select *
from Warehouse.email.dim_campaign
where sent_date > '2020-07-01' order by email_name
order by sent_date desc

	select count(*), count(distinct email_name)
	from Warehouse.email.dim_campaign

	select email_name, count(*)
	from Warehouse.email.dim_campaign
	group by email_name
	order by email_name

	select from_email, count(*)
	from Warehouse.email.dim_campaign
	group by from_email
	order by from_email


select top 1000 *
from Warehouse.email.fact_campaign_sent

	select top 1000 count(*), count(distinct idCampaign_sk_fk), count(distinct idSubscribers_sk_fk)
	from Warehouse.email.fact_campaign_sent

	select top 1000 idCampaign_sk_fk, count(*), count(distinct idSubscribers_sk_fk)
	from Warehouse.email.fact_campaign_sent
	group by idCampaign_sk_fk
	order by count(*) desc

	select top 1000 s.idCustomer_sk_fk, 
		s.email, 
		count(*) num
	from 
			Warehouse.email.fact_campaign_sent fc
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	group by s.idCustomer_sk_fk, s.email
	order by num desc

------------------------------------------------------------
------------------------------------------------------------

select top 1000 count(*) over (), 
	c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
	s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
	fc.event_date	
from 
		Warehouse.email.dim_campaign c
	inner join
		Warehouse.email.fact_campaign_sent fc on c.idCampaign_sk = fc.idCampaign_sk_fk
	inner join
		Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
where c.idCampaign_sk = 1087

select top 1000 count(*) over (), 
	c.idCampaign_sk, c.sendID_bk, c.campaign_website, c.email_name, c.email_subject, c.sent_date, c.job_status, 
	s.idSubscribers_sk, s.idCustomer_sk, s.subscriberkey, s.email,
	s.company_name_create, s.website_group_create, s.website_create,
	s.website_register, 
	s.customer_origin_name, s.current_customer_status_name, s.rank_num_tot_orders, s.num_tot_orders,
	s.customer_id, s.customer_name,
	fc.event_date	
from 
		Warehouse.email.dim_campaign_v c
	inner join
		Warehouse.email.fact_campaign_sent fc on c.idCampaign_sk = fc.idCampaign_sk_fk
	inner join
		Warehouse.email.dim_subscribers_v s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
where c.idCampaign_sk = 1087

	select top 1000 count(*) over (), 
		c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
		min(fc.event_date), count(*) num_actions		
	from 
			Warehouse.email.dim_campaign c
		inner join
			Warehouse.email.fact_campaign_not_sent fc on c.idCampaign_sk = fc.idCampaign_sk_fk
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	where c.idCampaign_sk = 1087
	group by c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email

	select top 1000 count(*) over (), 
		c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
		min(fc.event_date), count(*) num_actions		
	from 
			Warehouse.email.dim_campaign c
		inner join
			Warehouse.email.fact_campaign_opens fc on c.idCampaign_sk = fc.idCampaign_sk_fk
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	where c.idCampaign_sk = 1087
	group by c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email

	select top 1000 count(*) over (), 
		c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
		min(fc.event_date), count(*) num_actions		
	from 
			Warehouse.email.dim_campaign c
		inner join
			Warehouse.email.fact_campaign_clicks fc on c.idCampaign_sk = fc.idCampaign_sk_fk
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	where c.idCampaign_sk = 1087
	group by c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email

	select top 1000 count(*) over (), 
		c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
		min(fc.event_date), count(*) num_actions		
	from 
			Warehouse.email.dim_campaign c
		inner join
			Warehouse.email.fact_campaign_bounces fc on c.idCampaign_sk = fc.idCampaign_sk_fk
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	where c.idCampaign_sk = 1087
	group by c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email

	select top 1000 count(*) over (), 
		c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email,
		min(fc.event_date), count(*) num_actions		
	from 
			Warehouse.email.dim_campaign c
		inner join
			Warehouse.email.fact_campaign_unsubscribes fc on c.idCampaign_sk = fc.idCampaign_sk_fk
		inner join
			Warehouse.email.dim_subscribers s on fc.idSubscribers_sk_fk = s.idSubscribers_sk
	where c.idCampaign_sk = 1087
	group by c.idCampaign_sk, c.sendID_bk, c.email_name, c.email_subject, c.sent_date, c.job_status, 
		s.idSubscribers_sk, s.idCustomer_sk_fk, s.email
