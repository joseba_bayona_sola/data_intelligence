
drop table #email_engagement_ds_v

select -- top 1000 count(*) over (), 
	t.idCampaign_sk, 
	t.idSubscribers_sk, t.idCustomer_sk, t.subscriberkey, t.email,
	t.company_name_create, t.website_group_create, t.website_create,
	t.website_register, 
	t.customer_origin_name, t.current_customer_status_name, t.rank_num_tot_orders, t.num_tot_orders,
	t.customer_id, t.customer_name,
	t.delivered, t.not_delivered, t.sent_date,
	o.opens, o.open_date, c.clicks, c.click_date
into #email_engagement_ds_v
from
	(select
		idCampaign_sk, 
		idSubscribers_sk, idCustomer_sk, subscriberkey, email,
		company_name_create, website_group_create, website_create,
		website_register, 
		customer_origin_name, current_customer_status_name, rank_num_tot_orders, num_tot_orders,
		customer_id, customer_name,
		1 delivered, null not_delivered,
		event_date sent_date	
	from Warehouse.email.fact_campaign_sent_v
	where idCampaign_sk = 1087
	union
	select
		idCampaign_sk, 
		idSubscribers_sk, idCustomer_sk, subscriberkey, email,
		company_name_create, website_group_create, website_create,
		website_register, 
		customer_origin_name, current_customer_status_name, rank_num_tot_orders, num_tot_orders,
		customer_id, customer_name,
		null delivered, 1 not_delivered,
		event_date sent_date	
	from Warehouse.email.fact_campaign_not_sent_v
	where idCampaign_sk = 1087) t
left join
	(select idCampaign_sk, idSubscribers_sk, 
		count(*) opens, min(fc.event_date) open_date
	from Warehouse.email.fact_campaign_opens_v fc 
	where idCampaign_sk = 1087
	group by idCampaign_sk, idSubscribers_sk) o on t.idCampaign_sk = o.idCampaign_sk and t.idSubscribers_sk = o.idSubscribers_sk
left join
	(select idCampaign_sk, idSubscribers_sk, 
		count(*) clicks, min(fc.event_date) click_date
	from Warehouse.email.fact_campaign_clicks_v fc 
	where idCampaign_sk = 1087
	group by idCampaign_sk, idSubscribers_sk) c on t.idCampaign_sk = c.idCampaign_sk and t.idSubscribers_sk = c.idSubscribers_sk


select top 1000 *
from #email_engagement_ds_v

select top 1000 idCampaign_sk, count(*) num_sent, count(delivered) num_delivered, count(not_delivered) num_not_delivered, 
	sum(opens) num_opens, count(opens) num_unique_opens, 
	sum(clicks) num_clicks, count(clicks) num_unique_clicks
from #email_engagement_ds_v
group by idCampaign_sk


select top 1000 idCampaign_sk, count(*) num_sent, count(delivered) num_delivered, count(not_delivered) num_not_delivered, 
	sum(opens) num_opens, count(opens) num_unique_opens, 
	sum(clicks) num_clicks, count(clicks) num_unique_clicks, 
	sum(conversions) num_conversions, sum(bounces) num_bounces, sum(unsubscribes) num_unsubscribes
from Warehouse.tableau.email_engagement_ds_v
where idCampaign_sk = 1087
group by idCampaign_sk
order by idCampaign_sk desc

select top 1000 *
from Warehouse.email.email_engagement_ds_v
where idCampaign_sk = 1087


