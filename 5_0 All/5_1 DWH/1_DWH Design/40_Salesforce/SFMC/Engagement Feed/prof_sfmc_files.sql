
-- Subscribers_aud

select top 1000 ClientID, 
	SubscriberKey, EmailAddress, SubscriberID, Status,
	DateHeld, convert(datetime, DateCreated) DateCreated, DateUnsubscribed,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Subscribers_aud

	select count(*), count(distinct SubscriberID), count(distinct SubscriberKey)
	from Landing.sf_mc.Subscribers_aud

	select idETLBatchRun, count(*), count(distinct SubscriberID), count(distinct SubscriberKey)
	from Landing.sf_mc.Subscribers_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select Status, count(*)
	from Landing.sf_mc.Subscribers_aud
	group by Status
	order by Status

	select substring(EmailAddress, charindex('@', EmailAddress)+1, len(EmailAddress)), count(*)
	from Landing.sf_mc.Subscribers_aud
	group by substring(EmailAddress, charindex('@', EmailAddress)+1, len(EmailAddress))
	order by count(*) desc, substring(EmailAddress, charindex('@', EmailAddress)+1, len(EmailAddress))

	select year(convert(datetime, DateCreated)), month(convert(datetime, DateCreated)), day(convert(datetime, DateCreated)), count(*)
	from Landing.sf_mc.Subscribers_aud
	group by year(convert(datetime, DateCreated)), month(convert(datetime, DateCreated)), day(convert(datetime, DateCreated))
	order by year(convert(datetime, DateCreated)), month(convert(datetime, DateCreated)), day(convert(datetime, DateCreated))

-- SendJobs_aud

select ClientID,
	SendID, FromName, FromEmail,
	convert(datetime, SchedTime) SchedTime, convert(datetime, SentTime) SentTime, Subject, EmailName,
	TriggeredSendExternalKey, SendDefinitionExternalKey,
	JobStatus, PreviewURL, IsMultipart, Additional,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.SendJobs_aud
where SendID = 18277

	select count(*), count(distinct SendID), count(distinct EmailName)
	from Landing.sf_mc.SendJobs_aud

	select FromName, FromEmail, count(*)
	from Landing.sf_mc.SendJobs_aud
	group by FromName, FromEmail
	order by FromName, FromEmail

	select EmailName, count(*)
	from Landing.sf_mc.SendJobs_aud
	group by EmailName
	order by EmailName

	select EmailName, Subject, count(*)
	from Landing.sf_mc.SendJobs_aud
	group by EmailName, Subject
	order by EmailName, Subject

	select JobStatus, count(*)
	from Landing.sf_mc.SendJobs_aud
	group by JobStatus
	order by JobStatus

	select IsMultipart, count(*)
	from Landing.sf_mc.SendJobs_aud
	group by IsMultipart
	order by IsMultipart

	select year(convert(datetime, SentTime)), month(convert(datetime, SentTime)), day(convert(datetime, SentTime)), count(*)
	from Landing.sf_mc.SendJobs_aud
	group by year(convert(datetime, SentTime)), month(convert(datetime, SentTime)), day(convert(datetime, SentTime))
	order by year(convert(datetime, SentTime)), month(convert(datetime, SentTime)), day(convert(datetime, SentTime))

-- Send_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, BatchID,
	TriggeredSendExternalKey, campaignID, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Send_aud
where SendID = 18277

	select top 1000 count(*) over (), t1.* -- 0
	from 
			Landing.sf_mc.Send_aud t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null

	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.Send_aud

	select EventType, count(*)
	from Landing.sf_mc.Send_aud
	group by EventType
	order by EventType

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Send_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- NotSent_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, BatchID,
	TriggeredSendExternalKey, reason,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.NotSent_aud
where SendID = 18277

	select top 1000 count(*) over (), t1.* -- 1556
	from 
			Landing.sf_mc.NotSent_aud t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null

	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.NotSent_aud

	select EventType, count(*)
	from Landing.sf_mc.NotSent_aud
	group by EventType
	order by EventType

	select reason, count(*)
	from Landing.sf_mc.NotSent_aud
	group by reason
	order by reason

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.NotSent_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- Opens_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, BatchID,
	TriggeredSendExternalKey, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Opens_aud
where SendID = 18277

	select top 1000 count(*) over (), t1.* -- 42
	from 
			Landing.sf_mc.Opens_aud t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null

	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.Opens_aud

	select EventType, count(*)
	from Landing.sf_mc.Opens_aud
	group by EventType
	order by EventType

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Opens_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- Clicks_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, SendURLID, URLID, URL,
	Alias, BatchID, TriggeredSendExternalKey,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Clicks_aud
where SendID = 18277
-- where SendURLID = 279809 and URLID = 40707
-- where sendID = 18284 and SubscriberID = 4014862

	select top 1000 count(*) over (), t1.* -- 60
	from 
			Landing.sf_mc.Clicks_aud t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null

	select count(*), count(distinct SendID), count(distinct SubscriberID), count(distinct SendURLID)
	from Landing.sf_mc.Clicks_aud

	select EventType, count(*)
	from Landing.sf_mc.Clicks_aud
	group by EventType
	order by EventType

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Clicks_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

	select SendURLID, URLID, count(*)
	from Landing.sf_mc.Clicks_aud
	group by SendURLID, URLID
	order by SendURLID, URLID

	select alias, count(*)
	from Landing.sf_mc.Clicks_aud
	group by alias
	order by alias

-- Bounces_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, 
	BounceCategory, SMTPCode, BounceReason, BatchID, TriggeredSendExternalKey,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Bounces_aud

	select top 1000 count(*) over (), t1.* -- 0
	from 
			Landing.sf_mc.Bounces_aud t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null

	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.Bounces_aud

	select EventType, count(*)
	from Landing.sf_mc.Bounces_aud
	group by EventType
	order by EventType

	select BounceCategory, SMTPCode, BounceReason, count(*)
	from Landing.sf_mc.Bounces_aud
	group by BounceCategory, SMTPCode, BounceReason
	order by BounceCategory, SMTPCode, BounceReason

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Bounces_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- Conversions_aud

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, ReferringURL
	LinkAlias, ConversionData, BatchID, TriggeredSendExternalKey, URLID,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Conversions_aud

	select top 1000 count(*) over (), t1.* -- 0
	from 
			Landing.sf_mc.Unsubs t1
		left join
			Landing.sf_mc.Subscribers_aud t2 on t1.SubscriberID = t2.SubscriberID
	where t2.SubscriberID is null


	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.Conversions_aud

	select EventType, count(*)
	from Landing.sf_mc.Conversions_aud
	group by EventType
	order by EventType

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Conversions_aud
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- Unsubs

select ClientID,
	SendID, SubscriberKey, EmailAddress, SubscriberID, ListID,
	EventDate, EventType, Batchid, TriggeredSendExternalKey, UnsubReason,
	idETLBatchRun, ins_ts
from Landing.sf_mc.Unsubs

	select count(*), count(distinct SendID), count(distinct SubscriberID)
	from Landing.sf_mc.Unsubs

	select EventType, count(*)
	from Landing.sf_mc.Unsubs
	group by EventType
	order by EventType

	select UnsubReason, count(*)
	from Landing.sf_mc.Unsubs
	group by UnsubReason
	order by UnsubReason

	select year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate)), count(*)
	from Landing.sf_mc.Unsubs
	group by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))
	order by year(convert(datetime, EventDate)), month(convert(datetime, EventDate)), day(convert(datetime, EventDate))

-- Unsubs_dv_aud

select BusinessUnitID, 
	SubscriberID, SubscriberKey, 
	UnsubDateUTC, UnsubReason,
	idETLBatchRun, ins_ts, upd_ts
from Landing.sf_mc.Unsubs_dv_aud

	select count(*), count(distinct SubscriberID)
	from Landing.sf_mc.Unsubs_dv_aud

	select UnsubReason, count(*)
	from Landing.sf_mc.Unsubs_dv_aud
	group by UnsubReason
	order by UnsubReason

	select year(convert(datetime, UnsubDateUTC)), month(convert(datetime, UnsubDateUTC)), day(convert(datetime, UnsubDateUTC)), count(*)
	from Landing.sf_mc.Unsubs_dv_aud
	group by year(convert(datetime, UnsubDateUTC)), month(convert(datetime, UnsubDateUTC)), day(convert(datetime, UnsubDateUTC))
	order by year(convert(datetime, UnsubDateUTC)), month(convert(datetime, UnsubDateUTC)), day(convert(datetime, UnsubDateUTC))

-- COMPARE Unsubs against Unsubs_dv_aud

	select -- u1.ClientID, u1.SendID, u2.BusinessUnitID,
		u1.SubscriberKey, u2.SubscriberKey, u1.EmailAddress, u1.SubscriberID, u2.SubscriberID, 
		u1.EventDate, u2.UnsubDateUTC, u1.UnsubReason, u2.UnsubReason
	from 
			Landing.sf_mc.Unsubs_aud u1
		full join
			Landing.sf_mc.Unsubs_dv_aud u2 on u1.SubscriberID = u2.SubscriberID
	-- where u1.SubscriberID is not null and u2.SubscriberID is not null
	-- where u1.SubscriberID is not null and u2.SubscriberID is null
	where u1.SubscriberID is null and u2.SubscriberID is not null

-- Goals for Email Engagement Analysis

-- Data Design