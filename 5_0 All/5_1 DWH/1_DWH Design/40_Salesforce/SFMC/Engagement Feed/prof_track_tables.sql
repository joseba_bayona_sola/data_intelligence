
----------------------------------------------------
-- sfmc_send_feed

select ClientID, SendID, SubscriberKey, EmailAddress, SubscriberID, ListID, EventDate, EventType, BatchID, TriggeredSendExternalKey
from DW_GetLenses_jbs.dbo.sfmc_send_feed
order by SendID, SubscriberKey

	select count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed

	select ClientID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by ClientID
	order by ClientID

	select SendID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by SendID
	order by SendID

	select SendID, EventDate, count(*), count(*) over (partition by SendID) num_rep
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by SendID, EventDate
	order by SendID, EventDate

	select SendID, EmailAddress, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by SendID, EmailAddress
	order by count(*) desc

	select EmailAddress, SubscriberKey, SubscriberID, count(*), count(*) over (partition by EmailAddress) num_rep
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by EmailAddress, SubscriberKey, SubscriberID
	order by EmailAddress, SubscriberKey, SubscriberID

		select EmailAddress, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_send_feed
		group by EmailAddress
		order by EmailAddress

	select ListID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by ListID
	order by ListID	

	select EventType, BatchID, TriggeredSendExternalKey, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_send_feed
	group by EventType, BatchID, TriggeredSendExternalKey
	order by EventType, BatchID, TriggeredSendExternalKey

	
----------------------------------------------------
-- sfmc_open_feed

select ClientID, SendID, SubscriberKey, EmailAddress, SubscriberID, ListID, EventDate, EventType, BatchID, TriggeredSendExternalKey, 
	IsUnique, Browser, EmailClient, OperatingSystem, Device, 
	count(*) over (partition by SendID, SubscriberKey) num_rep, 
	dense_rank() over (partition by SendID, SubscriberKey order by EventDate) ord_rep
from DW_GetLenses_jbs.dbo.sfmc_open_feed
where SendID = 12638 and EmailAddress = 'emma-deangelis@outlook.com'
order by SendID, SubscriberKey, EventDate

	select count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed

	select ClientID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by ClientID
	order by ClientID

	select SendID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by SendID
	order by SendID

	select SendID, EmailAddress, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by SendID, EmailAddress
	order by count(*) desc

	select EmailAddress, SubscriberKey, SubscriberID, count(*), count(*) over (partition by EmailAddress) num_rep
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by EmailAddress, SubscriberKey, SubscriberID
	order by EmailAddress, SubscriberKey, SubscriberID

		select EmailAddress, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_open_feed
		group by EmailAddress
		order by EmailAddress

	select ListID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by ListID
	order by ListID	

	select EventType, BatchID, TriggeredSendExternalKey, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by EventType, BatchID, TriggeredSendExternalKey
	order by EventType, BatchID, TriggeredSendExternalKey

	select IsUnique, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by IsUnique
	order by IsUnique	

		select IsUnique, ord_rep, count(*)
		from
			(select ClientID, SendID, SubscriberKey, EmailAddress, SubscriberID, ListID, EventDate, EventType, BatchID, TriggeredSendExternalKey, 
				IsUnique, Browser, EmailClient, OperatingSystem, Device, 
				count(*) over (partition by SendID, SubscriberKey) num_rep, 
				dense_rank() over (partition by SendID, SubscriberKey order by EventDate) ord_rep
			from DW_GetLenses_jbs.dbo.sfmc_open_feed) t
		group by IsUnique, ord_rep
		order by IsUnique, ord_rep

	select Browser, EmailClient, OperatingSystem, Device, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_open_feed
	group by Browser, EmailClient, OperatingSystem, Device
	order by Browser, EmailClient, OperatingSystem, Device


----------------------------------------------------
-- sfmc_clicks_feed

select ClientID, SendID, SubscriberKey, EmailAddress, SubscriberID, ListID, EventDate, EventType,
	SendURLID, URLID, URL, Alias, 
	BatchID, TriggeredSendExternalKey, 
	Browser, EmailClient, OperatingSystem, Device
from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
where SendID = 13409 and EmailAddress = 'christianappiah@live.co.uk'
order by SendID, SubscriberKey, EventDate

	select count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed

	select ClientID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by ClientID
	order by ClientID

	select SendID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by SendID
	order by SendID

	select SendID, EmailAddress, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by SendID, EmailAddress
	order by count(*) desc

	select EmailAddress, SubscriberKey, SubscriberID, count(*), count(*) over (partition by EmailAddress) num_rep
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by EmailAddress, SubscriberKey, SubscriberID
	order by EmailAddress, SubscriberKey, SubscriberID

		select EmailAddress, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
		group by EmailAddress
		order by EmailAddress

	select ListID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by ListID
	order by ListID	

	select EventType, BatchID, TriggeredSendExternalKey, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by EventType, BatchID, TriggeredSendExternalKey
	order by EventType, BatchID, TriggeredSendExternalKey

	select Browser, EmailClient, OperatingSystem, Device, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by Browser, EmailClient, OperatingSystem, Device
	order by Browser, EmailClient, OperatingSystem, Device

	select SendURLID, URLID, URL, Alias, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
	group by SendURLID, URLID, URL, Alias
	order by SendURLID, URLID, URL, Alias

		select URLID, Alias, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_clicks_feed
		group by URLID, Alias
		order by URLID, Alias

----------------------------------------------------
-- sfmc_bounces_feed

select ClientID, SendID, SubscriberKey, EmailAddress, SubscriberID, ListID, EventDate, EventType,
	BounceCategory, SMTPCode, BounceReason, 
	BatchID, TriggeredSendExternalKey
from DW_GetLenses_jbs.dbo.sfmc_bounces_feed

	select count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed

	select ClientID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by ClientID
	order by ClientID

	select SendID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by SendID
	order by SendID

	select SendID, EmailAddress, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by SendID, EmailAddress
	order by count(*) desc

	select EmailAddress, SubscriberKey, SubscriberID, count(*), count(*) over (partition by EmailAddress) num_rep
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by EmailAddress, SubscriberKey, SubscriberID
	order by EmailAddress, SubscriberKey, SubscriberID

		select EmailAddress, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
		group by EmailAddress
		order by EmailAddress

	select ListID, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by ListID
	order by ListID	

	select EventType, BatchID, TriggeredSendExternalKey, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by EventType, BatchID, TriggeredSendExternalKey
	order by EventType, BatchID, TriggeredSendExternalKey

	select BounceCategory, SMTPCode, BounceReason, count(*)
	from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
	group by BounceCategory, SMTPCode, BounceReason
	order by BounceCategory, SMTPCode, BounceReason

		select BounceCategory, SMTPCode, count(*)
		from DW_GetLenses_jbs.dbo.sfmc_bounces_feed
		group by BounceCategory, SMTPCode
		order by BounceCategory, SMTPCode

----------------------------------------------------------------------------
----------------------------------------------------------------------------

select s.ClientID, s.SendID, 
	s.SubscriberKey, s.EmailAddress, s.SubscriberID, s.ListID, 
	s.EventType, s.EventDate, 
	count(*) over (partition by s.SendID, s.EmailAddress) num_open, o.EventType, o.EventDate, 
		o.IsUnique, o.Browser, o.EmailClient, o.OperatingSystem, o.Device
from 
		DW_GetLenses_jbs.dbo.sfmc_send_feed s
	left join
		DW_GetLenses_jbs.dbo.sfmc_open_feed o on s.SendID = o.SendID and s.EmailAddress = o.EmailAddress
order by s.SendID, s.EmailAddress, s.EventDate, o.EventDate

select s.ClientID, s.SendID, 
	s.SubscriberKey, s.EmailAddress, s.SubscriberID, s.ListID, 
	s.EventType, s.EventDate, 
	count(*) over (partition by s.SendID, s.EmailAddress) num_clicks, c.EventType, c.EventDate, 
		c.SendURLID, c.URLID, c.URL, c.Alias, 
		c.Browser, c.EmailClient, c.OperatingSystem, c.Device
from 
		DW_GetLenses_jbs.dbo.sfmc_send_feed s
	left join
		DW_GetLenses_jbs.dbo.sfmc_clicks_feed c on s.SendID = c.SendID and s.EmailAddress = c.EmailAddress
order by s.SendID, s.EmailAddress, s.EventDate, c.EventDate

select s.ClientID, s.SendID, 
	s.SubscriberKey, s.EmailAddress, s.SubscriberID, s.ListID, 
	s.EventType, s.EventDate, 
	count(*) over (partition by s.SendID, s.EmailAddress) num_bounces, b.EventType, b.EventDate, 
		b.BounceCategory, b.SMTPCode, b.BounceReason
from 
		DW_GetLenses_jbs.dbo.sfmc_send_feed s
	left join
		DW_GetLenses_jbs.dbo.sfmc_bounces_feed b on s.SendID = b.SendID and s.EmailAddress = b.EmailAddress
order by s.SendID, s.EmailAddress, s.EventDate, b.EventDate

----------------------------------------------------------------------------
----------------------------------------------------------------------------

select s.ClientID, s.SendID, 
	s.SubscriberKey, s.EmailAddress, s.SubscriberID, s.ListID, 
	s.EventType, s.EventDate, 
	count(o.SendID) over (partition by s.SendID, s.EmailAddress) num_open, o.EventType, o.EventDate, 
		o.IsUnique, o.Browser, o.EmailClient, o.OperatingSystem, o.Device,
	count(c.SendID) over (partition by s.SendID, s.EmailAddress) num_clics, c.EventType, c.EventDate, 
		c.SendURLID, c.URLID, c.URL, c.Alias, 
		c.Browser, c.EmailClient, c.OperatingSystem, c.Device
from 
		DW_GetLenses_jbs.dbo.sfmc_send_feed s
	left join
		DW_GetLenses_jbs.dbo.sfmc_open_feed o on s.SendID = o.SendID and s.EmailAddress = o.EmailAddress
	left join
		DW_GetLenses_jbs.dbo.sfmc_clicks_feed c on s.SendID = c.SendID and s.EmailAddress = c.EmailAddress
where 
	(s.SendID = 12638 and s.EmailAddress = 'emma-deangelis@outlook.com') or
	(s.SendID = 13409 and s.EmailAddress = 'christianappiah@live.co.uk') 
order by s.SendID, s.EmailAddress, s.EventDate, o.EventDate, c.EventDate