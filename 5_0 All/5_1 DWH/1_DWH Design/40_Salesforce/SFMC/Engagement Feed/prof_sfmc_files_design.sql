
-- Send_aud

select sj.ClientID,
	sj.SendID, sj.FromName, sj.FromEmail,
	sj.SchedTime, sj.SentTime, sj.Subject, sj.EmailName, sj.JobStatus, 
	s.SubscriberKey, s.EmailAddress, s.SubscriberID, convert(datetime, s.EventDate) EventDate
from
		(select ClientID,
			SendID, FromName, FromEmail,
			convert(datetime, SchedTime) SchedTime, convert(datetime, SentTime) SentTime, Subject, EmailName, JobStatus
		from Landing.sf_mc.SendJobs_aud
		-- where SendID = 16949
		where EmailName = 'VD_UK_IPG1_NEW_ROUTINE') sj
	inner join
		Landing.sf_mc.Send_aud s on sj.SendID = s.SendID

select sj.SendID, sj.FromName, sj.FromEmail, sj.Subject, sj.EmailName, sj.SentTime, count(*)
from
		(select ClientID,
			SendID, FromName, FromEmail,
			convert(datetime, SchedTime) SchedTime, convert(datetime, SentTime) SentTime, Subject, EmailName, JobStatus
		from Landing.sf_mc.SendJobs_aud
		-- where SendID = 16949
		where EmailName = 'VD_UK_IPG1_NEW_ROUTINE') sj
	inner join
		Landing.sf_mc.Send_aud s on sj.SendID = s.SendID
group by sj.SendID, sj.FromName, sj.FromEmail, sj.Subject, sj.EmailName, sj.SentTime
order by sj.SendID, sj.FromName, sj.FromEmail, sj.Subject, sj.EmailName, sj.SentTime

-- Conversions

select c.ClientID,
	c.SendID, c.SubscriberKey, c.EmailAddress, c.SubscriberID, 
	convert(datetime, c.EventDate), c.EventType, c.SendURLID, c.URLID, c.URL, 
	oh.entity_id, oh.increment_id, oh.created_at, oh.store_id, oh.customer_id, oh2.ga_medium, oh2.ga_source, oh2.marketing_channel_name_bk
from 
		Landing.sf_mc.Clicks_aud c
	inner join
		Landing.sf_mc.Subscribers_aud s on c.SubscriberID = s.SubscriberID
	inner join
		Landing.mag.sales_flat_order oh on c.EmailAddress = oh.customer_email
			and convert(date, c.EventDate) = convert(date, oh.created_at)
	inner join
		Landing.aux.sales_dim_order_header oh2 on oh.entity_id = oh2.order_id_bk
order by convert(datetime, c.EventDate), oh.customer_id, oh.created_at

