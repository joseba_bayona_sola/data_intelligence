
drop table #email_engagement_aggr_ds_v

select s.idCampaign_sk, 
	ca.sendID_bk, ca.campaign_website, ca.email_name, ca.email_subject, ca.sent_date, ca.job_status, 
	s.num_sent + isnull(ns.num_not_sent, 0) num_sent, s.num_sent num_delivered, isnull(ns.num_not_sent, 0) num_not_delivered,
	isnull(o.num_opens, 0) num_opens, isnull(o.num_unique_opens, 0) num_unique_opens, 
	isnull(c.num_clicks, 0) num_clicks, isnull(c.num_unique_clicks, 0) num_unique_clicks, 
	isnull(co.num_conversions, 0) num_conversions,
	isnull(b.num_hard_bounces, 0) num_hard_bounces, isnull(b.num_soft_bounces, 0) num_soft_bounces, isnull(u.num_unsubscribes, 0) num_unsubscribes
into #email_engagement_aggr_ds_v
from
		Warehouse.email.dim_campaign_v ca
	inner join
		(select idCampaign_sk, count(idSubscribers_sk) num_sent--, count(distinct idSubscribers_sk) num_distinct_sent
		from Warehouse.email.fact_campaign_sent_v
		group by idCampaign_sk) s on ca.idCampaign_sk = s.idCampaign_sk
	left join
		(select idCampaign_sk, count(idSubscribers_sk) num_not_sent--, count(distinct idSubscribers_sk) num_distinct_not_sent
		from Warehouse.email.fact_campaign_not_sent_v
		group by idCampaign_sk) ns on s.idCampaign_sk = ns.idCampaign_sk
	left join
		(select idCampaign_sk, count(idSubscribers_sk) num_opens, count(distinct idSubscribers_sk) num_unique_opens
		from Warehouse.email.fact_campaign_opens_v
		group by idCampaign_sk) o on s.idCampaign_sk = o.idCampaign_sk
	left join
		(select idCampaign_sk, count(idSubscribers_sk) num_clicks, count(distinct idSubscribers_sk) num_unique_clicks
		from Warehouse.email.fact_campaign_clicks_v
		group by idCampaign_sk) c on s.idCampaign_sk = c.idCampaign_sk
	left join
		(select idCampaign_sk, count(idSubscribers_sk) num_conversions
		from Warehouse.email.fact_campaign_conversions_v
		group by idCampaign_sk) co on s.idCampaign_sk = co.idCampaign_sk
	left join
		(select idCampaign_sk, 
			count(hard_bounce_flag) num_hard_bounces, count(soft_bounce_flag) num_soft_bounces
			-- count(distinct idSubscribers_sk) num_distinct_bounces
		from
			(select *, 
				case when (bounce_category <> 'Soft bounce') then 'Y' else NULL end hard_bounce_flag, 
				case when (bounce_category = 'Soft bounce') then 'Y' else NULL end soft_bounce_flag 
			from Warehouse.email.fact_campaign_bounces_v) t
		group by idCampaign_sk) b on s.idCampaign_sk = b.idCampaign_sk
	left join
		(select idCampaign_sk, count(idSubscribers_sk) num_unsubscribes
		from Warehouse.email.fact_campaign_unsubscribes_v
		group by idCampaign_sk) u on s.idCampaign_sk = u.idCampaign_sk
-- where s.idCampaign_sk = 1087
order by s.idCampaign_sk desc

	select idCampaign_sk, 
		sendID_bk, campaign_website, email_name, email_subject, sent_date, job_status, 
		num_sent, num_delivered, num_not_delivered,
		num_opens, num_opens * 100 / convert(decimal(12, 4), num_sent) perc_opens, num_unique_opens, num_unique_opens * 100 / convert(decimal(12, 4), num_sent) perc_unique_opens, 
		num_clicks, num_clicks * 100 / convert(decimal(12, 4), num_sent) perc_clicks, num_unique_clicks, num_unique_clicks * 100 / convert(decimal(12, 4), num_sent) perc_unique_clicks, null unique_clicks_to_open,
		num_conversions,
		num_hard_bounces, num_soft_bounces, num_unsubscribes
	from #email_engagement_aggr_ds_v
	-- where num_sent > 10
	-- where idCampaign_sk = 1087
	order by idCampaign_sk desc




-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

