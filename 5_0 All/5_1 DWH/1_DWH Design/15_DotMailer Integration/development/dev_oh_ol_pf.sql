
	select top 1000 entity_id, increment_id, store_id, customer_id, created_at, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder, 
		presc_verification_method, prescription_verification_type, 
		referafriend_code, referafriend_referer
	from Landing.mag.sales_flat_order_aud
	where customer_id in (726063, 2326232, 1635930, 452363)
	order by customer_id, created_at

	-- Check how are used in Normal ETL
		-- reminder_type_name_bk: based on reminder_type
		-- reminder_period_bk: based on reminder_period
		-- reminder_date: based on reminder_date
		-- reminder_sent: based on reminder_sent

		-- reorder_f, reorder_date: based on po_reorder_profile_aud table

	-- Add reminder, reorder data, last shipment, prescription

	select oh.idOrderHeader_sk,
		oh.order_id_bk order_id, oh.order_no, oh.order_date, oh.shipment_date,
		s.store_id_bk store_id, -- s.website, s.store_name, 
		oh.idCustomer_sk_fk, -- c.customer_id customer_id, c.customer_email, 
		oh.customer_order_seq_no,
		-- o_stat.order_status_name, 
		oh.reminder_date, oh2.reminder_type, oh2.reminder_mobile, oh2.reminder_period, oh2.reminder_presc, 
		case when (oh.reminder_date > getutcdate()) then 'F' else 'P' end reminder_flag,
		-- dense_rank() over (partition by oh.idCustomer_sk_fk order by case when (oh.reminder_date > getutcdate()) then 'F' else 'P' end, oh.reminder_date, oh.order_id_bk) ord_reminder,
		dense_rank() over (partition by oh.idCustomer_sk_fk order by case when (oh.reminder_date > getutcdate()) then 'F' else 'P' end, 
			abs(datediff(dd, getutcdate(), isnull(oh.reminder_date, dateadd(year, 99, getutcdate())))), oh.order_id_bk desc) ord_reminder,
		-- abs(datediff(dd, getutcdate(), isnull(oh.reminder_date, dateadd(year, 99, getutcdate())))),
			-- case when (oh.reminder_date > getutcdate()) then oh.reminder_date else oh.reminder_date desc end, oh.order_id_bk) ord_reminder
		oh.reorder_f, oh.reorder_date, 

		count(*) over (partition by oh.idCustomer_sk_fk ) cnt_oh,
		dense_rank() over (partition by oh.idCustomer_sk_fk order by oh.order_date, oh.order_id_bk) ord_oh
	-- into #oh
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.sales.dim_order_status o_stat on oh.idOrderStatus_sk_fk = o_stat.idOrderStatus_sk
		left join
			Landing.mag.sales_flat_order_aud oh2 on oh.order_id_bk = oh2.entity_id
		inner join
			Warehouse.act.dim_customer_signature_v c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
	where o_stat.order_status_name in ('OK', 'PARTIAL REFUND')
		and c.customer_id in (726063, 2326232, 1635930, 452363)
	order by c.customer_id, oh.order_date

	-- Add filters for exclusion depending on the product

	select oh.idOrderHeader_sk,
		ol.order_line_id_bk order_line_id, oh.order_id_bk order_id, oh.order_no, oh.order_date, oh.customer_order_seq_no,
		s.store_id_bk store_id, s.website, s.store_name, 
		oh.idCustomer_sk_fk, c.customer_id customer_id, c.customer_email, 
		o_stat.order_status_name, 

		pf.product_type_name_dm, pf.contact_lens_f_dm, pf.product_id_magento, pf.magento_sku product_family_code, pf.product_family_name, 

		ol.qty_unit, convert(decimal(12, 4), ol.local_subtotal * ol.local_to_global_rate) global_subtotal
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.act.dim_customer_signature_v c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			Warehouse.sales.dim_order_status o_stat on oh.idOrderStatus_sk_fk = o_stat.idOrderStatus_sk
		inner join
			Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk 
		inner join
			Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
	where o_stat.order_status_name in ('OK', 'PARTIAL REFUND')
		and c.customer_id in (726063, 2326232, 1635930, 452363)
	order by c.customer_id, oh.order_date

	select 
		oh.order_id_bk order_id, oh.order_no, oh.order_date, oh.customer_order_seq_no,
		s.store_id_bk store_id, -- s.website, s.store_name, 
		oh.idCustomer_sk_fk, -- c.customer_id customer_id, c.customer_email, 
		-- o_stat.order_status_name, 

		pf.product_type_name_dm, pf.contact_lens_f_dm, pf.product_id_magento, -- pf.magento_sku product_family_code, pf.product_family_name, 

			sum(ol.qty_unit) qty_unit, sum(convert(decimal(12, 4), ol.local_subtotal * ol.local_to_global_rate)) global_subtotal
	-- into #ol
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.act.dim_customer_signature_v c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			Warehouse.sales.dim_order_status o_stat on oh.idOrderStatus_sk_fk = o_stat.idOrderStatus_sk
		inner join
			Warehouse.sales.fact_order_line ol on oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk 
		inner join
			Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
	where o_stat.order_status_name in ('OK', 'PARTIAL REFUND')
		and c.customer_id in (726063, 2326232, 1635930, 452363)
	group by oh.order_id_bk, oh.order_no, oh.order_date, oh.customer_order_seq_no,
		s.store_id_bk, s.website, s.store_name, 
		oh.idCustomer_sk_fk, c.customer_id, c.customer_email, 
		o_stat.order_status_name, 

		pf.product_type_name_dm, pf.contact_lens_f_dm, pf.product_id_magento, pf.magento_sku, pf.product_family_name
	order by c.customer_id, oh.order_date, pf.product_id_magento

----------------------------------------------------------

-- NEED TO CALCULATE: 
	-- last (For CL, All) (Qty)

	-- product: last + previous

	-- reminder product

	select order_id, order_date, customer_order_seq_no, store_id, idCustomer_sk_fk, 
		product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
		qty_unit, global_subtotal, 

		dense_rank() over (partition by idCustomer_sk_fk order by order_date, order_id) ord_oh, 
		max(order_date) over (partition by idCustomer_sk_fk, product_id_magento) max_pf, 
		count(*) over (partition by idCustomer_sk_fk, product_id_magento) num_pf, 
		sum(global_subtotal) over (partition by idCustomer_sk_fk, product_id_magento) sum_pf 
	from #ol
	order by idCustomer_sk_fk, order_date, product_id_magento

	select order_id, order_date, customer_order_seq_no, store_id, idCustomer_sk_fk, 
		product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
		qty_unit, global_subtotal, 

		dense_rank() over (partition by idCustomer_sk_fk, product_type_name_dm order by max_pf desc, sum_pf desc, product_id_magento) ord_pf_category,
		dense_rank() over (partition by idCustomer_sk_fk, contact_lens_f_dm order by max_pf desc, sum_pf desc, product_id_magento) ord_pf_cl,
		dense_rank() over (partition by idCustomer_sk_fk order by max_pf desc, sum_pf desc, product_id_magento) ord_pf,

		ord_oh, max_pf, num_pf, sum_pf
	from
		(select order_id, order_date, customer_order_seq_no, store_id, idCustomer_sk_fk, 
			product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
			qty_unit, global_subtotal, 

			dense_rank() over (partition by idCustomer_sk_fk order by order_date, order_id) ord_oh, 
			max(order_date) over (partition by idCustomer_sk_fk, product_id_magento) max_pf, 
			count(*) over (partition by idCustomer_sk_fk, product_id_magento) num_pf, 
			sum(global_subtotal) over (partition by idCustomer_sk_fk, product_id_magento) sum_pf 
		from #ol) ol
	order by idCustomer_sk_fk, order_date, product_id_magento

-- 

	-- Add store_id, category ??

	select distinct idCustomer_sk_fk, product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
		ord_pf_category, ord_pf_cl, ord_pf, max_pf, num_pf, sum_pf
	-- select distinct idCustomer_sk_fk, product_id_magento
	-- into #ol_pf
	from		
		(select order_id, order_date, customer_order_seq_no, store_id, idCustomer_sk_fk, 
			product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
			qty_unit, global_subtotal, 

			dense_rank() over (partition by idCustomer_sk_fk, product_type_name_dm order by max_pf desc, sum_pf desc, product_id_magento) ord_pf_category,
			dense_rank() over (partition by idCustomer_sk_fk, contact_lens_f_dm order by max_pf desc, sum_pf desc, product_id_magento) ord_pf_cl,
			dense_rank() over (partition by idCustomer_sk_fk order by max_pf desc, sum_pf desc, product_id_magento) ord_pf,

			ord_oh, max_pf, num_pf, sum_pf
		from
			(select order_id, order_date, customer_order_seq_no, store_id, idCustomer_sk_fk, 
				product_type_name_dm, contact_lens_f_dm, product_id_magento, product_family_name, 
				qty_unit, global_subtotal, 

				dense_rank() over (partition by idCustomer_sk_fk order by order_date, order_id) ord_oh, 
				max(order_date) over (partition by idCustomer_sk_fk, product_id_magento) max_pf, 
				count(*) over (partition by idCustomer_sk_fk, product_id_magento) num_pf, 
				sum(global_subtotal) over (partition by idCustomer_sk_fk, product_id_magento) sum_pf 

			from #ol) ol) ol
	-- order by idCustomer_sk_fk, ord_pf
	order by idCustomer_sk_fk, product_type_name_dm, ord_pf_category

	select *
	from #ol_pf
	where ord_pf_category in (1, 2)
		and product_type_name_dm = 'Contact Lenses - Colours' -- Contact Lenses - Dailies / Contact Lenses - Monthlies & Other / Contact Lenses - Colours / Solutions & Eye Care / Other / Sunglasses / Glasses
	order by idCustomer_sk_fk, product_type_name_dm, ord_pf_category

	select *
	from #ol_pf
	where ord_pf_cl in (1, 2) and contact_lens_f_dm = 'Y'
	order by idCustomer_sk_fk, product_type_name_dm, ord_pf_category

	select *
	from #ol_pf
	where ord_pf in (1, 2)
	order by idCustomer_sk_fk, product_type_name_dm, ord_pf_category
