
select *
from Warehouse.prod.dim_product_family_v
order by product_id_magento

select product_type_name, category_name, product_type_oh_name, count(*)
from Warehouse.prod.dim_product_family_v
group by product_type_name, category_name, product_type_oh_name
order by product_type_name, category_name, product_type_oh_name

select *
from Warehouse.prod.dim_product_type_oh
order by product_type_oh_name

select *
from Warehouse.prod.dim_product_type_vat
order by product_type_oh_name

-- Contact Lenses - Dailies
-- Contact Lenses - Monthlies & Other
-- Contact Lenses - Colours
-- Solutions & Eye Care
-- Sunglasses
-- Glasses
-- Other

--------------------------------------------------------

select idProductFamily_sk, product_id_magento, product_id, product_type_name, category_name, product_type_oh_name, product_family_name, 
	case 
		when (product_type_oh_name = 'Colour') then 'Contact Lenses - Colours'
		when (category_name = 'CL - Daily' and product_type_oh_name <> 'Colour') then 'Contact Lenses - Dailies'
		when (category_name in ('CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') and product_type_oh_name <> 'Colour') then 'Contact Lenses - Monthlies & Other'
		when (product_type_name = 'Solutions & Eye Care') then 'Solutions & Eye Care'
		when (product_type_name = 'Sunglasses') then 'Sunglasses'
		when (product_type_name = 'Glasses') then 'Glasses'
		else 'Other'
	end product_type_dm, 
	case when (product_type_name = 'Contact Lenses') then 'Y' else 'N' end contact_lens_f_dm
into #pf
from Warehouse.prod.dim_product_family_v
-- where product_type_oh_name is null
order by product_type_dm, product_id_magento

	select top 1000 *
	from #pf
	order by product_type_dm, product_id_magento

	select product_type_dm, count(*)
	from #pf
	group by product_type_dm
	order by product_type_dm

	select product_type_dm, product_type_name, category_name, product_type_oh_name, count(*)
	from #pf
	group by product_type_dm, product_type_name, category_name, product_type_oh_name
	order by product_type_dm, product_type_name, category_name, product_type_oh_name
