
-- Landing

	select *
	from Landing.mag.core_store

	select *
	from Landing.mag.core_config_data

	select top 1000 *
	from Landing.mag.customer_entity_flat_aud
	where entity_id in (726063, 2326232, 1635930, 452363)
	order by entity_id

	select *
	from Landing.mag.catalog_product_entity_flat_aud

	-- Landing.mag.sales_flat_order
	-- Landing.mag.sales_flat_order_item

	select top 1000 *
	from Landing.mag.wearer

	select top 1000 *
	from Landing.mag.wearer_prescription

	select top 1000 *
	from Landing.mag.wearer_order_item

	select top 1000 entity_id, increment_id, store_id, customer_id, created_at, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder, 
		presc_verification_method, prescription_verification_type, 
		referafriend_code, referafriend_referer
	from Landing.mag.sales_flat_order_aud
	where customer_id in (726063, 2326232, 1635930, 452363)
	order by customer_id, created_at

-- Warehouse

select *
from Warehouse.prod.dim_product_family
where promotional_product = 1

select *
from Warehouse.sales.dim_order_header_v
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id_bk

select *
from Warehouse.sales.fact_order_line_v
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id_bk, product_id_magento


select *
from Warehouse.gen.dim_customer
where customer_id_bk in (726063, 2326232, 1635930, 452363)
order by customer_id_bk

select *
from Warehouse.gen.dim_customer_scd_v
where customer_id_bk in (726063, 2326232, 1635930, 452363)
order by customer_id_bk

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id


select *
from Warehouse.act.fact_customer_product_signature_v
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, category_name, product_id_magento

--------------------------

select *
from Warehouse.act.fact_customer_signature_aux_customers
union
select idCustomer_sk_fk
from Warehouse.gen.dim_customer_scd_v
where customer_id_bk in (726063, 2326232, 1635930, 452363)
