
	select top 1000 entity_id, website_id, store_id, 
		-- prefix, firstname, lastname, email, created_at, 
		referafriend_code, unsubscribe_all, unsubscribe_all_date, old_access_cust_no
	from Landing.mag.customer_entity_flat_aud
	where entity_id in (726063, 2326232, 1635930, 452363)
	order by entity_id


	-- Check how are used in Normal ETL
		-- referafriend_code: used
		-- unsubscribe_all: used for unsubscribe_mark_email_magento_f
		-- unsubscribe_all_date: used for unsubscribe_mark_email_magento_d (but only when unsubscribe_mark_email_magento_f = 'Y')
		-- old_access_cust_no: used for migrate_customer_id (with old_customer_id) 

	select *
	from Warehouse.gen.dim_customer
	where customer_id_bk in (726063, 2326232, 1635930, 452363)

	select top 1000 *
	from Warehouse.gen.dim_customer_scd
	where customer_id_bk in (726063, 2326232, 1635930, 452363)
	order by customer_id_bk, idCustomerSCD_sk

	select *
	from Warehouse.gen.dim_customer_scd_v
	where customer_id_bk in (726063, 2326232, 1635930, 452363)

	select *
	from Landing.aux.gen_dim_customer_aud
	where customer_id_bk in (726063, 2326232, 1635930, 452363)
