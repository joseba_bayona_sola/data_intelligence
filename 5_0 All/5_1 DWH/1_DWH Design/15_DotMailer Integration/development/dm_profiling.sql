
select top 1000 *
from Warehouse.dm.fact_customer_signature_dm 
order by client_id desc

	select count(*)
	from Warehouse.dm.fact_customer_signature_dm 

	select top 1000 ins_ts, count(*)
	from Warehouse.dm.fact_customer_signature_dm 
	group by ins_ts
	order by ins_ts

	select top 1000 upd_ts, count(*)
	from Warehouse.dm.fact_customer_signature_dm 
	group by upd_ts
	order by upd_ts

	select store_id, count(*)
	from Warehouse.dm.fact_customer_signature_dm 
	where (registration_date > '2019-09-01' or last_order_date > '2019-09-01')
	group by store_id
	order by store_id


	-- select top 1000 client_id, store_id, email, segment_lifecycle, registration_date, last_order_date, ins_ts, upd_ts
	select *
	-- from Warehouse.dm.fact_customer_signature_dm 
	from Warehouse.dm.EMV_sync_list
	where -- store_id in (28, 29) and
		(ins_ts between '2019-10-23 04:00:01.000' and '2019-10-24 04:00:01.000' or upd_ts between '2019-10-23 04:00:01.000' and '2019-10-24 04:00:01.000')
	order by store_id, client_id desc

-- currency: wrong in store_ids in (29, 30, 31) - wrong in core_config_date
-- language: wrong in store_ids in (30)
select top 1000 website_id, store_id, store_url, email_origine, currency, language, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by website_id, store_id, store_url, email_origine, currency, language
order by website_id, store_id, store_url, email_origine, currency, language

select title, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by title
order by title

select gender, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by gender
order by gender

select count(*)
from Warehouse.dm.fact_customer_signature_dm 
where dateofbirth is not null

-- reminder_date: why null in NEW, REGULAR, REACTIVATED / LAPSED // coming from sales_flat_order (NULL values for some reason ??)
select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where order_reminder_date is null
group by segment_lifecycle
order by segment_lifecycle

	select *
	from Warehouse.dm.fact_customer_signature_dm 
	where order_reminder_date is null and segment_lifecycle not in ('RNB', 'LAPSED')
	order by last_order_date desc

	select top 1000 *
	from Warehouse.dm.fact_customer_signature_dm 
	where order_reminder_date is null and segment_lifecycle in ('LAPSED') and segment_geog is null
	order by last_order_date desc

	select segment_geog, count(*)
	from Warehouse.dm.fact_customer_signature_dm 
	where order_reminder_date is null and segment_lifecycle in ('LAPSED')
	group by segment_geog
	order by count(*) desc

select signed_up_to_autoreorder, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by signed_up_to_autoreorder
order by signed_up_to_autoreorder

-- prescription_wearer: Why always NULL: Only 39 cases where reminder_presc=on - don't match with any OH in Landing.mag.wearer_order_item_aud
select prescription_wearer, count(*)
from DW_GetLenses_jbs.dbo.fact_customer_signature_dm 
group by prescription_wearer
order by prescription_wearer

select website_unsubscribe, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by website_unsubscribe
order by website_unsubscribe

-- w_unsub_date: why null values (Coming from Magento)
select top 1000 count(*) over (), count(*) over (partition by website_unsubscribe), *
from Warehouse.dm.fact_customer_signature_dm 
where w_unsub_date is null
order by client_id desc

select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where registration_date is null
group by segment_lifecycle
order by segment_lifecycle

select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where first_order_date is null
group by segment_lifecycle
order by segment_lifecycle

-- last_logged_in_date: Why NULL values in NEW, REGULAR, REACTIVATED
select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where last_logged_in_date is null
group by segment_lifecycle
order by segment_lifecycle

	select *
	from Warehouse.dm.fact_customer_signature_dm 
	where last_logged_in_date is null and segment_lifecycle not in ('RNB', 'LAPSED')
	order by last_order_date desc

select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where last_order_date is null
group by segment_lifecycle
order by segment_lifecycle

select num_of_orders, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by num_of_orders
order by num_of_orders

select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by segment_lifecycle
order by segment_lifecycle

select segment_geog, count(*)
from Warehouse.dm.fact_customer_signature_dm 
group by segment_geog
order by count(*) desc

-- segment_2: Why RAF with null values
select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where segment_2 is null
group by segment_lifecycle
order by segment_lifecycle

	select top 1000 *
	from Warehouse.dm.fact_customer_signature_dm 
	where segment_2 is null
	order by registration_date desc

	select top 1000 segment_geog, count(*)
	from Warehouse.dm.fact_customer_signature_dm 
	where segment_2 is null
	group by segment_geog
	order by segment_geog desc

-- segment_3: Why autologin with NULL values (Strange values in Magento - deleted ??)
select segment_lifecycle, count(*)
from Warehouse.dm.fact_customer_signature_dm 
where segment_3 is null
group by segment_lifecycle
order by segment_lifecycle

	select top 1000 *
	from Warehouse.dm.fact_customer_signature_dm 
	where segment_3 is null
	order by registration_date desc

-- check_sum

------------------------------------------------

-- Product Categories

select product_id_magento, product_type_name, product_type_name_dm, category_name, product_family_name
from Warehouse.prod.dim_product_family_v
order by product_type_name_dm, category_name, product_id_magento
