
	-- FOR: PRESCRIPTION_EXPIRY_DATE - PRESCRIPTION_WEARER - PRESCRIPTION_OPTICIAN

	-- 154854
	select top 1000 count(*) over (), count(*) over (partition by customer_id) num_rep, *
	from Landing.mag.wearer_aud
	where customer_id in (726063, 2326232, 1635930, 452363, 149485)
	order by num_rep desc, customer_id, id

		select top 1000 count(*), count(distinct customer_id)
		from Landing.mag.wearer_aud

	-- 233375
	select top 1000 count(*) over (), *
	from Landing.mag.wearer_prescription_aud

	select top 1000 count(*) over (), *
	from Landing.mag.wearer_order_item_aud

	-- only prescription ON and with reminder_date in future
	select top 1000 count(*) over (), entity_id, increment_id, reminder_presc, reminder_date
	from Landing.mag.sales_flat_order_aud
	where reminder_presc = 'on' -- and reminder_date > getdate() 
	order by reminder_date desc

	-- 233368
	select count(*) over (partition by w.id),
		w.id, w.customer_id, w.name, w.dob, 
		wp.id, wp.date_last_prescription, wp.optician_name
	from 
			Landing.mag.wearer_aud w
		inner join
			Landing.mag.wearer_prescription_aud wp on w.id = wp.wearer_id
	where w.customer_id in (726063, 2326232, 1635930, 452363, 149485)
	order by w.id, wp.id


	-- 233368
	select count(*) over (partition by w.id),
		w.id, wp.id, woi.id, 
		w.customer_id, w.name, w.dob, 
		wp.date_last_prescription, wp.optician_name, 
		woi.order_id
	from 
			Landing.mag.wearer_aud w
		inner join
			Landing.mag.wearer_prescription_aud wp on w.id = wp.wearer_id
		inner join
			Landing.mag.wearer_order_item_aud woi on wp.id = woi.wearer_prescription_id
	-- where w.customer_id in (726063, 2326232, 1635930, 452363, 149485)
	order by w.id, wp.id, woi.id

	-- Check how are used in Normal ETL
		-- Landing.mag.wearer_aud: for customer.dob (logic for only taking one)

		-- In sales OH - prescription_method_name_bk (but using sales_flat_order presc_verification_method, postoptics_auto_verification)