
select top 1000 count(*) over (partition by customer_email) num_rep, *
from Warehouse.gen.dim_customer_v
order by num_rep desc

	drop table #rep_cust

	select num_rep, idCustomer_sk, customer_id_bk, customer_email, store_id_bk, created_at, first_name, last_name
	into #rep_cust
	from	
		(select count(*) over (partition by customer_email) num_rep, idCustomer_sk, customer_id_bk, customer_email, store_id_bk, created_at, first_name, last_name
		from Warehouse.gen.dim_customer_v) c
	where num_rep > 1

		-- GDPR
		select c.*, c2.customer_id
		from 
				#rep_cust c
			left join
				Landing.map.gen_gdpr_customer_list c2 on c.customer_id_bk = c2.customer_id
		order by c.num_rep desc, c.customer_email, c.created_at

		-- Customer Signature
		select top 1000 c.num_rep, cs.customer_id, cs.customer_email, cs.register_date, cs.first_order_date, cs.last_order_date, cs.num_tot_orders
		from 
				Warehouse.act.fact_customer_signature_v cs
			inner join
				#rep_cust c on cs.customer_id = c.customer_id_bk
		order by c.num_rep desc, cs.customer_email, cs.register_date

		-- OH
		select c.num_rep, oh.customer_id, oh.customer_email, oh.order_id_bk, oh.order_date, oh.website, oh.customer_order_seq_no_gen
		from 
				Warehouse.sales.dim_order_header_v oh
			inner join
				#rep_cust c on oh.customer_id = c.customer_id_bk
		order by c.num_rep desc, oh.customer_email, oh.order_date

		-- SUPPRESED
		select *
		from Landing.mag.dotmailer_suppressed_contact_aud
		where email in (select distinct customer_email from #rep_cust)

		-- IMPORTED
		select *
		from Landing.mag.dotmailer_synced_contact_aud
		where email in (select distinct customer_email from #rep_cust)

-- MAGENTO DB QUERY
--select email, num_rep, num_rep_store
--from
--  (select email, count(*) num_rep, count(distinct website_id) num_rep_store
--  from magento01.customer_entity
--  group by email) c
--where num_rep > 1  
--order by num_rep desc, email
--limit 1000

--select c1.*
--from 
--    magento01.customer_entity c1
--  inner join
--    (select email, num_rep
--    from
--      (select email, count(*) num_rep
--      from magento01.customer_entity
--      group by email) c
--    where num_rep > 1) c2 on c1.email = c2.email 
--order by c1.email, c1.created_at

--------------------------------------

	select top 1000 email, store_id, timestamp
	from Landing.mag.dotmailer_suppressed_contact_aud
	where email = 'abenapa@gmail.com'

	select count(*), count(distinct email)
	from Landing.mag.dotmailer_suppressed_contact_aud

	select store_id, count(*) 
	from Landing.mag.dotmailer_suppressed_contact_aud
	group by store_id
	order by store_id

	select top 1000 email, count(*), count(distinct store_id)
	from Landing.mag.dotmailer_suppressed_contact_aud
	group by email
	order by count(distinct store_id) desc

	select top 1000 count(*) over (),
		c.idCustomer_sk, c.customer_id_bk, dms.email, dms.store_id, dms.timestamp
	from 
			Landing.mag.dotmailer_suppressed_contact_aud dms
		left join
			Warehouse.gen.dim_customer c on dms.email = c.customer_email
	where c.customer_email is null	

	select top 1000 count(*) over (), email, store_id, timestamp
	from
		(select email, store_id, timestamp, dense_rank() over (partition by email order by timestamp desc, store_id) ord_rep
		from Landing.mag.dotmailer_suppressed_contact_aud
		-- where email = 'abenapa@gmail.com'
		) dms
	where ord_rep = 1

--------------------------------------------------------------

	select top 1000 client_id, email, store_id, 
		firstname, country, email_origine, source, website_unsubscribe, w_unsub_date, 
		registration_date, last_order_date, segment_lifecycle, segment_geog, segment_2, segment_3, 
		timestamp
	from Landing.mag.dotmailer_synced_contact_aud
	where client_id in (1101000046, 1677777, 1422866) or email = 'anas.nasarullah@getlenses.co.uk'
	order by client_id, timestamp

	select count(*), count(distinct email), count(distinct client_id)
	from Landing.mag.dotmailer_synced_contact_aud

	select top 1000 client_id, count(*), count(distinct email)
	from Landing.mag.dotmailer_synced_contact_aud
	group by client_id
	order by count(*) desc
	-- order by count(distinct email) desc

	select top 1000 email, count(*)
	from Landing.mag.dotmailer_synced_contact_aud
	group by email
	order by count(*) desc


	select top 1000 count(*) over (),
		c.idCustomer_sk, c.customer_id_bk, dms.email, dms.store_id, dms.timestamp
	from 
			Landing.mag.dotmailer_synced_contact_aud dms
		inner join
			Warehouse.gen.dim_customer c on dms.email = c.customer_email
	where c.customer_email is null	


--------------------------------------------------------------
-- Questions

	-- Why Suppresed Customers in many stores (8, 9, 21)

	-- Why Suppresed Customers that are not in WH Customer table
