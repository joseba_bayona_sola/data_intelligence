
-- Steps: Update reorder_date on order_headers / invoice_headers / shipment_headers from max_rorder_order based on profile_id

UPDATE oh 
SET oh.reorder_date = NULL
FROM 
		order_headers oh 
	INNER JOIN 
		max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
WHERE oh.order_id != mo.max_order_iod


--invoice
UPDATE oh 
SET oh.reorder_date = NULL
FROM 
		invoice_headers oh 
	INNER JOIN 
		max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
WHERE oh.order_id != mo.max_order_iod


--shipment
UPDATE oh 
SET oh.reorder_date = NULL
FROM 
		shipment_headers oh 
	INNER JOIN 
		max_rorder_order mo ON mo.reorder_profile_id = oh.reorder_profile_id 
WHERE oh.order_id != mo.max_order_iod

---------------------------------------------------------------------------

select top 1000 order_id, order_no, document_type, document_date, store_name, customer_id, reorder_profile_id, reorder_date
from DW_GetLenses.dbo.order_headers
where document_type = 'ORDER'
	and reorder_date is not null

select top 1000 count(*)
from DW_GetLenses.dbo.order_headers
