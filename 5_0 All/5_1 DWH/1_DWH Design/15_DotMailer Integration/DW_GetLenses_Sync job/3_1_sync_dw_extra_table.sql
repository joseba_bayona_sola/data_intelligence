
-- Steps: create max_rorder_order table with maximun order_id per profile_id (from order headers)


	IF OBJECT_ID ('dbo.max_rorder_order') IS NOT NULL BEGIN
		drop table max_rorder_order
	END

	SELECT reorder_profile_id, max(order_id) max_order_iod
	INTO max_rorder_order
	FROM order_headers
	WHERE document_type = 'order' and reorder_profile_id IS NOT NULL AND reorder_profile_id != 0 and reorder_date is not null
	GROUP BY reorder_profile_id

	ALTER TABLE max_rorder_order ALTER COLUMN reorder_profile_id INT NOT NULL
	ALTER TABLE max_rorder_order ALTER COLUMN max_order_iod INT NOT NULL
	alter table max_rorder_order add primary key (reorder_profile_id,max_order_iod) 

---------------------------------------------------------	

select top 1000 *
from DW_GetLenses.dbo.max_rorder_order
order by reorder_profile_id

select count(*)
from DW_GetLenses.dbo.max_rorder_order
