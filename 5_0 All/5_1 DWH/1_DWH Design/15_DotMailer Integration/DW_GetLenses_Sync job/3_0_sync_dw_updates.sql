

-- Steps: 

	insert into dw_sync_status VALUES ('sync_dw_updates started', getdate())

		EXEC dw_alter_struct
		EXEC dw_add_indexes


	EXEC sync_dw_extra_table -- 1h
	insert into dw_sync_status VALUES ('sync_dw_extra_table finished', getdate())

	EXEC sync_dw_extra_updates -- 6min
	insert into dw_sync_status VALUES ('sync_dw_extra_updates finished', getdate())
	 
	EXEC EMV_rbl -- 4h
	insert into dw_sync_status VALUES ('EMV_rbl finished', getdate())

	EXEC update_emv_cols -- 1h
	insert into dw_sync_status VALUES ('update_emv_cols finished', getdate())

	EXEC create_order_line_sums -- 25 min
	insert into dw_sync_status VALUES ('create_order_line_sums finished', getdate())

	insert into dw_sync_status VALUES ('sync_dw_updates finished', getdate())