
-- Steps: 
	
	-- Create a previous EMV table backup (EMV_sync_list_prev)	

	-- Insert in products table 4 GEN products

	-- create ##EMV_last_order_item + 2 indexes
		-- Summary of Customer - Product relation with Last Order info 
		-- (Customer ID + Store ID (2) + Product ID, Name, SKU, URL, Category + Order ID, Date, Source, Item ID

	-- create ##EMV_last_shipment + 1 index
		-- Customer - Order data giving the date of the last Shipment 
		-- (Customer ID + Order ID, Source, Last Shipped Date

	-- create ##EMV_top_order_item 
		-- Summary of Customer - Product - Order relation 

	-- create ##EMV_websites + 1 index
		-- List of Websites

	-- create ##EMV_last_order + 1 index
		-- Summary of Customer - Order with rowNumber for Ordering
		
	-- create ##EMV_top_order0 - ##EMV_top_order0rev
		-- Reminders data per Customer - Order - Future Reminder Date
		-- Reminders data per Customer - Order - Past Reminder Date

	-- create ##EMV_top_order - ##EMV_top_orderrev
		-- Reminders data per Customer - Order - Future Reminder Date - Ordered
		-- Reminders data per Customer - Order - Past Reminder Date - Ordered

	-- Update EMV_last_order_item
		-- Update Name - SKU - product_id, url depending on Product - Store - Core Config Data 
		
	-- ##EMV_top_order_presc



---------------------------------------------

	-- Insert EMV_sync_list

	-- Update EMV_sync_list // FIRST, LAST, NUM_OF_ORERS, SEGMENT_LIFECYCLE (from ##EMV_last_order)

	-- Update EMV_sync_list  // STORE_ID (from all_order_headers)

	-- Update EMV_sync_list  // LAST_LOGGED_IN_DATE (from log_customer)

	-- Update EMV_sync_list  // ORDER REMINDER (from ##EMV_top_order, ##EMV_top_orderrev) (2 times)

	-- Update EMV_sync_list  // STORE_ID (To current VD stores)

	-- Update EMV_sync_list  // ORDER_REMINDER_PRODUCT (from ##EMV_top_order_item)

	-- Update EMV_sync_list  // PRESCRIPTION (from ##EMV_top_order_presc)

	-- Update EMV_sync_list  // DATEOFBIRTH (from dw_wearer)

	-- Update EMV_sync_list  // COUNTRY (from customer_addresses)

	-- Update EMV_sync_list  // DATEUNJOIN

	-- Create #local_config + Update EMV_sync_list   // GENDER, LANGUAGE, CURRENCY, EMAIL_ORIGINE, EMAIL_ORIGIN

	-- Update EMV_sync_list  // STORE_URL


---------------------------------------------

	-- Update EMV_sync_list  // DAILIES - MONTHLIES - COLOURS - SOLUTIONS - SUNGLASSES - GLASSES - OTHER (from EMV_last_order_item based on ROWNUMBER)
		-- (STORE_ID, SKU, NAME, URL, LAST_ORDER_DATE)

	-- Update EMV_sync_list  // LAST - LENS (OTHER - SOLUTIONS - COLOURS - MONTHLIES - DAILIES)
		-- (STORE_ID, SKU, NAME, URL, LAST_ORDER_DATE)

	-- Update EMV_sync_list  (CATEGORY)

---------------------------------------------

	-- Update EMV_sync_list  // EMVADMIN5 (from ##EMV_last_shipment)

	-- Update EMV_sync_list  // EMVADMIN1 (LAST_ORDER_ID)

	-- Update EMV_sync_list  // SEGMENT_3 (from autologin_last_date)