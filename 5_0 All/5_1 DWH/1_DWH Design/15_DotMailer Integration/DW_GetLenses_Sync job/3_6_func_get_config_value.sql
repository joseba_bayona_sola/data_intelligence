
CREATE function  [dbo].[func_get_config_value] (@path nvarchar(1000) = NULL , @store_id int = 0 )
RETURNS  varchar(MAX) 
AS
BEGIN
	declare @val varchar(MAX)

	select	@val = value 
	from	core_config_data 
	where	scope = 'stores' AND scope_id = @store_id and path = @path

	IF @val IS NOT  NULL BEGIN RETURN @val END

	select	@val = value 
	from	core_config_data 
	where	scope = 'websites' AND scope_id = @store_id and path = @path

	IF @val IS NOT  NULL BEGIN RETURN @val END

	select	@val = value 
	from	core_config_data 
	where	scope = 'websites' AND scope_id = @store_id and path = @path

	IF @val IS NOT  NULL BEGIN RETURN @val END

	select	@val = value 
	from	core_config_data 
	where	scope = 'default' AND scope_id = @store_id and path = @path

	IF @val IS NOT  NULL BEGIN RETURN @val END

	select	@val = value 
	from	core_config_data 
	where	scope = 'default' AND scope_id = 0 and path = @path

	IF @val IS NOT  NULL BEGIN RETURN @val 

END
 


