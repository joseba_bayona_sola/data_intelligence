
select title, gender
from DW_GetLenses.dbo.EMV_gender_prefix

select *
from DW_GetLenses.dbo.category_local_list

-- core_config_date
select * -- config_id, scope, scope_id, path, value
from DW_GetLenses.dbo.dw_core_config_data
-- where path = 'surveysweb/surveys/active'
where path = 'web/unsecure/base_url'
-- where path = 'general/locale/code'
-- where path = 'currency/options/base'
order by scope_id

select *
from DW_GetLenses.dbo.stores

-- categories
select category_id, product_type, category, modality, type, feature, promotional_product
from DW_GetLenses.dbo.categories

-- products
select p.product_id, p.store_name, p.product_type, p.category_id, c.product_type, c.category, p.name, p.sku, p.url_path, p.url_key,
	p.average_cost, p.promotional_product
from 
		DW_GetLenses.dbo.products p
	inner join
		DW_GetLenses.dbo.categories c on p.category_id = c.category_id
-- where product_id < 0
where product_id = 3193
-- where p.store_name = 'Default'
order by p.product_id, p.store_name


------------------------------------------------------------

-- customers

	select *
	from DW_GetLenses.dbo.customers
	where customer_id in (726063, 2326232, 1635930, 452363)

	select customer_id, website_id, store_name, firstname, lastname, email, prefix, created_at, referafriend_code, unsubscribe_all, unsubscribe_all_date, old_access_cust_no
	from DW_GetLenses.dbo.customers
	where customer_id in (726063, 2326232, 1635930, 452363)


-- EMV_last_order_item: Summary of Customer - Product relation with Last Order info
	-- RowNumber Logic: Per Customer, Report Category order by order date desc, num of lines)
	-- Product Data Update: Name - SKU - URL
	-- filter on canceled - closed
select top 1000 customer_store_id, order_store_id, 
	last_order_id, last_order_date, last_order_item_id, last_order_source,
	customer_id, 
	report_group_category, report_category, product_id, name, sku, url, google_shopping_gtin,
	rowNumber,
	order_date
from DW_GetLenses_jbs.dbo.EMV_last_order_item
-- where product_id = 1083
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, report_group_category, rowNumber, last_order_date

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item

	select top 1000 customer_id, product_id, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item
	group by customer_id, product_id
	order by count(*) desc, customer_id, product_id

	select top 1000 customer_id, product_id, rowNumber, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item
	group by customer_id, product_id, rowNumber
	order by count(*) desc, customer_id, product_id, rowNumber

	select top 1000 customer_id, rowNumber, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item
	group by customer_id, rowNumber
	order by count(*) desc, customer_id, rowNumber

	select report_group_category, report_category, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item
	group by report_group_category, report_category
	order by report_group_category, report_category

	select product_id, name, sku, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order_item
	group by product_id, name, sku
	order by product_id, name, sku


-- EMV_last_shipment: Customer - Order data giving the date of the last Shipment

select top 1000 customer_id, order_id, source, last_shipped
from DW_GetLenses_jbs.dbo.EMV_last_shipment
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id

	select count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_shipment

-- EMV_top_order_item: Summary of Customer - Product - Order relation 
	-- RowNumber Logic: Per Customer, Order order by product_id desc)
	-- Product Data Update: Name - SKU - URL

select top 1000 order_store_id, 
	order_id, created_at, order_source, 
	customer_id, 
	product_id, report_group_category, name, sku, 
	rowNumber
from DW_GetLenses_jbs.dbo.EMV_top_order_item
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id, product_id

	select top 1000 count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_top_order_item

	select top 1000 customer_id, product_id, order_id, count(*)
	from DW_GetLenses_jbs.dbo.EMV_top_order_item
	group by customer_id, product_id, order_id
	order by count(*) desc, customer_id, order_id, product_id

	select top 1000 customer_id, product_id, order_id, rowNumber, count(*)
	from DW_GetLenses_jbs.dbo.EMV_top_order_item
	group by customer_id, product_id, order_id, rowNumber
	order by count(*) desc, customer_id, product_id, order_id, rowNumber

-- EMV_websites

select store_id, store_name, website_group, store_type, visible, surveysweb_on
from DW_GetLenses_jbs.dbo.EMV_websites

-- EMV_last_order: Summary of Customer - Order with rowNumber for Ordering
	-- Logic for rowNumber, rowNumberRev
	-- Logic for rowNumberSurv, rowNumberSurvRev
	-- filter on canceled - closed
select top 1000 store_id, surveysweb_on,
	order_id, created_at, status, 
	customer_id, 
	rowNumber, rowNumberRev, RowNumberSurv, RowNumberSurvRev
from DW_GetLenses_jbs.dbo.EMV_last_order
where customer_id = 726063
order by customer_id, created_at

	select count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order

	select top 1000 customer_id, order_id, count(*)
	from DW_GetLenses_jbs.dbo.EMV_last_order
	group by customer_id, order_id
	order by count(*) desc, customer_id, order_id

-- EMV_top_order0: Reminders data per Customer - Order - Future Reminder Date

select top 1000 order_store_id, 
	order_id, created_at, order_source,
	customer_id, 
	reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period
from DW_GetLenses_jbs.dbo.EMV_top_order0
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_top_order0

-- EMV_top_order0rev: Reminders data per Customer - Order - Past Reminder Date

select top 1000 order_store_id, 
	order_id, created_at, order_source,
	customer_id, 
	reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period
from DW_GetLenses_jbs.dbo.EMV_top_order0rev
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_top_order0rev



-- EMV_top_order: Reminders data per Customer - Order - Future Reminder Date - Ordered

select top 1000 order_store_id, 
	order_id, created_at, order_source,
	customer_id, 
	reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period, 
	rowNumber
from DW_GetLenses_jbs.dbo.EMV_top_order
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_top_order

-- EMV_top_order0: Reminders data per Customer - Order - Past Reminder Date - Ordered

select top 1000 order_store_id, 
	order_id, created_at, order_source,
	customer_id, 
	reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period, 
	rowNumber
from DW_GetLenses_jbs.dbo.EMV_top_orderrev
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.EMV_top_orderrev


-- EMV_top_order_presc

select top 1000 order_store_id,
	customer_id, 
	order_id, order_no, order_source, 
	name, DOB, date_last_prescription, optician_name, id, 
	reminder_presc, reminder_type, 
	rowNumber
from DW_GetLenses_jbs.dbo.EMV_top_order_presc
where customer_id in (726063, 2326232, 1635930, 452363)
order by customer_id, order_id

	select count(*)
	from DW_GetLenses_jbs.dbo.EMV_top_order_presc
