
select *
from core_config_data
where path in ('dotmailer/access/active', 'dotmailer/access/user', 'dotmailer/access/password')

-- ----------------------------------------------


-- dotmailer_client
select auth_type, access_token, refresh_token, auth_object, expiry_date
from magento01.dotmailer_client
limit 1000

-- dotmailer_result
select id, import_id, 
  created_at, updated_at, status, store_id, 
  filename, info
from magento01.dotmailer_result
order by created_at desc

  select count(*)
  from magento01.dotmailer_result

  select status, count(*)
  from magento01.dotmailer_result
  group by status
  order by status

  select store_id, count(*)
  from magento01.dotmailer_result
  group by store_id
  order by store_id
  
  -- dotmailer_suppresed_contact
select email, status, store_id, dateRemoved, timestamp
from magento01.dotmailer_suppressed_contact
-- where status is not null
-- where dateRemoved is not null
order by timestamp desc
limit 1000

  select count(*)
  from magento01.dotmailer_suppressed_contact

  select store_id, count(*)
  from magento01.dotmailer_suppressed_contact
  group by store_id
  order by store_id
 
-- dotmailer_synced_data 
select client_id, store_id, email, firstname, country,
  source, email_origine, 
  registration_date, last_order_date, 
  segment_lifecycle, segment_geog, segment_2, segment_3, 
  website_unsubscribe, w_unsub_date, 
  timestamp
from magento01.dotmailer_synced_contact
order by client_id, timestamp
limit 1000

  select count(*), count(distinct client_id)
  from magento01.dotmailer_synced_contact
