
use DW_GetLenses
go


drop table DW_GetLenses_jbs.dbo.EMV_trace
go

create table DW_GetLenses_jbs.dbo.EMV_trace(
	message			varchar(250),
	ins_ts			datetime)

select *
from DW_GetLenses_jbs.dbo.EMV_trace

drop procedure dbo.run_EMV
go

create procedure dbo.run_EMV
as
begin
	set nocount on

		DELETE FROM DW_GetLenses.dbo.products WHERE product_id < 0

		INSERT INTO DW_GetLenses.dbo.products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -1 as product_id, category_id, 'Dailies' as name, 'GEN-DAILIES' as sku , 'lens', 'Default', 0
			from DW_GetLenses.dbo.categories 
			where type = 'Spherical' AND feature = 'Standard' AND category = 'Dailies' 
				AND product_type = 'Contact Lenses - Dailies')

		INSERT INTO DW_GetLenses.dbo.products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -2 as product_id, category_id, 'Monthlies & Other Lenses' as name, 'GEN-MONTHLY' as sku, 'lens', 'Default', 0
			from DW_GetLenses.dbo.categories 
			where type = 'Spherical' AND feature = 'Standard' AND category = 'Monthlies' 
				AND product_type = 'Contact Lenses - Monthlies & Other')

		INSERT INTO DW_GetLenses.dbo.products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -3 as product_id, category_id, 'Solutions & Eye Care' as name, 'GEN-SOLUTION' as sku, 'solution', 'Default', 0
			from DW_GetLenses.dbo.categories 
			where category = 'Multi-purpose Solutions' 
				AND product_type = 'Solutions & Eye Care')

		INSERT INTO DW_GetLenses.dbo.products (product_id, category_id, name, sku, product_type, store_name, average_cost)
			(select -4 as product_id, category_id, 'Other' as name, 'GEN-OTHER' as sku, 'othe', 'Default', 0
			from DW_GetLenses.dbo.categories 
			where category = 'Other' 
				AND product_type = 'Other')


	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('products', getutcdate())


		select MIN(sf.store_id) customer_store_id, 
			MIN(t0.store_id) order_store_id, t0.customer_id, t1.product_id, 
			max(pr1.google_shopping_gtin) as google_shopping_gtin,
			MAX(pr1.name) name, MAX(pr1.sku) sku, MAX(COALESCE(pr1.url_path,pr1.url_key)) url, 
			
			MAX(t0.created_at) last_order_date, MAX(t0.order_id) last_order_id, MAX(t0.source) last_order_source, 
			MAX(t1.line_id) last_order_item_id,
			MAX(cgr.category) report_category,
			ISNULL(CASE WHEN  cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END ,'OTHER') report_group_category,
			ROW_NUMBER() OVER (
				PARTITION BY t0.customer_id, ISNULL(CASE WHEN cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER')
				ORDER BY t0.customer_id DESC, ISNULL(CASE WHEN  cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER'), max(t0.created_at) DESC, COUNT(*) DESC) 
				AS [RowNumber],
			CAST('2000-01-01' AS date) AS order_date
		into DW_GetLenses_jbs.dbo.EMV_last_order_item
		FROM 
			DW_GetLenses.[dbo].[customers] c0, 
			DW_GetLenses.[dbo].[all_order_headers] t0, 
			DW_GetLenses.[dbo].[all_order_lines] t1, 
			DW_GetLenses.[dbo].[products] pr1, 
			DW_GetLenses.dbo.categories cgr , 
			DW_GetLenses.dbo.dw_stores_full sf
		WHERE 
			c0.customer_id = t0.customer_id AND 
			t0.order_id = t1.order_id AND t0.source = t1.source AND 
			t1.product_id=pr1.product_id AND 
			t0.status not in ('canceled', 'closed') AND 
			pr1.category_id is not null AND pr1.category_id=cgr.category_id  ANd pr1.store_name = 'Default' AND 
			sf.store_name = c0.store_name AND 
			((COALESCE(pr1.promotional_product,0) != 1  AND t1.price != 0) or pr1.sku like '%GEN%') AND 
			pr1.sku != 'BROCHURE' AND 
			t1.product_id NOT IN (SELECT product_id from all_products_not_visible_glasses)
		GROUP BY t0.customer_id, t1.product_id,  cgr.category, cgr.product_type;

		CREATE NONCLUSTERED INDEX [idx_1] ON DW_GetLenses_jbs.dbo.EMV_last_order_item([customer_id] ASC, [report_category] ASC, [RowNumber] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [idx_2] ON DW_GetLenses_jbs.dbo.EMV_last_order_item ([order_store_id], [product_id])

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_last_order_item', getutcdate())


		select MAX(created_at) last_shipped, customer_id, source, order_id 
		into DW_GetLenses_jbs.dbo.EMV_last_shipment
		from DW_GetLenses.[dbo].[all_shipment_headers] 
		group by customer_id, order_id, source;

		CREATE CLUSTERED INDEX [idx_1] ON DW_GetLenses_jbs.dbo.EMV_last_shipment([customer_id] ASC, [order_id] ASC, [last_shipped] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_last_shipment', getutcdate())


		select	
				t0.customer_id, t0.order_id, t0.source order_source, 
				MIN(t0.store_id) order_store_id, 
				t1.product_id, 
				MAX(pr1.name) name, MAX(pr1.sku) sku, 
				MAX(t0.created_at) created_at, 
				MAX(ISNULL(CASE WHEN cgr.category = '' THEN cgr.product_type ELSE cgr.category END ,'OTHER')) report_group_category,
				ROW_NUMBER() OVER (
					PARTITION BY t0.order_id, t0.source
					ORDER BY t0.customer_id, t1.product_id DESC, COUNT(*) DESC) AS [RowNumber]
		into DW_GetLenses_jbs.dbo.EMV_top_order_item
		FROM 
				DW_GetLenses.[dbo].[all_order_headers] t0, 
				DW_GetLenses.[dbo].[all_order_lines] t1, 
				DW_GetLenses.[dbo].[products] pr1
			LEFT JOIN 
				DW_GetLenses.dbo.categories cgr ON pr1.category_id = cgr.category_id
		WHERE 
			t0.order_id = t1.order_id AND t0.source = t1.source AND 
			t1.product_id = pr1.product_id AND 
			(pr1.promotional_product != 1  AND t1.price != 0 or pr1.sku like '%GEN%') AND 
			pr1.sku != 'BROCHURE' ANd pr1.store_name = 'Default'
		GROUP BY t0.customer_id, t0.order_id, t0.source, t1.product_id;

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order_item', getutcdate())


		select t0.*, 
			[dbo].[func_get_config_value] ('surveysweb/surveys/active', t0.store_id) surveysweb_on -- 1 - surveysweb module is on
		into DW_GetLenses_jbs.dbo.EMV_websites
		FROM DW_GetLenses.[dbo].[dw_stores_full] t0

		CREATE NONCLUSTERED INDEX [idx_web2] ON DW_GetLenses_jbs.dbo.EMV_websites([store_id] ASC)
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_websites', getutcdate())


		select	
			customer_id, order_id, t1.store_id, t1.surveysweb_on, source, 
			status, created_at, 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id ORDER BY t0.customer_id, t0.created_at) AS [RowNumber], 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id ORDER BY t0.customer_id, t0.created_at DESC) AS [RowNumberRev],
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id, t1.surveysweb_on ORDER BY t0.customer_id, t0.created_at) AS [RowNumberSurv], 
			ROW_NUMBER() OVER (PARTITION BY t0.customer_id, t1.surveysweb_on ORDER BY t0.customer_id, t0.created_at DESC) AS [RowNumberSurvRev]
        
		into DW_GetLenses_jbs.dbo.EMV_last_order
		FROM 
			[dbo].[all_order_headers] t0, 
			DW_GetLenses_jbs.dbo.EMV_websites t1
		where 
			t0.store_id=t1.store_id AND 
			t0.status not in ('canceled', 'closed');
						 
		CREATE NONCLUSTERED INDEX [idx_6] ON DW_GetLenses_jbs.dbo.EMV_last_order([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_last_order', getutcdate())


		select	
				customer_id, order_id, 'current_db' order_source, sf.store_id order_store_id,  
				document_date created_at, 
				cast(reminder_date as date) reminder_date, reminder_type, reminder_mobile, reminder_presc, reminder_period
        
		into DW_GetLenses_jbs.dbo.EMV_top_order0
		FROM 
			[dbo].[order_headers] t0, 
			dw_stores_full sf
					
		where t0.reminder_date IS NOT NULL -- AND ISDATE(t0.reminder_date)=1
			AND reminder_type IS NOT NULL -- AND reminder_type!='none'
			AND t0.status not in ('canceled', 'closed') 
			AND document_type = 'ORDER' 
			AND sf.store_name = t0.store_name;

		select * 
		into DW_GetLenses_jbs.dbo.EMV_top_order0rev 
		from DW_GetLenses_jbs.dbo.EMV_top_order0;

		-- remove all expired entries
		delete from DW_GetLenses_jbs.dbo.EMV_top_order0 where reminder_date < GETDATE();
		-- remove all not expired entries
		delete from DW_GetLenses_jbs.dbo.EMV_top_order0rev where reminder_date >= GETDATE();

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order0', getutcdate())

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order0rev', getutcdate())


		select *,
			ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY reminder_date, order_id DESC) AS [RowNumber]
        into DW_GetLenses_jbs.dbo.EMV_top_order
		FROM DW_GetLenses_jbs.dbo.EMV_top_order0;

		-- as obove but latest is 1, previous 2
		select *,
			ROW_NUMBER() OVER (PARTITION BY customer_id ORDER BY reminder_date DESC, order_id DESC) AS [RowNumber]
        into DW_GetLenses_jbs.dbo.EMV_top_orderrev
		FROM DW_GetLenses_jbs.dbo.EMV_top_order0rev;

		CREATE NONCLUSTERED INDEX [idx_2p] ON DW_GetLenses_jbs.dbo.EMV_top_order ([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		CREATE NONCLUSTERED INDEX [idx_2r] ON DW_GetLenses_jbs.dbo.EMV_top_orderrev ([customer_id] ASC) 
			WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, 
			ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order', getutcdate())

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_orderrev', getutcdate())

	UPDATE t1
	SET
		t1.name=p1.name, t1.sku =p1.sku, t1.product_id =p1.product_id,
		t1.google_shopping_gtin=p1.google_shopping_gtin,
		-- t1.url ='http://www.'+t2.name+'/'+p1.url_path
		t1.url =''+ [dbo].[func_get_config_value] ('web/unsecure/base_url', t1.order_store_id) +''+COALESCE(p1.url_path,p1.url_key)
	FROM
		DW_GetLenses_jbs.dbo.EMV_last_order_item t1, 
		dw_stores_full t2,
		products p1 
	WHERE
		t2.store_id = t1.order_store_id AND 
		p1.store_name = t2.store_name AND 
		t1.product_id = p1.product_id;

	UPDATE t1
	SET
		t1.name=p1.name,
		t1.sku =p1.sku,
		t1.product_id =p1.product_id
	FROM
		DW_GetLenses_jbs.dbo.EMV_top_order_item t1, 
		dw_stores_full sf,
		products p1 
	WHERE
	sf.store_id = t1.order_store_id AND 
	sf.store_name = p1.store_name AND
	t1.product_id = p1.product_id;

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_last_order_item U', getutcdate())

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order_item U', getutcdate())


		SELECT
			MAX(wearer.customer_id) customer_id, max(order_headers.order_id) order_id, max(order_headers.order_no) order_no, 
			'current_db' order_source, max(sf.store_id) order_store_id, 
			
			max(wearer.name) name, max(wearer.DOB) DOB, 
			max(wearer_prescription.date_last_prescription) date_last_prescription, max(wearer_prescription.optician_name) optician_name,
			wearer_prescription.id,
			MAX(reminder_presc) reminder_presc, MAX(reminder_type) reminder_type,
			ROW_NUMBER() OVER (PARTITION BY wearer_order_item.order_id ORDER BY wearer_prescription.date_last_prescription DESC) AS [RowNumber]
		INTO DW_GetLenses_jbs.dbo.EMV_top_order_presc
		FROM         
				dw_wearer wearer 
			INNER JOIN
				dw_wearer_prescription wearer_prescription ON wearer.id = wearer_prescription.wearer_id 
			INNER JOIN
				dw_wearer_order_item wearer_order_item ON wearer_prescription.id = wearer_order_item.wearer_prescription_id 
			INNER JOIN
				order_headers ON CAST(wearer_order_item.order_id as varchar) = order_headers.order_no
			LEFT JOIN 
				dw_stores_full sf ON sf.store_name = order_headers.store_name
		 WHERE
		  reminder_presc='on' and reminder_date > GETDATE() and
		  order_headers.status not in ('canceled', 'closed') AND order_headers.document_type = 'ORDER'
		group by wearer_prescription.id, wearer_order_item.order_id, wearer_prescription.date_last_prescription
		order by wearer_order_item.order_id DESC;

	insert into DW_GetLenses_jbs.dbo.EMV_trace(message, ins_ts) values ('EMV_top_order_presc', getutcdate())

end;

go


-- exec dbo.run_EMV
