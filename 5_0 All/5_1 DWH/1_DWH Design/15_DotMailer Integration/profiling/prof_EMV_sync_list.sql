
select top 1000 *
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc


select top 1000 client_id, 
	website_id, store_id, store_url,
	email, 
	title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
	currency, country, 
	datejoin, dateunjoin, 
	email_origine, email_origin, seed, clienturn, source, code, 
	product_id, 
	order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url, 
	-- signed_up_to_autoreorder, date_of_autoreorder,

	prescription_expiry_date, prescription_wearer, prescription_optician, 
	bounce, card_expiry_date, 
	-- email_preferences, 
	unsubscribe, website_unsubscribe, w_unsub_date, 
	-- trustpilot_service_review, trustpilot_service_review_rating, magento_product_review_rating,
	registration_date, first_order_date, 
	last_logged_in_date, last_order_date, last_order_id, last_order_source, num_of_orders, -- last_order_del_date
	-- aov, lifetime_spend, discount_code_used, num_of_referrals, 

	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
	segment, segment_2, segment_3, segment_4, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
	check_sum, 

	lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
	lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2,

	dailies_store_id1, dailies_store_id2,
	dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date, 
	google_shopping_gtin_daily_sku1, google_shopping_gtin_daily_sku2,

	monthlies_store_id1, monthlies_store_id2,
	monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2, monthlies_last_order_date, 
	google_shopping_gtin_monthly_sku1, google_shopping_gtin_monthly_sku2,

	colours_store_id1, colours_store_id2,
	colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date, 
	google_shopping_gtin_colours_sku1, google_shopping_gtin_colours_sku2,

	solutions_store_id1, solutions_store_id2,
	solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2, solutions_last_order_date, 
	google_shopping_gtin_solutions_sku1, google_shopping_gtin_solutions_sku2, 

	other_store_id1, other_store_id2,
	other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2, other_last_order_date, 
	google_shopping_gtin_other_sku1, google_shopping_gtin_other_sku2, 

	sunglasses_store_id1, sunglasses_store_id2,
	sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2, sunglasses_last_order_date, 

	glasses_store_id1, glasses_store_id2,
	glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2, glasses_last_order_date,

	last_product_type,
	last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
	last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2, 
	last_order_lens_qty
from DW_GetLenses.dbo.EMV_sync_list
where client_id in (726063, 2326232, 1635930, 452363)
-- where segment_lifecycle = 'RNB'
-- where segment_geog = 'LENSWAYNL'
order by client_id 


select top 1000 client_ID, 
	emvcellphone, datejoin, 
	seed, code, product_id, 
	bounce, card_expiry_date, unsubscribe, 
	segment_usage, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
	segment, segment_4, 
	emvadmin2, emvadmin3, emvadmin4
from DW_GetLenses.dbo.EMV_sync_list
where emvadmin4 is not null

-------------------------------------------------------------


select top 1000 *
from DW_GetLenses.dbo.EMV_sync_list
where client_id in (1696157, 1493667, 1340836, 1293554, 1283530, 1022946)

select top 1000 client_id, 
	website_id, store_id, store_url, 
	title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
	currency, country, 
	datejoin, dateunjoin, 
	email_origine, email_origin, seed, clienturn, source, code, 
	product_id, 
	order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc

	select count(*) -- 1313965
	from DW_GetLenses.dbo.EMV_sync_list

	select website_id, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by website_id
	order by website_id

	select store_id, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by store_id
	order by store_id

	select website_id, store_id, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by website_id, store_id
	order by website_id, store_id

	select count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	where dateofbirth is not null

	select gender, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by gender
	order by gender

	select source, code, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by source, code
	order by source, code

	select product_id, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by product_id
	order by product_id

	select dateunjoin, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by dateunjoin
	order by dateunjoin

	select store_id, email_origin, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by store_id, email_origin
	order by store_id, email_origin

	select order_reminder_source, order_reminder_pref, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by order_reminder_source, order_reminder_pref
	order by order_reminder_source, order_reminder_pref


select TOP 1000 client_id, 
	prescription_expiry_date, prescription_wearer, prescription_optician, 
	bounce, 
	card_expiry_date, 
	unsubscribe, website_unsubscribe, w_unsub_date, 
	registration_date, first_order_date, 
	last_logged_in_date, last_order_date, last_order_id, last_order_source, num_of_orders
from DW_GetLenses.dbo.EMV_sync_list
-- where prescription_expiry_date is not null or prescription_wearer is not null or prescription_optician is not null
order by client_id desc

	select unsubscribe, website_unsubscribe, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by unsubscribe, website_unsubscribe
	order by unsubscribe, website_unsubscribe

	select last_order_source, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by last_order_source
	order by last_order_source

	select num_of_orders, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by num_of_orders
	order by num_of_orders

select top 1000 client_id, 
	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
	segment, segment_2, segment_3, segment_4, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
	check_sum 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc

	select segment_lifecycle, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by segment_lifecycle
	order by segment_lifecycle

	select segment_geog, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by segment_geog
	order by segment_geog
	

----------------------------------------------------------------------------------------------------------------

select top 1000 client_id, store_id,
	lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
	lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2
from DW_GetLenses.dbo.EMV_sync_list
where lens_sku1 = 1083 and lens_url1 = '1-day-acuvue-moist'
-- where lens_sku1 = 1083 and lens_url1 = 'https://www.visiondirect.co.uk/1-day-acuvue-moist'
order by client_id desc

	select lens_sku1, lens_name1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by lens_sku1, lens_name1
	order by lens_sku1, lens_name1

	select lens_sku1, lens_name1, lens_url1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by lens_sku1, lens_name1, lens_url1
	order by lens_sku1, lens_name1, lens_url1

select top 1000 client_id, 
	dailies_store_id1, dailies_store_id2,
	dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc

select top 1000 client_id, 
	monthlies_store_id1, monthlies_store_id2,
	monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2,
	monthlies_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc

	select monthlies_sku1, monthlies_name1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by monthlies_sku1, monthlies_name1
	order by monthlies_sku1, monthlies_name1
	
	select monthlies_category1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by monthlies_category1
	order by monthlies_category1

select top 1000 client_id, 
	colours_store_id1, colours_store_id2,
	colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc


select top 1000 client_id, 
	solutions_store_id1, solutions_store_id2,
	solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2,
	solutions_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc
	
	select solutions_category1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by solutions_category1
	order by solutions_category1


select top 1000 client_id, 
	other_store_id1, other_store_id2,
	other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2,
	other_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc
	
	select other_category1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by other_category1
	order by other_category1


select top 1000 client_id, 
	sunglasses_store_id1, sunglasses_store_id2,
	sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2,
	sunglasses_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc
	
	select sunglasses_category1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by sunglasses_category1
	order by sunglasses_category1


select top 1000 client_id, 
	glasses_store_id1, glasses_store_id2,
	glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2,
	glasses_last_order_date 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc
	
	select glasses_category1, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by glasses_category1
	order by glasses_category1


select top 1000 client_id, 
	last_product_type,
	last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
	last_order_lens_qty,
	last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2 
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc

	select last_product_type, count(*)
	from DW_GetLenses.dbo.EMV_sync_list
	group by last_product_type
	order by last_product_type


select top 1000 client_id, 
	google_shopping_gtin_daily_sku1, google_shopping_gtin_daily_sku2, 
	google_shopping_gtin_monthly_sku1, google_shopping_gtin_monthly_sku2, 
	google_shopping_gtin_colours_sku1, google_shopping_gtin_colours_sku2, 
	google_shopping_gtin_solutions_sku1, google_shopping_gtin_solutions_sku2, 
	google_shopping_gtin_other_sku1, google_shopping_gtin_other_sku2
from DW_GetLenses.dbo.EMV_sync_list
order by client_id desc


-------------------------------------------------------------------------------------------------

select top 1000 client_id, 
	dailies_sku1, monthlies_sku1, colours_sku1, solutions_sku1, other_sku1, sunglasses_sku1, glasses_sku1, 
	dailies_f + monthlies_f + colours_f + solutions_f + other_f + sunglasses_f + glasses_f num_dist_prod, 
	dailies_last_order_date, monthlies_last_order_date, colours_last_order_date, 
	solutions_last_order_date, other_last_order_date, sunglasses_last_order_date, glasses_last_order_date
from 
	(select client_id, 
		dailies_sku1, monthlies_sku1, colours_sku1, solutions_sku1, other_sku1, sunglasses_sku1, glasses_sku1, 
		case when (dailies_sku1 is null) then 0 else 1 end dailies_f, 
		case when (monthlies_sku1 is null) then 0 else 1 end monthlies_f, 
		case when (colours_sku1 is null) then 0 else 1 end colours_f, 
		case when (solutions_sku1 is null) then 0 else 1 end solutions_f, 
		case when (other_sku1 is null) then 0 else 1 end other_f, 
		case when (sunglasses_sku1 is null) then 0 else 1 end sunglasses_f, 
		case when (glasses_sku1 is null) then 0 else 1 end glasses_f, 
		dailies_last_order_date, monthlies_last_order_date, colours_last_order_date, 
		solutions_last_order_date, other_last_order_date, sunglasses_last_order_date, glasses_last_order_date
	from DW_GetLenses.dbo.EMV_sync_list) t
order by num_dist_prod desc, client_id desc