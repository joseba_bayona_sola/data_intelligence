
use DW_GetLenses
go

		-- EMV_last_order_item: XX min
		select MIN(sf.store_id) customer_store_id, 
			MIN(t0.store_id) order_store_id, t0.customer_id, t1.product_id, 
			max(pr1.google_shopping_gtin) as google_shopping_gtin,
			MAX(pr1.name) name, MAX(pr1.sku) sku, MAX(COALESCE(pr1.url_path,pr1.url_key)) url, 
			
			MAX(t0.created_at) last_order_date, MAX(t0.order_id) last_order_id, MAX(t0.source) last_order_source, 
			MAX(t1.line_id) last_order_item_id,
			MAX(cgr.category) report_category,
			ISNULL(CASE WHEN  cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END ,'OTHER') report_group_category,
			ROW_NUMBER() OVER (
				PARTITION BY t0.customer_id, ISNULL(CASE WHEN cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER')
				ORDER BY t0.customer_id DESC, ISNULL(CASE WHEN  cgr.category = 'Colours' THEN  'Contact Lenses - Colours' ELSE cgr.product_type END, 'OTHER'), max(t0.created_at) DESC, COUNT(*) DESC) 
				AS [RowNumber],
			CAST('2000-01-01' AS date) AS order_date
		into DW_GetLenses_jbs.dbo.EMV_last_order_item
		FROM 
			[dbo].[customers] c0, 
			[dbo].[all_order_headers] t0, 
			[dbo].[all_order_lines] t1, 
			[dbo].[products] pr1, 
			categories cgr , 
			dw_stores_full sf
		WHERE 
			c0.customer_id = t0.customer_id AND 
			t0.order_id = t1.order_id AND t0.source = t1.source AND 
			t1.product_id=pr1.product_id AND 
			t0.status not in ('canceled', 'closed') AND 
			pr1.category_id is not null AND pr1.category_id=cgr.category_id  ANd pr1.store_name = 'Default' AND 
			sf.store_name = c0.store_name AND 
			((COALESCE(pr1.promotional_product,0) != 1  AND t1.price != 0) or pr1.sku like '%GEN%') AND 
			pr1.sku != 'BROCHURE' AND 
			t1.product_id NOT IN (SELECT product_id from all_products_not_visible_glasses)
		GROUP BY t0.customer_id, t1.product_id,  cgr.category, cgr.product_type;
