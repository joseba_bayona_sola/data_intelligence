

select event_type, event_time
from DW_GetLenses.dbo.dw_sync_status
order by event_time desc, event_type

	select event_type, count(*)
	from DW_GetLenses.dbo.dw_sync_status
	group by event_type
	order by event_type


select table_name, table_type, enabled, 
	create_from, 
	update_type, update_id, live, synced
from DW_GetLenses.dbo.dw_rebuild_tables
order by table_name


select table_name, build_from
from DW_GetLenses.dbo.dw_rebuild_build_list

--------------------------------------------------

select *
from DW_GetLenses.dbo.autologin_last_date

select top 1000 *
from DW_GetLenses.dbo.EMV_sync_list_updated_customers

	select top 1000 count(*)
	from DW_GetLenses.dbo.EMV_sync_list_updated_customers

select top 1000 *
from DW_GetLenses.dbo.EMV_include_customers

	select top 1000 count(*)
	from DW_GetLenses.dbo.EMV_include_customers

--------------------------------------------------

select top 1000 email, status, store_id, dateRemoved, timestamp
from DW_GetLenses.dbo.Dotmailer_Supressed_Data
where status is not null or dateRemoved is not null
order by dateRemoved desc

	select count(*)
	from DW_GetLenses.dbo.Dotmailer_Supressed_Data

	select store_id, count(*)
	from DW_GetLenses.dbo.Dotmailer_Supressed_Data
	group by store_id
	order by store_id


select top 1000 client_id, store_id, email, 
	source, email_origine, segment_lifecycle, country, website_unsubscribe, w_unsub_date, firstname, 
	registration_date, last_order_date, 
	segment_geog, segment_2, segment_3, 
	timestamp, check_sum	
from DW_GetLenses.dbo.Dotmailer_Imported_Data
order by client_id desc, store_id

	select count(*)
	from DW_GetLenses.dbo.Dotmailer_Imported_Data

	select store_id, count(*)
	from DW_GetLenses.dbo.Dotmailer_Imported_Data
	group by store_id
	order by store_id

--------------------------------------------------

select top 1000 *
from DW_GetLenses.dbo.EMV_gender_prefix

select top 1000 count(*) over (), *
from DW_GetLenses.dbo.EMV_sync_list_high_avail

select top 1000 count(*) over (), *
from DW_GetLenses.dbo.EMV_sync_list_hist

select top 1000 count(*) over (), *
from DW_GetLenses.dbo.EMV_sync_list_synced
