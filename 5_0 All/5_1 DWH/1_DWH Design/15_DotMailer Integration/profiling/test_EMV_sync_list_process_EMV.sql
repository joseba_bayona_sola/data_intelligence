
use DW_GetLenses
go
		-- Attributes: 
			-- SEGMENT_2 = referafriend_code // SEGMENT_GEOG = old_access_cust_no
		-- Filtering: 
			-- Why Migrated Customers not
			-- Why dw_binned_customers (33 k)
			-- Why either Have an Order or EMV_include_customers (1 k) // What about RNB
		INSERT INTO DW_GetLenses_jbs.[dbo].[EMV_sync_list](CLIENT_ID, WEBSITE_ID, STORE_ID, CLIENTURN, 
			FIRSTNAME, LASTNAME, EMAIL, TITLE, SOURCE, 
			REGISTRATION_DATE, SEGMENT_2, WEBSITE_UNSUBSCRIBE, SEGMENT_GEOG, W_UNSUB_DATE) 
		SELECT 
		   customer_id, c.website_id, sf.store_id, customer_id, 
		   firstname, lastname, email, prefix, 'Magento', 
		   created_at, referafriend_code, 
		   CASE WHEN unsubscribe_all = 'Yes' or unsubscribe_all = '1' THEN 1 ELSE 0 END, 
		   c.old_access_cust_no, c.unsubscribe_all_date     
		from 
			[dbo].[customers] c , 
			dw_stores_full sf	
		WHERE c.store_name is not NULL AND sf.store_name =  c.store_name 
			and c.customer_id not in (select entity_id from dw_binned_customers)
		   AND (old_access_cust_no not in ('LENSONNL' , 'LENSWAYUK' , 'LENSONCOUK', 'LENSONIE', 'LENSWAYNL','YOURLENSESNL') 
			or (c.customer_id IN ( select customer_id from order_headers ) or email in (select email from EMV_include_customers))); 
 

		-- Attributes
			-- FIRST: Using First Order Data
			-- LAST: Using Last Order Data
			-- SEGMENT_LIFECYCLE: Logic
		-- Filter: 
			-- Use of EMV_last_order: Summary of Customer - Order with rowNumber for Ordering
		UPDATE t1
		SET
			t1.FIRST_ORDER_DATE = oFi.created_at,
			t1.LAST_ORDER_DATE = oLa.created_at, t1.LAST_ORDER_ID = oLa.order_id, t1.LAST_ORDER_SOURCE = oLa.source,
			t1.NUM_OF_ORDERS = ISNULL(oLa.RowNumber,0),
			t1.SEGMENT_LIFECYCLE = 
				(CASE
					WHEN (oLa.surveysweb_on = 1) THEN 'SURVEY' -- if last order is survey
					WHEN ((oLa.RowNumber > 0) AND (DATEADD(month, -15, GETDATE()) > oLa.created_at)) THEN 'LAPSED' -- last order older than 15 months
					WHEN ((oLa.RowNumber > 1) AND (DATEADD(month, 15, oLa2.created_at) < oLa.created_at))  THEN 'REACTIVATED' -- time between last order and previous > 15 months, last orderer leas than 15m ago
					ELSE CASE
						WHEN ((oLa.RowNumberSurv = 0) OR (oLa.RowNumberSurv IS NULL)) THEN 'RNB' 
						WHEN (oLaNSurv.RowNumberSurv = 1) THEN 'NEW' 
						WHEN (oLaNSurv.RowNumberSurv > 1) THEN 'REGULAR'
						END
				END)
			FROM
					DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
				LEFT JOIN 
					DW_GetLenses_jbs.[dbo].EMV_last_order oFi ON oFi.customer_id = t1.CLIENT_ID AND oFi.RowNumber = 1 -- first order (including surveys)
				LEFT JOIN 
					DW_GetLenses_jbs.[dbo].EMV_last_order OLa ON oLa.customer_id = t1.CLIENT_ID AND oLa.RowNumberRev = 1 -- last order (including surveys)
				LEFT JOIN 
					DW_GetLenses_jbs.[dbo].EMV_last_order OLa2 ON oLa2.customer_id = t1.CLIENT_ID AND oLa2.RowNumberRev = 2 -- one before last order (including surveys)
				LEFT JOIN 
					DW_GetLenses_jbs.[dbo].EMV_last_order oFiNSurv ON oFiNSurv.customer_id = t1.CLIENT_ID AND oFiNSurv.RowNumberSurv = 1 AND oFinSurv.surveysweb_on = 0 -- first not-Survey order
				LEFT JOIN 
					DW_GetLenses_jbs.[dbo].EMV_last_order oLaNSurv ON oLaNSurv.customer_id = t1.CLIENT_ID AND oLaNSurv.RowNumberSurvRev=1 AND oLaNSurv.surveysweb_on = 0; -- last not-Survey order
						
		-- Attributes: 
			-- STORE_ID: The store from Last Order
		UPDATE t1
		SET
			t1.STORE_ID = ISNULL(t0.store_id, t1.STORE_ID)
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN
				[dbo].[all_order_headers] t0 ON  t0.order_id = t1.LAST_ORDER_ID AND t0.source = t1.LAST_ORDER_SOURCE;

		-- Attributes: 
			-- LAST_LOGGED_IN_DATE: From log_customer
		UPDATE t1
		SET
			t1.LAST_LOGGED_IN_DATE = ISNULL(t2.login_at, t1.LAST_ORDER_DATE)
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				(SELECT customer_id, MAX(login_at) login_at 
				FROM log_customer customer GROUP BY customer_id) t2 ON t2.customer_id = t1.CLIENT_ID;

		-- Attributes: 
			-- REMINDER Attributes
		UPDATE t1
		SET
			t1.STORE_ID = t2.order_store_id,
			t1.ORDER_REMINDER_DATE = t2.reminder_date, t1.ORDER_REMINDER_ID = t2.order_id, t1.ORDER_REMINDER_SOURCE= 'current_db', --t2.order_source, reminders only in one db
			t1.ORDER_REMINDER_NO = t2.reminder_mobile, t1.ORDER_REMINDER_PREF = t2.reminder_type,
			t1.ORDER_REMINDER_REORDER_URL = [dbo].[func_get_config_value] ('web/secure/base_url', t1.STORE_ID)+'sales/order/reorder/order_id/'+ CAST(t2.order_id AS varchar)+'/'
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN
				DW_GetLenses_jbs.dbo.EMV_top_order t2 ON t2.customer_id = t1.CLIENT_ID AND t2.RowNumber=1
		WHERE t2.order_id IS NOT NULL;

		UPDATE t1
		SET
			t1.STORE_ID = t2.order_store_id,
			t1.ORDER_REMINDER_DATE = t2.reminder_date, t1.ORDER_REMINDER_ID = t2.order_id, t1.ORDER_REMINDER_SOURCE= 'current_db', --t2.order_source, reminders only in one db
			t1.ORDER_REMINDER_NO = t2.reminder_mobile, t1.ORDER_REMINDER_PREF  = t2.reminder_type,
			t1.ORDER_REMINDER_REORDER_URL = [dbo].[func_get_config_value] ('web/secure/base_url', t1.STORE_ID)+'sales/order/reorder/order_id/'+ CAST(t2.order_id AS varchar)+'/'
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN
				DW_GetLenses_jbs.[dbo].EMV_top_orderrev t2 ON t2.customer_id = t1.CLIENT_ID AND t2.RowNumber=1
		WHERE t2.order_id IS NOT NULL
			and (t1.ORDER_REMINDER_DATE = '' OR t1.ORDER_REMINDER_DATE is null);


		-- ATTRIBUTES
			-- STORE_ID UPDATE
		UPDATE t1
		SET t1.STORE_ID = 22 --visiondirect.nl
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID IN (5, 6) --eur.nl

		UPDATE t1
		SET t1.STORE_ID = 23 --visiondirect.es
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 17 --eur.es

		UPDATE t1
		SET t1.STORE_ID = 21 --visiondirect.ie
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 3 --gl.ie

		UPDATE t1
		SET t1.STORE_ID = 24 --visiondirect.it
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID = 10 --gl.it

		UPDATE t1
		SET t1.STORE_ID = 20 --visiondirect.co.ik
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
		WHERE t1.STORE_ID IN (1,2,4) --gl.uk/nl/postoptics

		-- Attributes: ORDER_REMINDER_PRODUCT
		-- Filter: Summary of Customer - Product - Order relation : What product is the choosen one if many products in order	
		UPDATE t1
 		SET t1.ORDER_REMINDER_PRODUCT = t3.name
 		FROM
	 			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
 			LEFT JOIN 
				DW_GetLenses_jbs.dbo.EMV_top_order_item t3 ON t1.ORDER_REMINDER_ID = t3.order_id AND t3.order_source = 'current_db' 
					AND t3.report_group_category IN ('Dailies', 'Two Weeklies', 'Monthlies', 'Torics for Astigmatism', 'Multifocals', 'Colours');

		UPDATE t1
 		SET t1.ORDER_REMINDER_PRODUCT = t3.name
 		FROM
	 			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
 			LEFT JOIN 
				DW_GetLenses_jbs.dbo.EMV_top_order_item t3 ON t1.ORDER_REMINDER_ID = t3.order_id AND t3.order_source = 'current_db' 
 		WHERE t1.ORDER_REMINDER_PRODUCT IS NULL OR t1.ORDER_REMINDER_PRODUCT = '';

		-- ATTRIBUTES: PRESCRIPTION
		UPDATE t1
		SET	
			t1.PRESCRIPTION_EXPIRY_DATE = cast(pr1.date_last_prescription as date),
			t1.PRESCRIPTION_WEARER = 
				(CASE WHEN (pr1.name = 'myself') THEN (t1.FIRSTNAME+' '+t1.LASTNAME+'''s') ELSE pr1.name+'''s' END),
			t1.PRESCRIPTION_OPTICIAN = pr1.optician_name
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				[dbo].[order_headers] t0 ON t0.customer_id = t1.CLIENT_ID 
			LEFT JOIN 
				DW_GetLenses_jbs.dbo.EMV_top_order_presc pr1  ON pr1.order_id = t0.order_id 
		WHERE t0.reminder_presc = 'on' AND pr1.RowNumber = 1;
	
		-- ATTRIBUTES: DOB
		UPDATE t1
		SET	t1.DATEOFBIRTH = w1.DOB
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				dw_wearer w1 ON w1.customer_id=t1.CLIENT_ID and w1.name='myself'
		WHERE 1=1;

		-- ATTRIBUTES: COUNTRY
		UPDATE t1
		SET	t1.COUNTRY = country_id
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				customers t2 ON t2.customer_id=t1.CLIENT_ID
			LEFT JOIN 
				customer_addresses t3 ON t3.address_id=t2.default_billing;

		-- ATTRIBUTES: DATEUNJOIN (Why 2010-07-01/02/03)
		-- FILTER: bounces / unsubscribes / email
		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-01' 
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, bounces t2
		WHERE t1.[EMAIL] = t2.email COLLATE SQL_Latin1_General_CP1_CI_AS;

		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-02' 
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, unsubscribes t2
		WHERE t1.[EMAIL] = t2.email COLLATE SQL_Latin1_General_CP1_CI_AS;
		
		UPDATE t1
		set t1.DATEUNJOIN = '2010-07-03' 
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1
		WHERE t1.[EMAIL] LIKE '%@polocal.com' OR t1.[EMAIL] LIKE '%@gelocal.com';	


		-- TEMP table: local_config
		SELECT 
			LANGUAGE = UPPER(LEFT( [dbo].[func_get_config_value] ('general/locale/code', store_id) ,2)),
			CURRENCY = [dbo].[func_get_config_value] ('currency/options/base', store_id),
			store_id
		-- INTO #local_config
		FROM dw_stores_full

		-- Attributes: 
			-- EMAIL_ORIGINE - EMAIL_ORIGIN: store_name
			-- GENDER: Depending on Title
			-- LANGUAGE - CURRENCY: Depending on the Store
		UPDATE t1
		SET
			t1.EMAIL_ORIGINE = sf.store_name, t1.EMAIL_ORIGIN = sf.store_name,
			t1.GENDER = (CASE WHEN t3.GENDER IS NOT NULL THEN t3.GENDER ELSE '' END),
			t1.LANGUAGE=  lc.LANGUAGE, t1.CURRENCY = lc.CURRENCY
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			LEFT JOIN 
				EMV_gender_prefix t3 ON t3.title = t1.title
			LEFT JOIN 
				dw_stores_full sf ON sf.store_id = t1.STORE_ID
			LEFT JOIN 
				#local_config lc ON lc.store_id = sf.store_id

		-- Attributes: STORE_URL
		UPDATE t1
		SET t1.[STORE_URL] = [dbo].[func_get_config_value] ('web/unsecure/base_url', t1.STORE_ID)
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1

		--------------------------------

		-- Attributes: DAILIES
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber
		UPDATE t1
		SET
			t1.[DAILIES_STORE_ID1]=t2.order_store_id,
			t1.[DAILIES_SKU1]=t2.product_id, t1.[DAILIES_NAME1]=t2.name, t1.[DAILIES_URL1]=t2.url,
			t1.[DAILIES_LAST_ORDER_DATE]=t2.last_order_date, 
			t1.google_shopping_gtin_daily_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Dailies' AND 
			t2.customer_id = t1.CLIENT_ID;	

		UPDATE t1
		SET
			t1.[DAILIES_STORE_ID2]=t2.order_store_id,
			t1.[DAILIES_SKU2]=t2.product_id, t1.[DAILIES_NAME2]=t2.name, t1.[DAILIES_URL2]=t2.url,
			t1.google_shopping_gtin_daily_sku2=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Contact Lenses - Dailies' AND 
			t2.customer_id = t1.CLIENT_ID;


		-- Attributes: MONTHLIES
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber
		UPDATE t1
		SET
			t1.[MONTHLIES_STORE_ID1]=t2.order_store_id,
			t1.[MONTHLIES_SKU1]=t2.product_id, t1.[MONTHLIES_NAME1]=t2.name, t1.[MONTHLIES_URL1]=t2.url,
			t1.[MONTHLIES_CATEGORY1]=t2.report_category, 
			t1.[MONTHLIES_LAST_ORDER_DATE]=t2.last_order_date,
			t1.google_shopping_gtin_monthly_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE	
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Monthlies & Other' AND 
			t2.customer_id = t1.CLIENT_ID;	

		UPDATE t1
		SET
			t1.[MONTHLIES_STORE_ID1]=t2.order_store_id,
			t1.[MONTHLIES_SKU1]=t2.product_id, t1.[MONTHLIES_NAME1]=t2.name, t1.[MONTHLIES_URL1]=t2.url,
			t1.[MONTHLIES_CATEGORY1]=t2.report_category, 
			t1.[MONTHLIES_LAST_ORDER_DATE]=t2.last_order_date,
			t1.google_shopping_gtin_monthly_sku2=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE	
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Monthlies & Other' AND 
			t2.customer_id = t1.CLIENT_ID;	

			
		-- Attributes: COLOURS
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber			
		UPDATE t1
		SET
			t1.[COLOURS_STORE_ID1]=t2.order_store_id,
			t1.[COLOURS_SKU1]=t2.product_id, t1.[COLOURS_NAME1]=t2.name, t1.[COLOURS_URL1]=t2.url,
			t1.[COLOURS_LAST_ORDER_DATE]=t2.last_order_date,
			t1.google_shopping_gtin_colours_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Contact Lenses - Colours' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[COLOURS_STORE_ID2]=t2.order_store_id,
			t1.[COLOURS_SKU2]=t2.product_id, t1.[COLOURS_NAME2]=t2. name, t1.[COLOURS_URL2]=t2.url,
			t1.google_shopping_gtin_solutions_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Contact Lenses - Colours' AND
			t2.customer_id = t1.CLIENT_ID;

			
		-- Attributes: SOLUTIONS
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber			
		UPDATE t1
		SET
			t1.[SOLUTIONS_STORE_ID1]=t2.order_store_id,
			t1.[SOLUTIONS_SKU1]=t2.product_id, t1.[SOLUTIONS_NAME1]=t2.name, t1.[SOLUTIONS_URL1]=t2.url,
			t1.[SOLUTIONS_CATEGORY1]=t2.report_category,
			t1.[SOLUTIONS_LAST_ORDER_DATE]=t2.last_order_date,
			t1.google_shopping_gtin_solutions_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Solutions & Eye Care' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[SOLUTIONS_STORE_ID2]=t2.order_store_id,
			t1.[SOLUTIONS_SKU2]=t2.product_id, t1.[SOLUTIONS_NAME2]=t2.name, t1.[SOLUTIONS_URL2]=t2.url,
			t1.[SOLUTIONS_CATEGORY2]=t2.report_category,
			t1.google_shopping_gtin_solutions_sku2=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Solutions & Eye Care' AND
			t2.customer_id = t1.CLIENT_ID;

		-- Attributes: SUNGLASSES
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber
		UPDATE t1
		SET
			t1.[SUNGLASSES_STORE_ID1]=t2.order_store_id,
			t1.[SUNGLASSES_SKU1]=t2.product_id, t1.[SUNGLASSES_NAME1]=t2.name, t1.[SUNGLASSES_URL1]=t2.url,
			t1.[SUNGLASSES_CATEGORY1]=t2.report_category,
			t1.[SUNGLASSES_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Sunglasses' AND
			t2.customer_id = t1.CLIENT_ID;	
				
		UPDATE t1
		SET
			t1.[SUNGLASSES_STORE_ID2]=t2.order_store_id,
			t1.[SUNGLASSES_SKU2]=t2.product_id, t1.[SUNGLASSES_NAME2]=t2.name, t1.[SUNGLASSES_CATEGORY2]=t2.report_category,
			t1.[SUNGLASSES_URL2]=t2.url
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Sunglasses' AND
			t2.customer_id = t1.CLIENT_ID;


		-- Attributes: GLASSES
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber
		UPDATE t1
		SET
			t1.[GLASSES_STORE_ID1]=t2.order_store_id,
			t1.[GLASSES_SKU1]=t2.product_id, t1.[GLASSES_NAME1]=t2.name, t1.[GLASSES_URL1]=t2.url,
			t1.[GLASSES_CATEGORY1]=t2.report_category,
			t1.[GLASSES_LAST_ORDER_DATE]=t2.last_order_date
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Glasses' AND
			t2.customer_id = t1.CLIENT_ID;	
				
		UPDATE t1
		SET
			t1.[GLASSES_STORE_ID2]=t2.order_store_id,
			t1.[GLASSES_SKU2]=t2.product_id, t1.[GLASSES_NAME2]=t2.name, t1.[GLASSES_CATEGORY2]=t2.report_category,
			t1.[GLASSES_URL2]=t2.url
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Glasses' AND
			t2.customer_id = t1.CLIENT_ID;
	
	
		-- Attributes: OTHER
		-- Filter: Summary of Customer - Product relation with Last Order info // Based on RowNumber	
		UPDATE t1
		SET
			t1.[OTHER_STORE_ID1]=t2.order_store_id,
			t1.[OTHER_SKU1]=t2.product_id, t1.[OTHER_NAME1]=t2.name, t1.[OTHER_URL1]=t2.url,
			t1.[OTHER_CATEGORY1]=t2.report_category, 
			t1.[OTHER_LAST_ORDER_DATE]=t2.last_order_date,
			t1.google_shopping_gtin_other_sku1=t2.google_shopping_gtin
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 1 AND 
			t2.report_group_category = 'Other' AND
			t2.customer_id = t1.CLIENT_ID;	
		
		UPDATE t1
		SET
			t1.[OTHER_STORE_ID2]=t2.order_store_id,
			t1.[OTHER_SKU2]=t2.product_id, t1.[OTHER_NAME2]=t2. name, t1.[OTHER_URL2]=t2.url,
			t1.[OTHER_CATEGORY2]=t2.report_category,
			t1.google_shopping_gtin_other_sku2=t2.google_shopping_gtin
		FROM
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
				DW_GetLenses_jbs.[dbo].EMV_last_order_item t2
		WHERE 
			t2.[RowNumber] = 2 AND 
			t2.report_group_category = 'Other' AND
			t2.customer_id = t1.CLIENT_ID;

		--------------------------------

		-- Attributes
			-- EMVADMIN5: Last Shipped Date
			-- EMVADMIN1@ Last Order ID

		UPDATE t1	
		SET	t1.[EMVADMIN5] = t2.last_shipped
		FROM
			DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1, 
			DW_GetLenses_jbs.[dbo].EMV_last_shipment t2
		WHERE 
			t1.CLIENT_ID = t2.customer_id AND t1.LAST_ORDER_ID = t2.order_id AND t1.LAST_ORDER_SOURCE = t2.source;

		UPDATE t1	
		SET	t1.[EMVADMIN1] = t1.[LAST_ORDER_ID]
		FROM DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1

		-- Autologin
		select *
		from autologin_last_date


		-- Attributes: SEGMENT_3: Hash value
		UPDATE	t1 
		SET		
			t1.SEGMENT_3 = CAST(c.customer_id as varchar(100)) + '-' + 
				SUBSTRING(master.dbo.fn_varbintohexstr(HashBytes('MD5', LEFT(password_hash,10)+cast(customer_id as varchar(100))+REPLACE(CONVERT(VARCHAR(10), GETDATE(), 126), '/', '-'))), 3, 32)
		FROM	
				DW_GetLenses_jbs.[dbo].[EMV_sync_list] t1 
			INNER JOIN 
				customers c ON c.customer_id = t1.CLIENT_ID

		--------------------------------

		-- Attributes: LAST + LENS (COLOURS - MONTHLIES - DAILIES)
		-- Based on Priority: OTHER - SOLUTIONS - COLOURS - MONTHLIES - DAILIES // SUNGLASSES - GLASSES ??
			
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [OTHER_LAST_ORDER_DATE] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [OTHER_NAME1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [OTHER_NAME2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [OTHER_SKU1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [OTHER_SKU2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [OTHER_URL1] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET  [LAST_PRODUCT_URL2] = [OTHER_URL2] 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET  [LAST_PRODUCT_TYPE] = 'OTHER' 
		WHERE [OTHER_SKU1] IS NOT NULL AND [OTHER_SKU1] !=''


		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [SOLUTIONS_LAST_ORDER_DATE] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [SOLUTIONS_NAME1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [SOLUTIONS_NAME2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [SOLUTIONS_SKU1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [SOLUTIONS_SKU2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [SOLUTIONS_URL1] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [SOLUTIONS_URL2] 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'SOLUTIONS' 
		WHERE [SOLUTIONS_SKU1] IS NOT NULL AND [SOLUTIONS_SKU1] !=''


		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [COLOURS_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [COLOURS_LAST_ORDER_DATE]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [COLOURS_NAME1], [LENS_NAME1] = [COLOURS_NAME1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [COLOURS_NAME2], [LENS_NAME2] = [COLOURS_NAME2]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [COLOURS_SKU1], [LENS_SKU1] = [COLOURS_SKU1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [COLOURS_SKU2], [LENS_SKU2] = [COLOURS_SKU2] 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [COLOURS_URL1], [LENS_URL1] = [COLOURS_URL1]
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [COLOURS_URL2], [LENS_URL2] = [COLOURS_URL2] 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'COLOURS' 
		WHERE [COLOURS_SKU1] IS NOT NULL AND [COLOURS_SKU1] !=''

		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [MONTHLIES_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [MONTHLIES_LAST_ORDER_DATE] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [MONTHLIES_NAME1], [LENS_NAME1] = [MONTHLIES_NAME1] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [MONTHLIES_NAME2], [LENS_NAME2] = [MONTHLIES_NAME2]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [MONTHLIES_SKU1], [LENS_SKU1] = [MONTHLIES_SKU1]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [MONTHLIES_SKU2], [LENS_SKU2] = [MONTHLIES_SKU2]
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [MONTHLIES_URL1], [LENS_URL1] = [MONTHLIES_URL1]
		WHERE  [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [MONTHLIES_URL2], [LENS_URL2] = [MONTHLIES_URL2] 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'MONTHLIES' 
		WHERE [MONTHLIES_SKU1] IS NOT NULL AND [MONTHLIES_SKU1] !=''

		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_LAST_ORDER_DATE] = [DAILIES_LAST_ORDER_DATE], [LENS_LAST_ORDER_DATE] = [DAILIES_LAST_ORDER_DATE]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME1] = [DAILIES_NAME1], [LENS_NAME1] = [DAILIES_NAME1]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_NAME2] = [DAILIES_NAME2], [LENS_NAME2] = [DAILIES_NAME2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU1] = [DAILIES_SKU1], [LENS_SKU1] = [DAILIES_SKU1]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_SKU2] = [DAILIES_SKU2], [LENS_SKU2] = [DAILIES_SKU2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] != ''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL1] = [DAILIES_URL1], [LENS_URL1] = [DAILIES_URL1] 
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_URL2] = [DAILIES_URL2], [LENS_URL2] = [DAILIES_URL2]
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''
		UPDATE DW_GetLenses_jbs.[dbo].[EMV_sync_list] 
		SET [LAST_PRODUCT_TYPE] = 'DAILIES' 
		WHERE [DAILIES_SKU1] IS NOT NULL AND [DAILIES_SKU1] !=''

		--------------------------------

		-- Attributes: MONTHLIES_CATEGORY1 // SOLUTIONS_CATEGORY1
		-- Filter: Based on store_id and category_local_list
		UPDATE t1
		SET t1.MONTHLIES_CATEGORY1 = t2.category_local
		FROM 
				DW_GetLenses_jbs.[dbo].EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.MONTHLIES_CATEGORY1 = t2.category AND t1.MONTHLIES_STORE_ID1 = t2.store_id
		
		UPDATE t1
		SET t1.MONTHLIES_CATEGORY2 = t2.category_local
		FROM 
				DW_GetLenses_jbs.[dbo].EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.MONTHLIES_CATEGORY2 = t2.category AND t1.MONTHLIES_STORE_ID2 = t2.store_id
		
		UPDATE t1
		SET t1.SOLUTIONS_CATEGORY1 = t2.category_local
		FROM 
				DW_GetLenses_jbs.[dbo].EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.SOLUTIONS_CATEGORY1 = t2.category AND t1.SOLUTIONS_STORE_ID1 = t2.store_id
		
		UPDATE t1
		SET t1.SOLUTIONS_CATEGORY2 = t2.category_local
		FROM 
				DW_GetLenses_jbs.[dbo].EMV_sync_list t1
			INNER JOIN 
				category_local_list t2 ON t1.SOLUTIONS_CATEGORY2 = t2.category AND t1.SOLUTIONS_STORE_ID2 = t2.store_id

		-- Attributes: check_sum
		UPDATE EMV_sync_list 
		set check_sum = HASHBYTES('MD5', COALESCE(SOURCE ,'') + COALESCE(EMAIL_ORIGINE ,'') + COALESCE(SEGMENT_LIFECYCLE ,'') + COALESCE(cast(LAST_ORDER_DATE AS varchar(20)),'') +
			COALESCE(cast(WEBSITE_UNSUBSCRIBE AS varchar(10)),'') + COALESCE(COUNTRY ,'') + COALESCE(cast(REGISTRATION_DATE AS varchar(20)),'') + COALESCE(SEGMENT_GEOG ,'') +
			COALESCE(FIRSTNAME ,'') + COALESCE(SEGMENT_2 ,'') + COALESCE(SEGMENT_3 ,'') + COALESCE(cast(W_UNSUB_DATE AS varchar(20)),'')) 