
select oh_sc.order_id_bk, 
	oh_sc.store_id_bk, oh_sc.customer_id_bk, oh_sc.country_id_shipping_bk
from Landing.aux.sales_order_header_store_cust oh_sc

select oh_sc.order_id_bk, oh.invoice_date,
	oh_sc.store_id_bk, s.name, oh_sc.customer_id_bk, oh_sc.country_id_shipping_bk, c.country_type 
from 
		Landing.aux.sales_order_header_store_cust oh_sc
	inner join
		Landing.aux.sales_order_header_o_i_s_cr oh on oh_sc.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.core_store_aud s on oh_sc.store_id_bk = s.store_id
	inner join
		Landing.map.gen_countries_aud c on oh_sc.country_id_shipping_bk = c.country_code
order by c.country_type, oh_sc.country_id_shipping_bk, oh_sc.order_id_bk

select cr.countries_registered_code, 
	cr.country_type, cr.country_code,
	vr.vat_time_period_start, vr.vat_time_period_finish
from 
		Landing.map.vat_countries_registered_aud cr
	inner join
		(select distinct countries_registered_code, vat_time_period_start, vat_time_period_finish
		from Landing.map.vat_vat_rate_aud) vr on cr.countries_registered_code = vr.countries_registered_code
order by cr.countries_registered_code, vr.vat_time_period_start

----------------------------------------------

select oh_sc.order_id_bk, oh.invoice_date,
	oh_sc.store_id_bk, s.name, oh_sc.customer_id_bk, oh_sc.country_id_shipping_bk, c.country_type, 
	vr.countries_registered_code, vr2.countries_registered_code, 
	case when (vr.countries_registered_code is not null) then vr.countries_registered_code else vr2.countries_registered_code end countries_registered_code_def
from 
		Landing.aux.sales_order_header_store_cust oh_sc
	inner join
		(select order_id_bk, case when invoice_date is not null then invoice_date else order_date end invoice_date
		from Landing.aux.sales_order_header_o_i_s_cr) oh on oh_sc.order_id_bk = oh.order_id_bk
	inner join
		Landing.mag.core_store_aud s on oh_sc.store_id_bk = s.store_id
	inner join
		Landing.map.gen_countries_aud c on oh_sc.country_id_shipping_bk = c.country_code
	left join
		(select cr.countries_registered_code, 
			cr.country_type, cr.country_code,
			vr.vat_time_period_start, vr.vat_time_period_finish
		from 
				Landing.map.vat_countries_registered_aud cr
			inner join
				(select distinct countries_registered_code, vat_time_period_start, vat_time_period_finish
				from Landing.map.vat_vat_rate_aud) vr on cr.countries_registered_code = vr.countries_registered_code
		where cr.country_type in ('UK', 'EU') and cr.country_code <> 'ZZ') vr 
					on c.country_type = vr.country_type and oh_sc.country_id_shipping_bk = vr.country_code and oh.invoice_date between vr.vat_time_period_start and vr.vat_time_period_finish
	left join
		(select cr.countries_registered_code, 
			cr.country_type, cr.country_code,
			vr.vat_time_period_start, vr.vat_time_period_finish
		from 
				Landing.map.vat_countries_registered_aud cr
			inner join
				(select distinct countries_registered_code, vat_time_period_start, vat_time_period_finish
				from Landing.map.vat_vat_rate_aud) vr on cr.countries_registered_code = vr.countries_registered_code
		where cr.country_code = 'ZZ') vr2 
					on c.country_type = vr2.country_type and oh.invoice_date is not null and vr.countries_registered_code is null
-- where vr.countries_registered_code is null and vr2.countries_registered_code is null
order by c.country_type, oh_sc.country_id_shipping_bk, oh_sc.order_id_bk
