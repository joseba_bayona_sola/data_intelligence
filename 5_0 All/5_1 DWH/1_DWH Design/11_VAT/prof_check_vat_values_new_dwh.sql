

select top 1000 order_id_bk, order_no, order_date, store_name, -- customer_id, customer_email, 
	country_code_ship, countries_registered_code,
	order_status_name, 
	local_subtotal, local_subtotal_refund, 
	local_total_refund, local_store_credit_given, local_bank_online_given,
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee 	
from Warehouse.sales.dim_order_header_v
where order_id_bk in (5689086, 5661998, 5739085, 5739013, 5744097, 5744096)
order by order_id_bk desc


select top 1000 order_id_bk, order_no, order_date, store_name,
	country_code_ship, countries_registered_code, product_type_vat,
	product_family_name,
	qty_unit, qty_unit_refunded, 
	local_subtotal, local_subtotal_refund, 
	local_store_credit_given, local_bank_online_given, local_store_credit_used_refund,
	vat_rate, prof_fee_rate,
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	vat_percent
from Warehouse.sales.fact_order_line_v
where order_id_bk in (5689086, 5661998, 5739085, 5739013, 5744097, 5744096, 5744091)
order by order_id_bk desc

