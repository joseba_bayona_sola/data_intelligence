
select top 100 order_id, order_no, order_date, 
	store_name, shipping_country_id, 
	document_type, 
	local_subtotal_inc_vat, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_prof_fee
from DW_GetLenses.dbo.invoice_headers
where order_id in (5744097, 5744096, 5739085, 5739013, 5689086, 5661998, 5744091)
order by order_id desc, document_type desc


select top 100 ih.order_id, ih.order_no, ih.order_date, 
	ih.store_name, ih.shipping_country_id, 
	il.document_type, 
	il.name, 
	il.qty, 
	il.local_line_subtotal_inc_vat, 
	il.vat_percent, il.vat_percent_before_prof_fee, il.prof_fee_percent,
	il.local_line_total_inc_vat, il.local_line_total_exc_vat, il.local_line_total_vat, il.local_prof_fee
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join
		(select invoice_id, document_id, document_type,
			order_id, order_no, order_date, 
			store_name, shipping_country_id
		from DW_GetLenses.dbo.invoice_headers
		where order_id in (5744097, 5744096, 5739085, 5739013, 5689086, 5661998, 5744091)) ih on il.document_id = ih.document_id and il.document_type = ih.document_type
order by ih.order_id desc, il.document_type desc

select top 1000 order_id_bk, order_no, order_date, store_name,
	country_code_ship, countries_registered_code, product_type_vat,
	product_family_name,
	qty_unit, qty_unit_refunded, 
	local_subtotal, local_subtotal_refund, 
	local_store_credit_given, local_bank_online_given, local_store_credit_used_refund,
	vat_rate, prof_fee_rate,
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee
from Warehouse.sales.fact_order_line_v
where order_id_bk in (5689086, 5661998, 5739085, 5739013, 5744097, 5744096)
order by order_id_bk desc