

select top 1000 *
from Warehouse.sales.dim_order_header

select top 1000 *
from Warehouse.sales.fact_order_line


--------------------------------

	-- OH: 2 ways of calculating local_total_inc_vat value after refund - Compare
	select top 1000 order_id_bk, 
		local_subtotal, local_subtotal_refund, 
		local_total_refund, local_store_credit_given, local_bank_online_given,
		local_total_inc_vat, local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2		
	from Landing.aux.sales_dim_order_header_aud
	where local_total_aft_refund_inc_vat <> local_total_aft_refund_inc_vat_2		
	order by abs(local_total_aft_refund_inc_vat - local_total_aft_refund_inc_vat_2) desc

--------------------

	-- Looking in new DWH at OL level the important REFUND values (Value or not)
	select top 1000 order_id, order_no, 
		qty_unit, qty_unit_refunded, 
		local_subtotal, local_subtotal_refund, 
		local_store_credit_given, local_bank_online_given,
		local_total_inc_vat, local_total_aft_refund_inc_vat, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
	from Warehouse.sales.fact_order_line
	where 
		-- (local_store_credit_given is not null) or (local_bank_online_given is not null)
		(local_total_aft_refund_inc_vat is not null)


--------------------------------
-- OH - OL special values for REFUND

select top 1000 order_id_bk, order_no, order_date, store_name, customer_id, customer_email, 
	order_status_name, 
	local_subtotal, local_subtotal_refund, 
	local_total_refund, local_store_credit_given, local_bank_online_given,
	local_total_inc_vat, local_total_aft_refund_inc_vat	
from Warehouse.sales.dim_order_header_v
where order_status_name = 'REFUND' -- REFUND - PARTIAL REFUND
order by order_id_bk desc


select top 1000 order_id_bk, order_no, order_date, store_name, -- customer_id, customer_email, 
	order_status_name, 
	local_subtotal, local_subtotal_refund, 
	local_total_refund, local_store_credit_given, local_bank_online_given,
	local_total_inc_vat, local_total_aft_refund_inc_vat	
from Warehouse.sales.dim_order_header_v
where order_id_bk in (5344495, 5177311, 5689086, 5661998, 5739085, 5739013)
order by order_id_bk desc

select top 1000 order_id, order_no, 
	qty_unit, qty_unit_refunded, 
	local_subtotal, local_subtotal_refund, 
	local_store_credit_given, local_bank_online_given,
	local_total_inc_vat, local_total_aft_refund_inc_vat, 
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
from Warehouse.sales.fact_order_line
where order_id in (5344495, 5177311, 5689086, 5661998, 5739085, 5739013)
order by order_id desc


select top 1000 order_id_bk, 
	qty_unit, qty_unit_refunded, 
	local_subtotal, local_subtotal_refund, 
	local_store_credit_given, local_bank_online_given, local_store_credit_used_refund,
	local_total_inc_vat, local_total_aft_refund_inc_vat, 
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
from Landing.aux.sales_order_line_measures
where order_id_bk in (5344495, 5177311, 5689086, 5661998, 5739085, 5739013)
order by order_id_bk desc

-----------------------------------------------------------
-- COMPARE BETWEEN local_total_aft_refund_inc_vat CALCULATED FOR OH-OL
select oh.order_id_bk, order_no, order_date, store_name, -- customer_id, customer_email, 
	order_status_name, adjustment_order,
	local_subtotal, local_subtotal_refund, 
	local_total_refund, local_store_credit_given, local_bank_online_given,
	local_total_inc_vat, oh.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_inc_vat, 
	abs(oh.local_total_aft_refund_inc_vat - ol.local_total_aft_refund_inc_vat) diff	
from 
		Warehouse.sales.dim_order_header_v oh
	inner join
		(select order_id_bk, sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat
		from Landing.aux.sales_order_line_measures
		-- where local_store_credit_given <> 0 or local_bank_online_given <> 0
		group by order_id_bk) ol on oh.order_id_bk = ol.order_id_bk
-- where order_status_name = 'OK' and adjustment_order = 'Y'
where oh.order_id_bk in (5226041, 5227882, 5604768)
order by diff desc, order_status_name, oh.order_id_bk

	select order_id_bk, order_no, order_date, store_name, -- customer_id, customer_email, 
		order_status_name, adjustment_order,
		local_subtotal, local_subtotal_refund, 
		local_total_refund, local_store_credit_given, local_bank_online_given,
		local_total_inc_vat, local_total_aft_refund_inc_vat
	from Warehouse.sales.dim_order_header_v 
	where order_status_name <> 'OK' and adjustment_order = 'Y'
	order by order_id_bk desc
