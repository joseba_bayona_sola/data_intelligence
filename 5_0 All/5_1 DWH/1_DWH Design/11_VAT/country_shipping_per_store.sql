
select top 1000 *
from Warehouse.sales.dim_order_header_v
where year(order_date_c) = 2017

select top 1000 store_name, country_code_ship, country_name_ship, count(*)
from Warehouse.sales.dim_order_header_v
where year(order_date_c) = 2017
group by store_name, country_code_ship, country_name_ship
order by store_name, country_code_ship, country_name_ship

select store_name, country_code_ship, country_name_ship, num
from
	(select store_name, country_code_ship, country_name_ship, count(*) num
	from Warehouse.sales.dim_order_header_v
	where year(order_date_c) = 2017
	group by store_name, country_code_ship, country_name_ship) t
where
	(store_name= 'visiondirect.be/fr' and country_code_ship <> 'BE') or 
	(store_name= 'visiondirect.be/nl' and country_code_ship <> 'BE') or 
	(store_name= 'visiondirect.ca/en' and country_code_ship <> 'CA') or 
	(store_name= 'visiondirect.ca/fr' and country_code_ship <> 'CA') or 
	(store_name= 'visiondirect.es' and country_code_ship <> 'ES') or 
	(store_name= 'visiondirect.fr' and country_code_ship <> 'FR') or 
	(store_name= 'visiondirect.ie' and country_code_ship <> 'IE') or 
	(store_name= 'visiondirect.it' and country_code_ship <> 'IT') or 
	(store_name= 'visiondirect.nl' and country_code_ship <> 'NL') 
order by store_name, country_code_ship

select idOrderHeader_sk, order_id_bk, order_no, order_date, store_name, 
	customer_email, country_code_ship, country_code_bill, 
	order_status_name, 
	local_total_inc_vat, global_total_inc_vat, local_to_global_rate, order_currency_code
from Warehouse.sales.dim_order_header_v
where year(order_date_c) = 2017
	and store_name <> 'visiondirect.co.uk' and country_code_ship = 'GB'
order by store_name, order_date_c


---------------------------------

select country_code, country_name, country_type, 
	country_zone, country_continent, country_state
from Landing.map.gen_countries
order by country_type, country_code

	select country_type, count(*)
	from Landing.map.gen_countries
	group by country_type
	order by country_type

