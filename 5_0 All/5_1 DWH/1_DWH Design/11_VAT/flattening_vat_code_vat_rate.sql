	-- Update {$entity}_vat (margin, cost values)
	UPDATE {$entity}_vat ov
	SET
		vat_percent = (local_total_vat) / (local_total_exc_vat),
		vat_percent_before_prof_fee = vat_rate - 1 ,
		discount_percent = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat  + ABS(local_discount_exc_vat))),
		local_margin_amount = local_total_exc_vat - local_total_cost  ,
		local_margin_percent = (local_total_exc_vat - local_total_cost) / local_total_exc_vat
	WHERE vat_done = 0;