
select order_id_bk, region_id_shipping_bk
into #oh_region_shipping
from Landing.aux.sales_dim_order_header_aud
where region_id_shipping_bk is not null

select ohs.order_id_bk, ohs.region_id_shipping_bk, r.idRegion_sk
from 
		#oh_region_shipping ohs
	inner join
		Warehouse.gen.dim_region r on ohs.region_id_shipping_bk = r.region_id_bk

merge into Warehouse.sales.dim_order_header trg
using 
	(select ohs.order_id_bk, ohs.region_id_shipping_bk, r.idRegion_sk
	from 
			#oh_region_shipping ohs
		inner join
			Warehouse.gen.dim_region r on ohs.region_id_shipping_bk = r.region_id_bk) src 
	on trg.order_id_bk = src.order_id_bk
when matched then 
	update 
		set trg.idRegionShipping_sk_fk = src.idRegion_sk;

