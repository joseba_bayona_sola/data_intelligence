
select top 1000 payment_method_name_bk, payment_method_code, count(*)
from Landing.aux.sales_dim_order_header_aud
where year(order_date) = 2017
group by payment_method_name_bk, payment_method_code
order by payment_method_name_bk, payment_method_code

select year(order_date), month(order_date), count(*)
from Landing.aux.sales_dim_order_header_aud
where payment_method_code in ('globalcollect_cc', 'globalcollect_token')
group by year(order_date), month(order_date)
order by year(order_date), month(order_date)

select order_id_bk, payment_method_code, payment_method_name_bk
into #payment_method_provider
from Landing.aux.sales_dim_order_header_aud
where payment_method_code in ('globalcollect_cc', 'globalcollect_token')

-----------------------------------------

select count(*)
from #payment_method_provider pmp

select top 100000 pmp.*, pm.payment_method, count(*) over ()
from 
		#payment_method_provider pmp
	inner join
		Landing.map.sales_payment_method_aud pm on pmp.payment_method_code = pm.payment_method_code

select top 1000 pmp.*, pm.payment_method, pm2.idPaymentMethod_sk, count(*) over ()
from 
		#payment_method_provider pmp
	inner join
		Landing.map.sales_payment_method_aud pm on pmp.payment_method_code = pm.payment_method_code
	inner join
		Warehouse.sales.dim_payment_method pm2 on pm.payment_method = pm2.payment_method_name_bk

-----------------------------------------


-- Landing.aux.sales_dim_order_header_aud

merge Landing.aux.sales_dim_order_header_aud trg
using 
	(select pmp.order_id_bk, pm.payment_method payment_method_name_bk
	from 
			#payment_method_provider pmp
		inner join
			Landing.map.sales_payment_method_aud pm on pmp.payment_method_code = pm.payment_method_code) src
on trg.order_id_bk = src.order_id_bk
when matched and isnull(trg.payment_method_name_bk, '') <> isnull(src.payment_method_name_bk, '')
then 
	update set
		trg.payment_method_name_bk = src.payment_method_name_bk;

-- Warehouse.sales.dim_order_header
merge into Warehouse.sales.dim_order_header trg
using 
	(select pmp.order_id_bk, pm2.idPaymentMethod_sk idPaymentMethod_sk_fk
	from 
			#payment_method_provider pmp
		inner join
			Landing.map.sales_payment_method_aud pm on pmp.payment_method_code = pm.payment_method_code
		inner join
			Warehouse.sales.dim_payment_method pm2 on pm.payment_method = pm2.payment_method_name_bk) src
on trg.order_id_bk = src.order_id_bk
when matched and isnull(trg.idPaymentMethod_sk_fk, 0) <> isnull(src.idPaymentMethod_sk_fk, 0)
then 
	update set
		trg.idPaymentMethod_sk_fk = src.idPaymentMethod_sk_fk;


drop table #payment_method_provider
