
	select *
	from Landing.aux.act_fact_activity_sales
	order by customer_id_bk, activity_date

	select *
	from Landing.aux.act_fact_activity_other 

	---------------------------------------------------------
	
	-- DELETE STATEMENT
	delete from Landing.aux.act_fact_activity_sales

	-- INSERT STATEMENT:
	insert into Landing.aux.act_fact_activity_sales(order_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
		customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, 

		local_subtotal, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, 
		local_to_global_rate, order_currency_code
		)

		select oh.order_id_bk, 
			oh.idCalendarOrderDate idCalendarActivityDate, oh.order_date activity_date, null idCalendarPrevActivityDate,null idCalendarNextActivityDate,
			case when (oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')) THEN 'ORDER' else oh.order_status_name_bk end activity_type_name_bk,
			oh.store_id_bk, oh.customer_id_bk, 
			oh.channel_name_bk, oh.marketing_channel_name_bk, oh.price_type_name_bk, oh.discount_f, oh.product_type_oh_bk, oh.order_qty_time, oh.num_diff_product_type_oh,
			null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web,
			oh.local_subtotal, oh.local_discount, oh.local_store_credit_used, 
			oh.local_total_inc_vat, oh.local_total_exc_vat, 
			oh.local_to_global_rate, oh.order_currency_code
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				(select distinct customer_id_bk
				from Landing.aux.sales_dim_order_header_aud
				where customer_id_bk in (437782, 778490, 1415280)
				union
				select distinct customer_id_bk
				from Landing.aux.sales_dim_order_header_aud, 
					(select (lapsed_num_days * -1) -1 start_range, (lapsed_num_days * -1) +1 finish_range
					from Landing.map.act_lapsed_num_days) l	
				where customer_id_bk in (437782, 778490, 1415280) 
					and order_date between dateadd(day, start_range, getutcdate()) and dateadd(day, finish_range, getutcdate())) c on oh.customer_id_bk = c.customer_id_bk
		order by oh.customer_id_bk, oh.order_date

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			idCalendarActivityDate, activity_date, prev_activity_date, next_activity_date, 
			idCalendarPrevActivityDate, idCalendarNextActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			case when (customer_order_seq_no = 1) then 'NEW' else 'REGULAR' end customer_status_name_bk,
			customer_order_seq_no, customer_order_seq_no_web
		from
			(select order_id_bk, 
				idCalendarActivityDate, activity_date, 
				lag(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) prev_activity_date, 
				lead(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) next_activity_date, 
				lag(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarPrevActivityDate, 
				lead(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarNextActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				customer_status_name_bk, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) customer_order_seq_no, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk, s.website_group order by activity_date, order_id_bk) customer_order_seq_no_web 
			from 
					Landing.aux.act_fact_activity_sales acs
				inner join
					Landing.aux.mag_gen_store_v s on acs.store_id_bk = s.store_id) t) src
	on trg.order_id_bk = src.order_id_bk
	when matched then 
		update set 
			trg.prev_activity_date = src.prev_activity_date, trg.next_activity_date = isnull(src.next_activity_date, getutcdate()),
			trg.idCalendarPrevActivityDate = src.idCalendarPrevActivityDate, trg.idCalendarNextActivityDate = src.idCalendarNextActivityDate, 
			trg.customer_status_name_bk = src.customer_status_name_bk, 
			trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web;

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			activity_date, prev_activity_date, 
			activity_type_name_bk, customer_id_bk, 
			customer_order_seq_no, product_type_oh_bk, order_qty_time,
			case when (prev_activity_date is not null and abs(datediff(day, activity_date, prev_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER') then 'REACTIVATED' else customer_status_name_bk end customer_status_name_bk
		from Landing.aux.act_fact_activity_sales, 
			Landing.map.act_lapsed_num_days l) src
	on trg.order_id_bk = src.order_id_bk
	when matched and src.customer_status_name_bk <> trg.customer_status_name_bk 
		then 
		update set 
			trg.customer_status_name_bk = src.customer_status_name_bk;

---

	-- DELETE STATEMENT
	delete from Landing.aux.act_fact_activity_other 

		-- INSERT STATEMENT: REACTIVATE		
		insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
			idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			activity_seq_no)

			select customer_id_bk activity_id_bk, 
				idCalendarActivityDate, activity_date, 
				CONVERT(INT, (CONVERT(VARCHAR(8), lag(activity_date) over (partition by customer_id_bk order by activity_date, order_id_bk), 112))) idCalendarPrevActivityDate,				 
				'REACTIVATE' activity_type_name_bk, store_id_bk, customer_id_bk, 
				dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) activity_seq_no
			from Landing.aux.act_fact_activity_sales
			where customer_status_name_bk = 'REACTIVATED'
			order by customer_id_bk

		insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
			idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			activity_seq_no)

			select activity_id_bk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), activity_date, 112))) idCalendarActivityDate, activity_date, 
				CONVERT(INT, (CONVERT(VARCHAR(8), lag(activity_date) over (partition by customer_id_bk order by activity_seq_no), 112))) idCalendarPrevActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				activity_seq_no
			from
				(select customer_id_bk activity_id_bk, 
					dateadd(day, l.lapsed_num_days, activity_date) activity_date,
					'LAPSED' activity_type_name_bk, store_id_bk, customer_id_bk, 
					dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) activity_seq_no
				from Landing.aux.act_fact_activity_sales, 
					Landing.map.act_lapsed_num_days l
				where (abs(datediff(day, activity_date, next_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER'))l
			order by customer_id_bk

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

	-- MERGE STATEMENT 
	merge into Warehouse.act.fact_activity_sales with (tablock) as trg
	using Warehouse.act.fact_activity_sales_wrk src
		on (trg.order_id_bk = src.order_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.idCalendarActivityDate_sk_fk, 0), isnull(trg.activity_date, ''), isnull(trg.idCalendarPrevActivityDate_sk_fk, 0), isnull(trg.idCalendarNextActivityDate_sk_fk, 0), 
			isnull(trg.idActivityType_sk_fk, 0), isnull(trg.idStore_sk_fk, 0), isnull(trg.idCustomer_sk_fk, 0), 
			isnull(trg.idChannel_sk_fk, 0), isnull(trg.idMarketingChannel_sk_fk, 0), isnull(trg.idPriceType_sk_fk, 0), isnull(trg.discount_f, ''), 
			isnull(trg.idProductTypeOH_sk_fk, 0), isnull(trg.order_qty_time, 0), isnull(trg.num_diff_product_type_oh, 0), 
			isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), isnull(trg.customer_order_seq_no, 0), isnull(trg.customer_order_seq_no_web, 0), 

			isnull(trg.local_subtotal, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
			isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, '')
		intersect
		select 
			isnull(src.idCalendarActivityDate_sk_fk, 0), isnull(src.activity_date, ''), isnull(src.idCalendarPrevActivityDate_sk_fk, 0), isnull(src.idCalendarNextActivityDate_sk_fk, 0), 
			isnull(src.idActivityType_sk_fk, 0), isnull(src.idStore_sk_fk, 0), isnull(src.idCustomer_sk_fk, 0), 
			isnull(src.idChannel_sk_fk, 0), isnull(src.idMarketingChannel_sk_fk, 0), isnull(src.idPriceType_sk_fk, 0), isnull(src.discount_f, ''), 
			isnull(src.idProductTypeOH_sk_fk, 0), isnull(src.order_qty_time, 0), isnull(src.num_diff_product_type_oh, 0), 
			isnull(src.idCustomerStatusLifecycle_sk_fk, 0), isnull(src.customer_order_seq_no, 0), isnull(src.customer_order_seq_no_web, 0), 

			isnull(src.local_subtotal, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
			isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ''))			
		then 
			update set
				trg.idCalendarActivityDate_sk_fk = src.idCalendarActivityDate_sk_fk, trg.activity_date = src.activity_date, 
				trg.idCalendarPrevActivityDate_sk_fk = src.idCalendarPrevActivityDate_sk_fk, trg.idCalendarNextActivityDate_sk_fk = src.idCalendarNextActivityDate_sk_fk, 
				trg.next_order_freq_time = datediff(dd, src.activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), src.idCalendarNextActivityDate_sk_fk), 112), 126)),
				trg.idActivityType_sk_fk = src.idActivityType_sk_fk, trg.idStore_sk_fk = src.idStore_sk_fk, trg.idCustomer_sk_fk = src.idCustomer_sk_fk, 
				trg.idChannel_sk_fk = src.idChannel_sk_fk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk_fk, trg.idPriceType_sk_fk = src.idPriceType_sk_fk, trg.discount_f = src.discount_f, 
				trg.idProductTypeOH_sk_fk = src.idProductTypeOH_sk_fk, trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh,
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web, 

				trg.local_subtotal = src.local_subtotal, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
				
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_id_bk, 
				idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, idCalendarNextActivityDate_sk_fk, next_order_freq_time,
				idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
				idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk, discount_f,
				idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh,
				idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, 

				local_subtotal, local_discount, local_store_credit_used, 
				local_total_inc_vat, local_total_exc_vat, 

				local_to_global_rate, order_currency_code,
				idETLBatchRun_ins)
				
				values (src.order_id_bk, 
					src.idCalendarActivityDate_sk_fk, src.activity_date, src.idCalendarPrevActivityDate_sk_fk, src.idCalendarNextActivityDate_sk_fk, 
					datediff(dd, src.activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), src.idCalendarNextActivityDate_sk_fk), 112), 126)),
					src.idActivityType_sk_fk, src.idStore_sk_fk, src.idCustomer_sk_fk,
					src.idChannel_sk_fk, src.idMarketingChannel_sk_fk, src.idPriceType_sk_fk, src.discount_f, 
					src.idProductTypeOH_sk_fk, src.order_qty_time, src.num_diff_product_type_oh,
					src.idCustomerStatusLifecycle_sk_fk, src.customer_order_seq_no, src.customer_order_seq_no_web, 

					src.local_subtotal, src.local_discount, src.local_store_credit_used, 
					src.local_total_inc_vat, src.local_total_exc_vat, 

					src.local_to_global_rate, src.order_currency_code,				
					@idETLBatchRun)

	-- DELETE STATEMENT - REACTIVATE, LAPSED
	delete ao
	from 
			Warehouse.act.fact_activity_other ao
		inner join
			Warehouse.act.dim_activity_type a on ao.idActivityType_sk_fk = a.idActivityType_sk
	where a.activity_type_name in ('LAPSED', 'REACTIVATE') 
		and idCustomer_sk_fk in 
			(select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_other_wrk ot
				inner join
					Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE'))

		select ot.activity_id_bk, 
			ot.idCalendarActivityDate_sk_fk, ot.activity_date, ot.idCalendarPrevActivityDate_sk_fk, 
			ot.idActivityType_sk_fk, ot.idStore_sk_fk, ot.idCustomer_sk_fk,
			ot.activity_seq_no,
			ot.idETLBatchRun_ins
		from 
				Warehouse.act.fact_activity_other_wrk ot
			inner join
				Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
		where a.activity_type_name in ('LAPSED', 'REACTIVATE')

---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

		-- Update Customer Status taking REGISTER, LAPSED, REACTIVE from Other and NEW, REGULAR from Sales
		merge into Warehouse.act.fact_customer_signature_aux_other trg
		using 
			(select t.idCustomer_sk_fk, cs.idCustomerStatus_sk idCustomerStatusLifecycle_sk_fk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), t.activity_date, 112))) idCalendarStatusUpdateDate_sk_fk
			from
					(select idCustomer_sk_fk, 
						case 
							when (activity_type_name = 'REGISTER') then 'RNB'
							when (activity_type_name = 'REACTIVATE') then 'REACTIVATED'
							else activity_type_name
						end activity_type_name, activity_date
					from
						(select idCustomer_sk_fk, t.activity_type_name, a.r_imp, activity_date, 
							count(*) over (partition by idCustomer_sk_fk) num_rep_activities,
							dense_rank() over (partition by idCustomer_sk_fk order by activity_date, a.r_imp) ord_activities, 
							count(*) over (partition by idCustomer_sk_fk, activity_date) num_rep_activities_2
						from 
								(select idCustomer_sk_fk, activity_type_name, 
									case when activity_type_name = 'REGISTER' then min(activity_date) over (partition by idCustomer_sk_fk) else activity_date end activity_date
								from 
										(select idCustomer_sk_fk, activity_type_name, activity_date
										from Warehouse.act.fact_activity_other_full_v
										where activity_type_name in ('REGISTER', 'LAPSED')
										union
										select idCustomer_sk_fk, customer_status_name activity_type_name, activity_date
										from Warehouse.act.fact_customer_signature_aux_orders) t) t
							inner join
								(select 'REGISTER' activity_type_name, 1 r_imp
								union
								select 'NEW' activity_type_name, 2 r_imp
								union
								select 'LAPSED' activity_type_name, 3 r_imp
								union
								select 'REACTIVATED' activity_type_name, 4 r_imp
								union
								select 'REGULAR' activity_type_name, 5 r_imp) a on t.activity_type_name = a.activity_type_name
							) t
					where num_rep_activities = ord_activities) t
				inner join
					Warehouse.gen.dim_customer_status cs on t.activity_type_name = cs.customer_status_name_bk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
		when matched then
			update set 
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, 
				trg.idCalendarStatusUpdateDate_sk_fk = src.idCalendarStatusUpdateDate_sk_fk;