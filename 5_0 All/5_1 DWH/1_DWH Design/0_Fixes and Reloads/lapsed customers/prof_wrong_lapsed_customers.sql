
select top 1000 idCustomer_sk_fk, customer_id, customer_email, 
	website_create, num_tot_orders, last_order_date, store_last, datediff(dd, last_order_date, getutcdate())
from Warehouse.act.fact_customer_signature_v
where customer_status_name = 'LAPSED'
	and datediff(dd, last_order_date, getutcdate()) < 450
	-- and customer_id in (437782, 778490, 1415280)
order by store_last, last_order_date desc

-------------------------------------------------

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in (437782, 778490, 1415280)

select *
from Warehouse.act.fact_customer_signature_scd_v
where customer_id in (437782, 778490, 1415280)


select *
from Warehouse.sales.dim_order_header_v
where customer_id in (437782, 778490, 1415280)
order by customer_id, order_date

select *
from Warehouse.act.fact_activity_sales_v
where customer_id in (437782, 778490, 1415280)
order by customer_id, activity_date_c

select *
from Warehouse.act.fact_activity_other_v
where customer_id in (437782, 778490, 1415280)
order by customer_id, activity_date_c


----------------------

	select abs(datediff(day, activity_date, prev_activity_date)), 
		case when (prev_activity_date is not null and abs(datediff(day, activity_date, prev_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER') then 'REACTIVATED' else customer_status_name_bk end, 
		*
	from Landing.aux.act_fact_activity_sales, 
			Landing.map.act_lapsed_num_days l
	where customer_id_bk in (437782, 778490, 1415280)
	order by customer_id_bk, activity_date

	select *
	from Landing.aux.act_fact_activity_other 
	where customer_id_bk in (437782, 778490, 1415280)