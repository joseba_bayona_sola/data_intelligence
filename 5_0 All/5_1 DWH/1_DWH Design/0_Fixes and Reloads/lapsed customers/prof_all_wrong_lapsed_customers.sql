
select top 1000 *
from Warehouse.act.fact_activity_other_v
where activity_type_name = 'REACTIVATE'
order by customer_id

-- 309198
select top 1000 count(distinct customer_id)
from Warehouse.act.fact_activity_other_v
where activity_type_name = 'REACTIVATE'

select top 1000 t.customer_id, oh.customer_status_name, count(*) over ()
from
		(select distinct customer_id
		from Warehouse.act.fact_activity_other_v
		where activity_type_name = 'REACTIVATE') t
	left join
		Warehouse.sales.dim_order_header_v oh on t.customer_id = oh.customer_id and oh.customer_status_name = 'REACTIVATED'
-- where oh.customer_id is not null
where oh.customer_id is null

