
			select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_other_wrk ot
				inner join
					Warehouse.act.fact_activity_other ott on ot.idCustomer_sk_fk = ott.idCustomer_sk_fk
				inner join
					Warehouse.act.dim_activity_type a on ott.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE')
			except
			select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_other_wrk ot
				inner join
					Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE')

			select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_sales_wrk ot
				inner join
					Warehouse.act.fact_activity_other ott on ot.idCustomer_sk_fk = ott.idCustomer_sk_fk
				inner join
					Warehouse.act.dim_activity_type a on ott.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE')
			except
			select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_other_wrk ot
				inner join
					Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE')


			select *
			from Warehouse.gen.dim_customer_v
			where idCustomer_sk = 189978
1001958


	select abs(datediff(day, activity_date, prev_activity_date)), 
		case when (prev_activity_date is not null and abs(datediff(day, activity_date, prev_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER') then 'REACTIVATED' else customer_status_name_bk end, 
		*
	from Landing.aux.act_fact_activity_sales, 
			Landing.map.act_lapsed_num_days l
	where customer_id_bk in (516825)
	order by customer_id_bk, activity_date

	select *
	from Landing.aux.act_fact_activity_other 
	where customer_id_bk in (516825)

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in (437782, 778490, 1415280)

select *
from Warehouse.act.fact_customer_signature_scd_v
where customer_id in (437782, 778490, 1415280)


select *
from Warehouse.sales.dim_order_header_v
where customer_id in (516825)
order by customer_id, order_date

select *
from Warehouse.act.fact_activity_sales_v
where customer_id in (516825)
order by customer_id, activity_date_c

select *
from Warehouse.act.fact_activity_other_v
where customer_id in (516825)
order by customer_id, activity_date_c

						select idCustomer_sk_fk, t.activity_type_name, a.r_imp, activity_date, 
							count(*) over (partition by idCustomer_sk_fk) num_rep_activities,
							dense_rank() over (partition by idCustomer_sk_fk order by activity_date, a.r_imp) ord_activities, 
							count(*) over (partition by idCustomer_sk_fk, activity_date) num_rep_activities_2
						from 
								(select idCustomer_sk_fk, activity_type_name, 
									case when activity_type_name = 'REGISTER' then min(activity_date) over (partition by idCustomer_sk_fk) else activity_date end activity_date
								from 
										(select idCustomer_sk_fk, activity_type_name, activity_date
										from Warehouse.act.fact_activity_other_full_v
										where activity_type_name in ('REGISTER', 'LAPSED')
										union
										select idCustomer_sk_fk, customer_status_name activity_type_name, activity_date
										from Warehouse.act.fact_customer_signature_aux_orders) t) t
							inner join
								(select 'REGISTER' activity_type_name, 1 r_imp
								union
								select 'NEW' activity_type_name, 2 r_imp
								union
								select 'LAPSED' activity_type_name, 3 r_imp
								union
								select 'REACTIVATED' activity_type_name, 4 r_imp
								union
								select 'REGULAR' activity_type_name, 5 r_imp) a on t.activity_type_name = a.activity_type_name
					where idCustomer_sk_fk in (619798, 880920, 1347204)
					order by idCustomer_sk_fk, activity_date, a.r_imp