
select top 1000 *
from Warehouse.act.fact_customer_signature_v
where customer_id = 23947

select top 1000 *
from Warehouse.act.fact_customer_product_signature_v
where customer_id = 23947


select top 1000 *
from Warehouse.act.fact_activity_sales_v
where customer_id = 23947

select top 1000 idActivitySales_sk, order_id_bk, activity_date, 
	idCalendarActivityDate_sk_fk, idCalendarPrevActivityDate_sk_fk, prev_order_freq_time,
	datediff(dd, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), idCalendarPrevActivityDate_sk_fk), 112), 126), activity_date) prev_order_freq_time_2,
	idCalendarNextActivityDate_sk_fk, next_order_freq_time,
	datediff(dd, activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), idCalendarNextActivityDate_sk_fk), 112), 126)) next_order_freq_time_2
from Warehouse.act.fact_activity_sales
where idCustomer_sk_fk = 20

-----------------------------------------------------------------------

update Warehouse.act.fact_activity_sales
set prev_order_freq_time = datediff(dd, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), idCalendarPrevActivityDate_sk_fk), 112), 126), activity_date)	
