
-- Reason for Bug: I was using wrongly the customer_order_seq_no_web when doing the INSERT on the MERGE SP

-- Backup table done in order to compare
select * 
into DW_GetLenses_jbs.dbo.fact_activity_sales_backup
from Warehouse.act.fact_activity_sales

select top 1000 idActivitySales_sk, order_id_bk, idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no, customer_order_seq_no_gen
from Warehouse.act.fact_activity_sales
where idETLBatchRun_upd = 1948
	and idActivitySales_sk in (8469997, 8428531, 8428548, 8428587, 8428651, 8428695)
union
select idActivitySales_sk, order_id_bk, idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no, customer_order_seq_no_gen
from DW_GetLenses_jbs.dbo.fact_activity_sales_backup
where idActivitySales_sk in (8469997, 8428531, 8428548, 8428587, 8428651, 8428695)
order by idActivitySales_sk


select idActivitySales_sk, activity_date, order_id_bk, idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no, customer_order_seq_no_gen
from Warehouse.act.fact_activity_sales
where idCustomer_sk_fk = 1331533
order by activity_date

select idActivitySales_sk, activity_date, order_id_bk, idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no, customer_order_seq_no_gen
from DW_GetLenses_jbs.dbo.fact_activity_sales_backup
where idCustomer_sk_fk = 1331533
order by activity_date

---------------------------------------------

select idCustomer_sk_fk, customer_order_seq_no_gen, count(*)
from Warehouse.act.fact_activity_sales
where idActivityType_sk_fk <> 1
group by idCustomer_sk_fk, customer_order_seq_no_gen
having count(*) > 1
order by count(*) desc, idCustomer_sk_fk, customer_order_seq_no_gen

select ac.idActivitySales_sk, ac.activity_date, ac.order_id_bk, ac.idCustomer_sk_fk, ac.idActivityType_sk_fk, 
	ac.customer_order_seq_no, ac.customer_order_seq_no_web, ac.customer_order_seq_no_gen, ac.ins_ts, ac.upd_ts
from
		(select idCustomer_sk_fk, customer_order_seq_no_gen
		from Warehouse.act.fact_activity_sales
		where idActivityType_sk_fk <> 1
		group by idCustomer_sk_fk, customer_order_seq_no_gen
		having count(*) > 1) t
	inner join
		Warehouse.act.fact_activity_sales ac on t.idCustomer_sk_fk = ac.idCustomer_sk_fk and t.customer_order_seq_no_gen = ac.customer_order_seq_no_gen
order by ac.idCustomer_sk_fk, ac.activity_date

-------------------------------------------

select top 1000 idActivitySales_sk, activity_date, order_id_bk, idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, ins_ts, upd_ts
from Warehouse.act.fact_activity_sales
where idCustomer_sk_fk = 848
order by activity_date

select top 1000 *
from Warehouse.act.fact_customer_signature_v
where idCustomer_sk_fk = 848

select top 1000 *
from Warehouse.act.fact_customer_product_signature_v
where idCustomer_sk_fk = 848

select top 1000 *
from Landing.aux.act_fact_activity_sales
where customer_id_bk = 446659
order by activity_date

select top 1000 *
from Staging.act.fact_activity_sales
where customer_id_bk = 446659
order by activity_date

select top 1000 *
from Warehouse.act.fact_activity_sales_wrk
where idCustomer_sk_fk = 848
order by activity_date

select top 1000 *
from Warehouse.sales.dim_order_header
where idCustomer_sk_fk = 848
order by order_date