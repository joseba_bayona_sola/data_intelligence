/*
Duplicated in the excel:
Bausch and Lomb Ultra
Acuvue Vita
*/

select *
from DW_GetLenses_jbs.dbo.summary_pf_seg
where product_family_name in ('Acuvue Vita', 'Bausch and Lomb Ultra', 'everclear AIR')

	select segment, count(*)
	from DW_GetLenses_jbs.dbo.summary_pf_seg
	group by segment
	order by segment

	select segment, product_family_group_name, count(*)
	from DW_GetLenses_jbs.dbo.summary_pf_seg
	group by segment, product_family_group_name
	order by segment, product_family_group_name

	select product_family_name, count(*)
	from DW_GetLenses_jbs.dbo.summary_pf_seg
	group by product_family_name
	having count(*) > 1
	order by product_family_name

select *
from Warehouse.prod.dim_product_family_group

select *
from Warehouse.prod.dim_product_family
where product_id_bk in (3170, 2692)

select *
from Landing.map.prod_product_family_group_aud

select *
from Landing.map.prod_product_family_group_pf_aud

-----------------------------------

select	
	pf.product_id_magento, pf.product_family_name, pf.product_family_group_name
-- into DW_GetLenses_jbs.dbo.product_family_group_backup
from Warehouse.prod.dim_product_family_v pf



select	
	pf.product_id_magento, pf.product_family_name, aux.product_family_name, 
	pf.category_name, pf.product_family_group_name, aux.product_family_group_name, 
	aux.segment
from 
		Warehouse.prod.dim_product_family_v pf
	full join
		DW_GetLenses_jbs.dbo.summary_pf_seg aux on  pf.product_family_name = aux.product_family_name
-- where aux.product_family_name is null
-- where aux.product_family_name is null and pf.product_family_group_name is not null
-- where pf.product_family_name is null
-- where pf.product_family_group_name <> aux.product_family_group_name
where pf.product_family_group_name is null and aux.product_family_group_name is not null
order by pf.product_id_magento

select	
	pf.product_id_magento, pf.product_family_name, aux.product_family_name, 
	pf.category_name, pf.product_family_group_name, aux.product_family_group_name, 
	aux.segment
-- select pf.product_id_magento, pf.product_family_name, aux.segment
from 
		Warehouse.prod.dim_product_family_v pf
	full join
		DW_GetLenses_jbs.dbo.summary_pf_seg aux on  pf.product_family_name = aux.product_family_name
-- where aux.segment is not null 
-- where aux.segment is not null and pf.product_family_group_name <> aux.product_family_group_name order by aux.segment, pf.product_family_group_name, pf.product_id_magento
where aux.segment is not null and (pf.product_family_group_name <> aux.segment or pf.product_family_group_name is null) order by aux.segment, pf.product_family_group_name, pf.product_id_magento
-- where aux.product_family_name in ('Acuvue Vita', 'Bausch and Lomb Ultra', 'everclear AIR')
order by aux.segment, pf.product_id_magento

-----------------------------------

select	
	pf.product_id_magento, pf.product_family_name, 
	pf.category_name, pf.product_family_group_name, aux.product_family_group_name
from 
		Warehouse.prod.dim_product_family_v pf
	inner join
		DW_GetLenses_jbs.dbo.product_family_group_backup aux on  pf.product_id_magento = aux.product_id_magento
-- where aux.product_family_group_name is not null and pf.product_family_group_name is null
where aux.product_family_group_name is not null and pf.product_family_group_name = 'Others'