
select count(*)
from Landing.aux.act_customer_cr_reg
where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 

select old_access_cust_no, store_id_bk_cr, migration_date_oacn, count(*)
from Landing.aux.act_customer_cr_reg
where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 
group by old_access_cust_no, store_id_bk_cr, migration_date_oacn

select top 1000 t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
	case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year, 
	count(*) over ()
from 
		(select customer_id_bk, store_id_bk_cr, migration_date_oacn
		from Landing.aux.act_customer_cr_reg
		where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
	left join
		Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk
where oh.customer_id_bk is null
-- where oh.order_date > dateadd(year, -1, t.migration_date_oacn)
order by t.customer_id_bk, oh.order_date			

select customer_id_bk, count(num_mig_orders_last_year) num_mig_orders_last_year
from
	(select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
		case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
	from 
			(select customer_id_bk, store_id_bk_cr, migration_date_oacn
			from Landing.aux.act_customer_cr_reg
			where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
		left join
			Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk) t
group by customer_id_bk
having count(num_mig_orders_last_year) >=2
order by customer_id_bk
