
select count(*)
from #act_customer_cr_reg
where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 


select old_access_cust_no, store_id_bk_cr, migration_date_oacn, count(*)
from #act_customer_cr_reg
where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 
group by old_access_cust_no, store_id_bk_cr, migration_date_oacn

select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
	case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
from 
		(select customer_id_bk, store_id_bk_cr, migration_date_oacn
		from #act_customer_cr_reg
		where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
	left join
		Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk
-- where oh.customer_id_bk is null
-- where oh.order_date > dateadd(year, -1, t.migration_date_oacn)
order by t.customer_id_bk, oh.order_date			

select customer_id_bk, count(num_mig_orders_last_year) num_mig_orders_last_year
from
	(select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
		case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
	from 
			(select customer_id_bk, store_id_bk_cr, migration_date_oacn
			from #act_customer_cr_reg
			where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
		left join
			Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk) t
group by customer_id_bk
order by customer_id_bk

--------------------------------------------------------------
		
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, count(num_mig_orders_last_year) num_mig_orders_last_year
			from
				(select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
					case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
				from 
						(select customer_id_bk, store_id_bk_cr, migration_date_oacn
						from #act_customer_cr_reg
						where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk) t
			group by customer_id_bk) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set trg.num_mig_orders_last_year = src.num_mig_orders_last_year;

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'MIGRATION'
		where num_mig_orders_last_year >=2

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'ORGANIC'
		where num_mig_orders_last_year in (0, 1) or num_mig_orders_last_year is null

---------------------------------

select *
from #act_customer_cr_reg
where customer_origin_name_bk = 'MIGRATION'
order by num_mig_orders_last_year










select old_access_cust_no, store_id_bk_cr, migration_date_oacn, count(*)
from Landing.aux.act_customer_cr_reg
where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 
group by old_access_cust_no, store_id_bk_cr, migration_date_oacn


select *
from Landing.aux.sales_dim_order_header_aud oh 
where customer_id_bk = 70840 and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	and store_id_bk = -101


