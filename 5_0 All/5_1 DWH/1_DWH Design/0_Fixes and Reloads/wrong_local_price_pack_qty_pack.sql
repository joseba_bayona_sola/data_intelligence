
	select idETLBatchRun, codETLBatchType, codFlowType, idPackage, flow_name, package_name,
		description, dateFrom, dateTo, 
		startTime, finishTime, duration, runStatus, -- message,
		ins_ts
	from ControlDB.logging.t_ETLBatchRun_v
	where idETLBatchRun in (1283, 1446)

select order_line_id_bk, order_id_bk, order_no, order_date, 
	store_name, customer_id, order_status_name,
	product_id_magento, product_family_name, 
	qty_unit, qty_pack, pfps.size,
	local_price_unit, local_price_pack, local_subtotal, 
	idETLBatchRun_ins, idETLBatchRun_upd
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on ol.product_id_magento = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
where ol.store_name = 'visiondirect.es'
	-- and ol.product_id_magento = 1099
	-- and ol.order_date > '2017-01-01'
	-- and ol.local_price_pack < 9
	and ol.qty_unit / pfps.size <> ol.qty_pack
order by ol.product_id_magento, order_date

select order_line_id_bk, order_id_bk, order_no, order_date, 
	store_name, customer_id, order_status_name,
	product_id_magento, product_family_name, 
	qty_unit, qty_pack, pfps.size,
	local_price_unit, local_price_pack, local_subtotal, 
	idETLBatchRun_ins, idETLBatchRun_upd
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on ol.product_id_magento = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
where ol.website_group = 'VisionDirect'
	and ol.qty_unit / pfps.size <> ol.qty_pack
order by ol.product_id_magento, ol.store_name, order_date

select distinct order_id_bk
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on ol.product_id_magento = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
where ol.website_group = 'VisionDirect'
	and ol.qty_unit / pfps.size <> ol.qty_pack
