
select oh.entity_id
from 
		Landing.mag.sales_flat_order_aud oh
	inner join	 
		Landing.map.sales_summer_time_period_aud stp on year(oh.created_at) = stp.year and 
			oh.created_at between stp.from_date and stp.to_date
union
select ih.order_id
from 
		Landing.mag.sales_flat_invoice_aud ih
	inner join	 
		Landing.map.sales_summer_time_period_aud stp on year(ih.created_at) = stp.year and 
			ih.created_at between stp.from_date and stp.to_date
union
select sh.order_id
from 
		Landing.mag.sales_flat_shipment_aud sh
	inner join	 
		Landing.map.sales_summer_time_period_aud stp on year(sh.created_at) = stp.year and 
			sh.created_at between stp.from_date and stp.to_date
union
select ch.order_id
from 
		Landing.aux.mag_sales_flat_creditmemo ch
	inner join	 
		Landing.map.sales_summer_time_period_aud stp on year(ch.created_at) = stp.year and 
			ch.created_at between stp.from_date and stp.to_date

