
		drop table #sales_order_header_o_i_s_cr

		select oh.order_id_bk, 
			i.entity_id invoice_id, s.entity_id shipment_id, cr.entity_id creditmemo_id, 
			o.increment_id order_no, i.increment_id invoice_no, s.increment_id shipment_no, cr.increment_id creditmemo_no, 
			CONVERT(INT, (CONVERT(VARCHAR(8), o.created_at, 112))) idCalendarOrderDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, o.created_at)), 2) + ':' + case when (DATEPART(MINUTE, o.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeOrderDate, 
			o.created_at order_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), i.created_at, 112))) idCalendarInvoiceDate, i.created_at invoice_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), s.created_at, 112))) idCalendarShipmentDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, s.created_at)), 2) + ':' + case when (DATEPART(MINUTE, s.created_at) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
			s.created_at shipment_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), cr.created_at, 112))) idCalendarRefundDate, cr.created_at refund_date
		into #sales_order_header_o_i_s_cr
		from 
				Landing.aux.sales_order_header_o oh
			inner join
				(select entity_id, increment_id, -- created_at, 
					case when (o.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, o.created_at) else o.created_at end created_at
				from 
						Landing.mag.sales_flat_order_aud o
					left join	 
						Landing.map.sales_summer_time_period_aud stp on year(o.created_at) = stp.year) o on oh.order_id_bk = o.entity_id
			left join
				(select i.order_id, i.entity_id, i.increment_id, -- i.created_at, 
					case when (i.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, i.created_at) else i.created_at end created_at
				from	
					(select order_id, entity_id, increment_id, created_at
					from
						(select i.order_id, i.entity_id, i.increment_id, i.created_at, 
							rank() over (partition by i.order_id order by i.entity_id) rank_inv
						from 
								Landing.aux.sales_order_header_o oh
							inner join
								Landing.mag.sales_flat_invoice_aud i on oh.order_id_bk = i.order_id) t
					where rank_inv = 1) i
				left join	 
					Landing.map.sales_summer_time_period_aud stp on year(i.created_at) = stp.year) i on oh.order_id_bk = i.order_id
			left join
				(select s.order_id, s.entity_id, s.increment_id, -- s.created_at, 
					case when (s.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, s.created_at) else s.created_at end created_at
				from
					(select order_id, entity_id, increment_id, created_at
					from
						(select s.order_id, s.entity_id, s.increment_id, s.created_at, 
							rank() over (partition by s.order_id order by s.entity_id) rank_ship
						from 
								Landing.aux.sales_order_header_o oh
							inner join
								Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id) t
					where rank_ship = 1) s
				left join	 
					Landing.map.sales_summer_time_period_aud stp on year(s.created_at) = stp.year) s on oh.order_id_bk = s.order_id
			left join
				(select r.order_id, r.entity_id, r.increment_id, -- r.created_at,
					case when (r.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, r.created_at) else r.created_at end created_at
				from 
					(select order_id, entity_id, increment_id, created_at
					from
						(select order_id, entity_id, increment_id, created_at,
							dense_rank() over (partition by order_id order by num_order) cr_rank
						from Landing.aux.mag_sales_flat_creditmemo) t
					where cr_rank = 1) r
				left join	 
					Landing.map.sales_summer_time_period_aud stp on year(r.created_at) = stp.year) cr on oh.order_id_bk = cr.order_id
		order by oh.order_id_bk desc


		select t1.*
		from 
				Landing.aux.sales_order_header_o_i_s_cr t1
			inner join
				#sales_order_header_o_i_s_cr t2 on t1.order_id_bk = t2.order_id_bk
		where t1.order_date <> t2.order_date or t1.invoice_date <> t2.invoice_date or t1.shipment_date <> t2.shipment_date or t1.refund_date <> t2.refund_date

-------------------------------------


		select 
 			oi.item_id order_line_id_bk, oh.order_id_bk, 
			ii.invoice_id, si.shipment_id, cri.entity_id creditmemo_id,
			oh.order_no, ii.invoice_no, si.shipment_no, cri.increment_id creditmemo_no,
			ii.invoice_line_id, si.shipment_line_id, cri.item_id creditmemo_line_id,
			si.num_shipment_lines, cri.num_rep_rows num_creditmemo_lines,
			ii.idCalendarInvoiceDate, ii.invoice_date, 
			CONVERT(INT, (CONVERT(VARCHAR(8), si.shipment_date, 112))) idCalendarShipmentDate, 
			RIGHT('0' + CONVERT(VARCHAR(2), DATEPART(HOUR, si.shipment_date)), 2) + ':' + case when (DATEPART(MINUTE, si.shipment_date) between 1 and 29) then '00' else '30' end + ':00' idTimeShipmentDate, 
			si.shipment_date,
			CONVERT(INT, (CONVERT(VARCHAR(8), cri.created_at, 112))) idCalendarRefundDate, cri.created_at refund_date, 
			1 warehouse_id_bk
		into #sales_order_line_o_i_s_cr
		from 
				Landing.aux.sales_order_header_o_i_s_cr oh
			inner join
				Landing.mag.sales_flat_order_item_aud oi on oh.order_id_bk = oi.order_id
			left join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
			left join
				(select oh.order_id_bk, oh.invoice_id, oh.invoice_no, 
					ii.entity_id invoice_line_id, ii.order_item_id,
					oh.idCalendarInvoiceDate, oh.invoice_date
				from 
						Landing.aux.sales_order_header_o_i_s_cr oh
					inner join
						Landing.mag.sales_flat_invoice_aud i on oh.invoice_id = i.entity_id
					inner join
						Landing.mag.sales_flat_invoice_item_aud ii on i.entity_id = ii.parent_id) ii on oh.order_id_bk = ii.order_id_bk and oi.item_id = ii.order_item_id
			left join		
				(select s.order_id_bk, s.shipment_id, s.shipment_no, 
					s.shipment_line_id, s.order_item_id, 
					s.num_shipment_lines,
					-- s.shipment_date, 
					case when (s.shipment_date between stp.from_date and stp.to_date) then dateadd(hour, 1, s.shipment_date) else s.shipment_date end shipment_date
				from
						(select order_id_bk, shipment_id, shipment_no, 
							shipment_line_id, order_item_id, 
							num_shipment_lines,
							shipment_date
						from
							(select oh.order_id_bk, 
								s.entity_id shipment_id, s.increment_id shipment_no, 
								si.entity_id shipment_line_id, si.order_item_id,
								s.created_at shipment_date,
								count(*) over (partition by si.order_item_id) num_shipment_lines,
								rank() over (partition by si.order_item_id order by s.entity_id) rank_s
							from 
									Landing.aux.sales_order_header_o_i_s_cr oh
								inner join
									Landing.mag.sales_flat_shipment_aud s on oh.order_id_bk = s.order_id
								inner join
									Landing.mag.sales_flat_shipment_item_aud si on s.entity_id = si.parent_id) s --where oh.order_id_bk = 5275010
						where rank_s = 1) s 
					left join	 
						Landing.map.sales_summer_time_period_aud stp on year(s.shipment_date) = stp.year) si on oh.order_id_bk = si.order_id_bk and oi.item_id = si.order_item_id
			left join
				(select order_id, order_item_id, entity_id, increment_id, item_id, num_rep_rows, -- created_at,
					case when (cri.created_at between stp.from_date and stp.to_date) then dateadd(hour, 1, cri.created_at) else cri.created_at end created_at
				from 
						Landing.aux.mag_sales_flat_creditmemo_item cri
					left join
						Landing.map.sales_summer_time_period_aud stp on year(cri.created_at) = stp.year) cri on oh.order_id_bk = cri.order_id and oi.item_id = cri.order_item_id
		where ep.product_id is null
		order by oh.order_id_bk, oi.item_id 

		select t1.*
		from 
				Landing.aux.sales_order_line_o_i_s_cr t1
			inner join
				#sales_order_line_o_i_s_cr t2 on t1.order_line_id_bk = t2.order_line_id_bk
		where t1.invoice_date <> t2.invoice_date or t1.shipment_date <> t2.shipment_date or t1.refund_date <> t2.refund_date
