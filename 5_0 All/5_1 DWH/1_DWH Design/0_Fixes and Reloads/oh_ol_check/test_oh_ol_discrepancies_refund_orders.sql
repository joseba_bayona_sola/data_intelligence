
drop table #oh_ol_disc_oh
drop table #oh_ol_disc_ol

select idOrderHeader_sk, order_id_bk, order_no, order_date, invoice_date, refund_date, website, order_status_name, 
	local_subtotal, local_total_inc_vat, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
into #oh_ol_disc_oh
from Warehouse.sales.dim_order_header_v
-- where invoice_date between '2014-01-01' and '2015-01-01'
-- where invoice_date between '2015-01-01' and '2016-01-01'
-- where invoice_date between '2016-01-01' and '2017-01-01'
-- where invoice_date between '2017-01-01' and '2018-01-01'
where invoice_date between '2018-01-01' and '2019-01-01'
	and website like 'visiondirect%'

	select website, year(invoice_date), month(invoice_date), sum(local_total_aft_refund_inc_vat)
	from #oh_ol_disc_oh
	group by website, year(invoice_date), month(invoice_date)
	order by website, year(invoice_date), month(invoice_date)

select ol.order_id_bk, ol.order_no, ol.invoice_date, oh.website,
	ol.local_subtotal, ol.local_subtotal_refund, 
	ol.local_store_credit_given, ol.local_bank_online_given, 
	ol.local_total_inc_vat, ol.local_total_aft_refund_inc_vat, 
	ol.order_allocation_rate, ol.order_allocation_rate_aft_refund, ol.order_allocation_rate_refund
into #oh_ol_disc_ol
from 
		Landing.aux.sales_fact_order_line_aud ol
	inner join
		#oh_ol_disc_oh oh on ol.order_id_bk = oh.order_id_bk

	select website, year(invoice_date), month(invoice_date), sum(local_total_aft_refund_inc_vat)
	from #oh_ol_disc_ol
	group by website, year(invoice_date), month(invoice_date)
	order by website, year(invoice_date), month(invoice_date)

--------------------------------------------------------------

select top 1000 oh.order_id_bk, oh.order_no, oh.order_date, oh.invoice_date, oh.refund_date, oh.website, oh.order_status_name, 
	oh.local_total_inc_vat, ol.local_total_inc_vat, abs(oh.local_total_inc_vat - ol.local_total_inc_vat) local_total_inc_vat_diff, sum(abs(oh.local_total_inc_vat - ol.local_total_inc_vat)) over () sum_diff,
	oh.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_inc_vat, 
	abs(oh.local_total_aft_refund_inc_vat - ol.local_total_aft_refund_inc_vat) local_total_af_inc_vat_diff, sum(abs(oh.local_total_aft_refund_inc_vat - ol.local_total_aft_refund_inc_vat)) over () sum_diff_af,
	oh.local_total_refund, 
	oh.local_store_credit_given, ol.local_store_credit_given, oh.local_bank_online_given, ol.local_bank_online_given, 
	ol.order_allocation_rate, ol.order_allocation_rate_aft_refund, ol.order_allocation_rate_refund
from
		#oh_ol_disc_oh oh
	inner join
		(select order_id_bk, 
			sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat,
			sum(local_store_credit_given) local_store_credit_given, sum(local_bank_online_given) local_bank_online_given, 
			sum(ol.order_allocation_rate) order_allocation_rate, sum(ol.order_allocation_rate_aft_refund) order_allocation_rate_aft_refund, sum(ol.order_allocation_rate_refund) order_allocation_rate_refund
		from #oh_ol_disc_ol ol
		group by order_id_bk) ol on oh.order_id_bk = ol.order_id_bk
-- where abs(oh.local_total_inc_vat - ol.local_total_inc_vat) > 0.1
where abs(oh.local_total_aft_refund_inc_vat - ol.local_total_aft_refund_inc_vat) > 1
order by local_total_af_inc_vat_diff desc, oh.order_id_bk
order by oh.order_id_bk

create table DW_GetLenses_jbs.dbo.oh_ol_disc_oh(
	order_id_bk				int, 
	website					varchar(50))

insert into DW_GetLenses_jbs.dbo.oh_ol_disc_oh(order_id_bk, website)
	select oh.order_id_bk, oh.website
	from
			#oh_ol_disc_oh oh
		inner join
			(select order_id_bk, 
				sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat,
				sum(local_store_credit_given) local_store_credit_given, sum(local_bank_online_given) local_bank_online_given, 
				sum(ol.order_allocation_rate) order_allocation_rate, sum(ol.order_allocation_rate_aft_refund) order_allocation_rate_aft_refund, sum(ol.order_allocation_rate_refund) order_allocation_rate_refund
			from #oh_ol_disc_ol ol
			group by order_id_bk) ol on oh.order_id_bk = ol.order_id_bk
	where abs(oh.local_total_aft_refund_inc_vat - ol.local_total_aft_refund_inc_vat) > 1


select *
from DW_GetLenses_jbs.dbo.oh_ol_disc_oh

select oh.order_id_bk, oh.order_no, oh.order_date, oh.invoice_date, oh.refund_date, oh.website, oh.order_status_name, oh.customer_id, oh.customer_email,
	oh.local_total_inc_vat, ol.local_total_inc_vat, oh.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_inc_vat,
	oh.local_total_refund, 
	oh.local_store_credit_given, ol.local_store_credit_given, 
	oh.local_bank_online_given, ol.local_bank_online_given, 
	ol.order_allocation_rate, ol.order_allocation_rate_aft_refund, ol.order_allocation_rate_refund
from 
		DW_GetLenses_jbs.dbo.oh_ol_disc_oh ohd
	inner join
		Warehouse.sales.dim_order_header_v oh on ohd.order_id_bk = oh.order_id_bk
	inner join
		(select order_id_bk, 
			sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat,
			sum(local_store_credit_given) local_store_credit_given, sum(local_bank_online_given) local_bank_online_given, 
			sum(ol.order_allocation_rate) order_allocation_rate, sum(ol.order_allocation_rate_aft_refund) order_allocation_rate_aft_refund, sum(ol.order_allocation_rate_refund) order_allocation_rate_refund
		from Landing.aux.sales_fact_order_line_aud ol
		group by order_id_bk) ol on oh.order_id_bk = ol.order_id_bk
where year(oh.invoice_date) <> 2014
order by oh.order_id_bk
