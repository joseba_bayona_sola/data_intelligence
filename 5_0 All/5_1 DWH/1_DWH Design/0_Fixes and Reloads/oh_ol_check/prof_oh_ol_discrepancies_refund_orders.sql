
select top 1000 order_id_bk, invoice_date, website, local_total_inc_vat, local_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where order_id_bk between 1510701 and 1511801
	and website = 'visiondirect.co.uk'
order by order_id_bk

select top 1000 order_id_bk, invoice_date, website, 
	sum(local_total_inc_vat) local_total_inc_vat, 
	sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat 
from Warehouse.sales.fact_order_line_v
where order_id_bk between 1510701 and 1511801
	and website = 'visiondirect.co.uk'
group by order_id_bk, invoice_date, website
order by order_id_bk

select oh.order_id_bk, oh.invoice_date, oh.website, 
	oh.local_total_inc_vat, ol.local_total_inc_vat,
	oh.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_inc_vat
from
		(select order_id_bk, invoice_date, website, local_total_inc_vat, local_total_aft_refund_inc_vat
		from Warehouse.sales.dim_order_header_v
		where order_id_bk between 1510701 and 1511801
			and website = 'visiondirect.co.uk') oh
	inner join
		(select order_id_bk, invoice_date, website, 
			sum(local_total_inc_vat) local_total_inc_vat, 
			sum(local_total_aft_refund_inc_vat) local_total_aft_refund_inc_vat 
		from Warehouse.sales.fact_order_line_v
		where order_id_bk between 1510701 and 1511801
			and website = 'visiondirect.co.uk'
		group by order_id_bk, invoice_date, website) ol on oh.order_id_bk = ol.order_id_bk
where oh.local_total_aft_refund_inc_vat <> ol.local_total_aft_refund_inc_vat

select idOrderHeader_sk, order_id_bk, order_no, order_date, refund_date, website, order_status_name, 
	local_subtotal, local_total_inc_vat, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where order_id_bk = 1510878

select order_id_bk, order_no, order_date, refund_date, website, order_status_name, 
	product_id_magento, product_family_name,
	local_subtotal, local_total_inc_vat, 
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.fact_order_line_v
where order_id_bk = 1510878


select order_id_bk, order_no, invoice_date,
	local_subtotal, local_subtotal_refund, 
	local_store_credit_given, local_bank_online_given, 
	local_total_inc_vat, local_total_aft_refund_inc_vat, 
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
from Landing.aux.sales_fact_order_line_aud
where order_id_bk = 1510878

-- o.local_bank_online_given * ol.order_allocation_rate_refund local_bank_online_given, 