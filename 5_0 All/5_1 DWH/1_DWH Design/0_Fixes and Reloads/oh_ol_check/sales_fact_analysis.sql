
select order_id_bk, order_no, invoice_no, invoice_date, 
	website, countries_registered_code, 
	order_status_name, refund_date,
	customer_id, 
	local_total_aft_refund_inc_vat local_total_inc_vat, local_total_aft_refund_exc_vat local_total_exc_vat, 
	local_total_aft_refund_vat local_total_vat, local_total_aft_refund_prof_fee local_total_prof_fee, 
	global_total_aft_refund_inc_vat global_total_inc_vat, global_total_aft_refund_exc_vat global_total_exc_vat, 
	global_total_aft_refund_vat global_total_vat, global_total_aft_refund_prof_fee global_total_prof_fee 
into DW_GetLenses_jbs.dbo.oh
from Warehouse.sales.dim_order_header_v
where invoice_date between '2018-03-01' and '2018-04-01'

select order_id_bk, order_no, invoice_no, invoice_date, 
	website, countries_registered_code, 
	order_status_name, line_status_name, refund_date,
	customer_id, 
	local_total_aft_refund_inc_vat local_total_inc_vat, local_total_aft_refund_exc_vat local_total_exc_vat, 
	local_total_aft_refund_vat local_total_vat, local_total_aft_refund_prof_fee local_total_prof_fee, 
	global_total_aft_refund_inc_vat global_total_inc_vat, global_total_aft_refund_exc_vat global_total_exc_vat, 
	global_total_aft_refund_vat global_total_vat, global_total_aft_refund_prof_fee global_total_prof_fee 
into DW_GetLenses_jbs.dbo.ol
from Warehouse.sales.fact_order_line_v
where invoice_date between '2018-03-01' and '2018-04-01'

select order_id_bk, order_no, invoice_no, invoice_date, 
	website, countries_registered_code, 
	order_status_name, line_status_name, 
	customer_id, 
	local_total_inc_vat, local_total_exc_vat, 
	local_total_vat, local_total_prof_fee, 
	global_total_inc_vat, global_total_exc_vat, 
	global_total_vat, global_total_prof_fee 
into DW_GetLenses_jbs.dbo.ol2
from Warehouse.sales.fact_order_line_inv_v
where invoice_date between '2018-03-01' and '2018-04-01'


----------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.oh

select top 1000 *
from DW_GetLenses_jbs.dbo.ol

select top 1000 *
from DW_GetLenses_jbs.dbo.ol2

---------------------------------------------------------

select top 1000 website, count(*), count(distinct order_id_bk), 
	sum(global_total_exc_vat) global_total_exc_vat
from DW_GetLenses_jbs.dbo.oh
group by website
order by website

select top 1000 website, count(*), count(distinct order_id_bk), 
	sum(global_total_exc_vat) global_total_exc_vat
from DW_GetLenses_jbs.dbo.ol 
group by website
order by website

select top 1000 website, count(*), count(distinct order_id_bk), 
	sum(global_total_exc_vat) global_total_exc_vat
from DW_GetLenses_jbs.dbo.ol2
group by website
order by website


select top 1000 order_id_bk, order_no, invoice_no, invoice_date, website, order_status_name, global_total_exc_vat
from DW_GetLenses_jbs.dbo.oh
where order_id_bk in (6955131, 6981410)
order by order_id_bk

select top 1000 order_id_bk, order_no, invoice_no, invoice_date, website, order_status_name, global_total_exc_vat, 
	sum(global_total_exc_vat) over (partition by order_id_bk)
from DW_GetLenses_jbs.dbo.ol
where order_id_bk in (6955131, 6981410)
order by order_id_bk

select top 1000 order_id_bk, order_no, invoice_no, invoice_date, website, order_status_name, global_total_exc_vat,
	sum(global_total_exc_vat) over (partition by order_id_bk)
from DW_GetLenses_jbs.dbo.ol2
where order_id_bk in (6955131, 6981410)
order by order_id_bk


---------------------------------------------------------

--  Overall COMP
select
	oh.num_orders, ol.num_orders, ol2.num_orders, 
	oh.global_total_exc_vat, ol.global_total_exc_vat, ol2.global_total_exc_vat, 
	oh.global_total_exc_vat - ol.global_total_exc_vat, 
	oh.global_total_exc_vat - ol2.global_total_exc_vat
from
	(select count(distinct order_id_bk) num_orders, sum(global_total_exc_vat) global_total_exc_vat
	from DW_GetLenses_jbs.dbo.oh) oh,
	(select count(distinct order_id_bk) num_orders, sum(global_total_exc_vat) global_total_exc_vat
	from DW_GetLenses_jbs.dbo.ol) ol,
	(select count(distinct order_id_bk) num_orders, sum(global_total_exc_vat) global_total_exc_vat
	from DW_GetLenses_jbs.dbo.ol2) ol2

select
	oh.website,
	oh.num_orders, ol.num_orders, ol2.num_orders, 
	oh.global_total_exc_vat, ol.global_total_exc_vat, ol2.global_total_exc_vat, 
	oh.global_total_exc_vat - ol.global_total_exc_vat, 
	oh.global_total_exc_vat - ol2.global_total_exc_vat
from
		(select website, count(distinct order_id_bk) num_orders, 
			sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.oh
		group by website) oh
	inner join
		(select website, count(distinct order_id_bk) num_orders, 
			sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.ol 
		group by website) ol on oh.website = ol.website
	inner join
		(select website, count(distinct order_id_bk) num_orders, 
			sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.ol2
		group by website) ol2 on oh.website = ol2.website
order by oh.website

----------------------------------------------------------

-- OH - OL COMP

select top 1000 
	case when (oh.order_id_bk is not null) then oh.order_id_bk else ol.order_id_bk end order_id_bk, 
	case when (oh.order_id_bk is not null) then oh.website else ol.website end website, 
	case when (oh.order_id_bk is not null) then oh.invoice_date else ol.invoice_date end invoice_date, 
	oh.global_total_exc_vat, ol.global_total_exc_vat
from
		(select order_id_bk, website, invoice_date, global_total_exc_vat
		from DW_GetLenses_jbs.dbo.oh) oh
	full join
		(select order_id_bk, website, invoice_date, sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.ol
		group by order_id_bk, website, invoice_date) ol on oh.order_id_bk = ol.order_id_bk
-- where oh.order_id_bk is null
where ol.order_id_bk is null
-- where oh.order_id_bk is not null and ol.order_id_bk is not null and abs(oh.global_total_exc_vat - ol.global_total_exc_vat) > 0.1
order by website, invoice_date, order_id_bk


-- OL - OL2 COMP

select 
	case when (ol.order_id_bk is not null) then ol.order_id_bk else ol2.order_id_bk end order_id_bk, 
	case when (ol.order_id_bk is not null) then ol.website else ol2.website end website, 
	ol.invoice_date invoice_date, 
	ol.global_total_exc_vat, ol2.global_total_exc_vat
from 
		(select order_id_bk, website, invoice_date, sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.ol
		group by order_id_bk, website, invoice_date) ol 
	full join
		(select order_id_bk, website, sum(global_total_exc_vat) global_total_exc_vat
		from DW_GetLenses_jbs.dbo.ol2
		group by order_id_bk, website) ol2 on ol.order_id_bk = ol2.order_id_bk
-- where ol.order_id_bk is null
-- where ol2.order_id_bk is null
where ol.order_id_bk is not null and ol2.order_id_bk is not null and abs(ol.global_total_exc_vat - ol2.global_total_exc_vat) > 0.1
order by website, invoice_date, order_id_bk

------------------------------------------------

select top 1000 order_id_bk, order_no, order_date, invoice_date, website, order_status_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where order_id_bk = 6928069

select top 1000 order_id_bk, order_no, order_date, invoice_date, website, order_status_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.fact_order_line_v
where order_id_bk = 6723285

select order_id_bk, order_no, invoice_date, website, order_status_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat
from Warehouse.sales.fact_order_line_inv_v
where order_id_bk = 7020882
order by invoice_date
