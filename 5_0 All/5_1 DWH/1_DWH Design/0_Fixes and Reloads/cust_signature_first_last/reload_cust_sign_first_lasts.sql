
select 1696978 - 1679984

-- 1696978
select idCustomer_sk_fk, order_id_bk order_id_bk_first, 
	 idChannel_sk_fk idChannelFirst_sk_fk, idMarketingChannel_sk_fk idMarketingChannelFirst_sk_fk, idPriceType_sk_fk idPriceTypeFirst_sk_fk
into #customer_signature_first
from 
	(select order_id_bk, idCustomer_sk_fk, 
		dense_rank() over (partition by idCustomer_sk_fk order by customer_order_seq_no) num_order, 
		count(*) over (partition by idCustomer_sk_fk) num_tot_orders, 
		idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk
	from Warehouse.act.fact_activity_sales acs
	where idActivityType_sk_fk = 7)	t 
where num_order = 1

-- 1696978
select idCustomer_sk_fk, order_id_bk order_id_bk_last
into #customer_signature_last
from 
	(select order_id_bk, idCustomer_sk_fk, 
		dense_rank() over (partition by idCustomer_sk_fk order by customer_order_seq_no) num_order, 
		count(*) over (partition by idCustomer_sk_fk) num_tot_orders, 
		idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk
	from Warehouse.act.fact_activity_sales acs
	where idActivityType_sk_fk = 7)	t 
where num_order = num_tot_orders


select top 1000 f.idCustomer_sk_fk, f.order_id_bk_first, l.order_id_bk_last, 
	f.idChannelFirst_sk_fk, f.idMarketingChannelFirst_sk_fk, f.idPriceTypeFirst_sk_fk
from 
		#customer_signature_first f
	inner join
		#customer_signature_last l on f.idCustomer_sk_fk = l.idCustomer_sk_fk
order by idCustomer_sk_fk

---------------------------------------------------

select top 1000 idCustomer_sk_fk, order_id_bk_first, order_id_bk_last, 
	idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk
from Warehouse.act.fact_customer_signature

	merge into Warehouse.act.fact_customer_signature trg
	using 
		(select f.idCustomer_sk_fk, f.order_id_bk_first, l.order_id_bk_last, 
			f.idChannelFirst_sk_fk, f.idMarketingChannelFirst_sk_fk, f.idPriceTypeFirst_sk_fk
		from 
				#customer_signature_first f
			inner join
				#customer_signature_last l on f.idCustomer_sk_fk = l.idCustomer_sk_fk) src
	on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
	when matched and not exists 
		(select 
			isnull(trg.idChannelFirst_sk_fk, 0), isnull(trg.idMarketingChannelFirst_sk_fk, 0), isnull(trg.idPriceTypeFirst_sk_fk, 0), 
			isnull(trg.order_id_bk_first, 0), isnull(trg.order_id_bk_last, 0)
		intersect	 
		select 
			isnull(src.idChannelFirst_sk_fk, 0), isnull(src.idMarketingChannelFirst_sk_fk, 0), isnull(src.idPriceTypeFirst_sk_fk, 0), 
			isnull(src.order_id_bk_first, 0), isnull(src.order_id_bk_last, 0))
		then 
			update set
				trg.idChannelFirst_sk_fk = src.idChannelFirst_sk_fk, trg.idMarketingChannelFirst_sk_fk = src.idMarketingChannelFirst_sk_fk, trg.idPriceTypeFirst_sk_fk = src.idPriceTypeFirst_sk_fk, 

				trg.order_id_bk_first = src.order_id_bk_first, trg.order_id_bk_last = src.order_id_bk_last; 
								



---------------------------------------------------


drop table #customer_signature_first

drop table #customer_signature_last