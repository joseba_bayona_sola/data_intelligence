

-- wearer
select id, customer_id, name, dob
from magento01.wearer
order by id desc
limit 1000 

select count(*)
from magento01.wearer


-- wearer_prescription_method
select id, method, code
from magento01.wearer_prescription_method

select count(*)
from magento01.wearer_prescription_method


-- wearer_prescription
select id, 
  wearer_id, method_id, 
  date_last_prescription, date_start_prescription, 
  optician_name, optician_address1, optician_address2, optician_city, optician_state, optician_country, optician_postcode, optician_phone, optician_lookup_id, 
  reminder_sent
from magento01.wearer_prescription
order by id desc
limit 1000 

select count(*)
from magento01.wearer_prescription


-- wearer_order_item
select id, 
  order_id, item_id, quote_item_id, 
  prescription_type, 
  wearer_prescription_id, prescription_flag, item_data
from magento01.wearer_order_item
order by id desc
limit 1000 

select count(*)
from magento01.wearer_order_item
