
select top 1000 customer_id, customer_email, num_tot_orders, main_type_oh_name, num_diff_product_type_oh, 
	rank_avg_order_qty_time, avg_order_qty_time, stdev_order_qty_time, 
	rank_avg_order_freq_time, avg_order_freq_time, stdev_order_freq_time
from Warehouse.act.fact_customer_signature_v
where customer_id = 20577

select *
from Warehouse.sales.dim_rank_qty_time

select *
from Warehouse.sales.dim_rank_freq_time

select *
from Warehouse.sales.dim_order_header_v
where customer_id = 20577
order by order_date 

select *
from Warehouse.sales.fact_order_line_v
where customer_id = 20577
order by order_date 

select *
from Warehouse.act.fact_activity_sales_v
where customer_id = 20577
order by activity_date


-----------------------------------------

select stdev_order_qty_time, count(*)
from Warehouse.act.fact_customer_signature_v
group by stdev_order_qty_time
order by stdev_order_qty_time

select stdev_order_freq_time, count(*)
from Warehouse.act.fact_customer_signature_v
group by stdev_order_freq_time
order by stdev_order_freq_time


-----------------------------------------------


select rank_qty_time, max(qty_time_bk) max_qty_time_bk
from Warehouse.sales.dim_rank_qty_time
group by rank_qty_time
order by rank_qty_time

select rank_qty_time, max_qty_time_bk, lead(max_qty_time_bk) over (order by max_qty_time_bk)
from 
	(select rank_qty_time, max(qty_time_bk) max_qty_time_bk
	from Warehouse.sales.dim_rank_qty_time
	group by rank_qty_time) t

select st.stdev_order_qty_time, t.rank_qty_time, t.qty_time_bk, t.qty_time_bk_next, 
	st.num_customers
from
		(select stdev_order_qty_time, count(*) num_customers
		from Warehouse.act.fact_customer_signature_v
		group by stdev_order_qty_time) st
	left join
		(select rank_qty_time, max_qty_time_bk qty_time_bk, lead(max_qty_time_bk) over (order by max_qty_time_bk) qty_time_bk_next
		from 
			(select rank_qty_time, max(qty_time_bk) max_qty_time_bk
			from Warehouse.sales.dim_rank_qty_time
			group by rank_qty_time) t) t on st.stdev_order_qty_time >= t.qty_time_bk and st.stdev_order_qty_time < t.qty_time_bk_next
order by st.stdev_order_qty_time

select rank_qty_time, qty_time_bk, qty_time_bk_next, count(*), sum(num_customers)
from
	(select st.stdev_order_qty_time, t.rank_qty_time, t.qty_time_bk, t.qty_time_bk_next, 
		st.num_customers
	from
			(select stdev_order_qty_time, count(*) num_customers
			from Warehouse.act.fact_customer_signature_v
			group by stdev_order_qty_time) st
		left join
			(select rank_qty_time, max_qty_time_bk qty_time_bk, lead(max_qty_time_bk) over (order by max_qty_time_bk) qty_time_bk_next
			from 
				(select rank_qty_time, max(qty_time_bk) max_qty_time_bk
				from Warehouse.sales.dim_rank_qty_time
				group by rank_qty_time) t) t on st.stdev_order_qty_time >= t.qty_time_bk and st.stdev_order_qty_time < t.qty_time_bk_next) t
group by rank_qty_time, qty_time_bk, qty_time_bk_next
order by rank_qty_time, qty_time_bk, qty_time_bk_next

---------------------------------------------------------------------

select rank_freq_time, min(freq_time_bk) min_freq_time_bk
from Warehouse.sales.dim_rank_freq_time
group by rank_freq_time
order by rank_freq_time

select rank_freq_time, min_freq_time_bk, isnull(lead(min_freq_time_bk) over (order by min_freq_time_bk), 36000)
from 
	(select rank_freq_time, min(freq_time_bk) min_freq_time_bk
	from Warehouse.sales.dim_rank_freq_time
	group by rank_freq_time) t

select st.stdev_order_freq_time, t.rank_freq_time, t.freq_time_bk, t.freq_time_bk_next, st.num_customers
from
		(select stdev_order_freq_time, count(*) num_customers
		from Warehouse.act.fact_customer_signature_v
		group by stdev_order_freq_time) st
	left join
		(select rank_freq_time, min_freq_time_bk freq_time_bk, isnull(lead(min_freq_time_bk) over (order by min_freq_time_bk), 36000) freq_time_bk_next
		from 
			(select rank_freq_time, min(freq_time_bk) min_freq_time_bk
			from Warehouse.sales.dim_rank_freq_time
			group by rank_freq_time) t) t on st.stdev_order_freq_time >= freq_time_bk and st.stdev_order_freq_time < freq_time_bk_next
order by st.stdev_order_freq_time

select rank_freq_time, freq_time_bk, freq_time_bk_next, count(*), sum(num_customers)
from
	(select st.stdev_order_freq_time, t.rank_freq_time, t.freq_time_bk, t.freq_time_bk_next, st.num_customers
	from
			(select stdev_order_freq_time, count(*) num_customers
			from Warehouse.act.fact_customer_signature_v
			group by stdev_order_freq_time) st
		left join
			(select rank_freq_time, min_freq_time_bk freq_time_bk, isnull(lead(min_freq_time_bk) over (order by min_freq_time_bk), 36000) freq_time_bk_next
			from 
				(select rank_freq_time, min(freq_time_bk) min_freq_time_bk
				from Warehouse.sales.dim_rank_freq_time
				group by rank_freq_time) t) t on st.stdev_order_freq_time >= freq_time_bk and st.stdev_order_freq_time < freq_time_bk_next) t
group by rank_freq_time, freq_time_bk, freq_time_bk_next
order by rank_freq_time, freq_time_bk, freq_time_bk_next
