
select order_id_bk, order_no, order_date, website, order_status_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat, 
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where local_total_aft_refund_inc_vat = 0 AND local_store_credit_used = 0 and order_date > '2018-01-01' -- 2152
-- where local_total_aft_refund_inc_vat = 0 AND local_store_credit_used <> 0 and order_date > '2018-01-01'-- 11137
-- where local_total_aft_refund_inc_vat < 0 and order_date > '2018-01-01' -- 261
order by order_date desc

select top 1000 order_id_bk, order_no, order_date, website, order_status_name, 
	customer_id, customer_status_name, customer_order_seq_no, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, local_total_inc_vat,
	local_store_credit_given, local_bank_online_given, 
	local_total_aft_refund_inc_vat
from Warehouse.sales.dim_order_header_v
where customer_id = 1489609
order by order_date desc