
select top 1000 *
from Landing.aux.act_fact_activity_sales
where activity_type_name_bk = 'REFUND'
order by activity_date desc

select top 1000 customer_id_bk, count(*), count(distinct activity_type_name_bk)
from Landing.aux.act_fact_activity_sales
group by customer_id_bk
order by count(distinct activity_type_name_bk) desc, count(*) desc

select top 1000 *
from Landing.aux.act_fact_activity_sales
where customer_id_bk in (1074794, 1559595)
order by customer_id_bk, activity_type_name_bk, activity_date

---------------------

			select order_id_bk, 
				idCalendarActivityDate, activity_date, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) customer_order_seq_no, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk, s.website_group order by activity_date, order_id_bk) customer_order_seq_no_web 
			from 
					Landing.aux.act_fact_activity_sales acs
				inner join
					Landing.aux.mag_gen_store_v s on acs.store_id_bk = s.store_id
			where customer_id_bk in (1074794, 1559595)
			order by customer_id_bk, activity_type_name_bk, activity_date

			select order_id_bk, activity_date, activity_type_name_bk, customer_id_bk, 
				customer_order_seq_no, 
				dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) customer_order_seq_no_gen 
			from Landing.aux.act_fact_activity_sales
			where activity_type_name_bk in ('ORDER', 'REFUND')
				and customer_id_bk in (1074794, 1559595)
			-- order by customer_id_bk, activity_type_name_bk, activity_date
			order by customer_id_bk, activity_date

			select *
			from Landing.aux.sales_dim_order_header_aud
			where customer_id_bk in (1074794, 1559595)
				and local_total_aft_refund_inc_vat <= 0
			order by customer_id_bk, order_date
