
-- NEED TO HAVE Warehouse.act.fact_customer_signature_wrk WITH ALL CUSTOMERS

drop table Warehouse.act.fact_customer_signature_temp

select *
into Warehouse.act.fact_customer_signature_temp
from Warehouse.act.fact_customer_signature

select *
from Warehouse.act.fact_customer_signature_temp
where idETLBatchRun_upd = 1
-- order by upd_ts, idCustomer_sk_fk
order by idCalendarLastOrderDate_sk_fk, idCustomer_sk_fk

	merge into Warehouse.act.fact_customer_signature_temp with (tablock) as trg
	using Warehouse.act.fact_customer_signature_wrk src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk)
	when matched and not exists 
		(select 			
			isnull(trg.idStoreCreate_sk_fk, 0), isnull(trg.idCalendarCreateDate_sk_fk, 0), 
			isnull(trg.idStoreRegister_sk_fk, 0), isnull(trg.idCalendarRegisterDate_sk_fk, 0), 
			isnull(trg.idStoreFirstOrder_sk_fk, 0), isnull(trg.idStoreLastOrder_sk_fk, 0), 
			isnull(trg.idCalendarFirstOrderDate_sk_fk, 0), isnull(trg.idCalendarLastOrderDate_sk_fk, 0), isnull(trg.idCalendarLastLoginDate_sk_fk, 0), 
			
			isnull(trg.num_tot_orders_web, 0), 
			isnull(trg.num_tot_orders, 0), isnull(trg.subtotal_tot_orders, 0), isnull(trg.num_tot_cancel, 0), isnull(trg.subtotal_tot_cancel, 0), isnull(trg.num_tot_refund, 0), isnull(trg.subtotal_tot_refund, 0), 
			isnull(trg.num_tot_lapsed, 0), isnull(trg.num_tot_reactivate, 0), 
			isnull(trg.num_tot_discount_orders, 0), isnull(trg.discount_tot_value, 0), isnull(trg.num_tot_store_credit_orders, 0), isnull(trg.store_credit_tot_value, 0), 
			isnull(trg.num_tot_emails, 0), isnull(trg.num_tot_text_messages, 0), isnull(trg.num_tot_reviews, 0), 
			
			isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), isnull(trg.idCalendarStatusUpdateDate_sk_fk, 0), 
			
			isnull(trg.idProductTypeOHMain_sk_fk, 0), isnull(trg.num_diff_product_type_oh, 0), 
			isnull(trg.main_order_qty_time, 0), isnull(trg.min_order_qty_time, 0), isnull(trg.avg_order_qty_time, 0), isnull(trg.max_order_qty_time, 0), isnull(trg.stdev_order_qty_time, 0), 
			isnull(trg.main_order_freq_time, 0), isnull(trg.min_order_freq_time, 0), isnull(trg.avg_order_freq_time, 0), isnull(trg.max_order_freq_time, 0), isnull(trg.stdev_order_freq_time, 0), 
			isnull(trg.idChannelMain_sk_fk, 0), isnull(trg.idMarketingChannelMain_sk_fk, 0), -- 
			isnull(trg.idPriceTypeMain_sk_fk, 0), 
			
			isnull(trg.first_discount_amount, 0), isnull(trg.first_subtotal_amount, 0), isnull(trg.last_subtotal_amount, 0), 
			isnull(trg.local_to_global_rate, 0),  isnull(trg.order_currency_code, '')

		intersect
		select 
			isnull(src.idStoreCreate_sk_fk, 0), isnull(src.idCalendarCreateDate_sk_fk, 0), 
			isnull(src.idStoreRegister_sk_fk, 0), isnull(src.idCalendarRegisterDate_sk_fk, 0), 
			isnull(src.idStoreFirstOrder_sk_fk, 0), isnull(src.idStoreLastOrder_sk_fk, 0), 
			isnull(src.idCalendarFirstOrderDate_sk_fk, 0), isnull(src.idCalendarLastOrderDate_sk_fk, 0), isnull(src.idCalendarLastLoginDate_sk_fk, 0), 
			
			isnull(src.num_tot_orders_web, 0), 
			isnull(src.num_tot_orders, 0), isnull(src.subtotal_tot_orders, 0), isnull(src.num_tot_cancel, 0), isnull(src.subtotal_tot_cancel, 0), isnull(src.num_tot_refund, 0), isnull(src.subtotal_tot_refund, 0), 
			isnull(src.num_tot_lapsed, 0), isnull(src.num_tot_reactivate, 0), 
			isnull(src.num_tot_discount_orders, 0), isnull(src.discount_tot_value, 0), isnull(src.num_tot_store_credit_orders, 0), isnull(src.store_credit_tot_value, 0), 
			isnull(src.num_tot_emails, 0), isnull(src.num_tot_text_messages, 0), isnull(src.num_tot_reviews, 0), 
			
			isnull(src.idCustomerStatusLifecycle_sk_fk, 0), isnull(src.idCalendarStatusUpdateDate_sk_fk, 0), 
			
			isnull(src.idProductTypeOHMain_sk_fk, 0), isnull(src.num_diff_product_type_oh, 0), 
			isnull(src.main_order_qty_time, 0), isnull(src.min_order_qty_time, 0), isnull(src.avg_order_qty_time, 0), isnull(src.max_order_qty_time, 0), isnull(src.stdev_order_qty_time, 0), 
			isnull(src.main_order_freq_time, 0), isnull(src.min_order_freq_time, 0), isnull(src.avg_order_freq_time, 0), isnull(src.max_order_freq_time, 0), isnull(src.stdev_order_freq_time, 0), 
			isnull(src.idChannelMain_sk_fk, 0), isnull(src.idMarketingChannelMain_sk_fk, 0), -- 
			isnull(src.idPriceTypeMain_sk_fk, 0), 
			
			isnull(src.first_discount_amount, 0), isnull(src.first_subtotal_amount, 0), isnull(src.last_subtotal_amount, 0), 
			isnull(src.local_to_global_rate, 0),  isnull(src.order_currency_code, ''))
		then 
			update set
				trg.idStoreCreate_sk_fk = src.idStoreCreate_sk_fk, trg.idCalendarCreateDate_sk_fk = src.idCalendarCreateDate_sk_fk, 
				trg.idStoreRegister_sk_fk = src.idStoreRegister_sk_fk, trg.idCalendarRegisterDate_sk_fk = src.idCalendarRegisterDate_sk_fk, 
				trg.idStoreFirstOrder_sk_fk = src.idStoreFirstOrder_sk_fk, trg.idStoreLastOrder_sk_fk = src.idStoreLastOrder_sk_fk, 
				trg.idCalendarFirstOrderDate_sk_fk = src.idCalendarFirstOrderDate_sk_fk, trg.idCalendarLastOrderDate_sk_fk = src.idCalendarLastOrderDate_sk_fk, trg.idCalendarLastLoginDate_sk_fk = src.idCalendarLastLoginDate_sk_fk, 

				trg.num_tot_orders_web = src.num_tot_orders_web, 
				trg.num_tot_orders = src.num_tot_orders, trg.subtotal_tot_orders = src.subtotal_tot_orders, 
				trg.num_tot_cancel = src.num_tot_cancel, trg.subtotal_tot_cancel = src.subtotal_tot_cancel, 
				trg.num_tot_refund = src.num_tot_refund, trg.subtotal_tot_refund = src.subtotal_tot_refund, 
				trg.num_tot_lapsed = src.num_tot_lapsed, trg.num_tot_reactivate = src.num_tot_reactivate, 
				trg.num_tot_discount_orders = src.num_tot_discount_orders, trg.discount_tot_value = src.discount_tot_value, 
				trg.num_tot_store_credit_orders = src.num_tot_store_credit_orders, trg.store_credit_tot_value = src.store_credit_tot_value, 
				trg.num_tot_emails = src.num_tot_emails, trg.num_tot_text_messages = src.num_tot_text_messages, trg.num_tot_reviews = src.num_tot_reviews, 

				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, trg.idCalendarStatusUpdateDate_sk_fk = src.idCalendarStatusUpdateDate_sk_fk, 

				trg.idProductTypeOHMain_sk_fk = src.idProductTypeOHMain_sk_fk, trg.num_diff_product_type_oh = src.num_diff_product_type_oh, 
				trg.main_order_qty_time = src.main_order_qty_time, trg.min_order_qty_time = src.min_order_qty_time, 
				trg.avg_order_qty_time = src.avg_order_qty_time, trg.max_order_qty_time = src.max_order_qty_time, trg.stdev_order_qty_time = src.stdev_order_qty_time, 
				trg.main_order_freq_time = src.main_order_freq_time, trg.min_order_freq_time = src.min_order_freq_time, 
				trg.avg_order_freq_time = src.avg_order_freq_time, trg.max_order_freq_time = src.max_order_freq_time, trg.stdev_order_freq_time = src.stdev_order_freq_time, 
				trg.idChannelMain_sk_fk = src.idChannelMain_sk_fk, trg.idMarketingChannelMain_sk_fk = src.idMarketingChannelMain_sk_fk, trg.idPriceTypeMain_sk_fk = src.idPriceTypeMain_sk_fk, 

				trg.first_discount_amount = src.first_discount_amount, trg.first_subtotal_amount = src.first_subtotal_amount, trg.last_subtotal_amount = src.last_subtotal_amount, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 

				trg.idETLBatchRun_upd = 1;
