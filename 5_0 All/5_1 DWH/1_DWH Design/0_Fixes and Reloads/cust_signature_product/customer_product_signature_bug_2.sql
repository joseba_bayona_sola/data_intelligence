
drop table #wrong_cps

select top 1000 customer_id, customer_email, customer_status_name, website_register, register_date, 
	num_tot_orders_customer, num_dist_products, 
	product_id_magento, product_family_name, 
	num_tot_orders_product, 
	first_order_date, last_order_date
into #wrong_cps
from Warehouse.tableau.fact_customer_product_signature_v
where num_tot_orders_customer = 0
order by customer_id

	select *
	from #wrong_cps
	order by first_order_date

	-- 246
	select distinct customer_id, num_tot_orders_customer
	from #wrong_cps

	select website_register, count(*)
	from #wrong_cps
	group by website_register
	order by website_register

	select customer_status_name, count(*)
	from #wrong_cps
	group by customer_status_name
	order by customer_status_name

	delete from Warehouse.act.fact_customer_product_signature
	where idCustomer_sk_fk in 
		(select c.idCustomer_sk
		from
				(select distinct customer_id
				from #wrong_cps) cps 
			inner join
				Warehouse.gen.dim_customer_v c on cps.customer_id = c.customer_id_bk)

	---------------------------------------------

select oh.order_id_bk, oh.order_no, oh.order_date, oh.refund_date, oh.website, oh.customer_id, cps.num_tot_orders_customer,
	oh.order_status_name, oh.customer_order_seq_no_gen, 
	oh2.ins_ts, oh2.upd_ts
from 
		(select distinct customer_id, num_tot_orders_customer
		from #wrong_cps) cps
	inner join
		Warehouse.sales.dim_order_header_v oh on cps.customer_id = oh.customer_id
	inner join
		Warehouse.sales.dim_order_header oh2 on oh.order_id_bk = oh2.order_id_bk
order by oh.customer_id, oh.order_date

------------------------------------------------------

	select oh.order_id_bk, oh.order_date, oh.idCalendarOrderDate, oh.customer_id_bk
	into #oh
	from 
			(select distinct customer_id_bk
			from Landing.aux.sales_dim_order_header) c
		inner join 
			Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')

	select ol.order_line_id_bk, ol.order_id_bk, oh.order_date, oh.customer_id_bk, 
		ol.product_id_bk, ol.local_subtotal, ol.local_to_global_rate, 
		count(*) over (partition by customer_id_bk, product_id_bk) num_lines,
		rank() over (partition by customer_id_bk, product_id_bk order by order_date, order_line_id_bk) ord_lines
	into #ol
	from 
			#oh oh
		inner join 
			Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk

	select t1.customer_id_bk, t1.product_id_bk, 
		t1.idCalendarFirstOrderDate, t1.idCalendarLastOrderDate, 
		t2.order_id_bk_first, t3.order_id_bk_last, 
		t1.num_tot_orders, t1.subtotal_tot_orders
	from
			(select c.customer_id_bk, ol.product_id_bk, 
				min(oh.idCalendarOrderDate) idCalendarFirstOrderDate, max(oh.idCalendarOrderDate) idCalendarLastOrderDate, 
				count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders
			from
					(select distinct customer_id_bk
					from Landing.aux.sales_dim_order_header) c
				inner join
					#oh oh on c.customer_id_bk = oh.customer_id_bk -- and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				inner join
					#ol ol on oh.order_id_bk = ol.order_id_bk
			group by c.customer_id_bk, ol.product_id_bk) t1
		inner join
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_first
			from #ol
			where ord_lines = 1) t2 on t1.customer_id_bk = t2.customer_id_bk and t1.product_id_bk = t2.product_id_bk
		inner join	
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_last
			from #ol
			where ord_lines = num_lines) t3 on t1.customer_id_bk = t3.customer_id_bk and t1.product_id_bk = t3.product_id_bk

	select c.customer_id_bk, -1 product_id_bk, 
		0 idCalendarFirstOrderDate, 0 idCalendarLastOrderDate, 
		0 order_id_bk_first, 0 order_id_bk_last, 
		0 num_tot_orders, 0 subtotal_tot_orders
	from 
			(select distinct customer_id_bk
			from Landing.aux.sales_dim_order_header) c
		left join
			#oh oh on c.customer_id_bk = oh.customer_id_bk
	where oh.customer_id_bk is null