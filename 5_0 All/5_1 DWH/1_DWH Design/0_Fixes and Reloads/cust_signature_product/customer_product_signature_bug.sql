
-- 2170158
select top 1000 count(*)
from Warehouse.act.dim_customer_signature_v

-- 2171248
select top 1000 count(*)
from Warehouse.act.fact_customer_signature_v

select top 1000 idCustomer_sk_fk, count(distinct num_dist_products) c_num_dist_products
from Warehouse.act.fact_customer_product_signature
group by idCustomer_sk_fk
having count(distinct num_dist_products) > 1
order by idCustomer_sk_fk

select cps.*
from 
		Warehouse.act.fact_customer_product_signature cps
	inner join
		(select top 1000 idCustomer_sk_fk, count(distinct num_dist_products) c_num_dist_products
		from Warehouse.act.fact_customer_product_signature
		group by idCustomer_sk_fk
		having count(distinct num_dist_products) > 1) t on cps.idCustomer_sk_fk = t.idCustomer_sk_fk
order by cps.idCustomer_sk_fk, cps.num_dist_products, cps.idProductFamily_sk_fk

select cps.*
from 
		Warehouse.act.fact_customer_product_signature_v cps
	inner join
		(select top 1000 idCustomer_sk_fk, count(distinct num_dist_products) c_num_dist_products
		from Warehouse.act.fact_customer_product_signature
		group by idCustomer_sk_fk
		having count(distinct num_dist_products) > 1) t on cps.idCustomer_sk_fk = t.idCustomer_sk_fk
order by cps.customer_id, cps.num_dist_products, cps.product_id_magento

select *
from Warehouse.act.fact_customer_product_signature_v
where idCustomer_sk_fk = 44085

select *
from Warehouse.sales.dim_order_header_v
where customer_id = 1672016

select *
from Warehouse.sales.fact_order_line_v
where customer_id = 1672016
