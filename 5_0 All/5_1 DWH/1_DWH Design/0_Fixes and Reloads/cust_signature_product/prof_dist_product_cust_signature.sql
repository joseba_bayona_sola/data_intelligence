
select count(*)
from Warehouse.gen.dim_customer

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in (2456768, 2456781)

select *
from Warehouse.act.dim_customer_signature_v
where customer_id in (2456768, 2456781)

	select *
	from Warehouse.act.dim_customer_signature_v
	where idETLBatchRun_ins = 1836 and num_tot_orders <> 0
	order by customer_id

	select *
	from Warehouse.act.dim_customer_signature_v
	where num_tot_orders <> 0 and num_dist_products is null
	order by customer_id

select *
from Warehouse.act.fact_customer_product_signature_v
where customer_id in (2456768, 2456781)

select *
from Warehouse.act.fact_customer_product_signature
where idCustomer_sk_fk in (2175870, 2175908)


select *
from Warehouse.sales.fact_order_line_v
where customer_id = 2443595


		select cs.idCustomer_sk_fk,
			cs.customer_id, cs.customer_email, c.first_name, c.last_name,
			cs.company_name_create, cs.website_group_create, cs.website_create, cs.create_date, cs.create_time_mm,
			cs.website_register, cs.register_date, 
			cs.customer_origin_name,
			cs.market_name, c.country_code_s country_code_ship, 
			c.customer_unsubscribe_name,
			cs.customer_status_name, cs.rank_num_tot_orders, cs.num_tot_orders, cs.subtotal_tot_orders, cps.num_dist_products,
			cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.rank_main_order_qty_time, cs.main_order_qty_time, cs.rank_avg_order_freq_time, cs.avg_order_freq_time
		from 
				Warehouse.act.fact_customer_signature_v cs
			inner join
				Warehouse.gen.dim_customer_scd_v c on cs.idCustomer_sk_fk = c.idCustomer_sk_fk
			left join
				(select distinct idCustomer_sk_fk, num_dist_products
				from Warehouse.act.fact_customer_product_signature_v) cps on cs.idCustomer_sk_fk = cps.idCustomer_sk_fk
		where cs.customer_id = 2443595

select top 1000 *
from Warehouse.tableau.fact_customer_signature_v
