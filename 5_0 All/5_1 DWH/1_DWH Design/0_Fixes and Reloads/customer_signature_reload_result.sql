
select top 1000 idETLBatchRun_ins, count(*)
from Warehouse.act.fact_customer_signature_scd
group by idETLBatchRun_ins


select *
from Warehouse.act.fact_customer_signature_scd_v
where idCustomer_sk_fk in 
	(select top 1000 idCustomer_sk_fk
	from Warehouse.act.fact_customer_signature_scd
	where idETLBatchRun_ins = 1635)
order by customer_id, dateFrom

select top 1000 *
from Landing.aux.act_customer_cr_reg
where customer_id_bk = 1258637

select *
from Warehouse.sales.dim_order_header_v
where customer_id = 1258637
order by order_date

select *
from Warehouse.sales.fact_order_line_v
where customer_id = 1258637
order by order_date


-- 

select order_id_bk, 
from Warehouse.act.fact_activity_sales_v
where idETLBatchRun_upd = 1635
order by order_id_bk

select order_id_bk, channel_name, marketing_channel_name, 
	idETLBatchRun_ins, idETLBatchRun_upd
from Warehouse.sales.dim_order_header_v
where order_id_bk in 
	(select order_id_bk
	from Warehouse.act.fact_activity_sales_v
	where idETLBatchRun_upd = 1635)
order by order_id_bk

select top 1000 order_id_bk, channel_name_bk, marketing_channel_name_bk, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.aux.sales_dim_order_header_aud
where order_id_bk in 
	(select order_id_bk
	from Warehouse.act.fact_activity_sales_v
	where idETLBatchRun_upd = 1635)
order by order_id_bk