

select top 1000 store_id_bk, market_id_bk, count(*)
from Landing.aux.sales_dim_order_header_aud
group by store_id_bk, market_id_bk
order by store_id_bk, market_id_bk

merge into Landing.aux.sales_dim_order_header_aud trg
using 
	(select order_id_bk, market_id_bk
	from
		(select order_id_bk, store_id_bk, country_id_shipping_bk, 
			market_id_bk market_id_bk_old,  
					case 
						when smr1.market_id is not null then smr1.market_id 
						when smr2.market_id is not null then smr2.market_id
						when smr3.market_id is not null then smr3.market_id
					end market_id_bk		
		from	
				 Landing.aux.sales_dim_order_header_aud oh
			left join
				Landing.map.gen_store_market_rel smr1 on oh.store_id_bk = 20 and oh.store_id_bk = smr1.store_id and oh.country_id_shipping_bk = smr1.country		
			left join
				Landing.map.gen_store_market_rel smr2 on oh.store_id_bk <> 20 and oh.store_id_bk = smr2.store_id
			left join
				Landing.map.gen_store_market_rel smr3 on smr1.market_id is null and smr2.market_id is null and oh.store_id_bk = smr3.store_id and smr3.country is null and oh.country_id_shipping_bk is not null) t
	where market_id_bk is not null) src
on trg.order_id_bk = src.order_id_bk
when matched and isnull(trg.market_id_bk, 0) <> isnull(src.market_id_bk, 0) then
	update 
		set trg.market_id_bk = src.market_id_bk;


merge into Warehouse.sales.dim_order_header trg
using
	(select order_id_bk, oh.market_id_bk, m.idMarket_sk
	from 
			Landing.aux.sales_dim_order_header_aud oh
		inner join
			Warehouse.gen.dim_market m on oh.market_id_bk = m.market_id_bk
	where oh.market_id_bk is not null) src 
on src.order_id_bk = trg.order_id_bk
when matched and isnull(trg.idMarket_sk_fk, 0) <> isnull(src.idMarket_sk, 0) then
	update 
		set trg.idMarket_sk_fk = src.idMarket_sk;




