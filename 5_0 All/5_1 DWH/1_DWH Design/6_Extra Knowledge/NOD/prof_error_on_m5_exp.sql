





	-- Model 5



		select idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, 
			--1 / (1 + exp(-(0.116859 + (create_time_mm_LB10_z * -0.692795) + (customer_status_name_NEW_z * -0.932611) 
			--	+ (rank_num_tot_orders_z * 0.130753) + (num_tot_orders__0Pt975_2Pt92_z * -0.258725) 
			--	+ (consumption_index_all_orders_N_z * 0.783715) + (est_nmtno_all_orders * -0.416232)))) prediction_renewal, 
			-(0.116859 + (create_time_mm_LB10_z * -0.692795) + (customer_status_name_NEW_z * -0.932611) 
				+ (rank_num_tot_orders_z * 0.130753) + (num_tot_orders__0Pt975_2Pt92_z * -0.258725) 
				+ (consumption_index_all_orders_N_z * 0.783715) + (est_nmtno_all_orders * -0.416232)) exp_number, 
				(create_time_mm_LB10_z * -0.692795), (customer_status_name_NEW_z * -0.932611), 
				(rank_num_tot_orders_z * 0.130753), (num_tot_orders__0Pt975_2Pt92_z * -0.258725), 
				(consumption_index_all_orders_N_z * 0.783715), (est_nmtno_all_orders * -0.416232), est_nmtno_all_orders
		-- into #fact_customer_signature_nod_score_m5
		from #fact_customer_signature_nod_score_prep_m5
		order by exp_number desc
		-- order by idCustomer_sk_fk

		select * -- idCustomerSignatureNOD_sk, idCustomer_sk_fk, idOrderHeader_sk_fk, order_date, est_nmtno_all_orders
		from #fact_customer_signature_nod_score_prep
		order by idCustomer_sk_fk

		select *
		from Warehouse.nod.fact_customer_signature_nod_v
		where prediction_date = convert(date, getutcdate())
		order by idCustomer_sk_fk

		select top 1000 *
		from Warehouse.nod.fact_customer_product_signature_nod
		order by idCustomer_sk_fk, idProductFamily_sk_fk

		select *
		from Warehouse.prod.dim_product_family_v
		order by product_id_magento

		select *
		from Landing.aux.mag_prod_product_family_v
		order by product_id_bk

		select *
		from Landing.aux.prod_product_category

		select *
		from Landing.aux.prod_product_product_type_vat

----------------------------------------------


		select order_id_bk, idCustomer_sk_fk,
			idStore_sk_fk, idCalendarActivityDate_sk_fk, activity_date, 
			num_tot_orders_web,
			num_order, num_tot_orders, subtotal_tot_orders, 
			num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
			idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, order_qty_time_cl, order_qty_time_ncl,
			idChannel_sk_fk,idMarketingChannel_sk_fk, channel_name, idPriceType_sk_fk, price_type_name, 
			customer_status_name, customer_order_seq_no,
			global_discount, global_subtotal, 
			local_to_global_rate, order_currency_code
		from Warehouse.nod.fact_customer_signature_nod_aux_orders
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
			idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, idCalendarLastLoginDate_sk_fk, 
			idCustomerOrigin_sk_fk,
			num_tot_lapsed, num_tot_reactivate,
			num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,
			idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk
		from Warehouse.nod.fact_customer_signature_nod_aux_other
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
			idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
			num_tot_orders_web, num_tot_orders_gen, 
			num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund, 
			num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
			idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 
			order_id_bk_first, order_id_bk_last,	
			first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
			local_to_global_rate, order_currency_code
		from Warehouse.nod.fact_customer_signature_nod_aux_sales
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
			main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time 
		from Warehouse.nod.fact_customer_signature_nod_aux_sales_main_product
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			num_dist_products,
			all_history_spend, all_history_product_count, 

			spend_CLDaily, spend_CLTwoWeeklies, spend_CLMonthlies, spend_SL, spend_EC, spend_Other, 
			spend_pct_CLDaily, spend_pct_CLTwoWeeklies, spend_pct_CLMonthlies, spend_pct_SL, spend_pct_EC, spend_pct_Other, 
			orders_CLDaily, orders_CLTwoWeeklies, orders_CLMonthlies, orders_SL, orders_EC, orders_Other, 
			orders_pct_CLDaily, orders_pct_CLTwoWeeklies, orders_pct_CLMonthlies, orders_pct_SL, orders_pct_EC, orders_pct_Other, 

			avg_unit_price_paid_CLD, avg_unit_price_paid_CL2W, avg_unit_price_paid_CLM, avg_unit_price_paid_SL, avg_unit_price_paid_EC, avg_unit_price_paid_Oth, 
			rank_pct_aupp_CLD, rank_pct_aupp_CL2W, rank_pct_aupp_CLM, rank_pct_aupp_SL, rank_pct_aupp_EC, rank_pct_aupp_Oth 
		from Warehouse.nod.fact_customer_signature_nod_aux_sales_main_category
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time, 
			order_est_consumption_time, 
			consumption_index_all_orders, consumption_index_no_lenses, consumption_index_lenses_only,
			consumption_index_all_orders_stddev, consumption_index_no_lenses_stddev, consumption_index_lenses_only_stddev,
			consumption_index_all_orders_N, consumption_index_no_lenses_N, consumption_index_lenses_only_N,
			consumption_index_all_orders_CV, consumption_index_no_lenses_CV, consumption_index_lenses_only_CV,
			est_nmtno_all_orders, est_nmtno_no_lenses, est_nmtno_lenses_only,
			curr_order_type
		from Warehouse.nod.fact_customer_signature_nod_aux_sales_main_freq
		where idCustomer_sk_fk = 2

		select idCustomer_sk_fk, 
			ct_Affiliates, ct_CustomerService, ct_DirectEntry, ct_Email, ct_HistCH, ct_MigrationCH, ct_NonGA, ct_OrganicSearch, ct_Other, ct_PaidSearch, ct_SMS,
			pct_Affiliates, pct_CustomerService, pct_DirectEntry, pct_Email, pct_HistCH, pct_MigrationCH, pct_NonGA, pct_OrganicSearch, pct_Other, pct_PaidSearch, pct_SMS,
		
			avg_days_to_shipping, num_discounted_orders, num_reorder_orders, 
	
			pm_pct_card_new, pm_pct_card_stored, pm_pct_ideal, pm_pct_klarna, pm_pct_other, pm_pct_paypal
		from Warehouse.nod.fact_customer_signature_nod_aux_sales_main_other
	where idCustomer_sk_fk = 2
