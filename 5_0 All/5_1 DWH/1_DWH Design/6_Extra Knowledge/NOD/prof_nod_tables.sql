
-- Key Dates: 
	-- First Load of orders: 2019-05-10
		select top 1000 convert(date, ins_ts), count(*)
		from Warehouse.nod.fact_customer_signature_nod_score
		group by convert(date, ins_ts)
		order by convert(date, ins_ts)

	-- New SSIS Deploy: 2019-09-05
		select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
			spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
			spm.ins_ts	
		from 
				ControlDB.logging.t_SPRunMessage_v spm
			inner join
				ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
		where spm.sp_name = 'dwh_dwh_merge_nod_customer_signature_nod' --  lnd_stg_get_stock_wh_stock_item - lnd_stg_get_stock_wh_stock_item_batch - lnd_stg_get_stock_intransit_stock_item_batch
		order by spm.finishTime desc, spm.messageTime

-----------------------------------------------

select top 1000 *
from Warehouse.sales.dim_order_header_v
where customer_id = 1456443
order by order_date


	select top 1000 *
	from Warehouse.nod.fact_customer_signature_nod_v
	where idCustomer_sk_fk in (11399)
	order by idCustomer_sk_fk, order_date

select top 1000 *
from Warehouse.nod.fact_customer_signature_nod_score
where idCustomer_sk_fk = 11399
order by order_date

	select top 1000 *
	from Warehouse.nod.fact_customer_signature_nod_score_v
	where idCustomer_sk_fk in (11399)
	order by idCustomer_sk_fk, order_date

-- Overall Numbers

	select top 1000 count(*), count(distinct idOrderHeader_sk_fk)
	from Warehouse.nod.fact_customer_signature_nod

	select top 1000 count(*), count(distinct idOrderHeader_sk_fk)
	from Warehouse.nod.fact_customer_signature_nod_score

	select top 1000 idCustomer_sk_fk, order_date, count(*)
	from Warehouse.nod.fact_customer_signature_nod_score
	group by idCustomer_sk_fk, order_date
	order by count(*) desc, idCustomer_sk_fk, order_date

-- Check Refunded Orders ?? // Need to delete them from fact_customer_signature_nod, fact_customer_signature_nod_score + update evaluation on fact_customer_signature_nod_score

	-- Should not be CANCEL - REFUND
		-- CANCEL: Forget to filter when first reload? // order_id_bk_last can be CANCEL (Should not - in Act using fact_customer_signature_aux_orders) 
		-- REFUND: Not deleted after SSIS deployment on 2019-09-05
	select top 1000 oh.order_status_name, oh.invoice_date, oh.refund_date, csns.*
	from 
			Warehouse.nod.fact_customer_signature_nod_score csns
		inner join
			Warehouse.sales.dim_order_header_v oh on csns.idOrderHeader_sk_fk = oh.idOrderHeader_sk
	where oh.order_status_name not in ('OK', 'PARTIAL REFUND') order by oh.order_date
	-- where csns.idCustomer_sk_fk in (1, 8942) order by csns.idCustomer_sk_fk, oh.order_date
	

-- Check OK Orders // Need to add then to fact_customer_signature_nod, fact_customer_signature_nod_score + update evaluation on fact_customer_signature_nod_score

		drop table #oh 

		select 
			idCustomer_sk_fk, idOrderHeader_sk_fk, 
			order_date, invoice_date, shipment_date,
			customer_order_seq_no, 
			prediction_date, current_f
		into #oh
		from
			(select oh.idCustomer_sk_fk, oh.idOrderHeader_sk idOrderHeader_sk_fk, 
				convert(date, oh.order_date) order_date, oh.invoice_date invoice_date, convert(date, oh.shipment_date) shipment_date,
				oh.customer_order_seq_no, 
				convert(date, GETUTCDATE()) prediction_date, 'Y' current_f, 
				count(*) over (partition by oh.idCustomer_sk_fk, convert(date, oh.order_date)) num_rep, 
				count(*) over (partition by oh.idCustomer_sk_fk, convert(date, oh.order_date)) num_rep_days,
				dense_rank() over (partition by oh.idCustomer_sk_fk, convert(date, oh.order_date) order by oh.order_date, oh.order_id_bk) ord_rep, 
				csn.idCustomerSignatureNOD_sk
			from 
					Warehouse.sales.dim_order_header oh 
				inner join
					Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
				left join
					Warehouse.nod.fact_customer_signature_nod csn on oh.idCustomer_sk_fk = csn.idCustomer_sk_fk and oh.idOrderHeader_sk = csn.idOrderHeader_sk_fk
				left join
					Warehouse.nod.fact_customer_signature_nod csn2 on oh.idCustomer_sk_fk = csn2.idCustomer_sk_fk and convert(date, oh.order_date) = csn2.order_date
			where oh.invoice_date > '2019-08-01'
				and os.order_status_name in ('OK', 'PARTIAL REFUND')
				-- and (csn.idCustomerSignatureNOD_sk is null) -- or isnull(csn.shipment_date, '') <> isnull(convert(date, oh.shipment_date), ''))
				-- and (csn2.idCustomerSignatureNOD_sk is null)
				) oh 
		where num_rep = ord_rep

			select *
			from #oh
			order by invoice_date

			select idCustomer_sk_fk, count(*)
			from #oh
			group by idCustomer_sk_fk
			order by count(*) desc

			select idCustomer_sk_fk, order_date, count(*)
			from #oh
			group by idCustomer_sk_fk, order_date
			order by count(*) desc

			select oh.*, csns.*
			from 
					#oh oh
				left join
					Warehouse.nod.fact_customer_signature_nod_score csns on oh.idOrderHeader_sk_fk = csns.idOrderHeader_sk_fk
			where csns.idCustomerSignatureNODScore_sk is null -- 63460 vs 63474
				-- and oh.invoice_date < '2019-09-04'
			-- where csns.idCustomerSignatureNODScore_sk is not null 
			-- where oh.idCustomer_sk_fk = 11399
			order by oh.order_date

			select oh.idCustomer_sk_fk, count(*)
			from 
					#oh oh
				left join
					Warehouse.nod.fact_customer_signature_nod_score csns on oh.idOrderHeader_sk_fk = csns.idOrderHeader_sk_fk
			where csns.idCustomerSignatureNODScore_sk is null -- 63460 vs 63474
			group by oh.idCustomer_sk_fk
			order by count(*) desc

-- Investigate Customers making 2 orders on same day (Only problems when later one of them refunded)

-- Review Scoring process

	select trg.idCustomer_sk_fk, trg.idOrderHeader_sk_fk, trg.order_date, 
		trg.idOrderHeaderNext_sk_fk, trg.real_renewal, trg.real_next_order_date,
		csn.customer_order_seq_no, 
		csna.customer_order_seq_no, csna.idOrderHeader_sk_fk, csna.order_date
	from 
			Warehouse.nod.fact_customer_signature_nod_score trg
		inner join
			Warehouse.nod.fact_customer_signature_nod csn on trg.idCustomerSignatureNOD_sk_fk = csn.idCustomerSignatureNOD_sk
		left join
			Warehouse.nod.fact_customer_signature_nod csna on trg.idCustomer_sk_fk = csna.idCustomer_sk_fk and csna.customer_order_seq_no -1 = csn.customer_order_seq_no
	where -- csn.idCustomer_sk_fk = 11399 and 
		trg.real_next_order_date is null and csna.order_date is not null
		-- isnull(trg.real_next_order_date, '') <> isnull(csna.order_date, '')
	order by trg.order_date
