
select *
from Landing.aux.sales_dim_order_header

	select order_status_name_bk, count(*)
	from Landing.aux.sales_dim_order_header
	group by order_status_name_bk
	order by order_status_name_bk

select *
from Landing.aux.sales_fact_order_line

select top 1000 *
from Landing.aux.act_fact_activity_sales

select *
from Warehouse.prod.dim_product_family

select *
from Warehouse.prod.dim_product_family

select top 1000 c.customer_id_bk, oh.order_id_bk, oh.order_date,
	oh.order_status_name_bk, 
	ol.order_line_id_bk, ol.product_id_bk, ol.local_subtotal, ol.local_to_global_rate, ol.local_subtotal * ol.local_to_global_rate global_subtotal
from
		(select distinct customer_id_bk
		from Landing.aux.sales_dim_order_header) c
	inner join
		Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
order by c.customer_id_bk, oh.order_id_bk

select c.customer_id_bk, ol.product_id_bk, 
	min(oh.order_date) first_order_date, max(oh.order_date) last_order_date, 
	min(oh.idCalendarOrderDate) first_order_date, max(oh.idCalendarOrderDate) last_order_date, 
	count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders
from
		(select distinct customer_id_bk
		from Landing.aux.sales_dim_order_header) c
	inner join
		Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
group by c.customer_id_bk, ol.product_id_bk
order by c.customer_id_bk, ol.product_id_bk
