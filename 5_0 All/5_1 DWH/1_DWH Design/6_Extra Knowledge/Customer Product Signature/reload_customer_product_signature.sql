
select oh.customer_id_bk, ol.product_id_bk, 
	min(oh.idCalendarOrderDate) first_order_date, max(oh.idCalendarOrderDate) last_order_date, 
	count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders
into #customer_product_signature
from
		Landing.aux.sales_dim_order_header_aud oh 
	inner join
		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
where oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
group by oh.customer_id_bk, ol.product_id_bk

	select count(*) 
	from #customer_product_signature csp

	select count(*) 
	from 
			#customer_product_signature csp
		inner join
			Warehouse.gen.dim_customer c on csp.customer_id_bk = c.customer_id_bk
		inner join
			Warehouse.prod.dim_product_family pf on csp.product_id_bk = pf.product_id_bk


select top 1000 
	c.idCustomer_sk, pf.idProductFamily_sk, 
	csp.first_order_date, csp.last_order_date, 
	csp.num_tot_orders, csp.subtotal_tot_orders
from 
		#customer_product_signature csp
	inner join
		Warehouse.gen.dim_customer c on csp.customer_id_bk = c.customer_id_bk
	inner join
		Warehouse.prod.dim_product_family pf on csp.product_id_bk = pf.product_id_bk


	merge into Warehouse.act.fact_customer_product_signature with (tablock) as trg
	using 
		(select 
			c.idCustomer_sk idCustomer_sk_fk, pf.idProductFamily_sk idProductFamily_sk_fk, 
			csp.first_order_date idCalendarFirstOrderDate_sk_fk, csp.last_order_date idCalendarLastOrderDate_sk_fk, 
			csp.num_tot_orders, csp.subtotal_tot_orders
		from 
				#customer_product_signature csp
			inner join
				Warehouse.gen.dim_customer c on csp.customer_id_bk = c.customer_id_bk
			inner join
				Warehouse.prod.dim_product_family pf on csp.product_id_bk = pf.product_id_bk) src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk and trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk)
	when matched and not exists 
		(select 			
			isnull(trg.idCalendarFirstOrderDate_sk_fk, 0), isnull(trg.idCalendarLastOrderDate_sk_fk, 0), 
			isnull(trg.num_tot_orders, 0), isnull(trg.subtotal_tot_orders, 0)
		intersect
		select 
			isnull(src.idCalendarFirstOrderDate_sk_fk, 0), isnull(src.idCalendarLastOrderDate_sk_fk, 0), 
			isnull(src.num_tot_orders, 0), isnull(src.subtotal_tot_orders, 0))
		then 
			update set
				trg.idCalendarFirstOrderDate_sk_fk = src.idCalendarFirstOrderDate_sk_fk, trg.idCalendarLastOrderDate_sk_fk = src.idCalendarLastOrderDate_sk_fk, 
				trg.num_tot_orders = src.num_tot_orders, trg.subtotal_tot_orders = src.subtotal_tot_orders, 

				trg.idETLBatchRun_upd = 1685, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idCustomer_sk_fk, idProductFamily_sk_fk,
				idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
				num_tot_orders, subtotal_tot_orders,  
				idETLBatchRun_ins)
				
				values (src.idCustomer_sk_fk, src.idProductFamily_sk_fk, 
					src.idCalendarFirstOrderDate_sk_fk, src.idCalendarLastOrderDate_sk_fk, 
					src.num_tot_orders, src.subtotal_tot_orders, 
					1685);

drop table #customer_product_signature


update Warehouse.act.dim_customer_signature_v
set num_dist_products = 0

merge into Warehouse.act.dim_customer_signature_v trg
using
	(select distinct idCustomer_sk_fk, num_dist_products
	from Warehouse.act.fact_customer_product_signature_v) src on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
when matched then
	update set 
		trg.num_dist_products = src.num_dist_products;

