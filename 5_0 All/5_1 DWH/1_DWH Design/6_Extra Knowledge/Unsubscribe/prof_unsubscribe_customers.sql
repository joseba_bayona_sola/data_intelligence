
select *
from Warehouse.act.dim_activity_type_v
order by activity_group_name, activity_type_name

-----------------------------

select top 1000 idCustomerSCD_sk, idCustomer_sk_fk, customer_id_bk, 
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_dotmailer_f, unsubscribe_text_message_f, 
	reminder_f, reminder_type_name
from Warehouse.gen.dim_customer_SCD_v

select top 1000 idCustomerSCD_sk, idCustomer_sk_fk, customer_id_bk, 
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, 
	unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk, 
	unsubscribe_text_message_f, unsubscribe_text_message_d_sk_fk
from Warehouse.gen.dim_customer_SCD
where isCurrent = 'Y'

select unsubscribe_mark_email_magento_f, count(*)
from Warehouse.gen.dim_customer_SCD_v
group by unsubscribe_mark_email_magento_f
order by unsubscribe_mark_email_magento_f

	select unsubscribe_mark_email_magento_f, reminder_f, reminder_type_name, count(*)
	from Warehouse.gen.dim_customer_SCD_v
	group by unsubscribe_mark_email_magento_f, reminder_f, reminder_type_name
	order by unsubscribe_mark_email_magento_f, reminder_f, reminder_type_name

select unsubscribe_mark_email_dotmailer_f, count(*)
from Warehouse.gen.dim_customer_SCD_v
group by unsubscribe_mark_email_dotmailer_f
order by unsubscribe_mark_email_dotmailer_f

select unsubscribe_text_message_f, count(*)
from Warehouse.gen.dim_customer_SCD_v
group by unsubscribe_text_message_f
order by unsubscribe_text_message_f

select reminder_f, reminder_type_name, count(*)
from Warehouse.gen.dim_customer_SCD_v
group by reminder_f, reminder_type_name
order by reminder_f, reminder_type_name
