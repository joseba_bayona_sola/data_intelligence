
select top 1000 *
from Warehouse.act.fact_customer_signature

select top 1000 *
from Warehouse.act.fact_customer_signature_v

select top 1000 customer_status_name, count(*)
from Warehouse.act.fact_customer_signature_v
group by customer_status_name

select top 1000 website_create, count(*)
from Warehouse.act.fact_customer_signature_v
group by website_create

select top 1000 website_register, count(*)
from Warehouse.act.fact_customer_signature_v
group by website_register

select top 1000 website_group_create, website_register, customer_status_name, count(*)
from Warehouse.act.fact_customer_signature_v
group by website_group_create, website_register, customer_status_name
order by website_group_create, website_register, customer_status_name



select top 1000 idCustomer_sk_fk, customer_id, customer_email, customer_status_name, status_update_date
from Warehouse.act.fact_customer_signature_v
order by customer_id desc

---------------------------------------------

select top 1000 *
from Warehouse.gen.dim_customer_SCD_v

select top 1000 unsubscribe_mark_email_magento_f, count(*)
from Warehouse.gen.dim_customer_SCD
group by unsubscribe_mark_email_magento_f
order by unsubscribe_mark_email_magento_f

select top 1000 unsubscribe_mark_email_magento_f, count(*)
from Warehouse.gen.dim_customer_SCD_v
group by unsubscribe_mark_email_magento_f
order by unsubscribe_mark_email_magento_f


---------------------------------------------

select top 50000 idCustomer_sk_fk, customer_id, create_date, getutcdate() current_day, 
	datediff(mm, create_date, getutcdate()) diff_mm
from Warehouse.act.dim_customer_signature_v
order by create_date desc
