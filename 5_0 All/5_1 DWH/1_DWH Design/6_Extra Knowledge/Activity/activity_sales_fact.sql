
select count(*)
from Landing.aux.sales_dim_order_header

select distinct customer_id_bk
from Landing.aux.sales_dim_order_header
order by customer_id_bk

select oh.order_id_bk, 
	oh.idCalendarOrderDate idCalendarActivityDate, oh.order_date activity_date, 
	case when (oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')) THEN 'ORDER' else oh.order_status_name_bk end activity_type_name_bk,
	oh.store_id_bk, oh.customer_id_bk, 
	oh.channel_name_bk, oh.price_type_name_bk, oh.product_type_oh_bk, oh.order_qty_time,
	oh.local_subtotal, oh.local_discount, oh.local_store_credit_used, 
	oh.local_total_inc_vat, oh.local_total_exc_vat, 
	oh.local_to_global_rate, oh.order_currency_code
from 
		Landing.aux.sales_dim_order_header_aud oh
	inner join
		(select distinct customer_id_bk
		from Landing.aux.sales_dim_order_header) c on oh.customer_id_bk = c.customer_id_bk
order by oh.customer_id_bk, oh.order_date


select count(*)
from Landing.aux.sales_dim_order_header_aud


select count(*)
from Warehouse.sales.dim_order_header


select *
from Warehouse.act.dim_activity_type_v
order by activity_group_name, activity_type_name