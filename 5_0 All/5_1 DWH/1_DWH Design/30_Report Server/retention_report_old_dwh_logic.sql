
select order_id_bk, order_no, order_date, order_status_name, customer_order_seq_no
from Warehouse.sales.dim_order_header_v
where customer_id = 299270
order by order_status_name, order_date


		select
			cast((cast(year(ih.document_Date) as varchar(10))+'-'+cast(month(ih.document_date) as varchar(10))+'-01') as date) as period,
			ih.customer_id, ih.customer_order_seq_no,

			SUM(isnull(il.global_line_total_exc_vat,0)) AS [global_revenue_ex_VAT], sum(isnull(il.global_prof_fee,0)) as [global_prof_fee],
			sum(isnull(il.global_line_total_inc_vat,0)) as [global_revenue_in_VAT], sum(isnull(il.global_line_total_vat,0)) as [global_VAT],

			SUM(isnull(il.local_line_total_exc_vat,0)) AS [local_revenue_ex_VAT], sum(isnull(il.local_prof_fee,0)) as [local_prof_fee],
			sum(isnull(il.local_line_total_inc_vat,0)) as [local_revenue_in_VAT], sum(isnull(il.local_line_total_vat,0)) as [local_VAT]

		from 
				(select ih.*, c.created_in first_website 
				from 
						invoice_headers ih
					inner join
						DW_GetLenses.dbo.customers c on ih.customer_id = c.customer_id
				where ih.customer_id = 299270
				union all 
				select ih.*, c.created_in first_website 
				from	 
						dw_proforma.dbo.invoice_headers ih
					inner join
						DW_GetLenses.dbo.customers c on ih.customer_id = c.customer_id
				where document_date >= '01-feb-2014' and ih.store_name <> 'visiooptik.fr'
					and ih.customer_id = 299270) ih
			inner join 
				(select * 
				from invoice_lines 
				union all 
				select * 
				from dw_proforma.dbo.invoice_lines 
				where document_date>='01-feb-2014' and store_name<>'visiooptik.fr') il 
					on ih.document_id=il.document_id and ih.document_type=il.document_type and ih.store_name=il.store_name

		-- where 
			--ih.document_date >=''' +convert(varchar(11),@from_date,106)+''' and ih.document_date<dateadd(day,1,'''+convert(varchar(11),@to_date,106)+''')
			--and ih.store_name in ('+@store_name+')
			--and ih.customer_order_seq_no is not null
			--and ih.customer_id is not null ' + (case when @inv_to<>'All' then 'and ih.billing_country_id = '''+@inv_to+'''' else '' end)+ ' 
			-- ih.first_website = 'lensway.co.uk'
		group by 
			year(ih.document_Date), month(ih.document_date),
			ih.customer_id, ih.customer_order_seq_no
