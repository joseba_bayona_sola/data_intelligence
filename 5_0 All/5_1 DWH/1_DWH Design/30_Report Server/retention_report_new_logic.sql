
select top 1000 *
from DW_GetLenses_jbs.dbo.retention_ol
where idCustomer_sk_fk in (31656, 7215, 134870, 1732123, 1276800, 2068274, 1781105, 1542880, 1363459, 1825429, 2040034, 1811654, 1662449)
order by idCustomer_sk_fk, invoice_date


select idCustomer_sk_fk, invoice_date, year(invoice_date) yyyy, month(invoice_date) mm, 
	customer_order_seq_no, 
	rank() over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) customer_order_seq_no_own,
	local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses_jbs.dbo.retention_ol
where idCustomer_sk_fk in (31656, 7215, 134870, 1732123, 1276800, 2068274, 1781105, 1542880, 1363459, 1825429, 2040034, 1811654, 1662449)

select idCustomer_sk_fk, 
	invoice_date, yyyy, mm, 
	customer_order_seq_no, customer_order_seq_no_own,
	local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,
	lead(invoice_date) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_invoice_date, 
	lead(local_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_inc_vat, 
	lead(local_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_exc_vat, 
	lead(global_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_inc_vat, 
	lead(global_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_exc_vat

from
	(select idOrderHeader_sk, idCustomer_sk_fk, invoice_date, year(invoice_date) yyyy, month(invoice_date) mm, 
		customer_order_seq_no, 
		rank() over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) customer_order_seq_no_own,
		local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat
	from DW_GetLenses_jbs.dbo.retention_ol
	where idCustomer_sk_fk in (31656, 7215, 134870, 1732123, 1276800, 2068274, 1781105, 1542880, 1363459, 1825429, 2040034, 1811654, 1662449)) t

select period, customer_order_seq_no, month_diff, 
	count(distinct idCustomer_sk_fk) customer_count, sum(num_cust) next_order_customer_count, 
	sum(local_total_inc_vat) local_total_inc_vat, sum(local_total_exc_vat) local_total_exc_vat, sum(global_total_inc_vat) global_total_inc_vat, sum(global_total_exc_vat) global_total_exc_vat,
	sum(next_local_total_inc_vat) next_local_total_inc_vat, sum(next_local_total_exc_vat) next_local_total_exc_vat, 
	sum(next_global_total_inc_vat) next_global_total_inc_vat, sum(next_global_total_exc_vat) next_global_total_exc_vat
from
	(select idCustomer_sk_fk, customer_order_seq_no, customer_order_seq_no_own, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,
		next_local_total_inc_vat, next_local_total_exc_vat, next_global_total_inc_vat, next_global_total_exc_vat, 
		case when (period_next is not null) then 1 else NULL end num_cust
	from
		(select idCustomer_sk_fk, 
			invoice_date, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, -- yyyy, mm, 
			next_invoice_date, cast(cast(year(next_invoice_date) as varchar) + '-' + cast(month(next_invoice_date) as varchar) + '-01' as date) period_next, 
			-- year(next_invoice_date) yyyy_next, month(next_invoice_date) mm_next, 
			customer_order_seq_no, customer_order_seq_no_own, 
			local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,
			next_local_total_inc_vat, next_local_total_exc_vat, next_global_total_inc_vat, next_global_total_exc_vat
		from
			(select idCustomer_sk_fk, 
				invoice_date, yyyy, mm, 
				customer_order_seq_no, customer_order_seq_no_own,
				local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat,

				lead(invoice_date) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_invoice_date, 
				lead(local_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_inc_vat, 
				lead(local_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_local_total_exc_vat, 
				lead(global_total_inc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_inc_vat, 
				lead(global_total_exc_vat) over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) next_global_total_exc_vat

			from
				(select idOrderHeader_sk, idCustomer_sk_fk, invoice_date, year(invoice_date) yyyy, month(invoice_date) mm, 
					customer_order_seq_no, 
					rank() over (partition by idCustomer_sk_fk order by invoice_date, idOrderHeader_sk) customer_order_seq_no_own,
					local_total_inc_vat, local_total_exc_vat, global_total_inc_vat, global_total_exc_vat
				from DW_GetLenses_jbs.dbo.retention_ol
				-- where idCustomer_sk_fk in (31656, 7215, 134870, 1732123, 1276800, 2068274, 1781105, 1542880, 1363459, 1825429, 2040034, 1811654, 1662449)
				) t) t) t) t
group by period, customer_order_seq_no, month_diff
order by customer_order_seq_no, period, month_diff