	
	drop table #website 
	drop table #market 
	drop table #country 
	drop table #website_group 
	drop table #customer_origin
	drop table #marketing_channel 
	drop table #price_type 
	drop table #product_family 
	drop table #rank_qty_time

	------------------------------------

	declare @from_date date, @to_date date
	declare @website varchar(max), @market varchar(max), @country varchar(max), 
		@website_group varchar(max), @customer_origin varchar(max), 
		@marketing_channel varchar(max), @price_type varchar(max), 
		@product_family varchar(max), @rank_qty_time varchar(max) 	
	

	set @website = 'asda-contactlenses.co.uk,eur.getlenses.nl,gbp.getlenses.nl,getlenses.be,getlenses.co.uk,getlenses.es,getlenses.ie,getlenses.it,postoptics.co.uk,surveys.getlenses.co.uk,surveys.getlenses.nl,vhi-contactlenses.ie,visiondirect.be,visiondirect.ca,visiondirect.co.uk,visiondirect.es,visiondirect.fr,visiondirect.ie,visiondirect.it,visiondirect.nl'
	set @market = '-1,7,8,10,1,9,4,6,2,5,3'
	set @country = '4,17,7,63,13,2,10,6,11,5,12,8,16,1,15,14,18,33,25,21,20,37,22,38,27,29,34,31,19,36,35,32,107,30,24,23,26,118,48,39,53,125,42,216,47,49,55,40,50,120,43,41,46,51,99,52,54,56,57,60,59,61,62,226,64,66,211,89,68,65,70,73,75,72,71,76,81,176,217,77,86,80,58,83,84,90,85,79,88,93,92,82,87,94,95,100,97,98,96,101,110,106,102,109,108,103,105,104,111,45,113,115,112,114,126,116,119,122,123,124,117,127,136,128,133,132,137,130,134,135,149,145,143,157,159,156,146,154,144,151,152,155,248,158,74,140,139,148,141,153,138,160,147,-1,161,170,169,167,9,162,172,166,163,165,171,164,150,168,173,179,186,184,174,177,187,175,178,182,180,185,183,188,189,190,192,193,28,200,121,129,142,181,239,246,205,210,194,206,191,196,204,199,212,203,201,195,207,250,91,209,69,131,197,208,202,214,198,44,213,230,220,231,219,222,218,221,225,228,224,227,223,215,229,233,232,3,78,235,234,236,237,244,238,240,243,241,242,245,67,247,249,251,252'
	set @website_group = 'ASDA,EyePlan,Getlenses,lensbase,lenshome,Lenson,Lenson NL,Lensway,Lensway NL,MasterLens,VHI,VisionDirect,Visiondirect (A),VisioOptik,YourLenses NL'
	set @customer_origin = '1,2'
	set @marketing_channel = '23,24,25,26,27,28,29,30,45,46,31,32,33,34,35,36,37,38,39,40,41,42,43,44,-1'
	set @price_type = '1,2,3,4,5,8,6'
	set @product_family = '937,3,366,367,66,1,995,201,372,2,993,365,9,8,235,5,239,4,238,15,234,6,236,312,405,404,7,237,255,251,988,343,276,300,298,301,299,671,672,673,674,675,676,13,10,192,11,199,352,14,190,230,193,12,402,403,677,678,679,680,681,682,112,683,684,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,60,686,685,687,688,561,562,564,565,568,569,570,571,572,573,576,577,578,579,582,583,584,585,586,587,589,590,591,592,593,594,595,596,563,566,567,574,575,580,581,588,168,96,398,399,689,311,310,254,261,267,253,263,264,285,265,262,691,690,692,693,694,696,695,697,698,331,332,1001,986,699,700,880,881,16,186,306,307,959,998,703,704,946,947,169,989,990,294,293,705,706,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,707,708,709,710,711,712,17,187,278,281,18,191,296,326,19,177,282,283,289,290,20,178,977,21,180,972,153,22,275,244,233,357,361,371,713,714,242,243,241,77,78,996,81,82,1004,85,86,83,940,941,143,89,88,715,716,203,90,116,717,718,720,719,721,722,723,724,726,725,728,727,729,730,349,731,732,733,734,735,736,122,737,318,317,392,401,976,979,67,935,954,91,92,368,393,93,738,739,701,702,969,94,269,314,364,315,95,209,145,291,295,351,740,741,742,743,970,151,744,745,27,173,992,353,354,355,356,316,994,958,960,746,747,748,749,750,751,752,943,942,49,1000,753,754,113,755,756,757,758,759,760,761,762,763,764,765,766,123,124,768,767,769,770,79,396,395,348,141,771,772,773,774,955,319,325,321,324,333,670,1003,997,1005,344,363,362,1002,334,23,24,978,335,322,346,983,226,223,292,309,225,224,221,305,222,323,274,308,775,776,547,548,543,544,560,541,542,549,550,551,552,553,554,555,545,546,556,557,558,559,777,778,217,99,26,171,320,991,25,34,174,31,175,29,149,30,28,134,780,779,985,782,781,783,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,484,482,483,492,473,488,478,479,474,475,480,481,476,489,485,477,486,487,490,491,493,494,513,512,510,511,495,496,503,504,506,497,498,499,507,500,514,501,502,505,508,509,35,181,36,182,973,150,37,188,38,39,999,33,40,327,32,41,273,220,784,785,786,787,788,789,359,302,966,965,964,967,791,790,792,793,794,795,796,797,952,953,218,800,801,802,803,804,805,250,214,297,80,215,126,125,280,128,127,130,129,370,205,204,229,167,249,200,61,42,336,337,806,807,809,808,810,811,812,813,814,815,388,389,390,391,816,817,818,819,820,821,822,347,823,824,826,825,515,521,527,523,524,525,528,537,539,529,516,517,518,526,519,520,530,531,532,540,533,534,535,536,538,522,827,828,829,830,945,944,831,832,833,834,835,836,838,837,839,840,963,71,841,842,650,651,652,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,146,100,843,844,845,846,847,848,849,219,850,851,136,138,139,137,206,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,231,232,971,852,853,855,854,856,857,142,951,950,860,861,862,97,863,864,961,865,866,114,338,339,340,341,350,928,929,147,84,858,859,956,957,936,373,374,375,376,377,378,387,379,381,382,380,383,384,386,385,227,228,102,103,245,246,105,104,172,117,161,154,159,160,158,157,155,156,987,867,868,240,870,869,207,208,72,268,871,216,140,152,872,873,874,875,65,48,183,135,342,106,107,108,876,877,44,43,179,284,287,184,975,189,45,198,46,195,121,120,132,131,133,202,47,119,288,51,194,313,50,196,52,213,256,259,271,272,358,400,62,878,879,882,883,397,63,64,162,163,165,164,286,109,110,111,304,210,166,884,885,886,887,949,948,888,889,890,891,892,893,894,895,896,897,898,899,900,901,87,902,903,904,144,56,277,57,185,212,54,176,55,211,58,197,328,53,329,330,98,394,260,266,962,118,980,981,905,906,170,907,984,68,59,279,406,407,408,409,410,669,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,908,909,115,257,69,258,247,248,70,910,911,938,939,913,912,252,914,915,916,917,73,303,75,270,74,918,919,920,921,922,923,982,148,76,924,925,974,369,345,968,101,360,926,927,798,799,930,931,932,933,934'
	set @rank_qty_time = '++,00,01,02,03,04,06,09,12,15,18,24'

	select s.idStore_sk
	into #website
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website
			from Adhoc.dbo.fnSplitString (@website, ',') AS s) t on s.website = t.website


	select cast(s.string AS int) idMarket_sk
	into #market
	from Adhoc.dbo.fnSplitString (@market, ',') AS s

	select cast(s.string AS int) idCountry_sk
	into #country
	from Adhoc.dbo.fnSplitString (@country, ',') AS s

	select s.idStore_sk
	into #website_group
	from 
			Warehouse.gen.dim_store_v s
		inner join
			(select cast(s.string AS sysname) website_group
			from Adhoc.dbo.fnSplitString (@website_group, ',') AS s) t on s.website_group = t.website_group

	select cast(s.string AS int) idCustomerOrigin_sk
	into #customer_origin
	from Adhoc.dbo.fnSplitString (@customer_origin, ',') AS s

	select cast(s.string AS int) idMarketingChannel_sk
	into #marketing_channel
	from Adhoc.dbo.fnSplitString (@marketing_channel, ',') AS s

	select cast(s.string AS int) idPriceType_sk
	into #price_type
	from Adhoc.dbo.fnSplitString (@price_type, ',') AS s

	select cast(s.string AS int) idProductFamily_sk
	into #product_family
	from Adhoc.dbo.fnSplitString (@product_family, ',') AS s

	select rqt.qty_time_bk qty_time
	into #rank_qty_time
	from 
			Warehouse.sales.dim_rank_qty_time rqt
		inner join
			(select cast(s.string AS sysname) rank_qty_time
			from Adhoc.dbo.fnSplitString (@rank_qty_time, ',') AS s) t on rqt.rank_qty_time = t.rank_qty_time

	----------------------------------------

	drop table #customers

	select cs.idCustomer_sk_fk
	into #customers
	from 
			Warehouse.act.fact_customer_signature cs
		inner join
			#website_group wg on cs.idStoreCreate_sk_fk = wg.idStore_sk
		inner join
			#customer_origin co on cs.idCustomerOrigin_sk_fk = co.idCustomerOrigin_sk
		left join -- left
			#marketing_channel mc on cs.idMarketingChannelFirst_sk_fk = mc.idMarketingChannel_sk
		left join -- left
			#price_type pt on cs.idPriceTypeFirst_sk_fk = pt.idPriceType_sk

	drop table #oh

	select oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no, 
		order_status_name, idStore_sk
	into #oh
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			#customers c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			#website w on oh.idStore_sk_fk = w.idStore_sk
		inner join
			#market m on oh.idMarket_sk_fk = m.idMarket_sk
		inner join
			#country co on oh.idCountryShipping_sk_fk = co.idCountry_sk
		inner join
			Warehouse.sales.dim_order_status os on oh.idOrderStatus_sk_fk = os.idOrderStatus_sk
	where oh.invoice_date between '2018-04-01' and '2018-05-01'
		-- and os.order_status_name in ('OK', 'PARTIAL REFUND')


	drop table #retention

	select oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no, 
		-- sum(ol.local_total_inc_vat) local_total_inc_vat, sum(ol.local_total_exc_vat) local_total_exc_vat, 
		-- sum(ol.local_total_inc_vat * ol.local_to_global_rate) global_total_inc_vat, sum(ol.local_total_exc_vat * ol.local_to_global_rate) global_total_exc_vat
		sum(ol.local_total_aft_refund_inc_vat) local_total_inc_vat, sum(ol.local_total_aft_refund_exc_vat) local_total_exc_vat, 
		sum(ol.local_total_aft_refund_inc_vat * ol.local_to_global_rate) global_total_inc_vat, sum(ol.local_total_aft_refund_exc_vat * ol.local_to_global_rate) global_total_exc_vat
	into #retention
	from 
			Warehouse.sales.fact_order_line ol
		inner join
			#oh oh on ol.idOrderHeader_sk_fk = oh.idOrderHeader_sk
		inner join
			#product_family pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
		inner join
			#rank_qty_time rqt on ol.qty_time = rqt.qty_time
	group by oh.idOrderHeader_sk, oh.idCustomer_sk_fk, oh.invoice_date, oh.customer_order_seq_no

----------------------------------------

	select top 1000 *
	from #retention

	select year(invoice_date), month(invoice_date), sum(global_total_exc_vat)
	from #retention
	group by year(invoice_date), month(invoice_date)
	order by year(invoice_date), month(invoice_date)


	select top 1000 *
	from Warehouse.sales.fact_order_line

	select oh.idStore_sk, s.store_name, count(*)
	from 
		#oh oh
			inner join
		Warehouse.gen.dim_store_v s on oh.idStore_sk = s.idStore_sk
	group by oh.idStore_sk, s.store_name
	order by oh.idStore_sk

	select order_status_name, count(*)
	from #oh
	group by order_status_name
	order by order_status_name
