
select top 1000 *
from DW_GetLenses.dbo.customers
where created_in = 'lensway.co.uk'

select top 1000 old_access_cust_no, count(*)
from DW_GetLenses.dbo.customers
group by old_access_cust_no
order by old_access_cust_no

select top 1000 created_in, count(*)
from DW_GetLenses.dbo.customers
group by created_in
order by created_in

select top 1000 created_in, old_access_cust_no, count(*)
from DW_GetLenses.dbo.customers
group by created_in, old_access_cust_no
order by created_in, old_access_cust_no
