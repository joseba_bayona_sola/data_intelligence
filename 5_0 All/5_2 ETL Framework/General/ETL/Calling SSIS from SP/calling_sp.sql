DECLARE 
	@Path VARCHAR(200),
	@SQLServer VARCHAR(50),
	@DB VARCHAR(100), 
	@EmailAddress VARCHAR(500),
	@Cmd VARCHAR(4000),
	@ReturnCode INT,
	@Msg VARCHAR(1000)

	SELECT @Path = 'E:\SSIS\Packages\'
	SELECT @SQLServer = 'MSSQLTIPS01' 
	SELECT @DB = 'Foo_Dev'
	SELECT @EmailAddress = 'this_email_is_fake@foo.com'

	SELECT @Cmd = 'DTexec /FILE "' + @Path + 'Foo.dtsx" /MAXCONCURRENT 1 /CHECKPOINTING OFF /REPORTING EW'
	+ ' /SET \Package.Variables[User::varSourceSQLServer].Properties[Value];' + @SQLServer 
	+ ' /SET \Package.Variables[User::varErrorNotifyEmail].Properties[Value];' + @EmailAddress


	EXEC @ReturnCode = xp_cmdshell @Cmd

	IF @ReturnCode <> 0
	BEGIN
		SELECT @Msg = 'SSIS package execution failed for ' + @path + 'Foo.dtsx on SQL Server\Instance: ' + @SQLServer + '.' + @DB
	END;


	DECLARE @Cmd VARCHAR(4000), @ReturnCode INT

	SELECT @Cmd = 'DTexec /DTS "\SSISDB\DWH\ETL_map\ETLBatchRun.dtsx" /MAXCONCURRENT 1 /CHECKPOINTING OFF /REPORTING EW'  

	EXEC @ReturnCode = xp_cmdshell @Cmd


	--- working --- 

	DECLARE @Cmd VARCHAR(4000), @ReturnCode INT

	SELECT @Cmd = 'DTexec /ISSERVER "\SSISDB\DWH\ETL\ETLBatchRun.dtsx" /SERVER "." /Par "\"$Project::dateFrom(DateTime)\"";"\"2017-02-16 00:00:00\"" /Par "\"$Project::dateTo(DateTime)\"";"\"2017-02-17 00:00:00\"" /Par "\"description\"";"\"Prueba\""'  

	EXEC @ReturnCode = xp_cmdshell @Cmd


