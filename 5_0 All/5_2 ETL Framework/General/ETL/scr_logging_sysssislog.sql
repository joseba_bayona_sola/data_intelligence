
-- https://www.mssqltips.com/sqlservertip/4070/integrated-logging-with-the-integration-services-package-log-providers/

use ControlDB
go 

select *
from dbo.sysssislog
where event = 'PackageStart'
order by event, source, starttime

select *
from dbo.sysssislog
where executionId = '2D28AA8F-E862-47E5-9D13-B4B003431ADA'
order by event, source, starttime

select *
from dbo.sysssislog
-- where event in ('OnError', 'OnWarning', 'OnTaskFailed')
where event in ('OnInformation')
-- where event in ('OnProgress')
-- where event in ('PackageStart', 'PackageEnd')
-- where event in ('OnPostExecute', 'OnPostValidate', 'OnPreExecute', 'OnPreValidate')
order by event, executionid, source

select event, count(*)
from dbo.sysssislog
group by event
order by event

select executionId, count(*)
from dbo.sysssislog
group by executionId
order by executionId

