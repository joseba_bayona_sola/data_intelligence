
declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

-- set @d1 = convert(datetime, '2018/01/14', 120)
-- set @d2 = convert(datetime, '2018/01/15', 120)

set @d1 = getutcdate() - 1
set @d2 = getutcdate() 

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)


	set @sql = '
			select id, 
				issueid, createddate, issuedquantity, 
				cancelled, shippeddate ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					issueid, createddate, issuedquantity, 
					cancelled, shippeddate
				from public."inventory$batchstockissue"
				where createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')			
				'')
			order by createddate'
	exec(@sql)

-------------------------------------------------------



declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

-- set @d1 = convert(datetime, '2018/01/14', 120)
-- set @d2 = convert(datetime, '2018/01/15', 120)

set @d1 = getutcdate() - 1
set @d2 = getutcdate() 

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)

	set @sql = '
			select id, 
				fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
				createddate, changeddate '  +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
					createddate, changeddate
				from public."inventory$batchstockallocation"
				where createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or changeddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
				'')
			order by createddate	'
	exec(@sql)



-------------------------------------------------------



declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

-- set @d1 = convert(datetime, '2018/01/14', 120)
-- set @d2 = convert(datetime, '2018/01/15', 120)

set @d1 = getutcdate() - 1
set @d2 = getutcdate() 

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)

	set @sql = '
			select inventory$batchstockissueid batchstockissueid, inventory$batchstockallocationid batchstockallocationid '  +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$batchstockissueid", "inventory$batchstockallocationid"
				from 
						public."inventory$batchstockissue_batchstockallocation" bsibsa
					inner join
						public."inventory$batchstockallocation" bsa on bsibsa."inventory$batchstockallocationid" = bsa.id
				where bsa.createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or bsa.changeddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
				'')'

	exec(@sql)


-------------------------------------------------------



declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

-- set @d1 = convert(datetime, '2018/01/14', 120)
-- set @d2 = convert(datetime, '2018/01/15', 120)

set @d1 = getutcdate() - 1
set @d2 = getutcdate() 

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)

	set @sql = '
			select inventory$batchstockissueid batchstockissueid, inventory$batchstockallocationid batchstockallocationid ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select bsibsa."inventory$batchstockissueid", bsibsa."inventory$batchstockallocationid"
				from 
						public."inventory$batchstockissue_batchstockallocation" bsibsa
					inner join
						public."inventory$batchstockissue" bsi on bsibsa."inventory$batchstockissueid" = bsi.id 
					inner join
						public."inventory$batchstockallocation" bsa on bsibsa."inventory$batchstockallocationid" = bsa.id				
				where where bsi.createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') or
					(bsa.createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or bsa.changeddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))				
				'')'
	exec(@sql)

-- 				where bsi.createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') or

--					(bsa.createddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
--						or bsa.changeddate between to_timestamp(''''' + @dv1 + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dv2 + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))			

