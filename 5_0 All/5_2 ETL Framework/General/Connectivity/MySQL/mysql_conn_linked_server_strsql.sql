

---- WORKING

declare @sql varchar(max)


set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between curdate() - interval 4 day and curdate() - interval 3 day'')'

print (@sql)
exec(@sql)

--- 

---- WORKING

declare @sql varchar(max)
declare @d1 int=4, @d2 int=3

set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between curdate() - interval ' + cast(@d1 as varchar(5)) + ' day and curdate() - interval ' + cast(@d2 as varchar(5)) + ' day'')'

print (@sql)
exec(@sql)

--- 

---- NOT WORKING

declare @sql varchar(max)
declare @d1 datetime='2017-01-14', @d2 datetime='2017-01-15'

print (@d1)

set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between ' + cast(@d1 as varchar(20)) + ' and ' + cast(@d2 as varchar(20)) + ''')'

print (@sql)

exec(@sql)

--- 

---- WORKING

declare @sql varchar(max)
declare @d1 varchar(20)='14/01/2017', @d2 varchar(20)='15/01/2017'

set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between STR_TO_DATE(''''14/01/2017'''', ''''%d/%m/%Y'''') and STR_TO_DATE(''''15/01/2017'''', ''''%d/%m/%Y'''')'')'

print (@sql)

exec(@sql)


select * 
from openquery(MAGENTO, '
	select entity_id, increment_id 
	from magento01.sales_flat_order 
	where created_at between STR_TO_DATE(''14/01/2017'', ''%d/%m/%Y'') and STR_TO_DATE(''15/01/2017'', ''%d/%m/%Y'')')

--- 

---- WORKING

declare @sql varchar(max)
declare @d1 varchar(20)='14/01/2017', @d2 varchar(20)='15/01/2017'

set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between STR_TO_DATE(''''' + @d1 + ''''', ''''%d/%m/%Y'''') and STR_TO_DATE(''''' + @d2 + ''''', ''''%d/%m/%Y'''')'')'

print (@sql)

exec(@sql)


--- 

---- WORKING

declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

set @d1 = convert(datetime, '2017/01/14', 120)
set @d2 = convert(datetime, '2017/01/15', 120)

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)

set @sql = 'select * ' +
	'from openquery(MAGENTO, ''
		select entity_id, increment_id 
		from magento01.sales_flat_order 
		where created_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')'')'

print (@sql)

exec(@sql)

--------------------------------------------------------------------------------------

---- WORKING

declare @sql varchar(max)
declare @d1 datetime, @d2 datetime
declare @dv1 varchar(20), @dv2 varchar(20)

set @d1 = convert(datetime, '2017/01/14', 120)
set @d2 = convert(datetime, '2017/01/15', 120)

print(@d1)
print(@d2)

set @dv1 = convert(varchar, @d1, 120)
set @dv2 = convert(varchar, @d2, 120)

print(@dv1)
print(@dv2)

set @sql = 'select * from openquery(MAGENTO, ' + 
	'''select o.entity_id, o.increment_id, st.store_id, st.name ' + 
	'from magento01.sales_flat_order o inner join magento01.core_store st on o.store_id = st.store_id ' + 
	'where created_at between STR_TO_DATE(''''' + @dv1 + ''''', ''''%Y-%m-%d'''') and STR_TO_DATE(''''' + @dv2 + ''''', ''''%Y-%m-%d'''')'')'

print (@sql)

exec(@sql)
