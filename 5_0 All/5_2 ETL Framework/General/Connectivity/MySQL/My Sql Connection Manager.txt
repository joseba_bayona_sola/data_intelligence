
https://blogs.msdn.microsoft.com/mattm/2008/03/04/connecting-to-mysql-from-ssis/

TITLE: Microsoft Visual Studio
------------------------------

Could not get provider invariant name from the connection type qualifier "MySql.Data.MySqlClient.MySqlConnection, MySql.Data, Version=6.9.6.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d". You may need to manually edit the 'Qualifier' property of the connection manager if the default one selected is different from what you want.

------------------------------
ADDITIONAL INFORMATION:

Could not create a DbProviderFactory class for the provider invariant name 'Microsoft.SqlServerCe.Client.4.0'. Verify that this provider is installed correctly on this computer. (Microsoft.DataTransformationServices.Design)

For help, click: http://go.microsoft.com/fwlink?ProdName=Microsoft%C2%AE%20Visual%20Studio%C2%AE%202015&ProdVer=14.0.23107.0&EvtSrc=Microsoft.DataTransformationServices.Design.SR&EvtID=CouldNotGetFactory&LinkId=20476

------------------------------

Failed to find or load the registered .Net Framework Data Provider. (System.Data)

------------------------------
BUTTONS:

OK
------------------------------






<?xml version="1.0"?>
<DTS:ConnectionManager xmlns:DTS="www.microsoft.com/SqlServer/Dts"
  DTS:ObjectName="MySQL"
  DTS:DTSID="{5280B8D1-D05F-4B75-B337-A83D34A3E212}"
  DTS:CreationName="ADO.NET:MySql.Data.MySqlClient.MySqlConnection, MySql.Data, Version=6.9.6.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d">
  <DTS:ObjectData>
    <DTS:ConnectionManager
      DTS:ConnectionString="server=ec2-54-170-115-143.eu-west-1.compute.amazonaws.com;user id=root;port=3306;database=magento01;convertzerodatetime=False;connectiontimeout=300;defaultcommandtimeout=300000;usedefaultcommandtimeoutforef=True;allowbatch=True;">
      <DTS:Password
        DTS:Name="password"
        Sensitive="1">AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAzJDakEDBikakdW+Vbju4ZwAAAAACAAAAAAADZgAAwAAAABAAAAC5zM7/w/y7nDtyQhKRSw4/AAAAAASAAACgAAAAEAAAABvBh/9QbB7yf+YoAPLlZWx4AAAAyFh/bX/5fus6UdEwJaCFpYVA0GII3Pox5OxuzGM0Ooiee9+TnYPS4BYeW08yEqqVcUvReBzlPcirPMEUySy6zSahxI5oKX5Gw5Zw+vZjArcwvVXe099kMiICEz6P1QaYy+/HExmbteNF5zsb2g5v07eobmDqxf39FAAAANJQ8QxyfZVLbJVOOOyvtvKm10zs</DTS:Password>
    </DTS:ConnectionManager>
  </DTS:ObjectData>
</DTS:ConnectionManager>

---

<?xml version="1.0"?>
<DTS:ConnectionManager xmlns:DTS="www.microsoft.com/SqlServer/Dts"
  DTS:ObjectName="MSSQL_DW_GetLenses"
  DTS:DTSID="{123D16C7-0B19-4435-8A60-5ECEF8035E88}"
  DTS:CreationName="OLEDB">
  <DTS:ObjectData>
    <DTS:ConnectionManager
      DTS:ConnectionString="Data Source=.;Initial Catalog=DW_GetLenses;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;" />
  </DTS:ObjectData>
</DTS:ConnectionManager>