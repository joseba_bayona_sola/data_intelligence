
use Warehouse
go

drop view sales.fact_order_line_inv_v
go 

create view sales.fact_order_line_inv_v as

	select order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
		order_id_bk, order_id_bk order_id_bk_c, order_no, 
		
		invoice_id invoice_id, invoice_no invoice_no, 
		invoice_date, invoice_date_c, invoice_week_day, 
		shipment_date_r shipment_date, 
		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name, 
		customer_id, customer_id customer_id_c, customer_email, customer_name, 
		country_code_ship, country_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
	
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen, order_source, proforma,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name,
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		qty_unit, 
		qty_pack, 
		-- weight, 
		rank_qty_time, qty_time,

		price_type_name, discount_f,

		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

		global_subtotal, global_shipping, global_discount, global_store_credit_used, 
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 

		local_to_global_rate, order_currency_code, 

		countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,

		idETLBatchRun_ins, idETLBatchRun_upd
	from Warehouse.sales.fact_order_line_v
	where invoice_date is not null
	union
	select ol.order_line_id_bk,  null order_line_id_bk_c, 
		ol.order_id_bk, null order_id_bk_c, order_no, 

		creditmemo_id invoice_id, creditmemo_no invoice_no, 
		refund_date invoice_date, refund_date_c invoice_date_c, refund_week_day invoice_week_day, 
		shipment_date_r shipment_date, 
		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name, 
		customer_id, null customer_id_c, customer_email, customer_name, 
		country_code_ship, country_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
	
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen, order_source, proforma,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name,
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		qty_unit_refunded * -1 qty_unit, 
		convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack,
		rank_qty_time, qty_time,
		-- weight, 

		price_type_name, discount_f,

		local_subtotal_m local_subtotal, local_shipping_m local_shipping, local_discount_m local_discount, local_store_credit_used_m local_store_credit_used, 
		local_total_inc_vat_m local_total_inc_vat, local_total_exc_vat_m local_total_exc_vat, local_total_vat_m local_total_vat, local_total_prof_fee_m local_total_prof_fee, 
		-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

		convert(decimal(12, 4), local_subtotal_m * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping_m * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), local_discount_m * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used_m * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), local_total_inc_vat_m * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat_m * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), local_total_vat_m * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee_m * ol.local_to_global_rate) global_total_prof_fee, 
		-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

		convert(decimal(12, 4), local_total_inc_vat_vf_m) local_total_inc_vat_vf, convert(decimal(12, 4), local_total_exc_vat_vf_m) local_total_exc_vat_vf, 
		convert(decimal(12, 4), local_total_vat_vf_m) local_total_vat_vf, convert(decimal(12, 4), local_total_prof_fee_vf_m) local_total_prof_fee_vf, 

		local_to_global_rate, order_currency_code, 

		countries_registered_code, product_type_vat, ol.vat_rate, ol.prof_fee_rate,

		idETLBatchRun_ins, idETLBatchRun_upd
	from 
		(select *, 
			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund * -1 local_discount_m, 
			local_store_credit_used_refund * -1 local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m,

			local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
			(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
			local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m

		from
			(select *,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m, 
				local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
					+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
			from Warehouse.sales.fact_order_line_v
			where invoice_date is not null
				and (local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0)) ol) ol
go
					

					
					
					
drop view sales.fact_order_line_ship_v
go 

create view sales.fact_order_line_ship_v as	
	select order_line_id_bk, order_line_id_bk order_line_id_bk_c, 
		order_id_bk, order_id_bk order_id_bk_c, order_no, 
		
		shipment_id shipment_id, shipment_no shipment_no, 
		shipment_date_r shipment_date, shipment_date_c, shipment_week_day, 
		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name,
		customer_id, customer_id customer_id_c, customer_email,  
		country_code_ship, country_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
	
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen, order_source, proforma,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name,
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		qty_unit, 
		qty_pack, 
		-- weight, 
		rank_qty_time, qty_time,

		price_type_name, discount_f,

		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

		global_subtotal, global_shipping, global_discount, global_store_credit_used, 
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 

		local_to_global_rate, order_currency_code, 

		countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,

		idETLBatchRun_ins, idETLBatchRun_upd

	from Warehouse.sales.fact_order_line_v
	where shipment_date_r is not null
	union
	select ol.order_line_id_bk,  null order_line_id_bk_c, 
		ol.order_id_bk, null order_id_bk_c, order_no, 

		creditmemo_id shipment_id, creditmemo_no shipment_no, 
		refund_date shipment_date, refund_date_c shipment_date_c, refund_week_day shipment_week_day, 
		rank_shipping_days, shipping_days,

		acquired, tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		customer_origin_name,
		market_name,
		customer_id, null customer_id_c, customer_email, 
		country_code_ship, country_name_ship, postcode_shipping, 
		country_code_bill, country_name_bill, postcode_billing, 
	
		order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
		reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
		channel_name, marketing_channel_name, coupon_code,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, rank_seq_no_gen, customer_order_seq_no_gen, order_source, proforma,

		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, cl_type_name, cl_feature_name,
		product_id_magento, product_family_code, product_family_name, 

		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		qty_unit_refunded * -1 qty_unit, 
		convert(int, (qty_unit_refunded * -1) / (qty_unit / qty_pack)) qty_pack,
		-- weight, 
		rank_qty_time, qty_time,

		price_type_name, discount_f,

		local_subtotal_m local_subtotal, local_shipping_m local_shipping, local_discount_m local_discount, local_store_credit_used_m local_store_credit_used, 
		local_total_inc_vat_m local_total_inc_vat, local_total_exc_vat_m local_total_exc_vat, local_total_vat_m local_total_vat, local_total_prof_fee_m local_total_prof_fee, 
		-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

		convert(decimal(12, 4), local_subtotal_m * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), local_shipping_m * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), local_discount_m * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), local_store_credit_used_m * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), local_total_inc_vat_m * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), local_total_exc_vat_m * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), local_total_vat_m * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), local_total_prof_fee_m * ol.local_to_global_rate) global_total_prof_fee, 
		-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

		convert(decimal(12, 4), local_total_inc_vat_vf_m) local_total_inc_vat_vf, convert(decimal(12, 4), local_total_exc_vat_vf_m) local_total_exc_vat_vf, 
		convert(decimal(12, 4), local_total_vat_vf_m) local_total_vat_vf, convert(decimal(12, 4), local_total_prof_fee_vf_m) local_total_prof_fee_vf, 

		local_to_global_rate, order_currency_code, 

		countries_registered_code, product_type_vat, ol.vat_rate, ol.prof_fee_rate,

		idETLBatchRun_ins, idETLBatchRun_upd
	from 
		(select *, 
			local_subtotal_refund * -1 local_subtotal_m, local_shipping_refund * -1 local_shipping_m, local_discount_refund * -1 local_discount_m, 
			local_store_credit_used_refund * -1 local_store_credit_used_m, local_adjustment_refund local_adjustment_m, 

			local_total_inc_vat_m - ((local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_m,
			(local_total_inc_vat_m - (local_total_inc_vat_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_m,
			local_total_inc_vat_m * (prof_fee_rate / 100) local_total_prof_fee_m, 

			local_total_inc_vat_vf_m - ((local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate)) local_total_exc_vat_vf_m,
			(local_total_inc_vat_vf_m - (local_total_inc_vat_vf_m * (prof_fee_rate / 100))) * vat_rate / (100 + vat_rate) local_total_vat_vf_m,
			local_total_inc_vat_vf_m * (prof_fee_rate / 100) local_total_prof_fee_vf_m
		from
			(select *,
				local_subtotal_refund * -1 + local_shipping_refund * -1 - local_discount_refund * -1 - local_store_credit_used_refund * -1 - local_adjustment_refund 
					+ (local_store_credit_given - local_store_credit_used_refund) local_total_inc_vat_m,
				local_subtotal_refund_vf * -1 + local_shipping_refund_vf * -1 - local_discount_refund_vf * -1 - local_store_credit_used_refund_vf * -1 - local_adjustment_refund_vf 
					+ (local_store_credit_given_vf - local_store_credit_used_refund_vf) local_total_inc_vat_vf_m
			from Warehouse.sales.fact_order_line_v
			where shipment_date_r is not null
				and (local_store_credit_given <> 0 or local_bank_online_given <> 0 or qty_unit_refunded <> 0)) ol) ol
go