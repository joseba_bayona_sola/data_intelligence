use Warehouse
go 

--------------------------------------------------------

create table Warehouse.act.dim_activity_group(
	idActivityGroup_sk			int NOT NULL IDENTITY(1, 1), 
	activity_group_name_bk		varchar(50) NOT NULL,
	activity_group_name			varchar(50) NOT NULL,  
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.act.dim_activity_group add constraint [PK_act_dim_activity_group]
	primary key clustered (idActivityGroup_sk);
go

alter table Warehouse.act.dim_activity_group add constraint [DF_act_dim_activity_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.dim_activity_group add constraint [UNIQ_act_dim_activity_group_activity_group_name_bk]
	unique (activity_group_name_bk);
go



create table Warehouse.act.dim_activity_group_wrk(
	activity_group_name_bk		varchar(50) NOT NULL,
	activity_group_name			varchar(50) NOT NULL,  
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.act.dim_activity_group_wrk add constraint [DF_act_dim_activity_group_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.act.dim_activity_type(
	idActivityType_sk			int NOT NULL IDENTITY(1, 1), 
	activity_type_name_bk		varchar(50) NOT NULL,
	activity_type_name			varchar(50) NOT NULL,  
	idActivityGroup_sk_fk		int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.act.dim_activity_type add constraint [PK_act_dim_activity_type]
	primary key clustered (idActivityType_sk);
go

alter table Warehouse.act.dim_activity_type add constraint [DF_act_dim_activity_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.dim_activity_type add constraint [UNIQ_act_dim_activity_type_activity_type_name_bk]
	unique (activity_type_name_bk);
go

alter table Warehouse.act.dim_activity_type add constraint [FK_act_dim_activity_type_dim_activity_group]
	foreign key (idActivityGroup_sk_fk) references Warehouse.act.dim_activity_group (idActivityGroup_sk);
go

create table Warehouse.act.dim_activity_type_wrk(
	activity_type_name_bk		varchar(50) NOT NULL,
	activity_type_name			varchar(50) NOT NULL,  
	idActivityGroup_sk_fk		int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.act.dim_activity_type_wrk add constraint [DF_act_dim_activity_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





--------------------------------------------------------

create table Warehouse.act.fact_activity_sales(
	idActivitySales_sk					int NOT NULL IDENTITY(1, 1), 
	order_id_bk							bigint NOT NULL,

	idCalendarActivityDate_sk_fk		int NOT NULL, 
	activity_date						datetime NOT NULL,
	idCalendarPrevActivityDate_sk_fk	int,
	idCalendarNextActivityDate_sk_fk	int,
	next_order_freq_time 				int,

	idActivityType_sk_fk				int NOT NULL,
	idStore_sk_fk						int NOT NULL,
	idCustomer_sk_fk					int NOT NULL,

	idChannel_sk_fk						int NOT NULL,
	idMarketingChannel_sk_fk			int NOT NULL, 
	idPriceType_sk_fk					int NOT NULL, 
	discount_f							char(1),
	idProductTypeOH_sk_fk				int,
	order_qty_time						int,  
	num_diff_product_type_oh			int,

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,
	customer_order_seq_no				int NOT NULL, 
	customer_order_seq_no_web			int, 
	customer_order_seq_no_gen			int, 

	local_subtotal						decimal(12, 4),
	local_discount						decimal(12, 4),
	local_store_credit_used				decimal(12, 4),

	local_total_inc_vat					decimal(12, 4),
	local_total_exc_vat					decimal(12, 4),

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.fact_activity_sales add constraint [PK_act_fact_activity_sales]
	primary key clustered (idActivitySales_sk);
go

alter table Warehouse.act.fact_activity_sales add constraint [DF_act_fact_activity_sales_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.fact_activity_sales add constraint [UNIQ_act_fact_activity_sales_order_id_bk]
	unique (order_id_bk);
go

alter table Warehouse.act.fact_activity_sales add constraint [UNIQ_act_fact_activity_sales_cust_act_type_order_seq_no]
	unique (idCustomer_sk_fk, idActivityType_sk_fk, customer_order_seq_no);
go

alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_calendar_activity_date]
	foreign key (idCalendarActivityDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_calendar_prev_activity_date]
	foreign key (idCalendarPrevActivityDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_calendar_next_activity_date]
	foreign key (idCalendarNextActivityDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_activity_type]
	foreign key (idActivityType_sk_fk) references Warehouse.act.dim_activity_type (idActivityType_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_store]
	foreign key (idStore_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go

alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_channel]
	foreign key (idChannel_sk_fk) references Warehouse.sales.dim_channel (idChannel_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_marketing_channel]
	foreign key (idMarketingChannel_sk_fk) references Warehouse.sales.dim_marketing_channel (idMarketingChannel_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_price_type]
	foreign key (idPriceType_sk_fk) references Warehouse.sales.dim_price_type (idPriceType_sk);
go
alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_product_type_oh]
	foreign key (idProductTypeOH_sk_fk) references Warehouse.prod.dim_product_type_oh (idProductTypeOH_sk);
go

alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_customer_status]
	foreign key (idCustomerStatusLifecycle_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go

alter table Warehouse.act.fact_activity_sales add constraint [FK_act_fact_activity_sales_dim_rank_customer_order_seq_no]
	foreign key (customer_order_seq_no) references Warehouse.sales.dim_rank_customer_order_seq_no (idCustomer_order_seq_no_sk);
go


create table Warehouse.act.fact_activity_sales_wrk(
	order_id_bk							bigint NOT NULL,

	idCalendarActivityDate_sk_fk		int NOT NULL, 
	activity_date						datetime NOT NULL,
	idCalendarPrevActivityDate_sk_fk	int,
	idCalendarNextActivityDate_sk_fk	int,

	idActivityType_sk_fk				int NOT NULL,
	idStore_sk_fk						int NOT NULL,
	idCustomer_sk_fk					int NOT NULL,

	idChannel_sk_fk						int, 
	idMarketingChannel_sk_fk			int, 
	idPriceType_sk_fk					int NOT NULL, 
	discount_f							char(1),
	idProductTypeOH_sk_fk				int,
	order_qty_time						int,  
	num_diff_product_type_oh			int,

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,
	customer_order_seq_no				int NOT NULL,
	customer_order_seq_no_web			int,  
	customer_order_seq_no_gen			int, 

	local_subtotal						decimal(12, 4),
	local_discount						decimal(12, 4),
	local_store_credit_used				decimal(12, 4),

	local_total_inc_vat					decimal(12, 4),
	local_total_exc_vat					decimal(12, 4),

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_activity_sales_wrk add constraint [PK_act_fact_activity_sales_wrk]
	primary key clustered (order_id_bk);
go

alter table Warehouse.act.fact_activity_sales_wrk add constraint [DF_act_fact_activity_sales_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.fact_activity_sales_wrk add constraint [DF_act_fact_activity_sales_wrk_idChannel_sk_fk] DEFAULT -1 for idChannel_sk_fk;
go 
alter table Warehouse.act.fact_activity_sales_wrk add constraint [DF_act_fact_activity_sales_wrk_idMarketingChannel_sk_fk] DEFAULT -1 for idMarketingChannel_sk_fk;
go 


--------------------------------------------------------

create table Warehouse.act.fact_activity_other(
	idActivityOther_sk					int NOT NULL IDENTITY(1, 1), 
	activity_id_bk						bigint NOT NULL,

	idCalendarActivityDate_sk_fk		int NOT NULL, 
	activity_date						datetime NOT NULL,
	idCalendarPrevActivityDate_sk_fk	int,

	idActivityType_sk_fk				int NOT NULL,
	idStore_sk_fk						int NOT NULL,
	idCustomer_sk_fk					int NOT NULL,

	activity_seq_no						int NOT NULL,

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.fact_activity_other add constraint [PK_act_fact_activity_other]
	primary key clustered (idActivityOther_sk);
go

alter table Warehouse.act.fact_activity_other add constraint [DF_act_fact_activity_other_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.fact_activity_other add constraint [UNIQ_act_fact_activity_other_activity_id_bk_idActivity_Type_sk_fk]
	unique (activity_id_bk, idActivityType_sk_fk, idCalendarActivityDate_sk_fk);
go

alter table Warehouse.act.fact_activity_other add constraint [UNIQ_act_fact_activity_other_cust_act_type_act_seq_no]
	unique (idCustomer_sk_fk, idActivityType_sk_fk, activity_seq_no);
go



alter table Warehouse.act.fact_activity_other add constraint [FK_act_fact_activity_other_dim_calendar_activity_date]
	foreign key (idCalendarActivityDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_activity_other add constraint [FK_act_fact_activity_other_dim_calendar_prev_activity_date]
	foreign key (idCalendarPrevActivityDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.act.fact_activity_other add constraint [FK_act_fact_activity_other_dim_activity_type]
	foreign key (idActivityType_sk_fk) references Warehouse.act.dim_activity_type (idActivityType_sk);
go
alter table Warehouse.act.fact_activity_other add constraint [FK_act_fact_activity_other_dim_store]
	foreign key (idStore_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_activity_other add constraint [FK_act_fact_activity_other_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go



create table Warehouse.act.fact_activity_other_wrk(
	activity_id_bk						bigint NOT NULL,

	idCalendarActivityDate_sk_fk		int NOT NULL, 
	activity_date						datetime NOT NULL,
	idCalendarPrevActivityDate_sk_fk	int,

	idActivityType_sk_fk				int NOT NULL,
	idStore_sk_fk						int NOT NULL,
	idCustomer_sk_fk					int NOT NULL,

	activity_seq_no						int NOT NULL,

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_activity_other_wrk add constraint [PK_act_fact_activity_other_wrk]
	primary key clustered (activity_id_bk, idActivityType_sk_fk, idCalendarActivityDate_sk_fk);
go

alter table Warehouse.act.fact_activity_other_wrk add constraint [DF_act_fact_activity_other_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------


create table Warehouse.act.fact_customer_signature(
	idCustomer_sk_fk					int NOT NULL, 

	idStoreCreate_sk_fk					int,
	idCalendarCreateDate_sk_fk			int, 
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL, 
	idStoreFirstOrder_sk_fk				int,
	idStoreLastOrder_sk_fk				int,
	idCalendarFirstOrderDate_sk_fk		int,
	idCalendarLastOrderDate_sk_fk		int,
	idCalendarLastLoginDate_sk_fk		int,

	idCustomerOrigin_sk_fk				int,

	num_tot_orders_web					int, 
	num_tot_orders_gen					int, 
	num_tot_orders						int NOT NULL, 
	subtotal_tot_orders					decimal(12, 4) NOT NULL,
	num_tot_cancel						int NOT NULL, 
	subtotal_tot_cancel					decimal(12, 4) NOT NULL,
	num_tot_refund						int NOT NULL, 
	subtotal_tot_refund					decimal(12, 4) NOT NULL,

	num_tot_lapsed						int,
	num_tot_reactivate					int, 

	num_tot_discount_orders				int NOT NULL, 
	discount_tot_value					decimal(12, 4) NOT NULL,
	num_tot_store_credit_orders			int NOT NULL, 
	store_credit_tot_value				decimal(12, 4) NOT NULL,

	num_tot_emails						int, 
	num_tot_text_messages				int, 
	num_tot_reviews						int, 
	num_tot_logins						int,

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,
	idCalendarStatusUpdateDate_sk_fk	int NOT NULL,

	idProductTypeOHMain_sk_fk			int,
	num_diff_product_type_oh			int,
	main_order_qty_time					int, 
	min_order_qty_time					int, 
	avg_order_qty_time					decimal(12, 4),
	max_order_qty_time					int,
	stdev_order_qty_time				decimal(12, 4),

	main_order_freq_time				int, 
	min_order_freq_time					int, 
	avg_order_freq_time					decimal(12, 4),
	max_order_freq_time					int,
	stdev_order_freq_time				decimal(12, 4),

	idChannelMain_sk_fk					int, 
	idMarketingChannelMain_sk_fk		int, 
	idPriceTypeMain_sk_fk				int, 

	idChannelFirst_sk_fk				int, 
	idMarketingChannelFirst_sk_fk		int, 
	idPriceTypeFirst_sk_fk				int, 

	order_id_bk_first					bigint, 
	order_id_bk_last					bigint, 

	first_discount_amount				decimal(12, 4), 
	first_subtotal_amount				decimal(12, 4), 
	last_subtotal_amount				decimal(12, 4), 

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.fact_customer_signature add constraint [PK_act_fact_customer_signature]
	primary key clustered (idCustomer_sk_fk);
go

alter table Warehouse.act.fact_customer_signature add constraint [DF_act_fact_customer_signature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go


alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_store_create]
	foreign key (idStoreCreate_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_store_reg]
	foreign key (idStoreRegister_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_store_first]
	foreign key (idStoreFirstOrder_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_store_last]
	foreign key (idStoreLastOrder_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_calendar_reg]
	foreign key (idCalendarRegisterDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_calendar_first]
	foreign key (idCalendarFirstOrderDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_calendar_last]
	foreign key (idCalendarLastOrderDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_calendar_login]
	foreign key (idCalendarLastLoginDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_calendar_status]
	foreign key (idCalendarStatusUpdateDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go


alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_customer_status]
	foreign key (idCustomerStatusLifecycle_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_product_type_oh]
	foreign key (idProductTypeOHMain_sk_fk) references Warehouse.prod.dim_product_type_oh (idProductTypeOH_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_channel]
	foreign key (idChannelMain_sk_fk) references Warehouse.sales.dim_channel (idChannel_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_marketing_channel]
	foreign key (idMarketingChannelMain_sk_fk) references Warehouse.sales.dim_marketing_channel (idMarketingChannel_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_price_type]
	foreign key (idPriceTypeMain_sk_fk) references Warehouse.sales.dim_price_type (idPriceType_sk);
go


alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_channel_first]
	foreign key (idChannelFirst_sk_fk) references Warehouse.sales.dim_channel (idChannel_sk);
go
alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_marketing_channel_first]
	foreign key (idMarketingChannelFirst_sk_fk) references Warehouse.sales.dim_marketing_channel (idMarketingChannel_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_price_type_first]
	foreign key (idPriceTypeFirst_sk_fk) references Warehouse.sales.dim_price_type (idPriceType_sk);
go


alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_rank_customer_order_seq_no_o]
	foreign key (num_tot_orders) references Warehouse.sales.dim_rank_customer_order_seq_no (idCustomer_order_seq_no_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_rank_customer_order_seq_no_c]
	foreign key (num_tot_cancel) references Warehouse.sales.dim_rank_customer_order_seq_no (idCustomer_order_seq_no_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_rank_customer_order_seq_no_r]
	foreign key (num_tot_refund) references Warehouse.sales.dim_rank_customer_order_seq_no (idCustomer_order_seq_no_sk);
go

alter table Warehouse.act.fact_customer_signature add constraint [FK_act_fact_customer_signature_dim_customer_origin]
	foreign key (idCustomerOrigin_sk_fk) references Warehouse.gen.dim_customer_origin (idCustomerOrigin_sk);
go



create table Warehouse.act.fact_customer_signature_wrk(
	idCustomer_sk_fk					int NOT NULL, 

	idStoreCreate_sk_fk					int,
	idCalendarCreateDate_sk_fk			int, 
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL, 
	idStoreFirstOrder_sk_fk				int,
	idStoreLastOrder_sk_fk				int,
	idCalendarFirstOrderDate_sk_fk		int,
	idCalendarLastOrderDate_sk_fk		int,
	idCalendarLastLoginDate_sk_fk		int,

	idCustomerOrigin_sk_fk				int,

	num_tot_orders_web					int,
	num_tot_orders_gen					int, 
	num_tot_orders						int NOT NULL, 
	subtotal_tot_orders					decimal(12, 4) NOT NULL,
	num_tot_cancel						int NOT NULL, 
	subtotal_tot_cancel					decimal(12, 4) NOT NULL,
	num_tot_refund						int NOT NULL, 
	subtotal_tot_refund					decimal(12, 4) NOT NULL,

	num_tot_lapsed						int,
	num_tot_reactivate					int, 
		
	num_tot_discount_orders				int NOT NULL, 
	discount_tot_value					decimal(12, 4) NOT NULL,
	num_tot_store_credit_orders			int NOT NULL, 
	store_credit_tot_value				decimal(12, 4) NOT NULL,

	num_tot_emails						int, 
	num_tot_text_messages				int, 
	num_tot_reviews						int, 
	num_tot_logins						int,

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,
	idCalendarStatusUpdateDate_sk_fk	int NOT NULL,

	idProductTypeOHMain_sk_fk			int,
	num_diff_product_type_oh			int,
	main_order_qty_time					int, 
	min_order_qty_time					int, 
	avg_order_qty_time					decimal(12, 4),
	max_order_qty_time					int,
	stdev_order_qty_time				decimal(12, 4),

	main_order_freq_time				int, 
	min_order_freq_time					int, 
	avg_order_freq_time					decimal(12, 4),
	max_order_freq_time					int,
	stdev_order_freq_time				decimal(12, 4),

	idChannelMain_sk_fk					int, 
	idMarketingChannelMain_sk_fk		int, 
	idPriceTypeMain_sk_fk				int, 

	idChannelFirst_sk_fk				int, 
	idMarketingChannelFirst_sk_fk		int, 
	idPriceTypeFirst_sk_fk				int, 

	order_id_bk_first					bigint, 
	order_id_bk_last					bigint, 

	first_discount_amount				decimal(12, 4), 
	first_subtotal_amount				decimal(12, 4), 
	last_subtotal_amount				decimal(12, 4), 

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255),

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_customer_signature_wrk add constraint [PK_act_fact_customer_signature_wrk]
	primary key clustered (idCustomer_sk_fk);
go

alter table Warehouse.act.fact_customer_signature_wrk add constraint [DF_act_fact_customer_signature_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Warehouse.act.fact_customer_signature_scd(
	idCustomerSignatureSCD_sk			int NOT NULL IDENTITY(1, 1),
	idCustomer_sk_fk					int NOT NULL, 

	idStoreCreate_sk_fk					int,
	idCalendarCreateDate_sk_fk			int, 
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL, 

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,

	idCalendarFrom_sk_fk				int NOT NULL,
	idCalendarTo_sk_fk					int,

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_customer_signature_scd add constraint [PK_act_fact_customer_signature_scd]
	primary key clustered (idCustomerSignatureSCD_sk);
go

alter table Warehouse.act.fact_customer_signature_scd add constraint [DF_act_fact_customer_signature_scd_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_fact_customer_signature_scd_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go


alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_fact_customer_signature_scd_dim_store_create]
	foreign key (idStoreCreate_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go
alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_fact_customer_signature_scd_dim_store_reg]
	foreign key (idStoreRegister_sk_fk) references Warehouse.gen.dim_store (idStore_sk);
go

alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_fact_customer_signature_scd_dim_calendar_reg]
	foreign key (idCalendarRegisterDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go


alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_fact_customer_signature_scd_dim_customer_status]
	foreign key (idCustomerStatusLifecycle_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go


alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_dim_customer_signature_SCD_dim_calendar_from]
	foreign key (idCalendarFrom_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go

alter table Warehouse.act.fact_customer_signature_scd add constraint [FK_act_dim_customer_signature_SCD_dim_calendar_to]
	foreign key (idCalendarTo_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go


----------------------




create table Warehouse.act.fact_customer_signature_aux_orders(	
	order_id_bk							bigint NOT NULL,
	idCustomer_sk_fk					int NOT NULL, 
	idStore_sk_fk						int,
	idCalendarActivityDate_sk_fk		int,
	activity_date						datetime,
	
	num_tot_orders_web					int, 
	num_order							int,
	num_tot_orders						int, 
	subtotal_tot_orders					decimal(12, 4),

	num_tot_discount_orders				int, 
	discount_tot_value					decimal(12, 4),
	num_tot_store_credit_orders			int, 
	store_credit_tot_value				decimal(12, 4),

	idProductTypeOH_sk_fk				int, 
	product_type_oh_name				varchar(50),
	order_qty_time						int,
	idChannel_sk_fk						int, 
	channel_name						varchar(50), 
	idMarketingChannel_sk_fk			int, 
	marketing_channel_name				varchar(50), 
	idPriceType_sk_fk					int, 
	price_type_name						varchar(40), 

	customer_status_name				varchar(50) NOT NULL, 
	customer_order_seq_no				int NOT NULL, 

	global_discount						decimal(12, 4), 
	global_subtotal						decimal(12, 4), 

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255));
go

alter table Warehouse.act.fact_customer_signature_aux_orders add constraint [PK_act_fact_customer_signature_aux_orders]
	primary key clustered (order_id_bk);
go



create table Warehouse.act.fact_customer_signature_aux_other(
	idCustomer_sk_fk					int NOT NULL, 

	idStoreCreate_sk_fk					int,
	idCalendarCreateDate_sk_fk			int,
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL, 
	idCalendarLastLoginDate_sk_fk		int,
	
	idCustomerOrigin_sk_fk				int,

	num_tot_lapsed						int, 
	num_tot_reactivate					int,

	num_tot_emails						int, 
	num_tot_text_messages				int, 
	num_tot_reviews						int, 
	num_tot_logins						int, 

	idCustomerStatusLifecycle_sk_fk		int NOT NULL,
	idCalendarStatusUpdateDate_sk_fk	int NOT NULL);
go

alter table Warehouse.act.fact_customer_signature_aux_other add constraint [PK_act_fact_customer_signature_aux_other]
	primary key clustered (idCustomer_sk_fk);
go


create table Warehouse.act.fact_customer_signature_aux_sales(	
	idCustomer_sk_fk					int NOT NULL, 
	idStoreFirstOrder_sk_fk				int,
	idStoreLastOrder_sk_fk				int,
	idCalendarFirstOrderDate_sk_fk		int,
	idCalendarLastOrderDate_sk_fk		int,
	
	num_tot_orders_web					int, 
	num_tot_orders_gen					int, 
	num_tot_orders						int NOT NULL, 
	subtotal_tot_orders					decimal(12, 4) NOT NULL,
	num_tot_cancel						int NOT NULL, 
	subtotal_tot_cancel					decimal(12, 4) NOT NULL,
	num_tot_refund						int NOT NULL, 
	subtotal_tot_refund					decimal(12, 4) NOT NULL,

	num_tot_discount_orders				int NOT NULL, 
	discount_tot_value					decimal(12, 4) NOT NULL,
	num_tot_store_credit_orders			int NOT NULL, 
	store_credit_tot_value				decimal(12, 4) NOT NULL,

	idChannelFirst_sk_fk				int, 
	idMarketingChannelFirst_sk_fk		int, 
	idPriceTypeFirst_sk_fk				int, 

	order_id_bk_first					bigint, 
	order_id_bk_last					bigint, 

	first_discount_amount				decimal(12, 4), 
	first_subtotal_amount				decimal(12, 4), 
	last_subtotal_amount				decimal(12, 4), 

	local_to_global_rate				decimal(12, 4),
	order_currency_code					varchar(255));
go

alter table Warehouse.act.fact_customer_signature_aux_sales add constraint [PK_act_fact_customer_signature_aux_sales]
	primary key clustered (idCustomer_sk_fk);
go

create table Warehouse.act.fact_customer_signature_aux_sales_main(	
	idCustomer_sk_fk					int NOT NULL, 
	idProductTypeOHMain_sk_fk			int,
	num_diff_product_type_oh			int,
	main_order_qty_time					int, 
	min_order_qty_time					int, 
	avg_order_qty_time					decimal(12, 4),
	max_order_qty_time					int,
	stdev_order_qty_time				decimal(12, 4),

	main_order_freq_time				int, 
	min_order_freq_time					int, 
	avg_order_freq_time					decimal(12, 4),
	max_order_freq_time					int,
	stdev_order_freq_time				decimal(12, 4),

	idChannelMain_sk_fk					int, 
	idMarketingChannelMain_sk_fk		int, 
	idPriceTypeMain_sk_fk				int);
go
 
alter table Warehouse.act.fact_customer_signature_aux_sales_main add constraint [PK_act_fact_customer_signature_aux_sales_main]
	primary key clustered (idCustomer_sk_fk);
go










create table Warehouse.act.fact_customer_signature_aux_sales_main_product(
	idCustomer_sk_fk					int NOT NULL, 
	idProductTypeOHMain_sk_fk			int,
	num_diff_product_type_oh			int,
	main_order_qty_time					int, 
	min_order_qty_time					int, 
	avg_order_qty_time					decimal(12, 4),
	max_order_qty_time					int, 
	stdev_order_qty_time				decimal(12, 4));

alter table Warehouse.act.fact_customer_signature_aux_sales_main_product add constraint [PK_act_fact_customer_signature_aux_sales_main_product]
	primary key clustered (idCustomer_sk_fk);
go


create table Warehouse.act.fact_customer_signature_aux_sales_main_freq(
	idCustomer_sk_fk					int NOT NULL, 
	main_order_freq_time				int, 
	min_order_freq_time					int, 
	avg_order_freq_time					decimal(12, 4),
	max_order_freq_time					int, 
	stdev_order_freq_time				decimal(12, 4));

alter table Warehouse.act.fact_customer_signature_aux_sales_main_freq add constraint [PK_act_fact_customer_signature_aux_sales_main_freq]
	primary key clustered (idCustomer_sk_fk);
go


create table Warehouse.act.fact_customer_signature_aux_sales_main_ch(
	idCustomer_sk_fk				int NOT NULL, 
	idChannelMain_sk_fk				int);

alter table Warehouse.act.fact_customer_signature_aux_sales_main_ch add constraint [PK_act_fact_customer_signature_aux_sales_main_ch]
	primary key clustered (idCustomer_sk_fk);
go


create table Warehouse.act.fact_customer_signature_aux_sales_main_mch(
	idCustomer_sk_fk				int NOT NULL, 
	idMarketingChannelMain_sk_fk	int);

alter table Warehouse.act.fact_customer_signature_aux_sales_main_mch add constraint [PK_act_fact_customer_signature_aux_sales_main_mch]
	primary key clustered (idCustomer_sk_fk);
go

	
create table Warehouse.act.fact_customer_signature_aux_sales_main_pr(
	idCustomer_sk_fk				int NOT NULL, 
	idPriceTypeMain_sk_fk			int);

alter table Warehouse.act.fact_customer_signature_aux_sales_main_pr add constraint [PK_act_fact_customer_signature_aux_sales_main_pr]
	primary key clustered (idCustomer_sk_fk);
go



create table Warehouse.act.fact_customer_signature_aux_customers(
	idCustomer_sk_fk				int NOT NULL);

alter table Warehouse.act.fact_customer_signature_aux_customers add constraint [PK_act_fact_customer_signature_aux_customers]
	primary key clustered (idCustomer_sk_fk);
go





create table Warehouse.act.dim_customer_signature_v(
	idCustomer_sk_fk				int NOT NULL, 
	customer_id						int NOT NULL, 
	customer_email					varchar(255),
	first_name						varchar(255),
	last_name						varchar(255),
	company_name_create				varchar(100),
	website_group_create			varchar(100),
	website_create					varchar(100),
	create_date						date,
	create_time_mm					int,
	website_register				varchar(100),
	register_date					date,
	customer_origin_name			varchar(50),
	market_name						varchar(100),
	country_code_ship				char(2), 
	customer_unsubscribe_name		varchar(10),
	customer_status_name			varchar(50),
	rank_num_tot_orders				char(2), 
	num_tot_orders					int,
	subtotal_tot_orders				decimal(12, 4) NOT NULL,
	num_dist_products				int,
	main_type_oh_name				varchar(50), 
	num_diff_product_type_oh		int, 
	rank_main_order_qty_time		char(2),
	main_order_qty_time				int,
	rank_avg_order_freq_time		char(2),
	avg_order_freq_time				decimal(12, 4), 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime)

alter table Warehouse.act.dim_customer_signature_v add constraint [PK_act_dim_customer_signature_v]
	primary key clustered (idCustomer_sk_fk);
go

alter table Warehouse.act.dim_customer_signature_v add constraint [DF_act_dim_customer_signature_v_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.act.dim_customer_cr_reg(
	customer_id_bk						int NOT NULL, 
	idStoreCreate_sk_fk					int NOT NULL,
	idCalendarCreateDate_sk_fk			int NOT NULL, 
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL,
	idCustomerOrigin_sk_fk				int, 
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.dim_customer_cr_reg add constraint [PK_act_dim_customer_cr_reg]
	primary key clustered (customer_id_bk);
go

alter table Warehouse.act.dim_customer_cr_reg add constraint [DF_act_dim_customer_cr_reg_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Warehouse.act.dim_customer_cr_reg_wrk(
	customer_id_bk						int NOT NULL, 
	idStoreCreate_sk_fk					int NOT NULL,
	idCalendarCreateDate_sk_fk			int NOT NULL, 
	idStoreRegister_sk_fk				int NOT NULL,
	idCalendarRegisterDate_sk_fk		int NOT NULL,
	idCustomerOrigin_sk_fk				int, 
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.dim_customer_cr_reg_wrk add constraint [DF_act_dim_customer_cr_reg_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.act.dim_rank_cr_time_mm(
	idCR_time_mm_sk				int NOT NULL, 
	cr_time_mm_bk				int NOT NULL, 
	cr_time_mm					char(2) NOT NULL,
	cr_time_name				varchar(10) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.act.dim_rank_cr_time_mm add constraint [PK_act_dim_rank_cr_time_mm]
	primary key clustered (idCR_time_mm_sk);
go

alter table Warehouse.act.dim_rank_cr_time_mm add constraint [DF_act_dim_rank_cr_time_mm_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.dim_rank_cr_time_mm add constraint [UNIQ_act_dim_rank_cr_time_mm_cr_time_mm_bk]
	unique (cr_time_mm_bk);
go



create table Warehouse.act.dim_rank_cr_time_mm_wrk(
	cr_time_mm_bk				int NOT NULL, 
	cr_time_mm					char(2) NOT NULL,
	cr_time_name				varchar(10) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go 

alter table Warehouse.act.dim_rank_cr_time_mm_wrk add constraint [DF_act_dim_rank_cr_time_mm_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Warehouse.act.fact_customer_product_signature(
	idCustomer_sk_fk					int NOT NULL, 
	idProductFamily_sk_fk				int NOT NULL, 

	idCalendarFirstOrderDate_sk_fk		int,
	idCalendarLastOrderDate_sk_fk		int,

	order_id_bk_first					bigint, 
	order_id_bk_last					bigint, 
	
	num_tot_orders						int NOT NULL, 
	subtotal_tot_orders					decimal(12, 4) NOT NULL,

	num_dist_products					int, 
	product_percentage					decimal(12, 4),
	category_percentage					decimal(12, 4),
	
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.fact_customer_product_signature add constraint [PK_act_fact_customer_product_signature]
	primary key clustered (idCustomer_sk_fk, idProductFamily_sk_fk);
go

alter table Warehouse.act.fact_customer_product_signature add constraint [DF_act_fact_customer_product_signature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.act.fact_customer_product_signature add constraint [FK_act_fact_customer_product_signature_dim_calendar_first]
	foreign key (idCalendarFirstOrderDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_customer_product_signature add constraint [FK_act_fact_customer_product_signature_dim_calendar_last]
	foreign key (idCalendarLastOrderDate_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go


create table Warehouse.act.fact_customer_product_signature_wrk(
	idCustomer_sk_fk					int NOT NULL, 
	idProductFamily_sk_fk				int NOT NULL, 

	idCalendarFirstOrderDate_sk_fk		int,
	idCalendarLastOrderDate_sk_fk		int,

	order_id_bk_first					bigint, 
	order_id_bk_last					bigint, 
	
	num_tot_orders						int NOT NULL, 
	subtotal_tot_orders					decimal(12, 4) NOT NULL,
	
	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_customer_product_signature_wrk add constraint [PK_act_fact_customer_product_signature_wrk]
	primary key clustered (idCustomer_sk_fk, idProductFamily_sk_fk);
go

alter table Warehouse.act.fact_customer_product_signature_wrk add constraint [DF_act_fact_customer_product_signature_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.act.fact_activity_login(
	idActivityLogin_sk					int NOT NULL IDENTITY(1, 1), 
	idCustomer_sk_fk					int NOT NULL,

	idCalendarFirstLogin_sk_fk			int NOT NULL, 
	idCalendarLastLogin_sk_fk			int NOT NULL, 

	num_tot_logins						int NOT NULL,

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL, 
	idETLBatchRun_upd					bigint, 
	upd_ts								datetime);
go 

alter table Warehouse.act.fact_activity_login add constraint [PK_act_fact_activity_login]
	primary key clustered (idActivityLogin_sk);
go

alter table Warehouse.act.fact_activity_login add constraint [DF_act_fact_activity_login_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


alter table Warehouse.act.fact_activity_login add constraint [UNIQ_act_fact_activity_login_customer]
	unique (idCustomer_sk_fk);
go


alter table Warehouse.act.fact_activity_login add constraint [FK_act_fact_activity_login_dim_customer]
	foreign key (idCustomer_sk_fk) references Warehouse.gen.dim_customer (idCustomer_sk);
go

alter table Warehouse.act.fact_activity_login add constraint [FK_act_fact_activity_login_dim_calendar_first]
	foreign key (idCalendarFirstLogin_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go
alter table Warehouse.act.fact_activity_login add constraint [FK_act_fact_activity_login_dim_calendar_last]
	foreign key (idCalendarLastLogin_sk_fk) references Warehouse.gen.dim_calendar (idCalendar_sk);
go




create table Warehouse.act.fact_activity_login_wrk(
	idCustomer_sk_fk					int NOT NULL,

	idCalendarFirstLogin_sk_fk			int NOT NULL, 
	idCalendarLastLogin_sk_fk			int NOT NULL, 

	num_tot_logins						int NOT NULL,

	idETLBatchRun_ins					bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go 

alter table Warehouse.act.fact_activity_login_wrk add constraint [PK_act_fact_activity_login_wrk]
	primary key clustered (idCustomer_sk_fk);
go

alter table Warehouse.act.fact_activity_login_wrk add constraint [DF_act_fact_activity_login_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 