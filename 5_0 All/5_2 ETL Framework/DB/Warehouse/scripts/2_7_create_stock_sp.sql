
use Warehouse
go



drop procedure stock.stg_dwh_merge_stock_stock_method_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Method Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_stock_method_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_stock_method_type with (tablock) as trg
	using Warehouse.stock.dim_stock_method_type_wrk src
		on (trg.stock_method_type_bk = src.stock_method_type_bk)
	when matched and not exists 
		(select isnull(trg.stock_method_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.stock_method_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.stock_method_type_name = src.stock_method_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stock_method_type_bk, stock_method_type_name, description, idETLBatchRun_ins)
				values (src.stock_method_type_bk, src.stock_method_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stock.stg_dwh_merge_stock_stock_batch_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Batch Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_stock_batch_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_stock_batch_type with (tablock) as trg
	using Warehouse.stock.dim_stock_batch_type_wrk src
		on (trg.stock_batch_type_bk = src.stock_batch_type_bk)
	when matched and not exists 
		(select isnull(trg.stock_batch_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.stock_batch_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.stock_batch_type_name = src.stock_batch_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stock_batch_type_bk, stock_batch_type_name, description, idETLBatchRun_ins)
				values (src.stock_batch_type_bk, src.stock_batch_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stock.stg_dwh_merge_stock_stock_adjustment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Adjustment Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_stock_adjustment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_stock_adjustment_type with (tablock) as trg
	using Warehouse.stock.dim_stock_adjustment_type_wrk src
		on (trg.stock_adjustment_type_bk = src.stock_adjustment_type_bk)
	when matched and not exists 
		(select isnull(trg.stock_adjustment_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.stock_adjustment_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.stock_adjustment_type_name = src.stock_adjustment_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stock_adjustment_type_bk, stock_adjustment_type_name, description, idETLBatchRun_ins)
				values (src.stock_adjustment_type_bk, src.stock_adjustment_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stock.stg_dwh_merge_stock_stock_movement_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Movement Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_stock_movement_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_stock_movement_type with (tablock) as trg
	using Warehouse.stock.dim_stock_movement_type_wrk src
		on (trg.stock_movement_type_bk = src.stock_movement_type_bk)
	when matched and not exists 
		(select isnull(trg.stock_movement_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.stock_movement_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.stock_movement_type_name = src.stock_movement_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stock_movement_type_bk, stock_movement_type_name, description, idETLBatchRun_ins)
				values (src.stock_movement_type_bk, src.stock_movement_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stock.stg_dwh_merge_stock_stock_movement_period
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Movement Period from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_stock_movement_period
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_stock_movement_period with (tablock) as trg
	using Warehouse.stock.dim_stock_movement_period_wrk src
		on (trg.stock_movement_period_bk = src.stock_movement_period_bk)
	when matched and not exists 
		(select isnull(trg.stock_movement_period_name, ''), isnull(trg.description, ''), isnull(trg.period_start, ''), isnull(trg.period_finish, '')
		intersect
		select isnull(src.stock_movement_period_name, ''), isnull(src.description, ''), isnull(src.period_start, ''), isnull(src.period_finish, ''))
		then 
			update set
				trg.stock_movement_period_name = src.stock_movement_period_name, trg.description = src.description, 
				trg.period_start = src.period_start, trg.period_finish = src.period_finish, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish, idETLBatchRun_ins)
				values (src.stock_movement_period_bk, src.stock_movement_period_name, src.description, src.period_start, src.period_finish, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure stock.stg_dwh_merge_stock_wh_stock_item
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from WH Stock Item from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_wh_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_wh_stock_item with (tablock) as trg
	using Warehouse.stock.dim_wh_stock_item_wrk src
		on (trg.warehousestockitemid_bk = src.warehousestockitemid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idWarehouse_sk_fk, 0), isnull(trg.idStockItem_sk_fk, 0), isnull(trg.idStockMethodType_sk_fk, 0), 
			isnull(trg.wh_stock_item_description, ''), 
			
			isnull(trg.qty_received, 0), isnull(trg.qty_available, 0), isnull(trg.qty_outstanding_allocation, 0), isnull(trg.qty_allocated_stock, 0), isnull(trg.qty_issued_stock, 0), 
			isnull(trg.qty_on_hold, 0), isnull(trg.qty_registered, 0), isnull(trg.qty_disposed, 0), isnull(trg.qty_due_in, 0), 
			isnull(trg.stocked, 0)
		intersect
		select 
			isnull(src.idWarehouse_sk_fk, 0), isnull(src.idStockItem_sk_fk, 0), isnull(src.idStockMethodType_sk_fk, 0), 
			isnull(src.wh_stock_item_description, ''), 
			
			isnull(src.qty_received, 0), isnull(src.qty_available, 0), isnull(src.qty_outstanding_allocation, 0), isnull(src.qty_allocated_stock, 0), isnull(src.qty_issued_stock, 0), 
			isnull(src.qty_on_hold, 0), isnull(src.qty_registered, 0), isnull(src.qty_disposed, 0), isnull(src.qty_due_in, 0), 
			isnull(src.stocked, 0))
		then 
			update set
				trg.idWarehouse_sk_fk = src.idWarehouse_sk_fk, trg.idStockItem_sk_fk = src.idStockItem_sk_fk, trg.idStockMethodType_sk_fk = src.idStockMethodType_sk_fk, 
				trg.wh_stock_item_description = src.wh_stock_item_description, 
				
				trg.qty_received = src.qty_received, trg.qty_available = src.qty_available, trg.qty_outstanding_allocation = src.qty_outstanding_allocation, 
				trg.qty_allocated_stock = src.qty_allocated_stock, trg.qty_issued_stock = src.qty_issued_stock, 
				trg.qty_on_hold = src.qty_on_hold, trg.qty_registered = src.qty_registered, trg.qty_disposed = src.qty_disposed, trg.qty_due_in = src.qty_due_in, 
				trg.stocked = src.stocked, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (warehousestockitemid_bk, 
				idWarehouse_sk_fk, idStockItem_sk_fk, idStockMethodType_sk_fk, 
				wh_stock_item_description, 
				
				qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
				qty_on_hold, qty_registered, qty_disposed, qty_due_in,
				stocked, idETLBatchRun_ins)
				
				values (src.warehousestockitemid_bk, 
					src.idWarehouse_sk_fk, src.idStockItem_sk_fk, src.idStockMethodType_sk_fk, 
					src.wh_stock_item_description, 
					
					src.qty_received, src.qty_available, src.qty_outstanding_allocation, src.qty_allocated_stock, src.qty_issued_stock, 
					src.qty_on_hold, src.qty_registered, src.qty_disposed, src.qty_due_in,
					src.stocked, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure stock.stg_dwh_merge_stock_wh_stock_item_batch
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Merges data from WH Stock Item Batch from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_wh_stock_item_batch
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.dim_wh_stock_item_batch with (tablock) as trg
	using Warehouse.stock.dim_wh_stock_item_batch_wrk src
		on (trg.warehousestockitembatchid_bk = src.warehousestockitembatchid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idWHStockItem_sk_fk, 0), isnull(trg.idStockBatchType_sk_fk, 0), 
			isnull(trg.batch_id, 0), isnull(trg.fully_allocated_f, 0), isnull(trg.fully_issued_f, 0), isnull(trg.auto_adjusted_f, 0), 

			isnull(trg.qty_received, 0), isnull(trg.qty_available, 0), isnull(trg.qty_outstanding_allocation, 0), isnull(trg.qty_allocated_stock, 0), isnull(trg.qty_issued_stock, 0), 
			isnull(trg.qty_on_hold, 0), isnull(trg.qty_registered, 0), isnull(trg.qty_disposed, 0), 
			isnull(trg.qty_registered_sm, 0), isnull(trg.qty_disposed_sm, 0), 

			isnull(trg.batch_arrived_date, ''), isnull(trg.batch_confirmed_date, ''), isnull(trg.batch_stock_register_date, ''), 
			
			isnull(trg.local_product_unit_cost, 0), isnull(trg.local_carriage_unit_cost, 0), isnull(trg.local_duty_unit_cost, 0), isnull(trg.local_total_unit_cost, 0), 
			isnull(trg.local_interco_carriage_unit_cost, 0), isnull(trg.local_total_unit_cost_interco, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.currency_code, ''), 
			isnull(trg.delete_f, '') 
		intersect
		select 
			isnull(src.idWHStockItem_sk_fk, 0), isnull(src.idStockBatchType_sk_fk, 0), 
			isnull(src.batch_id, 0), isnull(src.fully_allocated_f, 0), isnull(src.fully_issued_f, 0), isnull(src.auto_adjusted_f, 0), 

			isnull(src.qty_received, 0), isnull(src.qty_available, 0), isnull(src.qty_outstanding_allocation, 0), isnull(src.qty_allocated_stock, 0), isnull(src.qty_issued_stock, 0), 
			isnull(src.qty_on_hold, 0), isnull(src.qty_registered, 0), isnull(src.qty_disposed, 0), 
			isnull(src.qty_registered_sm, 0), isnull(src.qty_disposed_sm, 0), 

			isnull(src.batch_arrived_date, ''), isnull(src.batch_confirmed_date, ''), isnull(src.batch_stock_register_date, ''), 
			
			isnull(src.local_product_unit_cost, 0), isnull(src.local_carriage_unit_cost, 0), isnull(src.local_duty_unit_cost, 0), isnull(src.local_total_unit_cost, 0), 
			isnull(src.local_interco_carriage_unit_cost, 0), isnull(src.local_total_unit_cost_interco, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.currency_code, ''), 
			isnull(src.delete_f, ''))
		then 
			update set
				trg.idWHStockItem_sk_fk = src.idWHStockItem_sk_fk, trg.idStockBatchType_sk_fk = src.idStockBatchType_sk_fk, 
				trg.batch_id = src.batch_id, trg.fully_allocated_f = src.fully_allocated_f, trg.fully_issued_f = src.fully_issued_f, trg.auto_adjusted_f = src.auto_adjusted_f, 
				
				trg.qty_received = src.qty_received, trg.qty_available = src.qty_available, trg.qty_outstanding_allocation = src.qty_outstanding_allocation, 
				trg.qty_allocated_stock = src.qty_allocated_stock, trg.qty_issued_stock = src.qty_issued_stock, 
				trg.qty_on_hold = src.qty_on_hold, trg.qty_registered = src.qty_registered, trg.qty_disposed = src.qty_disposed, 
				trg.qty_registered_sm = src.qty_registered_sm, trg.qty_disposed_sm = src.qty_disposed_sm, 

				trg.batch_arrived_date = src.batch_arrived_date, trg.batch_confirmed_date = src.batch_confirmed_date, trg.batch_stock_register_date = src.batch_stock_register_date, 
				
				trg.local_product_unit_cost = src.local_product_unit_cost, trg.local_carriage_unit_cost = src.local_carriage_unit_cost, trg.local_duty_unit_cost = src.local_duty_unit_cost, trg.local_total_unit_cost = src.local_total_unit_cost, 
				trg.local_interco_carriage_unit_cost = src.local_interco_carriage_unit_cost, trg.local_total_unit_cost_interco = src.local_total_unit_cost_interco, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.currency_code = src.currency_code, 
				trg.delete_f = src.delete_f, 

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (warehousestockitembatchid_bk, 
				idWHStockItem_sk_fk, idStockBatchType_sk_fk, 
				batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 
				
				qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
				qty_on_hold, qty_registered, qty_disposed, 
				qty_registered_sm, qty_disposed_sm, 

				batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 
				
				local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
				local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
				local_to_global_rate, currency_code,
				delete_f, idETLBatchRun_ins)
				
				values (src.warehousestockitembatchid_bk, 
					src.idWHStockItem_sk_fk, src.idStockBatchType_sk_fk, 
					src.batch_id, src.fully_allocated_f, src.fully_issued_f, src.auto_adjusted_f, 
					
					src.qty_received, src.qty_available, src.qty_outstanding_allocation, src.qty_allocated_stock, src.qty_issued_stock, 
					src.qty_on_hold, src.qty_registered, src.qty_disposed, 
					src.qty_registered_sm, src.qty_disposed_sm, 

					src.batch_arrived_date, src.batch_confirmed_date, src.batch_stock_register_date, 
					
					src.local_product_unit_cost, src.local_carriage_unit_cost, src.local_duty_unit_cost, src.local_total_unit_cost, 
					src.local_interco_carriage_unit_cost, src.local_total_unit_cost_interco, 
					src.local_to_global_rate, src.currency_code,
					src.delete_f, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure stock.stg_dwh_merge_stock_wh_stock_item_batch_movement
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Merges data from WH Stock Item Batch Movement from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure stock.stg_dwh_merge_stock_wh_stock_item_batch_movement
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stock.fact_wh_stock_item_batch_movement with (tablock) as trg
	using Warehouse.stock.fact_wh_stock_item_batch_movement_wrk src
		on (trg.batchstockmovementid_bk = src.batchstockmovementid_bk)
	when matched and not exists 
		(select 
			isnull(trg.batch_stock_move_id, 0), isnull(trg.move_id, ''), isnull(trg.move_line_id, ''), 
			isnull(trg.idWHStockItemBatch_sk_fk, 0), isnull(trg.idStockAdjustmentType_sk_fk, 0), isnull(trg.idStockMovementType_sk_fk, 0), isnull(trg.idStockMovementPeriod_sk_fk, 0), 
			
			isnull(trg.qty_movement, 0), isnull(trg.qty_registered, 0), isnull(trg.qty_disposed, 0), 
			
			isnull(trg.batch_movement_date, ''), 
			isnull(trg.delete_f, '')
		intersect
		select 
			isnull(src.batch_stock_move_id, 0), isnull(src.move_id, ''), isnull(src.move_line_id, ''), 
			isnull(src.idWHStockItemBatch_sk_fk, 0), isnull(src.idStockAdjustmentType_sk_fk, 0), isnull(src.idStockMovementType_sk_fk, 0), isnull(src.idStockMovementPeriod_sk_fk, 0), 
			
			isnull(src.qty_movement, 0), isnull(src.qty_registered, 0), isnull(src.qty_disposed, 0), 
			
			isnull(src.batch_movement_date, ''), 
			isnull(src.delete_f, ''))
		then 
			update set
				trg.batch_stock_move_id = src.batch_stock_move_id, trg.move_id = src.move_id, trg.move_line_id = src.move_line_id, 

				trg.idWHStockItemBatch_sk_fk = src.idWHStockItemBatch_sk_fk, 
				trg.idStockAdjustmentType_sk_fk = src.idStockAdjustmentType_sk_fk, trg.idStockMovementType_sk_fk = src.idStockMovementType_sk_fk, trg.idStockMovementPeriod_sk_fk = src.idStockMovementPeriod_sk_fk, 
				
				trg.qty_movement = src.qty_movement, trg.qty_registered = src.qty_registered, trg.qty_disposed = src.qty_disposed, 
				
				trg.batch_movement_date = src.batch_movement_date, 
				trg.delete_f = src.delete_f, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (batchstockmovementid_bk, 
				batch_stock_move_id, move_id, move_line_id, 
					
				idWHStockItemBatch_sk_fk, idStockAdjustmentType_sk_fk, idStockMovementType_sk_fk, idStockMovementPeriod_sk_fk, 
					
				qty_movement, qty_registered, qty_disposed, 
				batch_movement_date, 
				delete_f, idETLBatchRun_ins)
				
				values (src.batchstockmovementid_bk, 
					src.batch_stock_move_id, src.move_id, src.move_line_id, 
					
					src.idWHStockItemBatch_sk_fk, src.idStockAdjustmentType_sk_fk, src.idStockMovementType_sk_fk, src.idStockMovementPeriod_sk_fk, 
					
					src.qty_movement, src.qty_registered, src.qty_disposed, 
					src.batch_movement_date,
					src.delete_f, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
