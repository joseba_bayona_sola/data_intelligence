use Warehouse
go


drop procedure rec.stg_dwh_merge_rec_wh_shipment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from WH Shipment Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_wh_shipment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_wh_shipment_type with (tablock) as trg
	using Warehouse.rec.dim_wh_shipment_type_wrk src
		on (trg.wh_shipment_type_bk = src.wh_shipment_type_bk)
	when matched and not exists 
		(select isnull(trg.wh_shipment_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.wh_shipment_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.wh_shipment_type_name = src.wh_shipment_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (wh_shipment_type_bk, wh_shipment_type_name, description, idETLBatchRun_ins)
				values (src.wh_shipment_type_bk, src.wh_shipment_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure rec.stg_dwh_merge_rec_receipt_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Receipt Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_receipt_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_receipt_status with (tablock) as trg
	using Warehouse.rec.dim_receipt_status_wrk src
		on (trg.receipt_status_bk = src.receipt_status_bk)
	when matched and not exists 
		(select isnull(trg.receipt_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.receipt_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.receipt_status_name = src.receipt_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (receipt_status_bk, receipt_status_name, description, idETLBatchRun_ins)
				values (src.receipt_status_bk, src.receipt_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure rec.stg_dwh_merge_rec_receipt_line_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Receipt Line Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_receipt_line_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_receipt_line_status with (tablock) as trg
	using Warehouse.rec.dim_receipt_line_status_wrk src
		on (trg.receipt_line_status_bk = src.receipt_line_status_bk)
	when matched and not exists 
		(select isnull(trg.receipt_line_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.receipt_line_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.receipt_line_status_name = src.receipt_line_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (receipt_line_status_bk, receipt_line_status_name, description, idETLBatchRun_ins)
				values (src.receipt_line_status_bk, src.receipt_line_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure rec.stg_dwh_merge_rec_receipt_line_sync_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Receipt Line Sync Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_receipt_line_sync_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_receipt_line_sync_status with (tablock) as trg
	using Warehouse.rec.dim_receipt_line_sync_status_wrk src
		on (trg.receipt_line_sync_status_bk = src.receipt_line_sync_status_bk)
	when matched and not exists 
		(select isnull(trg.receipt_line_sync_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.receipt_line_sync_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.receipt_line_sync_status_name = src.receipt_line_sync_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (receipt_line_sync_status_bk, receipt_line_sync_status_name, description, idETLBatchRun_ins)
				values (src.receipt_line_sync_status_bk, src.receipt_line_sync_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure rec.stg_dwh_merge_rec_wh_shipment
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from WH Shipment from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_wh_shipment
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_wh_shipment with (tablock) as trg
	using Warehouse.rec.dim_wh_shipment_wrk src
		on (trg.shipment_id_bk = src.shipment_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.idWarehouse_sk_fk, 0), isnull(trg.idSupplier_sk_fk, 0), isnull(trg.idWHShipmentType_sk_fk, 0), 
			isnull(trg.shipment_number, '')
		intersect
		select
			isnull(src.idWarehouse_sk_fk, 0), isnull(src.idSupplier_sk_fk, 0), isnull(src.idWHShipmentType_sk_fk, 0), 
			isnull(src.shipment_number, ''))
		then 
			update set
				trg.idWarehouse_sk_fk = src.idWarehouse_sk_fk, trg.idSupplier_sk_fk = src.idSupplier_sk_fk, trg.idWHShipmentType_sk_fk = src.idWHShipmentType_sk_fk, 
				trg.shipment_number = src.shipment_number, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (shipment_id_bk, 
				idWarehouse_sk_fk, idSupplier_sk_fk, idWHShipmentType_sk_fk, 
				shipment_number, 
				idETLBatchRun_ins)
				
				values (src.shipment_id_bk, 
					src.idWarehouse_sk_fk, src.idSupplier_sk_fk, src.idWHShipmentType_sk_fk, 
					src.shipment_number, 
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure rec.stg_dwh_merge_rec_receipt
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Receipt from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.dim_receipt with (tablock) as trg
	using Warehouse.rec.dim_receipt_wrk src
		on (trg.receiptid_bk = src.receiptid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idWHShipment_sk_fk, 0), isnull(trg.idReceiptStatus_sk_fk, 0), 
			isnull(trg.receipt_number, ''), isnull(trg.receipt_no, 0), 
			isnull(trg.created_date, ''), isnull(trg.arrived_date, ''), isnull(trg.confirmed_date, ''), isnull(trg.stock_registered_date, ''), 
			isnull(trg.local_total_cost, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.currency_code, '')
		intersect
		select
			isnull(src.idWHShipment_sk_fk, 0), isnull(src.idReceiptStatus_sk_fk, 0), 
			isnull(src.receipt_number, ''), isnull(src.receipt_no, 0), 
			isnull(src.created_date, ''), isnull(src.arrived_date, ''), isnull(src.confirmed_date, ''), isnull(src.stock_registered_date, ''), 
			isnull(src.local_total_cost, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.currency_code, ''))
		then 
			update set
				trg.idWHShipment_sk_fk = src.idWHShipment_sk_fk, trg.idReceiptStatus_sk_fk = src.idReceiptStatus_sk_fk, 
				trg.receipt_number = src.receipt_number, trg.receipt_no = src.receipt_no, 
				trg.created_date = src.created_date, trg.arrived_date = src.arrived_date, trg.confirmed_date = src.confirmed_date, trg.stock_registered_date = src.stock_registered_date, 
				trg.local_total_cost = src.local_total_cost, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.currency_code = src.currency_code,  
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (receiptid_bk, 
				idWHShipment_sk_fk, idReceiptStatus_sk_fk, 
				receipt_number, receipt_no, 
				created_date, arrived_date, confirmed_date, stock_registered_date, 
				local_total_cost, 
				local_to_global_rate, currency_code,
				idETLBatchRun_ins)
				
				values (src.receiptid_bk, 
					src.idWHShipment_sk_fk, src.idReceiptStatus_sk_fk, 
					src.receipt_number, src.receipt_no, 
					src.created_date, src.arrived_date, src.confirmed_date, src.stock_registered_date, 
					src.local_total_cost, 
					src.local_to_global_rate, src.currency_code,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure rec.stg_dwh_merge_rec_receipt_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Receipt Line from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure rec.stg_dwh_merge_rec_receipt_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.rec.fact_receipt_line with (tablock) as trg
	using Warehouse.rec.fact_receipt_line_wrk src
		on (trg.receiptlineid_bk = src.receiptlineid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idReceipt_sk_fk, 0), isnull(trg.idReceiptLineStatus_sk_fk, 0), isnull(trg.idReceiptLineSyncStatus_sk_fk, 0), 
			isnull(trg.idPurchaseOrderLine_sk_fk, 0), isnull(trg.idStockItem_sk_fk, 0), 
			isnull(trg.created_date, ''), 
			isnull(trg.qty_ordered, 0), isnull(trg.qty_received, 0), isnull(trg.qty_accepted, 0), isnull(trg.qty_returned, 0), isnull(trg.qty_rejected, 0), 
			isnull(trg.local_unit_cost, 0), isnull(trg.local_total_cost, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.currency_code, ''), 
			isnull(trg.wh_stock_item_batch_f, '')
		intersect
		select
			isnull(src.idReceipt_sk_fk, 0), isnull(src.idReceiptLineStatus_sk_fk, 0), isnull(src.idReceiptLineSyncStatus_sk_fk, 0), 
			isnull(src.idPurchaseOrderLine_sk_fk, 0), isnull(src.idStockItem_sk_fk, 0), 
			isnull(src.created_date, ''), 
			isnull(src.qty_ordered, 0), isnull(src.qty_received, 0), isnull(src.qty_accepted, 0), isnull(src.qty_returned, 0), isnull(src.qty_rejected, 0), 
			isnull(src.local_unit_cost, 0), isnull(src.local_total_cost, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.currency_code, ''), 
			isnull(src.wh_stock_item_batch_f, ''))
		then 
			update set
				trg.idReceipt_sk_fk = src.idReceipt_sk_fk, trg.idReceiptLineStatus_sk_fk = src.idReceiptLineStatus_sk_fk, trg.idReceiptLineSyncStatus_sk_fk = src.idReceiptLineSyncStatus_sk_fk, 
				trg.idPurchaseOrderLine_sk_fk = src.idPurchaseOrderLine_sk_fk, trg.idStockItem_sk_fk = src.idStockItem_sk_fk, 
				trg.created_date = src.created_date, 
				trg.qty_ordered = src.qty_ordered, trg.qty_received = src.qty_received, trg.qty_accepted = src.qty_accepted, trg.qty_returned = src.qty_returned, trg.qty_rejected = src.qty_rejected, 
				trg.local_unit_cost = src.local_unit_cost, trg.local_total_cost = src.local_total_cost, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.currency_code = src.currency_code, 
				trg.wh_stock_item_batch_f = src.wh_stock_item_batch_f, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (receiptlineid_bk, 
				idReceipt_sk_fk, idReceiptLineStatus_sk_fk, idReceiptLineSyncStatus_sk_fk, 
				idPurchaseOrderLine_sk_fk, idStockItem_sk_fk, 
				created_date, 
				qty_ordered, qty_received, qty_accepted, qty_returned, qty_rejected, 
				local_unit_cost, local_total_cost, 
				local_to_global_rate, currency_code,
				wh_stock_item_batch_f,
				idETLBatchRun_ins)
				
				values (src.receiptlineid_bk, 
					src.idReceipt_sk_fk, src.idReceiptLineStatus_sk_fk, src.idReceiptLineSyncStatus_sk_fk, 
					src.idPurchaseOrderLine_sk_fk, src.idStockItem_sk_fk, 
					src.created_date, 
					src.qty_ordered, src.qty_received, src.qty_accepted, src.qty_returned, src.qty_rejected, 
					src.local_unit_cost, src.local_total_cost, 
					src.local_to_global_rate, src.currency_code,
					src.wh_stock_item_batch_f,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 