
use Warehouse
go

select idWHShipmentType_sk, wh_shipment_type_bk, wh_shipment_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_wh_shipment_type

select wh_shipment_type_bk, wh_shipment_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_wh_shipment_type_wrk


select idReceiptStatus_sk, receipt_status_bk, receipt_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_receipt_status

select receipt_status_bk, receipt_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_receipt_status_wrk


select idReceiptLineStatus_sk, receipt_line_status_bk, receipt_line_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_receipt_line_status

select receipt_line_status_bk, receipt_line_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_receipt_line_status_wrk



select idReceiptLineSyncStatus_sk, receipt_line_sync_status_bk, receipt_line_sync_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_receipt_line_sync_status

select receipt_line_sync_status_bk, receipt_line_sync_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_receipt_line_sync_status_wrk




select idWHShipment_sk, shipment_id_bk, 
	idWarehouse_sk_fk, idSupplier_sk_fk, idWHShipmentType_sk_fk, 
	shipment_number,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_wh_shipment

select shipment_id_bk, 
	idWarehouse_sk_fk, idSupplier_sk_fk, idWHShipmentType_sk_fk, 
	shipment_number,
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_wh_shipment_wrk



select idReceipt_sk, receiptid_bk, 
	idWHShipment_sk_fk, idReceiptStatus_sk_fk, 
	receipt_number, receipt_no, 
	created_date, arrived_date, confirmed_date, stock_registered_date, 
	local_total_cost, 
	local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.dim_receipt

select receiptid_bk, 
	idWHShipment_sk_fk, idReceiptStatus_sk_fk, 
	receipt_number, receipt_no, 
	created_date, arrived_date, confirmed_date, stock_registered_date, 
	local_total_cost, 
	local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.dim_receipt_wrk



select idReceiptLine_sk, receiptlineid_bk, 
	idReceipt_sk_fk, idReceiptLineStatus_sk_fk, idReceiptLineSyncStatus_sk_fk, 
	idPurchaseOrderLine_sk_fk, idStockItem_sk_fk, 
	created_date, 
	qty_ordered, qty_received, qty_accepted, qty_returned, qty_rejected, 
	local_unit_cost, local_total_cost, 
	local_to_global_rate, currency_code,
	wh_stock_item_batch_f,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.rec.fact_receipt_line

select receiptlineid_bk, 
	idReceipt_sk_fk, idReceiptLineStatus_sk_fk, idReceiptLineSyncStatus_sk_fk, 
	idPurchaseOrderLine_sk_fk, idStockItem_sk_fk, 
	created_date, 
	qty_ordered, qty_received, qty_accepted, qty_returned, qty_rejected, 
	local_unit_cost, local_total_cost, 
	local_to_global_rate, currency_code,
	wh_stock_item_batch_f, 
	idETLBatchRun_ins, ins_ts
from Warehouse.rec.fact_receipt_line_wrk

