
use Warehouse
go

select idPOSource_sk, po_source_bk, po_source_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_po_source

select po_source_bk, po_source_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_po_source_wrk



select idPOType_sk, po_type_bk, po_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_po_type

select po_type_bk, po_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_po_type_wrk



select idPOStatus_sk, po_status_bk, po_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_po_status

select po_status_bk, po_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_po_status_wrk



select idPOLStatus_sk, pol_status_bk, pol_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_pol_status

select pol_status_bk, pol_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_pol_status_wrk



select idPOLProblems_sk, pol_problems_bk, pol_problems_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_pol_problems

select pol_problems_bk, pol_problems_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_pol_problems_wrk


select idWHOperator_sk, wh_operator_bk, wh_operator_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_wh_operator

select wh_operator_bk, wh_operator_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_wh_operator_wrk



select idPurchaseOrder_sk, purchaseorderid_bk, 
	idWarehouse_sk_fk, idSupplier_sk_fk, 
	idPOSource_sk_fk, idPOType_sk_fk, idPOStatus_sk_fk, idWHOperatorCreate_sk_fk, idWHOperatorAssigned_sk_fk, 
	purchase_order_number, leadtime, 
	created_date, approved_date, confirmed_date, submitted_date, completed_date, due_date, 
	local_total_cost, local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.dim_purchase_order

select purchaseorderid_bk, 
	idWarehouse_sk_fk, idSupplier_sk_fk, 
	idPOSource_sk_fk, idPOType_sk_fk, idPOStatus_sk_fk, idWHOperatorCreate_sk_fk, idWHOperatorAssigned_sk_fk, 
	purchase_order_number, leadtime, 
	created_date, approved_date, confirmed_date, submitted_date, completed_date, due_date, 
	local_total_cost, local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts
from Warehouse.po.dim_purchase_order_wrk



select idPurchaseOrderLine_sk, purchaseorderlineid_bk, 
	idPurchaseOrder_sk_fk, idPOLStatus_sk_fk, idPOLProblems_sk_fk, 
	idStockItem_sk_fk, idProductFamilyPrice_sk_fk, 
	purchase_order_line_id, 
	quantity_ordered, quantity_received, quantity_planned, quantity_open, quantity_cancelled, 
	local_unit_cost, local_line_cost, local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.po.fact_purchase_order_line

select purchaseorderlineid_bk, 
	idPurchaseOrder_sk_fk, idPOLStatus_sk_fk, idPOLProblems_sk_fk, 
	idStockItem_sk_fk, idProductFamilyPrice_sk_fk, 
	purchase_order_line_id, 
	quantity_ordered, quantity_received, quantity_planned, quantity_open, quantity_cancelled, 
	local_unit_cost, local_line_cost, local_to_global_rate, currency_code,
	idETLBatchRun_ins, ins_ts
from Warehouse.po.fact_purchase_order_line_wrk