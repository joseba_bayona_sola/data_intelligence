use Warehouse
go 

create table Warehouse.prod.dim_product_type(
	idProductType_sk			int NOT NULL IDENTITY(1, 1), 
	product_type_bk				varchar(50) NOT NULL, 
	product_type_name			varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_type add constraint [PK_prod_dim_product_type]
	primary key clustered (idProductType_sk);
go

alter table Warehouse.prod.dim_product_type add constraint [DF_prod_dim_product_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_type add constraint [UNIQ_prod_dim_product_type_product_type_bk]
	unique (product_type_bk);
go


create table Warehouse.prod.dim_product_type_wrk(
	product_type_bk				varchar(50) NOT NULL, 
	product_type_name			varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_type_wrk add constraint [DF_prod_dim_product_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_category(
	idCategory_sk				int NOT NULL IDENTITY(1, 1), 
	category_bk					varchar(50) NOT NULL,
	category_name				varchar(50) NOT NULL,
	idProductType_sk_fk			int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_category add constraint [PK_prod_dim_category]
	primary key clustered (idCategory_sk);
go

alter table Warehouse.prod.dim_category add constraint [DF_prod_dim_category_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_category add constraint [UNIQ_prod_dim_category_category_bk]
	unique (category_bk);
go

alter table Warehouse.prod.dim_category add constraint [FK_prod_dim_category_dim_product_type]
	foreign key (idProductType_sk_fk) references Warehouse.prod.dim_product_type (idProductType_sk);
go

create table Warehouse.prod.dim_category_wrk(
	category_bk					varchar(50) NOT NULL,
	category_name				varchar(50) NOT NULL,
	idProductType_sk_fk			int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_category_wrk add constraint [DF_prod_dim_category_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_cl_type(
	idCL_Type_sk				int NOT NULL IDENTITY(1, 1), 
	cl_type_bk					varchar(50) NOT NULL, 
	cl_type_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_cl_type add constraint [PK_prod_dim_cl_type]
	primary key clustered (idCL_Type_sk);
go

alter table Warehouse.prod.dim_cl_type add constraint [DF_prod_dim_cl_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_cl_type add constraint [UNIQ_prod_dim_cl_type_cl_type_bk]
	unique (cl_type_bk);
go


create table Warehouse.prod.dim_cl_type_wrk(
	cl_type_bk					varchar(50) NOT NULL, 
	cl_type_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_cl_type_wrk add constraint [DF_prod_dim_cl_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_cl_feature(
	idCL_Feature_sk				int NOT NULL IDENTITY(1, 1), 
	cl_feature_bk				varchar(50) NOT NULL, 
	cl_feature_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_cl_feature add constraint [PK_prod_dim_cl_feature]
	primary key clustered (idCL_Feature_sk);
go

alter table Warehouse.prod.dim_cl_feature add constraint [DF_prod_dim_cl_feature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_cl_feature add constraint [UNIQ_prod_dim_cl_feature_cl_feature_bk]
	unique (cl_feature_bk);
go


create table Warehouse.prod.dim_cl_feature_wrk(
	cl_feature_bk				varchar(50) NOT NULL, 
	cl_feature_name				varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_cl_feature_wrk add constraint [DF_prod_dim_cl_feature_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_manufacturer(
	idManufacturer_sk			int NOT NULL IDENTITY(1, 1), 
	manufacturer_bk				int NOT NULL, 
	manufacturer_name			varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_manufacturer add constraint [PK_prod_dim_manufacturer]
	primary key clustered (idManufacturer_sk);
go

alter table Warehouse.prod.dim_manufacturer add constraint [DF_prod_dim_manufacturer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_manufacturer add constraint [UNIQ_prod_dim_manufacturer_manufacturer_bk]
	unique (manufacturer_bk);
go


create table Warehouse.prod.dim_manufacturer_wrk(
	manufacturer_bk				int NOT NULL, 
	manufacturer_name			varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_manufacturer_wrk add constraint [DF_prod_dim_manufacturer_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_product_lifecycle(
	idProductLifecycle_sk		int NOT NULL IDENTITY(1, 1), 
	product_lifecycle_bk		varchar(50) NOT NULL, 
	product_lifecycle_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_lifecycle add constraint [PK_prod_dim_product_lifecycle]
	primary key clustered (idProductLifecycle_sk);
go

alter table Warehouse.prod.dim_product_lifecycle add constraint [DF_prod_dim_product_lifecycle_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_lifecycle add constraint [UNIQ_prod_dim_product_lifecycle_product_lifecycle_bk]
	unique (product_lifecycle_bk);
go


create table Warehouse.prod.dim_product_lifecycle_wrk(
	product_lifecycle_bk		varchar(50) NOT NULL, 
	product_lifecycle_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_lifecycle_wrk add constraint [DF_prod_dim_product_lifecycle_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_product_visibility(
	idProductVisibility_sk		int NOT NULL IDENTITY(1, 1), 
	visibility_id_bk			int NOT NULL, 
	product_visibility_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_visibility add constraint [PK_prod_dim_product_visibility]
	primary key clustered (idProductVisibility_sk);
go

alter table Warehouse.prod.dim_product_visibility add constraint [DF_prod_dim_product_visibility_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_visibility add constraint [UNIQ_prod_dim_product_visibility_product_visibility_bk]
	unique (visibility_id_bk);
go


create table Warehouse.prod.dim_product_visibility_wrk(
	visibility_id_bk			int NOT NULL, 
	product_visibility_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_visibility_wrk add constraint [DF_prod_dim_product_visibility_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_glass_vision_type(
	idGlassVisionType_sk		int NOT NULL IDENTITY(1, 1), 
	glass_vision_type_bk		varchar(50) NOT NULL, 
	glass_vision_type_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_glass_vision_type add constraint [PK_prod_dim_glass_vision_type]
	primary key clustered (idGlassVisionType_sk);
go

alter table Warehouse.prod.dim_glass_vision_type add constraint [DF_prod_dim_glass_vision_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_glass_vision_type add constraint [UNIQ_prod_dim_glass_vision_type_glass_vision_type_bk]
	unique (glass_vision_type_bk);
go


create table Warehouse.prod.dim_glass_vision_type_wrk(
	glass_vision_type_bk		varchar(50) NOT NULL, 
	glass_vision_type_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_glass_vision_type_wrk add constraint [DF_prod_dim_glass_vision_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_glass_package_type(
	idGlassPackageType_sk		int NOT NULL IDENTITY(1, 1), 
	glass_package_type_bk		varchar(50) NOT NULL, 
	glass_package_type_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_glass_package_type add constraint [PK_prod_dim_glass_package_type]
	primary key clustered (idGlassPackageType_sk);
go

alter table Warehouse.prod.dim_glass_package_type add constraint [DF_prod_dim_glass_package_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_glass_package_type add constraint [UNIQ_prod_dim_glass_package_type_glass_package_type_bk]
	unique (glass_package_type_bk);
go


create table Warehouse.prod.dim_glass_package_type_wrk(
	glass_package_type_bk		varchar(50) NOT NULL, 
	glass_package_type_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_glass_package_type_wrk add constraint [DF_prod_dim_glass_package_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_param_BC(
	idBC_sk						char(3) NOT NULL,  
	base_curve_bk				char(3) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_BC add constraint [PK_prod_dim_param_BC]
	primary key clustered (idBC_sk);
go

alter table Warehouse.prod.dim_param_BC add constraint [DF_prod_dim_param_BC_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_BC add constraint [UNIQ_prod_dim_param_BC_base_curve_bk]
	unique (base_curve_bk);
go


create table Warehouse.prod.dim_param_BC_wrk(
	base_curve_bk				char(3) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_BC_wrk add constraint [DF_prod_dim_param_BC_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_param_DI(
	idDI_sk						char(4) NOT NULL,  
	diameter_bk					char(4) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_DI add constraint [PK_prod_dim_param_DI]
	primary key clustered (idDI_sk);
go

alter table Warehouse.prod.dim_param_DI add constraint [DF_prod_dim_param_DI_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_DI add constraint [UNIQ_prod_dim_param_DI_base_curve_bk]
	unique (diameter_bk);
go


create table Warehouse.prod.dim_param_DI_wrk(
	diameter_bk					char(4) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_DI_wrk add constraint [DF_prod_dim_param_DI_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_param_PO(
	idPO_sk						char(6) NOT NULL,  
	power_bk					char(6) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_PO add constraint [PK_prod_dim_param_PO]
	primary key clustered (idPO_sk);
go

alter table Warehouse.prod.dim_param_PO add constraint [DF_prod_dim_param_PO_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_PO add constraint [UNIQ_prod_dim_param_PO_base_curve_bk]
	unique (power_bk);
go


create table Warehouse.prod.dim_param_PO_wrk(
	power_bk					char(6) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_PO_wrk add constraint [DF_prod_dim_param_PO_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.prod.dim_param_CY(
	idCY_sk						char(5) NOT NULL,  
	cylinder_bk					char(5) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_CY add constraint [PK_prod_dim_dim_param_CY]
	primary key clustered (idCY_sk);
go

alter table Warehouse.prod.dim_param_CY add constraint [DF_prod_dim_param_CY_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_CY add constraint [UNIQ_prod_dim_param_CY_base_curve_bk]
	unique (cylinder_bk);
go


create table Warehouse.prod.dim_param_CY_wrk(
	cylinder_bk					char(5) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_CY_wrk add constraint [DF_prod_dim_param_CY_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_param_AX(
	idAX_sk						char(3) NOT NULL,  
	axis_bk						char(3) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_AX add constraint [PK_prod_dim_param_AX]
	primary key clustered (idAX_sk);
go

alter table Warehouse.prod.dim_param_AX add constraint [DF_prod_dim_param_AX_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_AX add constraint [UNIQ_prod_dim_param_AX_base_curve_bk]
	unique (axis_bk);
go


create table Warehouse.prod.dim_param_AX_wrk(
	axis_bk						char(3) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_AX_wrk add constraint [DF_prod_dim_param_AX_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.prod.dim_param_AD(
	idAD_sk						char(4) NOT NULL,  
	addition_bk					char(4) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_AD add constraint [PK_prod_dim_param_AD]
	primary key clustered (idAD_sk);
go

alter table Warehouse.prod.dim_param_AD add constraint [DF_prod_dim_param_AD_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_AD add constraint [UNIQ_prod_dim_param_AD_base_curve_bk]
	unique (addition_bk);
go


create table Warehouse.prod.dim_param_AD_wrk(
	addition_bk					char(4) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_AD_wrk add constraint [DF_prod_dim_param_AD_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

--------------------------------------------------------

create table Warehouse.prod.dim_param_DO(
	idDO_sk						char(1) NOT NULL,  
	dominance_bk				char(1) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_DO add constraint [PK_prod_dim_param_DO]
	primary key clustered (idDO_sk);
go

alter table Warehouse.prod.dim_param_DO add constraint [DF_prod_dim_param_DO_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_DO add constraint [UNIQ_prod_dim_param_DO_base_curve_bk]
	unique (dominance_bk);
go


create table Warehouse.prod.dim_param_DO_wrk(
	dominance_bk				char(1) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_DO_wrk add constraint [DF_prod_dim_param_DO_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_param_COL(
	idCOL_sk					int NOT NULL IDENTITY(1, 1), 
	colour_bk					varchar(50) NOT NULL, 
	colour_name					varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_param_COL add constraint [PK_prod_dim_param_COL]
	primary key clustered (idCOL_sk);
go

alter table Warehouse.prod.dim_param_COL add constraint [DF_prod_dim_param_COL_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_param_COL add constraint [UNIQ_prod_dim_param_COL_base_curve_bk]
	unique (colour_bk);
go


create table Warehouse.prod.dim_param_COL_wrk(
	colour_bk					varchar(50) NOT NULL, 
	colour_name					varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_param_COL_wrk add constraint [DF_prod_dim_param_COL_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.prod.dim_product_family(
	idProductFamily_sk			int NOT NULL IDENTITY(1, 1), 
	product_id_bk				int NOT NULL, 
	idManufacturer_sk_fk		int, 
	idCategory_sk_fk			int NOT NULL, 
	idCL_Type_sk_fk				int, 
	idCL_Feature_sk_fk			int, 
	idProductLifecycle_sk_fk	int NOT NULL, 
	idProductVisibility_sk_fk	int NOT NULL, 
	idProductFamilyGroup_sk_fk	int, 
	idProductTypeOH_sk_fk 		int,
	magento_sku					varchar(50), 
	product_family_code			char(5),
	product_family_name			varchar(255),
	glass_sunglass_name			varchar(255),
	glass_sunglass_colour		varchar(255),
	status						tinyint NOT NULL,
	promotional_product			tinyint NOT NULL,
	telesales_product			tinyint NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_family add constraint [PK_prod_dim_product_family]
	primary key clustered (idProductFamily_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [DF_prod_dim_product_family_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_family add constraint [UNIQ_prod_product_family_product_id_bk]
	unique (product_id_bk);
go


alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_manufacturer]
	foreign key (idManufacturer_sk_fk) references Warehouse.prod.dim_manufacturer (idManufacturer_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_category]
	foreign key (idCategory_sk_fk) references Warehouse.prod.dim_category (idCategory_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_cl_type]
	foreign key (idCL_Type_sk_fk) references Warehouse.prod.dim_cl_type (idCL_Type_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_cl_feature]
	foreign key (idCL_Feature_sk_fk) references Warehouse.prod.dim_cl_feature (idCL_Feature_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_product_lifecycle]
	foreign key (idProductLifecycle_sk_fk) references Warehouse.prod.dim_product_lifecycle (idProductLifecycle_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_product_visibility]
	foreign key (idProductVisibility_sk_fk) references Warehouse.prod.dim_product_visibility (idProductVisibility_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_product_family_group]
	foreign key (idProductFamilyGroup_sk_fk) references Warehouse.prod.dim_product_family_group (idProductFamilyGroup_sk);
go

alter table Warehouse.prod.dim_product_family add constraint [FK_prod_dim_product_family_dim_product_type_oh]
	foreign key (idProductTypeOH_sk_fk) references Warehouse.prod.dim_product_type_oh (idProductTypeOH_sk);
go


create table Warehouse.prod.dim_product_family_wrk(
	product_id_bk				int NOT NULL, 
	idManufacturer_sk_fk		int, 
	idCategory_sk_fk			int NOT NULL, 
	idCL_Type_sk_fk				int, 
	idCL_Feature_sk_fk			int, 
	idProductLifecycle_sk_fk	int NOT NULL, 
	idProductVisibility_sk_fk	int NOT NULL, 
	idProductFamilyGroup_sk_fk	int,
	idProductTypeOH_sk_fk 		int,
	magento_sku					varchar(50), 
	product_family_code			char(5),
	product_family_name			varchar(255),
	glass_sunglass_name			varchar(255),
	glass_sunglass_colour		varchar(255),
	status						tinyint NOT NULL,
	promotional_product			tinyint NOT NULL,
	telesales_product			tinyint NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_family_wrk add constraint [DF_prod_dim_product_family_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.prod.dim_product_type_oh(
	idProductTypeOH_sk			int NOT NULL IDENTITY(1, 1), 
	product_type_oh_bk			varchar(50) NOT NULL, 
	product_type_oh_name		varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_type_oh add constraint [PK_prod_dim_product_type_oh]
	primary key clustered (idProductTypeOH_sk);
go

alter table Warehouse.prod.dim_product_type_oh add constraint [DF_prod_dim_product_type_oh_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_type_oh add constraint [UNIQ_prod_dim_product_type_oh_product_type_bk]
	unique (product_type_oh_bk);
go


create table Warehouse.prod.dim_product_type_oh_wrk(
	product_type_oh_bk			varchar(50) NOT NULL, 
	product_type_oh_name		varchar(50) NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_type_oh_wrk add constraint [DF_prod_dim_product_type_oh_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.prod.dim_product_family_group(
	idProductFamilyGroup_sk		int NOT NULL IDENTITY(1, 1), 
	product_family_group_bk		varchar(50) NOT NULL, 
	product_family_group_name	varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go

alter table Warehouse.prod.dim_product_family_group add constraint [PK_prod_dim_product_family_group]
	primary key clustered (idProductFamilyGroup_sk);
go

alter table Warehouse.prod.dim_product_family_group add constraint [DF_prod_dim_product_family_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_family_group add constraint [UNIQ_prod_dim_product_family_group_product_family_group_bk]
	unique (product_family_group_bk);
go


create table Warehouse.prod.dim_product_family_group_wrk(
	product_family_group_bk		varchar(50) NOT NULL, 
	product_family_group_name	varchar(50) NOT NULL, 
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_family_group_wrk add constraint [DF_prod_dim_product_family_group_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Warehouse.prod.dim_product_family_pack_size(
	idProductFamilyPackSize_sk		int NOT NULL IDENTITY(1, 1), 
	packsizeid_bk					bigint NOT NULL,
	idProductFamily_sk_fk			int, 
	size							int,
	product_family_packsize_name	varchar(200),
	allocation_preference_order		int,
	purchasing_preference_order		int,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go

alter table Warehouse.prod.dim_product_family_pack_size add constraint [PK_prod_dim_product_family_pack_size]
	primary key clustered (idProductFamilyPackSize_sk);
go

alter table Warehouse.prod.dim_product_family_pack_size add constraint [DF_prod_dim_product_family_pack_size_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_family_pack_size add constraint [UNIQ_prod_dim_product_family_pack_size_packsizeid_bk]
	unique (packsizeid_bk);
go

alter table Warehouse.prod.dim_product_family_pack_size add constraint [FK_prod_dim_product_family_pack_size_dim_product_family]
	foreign key (idProductFamily_sk_fk) references Warehouse.prod.dim_product_family (idProductFamily_sk);
go


create table Warehouse.prod.dim_product_family_pack_size_wrk(
	packsizeid_bk					bigint NOT NULL,
	idProductFamily_sk_fk			int, 
	size							int,
	product_family_packsize_name	varchar(200),
	allocation_preference_order		int,
	purchasing_preference_order		int,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_family_pack_size_wrk add constraint [DF_prod_dim_product_family_pack_size_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Warehouse.prod.dim_price_type_pf(
	idPriceTypePF_sk				int NOT NULL IDENTITY(1, 1), 
	price_type_pf_bk				varchar(50) NOT NULL,
	price_type_pf_name				varchar(50) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go

alter table Warehouse.prod.dim_price_type_pf add constraint [PK_prod_dim_price_type_pf]
	primary key clustered (idPriceTypePF_sk);
go

alter table Warehouse.prod.dim_price_type_pf add constraint [DF_prod_dim_price_type_pf_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_price_type_pf add constraint [UNIQ_prod_dim_price_type_pf_price_type_pf_bk]
	unique (price_type_pf_bk);
go




create table Warehouse.prod.dim_price_type_pf_wrk(
	price_type_pf_bk				varchar(50) NOT NULL,
	price_type_pf_name				varchar(50) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Warehouse.prod.dim_price_type_pf_wrk add constraint [DF_prod_dim_price_type_pf_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Warehouse.prod.dim_product_family_price(
	idProductFamilyPrice_sk			int NOT NULL IDENTITY(1, 1), 
	supplierpriceid_bk				bigint NOT NULL,
	
	idSupplier_sk_fk				int NOT NULL, 
	idProductFamilyPackSize_sk_fk	int NOT NULL,
	idPriceTypePF_sk_fk				int NOT NULL,

	unit_price						decimal(12, 4), 
	currency_code					varchar(4),
	lead_time						int,
	effective_date					date,
	expiry_date						date,
	active							char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go

alter table Warehouse.prod.dim_product_family_price add constraint [PK_prod_dim_product_family_price]
	primary key clustered (idProductFamilyPrice_sk);
go

alter table Warehouse.prod.dim_product_family_price add constraint [DF_prod_dim_product_family_price_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product_family_price add constraint [UNIQ_prod_dim_product_family_price_supplierpriceid_bk]
	unique (supplierpriceid_bk);
go

alter table Warehouse.prod.dim_product_family_price add constraint [FK_prod_dim_product_family_price_dim_supplier]
	foreign key (idSupplier_sk_fk) references Warehouse.gen.dim_supplier (idSupplier_sk);
go

alter table Warehouse.prod.dim_product_family_price add constraint [FK_prod_dim_product_family_price_dim_product_family_pack_size]
	foreign key (idProductFamilyPackSize_sk_fk) references Warehouse.prod.dim_product_family_pack_size (idProductFamilyPackSize_sk);
go

alter table Warehouse.prod.dim_product_family_price add constraint [FK_prod_dim_product_family_price_dim_price_type_pf]
	foreign key (idPriceTypePF_sk_fk) references Warehouse.prod.dim_price_type_pf (idPriceTypePF_sk);
go



create table Warehouse.prod.dim_product_family_price_wrk(
	supplierpriceid_bk				bigint NOT NULL,
	
	idSupplier_sk_fk				int NOT NULL, 
	idProductFamilyPackSize_sk_fk	int NOT NULL,
	idPriceTypePF_sk_fk				int NOT NULL,

	unit_price						decimal(12, 4), 
	currency_code					varchar(4),
	lead_time						int,
	effective_date					date,
	expiry_date						date,
	active							char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_family_price_wrk add constraint [DF_prod_dim_product_family_price_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Warehouse.prod.dim_product(
	idProduct_sk					int NOT NULL IDENTITY(1, 1), 
	productid_erp_bk				bigint NOT NULL,
	
	idProductFamily_sk_fk			int NOT NULL, 
	idBC_sk_fk						char(3), 
	idDI_sk_fk						char(4), 
	idPO_sk_fk						char(6), 
	idCY_sk_fk						char(5), 
	idAX_sk_fk						char(3), 
	idAD_sk_fk						char(4), 
	idDO_sk_fk						char(1),
	idCOL_sk_fk						int, 

	parameter						varchar(200),
	product_description				varchar(200),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go

alter table Warehouse.prod.dim_product add constraint [PK_prod_dim_product]
	primary key clustered (idProduct_sk);
go

alter table Warehouse.prod.dim_product add constraint [DF_prod_dim_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_product add constraint [UNIQ_prod_dim_product_productid_erp_bk]
	unique (productid_erp_bk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_product_family]
	foreign key (idProductFamily_sk_fk) references Warehouse.prod.dim_product_family (idProductFamily_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_bc]
	foreign key (idBC_sk_fk) references Warehouse.prod.dim_param_bc (idBC_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_di]
	foreign key (idDI_sk_fk) references Warehouse.prod.dim_param_di (idDI_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_po]
	foreign key (idPO_sk_fk) references Warehouse.prod.dim_param_po (idPO_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_cy]
	foreign key (idCY_sk_fk) references Warehouse.prod.dim_param_cy (idCY_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_ax]
	foreign key (idAX_sk_fk) references Warehouse.prod.dim_param_ax (idAX_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_ad]
	foreign key (idAD_sk_fk) references Warehouse.prod.dim_param_ad (idAD_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_do]
	foreign key (idDO_sk_fk) references Warehouse.prod.dim_param_do (idDO_sk);
go

alter table Warehouse.prod.dim_product add constraint [FK_prod_dim_product_dim_param_col]
	foreign key (idCOL_sk_fk) references Warehouse.prod.dim_param_col (idCOL_sk);
go



create table Warehouse.prod.dim_product_wrk(
	productid_erp_bk				bigint NOT NULL,
	
	idProductFamily_sk_fk			int NOT NULL, 
	idBC_sk_fk						char(3), 
	idDI_sk_fk						char(4), 
	idPO_sk_fk						char(6), 
	idCY_sk_fk						char(5), 
	idAX_sk_fk						char(3), 
	idAD_sk_fk						char(4), 
	idDO_sk_fk						char(1),
	idCOL_sk_fk						int, 

	parameter						varchar(200),
	product_description				varchar(200),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Warehouse.prod.dim_product_wrk add constraint [DF_prod_dim_product_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





create table Warehouse.prod.dim_stock_item(
	idStockItem_sk					int NOT NULL IDENTITY(1, 1), 
	stockitemid_bk					bigint NOT NULL,
	
	idProduct_sk_fk					int NOT NULL, 
	packsize						int, 

	SKU								varchar(200), 
	stock_item_description			varchar(200),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go

alter table Warehouse.prod.dim_stock_item add constraint [PK_prod_dim_stock_item]
	primary key clustered (idStockItem_sk);
go

alter table Warehouse.prod.dim_stock_item add constraint [DF_prod_dim_stock_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.prod.dim_stock_item add constraint [UNIQ_prod_dim_stock_item_stockitemid_bk]
	unique (stockitemid_bk);
go

alter table Warehouse.prod.dim_stock_item add constraint [FK_prod_dim_stock_item_dim_product]
	foreign key (idProduct_sk_fk) references Warehouse.prod.dim_product (idProduct_sk);
go


create table Warehouse.prod.dim_stock_item_wrk(
	stockitemid_bk					bigint NOT NULL,
	
	idProduct_sk_fk					int NOT NULL, 
	packsize						int, 

	SKU								varchar(200), 
	stock_item_description			varchar(200),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Warehouse.prod.dim_stock_item_wrk add constraint [DF_prod_dim_stock_item_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
