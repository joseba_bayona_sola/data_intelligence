
use Warehouse
go 


drop procedure prod.stg_dwh_merge_prod_product_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_type with (tablock) as trg
	using Warehouse.prod.dim_product_type_wrk src
		on (trg.product_type_bk = src.product_type_bk)
	when matched and not exists 
		(select isnull(trg.product_type_name, '') 
		intersect
		select isnull(src.product_type_name, ''))
		then 
			update set
				trg.product_type_name = src.product_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (product_type_bk, product_type_name, idETLBatchRun_ins)
				values (src.product_type_bk, src.product_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Category from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_category
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_category with (tablock) as trg
	using Warehouse.prod.dim_category_wrk src
		on (trg.category_bk = src.category_bk)
	when matched and not exists 
		(select isnull(trg.category_name, ''), isnull(trg.idProductType_sk_fk, 0) 
		intersect
		select isnull(src.category_name, ''), isnull(src.idProductType_sk_fk, 0))
		then 
			update set
				trg.category_name = src.category_name, 
				trg.idProductType_sk_fk = src.idProductType_sk_fk, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (category_bk, category_name, idProductType_sk_fk, idETLBatchRun_ins)
				values (src.category_bk, src.category_name, src.idProductType_sk_fk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_cl_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from CL Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_cl_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_cl_type with (tablock) as trg
	using Warehouse.prod.dim_cl_type_wrk src
		on (trg.cl_type_bk = src.cl_type_bk)
	when matched and not exists 
		(select isnull(trg.cl_type_name, '') 
		intersect
		select isnull(src.cl_type_name, ''))
		then 
			update set
				trg.cl_type_name = src.cl_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (cl_type_bk, cl_type_name, idETLBatchRun_ins)
				values (src.cl_type_bk, src.cl_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_cl_feature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from CL Feature from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_cl_feature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_cl_feature with (tablock) as trg
	using Warehouse.prod.dim_cl_feature_wrk src
		on (trg.cl_feature_bk = src.cl_feature_bk)
	when matched and not exists 
		(select isnull(trg.cl_feature_name, '') 
		intersect
		select isnull(src.cl_feature_name, ''))
		then 
			update set
				trg.cl_feature_name = src.cl_feature_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (cl_feature_bk, cl_feature_name, idETLBatchRun_ins)
				values (src.cl_feature_bk, src.cl_feature_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure prod.stg_dwh_merge_prod_manufacturer
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Manufacturer from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_manufacturer
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_manufacturer with (tablock) as trg
	using Warehouse.prod.dim_manufacturer_wrk src
		on (trg.manufacturer_bk = src.manufacturer_bk)
	when matched and not exists 
		(select isnull(trg.manufacturer_name, '') 
		intersect
		select isnull(src.manufacturer_name, ''))
		then 
			update set
				trg.manufacturer_name = src.manufacturer_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (manufacturer_bk, manufacturer_name, idETLBatchRun_ins)
				values (src.manufacturer_bk, src.manufacturer_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure prod.stg_dwh_merge_prod_product_lifecycle
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Lifecycle from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_lifecycle
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_lifecycle with (tablock) as trg
	using Warehouse.prod.dim_product_lifecycle_wrk src
		on (trg.product_lifecycle_bk = src.product_lifecycle_bk)
	when matched and not exists 
		(select isnull(trg.product_lifecycle_name, '') 
		intersect
		select isnull(src.product_lifecycle_name, ''))
		then 
			update set
				trg.product_lifecycle_name = src.product_lifecycle_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (product_lifecycle_bk, product_lifecycle_name, idETLBatchRun_ins)
				values (src.product_lifecycle_bk, src.product_lifecycle_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_product_visibility
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Visibility from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_visibility
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_visibility with (tablock) as trg
	using Warehouse.prod.dim_product_visibility_wrk src
		on (trg.visibility_id_bk = src.visibility_id_bk)
	when matched and not exists 
		(select isnull(trg.product_visibility_name, '') 
		intersect
		select isnull(src.product_visibility_name, ''))
		then 
			update set
				trg.product_visibility_name = src.product_visibility_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (visibility_id_bk, product_visibility_name, idETLBatchRun_ins)
				values (src.visibility_id_bk, src.product_visibility_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure prod.stg_dwh_merge_prod_glass_vision_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Glass Vision Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_glass_vision_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_glass_vision_type with (tablock) as trg
	using Warehouse.prod.dim_glass_vision_type_wrk src
		on (trg.glass_vision_type_bk = src.glass_vision_type_bk)
	when matched and not exists 
		(select isnull(trg.glass_vision_type_name, '') 
		intersect
		select isnull(src.glass_vision_type_name, ''))
		then 
			update set
				trg.glass_vision_type_name = src.glass_vision_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (glass_vision_type_bk, glass_vision_type_name, idETLBatchRun_ins)
				values (src.glass_vision_type_bk, src.glass_vision_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_glass_package_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Glass Package Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_glass_package_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_glass_package_type with (tablock) as trg
	using Warehouse.prod.dim_glass_package_type_wrk src
		on (trg.glass_package_type_bk = src.glass_package_type_bk)
	when matched and not exists 
		(select isnull(trg.glass_package_type_name, '') 
		intersect
		select isnull(src.glass_package_type_name, ''))
		then 
			update set
				trg.glass_package_type_name = src.glass_package_type_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (glass_package_type_bk, glass_package_type_name, idETLBatchRun_ins)
				values (src.glass_package_type_bk, src.glass_package_type_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure prod.stg_dwh_merge_prod_param_bc
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - BC from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_bc
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_bc with (tablock) as trg
	using Warehouse.prod.dim_param_bc_wrk src
		on (trg.base_curve_bk = src.base_curve_bk)

	when not matched
		then 
			insert (idBC_sk, base_curve_bk, idETLBatchRun_ins)
				values (src.base_curve_bk, src.base_curve_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_param_di
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - DI from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_di
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_di with (tablock) as trg
	using Warehouse.prod.dim_param_di_wrk src
		on (trg.diameter_bk = src.diameter_bk)

	when not matched
		then 
			insert (idDI_sk, diameter_bk, idETLBatchRun_ins)
				values (src.diameter_bk, src.diameter_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_param_po
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - PO from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_po
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_po with (tablock) as trg
	using Warehouse.prod.dim_param_po_wrk src
		on (trg.power_bk = src.power_bk)

	when not matched
		then 
			insert (idPO_sk, power_bk, idETLBatchRun_ins)
				values (src.power_bk, src.power_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_param_cy
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - CY from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_cy
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_cy with (tablock) as trg
	using Warehouse.prod.dim_param_cy_wrk src
		on (trg.cylinder_bk = src.cylinder_bk)

	when not matched
		then 
			insert (idCY_sk, cylinder_bk, idETLBatchRun_ins)
				values (src.cylinder_bk, src.cylinder_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

drop procedure prod.stg_dwh_merge_prod_param_ax
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - AX from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_ax
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_ax with (tablock) as trg
	using Warehouse.prod.dim_param_ax_wrk src
		on (trg.axis_bk = src.axis_bk)

	when not matched
		then 
			insert (idAX_sk, axis_bk, idETLBatchRun_ins)
				values (src.axis_bk, src.axis_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_param_ad
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - AD from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_ad
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_ad with (tablock) as trg
	using Warehouse.prod.dim_param_ad_wrk src
		on (trg.addition_bk = src.addition_bk)

	when not matched
		then 
			insert (idAD_sk, addition_bk, idETLBatchRun_ins)
				values (src.addition_bk, src.addition_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

drop procedure prod.stg_dwh_merge_prod_param_do
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - DO from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_do
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_do with (tablock) as trg
	using Warehouse.prod.dim_param_do_wrk src
		on (trg.dominance_bk = src.dominance_bk)

	when not matched
		then 
			insert (idDO_sk, dominance_bk, idETLBatchRun_ins)
				values (src.dominance_bk, src.dominance_bk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_param_col
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Parameters - COL from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_param_col
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_param_col with (tablock) as trg
	using Warehouse.prod.dim_param_col_wrk src
		on (trg.colour_bk = src.colour_bk)

	when matched and not exists 
		(select isnull(trg.colour_name, '') 
		intersect
		select isnull(src.colour_name, ''))
		then 
			update set
				trg.colour_name = src.colour_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (colour_bk, colour_name, idETLBatchRun_ins)
				values (src.colour_bk, src.colour_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure prod.stg_dwh_merge_prod_product_family
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 05-04-2017
-- Changed: 
	--	16-01-2018	Joseba Bayona Sola	Add idProductFamilyGroup_sk_fk
	--	08-05-2018	Joseba Bayona Sola	Add idProductTypeOH_sk_fk
-- ==========================================================================================
-- Description: Merges data from Product Family from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_family
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_family with (tablock) as trg
	using Warehouse.prod.dim_product_family_wrk src
		on (trg.product_id_bk = src.product_id_bk)

	when matched and not exists 
		(select isnull(trg.idManufacturer_sk_fk, 0), isnull(trg.idCategory_sk_fk, 0), isnull(trg.idCL_Type_sk_fk, 0), isnull(trg.idCL_Feature_sk_fk, 0), 
			isnull(trg.idProductLifecycle_sk_fk, 0), isnull(trg.idProductVisibility_sk_fk, 0),  isnull(trg.idProductFamilyGroup_sk_fk, 0), isnull(trg.idProductTypeOH_sk_fk, 0), 
			isnull(trg.magento_sku, ''), isnull(trg.product_family_code, ''), isnull(trg.product_family_name, ''), 
			isnull(trg.glass_sunglass_name, ''), isnull(trg.glass_sunglass_colour, ''), 
			isnull(trg.status, 0), isnull(trg.promotional_product, 0), isnull(trg.telesales_product, 0) 
		intersect
		select isnull(src.idManufacturer_sk_fk, 0), isnull(src.idCategory_sk_fk, 0), isnull(src.idCL_Type_sk_fk, 0), isnull(src.idCL_Feature_sk_fk, 0), 
			isnull(src.idProductLifecycle_sk_fk, 0), isnull(src.idProductVisibility_sk_fk, 0), isnull(src.idProductFamilyGroup_sk_fk, 0), isnull(src.idProductTypeOH_sk_fk, 0),   
			isnull(src.magento_sku, ''), isnull(src.product_family_code, ''), isnull(src.product_family_name, ''), 
			isnull(src.glass_sunglass_name, ''), isnull(src.glass_sunglass_colour, ''), 
			isnull(src.status, 0), isnull(src.promotional_product, 0), isnull(src.telesales_product, 0)) 
		then 
			update set
				trg.idManufacturer_sk_fk = src.idManufacturer_sk_fk, trg.idCategory_sk_fk = src.idCategory_sk_fk, trg.idCL_Type_sk_fk = src.idCL_Type_sk_fk, trg.idCL_Feature_sk_fk = src.idCL_Feature_sk_fk, 
				trg.idProductLifecycle_sk_fk = src.idProductLifecycle_sk_fk, trg.idProductVisibility_sk_fk = src.idProductVisibility_sk_fk, 
				trg.idProductFamilyGroup_sk_fk = src.idProductFamilyGroup_sk_fk, trg.idProductTypeOH_sk_fk = src.idProductTypeOH_sk_fk, 
				trg.magento_sku = src.magento_sku, trg.product_family_code = src.product_family_code, trg.product_family_name = src.product_family_name, 
				trg.glass_sunglass_name = src.glass_sunglass_name, trg.glass_sunglass_colour = src.glass_sunglass_colour, 
				trg.status = src.status, trg.promotional_product = src.promotional_product, trg.telesales_product = src.telesales_product, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (product_id_bk, 
					idManufacturer_sk_fk, idCategory_sk_fk, idCL_Type_sk_fk, idCL_Feature_sk_fk, idProductLifecycle_sk_fk, idProductVisibility_sk_fk, idProductFamilyGroup_sk_fk, idProductTypeOH_sk_fk,
					magento_sku, product_family_code, product_family_name, 
					glass_sunglass_name, glass_sunglass_colour, 
					status, promotional_product, telesales_product, idETLBatchRun_ins)
				values (src.product_id_bk, 
					src.idManufacturer_sk_fk, src.idCategory_sk_fk, src.idCL_Type_sk_fk, src.idCL_Feature_sk_fk, src.idProductLifecycle_sk_fk, src.idProductVisibility_sk_fk, src.idProductFamilyGroup_sk_fk, src.idProductTypeOH_sk_fk,
					src.magento_sku, src.product_family_code, src.product_family_name, 
					src.glass_sunglass_name, src.glass_sunglass_colour, 
					src.status, src.promotional_product, src.telesales_product, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 






drop procedure prod.stg_dwh_merge_prod_product_type_oh
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Type OH from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_type_oh
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_type_oh with (tablock) as trg
	using Warehouse.prod.dim_product_type_oh_wrk src
		on (trg.product_type_oh_bk = src.product_type_oh_bk)
	when matched and not exists 
		(select isnull(trg.product_type_oh_name, '') 
		intersect
		select isnull(src.product_type_oh_name, ''))
		then 
			update set
				trg.product_type_oh_name = src.product_type_oh_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (product_type_oh_bk, product_type_oh_name, idETLBatchRun_ins)
				values (src.product_type_oh_bk, src.product_type_oh_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure prod.stg_dwh_merge_prod_product_family_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Family Group from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_family_group
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_family_group with (tablock) as trg
	using Warehouse.prod.dim_product_family_group_wrk src
		on (trg.product_family_group_bk = src.product_family_group_bk)
	when matched and not exists 
		(select isnull(trg.product_family_group_name, '') 
		intersect
		select isnull(src.product_family_group_name, ''))
		then 
			update set
				trg.product_family_group_name = src.product_family_group_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (product_family_group_bk, product_family_group_name, idETLBatchRun_ins)
				values (src.product_family_group_bk, src.product_family_group_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure prod.stg_dwh_merge_prod_product_family_pack_size
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Family Pack Size from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_family_pack_size
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_family_pack_size with (tablock) as trg
	using Warehouse.prod.dim_product_family_pack_size_wrk src
		on (trg.packsizeid_bk = src.packsizeid_bk)
	when matched and not exists 
		(select isnull(trg.idProductFamily_sk_fk, 0), isnull(trg.size, 0), isnull(trg.product_family_packsize_name, ''), 
			isnull(trg.allocation_preference_order, 0), isnull(trg.purchasing_preference_order, 0)
		intersect
		select isnull(src.idProductFamily_sk_fk, 0), isnull(src.size, 0), isnull(src.product_family_packsize_name, ''), 
			isnull(src.allocation_preference_order, 0), isnull(src.purchasing_preference_order, 0))
		then 
			update set
				trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk, trg.size = src.size, trg.product_family_packsize_name = src.product_family_packsize_name, 
				trg.allocation_preference_order = src.allocation_preference_order, trg.purchasing_preference_order = src.purchasing_preference_order, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (packsizeid_bk,
				idProductFamily_sk_fk, size, product_family_packsize_name, allocation_preference_order, purchasing_preference_order, idETLBatchRun_ins)
				
				values (src.packsizeid_bk,
					src.idProductFamily_sk_fk, src.size, src.product_family_packsize_name, src.allocation_preference_order, src.purchasing_preference_order, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_price_type_pf
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Price Type PF from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_price_type_pf
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_price_type_pf with (tablock) as trg
	using Warehouse.prod.dim_price_type_pf_wrk src
		on (trg.price_type_pf_bk = src.price_type_pf_bk)
	when matched and not exists 
		(select isnull(trg.price_type_pf_name, '')
		intersect
		select isnull(src.price_type_pf_name, ''))
		then 
			update set
				trg.price_type_pf_name = src.price_type_pf_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (price_type_pf_bk, price_type_pf_name, idETLBatchRun_ins)
				values (src.price_type_pf_bk, src.price_type_pf_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure prod.stg_dwh_merge_prod_product_family_price
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product Family Price from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product_family_price
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product_family_price with (tablock) as trg
	using Warehouse.prod.dim_product_family_price_wrk src
		on (trg.supplierpriceid_bk = src.supplierpriceid_bk)
	when matched and not exists 
		(select isnull(trg.idSupplier_sk_fk, 0), isnull(trg.idProductFamilyPackSize_sk_fk, 0), isnull(trg.idPriceTypePF_sk_fk, 0), 
			isnull(trg.unit_price, 0), isnull(trg.currency_code, ''), isnull(trg.lead_time, 0), 
			isnull(trg.effective_date, ''), isnull(trg.expiry_date, ''), isnull(trg.active, '')
		intersect
		select isnull(src.idSupplier_sk_fk, 0), isnull(src.idProductFamilyPackSize_sk_fk, 0), isnull(src.idPriceTypePF_sk_fk, 0), 
			isnull(src.unit_price, 0), isnull(src.currency_code, ''), isnull(src.lead_time, 0), 
			isnull(src.effective_date, ''), isnull(src.expiry_date, ''), isnull(src.active, ''))
		then 
			update set
				trg.idSupplier_sk_fk = src.idSupplier_sk_fk, trg.idProductFamilyPackSize_sk_fk = src.idProductFamilyPackSize_sk_fk, trg.idPriceTypePF_sk_fk = src.idPriceTypePF_sk_fk, 
				trg.unit_price = src.unit_price, trg.currency_code = src.currency_code, trg.lead_time = src.lead_time, 
				trg.effective_date = src.effective_date, trg.expiry_date = src.expiry_date, trg.active = src.active, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (supplierpriceid_bk,
				idSupplier_sk_fk, idProductFamilyPackSize_sk_fk, idPriceTypePF_sk_fk, 
				unit_price, currency_code, lead_time, 
				effective_date, expiry_date, active, idETLBatchRun_ins)
				
				values (src.supplierpriceid_bk,
					src.idSupplier_sk_fk, src.idProductFamilyPackSize_sk_fk, src.idPriceTypePF_sk_fk, 
					src.unit_price, src.currency_code, src.lead_time, 
					src.effective_date, src.expiry_date, src.active, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_product
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Product from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_product
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_product with (tablock) as trg
	using Warehouse.prod.dim_product_wrk src
		on (trg.productid_erp_bk = src.productid_erp_bk)
	when matched and not exists 
		(select isnull(trg.idProductFamily_sk_fk, 0), 
			isnull(trg.idBC_sk_fk, ''), isnull(trg.idDI_sk_fk, ''), isnull(trg.idPO_sk_fk, ''), 
			isnull(trg.idCY_sk_fk, ''), isnull(trg.idAX_sk_fk, ''), isnull(trg.idAD_sk_fk, ''), isnull(trg.idDO_sk_fk, ''), 
			isnull(trg.idCOL_sk_fk, 0), 
			isnull(trg.parameter, ''), isnull(trg.product_description, '')
		intersect
		select isnull(src.idProductFamily_sk_fk, 0), 
			isnull(src.idBC_sk_fk, ''), isnull(src.idDI_sk_fk, ''), isnull(src.idPO_sk_fk, ''), 
			isnull(src.idCY_sk_fk, ''), isnull(src.idAX_sk_fk, ''), isnull(src.idAD_sk_fk, ''), isnull(src.idDO_sk_fk, ''), 
			isnull(src.idCOL_sk_fk, 0), 
			isnull(src.parameter, ''), isnull(src.product_description, ''))
		then 
			update set
				trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk, 
				trg.idBC_sk_fk = src.idBC_sk_fk, trg.idDI_sk_fk = src.idDI_sk_fk, trg.idPO_sk_fk = src.idPO_sk_fk, 
				trg.idCY_sk_fk = src.idCY_sk_fk, trg.idAX_sk_fk = src.idAX_sk_fk, trg.idAD_sk_fk = src.idAD_sk_fk, trg.idDO_sk_fk = src.idDO_sk_fk, 
				trg.idCOL_sk_fk = src.idCOL_sk_fk, 
				trg.parameter = src.parameter, trg.product_description = src.product_description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (productid_erp_bk, 
				idProductFamily_sk_fk, idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, 
				parameter, product_description, idETLBatchRun_ins)
				
				values (src.productid_erp_bk, 
					src.idProductFamily_sk_fk, src.idBC_sk_fk, src.idDI_sk_fk, src.idPO_sk_fk, src.idCY_sk_fk, src.idAX_sk_fk, src.idAD_sk_fk, src.idDO_sk_fk, src.idCOL_sk_fk, 
					src.parameter, src.product_description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure prod.stg_dwh_merge_prod_stock_item
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Stock Item from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure prod.stg_dwh_merge_prod_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.prod.dim_stock_item with (tablock) as trg
	using Warehouse.prod.dim_stock_item_wrk src
		on (trg.stockitemid_bk = src.stockitemid_bk)
	when matched and not exists 
		(select isnull(trg.idProduct_sk_fk, 0), isnull(trg.packsize, 0), 
			isnull(trg.SKU, ''), isnull(trg.stock_item_description, '')
		intersect
		select isnull(src.idProduct_sk_fk, 0), isnull(src.packsize, 0), 
			isnull(src.SKU, ''), isnull(src.stock_item_description, ''))
		then 
			update set
				trg.idProduct_sk_fk = src.idProduct_sk_fk, trg.packsize = src.packsize, 
				trg.SKU = src.SKU, trg.stock_item_description = src.stock_item_description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (stockitemid_bk, 
				idProduct_sk_fk, packsize, SKU, stock_item_description, idETLBatchRun_ins)
				
				values (src.stockitemid_bk, 
					src.idProduct_sk_fk, src.packsize, src.SKU, src.stock_item_description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

