use Warehouse
go 


drop procedure dm.dwh_dwh_merge_dm_EMV_sync_list
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 03-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from EMV_sync_list from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure dm.dwh_dwh_merge_dm_EMV_sync_list
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.dm.EMV_sync_list with (tablock) as trg 
	using Warehouse.dm.EMV_sync_list_wrk src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk)
	when matched then
		update set
			trg.client_id = src.client_id, 
			trg.website_id = src.website_id, trg.store_id = src.store_id, trg.store_url = src.store_url, 
			trg.title = src.title, trg.firstname = src.firstname, trg.lastname = src.lastname, 
			trg.gender = src.gender, trg.dateofbirth = src.dateofbirth, trg.language = src.language, trg.emvcellphone = src.emvcellphone, 
			trg.currency = src.currency, trg.country = src.country, 
			trg.datejoin = src.datejoin, trg.dateunjoin = src.dateunjoin, 
			trg.email_origine = src.email_origine, trg.email_origin = src.email_origin, trg.seed = src.seed, 
			trg.clienturn = src.clienturn, trg.source = src.source, trg.code = src.code, 
			trg.product_id = src.product_id, 
			trg.order_reminder_id = src.order_reminder_id, trg.order_reminder_source = src.order_reminder_source, trg.order_reminder_date = src.order_reminder_date, 
			trg.order_reminder_no = src.order_reminder_no, trg.order_reminder_pref = src.order_reminder_pref, trg.order_reminder_product = src.order_reminder_product, 
			trg.order_reminder_reorder_url = src.order_reminder_reorder_url, 

			trg.prescription_expiry_date = src.prescription_expiry_date, trg.prescription_wearer = src.prescription_wearer, trg.prescription_optician = src.prescription_optician, 
			trg.bounce = src.bounce, trg.card_expiry_date = src.card_expiry_date, 
			trg.unsubscribe = src.unsubscribe, trg.website_unsubscribe = src.website_unsubscribe, trg.w_unsub_date = src.w_unsub_date, 
			trg.registration_date = src.registration_date, trg.first_order_date = src.first_order_date, 
			trg.last_logged_in_date = src.last_logged_in_date, trg.last_order_date = src.last_order_date, trg.last_order_id = src.last_order_id, trg.last_order_source = src.last_order_source, 
			trg.num_of_orders = src.num_of_orders, 

			trg.segment_lifecycle = src.segment_lifecycle, trg.segment_usage = src.segment_usage, trg.segment_geog = src.segment_geog, trg.segment_purch_behaviour = src.segment_purch_behaviour, 
			trg.segment_eysight = src.segment_eysight, trg.segment_sport = src.segment_sport, trg.segment_professional = src.segment_professional, trg.segment_lifestage = src.segment_lifestage, 
			trg.segment_vanity = src.segment_vanity, 
			trg.segment = src.segment, trg.segment_2 = src.segment_2, trg.segment_3 = src.segment_3, trg.segment_4 = src.segment_4, 
			trg.emvadmin1 = src.emvadmin1, trg.emvadmin2 = src.emvadmin2, trg.emvadmin3 = src.emvadmin3, trg.emvadmin4 = src.emvadmin4, trg.emvadmin5 = src.emvadmin5, 
			trg.check_sum = src.check_sum, 

			trg.lens_sku1 = src.lens_sku1, trg.lens_sku2 = src.lens_sku2, trg.lens_name1 = src.lens_name1, trg.lens_name2 = src.lens_name2, 
			trg.lens_url1 = src.lens_url1, trg.lens_url2 = src.lens_url2, 
			trg.lens_last_order_date = src.lens_last_order_date, 
			trg.lens_google_shopping_gtin_sku1 = src.lens_google_shopping_gtin_sku1, trg.lens_google_shopping_gtin_sku2 = src.lens_google_shopping_gtin_sku2, 

			trg.dailies_store_id1 = src.dailies_store_id1, trg.dailies_store_id2 = src.dailies_store_id2, 
			trg.dailies_sku1 = src.dailies_sku1, trg.dailies_sku2 = src.dailies_sku2, trg.dailies_name1 = src.dailies_name1, trg.dailies_name2 = src.dailies_name2, 
			trg.dailies_url1 = src.dailies_url1, trg.dailies_url2 = src.dailies_url2, 
			trg.dailies_last_order_date = src.dailies_last_order_date, 
			trg.google_shopping_gtin_daily_sku1 = src.google_shopping_gtin_daily_sku1, trg.google_shopping_gtin_daily_sku2 = src.google_shopping_gtin_daily_sku2, 

			trg.monthlies_store_id1 = src.monthlies_store_id1, trg.monthlies_store_id2 = src.monthlies_store_id2, 
			trg.monthlies_sku1 = src.monthlies_sku1, trg.monthlies_sku2 = src.monthlies_sku2, trg.monthlies_name1 = src.monthlies_name1, trg.monthlies_name2 = src.monthlies_name2, 
			trg.monthlies_url1 = src.monthlies_url1, trg.monthlies_url2 = src.monthlies_url2, trg.monthlies_category1 = src.monthlies_category1, trg.monthlies_category2 = src.monthlies_category2, 
			trg.monthlies_last_order_date = src.monthlies_last_order_date, 
			trg.google_shopping_gtin_monthly_sku1 = src.google_shopping_gtin_monthly_sku1, trg.google_shopping_gtin_monthly_sku2 = src.google_shopping_gtin_monthly_sku2, 

			trg.colours_store_id1 = src.colours_store_id1, trg.colours_store_id2 = src.colours_store_id2, 
			trg.colours_sku1 = src.colours_sku1, trg.colours_sku2 = src.colours_sku2, trg.colours_name1 = src.colours_name1, trg.colours_name2 = src.colours_name2, 
			trg.colours_url1 = src.colours_url1, trg.colours_url2 = src.colours_url2, 
			trg.colours_last_order_date = src.colours_last_order_date, 
			trg.google_shopping_gtin_colours_sku1 = src.google_shopping_gtin_colours_sku1, trg.google_shopping_gtin_colours_sku2 = src.google_shopping_gtin_colours_sku2, 

			trg.solutions_store_id1 = src.solutions_store_id1, trg.solutions_store_id2 = src.solutions_store_id2, 
			trg.solutions_sku1 = src.solutions_sku1, trg.solutions_sku2 = src.solutions_sku2, trg.solutions_name1 = src.solutions_name1, trg.solutions_name2 = src.solutions_name2, 
			trg.solutions_url1 = src.solutions_url1, trg.solutions_url2 = src.solutions_url2, trg.solutions_category1 = src.solutions_category1, trg.solutions_category2 = src.solutions_category2, 
			trg.solutions_last_order_date = src.solutions_last_order_date, 
			trg.google_shopping_gtin_solutions_sku1 = src.google_shopping_gtin_solutions_sku1, trg.google_shopping_gtin_solutions_sku2 = src.google_shopping_gtin_solutions_sku2, 

			trg.other_store_id1 = src.other_store_id1, trg.other_store_id2 = src.other_store_id2, 
			trg.other_sku1 = src.other_sku1, trg.other_sku2 = src.other_sku2, trg.other_name1 = src.other_name1, trg.other_name2 = src.other_name2, 
			trg.other_url1 = src.other_url1, trg.other_url2 = src.other_url2, trg.other_category1 = src.other_category1, trg.other_category2 = src.other_category2, 
			trg.other_last_order_date = src.other_last_order_date, 
			trg.google_shopping_gtin_other_sku1 = src.google_shopping_gtin_other_sku1, trg.google_shopping_gtin_other_sku2 = src.google_shopping_gtin_other_sku2, 

			trg.sunglasses_store_id1 = src.sunglasses_store_id1, trg.sunglasses_store_id2 = src.sunglasses_store_id2, 
			trg.sunglasses_sku1 = src.sunglasses_sku1, trg.sunglasses_sku2 = src.sunglasses_sku2, trg.sunglasses_name1 = src.sunglasses_name1, trg.sunglasses_name2 = src.sunglasses_name2, 
			trg.sunglasses_url1 = src.sunglasses_url1, trg.sunglasses_url2 = src.sunglasses_url2, trg.sunglasses_category1 = src.sunglasses_category1, trg.sunglasses_category2 = src.sunglasses_category2, 
			trg.sunglasses_last_order_date = src.sunglasses_last_order_date, 

			trg.glasses_store_id1 = src.glasses_store_id1, trg.glasses_store_id2 = src.glasses_store_id2, 
			trg.glasses_sku1 = src.glasses_sku1, trg.glasses_sku2 = src.glasses_sku2, trg.glasses_name1 = src.glasses_name1, trg.glasses_name2 = src.glasses_name2, 
			trg.glasses_url1 = src.glasses_url1, trg.glasses_url2 = src.glasses_url2, trg.glasses_category1 = src.glasses_category1, trg.glasses_category2 = src.glasses_category2, 
			trg.glasses_last_order_date = src.glasses_last_order_date, 

			trg.last_product_type = src.last_product_type, 
			trg.last_product_sku1 = src.last_product_sku1, trg.last_product_sku2 = src.last_product_sku2, trg.last_product_name1 = src.last_product_name1, trg.last_product_name2 = src.last_product_name2, 
			trg.last_product_url1 = src.last_product_url1, trg.last_product_url2 = src.last_product_url2, 
			trg.last_product_last_order_date = src.last_product_last_order_date, 
			trg.last_order_lens_qty = src.last_order_lens_qty, 
			trg.last_product_google_shopping_gtin_sku1 = src.last_product_google_shopping_gtin_sku1, trg.last_product_google_shopping_gtin_sku2 = src.last_product_google_shopping_gtin_sku2, 

			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched then
		insert (idCustomer_sk_fk, client_id, 
			website_id, store_id, store_url, 
			title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
			currency, country, 
			datejoin, dateunjoin, 
			email_origine, email_origin, seed, clienturn, source, code, 
			product_id, 
			order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url, 

			prescription_expiry_date, prescription_wearer, prescription_optician, 
			bounce, 
			card_expiry_date, 
			unsubscribe, website_unsubscribe, w_unsub_date, 
			registration_date, first_order_date, 
			last_logged_in_date, last_order_date, last_order_id, last_order_source, num_of_orders,

			segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
			segment, segment_2, segment_3, segment_4, 
			emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
			check_sum, 

			lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
			lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2,

			dailies_store_id1, dailies_store_id2,
			dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date, 
			google_shopping_gtin_daily_sku1, google_shopping_gtin_daily_sku2,

			monthlies_store_id1, monthlies_store_id2,
			monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2,
			monthlies_last_order_date, 
			google_shopping_gtin_monthly_sku1, google_shopping_gtin_monthly_sku2, 

			colours_store_id1, colours_store_id2,
			colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date, 
			google_shopping_gtin_colours_sku1, google_shopping_gtin_colours_sku2, 

			solutions_store_id1, solutions_store_id2,
			solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2,
			solutions_last_order_date, 
			google_shopping_gtin_solutions_sku1, google_shopping_gtin_solutions_sku2, 

			other_store_id1, other_store_id2,
			other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2,
			other_last_order_date, 
			google_shopping_gtin_other_sku1, google_shopping_gtin_other_sku2, 

			sunglasses_store_id1, sunglasses_store_id2,
			sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2,
			sunglasses_last_order_date, 

			glasses_store_id1, glasses_store_id2,
			glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2,
			glasses_last_order_date,

			last_product_type,
			last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
			last_order_lens_qty,
			last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2, 
	
			idETLBatchRun_ins) 
			
			values (src.idCustomer_sk_fk, src.client_id, 
				src.website_id, src.store_id, src.store_url, 
				src.title, src.firstname,  src.lastname, src.gender, src.dateofbirth, src.language, src.emvcellphone, 
				src.currency, src.country, 
				src.datejoin, src.dateunjoin, 
				src.email_origine, src.email_origin, src.seed, src.clienturn, src.source, src.code, 
				src.product_id, 
				src.order_reminder_id, src.order_reminder_source, src.order_reminder_date, src.order_reminder_no, src.order_reminder_pref, src.order_reminder_product, src.order_reminder_reorder_url, 

				src.prescription_expiry_date, src.prescription_wearer, src.prescription_optician, 
				src.bounce, 
				src.card_expiry_date, 
				src.unsubscribe, src.website_unsubscribe, src.w_unsub_date, 
				src.registration_date, src.first_order_date, 
				src.last_logged_in_date, src.last_order_date, src.last_order_id, src.last_order_source, src.num_of_orders,

				src.segment_lifecycle, src.segment_usage, src.segment_geog, src.segment_purch_behaviour, src.segment_eysight, src.segment_sport, src.segment_professional, src.segment_lifestage, src.segment_vanity, 
				src.segment, src.segment_2, src.segment_3, src.segment_4, 
				src.emvadmin1, src.emvadmin2, src.emvadmin3, src.emvadmin4, src.emvadmin5, 
				src.check_sum, 

				src.lens_sku1, src.lens_sku2, src.lens_name1, src.lens_name2, src.lens_url1, src.lens_url2, src.lens_last_order_date, 
				src.lens_google_shopping_gtin_sku1, src.lens_google_shopping_gtin_sku2,

				src.dailies_store_id1, src.dailies_store_id2,
				src.dailies_sku1, src.dailies_sku2, src.dailies_name1, src.dailies_name2, src.dailies_url1, src.dailies_url2, src.dailies_last_order_date, 
				src.google_shopping_gtin_daily_sku1, src.google_shopping_gtin_daily_sku2,

				src.monthlies_store_id1, src.monthlies_store_id2,
				src.monthlies_sku1, src.monthlies_sku2, src.monthlies_name1, src.monthlies_name2, src.monthlies_url1, src.monthlies_url2, src.monthlies_category1, src.monthlies_category2,
				src.monthlies_last_order_date, 
				src.google_shopping_gtin_monthly_sku1, src.google_shopping_gtin_monthly_sku2, 

				src.colours_store_id1, src.colours_store_id2,
				src.colours_sku1, src.colours_sku2, src.colours_name1, src.colours_name2, src.colours_url1, src.colours_url2, src.colours_last_order_date, 
				src.google_shopping_gtin_colours_sku1, src.google_shopping_gtin_colours_sku2, 

				src.solutions_store_id1, src.solutions_store_id2,
				src.solutions_sku1, src.solutions_sku2, src.solutions_name1, src.solutions_name2, src.solutions_url1, src.solutions_url2, src.solutions_category1, src.solutions_category2,
				src.solutions_last_order_date, 
				src.google_shopping_gtin_solutions_sku1, src.google_shopping_gtin_solutions_sku2, 

				src.other_store_id1, src.other_store_id2,
				src.other_sku1, src.other_sku2, src.other_name1, src.other_name2, src.other_url1, src.other_url2, src.other_category1, src.other_category2,
				src.other_last_order_date, 
				src.google_shopping_gtin_other_sku1, src.google_shopping_gtin_other_sku2, 

				src.sunglasses_store_id1, src.sunglasses_store_id2,
				src.sunglasses_sku1, src.sunglasses_sku2, src.sunglasses_name1, src.sunglasses_name2, src.sunglasses_url1, src.sunglasses_url2, src.sunglasses_category1, src.sunglasses_category2,
				src.sunglasses_last_order_date, 

				src.glasses_store_id1, src.glasses_store_id2,
				src.glasses_sku1, src.glasses_sku2, src.glasses_name1, src.glasses_name2, src.glasses_url1, src.glasses_url2, src.glasses_category1, src.glasses_category2,
				src.glasses_last_order_date,

				src.last_product_type,
				src.last_product_sku1, src.last_product_sku2, src.last_product_name1, src.last_product_name2, src.last_product_url1, src.last_product_url2, src.last_product_last_order_date, 
				src.last_order_lens_qty,
				src.last_product_google_shopping_gtin_sku1, src.last_product_google_shopping_gtin_sku2, 
	
				@idETLBatchRun)

	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
