use Warehouse
go 


drop table Warehouse.act.fact_customer_signature
go
drop table Warehouse.act.fact_customer_signature_wrk
go

drop table Warehouse.act.fact_customer_signature_scd
go

drop table Warehouse.act.fact_activity_sales
go
drop table Warehouse.act.fact_activity_sales_wrk
go

drop table Warehouse.act.fact_activity_other
go
drop table Warehouse.act.fact_activity_other_wrk
go



drop table Warehouse.act.dim_activity_type
go
drop table Warehouse.act.dim_activity_type_wrk
go

drop table Warehouse.act.dim_activity_group
go
drop table Warehouse.act.dim_activity_group_wrk
go



drop table Warehouse.act.fact_customer_signature_aux_other
go
drop table Warehouse.act.fact_customer_signature_aux_sales
go
drop table Warehouse.act.fact_customer_signature_aux_sales_main
go


drop table Warehouse.act.fact_customer_signature_aux_orders
go


drop table Warehouse.act.fact_customer_signature_aux_sales_main_product
go
drop table Warehouse.act.fact_customer_signature_aux_sales_main_freq
go
drop table Warehouse.act.fact_customer_signature_aux_sales_main_ch
go
drop table Warehouse.act.fact_customer_signature_aux_sales_main_pr
go

drop table Warehouse.act.fact_customer_signature_aux_customers
go



drop table Warehouse.act.dim_customer_signature_v
go



drop table Warehouse.act.dim_customer_cr_reg
go
drop table Warehouse.act.dim_customer_cr_reg_wrk
go



drop table Warehouse.act.dim_rank_cr_time_mm
go
drop table Warehouse.act.dim_rank_cr_time_mm_wrk
go


drop table Warehouse.act.fact_customer_product_signature
go
drop table Warehouse.act.fact_customer_product_signature_wrk
go


drop table Warehouse.act.fact_activity_login
go
drop table Warehouse.act.fact_activity_login_wrk
go
