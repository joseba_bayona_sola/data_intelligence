use Warehouse
go


drop view stock.dim_wh_stock_item_v
go 

create view stock.dim_wh_stock_item_v as
	select wsi.idWHStockItem_sk, wsi.warehousestockitemid_bk, 
		-- si.idStockItem_sk, si.idProduct_sk, si.idProductFamily_sk, si.product_id_magento, 
		-- w.idWarehouse_sk, w.warehouse_name,
		-- smt.idStockMethodType_sk, smt.stock_method_type_name, 
		w.warehouse_name, si.product_id_magento, smt.stock_method_type_name, 

		si.manufacturer_name, si.product_type_name, si.category_name, si.product_type_oh_name, si.product_family_group_name,
		si.product_family_name, si.packsize, 
		si.parameter, si.product_description,
		si.SKU, si.stock_item_description, wsi.wh_stock_item_description, 

		wsi.qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, wsi.qty_issued_stock, 
		wsi.qty_on_hold, wsi.qty_registered, wsi.qty_disposed, wsi.qty_due_in,
		wsi.stocked
	from 
			Warehouse.stock.dim_wh_stock_item wsi
		inner join
			Warehouse.prod.dim_stock_item_v si on wsi.idStockItem_sk_fk = si.idStockItem_sk
		inner join
			Warehouse.gen.dim_warehouse_v w on wsi.idWarehouse_sk_fk = w.idWarehouse_sk
		inner join
			Warehouse.stock.dim_stock_method_type smt on wsi.idStockMethodType_sk_fk = smt.idStockMethodType_sk
go



drop view stock.dim_wh_stock_item_batch_v
go 

create view stock.dim_wh_stock_item_batch_v as
	select wsib.idWHStockItemBatch_sk, wsib.warehousestockitembatchid_bk, wsi.idWHStockItem_sk,
		wsi.warehouse_name, wsi.product_id_magento, wsi.stock_method_type_name, sbt.stock_batch_type_name,

		wsi.manufacturer_name, wsi.product_type_name, wsi.category_name, wsi.product_type_oh_name, wsi.product_family_group_name,
		wsi.product_family_name, wsi.packsize, 
		wsi.parameter, wsi.product_description,
		wsi.SKU, wsi.stock_item_description, wsi.wh_stock_item_description, wsib.batch_id, 

		wsib.qty_received, wsib.qty_available, wsib.qty_outstanding_allocation, wsib.qty_allocated_stock, wsib.qty_issued_stock, 
		wsib.qty_on_hold, wsib.qty_registered, wsib.qty_disposed, 
		wsib.qty_registered_sm, wsib.qty_disposed_sm, 

		wsib.batch_arrived_date, wsib.batch_confirmed_date, wsib.batch_stock_register_date,

		wsib.local_product_unit_cost, wsib.local_total_unit_cost, 
		wsib.local_to_global_rate, wsib.currency_code
	from 
			Warehouse.stock.dim_wh_stock_item_batch wsib
		inner join
			Warehouse.stock.dim_wh_stock_item_v wsi on wsib.idWHStockItem_sk_fk = wsi.idWHStockItem_sk
		inner join
			Warehouse.stock.dim_stock_batch_type sbt on wsib.idStockBatchType_sk_fk = sbt.idStockBatchType_sk
go



drop view stock.fact_wh_stock_item_batch_movement_v
go 

create view stock.fact_wh_stock_item_batch_movement_v as
	select wsibm.idWHStockItemBatchMovement_sk, wsibm.batchstockmovementid_bk, wsib.idWHStockItemBatch_sk, wsib.idWHStockItem_sk,

		wsib.warehouse_name, wsib.product_id_magento, wsib.stock_method_type_name, wsib.stock_batch_type_name,

		wsib.manufacturer_name, wsib.product_type_name, wsib.category_name, wsib.product_type_oh_name, wsib.product_family_group_name,
		wsib.product_family_name, wsib.packsize, 
		wsib.parameter, wsib.product_description,
		wsib.SKU, wsib.stock_item_description, wsib.wh_stock_item_description, wsib.batch_id,

		wsibm.batch_stock_move_id, wsibm.move_id, wsibm.move_line_id,
		sat.stock_adjustment_type_name, smt.stock_movement_type_name, smp.stock_movement_period_name, 
	
		wsibm.qty_movement, wsibm.qty_registered, wsibm.qty_disposed, 
		wsibm.batch_movement_date
	from 
			Warehouse.stock.fact_wh_stock_item_batch_movement wsibm
		inner join
			Warehouse.stock.dim_wh_stock_item_batch_v wsib on wsibm.idWHStockItemBatch_sk_fk = wsib.idWHStockItemBatch_sk
		inner join
			Warehouse.stock.dim_stock_adjustment_type sat on wsibm.idStockAdjustmentType_sk_fk = sat.idStockAdjustmentType_sk
		inner join
			Warehouse.stock.dim_stock_movement_type smt on wsibm.idStockMovementType_sk_fk = smt.idStockMovementType_sk
		inner join
			Warehouse.stock.dim_stock_movement_period smp on wsibm.idStockMovementPeriod_sk_fk = smp.idStockMovementPeriod_sk
go