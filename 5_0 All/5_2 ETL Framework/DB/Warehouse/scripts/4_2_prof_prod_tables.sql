
use Warehouse
go 

select idProductType_sk, 
	product_type_bk, product_type_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_type

select product_type_bk, product_type_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_type_wrk


select idCategory_sk, 
	category_bk, category_name, 
	idProductType_sk_fk,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_category

select 	category_bk, category_name, 
	idProductType_sk_fk,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_category_wrk




select idCL_Type_sk, 
	cl_type_bk, cl_type_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_cl_type

select cl_type_bk, cl_type_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_cl_type_wrk


select idCL_Feature_sk, 
	cl_feature_bk, cl_feature_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_cl_feature

select cl_feature_bk, cl_feature_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_cl_feature_wrk




select idManufacturer_sk, 
	manufacturer_bk, manufacturer_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_manufacturer

select manufacturer_bk, manufacturer_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_manufacturer_wrk





select idProductLifecycle_sk, 
	product_lifecycle_bk, product_lifecycle_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_lifecycle

select product_lifecycle_bk, product_lifecycle_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_lifecycle_wrk

select idProductVisibility_sk, 
	visibility_id_bk, product_visibility_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_visibility

select visibility_id_bk, product_visibility_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_visibility_wrk




select idGlassVisionType_sk, 
	glass_vision_type_bk, glass_vision_type_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_glass_vision_type

select glass_vision_type_bk, glass_vision_type_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_glass_vision_type_wrk

select idGlassPackageType_sk, 
	glass_package_type_bk, glass_package_type_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_glass_package_type

select glass_package_type_bk, glass_package_type_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_glass_package_type_wrk




select idBC_sk, 
	base_curve_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_BC

select base_curve_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_BC_wrk


select idDI_sk, 
	diameter_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_DI

select diameter_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_DI_wrk


select idPO_sk, 
	power_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_PO

select power_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_PO_wrk





select idCY_sk, 
	cylinder_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_CY

select cylinder_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_CY_wrk


select idAX_sk, 
	axis_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_AX

select axis_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_AX_wrk




select idAD_sk, 
	addition_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_AD

select addition_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_AD_wrk


select idDO_sk, 
	dominance_bk, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_DO

select dominance_bk, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_DO_wrk




select idCOL_sk, 
	colour_bk, colour_name, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_param_COL

select colour_bk, colour_name, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_param_COL_wrk




select idProductFamily_sk, 
	product_id_bk, 
	idManufacturer_sk_fk, idCategory_sk_fk, idCL_Type_sk_fk, idCL_Feature_sk_fk, idProductLifecycle_sk_fk, idProductVisibility_sk_fk, idProductFamilyGroup_sk_fk, idProductTypeOH_sk_fk,
	magento_sku, product_family_code, product_family_name, 
	glass_sunglass_name, glass_sunglass_colour, 
	status, promotional_product, telesales_product,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_family

select 
	product_id_bk, 
	idManufacturer_sk_fk, idCategory_sk_fk, idCL_Type_sk_fk, idCL_Feature_sk_fk, idProductLifecycle_sk_fk, idProductVisibility_sk_fk, idProductFamilyGroup_sk_fk, idProductTypeOH_sk_fk,
	magento_sku, product_family_code, product_family_name, 
	glass_sunglass_name, glass_sunglass_colour, 
	status, promotional_product, telesales_product,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_family_wrk



select idProductTypeOH_sk, 
	product_type_oh_bk, product_type_oh_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_type_oh

select product_type_oh_bk, product_type_oh_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_type_oh_wrk



select idProductFamilyGroup_sk, 
	product_family_group_bk, product_family_group_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_family_group

select product_family_group_bk, product_family_group_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_family_group_wrk



select idProductFamilyPacksize_sk, packsizeid_bk,
	idProductFamily_sk_fk, size, product_family_packsize_name, allocation_preference_order, purchasing_preference_order,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_family_pack_size

select packsizeid_bk,
	idProductFamily_sk_fk, size, product_family_packsize_name, allocation_preference_order, purchasing_preference_order,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_family_pack_size_wrk


select idPriceTypePF_sk, price_type_pf_bk, price_type_pf_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_price_type_pf

select price_type_pf_bk, price_type_pf_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_price_type_pf_wrk


select idProductFamilyPrice_sk, supplierpriceid_bk,
	idSupplier_sk_fk, idProductFamilyPackSize_sk_fk, idPriceTypePF_sk_fk, 
	unit_price, currency_code, lead_time, 
	effective_date, expiry_date, active,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product_family_price

select supplierpriceid_bk,
	idSupplier_sk_fk, idProductFamilyPackSize_sk_fk, idPriceTypePF_sk_fk, 
	unit_price, currency_code, lead_time, 
	effective_date, expiry_date, active,
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_family_price_wrk


select idProduct_sk, productid_erp_bk, 
	idProductFamily_sk_fk, idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, 
	parameter, product_description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_product

select productid_erp_bk, 
	idProductFamily_sk_fk, idBC_sk_fk, idDI_sk_fk, idPO_sk_fk, idCY_sk_fk, idAX_sk_fk, idAD_sk_fk, idDO_sk_fk, idCOL_sk_fk, 
	parameter, product_description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_product_wrk


select idStockItem_sk, stockitemid_bk, 
	idProduct_sk_fk, packsize, SKU, stock_item_description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.prod.dim_stock_item

select stockitemid_bk, 
	idProduct_sk_fk, packsize, SKU, stock_item_description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.prod.dim_stock_item_wrk
