use Warehouse
go 

drop table Warehouse.gen.dim_calendar
go

drop table Warehouse.gen.dim_time
go
drop table Warehouse.gen.dim_time_wrk
go



drop table Warehouse.gen.dim_store
go
drop table Warehouse.gen.dim_store_wrk
go

drop table Warehouse.gen.dim_company
go
drop table Warehouse.gen.dim_company_wrk
go



drop table Warehouse.gen.dim_region
go
drop table Warehouse.gen.dim_region_wrk
go

drop table Warehouse.gen.dim_country
go
drop table Warehouse.gen.dim_country_wrk
go



drop table Warehouse.gen.dim_market
go
drop table Warehouse.gen.dim_market_wrk
go


drop table Warehouse.gen.dim_customer_type
go
drop table Warehouse.gen.dim_customer_type_wrk
go

drop table Warehouse.gen.dim_customer_status
go
drop table Warehouse.gen.dim_customer_status_wrk
go


drop table Warehouse.gen.dim_customer_SCD
go
drop table Warehouse.gen.dim_customer
go
drop table Warehouse.gen.dim_customer_wrk
go


drop table Warehouse.gen.dim_customer_unsubscribe
go
drop table Warehouse.gen.dim_customer_unsubscribe_wrk
go


drop table Warehouse.gen.dim_customer_origin
go
drop table Warehouse.gen.dim_customer_origin_wrk
go



drop table Warehouse.gen.dim_supplier_type
go
drop table Warehouse.gen.dim_supplier_type_wrk
go

drop table Warehouse.gen.dim_supplier
go
drop table Warehouse.gen.dim_supplier_wrk
go

drop table Warehouse.gen.dim_warehouse
go
drop table Warehouse.gen.dim_warehouse_wrk
go

