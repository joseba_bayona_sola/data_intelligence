use Warehouse
go 


drop procedure act.stg_dwh_merge_act_activity_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Activity Group from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_activity_group
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.dim_activity_group with (tablock) as trg
	using Warehouse.act.dim_activity_group_wrk src
		on (trg.activity_group_name_bk = src.activity_group_name_bk)
	when matched and not exists 
		(select isnull(trg.activity_group_name, '')
		intersect
		select isnull(src.activity_group_name, ''))
		then 
			update set
				trg.activity_group_name = src.activity_group_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (activity_group_name_bk, activity_group_name, idETLBatchRun_ins)
				values (src.activity_group_name_bk, src.activity_group_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.stg_dwh_merge_act_activity_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Activity Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_activity_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.dim_activity_type with (tablock) as trg
	using Warehouse.act.dim_activity_type_wrk src
		on (trg.activity_type_name_bk = src.activity_type_name_bk)
	when matched and not exists 
		(select isnull(trg.activity_type_name, ''), isnull(trg.idActivityGroup_sk_fk, 0)
		intersect
		select isnull(src.activity_type_name, ''), isnull(src.idActivityGroup_sk_fk, 0))
		then 
			update set
				trg.activity_type_name = src.activity_type_name, 
				trg.idActivityGroup_sk_fk = src.idActivityGroup_sk_fk, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (activity_type_name_bk, activity_type_name, 
				idActivityGroup_sk_fk, idETLBatchRun_ins)
				
				values (src.activity_type_name_bk, src.activity_type_name, 
					src.idActivityGroup_sk_fk, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.stg_dwh_merge_act_activity_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	07-09-2017	Joseba Bayona Sola	Add customer_order_seq_no_web attribute
	--	16-10-2017	Joseba Bayona Sola	Add idCalendarNextActivityDate_sk_fk attribute
	--	23-11-2017	Joseba Bayona Sola	Add idMarketingChannel_sk_fk attribute
	--	17-01-2018	Joseba Bayona Sola	Add next_order_freq_time attribute (Not in Comp)
	--	11-05-2018	Joseba Bayona Sola	Add customer_order_seq_no_gen attribute
-- ==========================================================================================
-- Description: Merges data from Activity Sales from Working table into Final DWH Fact Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_activity_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.fact_activity_sales with (tablock) as trg
	using Warehouse.act.fact_activity_sales_wrk src
		on (trg.order_id_bk = src.order_id_bk)
	when matched and not exists 
		(select 
			isnull(trg.idCalendarActivityDate_sk_fk, 0), isnull(trg.activity_date, ''), isnull(trg.idCalendarPrevActivityDate_sk_fk, 0), isnull(trg.idCalendarNextActivityDate_sk_fk, 0), 
			isnull(trg.idActivityType_sk_fk, 0), isnull(trg.idStore_sk_fk, 0), isnull(trg.idCustomer_sk_fk, 0), 
			isnull(trg.idChannel_sk_fk, 0), isnull(trg.idMarketingChannel_sk_fk, 0), isnull(trg.idPriceType_sk_fk, 0), isnull(trg.discount_f, ''), 
			isnull(trg.idProductTypeOH_sk_fk, 0), isnull(trg.order_qty_time, 0), isnull(trg.num_diff_product_type_oh, 0), 
			isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), isnull(trg.customer_order_seq_no, 0), isnull(trg.customer_order_seq_no_web, 0), isnull(trg.customer_order_seq_no_gen, 0), 

			isnull(trg.local_subtotal, 0), isnull(trg.local_discount, 0), isnull(trg.local_store_credit_used, 0), 
			isnull(trg.local_total_inc_vat, 0), isnull(trg.local_total_exc_vat, 0), 
			isnull(trg.local_to_global_rate, 0), isnull(trg.order_currency_code, '')
		intersect
		select 
			isnull(src.idCalendarActivityDate_sk_fk, 0), isnull(src.activity_date, ''), isnull(src.idCalendarPrevActivityDate_sk_fk, 0), isnull(src.idCalendarNextActivityDate_sk_fk, 0), 
			isnull(src.idActivityType_sk_fk, 0), isnull(src.idStore_sk_fk, 0), isnull(src.idCustomer_sk_fk, 0), 
			isnull(src.idChannel_sk_fk, 0), isnull(src.idMarketingChannel_sk_fk, 0), isnull(src.idPriceType_sk_fk, 0), isnull(src.discount_f, ''), 
			isnull(src.idProductTypeOH_sk_fk, 0), isnull(src.order_qty_time, 0), isnull(src.num_diff_product_type_oh, 0), 
			isnull(src.idCustomerStatusLifecycle_sk_fk, 0), isnull(src.customer_order_seq_no, 0), isnull(src.customer_order_seq_no_web, 0), isnull(src.customer_order_seq_no_gen, 0), 

			isnull(src.local_subtotal, 0), isnull(src.local_discount, 0), isnull(src.local_store_credit_used, 0), 
			isnull(src.local_total_inc_vat, 0), isnull(src.local_total_exc_vat, 0), 
			isnull(src.local_to_global_rate, 0), isnull(src.order_currency_code, ''))			
		then 
			update set
				trg.idCalendarActivityDate_sk_fk = src.idCalendarActivityDate_sk_fk, trg.activity_date = src.activity_date, 
				trg.idCalendarPrevActivityDate_sk_fk = src.idCalendarPrevActivityDate_sk_fk, trg.idCalendarNextActivityDate_sk_fk = src.idCalendarNextActivityDate_sk_fk, 
				trg.next_order_freq_time = datediff(dd, src.activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), src.idCalendarNextActivityDate_sk_fk), 112), 126)),
				trg.idActivityType_sk_fk = src.idActivityType_sk_fk, trg.idStore_sk_fk = src.idStore_sk_fk, trg.idCustomer_sk_fk = src.idCustomer_sk_fk, 
				trg.idChannel_sk_fk = src.idChannel_sk_fk, trg.idMarketingChannel_sk_fk = src.idMarketingChannel_sk_fk, trg.idPriceType_sk_fk = src.idPriceType_sk_fk, trg.discount_f = src.discount_f, 
				trg.idProductTypeOH_sk_fk = src.idProductTypeOH_sk_fk, trg.order_qty_time = src.order_qty_time, 
				trg.num_diff_product_type_oh = src.num_diff_product_type_oh,
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, trg.customer_order_seq_no = src.customer_order_seq_no, 
				trg.customer_order_seq_no_web = src.customer_order_seq_no_web, trg.customer_order_seq_no_gen = src.customer_order_seq_no_gen, 

				trg.local_subtotal = src.local_subtotal, trg.local_discount = src.local_discount, trg.local_store_credit_used = src.local_store_credit_used, 
				trg.local_total_inc_vat = src.local_total_inc_vat, trg.local_total_exc_vat = src.local_total_exc_vat, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 
				
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_id_bk, 
				idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, idCalendarNextActivityDate_sk_fk, next_order_freq_time,
				idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
				idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk, discount_f,
				idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh,
				idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

				local_subtotal, local_discount, local_store_credit_used, 
				local_total_inc_vat, local_total_exc_vat, 

				local_to_global_rate, order_currency_code,
				idETLBatchRun_ins)
				
				values (src.order_id_bk, 
					src.idCalendarActivityDate_sk_fk, src.activity_date, src.idCalendarPrevActivityDate_sk_fk, src.idCalendarNextActivityDate_sk_fk, 
					datediff(dd, src.activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), src.idCalendarNextActivityDate_sk_fk), 112), 126)),
					src.idActivityType_sk_fk, src.idStore_sk_fk, src.idCustomer_sk_fk,
					src.idChannel_sk_fk, src.idMarketingChannel_sk_fk, src.idPriceType_sk_fk, src.discount_f, 
					src.idProductTypeOH_sk_fk, src.order_qty_time, src.num_diff_product_type_oh,
					src.idCustomerStatusLifecycle_sk_fk, src.customer_order_seq_no, src.customer_order_seq_no_web, src.customer_order_seq_no_gen, 

					src.local_subtotal, src.local_discount, src.local_store_credit_used, 
					src.local_total_inc_vat, src.local_total_exc_vat, 

					src.local_to_global_rate, src.order_currency_code,				
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.stg_dwh_merge_act_activity_other
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	Change Merge Statement with BK adding activity_date
	--	11-09-2017	Joseba Bayona Sola	Change Merge Statement with BK putting activity_seq_no instead of activity_date (Fix of Lapsed - Reactivate Recalc.)
	--	25-09-2017	Joseba Bayona Sola	Change Strategy: Merge Statement for Register - Delete, Insert for Lapsed - Reactivate 
	--	05-04-2018	Joseba Bayona Sola	Change Strategy: Delete, Insert for Lapsed - Reactivate (From different table: fact_activity_other_wrk to fact_activity_sales_wrk)
-- ==========================================================================================
-- Description: Merges data from Activity Other from Working table into Final DWH Fact Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_activity_other
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT - REGISTER
	merge into Warehouse.act.fact_activity_other with (tablock) as trg
	using 
		(select ot.*
		from 
				Warehouse.act.fact_activity_other_wrk ot
			inner join
				Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
		where a.activity_type_name = 'REGISTER') src
		on (trg.activity_id_bk = src.activity_id_bk) and (trg.idActivityType_sk_fk = src.idActivityType_sk_fk) and (trg.activity_seq_no = src.activity_seq_no)
	when matched and not exists 
		(select 
			isnull(trg.idCalendarActivityDate_sk_fk, 0), 
			isnull(trg.activity_date, ''), isnull(trg.idCalendarPrevActivityDate_sk_fk, 0), 
			isnull(trg.idStore_sk_fk, 0), isnull(trg.idCustomer_sk_fk, 0) 
		intersect
		select 
			isnull(src.idCalendarActivityDate_sk_fk, 0), 
			isnull(src.activity_date, ''), isnull(src.idCalendarPrevActivityDate_sk_fk, 0), 
			isnull(src.idStore_sk_fk, 0), isnull(src.idCustomer_sk_fk, 0)) 
		then 
			update set
				trg.idCalendarActivityDate_sk_fk = src.idCalendarActivityDate_sk_fk, 
				trg.activity_date = src.activity_date, trg.idCalendarPrevActivityDate_sk_fk = src.idCalendarPrevActivityDate_sk_fk, 
				trg.idStore_sk_fk = src.idStore_sk_fk, trg.idCustomer_sk_fk = src.idCustomer_sk_fk, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (activity_id_bk, 
				idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, 
				idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
				activity_seq_no,
				idETLBatchRun_ins)
				
				values (src.activity_id_bk, 
					src.idCalendarActivityDate_sk_fk, src.activity_date, src.idCalendarPrevActivityDate_sk_fk, 
					src.idActivityType_sk_fk, src.idStore_sk_fk, src.idCustomer_sk_fk,
					src.activity_seq_no,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message


	-- DELETE STATEMENT - REACTIVATE, LAPSED
	delete ao
	from 
			Warehouse.act.fact_activity_other ao
		inner join
			Warehouse.act.dim_activity_type a on ao.idActivityType_sk_fk = a.idActivityType_sk
	where a.activity_type_name in ('LAPSED', 'REACTIVATE') 
		and idCustomer_sk_fk in 
			(select distinct ot.idCustomer_sk_fk
			from 
					Warehouse.act.fact_activity_sales_wrk ot
				inner join
					Warehouse.act.fact_activity_other ott on ot.idCustomer_sk_fk = ott.idCustomer_sk_fk
				inner join
					Warehouse.act.dim_activity_type a on ott.idActivityType_sk_fk = a.idActivityType_sk
			where a.activity_type_name in ('LAPSED', 'REACTIVATE'))

	set @rowAmountDelete = @@ROWCOUNT

	insert into Warehouse.act.fact_activity_other (activity_id_bk, 
		idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, 
		idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
		activity_seq_no,
		idETLBatchRun_ins)

		select ot.activity_id_bk, 
			ot.idCalendarActivityDate_sk_fk, ot.activity_date, ot.idCalendarPrevActivityDate_sk_fk, 
			ot.idActivityType_sk_fk, ot.idStore_sk_fk, ot.idCustomer_sk_fk,
			ot.activity_seq_no,
			ot.idETLBatchRun_ins
		from 
				Warehouse.act.fact_activity_other_wrk ot
			inner join
				Warehouse.act.dim_activity_type a on ot.idActivityType_sk_fk = a.idActivityType_sk
		where a.activity_type_name in ('LAPSED', 'REACTIVATE')

	set @rowAmountInsert = @@ROWCOUNT
	
	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.dwh_dwh_merge_act_order_header_from_activity_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	Update also customer_order_seq_no_web
	--	09-02-2018	Joseba Bayona Sola	Update only those OH where the attributes to be updated have different value from src
	--	11-05-2018	Joseba Bayona Sola	Update also customer_order_seq_no_gen
-- ==========================================================================================
-- Description: Merges data from Activity Sales to Sales Dim Order Header (customer status - customer order seq no)
-- ==========================================================================================

create procedure act.dwh_dwh_merge_act_order_header_from_activity_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.sales.dim_order_header trg
	using Warehouse.act.fact_activity_sales src
	on trg.order_id_bk = src.order_id_bk
	when matched and not exists
		(select 
			isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), 
			isnull(trg.customer_order_seq_no, 0), isnull(trg.customer_order_seq_no_web, 0), isnull(trg.customer_order_seq_no_gen, 0)
		intersect
		select 
			isnull(src.idCustomerStatusLifecycle_sk_fk, 0), 
			isnull(src.customer_order_seq_no, 0), isnull(src.customer_order_seq_no_web, 0), isnull(src.customer_order_seq_no_gen, 0))
		then
			update set 
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, 
				trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web, trg.customer_order_seq_no_gen = src.customer_order_seq_no_gen 

	--merge into Warehouse.sales.dim_order_header trg
	--using Warehouse.act.fact_activity_sales src
	--on trg.order_id_bk = src.order_id_bk
	--when matched then
	--	update set 
	--		trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, 
	--		trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web 
							
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 








drop procedure act.dwh_dwh_merge_act_customer_signature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New attributes: Create Web, Orders per Web, Lapsed and Reactivate, Std Dev
	--	23-11-2017	Joseba Bayona Sola	New attributes: idMarketingChannelMain_sk_fk
	--	20-04-2018	Joseba Bayona Sola	New attributes: idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk // order_id_bk_first, order_id_bk_last
	--	26-04-2018	Joseba Bayona Sola	New attributes: idCustomerOrigin_sk_fk
	--	11-05-2018	Joseba Bayona Sola	New attributes: num_tot_orders_gen
	--	30-05-2018	Joseba Bayona Sola	New attributes: num_tot_logins
-- ==========================================================================================
-- Description: Merges data from Customer Signature from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.dwh_dwh_merge_act_customer_signature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.fact_customer_signature with (tablock) as trg
	using Warehouse.act.fact_customer_signature_wrk src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk)
	when matched and not exists 
		(select 			
			isnull(trg.idStoreCreate_sk_fk, 0), isnull(trg.idCalendarCreateDate_sk_fk, 0), 
			isnull(trg.idStoreRegister_sk_fk, 0), isnull(trg.idCalendarRegisterDate_sk_fk, 0), 
			isnull(trg.idStoreFirstOrder_sk_fk, 0), isnull(trg.idStoreLastOrder_sk_fk, 0), 
			isnull(trg.idCalendarFirstOrderDate_sk_fk, 0), isnull(trg.idCalendarLastOrderDate_sk_fk, 0), isnull(trg.idCalendarLastLoginDate_sk_fk, 0), 
			
			isnull(trg.idCustomerOrigin_sk_fk, 0), 

			isnull(trg.num_tot_orders_web, 0), isnull(trg.num_tot_orders_gen, 0), 
			isnull(trg.num_tot_orders, 0), isnull(trg.subtotal_tot_orders, 0), isnull(trg.num_tot_cancel, 0), isnull(trg.subtotal_tot_cancel, 0), isnull(trg.num_tot_refund, 0), isnull(trg.subtotal_tot_refund, 0), 
			isnull(trg.num_tot_lapsed, 0), isnull(trg.num_tot_reactivate, 0), 
			isnull(trg.num_tot_discount_orders, 0), isnull(trg.discount_tot_value, 0), isnull(trg.num_tot_store_credit_orders, 0), isnull(trg.store_credit_tot_value, 0), 
			isnull(trg.num_tot_emails, 0), isnull(trg.num_tot_text_messages, 0), isnull(trg.num_tot_reviews, 0), isnull(trg.num_tot_logins, 0), 
			
			isnull(trg.idCustomerStatusLifecycle_sk_fk, 0), isnull(trg.idCalendarStatusUpdateDate_sk_fk, 0), 
			
			isnull(trg.idProductTypeOHMain_sk_fk, 0), isnull(trg.num_diff_product_type_oh, 0), 
			isnull(trg.main_order_qty_time, 0), isnull(trg.min_order_qty_time, 0), isnull(trg.avg_order_qty_time, 0), isnull(trg.max_order_qty_time, 0), isnull(trg.stdev_order_qty_time, 0), 
			isnull(trg.main_order_freq_time, 0), isnull(trg.min_order_freq_time, 0), isnull(trg.avg_order_freq_time, 0), isnull(trg.max_order_freq_time, 0), isnull(trg.stdev_order_freq_time, 0), 
			isnull(trg.idChannelMain_sk_fk, 0), isnull(trg.idMarketingChannelMain_sk_fk, 0), isnull(trg.idPriceTypeMain_sk_fk, 0), 
			isnull(trg.idChannelFirst_sk_fk, 0), isnull(trg.idMarketingChannelFirst_sk_fk, 0), isnull(trg.idPriceTypeFirst_sk_fk, 0), 

			isnull(trg.order_id_bk_first, 0), isnull(trg.order_id_bk_last, 0), 
			isnull(trg.first_discount_amount, 0), isnull(trg.first_subtotal_amount, 0), isnull(trg.last_subtotal_amount, 0), 
			isnull(trg.local_to_global_rate, 0),  isnull(trg.order_currency_code, '')

		intersect
		select 
			isnull(src.idStoreCreate_sk_fk, 0), isnull(src.idCalendarCreateDate_sk_fk, 0), 
			isnull(src.idStoreRegister_sk_fk, 0), isnull(src.idCalendarRegisterDate_sk_fk, 0), 
			isnull(src.idStoreFirstOrder_sk_fk, 0), isnull(src.idStoreLastOrder_sk_fk, 0), 
			isnull(src.idCalendarFirstOrderDate_sk_fk, 0), isnull(src.idCalendarLastOrderDate_sk_fk, 0), isnull(src.idCalendarLastLoginDate_sk_fk, 0), 
			
			isnull(src.idCustomerOrigin_sk_fk, 0), 

			isnull(src.num_tot_orders_web, 0), isnull(src.num_tot_orders_gen, 0), 
			isnull(src.num_tot_orders, 0), isnull(src.subtotal_tot_orders, 0), isnull(src.num_tot_cancel, 0), isnull(src.subtotal_tot_cancel, 0), isnull(src.num_tot_refund, 0), isnull(src.subtotal_tot_refund, 0), 
			isnull(src.num_tot_lapsed, 0), isnull(src.num_tot_reactivate, 0), 
			isnull(src.num_tot_discount_orders, 0), isnull(src.discount_tot_value, 0), isnull(src.num_tot_store_credit_orders, 0), isnull(src.store_credit_tot_value, 0), 
			isnull(src.num_tot_emails, 0), isnull(src.num_tot_text_messages, 0), isnull(src.num_tot_reviews, 0), isnull(src.num_tot_logins, 0), 
			
			isnull(src.idCustomerStatusLifecycle_sk_fk, 0), isnull(src.idCalendarStatusUpdateDate_sk_fk, 0), 
			
			isnull(src.idProductTypeOHMain_sk_fk, 0), isnull(src.num_diff_product_type_oh, 0), 
			isnull(src.main_order_qty_time, 0), isnull(src.min_order_qty_time, 0), isnull(src.avg_order_qty_time, 0), isnull(src.max_order_qty_time, 0), isnull(src.stdev_order_qty_time, 0), 
			isnull(src.main_order_freq_time, 0), isnull(src.min_order_freq_time, 0), isnull(src.avg_order_freq_time, 0), isnull(src.max_order_freq_time, 0), isnull(src.stdev_order_freq_time, 0), 
			isnull(src.idChannelMain_sk_fk, 0), isnull(src.idMarketingChannelMain_sk_fk, 0), isnull(src.idPriceTypeMain_sk_fk, 0), 
			isnull(src.idChannelFirst_sk_fk, 0), isnull(src.idMarketingChannelFirst_sk_fk, 0), isnull(src.idPriceTypeFirst_sk_fk, 0), 

			isnull(src.order_id_bk_first, 0), isnull(src.order_id_bk_last, 0), 			
			isnull(src.first_discount_amount, 0), isnull(src.first_subtotal_amount, 0), isnull(src.last_subtotal_amount, 0), 
			isnull(src.local_to_global_rate, 0),  isnull(src.order_currency_code, ''))
		then 
			update set
				trg.idStoreCreate_sk_fk = src.idStoreCreate_sk_fk, trg.idCalendarCreateDate_sk_fk = src.idCalendarCreateDate_sk_fk, 
				trg.idStoreRegister_sk_fk = src.idStoreRegister_sk_fk, trg.idCalendarRegisterDate_sk_fk = src.idCalendarRegisterDate_sk_fk, 
				trg.idStoreFirstOrder_sk_fk = src.idStoreFirstOrder_sk_fk, trg.idStoreLastOrder_sk_fk = src.idStoreLastOrder_sk_fk, 
				trg.idCalendarFirstOrderDate_sk_fk = src.idCalendarFirstOrderDate_sk_fk, trg.idCalendarLastOrderDate_sk_fk = src.idCalendarLastOrderDate_sk_fk, trg.idCalendarLastLoginDate_sk_fk = src.idCalendarLastLoginDate_sk_fk, 

				trg.idCustomerOrigin_sk_fk = src.idCustomerOrigin_sk_fk,

				trg.num_tot_orders_web = src.num_tot_orders_web, trg.num_tot_orders_gen = src.num_tot_orders_gen, 
				trg.num_tot_orders = src.num_tot_orders, trg.subtotal_tot_orders = src.subtotal_tot_orders, 
				trg.num_tot_cancel = src.num_tot_cancel, trg.subtotal_tot_cancel = src.subtotal_tot_cancel, 
				trg.num_tot_refund = src.num_tot_refund, trg.subtotal_tot_refund = src.subtotal_tot_refund, 
				trg.num_tot_lapsed = src.num_tot_lapsed, trg.num_tot_reactivate = src.num_tot_reactivate, 
				trg.num_tot_discount_orders = src.num_tot_discount_orders, trg.discount_tot_value = src.discount_tot_value, 
				trg.num_tot_store_credit_orders = src.num_tot_store_credit_orders, trg.store_credit_tot_value = src.store_credit_tot_value, 
				trg.num_tot_emails = src.num_tot_emails, trg.num_tot_text_messages = src.num_tot_text_messages, trg.num_tot_reviews = src.num_tot_reviews, trg.num_tot_logins = src.num_tot_logins, 

				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, trg.idCalendarStatusUpdateDate_sk_fk = src.idCalendarStatusUpdateDate_sk_fk, 

				trg.idProductTypeOHMain_sk_fk = src.idProductTypeOHMain_sk_fk, trg.num_diff_product_type_oh = src.num_diff_product_type_oh, 
				trg.main_order_qty_time = src.main_order_qty_time, trg.min_order_qty_time = src.min_order_qty_time, 
				trg.avg_order_qty_time = src.avg_order_qty_time, trg.max_order_qty_time = src.max_order_qty_time, trg.stdev_order_qty_time = src.stdev_order_qty_time, 
				trg.main_order_freq_time = src.main_order_freq_time, trg.min_order_freq_time = src.min_order_freq_time, 
				trg.avg_order_freq_time = src.avg_order_freq_time, trg.max_order_freq_time = src.max_order_freq_time, trg.stdev_order_freq_time = src.stdev_order_freq_time, 
				trg.idChannelMain_sk_fk = src.idChannelMain_sk_fk, trg.idMarketingChannelMain_sk_fk = src.idMarketingChannelMain_sk_fk, trg.idPriceTypeMain_sk_fk = src.idPriceTypeMain_sk_fk, 
				trg.idChannelFirst_sk_fk = src.idChannelFirst_sk_fk, trg.idMarketingChannelFirst_sk_fk = src.idMarketingChannelFirst_sk_fk, trg.idPriceTypeFirst_sk_fk = src.idPriceTypeFirst_sk_fk, 

				trg.order_id_bk_first = src.order_id_bk_first, trg.order_id_bk_last = src.order_id_bk_last, 
				trg.first_discount_amount = src.first_discount_amount, trg.first_subtotal_amount = src.first_subtotal_amount, trg.last_subtotal_amount = src.last_subtotal_amount, 
				trg.local_to_global_rate = src.local_to_global_rate, trg.order_currency_code = src.order_currency_code, 

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idCustomer_sk_fk, 
				idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
				idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
				idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
				idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, idCalendarLastLoginDate_sk_fk, 

				idCustomerOrigin_sk_fk, 

				num_tot_orders_web, num_tot_orders_gen, 
				num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund, 
				num_tot_lapsed, num_tot_reactivate,
				num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
				num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,

				idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk, 

				idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
				main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time,
				main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time,
				idChannelMain_sk_fk, idMarketingChannelMain_sk_fk, idPriceTypeMain_sk_fk, 
				idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 

				order_id_bk_first, order_id_bk_last,
				first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
				local_to_global_rate, order_currency_code, 
				
				idETLBatchRun_ins)
				
				values (src.idCustomer_sk_fk,
					src.idStoreCreate_sk_fk, src.idCalendarCreateDate_sk_fk,
					src.idStoreRegister_sk_fk, src.idCalendarRegisterDate_sk_fk, 
					src.idStoreFirstOrder_sk_fk, src.idStoreLastOrder_sk_fk, 
					src.idCalendarFirstOrderDate_sk_fk, src.idCalendarLastOrderDate_sk_fk, src.idCalendarLastLoginDate_sk_fk, 

					src.idCustomerOrigin_sk_fk,

					src.num_tot_orders_web, src.num_tot_orders_gen, 
					src.num_tot_orders, src.subtotal_tot_orders, src.num_tot_cancel, src.subtotal_tot_cancel, src.num_tot_refund, src.subtotal_tot_refund, 
					src.num_tot_lapsed, src.num_tot_reactivate,
					src.num_tot_discount_orders, src.discount_tot_value, src.num_tot_store_credit_orders, src.store_credit_tot_value, 
					src.num_tot_emails, src.num_tot_text_messages, src.num_tot_reviews, src.num_tot_logins,

					src.idCustomerStatusLifecycle_sk_fk, src.idCalendarStatusUpdateDate_sk_fk, 

					src.idProductTypeOHMain_sk_fk, src.num_diff_product_type_oh,
					src.main_order_qty_time, src.min_order_qty_time, src.avg_order_qty_time, src.max_order_qty_time, src.stdev_order_qty_time,
					src.main_order_freq_time, src.min_order_freq_time, src.avg_order_freq_time, src.max_order_freq_time, src.stdev_order_freq_time,
					src.idChannelMain_sk_fk, src.idMarketingChannelMain_sk_fk, src.idPriceTypeMain_sk_fk, 
					src.idChannelFirst_sk_fk, src.idMarketingChannelFirst_sk_fk, src.idPriceTypeFirst_sk_fk, 

					src.order_id_bk_first, src.order_id_bk_last,
					src.first_discount_amount, src.first_subtotal_amount, src.last_subtotal_amount, 
					src.local_to_global_rate, src.order_currency_code, 
					
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure act.dwh_dwh_get_act_customer_signature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New attributes: Create Web, Orders per Web, Lapsed and Reactivate, Std Dev
	--	16-11-2017	Joseba Bayona Sola	Add to fact_customer_signature_aux_customers (filters customers in activity views) also customers modified in CR-REG
	--	23-11-2017	Joseba Bayona Sola	Add idMarketingChannelMain_sk_fk
	--	08-01-2018	Joseba Bayona Sola	Add to fact_customer_signature_aux_customers the customers existing in DIM and still not in CUST SIGN
	--	05-03-2018	Joseba Bayona Sola	Change fact_activity_sales_wrk instead of fact_activity_sales with idETL filter
	--	20-04-2018	Joseba Bayona Sola	New attributes: idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk // order_id_bk_first, order_id_bk_last
	--	26-04-2018	Joseba Bayona Sola	New attributes: idCustomerOrigin_sk_fk
	--	11-05-2018	Joseba Bayona Sola	New attributes: num_tot_orders_gen
	--	30-05-2018	Joseba Bayona Sola	New attributes: num_tot_logins
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables using Aux Tables into Work Table
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_customers

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_customers(idCustomer_sk_fk)
		select idCustomer_sk_fk
		from Warehouse.act.fact_activity_sales_wrk
		-- from Warehouse.act.fact_activity_sales where idETLBatchRun_ins = @idETLBatchRun or idETLBatchRun_upd = @idETLBatchRun
		union
		select idCustomer_sk_fk
		from Warehouse.act.fact_activity_other
		where idETLBatchRun_ins = @idETLBatchRun or idETLBatchRun_upd = @idETLBatchRun
		union
		select c.idCustomer_sk
		from 
				Warehouse.act.dim_customer_cr_reg crreg
			inner join
				Warehouse.gen.dim_customer_v c on crreg.customer_id_bk = c.customer_id_bk
		where crreg.idETLBatchRun_ins = @idETLBatchRun or crreg.idETLBatchRun_upd = @idETLBatchRun
		union
		select c.idCustomer_sk
		from 
				Warehouse.gen.dim_customer c
			left join
				Warehouse.act.fact_customer_signature cs on c.idCustomer_sk = cs.idCustomer_sk_fk
		where cs.idCustomer_sk_fk is null

	exec act.dwh_dwh_get_act_customer_signature_aux_orders @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec act.dwh_dwh_get_act_customer_signature_aux_other @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec act.dwh_dwh_get_act_customer_signature_aux_sales @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec act.dwh_dwh_get_act_customer_signature_aux_sales_main @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	select o.idCustomer_sk_fk, 
		o.idStoreCreate_sk_fk, o.idCalendarCreateDate_sk_fk, 
		o.idStoreRegister_sk_fk, o.idCalendarRegisterDate_sk_fk, 
		s.idStoreFirstOrder_sk_fk, s.idStoreLastOrder_sk_fk, 
		s.idCalendarFirstOrderDate_sk_fk, s.idCalendarLastOrderDate_sk_fk, o.idCalendarLastLoginDate_sk_fk, 
		
		o.idCustomerOrigin_sk_fk,

		isnull(s.num_tot_orders_web, 0) num_tot_orders_web, isnull(s.num_tot_orders_gen, 0) num_tot_orders_gen,
		isnull(s.num_tot_orders, 0) num_tot_orders, isnull(s.subtotal_tot_orders, 0) subtotal_tot_orders, 
		isnull(s.num_tot_cancel, 0) num_tot_cancel, isnull(s.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
		isnull(s.num_tot_refund, 0) num_tot_refund, isnull(s.subtotal_tot_refund, 0) subtotal_tot_refund, 
		isnull(o.num_tot_lapsed, 0) num_tot_lapsed, isnull(o.num_tot_reactivate, 0) num_tot_reactivate,
		isnull(s.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(s.discount_tot_value, 0) discount_tot_value, 
		isnull(s.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(s.store_credit_tot_value, 0) store_credit_tot_value, 
		o.num_tot_emails, o.num_tot_text_messages, o.num_tot_reviews, o.num_tot_logins,

		o.idCustomerStatusLifecycle_sk_fk, o.idCalendarStatusUpdateDate_sk_fk, 

		sm.idProductTypeOHMain_sk_fk, sm.num_diff_product_type_oh,
		sm.main_order_qty_time, sm.min_order_qty_time, sm.avg_order_qty_time, sm.max_order_qty_time, sm.stdev_order_qty_time, 
		sm.main_order_freq_time, sm.min_order_freq_time, sm.avg_order_freq_time, sm.max_order_freq_time, sm.stdev_order_freq_time,
		sm.idChannelMain_sk_fk, sm.idMarketingChannelMain_sk_fk, sm.idPriceTypeMain_sk_fk, 
		s.idChannelFirst_sk_fk, s.idMarketingChannelFirst_sk_fk, s.idPriceTypeFirst_sk_fk, 

		s.order_id_bk_first, s.order_id_bk_last,
		s.first_discount_amount, s.first_subtotal_amount, s.last_subtotal_amount, 
		s.local_to_global_rate, s.order_currency_code, 

		@idETLBatchRun

	from 
			Warehouse.act.fact_customer_signature_aux_other o
		left join
			Warehouse.act.fact_customer_signature_aux_sales s on o.idCustomer_sk_fk = s.idCustomer_sk_fk
		left join
			Warehouse.act.fact_customer_signature_aux_sales_main sm on o.idCustomer_sk_fk = sm.idCustomer_sk_fk

			
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure act.dwh_dwh_get_act_customer_signature_aux_orders
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New attributes: Orders per Web
	--	07-09-2017	Joseba Bayona Sola	Add Marketing Channel Logic
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Orders Data
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_orders
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_orders

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_orders(order_id_bk, idCustomer_sk_fk,
		idStore_sk_fk, idCalendarActivityDate_sk_fk, activity_date, 
		num_order, num_tot_orders_web, num_tot_orders, subtotal_tot_orders, 
		num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
		idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
		idChannel_sk_fk, channel_name, idMarketingChannel_sk_fk, marketing_channel_name, idPriceType_sk_fk, price_type_name, 
		customer_status_name, customer_order_seq_no,
		global_discount, global_subtotal, 
		local_to_global_rate, order_currency_code)

		select order_id_bk, idCustomer_sk_fk, 
			idStore_sk_fk, idCalendarActivityDate_sk_fk, activity_date,
			dense_rank() over (partition by idCustomer_sk_fk order by customer_order_seq_no) num_order, 
			count(*) over (partition by idCustomer_sk_fk, s.website_group) num_tot_orders_web, 
			count(*) over (partition by idCustomer_sk_fk) num_tot_orders, sum(global_subtotal) over (partition by idCustomer_sk_fk) subtotal_tot_orders, 
			count (case when (global_discount = 0) then null else 1 end) over (partition by idCustomer_sk_fk) num_tot_discount_orders,
			sum(global_discount) over (partition by idCustomer_sk_fk) discount_tot_value, 
			count (case when (global_store_credit_used = 0) then null else 1 end) over (partition by idCustomer_sk_fk) num_tot_store_credit_orders,
			sum(global_store_credit_used) over (partition by idCustomer_sk_fk) store_credit_tot_value, 

			idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
			idChannel_sk_fk, channel_name, idMarketingChannel_sk_fk, marketing_channel_name, idPriceType_sk_fk, price_type_name, 
			customer_status_name, customer_order_seq_no,
			global_discount, global_subtotal, 
			1 local_to_global_rate, 'GBP' order_currency_code
		from 
				Warehouse.act.fact_activity_sales_full_v acs
			inner join
				Warehouse.gen.dim_store_v s on acs.idStore_sk_fk = s.idStore_sk
		where activity_type_name = 'ORDER'	
					
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure act.dwh_dwh_get_act_customer_signature_aux_other
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New attributes: Create Web, Lapsed and Reactivate
	--	16-11-2017	Joseba Bayona Sola	Add new CR-REG logic + Fix Bug on Status
	--	26-04-2018	Joseba Bayona Sola	New attributes: idCustomerOrigin_sk_fk
	--	30-05-2018	Joseba Bayona Sola	New attributes: num_tot_logins
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Other
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_other
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_other

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_other(idCustomer_sk_fk, 
		idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
		idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, idCalendarLastLoginDate_sk_fk, 
		idCustomerOrigin_sk_fk,
		num_tot_lapsed, num_tot_reactivate,
		num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,
		idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk)

		select aof.idCustomer_sk_fk, 
			crreg.idStoreCreate_sk_fk, crreg.idCalendarCreateDate_sk_fk, 
			crreg.idStoreRegister_sk_fk, crreg.idCalendarRegisterDate_sk_fk, al.idCalendarLastLogin_sk_fk idCalendarLastLoginDate_sk_fk, 
			crreg.idCustomerOrigin_sk_fk,
			null num_tot_lapsed, null num_tot_reactivate,
			null num_tot_emails, null num_tot_text_messages, null num_tot_reviews, isnull(al.num_tot_logins, 0) num_tot_logins,
			1 idCustomerStatusLifecycle_sk_fk, 1 idCalendarStatusUpdateDate_sk_fk 
		from 
				(select distinct idCustomer_sk_fk, customer_id
				from Warehouse.act.fact_activity_other_full_v) aof
			inner join
				Warehouse.act.dim_customer_cr_reg crreg on aof.customer_id = crreg.customer_id_bk
			left join
				Warehouse.act.fact_activity_login al on aof.idCustomer_sk_fk = al.idCustomer_sk_fk

		--select reg.idCustomer_sk_fk, 
		--	null idStoreCreate_sk_fk, null idCalendarCreateDate_sk_fk, 
		--	reg.idStoreRegister_sk_fk, reg.idCalendarRegisterDate_sk_fk, null idCalendarLastLoginDate_sk_fk, 
		--	null num_tot_lapsed, null num_tot_reactivate,
		--	null num_tot_emails, null num_tot_text_messages, null num_tot_reviews, 
		--	1 idCustomerStatusLifecycle_sk_fk, 1 idCalendarStatusUpdateDate_sk_fk 
		--from 
		--	(select idCustomer_sk_fk, 
		--		idStore_sk_fk idStoreRegister_sk_fk, idCalendarActivityDate_sk_fk idCalendarRegisterDate_sk_fk
		--	from act.fact_activity_other_full_v
		--	where activity_type_name = 'REGISTER') reg
		
		
		-- Update Customer Status taking REGISTER, LAPSED, REACTIVE from Other and NEW, REGULAR from Sales
		merge into Warehouse.act.fact_customer_signature_aux_other trg
		using 
			(select t.idCustomer_sk_fk, cs.idCustomerStatus_sk idCustomerStatusLifecycle_sk_fk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), t.activity_date, 112))) idCalendarStatusUpdateDate_sk_fk
			from
					(select idCustomer_sk_fk, 
						case 
							when (activity_type_name = 'REGISTER') then 'RNB'
							when (activity_type_name = 'REACTIVATE') then 'REACTIVATED'
							else activity_type_name
						end activity_type_name, activity_date
					from
						(select idCustomer_sk_fk, t.activity_type_name, a.r_imp, activity_date, 
							count(*) over (partition by idCustomer_sk_fk) num_rep_activities,
							dense_rank() over (partition by idCustomer_sk_fk order by activity_date, a.r_imp) ord_activities, 
							count(*) over (partition by idCustomer_sk_fk, activity_date) num_rep_activities_2
						from 
								(select idCustomer_sk_fk, activity_type_name, 
									case when activity_type_name = 'REGISTER' then min(activity_date) over (partition by idCustomer_sk_fk) else activity_date end activity_date
								from 
										(select idCustomer_sk_fk, activity_type_name, activity_date
										from Warehouse.act.fact_activity_other_full_v
										where activity_type_name in ('REGISTER', 'LAPSED')
										union
										select idCustomer_sk_fk, customer_status_name activity_type_name, activity_date
										from Warehouse.act.fact_customer_signature_aux_orders) t) t
							inner join
								(select 'REGISTER' activity_type_name, 1 r_imp
								union
								select 'NEW' activity_type_name, 2 r_imp
								union
								select 'LAPSED' activity_type_name, 3 r_imp
								union
								select 'REACTIVATED' activity_type_name, 4 r_imp
								union
								select 'REGULAR' activity_type_name, 5 r_imp) a on t.activity_type_name = a.activity_type_name
							) t
					where num_rep_activities = ord_activities) t
				inner join
					Warehouse.gen.dim_customer_status cs on t.activity_type_name = cs.customer_status_name_bk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
		when matched then
			update set 
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, 
				trg.idCalendarStatusUpdateDate_sk_fk = src.idCalendarStatusUpdateDate_sk_fk;
						
		-- Update num_tot_lapsed, num_tot_reactivate
		merge into Warehouse.act.fact_customer_signature_aux_other trg
		using 
			(select idCustomer_sk_fk, count(*) num_tot_lapsed
			from Warehouse.act.fact_activity_other_full_v
			where activity_type_name = 'LAPSED'
			group by idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
			when matched then
				update set trg.num_tot_lapsed = src.num_tot_lapsed;

		merge into Warehouse.act.fact_customer_signature_aux_other trg
		using 
			(select idCustomer_sk_fk, count(*) num_tot_reactivate
			from Warehouse.act.fact_activity_other_full_v
			where activity_type_name = 'REACTIVATE'
			group by idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
			when matched then
				update set trg.num_tot_reactivate = src.num_tot_reactivate;

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure act.dwh_dwh_get_act_customer_signature_aux_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New attributes: Orders per Web
	--	20-04-2018	Joseba Bayona Sola	New attributes: idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk // order_id_bk_first, order_id_bk_last
	--	11-05-2018	Joseba Bayona Sola	New attributes: num_tot_orders_gen
	--	15-05-2018	Joseba Bayona Sola	Add logic for FIRST attributes when customer with only refunds
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Sales
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_sales(idCustomer_sk_fk, 
		idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
		idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
		num_tot_orders_web, num_tot_orders_gen, num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund,
		num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
		idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 
		order_id_bk_first, order_id_bk_last,
		first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
		local_to_global_rate, order_currency_code)

		select
			case when (r.idCustomer_sk_fk is not null) then r.idCustomer_sk_fk else o_c.idCustomer_sk_fk end idCustomer_sk_fk,
			o_c.idStoreFirstOrder_sk_fk, o_c.idStoreLastOrder_sk_fk, 
			o_c.idCalendarFirstOrderDate_sk_fk, o_c.idCalendarLastOrderDate_sk_fk, 
			isnull(o_c.num_tot_orders_web, 0) num_tot_orders_web, isnull(o_c.num_tot_orders, 0) + isnull(r.num_tot_refund, 0) num_tot_orders_gen,
			isnull(o_c.num_tot_orders, 0) num_tot_orders, isnull(o_c.subtotal_tot_orders, 0) subtotal_tot_orders, 
			isnull(o_c.num_tot_cancel, 0) num_tot_cancel, isnull(o_c.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
			isnull(r.num_tot_refund, 0) num_tot_refund, isnull(r.subtotal_tot_refund, 0) subtotal_tot_refund, 
			isnull(o_c.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(o_c.discount_tot_value, 0) discount_tot_value, 
			isnull(o_c.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(o_c.store_credit_tot_value, 0) store_credit_tot_value, 
			o_c.idChannelFirst_sk_fk, o_c.idMarketingChannelFirst_sk_fk, o_c.idPriceTypeFirst_sk_fk, 
			o_c.order_id_bk_first, o_c.order_id_bk_last,
			o_c.first_discount_amount, o_c.first_subtotal_amount, o_c.last_subtotal_amount, 
			o_c.local_to_global_rate, o_c.order_currency_code 
		from 
				(select 
					case when (c.idCustomer_sk_fk is not null) then c.idCustomer_sk_fk else o1.idCustomer_sk_fk end idCustomer_sk_fk,
					o1.idStoreFirstOrder_sk_fk, o2.idStoreLastOrder_sk_fk, 
					o1.idCalendarFirstOrderDate_sk_fk, o2.idCalendarLastOrderDate_sk_fk, 
					isnull(o1.num_tot_orders_web, 0) num_tot_orders_web,
					isnull(o1.num_tot_orders, 0) num_tot_orders, isnull(o1.subtotal_tot_orders, 0) subtotal_tot_orders, 
					isnull(c.num_tot_cancel, 0) num_tot_cancel, isnull(c.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
					-- isnull(r.num_tot_refund, 0) num_tot_refund, isnull(r.subtotal_tot_refund, 0) subtotal_tot_refund, 
					isnull(o1.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(o1.discount_tot_value, 0) discount_tot_value, 
					isnull(o1.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(o1.store_credit_tot_value, 0) store_credit_tot_value, 
					o1.idChannelFirst_sk_fk, o1.idMarketingChannelFirst_sk_fk, o1.idPriceTypeFirst_sk_fk,
					o1.order_id_bk_first, o2.order_id_bk_last, 
					o1.first_discount_amount, o1.first_subtotal_amount, o2.last_subtotal_amount,
					o1.local_to_global_rate, o1.order_currency_code 
				from 
						(select idCustomer_sk_fk, 
							idStore_sk_fk idStoreFirstOrder_sk_fk, idCalendarActivityDate_sk_fk idCalendarFirstOrderDate_sk_fk, 
							global_discount first_discount_amount, global_subtotal first_subtotal_amount, 
							num_tot_orders_web, -- ?? Check correctly for the Customer store_id 
							num_tot_orders, subtotal_tot_orders, 
							num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
							idChannel_sk_fk idChannelFirst_sk_fk, idMarketingChannel_sk_fk idMarketingChannelFirst_sk_fk, idPriceType_sk_fk idPriceTypeFirst_sk_fk, 
							order_id_bk order_id_bk_first, 
							local_to_global_rate, order_currency_code
						from Warehouse.act.fact_customer_signature_aux_orders
						where num_order = 1) o1
					inner join
						(select idCustomer_sk_fk, 
							idStore_sk_fk idStoreLastOrder_sk_fk, idCalendarActivityDate_sk_fk idCalendarLastOrderDate_sk_fk, 
							global_subtotal last_subtotal_amount, 
							order_id_bk order_id_bk_last
						from Warehouse.act.fact_customer_signature_aux_orders
						where num_order = num_tot_orders) o2 on o1.idCustomer_sk_fk = o2.idCustomer_sk_fk
					full join
						(select idCustomer_sk_fk, count(*) num_tot_cancel, sum(global_subtotal) subtotal_tot_cancel
						from Warehouse.act.fact_activity_sales_full_v
						where activity_type_name = 'CANCEL'
						group by idCustomer_sk_fk) c on o1.idCustomer_sk_fk = c.idCustomer_sk_fk) o_c
			full join
				(select idCustomer_sk_fk, count(*) num_tot_refund, sum(global_subtotal) subtotal_tot_refund
				from Warehouse.act.fact_activity_sales_full_v
				where activity_type_name = 'REFUND'
				group by idCustomer_sk_fk) r on o_c.idCustomer_sk_fk = r.idCustomer_sk_fk
			
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	merge into Warehouse.act.fact_customer_signature_aux_sales trg
	using
		(select acs.idCustomer_sk_fk, acs.idChannel_sk_fk, acs.idMarketingChannel_sk_fk, acs.idPriceType_sk_fk
		from 
				(select idCustomer_sk_fk
				from Warehouse.act.fact_customer_signature_aux_sales
				where num_tot_orders = 0
					and num_tot_refund <> 0) cs
			inner join
				Warehouse.act.fact_activity_sales acs on cs.idCustomer_sk_fk = acs.idCustomer_sk_fk and acs.customer_order_seq_no_gen = 1 and acs.idActivityType_sk_fk = 9) src 
	on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
	when matched then 
		update set
			trg.idChannelFirst_sk_fk = src.idChannel_sk_fk, trg.idMarketingChannelFirst_sk_fk = src.idMarketingChannel_sk_fk, 
			trg.idPriceTypeFirst_sk_fk = src.idPriceType_sk_fk; 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New Attributes: Std Dev for qty_time, freq_time
	--	07-09-2017	Joseba Bayona Sola	Add Marketing Channel Logic
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Sales Main
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	exec act.dwh_dwh_get_act_customer_signature_aux_sales_main_product @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec act.dwh_dwh_get_act_customer_signature_aux_sales_main_freq @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec act.dwh_dwh_get_act_customer_signature_aux_sales_main_ch_pr @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_sales_main(idCustomer_sk_fk, 
		idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
		main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time, 
		main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time, 
		idChannelMain_sk_fk, idMarketingChannelMain_sk_fk, idPriceTypeMain_sk_fk)

		select pro.idCustomer_sk_fk, 
			pro.idProductTypeOHMain_sk_fk, pro.num_diff_product_type_oh,
			pro.main_order_qty_time, pro.min_order_qty_time, pro.avg_order_qty_time, pro.max_order_qty_time, pro.stdev_order_qty_time, 
			freq.main_order_freq_time, 
			isnull(freq.min_order_freq_time, 0) min_order_freq_time, isnull(freq.avg_order_freq_time, 0) avg_order_freq_time, isnull(freq.max_order_freq_time, 0) max_order_freq_time, 
			isnull(freq.stdev_order_freq_time, 0) stdev_order_freq_time, 
			ch.idChannelMain_sk_fk, mch.idMarketingChannelMain_sk_fk, pr.idPriceTypeMain_sk_fk
		from 
				Warehouse.act.fact_customer_signature_aux_sales_main_product pro
			left join
				Warehouse.act.fact_customer_signature_aux_sales_main_freq freq on pro.idCustomer_sk_fk = freq.idCustomer_sk_fk
			inner join
				Warehouse.act.fact_customer_signature_aux_sales_main_ch ch on pro.idCustomer_sk_fk = ch.idCustomer_sk_fk
			inner join
				Warehouse.act.fact_customer_signature_aux_sales_main_mch mch on pro.idCustomer_sk_fk = mch.idCustomer_sk_fk
			inner join
				Warehouse.act.fact_customer_signature_aux_sales_main_pr pr on pro.idCustomer_sk_fk = pr.idCustomer_sk_fk
				
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_product
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New Attributes: Std Dev for qty_time
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Sales Main - Product OH
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_product
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main_product

	-- INSERT Customers that only have 1 different Product Type order
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_product(idCustomer_sk_fk, idProductTypeOHMain_sk_fk, num_diff_product_type_oh)
		select so.idCustomer_sk_fk, so.idProductTypeOH_sk_fk, t.num_dist_product_type_oh
		from 
				Warehouse.act.fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idProductTypeOH_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
		group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh

	-- INSERT Customers that have more than 1 different Product Type order
		-- Logic based on more popular PT -> IF SAME decide where to spend more --> IF SAME decide ordering per name
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_product(idCustomer_sk_fk, idProductTypeOHMain_sk_fk, num_diff_product_type_oh)
		select idCustomer_sk_fk, idProductTypeOH_sk_fk, num_dist_product_type_oh
		from
			(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
				num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
				dense_rank() over (partition by idCustomer_sk_fk order by product_type_oh_name) num_row
			from
				(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
					num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
					max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_sum_rep_product_type_oh
				from
					(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
						num_dist_product_type_oh,
						num_rep_product_type_oh, sum_rep_product_type_oh, 
						max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
					from
						(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
							t.num_dist_product_type_oh,
							count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
						from 
								Warehouse.act.fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
								from Warehouse.act.fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh) t) t 
				where num_rep_product_type_oh = max_rep_product_type_oh) t
			where sum_rep_product_type_oh = max_sum_rep_product_type_oh) t
		where num_row = 1
			
	-- Update Data related with Qty Time			
		-- Logic based on more popular QTYT -> IF SAME decide on biggest one
		merge into Warehouse.act.fact_customer_signature_aux_sales_main_product trg
		using
			(select t1.idCustomer_sk_fk, t2.order_qty_time main_order_qty_time, 
				t1.min_order_qty_time, t1.avg_order_qty_time, t1.max_order_qty_time, t1.stdev_order_qty_time
			from
					(select t.idCustomer_sk_fk, 
						min(o.order_qty_time) min_order_qty_time, avg(o.order_qty_time) avg_order_qty_time, max(o.order_qty_time) max_order_qty_time, 
						stdevp(o.order_qty_time) stdev_order_qty_time
					from 
							Warehouse.act.fact_customer_signature_aux_sales_main_product t
						inner join
							Warehouse.act.fact_customer_signature_aux_orders o on t.idCustomer_sk_fk = o.idCustomer_sk_fk and t.idProductTypeOHMain_sk_fk = o.idProductTypeOH_sk_fk
					group by t.idCustomer_sk_fk) t1
				inner join
					(select idCustomer_sk_fk, order_qty_time
					from
						(select idCustomer_sk_fk, order_qty_time, num_rep_order_qty_time, 
							max(order_qty_time) over (partition by idCustomer_sk_fk) max_order_qty_time
						from
							(select idCustomer_sk_fk, order_qty_time, num_rep_order_qty_time, 
								max(num_rep_order_qty_time) over (partition by idCustomer_sk_fk) max_num_rep_order_qty_time
							from
								(select t.idCustomer_sk_fk, o.order_qty_time, count(*) num_rep_order_qty_time
								from 
										Warehouse.act.fact_customer_signature_aux_sales_main_product t
									inner join
										Warehouse.act.fact_customer_signature_aux_orders o on t.idCustomer_sk_fk = o.idCustomer_sk_fk and t.idProductTypeOHMain_sk_fk = o.idProductTypeOH_sk_fk
								group by t.idCustomer_sk_fk, o.order_qty_time) t) t
						where num_rep_order_qty_time = max_num_rep_order_qty_time) t
					where order_qty_time = max_order_qty_time) t2 on t1.idCustomer_sk_fk = t2.idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
		when matched then 
			update set 
				trg.main_order_qty_time = src.main_order_qty_time, 
				trg.min_order_qty_time = src.min_order_qty_time, trg.avg_order_qty_time = src.avg_order_qty_time, trg.max_order_qty_time = src.max_order_qty_time, 
				trg.stdev_order_qty_time = src.stdev_order_qty_time; 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_freq
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	New Attributes: Std Dev for freq_time
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Sales Main - Frequency between Orders
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_freq
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main_freq

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_freq(idCustomer_sk_fk, main_order_freq_time, 
		min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time)

		select idCustomer_sk_fk, null main_order_freq_time, 
			min(diff_days) min_order_freq_time, avg(diff_days) avg_order_freq_time, max(diff_days) max_order_freq_time, stdevp(diff_days) stdev_order_freq_time 
		from 
			(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
				idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
				diff_days, num_order 
			from
				(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
					idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
					datediff(day, activity_date, next_activity_date) diff_days,
					num_order
				from
					(select order_id_bk, idCustomer_sk_fk,
						idCalendarActivityDate_sk_fk, activity_date, 
						lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
						num_tot_orders, num_order, 
						customer_order_seq_no
					from Warehouse.act.fact_customer_signature_aux_orders) t) t
			where next_activity_date is not null) t
		group by idCustomer_sk_fk, num_tot_orders
				
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_ch_pr
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-06-2017
-- Changed: 
	--	23-11-2017	Joseba Bayona Sola	Add Marketing Channel Logic
-- ==========================================================================================
-- Description: Get data from Customer Signature from Activity Tables: Aux Table Sales Main - Channel, Price Type
-- ==========================================================================================

create procedure act.dwh_dwh_get_act_customer_signature_aux_sales_main_ch_pr
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect INT, @message varchar(500)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main_ch

	-- INSERT Customers that only have 1 different Channel order
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_ch(idCustomer_sk_fk, idChannelMain_sk_fk)
		select distinct so.idCustomer_sk_fk, so.idChannel_sk_fk
		from 
				Warehouse.act.fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idChannel_sk_fk) num_dist_channel
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idChannel_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- INSERT Customers that have more than 1 different Channel order
		-- Logic based on more popular CH -> IF SAME decide where to spend more --> IF SAME decide ordering per name
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_ch(idCustomer_sk_fk, idChannelMain_sk_fk)

		select idCustomer_sk_fk, idChannel_sk_fk
		from
			(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
				num_rep_channel, sum_rep_channel, max_rep_channel, 
				dense_rank() over (partition by idCustomer_sk_fk order by channel_name) num_row
			from
				(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
					num_rep_channel, sum_rep_channel, max_rep_channel, 
					max(sum_rep_channel) over (partition by idCustomer_sk_fk) max_sum_rep_channel
				from
					(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
						num_rep_channel, sum_rep_channel, 
						max(num_rep_channel) over (partition by idCustomer_sk_fk) max_rep_channel
					from
						(select so.idCustomer_sk_fk, so.idChannel_sk_fk, so.channel_name, 
							count(*) num_rep_channel, sum(so.global_subtotal) sum_rep_channel
						from 
								Warehouse.act.fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idChannel_sk_fk) num_dist_price_type
								from Warehouse.act.fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idChannel_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idChannel_sk_fk, so.channel_name) t) t 
				where num_rep_channel = max_rep_channel) t
			where sum_rep_channel = max_sum_rep_channel) t
		where num_row = 1	

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message



	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main_mch

	-- INSERT Customers that only have 1 different Marketing Channel order
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_mch(idCustomer_sk_fk, idMarketingChannelMain_sk_fk)
		select distinct so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk
		from 
				Warehouse.act.fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idMarketingChannel_sk_fk) num_dist_marketing_channel
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idMarketingChannel_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- INSERT Customers that have more than 1 different Marketing Channel order
		-- Logic based on more popular MCH -> IF SAME decide where to spend more --> IF SAME decide ordering per name
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_mch(idCustomer_sk_fk, idMarketingChannelMain_sk_fk)

		select idCustomer_sk_fk, idMarketingChannel_sk_fk
		from
			(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
				num_rep_channel, sum_rep_channel, max_rep_channel, 
				dense_rank() over (partition by idCustomer_sk_fk order by marketing_channel_name) num_row
			from
				(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
					num_rep_channel, sum_rep_channel, max_rep_channel, 
					max(sum_rep_channel) over (partition by idCustomer_sk_fk) max_sum_rep_channel
				from
					(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
						num_rep_channel, sum_rep_channel, 
						max(num_rep_channel) over (partition by idCustomer_sk_fk) max_rep_channel
					from
						(select so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk, so.marketing_channel_name, 
							count(*) num_rep_channel, sum(so.global_subtotal) sum_rep_channel
						from 
								Warehouse.act.fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idMarketingChannel_sk_fk) num_dist_price_type
								from Warehouse.act.fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idMarketingChannel_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk, so.marketing_channel_name) t) t 
				where num_rep_channel = max_rep_channel) t
			where sum_rep_channel = max_sum_rep_channel) t
		where num_row = 1	

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message




	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_signature_aux_sales_main_pr

	-- INSERT Customers that only have 1 different Product Type order
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_pr(idCustomer_sk_fk, idPriceTypeMain_sk_fk)
		select distinct so.idCustomer_sk_fk, so.idPriceType_sk_fk
		from 
				Warehouse.act.fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idPriceType_sk_fk) num_dist_price_type
				from Warehouse.act.fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idPriceType_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message
				
	-- INSERT Customers that have more than 1 different Price Type order
		-- Logic based on more popular PR -> IF SAME decide where to spend more --> IF SAME decide ordering per name
	insert into Warehouse.act.fact_customer_signature_aux_sales_main_pr(idCustomer_sk_fk, idPriceTypeMain_sk_fk)
		select idCustomer_sk_fk, idPriceType_sk_fk
		from
			(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
				num_rep_price_type, sum_rep_price_type, max_rep_price_type, 
				dense_rank() over (partition by idCustomer_sk_fk order by price_type_name) num_row
			from
				(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
					num_rep_price_type, sum_rep_price_type, max_rep_price_type, 
					max(sum_rep_price_type) over (partition by idCustomer_sk_fk) max_sum_rep_price_type
				from
					(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
						num_rep_price_type, sum_rep_price_type, 
						max(num_rep_price_type) over (partition by idCustomer_sk_fk) max_rep_price_type
					from
						(select so.idCustomer_sk_fk, so.idPriceType_sk_fk, so.price_type_name, 
							count(*) num_rep_price_type, sum(so.global_subtotal) sum_rep_price_type
						from 
								Warehouse.act.fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idPriceType_sk_fk) num_dist_price_type
								from Warehouse.act.fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idPriceType_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idPriceType_sk_fk, so.price_type_name) t) t 
				where num_rep_price_type = max_rep_price_type) t
			where sum_rep_price_type = max_sum_rep_price_type) t
		where num_row = 1		
						
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure act.dwh_dwh_ins_act_customer_signature_v
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-10-2017
-- Changed: 
	--	12-10-2017	Joseba Bayona Sola	Add subtotal_tot_orders attribute
	--	16-10-2017	Joseba Bayona Sola	Add rank_main_order_qty_time - rank_avg_order_freq_time attribute
	--	09-01-2018	Joseba Bayona Sola	Add company_name_create attribute
	--	14-03-2018	Joseba Bayona Sola	Add create_time_mm - customer_unsubscribe_name - customer_status_name
	--	21-03-2018	Joseba Bayona Sola	Add num_dist_products
	--	26-04-2018	Joseba Bayona Sola	Add customer_origin_name
-- ==========================================================================================
-- Description: Insert data from Customer Signature into a DIM table that will be used in Views
-- ==========================================================================================

create procedure act.dwh_dwh_ins_act_customer_signature_v
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)
	
	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.dim_customer_signature_v trg
	using 
		(select cs.idCustomer_sk_fk,
			cs.customer_id, cs.customer_email, c.first_name, c.last_name,
			cs.company_name_create, cs.website_group_create, cs.website_create, cs.create_date, cs.create_time_mm,
			cs.website_register, cs.register_date, 
			cs.customer_origin_name,
			cs.market_name, c.country_code_s country_code_ship, 
			c.customer_unsubscribe_name,
			cs.customer_status_name, cs.rank_num_tot_orders, cs.num_tot_orders, cs.subtotal_tot_orders, cps.num_dist_products,
			cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.rank_main_order_qty_time, cs.main_order_qty_time, cs.rank_avg_order_freq_time, cs.avg_order_freq_time
		from 
				Warehouse.act.fact_customer_signature_v cs
			inner join
				Warehouse.gen.dim_customer_scd_v c on cs.idCustomer_sk_fk = c.idCustomer_sk_fk
			left join
				(select distinct idCustomer_sk_fk, num_dist_products
				from Warehouse.act.fact_customer_product_signature_v) cps on cs.idCustomer_sk_fk = cps.idCustomer_sk_fk) src
	on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
	when matched and not exists
		(select 
			isnull(trg.customer_id, 0), isnull(trg.customer_email, ''), isnull(trg.first_name, ''), isnull(trg.last_name, ''), 
			isnull(trg.company_name_create, ''), isnull(trg.website_group_create, ''), isnull(trg.website_create, ''), isnull(trg.create_date, ''), isnull(trg.create_time_mm, 0), 
			isnull(trg.website_register, ''), isnull(trg.register_date, ''), 
			isnull(trg.customer_origin_name, ''), 
			isnull(trg.market_name, ''), isnull(trg.country_code_ship, ''), 
			isnull(trg.customer_unsubscribe_name, ''),
			isnull(trg.customer_status_name, ''), isnull(trg.rank_num_tot_orders, ''), isnull(trg.num_tot_orders, 0), isnull(trg.subtotal_tot_orders, 0), isnull(trg.num_dist_products, 0), 
			isnull(trg.main_type_oh_name, ''), isnull(trg.num_diff_product_type_oh, 0), 
			isnull(trg.rank_main_order_qty_time, 0), isnull(trg.main_order_qty_time, 0), isnull(trg.rank_avg_order_freq_time, 0), isnull(trg.avg_order_freq_time, 0)
		intersect
		select
			isnull(src.customer_id, 0), isnull(src.customer_email, ''), isnull(src.first_name, ''), isnull(src.last_name, ''), 
			isnull(src.company_name_create, ''), isnull(src.website_group_create, ''), isnull(src.website_create, ''), isnull(src.create_date, ''), isnull(src.create_time_mm, 0), 
			isnull(src.website_register, ''), isnull(src.register_date, ''), 
			isnull(src.customer_origin_name, ''), 
			isnull(src.market_name, ''), isnull(src.country_code_ship, ''), 
			isnull(src.customer_unsubscribe_name, ''),
			isnull(src.customer_status_name, ''), isnull(src.rank_num_tot_orders, ''), isnull(src.num_tot_orders, 0), isnull(src.subtotal_tot_orders, 0), isnull(src.num_dist_products, 0), 
			isnull(src.main_type_oh_name, ''), isnull(src.num_diff_product_type_oh, 0), 
			isnull(src.rank_main_order_qty_time, 0), isnull(src.main_order_qty_time, 0), isnull(src.rank_avg_order_freq_time, 0), isnull(src.avg_order_freq_time, 0))
		then
			update set
				trg.customer_id = src.customer_id, trg.customer_email = src.customer_email, trg.first_name = src.first_name, trg.last_name = src.last_name, 
				trg.company_name_create = src.company_name_create, trg.website_group_create = src.website_group_create, trg.website_create = src.website_create, 
				trg.create_date = src.create_date, trg.create_time_mm = src.create_time_mm, 
				trg.website_register = src.website_register, trg.register_date = src.register_date, 
				trg.customer_origin_name = src.customer_origin_name, 
				trg.market_name = src.market_name, trg.country_code_ship = src.country_code_ship, 
				trg.customer_unsubscribe_name = src.customer_unsubscribe_name, 
				trg.customer_status_name = src.customer_status_name, trg.rank_num_tot_orders = src.rank_num_tot_orders, trg.num_tot_orders = src.num_tot_orders, trg.subtotal_tot_orders = src.subtotal_tot_orders, 
				trg.num_dist_products = src.num_dist_products, 
				trg.main_type_oh_name = src.main_type_oh_name, trg.num_diff_product_type_oh = src.num_diff_product_type_oh, 
				trg.rank_main_order_qty_time = src.rank_main_order_qty_time, trg.main_order_qty_time = src.main_order_qty_time, 
				trg.rank_avg_order_freq_time = src.rank_avg_order_freq_time, trg.avg_order_freq_time = src.avg_order_freq_time, 

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched 
		then 
			insert (idCustomer_sk_fk,
				customer_id, customer_email, first_name, last_name,
				company_name_create, website_group_create, website_create, create_date, create_time_mm,
				website_register, register_date, 
				customer_origin_name,
				market_name, country_code_ship, 
				customer_unsubscribe_name,
				customer_status_name, rank_num_tot_orders, num_tot_orders, subtotal_tot_orders, num_dist_products,
				main_type_oh_name, num_diff_product_type_oh, rank_main_order_qty_time, main_order_qty_time, rank_avg_order_freq_time, avg_order_freq_time, 
				idETLBatchRun_ins)

				values (src.idCustomer_sk_fk,
					src.customer_id, src.customer_email, src.first_name, src.last_name,
					src.company_name_create, src.website_group_create, src.website_create, src.create_date, src.create_time_mm,
					src.website_register, src.register_date, 
					src.customer_origin_name,
					src.market_name, src.country_code_ship, 
					src.customer_unsubscribe_name,
					src.customer_status_name, src.rank_num_tot_orders, src.num_tot_orders, src.subtotal_tot_orders, src.num_dist_products,
					src.main_type_oh_name, src.num_diff_product_type_oh, src.rank_main_order_qty_time, src.main_order_qty_time, src.rank_avg_order_freq_time, src.avg_order_freq_time, 
					@idETLBatchRun)
	
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.stg_dwh_merge_act_customer_cr_reg
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-10-2017
-- Changed: 
	--	26-04-2018	Joseba Bayona Sola	Add idCustomerOrigin_sk_fk attribute
-- ==========================================================================================
-- Description: Merge data from Customer CR - REG table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_customer_cr_reg
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)
	
	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.dim_customer_cr_reg trg
	using Warehouse.act.dim_customer_cr_reg_wrk src
	on trg.customer_id_bk = src.customer_id_bk
	when matched and not exists
		(select 
			isnull(trg.idStoreCreate_sk_fk, 0), isnull(trg.idCalendarCreateDate_sk_fk, 0), 
			isnull(trg.idStoreRegister_sk_fk, 0), isnull(trg.idCalendarRegisterDate_sk_fk, 0), 
			isnull(trg.idCustomerOrigin_sk_fk, 0)
		intersect
		select
			isnull(src.idStoreCreate_sk_fk, 0), isnull(src.idCalendarCreateDate_sk_fk, 0), 
			isnull(src.idStoreRegister_sk_fk, 0), isnull(src.idCalendarRegisterDate_sk_fk, 0), 
			isnull(src.idCustomerOrigin_sk_fk, 0))
		then
			update set
				trg.idStoreCreate_sk_fk = src.idStoreCreate_sk_fk, trg.idCalendarCreateDate_sk_fk = src.idCalendarCreateDate_sk_fk, 
				trg.idStoreRegister_sk_fk = src.idStoreRegister_sk_fk, trg.idCalendarRegisterDate_sk_fk = src.idCalendarRegisterDate_sk_fk, 
				trg.idCustomerOrigin_sk_fk = src.idCustomerOrigin_sk_fk, 

				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched 
		then 
			insert (customer_id_bk, 
				idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
				idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
				idCustomerOrigin_sk_fk,
				idETLBatchRun_ins)

				values (src.customer_id_bk, 
					src.idStoreCreate_sk_fk, src.idCalendarCreateDate_sk_fk, 
					src.idStoreRegister_sk_fk, src.idCalendarRegisterDate_sk_fk, 
					src.idCustomerOrigin_sk_fk,
					@idETLBatchRun)
	
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure act.stg_dwh_merge_act_rank_cr_time_mm
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Rank CR Time MM from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_rank_cr_time_mm
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.dim_rank_cr_time_mm with (tablock) as trg
	using Warehouse.act.dim_rank_cr_time_mm_wrk src
		on (trg.cr_time_mm_bk = src.cr_time_mm_bk)
	when matched and not exists 
		(select isnull(trg.cr_time_mm, ''), isnull(trg.cr_time_name, '')
		intersect
		select isnull(src.cr_time_mm, ''), isnull(src.cr_time_name, ''))
		then 
			update set
				trg.cr_time_mm = src.cr_time_mm, trg.cr_time_name = src.cr_time_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idCR_Time_MM_sk,
				cr_time_mm_bk, cr_time_mm, cr_time_name, idETLBatchRun_ins)
				values (src.cr_time_mm_bk,
					src.cr_time_mm_bk, src.cr_time_mm, cr_time_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure act.stg_dwh_merge_act_customer_product_signature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 21-03-2018
-- Changed: 
	--	05-05-2018	Joseba Bayona Sola	Add order_id_bk_first, order_id_bk_last + Extra Merge + DEL, INS strategy
	--	06-06-2018	Joseba Bayona Sola	DEL, INS now with Customers with only CANCEL/REFUND. Exclude -1 from INSERT, UPDATE
-- ==========================================================================================
-- Description: Merges data from Customer Product Signature from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_customer_product_signature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);
	create table #MergeResults2 (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	--merge into Warehouse.act.fact_customer_product_signature with (tablock) as trg
	--using Warehouse.act.fact_customer_product_signature_wrk src
	--	on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk and trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk)
	--when matched and not exists 
	--	(select 			
	--		isnull(trg.idCalendarFirstOrderDate_sk_fk, 0), isnull(trg.idCalendarLastOrderDate_sk_fk, 0), 
	--		isnull(trg.order_id_bk_first, 0), isnull(trg.order_id_bk_last, 0), 
	--		isnull(trg.num_tot_orders, 0), isnull(trg.subtotal_tot_orders, 0)
	--	intersect
	--	select 
	--		isnull(src.idCalendarFirstOrderDate_sk_fk, 0), isnull(src.idCalendarLastOrderDate_sk_fk, 0), 
	--		isnull(src.order_id_bk_first, 0), isnull(src.order_id_bk_last, 0), 
	--		isnull(src.num_tot_orders, 0), isnull(src.subtotal_tot_orders, 0))
	--	then 
	--		update set
	--			trg.idCalendarFirstOrderDate_sk_fk = src.idCalendarFirstOrderDate_sk_fk, trg.idCalendarLastOrderDate_sk_fk = src.idCalendarLastOrderDate_sk_fk, 
	--			trg.order_id_bk_first = src.order_id_bk_first, trg.order_id_bk_last = src.order_id_bk_last, 
	--			trg.num_tot_orders = src.num_tot_orders, trg.subtotal_tot_orders = src.subtotal_tot_orders, 

	--			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	--when not matched
	--	then 
	--		insert (idCustomer_sk_fk, idProductFamily_sk_fk,
	--			idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
	--			order_id_bk_first, order_id_bk_last, 
	--			num_tot_orders, subtotal_tot_orders,  
	--			idETLBatchRun_ins)
				
	--			values (src.idCustomer_sk_fk, src.idProductFamily_sk_fk, 
	--				src.idCalendarFirstOrderDate_sk_fk, src.idCalendarLastOrderDate_sk_fk, 
	--				src.order_id_bk_first, src.order_id_bk_last, 
	--				src.num_tot_orders, src.subtotal_tot_orders, 
	--				@idETLBatchRun)
			
	--OUTPUT $action INTO #MergeResults;			

	--SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	--FROM    
	--		(SELECT mergeAction, 1 rows
	--		 FROM #MergeResults) c 
	--	 PIVOT
	--		(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	--set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
	--	+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
	--	+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	--exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
	--	@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
	--	@message = @message


	-- DELETE STATEMENT
	delete from Warehouse.act.fact_customer_product_signature
	where idCustomer_sk_fk in 
		(select distinct idCustomer_sk_fk
		from Warehouse.act.fact_customer_product_signature_wrk)

	set @rowAmountDelete = @@ROWCOUNT

	-- INSERT STATEMENT
	insert into Warehouse.act.fact_customer_product_signature(idCustomer_sk_fk, idProductFamily_sk_fk,
		idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
		order_id_bk_first, order_id_bk_last, 
		num_tot_orders, subtotal_tot_orders,  
		idETLBatchRun_ins)

		select idCustomer_sk_fk, idProductFamily_sk_fk,
			idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
			order_id_bk_first, order_id_bk_last, 
			num_tot_orders, subtotal_tot_orders, 
			@idETLBatchRun
		from Warehouse.act.fact_customer_product_signature_wrk
		where idProductFamily_sk_fk <> -1

	set @rowAmountInsert = @@ROWCOUNT

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	-- MERGE STATEMENT
	merge into Warehouse.act.fact_customer_product_signature with (tablock) as trg
	using 
		(select cps.idCustomer_sk_fk, cps.idProductFamily_sk_fk,
			count(*) over (partition by cps.idCustomer_sk_fk) num_dist_products,
			case when (sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk) = 0) then 0 
				else convert(decimal(12, 4), cps.subtotal_tot_orders * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) end product_percentage, 
			case when (sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk) = 0) then 0 
				else convert(decimal(12, 4), sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk, pf.category_name) * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) end category_percentage
		from 
				Warehouse.act.fact_customer_product_signature_wrk cps 
			inner join
				Warehouse.prod.dim_product_family_v pf on cps.idProductFamily_sk_fk = pf.idProductFamily_sk) src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk and trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk)
	when matched and not exists 
		(select 			
			isnull(trg.num_dist_products, 0), isnull(trg.product_percentage, 0), isnull(trg.category_percentage, 0) 
		intersect
		select 
			isnull(src.num_dist_products, 0), isnull(src.product_percentage, 0), isnull(src.category_percentage, 0))
		then 
			update set
				trg.num_dist_products = src.num_dist_products, trg.product_percentage = src.product_percentage, trg.category_percentage = src.category_percentage,
				
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	OUTPUT $action INTO #MergeResults2;	

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults2) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure act.stg_dwh_merge_act_activity_login
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 30-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Activity Login from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure act.stg_dwh_merge_act_activity_login
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.act.fact_activity_login with (tablock) as trg
	using Warehouse.act.fact_activity_login_wrk src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk)
	when matched and not exists 
		(select isnull(trg.idCalendarFirstLogin_sk_fk, 0), isnull(trg.idCalendarLastLogin_sk_fk, 0), isnull(trg.num_tot_logins, 0)
		intersect
		select isnull(src.idCalendarFirstLogin_sk_fk, 0), isnull(src.idCalendarLastLogin_sk_fk, 0), isnull(src.num_tot_logins, 0))
		then 
			update set
				trg.idCalendarFirstLogin_sk_fk = src.idCalendarFirstLogin_sk_fk, trg.idCalendarLastLogin_sk_fk = src.idCalendarLastLogin_sk_fk, trg.num_tot_logins = src.num_tot_logins, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (idCustomer_sk_fk, 
				idCalendarFirstLogin_sk_fk, idCalendarLastLogin_sk_fk, num_tot_logins, idETLBatchRun_ins)
				
				values (src.idCustomer_sk_fk, 
					src.idCalendarFirstLogin_sk_fk, src.idCalendarLastLogin_sk_fk, src.num_tot_logins, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 