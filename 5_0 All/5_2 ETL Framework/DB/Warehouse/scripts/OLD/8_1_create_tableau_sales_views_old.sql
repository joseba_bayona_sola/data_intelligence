
use Warehouse
go

drop view tableau.fact_oh_v
go 

create view tableau.fact_oh_v as

	select oh.order_id_bk, oh.invoice_id, oh.shipment_id, oh.creditmemo_id,
		oh.order_no,
		oh.order_date_c, oh.invoice_date_c, oh.shipment_date_c, oh.refund_date_c,
		oh.acquired, oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.website_group_create,
		oh.market_name, 
		oh.customer_id, oh.customer_email, 
		oh.country_code_ship, oh.country_code_bill, 
		oh.order_stage_name, oh.order_status_name, 
		oh.channel_name, oh.marketing_channel_name, oh.order_source, oh.proforma,
		oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, 
		oh.product_type_oh_name, oh.num_diff_product_type_oh, oh.rank_order_qty_time, oh.order_qty_time, 
		acs.rank_next_order_freq_time, acs.next_order_freq_time,

		oh.local_shipping, oh.local_discount, oh.local_store_credit_used, 
		oh.local_total_inc_vat, oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, 
		oh.local_total_refund, oh.local_store_credit_given, oh.local_bank_online_given, 

		oh.global_shipping, oh.global_discount, oh.global_store_credit_used,
		oh.global_total_inc_vat, oh.global_total_exc_vat, oh.global_total_vat, oh.global_total_prof_fee,
		oh.global_total_refund, oh.global_store_credit_given, oh.global_bank_online_given, 

		isnull(oh.idETLBatchRun_upd, oh.idETLBatchRun_ins) idETLBatchRun
	from 
			sales.dim_order_header_v oh
		inner join
			act.fact_activity_sales_v acs on oh.order_id_bk = acs.order_id_bk
go


drop view tableau.fact_ol_order_v
go 

create view tableau.fact_ol_order_v as

	select order_id_bk, invoice_id, shipment_id, creditmemo_id,
		order_no,
		order_date_c, invoice_date_c, shipment_date_c, refund_date_c,
		acquired, tld, website_group, website, store_name, 
		website_group_create,
		market_name,
		customer_id, customer_email, 
		country_code_ship, country_code_bill, 
		order_stage_name, line_status_name, adjustment_order, order_status_magento_name, 
		payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
		reminder_type_name, reminder_period_name, reorder_f,
		channel_name, marketing_channel_name, group_coupon_code_name, coupon_code,
		order_source, proforma,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, 

		product_type_name, category_name, 
		rank_qty_time, qty_time, 
		local_discount, local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		global_discount, global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee,
		isnull(idETLBatchRun_upd, idETLBatchRun_ins) idETLBatchRun
	from Warehouse.sales.fact_order_line_v
go



drop view tableau.fact_ol_product_v
go 

create view tableau.fact_ol_product_v as

	select order_id_bk, invoice_id, shipment_id, creditmemo_id,
		order_no,
		order_date_c, invoice_date_c, shipment_date_c, refund_date_c,
		acquired, tld, website_group, website, store_name, 
		website_group_create,
		market_name,
		customer_id, customer_email, 
		country_code_ship, country_code_bill, 
		line_status_name, 		
		channel_name, marketing_channel_name, order_source, proforma,
		customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, 

		manufacturer_name, 
		product_type_name, category_name, cl_type_name, cl_feature_name,
		product_id_magento, product_family_name, 
		base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
		glass_vision_type_name, glass_package_type_name, 
		sku_magento, sku_erp, 
		aura_product_f,

		qty_unit, qty_pack, 
		rank_qty_time, qty_time, 
		price_type_name, discount_f, 
		local_price_pack, local_price_pack_discount, 

		local_discount, local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		global_discount, global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee,
		isnull(idETLBatchRun_upd, idETLBatchRun_ins) idETLBatchRun

	from Warehouse.sales.fact_order_line_v
go

