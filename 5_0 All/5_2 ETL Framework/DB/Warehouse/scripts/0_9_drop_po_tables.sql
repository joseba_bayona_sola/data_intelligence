
use Warehouse
go

drop table Warehouse.po.fact_purchase_order_line
go
drop table Warehouse.po.fact_purchase_order_line_wrk
go


drop table Warehouse.po.dim_purchase_order
go
drop table Warehouse.po.dim_purchase_order_wrk
go




drop table Warehouse.po.dim_po_source
go
drop table Warehouse.po.dim_po_source_wrk
go

drop table Warehouse.po.dim_po_type
go
drop table Warehouse.po.dim_po_type_wrk
go

drop table Warehouse.po.dim_po_status
go
drop table Warehouse.po.dim_po_status_wrk
go

drop table Warehouse.po.dim_pol_status
go
drop table Warehouse.po.dim_pol_status_wrk
go

drop table Warehouse.po.dim_pol_problems
go
drop table Warehouse.po.dim_pol_problems_wrk
go

drop table Warehouse.po.dim_wh_operator
go
drop table Warehouse.po.dim_wh_operator_wrk
go




