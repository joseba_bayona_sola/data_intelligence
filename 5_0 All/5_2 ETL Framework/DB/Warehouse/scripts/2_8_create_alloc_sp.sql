
use Warehouse
go



drop procedure alloc.stg_dwh_merge_alloc_order_type_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Type ERP from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_order_type_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_order_type_erp with (tablock) as trg
	using Warehouse.alloc.dim_order_type_erp_wrk src
		on (trg.order_type_erp_bk = src.order_type_erp_bk)
	when matched and not exists 
		(select isnull(trg.order_type_erp_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_type_erp_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_type_erp_name = src.order_type_erp_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_type_erp_bk, order_type_erp_name, description, idETLBatchRun_ins)
				values (src.order_type_erp_bk, src.order_type_erp_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure alloc.stg_dwh_merge_alloc_order_status_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Order Status ERP from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_order_status_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_order_status_erp with (tablock) as trg
	using Warehouse.alloc.dim_order_status_erp_wrk src
		on (trg.order_status_erp_bk = src.order_status_erp_bk)
	when matched and not exists 
		(select isnull(trg.order_status_erp_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.order_status_erp_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.order_status_erp_name = src.order_status_erp_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (order_status_erp_bk, order_status_erp_name, description, idETLBatchRun_ins)
				values (src.order_status_erp_bk, src.order_status_erp_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure alloc.stg_dwh_merge_alloc_shipment_status_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Shipment Status ERP from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_shipment_status_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_shipment_status_erp with (tablock) as trg
	using Warehouse.alloc.dim_shipment_status_erp_wrk src
		on (trg.shipment_status_erp_bk = src.shipment_status_erp_bk)
	when matched and not exists 
		(select isnull(trg.shipment_status_erp_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.shipment_status_erp_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.shipment_status_erp_name = src.shipment_status_erp_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (shipment_status_erp_bk, shipment_status_erp_name, description, idETLBatchRun_ins)
				values (src.shipment_status_erp_bk, src.shipment_status_erp_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure alloc.stg_dwh_merge_alloc_allocation_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Allocation Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_allocation_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_allocation_status with (tablock) as trg
	using Warehouse.alloc.dim_allocation_status_wrk src
		on (trg.allocation_status_bk = src.allocation_status_bk)
	when matched and not exists 
		(select isnull(trg.allocation_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.allocation_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.allocation_status_name = src.allocation_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (allocation_status_bk, allocation_status_name, description, idETLBatchRun_ins)
				values (src.allocation_status_bk, src.allocation_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure alloc.stg_dwh_merge_alloc_allocation_strategy
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Allocation Strategy from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_allocation_strategy
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_allocation_strategy with (tablock) as trg
	using Warehouse.alloc.dim_allocation_strategy_wrk src
		on (trg.allocation_strategy_bk = src.allocation_strategy_bk)
	when matched and not exists 
		(select isnull(trg.allocation_strategy_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.allocation_strategy_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.allocation_strategy_name = src.allocation_strategy_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (allocation_strategy_bk, allocation_strategy_name, description, idETLBatchRun_ins)
				values (src.allocation_strategy_bk, src.allocation_strategy_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure alloc.stg_dwh_merge_alloc_allocation_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Allocation Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_allocation_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_allocation_type with (tablock) as trg
	using Warehouse.alloc.dim_allocation_type_wrk src
		on (trg.allocation_type_bk = src.allocation_type_bk)
	when matched and not exists 
		(select isnull(trg.allocation_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.allocation_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.allocation_type_name = src.allocation_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (allocation_type_bk, allocation_type_name, description, idETLBatchRun_ins)
				values (src.allocation_type_bk, src.allocation_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure alloc.stg_dwh_merge_alloc_allocation_record_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 19-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Allocation Record Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure alloc.stg_dwh_merge_alloc_allocation_record_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.alloc.dim_allocation_record_type with (tablock) as trg
	using Warehouse.alloc.dim_allocation_record_type_wrk src
		on (trg.allocation_record_type_bk = src.allocation_record_type_bk)
	when matched and not exists 
		(select isnull(trg.allocation_record_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.allocation_record_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.allocation_record_type_name = src.allocation_record_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (allocation_record_type_bk, allocation_record_type_name, description, idETLBatchRun_ins)
				values (src.allocation_record_type_bk, src.allocation_record_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
