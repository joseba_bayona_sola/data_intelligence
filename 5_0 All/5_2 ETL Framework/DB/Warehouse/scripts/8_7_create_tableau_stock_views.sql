
use Warehouse
go

drop view tableau.dim_wh_stock_item_v
go 

create view tableau.dim_wh_stock_item_v as

	select warehousestockitemid_bk, 
		warehouse_name, stock_method_type_name, 	
		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, product_id_magento, product_family_name, packsize, 
		SKU, stock_item_description, 
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, qty_due_in,
		stocked
	from Warehouse.stock.dim_wh_stock_item_v
go



drop view tableau.dim_wh_stock_item_batch_v
go 

create view tableau.dim_wh_stock_item_batch_v as

	select warehousestockitembatchid_bk, 
		warehouse_name, stock_method_type_name, stock_batch_type_name,
		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, product_id_magento, product_family_name, packsize, 
		SKU, stock_item_description, 
		batch_id, 

		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered_sm qty_registered, qty_disposed_sm qty_disposed, 

		batch_arrived_date, batch_confirmed_date, batch_stock_register_date

	from Warehouse.stock.dim_wh_stock_item_batch_v
go


drop view tableau.fact_wh_stock_item_batch_movement_v
go 

create view tableau.fact_wh_stock_item_batch_movement_v as

	select batchstockmovementid_bk, 
		warehouse_name, stock_method_type_name, stock_batch_type_name,
		manufacturer_name, 
		product_type_name, category_name, product_family_group_name, product_id_magento, product_family_name, packsize, 
		SKU, stock_item_description, 
		batch_id,

		batch_stock_move_id, move_id, move_line_id,
		stock_adjustment_type_name, stock_movement_type_name, stock_movement_period_name, 

		qty_movement, 

		batch_movement_date
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
go