use Warehouse
go

drop view tableau.fact_ol_inv_gen_v
go 

create view tableau.fact_ol_inv_gen_v as

	select order_line_id_bk_c, order_id_bk_c, order_no, invoice_no,
		invoice_date_c, 
		rank_shipping_days, shipping_days,
		-- acquired, 
		tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create, 
		customer_origin_name,
		ol.market_name, customer_id_c, customer_id, customer_email, customer_name,
		ol.country_code_ship, country_code_bill, 
		order_status_name, order_type_name, order_status_magento_name,
		payment_method_name,
		channel_name, marketing_channel_name, -- coupon_code,
		-- order_source, 
		proforma,
		customer_status_name, rank_seq_no_gen rank_seq_no, customer_order_seq_no_gen customer_order_seq_no, -- rank_seq_no, customer_order_seq_no, 
		rank_seq_no_web, customer_order_seq_no_web, 
		product_type_name, category_name, product_family_group_name, product_id_magento, product_family_name, 
		rank_qty_time, qty_time, 
		price_type_name, discount_f, 
		countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,
		local_discount, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		global_discount,
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
		isnull(idETLBatchRun_upd, idETLBatchRun_ins) idETLBatchRun
	from Warehouse.sales.fact_order_line_inv_v ol
go	



drop view tableau.fact_ol_ship_gen_v
go 

create view tableau.fact_ol_ship_gen_v as

	select order_line_id_bk_c, order_id_bk_c, order_no,
		shipment_date_c, 
		rank_shipping_days, shipping_days,
		-- acquired, 
		tld, website_group, website, store_name, 
		company_name_create, website_group_create, website_create,
		ol.market_name, customer_id_c, 
		ol.country_code_ship, country_code_bill, 
		order_status_name, order_type_name, order_status_magento_name,
		channel_name, marketing_channel_name, -- coupon_code,
		-- order_source, 
		proforma,
		customer_status_name, rank_seq_no_gen rank_seq_no, customer_order_seq_no_gen customer_order_seq_no, -- rank_seq_no, customer_order_seq_no, 
		rank_seq_no_web, customer_order_seq_no_web, 
		product_type_name, category_name, product_family_group_name, product_id_magento, product_family_name, 
		rank_qty_time, qty_time, 
		price_type_name, discount_f, 
		countries_registered_code, product_type_vat, vat_rate, prof_fee_rate,
		local_discount, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		global_discount,
		global_total_inc_vat, global_total_exc_vat, global_total_vat, global_total_prof_fee, 
		local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
		isnull(idETLBatchRun_upd, idETLBatchRun_ins) idETLBatchRun
	from Warehouse.sales.fact_order_line_ship_v ol
go	

