
use Warehouse
go

drop table Warehouse.rec.fact_receipt_line
go
drop table Warehouse.rec.fact_receipt_line_wrk
go

drop table Warehouse.rec.dim_receipt
go
drop table Warehouse.rec.dim_receipt_wrk
go

drop table Warehouse.rec.dim_wh_shipment
go
drop table Warehouse.rec.dim_wh_shipment_wrk
go



drop table Warehouse.rec.dim_wh_shipment_type
go
drop table Warehouse.rec.dim_wh_shipment_type_wrk
go

drop table Warehouse.rec.dim_receipt_status
go
drop table Warehouse.rec.dim_receipt_status_wrk
go

drop table Warehouse.rec.dim_receipt_line_status
go
drop table Warehouse.rec.dim_receipt_line_status_wrk
go

drop table Warehouse.rec.dim_receipt_line_sync_status
go
drop table Warehouse.rec.dim_receipt_line_sync_status_wrk
go




