
-- Invoice Reconciliation Adjustment
select idInvoiceReconciliationAdjustment_sk, adjustmenthistoryid_bk, 
	idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
	adjustment_date, 
	invrec_adjustment_type, invrec_adjustment_level,
	adjustment_amount, local_adjustment_amount, global_adjustment_amount,
	comment, user_name
from Warehouse.invrec.fact_invoice_reconciliation_adjustment_v
order by idInvoiceReconciliation_sk

	select 
		idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
		sum(adjustment_amount) adjustment_amount, sum(local_adjustment_amount) local_adjustment_amount, sum(global_adjustment_amount) global_adjustment_amount
	from Warehouse.invrec.fact_invoice_reconciliation_adjustment_v
	group by idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date
	order by idInvoiceReconciliation_sk

	select ir.idInvoiceReconciliation_sk, isnull(ir.invoice_ref, irc.invoice_ref) invoice_ref, ir.net_suite_no, isnull(ir.invoice_date, irc.invoice_date) invoice_date,
		ir.erp_adjusted_amount, ir.local_erp_adjusted_amount, ir.global_erp_adjusted_amount,
		irc.adjustment_amount, irc.local_adjustment_amount, irc.global_adjustment_amount
	from
		(select idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
			erp_adjusted_amount, local_erp_adjusted_amount, global_erp_adjusted_amount
		from Warehouse.invrec.dim_invoice_reconciliation_v
		where erp_adjusted_amount <> 0) ir
	full join
		(select 
			idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
			sum(adjustment_amount) adjustment_amount, sum(local_adjustment_amount) local_adjustment_amount, sum(global_adjustment_amount) global_adjustment_amount
		from Warehouse.invrec.fact_invoice_reconciliation_adjustment_v
		where invrec_adjustment_type <> 'LineTotal'
		group by idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date) irc on ir.idInvoiceReconciliation_sk = irc.idInvoiceReconciliation_sk
	-- where irc.idInvoiceReconciliation_sk is null -- 0
	-- where ir.idInvoiceReconciliation_sk is null -- 12 (all with total adj equal to 0)
	where ir.erp_adjusted_amount <> irc.adjustment_amount -- 223 // 48 (excluding LineTotal)


-- Invoice Reconciliation Carriage
select idInvoiceReconciliationCarriage_sk, carriageid_bk, 
	idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
	carriage_type, carriage_date, 
	carriage_cost, local_carriage_cost, global_carriage_cost
from Warehouse.invrec.fact_invoice_reconciliation_carriage_v
order by idInvoiceReconciliation_sk

	select 
		idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
		sum(carriage_cost), sum(local_carriage_cost), sum(global_carriage_cost)
	from Warehouse.invrec.fact_invoice_reconciliation_carriage_v
	group by idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date
	order by idInvoiceReconciliation_sk

	select ir.idInvoiceReconciliation_sk, isnull(ir.invoice_ref, irc.invoice_ref) invoice_ref, ir.net_suite_no, isnull(ir.invoice_date, irc.invoice_date) invoice_date,
		ir.invoice_carriage_amount, ir.local_invoice_carriage_amount, ir.global_invoice_carriage_amount, 
		irc.carriage_cost, irc.local_carriage_cost, irc.global_carriage_cost
	from
		(select idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
			invoice_carriage_amount, local_invoice_carriage_amount, global_invoice_carriage_amount, 
			erp_carriage_amount, local_erp_carriage_amount, global_erp_carriage_amount
		from Warehouse.invrec.dim_invoice_reconciliation_v
		where invoice_carriage_amount <> 0) ir
	full join
		Warehouse.invrec.fact_invoice_reconciliation_carriage_v irc on ir.idInvoiceReconciliation_sk = irc.idInvoiceReconciliation_sk
	-- where irc.idInvoiceReconciliation_sk is null -- 14
	where ir.idInvoiceReconciliation_sk is null -- 8
	-- where ir.invoice_carriage_amount <> irc.carriage_cost -- 1

-- Invoice Reconciliation Credit

select idInvoiceReconciliationCredit_sk, invoicecreditid_bk, 
	idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
    credit_note_no, reference, net_suite_no_credit, credit_note_status, 
	credit_date, credit_processed_date, 
	goods_credit, carriage_credit, local_to_wh_rate, local_to_global_rate, currency_code, currency_code_wh,
    comment
from Warehouse.invrec.fact_invoice_reconciliation_credit_v
where net_suite_no_credit = 'CRN 3280'

	select 
		idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
		sum(goods_credit), sum(carriage_credit) 
	from Warehouse.invrec.fact_invoice_reconciliation_credit_v
	group by idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date
	order by idInvoiceReconciliation_sk

	select ir.idInvoiceReconciliation_sk, isnull(ir.invoice_ref, irc.invoice_ref) invoice_ref, ir.net_suite_no, isnull(ir.invoice_date, irc.invoice_date) invoice_date,
		ir.credit_received, ir.local_credit_received, ir.global_credit_received, 
		irc.goods_credit, irc.carriage_credit
	from
		(select idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
			credit_received, local_credit_received, global_credit_received
		from Warehouse.invrec.dim_invoice_reconciliation_v
		where credit_received <> 0) ir
	full join
		(select 
			idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date, 
			sum(goods_credit) goods_credit, sum(carriage_credit) carriage_credit 
		from Warehouse.invrec.fact_invoice_reconciliation_credit_v
		group by idInvoiceReconciliation_sk, invoice_ref, net_suite_no, invoice_date) irc on ir.idInvoiceReconciliation_sk = irc.idInvoiceReconciliation_sk
	-- where irc.idInvoiceReconciliation_sk is null -- 0
	-- where ir.idInvoiceReconciliation_sk is null -- 0
	-- where ir.credit_received <> irc.goods_credit -- 151
	-- where ir.credit_received <> irc.goods_credit + irc.carriage_credit -- 0