
use Warehouse
go

drop view act.dim_activity_type_v
go 

create view act.dim_activity_type_v as
	select ag.idActivityGroup_sk, ag.activity_group_name,
		aty.idActivityType_sk, aty.activity_type_name
	from 
			Warehouse.act.dim_activity_type aty
		inner join
			Warehouse.act.dim_activity_group ag on aty.idActivityGroup_sk_fk = ag.idActivityGroup_sk
go



drop view act.fact_activity_sales_v
go 

create view act.fact_activity_sales_v as
	select acs.order_id_bk, 
		acs.activity_date, cal_a.calendar_date activity_date_c, cal_a.calendar_date_week_name activity_week_day, 
		-- rft.rank_freq_time rank_prev_order_freq_time, 
		-- datediff(dd, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), acs.idCalendarPrevActivityDate_sk_fk), 112), 126), acs.activity_date) prev_order_freq_time,
		rft2.rank_freq_time rank_next_order_freq_time, next_order_freq_time,
		-- datediff(dd, acs.activity_date, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), acs.idCalendarNextActivityDate_sk_fk), 112), 126)) next_order_freq_time,
		-- acs.idCalendarPrevActivityDate_sk_fk,
		aty.activity_group_name, aty.activity_type_name,
		s.acquired, s.tld, s.website_group, s.website, s.store_name, 
		c.market_name, 
		c.customer_id_bk customer_id, cs.customer_status_name, rcosn.rank_seq_no, acs.customer_order_seq_no, rcosnw.rank_seq_no rank_seq_no_web, acs.customer_order_seq_no_web, rcosng.rank_seq_no rank_seq_no_gen, acs.customer_order_seq_no_gen, 
		ch.channel_name, mch.marketing_channel_name, prt.price_type_name, acs.discount_f,
		pt.product_type_oh_name, rqt.rank_qty_time rank_order_qty_time, acs.order_qty_time, acs.num_diff_product_type_oh, 
		
		acs.local_subtotal, acs.local_discount, acs.local_store_credit_used, 
		acs.local_total_inc_vat, -- acs.local_total_exc_vat, 

		convert(decimal(12, 4), acs.local_subtotal * acs.local_to_global_rate) global_subtotal, convert(decimal(12, 4), acs.local_discount * acs.local_to_global_rate) global_discount, 
		convert(decimal(12, 4), acs.local_store_credit_used * acs.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), acs.local_total_inc_vat * acs.local_to_global_rate) global_total_inc_vat, -- convert(decimal(12, 4), acs.local_total_exc_vat * acs.local_to_global_rate) global_total_exc_vat, 

		acs.local_to_global_rate, acs.order_currency_code, 
		acs.idETLBatchRun_ins, acs.idETLBatchRun_upd
	from 
			Warehouse.act.fact_activity_sales acs
		inner join
			Warehouse.gen.dim_calendar cal_a on acs.idCalendarActivityDate_sk_fk = cal_a.idCalendar_sk
		inner join
			Warehouse.act.dim_activity_type_v aty on acs.idActivityType_sk_fk = aty.idActivityType_sk
		inner join
			Warehouse.gen.dim_store_v s on acs.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.gen.dim_customer_v c on acs.idCustomer_sk_fk = c.idCustomer_sk
		inner join
			Warehouse.sales.dim_channel ch on acs.idChannel_sk_fk = ch.idChannel_sk
		inner join
			Warehouse.sales.dim_marketing_channel mch on acs.idMarketingChannel_sk_fk = mch.idMarketingChannel_sk
		inner join
			Warehouse.sales.dim_price_type prt on acs.idPriceType_sk_fk = prt.idPriceType_sk
		left join
			Warehouse.prod.dim_product_type_oh pt on acs.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk
		left join
			Warehouse.gen.dim_customer_status cs on acs.idCustomerStatusLifecycle_sk_fk = cs.idCustomerStatus_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn on acs.customer_order_seq_no = rcosn.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosnw on acs.customer_order_seq_no_web = rcosnw.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosng on acs.customer_order_seq_no_gen = rcosng.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_qty_time rqt on acs.order_qty_time = rqt.idQty_time_sk
		-- left join
		-- 	Warehouse.sales.dim_rank_freq_time rft on datediff(dd, CONVERT(varchar(20), CONVERT(date, CONVERT(varchar(8), acs.idCalendarPrevActivityDate_sk_fk), 112), 126), acs.activity_date)
		-- 		= rft.idFreq_time_sk
		left join
			Warehouse.sales.dim_rank_freq_time rft2 on acs.next_order_freq_time = rft2.idFreq_time_sk
go


drop view act.fact_activity_sales_full_v
go 

create view act.fact_activity_sales_full_v as
	select acs.order_id_bk, 
		acs.idCalendarActivityDate_sk_fk, acs.activity_date, cal_a.calendar_date activity_date_c, cal_a.calendar_date_week_name activity_week_day, 
		-- acs.idCalendarPrevActivityDate_sk_fk,
		aty.activity_group_name, aty.activity_type_name,
		acs.idStore_sk_fk, s.store_name, 
		acs.idCustomer_sk_fk, c.customer_id_bk customer_id, cs.customer_status_name, acs.customer_order_seq_no, 
		acs.idChannel_sk_fk, ch.channel_name, acs.idMarketingChannel_sk_fk, mch.marketing_channel_name, acs.idPriceType_sk_fk, prt.price_type_name, 
		acs.idProductTypeOH_sk_fk, pt.product_type_oh_name, acs.order_qty_time, acs.num_diff_product_type_oh, 
		
		acs.local_subtotal, acs.local_discount, acs.local_store_credit_used, 
		acs.local_total_inc_vat, -- acs.local_total_exc_vat, 

		convert(decimal(12, 4), acs.local_subtotal * acs.local_to_global_rate) global_subtotal, convert(decimal(12, 4), acs.local_discount * acs.local_to_global_rate) global_discount, 
		convert(decimal(12, 4), acs.local_store_credit_used * acs.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), acs.local_total_inc_vat * acs.local_to_global_rate) global_total_inc_vat, -- convert(decimal(12, 4), acs.local_total_exc_vat * acs.local_to_global_rate) global_total_exc_vat, 

		acs.local_to_global_rate, acs.order_currency_code
	from 
			Warehouse.act.fact_activity_sales acs
		inner join
			Warehouse.act.fact_customer_signature_aux_customers sac on acs.idCustomer_sk_fk = sac.idCustomer_sk_fk
		inner join
			Warehouse.gen.dim_calendar cal_a on acs.idCalendarActivityDate_sk_fk = cal_a.idCalendar_sk
		inner join
			Warehouse.act.dim_activity_type_v aty on acs.idActivityType_sk_fk = aty.idActivityType_sk
		inner join
			Warehouse.gen.dim_store_v s on acs.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.gen.dim_customer c on acs.idCustomer_sk_fk = c.idCustomer_sk
		inner join
			Warehouse.sales.dim_channel ch on acs.idChannel_sk_fk = ch.idChannel_sk
		inner join
			Warehouse.sales.dim_marketing_channel mch on acs.idMarketingChannel_sk_fk = mch.idMarketingChannel_sk
		inner join
			Warehouse.sales.dim_price_type prt on acs.idPriceType_sk_fk = prt.idPriceType_sk
		left join
			Warehouse.prod.dim_product_type_oh pt on acs.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk
		left join
			Warehouse.gen.dim_customer_status cs on acs.idCustomerStatusLifecycle_sk_fk = cs.idCustomerStatus_sk
go




drop view act.fact_activity_other_v
go 

create view act.fact_activity_other_v as
	select 
		aco.activity_id_bk,
		aco.activity_date, cal_a.calendar_date activity_date_c, cal_a.calendar_date_week_name activity_week_day, 
		-- aco.idCalendarPrevActivityDate_sk_fk,
		aty.activity_group_name, aty.activity_type_name, aco.activity_seq_no,
		s.acquired, s.tld, s.website_group, s.website, s.store_name, 
		c.market_name, 
		c.customer_id_bk customer_id, 
		aco.idETLBatchRun_ins, aco.idETLBatchRun_upd
	from 
			Warehouse.act.fact_activity_other aco
		inner join
			Warehouse.gen.dim_calendar cal_a on aco.idCalendarActivityDate_sk_fk = cal_a.idCalendar_sk
		inner join
			Warehouse.act.dim_activity_type_v aty on aco.idActivityType_sk_fk = aty.idActivityType_sk
		inner join
			Warehouse.gen.dim_store_v s on aco.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.gen.dim_customer_v c on aco.idCustomer_sk_fk = c.idCustomer_sk
go

drop view act.fact_activity_other_full_v
go 

create view act.fact_activity_other_full_v as
	select 
		aco.activity_id_bk,
		aco.idCalendarActivityDate_sk_fk, aco.activity_date, cal_a.calendar_date activity_date_c, cal_a.calendar_date_week_name activity_week_day, 
		-- aco.idCalendarPrevActivityDate_sk_fk,
		aty.activity_group_name, aty.activity_type_name, aco.activity_seq_no,
		aco.idStore_sk_fk, s.store_name, 
		aco.idCustomer_sk_fk, c.customer_id_bk customer_id
	from 
			Warehouse.act.fact_activity_other aco
		inner join
			Warehouse.act.fact_customer_signature_aux_customers sac on aco.idCustomer_sk_fk = sac.idCustomer_sk_fk
		inner join
			Warehouse.gen.dim_calendar cal_a on aco.idCalendarActivityDate_sk_fk = cal_a.idCalendar_sk
		inner join
			Warehouse.act.dim_activity_type_v aty on aco.idActivityType_sk_fk = aty.idActivityType_sk
		inner join
			Warehouse.gen.dim_store_v s on aco.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.gen.dim_customer c on aco.idCustomer_sk_fk = c.idCustomer_sk
go			
			
			
			
			
drop view act.fact_customer_signature_v
go 

create view act.fact_customer_signature_v as
	select cs.idCustomer_sk_fk,
		c.customer_id_bk customer_id, c.customer_email, 
		s_cre.acquired acquired_create, s_cre.tld tld_create, s_cre.company_name company_name_create, s_cre.website_group website_group_create, s_cre.website website_create, s_cre.store_name store_create, 
		cal_cre.calendar_date create_date, cal_cre.calendar_date_week_name create_week_day, 
		datediff(mm, cal_cre.calendar_date, getutcdate()) create_time_mm, -- rcrt.cr_time_name create_time_name,
		s_reg.website website_register, s_reg.store_name store_register, cal_reg.calendar_date register_date, cal_reg.calendar_date_week_name register_week_day, 
		cor.customer_origin_name,
		c.market_name, 

		s_fi.store_name store_first, cal_fi.calendar_date first_order_date, cal_la.calendar_date_week_name first_order_week_day, 
		s_la.store_name store_last, cal_la.calendar_date last_order_date, cal_la.calendar_date_week_name last_order_week_day, 
		cal_ll.calendar_date last_login_date, -- idCalendarLastLoginDate_sk_fk, 

		cst.customer_status_name, cal_la.calendar_date status_update_date, cal_la.calendar_date_week_name status_update_week_day, 

		num_tot_orders_web,
		rcosn_g.rank_seq_no rank_num_tot_gen, num_tot_orders_gen, 
		rcosn_o.rank_seq_no rank_num_tot_orders, num_tot_orders, subtotal_tot_orders, 
		rcosn_c.rank_seq_no rank_num_tot_cancel, num_tot_cancel, subtotal_tot_cancel, 
		rcosn_r.rank_seq_no rank_num_tot_refund, num_tot_refund, subtotal_tot_refund, 
		num_tot_lapsed, num_tot_reactivate,
		num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
		-- num_tot_emails, num_tot_text_messages, num_tot_reviews, 
		num_tot_logins,

		pt.product_type_oh_name main_type_oh_name, num_diff_product_type_oh,
		rqt.rank_qty_time rank_main_order_qty_time, main_order_qty_time, 
		min_order_qty_time, rqt2.rank_qty_time rank_avg_order_qty_time, avg_order_qty_time, max_order_qty_time, rqt3.rank_qty_time rank_stdev_order_qty_time, stdev_order_qty_time,
		-- main_order_freq_time, 
		min_order_freq_time, rft.rank_freq_time rank_avg_order_freq_time, avg_order_freq_time, max_order_freq_time, rft2.rank_freq_time rank_stdev_order_freq_time, stdev_order_freq_time,
		ch.channel_name main_channel_name, mch.marketing_channel_name main_marketing_channel_name, prt.price_type_name main_price_type_name, 
		ch2.channel_name first_channel_name, mch2.marketing_channel_name first_marketing_channel_name, prt2.price_type_name first_price_type_name, 

		order_id_bk_first, order_id_bk_last,
		first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
		local_to_global_rate, order_currency_code, 
		cs.idETLBatchRun_ins, cs.idETLBatchRun_upd
	from 
			Warehouse.act.fact_customer_signature cs
		inner join
			Warehouse.gen.dim_customer_v c on cs.idCustomer_sk_fk = c.idCustomer_sk
		left join
			Warehouse.gen.dim_store_v s_cre on cs.idStoreCreate_sk_fk = s_cre.idStore_sk
		left join
			Warehouse.gen.dim_calendar cal_cre on cs.idCalendarCreateDate_sk_fk = cal_cre.idCalendar_sk
		inner join
			Warehouse.gen.dim_store_v s_reg on cs.idStoreRegister_sk_fk = s_reg.idStore_sk
		inner join
			Warehouse.gen.dim_calendar cal_reg on cs.idCalendarRegisterDate_sk_fk = cal_reg.idCalendar_sk
		left join
			Warehouse.gen.dim_store_v s_fi on cs.idStoreFirstOrder_sk_fk = s_fi.idStore_sk
		left join
			Warehouse.gen.dim_calendar cal_fi on cs.idCalendarFirstOrderDate_sk_fk = cal_fi.idCalendar_sk
		left join
			Warehouse.gen.dim_store_v s_la on cs.idStoreLastOrder_sk_fk = s_la.idStore_sk
		left join
			Warehouse.gen.dim_calendar cal_la on cs.idCalendarLastOrderDate_sk_fk = cal_la.idCalendar_sk
		inner join
			Warehouse.gen.dim_customer_status cst on cs.idCustomerStatusLifecycle_sk_fk = cst.idCustomerStatus_sk
		inner join
			Warehouse.gen.dim_calendar cal_cst on cs.idCalendarStatusUpdateDate_sk_fk = cal_cst.idCalendar_sk
		left join
			Warehouse.prod.dim_product_type_oh pt on cs.idProductTypeOHMain_sk_fk = pt.idProductTypeOH_sk
		left join
			Warehouse.sales.dim_channel ch on cs.idChannelMain_sk_fk = ch.idChannel_sk
		left join
			Warehouse.sales.dim_marketing_channel mch on cs.idMarketingChannelMain_sk_fk = mch.idMarketingChannel_sk
		left join
			Warehouse.sales.dim_price_type prt on cs.idPriceTypeMain_sk_fk = prt.idPriceType_sk
		left join
			Warehouse.sales.dim_channel ch2 on cs.idChannelFirst_sk_fk = ch2.idChannel_sk
		left join
			Warehouse.sales.dim_marketing_channel mch2 on cs.idMarketingChannelFirst_sk_fk = mch2.idMarketingChannel_sk
		left join
			Warehouse.sales.dim_price_type prt2 on cs.idPriceTypeFirst_sk_fk = prt2.idPriceType_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn_o on cs.num_tot_orders = rcosn_o.idCustomer_order_seq_no_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn_c on cs.num_tot_cancel = rcosn_c.idCustomer_order_seq_no_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn_r on cs.num_tot_refund = rcosn_r.idCustomer_order_seq_no_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn_g on cs.num_tot_orders_gen = rcosn_g.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_qty_time rqt on cs.main_order_qty_time = rqt.idQty_time_sk
		left join
			Warehouse.sales.dim_rank_qty_time rqt2 on cs.avg_order_qty_time = rqt2.idQty_time_sk
		left join
			Warehouse.sales.dim_rank_freq_time rft on cs.avg_order_freq_time = rft.idFreq_time_sk
		-- inner join
			-- Warehouse.act.dim_rank_cr_time_mm rcrt on datediff(mm, cal_cre.calendar_date, getutcdate()) = rcrt.idCR_Time_MM_sk
		left join
			Warehouse.gen.dim_customer_origin cor on cs.idCustomerOrigin_sk_fk = cor.idCustomerOrigin_sk
		left join
			(select rank_qty_time, max_qty_time_bk qty_time_bk, lead(max_qty_time_bk) over (order by max_qty_time_bk) qty_time_bk_next
			from 
				(select rank_qty_time, max(qty_time_bk) max_qty_time_bk
				from Warehouse.sales.dim_rank_qty_time
				group by rank_qty_time) t) rqt3 on cs.stdev_order_qty_time >= rqt3.qty_time_bk and cs.stdev_order_qty_time < rqt3.qty_time_bk_next
		left join
			(select rank_freq_time, min_freq_time_bk freq_time_bk, isnull(lead(min_freq_time_bk) over (order by min_freq_time_bk), 36000) freq_time_bk_next
			from 
				(select rank_freq_time, min(freq_time_bk) min_freq_time_bk
				from Warehouse.sales.dim_rank_freq_time
				group by rank_freq_time) t) rft2 on cs.stdev_order_freq_time >= rft2.freq_time_bk and cs.stdev_order_freq_time < rft2.freq_time_bk_next
		left join
			Warehouse.gen.dim_calendar cal_ll on cs.idCalendarLastLoginDate_sk_fk = cal_ll.idCalendar_sk

go
			


drop view act.fact_customer_signature_scd_v
go 

create view act.fact_customer_signature_scd_v as

	select cs_scd.idCustomer_sk_fk,
		c.customer_id_bk customer_id, c.customer_email,
		s_cre.website website_create, s_cre.store_name store_create, 
		cal_cre.calendar_date create_date, 	
		s_reg.website website_register, s_reg.store_name store_register, 
		cal_reg.calendar_date register_date,
		cst.customer_status_name, 
		dateFrom, dateTo, isCurrent, 
		count(*) over (partition by c.customer_id_bk) num_rep_cust
	from
			(select idCustomer_sk_fk, 
				idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
				idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
				idCustomerStatusLifecycle_sk_fk, 
				convert(int, (CONVERT(VARCHAR(8), upd_ts, 112))) dateFrom, null dateTo, 
				'Y' isCurrent
			from 
					Warehouse.act.fact_customer_signature cs
			where idCustomer_sk_fk in 
				(select distinct idCustomer_sk_fk
				from Warehouse.act.fact_customer_signature_scd)
			union
			select idCustomer_sk_fk, 
				idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
				idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
				idCustomerStatusLifecycle_sk_fk, 
				idCalendarFrom_sk_fk dateFrom, idCalendarTo_sk_fk dateTo, 
				'N' isCurrent
			from 	
				Warehouse.act.fact_customer_signature_scd) cs_scd		

		inner join
			Warehouse.gen.dim_customer_v c on cs_scd.idCustomer_sk_fk = c.idCustomer_sk
		left join
			Warehouse.gen.dim_store_v s_cre on cs_scd.idStoreCreate_sk_fk = s_cre.idStore_sk
		left join
			Warehouse.gen.dim_calendar cal_cre on cs_scd.idCalendarCreateDate_sk_fk = cal_cre.idCalendar_sk
		inner join
			Warehouse.gen.dim_store_v s_reg on cs_scd.idStoreRegister_sk_fk = s_reg.idStore_sk
		inner join
			Warehouse.gen.dim_calendar cal_reg on cs_scd.idCalendarRegisterDate_sk_fk = cal_reg.idCalendar_sk
		inner join
			Warehouse.gen.dim_customer_status cst on cs_scd.idCustomerStatusLifecycle_sk_fk = cst.idCustomerStatus_sk
go



drop view act.fact_customer_product_signature_v
go 

create view act.fact_customer_product_signature_v as
	select cps.idCustomer_sk_fk, cs.customer_id, cs.customer_email, cs.num_tot_orders num_tot_orders_customer,
		-- count(*) over (partition by cps.idCustomer_sk_fk) num_dist_products,
		cps.num_dist_products,
		pf.product_type_name, pf.category_name, pf.product_family_group_name, pf.product_id_magento, pf.product_family_name, 
		dcf.calendar_date first_order_date, dcl.calendar_date last_order_date,
		cps.order_id_bk_first, cps.order_id_bk_last, 
		rcosn_o.rank_seq_no rank_num_tot_orders_product, cps.num_tot_orders num_tot_orders_product, cps.subtotal_tot_orders, 
		cps.product_percentage, cps.category_percentage
		-- convert(decimal(12, 4), cps.subtotal_tot_orders * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) product_percentage, 
		-- convert(decimal(12, 4), sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk, pf.category_name) * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) category_percentage
	from 
			Warehouse.act.fact_customer_product_signature cps
		inner join
			Warehouse.act.dim_customer_signature_v cs on cps.idCustomer_sk_fk = cs.idCustomer_sk_fk
		inner join
			Warehouse.prod.dim_product_family_v pf on cps.idProductFamily_sk_fk = pf.idProductFamily_sk
		inner join
			Warehouse.gen.dim_calendar dcf on cps.idCalendarFirstOrderDate_sk_fk = dcf.idCalendar_sk
		inner join
			Warehouse.gen.dim_calendar dcl on cps.idCalendarLastOrderDate_sk_fk = dcl.idCalendar_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn_o on cps.num_tot_orders = rcosn_o.idCustomer_order_seq_no_sk
go



drop view act.fact_activity_login_v
go 

create view act.fact_activity_login_v as
	select 
		al.idCustomer_sk_fk, c.customer_id_bk customer_id, 
		cal_f.calendar_date first_login_date, cal_l.calendar_date last_login_date, 
		al.num_tot_logins
	from 
			Warehouse.act.fact_activity_login al
		inner join
			Warehouse.gen.dim_customer_v c on al.idCustomer_sk_fk = c.idCustomer_sk
		inner join
			Warehouse.gen.dim_calendar cal_f on al.idCalendarFirstLogin_sk_fk = cal_f.idCalendar_sk
		inner join
			Warehouse.gen.dim_calendar cal_l on al.idCalendarLastLogin_sk_fk = cal_l.idCalendar_sk
go