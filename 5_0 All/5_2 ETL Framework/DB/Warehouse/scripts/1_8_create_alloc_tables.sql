use Warehouse
go 

--------------------------------------------------------

create table Warehouse.alloc.dim_order_type_erp(
	idOrderTypeERP_sk				int NOT NULL IDENTITY(1, 1), 
	order_type_erp_bk				varchar(50) NOT NULL,
	order_type_erp_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_order_type_erp add constraint [PK_alloc_dim_order_type_erp]
	primary key clustered (idOrderTypeERP_sk);
go

alter table Warehouse.alloc.dim_order_type_erp add constraint [DF_alloc_dim_order_type_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_order_type_erp add constraint [UNIQ_alloc_dim_order_type_erp_order_type_erp_bk]
	unique (order_type_erp_bk);
go



create table Warehouse.alloc.dim_order_type_erp_wrk(
	order_type_erp_bk				varchar(50) NOT NULL,
	order_type_erp_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_order_type_erp_wrk add constraint [DF_alloc_dim_order_type_erp_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_order_status_erp(
	idOrderStatusERP_sk				int NOT NULL IDENTITY(1, 1), 
	order_status_erp_bk				varchar(50) NOT NULL,
	order_status_erp_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_order_status_erp add constraint [PK_alloc_dim_order_status_erp]
	primary key clustered (idOrderStatusERP_sk);
go

alter table Warehouse.alloc.dim_order_status_erp add constraint [DF_alloc_dim_order_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_order_status_erp add constraint [UNIQ_alloc_dim_order_status_erp_order_status_erp_bk]
	unique (order_status_erp_bk);
go



create table Warehouse.alloc.dim_order_status_erp_wrk(
	order_status_erp_bk				varchar(50) NOT NULL,
	order_status_erp_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_order_status_erp_wrk add constraint [DF_alloc_dim_order_status_erp_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_shipment_status_erp(
	idShipmentStatusERP_sk			int NOT NULL IDENTITY(1, 1), 
	shipment_status_erp_bk			varchar(50) NOT NULL,
	shipment_status_erp_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_shipment_status_erp add constraint [PK_alloc_dim_shipment_status_erp]
	primary key clustered (idShipmentStatusERP_sk);
go

alter table Warehouse.alloc.dim_shipment_status_erp add constraint [DF_alloc_dim_shipment_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_shipment_status_erp add constraint [UNIQ_alloc_dim_shipment_status_erp_shipment_status_erp_bk]
	unique (shipment_status_erp_bk);
go



create table Warehouse.alloc.dim_shipment_status_erp_wrk(
	shipment_status_erp_bk			varchar(50) NOT NULL,
	shipment_status_erp_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_shipment_status_erp_wrk add constraint [DF_alloc_dim_shipment_status_erp_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_allocation_status(
	idAllocationStatus_sk			int NOT NULL IDENTITY(1, 1), 
	allocation_status_bk			varchar(50) NOT NULL,
	allocation_status_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_allocation_status add constraint [PK_alloc_dim_allocation_status]
	primary key clustered (idAllocationStatus_sk);
go

alter table Warehouse.alloc.dim_allocation_status add constraint [DF_alloc_dim_allocation_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_allocation_status add constraint [UNIQ_alloc_dim_allocation_status_allocation_status_bk]
	unique (allocation_status_bk);
go



create table Warehouse.alloc.dim_allocation_status_wrk(
	allocation_status_bk			varchar(50) NOT NULL,
	allocation_status_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_allocation_status_wrk add constraint [DF_alloc_dim_allocation_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_allocation_strategy(
	idAllocationStrategy_sk			int NOT NULL IDENTITY(1, 1), 
	allocation_strategy_bk			varchar(50) NOT NULL,
	allocation_strategy_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_allocation_strategy add constraint [PK_alloc_dim_allocation_strategy]
	primary key clustered (idAllocationStrategy_sk);
go

alter table Warehouse.alloc.dim_allocation_strategy add constraint [DF_alloc_dim_allocation_strategy_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_allocation_strategy add constraint [UNIQ_alloc_dim_allocation_strategy_allocation_strategy_bk]
	unique (allocation_strategy_bk);
go



create table Warehouse.alloc.dim_allocation_strategy_wrk(
	allocation_strategy_bk			varchar(50) NOT NULL,
	allocation_strategy_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_allocation_strategy_wrk add constraint [DF_alloc_dim_allocation_strategy_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_allocation_type(
	idAllocationType_sk				int NOT NULL IDENTITY(1, 1), 
	allocation_type_bk				varchar(50) NOT NULL,
	allocation_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_allocation_type add constraint [PK_alloc_dim_allocation_type]
	primary key clustered (idAllocationType_sk);
go

alter table Warehouse.alloc.dim_allocation_type add constraint [DF_alloc_dim_allocation_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_allocation_type add constraint [UNIQ_alloc_dim_allocation_type_allocation_type_bk]
	unique (allocation_type_bk);
go



create table Warehouse.alloc.dim_allocation_type_wrk(
	allocation_type_bk				varchar(50) NOT NULL,
	allocation_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_allocation_type_wrk add constraint [DF_alloc_dim_allocation_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.alloc.dim_allocation_record_type(
	idAllocationRecordType_sk		int NOT NULL IDENTITY(1, 1), 
	allocation_record_type_bk		varchar(50) NOT NULL,
	allocation_record_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.alloc.dim_allocation_record_type add constraint [PK_alloc_dim_allocation_record_type]
	primary key clustered (idAllocationRecordType_sk);
go

alter table Warehouse.alloc.dim_allocation_record_type add constraint [DF_alloc_dim_allocation_record_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.alloc.dim_allocation_record_type add constraint [UNIQ_alloc_dim_allocation_record_type_allocation_record_type_bk]
	unique (allocation_record_type_bk);
go



create table Warehouse.alloc.dim_allocation_record_type_wrk(
	allocation_record_type_bk		varchar(50) NOT NULL,
	allocation_record_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.alloc.dim_allocation_record_type_wrk add constraint [DF_alloc_dim_allocation_record_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

