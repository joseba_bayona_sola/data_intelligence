use Warehouse
go

create table Warehouse.stat.fact_customer_status(
	idCustomerStatus_sk_fk		int NOT NULL, 
	website_register			varchar(100) NOT NULL, 
	website_group_create		varchar(100) NOT NULL, 
	idCR_time_mm_sk_fk			int NOT NULL,
	stat_date					date NOT NULL,
	num_customers				int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.stat.fact_customer_status add constraint [PK_stat_fact_customer_status]
	primary key clustered (idCustomerStatus_sk_fk, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date);
go

alter table Warehouse.stat.fact_customer_status add constraint [DF_stat_fact_customer_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stat.fact_customer_status add constraint [FK_stat_fact_customer_status_dim_customer_status]
	foreign key (idCustomerStatus_sk_fk) references Warehouse.gen.dim_customer_status (idCustomerStatus_sk);
go

alter table Warehouse.stat.fact_customer_status add constraint [FK_stat_fact_customer_status_dim_rank_cr_time_mm]
	foreign key (idCR_time_mm_sk_fk) references Warehouse.act.dim_rank_cr_time_mm (idCR_time_mm_sk);
go



create table Warehouse.stat.fact_magento_unsubscribe(
	magento_unsubscribe_f		char(1) NOT NULL, 
	website_register			varchar(100) NOT NULL, 
	website_group_create		varchar(100) NOT NULL, 
	idCR_time_mm_sk_fk			int NOT NULL,
	stat_date					date NOT NULL,
	num_customers				int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.stat.fact_magento_unsubscribe add constraint [PK_stat_fact_magento_unsubscribe]
	primary key clustered (magento_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date);
go

alter table Warehouse.stat.fact_magento_unsubscribe add constraint [DF_stat_fact_magento_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stat.fact_magento_unsubscribe add constraint [FK_stat_fact_magento_unsubscribe_dim_rank_cr_time_mm]
	foreign key (idCR_time_mm_sk_fk) references Warehouse.act.dim_rank_cr_time_mm (idCR_time_mm_sk);
go



create table Warehouse.stat.fact_dotmailer_unsubscribe(
	dotmailer_unsubscribe_f		char(1) NOT NULL, 
	website_register			varchar(100) NOT NULL, 
	website_group_create		varchar(100) NOT NULL, 
	idCR_time_mm_sk_fk			int NOT NULL,
	stat_date					date NOT NULL,
	num_customers				int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.stat.fact_dotmailer_unsubscribe add constraint [PK_stat_fact_dotmailer_unsubscribe]
	primary key clustered (dotmailer_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date);
go

alter table Warehouse.stat.fact_dotmailer_unsubscribe add constraint [DF_stat_fact_dotmailer_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stat.fact_dotmailer_unsubscribe add constraint [FK_stat_fact_dotmailer_unsubscribe_dim_rank_cr_time_mm]
	foreign key (idCR_time_mm_sk_fk) references Warehouse.act.dim_rank_cr_time_mm (idCR_time_mm_sk);
go



create table Warehouse.stat.fact_sms_unsubscribe(
	sms_unsubscribe_f			char(1) NOT NULL, 
	website_register			varchar(100) NOT NULL, 
	website_group_create		varchar(100) NOT NULL, 
	idCR_time_mm_sk_fk			int NOT NULL,
	stat_date					date NOT NULL,
	num_customers				int NOT NULL,
	idETLBatchRun_ins			bigint NOT NULL, 
	ins_ts						datetime NOT NULL, 
	idETLBatchRun_upd			bigint, 
	upd_ts						datetime);
go 

alter table Warehouse.stat.fact_sms_unsubscribe add constraint [PK_stat_fact_sms_unsubscribe]
	primary key clustered (sms_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date);
go

alter table Warehouse.stat.fact_sms_unsubscribe add constraint [DF_stat_fact_sms_unsubscribe_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stat.fact_sms_unsubscribe add constraint [FK_stat_fact_sms_unsubscribe_dim_rank_cr_time_mm]
	foreign key (idCR_time_mm_sk_fk) references Warehouse.act.dim_rank_cr_time_mm (idCR_time_mm_sk);
go
