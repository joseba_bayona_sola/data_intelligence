use Warehouse 
go

select idStockMethodType_sk, stock_method_type_bk, stock_method_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_stock_method_type

select stock_method_type_bk, stock_method_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_stock_method_type_wrk


select idStockBatchType_sk, stock_batch_type_bk, stock_batch_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_stock_batch_type

select stock_batch_type_bk, stock_batch_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_stock_batch_type_wrk


select idStockAdjustmentType_sk, stock_adjustment_type_bk, stock_adjustment_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_stock_adjustment_type

select stock_adjustment_type_bk, stock_adjustment_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_stock_adjustment_type_wrk


select idStockMovementType_sk, stock_movement_type_bk, stock_movement_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_stock_movement_type

select stock_movement_type_bk, stock_movement_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_stock_movement_type_wrk


select idStockMovementPeriod_sk, stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_stock_movement_period

select stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish,
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_stock_movement_period_wrk



select idWHStockItem_sk, warehousestockitemid_bk, 
	idWarehouse_sk_fk, idStockItem_sk_fk, idStockMethodType_sk_fk, 
	wh_stock_item_description, 
	
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, qty_due_in,
	stocked, 
	
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_wh_stock_item

select warehousestockitemid_bk, 
	idWarehouse_sk_fk, idStockItem_sk_fk, idStockMethodType_sk_fk, 
	wh_stock_item_description, 
	
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, qty_due_in,
	stocked, 
	
	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_wh_stock_item_wrk


select idWHStockItemBatch_sk, warehousestockitembatchid_bk, 
	idWHStockItem_sk_fk, idStockBatchType_sk_fk, 
	batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 
	
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, 
	qty_registered_sm, qty_disposed_sm, 

	batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 
	
	local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
	local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
	local_to_global_rate, currency_code, 
	delete_f, 

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.dim_wh_stock_item_batch

select warehousestockitembatchid_bk, 
	idWHStockItem_sk_fk, idStockBatchType_sk_fk, 
	batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 
	
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, 
	qty_registered_sm, qty_disposed_sm, 

	batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 
	
	local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
	local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
	local_to_global_rate, currency_code, 
	delete_f, 

	idETLBatchRun_ins, ins_ts
from Warehouse.stock.dim_wh_stock_item_batch_wrk


select idWHStockItemBatchMovement_sk, batchstockmovementid_bk, 
	batch_stock_move_id, move_id, move_line_id, 
	
	idWHStockItemBatch_sk_fk, idStockAdjustmentType_sk_fk, idStockMovementType_sk_fk, idStockMovementPeriod_sk_fk, 
	
	qty_movement, qty_registered, qty_disposed, 
	batch_movement_date, 
	delete_f, 

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.stock.fact_wh_stock_item_batch_movement

select batchstockmovementid_bk, 
	batch_stock_move_id, move_id, move_line_id, 
	
	idWHStockItemBatch_sk_fk, idStockAdjustmentType_sk_fk, idStockMovementType_sk_fk, idStockMovementPeriod_sk_fk, 
	
	qty_movement, qty_registered, qty_disposed, 
	batch_movement_date, 
	delete_f, 

	idETLBatchRun_ins, ins_ts
from Warehouse.stock.fact_wh_stock_item_batch_movement_wrk
