
use Warehouse
go


select idOrderTypeERP_sk, order_type_erp_bk, order_type_erp_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_order_type_erp

select order_type_erp_bk, order_type_erp_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_order_type_erp_wrk


select idOrderStatusERP_sk, order_status_erp_bk, order_status_erp_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_order_status_erp

select order_status_erp_bk, order_status_erp_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_order_status_erp_wrk


select idShipmentStatusERP_sk, shipment_status_erp_bk, shipment_status_erp_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_shipment_status_erp

select shipment_status_erp_bk, shipment_status_erp_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_shipment_status_erp_wrk


select idAllocationStatus_sk, allocation_status_bk, allocation_status_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_allocation_status

select allocation_status_bk, allocation_status_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_allocation_status_wrk


select idAllocationStrategy_sk, allocation_strategy_bk, allocation_strategy_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_allocation_strategy

select allocation_strategy_bk, allocation_strategy_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_allocation_strategy_wrk


select idAllocationType_sk, allocation_type_bk, allocation_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_allocation_type

select allocation_type_bk, allocation_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_allocation_type_wrk


select idAllocationRecordType_sk, allocation_record_type_bk, allocation_record_type_name, description, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.alloc.dim_allocation_record_type

select allocation_record_type_bk, allocation_record_type_name, description, 
	idETLBatchRun_ins, ins_ts
from Warehouse.alloc.dim_allocation_record_type_wrk
