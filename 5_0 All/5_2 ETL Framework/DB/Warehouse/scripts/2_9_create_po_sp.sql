use Warehouse
go


drop procedure po.stg_dwh_merge_po_po_source
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from PO Source from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_po_source
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_po_source with (tablock) as trg
	using Warehouse.po.dim_po_source_wrk src
		on (trg.po_source_bk = src.po_source_bk)
	when matched and not exists 
		(select isnull(trg.po_source_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.po_source_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.po_source_name = src.po_source_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (po_source_bk, po_source_name, description, idETLBatchRun_ins)
				values (src.po_source_bk, src.po_source_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure po.stg_dwh_merge_po_po_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from PO Type from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_po_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_po_type with (tablock) as trg
	using Warehouse.po.dim_po_type_wrk src
		on (trg.po_type_bk = src.po_type_bk)
	when matched and not exists 
		(select isnull(trg.po_type_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.po_type_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.po_type_name = src.po_type_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (po_type_bk, po_type_name, description, idETLBatchRun_ins)
				values (src.po_type_bk, src.po_type_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure po.stg_dwh_merge_po_po_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from PO Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_po_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_po_status with (tablock) as trg
	using Warehouse.po.dim_po_status_wrk src
		on (trg.po_status_bk = src.po_status_bk)
	when matched and not exists 
		(select isnull(trg.po_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.po_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.po_status_name = src.po_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (po_status_bk, po_status_name, description, idETLBatchRun_ins)
				values (src.po_status_bk, src.po_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure po.stg_dwh_merge_po_pol_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from POL Status from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_pol_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_pol_status with (tablock) as trg
	using Warehouse.po.dim_pol_status_wrk src
		on (trg.pol_status_bk = src.pol_status_bk)
	when matched and not exists 
		(select isnull(trg.pol_status_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.pol_status_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.pol_status_name = src.pol_status_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (pol_status_bk, pol_status_name, description, idETLBatchRun_ins)
				values (src.pol_status_bk, src.pol_status_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure po.stg_dwh_merge_po_pol_problems
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from POL Problems from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_pol_problems
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_pol_problems with (tablock) as trg
	using Warehouse.po.dim_pol_problems_wrk src
		on (trg.pol_problems_bk = src.pol_problems_bk)
	when matched and not exists 
		(select isnull(trg.pol_problems_name, ''), isnull(trg.description, '')
		intersect
		select isnull(src.pol_problems_name, ''), isnull(src.description, ''))
		then 
			update set
				trg.pol_problems_name = src.pol_problems_name, trg.description = src.description, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (pol_problems_bk, pol_problems_name, description, idETLBatchRun_ins)
				values (src.pol_problems_bk, src.pol_problems_name, src.description, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure po.stg_dwh_merge_po_wh_operator
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from WH Operator from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_wh_operator
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_wh_operator with (tablock) as trg
	using Warehouse.po.dim_wh_operator_wrk src
		on (trg.wh_operator_bk = src.wh_operator_bk)
	when matched and not exists 
		(select isnull(trg.wh_operator_name, '')
		intersect
		select isnull(src.wh_operator_name, ''))
		then 
			update set
				trg.wh_operator_name = src.wh_operator_name, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (wh_operator_bk, wh_operator_name, idETLBatchRun_ins)
				values (src.wh_operator_bk, src.wh_operator_name, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure po.stg_dwh_merge_po_purchase_order
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Purchase Order from Working table into Final DWH Dim Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_purchase_order
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.dim_purchase_order with (tablock) as trg
	using Warehouse.po.dim_purchase_order_wrk src
		on (trg.purchaseorderid_bk = src.purchaseorderid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idWarehouse_sk_fk, 0), isnull(trg.idSupplier_sk_fk, 0), 
			isnull(trg.idPOSource_sk_fk, 0), isnull(trg.idPOType_sk_fk, 0), isnull(trg.idPOStatus_sk_fk, 0), isnull(trg.idWHOperatorCreate_sk_fk, 0), isnull(trg.idWHOperatorAssigned_sk_fk, 0), 
			isnull(trg.purchase_order_number, ''), isnull(trg.leadtime, 0), 
			isnull(trg.created_date, ''), isnull(trg.approved_date, ''), isnull(trg.confirmed_date, ''), isnull(trg.submitted_date, ''), isnull(trg.completed_date, ''), isnull(trg.due_date, ''), 
			isnull(trg.local_total_cost, 0), isnull(trg.local_to_global_rate, 0), isnull(trg.currency_code, '')

		intersect
		select 
			isnull(src.idWarehouse_sk_fk, 0), isnull(src.idSupplier_sk_fk, 0), 
			isnull(src.idPOSource_sk_fk, 0), isnull(src.idPOType_sk_fk, 0), isnull(src.idPOStatus_sk_fk, 0), isnull(src.idWHOperatorCreate_sk_fk, 0), isnull(src.idWHOperatorAssigned_sk_fk, 0), 
			isnull(src.purchase_order_number, ''), isnull(src.leadtime, 0), 
			isnull(src.created_date, ''), isnull(src.approved_date, ''), isnull(src.confirmed_date, ''), isnull(src.submitted_date, ''), isnull(src.completed_date, ''), isnull(src.due_date, ''), 
			isnull(src.local_total_cost, 0), isnull(src.local_to_global_rate, 0), isnull(src.currency_code, ''))
		then 
			update set
				trg.idWarehouse_sk_fk = src.idWarehouse_sk_fk, trg.idSupplier_sk_fk = src.idSupplier_sk_fk, 
				trg.idPOSource_sk_fk = src.idPOSource_sk_fk, trg.idPOType_sk_fk = src.idPOType_sk_fk, trg.idPOStatus_sk_fk = src.idPOStatus_sk_fk, trg.idWHOperatorCreate_sk_fk = src.idWHOperatorCreate_sk_fk, trg.idWHOperatorAssigned_sk_fk = src.idWHOperatorAssigned_sk_fk, 
				trg.purchase_order_number = src.purchase_order_number, trg.leadtime = src.leadtime, 
				trg.created_date = src.created_date, trg.approved_date = src.approved_date, trg.confirmed_date = src.confirmed_date, trg.submitted_date = src.submitted_date, trg.completed_date = src.completed_date, trg.due_date = src.due_date, 
				trg.local_total_cost = src.local_total_cost, trg.local_to_global_rate = src.local_to_global_rate, trg.currency_code = src.currency_code, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (purchaseorderid_bk, 
				idWarehouse_sk_fk, idSupplier_sk_fk, 
				idPOSource_sk_fk, idPOType_sk_fk, idPOStatus_sk_fk, idWHOperatorCreate_sk_fk, idWHOperatorAssigned_sk_fk, 
				purchase_order_number, leadtime, 
				created_date, approved_date, confirmed_date, submitted_date, completed_date, due_date, 
				local_total_cost, local_to_global_rate, currency_code,
				idETLBatchRun_ins)
				
				values (src.purchaseorderid_bk, 
					src.idWarehouse_sk_fk, src.idSupplier_sk_fk, 
					src.idPOSource_sk_fk, src.idPOType_sk_fk, src.idPOStatus_sk_fk, src.idWHOperatorCreate_sk_fk, src.idWHOperatorAssigned_sk_fk, 
					src.purchase_order_number, src.leadtime, 
					src.created_date, src.approved_date, src.confirmed_date, src.submitted_date, src.completed_date, src.due_date, 
					src.local_total_cost, src.local_to_global_rate, src.currency_code, 
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure po.stg_dwh_merge_po_purchase_order_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data from Purchase Order Line from Working table into Final DWH Fact Table
-- ==========================================================================================

create procedure po.stg_dwh_merge_po_purchase_order_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.po.fact_purchase_order_line with (tablock) as trg
	using Warehouse.po.fact_purchase_order_line_wrk src
		on (trg.purchaseorderlineid_bk = src.purchaseorderlineid_bk)
	when matched and not exists 
		(select 
			isnull(trg.idPurchaseOrder_sk_fk, 0), isnull(trg.idPOLStatus_sk_fk, 0), isnull(trg.idPOLProblems_sk_fk, 0), 
			isnull(trg.idStockItem_sk_fk, 0), isnull(trg.idProductFamilyPrice_sk_fk, 0), 
			isnull(trg.purchase_order_line_id, 0), 
			isnull(trg.quantity_ordered, 0), isnull(trg.quantity_received, 0), isnull(trg.quantity_planned, 0), isnull(trg.quantity_open, 0), isnull(trg.quantity_cancelled, 0), 
			isnull(trg.local_unit_cost, 0), isnull(trg.local_line_cost, 0), isnull(trg.local_to_global_rate, 0), isnull(trg.currency_code, '')
		intersect
		select 
			isnull(src.idPurchaseOrder_sk_fk, 0), isnull(src.idPOLStatus_sk_fk, 0), isnull(src.idPOLProblems_sk_fk, 0), 
			isnull(src.idStockItem_sk_fk, 0), isnull(src.idProductFamilyPrice_sk_fk, 0), 
			isnull(src.purchase_order_line_id, 0), 
			isnull(src.quantity_ordered, 0), isnull(src.quantity_received, 0), isnull(src.quantity_planned, 0), isnull(src.quantity_open, 0), isnull(src.quantity_cancelled, 0), 
			isnull(src.local_unit_cost, 0), isnull(src.local_line_cost, 0), isnull(src.local_to_global_rate, 0), isnull(src.currency_code, ''))
		then 
			update set
				trg.idPurchaseOrder_sk_fk = src.idPurchaseOrder_sk_fk, trg.idPOLStatus_sk_fk = src.idPOLStatus_sk_fk, trg.idPOLProblems_sk_fk = src.idPOLProblems_sk_fk, 
				trg.idStockItem_sk_fk = src.idStockItem_sk_fk, trg.idProductFamilyPrice_sk_fk = src.idProductFamilyPrice_sk_fk, 
				trg.purchase_order_line_id = src.purchase_order_line_id, 
				trg.quantity_ordered = src.quantity_ordered, trg.quantity_received = src.quantity_received, trg.quantity_planned = src.quantity_planned, trg.quantity_open = src.quantity_open, trg.quantity_cancelled = src.quantity_cancelled, 
				trg.local_unit_cost = src.local_unit_cost, trg.local_line_cost = src.local_line_cost, trg.local_to_global_rate = src.local_to_global_rate, trg.currency_code = src.currency_code, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (purchaseorderlineid_bk, 
				idPurchaseOrder_sk_fk, idPOLStatus_sk_fk, idPOLProblems_sk_fk, 
				idStockItem_sk_fk, idProductFamilyPrice_sk_fk, 
				purchase_order_line_id, 
				quantity_ordered, quantity_received, quantity_planned, quantity_open, quantity_cancelled, 
				local_unit_cost, local_line_cost, local_to_global_rate, currency_code, 
				idETLBatchRun_ins)

				values (src.purchaseorderlineid_bk, 
					src.idPurchaseOrder_sk_fk, src.idPOLStatus_sk_fk, src.idPOLProblems_sk_fk, 
					src.idStockItem_sk_fk, src.idProductFamilyPrice_sk_fk, 
					src.purchase_order_line_id, 
					src.quantity_ordered, src.quantity_received, src.quantity_planned, src.quantity_open, src.quantity_cancelled, 
					src.local_unit_cost, src.local_line_cost, src.local_to_global_rate, src.currency_code,
					@idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
