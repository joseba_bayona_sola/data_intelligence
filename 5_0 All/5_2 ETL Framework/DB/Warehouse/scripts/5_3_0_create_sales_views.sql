
use Warehouse
go

drop view gen.dim_store_v
go 

create view gen.dim_store_v as
	select c.idCompany_sk, c.company_name, 
		s.idStore_sk, s.store_id_bk, s.store_name, s.website_type, s.website_group, s.website, s.tld, s.code_tld, s.acquired
	from 
			Warehouse.gen.dim_store s
		inner join
			Warehouse.gen.dim_company c on s.idCompany_sk_fk = c.idCompany_sk
go


drop view gen.dim_warehouse_v
go 

create view gen.dim_warehouse_v as
	select idWarehouse_sk, warehouseid_bk, warehouse_name, wh_short_name
	from Warehouse.gen.dim_warehouse
go


drop view gen.dim_supplier_v
go 

create view gen.dim_supplier_v as
	select s.idSupplier_sk, s.supplier_id_bk, s.idSupplierType_sk_fk, st.supplier_type_name,
		s.supplier_name, s.default_lead_time, s.default_transit_lead_time
	from 
			Warehouse.gen.dim_supplier s
		inner join
			Warehouse.gen.dim_supplier_type st on s.idSupplierType_sk_fk = st.idSupplierType_sk
go


drop view gen.dim_customer_v
go 

create view gen.dim_customer_v as
	select c.idCustomer_sk, 
		c.customer_id_bk, c.customer_email,
		c.created_at, c.updated_at,
		c.idStore_sk_fk, s.store_id_bk, s.store_name, 
		c.idMarket_sk_fk, m.market_id_bk, m.market_name,
		c.idCustomerType_sk_fk, ct.customer_type_name,
		c.idCustomerStatus_sk_fk, cs.customer_status_name, 
		c.prefix, c.first_name, c.last_name, 
		c.migrate_customer_f, c.migrate_customer_store, c.migrate_customer_id
	from 
			Warehouse.gen.dim_customer c
		inner join
			Warehouse.gen.dim_store_v s on c.idStore_sk_fk = s.idStore_sk
		left join
			Warehouse.gen.dim_market m on c.idMarket_sk_fk = m.idMarket_sk
		inner join
			Warehouse.gen.dim_customer_type ct on c.idCustomerType_sk_fk = ct.idCustomerType_sk
		left join
			Warehouse.gen.dim_customer_status cs on c.idCustomerStatus_sk_fk = cs.idCustomerStatus_sk
go


drop view gen.dim_customer_scd_v
go 

create view gen.dim_customer_scd_v as
	select cscd.idCustomerSCD_sk, c.idCustomer_sk idCustomer_sk_fk, 
		c.customer_id_bk, c.customer_email,
		c.created_at, c.updated_at,
		c.idStore_sk_fk, c.store_id_bk, c.store_name, 
		c.idMarket_sk_fk, c.market_id_bk, c.market_name,
		c.idCustomerType_sk_fk, c.customer_type_name,
		c.prefix, c.first_name, c.last_name, cscd.dob, cscd.gender,
		cscd.language, 
		cscd.phone_number, cscd.street, cscd.city, cscd.postcode, cscd.region, 
		cscd.idRegion_sk_fk, r.region_name, cscd.idCountry_sk_fk, co_b.country_code, 
		cscd.postcode_s, cscd.idCountry_s_sk_fk, co_s.country_code country_code_s, 
		cu.customer_unsubscribe_name, 
		cscd.unsubscribe_mark_email_magento_f, cscd.unsubscribe_mark_email_dotmailer_f, cscd.unsubscribe_text_message_f, 
		cscd.reminder_f, cscd.idReminderType_sk_fk, rt.reminder_type_name, cscd.idReminderPeriod_sk_fk, rp.reminder_period_name,
		cscd.reorder_f, cscd.days_worn_info
	from 
			Warehouse.gen.dim_customer_scd cscd
		inner join
			Warehouse.gen.dim_customer_v c on cscd.idCustomer_sk_fk = c.idCustomer_sk
		left join
			Warehouse.gen.dim_region r on cscd.idRegion_sk_fk = r.idRegion_sk
		inner join
			Warehouse.gen.dim_country co_b on cscd.idCountry_sk_fk = co_b.idCountry_sk
		inner join
			Warehouse.gen.dim_country co_s on cscd.idCountry_s_sk_fk = co_s.idCountry_sk
		inner join
			Warehouse.sales.dim_reminder_type rt on cscd.idReminderType_sk_fk = rt.idReminderType_sk
		inner join
			Warehouse.sales.dim_reminder_period rp on cscd.idReminderPeriod_sk_fk = rp.idReminderPeriod_sk
		inner join
			Warehouse.gen.dim_customer_unsubscribe cu on cscd.idCustomerUnsubscribe_sk_fk = cu.idCustomerUnsubscribe_sk
	where cscd.isCurrent = 'Y'
go



drop view prod.dim_product_family_v
go 

create view prod.dim_product_family_v as
	select pf.idProductFamily_sk, pf.product_id_bk product_id_magento,
		m.idManufacturer_sk, m.manufacturer_name,
		c.idProductType_sk, c.product_type_name, c.idCategory_sk, c.category_name, ptoh.product_type_oh_name,
		pfg.idProductFamilyGroup_sk, pfg.product_family_group_name, clt.idCL_Type_sk, clt.cl_type_name, clf.idCL_Feature_sk, clf.cl_feature_name, 
		pf.magento_sku, pf.product_family_name, pf.glass_sunglass_name, pf.glass_sunglass_colour, 
		pl.idProductLifecycle_sk, pl.product_lifecycle_name, pv.idProductVisibility_sk, pv.product_visibility_name,
		pf.status
	from 
			Warehouse.prod.dim_product_family pf
		left join
			Warehouse.prod.dim_manufacturer m on pf.idManufacturer_sk_fk = m.idManufacturer_sk

		inner join
			(select pt.idProductType_sk, pt.product_type_name,
				c.idCategory_sk, c.category_name
			from 
					Warehouse.prod.dim_category c
				inner join
					Warehouse.prod.dim_product_type pt on c.idProductType_sk_fk = pt.idProductType_sk) c on pf.idCategory_sk_fk = c.idCategory_sk

		left join
			Warehouse.prod.dim_cl_type clt on pf.idCL_Type_sk_fk = clt.idCL_Type_sk
		left join
			Warehouse.prod.dim_cl_feature clf on pf.idCL_Feature_sk_fk = clf.idCL_Feature_sk

		inner join
			Warehouse.prod.dim_product_lifecycle pl on pf.idProductLifecycle_sk_fk = pl.idProductLifecycle_sk
		inner join
			Warehouse.prod.dim_product_visibility pv on pf.idProductVisibility_sk_fk = pv.idProductVisibility_sk

		left join
			Warehouse.prod.dim_product_family_group pfg on pf.idProductFamilyGroup_sk_fk = pfg.idProductFamilyGroup_sk

		left join
			Warehouse.prod.dim_product_type_oh ptoh on pf.idProductTypeOH_sk_fk = ptoh.idProductTypeOH_sk
	where pf.idProductFamily_sk <> -1
go


drop view prod.dim_product_family_pack_size_v
go 

create view prod.dim_product_family_pack_size_v as

	select pfps.idProductFamilyPacksize_sk, pfps.packsizeid_bk,
		pf.idProductFamily_sk, pf.product_id_magento, 
		pf.manufacturer_name, pf.product_type_name, pf.category_name, pf.product_type_oh_name, pf.product_family_group_name,
		pf.product_family_name, pfps.size, pfps.product_family_packsize_name, 
		pfps.allocation_preference_order, pfps.purchasing_preference_order
	from 
			Warehouse.prod.dim_product_family_pack_size pfps
		inner join
			Warehouse.prod.dim_product_family_v pf on pfps.idProductFamily_sk_fk = pf.idProductFamily_sk
go


drop view prod.dim_product_family_price_v
go 

create view prod.dim_product_family_price_v as
	select pfp.idProductFamilyPrice_sk, pfp.supplierpriceid_bk,
		s.idSupplier_sk, s.supplier_type_name, s.supplier_name,
		pfps.idProductFamilyPackSize_sk, pfps.product_id_magento, 
		pfps.manufacturer_name, pfps.product_type_name, pfps.category_name, pfps.product_type_oh_name, pfps.product_family_group_name,
		pfps.product_family_name, pfps.size,
		pt.idPriceTypePF_sk, pt.price_type_pf_name,
		pfp.unit_price, pfp.currency_code, pfp.lead_time, 
		pfp.effective_date, pfp.expiry_date, pfp.active
	from 
			Warehouse.prod.dim_product_family_price pfp
		inner join
			Warehouse.prod.dim_product_family_pack_size_v pfps on pfp.idProductFamilyPackSize_sk_fk = pfps.idProductFamilyPackSize_sk
		inner join
			Warehouse.gen.dim_supplier_v s on pfp.idSupplier_sk_fk = s.idSupplier_sk
		inner join
			Warehouse.prod.dim_price_type_pf pt on pfp.idPriceTypePF_sk_fk = pt.idPriceTypePF_sk
go


drop view prod.dim_product_v
go 

create view prod.dim_product_v as
	select p.idProduct_sk, p.productid_erp_bk, 
		pf.idProductFamily_sk, pf.product_id_magento, 
		pf.manufacturer_name, pf.product_type_name, pf.category_name, pf.product_type_oh_name, pf.product_family_group_name,
		pf.product_family_name,
		p.idBC_sk_fk base_curve, p.idDI_sk_fk diameter, p.idPO_sk_fk power, p.idCY_sk_fk cylinder, p.idAX_sk_fk axis, p.idAD_sk_fk addition, p.idDO_sk_fk dominance, 
		c.colour_name colour,
		p.parameter, p.product_description
	from 
			Warehouse.prod.dim_product p
		inner join
			Warehouse.prod.dim_product_family_v pf on p.idProductFamily_sk_fk = pf.idProductFamily_sk
		left join
			Warehouse.prod.dim_param_col c on p.idCOL_sk_fk = c.idCOL_sk
go


drop view prod.dim_stock_item_v
go 

create view prod.dim_stock_item_v as
	select si.idStockItem_sk, si.stockitemid_bk, 
		p.idProduct_sk, p.idProductFamily_sk, p.product_id_magento, 
		p.manufacturer_name, p.product_type_name, p.category_name, p.product_type_oh_name, p.product_family_group_name,
		p.product_family_name, si.packsize, 
		p.parameter, p.product_description,
		si.SKU, si.stock_item_description
	from 
			Warehouse.prod.dim_stock_item si
		inner join
			Warehouse.prod.dim_product_v p on si.idProduct_sk_fk = p.idProduct_sk
go

drop view sales.dim_shipping_method_v
go 

create view sales.dim_shipping_method_v as
	select sc.idShippingCarrier_sk, sc.shipping_carrier_name, 
		sm.idShippingMethod_sk, sm.shipping_method_name, sm.description
	from 
			Warehouse.sales.dim_shipping_method sm
		inner join
			Warehouse.sales.dim_shipping_carrier sc on sm.idShippingCarrier_sk_fk = sc.idShippingCarrier_sk
go


drop view sales.dim_coupon_code_v
go 

create view sales.dim_coupon_code_v as
	select gcc.idGroupCouponCode_sk, gcc.group_coupon_code_name, 
		cc.idCouponCode_sk, cc.coupon_id_bk, cc.coupon_code, 
		cc.coupon_code_name, cc.from_date, cc.to_date, cc.expiration_date, cc.discount_type_name, cc.discount_amount, 
		cc.only_new_customer, cc.refer_a_friend
	from 
			Warehouse.sales.dim_coupon_code cc
		inner join
			Warehouse.sales.dim_group_coupon_code gcc on cc.idGroupCouponCode_sk_fk = gcc.idGroupCouponCode_sk
go





drop view sales.dim_order_header_v
go 

create view sales.dim_order_header_v as
	select oh.idOrderHeader_sk,
		oh.order_id_bk, oh.invoice_id, oh.shipment_id, oh.creditmemo_id, 
		oh.order_no, oh.invoice_no, oh.shipment_no, oh.creditmemo_no, 
 		oh.order_date, cal_o.calendar_date order_date_c, cal_o.calendar_date_week_name order_week_day, tim_o.time_name order_time_name, tim_o.day_part order_day_part,
		oh.invoice_date, cal_i.calendar_date invoice_date_c, cal_i.calendar_date_week_name invoice_week_day, 
		oh.shipment_date, cal_s.calendar_date shipment_date_c, cal_s.calendar_date_week_name shipment_week_day, tim_s.time_name shipment_time_name, tim_s.day_part shipment_day_part,
		oh.idCalendarRefundDate_sk_fk, oh.refund_date, cal_re.calendar_date refund_date_c, cal_re.calendar_date_week_name refund_week_day, 
		rsd.rank_shipping_days, datediff(dd, oh.order_date, oh.shipment_date) shipping_days,
		s.acquired, s.tld, s.website_group, s.website, s.store_name, 
	 	c.company_name_create, c.website_group_create, c.website_create,
		c.customer_origin_name,
		m.market_name, -- c.market_name, 
		c.customer_id customer_id, c.customer_email, c.first_name + ' ' + c.last_name customer_name,
		co_ship.country_code country_code_ship, co_ship.country_name country_name_ship, oh.postcode_shipping, 
		co_bill.country_code country_code_bill, co_bill.country_name country_name_bill, oh.postcode_billing, 
		c_uns.customer_unsubscribe_name,
		o_stag.order_stage_name,  o_stat.order_status_name, oh.adjustment_order, o_type.order_type_name,  o_statm.order_status_magento_name, 
		pm.payment_method_name, cc.cc_type_name, shm.shipping_carrier_name, shm.shipping_method_name, te.telesales_username, prt.price_type_name, oh.discount_f, prm.prescription_method_name,
		rt.reminder_type_name, rp.reminder_period_name, oh.reminder_date, oh.reorder_f, oh.reorder_date, 
		ch.channel_name, mch.marketing_channel_name, cco.group_coupon_code_name, cco.coupon_code,
		cs.customer_status_name, rcosn.rank_seq_no, oh.customer_order_seq_no, rcosnw.rank_seq_no rank_seq_no_web, oh.customer_order_seq_no_web, rcosng.rank_seq_no rank_seq_no_gen, oh.customer_order_seq_no_gen, 
		oh.order_source, 
		case 
			when (oh.proforma = 'H') then 'Historic'
			when (oh.proforma = 'Y') then 'Proforma'
			when (oh.proforma = 'N') then 'Vision Direct'
		end proforma,
		pt.product_type_oh_name, rqt.rank_qty_time rank_order_qty_time, oh.order_qty_time, oh.num_diff_product_type_oh,

		oh.countries_registered_code,

		oh.total_qty_unit, oh.total_qty_unit_refunded, oh.total_qty_pack, -- oh.weight, 

		oh.local_subtotal, oh.local_shipping, oh.local_discount, oh.local_store_credit_used, 
		oh.local_total_inc_vat, oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, 
		oh.local_total_refund, oh.local_store_credit_given, oh.local_bank_online_given, 
		oh.local_subtotal_refund, oh.local_shipping_refund, oh.local_discount_refund, oh.local_store_credit_used_refund, oh.local_adjustment_refund, 
		oh.local_total_aft_refund_inc_vat, oh.local_total_aft_refund_exc_vat, oh.local_total_aft_refund_vat, oh.local_total_aft_refund_prof_fee, 
		-- oh.local_product_cost, oh.local_shipping_cost, oh.local_total_cost, oh.local_margin, 

		convert(decimal(12, 4), oh.local_subtotal * oh.local_to_global_rate) global_subtotal, convert(decimal(12, 4), oh.local_shipping * oh.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), oh.local_discount * oh.local_to_global_rate) global_discount, convert(decimal(12, 4), oh.local_store_credit_used * oh.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), oh.local_total_inc_vat * oh.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), oh.local_total_exc_vat * oh.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), oh.local_total_vat * oh.local_to_global_rate) global_total_vat, convert(decimal(12, 4), oh.local_total_prof_fee * oh.local_to_global_rate) global_total_prof_fee, 
		convert(decimal(12, 4), oh.local_total_refund * oh.local_to_global_rate) global_total_refund, 
		convert(decimal(12, 4), oh.local_store_credit_given * oh.local_to_global_rate) global_store_credit_given, convert(decimal(12, 4), oh.local_bank_online_given * oh.local_to_global_rate) global_bank_online_given, 
		convert(decimal(12, 4), oh.local_subtotal_refund * oh.local_to_global_rate) global_subtotal_refund, convert(decimal(12, 4), oh.local_shipping_refund * oh.local_to_global_rate) global_shipping_refund, 
		convert(decimal(12, 4), oh.local_discount_refund * oh.local_to_global_rate) global_discount_refund, convert(decimal(12, 4), oh.local_store_credit_used_refund * oh.local_to_global_rate) global_store_credit_used_refund, 
		convert(decimal(12, 4), oh.local_adjustment_refund * oh.local_to_global_rate) global_adjustment_refund, 
		convert(decimal(12, 4), oh.local_total_aft_refund_inc_vat * oh.local_to_global_rate) global_total_aft_refund_inc_vat, convert(decimal(12, 4), oh.local_total_aft_refund_exc_vat * oh.local_to_global_rate) global_total_aft_refund_exc_vat, 
		convert(decimal(12, 4), oh.local_total_aft_refund_vat * oh.local_to_global_rate) global_total_aft_refund_vat, convert(decimal(12, 4), oh.local_total_aft_refund_prof_fee * oh.local_to_global_rate) global_total_aft_refund_prof_fee, 
		-- convert(decimal(12, 4), oh.local_product_cost * oh.local_to_global_rate) global_product_cost, convert(decimal(12, 4), oh.local_shipping_cost * oh.local_to_global_rate) global_shipping_cost, 
		-- convert(decimal(12, 4), oh.local_total_cost * oh.local_to_global_rate) global_total_cost, convert(decimal(12, 4), oh.local_margin * oh.local_to_global_rate) global_margin, 

		oh.local_to_global_rate, oh.order_currency_code, 
		oh.discount_percent, oh.vat_percent, oh.prof_fee_percent,
		oh.num_lines, 
		oh.idETLBatchRun_ins, oh.idETLBatchRun_upd 
	from 
			Warehouse.sales.dim_order_header oh
		inner join
			Warehouse.gen.dim_calendar cal_o on oh.idCalendarOrderDate_sk_fk = cal_o.idCalendar_sk
		inner join	
			Warehouse.gen.dim_time tim_o on oh.idTimeOrderDate_sk_fk = tim_o.idTime_sk
		left join
			Warehouse.gen.dim_calendar cal_i on oh.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_s on oh.idCalendarShipmentDate_sk_fk = cal_s.idCalendar_sk
		left join	
			Warehouse.gen.dim_time tim_s on oh.idTimeShipmentDate_sk_fk = tim_s.idTime_sk
		left join
			Warehouse.gen.dim_calendar cal_re on oh.idCalendarRefundDate_sk_fk = cal_re.idCalendar_sk
		inner join
			Warehouse.gen.dim_store_v s on oh.idStore_sk_fk = s.idStore_sk
		inner join
			Warehouse.gen.dim_market m on oh.idMarket_sk_fk = m.idMarket_sk
		inner join
			-- Warehouse.gen.dim_customer_scd_v c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
			Warehouse.act.dim_customer_signature_v c on oh.idCustomer_sk_fk = c.idCustomer_sk_fk
		inner join
			Warehouse.gen.dim_country co_ship on oh.idCountryShipping_sk_fk = co_ship.idCountry_sk
		inner join
			Warehouse.gen.dim_country co_bill on oh.idCountryBilling_sk_fk = co_bill.idCountry_sk
		inner join
			Warehouse.gen.dim_customer_unsubscribe c_uns on oh.idCustomerUnsubscribe_sk_fk = c_uns.idCustomerUnsubscribe_sk
		inner join
			Warehouse.sales.dim_order_stage o_stag on oh.idOrderStage_sk_fk = o_stag.idOrderStage_sk
		inner join
			Warehouse.sales.dim_order_status o_stat on oh.idOrderStatus_sk_fk = o_stat.idOrderStatus_sk
		inner join
			Warehouse.sales.dim_order_type o_type on oh.idOrderType_sk_fk = o_type.idOrderType_sk
		inner join
			Warehouse.sales.dim_order_status_magento o_statm on oh.idOrderStatusMagento_sk_fk = o_statm.idOrderStatusMagento_sk
		inner join
			Warehouse.sales.dim_payment_method pm on oh.idPaymentMethod_sk_fk = pm.idPaymentMethod_sk
		left join
			Warehouse.sales.dim_cc_type cc on oh.idCCType_sk_fk = cc.idCCType_sk
		inner join
			Warehouse.sales.dim_shipping_method_v shm on oh.idShippingMethod_sk_fk = shm.idShippingMethod_sk
		left join
			Warehouse.sales.dim_telesale te on oh.idTelesale_sk_fk = te.idTelesale_sk
		inner join
			Warehouse.sales.dim_price_type prt on oh.idPriceType_sk_fk = prt.idPriceType_sk
		left join
			Warehouse.sales.dim_prescription_method prm on oh.idPrescriptionMethod_sk_fk = prm.idPrescriptionMethod_sk
		inner join
			Warehouse.sales.dim_reminder_type rt on oh.idReminderType_sk_fk = rt.idReminderType_sk
		inner join
			Warehouse.sales.dim_reminder_period rp on oh.idReminderPeriod_sk_fk = rp.idReminderPeriod_sk
		inner join
			Warehouse.sales.dim_channel ch on oh.idChannel_sk_fk = ch.idChannel_sk
		inner join
			Warehouse.sales.dim_marketing_channel mch on oh.idMarketingChannel_sk_fk = mch.idMarketingChannel_sk
		left join
			Warehouse.sales.dim_coupon_code_v cco on oh.idCouponCode_sk_fk = cco.idCouponCode_sk
		left join
			Warehouse.gen.dim_customer_status cs on oh.idCustomerStatusLifecycle_sk_fk = cs.idCustomerStatus_sk
		left join
			Warehouse.prod.dim_product_type_oh pt on oh.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn on oh.customer_order_seq_no = rcosn.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosnw on oh.customer_order_seq_no_web = rcosnw.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosng on oh.customer_order_seq_no_gen = rcosng.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_qty_time rqt on oh.order_qty_time = rqt.idQty_time_sk
		left join
			Warehouse.sales.dim_rank_shipping_days rsd on datediff(dd, oh.order_date, oh.shipment_date) = rsd.idShipping_Days_sk
go 




drop view sales.fact_order_line_v
go 

create view sales.fact_order_line_v as
	select oh.idOrderHeader_sk, ol.idOrderLine_sk, 
		ol.order_line_id_bk, 
		oh.order_id_bk, ol.invoice_id, ol.shipment_id, isnull(ol.creditmemo_id, oh.creditmemo_id) creditmemo_id,
		oh.order_no, ol.invoice_no, ol.shipment_no, isnull(ol.creditmemo_no, oh.creditmemo_no) creditmemo_no,
 		oh.order_date, oh.order_date_c, oh.order_week_day, oh.order_time_name, oh.order_day_part,
		ol.invoice_date, cal_i.calendar_date invoice_date_c, cal_i.calendar_date_week_name invoice_week_day, 
		-- ol.shipment_date, case when (o_stat.order_status_name in ('REFUND', 'PARTIAL REFUND') and ol.local_total_aft_refund_inc_vat <> 0) then ol.refund_date else ol.shipment_date end shipment_date_r,
		ol.shipment_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) 
			else ol.shipment_date end shipment_date_r,
		cal_s.calendar_date shipment_date_c, cal_s.calendar_date_week_name shipment_week_day, tim_s.time_name shipment_time_name, tim_s.day_part shipment_day_part,
		isnull(ol.refund_date, oh.refund_date) refund_date, cal_re.calendar_date refund_date_c, cal_re.calendar_date_week_name refund_week_day, 
		
		rsd.rank_shipping_days, 
		datediff(dd, oh.order_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end) shipping_days,

		oh.acquired, oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.company_name_create, oh.website_group_create, oh.website_create,
		oh.customer_origin_name,
		oh.market_name,
		oh.customer_id, oh.customer_email, oh.customer_name,
		oh.country_code_ship, oh.country_name_ship, oh.postcode_shipping, 
		oh.country_code_bill, oh.country_name_bill, oh.postcode_billing, 
		oh.customer_unsubscribe_name,
		oh.order_stage_name, oh.order_status_name, o_stat.order_status_name line_status_name, oh.adjustment_order, oh.order_type_name, oh.order_status_magento_name, 
		oh.payment_method_name, oh.cc_type_name, oh.shipping_carrier_name, oh.shipping_method_name, oh.telesales_username, oh.prescription_method_name,
		oh.reminder_type_name, oh.reminder_period_name, oh.reminder_date, oh.reorder_f, oh.reorder_date, 
		oh.channel_name, oh.marketing_channel_name, oh.group_coupon_code_name, oh.coupon_code,
		oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, oh.rank_seq_no_gen, oh.customer_order_seq_no_gen,
		oh.order_source, oh.proforma,
		oh.product_type_oh_name, oh.order_qty_time, oh.num_diff_product_type_oh,

		pf.manufacturer_name, 
		pf.product_type_name, pf.category_name, pf.product_family_group_name, pf.cl_type_name, pf.cl_feature_name, 
		pf.product_id_magento, pf.magento_sku product_family_code, pf.product_family_name, 

		ol.idBC_sk_fk base_curve, ol.idDI_sk_fk diameter, ol.idPO_sk_fk power, ol.idCY_sk_fk cylinder, ol.idAX_sk_fk axis, ol.idAD_sk_fk addition, ol.idDO_sk_fk dominance, ol.idCOL_sk_fk colour, 
		ol.eye,
		gvt.glass_vision_type_name, gpt.glass_package_type_name, 
		ol.sku_magento, ol.sku_erp, 
		ol.aura_product_f,

		ol.countries_registered_code, ol.product_type_vat,

		ol.qty_unit, ol.qty_unit_refunded, ol.qty_pack, rqt.rank_qty_time, ol.qty_time, -- oh.weight, 

		prt.price_type_name, oh.discount_f,
		ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
		convert(decimal(12, 4), ol.local_price_unit * ol.local_to_global_rate) global_price_unit, convert(decimal(12, 4), ol.local_price_pack * ol.local_to_global_rate) global_price_pack, 
		convert(decimal(12, 4), ol.local_price_pack_discount * ol.local_to_global_rate) global_price_pack_discount, 

		ol.local_subtotal, ol.local_shipping, ol.local_discount, ol.local_store_credit_used, 
		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.local_store_credit_given, ol.local_bank_online_given, 
		ol.local_subtotal_refund, ol.local_shipping_refund, ol.local_discount_refund, ol.local_store_credit_used_refund, ol.local_adjustment_refund, 
		ol.local_total_aft_refund_inc_vat, ol.local_total_aft_refund_exc_vat, ol.local_total_aft_refund_vat, ol.local_total_aft_refund_prof_fee, 
		-- ol.local_product_cost, oh.local_shipping_cost, ol.local_total_cost, ol.local_margin, 

		convert(decimal(12, 4), ol.local_subtotal * ol.local_to_global_rate) global_subtotal, convert(decimal(12, 4), ol.local_shipping * ol.local_to_global_rate) global_shipping, 
		convert(decimal(12, 4), ol.local_discount * ol.local_to_global_rate) global_discount, convert(decimal(12, 4), ol.local_store_credit_used * ol.local_to_global_rate) global_store_credit_used, 
		convert(decimal(12, 4), ol.local_total_inc_vat * ol.local_to_global_rate) global_total_inc_vat, convert(decimal(12, 4), ol.local_total_exc_vat * ol.local_to_global_rate) global_total_exc_vat, 
		convert(decimal(12, 4), ol.local_total_vat * ol.local_to_global_rate) global_total_vat, convert(decimal(12, 4), ol.local_total_prof_fee * ol.local_to_global_rate) global_total_prof_fee, 
		convert(decimal(12, 4), ol.local_store_credit_given * ol.local_to_global_rate) global_store_credit_given, convert(decimal(12, 4), ol.local_bank_online_given * ol.local_to_global_rate) global_bank_online_given, 
		convert(decimal(12, 4), ol.local_subtotal_refund * ol.local_to_global_rate) global_subtotal_refund, convert(decimal(12, 4), ol.local_shipping_refund * ol.local_to_global_rate) global_shipping_refund, 
		convert(decimal(12, 4), ol.local_discount_refund * ol.local_to_global_rate) global_discount_refund, convert(decimal(12, 4), ol.local_store_credit_used_refund * ol.local_to_global_rate) global_store_credit_used_refund, 
		convert(decimal(12, 4), ol.local_adjustment_refund * ol.local_to_global_rate) global_adjustment_refund, 
		convert(decimal(12, 4), ol.local_total_aft_refund_inc_vat * ol.local_to_global_rate) global_total_aft_refund_inc_vat, convert(decimal(12, 4), ol.local_total_aft_refund_exc_vat * ol.local_to_global_rate) global_total_aft_refund_exc_vat, 
		convert(decimal(12, 4), ol.local_total_aft_refund_vat * ol.local_to_global_rate) global_total_aft_refund_vat, convert(decimal(12, 4), ol.local_total_aft_refund_prof_fee * ol.local_to_global_rate) global_total_aft_refund_prof_fee, 
		-- convert(decimal(12, 4), ol.local_product_cost * ol.local_to_global_rate) global_product_cost, convert(decimal(12, 4), ol.local_shipping_cost * ol.local_to_global_rate) global_shipping_cost, 
		-- convert(decimal(12, 4), ol.local_total_cost * ol.local_to_global_rate) global_total_cost, convert(decimal(12, 4), ol.local_margin * ol.local_to_global_rate) global_margin, 

		isnull(olvf.local_total_inc_vat_vf, ol.local_total_inc_vat) local_total_inc_vat_vf, isnull(olvf.local_total_exc_vat_vf, ol.local_total_exc_vat) local_total_exc_vat_vf,
		isnull(olvf.local_total_vat_vf, ol.local_total_vat) local_total_vat_vf, isnull(olvf.local_total_prof_fee_vf, ol.local_total_prof_fee) local_total_prof_fee_vf,
		isnull(olvf.local_store_credit_given_vf, ol.local_store_credit_given) local_store_credit_given_vf, isnull(olvf.local_bank_online_given_vf, ol.local_bank_online_given) local_bank_online_given_vf,
		isnull(olvf.local_subtotal_refund_vf, ol.local_subtotal_refund) local_subtotal_refund_vf, isnull(olvf.local_shipping_refund_vf, ol.local_shipping_refund) local_shipping_refund_vf, 
		isnull(olvf.local_discount_refund_vf, ol.local_discount_refund) local_discount_refund_vf, isnull(olvf.local_store_credit_used_refund_vf, ol.local_store_credit_used_refund) local_store_credit_used_refund_vf, 
		isnull(olvf.local_adjustment_refund_vf, ol.local_adjustment_refund) local_adjustment_refund_vf,

		ol.local_to_global_rate, ol.order_currency_code, 
		ol.discount_percent, ol.vat_percent, ol.prof_fee_percent, ol.vat_rate, ol.prof_fee_rate,
		ol.num_erp_allocation_lines, 
		ol.idETLBatchRun_ins, ol.idETLBatchRun_upd
	from 
			Warehouse.sales.dim_order_header_v oh 
		inner join
			Warehouse.sales.fact_order_line ol on oh.order_id_bk = ol.order_id -- oh.idOrderHeader_sk = ol.idOrderHeader_sk_fk  
		left join
			Warehouse.sales.fact_order_line_vat_fix olvf on ol.idOrderLine_sk = olvf.idOrderLine_sk_fk
		left join
			Warehouse.gen.dim_calendar cal_i on ol.idCalendarInvoiceDate_sk_fk = cal_i.idCalendar_sk
		left join
			Warehouse.gen.dim_calendar cal_s -- on ol.idCalendarShipmentDate_sk_fk = cal_s.idCalendar_sk
				on case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.idCalendarShipmentDate_sk_fk, oh.idCalendarRefundDate_sk_fk) else ol.idCalendarShipmentDate_sk_fk end = cal_s.idCalendar_sk
		left join	
			Warehouse.gen.dim_time tim_s on ol.idTimeShipmentDate_sk_fk = tim_s.idTime_sk
		left join
			Warehouse.gen.dim_calendar cal_re on isnull(ol.idCalendarRefundDate_sk_fk, oh.idCalendarRefundDate_sk_fk) = cal_re.idCalendar_sk
		inner join
			Warehouse.sales.dim_order_status o_stat on ol.idOrderStatus_line_sk_fk = o_stat.idOrderStatus_sk
		inner join
			Warehouse.sales.dim_price_type prt on ol.idPriceType_sk_fk = prt.idPriceType_sk	
		inner join
			Warehouse.prod.dim_product_family_v pf on ol.idProductFamily_sk_fk = pf.idProductFamily_sk
		left join
			Warehouse.prod.dim_glass_vision_type gvt on ol.idGlassVisionType_sk_fk = gvt.idGlassVisionType_sk
		left join
			Warehouse.prod.dim_glass_package_type gpt on ol.idGlassPackageType_sk_fk = gpt.idGlassPackageType_sk
		left join
			Warehouse.sales.dim_rank_qty_time rqt on ol.qty_time = rqt.idQty_time_sk
		left join
			Warehouse.sales.dim_rank_shipping_days rsd on 
				datediff(dd, oh.order_date, case when (ol.qty_unit_refunded <> 0 and ol.local_store_credit_given <> 0) then isnull(ol.shipment_date, ol.refund_date) else ol.shipment_date end) = rsd.idShipping_Days_sk
go


drop view sales.dim_order_header_product_type_v
go 

create view sales.dim_order_header_product_type_v as
	select idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name, 
		order_stage_name, order_status_name,
		customer_id, customer_status_name, rcosn.rank_seq_no, customer_order_seq_no, rcosnw.rank_seq_no rank_seq_no_web, customer_order_seq_no_web,
		min(daily_perc) daily_perc, min(daily_qty_time) daily_qty_time, min(two_weeklies_perc) two_weeklies_perc, min(two_weeklies_qty_time) two_weeklies_qty_time, 
		min(monthlies_perc) monthlies_perc, min(monthlies_qty_time) monthlies_qty_time, min(yearly_perc) yearly_perc, min(yearly_qty_time) yearly_qty_time, 
		min(colour_perc) colour_perc, min(colour_qty_time) colour_qty_time, 
		min(sunglasses_perc) sunglasses_perc, min(sunglasses_qty_time) sunglasses_qty_time, min(glasses_perc) glasses_perc, min(glasses_qty_time) glasses_qty_time, min(other_perc) other_perc, min(other_qty_time) other_qty_time
	from
		(select 
			idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name,
			order_stage_name, order_status_name,
			customer_id, customer_status_name, customer_order_seq_no, customer_order_seq_no_web,
			[CL - Daily] daily_perc, [CL - Two Weeklies] two_weeklies_perc, [CL - Monthlies] monthlies_perc, [CL - Yearly] yearly_perc, Colour colour_perc,
			Sunglasses sunglasses_perc, Glasses glasses_perc, Other other_perc,
			[p_CL - Daily] daily_qty_time, [p_CL - Two Weeklies] two_weeklies_qty_time, [p_CL - Monthlies] monthlies_qty_time, [p_CL - Yearly] yearly_qty_time, p_Colour colour_qty_time,
			p_Sunglasses sunglasses_qty_time, p_Glasses glasses_qty_time, p_Other other_qty_time
		from
				(select 
					oh.idOrderHeader_sk, oh.order_id_bk, oh.order_no, oh.order_date_c, oh.store_name,
					oh.order_stage_name, oh.order_status_name,
					oh.customer_id, oh.customer_status_name, oh.customer_order_seq_no, oh.customer_order_seq_no_web, 
					--pt.idProductTypeOH_sk, ohpt.order_flag, ohpt.order_qty_time
					pt.product_type_oh_name, 'p_' + pt.product_type_oh_name product_type_oh_name_2, 
					ohpt.order_percentage, ohpt.order_qty_time			
				from 
						Warehouse.sales.dim_order_header_product_type ohpt
					inner join
						Warehouse.sales.dim_order_header_v oh on ohpt.idOrderHeader_sk_fk = oh.idOrderHeader_sk
					inner join
						Warehouse.prod.dim_product_type_oh pt on ohpt.idProductTypeOH_sk_fk = pt.idProductTypeOH_sk) t
			pivot
				(min(order_percentage) for
				product_type_oh_name in ([CL - Daily], [CL - Two Weeklies], [CL - Monthlies], [CL - Yearly], Colour, Sunglasses, Glasses, Other)) pvt
			pivot
				(min(order_qty_time) for
				product_type_oh_name_2 in ([p_CL - Daily], [p_CL - Two Weeklies], [p_CL - Monthlies], [p_CL - Yearly], p_Colour, p_Sunglasses, p_Glasses, p_Other)) pvt2) t
		inner join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosn on t.customer_order_seq_no = rcosn.idCustomer_order_seq_no_sk
		left join
			Warehouse.sales.dim_rank_customer_order_seq_no rcosnw on t.customer_order_seq_no_web = rcosnw.idCustomer_order_seq_no_sk
	group by idOrderHeader_sk, order_id_bk, order_no, order_date_c, store_name, 
		order_stage_name, order_status_name, customer_id, customer_status_name, rcosn.rank_seq_no, customer_order_seq_no, rcosnw.rank_seq_no, customer_order_seq_no_web 
go


drop view sales.fact_mutual_quotation_v 
go

create view sales.fact_mutual_quotation_v as
	select mq.idMutualQuotation_sk, mq.quotation_id_bk, oh.order_id_bk, 
		mq.quotation_date, 
		mqt.mutual_quotation_type_name, m.mutual_name, mq.error_id, mq.mutual_amount,
		oh.order_date, oh.invoice_date, oh.shipment_date, 
		oh.rank_shipping_days, oh.shipping_days, 
		oh.website,
		oh.customer_id, oh.customer_email, oh.country_code_ship, 
		oh.order_status_name, oh.payment_method_name, 
		oh.customer_status_name, oh.rank_seq_no_gen, oh.customer_order_seq_no_gen,
		oh.rank_order_qty_time, oh.order_qty_time,
		oh.local_total_inc_vat, oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, oh.local_total_refund, 
		oh.global_total_inc_vat, oh.global_total_exc_vat, oh.global_total_vat, oh.global_total_prof_fee, oh.global_total_refund		
	from 
			Warehouse.sales.fact_mutual_quotation mq
		inner join
			Warehouse.sales.dim_mutual m on mq.idMutual_sk_fk = m.idMutual_sk
		inner join
			Warehouse.sales.dim_mutual_quotation_type mqt on mq.idMutualQuotationType_sk_fk = mqt.idMutualQuotationType_sk
		left outer join
			Warehouse.sales.dim_order_header_v oh on mq.idOrderHeader_sk_fk = oh.idOrderHeader_sk
go
