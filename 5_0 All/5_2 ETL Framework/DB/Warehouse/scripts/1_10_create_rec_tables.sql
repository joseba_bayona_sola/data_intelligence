use Warehouse
go 

--------------------------------------------------------

create table Warehouse.rec.dim_wh_shipment_type(
	idWHShipmentType_sk				int NOT NULL IDENTITY(1, 1), 
	wh_shipment_type_bk				varchar(50) NOT NULL,
	wh_shipment_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_wh_shipment_type add constraint [PK_rec_dim_wh_shipment_type]
	primary key clustered (idWHShipmentType_sk);
go

alter table Warehouse.rec.dim_wh_shipment_type add constraint [DF_rec_dim_wh_shipment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_wh_shipment_type add constraint [UNIQ_rec_dim_wh_shipment_type_wh_shipment_type_bk]
	unique (wh_shipment_type_bk);
go



create table Warehouse.rec.dim_wh_shipment_type_wrk(
	wh_shipment_type_bk				varchar(50) NOT NULL,
	wh_shipment_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_wh_shipment_type_wrk add constraint [DF_rec_dim_wh_shipment_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.rec.dim_receipt_status(
	idReceiptStatus_sk				int NOT NULL IDENTITY(1, 1), 
	receipt_status_bk				varchar(50) NOT NULL,
	receipt_status_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_receipt_status add constraint [PK_rec_dim_receipt_status]
	primary key clustered (idReceiptStatus_sk);
go

alter table Warehouse.rec.dim_receipt_status add constraint [DF_rec_dim_receipt_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_receipt_status add constraint [UNIQ_rec_dim_receipt_status_receipt_status_bk]
	unique (receipt_status_bk);
go



create table Warehouse.rec.dim_receipt_status_wrk(
	receipt_status_bk				varchar(50) NOT NULL,
	receipt_status_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_receipt_status_wrk add constraint [DF_rec_dim_receipt_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.rec.dim_receipt_line_status(
	idReceiptLineStatus_sk			int NOT NULL IDENTITY(1, 1), 
	receipt_line_status_bk			varchar(50) NOT NULL,
	receipt_line_status_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_receipt_line_status add constraint [PK_rec_dim_receipt_line_status]
	primary key clustered (idReceiptLineStatus_sk);
go

alter table Warehouse.rec.dim_receipt_line_status add constraint [DF_rec_dim_receipt_line_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_receipt_line_status add constraint [UNIQ_rec_dim_receipt_line_status_receipt_line_status_bk]
	unique (receipt_line_status_bk);
go



create table Warehouse.rec.dim_receipt_line_status_wrk(
	receipt_line_status_bk			varchar(50) NOT NULL,
	receipt_line_status_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_receipt_line_status_wrk add constraint [DF_rec_dim_receipt_line_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.rec.dim_receipt_line_sync_status(
	idReceiptLineSyncStatus_sk		int NOT NULL IDENTITY(1, 1), 
	receipt_line_sync_status_bk		varchar(50) NOT NULL,
	receipt_line_sync_status_name	varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_receipt_line_sync_status add constraint [PK_rec_dim_receipt_line_sync_status]
	primary key clustered (idReceiptLineSyncStatus_sk);
go

alter table Warehouse.rec.dim_receipt_line_sync_status add constraint [DF_rec_dim_receipt_line_sync_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_receipt_line_sync_status add constraint [UNIQ_rec_dim_receipt_line_sync_status_receipt_line_sync_status_bk]
	unique (receipt_line_sync_status_bk);
go



create table Warehouse.rec.dim_receipt_line_sync_status_wrk(
	receipt_line_sync_status_bk		varchar(50) NOT NULL,
	receipt_line_sync_status_name	varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_receipt_line_sync_status_wrk add constraint [DF_rec_dim_receipt_line_sync_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




--------------------------------------------------------

create table Warehouse.rec.dim_wh_shipment(
	idWHShipment_sk					int NOT NULL IDENTITY(1, 1), 
	shipment_id_bk					bigint NOT Null, 

	idWarehouse_sk_fk				int NOT NULL, 
	idSupplier_sk_fk				int NOT NULL, 
	idWHShipmentType_sk_fk			int, 

	shipment_number					varchar(20), 

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_wh_shipment add constraint [PK_rec_dim_wh_shipment]
	primary key clustered (idWHShipment_sk);
go

alter table Warehouse.rec.dim_wh_shipment add constraint [DF_rec_dim_wh_shipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_wh_shipment add constraint [UNIQ_rec_dim_wh_shipment_shipment_id_bk]
	unique (shipment_id_bk);
go


alter table Warehouse.rec.dim_wh_shipment add constraint [FK_rec_dim_wh_shipment_dim_warehouse]
	foreign key (idWarehouse_sk_fk) references Warehouse.gen.dim_warehouse (idWarehouse_sk);
go
alter table Warehouse.rec.dim_wh_shipment add constraint [FK_rec_dim_wh_shipment_dim_supplier]
	foreign key (idSupplier_sk_fk) references Warehouse.gen.dim_supplier (idSupplier_sk);
go

alter table Warehouse.rec.dim_wh_shipment add constraint [FK_rec_dim_wh_shipment_dim_wh_shipment_type]
	foreign key (idWHShipmentType_sk_fk) references Warehouse.rec.dim_wh_shipment_type (idWHShipmentType_sk);
go

create table Warehouse.rec.dim_wh_shipment_wrk(
	shipment_id_bk					bigint NOT Null, 

	idWarehouse_sk_fk				int NOT NULL, 
	idSupplier_sk_fk				int NOT NULL, 
	idWHShipmentType_sk_fk			int, 

	shipment_number					varchar(20), 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_wh_shipment_wrk add constraint [DF_rec_dim_wh_shipment_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.rec.dim_receipt(
	idReceipt_sk					int NOT NULL IDENTITY(1, 1), 
	receiptid_bk					bigint NOT NULL, 

	idWHShipment_sk_fk				int NOT NULL, 
	idReceiptStatus_sk_fk			int, 

	receipt_number					varchar(20), 
	receipt_no						int, 

	created_date					datetime, 
	arrived_date					datetime,
	confirmed_date					datetime, 
	stock_registered_date			datetime, 

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.dim_receipt add constraint [PK_rec_dim_receipt]
	primary key clustered (idReceipt_sk);
go

alter table Warehouse.rec.dim_receipt add constraint [DF_rec_dim_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.dim_receipt add constraint [UNIQ_rec_dim_receipt_receiptid_bk]
	unique (receiptid_bk);
go


alter table Warehouse.rec.dim_receipt add constraint [FK_rec_dim_receipt_dim_wh_shipment]
	foreign key (idWHShipment_sk_fk) references Warehouse.rec.dim_wh_shipment (idWHShipment_sk);
go

alter table Warehouse.rec.dim_receipt add constraint [FK_rec_dim_receipt_dim_receipt_status]
	foreign key (idReceiptStatus_sk_fk) references Warehouse.rec.dim_receipt_status (idReceiptStatus_sk);
go


create table Warehouse.rec.dim_receipt_wrk(
	receiptid_bk					bigint NOT NULL, 

	idWHShipment_sk_fk				int NOT NULL, 
	idReceiptStatus_sk_fk			int, 

	receipt_number					varchar(20), 
	receipt_no						int, 

	created_date					datetime, 
	arrived_date					datetime,
	confirmed_date					datetime, 
	stock_registered_date			datetime, 

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.dim_receipt_wrk add constraint [DF_rec_dim_receipt_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.rec.fact_receipt_line(
	idReceiptLine_sk				int NOT NULL IDENTITY(1, 1), 
	receiptlineid_bk				bigint NOT NULL, 

	idReceipt_sk_fk					int NOT NULL, 
	idReceiptLineStatus_sk_fk		int, 
	idReceiptLineSyncStatus_sk_fk	int, 
	idPurchaseOrderLine_sk_fk		int, 
	idStockItem_sk_fk				int, 

	created_date					datetime, 

	qty_ordered						decimal(28, 8), 
	qty_received					decimal(28, 8), 
	qty_accepted					decimal(28, 8), 
	qty_returned					decimal(28, 8), 
	qty_rejected					decimal(28, 8), 

	local_unit_cost					decimal(28, 8), 
	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	wh_stock_item_batch_f			char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.rec.fact_receipt_line add constraint [PK_rec_fact_receipt_line]
	primary key clustered (idReceiptLine_sk);
go

alter table Warehouse.rec.fact_receipt_line add constraint [DF_rec_fact_receipt_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.rec.fact_receipt_line add constraint [UNIQ_rec_fact_receipt_line_receiptlineid_bk]
	unique (receiptlineid_bk);
go

alter table Warehouse.rec.fact_receipt_line add constraint [FK_rec_fact_receipt_line_dim_receipt]
	foreign key (idReceipt_sk_fk) references Warehouse.rec.dim_receipt (idReceipt_sk);
go

alter table Warehouse.rec.fact_receipt_line add constraint [FK_rec_fact_receipt_line_dim_receipt_line_status]
	foreign key (idReceiptLineStatus_sk_fk) references Warehouse.rec.dim_receipt_line_status (idReceiptLineStatus_sk);
go
alter table Warehouse.rec.fact_receipt_line add constraint [FK_rec_fact_receipt_line_dim_receipt_line_sync_status]
	foreign key (idReceiptLineSyncStatus_sk_fk) references Warehouse.rec.dim_receipt_line_sync_status (idReceiptLineSyncStatus_sk);
go

alter table Warehouse.rec.fact_receipt_line add constraint [FK_rec_fact_receipt_line_dim_purchase_order_line]
	foreign key (idPurchaseOrderLine_sk_fk) references Warehouse.po.fact_purchase_order_line (idPurchaseOrderLine_sk);
go
alter table Warehouse.rec.fact_receipt_line add constraint [FK_rec_fact_receipt_line_dim_stock_item]
	foreign key (idStockItem_sk_fk) references Warehouse.prod.dim_stock_item (idStockItem_sk);
go


create table Warehouse.rec.fact_receipt_line_wrk(
	receiptlineid_bk				bigint NOT NULL, 

	idReceipt_sk_fk					int NOT NULL, 
	idReceiptLineStatus_sk_fk		int, 
	idReceiptLineSyncStatus_sk_fk	int, 
	idPurchaseOrderLine_sk_fk		int, 
	idStockItem_sk_fk				int, 

	created_date					datetime, 

	qty_ordered						decimal(28, 8), 
	qty_received					decimal(28, 8), 
	qty_accepted					decimal(28, 8), 
	qty_returned					decimal(28, 8), 
	qty_rejected					decimal(28, 8), 

	local_unit_cost					decimal(28, 8), 
	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	wh_stock_item_batch_f			char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.rec.fact_receipt_line_wrk add constraint [DF_rec_fact_receipt_line_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
