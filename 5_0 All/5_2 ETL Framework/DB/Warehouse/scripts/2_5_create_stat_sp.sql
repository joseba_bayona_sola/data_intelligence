
use Warehouse
go 


drop procedure stat.dwh_dwh_merge_stat_customer_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 05-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for Customer Status from Customer Signature table into Final DWH Fact Table
-- ==========================================================================================

create procedure stat.dwh_dwh_merge_stat_customer_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stat.fact_customer_status with (tablock) as trg
	using 
		(select cs.idCustomerStatus_sk idCustomerStatus_sk_fk, t.website_register, t.website_group_create, t.create_time_mm idCR_time_mm_sk_fk, t.stat_date, 
			t.num_customers
		from
				(select customer_status_name, website_register, website_group_create, create_time_mm, convert(date, getutcdate()) stat_date, count(*) num_customers
				from Warehouse.act.fact_customer_signature_v
				group by customer_status_name, website_register, website_group_create, create_time_mm) t
			inner join
				Warehouse.gen.dim_customer_status cs on t.customer_status_name = cs.customer_status_name_bk) src 
		on (trg.idCustomerStatus_sk_fk = src.idCustomerStatus_sk_fk and 
			trg.website_register = src.website_register and trg.website_group_create = src.website_group_create and 
			trg.idCR_time_mm_sk_fk = src.idCR_time_mm_sk_fk and trg.stat_date = src.stat_date) 
	when matched and not exists
		(select isnull(trg.num_customers, 0)
		intersect
		select isnull(src.num_customers, 0))
		then 
			update set
				trg.num_customers = src.num_customers, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (idCustomerStatus_sk_fk, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
				num_customers, idETLBatchRun_ins)
	
				values (src.idCustomerStatus_sk_fk, src.website_register, src.website_group_create, src.idCR_time_mm_sk_fk, src.stat_date, 
					src.num_customers, @idETLBatchRun)

		OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stat.dwh_dwh_merge_stat_magento_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for Customer Magento Unsubscribe from Customer SCD table into Final DWH Fact Table
-- ==========================================================================================

create procedure stat.dwh_dwh_merge_stat_magento_unsubscribe
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Warehouse.stat.fact_magento_unsubscribe with (tablock) as trg
	using 
		(select unsubscribe_mark_email_magento_f magento_unsubscribe_f, website_register, website_group_create, create_time_mm idCR_time_mm_sk_fk, convert(date, getutcdate()) stat_date, count(*) num_customers
		from 
				Warehouse.gen.dim_customer_scd_v c
			inner join
				Warehouse.act.fact_customer_signature_v cs on c.idCustomer_sk_fk = cs.idCustomer_sk_fk
		group by unsubscribe_mark_email_magento_f, website_register, website_group_create, create_time_mm) src 
		on (trg.magento_unsubscribe_f = src.magento_unsubscribe_f and 
			trg.website_register = src.website_register and trg.website_group_create = src.website_group_create and 
			trg.idCR_time_mm_sk_fk = src.idCR_time_mm_sk_fk and trg.stat_date = src.stat_date) 
	when matched and not exists
		(select isnull(trg.num_customers, 0)
		intersect
		select isnull(src.num_customers, 0))
		then 
			update set
				trg.num_customers = src.num_customers, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched
		then 
			insert (magento_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
				num_customers, idETLBatchRun_ins)
	
				values (src.magento_unsubscribe_f, src.website_register, src.website_group_create, src.idCR_time_mm_sk_fk, src.stat_date, 
					src.num_customers, @idETLBatchRun)

		OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stat.dwh_dwh_merge_stat_dotmailer_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for Customer Dotmailer Unsubscribe from Customer SCD table into Final DWH Fact Table
-- ==========================================================================================

create procedure stat.dwh_dwh_merge_stat_dotmailer_unsubscribe
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	--merge into Warehouse.stat.fact_dotmailer_unsubscribe with (tablock) as trg
	--using 
	--	() src 
	--	on (trg.dotmailer_unsubscribe_f = src.dotmailer_unsubscribe_f and 
	--		trg.website_register = src.website_register and trg.website_group_create = src.website_group_create and 
	--		trg.idCR_time_mm_sk_fk = src.idCR_time_mm_sk_fk and trg.stat_date = src.stat_date) 
	--when matched and not exists
	--	(select isnull(trg.num_customers, 0)
	--	intersect
	--	select isnull(src.num_customers, 0))
	--	then 
	--		update set
	--			trg.num_customers = src.num_customers, 
	--			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	--when not matched
	--	then 
	--		insert (dotmailer_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	--			num_customers, idETLBatchRun_ins)
	
	--			values (src.dotmailer_unsubscribe_f, src.website_register, src.website_group_create, src.idCR_time_mm_sk_fk, src.stat_date, 
	--				src.num_customers, @idETLBatchRun)

	--	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure stat.dwh_dwh_merge_stat_sms_unsubscribe
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for Customer SMS Unsubscribe from Customer SCD table into Final DWH Fact Table
-- ==========================================================================================

create procedure stat.dwh_dwh_merge_stat_sms_unsubscribe
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	--merge into Warehouse.stat.fact_sms_unsubscribe with (tablock) as trg
	--using 
	--	() src 
	--	on (trg.sms_unsubscribe_f = src.sms_unsubscribe_f and 
	--		trg.website_register = src.website_register and trg.website_group_create = src.website_group_create and 
	--		trg.idCR_time_mm_sk_fk = src.idCR_time_mm_sk_fk and trg.stat_date = src.stat_date) 
	--when matched and not exists
	--	(select isnull(trg.num_customers, 0)
	--	intersect
	--	select isnull(src.num_customers, 0))
	--	then 
	--		update set
	--			trg.num_customers = src.num_customers, 
	--			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	--when not matched
	--	then 
	--		insert (sms_unsubscribe_f, website_register, website_group_create, idCR_time_mm_sk_fk, stat_date, 
	--			num_customers, idETLBatchRun_ins)
	
	--			values (src.sms_unsubscribe_f, src.website_register, src.website_group_create, src.idCR_time_mm_sk_fk, src.stat_date, 
	--				src.num_customers, @idETLBatchRun)

	--	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 