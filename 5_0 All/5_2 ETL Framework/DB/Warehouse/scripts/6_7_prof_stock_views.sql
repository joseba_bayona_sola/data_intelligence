
-- dim_wh_stock_item_v
select top 1000 *
from Warehouse.stock.dim_wh_stock_item_v
-- where qty_received = 0
where wh_stock_item_description is null
order by product_id_magento, stock_item_description, warehouse_name

	select count(*)
	from Warehouse.stock.dim_wh_stock_item_v

	select warehouse_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by warehouse_name
	order by warehouse_name

	select product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by product_id_magento, product_family_name, packsize
	order by product_id_magento, product_family_name, packsize

	select warehouse_name, product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by warehouse_name, product_id_magento, product_family_name, packsize
	order by warehouse_name, product_id_magento, product_family_name, packsize

	select product_id_magento, product_family_name, stock_method_type_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_v
	group by product_id_magento, product_family_name, stock_method_type_name
	order by product_id_magento, product_family_name, stock_method_type_name

	-- QTY Check

	-- RUN
	select idWHStockItem_sk, warehousestockitemid_bk, warehouse_name, product_id_magento, stock_item_description, 
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_registered, qty_disposed, qty_on_hold,
		qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold)
	from Warehouse.stock.dim_wh_stock_item_v
	where product_id_magento = product_id_magento
		-- and qty_available = 0 and qty_allocated_stock = 0 and qty_received = 0 -- 178
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 709 (610)
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold and qty_registered = 0 and qty_disposed = 0 -- 123
		and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock -- 0
	order by product_id_magento, stock_item_description, warehouse_name

	select sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold)))
	from Warehouse.stock.dim_wh_stock_item_v
	where qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold

	-- RUN
	select product_id_magento, product_family_name, sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold))) num_rec
	from Warehouse.stock.dim_wh_stock_item_v
	where qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold
	group by product_id_magento, product_family_name
	order by num_rec desc, product_id_magento, product_family_name

-- dim_wh_stock_item_batch_v
select top 1000 *
from Warehouse.stock.dim_wh_stock_item_batch_v

	select count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_v

	select warehouse_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	group by warehouse_name
	order by warehouse_name

	select product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	group by product_id_magento, product_family_name, packsize
	order by product_id_magento, product_family_name, packsize

	select warehouse_name, product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	group by warehouse_name, product_id_magento, product_family_name, packsize
	order by warehouse_name, product_id_magento, product_family_name, packsize

	select product_id_magento, product_family_name, stock_method_type_name, count(*)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	group by product_id_magento, product_family_name, stock_method_type_name
	order by product_id_magento, product_family_name, stock_method_type_name

	select product_id_magento, product_family_name, stock_batch_type_name, count(*), min(batch_stock_register_date), max(batch_stock_register_date)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	group by product_id_magento, product_family_name, stock_batch_type_name
	order by product_id_magento, product_family_name, stock_batch_type_name

		select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, 
			qty_received, batch_stock_register_date
		from Warehouse.stock.dim_wh_stock_item_batch_v
		where product_id_magento = 1083 and stock_batch_type_name = 'RECEIPT_NO_MOV'
		order by batch_stock_register_date

	-- QTY Check

	-- RUN
	select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, batch_stock_register_date,
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, 
		qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, 
		qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold, 
		qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold)
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where product_id_magento = product_id_magento
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold
		-- and qty_outstanding_allocation <> qty_allocated_stock - qty_issued_stock
		-- and qty_registered <> qty_registered_sm
		-- and qty_disposed <> qty_disposed_sm

		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold -- 0
		-- and qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold -- 1104 (1007)
		and qty_registered - qty_disposed <> qty_registered_sm - qty_disposed_sm -- 1104 (1007)
	order by product_id_magento, stock_item_description, warehouse_name, batch_stock_register_date

	select sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold)))
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold

	-- RUN
	select product_id_magento, product_family_name, sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold))) num_rec
	from Warehouse.stock.dim_wh_stock_item_batch_v
	where qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold
	group by product_id_magento, product_family_name
	order by num_rec desc, product_id_magento, product_family_name

		-- RUN
		select isnull(t1.product_id_magento, t2.product_id_magento) product_id_magento, isnull(t1.product_family_name, t2.product_family_name) product_family_name, 
			t1.num_rec, t2.num_rec
		from
				(select product_id_magento, product_family_name, sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold))) num_rec
				from Warehouse.stock.dim_wh_stock_item_v
				where qty_received <> qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold
				group by product_id_magento, product_family_name) t1
			full join
				(select product_id_magento, product_family_name, sum(abs(qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold))) num_rec
				from Warehouse.stock.dim_wh_stock_item_batch_v
				where qty_received <> qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold
				group by product_id_magento, product_family_name) t2 on t1.product_id_magento = t2.product_id_magento
		order by product_id_magento, product_family_name

					-- WH_STOCK_ITEM vs WH_STOCK_ITEM_BATCH	

					select top 1000 idWHStockItem_sk, count(*) num_batches, 
						sum(qty_received) qty_received_b, sum(qty_available) qty_available_b, sum(qty_outstanding_allocation) qty_outstanding_allocation_b, 
						sum(qty_allocated_stock) qty_allocated_stock_b, sum(qty_issued_stock) qty_issued_stock_b, 
						sum(qty_registered) qty_registered_b, sum(qty_disposed) qty_disposed_b, sum(qty_on_hold) qty_on_hold_b
					from Warehouse.stock.dim_wh_stock_item_batch_v
					group by idWHStockItem_sk

					select wsi.idWHStockItem_sk, wsi.warehousestockitemid_bk, wsi.warehouse_name, wsi.product_id_magento, wsi.stock_item_description, 
						wsi.qty_received, wsib.qty_received_b, wsi.qty_available, wsib.qty_available_b, wsi.qty_outstanding_allocation, wsib.qty_outstanding_allocation_b, 
						wsi.qty_allocated_stock, wsib.qty_allocated_stock_b, wsi.qty_issued_stock, wsib.qty_issued_stock_b, 
						wsi.qty_registered, wsib.qty_registered_b, wsi.qty_disposed, wsib.qty_disposed_b, wsi.qty_on_hold, wsib.qty_on_hold_b
					from
							(select idWHStockItem_sk, warehousestockitemid_bk, warehouse_name, product_id_magento, stock_item_description, 
								qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_registered, qty_disposed, qty_on_hold
							from Warehouse.stock.dim_wh_stock_item_v
							where product_id_magento = 1083) wsi
						inner join
							(select idWHStockItem_sk, count(*) num_batches, 
								sum(qty_received) qty_received_b, sum(qty_available) qty_available_b, sum(qty_outstanding_allocation) qty_outstanding_allocation_b, 
								sum(qty_allocated_stock) qty_allocated_stock_b, sum(qty_issued_stock) qty_issued_stock_b, 
								sum(qty_registered) qty_registered_b, sum(qty_disposed) qty_disposed_b, sum(qty_on_hold) qty_on_hold_b
								-- sum(qty_registered_sm) qty_registered_b, sum(qty_disposed_sm) qty_disposed_b, sum(qty_on_hold) qty_on_hold_b
							from Warehouse.stock.dim_wh_stock_item_batch_v
							group by idWHStockItem_sk) wsib on wsi.idWHStockItem_sk = wsib.idWHStockItem_sk
					-- where wsi.qty_received <> wsib.qty_received_b
					-- where wsi.qty_available <> wsib.qty_available_b
					where wsi.qty_allocated_stock <> wsib.qty_allocated_stock_b
					-- where wsi.qty_registered <> wsib.qty_registered_b
					-- where wsi.qty_disposed <> wsib.qty_disposed_b


-- dim_wh_stock_item_batch_v
select top 1000 *
from Warehouse.stock.fact_wh_stock_item_batch_movement_v
order by batch_movement_date desc

	select warehouse_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by warehouse_name
	order by warehouse_name

	select product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, packsize
	order by product_id_magento, product_family_name, packsize

	select warehouse_name, product_id_magento, product_family_name, packsize, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by warehouse_name, product_id_magento, product_family_name, packsize
	order by warehouse_name, product_id_magento, product_family_name, packsize

	select product_id_magento, product_family_name, stock_method_type_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, stock_method_type_name
	order by product_id_magento, product_family_name, stock_method_type_name

	select product_id_magento, product_family_name, stock_batch_type_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, stock_batch_type_name
	order by product_id_magento, product_family_name, stock_batch_type_name

	select product_id_magento, product_family_name, stock_adjustment_type_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, stock_adjustment_type_name
	order by product_id_magento, product_family_name, stock_adjustment_type_name

	select product_id_magento, product_family_name, stock_movement_type_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, stock_movement_type_name
	order by product_id_magento, product_family_name, stock_movement_type_name

	select product_id_magento, product_family_name, stock_movement_period_name, count(*)
	from Warehouse.stock.fact_wh_stock_item_batch_movement_v
	group by product_id_magento, product_family_name, stock_movement_period_name
	order by product_id_magento, product_family_name, stock_movement_period_name

					-- WH_STOCK_ITEM_BATCH vs WH_STOCK_ITEM_BATCH_MOVEMENTS	 
					select idWHStockItemBatchMovement_sk, idWHStockItemBatch_sk, batch_id, 
						batch_stock_move_id, move_id, move_line_id, 
						stock_adjustment_type_name, stock_movement_type_name, stock_movement_period_name, 
						qty_movement, 
						sum(qty_registered) over (partition by idWHStockItemBatch_sk) qty_registered_b, 
						sum(qty_disposed) over (partition by idWHStockItemBatch_sk) qty_disposed_b, 
						batch_movement_date
					from Warehouse.stock.fact_wh_stock_item_batch_movement_v

					select wsib.idWHStockItemBatch_sk, wsib.warehousestockitembatchid_bk, wsib.warehouse_name, wsib.stock_batch_type_name, wsib.product_id_magento, wsib.batch_id, wsib.batch_stock_register_date,
						wsibm.batch_stock_move_id, wsibm.stock_adjustment_type_name, wsibm.stock_movement_type_name, wsibm.stock_movement_period_name, 
						wsib.qty_received, wsib.qty_registered, wsibm.qty_registered_b, wsib.qty_disposed, wsibm.qty_disposed_b, 
						wsibm.qty_movement
					from
							(select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, 
								qty_received, qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, batch_stock_register_date
							from Warehouse.stock.dim_wh_stock_item_batch_v
							where product_id_magento = 1083) wsib -- (qty_registered <> 0 or qty_disposed <> 0)
						left join
							(select idWHStockItemBatchMovement_sk, idWHStockItemBatch_sk, batch_id, 
								batch_stock_move_id, move_id, move_line_id, 
								stock_adjustment_type_name, stock_movement_type_name, stock_movement_period_name, 
								qty_movement, 
								sum(qty_registered) over (partition by idWHStockItemBatch_sk) qty_registered_b, 
								sum(qty_disposed) over (partition by idWHStockItemBatch_sk) qty_disposed_b, 
								batch_movement_date
							from Warehouse.stock.fact_wh_stock_item_batch_movement_v) wsibm on wsib.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk
					-- where (qty_registered <> 0 or qty_disposed <> 0) and wsibm.idWHStockItemBatch_sk is null
					-- where wsibm.idWHStockItemBatch_sk is not null
					-- where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_disposed <> wsibm.qty_disposed_b
					where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_registered <> wsibm.qty_registered_b
					order by wsib.batch_id

					------------------------------

					select idWHStockItemBatch_sk, 
						sum(qty_registered) qty_registered_b, 
						sum(qty_disposed) qty_disposed_b
					from Warehouse.stock.fact_wh_stock_item_batch_movement_v
					group by idWHStockItemBatch_sk

					-- RUN
					select wsib.idWHStockItemBatch_sk, wsib.warehousestockitembatchid_bk, wsib.warehouse_name, wsib.stock_batch_type_name, wsib.product_id_magento, wsib.stock_item_description,
						wsib.batch_id, wsib.batch_stock_register_date,
						wsib.qty_received, 
						wsib.qty_registered, wsib.qty_registered_sm, wsibm.qty_registered_b, 
						wsib.qty_disposed, wsib.qty_disposed_sm, wsibm.qty_disposed_b
					from
							(select idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, stock_batch_type_name, product_id_magento, stock_item_description, batch_id, 
								qty_received, qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, batch_stock_register_date
							from Warehouse.stock.dim_wh_stock_item_batch_v
							where product_id_magento = product_id_magento) wsib -- (qty_registered <> 0 or qty_disposed <> 0)
						left join
							(select idWHStockItemBatch_sk, 
								sum(qty_registered) qty_registered_b, 
								sum(qty_disposed) qty_disposed_b, 
								sum(qty_movement) qty_movement_b
							from Warehouse.stock.fact_wh_stock_item_batch_movement_v
							group by idWHStockItemBatch_sk) wsibm on wsib.idWHStockItemBatch_sk = wsibm.idWHStockItemBatch_sk
					-- where wsibm.idWHStockItemBatch_sk is not null
					-- where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_disposed <> 0 and wsib.qty_disposed <> wsibm.qty_disposed_b 
					-- where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_registered <> wsibm.qty_registered_b

					-- where (qty_registered <> 0 or qty_disposed <> 0) and wsibm.idWHStockItemBatch_sk is null	-- 300 (203)
					-- where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_disposed <> 0 and wsib.qty_disposed <> wsibm.qty_disposed_b and wsib.qty_disposed <> wsibm.qty_disposed_b - wsibm.qty_registered_b -- 203
					-- where wsibm.idWHStockItemBatch_sk is not null and wsib.qty_registered <> 0 and wsib.qty_registered <> wsibm.qty_registered_b and wsib.qty_registered <> wsibm.qty_registered_b - wsibm.qty_disposed_b -- 25
					order by product_id_magento, stock_item_description, warehouse_name, batch_stock_register_date


-------------------------------------------------------------

select top 1000 idWHStockItem_sk, warehousestockitemid_bk, warehouse_name, product_id_magento, stock_item_description, 
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_registered, qty_disposed, qty_on_hold,
	qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold, 
	qty_received - (qty_allocated_stock + qty_available - qty_registered + qty_disposed + qty_on_hold)
from Warehouse.stock.dim_wh_stock_item_v
where qty_disposed > 20000
order by warehouse_name, product_id_magento, stock_item_description

select whsib.idWHStockItemBatch_sk, warehousestockitembatchid_bk, warehouse_name, product_id_magento, stock_item_description, batch_id, batch_stock_register_date,
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, qty_on_hold, 
	qty_registered, qty_registered_sm, qty_disposed, qty_disposed_sm, 
	qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold, 
	qty_received - (qty_allocated_stock + qty_available - qty_registered_sm + qty_disposed_sm + qty_on_hold)
from
		(select top 1000 idWHStockItem_sk
		from Warehouse.stock.dim_wh_stock_item_v
		where qty_disposed > 20000) whsi
	inner join
		Warehouse.stock.dim_wh_stock_item_batch_v whsib on whsi.idWHStockItem_sk = whsib.idWHStockItem_sk
where whsib.qty_disposed_sm > 20000
order by warehouse_name, product_id_magento, stock_item_description	

select whsibm.idWHStockItemBatchMovement_sk, batchstockmovementid_bk, warehouse_name, product_id_magento, stock_item_description, batch_id, 
	stock_adjustment_type_name, stock_movement_type_name, stock_movement_period_name, 
	qty_movement, qty_registered, qty_disposed, 
	batch_movement_date
from
		(select whsib.idWHStockItemBatch_sk
		from
				(select top 1000 idWHStockItem_sk
				from Warehouse.stock.dim_wh_stock_item_v
				where qty_disposed > 20000) whsi
			inner join
				Warehouse.stock.dim_wh_stock_item_batch_v whsib on whsi.idWHStockItem_sk = whsib.idWHStockItem_sk
		where whsib.qty_disposed_sm > 20000) whsib
	inner join
		Warehouse.stock.fact_wh_stock_item_batch_movement_v whsibm on whsib.idWHStockItemBatch_sk = whsibm.idWHStockItemBatch_sk
order by warehouse_name, product_id_magento, stock_item_description	