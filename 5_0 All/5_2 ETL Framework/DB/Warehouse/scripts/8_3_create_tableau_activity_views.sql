
use Warehouse
go

drop view tableau.fact_customer_signature_v
go

create view tableau.fact_customer_signature_v as
	select f.customer_id, f.customer_email, 
		f.acquired_create, f.website_group_create, f.website_create, f.store_create, f.create_date,
		rcrt.cr_time_name rank_create_time, f.create_time_mm, 
		f.website_register, f.store_register, f.register_date, 
		f.customer_origin_name,
		f.market_name, 
		d.country_code_ship,
		f.customer_status_name, f.status_update_date,
		d.customer_unsubscribe_name,
		f.first_order_date, f.last_order_date, 
		f.rank_num_tot_orders, f.num_tot_orders, f.subtotal_tot_orders, 
		f.rank_num_tot_cancel, f.num_tot_cancel, f.subtotal_tot_cancel, 
		f.rank_num_tot_refund, f.num_tot_refund, f.subtotal_tot_refund, 
		f.num_tot_lapsed, f.num_tot_reactivate,
		f.num_tot_discount_orders, f.discount_tot_value, f.num_tot_store_credit_orders, f.store_credit_tot_value, 
		f.main_type_oh_name, f.num_diff_product_type_oh, d.num_dist_products, 
		-- f.main_order_qty_time, f.avg_order_qty_time, f.stdev_order_qty_time, 
		-- f.avg_order_freq_time, f.stdev_order_freq_time, 
		f.rank_main_order_qty_time, f.main_order_qty_time, f.rank_avg_order_qty_time, f.avg_order_qty_time, f.rank_stdev_order_qty_time, f.stdev_order_qty_time, 
		f.rank_avg_order_freq_time, f.avg_order_freq_time, f.rank_stdev_order_freq_time, f.stdev_order_freq_time, 
		f.main_channel_name, f.main_marketing_channel_name, f.main_price_type_name,
		f.first_channel_name, f.first_marketing_channel_name, f.first_price_type_name, 
		case when (cs.customer_id is not null) then 'Y' else 'N' end next_order_date_proj
	from 
			Warehouse.act.fact_customer_signature_v f
		inner join
			Warehouse.act.dim_customer_signature_v d on f.idCustomer_sk_fk = d.idCustomer_sk_fk
		inner join
			Warehouse.act.dim_rank_cr_time_mm rcrt on d.create_time_mm = rcrt.idCR_Time_MM_sk
		left join
			(select distinct customer_id
			from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
			where selected = 1) cs on f.customer_id = cs.customer_id
go


drop view tableau.fact_activity_sales_v
go 

create view tableau.fact_activity_sales_v as
	select acs.activity_id_bk, acs.activity_date_c, acs.activity_type_name, 
		-- acs.acquired, 
		acs.tld, acs.website_group, acs.website, acs.store_name, 
		acs.market_name, 
		acs.customer_id, cs.website_group_create, cs.country_code_ship, 
		cs.rank_num_tot_orders, cs.num_tot_orders, cs.subtotal_tot_orders,
		acs.rank_order_qty_time, acs.order_qty_time, acs.rank_next_order_freq_time, acs.next_order_freq_time,
		-- cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.main_order_qty_time, cs.avg_order_freq_time
		cs.main_type_oh_name, cs.num_diff_product_type_oh, cs.rank_main_order_qty_time, cs.main_order_qty_time, cs.rank_avg_order_freq_time, cs.avg_order_freq_time
	from
			(select order_id_bk activity_id_bk, activity_date_c, customer_status_name activity_type_name, 
				acquired, tld, website_group, website, store_name, 
				market_name, 
				customer_id, 
				rank_order_qty_time, order_qty_time, rank_next_order_freq_time, next_order_freq_time
			from Warehouse.act.fact_activity_sales_v
			where activity_type_name = 'ORDER' and customer_status_name = 'NEW'
			union
			select activity_id_bk, activity_date_c, activity_type_name, 
				acquired, tld, website_group, website, store_name, 
				market_name, 
				customer_id, 
				null rank_order_qty_time, null order_qty_time, null rank_next_order_freq_time, null next_order_freq_time
			from Warehouse.act.fact_activity_other_v
			where activity_type_name in ('REACTIVATE', 'LAPSED')
			--union
			--select activity_id_bk, activity_date_c, activity_type_name, 
			--	acquired, tld, website_group, website, store_name, 
			--	market_name, 
			--	customer_id
			--from Warehouse.act.fact_activity_other_v
			--where activity_type_name in ('REGISTER')
			--	and customer_id not in 
			--		(select distinct customer_id 
			--		from Warehouse.act.fact_activity_sales_v)
			) acs
		inner join
			Warehouse.act.dim_customer_signature_v cs on acs.customer_id = cs.customer_id
go
	


drop view tableau.fact_customer_product_signature_v
go

create view tableau.fact_customer_product_signature_v as
	select cpsv.customer_id, cpsv.customer_email, 
		dcs.customer_status_name, dcs.country_code_ship, dcs.market_name,
		dcs.website_group_create, dcs.website_create, dcs.create_date, dcs.website_register, dcs.register_date,
		dcs.rank_num_tot_orders rank_num_tot_orders_customer, cpsv.num_tot_orders_customer, cpsv.num_dist_products,
		cpsv.product_type_name, cpsv.category_name, cpsv.product_family_group_name, cpsv.product_id_magento, cpsv.product_family_name, 
		cpsv.first_order_date, cpsv.last_order_date,
		cpsv.rank_num_tot_orders_product, cpsv.num_tot_orders_product, cpsv.subtotal_tot_orders
	from 
			Warehouse.act.fact_customer_product_signature_v cpsv
		inner join
			Warehouse.act.dim_customer_signature_v dcs on cpsv.customer_id = dcs.customer_id
go
