
use Warehouse 
go 

drop table Warehouse.alloc.dim_order_type_erp
go
drop table Warehouse.alloc.dim_order_type_erp_wrk
go

drop table Warehouse.alloc.dim_order_status_erp
go
drop table Warehouse.alloc.dim_order_status_erp_wrk
go

drop table Warehouse.alloc.dim_shipment_status_erp
go
drop table Warehouse.alloc.dim_shipment_status_erp_wrk
go

drop table Warehouse.alloc.dim_allocation_status
go
drop table Warehouse.alloc.dim_allocation_status_wrk
go

drop table Warehouse.alloc.dim_allocation_strategy
go
drop table Warehouse.alloc.dim_allocation_strategy_wrk
go

drop table Warehouse.alloc.dim_allocation_type
go
drop table Warehouse.alloc.dim_allocation_type_wrk
go

drop table Warehouse.alloc.dim_allocation_record_type
go
drop table Warehouse.alloc.dim_allocation_record_type_wrk
go



