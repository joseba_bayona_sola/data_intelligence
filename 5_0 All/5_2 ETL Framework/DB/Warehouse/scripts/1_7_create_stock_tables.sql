use Warehouse
go 

--------------------------------------------------------

create table Warehouse.stock.dim_stock_method_type(
	idStockMethodType_sk			int NOT NULL IDENTITY(1, 1), 
	stock_method_type_bk			varchar(50) NOT NULL,
	stock_method_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_stock_method_type add constraint [PK_stock_dim_stock_method_type]
	primary key clustered (idStockMethodType_sk);
go

alter table Warehouse.stock.dim_stock_method_type add constraint [DF_stock_dim_stock_method_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_stock_method_type add constraint [UNIQ_stock_dim_stock_method_type_stock_method_type_bk]
	unique (stock_method_type_bk);
go



create table Warehouse.stock.dim_stock_method_type_wrk(
	stock_method_type_bk			varchar(50) NOT NULL,
	stock_method_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_stock_method_type_wrk add constraint [DF_stock_dim_stock_method_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.stock.dim_stock_batch_type(
	idStockBatchType_sk				int NOT NULL IDENTITY(1, 1), 
	stock_batch_type_bk				varchar(50) NOT NULL,
	stock_batch_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_stock_batch_type add constraint [PK_stock_dim_stock_batch_type]
	primary key clustered (idStockBatchType_sk);
go

alter table Warehouse.stock.dim_stock_batch_type add constraint [DF_stock_dim_stock_batch_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_stock_batch_type add constraint [UNIQ_stock_dim_stock_batch_type_stock_batch_type_bk]
	unique (stock_batch_type_bk);
go



create table Warehouse.stock.dim_stock_batch_type_wrk(
	stock_batch_type_bk				varchar(50) NOT NULL,
	stock_batch_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_stock_batch_type_wrk add constraint [DF_stock_dim_stock_batch_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.stock.dim_stock_adjustment_type(
	idStockAdjustmentType_sk		int NOT NULL IDENTITY(1, 1), 
	stock_adjustment_type_bk		varchar(50) NOT NULL,
	stock_adjustment_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_stock_adjustment_type add constraint [PK_stock_dim_stock_adjustment_type]
	primary key clustered (idStockAdjustmentType_sk);
go

alter table Warehouse.stock.dim_stock_adjustment_type add constraint [DF_stock_dim_stock_adjustment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_stock_adjustment_type add constraint [UNIQ_stock_dim_stock_adjustment_type_stock_adjustment_type_bk]
	unique (stock_adjustment_type_bk);
go



create table Warehouse.stock.dim_stock_adjustment_type_wrk(
	stock_adjustment_type_bk		varchar(50) NOT NULL,
	stock_adjustment_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_stock_adjustment_type_wrk add constraint [DF_stock_dim_stock_adjustment_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.stock.dim_stock_movement_type(
	idStockMovementType_sk			int NOT NULL IDENTITY(1, 1), 
	stock_movement_type_bk			varchar(50) NOT NULL,
	stock_movement_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_stock_movement_type add constraint [PK_stock_dim_stock_movement_type]
	primary key clustered (idStockMovementType_sk);
go

alter table Warehouse.stock.dim_stock_movement_type add constraint [DF_stock_dim_stock_movement_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_stock_movement_type add constraint [UNIQ_stock_dim_stock_movement_type_stock_movement_type_bk]
	unique (stock_movement_type_bk);
go



create table Warehouse.stock.dim_stock_movement_type_wrk(
	stock_movement_type_bk			varchar(50) NOT NULL,
	stock_movement_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_stock_movement_type_wrk add constraint [DF_stock_dim_stock_movement_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.stock.dim_stock_movement_period(
	idStockMovementPeriod_sk		int NOT NULL IDENTITY(1, 1), 
	stock_movement_period_bk		varchar(50) NOT NULL,
	stock_movement_period_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	period_start					date NOT NULL, 
	period_finish					date NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_stock_movement_period add constraint [PK_stock_dim_stock_movement_period]
	primary key clustered (idStockMovementPeriod_sk);
go

alter table Warehouse.stock.dim_stock_movement_period add constraint [DF_stock_dim_stock_movement_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_stock_movement_period add constraint [UNIQ_stock_dim_stock_movement_period_stock_movement_period_bk]
	unique (stock_movement_period_bk);
go



create table Warehouse.stock.dim_stock_movement_period_wrk(
	stock_movement_period_bk		varchar(50) NOT NULL,
	stock_movement_period_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	period_start					date NOT NULL, 
	period_finish					date NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_stock_movement_period_wrk add constraint [DF_stock_dim_stock_movement_period_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




--------------------------------------------------------

create table Warehouse.stock.dim_wh_stock_item(
	idWHStockItem_sk				int NOT NULL IDENTITY(1, 1), 
	warehousestockitemid_bk			bigint NOT NULL, 

	idWarehouse_sk_fk				int NOT NULL, 
	idStockItem_sk_fk				int NOT NULL, 
	idStockMethodType_sk_fk			int NOT NULL, 

	wh_stock_item_description		varchar(200),

	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 
	qty_due_in						decimal(28, 8), 

	stocked							int,

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_wh_stock_item add constraint [PK_stock_dim_wh_stock_item]
	primary key clustered (idWHStockItem_sk);
go

alter table Warehouse.stock.dim_wh_stock_item add constraint [DF_stock_dim_wh_stock_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_wh_stock_item add constraint [UNIQ_stock_dim_wh_stock_item_warehousestockitemid_bk]
	unique (warehousestockitemid_bk);
go


alter table Warehouse.stock.dim_wh_stock_item add constraint [FK_stock_dim_wh_stock_item_dim_warehouse]
	foreign key (idWarehouse_sk_fk) references Warehouse.gen.dim_warehouse (idWarehouse_sk);
go

alter table Warehouse.stock.dim_wh_stock_item add constraint [FK_stock_dim_wh_stock_item_dim_stock_item]
	foreign key (idStockItem_sk_fk) references Warehouse.prod.dim_stock_item (idStockItem_sk);
go

alter table Warehouse.stock.dim_wh_stock_item add constraint [FK_stock_dim_wh_stock_item_dim_stock_method_type]
	foreign key (idStockMethodType_sk_fk) references Warehouse.stock.dim_stock_method_type (idStockMethodType_sk);
go



create table Warehouse.stock.dim_wh_stock_item_wrk(
	warehousestockitemid_bk			bigint NOT NULL, 

	idWarehouse_sk_fk				int NOT NULL, 
	idStockItem_sk_fk				int NOT NULL, 
	idStockMethodType_sk_fk			int NOT NULL, 

	wh_stock_item_description		varchar(200),

	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 
	qty_due_in						decimal(28, 8),  

	stocked							int,
	
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_wh_stock_item_wrk add constraint [DF_stock_dim_wh_stock_item_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.stock.dim_wh_stock_item_batch(
	idWHStockItemBatch_sk			int NOT NULL IDENTITY(1, 1), 
	warehousestockitembatchid_bk	bigint NOT NULL, 

	idWHStockItem_sk_fk				int NOT NULL, 
	idStockBatchType_sk_fk			int NOT NULL, 

	batch_id						bigint,

	fully_allocated_f				int, 
	fully_issued_f					int, 
	auto_adjusted_f					int, 
	
	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	qty_registered_sm				decimal(28, 8), 
	qty_disposed_sm					decimal(28, 8), 

	batch_arrived_date				datetime,
	batch_confirmed_date			datetime,
	batch_stock_register_date		datetime,

	local_product_unit_cost			decimal(28, 8), 
	local_carriage_unit_cost		decimal(28, 8), 
	local_duty_unit_cost			decimal(28, 8), 
	local_total_unit_cost			decimal(28, 8), 

	local_interco_carriage_unit_cost	decimal(28, 8), 
	local_total_unit_cost_interco		decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	delete_f						char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.dim_wh_stock_item_batch add constraint [PK_stock_dim_wh_stock_item_batch]
	primary key clustered (idWHStockItemBatch_sk);
go

alter table Warehouse.stock.dim_wh_stock_item_batch add constraint [DF_stock_dim_wh_stock_item_batch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.dim_wh_stock_item_batch add constraint [UNIQ_stock_dim_wh_stock_item_batch_warehousestockitembatchid_bk]
	unique (warehousestockitembatchid_bk);
go


alter table Warehouse.stock.dim_wh_stock_item_batch add constraint [FK_stock_dim_wh_stock_item_batch_dim_wh_stock_item]
	foreign key (idWHStockItem_sk_fk) references Warehouse.stock.dim_wh_stock_item (idWHStockItem_sk);
go

alter table Warehouse.stock.dim_wh_stock_item_batch add constraint [FK_stock_dim_wh_stock_item_batch_dim_stock_batch_type]
	foreign key (idStockBatchType_sk_fk) references Warehouse.stock.dim_stock_batch_type (idStockBatchType_sk);
go



create table Warehouse.stock.dim_wh_stock_item_batch_wrk(
	warehousestockitembatchid_bk	bigint NOT NULL, 

	idWHStockItem_sk_fk				int NOT NULL, 
	idStockBatchType_sk_fk			int NOT NULL, 

	batch_id						bigint,

	fully_allocated_f				int, 
	fully_issued_f					int, 
	auto_adjusted_f					int, 
	
	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	qty_registered_sm				decimal(28, 8), 
	qty_disposed_sm					decimal(28, 8), 

	batch_arrived_date				datetime,
	batch_confirmed_date			datetime,
	batch_stock_register_date		datetime,

	local_product_unit_cost			decimal(28, 8), 
	local_carriage_unit_cost		decimal(28, 8), 
	local_duty_unit_cost			decimal(28, 8), 
	local_total_unit_cost			decimal(28, 8), 

	local_interco_carriage_unit_cost	decimal(28, 8), 
	local_total_unit_cost_interco		decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	delete_f						char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.dim_wh_stock_item_batch_wrk add constraint [DF_stock_dim_wh_stock_item_batch_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.stock.fact_wh_stock_item_batch_movement(
	idWHStockItemBatchMovement_sk	int NOT NULL IDENTITY(1, 1), 
	batchstockmovementid_bk			bigint NOT NULL, 

	batch_stock_move_id				bigint NOT NULL, 
	move_id							varchar(20) NOT NULL,
	move_line_id					varchar(20) NOT NULL,

	idWHStockItemBatch_sk_fk		int NOT NULL, 
	idStockAdjustmentType_sk_fk		int NOT NULL, 
	idStockMovementType_sk_fk		int NOT NULL, 
	idStockMovementPeriod_sk_fk		int, 

	qty_movement					decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	batch_movement_date				datetime,

	delete_f						char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [PK_stock_fact_wh_stock_item_batch_movement]
	primary key clustered (idWHStockItemBatchMovement_sk);
go

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [DF_stock_fact_wh_stock_item_batch_movement_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [UNIQ_stock_fact_wh_stock_item_batch_movement_batchstockmovementid_bk]
	unique (batchstockmovementid_bk);
go


alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [FK_stock_fact_wh_stock_item_batch_movement_dim_wh_stock_item_batch]
	foreign key (idWHStockItemBatch_sk_fk) references Warehouse.stock.dim_wh_stock_item_batch (idWHStockItemBatch_sk);
go

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [FK_stock_fact_wh_stock_item_batch_movement_dim_stock_adjustment_type]
	foreign key (idStockAdjustmentType_sk_fk) references Warehouse.stock.dim_stock_adjustment_type (idStockAdjustmentType_sk);
go

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [FK_stock_fact_wh_stock_item_batch_movement_dim_stock_movement_type]
	foreign key (idStockMovementType_sk_fk) references Warehouse.stock.dim_stock_movement_type (idStockMovementType_sk);
go

alter table Warehouse.stock.fact_wh_stock_item_batch_movement add constraint [FK_stock_fact_wh_stock_item_batch_movement_dim_stock_movement_period]
	foreign key (idStockMovementPeriod_sk_fk) references Warehouse.stock.dim_stock_movement_period (idStockMovementPeriod_sk);
go



create table Warehouse.stock.fact_wh_stock_item_batch_movement_wrk(
	batchstockmovementid_bk			bigint NOT NULL, 

	batch_stock_move_id				bigint NOT NULL, 
	move_id							varchar(20) NOT NULL,
	move_line_id					varchar(20) NOT NULL,

	idWHStockItemBatch_sk_fk		int NOT NULL, 
	idStockAdjustmentType_sk_fk		int NOT NULL, 
	idStockMovementType_sk_fk		int NOT NULL, 
	idStockMovementPeriod_sk_fk		int, 

	qty_movement					decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	batch_movement_date				datetime,

	delete_f						char(1),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.stock.fact_wh_stock_item_batch_movement_wrk add constraint [DF_stock_fact_wh_stock_item_batch_movement_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 