use Warehouse
go


	drop trigger gen.trg_dim_customer_SCD;
	go

	create trigger gen.trg_dim_customer_SCD on gen.dim_customer_SCD
	for update, delete
	as
	begin
		set nocount on

		insert gen.dim_customer_SCD (
			idCustomer_sk_fk, customer_id_bk, customer_email,
			created_at, updated_at,
			idStore_sk_fk, idMarket_sk_fk,
			idCustomerType_sk_fk, idCustomerStatus_sk_fk,
			prefix, first_name, last_name, 
			dob, gender, language, 
			phone_number, street, city, postcode, region, idRegion_sk_fk, idCountry_sk_fk, 
			postcode_s, idCountry_s_sk_fk, 
			idCustomerUnsubscribe_sk_fk,
			unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d_sk_fk, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d_sk_fk, 
			unsubscribe_text_message_f, unsubscribe_text_message_d_sk_fk, 
			reminder_f, idReminderType_sk_fk, idReminderPeriod_sk_fk, 
			reorder_f, 
			referafriend_code, days_worn_info, 
			idCalendarFrom_sk_fk, idCalendarTo_sk_fk, isCurrent,
			idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts)

			select d.idCustomer_sk_fk, d.customer_id_bk, d.customer_email,
				d.created_at, d.updated_at,
				d.idStore_sk_fk, d.idMarket_sk_fk,
				d.idCustomerType_sk_fk, d.idCustomerStatus_sk_fk,
				d.prefix, d.first_name, d.last_name, 
				d.dob, d.gender, d.language, 
				d.phone_number, d.street, d.city, d.postcode, d.region, d.idRegion_sk_fk, d.idCountry_sk_fk, 
				d.postcode_s, d.idCountry_s_sk_fk, 
				d.idCustomerUnsubscribe_sk_fk,
				d.unsubscribe_mark_email_magento_f, d.unsubscribe_mark_email_magento_d_sk_fk, d.unsubscribe_mark_email_dotmailer_f, d.unsubscribe_mark_email_dotmailer_d_sk_fk, 
				d.unsubscribe_text_message_f, d.unsubscribe_text_message_d_sk_fk, 
				d.reminder_f, d.idReminderType_sk_fk, d.idReminderPeriod_sk_fk, 
				d.reorder_f, 
				d.referafriend_code, d.days_worn_info, 
				d.idCalendarFrom_sk_fk, i.idCalendarFrom_sk_fk, 'N', 
				isnull(d.idETLBatchRun_upd, d.idETLBatchRun_ins), isnull(d.upd_ts, d.ins_ts), i.idETLBatchRun_upd, i.upd_ts
			from
				deleted d
			inner join
				inserted i on d.customer_id_bk = i.customer_id_bk;
	end;
	go