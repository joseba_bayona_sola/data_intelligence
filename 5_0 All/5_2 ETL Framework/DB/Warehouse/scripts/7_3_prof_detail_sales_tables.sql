use Warehouse
go 

------------------------------------------------------------------------
----------------------- dim_order_header ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun_ins, count(*) num_rows, count(upd_ts) num_upd
	from Warehouse.sales.dim_order_header
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*) num_rows
	from Warehouse.sales.dim_order_header
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	select count(*) num_rows, count(distinct idOrderHeader_sk) num_dist_rows
	from Warehouse.sales.dim_order_header


------------------------------------------------------------------------
----------------------- fact_order_line ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun_ins, count(*) num_rows, count(upd_ts) num_upd
	from Warehouse.sales.fact_order_line
	group by idETLBatchRun_ins
	order by idETLBatchRun_ins

	select idETLBatchRun_upd, count(*) num_rows
	from Warehouse.sales.fact_order_line
	group by idETLBatchRun_upd
	order by idETLBatchRun_upd

	select count(*) num_rows, count(distinct idOrderLine_sk) num_dist_rows
	from Warehouse.sales.fact_order_line
