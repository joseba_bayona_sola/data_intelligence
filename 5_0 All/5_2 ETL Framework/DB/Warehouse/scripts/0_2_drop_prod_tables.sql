use Warehouse
go 

drop table Warehouse.prod.dim_product_type
go
drop table Warehouse.prod.dim_product_type_wrk
go

drop table Warehouse.prod.dim_category
go
drop table Warehouse.prod.dim_category_wrk
go


drop table Warehouse.prod.dim_cl_type
go
drop table Warehouse.prod.dim_cl_type_wrk
go

drop table Warehouse.prod.dim_cl_feature
go
drop table Warehouse.prod.dim_cl_feature_wrk
go


drop table Warehouse.prod.dim_manufacturer
go
drop table Warehouse.prod.dim_manufacturer_wrk
go



drop table Warehouse.prod.dim_product_lifecycle
go
drop table Warehouse.prod.dim_product_lifecycle_wrk
go

drop table Warehouse.prod.dim_product_visibility
go
drop table Warehouse.prod.dim_product_visibility_wrk
go



drop table Warehouse.prod.dim_glass_vision_type
go
drop table Warehouse.prod.dim_glass_vision_type_wrk
go

drop table Warehouse.prod.dim_glass_package_type
go
drop table Warehouse.prod.dim_glass_package_type_wrk
go



drop table Warehouse.prod.dim_param_BC
go
drop table Warehouse.prod.dim_param_BC_wrk
go

drop table Warehouse.prod.dim_param_DI
go
drop table Warehouse.prod.dim_param_DI_wrk
go

drop table Warehouse.prod.dim_param_PO
go
drop table Warehouse.prod.dim_param_PO_wrk
go


drop table Warehouse.prod.dim_param_CY
go
drop table Warehouse.prod.dim_param_CY_wrk
go

drop table Warehouse.prod.dim_param_AX
go
drop table Warehouse.prod.dim_param_AX_wrk
go


drop table Warehouse.prod.dim_param_AD
go
drop table Warehouse.prod.dim_param_AD_wrk
go

drop table Warehouse.prod.dim_param_DO
go
drop table Warehouse.prod.dim_param_DO_wrk
go


drop table Warehouse.prod.dim_param_COL
go
drop table Warehouse.prod.dim_param_COL_wrk
go



drop table Warehouse.prod.dim_product_family
go
drop table Warehouse.prod.dim_product_family_wrk
go



drop table Warehouse.prod.dim_product_type_oh
go
drop table Warehouse.prod.dim_product_type_oh_wrk
go



drop table Warehouse.prod.dim_product_family_group
go
drop table Warehouse.prod.dim_product_family_group_wrk
go


drop table Warehouse.prod.dim_product_family_pack_size
go
drop table Warehouse.prod.dim_product_family_pack_size_wrk
go

drop table Warehouse.prod.dim_price_type_pf
go
drop table Warehouse.prod.dim_price_type_pf_wrk
go

drop table Warehouse.prod.dim_product_family_price
go
drop table Warehouse.prod.dim_product_family_price
go


drop table Warehouse.prod.dim_product
go
drop table Warehouse.prod.dim_product_wrk
go

drop table Warehouse.prod.dim_stock_item
go
drop table Warehouse.prod.dim_stock_item_wrk
go