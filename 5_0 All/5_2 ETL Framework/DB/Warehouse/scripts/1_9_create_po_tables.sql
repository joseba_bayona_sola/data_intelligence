use Warehouse
go 

--------------------------------------------------------

create table Warehouse.po.dim_po_source(
	idPOSource_sk					int NOT NULL IDENTITY(1, 1), 
	po_source_bk					varchar(50) NOT NULL,
	po_source_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_po_source add constraint [PK_po_dim_po_source]
	primary key clustered (idPOSource_sk);
go

alter table Warehouse.po.dim_po_source add constraint [DF_po_dim_po_source_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_po_source add constraint [UNIQ_po_dim_po_source_po_source_bk]
	unique (po_source_bk);
go



create table Warehouse.po.dim_po_source_wrk(
	po_source_bk					varchar(50) NOT NULL,
	po_source_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_po_source_wrk add constraint [DF_po_dim_po_source_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.po.dim_po_type(
	idPOType_sk						int NOT NULL IDENTITY(1, 1), 
	po_type_bk						varchar(50) NOT NULL,
	po_type_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_po_type add constraint [PK_po_dim_po_type]
	primary key clustered (idPOType_sk);
go

alter table Warehouse.po.dim_po_type add constraint [DF_po_dim_po_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_po_type add constraint [UNIQ_po_dim_po_type_po_type_bk]
	unique (po_type_bk);
go



create table Warehouse.po.dim_po_type_wrk(
	po_type_bk						varchar(50) NOT NULL,
	po_type_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_po_type_wrk add constraint [DF_po_dim_po_type_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.po.dim_po_status(
	idPOStatus_sk					int NOT NULL IDENTITY(1, 1), 
	po_status_bk					varchar(50) NOT NULL,
	po_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_po_status add constraint [PK_po_dim_po_status]
	primary key clustered (idPOStatus_sk);
go

alter table Warehouse.po.dim_po_status add constraint [DF_po_dim_po_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_po_status add constraint [UNIQ_po_dim_po_status_po_status_bk]
	unique (po_status_bk);
go



create table Warehouse.po.dim_po_status_wrk(
	po_status_bk					varchar(50) NOT NULL,
	po_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_po_status_wrk add constraint [DF_po_dim_po_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------------------------------------------

create table Warehouse.po.dim_pol_status(
	idPOLStatus_sk					int NOT NULL IDENTITY(1, 1), 
	pol_status_bk					varchar(50) NOT NULL,
	pol_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_pol_status add constraint [PK_po_dim_pol_status]
	primary key clustered (idPOLStatus_sk);
go

alter table Warehouse.po.dim_pol_status add constraint [DF_po_dim_pol_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_pol_status add constraint [UNIQ_po_dim_pol_status_pol_status_bk]
	unique (pol_status_bk);
go



create table Warehouse.po.dim_pol_status_wrk(
	pol_status_bk					varchar(50) NOT NULL,
	pol_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_pol_status_wrk add constraint [DF_po_dim_pol_status_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.po.dim_pol_problems(
	idPOLProblems_sk				int NOT NULL IDENTITY(1, 1), 
	pol_problems_bk					varchar(50) NOT NULL,
	pol_problems_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_pol_problems add constraint [PK_po_dim_pol_problems]
	primary key clustered (idPOLProblems_sk);
go

alter table Warehouse.po.dim_pol_problems add constraint [DF_po_dim_pol_problems_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_pol_problems add constraint [UNIQ_po_dim_pol_problems_pol_problems_bk]
	unique (pol_problems_bk);
go



create table Warehouse.po.dim_pol_problems_wrk(
	pol_problems_bk					varchar(50) NOT NULL,
	pol_problems_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_pol_problems_wrk add constraint [DF_po_dim_pol_problems_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.po.dim_wh_operator(
	idWHOperator_sk					int NOT NULL IDENTITY(1, 1), 
	wh_operator_bk					varchar(200) NOT NULL,
	wh_operator_name				varchar(200) NOT NULL, 

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_wh_operator add constraint [PK_po_dim_wh_operator]
	primary key clustered (idWHOperator_sk);
go

alter table Warehouse.po.dim_wh_operator add constraint [DF_po_dim_wh_operator_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_wh_operator add constraint [UNIQ_po_dim_wh_operator_wh_operator_bk]
	unique (wh_operator_bk);
go



create table Warehouse.po.dim_wh_operator_wrk(
	wh_operator_bk					varchar(200) NOT NULL,
	wh_operator_name				varchar(200) NOT NULL, 
	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_wh_operator_wrk add constraint [DF_po_dim_wh_operator_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------------------------------------------

create table Warehouse.po.dim_purchase_order(
	idPurchaseOrder_sk				int NOT NULL IDENTITY(1, 1), 
	purchaseorderid_bk				bigint NOT NULL,

	idWarehouse_sk_fk				int NOT NULL, 
	idSupplier_sk_fk				int NOT NULL, 
	idPOSource_sk_fk				int, 
	idPOType_sk_fk					int, 
	idPOStatus_sk_fk				int, 
	idWHOperatorCreate_sk_fk		int, 
	idWHOperatorAssigned_sk_fk		int, 

	purchase_order_number			varchar(20), 
	leadtime						int, 

	created_date					datetime,
	approved_date					datetime,
	confirmed_date					datetime,
	submitted_date					datetime,
	completed_date					datetime,
	due_date						datetime,

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.dim_purchase_order add constraint [PK_po_dim_purchase_order]
	primary key clustered (idPurchaseOrder_sk);
go

alter table Warehouse.po.dim_purchase_order add constraint [DF_po_dim_purchase_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.dim_purchase_order add constraint [UNIQ_po_dim_purchase_order_purchaseorderid_bk]
	unique (purchaseorderid_bk);
go


alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_warehouse]
	foreign key (idWarehouse_sk_fk) references Warehouse.gen.dim_warehouse (idWarehouse_sk);
go
alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_supplier]
	foreign key (idSupplier_sk_fk) references Warehouse.gen.dim_supplier (idSupplier_sk);
go

alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_po_source]
	foreign key (idPOSource_sk_fk) references Warehouse.po.dim_po_source (idPOSource_sk);
go
alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_po_type]
	foreign key (idPOType_sk_fk) references Warehouse.po.dim_po_type (idPOType_sk);
go
alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_po_status]
	foreign key (idPOStatus_sk_fk) references Warehouse.po.dim_po_status (idPOStatus_sk);
go

alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_wh_operator_cr]
	foreign key (idWHOperatorCreate_sk_fk) references Warehouse.po.dim_wh_operator (idWHOperator_sk);
go
alter table Warehouse.po.dim_purchase_order add constraint [FK_po_dim_purchase_order_dim_wh_operator_as]
	foreign key (idWHOperatorAssigned_sk_fk) references Warehouse.po.dim_wh_operator (idWHOperator_sk);
go


create table Warehouse.po.dim_purchase_order_wrk(
	purchaseorderid_bk				bigint NOT NULL,

	idWarehouse_sk_fk				int NOT NULL, 
	idSupplier_sk_fk				int NOT NULL, 
	idPOSource_sk_fk				int, 
	idPOType_sk_fk					int, 
	idPOStatus_sk_fk				int, 
	idWHOperatorCreate_sk_fk		int, 
	idWHOperatorAssigned_sk_fk		int, 

	purchase_order_number			varchar(20), 
	leadtime						int, 

	created_date					datetime,
	approved_date					datetime,
	confirmed_date					datetime,
	submitted_date					datetime,
	completed_date					datetime,
	due_date						datetime,

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.dim_purchase_order_wrk add constraint [DF_po_dim_purchase_order_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





--------------------------------------------------------

create table Warehouse.po.fact_purchase_order_line(
	idPurchaseOrderLine_sk			int NOT NULL IDENTITY(1, 1), 
	purchaseorderlineid_bk			bigint NOT NULL,

	idPurchaseOrder_sk_fk			int NOT NULL, 
	idPOLStatus_sk_fk				int, 
	idPOLProblems_sk_fk				int, 
	idStockItem_sk_fk				int NOT NULL, 
	idProductFamilyPrice_sk_fk		int NOT NULL, 

	purchase_order_line_id			bigint,

	quantity_ordered				decimal(28, 8), 
	quantity_received				decimal(28, 8), 
	quantity_planned				decimal(28, 8), 
	quantity_open					decimal(28, 8), 
	quantity_cancelled				decimal(28, 8), 

	local_unit_cost					decimal(28, 8),
	local_line_cost					decimal(28, 8),

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL, 
	idETLBatchRun_upd				bigint, 
	upd_ts							datetime);
go 

alter table Warehouse.po.fact_purchase_order_line add constraint [PK_po_fact_purchase_order_line]
	primary key clustered (idPurchaseOrderLine_sk);
go

alter table Warehouse.po.fact_purchase_order_line add constraint [DF_po_fact_purchase_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

alter table Warehouse.po.fact_purchase_order_line add constraint [UNIQ_po_fact_purchase_order_line_purchaseorderlineid_bk]
	unique (purchaseorderlineid_bk);
go


alter table Warehouse.po.fact_purchase_order_line add constraint [FK_po_fact_purchase_order_line_dim_purchase_order]
	foreign key (idPurchaseOrder_sk_fk) references Warehouse.po.dim_purchase_order (idPurchaseOrder_sk);
go

alter table Warehouse.po.fact_purchase_order_line add constraint [FK_po_fact_purchase_order_line_dim_pol_status]
	foreign key (idPOLStatus_sk_fk) references Warehouse.po.dim_pol_status (idPOLStatus_sk);
go
alter table Warehouse.po.fact_purchase_order_line add constraint [FK_po_fact_purchase_order_line_dim_pol_problems]
	foreign key (idPOLProblems_sk_fk) references Warehouse.po.dim_pol_problems (idPOLProblems_sk);
go

alter table Warehouse.po.fact_purchase_order_line add constraint [FK_po_fact_purchase_order_line_dim_stock_item]
	foreign key (idStockItem_sk_fk) references Warehouse.prod.dim_stock_item (idStockItem_sk);
go
alter table Warehouse.po.fact_purchase_order_line add constraint [FK_po_fact_purchase_order_line_dim_product_family_price]
	foreign key (idProductFamilyPrice_sk_fk) references Warehouse.prod.dim_product_family_price (idProductFamilyPrice_sk);
go


create table Warehouse.po.fact_purchase_order_line_wrk(
	purchaseorderlineid_bk			bigint NOT NULL,

	idPurchaseOrder_sk_fk			int NOT NULL, 
	idPOLStatus_sk_fk				int, 
	idPOLProblems_sk_fk				int, 
	idStockItem_sk_fk				int NOT NULL, 
	idProductFamilyPrice_sk_fk		int NOT NULL, 

	purchase_order_line_id			bigint,

	quantity_ordered				decimal(28, 8), 
	quantity_received				decimal(28, 8), 
	quantity_planned				decimal(28, 8), 
	quantity_open					decimal(28, 8), 
	quantity_cancelled				decimal(28, 8), 

	local_unit_cost					decimal(28, 8),
	local_line_cost					decimal(28, 8),

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun_ins				bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table Warehouse.po.fact_purchase_order_line_wrk add constraint [DF_po_fact_purchase_order_line_wrk_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

