
use Warehouse
go

select top 1000 idCustomer_sk_fk, client_id, 
	website_id, store_id, store_url, 
	title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
	currency, country, 
	datejoin, dateunjoin, 
	email_origine, email_origin, seed, clienturn, source, code, 
	product_id, 
	order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url, 

	prescription_expiry_date, prescription_wearer, prescription_optician, 
	bounce, 
	card_expiry_date, 
	unsubscribe, website_unsubscribe, w_unsub_date, 
	registration_date, first_order_date, 
	last_logged_in_date, last_order_date, last_order_id, last_order_source, num_of_orders,

	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
	segment, segment_2, segment_3, segment_4, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
	check_sum, 

	lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
	lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2,

	dailies_store_id1, dailies_store_id2,
	dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date, 
	google_shopping_gtin_daily_sku1, google_shopping_gtin_daily_sku2,

	monthlies_store_id1, monthlies_store_id2,
	monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2,
	monthlies_last_order_date, 
	google_shopping_gtin_monthly_sku1, google_shopping_gtin_monthly_sku2, 

	colours_store_id1, colours_store_id2,
	colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date, 
	google_shopping_gtin_colours_sku1, google_shopping_gtin_colours_sku2, 

	solutions_store_id1, solutions_store_id2,
	solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2,
	solutions_last_order_date, 
	google_shopping_gtin_solutions_sku1, google_shopping_gtin_solutions_sku2, 

	other_store_id1, other_store_id2,
	other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2,
	other_last_order_date, 
	google_shopping_gtin_other_sku1, google_shopping_gtin_other_sku2, 

	sunglasses_store_id1, sunglasses_store_id2,
	sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2,
	sunglasses_last_order_date, 

	glasses_store_id1, glasses_store_id2,
	glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2,
	glasses_last_order_date,

	last_product_type,
	last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
	last_order_lens_qty,
	last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2, 
	
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts 
from Warehouse.dm.EMV_sync_list


select top 1000 idCustomer_sk_fk, client_id, 
	website_id, store_id, store_url, 
	title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
	currency, country, 
	datejoin, dateunjoin, 
	email_origine, email_origin, seed, clienturn, source, code, 
	product_id, 
	order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url, 

	prescription_expiry_date, prescription_wearer, prescription_optician, 
	bounce, 
	card_expiry_date, 
	unsubscribe, website_unsubscribe, w_unsub_date, 
	registration_date, first_order_date, 
	last_logged_in_date, last_order_date, last_order_id, last_order_source, num_of_orders,

	segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
	segment, segment_2, segment_3, segment_4, 
	emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
	check_sum, 

	lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
	lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2,

	dailies_store_id1, dailies_store_id2,
	dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date, 

	monthlies_store_id1, monthlies_store_id2,
	monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2,
	monthlies_last_order_date, 

	colours_store_id1, colours_store_id2,
	colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date, 

	solutions_store_id1, solutions_store_id2,
	solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2,
	solutions_last_order_date, 

	other_store_id1, other_store_id2,
	other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2,
	other_last_order_date, 

	sunglasses_store_id1, sunglasses_store_id2,
	sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2,
	sunglasses_last_order_date, 

	glasses_store_id1, glasses_store_id2,
	glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2,
	glasses_last_order_date,

	last_product_type,
	last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
	last_order_lens_qty,
	last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2, 
	
	idETLBatchRun_ins, ins_ts
from Warehouse.dm.EMV_sync_list_wrk

