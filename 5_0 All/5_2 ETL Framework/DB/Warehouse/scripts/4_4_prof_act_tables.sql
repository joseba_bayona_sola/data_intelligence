use Warehouse
go 


select idActivityGroup_sk, 
	activity_group_name_bk, activity_group_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.dim_activity_group

select 
	activity_group_name_bk, activity_group_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.act.dim_activity_group_wrk



select idActivityType_sk, 
	activity_type_name_bk, activity_type_name,
	idActivityGroup_sk_fk,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.dim_activity_type

select 
	activity_type_name_bk, activity_type_name,
	idActivityGroup_sk_fk,
	idETLBatchRun_ins, ins_ts
from Warehouse.act.dim_activity_type_wrk



select idActivitySales_sk, 
	order_id_bk, 
	idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, idCalendarNextActivityDate_sk_fk, 
	idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
	idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk, discount_f, idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh,
	idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

	local_subtotal, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, 
	
	local_to_global_rate, order_currency_code,

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.fact_activity_sales

select 
	order_id_bk, 
	idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, idCalendarNextActivityDate_sk_fk, 
	idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
	idChannel_sk_fk, idMarketingChannel_sk_fk, idPriceType_sk_fk, discount_f, idProductTypeOH_sk_fk, order_qty_time, num_diff_product_type_oh,
	idCustomerStatusLifecycle_sk_fk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

	local_subtotal, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, 

	local_to_global_rate, order_currency_code,

	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_activity_sales_wrk




select idActivityOther_sk, 
	activity_id_bk, 
	idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, 
	idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
	activity_seq_no, 
	
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.fact_activity_other

select 
	activity_id_bk, 
	idCalendarActivityDate_sk_fk, activity_date, idCalendarPrevActivityDate_sk_fk, 
	idActivityType_sk_fk, idStore_sk_fk, idCustomer_sk_fk,
	activity_seq_no, 

	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_activity_other_wrk



select idCustomer_sk_fk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
	idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
	idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, idCalendarLastLoginDate_sk_fk, 

	idCustomerOrigin_sk_fk,

	num_tot_orders_web, 
	num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund, 
	num_tot_lapsed, num_tot_reactivate,
	num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
	num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,

	idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk, 

	idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
	main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time,
	main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time,
	idChannelMain_sk_fk, idMarketingChannelMain_sk_fk, idPriceTypeMain_sk_fk, 
	idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 

	order_id_bk_first, order_id_bk_last,
	first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
	local_to_global_rate, order_currency_code,

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.fact_customer_signature

select idCustomer_sk_fk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
	idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
	idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, idCalendarLastLoginDate_sk_fk, 

	idCustomerOrigin_sk_fk,

	num_tot_orders_web, 
	num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund, 
	num_tot_lapsed, num_tot_reactivate,
	num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
	num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,

	idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk, 

	idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
	main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time,
	main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time,
	idChannelMain_sk_fk, idMarketingChannelMain_sk_fk, idPriceTypeMain_sk_fk, 
	idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 

	order_id_bk_first, order_id_bk_last,
	first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
	local_to_global_rate, order_currency_code,
	
	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_customer_signature_wrk


select idCustomerSignatureSCD_sk, idCustomer_sk_fk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 

	idCustomerStatusLifecycle_sk_fk,  

	idCalendarFrom_sk_fk, idCalendarTo_sk_fk,
	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_customer_signature_scd



select order_id_bk, idCustomer_sk_fk,
	idStore_sk_fk, idCalendarActivityDate_sk_fk, activity_date, 
	num_tot_orders_web,
	num_order, num_tot_orders, subtotal_tot_orders, 
	num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
	idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
	idChannel_sk_fk,idMarketingChannel_sk_fk, channel_name, idPriceType_sk_fk, price_type_name, 
	customer_status_name, customer_order_seq_no,
	global_discount, global_subtotal, 
	local_to_global_rate, order_currency_code
from Warehouse.act.fact_customer_signature_aux_orders



select idCustomer_sk_fk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, idCalendarLastLoginDate_sk_fk, 
	idCustomerOrigin_sk_fk,
	num_tot_lapsed, num_tot_reactivate,
	num_tot_emails, num_tot_text_messages, num_tot_reviews, num_tot_logins,
	idCustomerStatusLifecycle_sk_fk, idCalendarStatusUpdateDate_sk_fk
from Warehouse.act.fact_customer_signature_aux_other


select idCustomer_sk_fk, 
	idStoreFirstOrder_sk_fk, idStoreLastOrder_sk_fk, 
	idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
	num_tot_orders_web, num_tot_orders_gen, 
	num_tot_orders, subtotal_tot_orders, num_tot_cancel, subtotal_tot_cancel, num_tot_refund, subtotal_tot_refund, 
	num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
	idChannelFirst_sk_fk, idMarketingChannelFirst_sk_fk, idPriceTypeFirst_sk_fk, 
	order_id_bk_first, order_id_bk_last,	
	first_discount_amount, first_subtotal_amount, last_subtotal_amount, 
	local_to_global_rate, order_currency_code
from Warehouse.act.fact_customer_signature_aux_sales


select idCustomer_sk_fk, 
	idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
	main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time, 
	main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time,
	idChannelMain_sk_fk, idMarketingChannelMain_sk_fk, idPriceTypeMain_sk_fk
from Warehouse.act.fact_customer_signature_aux_sales_main







select idCustomer_sk_fk, 
	idProductTypeOHMain_sk_fk, num_diff_product_type_oh,
	main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time 
from Warehouse.act.fact_customer_signature_aux_sales_main_product

select idCustomer_sk_fk, 
	main_order_freq_time, min_order_freq_time, avg_order_freq_time, max_order_freq_time, stdev_order_freq_time
from Warehouse.act.fact_customer_signature_aux_sales_main_freq

select idCustomer_sk_fk, idChannelMain_sk_fk
from Warehouse.act.fact_customer_signature_aux_sales_main_ch

select idCustomer_sk_fk, idMarketingChannelMain_sk_fk
from Warehouse.act.fact_customer_signature_aux_sales_main_mch

select idCustomer_sk_fk, idPriceTypeMain_sk_fk
from Warehouse.act.fact_customer_signature_aux_sales_main_pr



select idCustomer_sk_fk
from Warehouse.act.fact_customer_signature_aux_customers


	

select top 1000 idCustomer_sk_fk,
	customer_id, customer_email, first_name, last_name,
	company_name_create, website_group_create, website_create, create_date, create_time_mm,
	website_register, register_date, 
	customer_origin_name,
	market_name, country_code_ship, 
	customer_unsubscribe_name,
	customer_status_name, rank_num_tot_orders, num_tot_orders, subtotal_tot_orders,
	main_type_oh_name, num_diff_product_type_oh, rank_main_order_qty_time, main_order_qty_time, rank_avg_order_freq_time, avg_order_freq_time	
from Warehouse.act.dim_customer_signature_v




select customer_id_bk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
	idCustomerOrigin_sk_fk,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.dim_customer_cr_reg

select customer_id_bk, 
	idStoreCreate_sk_fk, idCalendarCreateDate_sk_fk, 
	idStoreRegister_sk_fk, idCalendarRegisterDate_sk_fk, 
	idCustomerOrigin_sk_fk,
	idETLBatchRun_ins, ins_ts
from Warehouse.act.dim_customer_cr_reg_wrk



select idCR_Time_MM_sk,
	cr_time_mm_bk, cr_time_mm, cr_time_name,
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.dim_rank_cr_time_mm

select cr_time_mm_bk, cr_time_mm, cr_time_name,
	idETLBatchRun_ins, ins_ts
from Warehouse.act.dim_rank_cr_time_mm_wrk




select idCustomer_sk_fk, idProductFamily_sk_fk
	idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
	order_id_bk_first, order_id_bk_last, 

	num_tot_orders, subtotal_tot_orders, 

	num_dist_products, product_percentage, category_percentage,

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.fact_customer_product_signature

select idCustomer_sk_fk, idProductFamily_sk_fk
	idCalendarFirstOrderDate_sk_fk, idCalendarLastOrderDate_sk_fk, 
	order_id_bk_first, order_id_bk_last, 

	num_tot_orders, subtotal_tot_orders, 

	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_customer_product_signature_wrk


select idActivityLogin_sk, 
	idCustomer_sk_fk, 
	idCalendarFirstLogin_sk_fk, idCalendarLastLogin_sk_fk, num_tot_logins, 

	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.act.fact_activity_login

select 
	idCustomer_sk_fk, 
	idCalendarFirstLogin_sk_fk, idCalendarLastLogin_sk_fk, num_tot_logins, 

	idETLBatchRun_ins, ins_ts
from Warehouse.act.fact_activity_login_wrk

