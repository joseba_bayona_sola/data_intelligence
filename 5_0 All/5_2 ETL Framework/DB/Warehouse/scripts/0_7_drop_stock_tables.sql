use Warehouse
go 


drop table Warehouse.stock.fact_wh_stock_item_batch_movement
go
drop table Warehouse.stock.fact_wh_stock_item_batch_movement_wrk
go

----------------------------------------------------------------

drop table Warehouse.stock.dim_wh_stock_item_batch
go
drop table Warehouse.stock.dim_wh_stock_item_batch_wrk
go

drop table Warehouse.stock.dim_wh_stock_item
go
drop table Warehouse.stock.dim_wh_stock_item_wrk
go

----------------------------------------------------------------

drop table Warehouse.stock.dim_stock_method_type
go
drop table Warehouse.stock.dim_stock_method_type_wrk
go

drop table Warehouse.stock.dim_stock_batch_type
go
drop table Warehouse.stock.dim_stock_batch_type_wrk
go

drop table Warehouse.stock.dim_stock_adjustment_type
go
drop table Warehouse.stock.dim_stock_adjustment_type_wrk
go

drop table Warehouse.stock.dim_stock_movement_type
go
drop table Warehouse.stock.dim_stock_movement_type_wrk
go

drop table Warehouse.stock.dim_stock_movement_period
go
drop table Warehouse.stock.dim_stock_movement_period_wrk
go

