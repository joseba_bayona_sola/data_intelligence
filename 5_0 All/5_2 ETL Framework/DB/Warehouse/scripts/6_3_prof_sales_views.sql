
-- DIM ORDER HEADER 
select top 1000 * 
from Warehouse.sales.dim_order_header_v

select top 1000 order_id_bk, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
 	order_date, order_date_c, order_week_day, order_time_name, order_day_part,
	invoice_date, invoice_date_c, invoice_week_day, 
	shipment_date, shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part,
	refund_date, refund_date_c, refund_week_day, 
	company_name, store_name, website_type, website_group, website, code_tld, market_name,
	customer_id, customer_email, 
	country_code_ship, country_name_ship, postcode_shipping, 
	country_code_bill, country_name_bill, postcode_billing, 
	order_stage_name,  order_status_name, adjustment_order, order_type_name, order_status_magento_name, 
	payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, price_type_name, discount_f,
	reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
	channel_name, coupon_code,
	customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, order_source,

	total_qty_unit, total_qty_unit_refunded, total_qty_pack, -- weight, 

	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, -- oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, -- local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

	global_subtotal, global_shipping, global_discount, global_store_credit_used, 
	global_total_inc_vat, -- global_total_exc_vat, global_total_vat, global_total_prof_fee, 
	global_total_refund, global_store_credit_given, global_bank_online_given, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund, 
	global_total_aft_refund_inc_vat, -- global_total_aft_refund_exc_vat, global_total_aft_refund_vat, global_total_aft_refund_prof_fee, 
	-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

	local_to_global_rate, order_currency_code, 
	discount_percent, -- vat_percent, oh.prof_fee_percent,
	num_lines 
from Warehouse.sales.dim_order_header_v
--where shipment_time_name in ('22:00:00', '22:30:00', '23:00:00', '23:30:00')
-- where customer_id in (1284317, 1182787, 1162831)
-- where customer_id in (1984033, 1468860, 869595, 947845)
-- where company_name <> 'Vision Direct BV'

	-- ORDER DATE - TIME
	select order_date_c, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_date_c
	order by order_date_c

	select order_week_day, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_week_day
	order by order_week_day

	select order_time_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_time_name
	order by order_time_name

	-- INVOICE DATE - TIME
	select invoice_date_c, count(*)
	from Warehouse.sales.dim_order_header_v
	group by invoice_date_c
	order by invoice_date_c

	select invoice_week_day, count(*)
	from Warehouse.sales.dim_order_header_v
	group by invoice_week_day
	order by invoice_week_day

	-- SHIPMENT DATE - TIME
	select shipment_date_c, count(*)
	from Warehouse.sales.dim_order_header_v
	group by shipment_date_c
	order by shipment_date_c

	select shipment_week_day, count(*)
	from Warehouse.sales.dim_order_header_v
	group by shipment_week_day
	order by shipment_week_day

	select shipment_time_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by shipment_time_name
	order by count(*) desc, shipment_time_name

	-- REFUND DATE - TIME
	select refund_date_c, count(*)
	from Warehouse.sales.dim_order_header_v
	group by refund_date_c
	order by refund_date_c

	select refund_week_day, count(*)
	from Warehouse.sales.dim_order_header_v
	group by refund_week_day
	order by refund_week_day

	-- COMPANY - STORE
	select company_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by company_name
	order by company_name

	select store_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by store_name
	order by store_name

	select website, count(*)
	from Warehouse.sales.dim_order_header_v
	group by website
	order by website

	select market_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by market_name
	order by market_name

	-- CUSTOMER
	select top 1000 customer_id, count(*) num
	from Warehouse.sales.dim_order_header_v
	where store_name <> 'visiondirect.nl'
	group by customer_id
	order by num desc
		

	-- COUNTRY
	select country_code_ship, count(*)
	from Warehouse.sales.dim_order_header_v
	group by country_code_ship
	order by country_code_ship

	select country_code_bill, count(*)
	from Warehouse.sales.dim_order_header_v
	group by country_code_bill
	order by country_code_bill

	-- ORDER 
	select order_stage_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_stage_name
	order by order_stage_name

	select order_status_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_status_name
	order by order_status_name

	select adjustment_order, count(*)
	from Warehouse.sales.dim_order_header_v
	group by adjustment_order
	order by adjustment_order

	select order_type_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_type_name
	order by order_type_name

	select order_status_magento_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by order_status_magento_name
	order by order_status_magento_name

	-- PAYMENT
	select payment_method_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by payment_method_name
	order by payment_method_name

	select cc_type_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by cc_type_name
	order by cc_type_name

	-- SHIPPING
	select shipping_carrier_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by shipping_carrier_name
	order by shipping_carrier_name

	select shipping_method_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by shipping_method_name
	order by shipping_method_name

	-- OTHER
	select telesales_username, count(*)
	from Warehouse.sales.dim_order_header_v
	group by telesales_username
	order by telesales_username

	select price_type_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by price_type_name
	order by price_type_name

	select discount_f, count(*)
	from Warehouse.sales.dim_order_header_v
	group by discount_f
	order by discount_f


	-- REMINDER - REORDER
	select reminder_type_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by reminder_type_name
	order by reminder_type_name

	select reminder_period_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by reminder_period_name
	order by reminder_period_name

	select reorder_f, count(*)
	from Warehouse.sales.dim_order_header_v
	group by reorder_f
	order by reorder_f

	-- MARKETING
	select channel_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by channel_name
	order by channel_name

	select coupon_code, count(*)
	from Warehouse.sales.dim_order_header_v
	group by coupon_code
	order by coupon_code

	-- CUSTOMER ACTIVITY
	select customer_status_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by customer_status_name
	order by customer_status_name

	select customer_order_seq_no, count(*)
	from Warehouse.sales.dim_order_header_v
	group by customer_order_seq_no
	order by customer_order_seq_no

	select rank_seq_no, count(*)
	from Warehouse.sales.dim_order_header_v
	group by rank_seq_no
	order by rank_seq_no

	select customer_order_seq_no_web, count(*)
	from Warehouse.sales.dim_order_header_v
	group by customer_order_seq_no_web
	order by customer_order_seq_no_web

	select rank_seq_no_web, count(*)
	from Warehouse.sales.dim_order_header_v
	group by rank_seq_no_web
	order by rank_seq_no_web
	
	-- QTY - PACK
	select total_qty_unit, count(*)
	from Warehouse.sales.dim_order_header_v
	group by total_qty_unit
	order by total_qty_unit

	select total_qty_pack, count(*)
	from Warehouse.sales.dim_order_header_v
	group by total_qty_pack
	order by total_qty_pack

	-- MEASURES

	select order_id_bk, invoice_id, shipment_id, creditmemo_id, 
		store_name, order_date,
		order_status_name, adjustment_order, 

		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, -- oh.local_total_exc_vat, oh.local_total_vat, oh.local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, -- local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

		local_to_global_rate, order_currency_code
	from Warehouse.sales.dim_order_header_v
	where  
		-- order_status_name = 'CANCEL'
		-- local_total_refund <> 0
		local_total_aft_refund_inc_vat < 0


-- FACT ORDER LINE 
select top 1000 * 
from Warehouse.sales.fact_order_line_v

select top 1000 
	order_id_bk, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
 	order_date, order_date_c, order_week_day, order_time_name, order_day_part,
	invoice_date, invoice_date_c, invoice_week_day, 
	shipment_date, shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part,
	refund_date, refund_date_c, refund_week_day, 

	company_name, store_name, website_type, website_group, website, code_tld, market_name, 
	customer_id, customer_email, 
	country_code_ship, country_name_ship, postcode_shipping, 
	country_code_bill, country_name_bill, postcode_billing, 
	order_stage_name, order_status_name, line_status_name, adjustment_order, order_type_name, order_status_magento_name, 
	payment_method_name, cc_type_name, shipping_carrier_name, shipping_method_name, telesales_username, 
	reminder_type_name, reminder_period_name, reminder_date, reorder_f, reorder_date, 
	channel_name, coupon_code,
	customer_status_name, rank_seq_no, customer_order_seq_no, rank_seq_no_web, customer_order_seq_no_web, order_source,

	manufacturer_name, 
	product_type_name, category_name, cl_type_name, cl_feature_name,
	product_id_magento, product_family_code, product_family_name, 

	base_curve, diameter, power, cylinder, axis, addition, dominance, colour, 
	glass_vision_type_name, glass_package_type_name, 
	sku_magento, sku_erp, 
	aura_product_f,

	qty_unit, qty_unit_refunded, qty_pack, qty_time, -- weight, 

	price_type_name, discount_f,
	local_price_unit, local_price_pack, local_price_pack_discount, 
	global_price_unit, global_price_pack, global_price_pack_discount, 

	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, -- local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, -- local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	-- local_product_cost, local_shipping_cost, local_total_cost, local_margin, 

	global_subtotal, global_shipping, global_discount, global_store_credit_used, 
	global_total_inc_vat, -- global_total_exc_vat, global_total_vat, global_total_prof_fee, 
	global_store_credit_given, global_bank_online_given, 
	global_subtotal_refund, global_shipping_refund, global_discount_refund, global_store_credit_used_refund, global_adjustment_refund, 
	global_total_aft_refund_inc_vat, -- global_total_aft_refund_exc_vat, global_total_aft_refund_vat, global_total_aft_refund_prof_fee, 
	-- global_product_cost, global_shipping_cost, global_total_cost, global_margin, 

	local_total_inc_vat_vf, -- local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf,

	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	num_erp_allocation_lines 
from Warehouse.sales.fact_order_line_v


	-- ORDER DATE - TIME
	select order_date_c, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_date_c
	order by order_date_c

	select order_week_day, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_week_day
	order by order_week_day

	select order_time_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_time_name
	order by order_time_name

	-- INVOICE DATE - TIME
	select invoice_date_c, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by invoice_date_c
	order by invoice_date_c

	select invoice_week_day, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by invoice_week_day
	order by invoice_week_day

	-- SHIPMENT DATE - TIME
	select shipment_date_c, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by shipment_date_c
	order by shipment_date_c

	select shipment_week_day, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by shipment_week_day
	order by shipment_week_day

	select shipment_time_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by shipment_time_name
	order by shipment_time_name

	-- REFUND DATE - TIME
	select refund_date_c, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by refund_date_c
	order by refund_date_c

	select refund_week_day, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by refund_week_day
	order by refund_week_day

	-- COMPANY - STORE
	select company_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by company_name
	order by company_name

	select store_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by store_name
	order by store_name

	select website, count(*)
	from Warehouse.sales.dim_order_header_v
	group by website
	order by website

	select market_name, count(*)
	from Warehouse.sales.dim_order_header_v
	group by market_name
	order by market_name

	-- COUNTRY
	select country_code_ship, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by country_code_ship
	order by country_code_ship

	select country_code_bill, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by country_code_bill
	order by country_code_bill

	-- ORDER 
	select order_stage_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_stage_name
	order by order_stage_name

	select order_status_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_status_name
	order by order_status_name

	select line_status_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by line_status_name
	order by line_status_name
	
	select adjustment_order, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by adjustment_order
	order by adjustment_order

	select order_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_type_name
	order by order_type_name

	select order_status_magento_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by order_status_magento_name
	order by order_status_magento_name

	-- PAYMENT
	select payment_method_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by payment_method_name
	order by payment_method_name

	select cc_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by cc_type_name
	order by cc_type_name

	-- SHIPPING
	select shipping_carrier_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by shipping_carrier_name
	order by shipping_carrier_name

	select shipping_method_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by shipping_method_name
	order by shipping_method_name

	-- OTHER
	select telesales_username, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by telesales_username
	order by telesales_username

	select price_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by price_type_name
	order by price_type_name

	select discount_f, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by discount_f
	order by discount_f

	-- REMINDER - REORDER
	select reminder_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by reminder_type_name
	order by reminder_type_name

	select reminder_period_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by reminder_period_name
	order by reminder_period_name

	select reorder_f, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by reorder_f
	order by reorder_f

	-- MARKETING
	select channel_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by channel_name
	order by channel_name

	select coupon_code, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by coupon_code
	order by coupon_code

	-- CUSTOMER ACTIVITY
	select customer_status_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by customer_status_name
	order by customer_status_name

	select customer_order_seq_no, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by customer_order_seq_no
	order by customer_order_seq_no

	select rank_seq_no, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by rank_seq_no
	order by rank_seq_no
	
	select customer_order_seq_no_web, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by customer_order_seq_no_web
	order by customer_order_seq_no_web

	select rank_seq_no_web, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by rank_seq_no_web
	order by rank_seq_no_web

	-- PRODUCT
	select manufacturer_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by manufacturer_name
	order by manufacturer_name

	select product_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by product_type_name
	order by product_type_name

	select product_type_name, category_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by product_type_name, category_name
	order by product_type_name, category_name

	select cl_type_name, cl_feature_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by cl_type_name, cl_feature_name
	order by cl_type_name, cl_feature_name

	select product_id_magento, product_family_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by product_id_magento, product_family_name
	order by product_id_magento, product_family_name

	
	select glass_vision_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by glass_vision_type_name
	order by glass_vision_type_name

	select glass_package_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by glass_package_type_name
	order by glass_package_type_name

	-- PRODUCT PARAMETERS
	select base_curve, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name <> 'Contact Lenses'
	group by base_curve
	order by base_curve

	select diameter, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by diameter
	order by diameter

	select power, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by power
	order by power

	select cylinder, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by cylinder
	order by cylinder

	select axis, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by axis
	order by axis

	select addition, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by addition
	order by addition

	select dominance, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by dominance
	order by dominance


	select colour, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	where product_type_name = 'Contact Lenses'
	group by colour
	order by colour


	-- QTY - PACK
	select qty_unit, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by qty_unit
	order by qty_unit

	select qty_pack, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by qty_pack
	order by qty_pack

	select qty_time, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by qty_time
	order by qty_time

	-- PRICE
	select price_type_name, count(*), count(distinct order_id_bk)
	from Warehouse.sales.fact_order_line_v
	group by price_type_name
	order by price_type_name