select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts
from Landing.mag.core_store

select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.mag.core_store_aud

select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts, 
	aud_type, aud_dateFrom, aud_dateTo
from Landing.mag.core_store_aud_hist


select store_id_bk, store_name, website_type, website_group, website, tld, code_tld, 
	idETLBatchRun, ins_ts
from Staging.gen.dim_store
order by website_type, website_group, website



select idStore_sk,
	store_id_bk, store_name, website_type, website_group, website, tld, code_tld, 
	idETLBatchRun_ins, ins_ts, idETLBatchRun_upd, upd_ts
from Warehouse.gen.dim_store

select store_id_bk, store_name, website_type, website_group, website, tld, code_tld, 
	idETLBatchRun, ins_ts
from Warehouse.gen.dim_store_wrk

----

-- migra_pr.dw_stores_full
select store_id, store_name, website_group, store_type, visible, 
	idETLBatchRun, ins_ts
from Landing.migra_pr.dw_stores_full
order by website_group, store_id

----
select store_name, tld, store_type, 
	idETLBatchRun, ins_ts
from Landing.map.gen_store_type

select store_name, tld, store_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_store_type_aud


select store_name, name, website_group, 
	idETLBatchRun, ins_ts
from Landing.map.gen_website_group

select store_name, name, website_group, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_website_group_aud

