
CREATE LOGIN reportserver_user WITH PASSWORD = 'reportserver_user';

CREATE USER reportserver_user FOR LOGIN reportserver_user;

ALTER SERVER ROLE sysadmin ADD MEMBER reportserver_user
GO
