
use Staging
go

drop table Staging.alloc.dim_order_type_erp
go

drop table Staging.alloc.dim_order_status_erp
go

drop table Staging.alloc.dim_shipment_status_erp
go

drop table Staging.alloc.dim_allocation_status
go

drop table Staging.alloc.dim_allocation_strategy
go

drop table Staging.alloc.dim_allocation_type
go

drop table Staging.alloc.dim_allocation_record_type
go



