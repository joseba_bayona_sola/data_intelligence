use Staging
go

select order_type_erp_bk, order_type_erp_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_order_type_erp

select order_status_erp_bk, order_status_erp_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_order_status_erp

select shipment_status_erp_bk, shipment_status_erp_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_shipment_status_erp

select allocation_status_bk, allocation_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_allocation_status

select allocation_strategy_bk, allocation_strategy_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_allocation_strategy

select allocation_type_bk, allocation_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_allocation_type

select allocation_record_type_bk, allocation_record_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.alloc.dim_allocation_record_type


