use Staging
go 


select time_name_bk, time_name, hour_name, hour, minute, day_part,
	idETLBatchRun, ins_ts
from Staging.gen.dim_time




select company_name_bk, company_name,
	idETLBatchRun, ins_ts
from Staging.gen.dim_company

select store_id_bk, store_name, website_type, website_group, website, tld, code_tld, acquired, company_name_bk,
	idETLBatchRun, ins_ts
from Staging.gen.dim_store
order by website_type, website_group, website



select country_id_bk, country_code, country_name,
	country_zone, country_type, country_continent, country_state,
	idETLBatchRun, ins_ts
from Staging.gen.dim_country

select region_id_bk, region_name, country_id_bk, 
	idETLBatchRun, ins_ts
from Staging.gen.dim_region


select market_id_bk, market_name, 
	idETLBatchRun, ins_ts
from Staging.gen.dim_market


select customer_type_name_bk, customer_type_name, description,
	idETLBatchRun, ins_ts
from Staging.gen.dim_customer_type

select customer_status_name_bk, customer_status_name, description,
	idETLBatchRun, ins_ts
from Staging.gen.dim_customer_status


select customer_unsubscribe_name_bk, customer_unsubscribe_name, description,
	idETLBatchRun, ins_ts
from Staging.gen.dim_customer_unsubscribe


select customer_origin_name_bk, customer_origin_name, description,
	idETLBatchRun, ins_ts
from Staging.gen.dim_customer_origin


select customer_id_bk, customer_email,
	created_at, updated_at,
	store_id_bk, market_id_bk, customer_type_name_bk, customer_status_name_bk,
	prefix, first_name, last_name, 
	dob, gender, language, 
	phone_number, street, city, postcode, region, region_id_bk, country_id_bk, 
	postcode_s, country_id_s_bk, 
	customer_unsubscribe_name_bk,
	unsubscribe_mark_email_magento_f, unsubscribe_mark_email_magento_d, unsubscribe_mark_email_dotmailer_f, unsubscribe_mark_email_dotmailer_d, 
	unsubscribe_text_message_f, unsubscribe_text_message_d, 
	reminder_f, reminder_type_name_bk, reminder_period_bk, 
	reorder_f, 
	referafriend_code, days_worn_info, 
	migrate_customer_f, migrate_customer_store, migrate_customer_id, 
	idETLBatchRun, ins_ts
from Staging.gen.dim_customer



select supplier_type_bk, supplier_type_name,
	idETLBatchRun, ins_ts
from Staging.gen.dim_supplier_type


select supplier_id_bk, supplier_type_bk, supplier_name, default_lead_time, default_transit_lead_time,
	idETLBatchRun, ins_ts
from Staging.gen.dim_supplier


select warehouseid_bk, warehouse_name, wh_short_name,
	idETLBatchRun, ins_ts
from Staging.gen.dim_warehouse
