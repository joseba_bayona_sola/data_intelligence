use Staging
go 


select product_type_bk, product_type_name, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_type

select category_bk, category_name, product_type_bk,
	idETLBatchRun, ins_ts
from Staging.prod.dim_category


select cl_type_bk, cl_type_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_cl_type

select cl_feature_bk, cl_feature_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_cl_feature


select manufacturer_bk, manufacturer_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_manufacturer



select product_lifecycle_bk, product_lifecycle_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_lifecycle

select visibility_id_bk, product_visibility_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_visibility


select glass_vision_type_bk, glass_vision_type_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_glass_vision_type

select glass_package_type_bk, glass_package_type_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_glass_package_type



select base_curve_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_bc

select diameter_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_di

select power_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_po


select cylinder_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_cy

select axis_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_ax


select addition_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_ad

select dominance_bk, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_do


select colour_bk, colour_name,
	idETLBatchRun, ins_ts
from Staging.prod.dim_param_col




select product_id_bk, 
	manufacturer_bk, category_bk, cl_type_bk, cl_feature_bk, product_lifecycle_bk, visibility_id_bk, product_family_group_bk, product_type_oh_bk,
	magento_sku, product_family_code, product_family_name, 
	glass_sunglass_name, glass_sunglass_colour, 
	status, promotional_product, telesales_product,
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_family



select product_type_oh_bk, product_type_oh_name, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_type_oh



select product_family_group_bk, product_family_group_name, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_family_group



select packsizeid_bk, 
	product_id_bk, size, product_family_packsize_name, 
	allocation_preference_order, purchasing_preference_order, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_family_pack_size


select price_type_pf_bk, price_type_pf_name, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_price_type_pf


select supplierpriceid_bk, 
	supplier_id_bk, packsizeid_bk, price_type_pf_bk, 
	unit_price, currency_code, lead_time, 
	effective_date, expiry_date, active,
	idETLBatchRun, ins_ts
from Staging.prod.dim_product_family_price


select productid_erp_bk, 
	product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, 
	parameter, product_description, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_product

select stockitemid_bk, 
	productid_erp_bk, packsize, 
	SKU, stock_item_description, 
	idETLBatchRun, ins_ts
from Staging.prod.dim_stock_item
