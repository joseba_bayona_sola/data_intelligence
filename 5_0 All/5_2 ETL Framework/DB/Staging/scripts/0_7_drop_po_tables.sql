
use Staging
go

drop table Staging.po.dim_po_source
go

drop table Staging.po.dim_po_type
go

drop table Staging.po.dim_po_status
go

drop table Staging.po.dim_pol_status
go

drop table Staging.po.dim_pol_problems
go

drop table Staging.po.dim_wh_operator
go


drop table Staging.po.dim_purchase_order
go

drop table Staging.po.fact_purchase_order_line
go



