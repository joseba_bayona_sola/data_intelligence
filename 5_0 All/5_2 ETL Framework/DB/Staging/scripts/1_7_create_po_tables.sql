
use Staging
go

create table Staging.po.dim_po_source(
	po_source_bk					varchar(50) NOT NULL,
	po_source_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_po_source add constraint [PK_po_dim_po_source]
	primary key clustered (po_source_bk);
go

alter table Staging.po.dim_po_source add constraint [DF_po_dim_po_source_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.po.dim_po_type(
	po_type_bk						varchar(50) NOT NULL,
	po_type_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_po_type add constraint [PK_po_dim_po_type]
	primary key clustered (po_type_bk);
go

alter table Staging.po.dim_po_type add constraint [DF_po_dim_po_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.po.dim_po_status(
	po_status_bk					varchar(50) NOT NULL,
	po_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_po_status add constraint [PK_po_dim_po_status]
	primary key clustered (po_status_bk);
go

alter table Staging.po.dim_po_status add constraint [DF_po_dim_po_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.po.dim_pol_status(
	pol_status_bk					varchar(50) NOT NULL,
	pol_status_name					varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_pol_status add constraint [PK_po_dim_pol_status]
	primary key clustered (pol_status_bk);
go

alter table Staging.po.dim_pol_status add constraint [DF_po_dim_pol_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.po.dim_pol_problems(
	pol_problems_bk					varchar(50) NOT NULL,
	pol_problems_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_pol_problems add constraint [PK_po_dim_pol_problems]
	primary key clustered (pol_problems_bk);
go

alter table Staging.po.dim_pol_problems add constraint [DF_po_dim_pol_problems_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.po.dim_wh_operator(
	wh_operator_bk					varchar(200) NOT NULL,
	wh_operator_name				varchar(200) NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_wh_operator add constraint [PK_po_dim_wh_operator]
	primary key clustered (wh_operator_bk);
go

alter table Staging.po.dim_wh_operator add constraint [DF_po_dim_wh_operator_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.po.dim_purchase_order(
	purchaseorderid_bk				bigint NOT NULL,

	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	po_source_bk					varchar(50), 
	po_type_bk						varchar(50),
	po_status_bk					varchar(50),
	wh_operator_cr_bk				varchar(200), 
	wh_operator_as_bk				varchar(200), 

	purchase_order_number			varchar(20), 
	leadtime						int, 

	created_date					datetime,
	approved_date					datetime,
	confirmed_date					datetime,

	submitted_date					datetime,
	completed_date					datetime,
	due_date						datetime,

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.dim_purchase_order add constraint [PK_po_dim_purchase_order]
	primary key clustered (purchaseorderid_bk);
go

alter table Staging.po.dim_purchase_order add constraint [DF_po_dim_purchase_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.po.fact_purchase_order_line(
	purchaseorderlineid_bk			bigint NOT NULL,

	purchaseorderid_bk				bigint NOT NULL,
	pol_status_bk					varchar(50),
	pol_problems_bk					varchar(50), 
	stockitemid_bk					bigint,
	supplierpriceid_bk				bigint,

	purchase_order_line_id			bigint,

	quantity_ordered				decimal(28, 8), 
	quantity_received				decimal(28, 8), 
	quantity_planned				decimal(28, 8), 
	quantity_open					decimal(28, 8), 
	quantity_cancelled				decimal(28, 8), 

	local_unit_cost					decimal(28, 8),
	local_line_cost					decimal(28, 8),

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.po.fact_purchase_order_line add constraint [PK_po_fact_purchase_order_line]
	primary key clustered (purchaseorderlineid_bk);
go

alter table Staging.po.fact_purchase_order_line add constraint [DF_po_fact_purchase_order_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

