
use Staging
go

select wh_shipment_type_bk, wh_shipment_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.rec.dim_wh_shipment_type

select receipt_status_bk, receipt_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.rec.dim_receipt_status

select receipt_line_status_bk, receipt_line_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.rec.dim_receipt_line_status

select receipt_line_sync_status_bk, receipt_line_sync_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.rec.dim_receipt_line_sync_status


select shipment_id_bk, 
	warehouseid_bk, supplier_id_bk, wh_shipment_type_bk, 
	shipment_number,
	idETLBatchRun, ins_ts
from Staging.rec.dim_wh_shipment

select receiptid_bk, 
	shipment_id_bk, receipt_status_bk, 
	receipt_number, receipt_no, 
	created_date, arrived_date, confirmed_date, stock_registered_date, 
	local_total_cost, 
	local_to_global_rate, currency_code,
	idETLBatchRun, ins_ts
from Staging.rec.dim_receipt

select receiptlineid_bk, 
	receiptid_bk, receipt_line_status_bk, receipt_line_sync_status_bk, 
	purchaseorderlineid_bk, stockitemid_bk, 
	created_date, 
	qty_ordered, qty_received, qty_accepted, qty_returned, qty_rejected, 
	local_unit_cost, local_total_cost, 
	local_to_global_rate, currency_code,
	wh_stock_item_batch_f,
	idETLBatchRun, ins_ts
from Staging.rec.fact_receipt_line
