use Staging
go

select reminder_type_name_bk, reminder_type_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_reminder_type

select reminder_period_bk, reminder_period_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_reminder_period



select order_stage_name_bk, order_stage_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_stage

select order_status_name_bk, order_status_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_status

select order_type_name_bk, order_type_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_type

select status_bk, order_status_magento_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_status_magento



select payment_method_name_bk, payment_method_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_payment_method

select cc_type_name_bk, cc_type_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_cc_type


select shipping_carrier_name_bk, shipping_carrier_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_shipping_carrier

select shipping_description_bk, shipping_method_name, shipping_carrier_name_bk, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_shipping_method


select telesales_admin_username_bk, telesales_username, user_id, telesales_first_name, telesales_last_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_telesale


select price_type_name_bk, price_type_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_price_type



select prescription_method_name_bk, prescription_method_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_prescription_method



select channel_name_bk, channel_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_channel

select marketing_channel_name_bk, marketing_channel_name, sup_marketing_channel_name, description,
	idETLBatchRun, ins_ts
from Staging.sales.dim_marketing_channel

select publisher_id_bk, affiliate_name, affiliate_website, affiliate_network_name, affiliate_type_name, affiliate_status_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_affiliate

select channel_bk, group_coupon_code_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_group_coupon_code

select coupon_id_bk, channel_bk, rule_id, coupon_code, coupon_code_name, 
	from_date, to_date, expiration_date, discount_type_name, discount_amount, only_new_customer, refer_a_friend,
	idETLBatchRun, ins_ts
from Staging.sales.dim_coupon_code


select device_category_name_bk, device_category_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_device_category

select device_brand_name_bk, device_category_name_bk, device_brand_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_device_brand

select device_model_name_bk, device_brand_name_bk, device_model_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_device_model

select device_browser_name_bk, device_browser_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_device_browser

select device_os_name_bk, device_os_name,
	idETLBatchRun, ins_ts
from Staging.sales.dim_device_os


select customer_order_seq_no_bk, rank_seq_no,
	idETLBatchRun, ins_ts
from Staging.sales.dim_rank_customer_order_seq_no

select qty_time_bk, rank_qty_time,
	idETLBatchRun, ins_ts
from Staging.sales.dim_rank_qty_time

select freq_time_bk, rank_freq_time,
	idETLBatchRun, ins_ts
from Staging.sales.dim_rank_freq_time

select shipping_days_bk, rank_shipping_days,
	idETLBatchRun, ins_ts
from Staging.sales.dim_rank_shipping_days



select order_id_bk, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarOrderDate, idTimeOrderDate, order_date, 
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date, 
	store_id_bk, market_id_bk, customer_id_bk, 
	country_id_shipping_bk, postcode_shipping, country_id_billing_bk, postcode_billing, 
	customer_unsubscribe_name_bk,
	order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
	payment_method_name_bk, cc_type_name_bk, shipping_description_bk, telesales_admin_username_bk, price_type_name_bk, discount_f, prescription_method_name_bk,
	reminder_type_name_bk, reminder_period_bk, reminder_date, reorder_f, reorder_date, 
	channel_name_bk, marketing_channel_name_bk, publisher_id_bk, coupon_id_bk, device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
	customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, order_source, proforma, 
	product_type_oh_bk, order_qty_time, num_diff_product_type_oh, aura_product_p,
	countries_registered_code,
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_header

select order_id_bk, product_type_oh_bk, 
	order_percentage, order_flag, order_qty_time,
	idETLBatchRun, ins_ts
from Staging.sales.dim_order_header_product_type

select order_line_id_bk, 
	order_id, invoice_id, shipment_id, creditmemo_id, 
	order_no, invoice_no, shipment_no, creditmemo_no, 
	idCalendarInvoiceDate, invoice_date, 
	idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
	idCalendarRefundDate, refund_date, 
	warehouse_id_bk, order_id_bk, order_status_name_bk, price_type_name_bk, discount_f,
	product_id_bk, 
	base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
	sku_magento, sku_erp, 
	glass_vision_type_bk, glass_vision_type_bk, 
	aura_product_f,
	countries_registered_code, product_type_vat,

	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund,
	num_erp_allocation_lines,

	idETLBatchRun, ins_ts
from Staging.sales.fact_order_line




select 
	order_line_id_bk, order_id, order_no, order_date, 
	countries_registered_code, order_currency_code, exchange_rate,
	local_total_inc_vat_vf, local_total_exc_vat_vf, local_total_vat_vf, local_total_prof_fee_vf, 
	local_store_credit_given_vf, local_bank_online_given_vf, 
	local_subtotal_refund_vf, local_shipping_refund_vf, local_discount_refund_vf, local_store_credit_used_refund_vf, local_adjustment_refund_vf, 
	idETLBatchRun, ins_ts
from Staging.sales.fact_order_line_vat_fix



select mutual_id_bk, mutual_code, mutual_name, 
	idETLBatchRun, ins_ts 
from Staging.sales.dim_mutual


select type_id_bk, mutual_quotation_type_name, 
	idETLBatchRun, ins_ts
from Staging.sales.dim_mutual_quotation_type


select quotation_id_bk, mutual_id_bk, type_id_bk, order_id_bk, 
	mutual_amount, quotation_date, error_id,
	idETLBatchRun, ins_ts
from Staging.sales.fact_mutual_quotation