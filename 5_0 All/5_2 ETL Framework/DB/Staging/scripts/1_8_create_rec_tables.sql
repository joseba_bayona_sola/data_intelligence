
use Staging
go

create table Staging.rec.dim_wh_shipment_type(
	wh_shipment_type_bk				varchar(50) NOT NULL,
	wh_shipment_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_wh_shipment_type add constraint [PK_rec_dim_wh_shipment_type]
	primary key clustered (wh_shipment_type_bk);
go

alter table Staging.rec.dim_wh_shipment_type add constraint [DF_rec_dim_wh_shipment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.rec.dim_receipt_status(
	receipt_status_bk				varchar(50) NOT NULL,
	receipt_status_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_receipt_status add constraint [PK_rec_dim_receipt_status]
	primary key clustered (receipt_status_bk);
go

alter table Staging.rec.dim_receipt_status add constraint [DF_rec_dim_receipt_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.rec.dim_receipt_line_status(
	receipt_line_status_bk			varchar(50) NOT NULL,
	receipt_line_status_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_receipt_line_status add constraint [PK_rec_dim_receipt_line_status]
	primary key clustered (receipt_line_status_bk);
go

alter table Staging.rec.dim_receipt_line_status add constraint [DF_rec_dim_receipt_line_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.rec.dim_receipt_line_sync_status(
	receipt_line_sync_status_bk		varchar(50) NOT NULL,
	receipt_line_sync_status_name	varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_receipt_line_sync_status add constraint [PK_rec_dim_receipt_line_sync_status]
	primary key clustered (receipt_line_sync_status_bk);
go

alter table Staging.rec.dim_receipt_line_sync_status add constraint [DF_rec_dim_receipt_line_sync_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.rec.dim_wh_shipment(
	shipment_id_bk					bigint NOT Null, 

	warehouseid_bk					bigint NOT NULL, 
	supplier_id_bk					bigint NOT NULL, 
	wh_shipment_type_bk				varchar(50), 

	shipment_number					varchar(20), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_wh_shipment add constraint [PK_rec_dim_wh_shipment]
	primary key clustered (shipment_id_bk);
go

alter table Staging.rec.dim_wh_shipment add constraint [DF_rec_dim_wh_shipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.rec.dim_receipt(
	receiptid_bk					bigint NOT NULL, 

	shipment_id_bk					bigint NOT Null, 
	receipt_status_bk				varchar(50), 

	receipt_number					varchar(20), 
	receipt_no						int, 

	created_date					datetime, 
	arrived_date					datetime,
	confirmed_date					datetime, 
	stock_registered_date			datetime, 

	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.dim_receipt add constraint [PK_rec_dim_receipt]
	primary key clustered (receiptid_bk);
go

alter table Staging.rec.dim_receipt add constraint [DF_rec_dim_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.rec.fact_receipt_line(
	receiptlineid_bk				bigint NOT NULL, 

	receiptid_bk					bigint NOT NULL,
	receipt_line_status_bk			varchar(50), 
	receipt_line_sync_status_bk		varchar(50), 
	purchaseorderlineid_bk			bigint, 
	stockitemid_bk					bigint, 

	created_date					datetime, 

	qty_ordered						decimal(28, 8), 
	qty_received					decimal(28, 8), 
	qty_accepted					decimal(28, 8), 
	qty_returned					decimal(28, 8), 
	qty_rejected					decimal(28, 8), 

	local_unit_cost					decimal(28, 8), 
	local_total_cost				decimal(28, 8), 

	local_to_global_rate			decimal(12, 4), 
	currency_code					varchar(10),

	wh_stock_item_batch_f			char(1),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.rec.fact_receipt_line add constraint [PK_rec_fact_receipt_line]
	primary key clustered (receiptlineid_bk);
go

alter table Staging.rec.fact_receipt_line add constraint [DF_rec_fact_receipt_line_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
