use Staging
go

drop procedure rec.stg_dwh_get_rec_wh_shipment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Shipment Type in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_wh_shipment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select wh_shipment_type_bk, wh_shipment_type_name, description, @idETLBatchRun
	from Staging.rec.dim_wh_shipment_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure rec.stg_dwh_get_rec_receipt_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Receipt Status in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_receipt_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select receipt_status_bk, receipt_status_name, description, @idETLBatchRun
	from Staging.rec.dim_receipt_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure rec.stg_dwh_get_rec_receipt_line_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Receipt Line Status in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_receipt_line_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select receipt_line_status_bk, receipt_line_status_name, description, @idETLBatchRun
	from Staging.rec.dim_receipt_line_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure rec.stg_dwh_get_rec_receipt_line_sync_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Receipt Line Sync Status in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_receipt_line_sync_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select receipt_line_sync_status_bk, receipt_line_sync_status_name, description, @idETLBatchRun
	from Staging.rec.dim_receipt_line_sync_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure rec.stg_dwh_get_rec_wh_shipment
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Shipment in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_wh_shipment
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select shipment_id_bk, 
		warehouseid_bk, supplier_id_bk, wh_shipment_type_bk, 
		shipment_number, 
		@idETLBatchRun
	from Staging.rec.dim_wh_shipment

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure rec.stg_dwh_get_rec_receipt
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select receiptid_bk, 
		shipment_id_bk, receipt_status_bk, 
		receipt_number, receipt_no, 
		created_date, arrived_date, confirmed_date, stock_registered_date, 
		local_total_cost, 
		local_to_global_rate, currency_code, 
		@idETLBatchRun
	from Staging.rec.dim_receipt

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure rec.stg_dwh_get_rec_receipt_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt Line in Staging Table 
-- ==========================================================================================

create procedure rec.stg_dwh_get_rec_receipt_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select receiptlineid_bk, 
		receiptid_bk, receipt_line_status_bk, receipt_line_sync_status_bk, 
		purchaseorderlineid_bk, stockitemid_bk, 
		created_date, 
		qty_ordered, qty_received, qty_accepted, qty_returned, qty_rejected, 
		local_unit_cost, local_total_cost, 
		local_to_global_rate, currency_code,
		wh_stock_item_batch_f, 
		@idETLBatchRun
	from Staging.rec.fact_receipt_line

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go