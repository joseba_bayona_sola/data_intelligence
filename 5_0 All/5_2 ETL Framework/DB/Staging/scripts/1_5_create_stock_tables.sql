use Staging
go

create table Staging.stock.dim_stock_method_type(
	stock_method_type_bk			varchar(50) NOT NULL,
	stock_method_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_stock_method_type add constraint [PK_stock_dim_stock_method_type]
	primary key clustered (stock_method_type_bk);
go

alter table Staging.stock.dim_stock_method_type add constraint [DF_stock_dim_stock_method_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.stock.dim_stock_batch_type(
	stock_batch_type_bk				varchar(50) NOT NULL,
	stock_batch_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_stock_batch_type add constraint [PK_stock_dim_stock_batch_type]
	primary key clustered (stock_batch_type_bk);
go

alter table Staging.stock.dim_stock_batch_type add constraint [DF_stock_dim_stock_batch_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.stock.dim_stock_adjustment_type(
	stock_adjustment_type_bk		varchar(50) NOT NULL,
	stock_adjustment_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_stock_adjustment_type add constraint [PK_stock_dim_stock_adjustment_type]
	primary key clustered (stock_adjustment_type_bk);
go

alter table Staging.stock.dim_stock_adjustment_type add constraint [DF_stock_dim_stock_adjustment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.stock.dim_stock_movement_type(
	stock_movement_type_bk			varchar(50) NOT NULL,
	stock_movement_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_stock_movement_type add constraint [PK_stock_dim_stock_movement_type]
	primary key clustered (stock_movement_type_bk);
go

alter table Staging.stock.dim_stock_movement_type add constraint [DF_stock_dim_stock_movement_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.stock.dim_stock_movement_period(
	stock_movement_period_bk		varchar(50) NOT NULL,
	stock_movement_period_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	period_start					date NOT NULL, 
	period_finish					date NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_stock_movement_period add constraint [PK_stock_dim_stock_movement_period]
	primary key clustered (stock_movement_period_bk);
go

alter table Staging.stock.dim_stock_movement_period add constraint [DF_stock_dim_stock_movement_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.stock.dim_wh_stock_item(
	warehousestockitemid_bk			bigint NOT NULL, 

	warehouseid_bk					bigint NOT NULL, 
	stockitemid_bk					bigint NOT NULL, 
	stock_method_type_bk			varchar(50) NOT NULL, 

	wh_stock_item_description		varchar(200),

	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 
	qty_due_in						decimal(28, 8), 

	stocked							int,

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_wh_stock_item add constraint [PK_stock_dim_wh_stock_item]
	primary key clustered (warehousestockitemid_bk);
go

alter table Staging.stock.dim_wh_stock_item add constraint [DF_stock_dim_wh_stock_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.stock.dim_wh_stock_item_batch(
	warehousestockitembatchid_bk	bigint NOT NULL, 

	warehousestockitemid_bk			bigint NOT NULL,
	stock_batch_type_bk				varchar(50) NOT NULL, 

	batch_id						bigint,

	fully_allocated_f				int, 
	fully_issued_f					int, 
	auto_adjusted_f					int, 
	
	qty_received					decimal(28, 8), 
	qty_available					decimal(28, 8), 
	qty_outstanding_allocation		decimal(28, 8), 
	qty_allocated_stock				decimal(28, 8), 
	qty_issued_stock				decimal(28, 8), 
	qty_on_hold						decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	qty_registered_sm				decimal(28, 8), 
	qty_disposed_sm					decimal(28, 8), 

	batch_arrived_date				datetime,
	batch_confirmed_date			datetime,
	batch_stock_register_date		datetime,

	local_product_unit_cost			decimal(28, 8), 
	local_carriage_unit_cost		decimal(28, 8), 
	local_duty_unit_cost			decimal(28, 8), 
	local_total_unit_cost			decimal(28, 8), 

	local_interco_carriage_unit_cost	decimal(28, 8), 
	local_total_unit_cost_interco		decimal(28, 8), 

	local_to_global_rate			decimal(12, 4),
	currency_code					varchar(10),

	delete_f						char(1), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.dim_wh_stock_item_batch add constraint [PK_stock_dim_wh_stock_item_batch]
	primary key clustered (warehousestockitembatchid_bk);
go

alter table Staging.stock.dim_wh_stock_item_batch add constraint [DF_stock_dim_wh_stock_item_batch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.stock.fact_wh_stock_item_batch_movement(
	batchstockmovementid_bk			bigint NOT NULL, 

	batch_stock_move_id				bigint NOT NULL, 
	move_id							varchar(20) NOT NULL,
	move_line_id					varchar(20) NOT NULL,

	warehousestockitembatchid_bk	bigint NOT NULL,
	stock_adjustment_type_bk		varchar(50) NOT NULL, 
	stock_movement_type_bk			varchar(50) NOT NULL, 
	stock_movement_period_bk		varchar(50), 

	qty_movement					decimal(28, 8), 
	qty_registered					decimal(28, 8), 
	qty_disposed					decimal(28, 8), 

	batch_movement_date				datetime,

	delete_f						char(1), 

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.stock.fact_wh_stock_item_batch_movement add constraint [PK_stock_fact_wh_stock_item_batch_movement]
	primary key clustered (batchstockmovementid_bk);
go

alter table Staging.stock.fact_wh_stock_item_batch_movement add constraint [DF_stock_fact_wh_stock_item_batch_movement_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

