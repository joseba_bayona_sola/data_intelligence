use Staging
go


select stock_method_type_bk, stock_method_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.stock.dim_stock_method_type

select stock_batch_type_bk, stock_batch_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.stock.dim_stock_batch_type

select stock_adjustment_type_bk, stock_adjustment_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.stock.dim_stock_adjustment_type

select stock_movement_type_bk, stock_movement_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.stock.dim_stock_movement_type

select stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish,
	idETLBatchRun, ins_ts
from Staging.stock.dim_stock_movement_period


select warehousestockitemid_bk, 
	warehouseid_bk, stockitemid_bk, stock_method_type_bk, 
	wh_stock_item_description, 

	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, qty_due_in,
	stocked, 
	
	idETLBatchRun, ins_ts
from Staging.stock.dim_wh_stock_item


select warehousestockitembatchid_bk, 
	warehousestockitemid_bk, stock_batch_type_bk, 
	batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 
	
	qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
	qty_on_hold, qty_registered, qty_disposed, 
	qty_registered_sm, qty_disposed_sm, 

	batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 
	
	local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
	local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
	local_to_global_rate, currency_code, 
	delete_f,

	idETLBatchRun, ins_ts
from Staging.stock.dim_wh_stock_item_batch


select batchstockmovementid_bk, 
	batch_stock_move_id, move_id, move_line_id, 

	warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 

	qty_movement, qty_registered, qty_disposed, 
	batch_movement_date, 
	delete_f,

	idETLBatchRun, ins_ts
from Staging.stock.fact_wh_stock_item_batch_movement
