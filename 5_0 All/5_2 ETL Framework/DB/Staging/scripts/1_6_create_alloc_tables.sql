
use Staging
go

create table Staging.alloc.dim_order_type_erp(
	order_type_erp_bk				varchar(50) NOT NULL,
	order_type_erp_name				varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_order_type_erp add constraint [PK_alloc_dim_order_type_erp]
	primary key clustered (order_type_erp_bk);
go

alter table Staging.alloc.dim_order_type_erp add constraint [DF_alloc_dim_order_type_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.alloc.dim_order_status_erp(
	order_status_erp_bk				varchar(50) NOT NULL,
	order_status_erp_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_order_status_erp add constraint [PK_alloc_dim_order_status_erp]
	primary key clustered (order_status_erp_bk);
go

alter table Staging.alloc.dim_order_status_erp add constraint [DF_alloc_dim_order_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.alloc.dim_shipment_status_erp(
	shipment_status_erp_bk			varchar(50) NOT NULL,
	shipment_status_erp_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_shipment_status_erp add constraint [PK_alloc_dim_shipment_status_erp]
	primary key clustered (shipment_status_erp_bk);
go

alter table Staging.alloc.dim_shipment_status_erp add constraint [DF_alloc_dim_shipment_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.alloc.dim_allocation_status(
	allocation_status_bk			varchar(50) NOT NULL,
	allocation_status_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_allocation_status add constraint [PK_alloc_dim_allocation_status]
	primary key clustered (allocation_status_bk);
go

alter table Staging.alloc.dim_allocation_status add constraint [DF_alloc_dim_allocation_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.alloc.dim_allocation_strategy(
	allocation_strategy_bk			varchar(50) NOT NULL,
	allocation_strategy_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_allocation_strategy add constraint [PK_alloc_dim_allocation_strategy]
	primary key clustered (allocation_strategy_bk);
go

alter table Staging.alloc.dim_allocation_strategy add constraint [DF_alloc_dim_allocation_strategy_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.alloc.dim_allocation_type(
	allocation_type_bk				varchar(50) NOT NULL,
	allocation_type_name			varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_allocation_type add constraint [PK_alloc_dim_allocation_type]
	primary key clustered (allocation_type_bk);
go

alter table Staging.alloc.dim_allocation_type add constraint [DF_alloc_dim_allocation_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.alloc.dim_allocation_record_type(
	allocation_record_type_bk		varchar(50) NOT NULL,
	allocation_record_type_name		varchar(50) NOT NULL, 
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.alloc.dim_allocation_record_type add constraint [PK_alloc_dim_allocation_record_type]
	primary key clustered (allocation_record_type_name);
go

alter table Staging.alloc.dim_allocation_record_type add constraint [DF_alloc_dim_allocation_record_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


