use Staging
go

create table Staging.prod.dim_product_type(
	product_type_bk		varchar(50) NOT NULL, 
	product_type_name	varchar(50) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.prod.dim_product_type add constraint [PK_prod_dim_product_type]
	primary key clustered (product_type_bk);
go

alter table Staging.prod.dim_product_type add constraint [DF_prod_dim_product_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_category(
	category_bk			varchar(50) NOT NULL, 
	category_name		varchar(50) NOT NULL, 
	product_type_bk		varchar(50) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.prod.dim_category add constraint [PK_prod_dim_category]
	primary key clustered (category_bk);
go

alter table Staging.prod.dim_category add constraint [DF_prod_dim_category_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_cl_type(
	cl_type_bk			varchar(50) NOT NULL, 
	cl_type_name		varchar(50) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.prod.dim_cl_type add constraint [PK_prod_dim_cl_type]
	primary key clustered (cl_type_bk);
go

alter table Staging.prod.dim_cl_type add constraint [DF_prod_dim_cl_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

create table Staging.prod.dim_cl_feature(
	cl_feature_bk		varchar(50) NOT NULL, 
	cl_feature_name		varchar(50) NOT NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.prod.dim_cl_feature add constraint [PK_prod_dim_cl_feature]
	primary key clustered (cl_feature_bk);
go

alter table Staging.prod.dim_cl_feature add constraint [DF_prod_dim_cl_feature_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_manufacturer(
	manufacturer_bk		int NOT NULL, 
	manufacturer_name	varchar(50) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table Staging.prod.dim_manufacturer add constraint [PK_prod_dim_manufacturer]
	primary key clustered (manufacturer_bk);
go

alter table Staging.prod.dim_manufacturer add constraint [DF_prod_dim_manufacturer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_product_lifecycle(
	product_lifecycle_bk	varchar(50) NOT NULL, 
	product_lifecycle_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_product_lifecycle add constraint [PK_prod_dim_product_lifecycle]
	primary key clustered (product_lifecycle_bk);
go

alter table Staging.prod.dim_product_lifecycle add constraint [DF_prod_dim_product_lifecycle_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_product_visibility(
	visibility_id_bk		int NOT NULL, 
	product_visibility_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_product_visibility add constraint [PK_prod_dim_product_visibility]
	primary key clustered (visibility_id_bk);
go

alter table Staging.prod.dim_product_visibility add constraint [DF_prod_dim_product_visibility_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_glass_vision_type(
	glass_vision_type_bk	varchar(50) NOT NULL, 
	glass_vision_type_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_glass_vision_type add constraint [PK_prod_dim_glass_vision_type]
	primary key clustered (glass_vision_type_bk);
go

alter table Staging.prod.dim_glass_vision_type add constraint [DF_prod_dim_glass_vision_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_glass_package_type(
	glass_package_type_bk	varchar(50) NOT NULL, 
	glass_package_type_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_glass_package_type add constraint [PK_prod_dim_glass_package_type]
	primary key clustered (glass_package_type_bk);
go

alter table Staging.prod.dim_glass_package_type add constraint [DF_prod_dim_glass_package_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_param_BC(
	base_curve_bk			char(3) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_BC add constraint [PK_prod_dim_param_BC]
	primary key clustered (base_curve_bk);
go

alter table Staging.prod.dim_param_BC add constraint [DF_prod_dim_param_BC_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_param_DI(
	diameter_bk				char(4) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_DI add constraint [PK_prod_dim_param_DI]
	primary key clustered (diameter_bk);
go

alter table Staging.prod.dim_param_DI add constraint [DF_prod_dim_param_DI_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_param_PO(
	power_bk				char(6) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_PO add constraint [PK_prod_dim_param_PO]
	primary key clustered (power_bk);
go

alter table Staging.prod.dim_param_PO add constraint [DF_prod_dim_param_PO_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_param_CY(
	cylinder_bk				char(5) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_CY add constraint [PK_prod_dim_param_CY]
	primary key clustered (cylinder_bk);
go

alter table Staging.prod.dim_param_CY add constraint [DF_prod_dim_param_CY_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_param_AX(
	axis_bk					char(3) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_AX add constraint [PK_prod_dim_param_AX]
	primary key clustered (axis_bk);
go

alter table Staging.prod.dim_param_AX add constraint [DF_prod_dim_param_AX_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_param_AD(
	addition_bk				char(4) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_AD add constraint [PK_prod_dim_param_AD]
	primary key clustered (addition_bk);
go

alter table Staging.prod.dim_param_AD add constraint [DF_prod_dim_param_AD_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


create table Staging.prod.dim_param_DO(
	dominance_bk			char(1) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_DO add constraint [PK_prod_dim_param_DO]
	primary key clustered (dominance_bk);
go

alter table Staging.prod.dim_param_DO add constraint [DF_prod_dim_param_DO_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_param_COL(
	colour_bk				varchar(50) NOT NULL, 
	colour_name				varchar(50) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_param_COL add constraint [PK_prod_dim_param_COL]
	primary key clustered (colour_bk);
go

alter table Staging.prod.dim_param_COL add constraint [DF_prod_dim_param_COL_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_product_family(
	product_id_bk				int NOT NULL, 
	manufacturer_bk				int, 
	category_bk					varchar(50) NOT NULL,
	cl_type_bk					varchar(50), 
	cl_feature_bk				varchar(50), 
	product_lifecycle_bk		varchar(50) NOT NULL, 
	visibility_id_bk			int NOT NULL,
	product_family_group_bk		varchar(50),
	product_type_oh_bk			varchar(50),
	magento_sku					varchar(50), 
	product_family_code			char(5),
	product_family_name			varchar(255),
	glass_sunglass_name			varchar(255),
	glass_sunglass_colour		varchar(255),
	status						tinyint NOT NULL,
	promotional_product			tinyint NOT NULL,
	telesales_product			tinyint NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.prod.dim_product_family add constraint [PK_prod_dim_product_family]
	primary key clustered (product_id_bk);
go

alter table Staging.prod.dim_product_family add constraint [DF_prod_dim_product_family_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




create table Staging.prod.dim_product_type_oh(
	product_type_oh_bk		varchar(50) NOT NULL, 
	product_type_oh_name	varchar(50) NOT NULL, 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table Staging.prod.dim_product_type_oh add constraint [PK_prod_dim_product_type_oh]
	primary key clustered (product_type_oh_bk);
go

alter table Staging.prod.dim_product_type_oh add constraint [DF_prod_dim_product_type_oh_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_product_family_group(
	product_family_group_bk		varchar(50) NOT NULL, 
	product_family_group_name	varchar(50) NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table Staging.prod.dim_product_family_group add constraint [PK_prod_dim_product_family_group]
	primary key clustered (product_family_group_bk);
go

alter table Staging.prod.dim_product_family_group add constraint [DF_prod_dim_product_family_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_product_family_pack_size(
	packsizeid_bk					bigint NOT NULL,
	product_id_bk					int,
	size							int,
	product_family_packsize_name	varchar(200),
	allocation_preference_order		int,
	purchasing_preference_order		int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.prod.dim_product_family_pack_size add constraint [PK_prod_dim_product_family_pack_size]
	primary key clustered (packsizeid_bk);
go

alter table Staging.prod.dim_product_family_pack_size add constraint [DF_prod_dim_product_family_pack_size_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_price_type_pf(
	price_type_pf_bk				varchar(50) NOT NULL,
	price_type_pf_name				varchar(50) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.prod.dim_price_type_pf add constraint [PK_prod_dim_price_type_pf]
	primary key clustered (price_type_pf_bk);
go

alter table Staging.prod.dim_price_type_pf add constraint [DF_prod_dim_price_type_pf_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_product_family_price(
	supplierpriceid_bk				bigint NOT NULL,
	
	supplier_id_bk					bigint NOT NULL, 
	packsizeid_bk					bigint NOT NULL,
	price_type_pf_bk				varchar(50) NOT NULL,

	unit_price						decimal(12, 4), 
	currency_code					varchar(4),
	lead_time						int,
	effective_date					date,
	expiry_date						date,
	active							char(1),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.prod.dim_product_family_price add constraint [PK_prod_dim_product_family_price]
	primary key clustered (supplierpriceid_bk);
go

alter table Staging.prod.dim_product_family_price add constraint [DF_prod_dim_product_family_price_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_product(
	productid_erp_bk				bigint NOT NULL,
	
	product_id_bk					int,
	base_curve_bk					char(3), 
	diameter_bk						char(4), 
	power_bk						char(6), 
	cylinder_bk						char(5), 
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1),
	colour_bk						varchar(50),

	parameter						varchar(200),
	product_description				varchar(200),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.prod.dim_product add constraint [PK_prod_dim_product]
	primary key clustered (productid_erp_bk);
go

alter table Staging.prod.dim_product add constraint [DF_prod_dim_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



create table Staging.prod.dim_stock_item(
	stockitemid_bk					bigint NOT NULL,
	
	productid_erp_bk				bigint,
	packsize						int, 

	SKU								varchar(200),
	stock_item_description			varchar(200),

	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table Staging.prod.dim_stock_item add constraint [PK_prod_dim_stock_item]
	primary key clustered (stockitemid_bk);
go

alter table Staging.prod.dim_stock_item add constraint [DF_prod_dim_stock_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 