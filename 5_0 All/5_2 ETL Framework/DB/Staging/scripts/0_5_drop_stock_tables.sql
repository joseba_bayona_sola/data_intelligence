go 

drop table Staging.stock.dim_stock_method_type
go

drop table Staging.stock.dim_stock_batch_type
go

drop table Staging.stock.dim_stock_adjustment_type
go

drop table Staging.stock.dim_stock_movement_type
go

drop table Staging.stock.dim_stock_movement_period
go


drop table Staging.stock.dim_wh_stock_item
go

drop table Staging.stock.dim_wh_stock_item_batch
go

drop table Staging.stock.fact_wh_stock_item_batch_movement
go
