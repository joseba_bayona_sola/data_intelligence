use Staging
go

drop procedure stock.stg_dwh_get_stock_stock_method_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Method Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_stock_method_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select stock_method_type_bk, stock_method_type_name, description, @idETLBatchRun 
	from Staging.stock.dim_stock_method_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_stock_batch_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Batch Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_stock_batch_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select stock_batch_type_bk, stock_batch_type_name, description, @idETLBatchRun 
	from Staging.stock.dim_stock_batch_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_stock_adjustment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Adjustment Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_stock_adjustment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select stock_adjustment_type_bk, stock_adjustment_type_name, description, @idETLBatchRun 
	from Staging.stock.dim_stock_adjustment_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_stock_movement_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Movement Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_stock_movement_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select stock_movement_type_bk, stock_movement_type_name, description, @idETLBatchRun 
	from Staging.stock.dim_stock_movement_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_stock_movement_period
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Movement Period in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_stock_movement_period
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish, @idETLBatchRun 
	from Staging.stock.dim_stock_movement_period

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_wh_stock_item
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Stock Item Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_wh_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select warehousestockitemid_bk, 
		warehouseid_bk, stockitemid_bk, stock_method_type_bk, 
		wh_stock_item_description, 

		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, qty_due_in,
		stocked, @idETLBatchRun
	from Staging.stock.dim_wh_stock_item

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_wh_stock_item_batch
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Reads data from Stock Movement Type in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_wh_stock_item_batch
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select warehousestockitembatchid_bk, 
		warehousestockitemid_bk, stock_batch_type_bk, 
		batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 
	
		qty_received, qty_available, qty_outstanding_allocation, qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, 
		qty_registered_sm, qty_disposed_sm, 

		batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 
	
		local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
		local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
		local_to_global_rate, currency_code, 
		delete_f,
	
		@idETLBatchRun
	from Staging.stock.dim_wh_stock_item_batch

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure stock.stg_dwh_get_stock_wh_stock_item_batch_movement
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Reads data from WH Stock Item Batch Movement in Staging Table 
-- ==========================================================================================

create procedure stock.stg_dwh_get_stock_wh_stock_item_batch_movement
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 

		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 

		qty_movement, qty_registered, qty_disposed, 
		batch_movement_date, 
		delete_f,

		@idETLBatchRun
	from Staging.stock.fact_wh_stock_item_batch_movement

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go