use Staging
go 

drop table Staging.gen.dim_time
go


drop table Staging.gen.dim_company
go

drop table Staging.gen.dim_store
go


drop table Staging.gen.dim_country
go

drop table Staging.gen.dim_region
go


drop table Staging.gen.dim_market
go


drop table Staging.gen.dim_customer_type
go

drop table Staging.gen.dim_customer_status
go

drop table Staging.gen.dim_customer_unsubscribe
go

drop table Staging.gen.dim_customer_origin
go


drop table Staging.gen.dim_customer
go


drop table Staging.gen.dim_supplier_type
go

drop table Staging.gen.dim_supplier
go

drop table Staging.gen.dim_warehouse
go

