
use Staging
go

drop table Staging.rec.dim_wh_shipment_type
go

drop table Staging.rec.dim_receipt_status
go

drop table Staging.rec.dim_receipt_line_status
go

drop table Staging.rec.dim_receipt_line_sync_status
go


drop table Staging.rec.dim_wh_shipment
go

drop table Staging.rec.dim_receipt
go

drop table Staging.rec.fact_receipt_line
go


