
use Staging
go


select po_source_bk, po_source_name, description, 
	idETLBatchRun, ins_ts
from Staging.po.dim_po_source

select po_type_bk, po_type_name, description, 
	idETLBatchRun, ins_ts
from Staging.po.dim_po_type

select po_status_bk, po_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.po.dim_po_status

select pol_status_bk, pol_status_name, description, 
	idETLBatchRun, ins_ts
from Staging.po.dim_pol_status

select pol_problems_bk, pol_problems_name, description, 
	idETLBatchRun, ins_ts
from Staging.po.dim_pol_problems

select wh_operator_bk, wh_operator_name, 
	idETLBatchRun, ins_ts
from Staging.po.dim_wh_operator



select purchaseorderid_bk, 
	warehouseid_bk, supplier_id_bk, 
	po_source_bk, po_type_bk, po_status_bk, wh_operator_cr_bk, wh_operator_as_bk, 
	purchase_order_number, leadtime, 
	created_date, approved_date, confirmed_date, submitted_date, completed_date, due_date, 
	local_total_cost, local_to_global_rate, currency_code,
	idETLBatchRun, ins_ts
from Staging.po.dim_purchase_order


select purchaseorderlineid_bk, 
	purchaseorderid_bk, pol_status_bk, pol_problems_bk, 
	stockitemid_bk, supplierpriceid_bk, 
	purchase_order_line_id, 
	quantity_ordered, quantity_received, quantity_planned, quantity_open, quantity_cancelled, 
	local_unit_cost, local_line_cost, local_to_global_rate, currency_code,
	idETLBatchRun, ins_ts
from Staging.po.fact_purchase_order_line
