use ControlDB
go

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'TEMP_XXXX' name, 'Template Flow for future developments' description) t
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'TEMP_XXXX' flow_name, 'temp_xxxx' name, 'Landing Table for TEMPLATE FLOW: Data coming from Magento' description
			union
			select 'Staging' database_name, 'TEMP_XXXX' flow_name, 'dim_temp_xxxx' name, 'Staging Table for TEMPLATE FLOW: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'TEMP_XXXX' flow_name, 'dim_temp_xxxx' name, 'Warehouse Table for TEMPLATE FLOW: Store Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'jsola' developer,  'TEMP_XXXX' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'temp_srcmag_lnd_temp_xxxx' name, 'SSIS PKG for reading TEMPLATE FLOW data from Magento to Landing' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'TEMP_XXXX' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'temp_lnd_stg_temp_xxxx' name, 'SSIS PKG for reading TEMPLATE FLOW data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'TEMP_XXXX' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'temp_stg_dwh_temp_xxxx' name, 'SSIS PKG for reading TEMPLATE FLOW data from Staging to Warehouse - Update Dimension' description
			) t
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'temp_srcmag_lnd_temp_xxxx' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_temp_xxxx' name, 'GET SP for reading XXXXX data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'temp_lnd_stg_temp_xxxx' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_temp_xxxx' name, 'GET SP for reading XXXXX data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'temp_stg_dwh_temp_xxxx' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_temp_xxxx' name, 'GET SP for reading XXXXX data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'temp_stg_dwh_temp_xxxx' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_temp_xxxx' name, 'MERGE SP for upading XXXXX Dimension data' description
			) t
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
go 