
use Staging
go 

create table Staging.gen.dim_temp_xxxx(
	xxxx_1_bk			int NOT NULL, 
	xxxx_2				varchar(100) NULL, 
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go 

alter table Staging.gen.dim_temp_xxxx add constraint [PK_dim_temp_xxxx]
	primary key clustered (xxxx_1_bk);
go

alter table Staging.gen.dim_temp_xxxx add constraint [DF_gen_dim_temp_xxxx_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



--------------------- SP ----------------------------------

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from TEMPLATE in Staging Table 
-- ==========================================================================================

create procedure gen.stg_dwh_get_temp_xxxx
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Staging data
	select xxxx_1_bk, xxxx_2, @idETLBatchRun
	from Staging.gen.dim_temp_xxxx	
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 