
select oi.product_id, pfps.size, p.name, oi.num
from
		(select product_id, count(*) num
		from Landing.mag.sales_flat_order_item_aud
		group by product_id) oi
	left join
		Landing.mend.gen_prod_productfamilypacksize_v pfps on oi.product_id = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
	left join
		Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
	left join
		Landing.mag.catalog_product_entity_flat p on oi.product_id = p.entity_id and p.store_id = 0
where ep.product_id is null
	and pfps.size is null
order by oi.product_id

select p.product_id, p.size, p.name, p.num
from
		(select oi.product_id, pfps.size, p.name, oi.num
		from
				(select product_id, count(*) num
				from Landing.mag.sales_flat_order_item_aud
				group by product_id) oi
			left join
				Landing.mend.gen_prod_productfamilypacksize_v pfps on oi.product_id = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
			left join
				Landing.map.prod_exclude_products ep on oi.product_id = ep.product_id
			left join
				Landing.mag.catalog_product_entity_flat p on oi.product_id = p.entity_id and p.store_id = 0
		where ep.product_id is null
			and pfps.size is null) p
	inner join
		Landing.aux.sales_fact_order_line_aud ol on p.product_id = ol.product_id_bk
order by p.product_id

select *
from Landing.aux.sales_fact_order_line_aud
where product_id_bk = 1127

