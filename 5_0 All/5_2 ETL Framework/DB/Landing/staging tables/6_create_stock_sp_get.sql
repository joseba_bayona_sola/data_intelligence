
use Landing
go

drop procedure mend.lnd_stg_get_stock_stock_method_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Method Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_stock_method_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select smt.stock_method_type stock_method_type_bk, 
		isnull(msmt.stock_method_type_name, smt.stock_method_type) stock_method_type_name, isnull(msmt.description, smt.stock_method_type) description, 
		@idETLBatchRun
	from
			(select distinct stock_method_type
			from Landing.map.erp_stock_method_type_aud
			union
			select distinct stockingmethod
			from Landing.mend.gen_wh_warehousestockitem_v) smt
		left join
			(select stock_method_type, min(stock_method_type_name) stock_method_type_name, min(description) description
			from Landing.map.erp_stock_method_type
			group by stock_method_type) msmt on smt.stock_method_type = msmt.stock_method_type


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_stock_stock_batch_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Batch Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_stock_batch_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select sbt.stock_batch_type stock_batch_type_bk, sbt.stock_batch_type_name, sbt.description, @idETLBatchRun
	from
		(select stock_batch_type, min(stock_batch_type_name) stock_batch_type_name, min(description) description
		from Landing.map.erp_stock_batch_type_aud
		group by stock_batch_type) sbt

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_stock_stock_adjustment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Adjustment Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_stock_adjustment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select sat.stock_adjustment_type stock_adjustment_type_bk, 
		isnull(msat.stock_adjustment_type_name, sat.stock_adjustment_type) stock_adjustment_type_name, isnull(msat.description, sat.stock_adjustment_type) description, 
		@idETLBatchRun
	from
			(select distinct stock_adjustment_type
			from Landing.map.erp_stock_adjustment_type_aud
			union
			select distinct stockadjustment
			from Landing.mend.gen_wh_stockmovement_v
			where stockadjustment is not null
			union
			select distinct batchstockmovementtype
			from Landing.mend.gen_wh_batchstockmovement_v) sat
		left join
			(select stock_adjustment_type, min(stock_adjustment_type_name) stock_adjustment_type_name, min(description) description 
			from Landing.map.erp_stock_adjustment_type_aud
			group by stock_adjustment_type) msat on sat.stock_adjustment_type = msat.stock_adjustment_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_stock_stock_movement_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Movement Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_stock_movement_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select smt.stock_movement_type stock_movement_type_bk, 
		isnull(msmt.stock_movement_type_name, smt.stock_movement_type) stock_movement_type_name, isnull(msmt.description, smt.stock_movement_type) description, 
		@idETLBatchRun
	from
			(select distinct stock_movement_type
			from Landing.map.erp_stock_movement_type
			union
			select distinct movetype
			from Landing.mend.gen_wh_stockmovement_v
			where movetype is not null
			union
			select distinct movementtype
			from Landing.mend.gen_wh_stockmovement_v
			where movementtype is not null) smt 
		left join
			(select stock_movement_type, min(stock_movement_type_name) stock_movement_type_name, min(description) description
			from Landing.map.erp_stock_movement_type
			group by stock_movement_type) msmt on smt.stock_movement_type = msmt.stock_movement_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_stock_stock_movement_period
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Movement Period in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_stock_movement_period
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select stock_movement_period stock_movement_period_bk, stock_movement_period_name, description, period_start, period_finish, @idETLBatchRun
	from
		(select stock_movement_period, min(stock_movement_period_name) stock_movement_period_name, min(description) description, 
			min(period_start) period_start, min(period_finish) period_finish
		from Landing.map.erp_stock_movement_period_aud
		group by stock_movement_period) smp


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mend.lnd_stg_get_stock_wh_stock_item
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Stock Item in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_wh_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	delete from Landing.aux.stock_warehousestockitem

	insert into Landing.aux.stock_warehousestockitem (warehousestockitemid)

		select warehousestockitemid
		from Landing.mend.wh_warehousestockitembatch_warehousestockitem
		union
		select distinct warehousestockitemid
		from
				(select distinct warehousestockitembatchid
				from Landing.mend.wh_batchstockmovement_warehousestockitembatch) bsm_wsib
			inner join
				Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsib_wsi on bsm_wsib.warehousestockitembatchid = wsib_wsi.warehousestockitembatchid


	-- SELECT STATEMENT: 
	select wsi.warehousestockitemid warehousestockitemid_bk,
		wsi.warehouseid_bk, wsi.stockitemid_bk, wsi.stock_method_type_bk, 
		wsi.wh_stock_item_description, 

		isnull(wsib.qty_received, 0) qty_received, wsi.qty_available, wsi.qty_outstanding_allocation, wsi.qty_allocated_stock, isnull(wsib.qty_issued_stock, 0) qty_issued_stock, 
		wsi.qty_on_hold, isnull(wsib.qty_registered, 0) qty_registered, isnull(wsib.qty_disposed, 0) qty_disposed, wsi.qty_due_in,
		wsi.stocked, @idETLBatchRun
	from
			(select warehousestockitemid, 
				warehouseid warehouseid_bk, stockitemid stockitemid_bk, stockingmethod stock_method_type_bk, 
				id_string wh_stock_item_description, 

				availableqty qty_available, outstandingallocation qty_outstanding_allocation, allocatedstockqty qty_allocated_stock, 
				onhold qty_on_hold, dueinqty qty_due_in,
				stocked stocked
			from 
					Landing.mend.gen_wh_warehousestockitem_v wsi
				inner join	
					Landing.aux.mag_prod_product_family_v pf on wsi.productfamilyid = pf.productfamilyid) wsi
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsi.warehousestockitemid = awsi.warehousestockitemid
		left join
			(select warehousestockitemid_bk, 
				sum(qty_received) qty_received, sum(qty_issued_stock) qty_issued_stock, 
				sum(qty_registered_sm) qty_registered, sum(qty_disposed_sm) qty_disposed
			from Landing.aux.mend_stock_warehousestockitembatch_v
			group by warehousestockitemid_bk) wsib on wsi.warehousestockitemid = wsib.warehousestockitemid_bk


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_stock_wh_stock_item_batch
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Reads data from WH Stock Item Batch in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_wh_stock_item_batch
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select warehousestockitembatchid_bk, warehousestockitemid_bk, 
		stock_batch_type_bk,
		batch_id, fully_allocated_f, fully_issued_f, auto_adjusted_f, 

		qty_received, qty_available, qty_outstanding_allocation, 
		qty_allocated_stock, qty_issued_stock, 
		qty_on_hold, qty_registered, qty_disposed, 
		qty_registered_sm, qty_disposed_sm, 

		batch_arrived_date, batch_confirmed_date, batch_stock_register_date, 

		local_product_unit_cost, local_carriage_unit_cost, local_duty_unit_cost, local_total_unit_cost, 
		local_interco_carriage_unit_cost, local_total_unit_cost_interco, 
		local_to_global_rate, currency_code, 
		'N' delete_f,
		@idETLBatchRun
	from Landing.aux.mend_stock_warehousestockitembatch_v

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_stock_wh_stock_item_batch_movement
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-06-2018
-- Changed: 
	--	27-11-2018	Joseba Bayona Sola	Add delete_f flag
-- ==========================================================================================
-- Description: Reads data from WH Stock Item Batch Movement in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_stock_wh_stock_item_batch_movement
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		qty_registered, qty_disposed, 
		batch_movement_date, 
		'N' delete_f,
		@idETLBatchRun	
	from Landing.aux.mend_stock_batchstockmovement_v
	
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go