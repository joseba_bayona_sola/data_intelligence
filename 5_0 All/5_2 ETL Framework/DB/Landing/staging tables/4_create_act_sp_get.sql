use Landing
go 

drop procedure mag.lnd_stg_get_act_activity_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Group in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_activity_group
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_type 
	select activity_group activity_group_name_bk, activity_group activity_group_name, @idETLBatchRun
	from
		(select distinct activity_group
		from Landing.map.act_activity_group_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_act_activity_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_activity_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_type 
	select activity_type activity_type_name_bk, activity_type activity_type_name, 
		activity_group activity_group_name_bk, 
		@idETLBatchRun
	from 
		(select distinct activity_group, activity_type
		from Landing.map.act_activity_group_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_act_activity_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	07-09-2017	Joseba Bayona Sola	Add customer_order_seq_no_web attribute
	--	16-10-2017	Joseba Bayona Sola	Add idCalendarNextActivityDate attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk attribute
	--	11-05-2018	Joseba Bayona Sola	Add customer_order_seq_no_gen attribute
-- ==========================================================================================
-- Description: Reads data from Activity Sales in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_activity_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_type 
	select order_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
		customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

		local_subtotal, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, 
		local_to_global_rate, order_currency_code, 
		@idETLBatchRun
	from Landing.aux.act_fact_activity_sales

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_act_activity_other
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Other in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_activity_other
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: From Orders table (sales_flat_order) take different values regarding reminder_type 
	select activity_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		activity_seq_no, 

		@idETLBatchRun
	from Landing.aux.act_fact_activity_other

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mag.lnd_stg_get_aux_act_activity_sales
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	21-06-2017	Joseba Bayona Sola	Add discount_f attribute
	--	07-09-2017	Joseba Bayona Sola	Add customer_order_seq_no_web attribute + prev, next for Reactivate, Lapsed Logic
	--	16-10-2017	Joseba Bayona Sola	Add idCalendarNextActivityDate attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk attribute
	--	05-04-2018	Joseba Bayona Sola	In Merge for identifying REACTIVATE put >= instead of >
	--	11-05-2018	Joseba Bayona Sola	Add customer_order_seq_no_gen attribute + local_total_aft_refund_inc_vat attribute
-- ==========================================================================================
-- Description: Aux SP that prepares Activity Sales Data
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_act_activity_sales
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.act_fact_activity_sales

	-- INSERT STATEMENT:
	insert into Landing.aux.act_fact_activity_sales(order_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
		customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

		local_subtotal, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, 
		local_total_aft_refund_inc_vat, 
		local_to_global_rate, order_currency_code
		)

		select oh.order_id_bk, 
			oh.idCalendarOrderDate idCalendarActivityDate, oh.order_date activity_date, null idCalendarPrevActivityDate,null idCalendarNextActivityDate,
			case when (oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')) THEN 'ORDER' else oh.order_status_name_bk end activity_type_name_bk,
			oh.store_id_bk, oh.customer_id_bk, 
			oh.channel_name_bk, oh.marketing_channel_name_bk, oh.price_type_name_bk, oh.discount_f, oh.product_type_oh_bk, oh.order_qty_time, oh.num_diff_product_type_oh,
			null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web, null customer_order_seq_no_gen,
			oh.local_subtotal, oh.local_discount, oh.local_store_credit_used, 
			oh.local_total_inc_vat, oh.local_total_exc_vat, 
			oh.local_total_aft_refund_inc_vat,
			oh.local_to_global_rate, oh.order_currency_code
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				(select distinct customer_id_bk
				from Landing.aux.sales_dim_order_header
				union
				select distinct customer_id_bk
				from Landing.aux.sales_dim_order_header_aud, 
					(select (lapsed_num_days * -1) -1 start_range, (lapsed_num_days * -1) +1 finish_range
					from Landing.map.act_lapsed_num_days) l	
				where order_date between dateadd(day, start_range, getutcdate()) and dateadd(day, finish_range, getutcdate())) c on oh.customer_id_bk = c.customer_id_bk
		order by oh.customer_id_bk, oh.order_date

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			idCalendarActivityDate, activity_date, prev_activity_date, next_activity_date, 
			idCalendarPrevActivityDate, idCalendarNextActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			case when (customer_order_seq_no = 1) then 'NEW' else 'REGULAR' end customer_status_name_bk,
			customer_order_seq_no, customer_order_seq_no_web
		from
			(select order_id_bk, 
				idCalendarActivityDate, activity_date, 
				lag(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) prev_activity_date, 
				lead(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) next_activity_date, 
				lag(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarPrevActivityDate, 
				lead(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarNextActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				customer_status_name_bk, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) customer_order_seq_no, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk, s.website_group order by activity_date, order_id_bk) customer_order_seq_no_web 
			from 
					Landing.aux.act_fact_activity_sales acs
				inner join
					Landing.aux.mag_gen_store_v s on acs.store_id_bk = s.store_id) t) src
	on trg.order_id_bk = src.order_id_bk
	when matched then 
		update set 
			trg.prev_activity_date = src.prev_activity_date, trg.next_activity_date = isnull(src.next_activity_date, getutcdate()),
			trg.idCalendarPrevActivityDate = src.idCalendarPrevActivityDate, trg.idCalendarNextActivityDate = src.idCalendarNextActivityDate, 
			trg.customer_status_name_bk = src.customer_status_name_bk, 
			trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web;

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			activity_date, prev_activity_date, 
			activity_type_name_bk, customer_id_bk, 
			customer_order_seq_no, product_type_oh_bk, order_qty_time,
			case when (prev_activity_date is not null and abs(datediff(day, activity_date, prev_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER') then 'REACTIVATED' else customer_status_name_bk end customer_status_name_bk
		from Landing.aux.act_fact_activity_sales, 
			Landing.map.act_lapsed_num_days l) src
	on trg.order_id_bk = src.order_id_bk
	when matched and src.customer_status_name_bk <> trg.customer_status_name_bk 
		then 
		update set 
			trg.customer_status_name_bk = src.customer_status_name_bk;



	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, activity_date, activity_type_name_bk, customer_id_bk, 
			dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) customer_order_seq_no_gen 
		from Landing.aux.act_fact_activity_sales
		where activity_type_name_bk in ('ORDER', 'REFUND')) src
	on (trg.order_id_bk = src.order_id_bk)
	when matched then 
		update set 
			trg.customer_order_seq_no_gen = src.customer_order_seq_no_gen;

	update Landing.aux.act_fact_activity_sales
	set customer_order_seq_no_gen = customer_order_seq_no
	where customer_order_seq_no_gen is null


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_aux_act_activity_other
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	07-09-2017	Joseba Bayona Sola	Insert statements for Reactivate, Lapsed Logic
	--	22-09-2017	Joseba Bayona Sola	Correct LAPSED sentence to include >= so orders on 450 diff days are marked as lapsed 
	--	16-11-2017	Joseba Bayona Sola	Comment REGISTER logic (Will be done from Customer CR-REG) 
	--	16-11-2017	Joseba Bayona Sola	On LAPSED activities set the day at 00:00:00
-- ==========================================================================================
-- Description: Aux SP that prepares Activity Other Data
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_act_activity_other
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


	-- DELETE STATEMENT
	delete from Landing.aux.act_fact_activity_other 

	-- ACTIVITY: REGISTRATIONS
	-- exec mag.lnd_stg_get_aux_act_customer_registration @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

		-- INSERT STATEMENT: REGISTRATIONS
		--insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
		--	idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
		--	activity_type_name_bk, store_id_bk, customer_id_bk, 
		--	activity_seq_no)

		--	select customer_id_bk activity_id_bk, 
		--		idCalendarRegisterDate idCalendarActivityDate, register_date activity_date, null idCalendarPrevActivityDate, 
		--		'REGISTER' activity_type_name_bk, store_id_bk store_id_bk, customer_id_bk customer_id_bk, 
		--		1 activity_seq_no
		--	from Landing.aux.act_customer_registration

		--set @rowAmountSelect = @@ROWCOUNT
		--set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

		--exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

		-- INSERT STATEMENT: REACTIVATE		
		insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
			idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			activity_seq_no)

			select customer_id_bk activity_id_bk, 
				idCalendarActivityDate, activity_date, 
				CONVERT(INT, (CONVERT(VARCHAR(8), lag(activity_date) over (partition by customer_id_bk order by activity_date, order_id_bk), 112))) idCalendarPrevActivityDate,				 
				'REACTIVATE' activity_type_name_bk, store_id_bk, customer_id_bk, 
				dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) activity_seq_no
			from Landing.aux.act_fact_activity_sales
			where customer_status_name_bk = 'REACTIVATED'
			order by customer_id_bk

		set @rowAmountSelect = @@ROWCOUNT
		set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

		exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

		-- INSERT STATEMENT: LAPSED
		insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
			idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			activity_seq_no)

			select activity_id_bk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), activity_date, 112))) idCalendarActivityDate, convert(datetime, convert(date, activity_date)) activity_date, -- activity_date, 
				CONVERT(INT, (CONVERT(VARCHAR(8), lag(activity_date) over (partition by customer_id_bk order by activity_seq_no), 112))) idCalendarPrevActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				activity_seq_no
			from
				(select customer_id_bk activity_id_bk, 
					dateadd(day, l.lapsed_num_days, activity_date) activity_date,
					'LAPSED' activity_type_name_bk, store_id_bk, customer_id_bk, 
					dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) activity_seq_no
				from Landing.aux.act_fact_activity_sales, 
					Landing.map.act_lapsed_num_days l
				where (abs(datediff(day, activity_date, next_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER'))l
			order by customer_id_bk

		set @rowAmountSelect = @@ROWCOUNT
		set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

		exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





drop procedure mag.lnd_stg_get_aux_act_customer_registration
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Aux SP that loads data into Customer Registration Table
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_act_customer_registration
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.act_customer_registration

	-- INSERT STATEMENT:
		-- Need to solve the Register Store as the customer entity flat attribute is the current store: First order done?
	insert into Landing.aux.act_customer_registration(customer_id_bk, store_id_bk, 
		idCalendarRegisterDate, register_date, 
		idETLBatchRun)

		select entity_id customer_id, store_id store_id_bk, 
			CONVERT(INT, (CONVERT(VARCHAR(8), created_at, 112))) idCalendarRegisterDate, created_at register_date, 
			@idETLBatchRun
		from Landing.mag.customer_entity_flat

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






drop procedure mag.lnd_stg_get_aux_act_activity_sales_all_cust
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 08-09-2017
-- Changed: 
	--	16-10-2017	Joseba Bayona Sola	Add idCalendarNextActivityDate attribute
	--	23-11-2017	Joseba Bayona Sola	Add marketing_channel_name_bk attribute
	--	05-04-2018	Joseba Bayona Sola	In Merge for identifying REACTIVATE put >= instead of >
	--	11-05-2018	Joseba Bayona Sola	Add customer_order_seq_no_gen attribute + local_total_aft_refund_inc_vat attribute
-- ==========================================================================================
-- Description: Aux SP that prepares Activity Sales Data + Other Data for customers wanted
-- ==========================================================================================

create procedure mag.lnd_stg_get_aux_act_activity_sales_all_cust
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- DELETE STATEMENT
	delete from Landing.aux.act_fact_activity_sales

	-- INSERT STATEMENT:
	insert into Landing.aux.act_fact_activity_sales(order_id_bk, 
		idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, idCalendarNextActivityDate, 
		activity_type_name_bk, store_id_bk, customer_id_bk, 
		channel_name_bk, marketing_channel_name_bk, price_type_name_bk, discount_f, product_type_oh_bk, order_qty_time, num_diff_product_type_oh,
		customer_status_name_bk, customer_order_seq_no, customer_order_seq_no_web, customer_order_seq_no_gen, 

		local_subtotal, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, 
		local_total_aft_refund_inc_vat,
		local_to_global_rate, order_currency_code
		)

		select oh.order_id_bk, 
			oh.idCalendarOrderDate idCalendarActivityDate, oh.order_date activity_date, null idCalendarPrevActivityDate, null idCalendarNextActivityDate,
			case when (oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')) THEN 'ORDER' else oh.order_status_name_bk end activity_type_name_bk,
			oh.store_id_bk, oh.customer_id_bk, 
			oh.channel_name_bk, oh.marketing_channel_name_bk, oh.price_type_name_bk, oh.discount_f, oh.product_type_oh_bk, oh.order_qty_time, oh.num_diff_product_type_oh,
			null customer_status_name_bk, null customer_order_seq_no, null customer_order_seq_no_web, null customer_order_seq_no_gen,
			oh.local_subtotal, oh.local_discount, oh.local_store_credit_used, 
			oh.local_total_inc_vat, oh.local_total_exc_vat, 
			oh.local_total_aft_refund_inc_vat,
			oh.local_to_global_rate, oh.order_currency_code
		from 
				Landing.aux.sales_dim_order_header_aud oh
			inner join
				(select distinct entity_id customer_id_bk
				from Landing.mag.customer_entity_flat) c on oh.customer_id_bk = c.customer_id_bk
		order by oh.customer_id_bk, oh.order_date

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			idCalendarActivityDate, activity_date, prev_activity_date, next_activity_date, 
			idCalendarPrevActivityDate, idCalendarNextActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			case when (customer_order_seq_no = 1) then 'NEW' else 'REGULAR' end customer_status_name_bk,
			customer_order_seq_no, customer_order_seq_no_web
		from
			(select order_id_bk, 
				idCalendarActivityDate, activity_date, 
				lag(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) prev_activity_date, 
				lead(activity_date) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) next_activity_date, 
				lag(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarPrevActivityDate, 
				lead(idCalendarActivityDate) over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) idCalendarNextActivityDate, 
				activity_type_name_bk, store_id_bk, customer_id_bk, 
				customer_status_name_bk, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk order by activity_date, order_id_bk) customer_order_seq_no, 
				dense_rank() over (partition by customer_id_bk, activity_type_name_bk, s.website_group order by activity_date, order_id_bk) customer_order_seq_no_web 
			from 
					Landing.aux.act_fact_activity_sales acs
				inner join
					Landing.aux.mag_gen_store_v s on acs.store_id_bk = s.store_id) t) src
	on trg.order_id_bk = src.order_id_bk
	when matched then 
		update set 
			trg.prev_activity_date = src.prev_activity_date, trg.next_activity_date = isnull(src.next_activity_date, getutcdate()),
			trg.idCalendarPrevActivityDate = src.idCalendarPrevActivityDate, trg.idCalendarNextActivityDate = src.idCalendarNextActivityDate, 
			trg.customer_status_name_bk = src.customer_status_name_bk, 
			trg.customer_order_seq_no = src.customer_order_seq_no, trg.customer_order_seq_no_web = src.customer_order_seq_no_web;

	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, 
			activity_date, prev_activity_date, 
			activity_type_name_bk, customer_id_bk, 
			customer_order_seq_no, product_type_oh_bk, order_qty_time,
			case when (prev_activity_date is not null and abs(datediff(day, activity_date, prev_activity_date)) >= l.lapsed_num_days and activity_type_name_bk = 'ORDER') then 'REACTIVATED' else customer_status_name_bk end customer_status_name_bk
		from Landing.aux.act_fact_activity_sales, 
			Landing.map.act_lapsed_num_days l) src
	on trg.order_id_bk = src.order_id_bk
	when matched and src.customer_status_name_bk <> trg.customer_status_name_bk 
		then 
		update set 
			trg.customer_status_name_bk = src.customer_status_name_bk;


	merge into Landing.aux.act_fact_activity_sales trg
	using 
		(select order_id_bk, activity_date, activity_type_name_bk, customer_id_bk, 
			dense_rank() over (partition by customer_id_bk order by activity_date, order_id_bk) customer_order_seq_no_gen 
		from Landing.aux.act_fact_activity_sales
		where activity_type_name_bk in ('ORDER', 'REFUND')) src
	on (trg.order_id_bk = src.order_id_bk)
	when matched then 
		update set 
			trg.customer_order_seq_no_gen = src.customer_order_seq_no_gen;

	update Landing.aux.act_fact_activity_sales
	set customer_order_seq_no_gen = customer_order_seq_no
	where customer_order_seq_no_gen is null


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message


	exec mag.lnd_stg_get_aux_act_activity_other @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure aux.lnd_stg_get_act_customer_cr_reg
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-10-2017
-- Changed: 
	--	16-11-2017	Joseba Bayona Sola	Call to Aux SP + Return Results + Insert REGISTER activities
	--	26-04-2018	Joseba Bayona Sola	Add customer_origin_name_bk
-- ==========================================================================================
-- Description: Reads data from Customer CR - REG Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure aux.lnd_stg_get_act_customer_cr_reg
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	exec Landing.aux.lnd_stg_get_aux_act_customer_cr_reg @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- INSERT STATEMENT: REGISTRATIONS
		insert into Landing.aux.act_fact_activity_other(activity_id_bk, 
			idCalendarActivityDate, activity_date, idCalendarPrevActivityDate, 
			activity_type_name_bk, store_id_bk, customer_id_bk, 
			activity_seq_no)

			select customer_id_bk activity_id_bk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), register_date, 112))) idCalendarActivityDate, register_date activity_date, null idCalendarPrevActivityDate, 
				'REGISTER' activity_type_name_bk, store_id_bk_reg store_id_bk, customer_id_bk customer_id_bk, 
				1 activity_seq_no
			from 
					Landing.aux.act_customer_cr_reg crreg
				inner join
					(select entity_id customer_id
					from Landing.mag.customer_entity_flat
					union
					select distinct customer_id
					from Landing.mag.sales_flat_order) t on crreg.customer_id_bk = t.customer_id

		set @rowAmountSelect = @@ROWCOUNT
		set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

		exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	-- SELECT STATEMENT
	select customer_id_bk, 
			store_id_bk_cr, CONVERT(INT, (CONVERT(VARCHAR(8), create_date, 112))) idCalendarCreateDate, 
			store_id_bk_reg, CONVERT(INT, (CONVERT(VARCHAR(8), register_date, 112))) idCalendarRegisterDate, 
			customer_origin_name_bk,
			@idETLBatchRun
	from 
			Landing.aux.act_customer_cr_reg crreg
		inner join
			(select entity_id customer_id
			from Landing.mag.customer_entity_flat
			union
			select distinct customer_id
			from Landing.mag.sales_flat_order) t on crreg.customer_id_bk = t.customer_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





drop procedure aux.lnd_stg_get_aux_act_customer_cr_reg
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-11-2017
-- Changed: 
	--	16-11-2017	Joseba Bayona Sola	Temp Table Inserts + Logic for CR, REG + Merge to Aux Table
	--	15-02-2018	Joseba Bayona Sola	In Step1 Ins replace sales_flat_order for sales_order_header_store_cust (For including migrate flow)
	--	26-04-2018	Joseba Bayona Sola	Add customer_origin_name_bk
-- ==========================================================================================
-- Description: Prepares data from Customer CR - REG and inserts it on Aux Table
-- ==========================================================================================

create procedure aux.lnd_stg_get_aux_act_customer_cr_reg
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin

	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	create table #act_customer_cr_reg(
		customer_id_bk						int NOT NULL,
		email								varchar(255),
		store_id							int, 
		created_at							datetime NOT NULL,

		old_access_cust_no					varchar(255),
		store_id_oacn						int,
		website_group_oacn					varchar(255),

		migrate_db							varchar(25),
		migrate_old_customer_id				bigint, 
		migrate_created_at					datetime,
				
		num_dist_websites					int,

		store_id_first_all					int, 
		first_all_date						datetime,

		store_id_first_vd					int,
		first_vd_date						datetime,

		store_id_first_non_vd				int,
		first_non_vd_date					datetime,

		store_id_last_non_vd				int,
		last_non_vd_date					datetime,

		non_vd_customer_status_at_migration_date	varchar(50),

		store_id_last_vd_before_migration	int,
		last_vd_before_migration_date		datetime,
		customer_status_at_migration_date	varchar(50),

		migration_date						datetime,

		store_id_last_vd_before_oacn		int,
		last_vd_before_oacn_date			datetime,
		customer_status_at_oacn_date		varchar(50),

		migration_date_oacn					datetime,

		store_id_bk_cr						int,
		create_date							datetime,	

		store_id_bk_reg						int,
		register_date						datetime, 
		
		customer_origin_name_bk				varchar(50), 
		num_mig_orders_last_year			int)

	-- Step 1

		insert into #act_customer_cr_reg(customer_id_bk, email, store_id, created_at, 
			old_access_cust_no, store_id_oacn, website_group_oacn, 
			migrate_db, migrate_old_customer_id, migrate_created_at)

			select c.entity_id customer_id_bk, c.email, c.store_id, c.created_at, 
				c.old_access_cust_no, oacn.store_id store_id_oacn, oacn.website_group website_group_oacn, 
				mc.db migrate_db, mc.old_customer_id, mc.created_at migrate_created_at
			from 
					(select entity_id customer_id
					from Landing.mag.customer_entity_flat
					union
					select distinct customer_id_bk customer_id
					from Landing.aux.sales_order_header_store_cust) t
				inner join					
					Landing.mag.customer_entity_flat_aud c on t.customer_id = c.entity_id
				left join
					(select oacn.old_access_cust_no, s.store_id, s.website_group
					from 
							Landing.map.act_website_old_access_cust_no_aud oacn
						inner join
							Landing.aux.mag_gen_store_v s on oacn.store_id = s.store_id) oacn on c.old_access_cust_no = oacn.old_access_cust_no
				left join
					(select db, new_magento_id, old_customer_id, created_at
					from
						(select db, new_magento_id, old_customer_id, created_at, 
							count(*) over (partition by new_magento_id) num_rep, 
							rank() over (partition by new_magento_id order by created_at, db, old_customer_id) ord_rep -- also old_customer_id as a Migr. customer could have more than 1 old account
						from Landing.migra.migrate_customerdata_v
						where new_magento_id <> 0) c
					where ord_rep = 1) mc on c.entity_id = mc.new_magento_id

	-- Step 2:  

		-- num_dist_websites
		merge into #act_customer_cr_reg trg
		using
			(select t.customer_id_bk, count(distinct s.website_group) dist_website
			from 
					#act_customer_cr_reg t
				left join
					Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				left join
					(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
					from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
			group by t.customer_id_bk) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set trg.num_dist_websites = src.dist_website;

		-- store_id_first_all, first_all_date 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where r_all = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_all = src.store_id, 
				trg.first_all_date = src.order_date;

		-- store_id_first_vd, first_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 1 and r_non_vd = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_vd = src.store_id, 
				trg.first_vd_date = src.order_date;

		-- store_id_first_non_vd, first_non_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 0 and r_non_vd = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_non_vd = src.store_id, 
				trg.first_non_vd_date = src.order_date;

		-- store_id_last_non_vd, last_non_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd, 
					count(*) over (partition by t.customer_id_bk, s.website_group_f) num_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 0 and r_non_vd = num_non_vd) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_non_vd = src.store_id, 
				trg.last_non_vd_date = src.order_date;


	-- Step 3: migration_date: All Cust with first order in Non Current VD

		merge into #act_customer_cr_reg trg
		using
			(select c.customer_id_bk, wmd.migration_date
			from 
					#act_customer_cr_reg c
				inner join
					Landing.aux.mag_gen_store_v s on c.store_id_last_non_vd = s.store_id -- first or last ??
				inner join
					Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.migration_date = src.migration_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_non_vd_date, migration_date) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end non_vd_customer_status_at_migration_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where migration_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.non_vd_customer_status_at_migration_date = src.non_vd_customer_status_at_migration_date;

	-- Step 4.1: store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					t.migration_date, 
					count(*) over (partition by t.customer_id_bk) num_rows, 
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all
				from 
						#act_customer_cr_reg t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					left join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
				where t.migration_date is not null
					and s.website_group_f = 1 and oh.order_date < t.migration_date) t
			where num_rows = r_all) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_vd_before_migration = src.store_id, 
				trg.last_vd_before_migration_date = src.order_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_vd_before_migration_date, migration_date) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end customer_status_at_migration_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where last_vd_before_migration_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.customer_status_at_migration_date = src.customer_status_at_migration_date;

	-- Step 4.2: migration_date_oacn // store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date

		merge into #act_customer_cr_reg trg
		using
			(select t.customer_id_bk, wmd.migration_date migration_date_oacn
			from 
					#act_customer_cr_reg t
				inner join
					Landing.map.act_website_old_access_cust_no_aud oacn on t.old_access_cust_no = oacn.old_access_cust_no
				inner join
					Landing.aux.mag_gen_store_v s on oacn.store_id = s.store_id
				inner join
					Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group
			where 
				t.old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.migration_date_oacn = src.migration_date_oacn;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					t.migration_date_oacn, 
					count(*) over (partition by t.customer_id_bk) num_rows, 
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all
				from 
						#act_customer_cr_reg t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					left join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
				where t.migration_date_oacn is not null
					and s.website_group_f = 1 and oh.order_date < t.migration_date_oacn) t
			where num_rows = r_all) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_vd_before_oacn = src.store_id, 
				trg.last_vd_before_oacn_date = src.order_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_vd_before_oacn_date, migration_date_oacn) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end customer_status_at_oacn_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where last_vd_before_oacn_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.customer_status_at_oacn_date = src.customer_status_at_oacn_date;

	-- Step 5: store_id_bk_cr, create_date 
	
		-- 5.1: 0 Orders
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = created_at -- Need to add logic on store_id
			where store_id_first_all is null and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null)
	
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, create_date = created_at
			where store_id_first_all is null and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')

		-- 5.2: XX Orders - Only in VD
			
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null)
			
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 
			

		-- 5.3: XX Orders - Only Migrate Website


			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_non_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) and
				store_id_first_non_vd not in (select store_id
										 from Landing.map.act_website_old_access_cust_no_aud
										 where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) and
				store_id_first_non_vd in (select store_id
										 from Landing.map.act_website_old_access_cust_no_aud
										 where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 

		-- 5.4: XX Orders - Both VD - Migrate

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_last_non_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end 
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) and
				(non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED') and
				store_id_last_non_vd not in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) and
				(non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED') and
				store_id_last_non_vd in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL'))


			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end 
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') or old_access_cust_no is null) and
				(customer_status_at_migration_date = 'ACTIVE' or non_vd_customer_status_at_migration_date = 'LAPSED')

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL') 


	-- Step 6: store_id_bk_reg, register_date

	merge into #act_customer_cr_reg trg
	using
		(select customer_id_bk,
			isnull(wrm.store_id_register, t.store_id) store_id_bk_reg, 
			isnull(wmd.migration_date, t.create_date) register_date
		from 
				#act_customer_cr_reg t
			left join
				Landing.map.act_website_register_mapping_aud wrm on t.store_id = wrm.store_id
			left join
				Landing.aux.mag_gen_store_v s on t.store_id_bk_cr = s.store_id
			left join
				Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group) src on trg.customer_id_bk = src.customer_id_bk
	when matched then
		update 
		set  
			trg.store_id_bk_reg = src.store_id_bk_reg, 
			trg.register_date = src.register_date;

	-- Step 7: customer_origin_name_bk, num_mig_orders_last_year

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, count(num_mig_orders_last_year) num_mig_orders_last_year
			from
				(select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
					case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
				from 
						(select customer_id_bk, store_id_bk_cr, migration_date_oacn
						from #act_customer_cr_reg
						where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk) t
			group by customer_id_bk) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set trg.num_mig_orders_last_year = src.num_mig_orders_last_year;

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'MIGRATION'
		where num_mig_orders_last_year >=2

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'ORGANIC'
		where num_mig_orders_last_year in (0, 1) or num_mig_orders_last_year is null



	-- FINAL MERGE STATEMENT
	merge into Landing.aux.act_customer_cr_reg trg
	using #act_customer_cr_reg src
		on trg.customer_id_bk = src.customer_id_bk
	when matched and not exists
		(select 
			isnull(trg.email, ''), isnull(trg.store_id, 0), isnull(trg.created_at, ''), 
			isnull(trg.old_access_cust_no, ''), isnull(trg.store_id_oacn, 0), isnull(trg.website_group_oacn, ''), 
			isnull(trg.migrate_db, ''), isnull(trg.migrate_old_customer_id, 0), isnull(trg.migrate_created_at, ''), 
			isnull(trg.num_dist_websites, 0), 
			isnull(trg.store_id_first_all, 0), isnull(trg.first_all_date, ''), 
			isnull(trg.store_id_first_vd, 0), isnull(trg.first_vd_date, ''), 
			isnull(trg.store_id_first_non_vd, 0), isnull(trg.first_non_vd_date, ''), 
			isnull(trg.store_id_last_non_vd, 0), isnull(trg.last_non_vd_date, ''), 
			isnull(trg.non_vd_customer_status_at_migration_date, ''), 
			isnull(trg.store_id_last_vd_before_migration, 0), isnull(trg.last_vd_before_migration_date, ''), isnull(trg.customer_status_at_migration_date, ''), 
			isnull(trg.migration_date, ''), 
			isnull(trg.store_id_last_vd_before_oacn, 0), isnull(trg.last_vd_before_oacn_date, ''), isnull(trg.customer_status_at_oacn_date, ''), 
			isnull(trg.migration_date_oacn, ''), 
			isnull(trg.store_id_bk_cr, 0), isnull(trg.create_date, ''), 
			isnull(trg.store_id_bk_reg, 0), isnull(trg.register_date, ''), 
			isnull(trg.customer_origin_name_bk, ''), isnull(trg.num_mig_orders_last_year, 0)
		intersect
		select 
			isnull(src.email, ''), isnull(src.store_id, 0), isnull(src.created_at, ''), 
			isnull(src.old_access_cust_no, ''), isnull(src.store_id_oacn, 0), isnull(src.website_group_oacn, ''), 
			isnull(src.migrate_db, ''), isnull(src.migrate_old_customer_id, 0), isnull(src.migrate_created_at, ''), 
			isnull(src.num_dist_websites, 0), 
			isnull(src.store_id_first_all, 0), isnull(src.first_all_date, ''), 
			isnull(src.store_id_first_vd, 0), isnull(src.first_vd_date, ''), 
			isnull(src.store_id_first_non_vd, 0), isnull(src.first_non_vd_date, ''), 
			isnull(src.store_id_last_non_vd, 0), isnull(src.last_non_vd_date, ''), 
			isnull(src.non_vd_customer_status_at_migration_date, ''), 
			isnull(src.store_id_last_vd_before_migration, 0), isnull(src.last_vd_before_migration_date, ''), isnull(src.customer_status_at_migration_date, ''), 
			isnull(src.migration_date, ''), 
			isnull(src.store_id_last_vd_before_oacn, 0), isnull(src.last_vd_before_oacn_date, ''), isnull(src.customer_status_at_oacn_date, ''), 
			isnull(src.migration_date_oacn, ''), 
			isnull(src.store_id_bk_cr, 0), isnull(src.create_date, ''), 
			isnull(src.store_id_bk_reg, 0), isnull(src.register_date, ''), 
			isnull(src.customer_origin_name_bk, ''), isnull(src.num_mig_orders_last_year, 0))
		then
		update set 
			trg.email = src.email, trg.store_id = src.store_id, trg.created_at = src.created_at, 
			trg.old_access_cust_no = src.old_access_cust_no, trg.store_id_oacn = src.store_id_oacn, trg.website_group_oacn = src.website_group_oacn, 
			trg.migrate_db = src.migrate_db, trg.migrate_old_customer_id = src.migrate_old_customer_id, trg.migrate_created_at = src.migrate_created_at, 
			trg.num_dist_websites = src.num_dist_websites, 
			trg.store_id_first_all = src.store_id_first_all, trg.first_all_date = src.first_all_date, 
			trg.store_id_first_vd = src.store_id_first_vd, trg.first_vd_date = src.first_vd_date, 
			trg.store_id_first_non_vd = src.store_id_first_non_vd, trg.first_non_vd_date = src.first_non_vd_date, 
			trg.store_id_last_non_vd = src.store_id_last_non_vd, trg.last_non_vd_date = src.last_non_vd_date, 
			trg.non_vd_customer_status_at_migration_date = src.non_vd_customer_status_at_migration_date, 
			trg.store_id_last_vd_before_migration = src.store_id_last_vd_before_migration, trg.last_vd_before_migration_date = src.last_vd_before_migration_date, 
			trg.customer_status_at_migration_date = src.customer_status_at_migration_date, 
			trg.migration_date = src.migration_date, 
			trg.store_id_last_vd_before_oacn = src.store_id_last_vd_before_oacn, trg.last_vd_before_oacn_date = src.last_vd_before_oacn_date, trg.customer_status_at_oacn_date = src.customer_status_at_oacn_date, 
			trg.migration_date_oacn = src.migration_date_oacn, 
			trg.store_id_bk_cr = src.store_id_bk_cr, trg.create_date = src.create_date, 
			trg.store_id_bk_reg = src.store_id_bk_reg, trg.register_date = src.register_date, 
			trg.customer_origin_name_bk = src.customer_origin_name_bk, trg.num_mig_orders_last_year = src.num_mig_orders_last_year, 
			trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()

	when not matched then 
		insert (customer_id_bk, 
			email, store_id, created_at, 
			old_access_cust_no, store_id_oacn, website_group_oacn,
			migrate_db, migrate_old_customer_id, migrate_created_at, 
			num_dist_websites, 
			store_id_first_all, first_all_date, store_id_first_vd, first_vd_date, store_id_first_non_vd, first_non_vd_date, 
			store_id_last_non_vd, last_non_vd_date, 
			non_vd_customer_status_at_migration_date,
			store_id_last_vd_before_migration, last_vd_before_migration_date, 
			customer_status_at_migration_date, 
			migration_date, 
			store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date,
			migration_date_oacn, 
			store_id_bk_cr, create_date, 
			store_id_bk_reg, register_date, 
			customer_origin_name_bk, num_mig_orders_last_year,
			idETLBatchRun_ins) 
		
			values (src.customer_id_bk, 
				src.email, src.store_id, src.created_at, 
				src.old_access_cust_no, src.store_id_oacn, src.website_group_oacn,
				src.migrate_db, src.migrate_old_customer_id, src.migrate_created_at, 
				src.num_dist_websites, 
				src.store_id_first_all, src.first_all_date, src.store_id_first_vd, src.first_vd_date, src.store_id_first_non_vd, src.first_non_vd_date, 
				src.store_id_last_non_vd, src.last_non_vd_date, 
				src.non_vd_customer_status_at_migration_date,
				src.store_id_last_vd_before_migration, src.last_vd_before_migration_date, 
				src.customer_status_at_migration_date, 
				src.migration_date, 
				src.store_id_last_vd_before_oacn, src.last_vd_before_oacn_date, src.customer_status_at_oacn_date,
				src.migration_date_oacn, 
				src.store_id_bk_cr, src.create_date, 
				src.store_id_bk_reg, src.register_date, 
				src.customer_origin_name_bk, src.num_mig_orders_last_year,
				@idETLBatchRun)

	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
	drop table #act_customer_cr_reg

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_act_rank_cr_time_mm
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-03-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Rank CR Time MM in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_rank_cr_time_mm
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output


		create table #cr_time_mm(
			cr_time_mm_bk				int NOT NULL, 
			cr_time_mm					char(2) NOT NULL,
			cr_time_name				varchar(10) NOT NULL
			)

		declare @min_rank int, @max_rank int
		declare @cr_time_mm char(2)
		declare @cr_time_name varchar(10)

		declare db_cursor cursor for
		select min_rank, 
			case when (max_rank > 30000) then 30000 else max_rank end max_rank, 
			rank_cr_time_mm, cr_time_name
		from Landing.map.act_rank_cr_time_mm

		open db_cursor 
		fetch next from db_cursor into @min_rank, @max_rank, @cr_time_mm, @cr_time_name

		while @@fetch_status = 0
		begin

			WITH gen AS (
				SELECT @min_rank AS num
				UNION ALL
				SELECT num+1 FROM gen WHERE num+1<=@max_rank
			)

			insert into #cr_time_mm(cr_time_mm_bk, cr_time_mm, cr_time_name)
				SELECT num, @cr_time_mm, @cr_time_name 
				FROM gen
				option (maxrecursion 30000)

			fetch next from db_cursor into @min_rank, @max_rank, @cr_time_mm, @cr_time_name
		end

		close db_cursor
		deallocate db_cursor

	-- SELECT STATEMENT: From price_type Mapping Table
	select cr_time_mm_bk cr_time_mm_bk, cr_time_mm cr_time_mm, cr_time_name cr_time_name, @idETLBatchRun
	from #cr_time_mm

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #cr_time_mm

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_act_customer_product_signature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 01-06-2017
-- Changed: 
	--	06-06-2018	Joseba Bayona Sola	Add customers affected in current dim_order_header who are in CANCEL - REFUND (for deleting in Warehouse table)
-- ==========================================================================================
-- Description: Reads data from Activity Group in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_customer_product_signature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	--select c.customer_id_bk, ol.product_id_bk, 
	--	min(oh.idCalendarOrderDate) idCalendarFirstOrderDate, max(oh.idCalendarOrderDate) idCalendarLastOrderDate, 
	--	count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders, 
	--	@idETLBatchRun
	--from
	--		(select distinct customer_id_bk
	--		from Landing.aux.sales_dim_order_header) c
	--	inner join
	--		Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
	--	inner join
	--		Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk
	--group by c.customer_id_bk, ol.product_id_bk

	select oh.order_id_bk, oh.order_date, oh.idCalendarOrderDate, oh.customer_id_bk
	into #oh
	from 
			(select distinct customer_id_bk
			from Landing.aux.sales_dim_order_header) c
		inner join 
			Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')

	select ol.order_line_id_bk, ol.order_id_bk, oh.order_date, oh.customer_id_bk, 
		ol.product_id_bk, ol.local_subtotal, ol.local_to_global_rate, 
		count(*) over (partition by customer_id_bk, product_id_bk) num_lines,
		rank() over (partition by customer_id_bk, product_id_bk order by order_date, order_line_id_bk) ord_lines
	into #ol
	from 
			#oh oh
		inner join 
			Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk

	select t1.customer_id_bk, t1.product_id_bk, 
		t1.idCalendarFirstOrderDate, t1.idCalendarLastOrderDate, 
		t2.order_id_bk_first, t3.order_id_bk_last, 
		t1.num_tot_orders, t1.subtotal_tot_orders, 
		@idETLBatchRun
	from
			(select c.customer_id_bk, ol.product_id_bk, 
				min(oh.idCalendarOrderDate) idCalendarFirstOrderDate, max(oh.idCalendarOrderDate) idCalendarLastOrderDate, 
				count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders
			from
					(select distinct customer_id_bk
					from Landing.aux.sales_dim_order_header) c
				inner join
					#oh oh on c.customer_id_bk = oh.customer_id_bk -- and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				inner join
					#ol ol on oh.order_id_bk = ol.order_id_bk
			group by c.customer_id_bk, ol.product_id_bk) t1
		inner join
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_first
			from #ol
			where ord_lines = 1) t2 on t1.customer_id_bk = t2.customer_id_bk and t1.product_id_bk = t2.product_id_bk
		inner join	
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_last
			from #ol
			where ord_lines = num_lines) t3 on t1.customer_id_bk = t3.customer_id_bk and t1.product_id_bk = t3.product_id_bk
	union
	select c.customer_id_bk, -1 product_id_bk, 
		0 idCalendarFirstOrderDate, 0 idCalendarLastOrderDate, 
		0 order_id_bk_first, 0 order_id_bk_last, 
		0 num_tot_orders, 0 subtotal_tot_orders, 
		@idETLBatchRun
	from 
			(select distinct customer_id_bk
			from Landing.aux.sales_dim_order_header) c
		left join
			#oh oh on c.customer_id_bk = oh.customer_id_bk
	where oh.customer_id_bk is null

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	drop table #oh
	drop table #ol

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mag.lnd_stg_get_act_activity_login
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 30-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Activity Login in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_act_activity_login
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select lc.customer_id_bk, 
		CONVERT(INT, (CONVERT(VARCHAR(8), lc.first_login, 112))) idCalendarFirstLogin, 
		CONVERT(INT, (CONVERT(VARCHAR(8), lc.last_login, 112))) idCalendarLastLogin, 
		lc.num_tot_logins, 
		@idETLBatchRun	
	from
			(select customer_id customer_id_bk, min(login_at) first_login, max(login_at) last_login, count(*) num_tot_logins
			from Landing.mag.log_customer_aud
			group by customer_id) lc
		inner join
			(select distinct customer_id
			from Landing.mag.log_customer) lc2 on lc.customer_id_bk = lc2.customer_id
		inner join
			Landing.aux.gen_dim_customer_aud c on lc.customer_id_bk = c.customer_id_bk


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go
