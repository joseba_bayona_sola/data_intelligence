
use Landing
go

drop procedure mend.lnd_stg_get_po_po_source
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from PO Source in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_po_source
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select pos.po_source po_source_bk, 
		isnull(post.po_source_name, pos.po_source) po_source_name, isnull(post.description, pos.po_source) description, 
		@idETLBatchRun
	from
			(select distinct po_source
			from Landing.map.erp_po_source_aud
			union
			select distinct source
			from Landing.mend.gen_purc_purchaseorder_v
			where source is not null) pos
		left join
			(select po_source, min(po_source_name) po_source_name, min(description) description
			from Landing.map.erp_po_source_aud
			group by po_source) post on pos.po_source = post.po_source

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_po_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from PO Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_po_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select pos.po_type po_type_bk, 
		isnull(post.po_type_name, pos.po_type) po_type_name, isnull(post.description, pos.po_type) description, 
		@idETLBatchRun
	from
			(select distinct po_type
			from Landing.map.erp_po_type_aud
			union
			select distinct potype
			from Landing.mend.gen_purc_purchaseorder_v
			where potype is not null) pos
		left join
			(select po_type, min(po_type_name) po_type_name, min(description) description
			from Landing.map.erp_po_type_aud
			group by po_type) post on pos.po_type = post.po_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_po_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from PO Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_po_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select pos.po_status po_status_bk, 
		isnull(post.po_status_name, pos.po_status) po_status_name, isnull(post.description, pos.po_status) description, 
		@idETLBatchRun
	from
			(select distinct po_status
			from Landing.map.erp_po_status_aud
			union
			select distinct status
			from Landing.mend.gen_purc_purchaseorder_v
			where status is not null) pos
		left join
			(select po_status, min(po_status_name) po_status_name, min(description) description
			from Landing.map.erp_po_status_aud
			group by po_status) post on pos.po_status = post.po_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_pol_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from POL Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_pol_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select pos.pol_status pol_status_bk, 
		isnull(post.pol_status_name, pos.pol_status) pol_status_name, isnull(post.description, pos.pol_status) description, 
		@idETLBatchRun
	from
			(select distinct pol_status
			from Landing.map.erp_pol_status_aud
			union
			select distinct status_lh
			from Landing.mend.gen_purc_purchaseorderline_v
			where status_lh is not null) pos
		left join
			(select pol_status, min(pol_status_name) pol_status_name, min(description) description
			from Landing.map.erp_pol_status_aud
			group by pol_status) post on pos.pol_status = post.pol_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_pol_problems
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from POL Problems in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_pol_problems
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select pos.pol_problems pol_problems_bk, 
		isnull(post.pol_problems_name, pos.pol_problems) pol_problems_name, isnull(post.description, pos.pol_problems) description, 
		@idETLBatchRun
	from
			(select distinct pol_problems
			from Landing.map.erp_pol_problems_aud
			union
			select distinct problems
			from Landing.mend.gen_purc_purchaseorderline_v
			where problems is not null) pos
		left join
			(select pol_problems, min(pol_problems_name) pol_problems_name, min(description) description
			from Landing.map.erp_pol_problems_aud
			group by pol_problems) post on pos.pol_problems = post.pol_problems

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_wh_operator
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Operator in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_wh_operator
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 'N/A' wh_operator_bk, 'N/A' wh_operator_name,  
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_po_purchase_order
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Purchase Order in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_purchase_order
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 1 purchaseorderid_bk, 
		1 warehouseid_bk, 1 supplier_id_bk, 
		'N/A' po_source_bk, 'N/A' po_type_bk, 'N/A' po_status_bk, 'N/A' wh_operator_cr_bk, 'N/A' wh_operator_as_bk, 
		'N/A' purchase_order_number, 1 leadtime, 
		NULL created_date, NULL approved_date, NULL confirmed_date, NULL submitted_date, NULL completed_date, NULL due_date, 
		0 local_total_cost, 0 local_to_global_rate, NULL currency_code,  
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mend.lnd_stg_get_po_purchase_order_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Purchase Order Line in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_po_purchase_order_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 1 purchaseorderlineid_bk, 
		1 purchaseorderid_bk, 'N/A' pol_status_bk, 'N/A' pol_problems_bk, 
		1 stockitemid_bk, 1 supplierpriceid_bk, 
		1 purchase_order_line_id, 
		0 quantity_ordered, 0 quantity_received, 0 quantity_planned, 0 quantity_open, 0 quantity_cancelled, 
		0 local_unit_cost, 0 local_line_cost, 0 local_to_global_rate, NULL currency_code,  
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


