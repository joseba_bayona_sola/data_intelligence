use Landing
go

drop procedure mend.lnd_stg_get_rec_wh_shipment_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Shipment Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_wh_shipment_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select st.wh_shipment_type wh_shipment_type_bk, 
		isnull(mst.wh_shipment_type_name, st.wh_shipment_type) wh_shipment_type_name, isnull(mst.description, st.wh_shipment_type) description, 
		@idETLBatchRun
	from
			(select distinct wh_shipment_type
			from Landing.map.erp_wh_shipment_type_aud
			union
			select distinct shipmentType
			from Landing.mend.gen_whship_receipt_v
			where shipmentType is not null) st
		left join
			(select wh_shipment_type, min(wh_shipment_type_name) wh_shipment_type_name, min(description) description
			from Landing.map.erp_wh_shipment_type_aud
			group by wh_shipment_type) mst on st.wh_shipment_type = mst.wh_shipment_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_rec_receipt_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_receipt_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select rs.receipt_status receipt_status_bk, 
		isnull(mrs.receipt_status_name, rs.receipt_status) receipt_status_name, isnull(mrs.description, rs.receipt_status) description, 
		@idETLBatchRun
	from
			(select distinct receipt_status
			from Landing.map.erp_receipt_status_aud
			union
			select distinct status
			from Landing.mend.gen_whship_receipt_v
			where status is not null) rs
		left join
			(select receipt_status, min(receipt_status_name) receipt_status_name, min(description) description
			from Landing.map.erp_receipt_status_aud
			group by receipt_status) mrs on rs.receipt_status = mrs.receipt_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_rec_receipt_line_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt Line Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_receipt_line_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select rs.receipt_line_status receipt_line_status_bk, 
		isnull(mrs.receipt_line_status_name, rs.receipt_line_status) receipt_line_status_name, isnull(mrs.description, rs.receipt_line_status) description, 
		@idETLBatchRun
	from
			(select distinct receipt_line_status
			from Landing.map.erp_receipt_line_status_aud
			union
			select distinct status_rl
			from Landing.mend.gen_whship_receiptline_1_v
			where status_rl is not null) rs
		left join
			(select receipt_line_status, min(receipt_line_status_name) receipt_line_status_name, min(description) description
			from Landing.map.erp_receipt_line_status_aud
			group by receipt_line_status) mrs on rs.receipt_line_status = mrs.receipt_line_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_rec_receipt_line_sync_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt Line Sync Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_receipt_line_sync_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select rs.receipt_line_sync_status receipt_line_sync_status_bk, 
		isnull(mrs.receipt_line_sync_status_name, rs.receipt_line_sync_status) receipt_line_sync_status_name, isnull(mrs.description, rs.receipt_line_sync_status) description, 
		@idETLBatchRun
	from
			(select distinct receipt_line_sync_status
			from Landing.map.erp_receipt_line_sync_status_aud
			union
			select distinct syncstatus
			from Landing.mend.gen_whship_receiptline_1_v
			where syncstatus is not null) rs
		left join
			(select receipt_line_sync_status, min(receipt_line_sync_status_name) receipt_line_sync_status_name, min(description) description
			from Landing.map.erp_receipt_line_sync_status_aud
			group by receipt_line_sync_status) mrs on rs.receipt_line_sync_status = mrs.receipt_line_sync_status

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_rec_wh_shipment
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from WH Shipment in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_wh_shipment
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 1 shipment_id_bk, 
		1 warehouseid_bk, 1 supplier_id_bk, 'N/A' wh_shipment_type_bk, 
		'N/A' shipment_number, 
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.lnd_stg_get_rec_receipt
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 1 receiptid_bk, 
		1 shipment_id_bk, 'N/A' receipt_status_bk, 
		'N/A' receipt_number, 1 receipt_no, 
		NULL created_date, NULL arrived_date, NULL confirmed_date, NULL stock_registered_date, 
		0 local_total_cost, 
		0 local_to_global_rate, NULL currency_code, 
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




drop procedure mend.lnd_stg_get_rec_receipt_line
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 24-07-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Receipt Line in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_rec_receipt_line
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select 1 receiptlineid_bk, 
		1 receiptid_bk, 'N/A' receipt_line_status_bk, 'N/A' receipt_line_sync_status_bk, 
		1 purchaseorderlineid_bk, 1 stockitemid_bk, 
		NULL created_date, 
		0 qty_ordered, 0 qty_received, 0 qty_accepted, 0 qty_returned, 0 qty_rejected, 
		0 local_unit_cost, 0 local_total_cost, 
		0 local_to_global_rate, NULL currency_code,
		'N' wh_stock_item_batch_f, 
		@idETLBatchRun


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go
