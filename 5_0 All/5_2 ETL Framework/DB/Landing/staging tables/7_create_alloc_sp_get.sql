
use Landing
go

drop procedure mend.lnd_stg_get_alloc_order_type_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Type ERP in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_order_type_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ote.order_type_erp order_type_erp_bk, 
		isnull(mote.order_type_erp_name, ote.order_type_erp) order_type_erp_name, isnull(mote.description, ote.order_type_erp) description, 
		@idETLBatchRun
	from
			(select distinct order_type_erp
			from Landing.map.erp_order_type_erp_aud
			union
			select distinct _type
			from Landing.mend.gen_order_order_v
			where _type is not null) ote
		left join
			(select order_type_erp, min(order_type_erp_name) order_type_erp_name, min(description) description
			from Landing.map.erp_order_type_erp_aud
			group by order_type_erp) mote on ote.order_type_erp = mote.order_type_erp

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_order_status_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Order Status ERP in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_order_status_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ose.order_status_erp order_status_erp_bk, 
		isnull(mose.order_status_erp_name, ose.order_status_erp) order_status_erp_name, isnull(mose.description, ose.order_status_erp) description, 
		@idETLBatchRun
	from
			(select distinct order_status_erp
			from Landing.map.erp_order_status_erp_aud
			union
			select distinct orderStatus
			from Landing.mend.gen_order_order_v
			where orderStatus is not null) ose
		left join
			(select order_status_erp, min(order_status_erp_name) order_status_erp_name, min(description) description
			from Landing.map.erp_order_status_erp_aud
			group by order_status_erp) mose on ose.order_status_erp = mose.order_status_erp

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_shipment_status_erp
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Shipment Status ERP in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_shipment_status_erp
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select sse.shipment_status_erp shipment_status_erp_bk, 
		isnull(msse.shipment_status_erp_name, sse.shipment_status_erp) shipment_status_erp_name, isnull(msse.description, sse.shipment_status_erp) description, 
		@idETLBatchRun
	from
			(select distinct shipment_status_erp
			from Landing.map.erp_shipment_status_erp_aud
			union
			select distinct shipmentStatus
			from Landing.mend.gen_order_order_v
			where shipmentStatus is not null) sse
		left join
			(select shipment_status_erp, min(shipment_status_erp_name) shipment_status_erp_name, min(description) description
			from Landing.map.erp_shipment_status_erp_aud
			group by shipment_status_erp) msse on sse.shipment_status_erp = msse.shipment_status_erp

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_allocation_status
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Allocation Status in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_allocation_status
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ast.allocation_status allocation_status_bk, 
		isnull(mast.allocation_status_name, ast.allocation_status) allocation_status_name, isnull(mast.description, ast.allocation_status) description, 
		@idETLBatchRun
	from
			(select distinct allocation_status
			from Landing.map.erp_allocation_status_aud
			union
			select distinct allocationStatus
			from Landing.mend.gen_order_order_v
			where allocationStatus is not null) ast
		left join
			(select allocation_status, min(allocation_status_name) allocation_status_name, min(description) description
			from Landing.map.erp_allocation_status_aud
			group by allocation_status) mast on ast.allocation_status = mast.allocation_status


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_allocation_strategy
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Allocation Strategy in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_allocation_strategy
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ast.allocation_strategy allocation_strategy_bk, 
		isnull(mast.allocation_strategy_name, ast.allocation_strategy) allocation_strategy_name, isnull(mast.description, ast.allocation_strategy) description, 
		@idETLBatchRun
	from
			(select distinct allocation_strategy
			from Landing.map.erp_allocation_strategy_aud
			union
			select distinct allocationStrategy
			from Landing.mend.gen_order_order_v
			where allocationStrategy is not null) ast
		left join
			(select allocation_strategy, min(allocation_strategy_name) allocation_strategy_name, min(description) description
			from Landing.map.erp_allocation_strategy_aud
			group by allocation_strategy) mast on ast.allocation_strategy = mast.allocation_strategy

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_allocation_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Allocation Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_allocation_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ast.allocation_type allocation_type_bk, 
		isnull(mast.allocation_type_name, ast.allocation_type) allocation_type_name, isnull(mast.description, ast.allocation_type) description, 
		@idETLBatchRun
	from
			(select distinct allocation_type
			from Landing.map.erp_allocation_type_aud
			union
			select distinct allocationType
			from Landing.mend.gen_all_stockallocationtransaction_v
			where allocationType is not null) ast
		left join
			(select allocation_type, min(allocation_type_name) allocation_type_name, min(description) description
			from Landing.map.erp_allocation_type_aud
			group by allocation_type) mast on ast.allocation_type = mast.allocation_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.lnd_stg_get_alloc_allocation_record_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 20-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Allocation Record Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_alloc_allocation_record_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: 
	select ast.allocation_record_type allocation_record_type_bk, 
		isnull(mast.allocation_record_type_name, ast.allocation_record_type) allocation_record_type_name, isnull(mast.description, ast.allocation_record_type) description, 
		@idETLBatchRun
	from
			(select distinct allocation_record_type
			from Landing.map.erp_allocation_record_type_aud
			union
			select distinct recordType
			from Landing.mend.gen_all_stockallocationtransaction_v
			where recordType is not null) ast
		left join
			(select allocation_record_type, min(allocation_record_type_name) allocation_record_type_name, min(description) description
			from Landing.map.erp_allocation_record_type_aud
			group by allocation_record_type) mast on ast.allocation_record_type = mast.allocation_record_type

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

