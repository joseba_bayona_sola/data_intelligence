use Landing
go 


drop procedure mag.lnd_stg_get_prod_product_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select product_type product_type_bk, product_type product_type_name, @idETLBatchRun
	from
		(select distinct product_type
		from Landing.map.prod_product_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

drop procedure mag.lnd_stg_get_prod_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Category in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_category
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Category from category table
	select category category_bk, category category_name, product_type product_type_bk, @idETLBatchRun
	from
		(select distinct product_type, category
		from Landing.map.prod_category) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_cl_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from CL Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_cl_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: CL Types from cl_type table
	select cl_type cl_type_bk, cl_type cl_type_name, @idETLBatchRun
	from
		(select distinct cl_type
		from Landing.map.prod_cl_type) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

drop procedure mag.lnd_stg_get_prod_cl_feature
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from CL Feature in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_cl_feature
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: CL Feature from cl_feature table
	select cl_feature cl_feature_bk, cl_feature cl_feature_name, @idETLBatchRun
	from
		(select distinct cl_feature
		from Landing.map.prod_cl_feature) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_manufacturer
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 31-03-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Manufacturer in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_manufacturer
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Manufacturers from Product Entity Table (Manufacturer) + JOIN with attribute_option_value (manufacturers name)
	select manufacturer manufacturer_bk, manufacturer_name manufacturer_name, @idETLBatchRun
	from
		(select p.manufacturer, m.value manufacturer_name, 
			count(*) over (partition by p.manufacturer) num_rep, 
			rank() over (partition by p.manufacturer order by m.value) r
		from 
				Landing.mag.catalog_product_entity_flat p
			inner join
				Landing.mag.eav_attribute_option_value m on p.manufacturer = m.option_id
		where p.store_id = 0
		group by p.manufacturer, m.value) t
	where num_rep = r
	order by manufacturer_name 

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_product_lifecycle
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Lifecycle in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_lifecycle
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select product_lifecycle product_lifecycle_bk, product_lifecycle product_lifecycle_name, @idETLBatchRun
	from
		(select UPPER(product_lifecycle) product_lifecycle, count(*) num
		from Landing.mag.catalog_product_entity_flat
		where store_id = 0
			and (product_lifecycle is not NULL and product_lifecycle <> '')
		group by product_lifecycle) t
	order by product_lifecycle

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_product_visibility
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Visibility in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_visibility
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select visibility visibility_id_bk, visibility_name product_visibility_name, @idETLBatchRun
	from Landing.map.prod_visibility
	order by visibility

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_glass_vision_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Glass Vision Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_glass_vision_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select product_name glass_vision_type_bk, product_name glass_vision_type_name, @idETLBatchRun
	from
		(select distinct product_name
		from Landing.map.prod_exclude_products
		where info = 'vision type') t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_glass_package_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Glass Package Type in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_glass_package_type
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select product_name glass_package_type_bk, product_name glass_package_type_name, @idETLBatchRun
	from
		(select distinct product_name
		from Landing.map.prod_exclude_products
		where info = 'package type') t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_param_bc
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - BC in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_bc
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select base_curve base_curve_bk, @idETLBatchRun
	from
		(select distinct base_curve
		from Landing.map.prod_param_base_curve) t
	order by base_curve

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_param_di
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - DI in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_di
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select diameter diameter_bk, @idETLBatchRun
	from
		(select distinct diameter
		from Landing.map.prod_param_diameter) t
	order by diameter

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_param_po
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - PO in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_po
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select power power_bk, @idETLBatchRun
	from
		(select distinct power
		from Landing.map.prod_param_power) t
	order by power

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_param_cy
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - CY in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_cy
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select cylinder cylinder_bk, @idETLBatchRun
	from
		(select distinct cylinder
		from Landing.map.prod_param_cylinder) t
	order by cylinder

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_param_ax
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - AX in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_ax
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select axis axis_bk, @idETLBatchRun
	from
		(select distinct axis
		from Landing.map.prod_param_axis) t
	order by axis


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_param_ad
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - AD in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_ad
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select addition addition_bk, @idETLBatchRun
	from
		(select distinct addition
		from Landing.map.prod_param_addition) t
	order by addition

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.lnd_stg_get_prod_param_do
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - DO in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_do
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select dominance dominance_bk, @idETLBatchRun
	from
		(select distinct dominance
		from Landing.map.prod_param_dominance) t
	order by dominance

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_get_prod_param_col
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 04-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Parameter - COL in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_param_col
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: XXXX
	select co colour_bk, co colour_name, @idETLBatchRun
	from
		(select co, count(*) num
		from Landing.mag.edi_stock_item
		where co is not null and co <> ''
		group by co) t
	order by co


	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop view aux.mag_prod_product_family_v 
go

create view aux.mag_prod_product_family_v as

	select p.product_id product_id_bk, pfm.productfamilyid,
		p.manufacturer_bk, cp.category_bk, cp.cl_type_bk, cp.cl_feature_bk, p.product_lifecycle_bk, p.visibility_id_bk, 
		pfg.product_family_group product_family_group_bk, ptoh.product_type_oh_bk,
		p.magento_sku, null product_family_code,
		p.product_family_name, 
		case
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then substring(p.product_family_name, 1, charindex(' In ', p.product_family_name) - 1)
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) = 0) then p.product_family_name
			else null
		end glass_sunglass_name,
		case
			when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then 
				substring(p.product_family_name, charindex(' In ', p.product_family_name) + 4, len(p.product_family_name) - charindex(' In ', p.product_family_name))
			else null
		end glass_sunglass_colour,
		p.status, p.promotional_product, p.telesales_product
	from 
			(select p1.entity_id product_id, 
				p1.manufacturer manufacturer_bk, 
				case 
					when (isnull(p2.product_lifecycle, p1.product_lifecycle) is null or isnull(p2.product_lifecycle, p1.product_lifecycle) = '') then 'REGULAR'
					else UPPER(isnull(p2.product_lifecycle, p1.product_lifecycle)) 
				end product_lifecycle_bk,
				isnull(p2.visibility, p1.visibility) visibility_id_bk,
				p1.sku magento_sku, 
				p1.name product_family_name, 
				isnull(p2.status, p1.status) status,
				case when (p1.promotional_product is NULL or p1.promotional_product = '') then 0 else p1.promotional_product end promotional_product, 
				case when (p1.telesales_only is NULL or p1.telesales_only = '') then 0 else p1.telesales_only end telesales_product
			from 
					Landing.mag.catalog_product_entity_flat p1
				left join
					Landing.mag.catalog_product_entity_flat p2 on p1.entity_id = p2.entity_id and p2.store_id = 20 
			where p1.store_id = 0) p
		left join
			Landing.map.prod_exclude_products ep on p.product_id = ep.product_id
		inner join
			Landing.aux.prod_product_category cp on p.product_id = cp.product_id
		left join
			Landing.map.prod_product_family_group_pf_aud pfg on p.product_id = pfg.product_id
		left join 
			Landing.aux.prod_product_product_type_oh ptoh on p.product_id = ptoh.product_id
		left join
			Landing.mend.gen_prod_productfamily_v pfm on p.product_id = pfm.magentoProductID_int
	where ep.product_id is null
	union
	select product_id_bk, productfamilyid,
		manufacturer_id_bk manufacturer_bk, category_bk, null cl_type_bk, null cl_feature_bk, product_lifecycle_bk, visibility_id_bk, null product_family_group_bk, null product_type_oh_bk,
		magentoProductID magento_sku, null product_family_code,
		name product_family_name, 
		null glass_sunglass_name, null glass_sunglass_colour,
		0 status, 0 promotional_product, 0 telesales_product
	from Landing.map.prod_product_family_erp_aud
go



drop procedure mag.lnd_stg_get_prod_product_family
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 05-04-2017
-- Changed: 
	--	02-06-2017	Joseba Bayona Sola	Add call to SP that creates product - product type oh relationship
	--	26-07-2017	Joseba Bayona Sola	Add call to SP that creates product - product type VAT relationship
	--	16-01-2018	Joseba Bayona Sola	Add product_family_group_bk
	--	08-05-2018	Joseba Bayona Sola	Add product_type_oh_bk
	--	07-06-2018	Joseba Bayona Sola	Change Logic - Get query from View
-- ==========================================================================================
-- Description: Reads data from Product Family in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_family
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Product - Category Relationship: SP inserting data into Auxiliary Table
	exec Landing.mag.lnd_stg_aux_prod_product_category @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Product - Product Type OH Relationship: SP inserting data into Auxiliary Table
	exec Landing.mag.lnd_stg_aux_prod_product_product_type_oh @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun

	-- Product - Product Type VAT Relationship: SP inserting data into Auxiliary Table
	exec Landing.mag.lnd_stg_aux_prod_product_product_type_vat @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun


	-- Products: Filter Excluded Products + Take needed attributes + Prepare Attributes --> To Temp Table
	--select p1.entity_id product_id, 
	--	p1.manufacturer manufacturer_bk, 
	--	case 
	--		when (isnull(p2.product_lifecycle, p1.product_lifecycle) is null or isnull(p2.product_lifecycle, p1.product_lifecycle) = '') then 'REGULAR'
	--		else UPPER(isnull(p2.product_lifecycle, p1.product_lifecycle)) 
	--	end product_lifecycle_bk,
	--	isnull(p2.visibility, p1.visibility) visibility_id_bk,
	--	p1.sku magento_sku, 
	--	p1.name product_family_name, 
	--	isnull(p2.status, p1.status) status,
	--	case when (p1.promotional_product is NULL or p1.promotional_product = '') then 0 else p1.promotional_product end promotional_product, 
	--	case when (p1.telesales_only is NULL or p1.telesales_only = '') then 0 else p1.telesales_only end telesales_product
	--into 
	--	#magento_products
	--from 
	--		Landing.mag.catalog_product_entity_flat p1
	--	left join
	--		Landing.mag.catalog_product_entity_flat p2 on p1.entity_id = p2.entity_id and p2.store_id = 20 
	--where p1.store_id = 0 
	--order by p1.entity_id

	---- Products: Final SELECT where bk attributes are taken (JOIN TO TABLES)
	--select p.product_id product_id_bk, 
	--	p.manufacturer_bk, cp.category_bk, cp.cl_type_bk, cp.cl_feature_bk, p.product_lifecycle_bk, p.visibility_id_bk, pfg.product_family_group product_family_group_bk, ptoh.product_type_oh_bk,
	--	p.magento_sku, null product_family_code,
	--	p.product_family_name, 
	--	case
	--		when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then substring(p.product_family_name, 1, charindex(' In ', p.product_family_name) - 1)
	--		when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) = 0) then p.product_family_name
	--		else null
	--	end glass_sunglass_name,
	--	case
	--		when (cp.category_bk in ('Glasses', 'Sunglasses') and charindex(' In ', p.product_family_name) > 0) then 
	--			substring(p.product_family_name, charindex(' In ', p.product_family_name) + 4, len(p.product_family_name) - charindex(' In ', p.product_family_name))
	--		else null
	--	end glass_sunglass_colour,
	--	p.status, p.promotional_product, p.telesales_product, @idETLBatchRun
	--from 
	--		#magento_products p
	--	left join
	--		Landing.map.prod_exclude_products ep on p.product_id = ep.product_id
	--	inner join
	--		Landing.aux.prod_product_category cp on p.product_id = cp.product_id
	--	left join
	--		Landing.map.prod_product_family_group_pf_aud pfg on p.product_id = pfg.product_id
	--	left join 
	--		Landing.aux.prod_product_product_type_oh ptoh on p.product_id = ptoh.product_id
	--where ep.product_id is null
	--order by p.product_id

	select product_id_bk, 
		manufacturer_bk, category_bk, cl_type_bk, cl_feature_bk, product_lifecycle_bk, visibility_id_bk, product_family_group_bk, product_type_oh_bk,
		magento_sku, null product_family_code,
		product_family_name, 
		glass_sunglass_name, glass_sunglass_colour,
		status, promotional_product, telesales_product, @idETLBatchRun
	from Landing.aux.mag_prod_product_family_v

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_aux_prod_product_category
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 05-04-2017
-- Changed: 
	--	25-08-2017	Joseba Bayona Sola	Use of map.product_category_rel in order to set up categories using map file data for products without category rel. in Magento
	--	24-10-2017	Joseba Bayona Sola	Manual hardcoded update in order update swimming glasses products to Other Category
-- ==========================================================================================
-- Description: Relates Products with Categories and puts them in an auxiliar table
-- ==========================================================================================

create procedure mag.lnd_stg_aux_prod_product_category
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Product - Category Relationship --> New SP inserting data into Auxiliary Table 

	-- Deletes current values from Aux Table
	delete from Landing.aux.prod_product_category

	-- Creates TEMP Table - #catalog_category_entity_path
	-- Magento Categories from Parent - Child Redundant Configuration to Flat
	select d1.entity_id category_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
	into #catalog_category_entity_path
    from 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d1 
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d2 ON d1.parent_id = d2.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d3 ON d2.parent_id = d3.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d4 ON d3.parent_id = d4.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d5 ON d4.parent_id = d5.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d6 ON d5.parent_id = d6.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d7 ON d6.parent_id = d7.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat
			where store_id = 0) d8 ON d7.parent_id = d8.entity_id
	order by d1.entity_id;

	-- Creates TEMP Table - #product_magento_category
	-- Product relationship with diferent magento categories
	-- Product related to Flatten Categories (could be many) + Unpivot + Reduce Combinations by Group by
	select product_id, name, category_name, count(*) num
	into #product_magento_category
	from
		(select product_id, name, category_num, category_name
		from
				(SELECT p.entity_id product_id, p.name,
					cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
				FROM 
						(select entity_id, name, product_type, telesales_only
						from Landing.mag.catalog_product_entity_flat
						where store_id = 0) p 
					LEFT JOIN 
						Landing.mag.catalog_category_product cpc ON p.entity_id = cpc.product_id
					LEFT JOIN 
						Landing.mag.catalog_category_entity c ON cpc.category_id = c.entity_id 
					LEFT JOIN 
						#catalog_category_entity_path cp ON c.entity_id = cp.category_id) t
			unpivot
				(category_name for category_num in 
					(name1, name2, name3, name4, name5, name6, name7, name8)
				) unpvt) t
	group by product_id, name, category_name

	-- RELATION: Product - DWH Category relation using Map Table
	insert into Landing.aux.prod_product_category(product_id, category_bk) 
		select p.entity_id product_id, -- isnull(pc.category, 'Other') category_bk
			isnull(isnull(pc.category, pc2.category), 'Other') category_bk
		from 
				Landing.mag.catalog_product_entity_flat p
			left join
				(select product_id, name, category
				from
					(select product_id, name, category,
						count(*) over (partition by product_id) num_rep, 
						rank() over (partition by product_id order by category) num_ord
					from
						(select distinct t1.product_id, t1.name, t2.category 
						from
								#product_magento_category t1
							inner join
								Landing.map.prod_category t2 on t1.category_name = t2.category_magento) t) t
				where num_ord = 1) pc on p.entity_id = pc.product_id
			left join
				Landing.map.prod_product_category_rel_aud pcr on p.entity_id = pcr.product_id
			left join
				Landing.map.prod_category pc2 on pcr.category_magento = pc2.category_magento
		where p.store_id = 0
		order by product_id;
	
	-- Manual update for Swimming Glasses Products to relate them to Other instead of Glasses
	update Landing.aux.prod_product_category
	set category_bk = 'Other'
	where product_id in (2326, 2331)
	
	-- Creates TEMP Table - #product_cl_info
	-- RELATION: Product - DWH CL Type relation using Map Table
	-- RELATION: Product - DWH CL Feature relation using Map Table
	select p.product_id, 
		isnull(pc.cl_type, 'Spherical') cl_type_bk, 
		case when pcol.product_id is not null then 'Colour' else isnull(pc2.cl_feature, 'Standard') end cl_feature_bk
	into #product_cl_info
	from 
			Landing.aux.prod_product_category p
		left join
			(select product_id, name, cl_type
			from
				(select product_id, name, cl_type,
					count(*) over (partition by product_id) num_rep, 
					rank() over (partition by product_id order by len(cl_type) desc) num_ord
				from
					(select distinct t1.product_id, t1.name, t2.cl_type 
					from
							#product_magento_category t1
						inner join
							Landing.map.prod_cl_type t2 on t1.category_name = t2.cl_type_magento) t) t
			where num_ord = 1) pc on p.product_id = pc.product_id
		left join
			(select product_id, name, cl_feature
			from
				(select product_id, name, cl_feature,
					count(*) over (partition by product_id) num_rep, 
					rank() over (partition by product_id order by len(cl_feature) desc) num_ord
				from
					(select distinct t1.product_id, t1.name, t2.cl_feature 
					from
							#product_magento_category t1
						inner join
							Landing.map.prod_cl_feature t2 on t1.category_name = t2.cl_feature_magento) t) t
			where num_ord = 1) pc2 on p.product_id = pc2.product_id
		left join
			Landing.map.prod_product_colour pcol on p.product_id = pcol.product_id
	where p.category_bk in ('CL - Daily', 'CL - Two Weeklies', 'CL - Monthlies', 'CL - Yearly')
	order by p.product_id;

	-- MERGE: Update Aux table with CL Type and CL Feature 
	merge into Landing.aux.prod_product_category as trg
	using #product_cl_info src
		on (trg.product_id = src.product_id)
	when matched then
		update set
			trg.cl_type_bk = src.cl_type_bk, 
			trg.cl_feature_bk = src.cl_feature_bk;

	-- Drop Temp Tables
	drop table #product_cl_info
	drop table #product_magento_category
	drop table #catalog_category_entity_path

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.lnd_stg_aux_prod_product_product_type_oh
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	29-08-2017	Joseba Bayona Sola	Yearly added and CL abbreviation
-- ==========================================================================================
-- Description: Relates Products with Product Type OH and puts them in an auxiliar table
-- ==========================================================================================

create procedure mag.lnd_stg_aux_prod_product_product_type_oh
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Deletes current values from Aux Table
	delete from Landing.aux.prod_product_product_type_oh


	insert into Landing.aux.prod_product_product_type_oh(product_id, product_type_oh_bk)
		select product_id, 
			case when (cl_feature_bk = 'Colour') then cl_feature_bk else
				case when (category_bk in ('CL - Daily', 'CL - Two Weeklies', 'CL - Monthlies', 'CL - Yearly', 'Glasses', 'Sunglasses')) then category_bk else 'Other' end end product_type_oh
		from Landing.aux.prod_product_category

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.lnd_stg_aux_prod_product_product_type_vat
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 25-07-2017
-- Changed: 
	--	29-08-2017	Joseba Bayona Sola	Yearly added and CL abbreviation
-- ==========================================================================================
-- Description: Relates Products with Product Type VAT and puts them in an auxiliar table
-- ==========================================================================================

create procedure mag.lnd_stg_aux_prod_product_product_type_vat
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Deletes current values from Aux Table
	delete from Landing.aux.prod_product_product_type_vat


	insert into Landing.aux.prod_product_product_type_vat(product_id, product_type_vat)
		select product_id, 
			case 
				when (cl_feature_bk = 'Colour') then 'Colour Plano' 
				when (category_bk in ('CL - Daily', 'CL - Two Weeklies', 'CL - Monthlies', 'CL - Yearly')) then 'Contact Lenses'
				when (category_bk in ('Glasses', 'Sunglasses')) then category_bk 
				when (category_bk like 'SL%' or category_bk like 'EC%') then 'Solutions & Eye Care'
				else 'Other' end product_type_vat
		from Landing.aux.prod_product_category

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.lnd_stg_get_prod_product_type_oh
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Type OH in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_type_oh
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select product_type_oh product_type_oh_bk, product_type_oh product_type_oh_name, @idETLBatchRun
	from
		(select distinct product_type_oh
		from Landing.map.prod_product_type_oh) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.lnd_stg_get_prod_product_family_group
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Type OH in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mag.lnd_stg_get_prod_product_family_group
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Family Group from product_family_group table
	select product_family_group product_family_group_bk, product_family_group_name product_family_group_name, @idETLBatchRun
	from
		(select product_family_group, min(product_family_group_name) product_family_group_name
		from Landing.map.prod_product_family_group
		group by product_family_group) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mend.lnd_stg_get_prod_product_family_pack_size
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Family Pack Size in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_prod_product_family_pack_size
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select pfps.packsizeid, 
		pf.product_id_bk, convert(int, pfps.size) size, pfps.description product_family_packsize_name, 
		pfps.allocationPreferenceOrder allocation_preference_order, pfps.purchasingPreferenceOrder purchasing_preference_order, 
		@idETLBatchRun
	from 
			Landing.mend.gen_prod_productfamilypacksize_v pfps
		inner join
			Landing.aux.mag_prod_product_family_v pf on pfps.productfamilyid = pf.productfamilyid
	where packsizeid is not null

		
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mend.lnd_stg_get_prod_price_type_pf
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Price Type PF in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_prod_price_type_pf
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select subMetaObjectName price_type_pf_bk, subMetaObjectName price_type_pf_name, @idETLBatchRun
	from
		(select distinct subMetaObjectName
		from Landing.mend.gen_prod_productfamilypacksize_supplierprice_v
		where subMetaObjectName is not null) t

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mend.lnd_stg_get_prod_product_family_price
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product Family Price in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_prod_product_family_price
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select pfpssp.supplierpriceid supplierpriceid_bk, 
		pfpssp.supplier_id supplier_id_bk, pfpssp.packsizeid packsizeid_bk, pfpssp.subMetaObjectName price_type_pf_bk, 
		pfpssp.unitPrice unit_price, pfpssp.currencyCode currency_code, pfpssp.leadTime lead_time, 
		pfpssp.effectiveDate effective_date, pfpssp.expiryDate expiry_date, pfpssp.active, 
		@idETLBatchRun
	from 
			Landing.mend.gen_prod_productfamilypacksize_supplierprice_v pfpssp
		inner join
			Landing.aux.mag_prod_product_family_v pf on pfpssp.productfamilyid = pf.productfamilyid
	where pfpssp.supplierpriceid is not null
		
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mend.lnd_stg_get_prod_product
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Product in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_prod_product
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select p.productid product_id_erp_bk, pf.product_id_bk,
		bc base_curve_bk, 
		case 
			when (len(di) = 2) then di + '.0'
			else di 
		end diameter_bk, 
		case 
			when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
			when (len(po) = 3) then po + '.00'
			when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
			when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
			when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
			when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
			else po 
		end power_bk, 
		cy cylinder_bk, 
		case 
			when (len(ax) = 1) then '00' + ax
			when (len(ax) = 2) then '0' + ax
			else ax 
		end axis_bk, 
		case 
			when ad in ('Medium', 'MF', 'MID') then 'MED'
			else ad 
		end addition_bk, 
		_do dominance_bk, co colour_bk, 
		p.SKU_product parameter, p.description product_description, @idETLBatchRun
	
	from 
			Landing.mend.gen_prod_product_v p
		inner join
			Landing.aux.mag_prod_product_family_v pf on p.productfamilyid = pf.productfamilyid
	where productid is not null

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mend.lnd_stg_get_prod_stock_item
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Stock Item in a Landing tables and inserts data in Staging table
-- ==========================================================================================

create procedure mend.lnd_stg_get_prod_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- SELECT STATEMENT: Product Types from product_type table
	select si.stockitemid stockitemid_bk, 
		si.productid productid_erp_bk, si.packSize packsize, 
		si.SKU, si.stockitemdescription stock_item_description, 
		@idETLBatchRun
	from 
			Landing.mend.gen_prod_stockitem_v si
		inner join
			Landing.aux.mag_prod_product_family_v pf on si.productfamilyid = pf.productfamilyid
	where si.stockitemid is not null
		
	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 