
use Landing
go

select store_name, tld, store_type, 
	idETLBatchRun, ins_ts
from Landing.map.gen_store_type

select *
from 
	Landing.map.gen_store_type t1
inner join
	Landing.map.gen_store_type t2 on (isnull(t1.store_name, '') = isnull(t2.store_name, '')) and (isnull(t1.tld, '') = isnull(t2.tld, ''))

select store_name, tld, store_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_store_type_aud



select store_name, name, website_group, 
	idETLBatchRun, ins_ts
from Landing.map.gen_website_group

select store_name, name, website_group, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.gen_website_group_aud
