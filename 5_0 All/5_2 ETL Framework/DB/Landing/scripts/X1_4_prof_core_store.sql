use Landing
go 

select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts
from Landing.mag.core_store

select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.mag.core_store_aud

select store_id, code, website_id, group_id, name, sort_order, is_active, 
	idETLBatchRun, ins_ts, 
	aud_type, aud_dateFrom, aud_dateTo
from Landing.mag.core_store_aud_hist
