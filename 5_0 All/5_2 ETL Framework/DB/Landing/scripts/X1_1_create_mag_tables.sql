use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.mag.core_store
create table mag.core_store(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go 

alter table mag.core_store add constraint [PK_mag_core_store]
	primary key clustered (store_id);
go

alter table mag.core_store add constraint [DF_mag_core_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.core_store_aud
create table mag.core_store_aud(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	upd_ts			datetime);
go 

alter table mag.core_store_aud add constraint [PK_mag_core_store_aud]
	primary key clustered (store_id);
go

alter table mag.core_store_aud add constraint [DF_mag_core_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.mag.core_store_aud_hist
create table mag.core_store_aud_hist(
	store_id		int NOT NULL, 
	code			varchar(32), 
	website_id		int NOT NULL, 
	group_id		int NOT NULL, 
	name			varchar(255) NOT NULL, 
	sort_order		int NOT NULL, 
	is_active		int	NOT NULL, 
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL, 
	aud_type		char(1) NOT NULL, 
	aud_dateFrom	datetime NOT NULL, 
	aud_dateTo		datetime NOT NULL);
go 

alter table mag.core_store_aud_hist add constraint [DF_mag_core_store_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
