use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.mag.trg_core_store
drop trigger mag.trg_core_store;

create trigger mag.trg_core_store on mag.core_store
after insert
as
begin
	set nocount on

	merge into mag.core_store_aud with (tablock) as trg
	using inserted src
		on (trg.store_id = src.store_id)
	when matched and
		(isnull(trg.code, '') <> isnull(src.code, '')) and (isnull(trg.name, '') <> isnull(src.name, '')) and
		(isnull(trg.website_id, 0) <> isnull(src.website_id, 0)) and (isnull(trg.group_id, 0) <> isnull(src.group_id, 0)) and
		(isnull(trg.sort_order, 0) <> isnull(src.sort_order, 0)) and (isnull(trg.is_active, 0) <> isnull(src.is_active, 0))
		then
			update set
				trg.code = src.code, trg.name = src.name, 
				trg.website_id = src.website_id, trg.group_id = src.group_id, 
				trg.sort_order = src.sort_order, trg.is_active = src.is_active,
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun)
				values (src.store_id, src.code, src.name, src.website_id, src.group_id, src.sort_order, src.is_active, src.idETLBatchRun);
end; 
go

-- Landing.mag.core_store_aud_trg
drop trigger mag.trg_core_store_aud;

create trigger mag.trg_core_store_aud on mag.core_store_aud
for update, delete
as
begin
	set nocount on

	insert mag.core_store_aud_hist (store_id, code, name, website_id, group_id, sort_order, is_active, idETLBatchRun,
		aud_type, aud_dateFrom, aud_dateTo)
		
		select d.store_id, d.code, d.name, d.website_id, d.group_id, d.sort_order, d.is_active, d.idETLBatchRun,
			case when (i.store_id is null) then 'D' else 'U' end aud_type, d.ins_ts aud_dateFrom, i.ins_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.store_id = i.store_id;
end;
go			 