
use Landing
go

drop procedure dbo.test_Mendix_db
go 

create procedure dbo.test_Mendix_db
	@idETLBatchRun bigint, @idPackageRun bigint,
	@test_flag int OUTPUT
as

begin
	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @test_flag = 0

	begin try
		exec sp_testlinkedserver N'MENDIX_DB_RESTORE'; 
		set @test_flag = 1

		-- set @test_flag = 1 / 0 -- ERROR TESTING PURPOSES
	end try 

	begin catch
		set @test_flag = 0
	end catch 

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

	return @test_flag;
end
go


------------------------------------------------------------

-- declare @test_flag INT = 10

-- print 'Result is' + convert(varchar(10), @test_flag)

-- exec dbo.test_Mendix_db @test_flag = @test_flag OUTPUT

-- print 'Result is' + convert(varchar(10), @test_flag)
