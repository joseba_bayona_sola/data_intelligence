use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.map_gen_store_type
create table map.gen_store_type(
	store_name		varchar(255) NULL,
	tld				varchar(255) NULL,
	store_type		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_store_type add constraint [DF_map_gen_store_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.map_gen_store_type_aud
create table map.gen_store_type_aud(
	store_name		varchar(255) NULL,
	tld				varchar(255) NULL,
	store_type		varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_store_type_aud add constraint [DF_map_gen_store_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.map_gen_website_group
create table map.gen_website_group(
	store_name		varchar(255) NULL,
	name			varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table map.gen_website_group add constraint [DF_map_gen_website_group_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.map_gen_website_group_aud
create table map.gen_website_group_aud(
	store_name		varchar(255) NULL,
	name			varchar(255) NULL,
	website_group	varchar(255) NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL,
	upd_ts			datetime);
go

alter table map.gen_website_group_aud add constraint [DF_map_gen_website_group_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

