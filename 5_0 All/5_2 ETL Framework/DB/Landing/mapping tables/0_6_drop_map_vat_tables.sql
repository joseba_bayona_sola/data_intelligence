use Landing
go 

drop table map.vat_countries_registered
go 
drop table map.vat_countries_registered_aud
go 

drop table map.vat_vat_type_rate
go 
drop table map.vat_vat_type_rate_aud
go 

drop table map.vat_product_type_vat
go 
drop table map.vat_product_type_vat_aud
go 



drop table map.vat_vat_rate
go 
drop table map.vat_vat_rate_aud
go 

drop table map.vat_prof_fee_rate
go 
drop table map.vat_prof_fee_rate_aud
go 


drop table map.vat_prof_fee_fixed_value
go 
drop table map.vat_prof_fee_fixed_value_aud
go 



drop table map.vat_countries_registered_store
go 
drop table map.vat_countries_registered_store_aud
go 
