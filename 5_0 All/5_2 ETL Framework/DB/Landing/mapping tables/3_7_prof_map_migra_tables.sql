use Landing
go

-- migra_website
select migra_website_name, etl_name, description,
	idETLBatchRun, ins_ts
from Landing.map.migra_website

select migra_website_name, etl_name, description,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.migra_website_aud



-- migra_order_status
select orderstatusname, description, status, 
	order_type, invoice_type, shipment_type,
	idETLBatchRun, ins_ts
from Landing.map.migra_order_status

select orderstatusname, description, status, 
	order_type, invoice_type, shipment_type,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.migra_order_status_aud



-- migra_website_proforma
select website_group, proforma_date,
	idETLBatchRun, ins_ts
from Landing.map.migra_website_proforma

select website_group, proforma_date,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.migra_website_proforma_aud

