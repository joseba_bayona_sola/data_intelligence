use Landing
go 

drop table map.act_activity_group_type
go 
drop table map.act_activity_group_type_aud
go 


drop table map.act_migrate_store_date
go 
drop table map.act_migrate_store_date_aud
go 


drop table map.act_lapsed_num_days
go 


drop table map.act_website_migrate_date
go 
drop table map.act_website_migrate_date_aud
go 


drop table map.act_website_register_mapping
go 
drop table map.act_website_register_mapping_aud
go 


drop table map.act_website_old_access_cust_no
go 
drop table map.act_website_old_access_cust_no_aud
go 


drop table map.act_rank_cr_time_mm
go 
drop table map.act_rank_cr_time_mm_aud
go 
