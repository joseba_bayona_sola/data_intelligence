use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_sales_order_stage
drop trigger map.trg_sales_order_stage;
go 

create trigger map.trg_sales_order_stage on map.sales_order_stage
after insert
as
begin
	set nocount on

	merge into map.sales_order_stage_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_stage, '') = isnull(src.order_stage, '')) 
	when matched and
		(isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_stage, description, idETLBatchRun)
				values (src.order_stage, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_sales_order_status
drop trigger map.trg_sales_order_status;
go 

create trigger map.trg_sales_order_status on map.sales_order_status
after insert
as
begin
	set nocount on

	merge into map.sales_order_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_status, '') = isnull(src.order_status, '')) 
	when matched and
		(isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_status, description, idETLBatchRun)
				values (src.order_status, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_sales_order_type
drop trigger map.trg_sales_order_type;
go 

create trigger map.trg_sales_order_type on map.sales_order_type
after insert
as
begin
	set nocount on

	merge into map.sales_order_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_type, '') = isnull(src.order_type, '')) 
	when matched and
		((isnull(trg.description, '') <> isnull(src.description, '')))
		then 
			update set
				trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_type, description, idETLBatchRun)
				values (src.order_type, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_sales_order_status_magento
drop trigger map.trg_sales_order_status_magento;
go 

create trigger map.trg_sales_order_status_magento on map.sales_order_status_magento
after insert
as
begin
	set nocount on

	merge into map.sales_order_status_magento_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_status_magento_code, '') = isnull(src.order_status_magento_code, '')) 
	when matched and
		((isnull(trg.order_status_magento, '') <> isnull(src.order_status_magento, '')) or (isnull(trg.description, '') <> isnull(src.description, '')))
		then 
			update set
				trg.order_status_magento = src.order_status_magento, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_status_magento, order_status_magento_code, description, idETLBatchRun)
				values (src.order_status_magento, src.order_status_magento_code, src.description, src.idETLBatchRun);
end;
go 






-- Landing.map.trg_sales_payment_method
drop trigger map.trg_sales_payment_method;
go 

create trigger map.trg_sales_payment_method on map.sales_payment_method
after insert
as
begin
	set nocount on

	merge into map.sales_payment_method_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.payment_method_code, '') = isnull(src.payment_method_code, '')) 
	when matched and
		((isnull(trg.payment_method, '') <> isnull(src.payment_method, '')) or (isnull(trg.description, '') <> isnull(src.description, '')) or 
			(isnull(trg.payment_method_name, '') <> isnull(src.payment_method_name, '')))
		then 
			update set
				trg.payment_method = src.payment_method, trg.description = src.description, 
				trg.payment_method_name = src.payment_method_name, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (payment_method, payment_method_code, description, payment_method_name, idETLBatchRun)
				values (src.payment_method, src.payment_method_code, src.description, src.payment_method_name, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_sales_cc_type
drop trigger map.trg_sales_cc_type;
go 

create trigger map.trg_sales_cc_type on map.sales_cc_type
after insert
as
begin
	set nocount on

	merge into map.sales_cc_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.card_type_code, '') = isnull(src.card_type_code, '')) 
	when matched and
		(isnull(trg.cc_type, '') <> isnull(src.cc_type, ''))
		then 
			update set
				trg.cc_type = src.cc_type, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (cc_type, card_type_code, idETLBatchRun)
				values (src.cc_type, src.card_type_code, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_sales_shipping_method
drop trigger map.trg_sales_shipping_method;
go 

create trigger map.trg_sales_shipping_method on map.sales_shipping_method
after insert
as
begin
	set nocount on

	merge into map.sales_shipping_method_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.shipping_method_code, '') = isnull(src.shipping_method_code, '')) 
	when matched and
		((isnull(trg.shipping_method, '') <> isnull(src.shipping_method, '')) or (isnull(trg.shipping_method_code_erp, '') <> isnull(src.shipping_method_code_erp, '')) or
		(isnull(trg.description, '') <> isnull(src.description, '')))

		then 
			update set
				trg.shipping_method = src.shipping_method, trg.shipping_method_code_erp = src.shipping_method_code_erp, 
				trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (shipping_method, shipping_method_code, shipping_method_code_erp, description, idETLBatchRun)
				values (src.shipping_method, src.shipping_method_code, src.shipping_method_code_erp, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_sales_price_type
drop trigger map.trg_sales_price_type;
go 

create trigger map.trg_sales_price_type on map.sales_price_type
after insert
as
begin
	set nocount on

	merge into map.sales_price_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.price_type, '') = isnull(src.price_type, '')) 
	when matched and
		((isnull(trg.description, '') <> isnull(src.description, '')) or (isnull(trg.priority, 0) <> isnull(src.priority, 0)))
		then 
			update set
				trg.description = src.description, trg.priority = src.priority, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (price_type, description, priority, idETLBatchRun)
				values (src.price_type, src.description, src.priority, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_sales_channel
drop trigger map.trg_sales_channel;
go 

create trigger map.trg_sales_channel on map.sales_channel
after insert
as
begin
	set nocount on

	merge into map.sales_channel_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.channel, '') = isnull(src.channel, '')) 
	when matched and
		((isnull(trg.channel_name, '') <> isnull(src.channel_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))) 
		then 
			update set
				trg.channel_name = src.channel_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (channel, channel_name, description, idETLBatchRun)
				values (src.channel, src.channel_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_sales_marketing_channel
drop trigger map.trg_sales_marketing_channel;
go 

create trigger map.trg_sales_marketing_channel on map.sales_marketing_channel
after insert
as
begin
	set nocount on

	merge into map.sales_marketing_channel_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.marketing_channel, '') = isnull(src.marketing_channel, '')) 
	when matched and
		((isnull(trg.marketing_channel_name, '') <> isnull(src.marketing_channel_name, '')) or 
		(isnull(trg.sup_marketing_channel, '') <> isnull(src.sup_marketing_channel, '')) or (isnull(trg.description, '') <> isnull(src.description, '')) or 
		(isnull(trg.ga, '') <> isnull(src.ga, '')) or (isnull(trg.bc, '') <> isnull(src.bc, '')) or (isnull(trg.cc, '') <> isnull(src.cc, '')) or (isnull(trg.ext, '') <> isnull(src.ext, ''))) 
		then 
			update set
				trg.marketing_channel_name = src.marketing_channel_name, 
				trg.sup_marketing_channel = src.sup_marketing_channel, trg.description = src.description, 
				trg.ga = src.ga, trg.bc = src.bc, trg.cc = src.cc, trg.ext = src.ext, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (marketing_channel, marketing_channel_name, sup_marketing_channel, description, 
				ga, bc, cc, ext, idETLBatchRun)
				
				values (src.marketing_channel, src.marketing_channel_name, src.sup_marketing_channel, src.description, 
					src.ga, src.bc, src.cc, src.ext, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_sales_affiliate
drop trigger map.trg_sales_affiliate;
go 

create trigger map.trg_sales_affiliate on map.sales_affiliate
after insert
as
begin
	set nocount on

	merge into map.sales_affiliate_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.publisher_id, 0) = isnull(src.publisher_id, 0)) 
	when matched and
		((isnull(trg.affiliate_name, '') <> isnull(src.affiliate_name, '')) or (isnull(trg.affiliate_website, 0) <> isnull(src.affiliate_website, 0)) or
		(isnull(trg.affiliate_network, '') <> isnull(src.affiliate_network, '')) or (isnull(trg.affiliate_type, 0) <> isnull(src.affiliate_type, 0)) or
		(isnull(trg.affiliate_status, '') <> isnull(src.affiliate_status, '')))

		then 
			update set
				trg.affiliate_name = src.affiliate_name, trg.affiliate_website = src.affiliate_website, 
				trg.affiliate_network = src.affiliate_network, trg.affiliate_type = src.affiliate_type, trg.affiliate_status = src.affiliate_status, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (publisher_id, affiliate_name, affiliate_website, affiliate_network, affiliate_type, affiliate_status, idETLBatchRun)
				values (src.publisher_id, src.affiliate_name, src.affiliate_website, src.affiliate_network, src.affiliate_type, src.affiliate_status, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_sales_prescription_method
drop trigger map.trg_sales_prescription_method;
go 

create trigger map.trg_sales_prescription_method on map.sales_prescription_method
after insert
as
begin
	set nocount on

	merge into map.sales_prescription_method_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.prescription_method, '') = isnull(src.prescription_method, '')) 
	when matched and
		((isnull(trg.description, '') <> isnull(src.description, '')))

		then 
			update set
				trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (prescription_method, description, idETLBatchRun)
				values (src.prescription_method, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_sales_rank_customer_order_seq_no
drop trigger map.trg_sales_rank_customer_order_seq_no;
go 

create trigger map.trg_sales_rank_customer_order_seq_no on map.sales_rank_customer_order_seq_no
after insert
as
begin
	set nocount on

	merge into map.sales_rank_customer_order_seq_no_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.min_rank, 0) = isnull(src.min_rank, 0)) 
	when matched and
		((isnull(trg.max_rank, 0) <> isnull(src.max_rank, 0)) or (isnull(trg.rank_seq_no, '') <> isnull(src.rank_seq_no, '')))

		then 
			update set
				trg.max_rank = src.max_rank, trg.rank_seq_no = src.rank_seq_no, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (min_rank, max_rank, rank_seq_no, idETLBatchRun)
				values (src.min_rank, src.max_rank, src.rank_seq_no, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_sales_rank_qty_time
drop trigger map.trg_sales_rank_qty_time;
go 

create trigger map.trg_sales_rank_qty_time on map.sales_rank_qty_time
after insert
as
begin
	set nocount on

	merge into map.sales_rank_qty_time_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.min_rank, 0) = isnull(src.min_rank, 0)) 
	when matched and
		((isnull(trg.max_rank, 0) <> isnull(src.max_rank, 0)) or (isnull(trg.rank_qty_time, '') <> isnull(src.rank_qty_time, '')))

		then 
			update set
				trg.max_rank = src.max_rank, trg.rank_qty_time = src.rank_qty_time, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (min_rank, max_rank, rank_qty_time, idETLBatchRun)
				values (src.min_rank, src.max_rank, src.rank_qty_time, src.idETLBatchRun);
end;
go 





-- Landing.map.trg_sales_rank_freq_time
drop trigger map.trg_sales_rank_freq_time;
go 

create trigger map.trg_sales_rank_freq_time on map.sales_rank_freq_time
after insert
as
begin
	set nocount on

	merge into map.sales_rank_freq_time_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.min_rank, 0) = isnull(src.min_rank, 0)) 
	when matched and
		((isnull(trg.max_rank, 0) <> isnull(src.max_rank, 0)) or (isnull(trg.rank_freq_time, '') <> isnull(src.rank_freq_time, '')))

		then 
			update set
				trg.max_rank = src.max_rank, trg.rank_freq_time = src.rank_freq_time, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (min_rank, max_rank, rank_freq_time, idETLBatchRun)
				values (src.min_rank, src.max_rank, src.rank_freq_time, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_sales_rank_shipping_days
drop trigger map.trg_sales_rank_shipping_days;
go 

create trigger map.trg_sales_rank_shipping_days on map.sales_rank_shipping_days
after insert
as
begin
	set nocount on

	merge into map.sales_rank_shipping_days_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.min_rank, 0) = isnull(src.min_rank, 0)) 
	when matched and
		((isnull(trg.max_rank, 0) <> isnull(src.max_rank, 0)) or (isnull(trg.rank_shipping_days, '') <> isnull(src.rank_shipping_days, '')))

		then 
			update set
				trg.max_rank = src.max_rank, trg.rank_shipping_days = src.rank_shipping_days, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (min_rank, max_rank, rank_shipping_days, idETLBatchRun)
				values (src.min_rank, src.max_rank, src.rank_shipping_days, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_sales_summer_time_period
drop trigger map.trg_sales_summer_time_period;
go 

create trigger map.trg_sales_summer_time_period on map.sales_summer_time_period
after insert
as
begin
	set nocount on

	merge into map.sales_summer_time_period_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.year, 0) = isnull(src.year, 0)) 
	when matched and
		((isnull(trg.from_date, '') <> isnull(src.from_date, 0)) or (isnull(trg.to_date, '') <> isnull(src.to_date, '')))

		then 
			update set
				trg.from_date = src.from_date, trg.to_date = src.to_date, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (year, from_date, to_date, idETLBatchRun)
				values (src.year, src.from_date, src.to_date, src.idETLBatchRun);
end;
go 

