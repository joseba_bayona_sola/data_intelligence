use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.migra_website
create table map.migra_website(
	migra_website_name		varchar(20) NULL,
	etl_name				varchar(10) NULL,
	description				varchar(255) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.migra_website add constraint [DF_map_migra_website_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.migra_website_aud
create table map.migra_website_aud(
	migra_website_name		varchar(20) NULL,
	etl_name				varchar(10) NULL,
	description				varchar(255) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.migra_website_aud add constraint [DF_map_migra_website_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.migra_order_status
create table map.migra_order_status(
	orderstatusname			char(1) NOT NULL, 
	description				varchar(100) NOT NULL,
	status					varchar(50) NOT NULL, 
	order_type				varchar(20) NULL,
	invoice_type			varchar(20) NULL,
	shipment_type			varchar(20) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.migra_order_status add constraint [DF_map_migra_order_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.migra_order_status_aud
create table map.migra_order_status_aud(
	orderstatusname			char(1) NOT NULL, 
	description				varchar(100) NOT NULL,
	status					varchar(50) NOT NULL, 
	order_type				varchar(20) NULL,
	invoice_type			varchar(20) NULL,
	shipment_type			varchar(20) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.migra_order_status_aud add constraint [DF_map_migra_order_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.migra_website_proforma
create table map.migra_website_proforma(
	website_group			varchar(255) NOT NULL, 
	proforma_date			date NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.migra_website_proforma add constraint [DF_map_migra_website_proforma_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.migra_website_proforma_aud
create table map.migra_website_proforma_aud(
	website_group			varchar(255) NOT NULL, 
	proforma_date			date NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.migra_website_proforma_aud add constraint [DF_map_migra_website_proforma_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
