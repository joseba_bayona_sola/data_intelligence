use Landing
go

select ga_medium, channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_ga_medium_ch

select ga_source, marketing_channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_ga_source_mch


select affil_code, marketing_channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_affil_code_mch

select affil_code_like, marketing_channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_affil_code_like_mch


select cc_channel, marketing_channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_cc_channel_mch


select channel, marketing_channel_name,
	idETLBatchRun, ins_ts
from Landing.map.sales_ch_migrate_channel_mch

