use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_vat_countries_registered
drop trigger map.trg_vat_countries_registered;
go 

create trigger map.trg_vat_countries_registered on map.vat_countries_registered
after insert
as
begin
	set nocount on

	merge into map.vat_countries_registered_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.countries_registered_code, '') = isnull(src.countries_registered_code, '')) 
	when matched and
		(isnull(trg.country_type, '') <> isnull(src.country_type, '')) or (isnull(trg.country_code, '') <> isnull(src.country_code, '')) or 
		(isnull(trg.country_registered_name, '') <> isnull(src.country_registered_name, ''))
		
		then 
			update set
				trg.country_type = src.country_type, trg.country_code = src.country_code, 
				trg.country_registered_name = src.country_registered_name, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (countries_registered_code, country_type, country_code, country_registered_name, idETLBatchRun)
				values (src.countries_registered_code, src.country_type, src.country_code, src.country_registered_name, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_vat_vat_type_rate
drop trigger map.trg_vat_vat_type_rate;
go 

create trigger map.trg_vat_vat_type_rate on map.vat_vat_type_rate
after insert
as
begin
	set nocount on

	merge into map.vat_vat_type_rate_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.vat_type_rate_code, '') = isnull(src.vat_type_rate_code, '')) 
	when matched and
		(isnull(trg.vat_type_rate_name, '') <> isnull(src.vat_type_rate_name, ''))
		then 
			update set
				trg.vat_type_rate_name = src.vat_type_rate_name, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (vat_type_rate_code, vat_type_rate_name, idETLBatchRun)
				values (src.vat_type_rate_code, src.vat_type_rate_name, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_vat_product_type_vat
drop trigger map.trg_vat_product_type_vat;
go 

create trigger map.trg_vat_product_type_vat on map.vat_product_type_vat
after insert
as
begin
	set nocount on

	merge into map.vat_product_type_vat_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.product_type_vat, '') = isnull(src.product_type_vat, ''))

	when not matched 
		then
			insert (product_type_vat, idETLBatchRun)
				values (src.product_type_vat, src.idETLBatchRun);
end;
go 





-- Landing.map.trg_vat_vat_rate
drop trigger map.trg_vat_vat_rate;
go 

create trigger map.trg_vat_vat_rate on map.vat_vat_rate
after insert
as
begin
	set nocount on

	merge into map.vat_vat_rate_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.countries_registered_code, '') = isnull(src.countries_registered_code, '')) and (isnull(trg.product_type_vat, '') = isnull(src.product_type_vat, ''))
			and (isnull(trg.vat_type_rate_code, '') = isnull(src.vat_type_rate_code, '')) and (isnull(trg.vat_time_period_start, '') = isnull(src.vat_time_period_start, ''))
	when matched and
		(isnull(trg.vat_time_period_finish, '') <> isnull(src.vat_time_period_finish, '')) or 
		(isnull(trg.vat_rate, 0) <> isnull(src.vat_rate, 0)) or (isnull(trg.active, '') <> isnull(src.active, ''))
		then 
			update set
				trg.vat_time_period_finish = src.vat_time_period_finish, 
				trg.vat_rate = src.vat_rate, trg.active = src.active, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (countries_registered_code, product_type_vat, vat_type_rate_code, vat_time_period_start, vat_time_period_finish, 
				vat_rate, active, idETLBatchRun)
				
				values (src.countries_registered_code, src.product_type_vat, src.vat_type_rate_code, src.vat_time_period_start, src.vat_time_period_finish, 
					src.vat_rate, src.active, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_vat_prof_fee_rate
drop trigger map.trg_vat_prof_fee_rate;
go 

create trigger map.trg_vat_prof_fee_rate on map.vat_prof_fee_rate
after insert
as
begin
	set nocount on

	merge into map.vat_prof_fee_rate_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.countries_registered_code, '') = isnull(src.countries_registered_code, '')) and (isnull(trg.product_type_vat, '') = isnull(src.product_type_vat, ''))
			and (isnull(trg.vat_time_period_start, '') = isnull(src.vat_time_period_start, ''))
	when matched and
		(isnull(trg.vat_time_period_finish, '') <> isnull(src.vat_time_period_finish, '')) or 
		(isnull(trg.prof_fee_rate, 0) <> isnull(src.prof_fee_rate, 0)) or (isnull(trg.active, '') <> isnull(src.active, ''))
		then 
			update set
				trg.vat_time_period_finish = src.vat_time_period_finish, 
				trg.prof_fee_rate = src.prof_fee_rate, trg.active = src.active, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (countries_registered_code, product_type_vat, vat_time_period_start, vat_time_period_finish, 
				prof_fee_rate, active, idETLBatchRun)
				
				values (src.countries_registered_code, src.product_type_vat, src.vat_time_period_start, src.vat_time_period_finish, 
					src.prof_fee_rate, src.active, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_vat_prof_fee_fixed_value
drop trigger map.trg_vat_prof_fee_fixed_value;
go 

create trigger map.trg_vat_prof_fee_fixed_value on map.vat_prof_fee_fixed_value
after insert
as
begin
	set nocount on

	merge into map.vat_prof_fee_fixed_value_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.countries_registered_code, '') = isnull(src.countries_registered_code, '')) 
			and (isnull(trg.order_value_min, 0) = isnull(src.order_value_min, 0)) and (isnull(trg.order_value_max, 0) = isnull(src.order_value_max, 0))
	when matched and
		(isnull(trg.prof_fee_fixed_value, 0) <> isnull(src.prof_fee_fixed_value, 0)) or (isnull(trg.active, '') <> isnull(src.active, ''))
		then 
			update set
				trg.prof_fee_fixed_value = src.prof_fee_fixed_value, trg.active = src.active, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (countries_registered_code, order_value_min, order_value_max, 
				prof_fee_fixed_value, active, idETLBatchRun)
				
				values (src.countries_registered_code, src.order_value_min, src.order_value_max, 
					src.prof_fee_fixed_value, src.active, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_vat_countries_registered_store
drop trigger map.trg_vat_countries_registered_store;
go 

create trigger map.trg_vat_countries_registered_store on map.vat_countries_registered_store
after insert
as
begin
	set nocount on

	merge into map.vat_countries_registered_store_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.countries_registered_code, '') = isnull(src.countries_registered_code, '')) and (isnull(trg.store_id, 0) = isnull(src.store_id, 0))

	when not matched 
		then
			insert (countries_registered_code, store_id, idETLBatchRun)
				values (src.countries_registered_code, src.store_id, src.idETLBatchRun);
end;
go 
