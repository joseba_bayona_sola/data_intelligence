use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.act_activity_group_type
create table map.act_activity_group_type(
	activity_group			varchar(50) NULL,
	activity_type			varchar(50) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_activity_group_type add constraint [DF_map_act_activity_group_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_activity_group_type_aud
create table map.act_activity_group_type_aud(
	activity_group			varchar(50) NULL,
	activity_type			varchar(50) NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.act_activity_group_type_aud add constraint [DF_map_act_activity_group_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_migrate_store_date
create table map.act_migrate_store_date(
	store_id				int NOT NULL, 
	store_name				varchar(255) NOT NULL, 
	migration_date			date NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_migrate_store_date add constraint [DF_map_act_migrate_store_date_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_migrate_store_date_aud
create table map.act_migrate_store_date_aud(
	store_id				int NOT NULL, 
	store_name				varchar(255) NOT NULL, 
	migration_date			date NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.act_migrate_store_date_aud add constraint [DF_map_act_migrate_store_date_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_lapsed_num_days
create table map.act_lapsed_num_days(
	lapsed_num_days			int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_lapsed_num_days add constraint [DF_map_act_lapsed_num_days_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_website_migrate_date
create table map.act_website_migrate_date(
	website_group			varchar(255) NOT NULL, 
	migration_date			date NOT NULL,
	migration				char(1) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_website_migrate_date add constraint [DF_map_act_website_migrate_date_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_website_migrate_date_aud
create table map.act_website_migrate_date_aud(
	website_group			varchar(255) NOT NULL, 
	migration_date			date NOT NULL,
	migration				char(1) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.act_website_migrate_date_aud add constraint [DF_map_act_website_migrate_date_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_website_register_mapping
create table map.act_website_register_mapping(
	store_id				int NOT NULL, 
	store_id_register		int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_website_register_mapping add constraint [DF_map_act_website_register_mapping_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_website_register_mapping_aud
create table map.act_website_register_mapping_aud(
	store_id				int NOT NULL, 
	store_id_register		int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.act_website_register_mapping_aud add constraint [DF_map_act_website_register_mapping_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_website_old_access_cust_no
create table map.act_website_old_access_cust_no(
	old_access_cust_no		varchar(255) NOT NULL, 
	store_id				int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.act_website_old_access_cust_no add constraint [DF_map_act_website_old_access_cust_no_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_website_old_access_cust_no_aud
create table map.act_website_old_access_cust_no_aud(
	old_access_cust_no		varchar(255) NOT NULL, 
	store_id				int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go

alter table map.act_website_old_access_cust_no_aud add constraint [DF_map_act_website_old_access_cust_no_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.act_rank_cr_time_mm
create table map.act_rank_cr_time_mm(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_cr_time_mm		char(2) NOT NULL,
	cr_time_name		varchar(10) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table map.act_rank_cr_time_mm add constraint [DF_map_act_rank_cr_time_mm_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.act_rank_cr_time_mm_aud
create table map.act_rank_cr_time_mm_aud(
	min_rank			int NOT NULL, 
	max_rank			int NOT NULL, 
	rank_cr_time_mm		char(2) NOT NULL,
	cr_time_name		varchar(10) NOT NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL,
	upd_ts				datetime);
go

alter table map.act_rank_cr_time_mm_aud add constraint [DF_map_act_rank_cr_time_mm_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


