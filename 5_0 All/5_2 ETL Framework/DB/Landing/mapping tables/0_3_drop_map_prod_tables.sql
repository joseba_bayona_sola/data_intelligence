use Landing
go 

drop table map.prod_product_type
go 
drop table map.prod_product_type_aud
go 

drop table map.prod_category
go 
drop table map.prod_category_aud
go 

drop table map.prod_cl_type
go 
drop table map.prod_cl_type_aud
go 

drop table map.prod_cl_feature
go 
drop table map.prod_cl_feature_aud
go 


drop table map.prod_exclude_products
go 
drop table map.prod_exclude_products_aud
go 

drop table map.prod_visibility
go 
drop table map.prod_visibility_aud
go 




drop table map.prod_param_base_curve
go 
drop table map.prod_param_base_curve_aud
go 

drop table map.prod_param_diameter
go 
drop table map.prod_param_diameter_aud
go 

drop table map.prod_param_power
go 
drop table map.prod_param_power_aud
go 


drop table map.prod_param_cylinder
go 
drop table map.prod_param_cylinder_aud
go 

drop table map.prod_param_axis
go 
drop table map.prod_param_axis_aud
go 


drop table map.prod_param_addition
go 
drop table map.prod_param_addition_aud
go 

drop table map.prod_param_dominance
go 
drop table map.prod_param_dominance_aud
go 



drop table map.prod_product_30_90
go 
drop table map.prod_product_30_90_aud
go 

drop table map.prod_product_aura
go 
drop table map.prod_product_aura_aud
go 



drop table map.prod_product_type_oh
go 
drop table map.prod_product_type_oh_aud
go 



drop table map.prod_product_category_rel
go 
drop table map.prod_product_category_rel_aud
go 



drop table map.prod_product_colour
go 
drop table map.prod_product_colour_aud
go 



drop table map.prod_product_family_group
go 
drop table map.prod_product_family_group_aud
go 



drop table map.prod_product_family_group_pf
go 
drop table map.prod_product_family_group_pf_aud
go 


drop table map.prod_product_family_erp
go 
drop table map.prod_product_family_erp_aud
go 
