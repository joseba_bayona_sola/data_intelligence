use Landing
go 

drop table map.sales_order_stage
go
drop table map.sales_order_stage_aud
go

drop table map.sales_order_status
go
drop table map.sales_order_status_aud
go

drop table map.sales_order_type
go
drop table map.sales_order_type_aud
go

drop table map.sales_order_status_magento
go
drop table map.sales_order_status_magento_aud
go



drop table map.sales_payment_method
go
drop table map.sales_payment_method_aud
go

drop table map.sales_cc_type
go
drop table map.sales_cc_type_aud
go



drop table map.sales_shipping_method
go
drop table map.sales_shipping_method_aud
go


drop table map.sales_price_type
go
drop table map.sales_price_type_aud
go


drop table map.sales_channel
go
drop table map.sales_channel_aud
go


drop table map.sales_marketing_channel
go
drop table map.sales_marketing_channel_aud
go



drop table map.sales_affiliate
go
drop table map.sales_affiliate_aud
go



drop table map.sales_prescription_method
go
drop table map.sales_prescription_method_aud
go



drop table map.sales_rank_customer_order_seq_no
go
drop table map.sales_rank_customer_order_seq_no_aud
go



drop table map.sales_rank_qty_time
go
drop table map.sales_rank_qty_time_aud
go



drop table map.sales_rank_freq_time
go
drop table map.sales_rank_freq_time_aud
go



drop table map.sales_rank_shipping_days
go
drop table map.sales_rank_shipping_days_aud
go



drop table map.sales_summer_time_period
go
drop table map.sales_summer_time_period_aud
go
