use Landing
go 

--------------------- Tables ----------------------------------

-- Landing.map.sales_ch_ga_medium_ch
create table map.sales_ch_ga_medium_ch(
	ga_medium				varchar(100) NOT NULL,
	channel_name			varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_ga_medium_ch add constraint [DF_map_sales_ch_ga_medium_ch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.sales_ch_ga_source_mch
create table map.sales_ch_ga_source_mch(
	ga_source				varchar(100) NOT NULL,
	marketing_channel_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_ga_source_mch add constraint [DF_map_sales_ch_ga_source_mch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.sales_ch_affil_code_mch
create table map.sales_ch_affil_code_mch(
	affil_code				varchar(255) NOT NULL,
	marketing_channel_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_affil_code_mch add constraint [DF_map_sales_ch_affil_code_mch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.sales_ch_affil_code_like_mch
create table map.sales_ch_affil_code_like_mch(
	affil_code_like			varchar(255) NOT NULL,
	marketing_channel_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_affil_code_like_mch add constraint [DF_map_sales_ch_affil_code_like_mch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.sales_ch_cc_channel_mch
create table map.sales_ch_cc_channel_mch(
	cc_channel				varchar(255) NOT NULL,
	marketing_channel_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_cc_channel_mch add constraint [DF_map_sales_ch_cc_channel_mch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.sales_ch_migrate_channel_mch
create table map.sales_ch_migrate_channel_mch(
	channel					varchar(64) NOT NULL,
	marketing_channel_name	varchar(50) NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go

alter table map.sales_ch_migrate_channel_mch add constraint [DF_map_sales_ch_migrate_channel_mch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


