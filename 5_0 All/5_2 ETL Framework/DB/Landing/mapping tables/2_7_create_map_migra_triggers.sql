use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_migra_website
drop trigger map.trg_migra_website;
go 

create trigger map.trg_migra_website on map.migra_website
after insert
as
begin
	set nocount on

	merge into map.migra_website_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.migra_website_name, '') = isnull(src.migra_website_name, '')) 
	when matched and
		(isnull(trg.etl_name, '') <> isnull(src.etl_name, '')) or (isnull(trg.description, '') <> isnull(src.description, '')) 
		then 
			update set
				trg.etl_name = src.etl_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (migra_website_name, etl_name, description, idETLBatchRun)
				values (src.migra_website_name, src.etl_name, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_migra_order_status
drop trigger map.trg_migra_order_status;
go 

create trigger map.trg_migra_order_status on map.migra_order_status
after insert
as
begin
	set nocount on

	merge into map.migra_order_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.orderstatusname, '') = isnull(src.orderstatusname, '')) 
	when matched and
		(isnull(trg.description, '') <> isnull(src.description, '')) or (isnull(trg.status, '') <> isnull(src.status, '')) or 
		(isnull(trg.order_type, '') <> isnull(src.order_type, '')) or (isnull(trg.invoice_type, '') <> isnull(src.invoice_type, '')) or (isnull(trg.shipment_type, '') <> isnull(src.shipment_type, ''))
		then 
			update set
				trg.description = src.description, trg.status = src.status, 
				trg.order_type = src.order_type, trg.invoice_type = src.invoice_type, trg.shipment_type = src.shipment_type, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (orderstatusname, description, status, 
				order_type, invoice_type, shipment_type, 
				idETLBatchRun)
				
				values (src.orderstatusname, src.description, src.status, 
					src.order_type, src.invoice_type, src.shipment_type, 
					src.idETLBatchRun);
end;
go 




-- Landing.map.trg_migra_website_proforma
drop trigger map.trg_migra_website_proforma;
go 

create trigger map.trg_migra_website_proforma on map.migra_website_proforma
after insert
as
begin
	set nocount on

	merge into map.migra_website_proforma_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.website_group, '') = isnull(src.website_group, '')) 
	when matched and
		(isnull(trg.proforma_date, '') <> isnull(src.proforma_date, '')) 
		then 
			update set
				trg.proforma_date = src.proforma_date, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (website_group, proforma_date, 
				idETLBatchRun)
				
				values (src.website_group, src.proforma_date, 
					src.idETLBatchRun);
end;
go 
