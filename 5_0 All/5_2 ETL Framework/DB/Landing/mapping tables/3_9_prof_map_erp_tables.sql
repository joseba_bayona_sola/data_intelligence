
use Landing
go


select stock_method_type, stock_method_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_stock_method_type

select stock_method_type, stock_method_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_stock_method_type_aud


select stock_batch_type, stock_batch_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_stock_batch_type

select stock_batch_type, stock_batch_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_stock_batch_type_aud


select stock_adjustment_type, stock_adjustment_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_stock_adjustment_type

select stock_adjustment_type, stock_adjustment_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_stock_adjustment_type_aud


select stock_movement_type, stock_movement_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_stock_movement_type

select stock_movement_type, stock_movement_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_stock_movement_type_aud


select stock_movement_period, stock_movement_period_name, description, period_start, period_finish,
	idETLBatchRun, ins_ts
from Landing.map.erp_stock_movement_period

select stock_movement_period, stock_movement_period_name, description, period_start, period_finish,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_stock_movement_period_aud



select order_type_erp, order_type_erp_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_order_type_erp

select order_type_erp, order_type_erp_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_order_type_erp_aud


select order_status_erp, order_status_erp_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_order_status_erp

select order_status_erp, order_status_erp_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_order_status_erp_aud


select shipment_status_erp, shipment_status_erp_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_shipment_status_erp

select shipment_status_erp, shipment_status_erp_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_shipment_status_erp_aud


select allocation_status, allocation_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_allocation_status

select allocation_status, allocation_status_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_allocation_status_aud


select allocation_strategy, allocation_strategy_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_allocation_strategy

select allocation_strategy, allocation_strategy_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_allocation_strategy_aud


select allocation_type, allocation_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_allocation_type

select allocation_type, allocation_type_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_allocation_type_aud


select allocation_record_type, allocation_record_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_allocation_record_type

select allocation_record_type, allocation_record_type_name, description, 
	idETLBatchRun, upd_ts
from Landing.map.erp_allocation_record_type_aud



select po_source, po_source_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_po_source

select po_source, po_source_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_po_source_aud


select po_type, po_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_po_type

select po_type, po_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_po_type_aud


select po_status, po_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_po_status

select po_status, po_status_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_po_status_aud


select pol_status, pol_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_pol_status

select pol_status, pol_status_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_pol_status_aud


select pol_problems, pol_problems_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_pol_problems

select pol_problems, pol_problems_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_pol_problems_aud



select wh_shipment_type, wh_shipment_type_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_wh_shipment_type

select wh_shipment_type, wh_shipment_type_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_wh_shipment_type_aud


select receipt_status, receipt_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_receipt_status

select receipt_status, receipt_status_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_receipt_status_aud


select receipt_line_status, receipt_line_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_receipt_line_status

select receipt_line_status, receipt_line_status_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_receipt_line_status_aud


select receipt_line_sync_status, receipt_line_sync_status_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.erp_receipt_line_sync_status

select receipt_line_sync_status, receipt_line_sync_status_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.erp_receipt_line_sync_status_aud
