use Landing
go 

--------------------- Triggers ----------------------------------

-- Landing.map.trg_erp_stock_method_type
drop trigger map.trg_erp_stock_method_type;
go 

create trigger map.trg_erp_stock_method_type on map.erp_stock_method_type
after insert
as
begin
	set nocount on

	merge into map.erp_stock_method_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.stock_method_type, '') = isnull(src.stock_method_type, '')) 
	when matched and
		(isnull(trg.stock_method_type_name, '') <> isnull(src.stock_method_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.stock_method_type_name = src.stock_method_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (stock_method_type, stock_method_type_name, description, idETLBatchRun)
				values (src.stock_method_type, src.stock_method_type_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_stock_batch_type
drop trigger map.trg_erp_stock_batch_type;
go 

create trigger map.trg_erp_stock_batch_type on map.erp_stock_batch_type
after insert
as
begin
	set nocount on

	merge into map.erp_stock_batch_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.stock_batch_type, '') = isnull(src.stock_batch_type, '')) 
	when matched and
		(isnull(trg.stock_batch_type_name, '') <> isnull(src.stock_batch_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.stock_batch_type_name = src.stock_batch_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (stock_batch_type, stock_batch_type_name, description, idETLBatchRun)
				values (src.stock_batch_type, src.stock_batch_type_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_stock_adjustment_type
drop trigger map.trg_erp_stock_adjustment_type;
go 

create trigger map.trg_erp_stock_adjustment_type on map.erp_stock_adjustment_type
after insert
as
begin
	set nocount on

	merge into map.erp_stock_adjustment_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.stock_adjustment_type, '') = isnull(src.stock_adjustment_type, '')) 
	when matched and
		(isnull(trg.stock_adjustment_type_name, '') <> isnull(src.stock_adjustment_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.stock_adjustment_type_name = src.stock_adjustment_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (stock_adjustment_type, stock_adjustment_type_name, description, idETLBatchRun)
				values (src.stock_adjustment_type, src.stock_adjustment_type_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_stock_movement_type
drop trigger map.trg_erp_stock_movement_type;
go 

create trigger map.trg_erp_stock_movement_type on map.erp_stock_movement_type
after insert
as
begin
	set nocount on

	merge into map.erp_stock_movement_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.stock_movement_type, '') = isnull(src.stock_movement_type, '')) 
	when matched and
		(isnull(trg.stock_movement_type_name, '') <> isnull(src.stock_movement_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.stock_movement_type_name = src.stock_movement_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (stock_movement_type, stock_movement_type_name, description, idETLBatchRun)
				values (src.stock_movement_type, src.stock_movement_type_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_stock_movement_period
drop trigger map.trg_erp_stock_movement_period;
go 

create trigger map.trg_erp_stock_movement_period on map.erp_stock_movement_period
after insert
as
begin
	set nocount on

	merge into map.erp_stock_movement_period_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.stock_movement_period, '') = isnull(src.stock_movement_period, '')) 
	when matched and
		(isnull(trg.stock_movement_period_name, '') <> isnull(src.stock_movement_period_name, '')) or (isnull(trg.description, '') <> isnull(src.description, '')) or 
			(isnull(trg.period_start, '') <> isnull(src.period_start, '')) or (isnull(trg.period_finish, '') <> isnull(src.period_finish, ''))
		then 
			update set
				trg.stock_movement_period_name = src.stock_movement_period_name, trg.description = src.description, 
				trg.period_start = src.period_start, trg.period_finish = src.period_finish, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (stock_movement_period, stock_movement_period_name, description, period_start, period_finish, idETLBatchRun)
				values (src.stock_movement_period, src.stock_movement_period_name, src.description, src.period_start, src.period_finish, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_erp_order_type_erp
drop trigger map.trg_erp_order_type_erp;
go 

create trigger map.trg_erp_order_type_erp on map.erp_order_type_erp
after insert
as
begin
	set nocount on

	merge into map.erp_order_type_erp_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_type_erp, '') = isnull(src.order_type_erp, '')) 
	when matched and
		(isnull(trg.order_type_erp_name, '') <> isnull(src.order_type_erp_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.order_type_erp_name = src.order_type_erp_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_type_erp, order_type_erp_name, description, idETLBatchRun)
				values (src.order_type_erp, src.order_type_erp_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_order_status_erp
drop trigger map.trg_erp_order_status_erp;
go 

create trigger map.trg_erp_order_status_erp on map.erp_order_status_erp
after insert
as
begin
	set nocount on

	merge into map.erp_order_status_erp_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.order_status_erp, '') = isnull(src.order_status_erp, '')) 
	when matched and
		(isnull(trg.order_status_erp_name, '') <> isnull(src.order_status_erp_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.order_status_erp_name = src.order_status_erp_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (order_status_erp, order_status_erp_name, description, idETLBatchRun)
				values (src.order_status_erp, src.order_status_erp_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_order_shipment_erp
drop trigger map.trg_erp_order_shipment_erp;
go 

create trigger map.trg_erp_order_shipment_erp on map.erp_shipment_status_erp
after insert
as
begin
	set nocount on

	merge into map.erp_shipment_status_erp_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.shipment_status_erp, '') = isnull(src.shipment_status_erp, '')) 
	when matched and
		(isnull(trg.shipment_status_erp_name, '') <> isnull(src.shipment_status_erp_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.shipment_status_erp_name = src.shipment_status_erp_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (shipment_status_erp, shipment_status_erp_name, description, idETLBatchRun)
				values (src.shipment_status_erp, src.shipment_status_erp_name, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_erp_allocation_status
drop trigger map.trg_erp_allocation_status;
go 

create trigger map.trg_erp_allocation_status on map.erp_allocation_status
after insert
as
begin
	set nocount on

	merge into map.erp_allocation_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.allocation_status, '') = isnull(src.allocation_status, '')) 
	when matched and
		(isnull(trg.allocation_status_name, '') <> isnull(src.allocation_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.allocation_status_name = src.allocation_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (allocation_status, allocation_status_name, description, idETLBatchRun)
				values (src.allocation_status, src.allocation_status_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_allocation_strategy
drop trigger map.trg_erp_allocation_strategy;
go 

create trigger map.trg_erp_allocation_strategy on map.erp_allocation_strategy
after insert
as
begin
	set nocount on

	merge into map.erp_allocation_strategy_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.allocation_strategy, '') = isnull(src.allocation_strategy, '')) 
	when matched and
		(isnull(trg.allocation_strategy_name, '') <> isnull(src.allocation_strategy_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.allocation_strategy_name = src.allocation_strategy_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (allocation_strategy, allocation_strategy_name, description, idETLBatchRun)
				values (src.allocation_strategy, src.allocation_strategy_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_allocation_type
drop trigger map.trg_erp_allocation_type;
go 

create trigger map.trg_erp_allocation_type on map.erp_allocation_type
after insert
as
begin
	set nocount on

	merge into map.erp_allocation_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.allocation_type, '') = isnull(src.allocation_type, '')) 
	when matched and
		(isnull(trg.allocation_type_name, '') <> isnull(src.allocation_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.allocation_type_name = src.allocation_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (allocation_type, allocation_type_name, description, idETLBatchRun)
				values (src.allocation_type, src.allocation_type_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_allocation_record_type
drop trigger map.trg_erp_allocation_record_type;
go 

create trigger map.trg_erp_allocation_record_type on map.erp_allocation_record_type
after insert
as
begin
	set nocount on

	merge into map.erp_allocation_record_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.allocation_record_type, '') = isnull(src.allocation_record_type, '')) 
	when matched and
		(isnull(trg.allocation_record_type_name, '') <> isnull(src.allocation_record_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.allocation_record_type_name = src.allocation_record_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (allocation_record_type, allocation_record_type_name, description, idETLBatchRun)
				values (src.allocation_record_type, src.allocation_record_type_name, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_erp_po_source
drop trigger map.trg_erp_po_source;
go 

create trigger map.trg_erp_po_source on map.erp_po_source
after insert
as
begin
	set nocount on

	merge into map.erp_po_source_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.po_source, '') = isnull(src.po_source, '')) 
	when matched and
		(isnull(trg.po_source_name, '') <> isnull(src.po_source_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.po_source_name = src.po_source_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (po_source, po_source_name, description, idETLBatchRun)
				values (src.po_source, src.po_source_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_po_type
drop trigger map.trg_erp_po_type;
go 

create trigger map.trg_erp_po_type on map.erp_po_type
after insert
as
begin
	set nocount on

	merge into map.erp_po_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.po_type, '') = isnull(src.po_type, '')) 
	when matched and
		(isnull(trg.po_type_name, '') <> isnull(src.po_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.po_type_name = src.po_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (po_type, po_type_name, description, idETLBatchRun)
				values (src.po_type, src.po_type_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_erp_po_status
drop trigger map.trg_erp_po_status;
go 

create trigger map.trg_erp_po_status on map.erp_po_status
after insert
as
begin
	set nocount on

	merge into map.erp_po_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.po_status, '') = isnull(src.po_status, '')) 
	when matched and
		(isnull(trg.po_status_name, '') <> isnull(src.po_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.po_status_name = src.po_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (po_status, po_status_name, description, idETLBatchRun)
				values (src.po_status, src.po_status_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_pol_status
drop trigger map.trg_erp_pol_status;
go 

create trigger map.trg_erp_pol_status on map.erp_pol_status
after insert
as
begin
	set nocount on

	merge into map.erp_pol_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.pol_status, '') = isnull(src.pol_status, '')) 
	when matched and
		(isnull(trg.pol_status_name, '') <> isnull(src.pol_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.pol_status_name = src.pol_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (pol_status, pol_status_name, description, idETLBatchRun)
				values (src.pol_status, src.pol_status_name, src.description, src.idETLBatchRun);
end;
go 



-- Landing.map.trg_erp_pol_problems
drop trigger map.trg_erp_pol_problems;
go 

create trigger map.trg_erp_pol_problems on map.erp_pol_problems
after insert
as
begin
	set nocount on

	merge into map.erp_pol_problems_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.pol_problems, '') = isnull(src.pol_problems, '')) 
	when matched and
		(isnull(trg.pol_problems_name, '') <> isnull(src.pol_problems_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.pol_problems_name = src.pol_problems_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (pol_problems, pol_problems_name, description, idETLBatchRun)
				values (src.pol_problems, src.pol_problems_name, src.description, src.idETLBatchRun);
end;
go 




-- Landing.map.trg_wh_shipment_type
drop trigger map.trg_wh_shipment_type;
go 

create trigger map.trg_wh_shipment_type on map.erp_wh_shipment_type
after insert
as
begin
	set nocount on

	merge into map.erp_wh_shipment_type_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.wh_shipment_type, '') = isnull(src.wh_shipment_type, '')) 
	when matched and
		(isnull(trg.wh_shipment_type_name, '') <> isnull(src.wh_shipment_type_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.wh_shipment_type_name = src.wh_shipment_type_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (wh_shipment_type, wh_shipment_type_name, description, idETLBatchRun)
				values (src.wh_shipment_type, src.wh_shipment_type_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_receipt_status
drop trigger map.trg_receipt_status;
go 

create trigger map.trg_receipt_status on map.erp_receipt_status
after insert
as
begin
	set nocount on

	merge into map.erp_receipt_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.receipt_status, '') = isnull(src.receipt_status, '')) 
	when matched and
		(isnull(trg.receipt_status_name, '') <> isnull(src.receipt_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.receipt_status_name = src.receipt_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (receipt_status, receipt_status_name, description, idETLBatchRun)
				values (src.receipt_status, src.receipt_status_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_receipt_line_status
drop trigger map.trg_receipt_line_status;
go 

create trigger map.trg_receipt_line_status on map.erp_receipt_line_status
after insert
as
begin
	set nocount on

	merge into map.erp_receipt_line_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.receipt_line_status, '') = isnull(src.receipt_line_status, '')) 
	when matched and
		(isnull(trg.receipt_line_status_name, '') <> isnull(src.receipt_line_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.receipt_line_status_name = src.receipt_line_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (receipt_line_status, receipt_line_status_name, description, idETLBatchRun)
				values (src.receipt_line_status, src.receipt_line_status_name, src.description, src.idETLBatchRun);
end;
go 


-- Landing.map.trg_receipt_line_sync_status
drop trigger map.trg_receipt_line_sync_status;
go 

create trigger map.trg_receipt_line_sync_status on map.erp_receipt_line_sync_status
after insert
as
begin
	set nocount on

	merge into map.erp_receipt_line_sync_status_aud with (tablock) as trg
	using inserted src
		on (isnull(trg.receipt_line_sync_status, '') = isnull(src.receipt_line_sync_status, '')) 
	when matched and
		(isnull(trg.receipt_line_sync_status_name, '') <> isnull(src.receipt_line_sync_status_name, '')) or (isnull(trg.description, '') <> isnull(src.description, ''))
		then 
			update set
				trg.receipt_line_sync_status_name = src.receipt_line_sync_status_name, trg.description = src.description, 
				trg.upd_ts = getutcdate()
	when not matched 
		then
			insert (receipt_line_sync_status, receipt_line_sync_status_name, description, idETLBatchRun)
				values (src.receipt_line_sync_status, src.receipt_line_sync_status_name, src.description, src.idETLBatchRun);
end;
go 

