use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.vat_countries_registered
create table map.vat_countries_registered(
	countries_registered_code	varchar(10) NOT NULL, 
	country_type				varchar(10) NOT NULL, 
	country_code				varchar(50) NOT NULL, 
	country_registered_name		varchar(50) NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_countries_registered add constraint [DF_map_vat_countries_registered_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_countries_registered_aud
create table map.vat_countries_registered_aud(
	countries_registered_code	varchar(10) NOT NULL, 
	country_type				varchar(10) NOT NULL, 
	country_code				varchar(50) NOT NULL, 
	country_registered_name		varchar(50) NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_countries_registered_aud add constraint [DF_map_vat_countries_registered_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.vat_vat_type_rate
create table map.vat_vat_type_rate(
	vat_type_rate_code			char(1) NOT NULL, 
	vat_type_rate_name			varchar(20) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_vat_type_rate add constraint [DF_map_vat_vat_type_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_vat_type_rate_aud
create table map.vat_vat_type_rate_aud(
	vat_type_rate_code			char(1) NOT NULL, 
	vat_type_rate_name			varchar(20) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_vat_type_rate_aud add constraint [DF_map_vat_vat_type_rate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.vat_product_type_vat
create table map.vat_product_type_vat(
	product_type_vat			varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_product_type_vat add constraint [DF_map_vat_product_type_vat_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_product_type_vat_aud
create table map.vat_product_type_vat_aud(
	product_type_vat			varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_product_type_vat_aud add constraint [DF_map_vat_product_type_vat_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 






-- Landing.map.vat_vat_rate
create table map.vat_vat_rate(
	countries_registered_code	varchar(10) NOT NULL, 
	product_type_vat			varchar(255) NOT NULL,
	vat_type_rate_code			char(1) NOT NULL, 
	vat_time_period_start		date NOT NULL, 
	vat_time_period_finish		date NOT NULL, 
	vat_rate					decimal(12, 4) NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_vat_rate add constraint [DF_map_vat_vat_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_vat_rate_aud
create table map.vat_vat_rate_aud(
	countries_registered_code	varchar(10) NOT NULL, 
	product_type_vat			varchar(255) NOT NULL,
	vat_type_rate_code			char(1) NOT NULL, 
	vat_time_period_start		date NOT NULL, 
	vat_time_period_finish		date NOT NULL, 
	vat_rate					decimal(12, 4) NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_vat_rate_aud add constraint [PK_vat_vat_rate_aud]
	primary key clustered (countries_registered_code, product_type_vat, vat_type_rate_code, vat_time_period_start)
go

alter table map.vat_vat_rate_aud add constraint [DF_map_vat_vat_rate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.vat_prof_fee_rate
create table map.vat_prof_fee_rate(
	countries_registered_code	varchar(10) NOT NULL, 
	product_type_vat			varchar(255) NOT NULL,
	vat_time_period_start		date NOT NULL, 
	vat_time_period_finish		date NOT NULL, 
	prof_fee_rate				int NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_prof_fee_rate add constraint [DF_map_vat_prof_fee_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_prof_fee_rate_aud
create table map.vat_prof_fee_rate_aud(
	countries_registered_code	varchar(10) NOT NULL, 
	product_type_vat			varchar(255) NOT NULL,
	vat_time_period_start		date NOT NULL, 
	vat_time_period_finish		date NOT NULL, 
	prof_fee_rate				int NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go


alter table map.vat_prof_fee_rate_aud add constraint [PK_vat_prof_fee_rate_aud]
	primary key clustered (countries_registered_code, product_type_vat, vat_time_period_start)
go

alter table map.vat_prof_fee_rate_aud add constraint [DF_map_vat_prof_fee_rate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.vat_prof_fee_fixed_value
create table map.vat_prof_fee_fixed_value(
	countries_registered_code	varchar(10) NOT NULL, 
	order_value_min				decimal(12, 4) NOT NULL, 
	order_value_max				decimal(12, 4) NOT NULL, 
	prof_fee_fixed_value		decimal(12, 4) NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_prof_fee_fixed_value add constraint [DF_map_vat_prof_fee_fixed_value_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_prof_fee_fixed_value_aud
create table map.vat_prof_fee_fixed_value_aud(
	countries_registered_code	varchar(10) NOT NULL, 
	order_value_min				decimal(12, 4) NOT NULL, 
	order_value_max				decimal(12, 4) NOT NULL, 
	prof_fee_fixed_value		decimal(12, 4) NOT NULL, 
	active						char(1) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_prof_fee_fixed_value_aud add constraint [DF_map_vat_prof_fee_fixed_value_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 








-- Landing.map.vat_countries_registered_store
create table map.vat_countries_registered_store(
	countries_registered_code	varchar(10) NOT NULL, 
	store_id					int NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.vat_countries_registered_store add constraint [DF_map_vat_countries_registered_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.vat_countries_registered_store_aud
create table map.vat_countries_registered_store_aud(
	countries_registered_code	varchar(10) NOT NULL, 
	store_id					int NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.vat_countries_registered_store_aud add constraint [DF_map_vat_countries_registered_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

