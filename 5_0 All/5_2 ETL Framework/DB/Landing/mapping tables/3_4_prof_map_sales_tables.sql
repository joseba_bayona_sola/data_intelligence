use Landing
go

select order_stage, description,
	idETLBatchRun, ins_ts
from Landing.map.sales_order_stage

select order_stage, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_order_stage_aud


select order_status, description, 
	idETLBatchRun, ins_ts
from Landing.map.sales_order_status

select order_status, description,  
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_order_status_aud


select order_type, description,  
	idETLBatchRun, ins_ts
from Landing.map.sales_order_type

select order_type, description,   
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_order_type_aud


select order_status_magento, order_status_magento_code, description,
	idETLBatchRun, ins_ts
from Landing.map.sales_order_status_magento

select order_status_magento, order_status_magento_code, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_order_status_magento_aud



select payment_method, payment_method_code, description, payment_method_name, 
	idETLBatchRun, ins_ts
from Landing.map.sales_payment_method

select payment_method, payment_method_code, description, payment_method_name, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_payment_method_aud


select cc_type, card_type_code,
	idETLBatchRun, ins_ts
from Landing.map.sales_cc_type

select cc_type, card_type_code, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_cc_type_aud



select shipping_method, shipping_method_code, shipping_method_code_erp, description, 
	idETLBatchRun, ins_ts
from Landing.map.sales_shipping_method

select shipping_method, shipping_method_code, shipping_method_code_erp, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_shipping_method_aud



select price_type, description, priority,
	idETLBatchRun, ins_ts
from Landing.map.sales_price_type

select price_type, description, priority, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_price_type_aud




select channel, channel_name, description, 
	idETLBatchRun, ins_ts
from Landing.map.sales_channel

select channel, channel_name, description, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_channel_aud



select marketing_channel, marketing_channel_name, sup_marketing_channel, description, 
	ga, bc, cc, ext,
	idETLBatchRun, ins_ts
from Landing.map.sales_marketing_channel

select marketing_channel, marketing_channel_name, sup_marketing_channel, description, 
	ga, bc, cc, ext,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_marketing_channel_aud



select publisher_id, affiliate_name, affiliate_website, affiliate_network, affiliate_type, affiliate_status, 
	idETLBatchRun, ins_ts
from Landing.map.sales_affiliate

select publisher_id, affiliate_name, affiliate_website, affiliate_network, affiliate_type, affiliate_status, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_affiliate_aud



select prescription_method, description,
	idETLBatchRun, ins_ts
from Landing.map.sales_prescription_method

select prescription_method, description,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_prescription_method_aud



select min_rank, max_rank, rank_seq_no,
	idETLBatchRun, ins_ts
from Landing.map.sales_rank_customer_order_seq_no

select min_rank, max_rank, rank_seq_no,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_rank_customer_order_seq_no_aud



select min_rank, max_rank, rank_qty_time,
	idETLBatchRun, ins_ts
from Landing.map.sales_rank_qty_time

select min_rank, max_rank, rank_qty_time,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_rank_qty_time_aud



select min_rank, max_rank, rank_freq_time,
	idETLBatchRun, ins_ts
from Landing.map.sales_rank_freq_time

select min_rank, max_rank, rank_freq_time,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_rank_freq_time_aud



select min_rank, max_rank, rank_shipping_days,
	idETLBatchRun, ins_ts
from Landing.map.sales_rank_shipping_days

select min_rank, max_rank, rank_shipping_days,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_rank_shipping_days_aud



select year, from_date, to_date,
	idETLBatchRun, ins_ts
from Landing.map.sales_summer_time_period

select year, from_date, to_date,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.sales_summer_time_period_aud
