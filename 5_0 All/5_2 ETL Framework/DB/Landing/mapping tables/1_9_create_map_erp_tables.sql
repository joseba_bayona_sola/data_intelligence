use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.map.erp_stock_method_type
create table map.erp_stock_method_type(
	stock_method_type			varchar(50) NOT NULL,
	stock_method_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_stock_method_type add constraint [DF_map_erp_stock_method_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_stock_method_type_aud
create table map.erp_stock_method_type_aud(
	stock_method_type			varchar(50) NOT NULL,
	stock_method_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_stock_method_type_aud add constraint [DF_map_erp_stock_method_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_stock_batch_type
create table map.erp_stock_batch_type(
	stock_batch_type			varchar(50) NOT NULL,
	stock_batch_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_stock_batch_type add constraint [DF_map_erp_stock_batch_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_stock_batch_type_aud
create table map.erp_stock_batch_type_aud(
	stock_batch_type			varchar(50) NOT NULL,
	stock_batch_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_stock_batch_type_aud add constraint [DF_map_erp_stock_batch_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.erp_stock_adjustment_type
create table map.erp_stock_adjustment_type(
	stock_adjustment_type		varchar(50) NOT NULL,
	stock_adjustment_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_stock_adjustment_type add constraint [DF_map_erp_stock_adjustment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_stock_adjustment_type_aud
create table map.erp_stock_adjustment_type_aud(
	stock_adjustment_type		varchar(50) NOT NULL,
	stock_adjustment_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_stock_adjustment_type_aud add constraint [DF_map_erp_stock_adjustment_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 





-- Landing.map.erp_stock_movement_type
create table map.erp_stock_movement_type(
	stock_movement_type			varchar(50) NOT NULL,
	stock_movement_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_stock_movement_type add constraint [DF_map_erp_stock_movement_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_stock_movement_type_aud
create table map.erp_stock_movement_type_aud(
	stock_movement_type			varchar(50) NOT NULL,
	stock_movement_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_stock_movement_type_aud add constraint [DF_map_erp_stock_movement_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.erp_stock_movement_period
create table map.erp_stock_movement_period(
	stock_movement_period		varchar(50) NOT NULL,
	stock_movement_period_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	period_start				date NOT NULL, 
	period_finish				date NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_stock_movement_period add constraint [DF_erp_stock_movement_period_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_stock_movement_period_aud
create table map.erp_stock_movement_period_aud(
	stock_movement_period		varchar(50) NOT NULL,
	stock_movement_period_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	period_start				date NOT NULL, 
	period_finish				date NOT NULL, 
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_stock_movement_period_aud add constraint [DF_map_erp_stock_movement_period_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.map.erp_order_type_erp
create table map.erp_order_type_erp(
	order_type_erp				varchar(50) NOT NULL,
	order_type_erp_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_order_type_erp add constraint [DF_map_erp_order_type_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_order_type_erp_aud
create table map.erp_order_type_erp_aud(
	order_type_erp				varchar(50) NOT NULL,
	order_type_erp_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_order_type_erp_aud add constraint [DF_map_erp_order_type_erp_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_order_status_erp
create table map.erp_order_status_erp(
	order_status_erp			varchar(50) NOT NULL,
	order_status_erp_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_order_status_erp add constraint [DF_map_erp_order_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_order_status_erp_aud
create table map.erp_order_status_erp_aud(
	order_status_erp			varchar(50) NOT NULL,
	order_status_erp_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_order_status_erp_aud add constraint [DF_map_erp_order_status_erp_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_shipment_status_erp
create table map.erp_shipment_status_erp(
	shipment_status_erp			varchar(50) NOT NULL,
	shipment_status_erp_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_shipment_status_erp add constraint [DF_map_erp_shipment_status_erp_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_shipment_status_erp_aud
create table map.erp_shipment_status_erp_aud(
	shipment_status_erp			varchar(50) NOT NULL,
	shipment_status_erp_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_shipment_status_erp_aud add constraint [DF_map_erp_shipment_status_erp_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_allocation_status
create table map.erp_allocation_status(
	allocation_status			varchar(50) NOT NULL,
	allocation_status_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_allocation_status add constraint [DF_map_erp_allocation_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_allocation_status_aud
create table map.erp_allocation_status_aud(
	allocation_status			varchar(50) NOT NULL,
	allocation_status_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_allocation_status_aud add constraint [DF_map_erp_allocation_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_allocation_strategy
create table map.erp_allocation_strategy(
	allocation_strategy			varchar(50) NOT NULL,
	allocation_strategy_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_allocation_strategy add constraint [DF_map_erp_allocation_strategy_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_allocation_strategy_aud
create table map.erp_allocation_strategy_aud(
	allocation_strategy			varchar(50) NOT NULL,
	allocation_strategy_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_allocation_strategy_aud add constraint [DF_map_erp_allocation_strategy_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_allocation_type
create table map.erp_allocation_type(
	allocation_type				varchar(50) NOT NULL,
	allocation_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_allocation_type add constraint [DF_map_erp_allocation_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_allocation_type_aud
create table map.erp_allocation_type_aud(
	allocation_type				varchar(50) NOT NULL,
	allocation_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_allocation_type_aud add constraint [DF_map_erp_allocation_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_allocation_record_type
create table map.erp_allocation_record_type(
	allocation_record_type		varchar(50) NOT NULL,
	allocation_record_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_allocation_record_type add constraint [DF_map_erp_allocation_record_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_allocation_record_type_aud
create table map.erp_allocation_record_type_aud(
	allocation_record_type		varchar(50) NOT NULL,
	allocation_record_type_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_allocation_record_type_aud add constraint [DF_map_erp_allocation_record_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 






-- Landing.map.erp_po_source
create table map.erp_po_source(
	po_source					varchar(50) NOT NULL,
	po_source_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_po_source add constraint [DF_map_erp_po_source_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_po_source_aud
create table map.erp_po_source_aud(
	po_source					varchar(50) NOT NULL,
	po_source_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_po_source_aud add constraint [DF_map_erp_po_source_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_po_type
create table map.erp_po_type(
	po_type						varchar(50) NOT NULL,
	po_type_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_po_type add constraint [DF_map_erp_po_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_po_type_aud
create table map.erp_po_type_aud(
	po_type						varchar(50) NOT NULL,
	po_type_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_po_type_aud add constraint [DF_map_erp_po_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_po_status
create table map.erp_po_status(
	po_status					varchar(50) NOT NULL,
	po_status_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_po_status add constraint [DF_map_erp_po_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_po_status_aud
create table map.erp_po_status_aud(
	po_status					varchar(50) NOT NULL,
	po_status_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_po_status_aud add constraint [DF_map_erp_po_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_pol_status
create table map.erp_pol_status(
	pol_status					varchar(50) NOT NULL,
	pol_status_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_pol_status add constraint [DF_map_erp_pol_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_pol_status_aud
create table map.erp_pol_status_aud(
	pol_status					varchar(50) NOT NULL,
	pol_status_name				varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_pol_status_aud add constraint [DF_map_erp_pol_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_pol_problems
create table map.erp_pol_problems(
	pol_problems				varchar(50) NOT NULL,
	pol_problems_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_pol_problems add constraint [DF_map_erp_pol_problems_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_pol_problems_aud
create table map.erp_pol_problems_aud(
	pol_problems				varchar(50) NOT NULL,
	pol_problems_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_pol_problems_aud add constraint [DF_map_erp_pol_problems_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_wh_shipment_type
create table map.erp_wh_shipment_type(
	wh_shipment_type			varchar(50) NOT NULL,
	wh_shipment_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_wh_shipment_type add constraint [DF_map_erp_wh_shipment_type_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_wh_shipment_type_aud
create table map.erp_wh_shipment_type_aud(
	wh_shipment_type			varchar(50) NOT NULL,
	wh_shipment_type_name		varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_wh_shipment_type_aud add constraint [DF_map_erp_wh_shipment_type_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_receipt_status
create table map.erp_receipt_status(
	receipt_status				varchar(50) NOT NULL,
	receipt_status_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_receipt_status add constraint [DF_map_erp_receipt_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_receipt_status_aud
create table map.erp_receipt_status_aud(
	receipt_status				varchar(50) NOT NULL,
	receipt_status_name			varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_receipt_status_aud add constraint [DF_map_erp_receipt_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.map.erp_receipt_line_status
create table map.erp_receipt_line_status(
	receipt_line_status			varchar(50) NOT NULL,
	receipt_line_status_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL);
go

alter table map.erp_receipt_line_status add constraint [DF_map_erp_receipt_line_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_receipt_line_status_aud
create table map.erp_receipt_line_status_aud(
	receipt_line_status			varchar(50) NOT NULL,
	receipt_line_status_name	varchar(50) NOT NULL,
	description					varchar(255) NOT NULL,
	idETLBatchRun				bigint NOT NULL, 
	ins_ts						datetime NOT NULL,
	upd_ts						datetime);
go

alter table map.erp_receipt_line_status_aud add constraint [DF_map_erp_receipt_line_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.map.erp_receipt_line_sync_status
create table map.erp_receipt_line_sync_status(
	receipt_line_sync_status		varchar(50) NOT NULL,
	receipt_line_sync_status_name	varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table map.erp_receipt_line_sync_status add constraint [DF_map_erp_receipt_line_sync_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.map.erp_receipt_line_sync_status_aud
create table map.erp_receipt_line_sync_status_aud(
	receipt_line_sync_status		varchar(50) NOT NULL,
	receipt_line_sync_status_name	varchar(50) NOT NULL,
	description						varchar(255) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table map.erp_receipt_line_sync_status_aud add constraint [DF_map_erp_receipt_line_sync_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
