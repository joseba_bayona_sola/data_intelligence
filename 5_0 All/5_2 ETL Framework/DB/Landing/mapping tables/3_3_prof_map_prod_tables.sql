use Landing
go

select product_type, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_type

select product_type, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_type_aud



select product_type, category, category_magento,
	idETLBatchRun, ins_ts
from Landing.map.prod_category

select product_type, category, category_magento,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_category_aud



select cl_type, cl_type_magento,
	idETLBatchRun, ins_ts
from Landing.map.prod_cl_type

select cl_type, cl_type_magento,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_cl_type_aud



select cl_feature, cl_feature_magento,
	idETLBatchRun, ins_ts
from Landing.map.prod_cl_feature

select cl_feature, cl_feature_magento,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_cl_feature_aud



select product_id, product_name, info,
	idETLBatchRun, ins_ts
from Landing.map.prod_exclude_products

select product_id, product_name, info,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_exclude_products_aud


select visibility, visibility_name,
	idETLBatchRun, ins_ts
from Landing.map.prod_visibility

select visibility, visibility_name,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_visibility_aud



select base_curve,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_base_curve

select base_curve,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_base_curve_aud


select diameter,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_diameter

select diameter,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_diameter_aud


select power,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_power

select power,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_power_aud




select cylinder,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_cylinder

select cylinder,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_cylinder_aud


select axis,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_axis

select axis,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_axis_aud



select addition,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_addition

select addition,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_addition_aud


select dominance,
	idETLBatchRun, ins_ts
from Landing.map.prod_param_dominance

select dominance,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_param_dominance_aud



select product_id, product_id_new, size_new,
	idETLBatchRun, ins_ts
from Landing.map.prod_product_30_90

select product_id, product_id_new, size_new,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_30_90_aud


select product_id, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_aura

select product_id, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_aura_aud



select product_type_oh, priority, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_type_oh

select product_type_oh, priority,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_type_oh_aud




select product_id, product_name, category_magento, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_category_rel

select product_id, product_name, category_magento,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_category_rel_aud


select product_id, product_name, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_colour

select product_id, product_name, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_colour_aud



select product_family_group, product_family_group_name, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_family_group

select product_family_group, product_family_group_name,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_family_group_aud



select product_id, product_name, product_family_group, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_family_group_pf

select product_id, product_name, product_family_group,
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_family_group_pf_aud


select productfamilyid, magentoProductID, magentoProductID_int, product_id_bk, 
	name, manufacturer_id_bk, category_bk, product_lifecycle_bk, visibility_id_bk, 
	idETLBatchRun, ins_ts
from Landing.map.prod_product_family_erp

select productfamilyid, magentoProductID, magentoProductID_int, product_id_bk, 
	name, manufacturer_id_bk, category_bk, product_lifecycle_bk, visibility_id_bk, 
	idETLBatchRun, ins_ts, upd_ts
from Landing.map.prod_product_family_erp_aud
