use Landing
go 

drop table map.erp_stock_method_type
go 
drop table map.erp_stock_method_type_aud
go 

drop table map.erp_stock_batch_type
go 
drop table map.erp_stock_batch_type_aud
go 

drop table map.erp_stock_adjustment_type
go 
drop table map.erp_stock_adjustment_type_aud
go 

drop table map.erp_stock_movement_type
go 
drop table map.erp_stock_movement_type_aud
go 

drop table map.erp_stock_movement_period
go 
drop table map.erp_stock_movement_period_aud
go 



drop table map.erp_order_type_erp
go 
drop table map.erp_order_type_erp_aud
go 

drop table map.erp_order_status_erp
go 
drop table map.erp_order_status_erp_aud
go 

drop table map.erp_shipment_status_erp
go 
drop table map.erp_shipment_status_erp_aud
go 

drop table map.erp_allocation_status
go 
drop table map.erp_allocation_status_aud
go 

drop table map.erp_allocation_strategy
go 
drop table map.erp_allocation_strategy_aud
go 

drop table map.erp_allocation_type
go 
drop table map.erp_allocation_type_aud
go 

drop table map.erp_allocation_record_type
go 
drop table map.erp_allocation_record_type_aud
go 



drop table map.erp_po_source
go 
drop table map.erp_po_source_aud
go 

drop table map.erp_po_type
go 
drop table map.erp_po_type_aud
go 

drop table map.erp_po_status
go 
drop table map.erp_po_status_aud
go 

drop table map.erp_pol_status
go 
drop table map.erp_pol_status_aud
go 

drop table map.erp_pol_problems
go 
drop table map.erp_pol_problems_aud
go 



drop table map.erp_wh_shipment_type
go 
drop table map.erp_wh_shipment_type_aud
go 

drop table map.erp_receipt_status
go 
drop table map.erp_receipt_status_aud
go 

drop table map.erp_receipt_line_status
go 
drop table map.erp_receipt_line_status_aud
go 

drop table map.erp_receipt_line_sync_status
go 
drop table map.erp_receipt_line_sync_status_aud
go 

