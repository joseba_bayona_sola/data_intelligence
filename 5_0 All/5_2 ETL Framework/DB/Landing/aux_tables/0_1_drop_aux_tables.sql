use Landing
go 

drop table aux.prod_product_category
go 

drop table aux.prod_product_product_type_oh
go 

drop table aux.prod_product_product_type_vat
go 


drop table aux.mag_core_config_data_store
go 

drop table aux.mag_sales_flat_creditmemo
go 

drop table aux.mag_sales_flat_creditmemo_item
go 

drop table aux.mag_edi_stock_item
go 

drop table aux.mag_catalog_product_price
go 
drop table aux.mag_catalog_product_price_aud
go 
drop table aux.mag_catalog_product_price_aud_hist
go 



drop table aux.gen_customer_unsubscribe_magento
go

drop table aux.gen_customer_unsubscribe_dotmailer
go

drop table aux.gen_customer_unsubscribe_textmessage
go

drop table aux.gen_customer_reminder
go

drop table aux.gen_customer_reorder
go



drop table aux.sales_order_header_o
go

drop table aux.sales_order_header_o_i_s_cr
go

drop table aux.sales_order_header_store_cust
go

drop table aux.sales_order_header_dim_order
go

drop table aux.sales_order_header_dim_mark
go

drop table aux.sales_order_header_measures
go

drop table aux.sales_order_header_vat
go

drop table aux.sales_order_header_oh_pt
go


drop table aux.sales_order_line_o_i_s_cr
go

drop table aux.sales_order_line_product
go

drop table aux.sales_order_line_measures
go

drop table aux.sales_order_line_vat
go



drop table aux.sales_dim_order_header
go
drop table aux.sales_dim_order_header_aud
go

drop table aux.sales_fact_order_line
go
drop table aux.sales_fact_order_line_aud
go



drop table aux.act_fact_activity_sales
go

drop table aux.act_fact_activity_other
go


drop table aux.act_customer_registration
go

drop table aux.act_customer_registration_aud
go



drop table aux.act_customer_cr_reg
go


drop table aux.sales_exchange_rate
go


drop table aux.gen_dim_customer
go
drop table aux.gen_dim_customer_aud
go


drop table aux.gdpr_customer_list
go


drop table aux.stock_warehousestockitem
go
