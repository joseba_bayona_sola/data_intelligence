use Landing
go 

------------------------------------------------------------------------
----------------------- mag_catalog_product_price ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from aux.mag_catalog_product_price_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows
	from aux.mag_catalog_product_price_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from aux.mag_catalog_product_price_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows
	from aux.mag_catalog_product_price_aud_hist

	select *
	from
		(select count(*) over(partition by store_id, product_id, price_type, qty, pla_type) num_rep, *
		from aux.mag_catalog_product_price_aud_hist) t
	-- where num_rep > 1
	order by num_rep desc, store_id, product_id, price_type, qty, pla_type, aud_dateFrom



------------------------------------------------------------------------
----------------------- sales_dim_order_header ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from aux.sales_dim_order_header_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct order_id_bk)
	from aux.sales_dim_order_header_aud


------------------------------------------------------------------------
----------------------- sales_fact_order_line ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from aux.sales_fact_order_line_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct order_line_id_bk)
	from aux.sales_fact_order_line_aud


	select *
	from aux.sales_fact_order_line_aud
	where idETLBatchRun = 1
		and upd_ts is not null
