
	-- 	o.base_grand_total - ((isnull(o.base_customer_balance_refunded, 0) + isnull(o.base_total_refunded, 0)) - isnull(o.bs_customer_bal_total_refunded, 0)) local_total_after_refund_inc_vat, 

	-- (local_subtotal - local_subtotal_refund) + (local_shipping - local_shipping_refund) - (local_discount - local_discount_refund)  
	--			- (local_store_credit_used - local_store_credit_used_refund) - local_adjustment_refund 
	--			+ (local_store_credit_given - local_store_credit_used_refund)

	select *
	from #sales_order_header_measures trg
	where local_total_inc_vat <> local_total_aft_refund_inc_vat
	order by local_total_aft_refund_inc_vat

	select *
	from #sales_order_header_measures trg
	where local_store_credit_used <> 0
		and local_total_refund <> 0
	
	select *
	from #sales_order_header_measures trg
	where local_bank_online_given <> 0

	select *
	from #sales_order_header_measures trg
	where local_adjustment_refund <> 0

	-- local_total_aft_refund_inc_vat_2
	select abs(local_total_aft_refund_inc_vat - local_total_aft_refund_inc_vat_2) diff, 
		order_id_bk, 
		local_store_credit_used, local_store_credit_used_refund, 
		local_adjustment_refund, 
		local_total_inc_vat, local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, 
		local_store_credit_given, local_bank_online_given
	from
		(select order_id_bk, 
			local_subtotal, local_subtotal_refund, 
			local_shipping, local_shipping_refund, 
			local_discount, local_discount_refund, 
			local_store_credit_used, local_store_credit_used_refund, 
			local_adjustment_refund, 
			local_total_inc_vat, local_total_aft_refund_inc_vat,
			(local_subtotal - local_subtotal_refund) + (local_shipping - local_shipping_refund) - (local_discount - local_discount_refund)  
				- (local_store_credit_used - local_store_credit_used_refund) - local_adjustment_refund 
				+ (local_store_credit_given - local_store_credit_used_refund) local_total_aft_refund_inc_vat_2, 
			local_store_credit_given, local_bank_online_given
		from #sales_order_header_measures
		-- where local_total_refund <> 0
		) t
	where abs(local_total_aft_refund_inc_vat - local_total_aft_refund_inc_vat_2) > 0.1
	order by local_store_credit_given, local_adjustment_refund, diff desc, order_id_bk


	-- Total Inc Comp between Header - Line
	select *, 
		abs(sum_local_total_inc_vat - local_total_inc_vat) diff,
		abs(sum_local_aft_refund_total_inc_vat - local_total_aft_refund_inc_vat) diff_2
	from
		(select ol.order_id_bk, ol.order_line_id_bk, ol.local_subtotal, ol.local_shipping, ol.local_discount, ol.local_store_credit_used, ol.local_total_inc_vat local_total_inc_vat_ol, 
			sum(ol.local_total_inc_vat) over (partition by ol.order_id_bk) sum_local_total_inc_vat, 
			sum(ol.local_total_aft_refund_inc_vat) over (partition by ol.order_id_bk) sum_local_aft_refund_total_inc_vat, 
			o.local_total_inc_vat, o.local_total_aft_refund_inc_vat
		from 
				Landing.aux.sales_order_line_measures ol
			inner join
				#sales_order_header_measures o on ol.order_id_bk = o.order_id_bk) t
	where order_id_bk = 5306521
	-- order by diff desc, order_id_bk, order_line_id_bk 
	order by diff_2 desc, order_id_bk, order_line_id_bk 


	-- Total Inc Values after Refund per OL for PARTIAL REFUND // REFUND ORDERS
	select ol.order_line_id_bk, ol.order_id_bk, ol.order_status_name_bk, 
		ol.local_subtotal, ol.local_subtotal_refund, ol.local_shipping, ol.local_shipping_refund, 
		-- local_discount, local_discount_refund, local_store_credit_used,	local_store_credit_used_refund, 
		ol.local_adjustment_refund, 
		ol.local_store_credit_given, ol.local_bank_online_given, 
		ol.local_total_inc_vat, ol.local_total_aft_refund_inc_vat, 
		ol.order_allocation_rate, ol.order_allocation_rate_aft_refund, ol.order_allocation_rate_refund
	from 
			Landing.aux.sales_order_line_measures ol
		inner join
			Landing.aux.sales_order_header_dim_order o on ol.order_id_bk = o.order_id_bk and o.order_status_name_bk = 'REFUND' -- REFUND // PARTIAL REFUND
	order by ol.order_id_bk, ol.order_line_id_bk

	select order_line_id_bk, order_id_bk, order_status_name_bk, 
		local_subtotal, local_subtotal_refund, local_shipping, local_shipping_refund, 
		-- local_discount, local_discount_refund, local_store_credit_used,	local_store_credit_used_refund, 
		local_adjustment_refund, 
		local_store_credit_given, local_bank_online_given, 
		local_total_inc_vat, local_total_aft_refund_inc_vat, 
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund
	from Landing.aux.sales_order_line_measures
	where abs(local_total_aft_refund_inc_vat - (local_total_inc_vat + local_bank_online_given)) > 0.1
		and abs(local_total_inc_vat - local_bank_online_given) > 0.1
	order by local_total_aft_refund_inc_vat

	--- 



------------------------------------------------------------------------------


select order_id_bk, 
	price_type_name_bk, 
	total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_total_refund, local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent,
	num_lines
from Landing.aux.sales_order_header_measures


select order_line_id_bk, order_id_bk, 
	order_status_name_bk, price_type_name_bk,
	qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
	local_price_unit, local_price_pack, local_price_pack_discount, 
	local_subtotal, local_shipping, local_discount, local_store_credit_used, 
	local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
	local_store_credit_given, local_bank_online_given, 
	local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
	local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
	local_product_cost, local_shipping_cost, local_total_cost, local_margin, 
	local_to_global_rate, order_currency_code, 
	discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
	order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
	num_erp_allocation_lines
from Landing.aux.sales_order_line_measures

