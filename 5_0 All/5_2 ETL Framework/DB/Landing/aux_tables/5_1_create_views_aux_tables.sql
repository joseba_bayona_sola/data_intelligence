
use Landing
go 

----------------------- mag_catalog_product_price ----------------------------

create view aux.mag_catalog_product_price_aud_v as
	select record_type, count(*) over (partition by store_id, product_id, price_type, qty) num_records, 
		store_id, store_name, product_id, product_name, product_lifecycle, 
		value, qty, min_qty_prod, num_packs_tier_price, 
		total_price, pack_price, disc_percent, max_pack_price, 
		promo_key, price_type, pla_type,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from 
		(select 'N' record_type, store_id, store_name, product_id, product_name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, disc_percent, max_pack_price, 
			promo_key, price_type, pla_type,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from aux.mag_catalog_product_price_aud
		union
		select 'H' record_type, store_id, store_name, product_id, product_name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, disc_percent, max_pack_price, 
			promo_key, price_type, pla_type,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from aux.mag_catalog_product_price_aud_hist) t 
go 




drop view aux.act_customer_cr_reg_v
go 

create view aux.act_customer_cr_reg_v as
	select customer_id_bk, 
		case 
			when (store_id_first_all is null) then 1
			when (store_id_first_all is not null and store_id_last_non_vd is null) then 2
			when (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) then 3
			when (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) then 4
			else 5
		end cust_type,
		email, crreg.store_id, created_at, 
		old_access_cust_no, store_id_oacn, 
		num_dist_websites, 
		store_id_first_all, first_all_date, store_id_first_vd, first_vd_date, 
		store_id_first_non_vd, first_non_vd_date, store_id_last_non_vd, last_non_vd_date, non_vd_customer_status_at_migration_date,
		store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date, migration_date, 
		store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date, migration_date_oacn, 
		store_id_bk_cr, s1.store_name store_name_cr, create_date, 
		store_id_bk_reg, s2.store_name store_name_reg, register_date
	from 
			Landing.aux.act_customer_cr_reg crreg
		inner join
			Landing.aux.mag_gen_store_v s1 on crreg.store_id_bk_cr = s1.store_id
		inner join
			Landing.aux.mag_gen_store_v s2 on crreg.store_id_bk_reg = s2.store_id
go

-------------------------------------------------------------

drop view aux.mend_stock_batchstockmovement_v
go 

create view aux.mend_stock_batchstockmovement_v as

	select batchstockmovementid_bk, 
		batch_stock_move_id, move_id, move_line_id, 
		warehousestockitembatchid_bk, stock_adjustment_type_bk, stock_movement_type_bk, stock_movement_period_bk, 
		qty_movement, 
		case 
			when (stock_adjustment_type_bk in ('Manual_Positive')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_2')) then qty_movement
			when (stock_adjustment_type_bk in ('Positive') and stock_movement_period_bk in ('SM_PERIOD_1') and qty_movement <> wsib.receivedquantity) then qty_movement 
			else 0 
		end qty_registered, 
		case when (stock_adjustment_type_bk in ('Negative', 'Manual_Negative')) then qty_movement else 0 end qty_disposed, 
		batch_movement_date
	from 
			(select batchstockmovementid batchstockmovementid_bk, 
				batchstockmovement_id batch_stock_move_id, 
				case 
					when (moveid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (moveid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else moveid
				end	move_id, 
				case 
					when (movelineid is null) then 'M' + convert(varchar, batchstockmovement_id) 
					when (movelineid = '') then 'SS' + convert(varchar, batchstockmovement_id)
					else movelineid
				end	move_line_id, 	

				warehousestockitembatchid warehousestockitembatchid_bk, 
				case when (stockadjustment is null) then batchstockmovementtype else stockadjustment end stock_adjustment_type_bk, 
				case when (movetype is null) then 'MANUAL' else movetype end stock_movement_type_bk, 
				smp.stock_movement_period stock_movement_period_bk, 
	
				quantity qty_movement, 
				isnull(batchstockmovement_createdate, stockmovement_createdate) batch_movement_date	
			from 
					Landing.mend.gen_wh_batchstockmovement_v bsm
				inner join
					Landing.map.erp_stock_movement_period_aud smp on isnull(bsm.batchstockmovement_createdate, bsm.stockmovement_createdate) between smp.period_start and smp.period_finish
			) bsm
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsm.warehousestockitembatchid_bk = wsib.warehousestockitembatchid
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
go


drop view aux.mend_stock_warehousestockitembatch_v
go 

create view aux.mend_stock_warehousestockitembatch_v as

	select warehousestockitembatchid warehousestockitembatchid_bk, 
		wsib.warehousestockitemid warehousestockitemid_bk, 
		case 
			when (receiptlineid is null and batchstockmovement_f = 0) then 'NO_RECEIPT_NO_MOV'
			when (receiptlineid is null and batchstockmovement_f = 1) then 'NO_RECEIPT_MOV'
			when (receiptlineid is not null and batchstockmovement_f = 0) then 'RECEIPT_NO_MOV'
			when (receiptlineid is not null and batchstockmovement_f = 1) then 'RECEIPT_MOV'
		end stock_batch_type_bk,
		batch_id, fullyallocated fully_allocated_f, fullyissued fully_issued_f, autoadjusted auto_adjusted_f, 

		receivedquantity qty_received, receivedquantity + registeredQuantity - allocatedquantity - disposedQuantity - onHoldQuantity qty_available, allocatedquantity - issuedquantity qty_outstanding_allocation, 
		allocatedquantity qty_allocated_stock, issuedquantity qty_issued_stock, 
		onholdquantity qty_on_hold, registeredquantity qty_registered, disposedquantity qty_disposed, 
		isnull(wsibsm.qty_registered, 0) qty_registered_sm, isnull(wsibsm.qty_disposed, 0) qty_disposed_sm, 

		convert(datetime, arrivedDate) batch_arrived_date, convert(datetime, confirmedDate) batch_confirmed_date, convert(datetime, stockregistereddate) batch_stock_register_date, 

		productUnitCost local_product_unit_cost, carriageUnitCost local_carriage_unit_cost, dutyUnitCost local_duty_unit_cost, totalUnitCost local_total_unit_cost, 
		interCoCarriageUnitCost local_interco_carriage_unit_cost, totalUnitCostIncInterCo local_total_unit_cost_interco, 
		exchangeRate local_to_global_rate, currency currency_code
	from 
			Landing.mend.gen_wh_warehousestockitembatch_v wsib
		left join
			(select warehousestockitembatchid_bk, 
				sum(qty_registered) qty_registered, sum(qty_disposed) qty_disposed
			from Landing.aux.mend_stock_batchstockmovement_v
			group by warehousestockitembatchid_bk) wsibsm on wsib.warehousestockitembatchid = wsibsm.warehousestockitembatchid_bk
		inner join
			Landing.aux.stock_warehousestockitem awsi on wsib.warehousestockitemid = awsi.warehousestockitemid
		inner join
			Landing.aux.mag_prod_product_family_v pf on wsib.productfamilyid = pf.productfamilyid
go
