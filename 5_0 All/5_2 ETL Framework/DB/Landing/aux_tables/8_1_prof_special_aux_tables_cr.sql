
-- mag_sales_flat_creditmemo
	-- when having many rows per H - I (num_rep_rows): entity_id, increment_id, created_at belong to first created row
select order_id, num_order, 
	entity_id, increment_id, created_at,
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative 
from Landing.aux.mag_sales_flat_creditmemo
where num_order = 2
-- where sum_base_subtotal = 0 and num_order = 1
where num_rep_rows in (5, 7) or order_id in (5653465, 5809582, 5732174)

	-- num_order: 1: rows in CR_H and CR_I // 2: rows only in CR_H
	select num_order, count(*)
	from Landing.aux.mag_sales_flat_creditmemo
	group by num_order
	order by num_order

	-- num_rep_rows: number of creditmemo rows per H - I 
	select num_order, num_rep_rows, count(*)
	from Landing.aux.mag_sales_flat_creditmemo
	group by num_order, num_rep_rows
	order by num_order, num_rep_rows

	-- orders with CR_H and CR_I records
	select order_id, count(*)
	from Landing.aux.mag_sales_flat_creditmemo
	group by order_id
	having count(*) > 1
	order by order_id



-- mag_sales_flat_creditmemo_item
select order_id, order_item_id, 
	entity_id, increment_id, created_at, item_id,
	num_rep_rows, 
	sum_qty, sum_base_row_total, sum_base_discount_amount
from Landing.aux.mag_sales_flat_creditmemo_item
where order_id = 4426181

	select num_rep_rows, count(*)
	from Landing.aux.mag_sales_flat_creditmemo_item
	group by num_rep_rows
	order by num_rep_rows

-- ORDERS WITH 2 CREDITMEMO DIFF RECORDS
select order_id, num_order, 
	entity_id, increment_id, 
	num_rep_rows, 
	sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
	sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative 
from Landing.aux.mag_sales_flat_creditmemo
where order_id in 
	(select distinct order_id
	from Landing.aux.mag_sales_flat_creditmemo
	where num_order = 2)
order by order_id, num_order

select order_id, 
	sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
	sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
	sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative
from Landing.aux.mag_sales_flat_creditmemo
where order_id = 4439305
group by order_id
--having sum(isnull(sum_base_discount_amount, 0)) <> 0

-- ORDERS HEADER NUM COMPR
select order_id, 
	case when (sum_base_discount_amount < 0) then sum_base_discount_amount * -1 else sum_base_discount_amount end,
	sum_base_grand_total,
	sum_base_subtotal + sum_base_shipping_amount - sum_base_discount_amount - sum_base_customer_balance_amount + sum_base_adjustment calc_sum_base_grand_total, 
		-- (case when (sum_base_discount_amount < 0) then sum_base_discount_amount * -1 else sum_base_discount_amount end)
	abs(sum_base_grand_total - (sum_base_subtotal + sum_base_shipping_amount - sum_base_discount_amount - sum_base_customer_balance_amount + sum_base_adjustment)) diff
from
	(select order_id, 
		sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
		sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
		sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative
	from Landing.aux.mag_sales_flat_creditmemo
	group by order_id) t
--where abs(sum_base_grand_total - (sum_base_subtotal + sum_base_shipping_amount - sum_base_discount_amount - sum_base_customer_balance_amount + sum_base_adjustment)) > 0.1
order by diff desc

	-- WITHOUT CONVERTING THE DISCOUNT
	select cr.order_id, diff,
		sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
		sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
		sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative
	from 
		Landing.aux.mag_sales_flat_creditmemo cr
	 inner join
		(select order_id, 
			abs(sum_base_grand_total - (sum_base_subtotal + sum_base_shipping_amount - sum_base_discount_amount - sum_base_customer_balance_amount + sum_base_adjustment)) diff
		from
			(select order_id, 
				sum(isnull(sum_base_subtotal, 0)) sum_base_subtotal, sum(isnull(sum_base_shipping_amount, 0)) sum_base_shipping_amount, sum(isnull(sum_base_discount_amount, 0)) sum_base_discount_amount, 
				sum(isnull(sum_base_customer_balance_amount, 0)) sum_base_customer_balance_amount, sum(isnull(sum_base_grand_total, 0)) sum_base_grand_total, 
				sum(isnull(sum_base_adjustment, 0)) sum_base_adjustment, sum(isnull(sum_base_adjustment_positive, 0)) sum_base_adjustment_positive, sum(isnull(sum_base_adjustment_negative, 0)) sum_base_adjustment_negative
			from Landing.aux.mag_sales_flat_creditmemo
			group by order_id) t
		where abs(sum_base_grand_total - (sum_base_subtotal + sum_base_shipping_amount - sum_base_discount_amount - sum_base_customer_balance_amount + sum_base_adjustment)) > 0.1) t on cr.order_id = t.order_id
	group by cr.order_id, diff
	order by cr.order_id

-- ORDER CREDITMEMO HEADER VS ITEM (SUBTOTAL = 0)

select cr.order_id, cri.order_item_id, 
	cr.entity_id, cr.increment_id, 
	cr.num_rep_rows, 
	cr.sum_base_subtotal, cr.sum_base_shipping_amount, cr.sum_base_discount_amount, cr.sum_base_customer_balance_amount, cr.sum_base_grand_total, 
	cr.sum_base_adjustment, 
	cri.num_rep_rows, 
	cri.sum_qty, cri.sum_base_row_total
from 
	Landing.aux.mag_sales_flat_creditmemo cr
inner join
	Landing.aux.mag_sales_flat_creditmemo_item cri on cr.order_id = cri.order_id
where cr.sum_base_subtotal = 0 and cr.num_order = 1
order by cr.order_id, cri.order_item_id

-- ORDER CREDITMEMO HEADER VS ITEM (SUBTOTAL ITEMS vs SUM_SUBTOTAL 0)

select top 1000 count(*) over () num_tot, *
from 
	(select cr.order_id, cri.order_item_id, 
		cr.entity_id, cr.increment_id, 
		cr.num_rep_rows, 
		sum(cri.sum_base_row_total) over (partition by cr.order_id) sum_subtotal_item,
		cr.sum_base_subtotal, cr.sum_base_shipping_amount, cr.sum_base_discount_amount, cr.sum_base_customer_balance_amount, cr.sum_base_grand_total, 
		cr.sum_base_adjustment, 
		cri.num_rep_rows num_rep_rows_cri, 
		cri.sum_qty, cri.sum_base_row_total	
	from 
			Landing.aux.mag_sales_flat_creditmemo cr
		inner join
			Landing.aux.mag_sales_flat_creditmemo_item cri on cr.order_id = cri.order_id
	where cr.num_order = 1) t
where abs(sum_subtotal_item - sum_base_subtotal) > 0
order by abs(sum_subtotal_item - sum_base_subtotal) desc, order_id, order_item_id


-- ORDER CREDITMEMO HEADER VS ITEM (DISCOUNT ITEMS vs SUM_DISCOUNT 0)

select top 1000 count(*) over () num_tot, *
from 
	(select cr.order_id, cri.order_item_id, 
		cr.entity_id, cr.increment_id, 
		cr.num_rep_rows, 
		sum(cri.sum_base_discount_amount) over (partition by cr.order_id) sum_discount_item,
		cr.sum_base_subtotal, cr.sum_base_shipping_amount, cr.sum_base_discount_amount, cr.sum_base_customer_balance_amount, cr.sum_base_grand_total, 
		cr.sum_base_adjustment, 
		cri.num_rep_rows num_rep_rows_cri, 
		cri.sum_qty, cri.sum_base_row_total	
	from 
			Landing.aux.mag_sales_flat_creditmemo cr
		inner join
			Landing.aux.mag_sales_flat_creditmemo_item cri on cr.order_id = cri.order_id
	where cr.num_order = 1) t
where abs(sum_discount_item - sum_base_discount_amount) > 0
order by abs(sum_discount_item - sum_base_discount_amount) desc, order_id, order_item_id
