
use Landing
go 


drop procedure mag.srcmag_lnd_get_aux_mag_tables
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Creates Magento related Auxiliary Tables 
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_aux_mag_tables
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Landing.aux.mag_core_config_data_store
	delete from Landing.aux.mag_core_config_data_store

	insert into Landing.aux.mag_core_config_data_store (store_id, scope_id, path, value)
		select
			case when (t2.store_id is null) then t1.store_id else t2.store_id end store_id, 
			case when (t2.scope_id is null) then t1.scope_id else t2.scope_id end scope_id, 
			case when (t2.path is null) then t1.path else t2.path end path, 
			case when (t2.value is null) then t1.value else t2.value end value
		from
			(select cs.store_id, cd.scope_id, cd.path, cd.value
			from 
					Landing.mag.core_config_data cd
				cross join
					Landing.mag.core_store cs
			where cd.scope_id = 0 and cs.store_id <> 0) t1
		left join
			(select cs.store_id, cd.scope_id, cd.path, max(cd.value) value
			from 
					Landing.mag.core_store cs
				inner join
					Landing.mag.core_config_data cd on cs.store_id = cd.scope_id
			where cs.store_id <> 0
			group by cs.store_id, cd.scope_id, cd.path
				) t2 on t1.store_id = t2.store_id and t1.path = t2.path
		order by path, store_id	


	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.srcmag_lnd_get_aux_mag_sales_flat_creditmemo
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-05-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Creates Magento related Auxiliary Tables  - sales_flat_creditmemo
	--  Need to convert base_discount_amount to positive as sometimes in negative
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_aux_mag_sales_flat_creditmemo
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Landing.aux.mag_sales_flat_creditmemo
	delete from Landing.aux.mag_sales_flat_creditmemo

	insert into Landing.aux.mag_sales_flat_creditmemo(order_id, num_order, 
		entity_id, increment_id, created_at,
		num_rep_rows, 
		sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
		sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative) 

		select order_id, num_order,
			-- dense_rank() over (partition by order_id order by sum_base_subtotal desc, entity_id) num_order,
			entity_id, increment_id, created_at,
			num_rep_rows, 
			sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
			sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
		from
			(select 
				order_id, 1 num_order, 
				entity_id, increment_id, created_at,
				num_rep_rows, 
				sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
				sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
			from
				(select  
					cr.entity_id, cr.increment_id, cr.order_id, cr.created_at,
					dense_rank() over (partition by order_id order by cr.entity_id) num_cr,
					count(*) over (partition by order_id) num_rep_rows,
					sum(cr.base_subtotal) over (partition by order_id) sum_base_subtotal, 
					sum(cr.base_shipping_amount) over (partition by order_id) sum_base_shipping_amount, 
					sum(case when (cr.base_discount_amount < 0) then cr.base_discount_amount * -1 else cr.base_discount_amount end) over (partition by order_id) sum_base_discount_amount, 
					sum(cr.base_customer_balance_amount) over (partition by order_id) sum_base_customer_balance_amount, sum(cr.base_grand_total) over (partition by order_id) sum_base_grand_total, 
					sum(cr.base_adjustment) over (partition by order_id) sum_base_adjustment, 
					sum(cr.base_adjustment_positive) over (partition by order_id) sum_base_adjustment_positive, sum(cr.base_adjustment_negative) over (partition by order_id) sum_base_adjustment_negative, 

					cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
					cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
				from Landing.mag.sales_flat_creditmemo_aud cr
				where cr.entity_id in 
					(select distinct parent_id
					from Landing.mag.sales_flat_creditmemo_item_aud) ) t
			where num_cr = 1
			union
			select 
				order_id, 2 num_order, 
				entity_id, increment_id, created_at,
				num_rep_rows, 
				sum_base_subtotal, sum_base_shipping_amount, sum_base_discount_amount, sum_base_customer_balance_amount, sum_base_grand_total, 
				sum_base_adjustment, sum_base_adjustment_positive, sum_base_adjustment_negative
			from
				(select  
					cr.entity_id, cr.increment_id, cr.order_id, cr.created_at,
					dense_rank() over (partition by order_id order by cr.entity_id) num_cr,
					count(*) over (partition by order_id) num_rep_rows,
					sum(cr.base_subtotal) over (partition by order_id) sum_base_subtotal, 
					sum(cr.base_shipping_amount) over (partition by order_id) sum_base_shipping_amount, 
					sum(case when (cr.base_discount_amount < 0) then cr.base_discount_amount * -1 else cr.base_discount_amount end) over (partition by order_id) sum_base_discount_amount, 
					sum(cr.base_customer_balance_amount) over (partition by order_id) sum_base_customer_balance_amount, sum(cr.base_grand_total) over (partition by order_id) sum_base_grand_total, 
					sum(cr.base_adjustment) over (partition by order_id) sum_base_adjustment, 
					sum(cr.base_adjustment_positive) over (partition by order_id) sum_base_adjustment_positive, sum(cr.base_adjustment_negative) over (partition by order_id) sum_base_adjustment_negative, 

					cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
					cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
				from 
						Landing.mag.sales_flat_creditmemo_aud cr
					left join	
						Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id
				where cri.parent_id is null) t
			where num_cr = 1) t
		order by order_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure mag.srcmag_lnd_get_aux_mag_sales_flat_creditmemo_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-05-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Creates Magento related Auxiliary Tables - sales_flat_creditmemo_item
	-- No need to convert base_discount_amount as it is always positive
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_aux_mag_sales_flat_creditmemo_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Landing.aux.mag_sales_flat_creditmemo_item
	delete from Landing.aux.mag_sales_flat_creditmemo_item

	insert into Landing.aux.mag_sales_flat_creditmemo_item(order_id, order_item_id, 
		entity_id, increment_id, created_at, item_id,
		num_rep_rows, 
		sum_qty, sum_base_row_total, sum_base_discount_amount)

		select order_id, order_item_id, 
			entity_id, increment_id, created_at, item_id,
			num_rep_rows, 
			sum_qty, sum_base_row_total, sum_base_discount_amount
		from
			(select  
				cr.entity_id, cr.increment_id, cr.created_at, cr.order_id, cri.order_item_id, cri.entity_id item_id,
				dense_rank() over (partition by order_id, order_item_id order by cr.entity_id) num_cr,
				count(*) over (partition by order_id, order_item_id) num_rep_rows,
				sum(cri.qty) over (partition by order_id, order_item_id) sum_qty,
				sum(cri.base_row_total) over (partition by order_id, order_item_id) sum_base_row_total,
				sum(cri.base_discount_amount) over (partition by order_id, order_item_id) sum_base_discount_amount,
				cri.qty, cri.base_row_total, 
				cr.base_subtotal, cr.base_shipping_amount, cr.base_discount_amount, cr.base_customer_balance_amount, cr.base_grand_total, 
				cr.base_adjustment, cr.base_adjustment_positive, cr.base_adjustment_negative
			from 
					Landing.mag.sales_flat_creditmemo_aud cr
				inner join	
					Landing.mag.sales_flat_creditmemo_item_aud cri on cr.entity_id = cri.parent_id) t
		where num_cr = 1
		order by order_id, order_item_id

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.srcmag_lnd_get_aux_mag_edi_stock_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-05-2017
-- Changed: 
	--	17-07-2018	Joseba Bayona Sola	Change BC parsing - after replica bc values too long
-- ==========================================================================================
-- Description: Creates Magento related Auxiliary Tables - edi_stock_item
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_aux_mag_edi_stock_item
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Landing.aux.mag_edi_stock_item
	delete from Landing.aux.mag_edi_stock_item

	insert into Landing.aux.mag_edi_stock_item(product_id, product_code, 
		bc, di, po, cy, ax, ad, do, co)

	select 	
		product_id, product_code, 
		max(bc) bc, max(di) di, max(po) po, max(cy) cy, max(ax) ax, max(ad) ad, max(do) do, max(co) co
	from
		(select distinct
			product_id, product_code, 
			left(bc, 3) bc, 
			left(di, 4) di, 
			case 
				when (len(po) = 5 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 5)
				when (len(po) = 5 and charindex('.', po) = 4) then po + '0'
				when (len(po) = 1) then '+0' + po + '.00'
				when (len(po) = 2) then substring(po, 1, 1) + '0' + substring(po, 2, 1) + '.00'
				when (len(po) = 3 and charindex('-', po) = 1) then po + '.00'
				when (len(po) = 3 and charindex('-', po) = 0) then '+0' + po + '0'
				when (len(po) = 4 and charindex('.', po) = 3) then substring(po, 1, 1) + '0' + substring(po, 2, 3) + '0'
				when (len(po) = 4 and charindex('.', po) = 2) then '+0' + po
				when (len(po) = 12) then '+0' + substring(po, 1, 4)
				when (len(po) = 13) then substring(po, 1, 1) + '0' + + substring(po, 2, 4)
				when (len(po) = 14) then substring(po, 1, 6)
				else po
			end po,
			cy, 
			case
				when (len(ax) = 1) then '00' + ax
				when (len(ax) = 2) then '0' + ax
				else ax end ax, 
			case 
				when ad in ('Medium', 'MF', 'MID') then 'MED'
				else ad end ad, 
			do, 
			co
		from 
			(select product_id, sku, product_code, 
				case when (bc = '') then NULL else bc end bc, case when (di = '') then NULL else di end di, case when (po = '') then NULL else replace(po, char(13), '') end po, 
				case when (cy = '') then NULL else cy end cy, case when (ax = '') then NULL else ax end ax, 
				case when (ad = '') then NULL else ad end ad, case when (do = '') then NULL else do end do, case when (co = '') then NULL else co end co 
			from
				(select distinct product_id, sku, product_code, 
					bc, di, po, cy, ax, ad, do, co
				from Landing.mag.edi_stock_item_aud) esi) esi) t
	group by product_id, product_code

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 





drop procedure mag.srcmag_lnd_get_aux_mag_catalog_product_price
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-05-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Creates Magento related Auxiliary Tables - catalog_product_price
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_aux_mag_catalog_product_price
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- Landing.aux.mag_edi_stock_item
	delete from Landing.aux.mag_catalog_product_price

	insert into Landing.aux.mag_catalog_product_price(store_id, store_name, product_id, product_name, product_lifecycle, 
		value, qty, min_qty_prod, num_packs_tier_price, 
		total_price, pack_price, disc_percent, max_pack_price, 
		promo_key, price_type, pla_type,
		idETLBatchRun)

		select store_id, store_name, product_id, name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, 
			(max_pack_price - pack_price) * 100 / case when (max_pack_price = 0) then 1 else max_pack_price end disc_percent, max_pack_price,
			promo_key, 
			case when (promo_key <> '') then 'PLA' else
				case when (num_packs_tier_price > 1) then 'Tier Pricing' else 'Regular' end end price_type, 
			case when (len(left(promo_key, 3)) = 0) then 'REG' else left(promo_key, 3) end pla_type,
			@idETLBatchRun
		from
			(select store_id, store_name, product_id, name, product_lifecycle, 
				value, qty, min_qty_prod, qty / min_qty_prod num_packs_tier_price, 
				total_price, total_price / (qty / min_qty_prod) pack_price, 
				promo_key, 
				max(total_price / (qty / min_qty_prod)) over (partition by product_id, store_id) max_pack_price
			from
				(select tp.website_id store_id, s.name store_name, tp.entity_id product_id, p.name, p.product_lifecycle, 
					tp.value, tp.qty, 
					min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
					tp.value * tp.qty total_price,
					tp.promo_key
				from 
						Landing.mag.catalog_product_entity_tier_price tp
					inner join
						(select entity_id, sku, name, product_lifecycle
						from Landing.mag.catalog_product_entity_flat_aud 
						where store_id = 0) p on tp.entity_id = p.entity_id 
					inner join
						Landing.mag.core_store_aud s on tp.website_id = s.store_id
				where tp.customer_group_id = 0 and tp.all_groups = 1) t) t
		where store_id not in (0, 29) 
		order by product_id, store_id, qty, promo_key

	insert into Landing.aux.mag_catalog_product_price(store_id, store_name, product_id, product_name, product_lifecycle, 
		value, qty, min_qty_prod, num_packs_tier_price, 
		total_price, pack_price, disc_percent, max_pack_price, 
		promo_key, price_type, pla_type,
		idETLBatchRun)

		select 29 store_id, 'visiondirect.be/fr' store_name, 
			product_id, name, product_lifecycle, 
			value, qty, min_qty_prod, num_packs_tier_price, 
			total_price, pack_price, 
			(max_pack_price - pack_price) * 100 / case when (max_pack_price = 0) then 1 else max_pack_price end disc_percent, max_pack_price,
			promo_key, 
			case when (promo_key <> '') then 'PLA' else
				case when (num_packs_tier_price > 1) then 'Tier Pricing' else 'Regular' end end price_type, 
			case when (len(left(promo_key, 3)) = 0) then 'REG' else left(promo_key, 3) end pla_type,
			@idETLBatchRun
		from
			(select store_id, store_name, product_id, name, product_lifecycle, 
				value, qty, min_qty_prod, qty / min_qty_prod num_packs_tier_price, 
				total_price, total_price / (qty / min_qty_prod) pack_price, 
				promo_key, 
				max(total_price / (qty / min_qty_prod)) over (partition by product_id, store_id) max_pack_price
			from
				(select tp.website_id store_id, s.name store_name, tp.entity_id product_id, p.name, p.product_lifecycle, 
					tp.value, tp.qty, 
					min(qty) over (partition by tp.entity_id, tp.website_id) min_qty_prod,
					tp.value * tp.qty total_price,
					tp.promo_key
				from 
						Landing.mag.catalog_product_entity_tier_price tp
					inner join
						(select entity_id, sku, name, product_lifecycle
						from Landing.mag.catalog_product_entity_flat_aud 
						where store_id = 0) p on tp.entity_id = p.entity_id 
					inner join
						Landing.mag.core_store_aud s on tp.website_id = s.store_id
				where tp.customer_group_id = 0 and tp.all_groups = 1) t) t
		where store_id in (28) 
		order by product_id, store_id, qty, promo_key

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 



drop procedure aux.lnd_lnd_merge_aux_sales_exchange_rate
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-01-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges data for Exchange Rate table from OH values (EUR TO GBP)
-- ==========================================================================================

create procedure aux.lnd_lnd_merge_aux_sales_exchange_rate
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Landing.aux.sales_exchange_rate with (tablock) as trg
	using  
		(select exchange_rate_day, local_to_global_rate eur_to_gbp, convert(decimal(12, 4), 1 / local_to_global_rate) gbp_to_eur
		from
			(select distinct exchange_rate_day, local_to_global_rate
			from
				(select convert(date, order_date) exchange_rate_day, local_to_global_rate, 
					count(*) over (partition by convert(date, order_date)) num_rep, 
					rank() over (partition by convert(date, order_date) order by count(*)) ord_rep
				from Landing.aux.sales_dim_order_header_aud
				where order_source = 'M'
					and order_currency_code = 'EUR'
					and order_status_name_bk = 'OK'
				group by convert(date, order_date), local_to_global_rate) t
			where num_rep = ord_rep) t) src
		on (trg.exchange_rate_day = src.exchange_rate_day)
	when matched and not exists 
		(select isnull(trg.eur_to_gbp, 0), isnull(trg.gbp_to_eur, 0)
		intersect
		select isnull(src.eur_to_gbp, 0), isnull(src.gbp_to_eur, 0))
		then 
			update set
				trg.eur_to_gbp = src.eur_to_gbp, trg.gbp_to_eur = src.gbp_to_eur, 
				trg.idETLBatchRun_upd = @idETLBatchRun, trg.upd_ts = getutcdate()
	when not matched
		then 
			insert (exchange_rate_day, eur_to_gbp, gbp_to_eur, idETLBatchRun_ins)
				values (src.exchange_rate_day, src.eur_to_gbp, src.gbp_to_eur, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure mag.srcmag_lnd_merge_aux_gdpr_customer_list
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 25-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Updates the list of GDPR customers who wants personal data removed
-- ==========================================================================================

create procedure mag.srcmag_lnd_merge_aux_gdpr_customer_list
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	-- MERGE STATEMENT 
	merge into Landing.aux.gdpr_customer_list with (tablock) as trg
	using Landing.map.gen_gdpr_customer_list src
		on (trg.customer_id = src.customer_id)

	when not matched
		then 
			insert (customer_id, idETLBatchRun_ins)
				values (src.customer_id, @idETLBatchRun)
			
	OUTPUT $action INTO #MergeResults;			

	update Landing.aux.gdpr_customer_list
	set 
		email = 'XXXXXXX - ' + convert(varchar, num_customer), 
		firstname = 'XXXXXXX - ' + convert(varchar, num_customer), 
		lastname = 'XXXXXXX - ' + convert(varchar, num_customer), 
		street = 'XXXXXXX - ' + convert(varchar, num_customer), 
		city = 'XXXXXXX - ' + convert(varchar, num_customer), 
		postcode = 'XXXXXXX - ' + convert(varchar, num_customer)

	SELECT  @rowAmountInsert = [INSERT], @rowAmountUpdate = [UPDATE], @rowAmountDelete = [DELETE]
	FROM    
			(SELECT mergeAction, 1 rows
			 FROM #MergeResults) c 
		 PIVOT
			(COUNT(rows) FOR mergeAction IN ([INSERT], [UPDATE], [DELETE])) AS pvt	

	set @message = 'INSERT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountInsert) 
		+ ' UPDATE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountUpdate) 
		+ ' DELETE # Rows: ' + CONVERT(VARCHAR(10), @rowAmountDelete) 

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


drop procedure mag.srcmag_lnd_merge_aux_gdpr_customer_entity
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges the list of GDPR customers into the source customer entity tables
-- ==========================================================================================

create procedure mag.srcmag_lnd_merge_aux_gdpr_customer_entity
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	select customer_id, email, firstname, lastname, street, city, postcode
	into #gdpr_customer
	from Landing.aux.gdpr_customer_list
	where idETLBatchRun_ins = @idETLBatchRun
	
	set @rowAmountInsert = @@ROWCOUNT

	-- mag.customer_entity
	delete from Landing.mag.customer_entity
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity (entity_id, increment_id, email, 
			entity_type_id, attribute_set_id, 
			group_id, website_group_id, website_id, store_id, 
			is_active, disable_auto_group_change, created_at, updated_at, 
			idETLBatchRun)

		select entity_id, increment_id, gcl.email, 
			entity_type_id, attribute_set_id, 
			group_id, website_group_id, website_id, store_id, 
			is_active, disable_auto_group_change, created_at, updated_at, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_aud c
			inner join
				#gdpr_customer gcl on c.entity_id = gcl.customer_id

	-- mag.customer_entity_datetime
	delete from Landing.mag.customer_entity_datetime
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity_datetime (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_datetime_aud cd
			inner join
				#gdpr_customer gcl on cd.entity_id = gcl.customer_id

	-- mag.customer_entity_decimal
	delete from Landing.mag.customer_entity_decimal
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity_decimal (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_decimal_aud cd
			inner join
				#gdpr_customer gcl on cd.entity_id = gcl.customer_id

	-- mag.customer_entity_int
	delete from Landing.mag.customer_entity_int
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity_int (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_int_aud cd
			inner join
				#gdpr_customer gcl on cd.entity_id = gcl.customer_id

	-- mag.customer_entity_text
	delete from Landing.mag.customer_entity_text
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity_text (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_text_aud cd
			inner join
				#gdpr_customer gcl on cd.entity_id = gcl.customer_id

	-- mag.customer_entity_varchar
	delete from Landing.mag.customer_entity_varchar
	where entity_id in 
		(select customer_id
		from #gdpr_customer)

	insert into Landing.mag.customer_entity_varchar (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, 
			case
				when (attribute_id = 5) then gcl.firstname
				when (attribute_id = 7) then gcl.lastname
				else value
			end value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_entity_varchar_aud cd
			inner join
				#gdpr_customer gcl on cd.entity_id = gcl.customer_id

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

	drop table #gdpr_customer
end;
go 


drop procedure mag.srcmag_lnd_merge_aux_gdpr_customer_address_entity
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges the list of GDPR customers into the source customer entity tables
-- ==========================================================================================

create procedure mag.srcmag_lnd_merge_aux_gdpr_customer_address_entity
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	select gcl.customer_id, cef.default_billing customer_address_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
	into #gdpr_customer_address
	from 
			Landing.aux.gdpr_customer_list gcl
		inner join
			Landing.mag.customer_entity_flat_aud cef on gcl.customer_id = cef.entity_id
	where gcl.idETLBatchRun_ins = @idETLBatchRun
	union
	select gcl.customer_id, cef.default_shipping customer_address_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
	from 
			Landing.aux.gdpr_customer_list gcl
		inner join
			Landing.mag.customer_entity_flat_aud cef on gcl.customer_id = cef.entity_id
	where gcl.idETLBatchRun_ins = @idETLBatchRun

	set @rowAmountInsert = @@ROWCOUNT

	-- mag.customer_address_entity
	delete from Landing.mag.customer_address_entity
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity (entity_id, increment_id, parent_id, 
		entity_type_id, attribute_set_id, is_active, 
		created_at, updated_at, 
		idETLBatchRun)

		select entity_id, increment_id, parent_id, 
			entity_type_id, attribute_set_id, is_active, 
			created_at, updated_at, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_aud c
			inner join
				#gdpr_customer_address gcl on c.entity_id = gcl.customer_address_id

	-- mag.customer_address_entity_datetime
	delete from Landing.mag.customer_address_entity_datetime
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity_datetime (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_datetime_aud cd
			inner join
				#gdpr_customer_address gcl on cd.entity_id = gcl.customer_address_id

	-- mag.customer_address_entity_decimal
	delete from Landing.mag.customer_address_entity_decimal
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity_decimal (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_decimal_aud cd
			inner join
				#gdpr_customer_address gcl on cd.entity_id = gcl.customer_address_id

	-- mag.customer_address_entity_int
	delete from Landing.mag.customer_address_entity_int
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity_int (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_int_aud cd
			inner join
				#gdpr_customer_address gcl on cd.entity_id = gcl.customer_address_id

	-- mag.customer_address_entity_text
	delete from Landing.mag.customer_address_entity_text
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity_text (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, 
			case when (attribute_id = 23) then gcl.street else value end value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_text_aud cd
			inner join
				#gdpr_customer_address gcl on cd.entity_id = gcl.customer_address_id

	-- mag.customer_address_entity_varchar
	delete from Landing.mag.customer_address_entity_varchar
	where entity_id in 
		(select customer_address_id
		from #gdpr_customer_address)

	insert into Landing.mag.customer_address_entity_varchar (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun)

		select value_id, 
			entity_type_id, attribute_id, entity_id, 
			case 
				when (attribute_id = 18) then gcl.firstname
				when (attribute_id = 20) then gcl.lastname
				when (attribute_id = 24) then gcl.city
				when (attribute_id = 28) then gcl.postcode
				else value
			end value, 
			@idETLBatchRun
		from 
				Landing.mag.customer_address_entity_varchar_aud cd
			inner join
				#gdpr_customer_address gcl on cd.entity_id = gcl.customer_address_id


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

	drop table #gdpr_customer_address

end;
go 


drop procedure mag.srcmag_lnd_merge_aux_gdpr_sales_flat_order
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges the list of GDPR customers into the source customer entity tables
-- ==========================================================================================

create procedure mag.srcmag_lnd_merge_aux_gdpr_sales_flat_order
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	select customer_id, email, firstname, lastname, street, city, postcode
	into #gdpr_customer
	from Landing.aux.gdpr_customer_list
	where idETLBatchRun_ins = @idETLBatchRun

	select t.customer_id, t.entity_id order_id, t.email, t.firstname, t.lastname, t.street, t.city, t.postcode
	into #gdpr_customer_order
	from
		(select gcl.customer_id, oh.entity_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
		from 
				Landing.mag.sales_flat_order_aud oh 
			inner join
				#gdpr_customer gcl on oh.customer_id = gcl.customer_id
		union 
		select gcl.customer_id, oh.entity_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
		from 
				Landing.mag.sales_flat_order_aud_hist oh 
			inner join
				#gdpr_customer gcl on oh.customer_id = gcl.customer_id) t

	set @rowAmountInsert = @@ROWCOUNT

	-- mag.sales_flat_order
	delete from Landing.mag.sales_flat_order
	where entity_id in 
		(select order_id
		from #gdpr_customer_order)


	insert into Landing.mag.sales_flat_order (entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, status, 
		total_qty_ordered, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		base_to_global_rate, base_to_order_rate, order_currency_code,
		customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
		customer_taxvat, customer_gender, customer_note, customer_note_notify, 
		shipping_description, 
		coupon_code, applied_rule_ids,
		affilBatch, affilCode, affilUserRef, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
		presc_verification_method, prescription_verification_type, 
		referafriend_code, referafriend_referer, 
		telesales_method_code, telesales_admin_username, 
		remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
		old_order_id,
		mutual_amount, mutual_quotation_id,
		idETLBatchRun) 

		select entity_id, increment_id, store_id, gcl.customer_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			state, status, 
			total_qty_ordered, 
			base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
			base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
			base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
			base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
			base_grand_total, 
			base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
			base_to_global_rate, base_to_order_rate, order_currency_code,
			customer_dob, gcl.email customer_email, gcl.firstname customer_firstname, gcl.lastname customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
			customer_taxvat, customer_gender, customer_note, customer_note_notify, 
			shipping_description, 
			coupon_code, applied_rule_ids,
			affilBatch, affilCode, affilUserRef, 
			reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
			reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
			presc_verification_method, prescription_verification_type, 
			referafriend_code, referafriend_referer, 
			telesales_method_code, telesales_admin_username, 
			remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
			old_order_id,
			mutual_amount, mutual_quotation_id,
			@idETLBatchRun
		from 
				Landing.mag.sales_flat_order_aud oh
			inner join
				#gdpr_customer_order gcl on oh.entity_id = gcl.order_id


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

	drop table #gdpr_customer

	drop table #gdpr_customer_order

end;
go 


drop procedure mag.srcmag_lnd_merge_aux_gdpr_sales_flat_order_address
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 26-06-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Merges the list of GDPR customers into the source customer entity tables
-- ==========================================================================================

create procedure mag.srcmag_lnd_merge_aux_gdpr_sales_flat_order_address
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountInsert INT, @rowAmountUpdate INT, @rowAmountDelete INT, @message varchar(500)

	create table #MergeResults (MergeAction char(6) NOT NULL);

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	select customer_id, email, firstname, lastname, street, city, postcode
	into #gdpr_customer
	from Landing.aux.gdpr_customer_list
	where idETLBatchRun_ins = @idETLBatchRun

	select distinct t.customer_id, t.entity_id order_id, oa.entity_id order_address_id, t.email, t.firstname, t.lastname, t.street, t.city, t.postcode
	into #gdpr_customer_order_address
	from
		(select gcl.customer_id, oh.entity_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
		from 
				Landing.mag.sales_flat_order_aud oh 
			inner join
				#gdpr_customer gcl on oh.customer_id = gcl.customer_id
		union 
		select gcl.customer_id, oh.entity_id, gcl.email, gcl.firstname, gcl.lastname, gcl.street, gcl.city, gcl.postcode
		from 
				Landing.mag.sales_flat_order_aud_hist oh 
			inner join
				#gdpr_customer gcl on oh.customer_id = gcl.customer_id) t
	inner join
		Landing.mag.sales_flat_order_address_aud oa on t.entity_id = oa.parent_id

	set @rowAmountInsert = @@ROWCOUNT

	-- mag.sales_flat_order_address

	delete from Landing.mag.sales_flat_order_address
	where entity_id in 
		(select order_address_id
		from #gdpr_customer_order_address)

	insert into Landing.mag.sales_flat_order_address (entity_id, parent_id, customer_address_id, customer_id, 
		address_type, 
		email, company, 
		firstname, lastname, prefix, middlename, suffix, 
		street, city, postcode, region, 
		fax, telephone, 
		region_id, country_id, 
		idETLBatchRun)

		select entity_id, parent_id, customer_address_id, oa.customer_id, 
			address_type, 
			gcl.email, company, 
			gcl.firstname, gcl.lastname, prefix, middlename, suffix, 
			gcl.street, gcl.city, gcl.postcode, region, 
			fax, telephone, 
			region_id, country_id, 
			@idETLBatchRun
		from 
				Landing.mag.sales_flat_order_address_aud oa
			inner join
				#gdpr_customer_order_address gcl on oa.entity_id = gcl.order_address_id


	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, 
		@rowAmountInsert = @rowAmountInsert, @rowAmountUpdate = @rowAmountUpdate, @rowAmountDelete = @rowAmountDelete, 
		@message = @message
		
	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'
	
	drop table #gdpr_customer

	drop table #gdpr_customer_order_address

end;
go 