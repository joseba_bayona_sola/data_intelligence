use Landing
go 

------------------------------------------------------------------------
----------------------- dw_stores_full ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_pr.dw_stores_full 
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct store_id) num_dist_rows
	from Landing.migra_pr.dw_stores_full


------------------------------------------------------------------------
----------------------- order_headers ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_pr.order_headers 
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct order_id) num_dist_rows
	from Landing.migra_pr.order_headers


------------------------------------------------------------------------
----------------------- order_lines ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_pr.order_lines 
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct line_id) num_dist_rows
	from Landing.migra_pr.order_lines


