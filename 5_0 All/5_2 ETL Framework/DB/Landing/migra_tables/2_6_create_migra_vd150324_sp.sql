use Landing
go 

--------------------- SP ----------------------------------

drop procedure migra_vd150324.migvd150324_lnd_get_migrate_customerdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 27-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensbase Table (migrate_customerdata)
-- ==========================================================================================

create procedure migra_vd150324.migvd150324_lnd_get_migrate_customerdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_customer_id, 
				new_magento_id, email, created_at, 
				domainID, domainName, 
				unsubscribe_newsletter, disable_saved_card, RAF_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_customer_id, 
					new_magento_id, email, created_at, 
					domainID, domainName, 
					unsubscribe_newsletter, disable_saved_card, RAF_code 
				from vd150324.migrate_customerdata'')'	
					
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_vd150324.migvd150324_lnd_get_migrate_orderdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 27-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensbase Table (migrate_orderdata)
-- ==========================================================================================

create procedure migra_vd150324.migvd150324_lnd_get_migrate_orderdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_order_id, 
				magento_order_id, new_increment_id, 
				store_id, website_name, domainID, domainName, 
				email, old_customer_id, magento_customer_id, 
				created_at,
				migrated, orderStatusName, 
				shipping_description, channel, source_code, 
				billing_postcode, billing_country, shipping_postcode, shipping_country, 
				base_grand_total, base_shipping_amount, base_discount_amount, 
				base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
				tax_rate, tax_free_amount, 
				order_currency, exchangeRate,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_order_id, 
					magento_order_id, new_increment_id, 
					store_id, website_name, domainID, domainName, 
					email, old_customer_id, magento_customer_id, 
					created_at,
					migrated, orderStatusName, 
					shipping_description, channel, source_code, 
					billing_postcode, billing_country, shipping_postcode, shipping_country, 
					base_grand_total, base_shipping_amount, base_discount_amount, 
					base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
					tax_rate, tax_free_amount, 
					order_currency, exchangeRate
				from vd150324.migrate_orderdata'')'


	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_vd150324.migvd150324_lnd_get_migrate_orderlinedata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 27-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensbase Table (migrate_orderlinedata)
-- ==========================================================================================

create procedure migra_vd150324.migvd150324_lnd_get_migrate_orderlinedata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_item_id, 
				old_order_id, magento_order_id, 
				product_id, productID, productName, packsize, eye, 
				base_curve, diameter, power, cylinder, axis, [add], null dominant, color, 
				quantity, row_total, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_item_id, 
					old_order_id, magento_order_id, 
					product_id, productID, productName, packsize, eye, 
					base_curve, diameter, power, cylinder, axis, `add`, color, 
					quantity, row_total
				from vd150324.migrate_orderlinedata'')'
	
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_vd150324.migvd150324_lnd_get_migrate_order_id_mapping
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 27-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensbase Table (migrate_order_id_mapping)
-- ==========================================================================================

create procedure migra_vd150324.migvd150324_lnd_get_migrate_order_id_mapping
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select internal_order_id, user_visible_order_id, 
				has_lenses, has_solutions, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select internal_order_id, user_visible_order_id, 
					has_lenses, has_solutions
				from vd150324.migrate_order_id_mapping'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

