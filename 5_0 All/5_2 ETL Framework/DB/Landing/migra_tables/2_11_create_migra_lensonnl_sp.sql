use Landing
go 

--------------------- SP ----------------------------------

drop procedure migra_lensonnl.miglensonnl_lnd_get_migrate_customerdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lenson NL Table (migrate_customerdata)
-- ==========================================================================================

create procedure migra_lensonnl.miglensonnl_lnd_get_migrate_customerdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_customer_id, 
				new_magento_id, email, created_at, 
				domainID, domainName, 
				unsubscribe_newsletter, disable_saved_card, RAF_code, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_customer_id, 
					new_magento_id, email, created_at, 
					domainID, domainName, 
					unsubscribe_newsletter, disable_saved_card, RAF_code 
				from lenson_nl.migrate_customerdata'')'	
					
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 






drop procedure migra_lensonnl.miglensonnl_lnd_get_migrate_orderdata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderdata)
-- ==========================================================================================

create procedure migra_lensonnl.miglensonnl_lnd_get_migrate_orderdata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_order_id, 
				magento_order_id, new_increment_id, 
				store_id, website_name, domainID, domainName, 
				email, old_customer_id, magento_customer_id, 
				created_at,
				migrated, null orderStatusName, 
				shipping_description, null channel, null source_code, 
				billing_postcode, billing_country, shipping_postcode, shipping_country, 
				base_grand_total, base_shipping_amount, base_discount_amount, 
				null base_grand_total_exc_vat, null base_shipping_amount_exc_vat, null base_discount_amount_exc_vat, 
				null tax_rate, null tax_free_amount, 
				order_currency, null exchangeRate,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_order_id, 
					magento_order_id, new_increment_id, 
					store_id, website_name, domainID, domainName, 
					email, old_customer_id, magento_customer_id, 
					created_at,
					migrated, 
					shipping_description, 
					billing_postcode, billing_country, shipping_postcode, shipping_country, 
					base_grand_total, base_shipping_amount, base_discount_amount, 
					order_currency
				from lenson_nl.migrate_orderdata'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_lensonnl.miglensonnl_lnd_get_migrate_orderlinedata
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 02-10-2017
-- Changed: 
	--	14-02-2018	Joseba Bayona Sola	old_product_id in productID attribute
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderlinedata)
-- ==========================================================================================

create procedure migra_lensonnl.miglensonnl_lnd_get_migrate_orderlinedata
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select old_item_id, 
				old_order_id, magento_order_id, 
				product_id, old_product_id productID, productName, packsize, eye, 
				base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
				quantity, row_total, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
				''select old_item_id, 
					old_order_id, magento_order_id, 
					product_id, convert(old_product_id, unsigned int) old_product_id, productName, packsize, eye, 
					base_curve, diameter, power, cylinder, axis, `add`, dominant, color, 
					quantity, row_total
				from lenson_nl.migrate_orderlinedata'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




drop procedure migra_lensonnl.miglensonnl_lnd_merge_migrate_updates
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 14-02-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from My SQL Lensway NL Table (migrate_orderlinedata)
-- ==========================================================================================

create procedure migra_lensonnl.miglensonnl_lnd_merge_migrate_updates
 	@idETLBatchRun bigint, @idPackageRun bigint
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	merge into Landing.migra_lensonnl.migrate_orderdata trg
	using 
		(select mc.old_customer_id, mc.new_magento_id, c.entity_id, c.email
		from 
				Landing.migra_lensonnl.migrate_customerdata mc 
			left join
				Landing.mag.customer_entity_flat_aud c on mc.new_magento_id = c.entity_id) src on trg.old_customer_id = src.old_customer_id
	when matched and magento_customer_id = 0 then
		update set 
			trg.magento_customer_id = src.new_magento_id;


	merge into Landing.migra_lensonnl.migrate_orderlinedata trg
	using 
		(select product_id, min(magento_id) magento_id
		from Landing.csv.lensonnl_product_mapping
		group by product_id) src on trg.productID = src.product_id
	when matched and trg.product_id is null then
		update set
			trg.product_id = src.magento_id;

	update Landing.migra_lensonnl.migrate_orderlinedata
	set product_id = 3154
	where product_id is null


	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 




