
--- migrate_customerdata_v
select top 1000 db,
	old_customer_id, 
	new_magento_id, email, created_at, 
	domainID, domainName, 
	unsubscribe_newsletter, disable_saved_card, RAF_code
from Landing.migra.migrate_customerdata_v
-- where old_customer_id = 5971
where email = 'ashley.mealor@gmail.com'

	select count(*)
	from Landing.migra.migrate_customerdata_v

	select count(*)
	from Landing.migra.migrate_customerdata_f_v -- Customers with Orders

	select db, count(*), min(old_customer_id) mi, max(old_customer_id) ma
	from Landing.migra.migrate_customerdata_v
	-- where new_magento_id <> 0
	group by db
	order by db

	select db, count(*), min(old_customer_id) mi, max(old_customer_id) ma
	from Landing.migra.migrate_customerdata_f_v
	-- where new_magento_id <> 0
	group by db
	order by db

	select old_customer_id, count(*)
	from Landing.migra.migrate_customerdata_v
	group by old_customer_id
	having count(*) > 1
	order by old_customer_id

	select domainID, domainName, count(*)
	from Landing.migra.migrate_customerdata_v
	group by domainID, domainName
	order by domainID, domainName


	select email, count(*)
	from Landing.migra.migrate_customerdata_f_v
	group by email
	having count(*) > 1
	order by count(*) desc, email

--- migrate_orderdata_v
select top 1000 db,
	old_order_id, 
	magento_order_id, new_increment_id, 
	store_id, website_name, domainID, domainName, 
	email, old_customer_id, magento_customer_id, 
	created_at,
	migrated, orderStatusName, 
	shipping_description, channel, source_code, 
	billing_postcode, billing_country, shipping_postcode, shipping_country, 
	base_grand_total, base_shipping_amount, base_discount_amount, 
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat, 
	tax_rate, tax_free_amount, 
	order_currency, exchangeRate
from Landing.migra.migrate_orderdata_v
where db = 'lensonnl' -- vd150324 - lb150324 - lb150324 - VisioOptik - lensway - lenson - lensonnl - lenswaynl
-- where email = 'ashley.mealor@gmail.com'
-- where base_discount_amount <> 0

	select count(*)
	from Landing.migra.migrate_orderdata_v

	select db, count(*), min(old_order_id) mi, max(old_order_id) ma
	from Landing.migra.migrate_orderdata_v
	-- where magento_customer_id <> 0 -- magento_order_id - store_id - magento_customer_id
	group by db
	order by db

	select old_order_id, count(*)
	from Landing.migra.migrate_orderdata_v
	group by old_order_id
	having count(*) > 1
	order by old_order_id

	select store_id, count(*)
	from Landing.migra.migrate_orderdata_v
	group by store_id
	order by store_id

	select website_name, count(*)
	from Landing.migra.migrate_orderdata_v
	group by website_name
	order by website_name

	select domainID, domainName, count(*)
	from Landing.migra.migrate_orderdata_v
	group by domainID, domainName
	order by domainID, domainName

	select orderStatusName, count(*)
	from Landing.migra.migrate_orderdata_v
	group by orderStatusName
	order by orderStatusName

	select shipping_country, count(*)
	from Landing.migra.migrate_orderdata_v
	group by shipping_country
	order by shipping_country


	select shipping_description, count(*)
	from Landing.migra.migrate_orderdata_v
	group by shipping_description
	order by shipping_description

	select channel, count(*)
	from Landing.migra.migrate_orderdata_v
	group by channel
	order by channel

	select db, count(*)
	from Landing.migra.migrate_orderdata_v
	where base_discount_amount <> 0
	group by db
	order by db


	select order_currency, count(*)
	from Landing.migra.migrate_orderdata_v
	group by order_currency
	order by order_currency

	select db, count(*)
	from Landing.migra.migrate_orderdata_v
	where exchangeRate is not null
	group by db
	order by db


--- migrate_orderlinedata_v
select top 1000 db,
	old_item_id, 
	old_order_id, magento_order_id, 
	product_id, productID, product_id_def, productName, packsize, eye, 
	base_curve, diameter, power, cylinder, axis, [add], dominant, color, 
	quantity, row_total
from Landing.migra.migrate_orderlinedata_v
where db = 'lensonnl' -- vd150324 - lb150324 - lh150324 - VisioOptik - lensway - lenson - lensonnl - lenwaynl
-- where product_id_def = 10017

	select count(*)
	from Landing.migra.migrate_orderlinedata_v

	select db, count(*)
	from Landing.migra.migrate_orderlinedata_v
	-- where magento_order_id <> 0
	group by db
	order by db

	select old_order_id, count(*)
	from Landing.migra.migrate_orderdata_v
	group by old_order_id
	having count(*) > 1
	order by old_order_id

	select product_id, count(*)
	from Landing.migra.migrate_orderlinedata_v
	group by product_id
	order by product_id

	select product_id_def, count(*)
	from Landing.migra.migrate_orderlinedata_v
	group by product_id_def
	order by product_id_def

	select product_id, productName, count(*)
	from Landing.migra.migrate_orderlinedata_v
	group by product_id, productName
	order by product_id, productName

--- migrate_order_id_mapping_v
select top 1000 db,
	internal_order_id, user_visible_order_id, 
	has_lenses, has_solutions
from Landing.migra.migrate_order_id_mapping_v

	select db, count(*)
	from Landing.migra.migrate_order_id_mapping_v
	group by db
	order by db

	select user_visible_order_id, count(*)
	from Landing.migra.migrate_order_id_mapping_v
	group by user_visible_order_id
	having count(*) > 1
	order by user_visible_order_id