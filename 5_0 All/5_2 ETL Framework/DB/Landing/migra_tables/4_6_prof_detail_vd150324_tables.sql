use Landing
go 

------------------------------------------------------------------------
----------------------- migrate_customerdata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_vd150324.migrate_customerdata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_customer_id) num_dist_rows
	from Landing.migra_vd150324.migrate_customerdata


------------------------------------------------------------------------
----------------------- migrate_orderdata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_vd150324.migrate_orderdata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_order_id) num_dist_rows
	from Landing.migra_vd150324.migrate_orderdata


------------------------------------------------------------------------
----------------------- migrate_orderlinedata ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_vd150324.migrate_orderlinedata
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_item_id) num_dist_rows
	from Landing.migra_vd150324.migrate_orderlinedata


------------------------------------------------------------------------
----------------------- migrate_order_id_mapping ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows
	from Landing.migra_vd150324.migrate_order_id_mapping
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct internal_order_id) num_dist_rows
	from Landing.migra_vd150324.migrate_order_id_mapping

