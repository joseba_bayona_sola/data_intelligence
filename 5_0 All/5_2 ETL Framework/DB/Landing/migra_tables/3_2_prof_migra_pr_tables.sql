
-- migra_pr.dw_stores_full
select store_id, store_name, website_group, store_type, visible, 
	idETLBatchRun, ins_ts
from Landing.migra_pr.dw_stores_full
order by website_group, store_id



-- migra_pr.order_headers
select order_id, order_no, order_date, 
	document_type, status, 
	customer_id, customer_email, 
	shipping_carrier, shipping_method, 
	idETLBatchRun, ins_ts
from Landing.migra_pr.order_headers

-- migra_pr.order_lines
select line_id, order_id, order_no, document_date, 
	document_type, 
	product_id, sku, name, qty, 
	idETLBatchRun, ins_ts
from Landing.migra_pr.order_lines
