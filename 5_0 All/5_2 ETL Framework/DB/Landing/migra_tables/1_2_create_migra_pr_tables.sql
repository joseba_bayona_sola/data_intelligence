use Landing
go 

--------------------- Tables ----------------------------------
-- Landing.migra_pr.dw_stores_full
create table migra_pr.dw_stores_full(
	store_id		int NOT NULL,
	store_name		varchar(100) NULL,
	website_group	varchar(100) NULL,
	store_type		varchar(100) NULL,
	visible			int NULL,
	idETLBatchRun	bigint NOT NULL, 
	ins_ts			datetime NOT NULL);
go

alter table migra_pr.dw_stores_full add constraint [PK_migra_pr_dw_stores_full]
	primary key clustered (store_id);
go

alter table migra_pr.dw_stores_full add constraint [DF_migra_pr_dw_stores_full_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------- Tables ----------------------------------
-- Landing.migra_pr.order_headers
create table migra_pr.order_headers(
	order_id			numeric(10, 0) NOT NULL,
	order_no			varchar(50) NULL,
	order_date			datetime2 NULL,
	store_name			varchar(100) NULL,
	document_type		varchar(50) NOT NULL,
	status				varchar(32) NULL,
	customer_id			numeric(10, 0) NULL,
	customer_email		varchar(255) NULL,
	shipping_carrier	varchar(255) NULL,
	shipping_method		varchar(255) NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go

alter table migra_pr.order_headers add constraint [PK_migra_pr_order_headers]
	primary key clustered (order_id);
go

alter table migra_pr.order_headers add constraint [DF_migra_pr_order_headers_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


--------------------- Tables ----------------------------------
-- Landing.migra_pr.order_lines
create table migra_pr.order_lines(
	line_id				int NOT NULL,
	order_id			int NULL,
	order_no			varchar(50) NULL,
	document_date		datetime2 NULL,
	store_name			varchar(100) NULL,
	document_type		varchar(50) NOT NULL,
	product_id			int NULL,
	sku					varchar(255) NULL,
	name				varchar(255) NULL,
	qty					numeric(13, 4) NULL,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL);
go


alter table migra_pr.order_lines add constraint [PK_migra_pr_order_lines]
	primary key clustered (line_id);
go

alter table migra_pr.order_lines add constraint [DF_migra_pr_order_lines_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
