use Landing
go 


-- Landing.migra_lensonnl.migrate_customerdata
create table migra_lensonnl.migrate_customerdata(
	old_customer_id			bigint NOT NULL, 
	new_magento_id			int, 
	email					varchar(50), 
	created_at				datetime, 
	domainID				varchar(10), 
	domainName				varchar(40),
	unsubscribe_newsletter	int, 
	disable_saved_card		int, 
	RAF_code				varchar(255), 
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go 

alter table migra_lensonnl.migrate_customerdata add constraint [PK_migra_lensonnl_migrate_customerdata]
	primary key clustered (old_customer_id);
go

alter table migra_lensonnl.migrate_customerdata add constraint [DF_migra_lensonnl_migrate_customerdata_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 




-- Landing.migra_lensonnl.migrate_orderdata
create table migra_lensonnl.migrate_orderdata(
	old_order_id					bigint NOT NULL, 
	magento_order_id				int NOT NULL, 
	new_increment_id				varchar(255), 
	store_id						int NOT NULL, 
	website_name					varchar(20), 
	domainID						varchar(10), 
	domainName						varchar(40), 
	email							varchar(255), 
	old_customer_id					bigint, 
	magento_customer_id				int NOT NULL, 
	created_at						datetime, 
	migrated						int NOT NULL, 
	orderStatusName					varchar(13), 
	shipping_description			varchar(255), 
	channel							varchar(64), 
	source_code						varchar(64), 
	billing_postcode				varchar(255), 
	billing_country					varchar(255), 
	shipping_postcode				varchar(255), 
	shipping_country				varchar(255), 
	base_grand_total				decimal(12, 4), 
	base_shipping_amount			decimal(12, 4), 
	base_discount_amount			decimal(12, 4), 
	base_grand_total_exc_vat		decimal(12, 4), 
	base_shipping_amount_exc_vat	decimal(12, 4), 
	base_discount_amount_exc_vat	decimal(12, 4), 
	tax_rate						decimal(12, 4), 
	tax_free_amount					decimal(12, 4), 
	order_currency					varchar(3), 
	exchangeRate					decimal(12, 4),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL)
go 

alter table migra_lensonnl.migrate_orderdata add constraint [PK_migra_lensonnl_migrate_orderdata]
	primary key clustered (old_order_id);
go

alter table migra_lensonnl.migrate_orderdata add constraint [DF_migra_lensonnl_migrate_orderdata_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.migra_lensonnl.migrate_orderlinedata
create table migra_lensonnl.migrate_orderlinedata(
	old_item_id						bigint NOT NULL, 
	old_order_id					bigint NOT NULL, 
	magento_order_id				int, 
	product_id						int, 
	productID						int, 
	productName						varchar(255), 
	packsize						int, 
	eye								varchar(10), 
	base_curve						varchar(20), 
	diameter						varchar(20), 
	power							varchar(20), 
	cylinder						varchar(20), 
	axis							varchar(20), 
	[add]							varchar(20), 
	dominant						varchar(20), 
	color							varchar(20), 
	quantity						int, 
	row_total						decimal(12, 4),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL)
go 

alter table migra_lensonnl.migrate_orderlinedata add constraint [PK_migra_lensonnl_migrate_orderlinedata]
	primary key clustered (old_item_id);
go

alter table migra_lensonnl.migrate_orderlinedata add constraint [DF_migra_lensonnl_migrate_orderlinedata_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

create index [INX_migra_lensonnl_migrate_orderlinedata_old_order_id] on migra_lensonnl.migrate_orderlinedata(old_order_id) 
go
