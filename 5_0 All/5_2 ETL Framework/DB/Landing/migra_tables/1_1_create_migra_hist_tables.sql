use Landing
go 


-- Landing.migra_hist.dw_hist_order
create table migra_hist.dw_hist_order(
	order_id			int NOT NULL, 
	source				varchar(13) NOT NULL, 
	store_id			int, 
	customer_id			int, 
	created_at			datetime2, 
	grand_total			decimal(12, 2),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL)
go 

alter table migra_hist.dw_hist_order add constraint [DF_migra_hist_dw_hist_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 


-- Landing.migra_hist.dw_hist_order_item
create table migra_hist.dw_hist_order_item(
	order_id			int NOT NULL, 
	source				varchar(13) NOT NULL, 
	product_id			int, 
	sku					varchar(255), 
	qty_ordered			decimal(14, 4), 
	row_total			decimal(12, 4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL)
go 

alter table migra_hist.dw_hist_order_item add constraint [DF_migra_hist_dw_hist_order_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 



-- Landing.migra_hist.dw_hist_shipment
create table migra_hist.dw_hist_shipment(
	shipment_id			int NOT NULL, 
	source				varchar(13) NOT NULL, 
	store_id			int, 
	customer_id			int, 
	created_at			datetime2,
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL)
go 

alter table migra_hist.dw_hist_shipment add constraint [DF_migra_hist_dw_hist_shipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

-- Landing.migra_hist.dw_hist_shipment_item
create table migra_hist.dw_hist_shipment_item(
	shipment_id			int NOT NULL, 
	source				varchar(13) NOT NULL, 
	product_id			int, 
	sku					varchar(255), 
	qty					decimal(14, 4),
	idETLBatchRun		bigint NOT NULL, 
	ins_ts				datetime NOT NULL)
go 

alter table migra_hist.dw_hist_shipment_item add constraint [DF_migra_hist_dw_hist_shipment_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 
