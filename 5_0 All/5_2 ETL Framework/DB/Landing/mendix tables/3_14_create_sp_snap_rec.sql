
------------------------------ SP -----------------------------------

use Landing
go

------------------- snapreceipts$snapreceipt -------------------

drop procedure mend.srcmend_lnd_get_rec_snapreceipt
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - rec_snapreceipt
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_rec_snapreceipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				receiptid, 
				lines, lineqty, 	
				receiptStatus, receiptprocessStatus, processDetail,  priority, 
				status, stockstatus, 
				ordertype, orderclass, 
				suppliername, consignmentID, 
				weight, actualWeight, volume, 
				dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
				datesfixed, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					receiptid, 
					lines, lineqty, 	
					receiptStatus, receiptprocessStatus, processDetail,  priority, 
					status, stockstatus, 
					ordertype, orderclass, 
					suppliername, consignmentID, 
					weight, actualWeight, volume, 
					dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
					datesfixed 
				from public."snapreceipts$snapreceipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ snapreceipts$snapreceipt_shipment ------------

drop procedure mend.srcmend_lnd_get_rec_snapreceipt_receipt
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - rec_snapreceipt_shipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_rec_snapreceipt_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select snapreceipts$snapreceiptid snapreceiptid, warehouseshipments$receiptid receiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "snapreceipts$snapreceiptid", "warehouseshipments$receiptid" 
					from public."snapreceipts$snapreceipt_receipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------ snapreceipts$snapreceiptline ------------

drop procedure mend.srcmend_lnd_get_rec_snapreceiptline
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - rec_snapreceiptline
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_rec_snapreceiptline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				line, skuid, 
				status, level, unitofmeasure, 
				qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					line, skuid, 
					status, level, unitofmeasure, 
					qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected 
				from public."snapreceipts$snapreceiptline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ snapreceipts$snapreceiptline_snapreceipt ------------

drop procedure mend.srcmend_lnd_get_rec_snapreceiptline_snapreceipt
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - rec_snapreceiptline_snapreceipt
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_rec_snapreceiptline_snapreceipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select snapreceipts$snapreceiptlineid snapreceiptlineid, snapreceipts$snapreceiptid snapreceiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "snapreceipts$snapreceiptlineid", "snapreceipts$snapreceiptid" 
					from public."snapreceipts$snapreceiptline_snapreceipt"'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ snapreceipts$snapreceiptline_stockitem ------------

drop procedure mend.srcmend_lnd_get_rec_snapreceiptline_stockitem
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - rec_snapreceiptline_stockitem
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_rec_snapreceiptline_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select snapreceipts$snapreceiptlineid snapreceiptlineid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "snapreceipts$snapreceiptlineid", "product$stockitemid" 
					from public."snapreceipts$snapreceiptline_stockitem"'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go