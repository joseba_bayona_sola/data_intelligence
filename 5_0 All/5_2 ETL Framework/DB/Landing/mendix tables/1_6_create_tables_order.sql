
---------- Script creacion de tablas ----------

use Landing
go

------------ orderprocessing$order ------------

create table mend.order_order (
	id								bigint NOT NULL, 
	incrementID						varchar(30),
	magentoOrderID					varchar(200),
	orderStatus						varchar(200), 
	orderLinesMagento				int,
	orderLinesDeduped				int,
	_type							varchar(200),
	shipmentStatus					varchar(200),
	statusUpdated					bit,
	allocationStatus				varchar(200),
	allocationStrategy				varchar(19),
	allocatedDate					datetime2,
	promisedShippingDate			datetime2,
	promisedDeliveryDate			datetime2,
	labelRenderer					varchar(6),  
	couponCode						varchar(255),
	shippingMethod					varchar(255),
	shippingDescription				varchar(255),
	paymentMethod					varchar(200),
	automatic_reorder				bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_order add constraint [PK_mend_order_order]
	primary key clustered (id);
go
alter table mend.order_order add constraint [DF_mend_order_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_aud(
	id								bigint NOT NULL, 
	incrementID						varchar(30),
	magentoOrderID					varchar(200),
	orderStatus						varchar(200), 
	orderLinesMagento				int,
	orderLinesDeduped				int,
	_type							varchar(200),
	shipmentStatus					varchar(200),
	statusUpdated					bit,
	allocationStatus				varchar(200),
	allocationStrategy				varchar(19),
	allocatedDate					datetime2,
	promisedShippingDate			datetime2,
	promisedDeliveryDate			datetime2,
	labelRenderer					varchar(6),  
	couponCode						varchar(255),
	shippingMethod					varchar(255),
	shippingDescription				varchar(255),
	paymentMethod					varchar(200),
	automatic_reorder				bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_order_aud add constraint [PK_mend_order_order_aud]
	primary key clustered (id);
go
alter table mend.order_order_aud add constraint [DF_mend_order_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_aud_hist(
	id								bigint NOT NULL, 
	incrementID						varchar(30),
	magentoOrderID					varchar(200),
	orderStatus						varchar(200), 
	orderLinesMagento				int,
	orderLinesDeduped				int,
	_type							varchar(200),
	shipmentStatus					varchar(200),
	statusUpdated					bit,
	allocationStatus				varchar(200),
	allocationStrategy				varchar(19),
	allocatedDate					datetime2,
	promisedShippingDate			datetime2,
	promisedDeliveryDate			datetime2,
	labelRenderer					varchar(6),  
	couponCode						varchar(255),
	shippingMethod					varchar(255),
	shippingDescription				varchar(255),
	paymentMethod					varchar(200),
	automatic_reorder				bit,
	createdDate						datetime2,
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_order_aud_hist add constraint [DF_mend.order_order_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ orderprocessing$order_customer ------------

create table mend.order_order_customer (
	orderid							bigint NOT NULL,
	retailcustomerid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_order_customer add constraint [PK_mend_order_order_customer]
	primary key clustered (orderid, retailcustomerid);
go
alter table mend.order_order_customer add constraint [DF_mend_order_order_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_customer_aud(
	orderid							bigint NOT NULL,
	retailcustomerid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_order_customer_aud add constraint [PK_mend_order_order_customer_aud]
	primary key clustered (orderid, retailcustomerid);
go
alter table mend.order_order_customer_aud add constraint [DF_mend_order_order_customer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ orderprocessing$order_magwebstore ------------

create table mend.order_order_magwebstore (
	orderid							bigint NOT NULL,
	magwebstoreid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_order_magwebstore add constraint [PK_mend_order_order_magwebstore]
	primary key clustered (orderid, magwebstoreid);
go
alter table mend.order_order_magwebstore add constraint [DF_mend_order_order_magwebstore_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_magwebstore_aud(
	orderid							bigint NOT NULL,
	magwebstoreid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_order_magwebstore_aud add constraint [PK_mend_order_order_magwebstore_aud]
	primary key clustered (orderid, magwebstoreid);
go
alter table mend.order_order_magwebstore_aud add constraint [DF_mend_order_order_magwebstore_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ orderprocessing$basevalues ------------

create table mend.order_basevalues (
	id								bigint NOT NULL, 
	subtotal						decimal(28,8),
	subtotalInclTax					decimal(28,8),
	subtotalInvoiced				decimal(28,8),
	subtotalCanceled				decimal(28,8),
	subtotalRefunded				decimal(28,8), 
	discountAmount					decimal(28,8),
	discountInvoiced				decimal(28,8),
	discountCanceled				decimal(28,8),
	discountRefunded				decimal(28,8),
	shippingAmount					decimal(28,8),
	shippingInvoiced				decimal(28,8),
	shippingCanceled				decimal(28,8),
	shippingRefunded				decimal(28,8), 					 
	adjustmentNegative				decimal(28,8),
	adjustmentPositive				decimal(28,8),
	custBalanceAmount				decimal(28,8),
	customerBalanceAmount			decimal(28,8),
	customerBalanceInvoiced			decimal(28,8),
	customerBalanceRefunded			decimal(28,8),
	customerBalanceTotRefunded		decimal(28,8),
	customerBalanceTotalRefunded	decimal(28,8),
	grandTotal						decimal(28,8),
	totalInvoiced					decimal(28,8),
	totalCanceled					decimal(28,8),
	totalRefunded					decimal(28,8),
	toOrderRate						decimal(28,8),
	toGlobalRate					decimal(28,8),
	currencyCode					varchar(10),
	mutualamount					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_basevalues add constraint [PK_mend_order_basevalues]
	primary key clustered (id);
go
alter table mend.order_basevalues add constraint [DF_mend_order_basevalues_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_basevalues_aud(
	id								bigint NOT NULL, 
	subtotal						decimal(28,8),
	subtotalInclTax					decimal(28,8),
	subtotalInvoiced				decimal(28,8),
	subtotalCanceled				decimal(28,8),
	subtotalRefunded				decimal(28,8), 
	discountAmount					decimal(28,8),
	discountInvoiced				decimal(28,8),
	discountCanceled				decimal(28,8),
	discountRefunded				decimal(28,8),
	shippingAmount					decimal(28,8),
	shippingInvoiced				decimal(28,8),
	shippingCanceled				decimal(28,8),
	shippingRefunded				decimal(28,8), 					 
	adjustmentNegative				decimal(28,8),
	adjustmentPositive				decimal(28,8),
	custBalanceAmount				decimal(28,8),
	customerBalanceAmount			decimal(28,8),
	customerBalanceInvoiced			decimal(28,8),
	customerBalanceRefunded			decimal(28,8),
	customerBalanceTotRefunded		decimal(28,8),
	customerBalanceTotalRefunded	decimal(28,8),
	grandTotal						decimal(28,8),
	totalInvoiced					decimal(28,8),
	totalCanceled					decimal(28,8),
	totalRefunded					decimal(28,8),
	toOrderRate						decimal(28,8),
	toGlobalRate					decimal(28,8),
	currencyCode					varchar(10),
	mutualamount					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_basevalues_aud add constraint [PK_mend_order_basevalues_aud]
	primary key clustered (id);
go
alter table mend.order_basevalues_aud add constraint [DF_mend_order_basevalues_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_basevalues_aud_hist(
	id								bigint NOT NULL, 
	subtotal						decimal(28,8),
	subtotalInclTax					decimal(28,8),
	subtotalInvoiced				decimal(28,8),
	subtotalCanceled				decimal(28,8),
	subtotalRefunded				decimal(28,8), 
	discountAmount					decimal(28,8),
	discountInvoiced				decimal(28,8),
	discountCanceled				decimal(28,8),
	discountRefunded				decimal(28,8),
	shippingAmount					decimal(28,8),
	shippingInvoiced				decimal(28,8),
	shippingCanceled				decimal(28,8),
	shippingRefunded				decimal(28,8), 					 
	adjustmentNegative				decimal(28,8),
	adjustmentPositive				decimal(28,8),
	custBalanceAmount				decimal(28,8),
	customerBalanceAmount			decimal(28,8),
	customerBalanceInvoiced			decimal(28,8),
	customerBalanceRefunded			decimal(28,8),
	customerBalanceTotRefunded		decimal(28,8),
	customerBalanceTotalRefunded	decimal(28,8),
	grandTotal						decimal(28,8),
	totalInvoiced					decimal(28,8),
	totalCanceled					decimal(28,8),
	totalRefunded					decimal(28,8),
	toOrderRate						decimal(28,8),
	toGlobalRate					decimal(28,8),
	currencyCode					varchar(10),
	mutualamount					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_basevalues_aud_hist add constraint [DF_mend.order_basevalues_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$basevalues_order ------------

create table mend.order_basevalues_order (
	basevaluesid					bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_basevalues_order add constraint [PK_mend_order_basevalues_order]
	primary key clustered (basevaluesid, orderid);
go
alter table mend.order_basevalues_order add constraint [DF_mend_order_basevalues_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_basevalues_order_aud(
	basevaluesid					bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_basevalues_order_aud add constraint [PK_mend_order_basevalues_order_aud]
	primary key clustered (basevaluesid, orderid);
go
alter table mend.order_basevalues_order_aud add constraint [DF_mend_order_basevalues_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go








------------ orderprocessing$order_statusupdate ------------

create table mend.order_order_statusupdate (
	id								bigint NOT NULL,
	status							varchar(22),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_order_statusupdate add constraint [PK_mend_order_order_statusupdate]
	primary key clustered (id);
go
alter table mend.order_order_statusupdate add constraint [DF_mend_order_order_statusupdate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_statusupdate_aud(
	id								bigint NOT NULL,
	status							varchar(22),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_order_statusupdate_aud add constraint [PK_mend_order_order_statusupdate_aud]
	primary key clustered (id);
go
alter table mend.order_order_statusupdate_aud add constraint [DF_mend_order_order_statusupdate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_order_statusupdate_aud_hist(
	id								bigint NOT NULL,
	status							varchar(22),
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_order_statusupdate_aud_hist add constraint [DF_mend.order_order_statusupdate_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$statusupdate_order ------------

create table mend.order_statusupdate_order (
	order_statusupdateid			bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_statusupdate_order add constraint [PK_mend_order_statusupdate_order]
	primary key clustered (order_statusupdateid, orderid);
go
alter table mend.order_statusupdate_order add constraint [DF_mend_order_statusupdate_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_statusupdate_order_aud(
	order_statusupdateid			bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_statusupdate_order_aud add constraint [PK_mend_order_statusupdate_order_aud]
	primary key clustered (order_statusupdateid, orderid);
go
alter table mend.order_statusupdate_order_aud add constraint [DF_mend_order_statusupdate_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ orderprocessing$orderprocessingtransaction ------------

create table mend.order_orderprocessingtransaction (
	id								bigint NOT NULL,
	orderReference					varchar(200),	
	status							varchar(200),
	detail							varchar(255), 
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderprocessingtransaction add constraint [PK_mend_order_orderprocessingtransaction]
	primary key clustered (id);
go
alter table mend.order_orderprocessingtransaction add constraint [DF_mend_order_orderprocessingtransaction_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderprocessingtransaction_aud(
	id								bigint NOT NULL,
	orderReference					varchar(200),	
	status							varchar(200),
	detail							varchar(255), 
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderprocessingtransaction_aud add constraint [PK_mend_order_orderprocessingtransaction_aud]
	primary key clustered (id);
go
alter table mend.order_orderprocessingtransaction_aud add constraint [DF_mend_order_orderprocessingtransaction_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderprocessingtransaction_aud_hist(
	id								bigint NOT NULL,
	orderReference					varchar(200),	
	status							varchar(200),
	detail							varchar(255), 
	timestamp						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_orderprocessingtransaction_aud_hist add constraint [DF_mend.order_orderprocessingtransaction_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$orderprocessingtransaction_order ------------

create table mend.order_orderprocessingtransaction_order (
	orderprocessingtransactionid	bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderprocessingtransaction_order add constraint [PK_mend_order_orderprocessingtransaction_order]
	primary key clustered (orderprocessingtransactionid, orderid);
go
alter table mend.order_orderprocessingtransaction_order add constraint [DF_mend_order_orderprocessingtransaction_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderprocessingtransaction_order_aud(
	orderprocessingtransactionid	bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderprocessingtransaction_order_aud add constraint [PK_mend_order_orderprocessingtransaction_order_aud]
	primary key clustered (orderprocessingtransactionid, orderid);
go
alter table mend.order_orderprocessingtransaction_order_aud add constraint [DF_mend_order_orderprocessingtransaction_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------ orderprocessing$orderlineabstract ------------

create table mend.order_orderlineabstract (
	id								bigint NOT NULL, 
	magentoItemID					bigint, 
	quantityOrdered					decimal(28,8),
	quantityInvoiced				decimal(28,8),
	quantityRefunded				decimal(28,8),
	quantityIssued					decimal(28,8),
	quantityAllocated				decimal(28,8),
	quantityShipped					decimal(28,8), 
	basePrice						decimal(28,8),
	baseRowPrice					decimal(28,8), 
	allocated						bit, 
	orderLineType					varchar(10),
	subMetaObjectName				varchar(255),
	sku								varchar(200),
	eye								varchar(200),
	lensGroupID						varchar(200),	 
	netPrice						decimal(28,8), 
	netRowPrice						decimal(28,8),
	createdDate						datetime2,
	changedDate						datetime2, 
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderlineabstract add constraint [PK_mend_order_orderlineabstract]
	primary key clustered (id);
go
alter table mend.order_orderlineabstract add constraint [DF_mend_order_orderlineabstract_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlineabstract_aud(
	id								bigint NOT NULL, 
	magentoItemID					bigint, 
	quantityOrdered					decimal(28,8),
	quantityInvoiced				decimal(28,8),
	quantityRefunded				decimal(28,8),
	quantityIssued					decimal(28,8),
	quantityAllocated				decimal(28,8),
	quantityShipped					decimal(28,8), 
	basePrice						decimal(28,8),
	baseRowPrice					decimal(28,8), 
	allocated						bit, 
	orderLineType					varchar(10),
	subMetaObjectName				varchar(255),
	sku								varchar(200),
	eye								varchar(200),
	lensGroupID						varchar(200),	 
	netPrice						decimal(28,8), 
	netRowPrice						decimal(28,8),
	createdDate						datetime2,
	changedDate						datetime2, 
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlineabstract_aud add constraint [PK_mend_order_orderlineabstract_aud]
	primary key clustered (id);
go
alter table mend.order_orderlineabstract_aud add constraint [DF_mend_order_orderlineabstract_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlineabstract_aud_hist(
	id								bigint NOT NULL, 
	magentoItemID					bigint, 
	quantityOrdered					decimal(28,8),
	quantityInvoiced				decimal(28,8),
	quantityRefunded				decimal(28,8),
	quantityIssued					decimal(28,8),
	quantityAllocated				decimal(28,8),
	quantityShipped					decimal(28,8), 
	basePrice						decimal(28,8),
	baseRowPrice					decimal(28,8), 
	allocated						bit, 
	orderLineType					varchar(10),
	subMetaObjectName				varchar(255),
	sku								varchar(200),
	eye								varchar(200),
	lensGroupID						varchar(200),	 
	netPrice						decimal(28,8), 
	netRowPrice						decimal(28,8),
	createdDate						datetime2,
	changedDate						datetime2, 
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_orderlineabstract_aud_hist add constraint [DF_mend.order_orderlineabstract_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlineabstract_ol_aud(
	id								bigint NOT NULL, 
	magentoItemID					bigint, 
	quantityOrdered					decimal(28,8),
	quantityInvoiced				decimal(28,8),
	quantityRefunded				decimal(28,8),
	quantityIssued					decimal(28,8),
	quantityAllocated				decimal(28,8),
	quantityShipped					decimal(28,8), 
	basePrice						decimal(28,8),
	baseRowPrice					decimal(28,8), 
	allocated						bit, 
	orderLineType					varchar(10),
	subMetaObjectName				varchar(255),
	sku								varchar(200),
	eye								varchar(200),
	lensGroupID						varchar(200),	 
	netPrice						decimal(28,8), 
	netRowPrice						decimal(28,8),
	createdDate						datetime2,
	changedDate						datetime2, 
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlineabstract_ol_aud add constraint [PK_mend_order_orderlineabstract_ol_aud]
	primary key clustered (id);
go
alter table mend.order_orderlineabstract_ol_aud add constraint [DF_mend_order_orderlineabstract_ol_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




create table mend.order_orderlineabstract_olm_aud(
	id								bigint NOT NULL, 
	magentoItemID					bigint, 
	quantityOrdered					decimal(28,8),
	quantityInvoiced				decimal(28,8),
	quantityRefunded				decimal(28,8),
	quantityIssued					decimal(28,8),
	quantityAllocated				decimal(28,8),
	quantityShipped					decimal(28,8), 
	basePrice						decimal(28,8),
	baseRowPrice					decimal(28,8), 
	allocated						bit, 
	orderLineType					varchar(10),
	subMetaObjectName				varchar(255),
	sku								varchar(200),
	eye								varchar(200),
	lensGroupID						varchar(200),	 
	netPrice						decimal(28,8), 
	netRowPrice						decimal(28,8),
	createdDate						datetime2,
	changedDate						datetime2, 
	system$owner					bigint,
	system$changedBy				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlineabstract_olm_aud add constraint [PK_mend_order_orderlineabstract_olm_aud]
	primary key clustered (id);
go
alter table mend.order_orderlineabstract_olm_aud add constraint [DF_mend_order_orderlineabstract_olm_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------ orderprocessing$orderlinemagento ------------

create table mend.order_orderlinemagento (
	id								bigint NOT NULL,
	lineAdded						varchar(3),
	deduplicate						bit,
	fulfilled						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderlinemagento add constraint [PK_mend_order_orderlinemagento]
	primary key clustered (id);
go
alter table mend.order_orderlinemagento add constraint [DF_mend_order_orderlinemagento_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlinemagento_aud(
	id								bigint NOT NULL,
	lineAdded						varchar(3),
	deduplicate						bit,
	fulfilled						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlinemagento_aud add constraint [PK_mend_order_orderlinemagento_aud]
	primary key clustered (id);
go
alter table mend.order_orderlinemagento_aud add constraint [DF_mend_order_orderlinemagento_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlinemagento_aud_hist(
	id								bigint NOT NULL,
	lineAdded						varchar(3),
	deduplicate						bit,
	fulfilled						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_orderlinemagento_aud_hist add constraint [DF_mend.order_orderlinemagento_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$orderlinemagento_order ------------

create table mend.order_orderlinemagento_order (
	orderlinemagentoid				bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderlinemagento_order add constraint [PK_mend_order_orderlinemagento_order]
	primary key clustered (orderlinemagentoid, orderid);
go
alter table mend.order_orderlinemagento_order add constraint [DF_mend_order_orderlinemagento_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlinemagento_order_aud(
	orderlinemagentoid				bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlinemagento_order_aud add constraint [PK_mend_order_orderlinemagento_order_aud]
	primary key clustered (orderlinemagentoid, orderid);
go
alter table mend.order_orderlinemagento_order_aud add constraint [DF_mend_order_orderlinemagento_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ orderprocessing$orderline ------------

create table mend.order_orderline (
	id								bigint NOT NULL,
	status							varchar(5),
	packsOrdered					decimal(28,8),
	packsAllocated					decimal(28,8),
	packsShipped					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderline add constraint [PK_mend_order_orderline]
	primary key clustered (id);
go
alter table mend.order_orderline add constraint [DF_mend_order_orderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderline_aud(
	id								bigint NOT NULL,
	status							varchar(5),
	packsOrdered					decimal(28,8),
	packsAllocated					decimal(28,8),
	packsShipped					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderline_aud add constraint [PK_mend_order_orderline_aud]
	primary key clustered (id);
go
alter table mend.order_orderline_aud add constraint [DF_mend_order_orderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderline_aud_hist(
	id								bigint NOT NULL,
	status							varchar(5),
	packsOrdered					decimal(28,8),
	packsAllocated					decimal(28,8),
	packsShipped					decimal(28,8),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_orderline_aud_hist add constraint [DF_mend.order_orderline_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$orderline_order ------------

create table mend.order_orderline_order (
	orderlineid						bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderline_order add constraint [PK_mend_order_orderline_order]
	primary key clustered (orderlineid, orderid);
go
alter table mend.order_orderline_order add constraint [DF_mend_order_orderline_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderline_order_aud(
	orderlineid						bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderline_order_aud add constraint [PK_mend_order_orderline_order_aud]
	primary key clustered (orderlineid, orderid);
go
alter table mend.order_orderline_order_aud add constraint [DF_mend_order_orderline_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------ orderprocessing$shippinggroup ------------

create table mend.order_shippinggroup (
	id								bigint NOT NULL, 
	dispensingFee					bit, 
	allocationStatus				varchar(7),
	shipmentStatus					varchar(7), 
	wholesale						bit, 
	letterBoxAble					bit,
	onHold							bit,
	adHoc							bit,
	cancelled						bit, 
	createdDate						datetime2,
	changedDate						datetime2, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_shippinggroup add constraint [PK_mend_order_shippinggroup]
	primary key clustered (id);
go
alter table mend.order_shippinggroup add constraint [DF_mend_order_shippinggroup_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_shippinggroup_aud(
	id								bigint NOT NULL, 
	dispensingFee					bit, 
	allocationStatus				varchar(7),
	shipmentStatus					varchar(7), 
	wholesale						bit, 
	letterBoxAble					bit,
	onHold							bit,
	adHoc							bit,
	cancelled						bit, 
	createdDate						datetime2,
	changedDate						datetime2, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_shippinggroup_aud add constraint [PK_mend_order_shippinggroup_aud]
	primary key clustered (id);
go
alter table mend.order_shippinggroup_aud add constraint [DF_mend_order_shippinggroup_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_shippinggroup_aud_hist(
	id								bigint NOT NULL, 
	dispensingFee					bit, 
	allocationStatus				varchar(7),
	shipmentStatus					varchar(7), 
	wholesale						bit, 
	letterBoxAble					bit,
	onHold							bit,
	adHoc							bit,
	cancelled						bit, 
	createdDate						datetime2,
	changedDate						datetime2, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.order_shippinggroup_aud_hist add constraint [DF_mend.order_shippinggroup_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$shippinggroup_order ------------

create table mend.order_shippinggroup_order (
	shippinggroupid					bigint NOT NULL, 
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_shippinggroup_order add constraint [PK_mend_order_shippinggroup_order]
	primary key clustered (shippinggroupid, orderid);
go
alter table mend.order_shippinggroup_order add constraint [DF_mend_order_shippinggroup_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_shippinggroup_order_aud(
	shippinggroupid					bigint NOT NULL, 
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_shippinggroup_order_aud add constraint [PK_mend_order_shippinggroup_order_aud]
	primary key clustered (shippinggroupid, orderid);
go
alter table mend.order_shippinggroup_order_aud add constraint [DF_mend_order_shippinggroup_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$orderlineabstract_shippinggroup ------------

create table mend.order_orderlineabstract_shippinggroup (
	orderlineabstractid				bigint NOT NULL, 
	shippinggroupid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderlineabstract_shippinggroup add constraint [PK_mend_order_orderlineabstract_shippinggroup]
	primary key clustered (orderlineabstractid, shippinggroupid);
go
alter table mend.order_orderlineabstract_shippinggroup add constraint [DF_mend_order_orderlineabstract_shippinggroup_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderlineabstract_shippinggroup_aud(
	orderlineabstractid				bigint NOT NULL, 
	shippinggroupid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderlineabstract_shippinggroup_aud add constraint [PK_mend_order_orderlineabstract_shippinggroup_aud]
	primary key clustered (orderlineabstractid, shippinggroupid);
go
alter table mend.order_orderlineabstract_shippinggroup_aud add constraint [DF_mend_order_orderlineabstract_shippinggroup_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------ orderprocessing$orderline_product ------------

create table mend.order_orderline_product(
	orderlineabstractid				bigint NOT NULL, 
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderline_product add constraint [PK_mend_order_orderline_product]
	primary key clustered (orderlineabstractid, productid);
go
alter table mend.order_orderline_product add constraint [DF_mend_order_orderline_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderline_product_aud(
	orderlineabstractid				bigint NOT NULL, 
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderline_product_aud add constraint [PK_mend_order_orderline_product_aud]
	primary key clustered (orderlineabstractid, productid);
go
alter table mend.order_orderline_product_aud add constraint [DF_mend_order_orderline_product_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ orderprocessing$orderline_requiredstockitem ------------

create table mend.order_orderline_requiredstockitem(
	orderlineabstractid				bigint NOT NULL, 
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderline_requiredstockitem add constraint [PK_mend_order_orderline_requiredstockitem]
	primary key clustered (orderlineabstractid, stockitemid);
go
alter table mend.order_orderline_requiredstockitem add constraint [DF_mend_order_orderline_requiredstockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderline_requiredstockitem_aud(
	orderlineabstractid				bigint NOT NULL, 
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderline_requiredstockitem_aud add constraint [PK_mend_order_orderline_requiredstockitem_aud]
	primary key clustered (orderlineabstractid, stockitemid);
go
alter table mend.order_orderline_requiredstockitem_aud add constraint [DF_mend_order_orderline_requiredstockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ orderprocessing$address_order ------------

create table mend.order_address_order (
	orderaddressid					bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_address_order add constraint [PK_mend_order_address_order]
	primary key clustered (orderaddressid, orderid);
go
alter table mend.order_address_order add constraint [DF_mend_order_address_order_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_address_order_aud(
	orderaddressid					bigint NOT NULL,
	orderid							bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_address_order_aud add constraint [PK_mend_order_address_order_aud]
	primary key clustered (orderaddressid, orderid);
go
alter table mend.order_address_order_aud add constraint [DF_mend_order_address_order_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ orderprocessing$orderaddress_country ------------

create table mend.order_orderaddress_country (
	orderaddressid					bigint NOT NULL,
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.order_orderaddress_country add constraint [PK_mend_order_orderaddress_country]
	primary key clustered (orderaddressid, countryid);
go
alter table mend.order_orderaddress_country add constraint [DF_mend_order_orderaddress_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.order_orderaddress_country_aud(
	orderaddressid					bigint NOT NULL,
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.order_orderaddress_country_aud add constraint [PK_mend_order_orderaddress_country_aud]
	primary key clustered (orderaddressid, countryid);
go
alter table mend.order_orderaddress_country_aud add constraint [DF_mend_order_orderaddress_country_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

