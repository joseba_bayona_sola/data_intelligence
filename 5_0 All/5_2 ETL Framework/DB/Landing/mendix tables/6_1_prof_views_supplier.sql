use Landing
go 

----------------------- supp_supplier ----------------------------

select * 
from mend.supp_supplier_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- supp_supplierprice ----------------------------

select * 
from mend.supp_supplierprice_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- supp_standardprice ----------------------------

select * 
from mend.supp_standardprice_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- supp_manufacturer ----------------------------

select * 
from mend.supp_manufacturer_aud_v
where num_records > 1
order by id, idETLBatchRun
