
------------------------ Drop tables -------------------------------

use Landing
go

------------ purchaseorders$purchaseorder ------------

drop table mend.purc_purchaseorder;
go
drop table mend.purc_purchaseorder_aud;
go
drop table mend.purc_purchaseorder_aud_hist;
go

------------ purchaseorders$purchaseorder_supplier ------------

drop table mend.purc_purchaseorder_supplier;
go
drop table mend.purc_purchaseorder_supplier_aud;
go

------------ purchaseorders$purchaseorder_warehouse ------------

drop table mend.purc_purchaseorder_warehouse;
go
drop table mend.purc_purchaseorder_warehouse_aud;
go






------------ purchaseorders$purchaseorderlineheader ------------

drop table mend.purc_purchaseorderlineheader;
go
drop table mend.purc_purchaseorderlineheader_aud;
go
drop table mend.purc_purchaseorderlineheader_aud_hist;
go

------------ purchaseorders$purchaseorderlineheader_purchaseorder ------------

drop table mend.purc_purchaseorderlineheader_purchaseorder;
go
drop table mend.purc_purchaseorderlineheader_purchaseorder_aud;
go

------------ purchaseorders$purchaseorderlineheader_productfamily ------------

drop table mend.purc_purchaseorderlineheader_productfamily;
go
drop table mend.purc_purchaseorderlineheader_productfamily_aud;
go

------------ purchaseorders$purchaseorderlineheader_packsize ------------

drop table mend.purc_purchaseorderlineheader_packsize;
go
drop table mend.purc_purchaseorderlineheader_packsize_aud;
go

------------ purchaseorders$purchaseorderlineheader_supplierprices ------------

drop table mend.purc_purchaseorderlineheader_supplierprices;
go
drop table mend.purc_purchaseorderlineheader_supplierprices_aud;
go







------------ purchaseorders$purchaseorderline ------------

drop table mend.purc_purchaseorderline;
go
drop table mend.purc_purchaseorderline_aud;
go
drop table mend.purc_purchaseorderline_aud_hist;
go

------------ purchaseorders$purchaseorderline_purchaseorderlineheader ------------

drop table mend.purc_purchaseorderline_purchaseorderlineheader;
go
drop table mend.purc_purchaseorderline_purchaseorderlineheader_aud;
go

------------ purchaseorders$purchaseorderline_stockitem ------------

drop table mend.purc_purchaseorderline_stockitem;
go
drop table mend.purc_purchaseorderline_stockitem_aud;
go
