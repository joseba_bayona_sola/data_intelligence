
---------- Script creacion de tablas ----------

use Landing
go

------------ companyorganisation$company ------------

create table mend.comp_company (
	id								bigint NOT NULL,
	companyregnum					varchar(200),
	companyname						varchar(200),
	code							varchar(3),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_company add constraint [PK_mend_comp_company]
	primary key clustered (id);
go
alter table mend.comp_company add constraint [DF_mend_comp_company_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_company_aud(
	id								bigint NOT NULL,
	companyregnum					varchar(200),
	companyname						varchar(200),
	code							varchar(3),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_company_aud add constraint [PK_mend_comp_company_aud]
	primary key clustered (id);
go
alter table mend.comp_company_aud add constraint [DF_mend_comp_company_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_company_aud_hist(
	id								bigint NOT NULL,
	companyregnum					varchar(200),
	companyname						varchar(200),
	code							varchar(3),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.comp_company_aud_hist add constraint [DF_mend.comp_company_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------ companyorganisation$magwebstore ------------

create table mend.comp_magwebstore (
	id								bigint NOT NULL,
	storeid							varchar(200),
	storename						varchar(200),
	language						varchar(200),
	country							varchar(200),
	storecurrencycode				varchar(200),
	storetoorderrate				numeric(28,8),
	storetobaserate					numeric(28,8),
	snapcode						varchar(200),
	dispensingstatement				varchar(1000),
	system$owner					bigint,
	system$changedby				bigint,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_magwebstore add constraint [PK_mend_comp_magwebstore]
	primary key clustered (id);
go
alter table mend.comp_magwebstore add constraint [DF_mend_comp_magwebstore_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_magwebstore_aud(
	id								bigint NOT NULL,
	storeid							varchar(200),
	storename						varchar(200),
	language						varchar(200),
	country							varchar(200),
	storecurrencycode				varchar(200),
	storetoorderrate				numeric(28,8),
	storetobaserate					numeric(28,8),
	snapcode						varchar(200),
	dispensingstatement				varchar(1000),
	system$owner					bigint,
	system$changedby				bigint,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_magwebstore_aud add constraint [PK_mend_comp_magwebstore_aud]
	primary key clustered (id);
go
alter table mend.comp_magwebstore_aud add constraint [DF_mend_comp_magwebstore_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_magwebstore_aud_hist(
	id								bigint NOT NULL,
	storeid							varchar(200),
	storename						varchar(200),
	language						varchar(200),
	country							varchar(200),
	storecurrencycode				varchar(200),
	storetoorderrate				numeric(28,8),
	storetobaserate					numeric(28,8),
	snapcode						varchar(200),
	dispensingstatement				varchar(1000),
	system$owner					bigint,
	system$changedby				bigint,
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.comp_magwebstore_aud_hist add constraint [DF_mend.comp_magwebstore_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ countrymanagement$country ------------

create table mend.comp_country (
	id								bigint NOT NULL,
	countryname						varchar(200),
	iso_2							varchar(2),
	iso_3							varchar(3),
	numcode							int,
	currencyformat					varchar(20),
	taxationarea					varchar(5),
	standardvatrate					numeric(28,8),
	reducedvatrate					numeric(28,8),
	selected						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_country add constraint [PK_mend_comp_country]
	primary key clustered (id);
go
alter table mend.comp_country add constraint [DF_mend_comp_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_country_aud(
	id								bigint NOT NULL,
	countryname						varchar(200),
	iso_2							varchar(2),
	iso_3							varchar(3),
	numcode							int,
	currencyformat					varchar(20),
	taxationarea					varchar(5),
	standardvatrate					numeric(28,8),
	reducedvatrate					numeric(28,8),
	selected						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_country_aud add constraint [PK_mend_comp_country_aud]
	primary key clustered (id);
go
alter table mend.comp_country_aud add constraint [DF_mend_comp_country_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_country_aud_hist(
	id								bigint NOT NULL,
	countryname						varchar(200),
	iso_2							varchar(2),
	iso_3							varchar(3),
	numcode							int,
	currencyformat					varchar(20),
	taxationarea					varchar(5),
	standardvatrate					numeric(28,8),
	reducedvatrate					numeric(28,8),
	selected						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.comp_country_aud_hist add constraint [DF_mend.comp_country_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ countrymanagement$currency ------------

create table mend.comp_currency (
	id								bigint NOT NULL,
	currencycode					varchar(200),
	currencyname					varchar(50),
	standardrate					numeric(28,8),
	spotrate						numeric(28,8),
	laststandardupdate				datetime2,
	lastspotupdate					datetime2,  
	createddate						datetime2,
	changeddate						datetime2,  
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_currency add constraint [PK_mend_comp_currency]
	primary key clustered (id);
go
alter table mend.comp_currency add constraint [DF_mend_comp_currency_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_currency_aud(
	id								bigint NOT NULL,
	currencycode					varchar(200),
	currencyname					varchar(50),
	standardrate					numeric(28,8),
	spotrate						numeric(28,8),
	laststandardupdate				datetime2,
	lastspotupdate					datetime2,  
	createddate						datetime2,
	changeddate						datetime2,  
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_currency_aud add constraint [PK_mend_comp_currency_aud]
	primary key clustered (id);
go
alter table mend.comp_currency_aud add constraint [DF_mend_comp_currency_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_currency_aud_hist(
	id								bigint NOT NULL,
	currencycode					varchar(200),
	currencyname					varchar(50),
	standardrate					numeric(28,8),
	spotrate						numeric(28,8),
	laststandardupdate				datetime2,
	lastspotupdate					datetime2,  
	createddate						datetime2,
	changeddate						datetime2,  
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.comp_currency_aud_hist add constraint [DF_mend.comp_currency_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------ countrymanagement$country_currency ------------

create table mend.comp_country_currency (
	countryid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_country_currency add constraint [PK_mend_comp_country_currency]
	primary key clustered (countryid, currencyid);
go
alter table mend.comp_country_currency add constraint [DF_mend_comp_country_currency_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_country_currency_aud(
	countryid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_country_currency_aud add constraint [PK_mend_comp_country_currency_aud]
	primary key clustered (countryid, currencyid);
go
alter table mend.comp_country_currency_aud add constraint [DF_mend_comp_country_currency_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------ countrymanagement$spotrate ------------

create table mend.comp_spotrate (
	id								bigint NOT NULL,
	value							numeric(28,8),
	datasource						varchar(6),
	createddate						datetime,
	effectivedate					datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_spotrate add constraint [PK_mend_comp_spotrate]
	primary key clustered (id);
go
alter table mend.comp_spotrate add constraint [DF_mend_comp_spotrate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_spotrate_aud(
	id								bigint NOT NULL,
	value							numeric(28,8),
	datasource						varchar(6),
	createddate						datetime,
	effectivedate					datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_spotrate_aud add constraint [PK_mend_comp_spotrate_aud]
	primary key clustered (id);
go
alter table mend.comp_spotrate_aud add constraint [DF_mend_comp_spotrate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_spotrate_aud_hist(
	id								bigint NOT NULL,
	value							numeric(28,8),
	datasource						varchar(6),
	createddate						datetime,
	effectivedate					datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.comp_spotrate_aud_hist add constraint [DF_mend.comp_spotrate_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ countrymanagement$currentspotrate_currency ------------

create table mend.comp_currentspotrate_currency (
	spotrateid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_currentspotrate_currency add constraint [PK_mend_comp_currentspotrate_currency]
	primary key clustered (spotrateid, currencyid);
go
alter table mend.comp_currentspotrate_currency add constraint [DF_mend_comp_currentspotrate_currency_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_currentspotrate_currency_aud(
	spotrateid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_currentspotrate_currency_aud add constraint [PK_mend_comp_currentspotrate_currency_aud]
	primary key clustered (spotrateid, currencyid);
go
alter table mend.comp_currentspotrate_currency_aud add constraint [DF_mend_comp_currentspotrate_currency_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


------------ countrymanagement$historicalspotrate_currency ------------

create table mend.comp_historicalspotrate_currency (
	spotrateid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.comp_historicalspotrate_currency add constraint [PK_mend_comp_historicalspotrate_currency]
	primary key clustered (spotrateid, currencyid);
go
alter table mend.comp_historicalspotrate_currency add constraint [DF_mend_comp_historicalspotrate_currency_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.comp_historicalspotrate_currency_aud(
	spotrateid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.comp_historicalspotrate_currency_aud add constraint [PK_mend_comp_historicalspotrate_currency_aud]
	primary key clustered (spotrateid, currencyid);
go
alter table mend.comp_historicalspotrate_currency_aud add constraint [DF_mend_comp_historicalspotrate_currency_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go