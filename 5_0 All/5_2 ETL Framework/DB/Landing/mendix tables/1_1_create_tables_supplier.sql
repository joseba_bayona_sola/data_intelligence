use Landing
go

------------------------ Tables -------------------------------

------------------- suppliers$supplier -------------------------

create table mend.supp_supplier(
	id								bigint NOT NULL,
	supplierID						varchar(20),
	supplierincrementid				bigint,
	supplierName					varchar(200),
	currencyCode					varchar(4),
	supplierType					varchar(200),
	isEDIEnabled					bit,
	vatCode							varchar(200),
	defaultLeadTime					int, 
	email							varchar(200),
	telephone						varchar(20),
	fax								varchar(20),
	flatFileConfigurationToUse		varchar(20),
	street1							varchar(100),
	street2							varchar(100),
	street3							varchar(100),
	city							varchar(100),
	region							varchar(100),
	postcode						varchar(20),
	vendorCode						varchar(200),
	vendorShortName					varchar(200),
	vendorStoreNumber				varchar(50),
	defaulttransitleadtime			int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplier add constraint [PK_mend_supp_supplier]
	primary key clustered (id);
go
alter table mend.supp_supplier add constraint [DF_mend_supp_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplier_aud(
	id								bigint NOT NULL,
	supplierID						varchar(20),
	supplierincrementid				bigint,
	supplierName					varchar(200),
	currencyCode					varchar(4),
	supplierType					varchar(200),
	isEDIEnabled					bit,
	vatCode							varchar(200),
	defaultLeadTime					int, 
	email							varchar(200),
	telephone						varchar(20),
	fax								varchar(20),
	flatFileConfigurationToUse		varchar(20),
	street1							varchar(100),
	street2							varchar(100),
	street3							varchar(100),
	city							varchar(100),
	region							varchar(100),
	postcode						varchar(20),
	vendorCode						varchar(200),
	vendorShortName					varchar(200),
	vendorStoreNumber				varchar(50),
	defaulttransitleadtime			int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplier_aud add constraint [PK_mend_supp_supplier_aud]
	primary key clustered (id);
go
alter table mend.supp_supplier_aud add constraint [DF_mend_supp_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplier_aud_hist(
	id								bigint NOT NULL,
	supplierID						varchar(20),
	supplierincrementid				bigint,
	supplierName					varchar(200),
	currencyCode					varchar(4),
	supplierType					varchar(200),
	isEDIEnabled					bit,
	vatCode							varchar(200),
	defaultLeadTime					int, 
	email							varchar(200),
	telephone						varchar(20),
	fax								varchar(20),
	flatFileConfigurationToUse		varchar(20),
	street1							varchar(100),
	street2							varchar(100),
	street3							varchar(100),
	city							varchar(100),
	region							varchar(100),
	postcode						varchar(20),
	vendorCode						varchar(200),
	vendorShortName					varchar(200),
	vendorStoreNumber				varchar(50),
	defaulttransitleadtime			int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.supp_supplier_aud_hist add constraint [DF_mend.supp_supplier_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- suppliers$supplier_country -------------------------

create table mend.supp_supplier_country(
	supplierid						bigint NOT NULL,
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplier_country add constraint [PK_mend_supp_supplier_country]
	primary key clustered (supplierid, countryid);
go
alter table mend.supp_supplier_country add constraint [DF_mend_supp_supplier_country_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplier_country_aud(
	supplierid						bigint NOT NULL,
	countryid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplier_country_aud add constraint [PK_mend_supp_supplier_country_aud]
	primary key clustered (supplierid, countryid);
go
alter table mend.supp_supplier_country_aud add constraint [DF_mend_supp_supplier_country_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- suppliers$supplier_company -------------------------

create table mend.supp_supplier_company(
	supplierid						bigint NOT NULL,
	companyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplier_company add constraint [PK_mend_supp_supplier_company]
	primary key clustered (supplierid, companyid);
go
alter table mend.supp_supplier_company add constraint [DF_mend_supp_supplier_company_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplier_company_aud(
	supplierid						bigint NOT NULL,
	companyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplier_company_aud add constraint [PK_mend_supp_supplier_company_aud]
	primary key clustered (supplierid, companyid);
go
alter table mend.supp_supplier_company_aud add constraint [DF_mend_supp_supplier_company_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------------- suppliers$supplier_currency -------------------------

create table mend.supp_supplier_currency(
	supplierid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplier_currency add constraint [PK_mend_supp_supplier_currency]
	primary key clustered (supplierid, currencyid);
go
alter table mend.supp_supplier_currency add constraint [DF_mend_supp_supplier_currency_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplier_currency_aud(
	supplierid						bigint NOT NULL,
	currencyid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplier_currency_aud add constraint [PK_mend_supp_supplier_currency_aud]
	primary key clustered (supplierid, currencyid);
go
alter table mend.supp_supplier_currency_aud add constraint [DF_mend_supp_supplier_currency_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------------- suppliers$supplierprice -------------------------

create table mend.supp_supplierprice(
	id								bigint NOT NULL,
	subMetaObjectName				varchar(255),
	unitPrice						decimal(28,8),
	leadTime						int,
	totalWarehouseUnitPrice			decimal(28,8),
	orderMultiple					int,
	directPrice						bit,
	expiredPrice					bit,
	totallt							int, 
	shiplt							int, 
	transferlt						int, 
	comments						varchar(500),
	lastUpdated						datetime2,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplierprice add constraint [PK_mend_supp_supplierprice]
	primary key clustered (id);
go
alter table mend.supp_supplierprice add constraint [DF_mend_supp_supplierprice_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplierprice_aud(
	id								bigint NOT NULL,
	subMetaObjectName				varchar(255),
	unitPrice						decimal(28,8),
	leadTime						int,
	totalWarehouseUnitPrice			decimal(28,8),
	orderMultiple					int,
	directPrice						bit,
	expiredPrice					bit,
	totallt							int, 
	shiplt							int, 
	transferlt						int, 
	comments						varchar(500),
	lastUpdated						datetime2,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplierprice_aud add constraint [PK_mend_supp_supplierprice_aud]
	primary key clustered (id);
go
alter table mend.supp_supplierprice_aud add constraint [DF_mend_supp_supplierprice_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplierprice_aud_hist(
	id								bigint NOT NULL,
	subMetaObjectName				varchar(255),
	unitPrice						decimal(28,8),
	leadTime						int,
	totalWarehouseUnitPrice			decimal(28,8),
	orderMultiple					int,
	directPrice						bit,
	expiredPrice					bit,
	totallt							int, 
	shiplt							int, 
	transferlt						int, 
	comments						varchar(500),
	lastUpdated						datetime2,
	createdDate						datetime2,
	changedDate						datetime2,
	system$owner					bigint,
	system$changedby				bigint,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.supp_supplierprice_aud_hist add constraint [DF_mend.supp_supplierprice_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- suppliers$standardprice -------------------------

create table mend.supp_standardprice(
	id								bigint NOT NULL,
	moq								int,
	effectiveDate					datetime2,
	expiryDate						datetime2,
	maxqty							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_standardprice add constraint [PK_mend_supp_standardprice]
	primary key clustered (id);
go
alter table mend.supp_standardprice add constraint [DF_mend_supp_standardprice_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_standardprice_aud(
	id								bigint NOT NULL,
	moq								int,
	effectiveDate					datetime2,
	expiryDate						datetime2,
	maxqty							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_standardprice_aud add constraint [PK_mend_supp_standardprice_aud]
	primary key clustered (id);
go
alter table mend.supp_standardprice_aud add constraint [DF_mend_supp_standardprice_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_standardprice_aud_hist(
	id								bigint NOT NULL,
	moq								int,
	effectiveDate					datetime2,
	expiryDate						datetime2,
	maxqty							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.supp_standardprice_aud_hist add constraint [DF_mend_supp_standardprice_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- suppliers$supplierprices_supplier -------------------------

create table mend.supp_supplierprices_supplier(
	supplierpriceid					bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplierprices_supplier add constraint [PK_mend_supp_supplierprices_supplier]
	primary key clustered (supplierpriceid, supplierid);
go
alter table mend.supp_supplierprices_supplier add constraint [DF_mend_supp_supplierprices_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplierprices_supplier_aud(
	supplierpriceid					bigint NOT NULL,
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplierprices_supplier_aud add constraint [PK_mend_supp_supplierprices_supplier_aud]
	primary key clustered (supplierpriceid, supplierid);
go
alter table mend.supp_supplierprices_supplier_aud add constraint [DF_mend_supp_supplierprices_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- suppliers$supplierprices_packsize -------------------------

create table mend.supp_supplierprices_packsize(
	supplierpriceid					bigint NOT NULL,
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_supplierprices_packsize add constraint [PK_mend_supp_supplierprices_packsize]
	primary key clustered (supplierpriceid, packsizeid);
go
alter table mend.supp_supplierprices_packsize add constraint [DF_mend_supp_supplierprices_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_supplierprices_packsize_aud(
	supplierpriceid					bigint NOT NULL,
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_supplierprices_packsize_aud add constraint [PK_mend_supp_supplierprices_packsize_aud]
	primary key clustered (supplierpriceid, packsizeid);
go
alter table mend.supp_supplierprices_packsize_aud add constraint [DF_mend_supp_supplierprices_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




------------------- suppliers$manufacturer -------------------------

create table mend.supp_manufacturer(
	id								bigint NOT NULL,
	manufacturerid					bigint, 
	manufacturername				varchar(200),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_manufacturer add constraint [PK_mend_supp_manufacturer]
	primary key clustered (id);
go
alter table mend.supp_manufacturer add constraint [DF_mend_supp_manufacturer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_manufacturer_aud(
	id								bigint NOT NULL,
	manufacturerid					bigint, 
	manufacturername				varchar(200),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_manufacturer_aud add constraint [PK_mend_supp_manufacturer_aud]
	primary key clustered (id);
go
alter table mend.supp_manufacturer_aud add constraint [DF_mend_supp_manufacturer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_manufacturer_aud_hist(
	id								bigint NOT NULL,
	manufacturerid					bigint, 
	manufacturername				varchar(200),
	createddate						datetime2,
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.supp_manufacturer_aud_hist add constraint [DF_mend.supp_manufacturer_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------------- product$packsize_manufacturer -------------------------

create table mend.supp_packsize_manufacturer(
	packsizeid						bigint NOT NULL,
	manufacturerid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mend.supp_packsize_manufacturer add constraint [PK_mend_supp_packsize_manufacturer]
	primary key clustered (packsizeid, manufacturerid);
go
alter table mend.supp_packsize_manufacturer add constraint [DF_mend_supp_packsize_manufacturer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.supp_packsize_manufacturer_aud(
	packsizeid						bigint NOT NULL,
	manufacturerid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.supp_packsize_manufacturer_aud add constraint [PK_mend_supp_packsize_manufacturer_aud]
	primary key clustered (packsizeid, manufacturerid);
go
alter table mend.supp_packsize_manufacturer_aud add constraint [DF_mend_supp_packsize_manufacturer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
