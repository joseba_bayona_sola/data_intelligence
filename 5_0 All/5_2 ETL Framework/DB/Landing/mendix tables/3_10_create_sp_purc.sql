
------------------------------ SP -----------------------------------

use Landing
go

------------ purchaseorders$purchaseorder ------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorder
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Exclude tpotransferleadtime, tpotransferdate attributes
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorder
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorder
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				ordernumber, source, status, potype,
				leadtime, 
				itemcost, totalprice, formattedprice, 
				duedate, receiveddate, createddate, 
				approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
				transferleadtime, transitleadtime, 
				spoleadtime, spoduedate, 
				tpoleadtime, NULL tpotransferleadtime, NULL tpotransferdate, tpoduedate, 
				requiredshipdate, requiredtransferdate, 
				estimatedpaymentdate, estimateddepositdate,
				supplierreference,
				changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
				'from openquery(MENDIX_DB_RESTORE,''
					select id,
						ordernumber, source, status, potype,
						leadtime, 
						itemcost, totalprice, formattedprice, 
						duedate, receiveddate, createddate, 
						approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
						transferleadtime, transitleadtime, 
						spoleadtime, spoduedate, 
						tpoleadtime, tpoduedate, 
						requiredshipdate, requiredtransferdate, 
						estimatedpaymentdate, estimateddepositdate,
						supplierreference,
						changeddate						 
					from public."purchaseorders$purchaseorder"'')' -- tpotransferleadtime, tpotransferdate, 
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

--------------- purchaseorders$purchaseorder_supplier ---------------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorder_supplier
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorder_supplier
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorder_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderid purchaseorderid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderid", "suppliers$supplierid" 
					from public."purchaseorders$purchaseorder_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorder_warehouse -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorder_warehouse
go

-- ===========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorder_warehouse
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorder_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderid purchaseorderid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderid", "inventory$warehouseid" 
					from public."purchaseorders$purchaseorder_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go











------------------- purchaseorders$purchaseorderlineheader -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader
go

-- ===========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderlineheader
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				status, problems, 
				lines, items, totalprice, 
				createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						status, problems, 
						lines, items, totalprice, 
						createddate, changeddate 
					from public."purchaseorders$purchaseorderlineheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderlineheader_purchaseorder -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_purchaseorder
go

-- =========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderlineheader_purchaseorder
-- =========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_purchaseorder
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineheaderid purchaseorderlineheaderid, purchaseorders$purchaseorderid purchaseorderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineheaderid", "purchaseorders$purchaseorderid" 
					from public."purchaseorders$purchaseorderlineheader_purchaseorder"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderlineheader_productfamily -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_productfamily
go

-- =========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderlineheader_productfamily
-- =========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_productfamily
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineheaderid purchaseorderlineheaderid, product$productfamilyid productfamilyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineheaderid", "product$productfamilyid" 
					from public."purchaseorders$purchaseorderlineheader_productfamily"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderlineheader_packsize -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_packsize
go

-- =========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderlineheader_packsize
-- =========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineheaderid purchaseorderlineheaderid, product$packsizeid packsizeid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineheaderid", "product$packsizeid" 
					from public."purchaseorders$purchaseorderlineheader_packsize"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderlineheader_supplierprices -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_supplierprices
go

-- ==========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderlineheader_supplierprices
-- ==========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderlineheader_supplierprices
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineheaderid purchaseorderlineheaderid, suppliers$supplierpriceid supplierpriceid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineheaderid", "suppliers$supplierpriceid" 
					from public."purchaseorders$purchaseorderlineheader_supplierprices"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go












------------------- purchaseorders$purchaseorderline -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderline
go

-- ==========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	09-01-2018	Joseba Bayona Sola	quantityDelivered is not anymore in Mendix Table so NULL
-- ==========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderline
-- ==========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				po_lineID, stockItemID,
				quantity, quantityReceived, NULL quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
				lineprice, 
				backtobackduedate,
				createdDate, changedDate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						po_lineID, stockItemID,
						quantity, quantityReceived, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
						lineprice, 
						backtobackduedate,
						createdDate, changedDate 
					from public."purchaseorders$purchaseorderline"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderline_purchaseorderlineheader -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderline_purchaseorderlineheader
go

-- ==========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderline_purchaseorderlineheader
-- ==========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderline_purchaseorderlineheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineid purchaseorderlineid, purchaseorders$purchaseorderlineheaderid purchaseorderlineheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineid", "purchaseorders$purchaseorderlineheaderid" 
					from public."purchaseorders$purchaseorderline_purchaseorderlineheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- purchaseorders$purchaseorderline_stockitem -------------------

drop procedure mend.srcmend_lnd_get_purc_purchaseorderline_stockitem
go

-- ==========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 14-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - purc_purchaseorderline_stockitem
-- ==========================================================================================================

create procedure mend.srcmend_lnd_get_purc_purchaseorderline_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select purchaseorders$purchaseorderlineid purchaseorderlineid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "purchaseorders$purchaseorderlineid", "product$stockitemid" 
					from public."purchaseorders$purchaseorderline_stockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

