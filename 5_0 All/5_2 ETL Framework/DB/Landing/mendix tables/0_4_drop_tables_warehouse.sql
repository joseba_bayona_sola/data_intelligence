
------------------------ Drop tables -------------------------------

use Landing
go

------------ inventory$warehouse ------------

drop table mend.wh_warehouse;
go
drop table mend.wh_warehouse_aud;
go
drop table mend.wh_warehouse_aud_hist;
go


------------ inventory$warehousesupplier_warehouse ------------

drop table mend.wh_warehousesupplier_warehouse;
go
drop table mend.wh_warehousesupplier_warehouse_aud;
go


------------ inventory$warehousesupplier_supplier ------------

drop table mend.wh_warehousesupplier_supplier;
go
drop table mend.wh_warehousesupplier_supplier_aud;
go





------------ inventory$supplierroutine ------------

drop table mend.wh_supplierroutine;
go
drop table mend.wh_supplierroutine_aud;
go
drop table mend.wh_supplierroutine_aud_hist;
go

------------ inventory$supplierroutine_warehousesupplier ------------

drop table mend.wh_supplierroutine_warehousesupplier;
go
drop table mend.wh_supplierroutine_warehousesupplier_aud;
go




------------ inventory$preference ------------

drop table mend.wh_preference;
go
drop table mend.wh_preference_aud;
go
drop table mend.wh_preference_aud_hist;
go


------------ inventory$warehouse_preference ------------

drop table mend.wh_warehouse_preference;
go
drop table mend.wh_warehouse_preference_aud;
go

------------ inventory$country_preference ------------

drop table mend.wh_country_preference;
go
drop table mend.wh_country_preference_aud;
go

