use Landing
go 

------------ inventory$warehousestockitem ------------

select * 
from mend.wh_warehousestockitem_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$warehousestockitembatch ------------

select * 
from mend.wh_warehousestockitembatch_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$batchstockissue ------------

select * 
from mend.wh_batchstockissue_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$batchstockallocation ------------

select * 
from mend.wh_batchstockallocation_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$repairstockbatchs ------------

select * 
from mend.wh_repairstockbatchs_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$stockmovement ------------

select * 
from mend.wh_stockmovement_aud_v
where num_records > 1
order by id, idETLBatchRun

------------ inventory$batchstockmovement ------------

select * 
from mend.wh_batchstockmovement_aud_v
where num_records > 1
order by id, idETLBatchRun