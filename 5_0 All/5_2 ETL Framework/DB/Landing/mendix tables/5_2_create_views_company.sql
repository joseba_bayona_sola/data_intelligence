use Landing
go 

------------------- comp_company -------------------

drop view mend.comp_company_aud_v
go 

create view mend.comp_company_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		companyregnum, companyname,
		code, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			companyregnum, companyname,
			code, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.comp_company_aud
		union
		select 'H' record_type, id, 
			companyregnum, companyname,
			code, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_company_aud_hist) t
go

------------------- comp_magwebstore -------------------

drop view mend.comp_magwebstore_aud_v
go 

create view mend.comp_magwebstore_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		storeid, storename, language, country,
		storecurrencycode, storetoorderrate, storetobaserate,
		snapcode, 
		dispensingstatement, 
		system$owner, system$changedby,
		createddate, changeddate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			storeid, storename, language, country,
			storecurrencycode, storetoorderrate, storetobaserate,
			snapcode, 
			dispensingstatement, 
			system$owner, system$changedby,
			createddate, changeddate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.comp_magwebstore_aud
		union
		select 'H' record_type, id, 
			storeid, storename, language, country,
			storecurrencycode, storetoorderrate, storetobaserate,
			snapcode, 
			dispensingstatement, 
			system$owner, system$changedby,
			createddate, changeddate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_magwebstore_aud_hist) t
go



------------------- comp_country -------------------

drop view mend.comp_country_aud_v 
go

create view mend.comp_country_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		countryname, iso_2, iso_3, numcode, 
		currencyformat, 
		taxationarea, standardvatrate, reducedvatrate, 
		selected, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			countryname, iso_2, iso_3, numcode, 
			currencyformat, 
			taxationarea, standardvatrate, reducedvatrate, 
			selected, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.comp_country_aud
		union
		select 'H' record_type, id, 
			countryname, iso_2, iso_3, numcode, 
			currencyformat, 
			taxationarea, standardvatrate, reducedvatrate, 
			selected, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_country_aud_hist) t
go


------------------- comp_currency -------------------

drop view mend.comp_currency_aud_v 
go 

create view mend.comp_currency_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		currencycode, currencyname, standardrate, spotrate, 
		laststandardupdate, lastspotupdate, 
		createddate, changeddate, system$owner, system$changedby,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			currencycode, currencyname, standardrate, spotrate, 
			laststandardupdate, lastspotupdate, 
			createddate, changeddate, system$owner, system$changedby,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.comp_currency_aud
		union
		select 'H' record_type, id, 
			currencycode, currencyname, standardrate, spotrate, 
			laststandardupdate, lastspotupdate, 
			createddate, changeddate, system$owner, system$changedby,			
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_currency_aud_hist) t
go


------------------- comp_spotrate -------------------

drop view mend.comp_spotrate_aud_v 
go 

create view mend.comp_spotrate_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		value, datasource, 
		createddate, effectivedate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			value, datasource, 
			createddate, effectivedate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.comp_spotrate_aud
		union
		select 'H' record_type, id, 
			value, datasource, 
			createddate, effectivedate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.comp_spotrate_aud_hist) t
go
