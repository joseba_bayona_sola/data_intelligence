
------------------------------ SP -----------------------------------

use Landing
go

------------------- intersitetransfer$transferheader -------------------

drop procedure mend.srcmend_lnd_get_inter_transferheader
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_transferheader
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_inter_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				transfernum, allocatedstrategy, 
				totalremaining,
				createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					transfernum, allocatedstrategy, 
					totalremaining,
					createddate, changeddate 
				from public."intersitetransfer$transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ purchaseorders$supplypo_transferheader ------------

drop procedure mend.srcmend_lnd_get_inter_supplypo_transferheader
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_supplypo_transferheader
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_supplypo_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$transferheaderid transferheaderid, purchaseorders$purchaseorderid purchaseorderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$transferheaderid", "purchaseorders$purchaseorderid" 
				from public."purchaseorders$supplypo_transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ purchaseorders$transferpo_transferheader ------------

drop procedure mend.srcmend_lnd_get_inter_transferpo_transferheader
go

-- ==============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==============================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_transferpo_transferheader
-- ==============================================================================================

create procedure mend.srcmend_lnd_get_inter_transferpo_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$transferheaderid transferheaderid, purchaseorders$purchaseorderid purchaseorderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$transferheaderid", "purchaseorders$purchaseorderid" 
				from public."purchaseorders$transferpo_transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ orderprocessing$order_transferheader ------------

drop procedure mend.srcmend_lnd_get_inter_order_transferheader
go

-- ==============================================================================================
-- Author: Joseba Bayona Sola
-- Date: 07-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==============================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_order_transferheader
-- ==============================================================================================

create procedure mend.srcmend_lnd_get_inter_order_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select "orderprocessing$orderid" orderid, intersitetransfer$transferheaderid transferheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "orderprocessing$orderid", "intersitetransfer$transferheaderid" 
				from public."orderprocessing$order_transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go







------------------- intersitetransfer$intransitbatchheader -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitbatchheader
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitbatchheader
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitbatchheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, createddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, createddate 
				from public."intersitetransfer$intransitbatchheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ intersitetransfer$intransitbatchheader_transferheader ------------

drop procedure mend.srcmend_lnd_get_inter_intransitbatchheader_transferheader
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitbatchheader_transferheader
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitbatchheader_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitbatchheaderid intransitbatchheaderid, intersitetransfer$transferheaderid transferheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitbatchheaderid", "intersitetransfer$transferheaderid" 
				from public."intersitetransfer$intransitbatchheader_transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ intersitetransfer$intransitbatchheader_customershipment ------------

drop procedure mend.srcmend_lnd_get_inter_intransitbatchheader_customershipment
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitbatchheader_customershipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitbatchheader_customershipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitbatchheaderid intransitbatchheaderid, customershipments$customershipmentid customershipmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitbatchheaderid", "customershipments$customershipmentid" 
				from public."intersitetransfer$intransitbatchheader_customershipment"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go







------------------- intersitetransfer$intransitstockitembatch -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitstockitembatch
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	05-06-2018	Joseba Bayona Sola	Remove used attribute
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitstockitembatch
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitstockitembatch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				issuedquantity, remainingquantity, 
				NULL used,
				productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
				currency,
				groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
				createddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					issuedquantity, remainingquantity, 
					
					productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
					currency,
					groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
					createddate 
				from public."intersitetransfer$intransitstockitembatch"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- intersitetransfer$intransitstockitembatch_transferheader -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_transferheader
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitstockitembatch_transferheader
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_transferheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitstockitembatchid intransitstockitembatchid, intersitetransfer$transferheaderid transferheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitstockitembatchid", "intersitetransfer$transferheaderid" 
				from public."intersitetransfer$intransitstockitembatch_transferheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- intersitetransfer$intransitstockitembatch_intransitbatchheader -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_intransitbatchheader
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitstockitembatch_intransitbatchheader
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_intransitbatchheader
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitstockitembatchid intransitstockitembatchid, intersitetransfer$intransitbatchheaderid intransitbatchheaderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitstockitembatchid", "intersitetransfer$intransitbatchheaderid" 
				from public."intersitetransfer$intransitstockitembatch_intransitbatchheader"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- intersitetransfer$intransitstockitembatch_stockitem -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_stockitem
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitstockitembatch_stockitem
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_stockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitstockitembatchid intransitstockitembatchid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitstockitembatchid", "product$stockitemid" 
				from public."intersitetransfer$intransitstockitembatch_stockitem"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- intersitetransfer$intransitstockitembatch_shipment -------------------

drop procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_receipt
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 22-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	22-03-2018	Joseba Bayona Sola	Shipment to Receipt change
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - inter_intransitstockitembatch_shipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_inter_intransitstockitembatch_receipt
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select intersitetransfer$intransitstockitembatchid intransitstockitembatchid, warehouseshipments$receiptid receiptid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "intersitetransfer$intransitstockitembatchid", "warehouseshipments$receiptid" 
				from public."intersitetransfer$intransitstockitembatch_receipt"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go