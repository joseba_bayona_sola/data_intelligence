use Landing
go 

------------------- calendar$shippingmethod -------------------

drop view mend.cal_shippingmethod_aud_v
go 

create view mend.cal_shippingmethod_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
		leadtime, letterRate1, letterRate2, tracked, invoicedocs,
		defaultweight, minweight, scurrimappable,
		mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
		mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
		mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
		mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
			leadtime, letterRate1, letterRate2, tracked, invoicedocs,
			defaultweight, minweight, scurrimappable,
			mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
			mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.cal_shippingmethod_aud
		union
		select 'H' record_type, id, 
			magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
			leadtime, letterRate1, letterRate2, tracked, invoicedocs,
			defaultweight, minweight, scurrimappable,
			mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
			mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_shippingmethod_aud_hist) t
go



------------------- calendar$defaultcalendar -------------------

drop view mend.cal_defaultcalendar_aud_v
go 

create view mend.cal_defaultcalendar_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
		mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
		timeHorizonWeeks,
		createdDate, changedDate, 
		system$changedby, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.cal_defaultcalendar_aud
		union
		select 'H' record_type, id, 
			mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
			mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_defaultcalendar_aud_hist) t
go




------------------- calendar$defaultwarehousesuppliercalender -------------------

drop view mend.cal_defaultwarehousesuppliercalender_aud_v
go 

create view mend.cal_defaultwarehousesuppliercalender_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
		mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
		mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
		timeHorizonWeeks,
		createdDate, changedDate, 
		system$changedby, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
			mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
			mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.cal_defaultwarehousesuppliercalender_aud
		union
		select 'H' record_type, id, 
			mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
			mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
			mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
			timeHorizonWeeks,
			createdDate, changedDate, 
			system$changedby, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.cal_defaultwarehousesuppliercalender_aud_hist) t
go