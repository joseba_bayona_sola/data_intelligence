use Landing
go 

----------------------- warehouseshipments$receipt ----------------------------

select * 
from mend.whship_receipt_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- warehouseshipments$receiptline -------------------

select * 
from mend.whship_receiptline_aud_v
where num_records > 1
order by id, idETLBatchRun

------------------- warehouseshipments$receiptlineheader -------------------

select * 
from mend.whship_receiptlineheader_aud_v
where num_records > 1
order by id, idETLBatchRun

