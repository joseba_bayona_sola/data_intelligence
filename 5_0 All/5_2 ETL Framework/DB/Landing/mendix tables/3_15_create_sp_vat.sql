
------------------------------ SP -----------------------------------

use Landing
go

------------------- vat$commodity -------------------

drop procedure mend.srcmend_lnd_get_vat_commodity
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_commodity
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_commodity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				commoditycode, commodityname, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					commoditycode, commodityname 
				from public."vat$commodity" '')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ product$productfamily_commodity ------------

drop procedure mend.srcmend_lnd_get_vat_productfamily_commodity
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_productfamily_commodity
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_vat_productfamily_commodity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$productfamilyid productfamilyid, vat$commodityid commodityid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "product$productfamilyid", "vat$commodityid" 
				from public."product$productfamily_commodity"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------------- vat$dispensingservicesrate -------------------

drop procedure mend.srcmend_lnd_get_vat_dispensingservicesrate
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_dispensingservicesrate
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_dispensingservicesrate
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				dispensingservicesrate, rateeffectivedate, rateenddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					dispensingservicesrate, rateeffectivedate, rateenddate 
				from public."vat$dispensingservicesrate"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$dispensingservices_commodity -------------------

drop procedure mend.srcmend_lnd_get_vat_dispensingservices_commodity
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_dispensingservices_commodity
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_dispensingservices_commodity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$dispensingservicesrateid dispensingservicesrateid, vat$commodityid commodityid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$dispensingservicesrateid", "vat$commodityid"
				from public."vat$dispensingservices_commodity"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$dispensingservicesrate_country -------------------

drop procedure mend.srcmend_lnd_get_vat_dispensingservicesrate_country
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_dispensingservicesrate_country
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_dispensingservicesrate_country
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$dispensingservicesrateid dispensingservicesrateid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$dispensingservicesrateid", "countrymanagement$countryid" 
				from public."vat$dispensingservicesrate_country"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go








------------------- vat$vatrating -------------------

drop procedure mend.srcmend_lnd_get_vat_vatrating
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatrating
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatrating
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				vatratingcode, vatratingdescription, ' + cast(@idETLBatchRun as varchar(20)) + ' '+ 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					vatratingcode, vatratingdescription 
				from public."vat$vatrating"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$vatrating_country -------------------

drop procedure mend.srcmend_lnd_get_vat_vatrating_country
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatrating_country
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatrating_country
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$vatratingid vatratingid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$vatratingid", "countrymanagement$countryid" 
				from public."vat$vatrating_country"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------------- vat$vatrate -------------------

drop procedure mend.srcmend_lnd_get_vat_vatrate
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatrate
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatrate
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				vatrate, vatrateeffectivedate, vatrateenddate, 
				createddate, changeddate, 
				system$owner, system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					vatrate, vatrateeffectivedate, vatrateenddate, 
					createddate, changeddate, 
					"system$owner", "system$changedby" 
				from public."vat$vatrate"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$vatrate_vatrating -------------------

drop procedure mend.srcmend_lnd_get_vat_vatrate_vatrating
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatrate_vatrating
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatrate_vatrating
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$vatrateid vatrateid, vat$vatratingid vatratingid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$vatrateid", "vat$vatratingid" 
				from public."vat$vatrate_vatrating"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go







------------------- vat$vatschedule -------------------

drop procedure mend.srcmend_lnd_get_vat_vatschedule
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatschedule
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatschedule
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate 
				from public."vat$vatschedule"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$vatschedule_vatrating -------------------

drop procedure mend.srcmend_lnd_get_vat_vatschedule_vatrating
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatschedule_vatrating
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatschedule_vatrating
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$vatscheduleid vatscheduleid, vat$vatratingid vatratingid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$vatscheduleid", "vat$vatratingid" 
				from public."vat$vatschedule_vatrating"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- vat$vatschedule_commodity -------------------

drop procedure mend.srcmend_lnd_get_vat_vatschedule_commodity
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 28-06-2017
-- Changed: 
	--	07-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - vat_vatschedule_commodity
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_vat_vatschedule_commodity
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select vat$vatscheduleid vatscheduleid, vat$commodityid commodityid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "vat$vatscheduleid", "vat$commodityid" 
				from public."vat$vatschedule_commodity"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go