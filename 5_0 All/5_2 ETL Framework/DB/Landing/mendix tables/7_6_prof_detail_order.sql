use Landing
go 

------------------------------------------------------------------------
----------------------- orderprocessing$order --------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_order_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_order_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_order_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			incrementID, magentoOrderID, orderStatus, 
			orderLinesMagento, orderLinesDeduped,
			_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
			allocatedDate, promisedShippingDate, promisedDeliveryDate,
			labelRenderer,  
			couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
			createdDate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_order_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------ orderprocessing$order_customer ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_order_customer_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
------------------ orderprocessing$order_magwebstore -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_order_magwebstore_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
---------------------- orderprocessing$basevalues ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_basevalues_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_basevalues_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_basevalues_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_basevalues_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id,
			subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
			discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
			shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
			adjustmentNegative, adjustmentPositive, 				
			custBalanceAmount, 
			customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
			grandTotal, totalInvoiced, totalCanceled, totalRefunded,
			toOrderRate, toGlobalRate, currencyCode,
			mutualamount,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_basevalues_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------ orderprocessing$basevalues_order --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_basevalues_order_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------------
------------------ orderprocessing$order_statusupdate ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_order_statusupdate_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_order_statusupdate_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_order_statusupdate_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_order_statusupdate_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, status, timestamp, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_order_statusupdate_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------ orderprocessing$statusupdate_order ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_statusupdate_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
-------------- orderprocessing$orderprocessingtransaction --------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderprocessingtransaction_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderprocessingtransaction_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_orderprocessingtransaction_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderprocessingtransaction_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, status, timestamp, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderprocessingtransaction_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------- orderprocessing$orderprocessingtransaction_order -----------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderprocessingtransaction_order_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------------
------------------ orderprocessing$orderlineabstract -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderlineabstract_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderlineabstract_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_orderlineabstract_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderlineabstract_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			magentoItemID, 
			quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
			basePrice, baseRowPrice, 
			allocated, 
			orderLineType, subMetaObjectName,
			sku, eye, lensGroupID, 
			netPrice, netRowPrice,
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderlineabstract_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------ orderprocessing$orderlinemagento --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderlinemagento_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderlinemagento_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_orderlinemagento_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderlinemagento_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, lineAdded, deduplicate, fulfilled, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderlinemagento_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
---------------- orderprocessing$orderlinemagento_order ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderlinemagento_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
---------------------- orderprocessing$orderline -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderline_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_orderline_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_orderlinemagento_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, status, packsOrdered, packsAllocated, packsShipped, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderline_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
------------------- orderprocessing$orderline_order --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderline_order_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
------------------- orderprocessing$shippinggroup ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_shippinggroup_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_shippinggroup_aud

	select idETLBatchRun, count(*) num_rows
	from mend.order_shippinggroup_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.order_shippinggroup_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			dispensingFee, 
			allocationStatus, shipmentStatus, 
			wholesale, 
			letterBoxAble, onHold, adHoc, cancelled,
			createdDate, changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_shippinggroup_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------- orderprocessing$shippinggroup_order ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_shippinggroup_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------- orderprocessing$orderlineabstract_shippinggroup ------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderlineabstract_shippinggroup_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
---------------- orderprocessing$orderline_product  --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderline_product_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
------------- orderprocessing$orderline_requiredstockitem  -------------
------------------------------------------------------------------------

select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderline_requiredstockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun



------------------------------------------------------------------------
------------------ orderprocessing$address_order ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_address_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
------------------ orderprocessing$orderaddress_country ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.order_orderaddress_country_aud
	group by idETLBatchRun
	order by idETLBatchRun
