
------------------------------- Triggers ----------------------------------

use Landing
go

------------ intersitetransfer$transferheader ------------

drop trigger mend.trg_inter_transferheader;
go 

create trigger mend.trg_inter_transferheader on mend.inter_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.transfernum, ' '), isnull(trg.allocatedstrategy, ' '),
			isnull(trg.totalremaining, 0),
			isnull(trg.createddate, ' '), isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.transfernum, ' '), isnull(src.allocatedstrategy, ' '),
			isnull(src.totalremaining, 0),
			isnull(src.createddate, ' '), isnull(src.changeddate, ' '))
			
		then
			update set
				trg.transfernum = src.transfernum, trg.allocatedstrategy = src.allocatedstrategy,
				trg.totalremaining = src.totalremaining,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				transfernum, allocatedstrategy, 
				totalremaining,
				createddate, changeddate, 
				idETLBatchRun)

				values (src.id,
					src.transfernum, src.allocatedstrategy, 
					src.totalremaining,
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_inter_transferheader_aud;
go 

create trigger mend.trg_inter_transferheader_aud on mend.inter_transferheader_aud
for update, delete
as
begin
	set nocount on

	insert mend.inter_transferheader_aud_hist (id,
		transfernum, allocatedstrategy, 
		totalremaining,
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.transfernum, d.allocatedstrategy, 
			d.totalremaining,
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$supplypo_transferheader -------------------

drop trigger mend.trg_inter_supplypo_transferheader;
go 

create trigger mend.trg_inter_supplypo_transferheader on mend.inter_supplypo_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_supplypo_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.transferheaderid = src.transferheaderid and trg.purchaseorderid = src.purchaseorderid)
	when not matched 
		then
			insert (transferheaderid, purchaseorderid, 
				idETLBatchRun)

				values (src.transferheaderid, src.purchaseorderid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$transferpo_transferheader -------------------

drop trigger mend.trg_inter_transferpo_transferheader;
go 

create trigger mend.trg_inter_transferpo_transferheader on mend.inter_transferpo_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_transferpo_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.transferheaderid = src.transferheaderid and trg.purchaseorderid = src.purchaseorderid)
	when not matched 
		then
			insert (transferheaderid, purchaseorderid, 
				idETLBatchRun)

				values (src.transferheaderid, src.purchaseorderid,
					src.idETLBatchRun);
end; 
go


------------------- orderprocessing$order_transferheader -------------------

drop trigger mend.trg_inter_order_transferheader;
go 

create trigger mend.trg_inter_order_transferheader on mend.inter_order_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_order_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.orderid = src.orderid and trg.transferheaderid = src.transferheaderid)
	when not matched 
		then
			insert (orderid, transferheaderid,
				idETLBatchRun)

				values (src.orderid, src.transferheaderid,
					src.idETLBatchRun);
end; 
go








------------------- intersitetransfer$intransitbatchheader -------------------

drop trigger mend.trg_inter_intransitbatchheader;
go 

create trigger mend.trg_inter_intransitbatchheader on mend.inter_intransitbatchheader
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitbatchheader_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select isnull(trg.createddate, ' ')
		intersect
		select isnull(src.createddate, ' '))
			
		then
			update set
				trg.createddate = src.createddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, createddate, 
				idETLBatchRun)

				values (src.id, src.createddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_inter_intransitbatchheader_aud;
go 

create trigger mend.trg_inter_intransitbatchheader_aud on mend.inter_intransitbatchheader_aud
for update, delete
as
begin
	set nocount on

	insert mend.inter_intransitbatchheader_aud_hist (id, createddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.createddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- intersitetransfer$intransitbatchheader_transferheader -------------------

drop trigger mend.trg_inter_intransitbatchheader_transferheader;
go 

create trigger mend.trg_inter_intransitbatchheader_transferheader on mend.inter_intransitbatchheader_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitbatchheader_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.intransitbatchheaderid = src.intransitbatchheaderid and trg.transferheaderid = src.transferheaderid)
	when not matched 
		then
			insert (intransitbatchheaderid, transferheaderid, 
				idETLBatchRun)

				values (src.intransitbatchheaderid, src.transferheaderid,
					src.idETLBatchRun);
end; 
go

------------------- intersitetransfer$intransitbatchheader_customershipment -------------------

drop trigger mend.trg_inter_intransitbatchheader_customershipment;
go 

create trigger mend.trg_inter_intransitbatchheader_customershipment on mend.inter_intransitbatchheader_customershipment
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitbatchheader_customershipment_aud with (tablock) as trg
	using inserted src
		on (trg.intransitbatchheaderid = src.intransitbatchheaderid and trg.customershipmentid = src.customershipmentid)
	when not matched 
		then
			insert (intransitbatchheaderid, customershipmentid, 
				idETLBatchRun)

				values (src.intransitbatchheaderid, src.customershipmentid,
					src.idETLBatchRun);
end; 
go







------------------- intersitetransfer$intransitstockitembatch -------------------

drop trigger mend.trg_inter_intransitstockitembatch;
go 

create trigger mend.trg_inter_intransitstockitembatch on mend.inter_intransitstockitembatch
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitstockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.issuedquantity, 0), isnull(trg.remainingquantity, 0),
			-- isnull(trg.used, ' '),
			isnull(trg.productunitcost, 0), isnull(trg.carriageUnitCost, 0), isnull(trg.totalunitcost, 0), isnull(trg.totalUnitCostIncInterCo, 0),
			isnull(trg.currency, ' '),
			isnull(trg.groupFIFOdate, ' '), isnull(trg.arriveddate, ' '), isnull(trg.confirmeddate, ' '), isnull(trg.stockregistereddate, ' '),
			isnull(trg.createddate, ' ')
		intersect
		select 
			isnull(src.issuedquantity, 0), isnull(src.remainingquantity, 0),
			-- isnull(src.used, ' '),
			isnull(src.productunitcost, 0), isnull(src.carriageUnitCost, 0), isnull(src.totalunitcost, 0), isnull(src.totalUnitCostIncInterCo, 0),
			isnull(src.currency, ' '),
			isnull(src.groupFIFOdate, ' '), isnull(src.arriveddate, ' '), isnull(src.confirmeddate, ' '), isnull(src.stockregistereddate, ' '),
			isnull(src.createddate, ' '))
			
		then
			update set
				trg.issuedquantity = src.issuedquantity, trg.remainingquantity = src.remainingquantity,
				-- trg.used = src.used,
				trg.productunitcost = src.productunitcost, trg.carriageUnitCost = src.carriageUnitCost, trg.totalunitcost = src.totalunitcost, trg.totalUnitCostIncInterCo = src.totalUnitCostIncInterCo,
				trg.currency = src.currency,
				trg.groupFIFOdate = src.groupFIFOdate, trg.arriveddate = src.arriveddate, trg.confirmeddate = src.confirmeddate, trg.stockregistereddate = src.stockregistereddate,
				trg.createddate = src.createddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				issuedquantity, remainingquantity, 
				used,
				productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
				currency,
				groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
				createddate, 
				idETLBatchRun)

				values (src.id,
					src.issuedquantity, src.remainingquantity, 
					src.used,
					src.productunitcost, src.carriageUnitCost, src.totalunitcost, src.totalUnitCostIncInterCo,
					src.currency,
					src.groupFIFOdate, src.arriveddate, src.confirmeddate, src.stockregistereddate, 
					src.createddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_inter_intransitstockitembatch_aud;
go 

create trigger mend.trg_inter_intransitstockitembatch_aud on mend.inter_intransitstockitembatch_aud
for update, delete
as
begin
	set nocount on

	insert mend.inter_intransitstockitembatch_aud_hist (id,
		issuedquantity, remainingquantity, 
		used,
		productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
		currency,
		groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
		createddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.issuedquantity, d.remainingquantity, 
			d.used,
			d.productunitcost, d.carriageUnitCost, d.totalunitcost, d.totalUnitCostIncInterCo,
			d.currency,
			d.groupFIFOdate, d.arriveddate, d.confirmeddate, d.stockregistereddate, 
			d.createddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- intersitetransfer$intransitstockitembatch_transferheader -------------------

drop trigger mend.trg_inter_intransitstockitembatch_transferheader;
go 

create trigger mend.trg_inter_intransitstockitembatch_transferheader on mend.inter_intransitstockitembatch_transferheader
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitstockitembatch_transferheader_aud with (tablock) as trg
	using inserted src
		on (trg.intransitstockitembatchid = src.intransitstockitembatchid and trg.transferheaderid = src.transferheaderid)
	when not matched 
		then
			insert (intransitstockitembatchid, transferheaderid, 
				idETLBatchRun)

				values (src.intransitstockitembatchid, src.transferheaderid,
					src.idETLBatchRun);
end; 
go

------------------- intersitetransfer$intransitstockitembatch_intransitbatchheader -------------------

drop trigger mend.trg_inter_intransitstockitembatch_intransitbatchheader;
go 

create trigger mend.trg_inter_intransitstockitembatch_intransitbatchheader on mend.inter_intransitstockitembatch_intransitbatchheader
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitstockitembatch_intransitbatchheader_aud with (tablock) as trg
	using inserted src
		on (trg.intransitstockitembatchid = src.intransitstockitembatchid and trg.intransitbatchheaderid = src.intransitbatchheaderid)
	when not matched 
		then
			insert (intransitstockitembatchid, intransitbatchheaderid, 
				idETLBatchRun)

				values (src.intransitstockitembatchid, src.intransitbatchheaderid,
					src.idETLBatchRun);
end; 
go

------------------- intersitetransfer$intransitstockitembatch_stockitem -------------------

drop trigger mend.trg_inter_intransitstockitembatch_stockitem;
go 

create trigger mend.trg_inter_intransitstockitembatch_stockitem on mend.inter_intransitstockitembatch_stockitem
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitstockitembatch_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.intransitstockitembatchid = src.intransitstockitembatchid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (intransitstockitembatchid, stockitemid, 
				idETLBatchRun)

				values (src.intransitstockitembatchid, src.stockitemid,
					src.idETLBatchRun);
end; 
go

------------------- intersitetransfer$intransitstockitembatch_receipt -------------------

drop trigger mend.trg_inter_intransitstockitembatch_receipt;
go 

create trigger mend.trg_inter_intransitstockitembatch_receipt on mend.inter_intransitstockitembatch_receipt
after insert
as
begin
	set nocount on

	merge into mend.inter_intransitstockitembatch_receipt_aud with (tablock) as trg
	using inserted src
		on (trg.intransitstockitembatchid = src.intransitstockitembatchid and trg.receiptid = src.receiptid)
	when not matched 
		then
			insert (intransitstockitembatchid, receiptid, 
				idETLBatchRun)

				values (src.intransitstockitembatchid, src.receiptid,
					src.idETLBatchRun);
end; 
go