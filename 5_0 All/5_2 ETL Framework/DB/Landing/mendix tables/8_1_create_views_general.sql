
Use Landing
go 

--------------- WAREHOUSE RELATED -----------------------------

drop view mend.gen_wh_warehouse_v 
go 

create view mend.gen_wh_warehouse_v as
	select id warehouseid, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore
	from mend.wh_warehouse_aud
go




drop view mend.gen_wh_warehouse_calendar_v 
go 

create view mend.gen_wh_warehouse_calendar_v  as
	select w.warehouseid, c.id defaultcalendarid, 
		w.code, w.name, w.warehouseType, 
		c.mondayWorking, convert(varchar(8), c.mondayCutOff, 108) mondayCutOff, 
		c.tuesdayWorking, convert(varchar(8), c.tuesdayCutOff, 108) tuesdayCutOff, 
		c.wednesdayWorking, convert(varchar(8), c.wednesdayCutOff, 108) wednesdayCutOff, 
		c.thursdayWorking, convert(varchar(8), c.thursdayCutOff, 108) thursdayCutOff, 
		c.fridayWorking, convert(varchar(8), c.fridayCutOff, 108) fridayCutOff, 
		c.saturdayWorking, convert(varchar(8), c.saturdayCutOff, 108) saturdayCutOff, 
		c.sundayWorking, convert(varchar(8), c.sundayCutOff, 108) sundayCutOff, 
		c.timeHorizonWeeks
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.cal_defaultcalendar_warehouse_aud cw on w.warehouseid = cw.warehouseid
		inner join
			Landing.mend.cal_defaultcalendar_aud c on cw.defaultcalendarid = c.id
go


drop view mend.gen_wh_warehouse_shippingmethod_v 
go 

create view mend.gen_wh_warehouse_shippingmethod_v as
	select w.warehouseid, sm.id shippingmethodid, 
		w.code, w.name, w.warehouseType, 
		sm.magentoShippingDescription, sm.magentoShippingMethod, sm.scurriShippingMethod, sm.snapShippingMethod, 
		sm.leadtime, sm.letterRate1, sm.letterRate2, sm.tracked, sm.invoicedocs,
		sm.defaultweight, sm.minweight, sm.scurrimappable,
		convert(varchar(8), sm.mondayDefaultCutOff, 108) mondayDefaultCutOff, sm.mondayWorking, sm.mondayDeliveryWorking, convert(varchar(8), sm.mondayCollectionTime, 108) mondayCollectionTime, 
		convert(varchar(8), sm.tuesdayDefaultCutOff, 108) tuesdayDefaultCutOff, sm.tuesdayWorking, sm.tuesdayDeliveryWorking, convert(varchar(8), sm.tuesdayCollectionTime, 108) tuesdayCollectionTime,
		convert(varchar(8), sm.wednesdayDefaultCutOff, 108) wednesdayDefaultCutOff, sm.wednesdayWorking, sm.wednesdayDeliveryWorking, convert(varchar(8), sm.wednesdayCollectionTime, 108) wednesdayCollectionTime,
		convert(varchar(8), sm.thursdayDefaultCutOff, 108) thursdayDefaultCutOff, sm.thursdayWorking, sm.thursdayDeliveryWorking, convert(varchar(8), sm.thursdayCollectionTime, 108) thursdayCollectionTime,
		convert(varchar(8), sm.fridayDefaultCutOff, 108) fridayDefaultCutOff, sm.fridayWorking, sm.fridayDeliveryWorking, convert(varchar(8), sm.mondayCollectionTime, 108) fridayCollectionTime,
		convert(varchar(8), sm.saturdayDefaultCutOff, 108) saturdayDefaultCutOff, sm.saturdayWorking, sm.saturdayDeliveryWorking, convert(varchar(8), sm.saturdayCollectionTime, 108) saturdayCollectionTime,
		convert(varchar(8), sm.sundayDefaultCutOff, 108) sundayDefaultCutOff, sm.sundayWorking, sm.sundayDeliveryWorking, convert(varchar(8), sm.sundayCollectionTime, 108) sundayCollectionTime
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.cal_shippingmethods_warehouse_aud smw on w.warehouseid = smw.warehouseid
		inner join
			Landing.mend.cal_shippingmethod_aud sm on smw.shippingmethodid = sm.id
go

drop view mend.gen_wh_warehouse_supplier_v 
go 

create view mend.gen_wh_warehouse_supplier_v as
	select w.warehouseid, sr.id supplierroutineid, 
		w.code, w.name, w.warehouseType, 
		sr.supplierName, 
		sr.mondayDefaultCanSupply, convert(varchar(8), sr.mondayOrderCutOffTime, 108) mondayOrderCutOffTime, sr.mondayDefaultCanOrder, 
		sr.tuesdayDefaultCanSupply, convert(varchar(8), sr.tuesdayOrderCutOffTime, 108) tuesdayOrderCutOffTime, sr.tuesdayDefaultCanOrder, 
		sr.wednesdayDefaultCanSupply, convert(varchar(8), sr.wednesdayOrderCutOffTime, 108) wednesdayOrderCutOffTime, sr.wednesdayDefaultCanOrder, 
		sr.thursdayDefaultCanSupply, convert(varchar(8), sr.thursdayOrderCutOffTime, 108) thursdayOrderCutOffTime, sr.thursdayDefaultCanOrder, 
		sr.fridayDefaultCanSupply, convert(varchar(8), sr.fridayOrderCutOffTime, 108) fridayOrderCutOffTime, sr.fridayDefaultCanOrder, 
		sr.saturdayDefaultCanSupply, convert(varchar(8), sr.saturdayOrderCutOffTime, 108) saturdayOrderCutOffTime, sr.saturdayDefaultCanOrder, 
		sr.sundayDefaultCanSupply, convert(varchar(8), sr.sundayOrderCutOffTime, 108) sundayOrderCutOffTime, sr.sundayDefaultCanOrder
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.wh_warehousesupplier_warehouse wsw on w.warehouseid = wsw.warehouseid
		inner join
			Landing.mend.wh_supplierroutine_warehousesupplier_aud srw on wsw.warehousesupplierid = srw.warehousesupplierID
		inner join
			Landing.mend.wh_supplierroutine_aud sr on srw.supplierRoutineID = sr.id
go


drop view mend.gen_wh_warehouse_country_v 
go 

create view mend.gen_wh_warehouse_country_v as
	select w.warehouseid, p.id preferenceid, c.id countryid,
		w.code, w.name, w.warehouseType, 
		c.iso_2 country_code, c.countryname,
		p.order_num
	from 
			Landing.mend.gen_wh_warehouse_v w
		inner join
			Landing.mend.wh_warehouse_preference_aud wp on w.warehouseid = wp.warehouseid
		inner join
			Landing.mend.wh_preference_aud p on wp.preferenceid = p.id
		inner join
			Landing.mend.wh_country_preference_aud cp on p.id = cp.preferenceid
		inner join
			Landing.mend.comp_country_aud c on cp.countryid = c.id
go		



--------------- SUPPLIER RELATED -----------------------------

drop view mend.gen_supp_supplier_v 
go 

create view mend.gen_supp_supplier_v as
	select id supplier_id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		defaultLeadTime,
		flatFileConfigurationToUse,		
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime
	from Landing.mend.supp_supplier_aud
go


drop view mend.gen_supp_supplierprice_v 
go 

create view mend.gen_supp_supplierprice_v as
	select s.supplier_id, sp.id supplierpriceid,
		s.supplierID, s.supplierName, s.currencyCode, s.supplierType, 
		sp.subMetaObjectName,
		sp.unitPrice, sp.leadTime, sp.totalWarehouseUnitPrice, sp.orderMultiple,
		sp.directPrice, sp.expiredPrice,
		sp.totallt, sp.shiplt, sp.transferlt,
		sp.comments,
		sp.lastUpdated,
		sp.createdDate, sp.changedDate, 
		stp.moq, stp.effectiveDate, stp.expiryDate, stp.maxqty
	from 
		Landing.mend.gen_supp_supplier_v s
	left join
		Landing.mend.supp_supplierprices_supplier_aud ssp on s.supplier_id = ssp.supplierid
	left join	
		Landing.mend.supp_supplierprice_aud sp on ssp.supplierpriceid = sp.id
	left join
		Landing.mend.supp_standardprice_aud stp on sp.id = stp.id
go



--------------- PRODUCT RELATED -----------------------------

drop view mend.gen_prod_productfamily_v 
go 

create view mend.gen_prod_productfamily_v as
	select pf.id productfamilyid, 
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pf.id end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name, pf.status
	from Landing.mend.prod_productfamily_aud pf
go


drop view mend.gen_prod_productfamilypacksize_v 
go 

create view mend.gen_prod_productfamilypacksize_v as
	select pspf.productfamilyid, pspf.packsizeid, 
		dense_rank() over (partition by pf.magentoProductID order by size, pspf.packsizeid) productfamilysizerank,
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pspf.productfamilyid end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name, pf.status,
		ps.description, ps.size, 
		ps.packagingNo, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, 
		ps.weight, ps.height, ps.width, ps.depth 
	from 
			Landing.mend.prod_productfamily_aud pf
		left join
			Landing.mend.prod_packsize_productfamily_aud pspf on pf.id = pspf.productfamilyid
		right join
			Landing.mend.prod_packsize_aud ps on pspf.packsizeid = ps.id
go


drop view mend.gen_prod_productfamilypacksize_supplierprice_v 
go 

create view mend.gen_prod_productfamilypacksize_supplierprice_v as

	select pfp.productfamilyid, pfp.packsizeid, sp.supplier_id, sp.supplierpriceid,
		pfp.magentoProductID_int, pfp.magentoProductID, pfp.magentoSKU, pfp.name, pfp.status, pfp.productFamilySizeRank, pfp.size, 
		sp.supplierID, sp.supplierName, sp.subMetaObjectName, sp.unitPrice, sp.currencyCode, sp.leadTime, 
		sp.effectiveDate, sp.expiryDate, 
		case when (getutcdate() between effectiveDate and expiryDate) then 1 else 0 end active
	from 
			Landing.mend.gen_prod_productfamilypacksize_v pfp
		left join	
			Landing.mend.supp_supplierprices_packsize_aud spp on pfp.packsizeid = spp.packsizeid
		left join
			Landing.mend.gen_supp_supplierprice_v sp on spp.supplierpriceid = sp.supplierpriceid
go


drop view mend.gen_prod_product_v 
go 

create view mend.gen_prod_product_v as
	select pf.id productfamilyid, p.id productid,
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else pf.id end magentoProductID_int,
		pf.magentoProductID,
		pf.magentoSKU, pf.name, 
		p.valid, p.SKU SKU_product, p.oldSKU, p.description, 
		cl.bc, cl.di, cl.po, cl.cy, cl.ax,  cl.ad, cl._do, cl.co, cl.co_EDI
	from 
			Landing.mend.prod_productfamily_aud pf
		left join
			Landing.mend.prod_product_productfamily_aud pfp on pf.id = pfp.productfamilyid
		left join
			Landing.mend.prod_product_aud p on pfp.productid = p.id
		left join
			Landing.mend.prod_contactlens_aud cl on p.id = cl.id
go	

drop view mend.gen_prod_stockitem_v 
go 

create view mend.gen_prod_stockitem_v as
	select p.productfamilyid, p.productid, si.id stockitemid, 
		p.magentoProductID_int, p.magentoProductID,
		p.magentoSKU, p.name, 
		p.valid, p.SKU_product, p.oldSKU, p.description, 
		si.packSize, si.SKU, si.stockitemdescription, -- si.oldSKU, 
		p.bc, p.di, p.po, p.cy, p.ax, p.ad, p._do, p.co, p.co_EDI, 
		si.manufacturerArticleID, si.manufacturerCodeNumber,
		si.SNAPUploadStatus
	from 
			Landing.mend.gen_prod_product_v p
		left join
			Landing.mend.prod_stockitem_product_aud sip on p.productid = sip.productid
		left join
			Landing.mend.prod_stockitem_aud si on sip.stockitemid = si.id
go


--------------- STOCK RELATED -----------------------------

drop view mend.gen_wh_warehousestockitem_v 
go 

create view mend.gen_wh_warehousestockitem_v as
	select si.productfamilyid, si.productid, si.stockitemid, wsi.id warehousestockitemid,
		w.warehouseid,
		si.magentoProductID_int, si.magentoProductID,
		si.magentoSKU, si.name, si.SKU_product, si.description, si.packSize, si.SKU, si.stockitemdescription,
		si.bc, si.di, si.po, si.cy, si.ax, si.ad, si._do, si.co, si.co_EDI, 
		w.code, w.name warehouse_name, wsi.id_string, 
		wsi.actualstockqty, wsi.availableqty, wsi.outstandingallocation, wsi.allocatedstockqty, wsi.onhold, wsi.dueinqty, wsi.forwarddemand, 
		wsi.stocked, 		
		wsi.stockingmethod
	from 
			Landing.mend.gen_prod_stockitem_v si
		inner join
			Landing.mend.wh_warehousestockitem_stockitem_aud wsisi on si.stockitemid = wsisi.stockitemid 	
		inner join -- 12 SI without WSI
			Landing.mend.wh_warehousestockitem_aud wsi on wsisi.warehousestockitemid = wsi.id
		inner join
			Landing.mend.wh_located_at_aud wsiw on wsi.id = wsiw.warehousestockitemid
		inner join
			Landing.mend.gen_wh_warehouse_v w on wsiw.warehouseid = w.warehouseid
go


drop view mend.gen_wh_warehousestockitembatch_v 
go 

create view mend.gen_wh_warehousestockitembatch_v as
	select wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, wsib.id warehousestockitembatchid,
		wsibrl.receiptlineid,
		case when (bstwsib.warehousestockitembatchid is null) then 0 else 1 end batchstockmovement_f, 
		case when (rstwsib.warehousestockitembatchid is null) then 0 else 1 end batchrepair_f,
		wsi.magentoProductID_int, wsi.magentoProductID,
		wsi.name, wsi.stockitemdescription, wsi.packSize, -- wsi.magentoSKU, wsi.SKU_product, wsi.SKU
		-- wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.co_EDI, 
		wsi.code, wsi.warehouse_name, wsi.id_string, 
		-- wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		-- wsi.outstandingallocation, wsi.onhold,
		-- wsi.stockingmethod, 
		wsib.batch_id,
		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.autoadjusted, 
		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, wsib.forwardDemand, wsib.registeredQuantity,
		wsib.groupFIFODate, wsib.arrivedDate, wsib.confirmedDate, wsib.stockregisteredDate, wsib.receiptCreatedDate,
		wsib.productUnitCost, wsib.carriageUnitCost, wsib.dutyUnitCost, wsib.totalUnitCost, 
		wsib.inteCoCarriageUnitCost interCoCarriageUnitCost, wsib.totalUnitCostIncInterCo,
		wsib.exchangeRate, wsib.currency
	from 
			Landing.mend.gen_wh_warehousestockitem_v wsi
		inner join
			Landing.mend.wh_warehousestockitembatch_warehousestockitem_aud wsiwsib on wsi.warehousestockitemid = wsiwsib.warehousestockitemid
		inner join
			Landing.mend.wh_warehousestockitembatch_aud wsib on wsiwsib.warehousestockitembatchid = wsib.id
		left join
			Landing.mend.whship_warehousestockitembatch_receiptline wsibrl on wsib.id = wsibrl.warehousestockitembatchid
		left join
			(select warehousestockitembatchid, count(*) num
			from Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud
			group by warehousestockitembatchid) bstwsib on wsib.id = bstwsib.warehousestockitembatchid
		left join
			(select warehousestockitembatchid, count(*) num
			from Landing.mend.wh_repairstockbatchs_warehousestockitembatch_aud
			group by warehousestockitembatchid) rstwsib on wsib.id = rstwsib.warehousestockitembatchid
go




drop view mend.gen_wh_warehousestockitembatch_repair_v 
go 

create view mend.gen_wh_warehousestockitembatch_repair_v as
	select rsb.id repairstockbatchsid,
		wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, 
		rsb.batchtransaction_id, rsb.reference, rsb.date,
		rsb.oldallocation, rsb.newallocation, rsb.oldissue, rsb.newissue, rsb.processed, 
		wsib.magentoProductID_int, wsib.magentoProductID, 
		wsib.name, wsib.stockitemdescription, -- wsib.magentoSKU, wsib.SKU_product, wsib.packSize, wsib.SKU, 
		-- wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		-- wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
		-- wsib.outstandingallocation, wsib.onhold,
		-- wsib.stockingmethod, 
		wsib.batch_id
		-- wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
		-- wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
		-- wsib.confirmedDate, 
		-- wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency
	from 
			Landing.mend.wh_repairstockbatchs_aud rsb 
		inner join
			Landing.mend.wh_repairstockbatchs_warehousestockitembatch_aud rsbwsib on rsb.id = rsbwsib.repairstockbatchsid 
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on rsbwsib.warehousestockitembatchid = wsib.warehousestockitembatchid 
go


drop view mend.gen_wh_stockmovement_v 
go 

create view mend.gen_wh_stockmovement_v as

	select sm.id stockmovementid, wsi.productfamilyid, wsi.productid, wsi.stockitemid, wsi.warehousestockitemid, 
		sm.moveid, sm.movelineid, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.dateCreated stockmovement_createdate, sm.dateClosed stockmovement_closeddate,
		wsi.magentoProductID_int, wsi.magentoProductID,
		wsi.name, wsi.stockitemdescription, -- wsi.magentoSKU, wsi.SKU_product, wsi.packSize, wsi.SKU, 
		-- wsi.bc, wsi.di, wsi.po, wsi.cy, wsi.ax, wsi.ad, wsi._do, wsi.co, wsi.co_EDI, 
		wsi.code, wsi.warehouse_name, wsi.id_string
		-- wsi.availableqty, wsi.actualstockqty, wsi.allocatedstockqty, wsi.forwarddemand, wsi.dueinqty, wsi.stocked, 
		-- wsi.outstandingallocation, wsi.onhold,
		-- wsi.stockingmethod
	from 
			Landing.mend.wh_stockmovement_aud sm 
		inner join
			Landing.mend.wh_stockmovement_warehousestockitem_aud smwsi on sm.id = smwsi.stockmovementid 
		inner join
			Landing.mend.gen_wh_warehousestockitem_v wsi on smwsi.warehousestockitemid = wsi.warehousestockitemid 
go


drop view mend.gen_wh_batchstockmovement_v 
go 

create view mend.gen_wh_batchstockmovement_v as

	select bsm.id batchstockmovementid, sm.stockmovementid, wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid,
		sm.moveid, sm.movelineid, bsm.batchstockmovement_id, 
		sm.stockadjustment, sm.movementtype, sm.movetype, sm._class, sm.reasonid, sm.status, 
		sm.skuid, sm.qtyactioned, 
		sm.fromstate, sm.tostate, sm.fromstatus, sm.tostatus, sm.operator, sm.supervisor,
		sm.stockmovement_createdate, sm.stockmovement_closeddate,
		bsm.createdDate batchstockmovement_createdate, bsm.batchstockmovementtype, bsm.quantity, bsm.comment,
		wsib.magentoProductID_int, wsib.magentoProductID,
		wsib.name, wsib.stockitemdescription, -- sm.magentoSKU, sm.SKU_product, sm.packSize, sm.SKU, 
		-- sm.bc, sm.di, sm.po, sm.cy, sm.ax, sm.ad, sm._do, sm.co, sm.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		-- sm.availableqty, sm.actualstockqty, sm.allocatedstockqty, sm.forwarddemand, sm.dueinqty, sm.stocked, 
		-- sm.outstandingallocation, sm.onhold,
		-- sm.stockingmethod, 
		wsib.batch_id
	from 
			Landing.mend.gen_wh_stockmovement_v sm
		full join
			Landing.mend.wh_batchstockmovement_stockmovement_aud bsmsm on sm.stockmovementid = bsmsm.stockmovementid
		full join
			Landing.mend.wh_batchstockmovement_aud bsm on bsmsm.batchstockmovementid = bsm.id
		inner join
			Landing.mend.wh_batchstockmovement_warehousestockitembatch_aud bsmwsi on bsm.id = bsmwsi.batchstockmovementid
		inner join	
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsmwsi.warehousestockitembatchid = wsib.warehousestockitembatchid
go



--------------- ORDER RELATED -----------------------------

drop view mend.gen_order_order_v 
go 

create view mend.gen_order_order_v as
	select o.id orderid, bv.id basevaluesid, -- oc.retailcustomerid, os.magwebstoreid,
		case when (isnumeric(o.magentoOrderID) = 1) then convert(bigint, o.magentoOrderID) else o.id end magentoOrderID_int, magentoOrderID,
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		o._type, o.shipmentStatus, 
		o.allocationStatus, o.allocationStrategy, o.allocatedDate, 
		o.promisedshippingdate, o.promiseddeliverydate,
		o.labelRenderer,  
		o.shippingMethod, o.shippingDescription, 
		bv.subtotal, bv.subtotalInclTax, bv.subtotalInvoiced, bv.subtotalCanceled, bv.subtotalRefunded, 
		bv.discountAmount, bv.discountInvoiced, bv.discountCanceled, bv.discountRefunded, 
		bv.shippingAmount, bv.shippingInvoiced, bv.shippingCanceled, bv.shippingRefunded, 					 
		bv.adjustmentNegative, bv.adjustmentPositive, 					
		bv.custBalanceAmount, 
		bv.customerBalanceAmount, bv.customerBalanceInvoiced, bv.customerBalanceRefunded, bv.customerBalanceTotRefunded, bv.customerBalanceTotalRefunded, 
		bv.grandTotal, bv.totalInvoiced, bv.totalCanceled, bv.totalRefunded,
		bv.toOrderRate, bv.toGlobalRate, bv.currencyCode
	from	
			Landing.mend.order_order_aud o
		inner join
			Landing.mend.order_basevalues_order_aud obv on o.id = obv.orderid
		inner join
			Landing.mend.order_basevalues_aud bv on obv.basevaluesid = bv.id
		-- left join
			-- Landing.mend.order_order_customer_aud oc on o.id = oc.orderid -- include Wholesale orders
		-- left join
			-- Landing.mend.order_order_magwebstore_aud os on o.id = os.orderid
go


drop view mend.gen_order_orderline_v 
go 

create view mend.gen_order_orderline_v as
	select o.orderid, o.basevaluesid, ol.id orderlineid, olp.productid, p.productfamilyid, -- o.retailcustomerid, o.magwebstoreid, 
		o.magentoOrderID_int, o.magentoOrderID, ola.magentoItemID,
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped,
		p.magentoProductID_int, p.magentoProductID, 
		p.name, ola.sku, -- param ??
		ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityIssued, ola.quantityAllocated, ola.quantityShipped, 
		ola.basePrice, ola.baseRowPrice, 
		ola.allocated, 
		ola.orderLineType, ola.subMetaObjectName
	from 
			Landing.mend.gen_order_order_v o
		inner join
			Landing.mend.order_orderline_order_aud olo on o.orderid = olo.orderid
		inner join
			Landing.mend.order_orderline_aud ol on olo.orderlineid = ol.id
		inner join
			Landing.mend.order_orderlineabstract_ol_aud ola on ol.id = ola.id -- Needed? If making the query slow
		inner join
			Landing.mend.order_orderline_product_aud olp on ol.id = olp.orderlineabstractid
		inner join
			Landing.mend.gen_prod_product_v p on olp.productid = p.productid
go



drop view mend.gen_order_orderlinemagento_v 
go 

create view mend.gen_order_orderlinemagento_v as
	select o.orderid, o.basevaluesid, olm.id orderlinemagentoid, olp.productid, p.productfamilyid, --o.retailcustomerid, o.magwebstoreid, 
		o.magentoOrderID_int, ola.magentoItemID,
		o.incrementID, o.createdDate, o.orderStatus, 
		o.orderLinesMagento, o.orderLinesDeduped,
		olm.lineAdded, olm.deduplicate, olm.fulfilled,
		p.magentoProductID_int, p.magentoProductID, 
		p.name, ola.sku, -- param ??
		ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityIssued, ola.quantityAllocated, ola.quantityShipped, 
		ola.basePrice, ola.baseRowPrice, 
		ola.allocated, 
		ola.orderLineType, ola.subMetaObjectName
	from 
			Landing.mend.gen_order_order_v o
		inner join
			Landing.mend.order_orderlinemagento_order_aud olmo on o.orderid = olmo.orderid
		inner join
			Landing.mend.order_orderlinemagento_aud olm on olmo.orderlinemagentoid = olm.id
		inner join
			Landing.mend.order_orderlineabstract_olm_aud ola on olm.id = ola.id -- Needed? If making the query slow
		inner join
			Landing.mend.order_orderline_product_aud olp on olm.id = olp.orderlineabstractid
		inner join
			Landing.mend.gen_prod_product_v p on olp.productid = p.productid
go

--------------- SHIPMENT RELATED -----------------------------

drop view mend.gen_ship_customershipment_v 
go 

create view mend.gen_ship_customershipment_v as
	select s.id customershipmentid, o.orderid, 
		o.magentoOrderID_int, o.incrementID, o.createdDate createdDate_order, o.orderStatus,
		s.orderIncrementID, s.shipmentNumber, 
		s.status, s.warehouseMethod, s.labelRenderer, 
		s.shippingMethod, s.shippingDescription, 
		s.notify, s.fullyShipped, s.syncedToMagento, 	
		s.shipmentValue, s.shippingTotal,
		s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate,  
		lcu.timestamp snap_timestamp,
		s.createdDate, s.changedDate
	from 
			Landing.mend.ship_customershipment_aud s
		inner join
			Landing.mend.ship_customershipment_order_aud so on s.id = so.customershipmentid
		inner join
			Landing.mend.gen_order_order_v o on so.orderid = o.orderid
		left join
			(select customershipmentid, max(lifecycle_updateid) lifecycle_updateid
			from Landing.mend.ship_customershipment_progress_aud
			group by customershipmentid) lcus on s.id = lcus.customershipmentid
			-- Landing.mend.ship_customershipment_progress_aud lcus on s.id = lcus.customershipmentid
		left join
			Landing.mend.ship_lifecycle_update_aud lcu on lcus.lifecycle_updateid = lcu.id
go



--------------- ALLOCATION RELATED -----------------------------

drop view mend.gen_all_stockallocationtransaction_v 
go 

create view mend.gen_all_stockallocationtransaction_v as
	select sat.id orderlinestockallocationtransactionid, -- btsat.batchstockallocationid,
		satw.warehouseid, sato.orderid, satol.orderlineid, satwsi.warehousestockitemid, satps.packsizeid, -- satwsib.warehousestockitembatchid, 
		o.magentoOrderID_int, o.incrementID, w.name wh_name,
		sat.transactionID, sat.timestamp,
		sat.allocationType, sat.recordType, 
		sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
		sat.stockAllocated, sat.cancelled, 
		sat.issuedDateTime
	from 
			Landing.mend.all_orderlinestockallocationtransaction_aud sat
		left join
			Landing.mend.all_at_warehouse_aud satw on sat.id = satw.orderlinestockallocationtransactionid
		left join
			Landing.mend.gen_wh_warehouse_v w on satw.warehouseid = w.warehouseid
		inner join
			Landing.mend.all_at_order_aud sato on sat.id = sato.orderlinestockallocationtransactionid
		inner join
			Landing.mend.gen_order_order_v o on sato.orderid = o.orderid
		inner join
			Landing.mend.all_at_orderline_aud satol on sat.id = satol.orderlinestockallocationtransactionid
		inner join
			Landing.mend.all_at_warehousestockitem_aud satwsi on sat.id = satwsi.orderlinestockallocationtransactionid
		left join
			Landing.mend.all_at_packsize_aud satps on sat.id = satps.orderlinestockallocationtransactionid
		-- inner join
			-- Landing.mend.all_at_batch_aud satwsib on sat.id = satwsib.orderlinestockallocationtransactionid
		-- left join
			-- Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction_aud btsat on sat.id = btsat.orderlinestockallocationtransactionid
go

drop view mend.gen_all_magentoallocation_v 
go 

create view mend.gen_all_magentoallocation_v as
	select ma.id magentoallocationid, masat.orderlinestockallocationtransactionid,
		maw.warehouseid, maolm.orderlinemagentoid,
		w.name wh_name,
		ma.shipped, ma.cancelled cancelled_magentoall, 
		ma.quantity
	from 
			Landing.mend.all_magentoallocation_aud ma
		left join
			Landing.mend.all_magentoallocation_warehouse_aud maw on ma.id = maw.magentoallocationid
		left join
			Landing.mend.gen_wh_warehouse_v w on maw.warehouseid = w.warehouseid
		inner join 
			Landing.mend.all_magentoallocation_orderlinemagento_aud maolm on ma.id = maolm.magentoallocationid
		inner join
			Landing.mend.all_magentoallocatio_orderlinestockallocationtransac_aud masat on ma.id = masat.magentoallocationid
go

--create view mend.gen_all_magentoallocation_v as
--	select ma.id magentoallocationid, sat.orderlinestockallocationtransactionid, -- sat.batchstockallocationid,
--		ma.shipped, ma.cancelled cancelled_magentoall, 
--		ma.quantity,
--		sat.warehouseid, sat.orderid, sat.orderlineid, sat.warehousestockitemid, sat.packsizeid, -- sat.customershipmentlineid, sat.warehousestockitembatchid, 
--		sat.magentoOrderID_int, sat.incrementID, sat.wh_name,
--		sat.transactionID, sat.timestamp,
--		sat.allocationType, sat.recordType, 
--		sat.allocatedUnits, sat.allocatedStockQuantity, sat.issuedQuantity, sat.packSize, 
--		sat.stockAllocated, sat.cancelled, 
--		sat.issuedDateTime
--	from 
--			Landing.mend.gen_all_stockallocationtransaction_v sat
--		inner join -- 200 k stock allocation rows (child + glasses + ??)
--			Landing.mend.all_magentoallocatio_orderlinestockallocationtransac_aud masat on sat.orderlinestockallocationtransactionid = masat.orderlinestockallocationtransactionid 
--		inner join
--			Landing.mend.all_magentoallocation_aud ma on masat.magentoallocationid = ma.id
--go


drop view mend.gen_wh_warehousestockitembatch_alloc_v 
go 

create view mend.gen_wh_warehousestockitembatch_alloc_v as
	select wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, 
		bsa.id batchstockallocationid, btsat.orderlinestockallocationtransactionid,
		wsib.magentoProductID_int, wsib.magentoProductID,
		wsib.name, wsib.stockitemdescription, -- wsib.magentoSKU, wsib.SKU_product, wsib.packSize, wsib.SKU, 
		-- wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		-- wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
		-- wsib.outstandingallocation, wsib.onhold,
		-- wsib.stockingmethod, 
		wsib.batch_id,
		-- wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
		-- wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
		-- wsib.confirmedDate, 
		-- wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency, 
		bsa.fullyissued fullyissued_alloc, bsa.cancelled, bsa.allocatedquantity allocatedquantity_alloc, bsa.allocatedunits, bsa.issuedquantity issuedquantity_alloc, 
		bsa.createdDate allocation_date
	from 
			Landing.mend.wh_batchstockallocation_aud bsa 
		left join -- without orderlinestockallocationtransactionid when cancel
			Landing.mend.wh_batchtransaction_orderlinestockallocationtransaction_aud btsat on bsa.id = btsat.batchstockallocationid
		inner join 
			Landing.mend.wh_batchtransaction_warehousestockitembatch_aud btwsib on bsa.id = btwsib.batchstockallocationid
		inner join -- on left 30k more but look like wrong records
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on btwsib.warehousestockitembatchid = wsib.warehousestockitembatchid
go



drop view mend.gen_wh_warehousestockitembatch_issue_v 
go 

create view mend.gen_wh_warehousestockitembatch_issue_v as
	select wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, 
		bsibsa.batchstockallocationid, bsi.id batchstockissueid,
		wsib.magentoProductID_int, wsib.magentoProductID,
		wsib.name, wsib.stockitemdescription, -- wsib.magentoSKU, wsib.SKU_product, wsib.packSize, wsib.SKU, 
		-- wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
		wsib.code, wsib.warehouse_name, wsib.id_string, 
		-- wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
		-- wsib.outstandingallocation, wsib.onhold,
		-- wsib.stockingmethod, 
		wsib.batch_id,
		-- wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
		-- wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
		-- wsib.confirmedDate, 
		-- wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency, 
		bsi.issueid, bsi.issuedquantity issuedquantity_issue, bsi.createddate issue_date 
	from
			Landing.mend.wh_batchstockissue_aud bsi
		inner join
			Landing.mend.wh_batchstockissue_warehousestockitembatch_aud bsiwsib on bsi.id = bsiwsib.batchstockissueid 
		inner join
			Landing.mend.gen_wh_warehousestockitembatch_v wsib on bsiwsib.warehousestockitembatchid = wsib.warehousestockitembatchid 
		inner join
			Landing.mend.wh_batchstockissue_batchstockallocation_aud bsibsa on bsi.id = bsibsa.batchstockissueid 
go

--create view mend.gen_wh_warehousestockitembatch_issue_v as
--	select wsib.productfamilyid, wsib.productid, wsib.stockitemid, wsib.warehousestockitemid, wsib.warehousestockitembatchid, bsi.id batchstockissueid,
--		wsib.magentoProductID_int,
--		wsib.magentoSKU, wsib.name, wsib.SKU_product, wsib.packSize, wsib.SKU, 
--		wsib.bc, wsib.di, wsib.po, wsib.cy, wsib.ax, wsib.ad, wsib._do, wsib.co, wsib.co_EDI, 
--		wsib.code, wsib.warehouse_name, wsib.id_string, 
--		wsib.availableqty, wsib.actualstockqty, wsib.allocatedstockqty, wsib.forwarddemand, wsib.dueinqty, wsib.stocked, 
--		wsib.outstandingallocation, wsib.onhold,
--		wsib.stockingmethod, 
--		wsib.batch_id,
--		wsib.fullyallocated, wsib.fullyIssued, wsib.status, wsib.arriveddate,  
--		wsib.receivedquantity, wsib.allocatedquantity, wsib.issuedquantity, wsib.onHoldQuantity, wsib.disposedQuantity, 
--		wsib.confirmedDate, 
--		wsib.productUnitCost, wsib.totalUnitCost, wsib.exchangeRate, currency, 
--		bsi.issueid, bsi.createddate, bsi.issuedquantity issuedquantity_issue 
--	from 
--			Landing.mend.gen_wh_warehousestockitembatch_v wsib
--		left join
--			Landing.mend.wh_batchstockissue_warehousestockitembatch_aud bsiwsib on wsib.warehousestockitembatchid = bsiwsib.warehousestockitembatchid
--		left join
--			Landing.mend.wh_batchstockissue_aud bsi on bsiwsib.batchstockissueid = bsi.id
--go



--drop view mend.gen_wh_warehousestockitembatch_issue_alloc_v 
--go 

--create view mend.gen_wh_warehousestockitembatch_issue_alloc_v as
--	select wsibi.productfamilyid, wsibi.productid, wsibi.stockitemid, wsibi.warehousestockitemid, wsibi.warehousestockitembatchid, wsibi.batchstockissueid, bsa.id batchstockallocationid,
--		wsibi.magentoProductID_int,
--		wsibi.magentoSKU, wsibi.name, wsibi.SKU_product, wsibi.packSize, wsibi.SKU, 
--		wsibi.bc, wsibi.di, wsibi.po, wsibi.cy, wsibi.ax, wsibi.ad, wsibi._do, wsibi.co, wsibi.co_EDI, 
--		wsibi.code, wsibi.warehouse_name, wsibi.id_string, 
--		wsibi.availableqty, wsibi.actualstockqty, wsibi.allocatedstockqty, wsibi.forwarddemand, wsibi.dueinqty, wsibi.stocked, 
--		wsibi.outstandingallocation, wsibi.onhold,
--		wsibi.stockingmethod, 
--		wsibi.batch_id,
--		wsibi.fullyallocated, wsibi.fullyIssued, wsibi.status, wsibi.arriveddate,  
--		wsibi.receivedquantity, wsibi.allocatedquantity, wsibi.issuedquantity, wsibi.onHoldQuantity, wsibi.disposedQuantity, 
--		wsibi.confirmedDate, 
--		wsibi.productUnitCost, wsibi.totalUnitCost, wsibi.exchangeRate, wsibi.currency, 
--		wsibi.issueid, wsibi.createddate, wsibi.issuedquantity_issue, 
--		bsa.fullyissued fullyissued_alloc, bsa.cancelled, bsa.allocatedquantity allocatedquantity_alloc, bsa.allocatedunits, bsa.issuedquantity issuedquantity_alloc 
--	from 
--			Landing.mend.gen_wh_warehousestockitembatch_issue_v wsibi
--		full join
--			Landing.mend.wh_batchstockissue_batchstockallocation_aud bsibsa on wsibi.batchstockissueid = bsibsa.batchstockissueid 
--		full join
--			Landing.mend.wh_batchstockallocation_aud bsa on bsibsa.batchstockallocationid = bsa.id
--go




--------------- PURCHASE ORDERS RELATED -----------------------------

drop view mend.gen_purc_purchaseorder_v 
go 

create view mend.gen_purc_purchaseorder_v  as
	select po.id purchaseorderid, w.warehouseid, s.supplier_id, 
		w.code, w.name warehouse_name, 
		s.supplierID, s.supplierName,
		po.ordernumber, po.source, po.status, po.potype,
		po.leadtime, 
		po.itemcost, po.totalprice, po.formattedprice, 
		po.createddate, po.duedate, po.receiveddate, 
		po.approveddate, po.approvedby, po.confirmeddate, po.confirmedby, po.submitteddate, po.submittedby, po.completeddate, po.completedby
	from 
			mend.purc_purchaseorder po
		inner join
			mend.purc_purchaseorder_warehouse pow on po.id = pow.purchaseorderid
		inner join	
			Landing.mend.gen_wh_warehouse_v w on pow.warehouseid = w.warehouseid
		left join
			Landing.mend.purc_purchaseorder_supplier pos on po.id = pos.purchaseorderid
			-- Landing.mend.purc_purchaseorder_supplier_aud pos on po.id = pos.purchaseorderid
		left join
			Landing.mend.gen_supp_supplier_v s on pos.supplierid = s.supplier_id
go


drop view mend.gen_purc_purchaseorderlineheader_v 
go 

create view mend.gen_purc_purchaseorderlineheader_v  as
	select id purchaseorderlineheaderid, po.purchaseorderid, po.warehouseid, po.supplier_id, polhpf.productfamilyid, polhps.packsizeid, polhsp.supplierpriceid,
		po.code, po.warehouse_name, 
		po.supplierID, po.supplierName,
		po.ordernumber, po.source, po.status, po.potype, po.createddate,
		po.leadtime, 
		po.itemcost, po.totalprice, 
		polh.status status_lh, polh.problems, 
		polh.lines, polh.items, polh.totalprice total_price_lh
	from 
			Landing.mend.gen_purc_purchaseorder_v po 
		inner join -- some po without polhpo
			Landing.mend.purc_purchaseorderlineheader_purchaseorder polhpo on po.purchaseorderid = polhpo.purchaseorderid
		inner join
			Landing.mend.purc_purchaseorderlineheader polh on polhpo.purchaseorderlineheaderid = polh.id 
		inner join
			 Landing.mend.purc_purchaseorderlineheader_productfamily polhpf on polh.id = polhpf.purchaseorderlineheaderid
		inner join
			 Landing.mend.purc_purchaseorderlineheader_packsize polhps on polh.id = polhps.purchaseorderlineheaderid
		left join	-- inner ??	 
			 Landing.mend.purc_purchaseorderlineheader_supplierprices polhsp on polh.id = polhsp.purchaseorderlineheaderid
go


drop view mend.gen_purc_purchaseorderline_v 
go 

create view mend.gen_purc_purchaseorderline_v  as
	select id purchaseorderlineid, polh.purchaseorderlineheaderid, polh.purchaseorderid, polh.warehouseid, polh.supplier_id, polh.productfamilyid, polh.packsizeid, polh.supplierpriceid, polsi.stockitemid,
		polh.code, polh.warehouse_name, 
		polh.supplierID, polh.supplierName,
		polh.ordernumber, polh.source, polh.status, polh.potype, polh.createddate,
		polh.leadtime, 
		polh.itemcost, polh.totalprice, 
		polh.status_lh, polh.problems, 
		polh.lines, polh.items, polh.total_price_lh,
		pol.po_lineID, 
		pol.quantity, pol.quantityReceived, pol.quantityDelivered, pol.quantityInTransit, pol.quantityPlanned, pol.quantityOpen, pol.quantityUnallocated, pol.quantityCancelled, pol.quantityExWorks, pol.quantityRejected,
		pol.lineprice, 
		pol.backtobackduedate
	from 
			Landing.mend.gen_purc_purchaseorderlineheader_v polh 
		inner join -- could be FULL
			Landing.mend.purc_purchaseorderline_purchaseorderlineheader polpolh on polh.purchaseorderlineheaderid = polpolh.purchaseorderlineheaderid 
		inner join	-- could be FULL
			Landing.mend.purc_purchaseorderline pol on polpolh.purchaseorderlineid = pol.id 
		left join	-- inner ??
			Landing.mend.purc_purchaseorderline_stockitem polsi on pol.id = polsi.purchaseorderlineid
go


--------------- SHORTAGE RELATED -----------------------------

drop view mend.gen_short_shortageheader_v 
go 

create view mend.gen_short_shortageheader_v  as
	select shh.id shortageheaderid, s.supplier_id, shhws.warehouseid warehouseid_src, shhwd.warehouseid warehouse_dest, shhps.packsizeid,
		shh.wholesale, 
		ws.code code_src, ws.name warehouse_name_src, wd.code code_dest, wd.name warehouse_name_dest, s.supplierID, s.supplierName
	from 
			Landing.mend.short_shortageheader shh
		inner join
			Landing.mend.short_shortageheader_sourcewarehouse shhws on shh.id = shhws.shortageheaderid
		inner join
			Landing.mend.gen_wh_warehouse_v ws on shhws.warehouseid = ws.warehouseid
		left join
			Landing.mend.short_shortageheader_destinationwarehouse shhwd on shh.id = shhwd.shortageheaderid
		left join
			Landing.mend.gen_wh_warehouse_v wd on shhwd.warehouseid = wd.warehouseid
		inner join
			Landing.mend.short_shortageheader_supplier shhs on shh.id = shhs.shortageheaderid
		inner join
			Landing.mend.gen_supp_supplier_v s on shhs.supplierid = s.supplier_id
		left join
			Landing.mend.short_shortageheader_packsize shhps on shh.id = shhps.shortageheaderid
go

drop view mend.gen_short_shortage_v 
go 

create view mend.gen_short_shortage_v  as			
	select sh.id shortage_id, shh.shortageheaderid, shh.supplier_id, shh.warehouseid_src, shh.warehouse_dest, shh.packsizeid, 
		shpol.purchaseorderlineid, shp.productid, shsi.stockitemid, shwsi.warehousestockitemid, -- shsp.standardpriceid, 
		shh.wholesale, 
		shh.code_src, shh.warehouse_name_src, shh.code_dest, shh.warehouse_name_dest, shh.supplierID, shh.supplierName,
		sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
		-- sh.wholesale, 
		sh.status, 
		sh.unitquantity, sh.packquantity, 
		sh.requireddate, 
		sh.createdDate
	from 
			Landing.mend.gen_short_shortageheader_v shh 
		left join
			Landing.mend.short_shortage_shortageheader shshh on shh.shortageheaderid = shshh.shortageheaderid
		left join
			Landing.mend.short_shortage sh on shshh.shortageid = sh.id 
		left join
			Landing.mend.short_shortage_purchaseorderline shpol on sh.id = shpol.shortageid
		left join
			Landing.mend.short_shortage_product shp on sh.id = shp.shortageid
		left join	
			Landing.mend.short_shortage_stockitem shsi on sh.id = shsi.shortageid
		left join	
			Landing.mend.short_shortage_warehousestockitem shwsi on sh.id = shwsi.shortageid
		-- left join	
			-- Landing.mend.short_shortage_standardprice_aud shsp on sh.id = shsp.shortageid
go
	

drop view mend.gen_short_orderlineshortage_v 
go 

create view mend.gen_short_orderlineshortage_v  as		

	select olsh.id orderlineshortageid, sh.shortage_id, sh.shortageheaderid, sh.supplier_id, sh.warehouseid_src, sh.warehouse_dest, sh.packsizeid,
		sh.purchaseorderlineid, sh.productid, sh.stockitemid, sh.warehousestockitemid, olshol.orderlineid, -- sh.standardpriceid, 
		sh.wholesale, 
		sh.code_src, sh.warehouse_name_src, sh.code_dest, sh.warehouse_name_dest, sh.supplierID, sh.supplierName, 
		sh.shortageid, sh.purchaseordernumber, sh.ordernumbers,
		sh.status, 
		sh.unitquantity, sh.packquantity, 
		sh.requireddate, 
		sh.createdDate,
		olsh.orderlineshortageid orderlineshortage_id,
		olsh.orderlineshortagetype,
		olsh.unitquantity unitquantity_ol, olsh.packquantity packquantity_ol,
		olsh.shortageprogress, olsh.adviseddate, 
		olsh.createdDate createdDate_ol
	from 
			Landing.mend.gen_short_shortage_v sh
		left join
			Landing.mend.short_orderlineshortage_shortage olshsh on sh.shortage_id = olshsh.shortageid 
		left join
			Landing.mend.short_orderlineshortage olsh on olshsh.orderlineshortageid = olsh.id
		left join
			Landing.mend.short_orderlineshortage_orderline olshol on olsh.id = olshol.orderlineshortageid
go



--------------- WAREHOUSE SHIPMENT RELATED -----------------------------

drop view mend.gen_whship_receipt_v 
go 

create view mend.gen_whship_receipt_v as
	select whs.id receiptid, w.warehouseid, s.supplier_id, -- wsib_whs.warehousestockitembatchid,
		whs.shipmentID shipment_ID, whs.receiptID receipt_ID, whs.receiptNumber, whs.orderNumber, whs.invoiceRef,
		w.code, w.name warehouse_name, s.supplierID, s.supplierName,
		whs.status, whs.shipmentType, 
		whs.dueDate, whs.arrivedDate, whs.confirmedDate, whs.stockRegisteredDate, 
		whs.totalItemPrice, whs.interCompanyCarriagePrice, whs.inboundCarriagePrice, whs.interCompanyProfit, whs.duty,
		whs.lock,
		whs.auditComment,
		whs.createdDate
	from 
			Landing.mend.whship_receipt whs
		inner join
			Landing.mend.whship_receipt_warehouse whsw on whs.id = whsw.receiptid
		inner join
			Landing.mend.gen_wh_warehouse_v w on whsw.warehouseid = w.warehouseid
		inner join
			Landing.mend.whship_receipt_supplier whss on whs.id = whss.receiptid
		inner join
			Landing.mend.gen_supp_supplier_v s on whss.supplierid = s.supplier_id
		-- inner join
			-- Landing.mend.whship_warehousestockitembatch_shipment_aud wsib_whs on whs.id = wsib_whs.shipmentid
go


drop view mend.gen_whship_receiptlineheader_v 
go 

create view mend.gen_whship_receiptlineheader_v as
	select rlh.id receiptlineheaderid, r.receiptid, r.warehouseid, r.supplier_id, rlhpf.productfamilyid, rlhps.packsizeid,
		r.shipment_ID, r.receipt_ID, r.receiptNumber, r.orderNumber, 
		r.code, r.warehouse_name, r.supplierID, r.supplierName,
		r.status, r.shipmentType, r.totalItemPrice, 
		r.createdDate,
		rlh.totalquantityordered, rlh.totalquantityaccepted, 
		rlh.unitcost, rlh.totalcostordered, rlh.totalcostaccepted
	from 
			Landing.mend.gen_whship_receipt_v r
		left join -- right - inner
			Landing.mend.whship_receiptlineheader_receipt rlhr on r.receiptid = rlhr.receiptid
		left join -- right - inner
			Landing.mend.whship_receiptlineheader rlh on rlhr.receiptlineheaderid = rlh.id 
		left join
			Landing.mend.whship_receiptlineheader_productfamily rlhpf on rlh.id = rlhpf.receiptlineheaderid
		left join
			Landing.mend.whship_receiptlineheader_packsize rlhps on rlh.id = rlhps.receiptlineheaderid
go


drop view mend.gen_whship_receiptline_1_v 
go 

create view mend.gen_whship_receiptline_1_v as
	select rl.id receiptlineid, r.receiptid, r.warehouseid, r.supplier_id, rlpol.purchaseorderlineid, rlwsib.warehousestockitembatchid,
		r.shipment_ID, r.receipt_ID, r.receiptNumber, r.orderNumber, 
		r.code, r.warehouse_name, r.supplierID, r.supplierName,
		r.status, r.shipmentType, r.totalItemPrice, 
		r.createdDate, 
		rl.line, rl.stockitem, rl.status status_rl, rl.syncstatus, rl.syncresolution,
		rl.unitCost, rl.quantityordered, rl.quantityreceived, rl.quantityaccepted, rl.quantityreturned, rl.quantityrejected, 
		rl.created
	from	
			Landing.mend.gen_whship_receipt_v r
		inner join
			Landing.mend.whship_receiptline_receipt rlr on r.receiptid = rlr.receiptid
		inner join
			Landing.mend.whship_receiptline rl on rlr.receiptlineid = rl.id
		left join
			Landing.mend.whship_receiptline_purchaseorderline rlpol on rl.id = rlpol.receiptlineid
		left join
			Landing.mend.whship_warehousestockitembatch_receiptline rlwsib on rl.id = rlwsib.receiptlineid
go

drop view mend.gen_whship_receiptline_2_v 
go 

create view mend.gen_whship_receiptline_2_v as
	select rl.id receiptlineid, rlh.receiptlineheaderid, rlh.receiptid, rlh.warehouseid, rlh.supplier_id, rlh.productfamilyid, rlh.packsizeid,
		rlpol.purchaseorderlineid, rlwsib.warehousestockitembatchid,
		rlh.shipment_ID, rlh.receipt_ID, rlh.receiptNumber, rlh.orderNumber, 
		rlh.code, rlh.warehouse_name, rlh.supplierID, rlh.supplierName,
		rlh.status, rlh.shipmentType, rlh.totalItemPrice, 
		rlh.createdDate, 
		rl.line, rl.stockitem, rl.status status_rl, rl.syncstatus, rl.syncresolution,
		rl.unitCost, rl.quantityordered, rl.quantityreceived, rl.quantityaccepted, rl.quantityreturned, rl.quantityrejected, 
		rl.created
	from 
			Landing.mend.gen_whship_receiptlineheader_v rlh 
		inner join
			Landing.mend.whship_receiptline_receiptlineheader rlr on rlh.receiptlineheaderid = rlr.receiptlineheaderid
		inner join
			Landing.mend.whship_receiptline rl on rlr.receiptlineid = rl.id
		left join
			Landing.mend.whship_receiptline_purchaseorderline rlpol on rl.id = rlpol.receiptlineid
		left join
			Landing.mend.whship_warehousestockitembatch_receiptline rlwsib on rl.id = rlwsib.receiptlineid
go


--------------- INTERSITE TRANSFERS RELATED -----------------------------

drop view mend.gen_inter_transferheader_v 
go 

create view mend.gen_inter_transferheader_v as

	select th.id transferheaderid, thpo_s.purchaseorderid purchaseorderid_s, thpo_t.purchaseorderid purchaseorderid_t, oth.orderid,
		th.transfernum, th.allocatedstrategy, 
		th.totalremaining,
		th.createddate, -- th.changeddate		 
		po_t.code code_t, po_t.warehouse_name warehouse_name_t, po_t.supplierID supplierID_t, po_t.supplierName supplierName_t, po_t.ordernumber ordernumber_t, 
		po_s.code code_s, po_s.warehouse_name warehouse_name_s, po_s.supplierID supplierID_s, po_s.supplierName supplierName_s, po_s.ordernumber ordernumber_s
	from 
			Landing.mend.inter_transferheader th
		left join
			Landing.mend.inter_supplypo_transferheader thpo_s on th.id = thpo_s.transferheaderid
		left join
			Landing.mend.gen_purc_purchaseorder_v po_s on thpo_s.purchaseorderid = po_s.purchaseorderid
		left join
			Landing.mend.inter_transferpo_transferheader thpo_t on th.id = thpo_t.transferheaderid
		left join
			Landing.mend.gen_purc_purchaseorder_v po_t on thpo_t.purchaseorderid = po_t.purchaseorderid
		left join
			Landing.mend.inter_order_transferheader oth on th.id = oth.transferheaderid
go


drop view mend.gen_inter_intransitbatchheader_v 
go 

create view mend.gen_inter_intransitbatchheader_v as

	select ibh.id intransitbatchheaderid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, ibhcs.customershipmentid, -- cs.orderid, 
		th.transfernum, th.allocatedstrategy, th.totalremaining, th.createddate, th.ordernumber_t, th.ordernumber_s, 
		ibh.createddate createddate_ibh
		-- , cs.magentoOrderID_int, cs.incrementID
	from 
			Landing.mend.gen_inter_transferheader_v th 
		left join
			Landing.mend.inter_intransitbatchheader_transferheader ibhth on th.transferheaderid = ibhth.transferheaderid
		left join
			Landing.mend.inter_intransitbatchheader ibh on ibhth.intransitbatchheaderid = ibh.id 
		left join
			Landing.mend.inter_intransitbatchheader_customershipment ibhcs on ibh.id = ibhcs.intransitbatchheaderid
		-- left join
			-- Landing.mend.gen_ship_customershipment_v cs on ibhcs.customershipmentid = cs.customershipmentid
go


drop view mend.gen_inter_intransitstockitembatch_th_v 
go 

create view mend.gen_inter_intransitstockitembatch_th_v as
	select id intransitstockitembatchid, th.transferheaderid, th.purchaseorderid_s, th.purchaseorderid_t, isibsi.stockitemid, isibwsh.shipmentid,
		-- ibh.intransitbatchheaderid, ibh.customershipmentid, 
		th.transfernum, th.allocatedstrategy, th.totalremaining, th.createddate, th.ordernumber_s, th.ordernumber_t, 
		-- ibh.createddate_ibh, ibh.magentoOrderID_int, ibh.incrementID,
		isib.issuedquantity, isib.remainingquantity, 
		isib.used,
		isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
		isib.currency,
		isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
		isib.createddate createddate_isib
	from
			Landing.mend.gen_inter_transferheader_v th 
		left join
			Landing.mend.inter_intransitstockitembatch_transferheader_aud isibth on th.transferheaderid = isibth.transferheaderid
		left join
			Landing.mend.inter_intransitstockitembatch_aud isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem_aud isibsi on isib.id = isibsi.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitstockitembatch_shipment_aud isibwsh on isib.id = isibwsh.intransitstockitembatchid
go


drop view mend.gen_inter_intransitstockitembatch_ibh_v 
go 

create view mend.gen_inter_intransitstockitembatch_ibh_v as
	select id intransitstockitembatchid, ibh.intransitbatchheaderid, ibh.transferheaderid, ibh.purchaseorderid_s, ibh.purchaseorderid_t, ibh.customershipmentid, 
		isibsi.stockitemid, isibwsh.receiptid,
		ibh.transfernum, ibh.allocatedstrategy, ibh.totalremaining, ibh.createddate, ibh.ordernumber_s, ibh.ordernumber_t, 
		ibh.createddate_ibh, -- ibh.magentoOrderID_int, ibh.incrementID,
		isib.issuedquantity, isib.remainingquantity, 
		isib.used,
		isib.productunitcost, isib.carriageUnitCost, isib.totalunitcost, isib.totalUnitCostIncInterCo,
		isib.currency,
		isib.groupFIFOdate, isib.arriveddate, isib.confirmeddate, isib.stockregistereddate, 
		isib.createddate createddate_isib
	from
			Landing.mend.gen_inter_intransitbatchheader_v ibh 
		left join
			Landing.mend.inter_intransitstockitembatch_intransitbatchheader isibth on ibh.intransitbatchheaderid = isibth.intransitbatchheaderid
		left join
			Landing.mend.inter_intransitstockitembatch isib on isibth.intransitstockitembatchid	= isib.id 
		left join
			Landing.mend.inter_intransitstockitembatch_stockitem isibsi on isib.id = isibsi.intransitstockitembatchid
		left join
			Landing.mend.inter_intransitstockitembatch_receipt isibwsh on isib.id = isibwsh.intransitstockitembatchid
go

--------------- SNAP RECEIPT RELATED -----------------------------

drop view mend.gen_rec_snapreceipt_v 
go 

create view mend.gen_rec_snapreceipt_v as

	select sr.id snapreceiptid, srsh.shipmentid,
		sr.receiptid, 
		sr.lines, sr.lineqty, 	
		sr.receiptStatus, sr.receiptprocessStatus, sr.processDetail,  sr.priority, 
		sr.status, sr.stockstatus, 
		sr.ordertype, sr.orderclass, 
		sr.suppliername, sr.consignmentID, 
		sr.weight, sr.actualWeight, sr.volume, 
		sr.dateDueIn, sr.dateReceipt, sr.dateCreated, sr.dateArrival, sr.dateClosed,  
		sr.datesfixed
	from 
			Landing.mend.rec_snapreceipt_aud sr
		left join
			Landing.mend.rec_snapreceipt_shipment_aud srsh on sr.id = srsh.snapreceiptid
go


drop view mend.gen_rec_snapreceiptline_v 
go 

create view mend.gen_rec_snapreceiptline_v as

	select srl.id snapreceiptlineid, sr.snapreceiptid, sr.shipmentid, srlsi.stockitemid,
		sr.receiptid, sr.lines, sr.lineqty, 	
		sr.receiptStatus, sr.receiptprocessStatus, sr.processDetail, sr.ordertype, sr.orderclass, sr.dateCreated,
		srl.line, srl.skuid, 
		srl.status, srl.level, srl.unitofmeasure, 
		srl.qtyOrdered, srl.qtyAdvised, srl.qtyReceived, srl.qtyDueIn, srl.qtyRejected
	from 
			Landing.mend.gen_rec_snapreceipt_v sr 
		full join
			Landing.mend.rec_snapreceiptline_snapreceipt_aud srlsr on sr.snapreceiptid = srlsr.snapreceiptid 
		full join
			Landing.mend.rec_snapreceiptline_aud srl on srlsr.snapreceiptlineid = srl.id
		left join
			Landing.mend.rec_snapreceiptline_stockitem_aud srlsi on srl.id = srlsi.snapreceiptlineid
go



--------------- VAT RELATED -----------------------------

drop view mend.gen_vat_commodity_v 
go 

create view mend.gen_vat_commodity_v as

	select c.id commodityid, cpf.productfamilyid,
		c.commoditycode, c.commodityname, 
		case when (isnumeric(pf.magentoProductID) = 1) then convert(int, pf.magentoProductID) else cpf.productfamilyid end magentoProductID_int,
		pf.magentoProductID, pf.magentoSKU, pf.name
	from 
			Landing.mend.vat_commodity_aud c
		inner join
			Landing.mend.vat_productfamily_commodity_aud cpf on c.id = cpf.commodityid
		inner join
			Landing.mend.prod_productfamily_aud pf on cpf.productfamilyid = pf.id
go


drop view mend.gen_vat_dispensingservicesrate_v 
go 

create view mend.gen_vat_dispensingservicesrate_v as

	select dsr.id dispensingservicesrateid, c.commodityid, c.productfamilyid, dsrco.countryid,
		c.commoditycode, c.commodityname, dsr.dispensingservicesrate, dsr.rateeffectivedate, dsr.rateenddate,
		c.magentoProductID_int, c.magentoProductID, c.magentoSKU, c.name
	from 
			Landing.mend.vat_dispensingservicesrate_aud dsr
		inner join
			Landing.mend.vat_dispensingservices_commodity_aud dsrc on dsr.id = dsrc.dispensingservicesrateid
		full join
			Landing.mend.gen_vat_commodity_v c on dsrc.commodityid = c.commodityid
		left join
			Landing.mend.vat_dispensingservicesrate_country_aud dsrco on dsr.id = dsrco.dispensingservicesrateid
go



drop view mend.gen_vat_vatrating_v 
go 

create view mend.gen_vat_vatrating_v as

	select vrat.id vatratingid, c.id countryid, vr.id vatrateid,
		vrat.vatratingcode, vrat.vatratingdescription, 
		c.countryname, 
		vr.vatrate, vr.vatrateeffectivedate, vr.vatrateenddate
	from 
			Landing.mend.vat_vatrating_aud vrat
		inner join
			Landing.mend.vat_vatrating_country_aud vratc on vrat.id = vratc.vatratingid
		inner join
			Landing.mend.comp_country_aud c on vratc.countryid = c.id
		inner join
			Landing.mend.vat_vatrate_vatrating_aud vratvr on vrat.id = vratvr.vatratingid
		inner join
			Landing.mend.vat_vatrate_aud vr on vratvr.vatrateid = vr.id
go


drop view mend.gen_vat_vatschedule_v 
go 

create view mend.gen_vat_vatschedule_v as

	select vs.id vatscheduleid, vrat.vatratingid, vrat.countryid, vrat.vatrateid, c.id commodityid,
		vrat.vatratingcode, vrat.vatratingdescription, vrat.countryname, vrat.vatrate, vrat.vatrateeffectivedate, vrat.vatrateenddate,
		c.commoditycode, c.commodityname,
		vs.vatsaletype, vs.taxationarea, vs.scheduleeffectivedate, vs.scheduleenddate
	from 
			Landing.mend.vat_vatschedule_aud vs
		inner join
			Landing.mend.vat_vatschedule_vatrating_aud vsvrat on vs.id = vsvrat.vatscheduleid
		inner join
			Landing.mend.gen_vat_vatrating_v vrat on vsvrat.vatratingid = vrat.vatratingid
		inner join
			Landing.mend.vat_vatschedule_commodity_aud vsc on vs.id = vsc.vatscheduleid
		inner join
			Landing.mend.vat_commodity c on vsc.commodityid = c.id
go



