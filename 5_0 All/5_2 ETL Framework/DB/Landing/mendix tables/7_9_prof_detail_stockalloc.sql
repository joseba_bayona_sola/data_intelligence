use Landing
go 

------------------------------------------------------------------------
--------- stockallocation$orderlinestockallocationtransaction ----------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_orderlinestockallocationtransaction_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.all_orderlinestockallocationtransaction_aud

	select idETLBatchRun, count(*) num_rows
	from mend.all_orderlinestockallocationtransaction_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.all_orderlinestockallocationtransaction_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			transactionID, timestamp,
			allocationType, recordType, 
			allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
			stockAllocated, cancelled, 
			issuedDateTime, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.all_orderlinestockallocationtransaction_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom
	
------------------------------------------------------------------------
---------------------- stockallocation$at_order ------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_at_order_aud
	group by idETLBatchRun
	order by idETLBatchRun

	
------------------------------------------------------------------------
---------------------- stockallocation$at_orderline --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_at_orderline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	
------------------------------------------------------------------------
---------------------- stockallocation$at_warehouse --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_at_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

	
------------------------------------------------------------------------
------------------ stockallocation$at_warehousestockitem ---------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_at_warehousestockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun

	
------------------------------------------------------------------------
--------------------- stockallocation$at_packsize ----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_at_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun

	
------------------------------------------------------------------------------------------------------------
--------------------- stockallocatio$orderlinestockallocationtransa_customershipmentl ----------------------
------------------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_orderlinestockallocationtransa_customershipmentl_aud
	group by idETLBatchRun
	order by idETLBatchRun
	




------------------------------------------------------------------------
----------------- stockallocation$magentoallocation --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_magentoallocation_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.all_magentoallocation_aud

	select idETLBatchRun, count(*) num_rows
	from mend.all_magentoallocation_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.all_magentoallocation_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			allocationFull, shipped, cancelled, 
			quantity, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.all_magentoallocation_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom
	
------------------------------------------------------------------------------------------------------
----------------- stockallocatio$magentoallocatio_orderlinestockallocationtransac --------------------
------------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_magentoallocatio_orderlinestockallocationtransac_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------------------------------------
----------------- stockallocation$magentoallocation_orderlinemagento --------------------
------------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_magentoallocation_orderlinemagento_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------------------------------------
----------------- stockallocation$magentoallocation_warehouse --------------------
------------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.all_magentoallocation_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun
