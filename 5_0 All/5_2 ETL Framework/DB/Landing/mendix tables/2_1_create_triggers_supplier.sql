------------------------------- Triggers ----------------------------------

use Landing
go

------------------------------- Triggers ----------------------------------

--------------------------- suppliers$supplier ----------------------------

drop trigger mend.trg_supp_supplier;
go 

create trigger mend.trg_supp_supplier on mend.supp_supplier
after insert
as
begin
	set nocount on

	merge into mend.supp_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.supplierID, 0), isnull(trg.supplierincrementid, 0), isnull(trg.supplierName, ' '), isnull(trg.currencyCode, ' '), isnull(trg.supplierType, ' '), -- isnull(trg.isEDIEnabled, 0), 
			isnull(trg.vatCode, ' '), isnull(trg.defaultLeadTime, 0), 
			isnull(trg.email, ' '), isnull(trg.telephone, ' '), isnull(trg.fax, ' '), 
			-- isnull(trg.flatFileConfigurationToUse, ' '), 
			isnull(trg.street1, ' '), isnull(trg.street2, ' '), isnull(trg.street3, ' '), 
			isnull(trg.city, ' '), isnull(trg.region, ' '), isnull(trg.postcode, ' '),
			isnull(trg.vendorCode, ' '), isnull(trg.vendorShortName, ' '), isnull(trg.vendorStoreNumber, ' '), 
			isnull(trg.defaulttransitleadtime, 0)
		intersect
		select 
			isnull(src.supplierID, 0), isnull(src.supplierincrementid, 0), isnull(src.supplierName, ' '), isnull(src.currencyCode, ' '), isnull(src.supplierType, ' '), -- isnull(src.isEDIEnabled, 0), 
			isnull(src.vatCode, ' '), isnull(src.defaultLeadTime, 0), 
			isnull(src.email, ' '), isnull(src.telephone, ' '), isnull(src.fax, ' '), 
			-- isnull(src.flatFileConfigurationToUse, ' '), 
			isnull(src.street1, ' '), isnull(src.street2, ' '), isnull(src.street3, ' '), 
			isnull(src.city, ' '), isnull(src.region, ' '), isnull(src.postcode, ' '),
			isnull(src.vendorCode, ' '), isnull(src.vendorShortName, ' '), isnull(src.vendorStoreNumber, ' '), 
			isnull(src.defaulttransitleadtime, 0))
			
		then
			update set
				trg.supplierID = src.supplierID, trg.supplierincrementid = src.supplierincrementid, trg.supplierName = src.supplierName, trg.currencyCode = src.currencyCode, 
				trg.supplierType = src.supplierType, -- trg.isEDIEnabled = src.isEDIEnabled, 
				trg.vatCode = src.vatCode, trg.defaultLeadTime = src.defaultLeadTime, 
				trg.email = src.email, trg.telephone = src.telephone, trg.fax = src.fax, 
				-- trg.flatFileConfigurationToUse = src.flatFileConfigurationToUse, 
				trg.street1 = src.street1, trg.street2 = src.street2, trg.street3 = src.street3, 
				trg.city = src.city, trg.region = src.region, trg.postcode = src.postcode, 
				trg.vendorCode = src.vendorCode, trg.vendorShortName = src.vendorShortName, trg.vendorStoreNumber = src.vendorStoreNumber, 
				trg.defaulttransitleadtime = src.defaulttransitleadtime, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
				vatCode, defaultLeadTime, 
				email, telephone, fax,
				flatFileConfigurationToUse,
				street1, street2, street3, city, region, postcode,
				vendorCode, vendorShortName, vendorStoreNumber,
				defaulttransitleadtime,
				idETLBatchRun)

				values (src.id,
					src.supplierID, src.supplierincrementid, src.supplierName, src.currencyCode, src.supplierType, src.isEDIEnabled,
					src.vatCode, src.defaultLeadTime,
					src.email, src.telephone, src.fax,
					src.flatFileConfigurationToUse,
					src.street1, src.street2, src.street3, src.city, src.region, src.postcode,
					src.vendorCode, src.vendorShortName, src.vendorStoreNumber,
					src.defaulttransitleadtime,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_supp_supplier_aud;
go 

create trigger mend.trg_supp_supplier_aud on mend.supp_supplier_aud
for update, delete
as
begin
	set nocount on

	insert mend.supp_supplier_aud_hist (id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		vatCode, defaultLeadTime,
		email, telephone, fax,
		flatFileConfigurationToUse,
		street1, street2, street3, city, region, postcode,
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.supplierID, d.supplierincrementid, d.supplierName, d.currencyCode, d.supplierType, d.isEDIEnabled, 
			d.vatCode, d.defaultLeadTime, 
			d.email, d.telephone, d.fax, 
			d.flatFileConfigurationToUse,
			d.street1, d.street2, d.street3, 
			d.city, d.region, d.postcode, 
			d.vendorCode, d.vendorShortName, d.vendorStoreNumber,
			d.defaulttransitleadtime, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

----------------------- suppliers$supplier_country -------------------------

drop trigger mend.trg_supp_supplier_country;
go 

create trigger mend.trg_supp_supplier_country on mend.supp_supplier_country
after insert
as
begin
	set nocount on

	merge into mend.supp_supplier_country_aud with (tablock) as trg
	using inserted src
		on (trg.supplierid = src.supplierid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (supplierid, countryid, 
				idETLBatchRun)

				values (src.supplierid, src.countryid,
					src.idETLBatchRun);
end; 
go

------------------- suppliers$supplier_company -------------------------

drop trigger mend.trg_supp_supplier_company;
go 

create trigger mend.trg_supp_supplier_company on mend.supp_supplier_company
after insert
as
begin
	set nocount on

	merge into mend.supp_supplier_company_aud with (tablock) as trg
	using inserted src
		on (trg.supplierid = src.supplierid and trg.companyid = src.companyid)
	when not matched 
		then
			insert (supplierid, companyid, 
				idETLBatchRun)

				values (src.supplierid, src.companyid,
					src.idETLBatchRun);
end; 
go


------------------- suppliers$supplier_currency -------------------------

drop trigger mend.trg_supp_supplier_currency;
go 

create trigger mend.trg_supp_supplier_currency on mend.supp_supplier_currency
after insert
as
begin
	set nocount on

	merge into mend.supp_supplier_currency_aud with (tablock) as trg
	using inserted src
		on (trg.supplierid = src.supplierid and trg.currencyid = src.currencyid)
	when not matched 
		then
			insert (supplierid, currencyid, 
				idETLBatchRun)

				values (src.supplierid, src.currencyid,
					src.idETLBatchRun);
end; 
go




------------------- suppliers$supplierprice -------------------------

drop trigger mend.trg_supp_supplierprice;
go 

create trigger mend.trg_supp_supplierprice on mend.supp_supplierprice
after insert
as
begin
	set nocount on

	merge into mend.supp_supplierprice_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.subMetaObjectName, ' '), 
			isnull(trg.unitPrice, 0), isnull(trg.leadTime, 0), isnull(trg.totalWarehouseUnitPrice, 0), isnull(trg.orderMultiple, 0), 
			isnull(trg.directPrice, 0), isnull(trg.expiredPrice, 0), 
			isnull(trg.totallt, 0), isnull(trg.shiplt, 0), isnull(trg.transferlt, 0), 
			isnull(trg.comments, ' '), 
			isnull(trg.lastUpdated, ' '), 
			isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '), 
			isnull(trg.system$owner, 0), isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.subMetaObjectName, ' '), 
			isnull(src.unitPrice, 0), isnull(src.leadTime, 0), isnull(src.totalWarehouseUnitPrice, 0), isnull(src.orderMultiple, 0), 
			isnull(src.directPrice, 0), isnull(src.expiredPrice, 0), 
			isnull(src.totallt, 0), isnull(src.shiplt, 0), isnull(src.transferlt, 0), 
			isnull(src.comments, ' '), 
			isnull(src.lastUpdated, ' '), 
			isnull(src.createdDate, ' '), isnull(src.changedDate, ' '), 
			isnull(src.system$owner, 0), isnull(src.system$changedby, 0))
			
		then
			update set
				trg.subMetaObjectName = src.subMetaObjectName, 
				trg.unitPrice = src.unitPrice, trg.leadTime = src.leadTime, 
				trg.totalWarehouseUnitPrice = src.totalWarehouseUnitPrice, trg.orderMultiple = src.orderMultiple, 
				trg.directPrice = src.directPrice, trg.expiredPrice = src.expiredPrice, 
				trg.totallt = src.totallt, trg.shiplt = src.shiplt, trg.transferlt = src.transferlt, 
				trg.comments = src.comments, 
				trg.lastUpdated = src.lastUpdated, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate, 
				trg.system$owner = src.system$owner, trg.system$changedby = src.system$changedby, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				subMetaObjectName,
				unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
				directPrice, expiredPrice,
				totallt, shiplt, transferlt, 
				comments,
				lastUpdated,
				createdDate, changedDate,
				system$owner, system$changedby,
				idETLBatchRun)

				values (src.id,
					src.subMetaObjectName,
					src.unitPrice, src.leadTime, src.totalWarehouseUnitPrice, src.orderMultiple,
					src.directPrice, src.expiredPrice,
					src.totallt, src.shiplt, src.transferlt, 
					src.comments,
					src.lastUpdated,
					src.createdDate, src.changedDate,
					src.system$owner, src.system$changedby,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_supp_supplierprice_aud;
go 

create trigger mend.trg_supp_supplierprice_aud on mend.supp_supplierprice_aud
for update, delete
as
begin
	set nocount on

	insert mend.supp_supplierprice_aud_hist (id,
		subMetaObjectName,
		unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
		directPrice, expiredPrice,
		totallt, shiplt, transferlt, 
		comments,
		lastUpdated,
		createdDate, changedDate,
		system$owner, system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.subMetaObjectName,
			d.unitPrice, d.leadTime, d.totalWarehouseUnitPrice, d.orderMultiple,
			d.directPrice, d.expiredPrice,
			d.totallt, d.shiplt, d.transferlt, 
			d.comments,
			d.lastUpdated,
			d.createdDate, d.changedDate,
			d.system$owner, d.system$changedby,
			d.idETLBatchRun, 
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- suppliers$standardprice -------------------------

drop trigger mend.trg_supp_standardprice;
go 

create trigger mend.trg_supp_standardprice on mend.supp_standardprice
after insert
as
begin
	set nocount on

	merge into mend.supp_standardprice_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.moq, 0), isnull(trg.effectiveDate, ' '), isnull(trg.expiryDate, ' '), isnull(trg.maxqty, 0)
		intersect
		select 
			isnull(src.moq, 0), isnull(src.effectiveDate, ' '), isnull(src.expiryDate, ' '), isnull(src.maxqty, 0)) 	
		then
			update set
				trg.moq = src.moq, trg.effectiveDate = src.effectiveDate, trg.expiryDate = src.expiryDate, trg.maxqty = src.maxqty, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				moq, effectiveDate, expiryDate, maxqty,
				idETLBatchRun)

				values (src.id, 
					src.moq, src.effectiveDate, src.expiryDate, src.maxqty,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_supp_standardprice_aud;
go 

create trigger mend.trg_supp_standardprice_aud on mend.supp_standardprice_aud
for update, delete
as
begin
	set nocount on

	insert mend.supp_standardprice_aud_hist (id, 
		moq, effectiveDate, expiryDate, maxqty,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.moq, d.effectiveDate, d.expiryDate, d.maxqty,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- suppliers$supplierprices_supplier -------------------------

drop trigger mend.trg_supp_supplierprices_supplier;
go 

create trigger mend.trg_supp_supplierprices_supplier on mend.supp_supplierprices_supplier
after insert
as
begin
	set nocount on

	merge into mend.supp_supplierprices_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.supplierpriceid = src.supplierpriceid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (supplierpriceid, supplierid,
				idETLBatchRun)

				values (src.supplierpriceid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------------- suppliers$supplierprices_packsize -------------------------

drop trigger mend.trg_supp_supplierprices_packsize;
go 

create trigger mend.trg_supp_supplierprices_packsize on mend.supp_supplierprices_packsize
after insert
as
begin
	set nocount on

	merge into mend.supp_supplierprices_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.supplierpriceid = src.supplierpriceid and trg.packsizeid = src.packsizeid)
	when not matched 
		then
			insert (supplierpriceid, packsizeid,
				idETLBatchRun)

				values (src.supplierpriceid, src.packsizeid,
					src.idETLBatchRun);
end; 
go




--------------------------- suppliers$manufacturer ----------------------------

drop trigger mend.trg_supp_manufacturer;
go 

create trigger mend.trg_supp_manufacturer on mend.supp_manufacturer
after insert
as
begin
	set nocount on

	merge into mend.supp_manufacturer_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.manufacturerid, 0), isnull(trg.manufacturername, ' '), isnull(trg.createddate, ' '), isnull(trg.changeddate, ' ') 
		intersect
		select 
			isnull(src.manufacturerid, 0), isnull(src.manufacturername, ' '), isnull(src.createddate, ' '), isnull(src.changeddate, ' '))
			
		then
			update set
				trg.manufacturerid = src.manufacturerid, trg.manufacturername = src.manufacturername, trg.createddate = src.createddate, trg.changeddate = src.changeddate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, manufacturerid, manufacturername, createddate, changeddate, 				
				idETLBatchRun)

				values (src.id, src.manufacturerid, src.manufacturername, src.createddate, src.changeddate, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_supp_manufacturer_aud;
go 

create trigger mend.trg_supp_manufacturer_aud on mend.supp_manufacturer_aud
for update, delete
as
begin
	set nocount on

	insert mend.supp_manufacturer_aud_hist (id, manufacturerid, manufacturername, createddate, changeddate, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.manufacturerid, d.manufacturername, d.createddate, d.changeddate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go



------------------- product$packsize_manufacturer -------------------------

drop trigger mend.trg_supp_packsize_manufacturer;
go 

create trigger mend.trg_supp_packsize_manufacturer on mend.supp_packsize_manufacturer
after insert
as
begin
	set nocount on

	merge into mend.supp_packsize_manufacturer_aud with (tablock) as trg
	using inserted src
		on (trg.packsizeid = src.packsizeid and trg.manufacturerid = src.manufacturerid)
	when not matched 
		then
			insert (packsizeid, manufacturerid,
				idETLBatchRun)

				values (src.packsizeid, src.manufacturerid,
					src.idETLBatchRun);
end; 
go