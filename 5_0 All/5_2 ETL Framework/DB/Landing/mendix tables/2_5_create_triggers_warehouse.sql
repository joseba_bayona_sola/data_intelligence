
------------------------------- Triggers ----------------------------------

use Landing
go

------------ inventory$warehousestockitem ------------

drop trigger mend.trg_wh_warehousestockitem;
go 

create trigger mend.trg_wh_warehousestockitem on mend.wh_warehousestockitem
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousestockitem_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.id_string, ' '),
			isnull(trg.availableqty, 0), isnull(trg.actualstockqty, 0), isnull(trg.allocatedstockqty, 0), isnull(trg.forwarddemand, 0), isnull(trg.dueinqty, 0), isnull(trg.stocked, ' '),
			isnull(trg.outstandingallocation, 0), isnull(trg.onhold, 0),
			isnull(trg.stockingmethod, ' '),
			isnull(trg.changed, ' ')
			-- , isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '),
		intersect
		select 
			isnull(src.id_string, ' '),
			isnull(src.availableqty, 0), isnull(src.actualstockqty, 0), isnull(src.allocatedstockqty, 0), isnull(src.forwarddemand, 0), isnull(src.dueinqty, 0), isnull(trg.stocked, ' '),
			isnull(src.outstandingallocation, 0), isnull(src.onhold, 0), 
			isnull(src.stockingmethod, ' '),
			isnull(src.changed, ' ')
			-- , isnull(src.createdDate, ' '), isnull(trg.changedDate, ' '),
			)
			
		then
			update set
				trg.id_string = src.id_string,
				trg.availableqty = src.availableqty, trg.actualstockqty = src.actualstockqty, trg.allocatedstockqty = src.allocatedstockqty, trg.forwarddemand = src.forwarddemand, 
				trg.dueinqty = src.dueinqty, trg.stocked = src.stocked, 
				trg.outstandingallocation = src.outstandingallocation, trg.onhold = src.onhold, 
				trg.stockingmethod = src.stockingmethod, 
				trg.changed = src.changed, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				id_string,
				availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
				outstandingallocation, onhold,
				stockingmethod, 
				changed, 
				createdDate, changedDate, 
				idETLBatchRun)

				values (src.id,
					src.id_string,
					src.availableqty, src.actualstockqty, src.allocatedstockqty, src.forwarddemand, src.dueinqty, src.stocked, 
					src.outstandingallocation, src.onhold,
					src.stockingmethod, 
					src.changed,
					src.createdDate, src.changedDate, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_warehousestockitem_aud;
go 

create trigger mend.trg_wh_warehousestockitem_aud on mend.wh_warehousestockitem_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_warehousestockitem_aud_hist (id,
		id_string,
		availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
		outstandingallocation, onhold,
		stockingmethod, 
		changed,
		createdDate, changedDate, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.id_string,
			d.availableqty, d.actualstockqty, d.allocatedstockqty, d.forwarddemand, d.dueinqty, d.stocked, 
			d.outstandingallocation, d.onhold,
			d.stockingmethod, 
			d.changed,
			d.createdDate, d.changedDate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$located_at ------------

drop trigger mend.trg_wh_located_at;
go 

create trigger mend.trg_wh_located_at on mend.wh_located_at
after insert
as
begin
	set nocount on

	merge into mend.wh_located_at_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitemid = src.warehousestockitemid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (warehousestockitemid, warehouseid, 
				idETLBatchRun)

				values (src.warehousestockitemid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------ inventory$warehousestockitem_stockitem ------------

drop trigger mend.trg_wh_warehousestockitem_stockitem;
go 

create trigger mend.trg_wh_warehousestockitem_stockitem on mend.wh_warehousestockitem_stockitem
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousestockitem_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitemid = src.warehousestockitemid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (warehousestockitemid, stockitemid, 
				idETLBatchRun)

				values (src.warehousestockitemid, src.stockitemid,
					src.idETLBatchRun);
end; 
go

------------ inventory$warehousestockitembatch ------------

drop trigger mend.trg_wh_warehousestockitembatch;
go 

create trigger mend.trg_wh_warehousestockitembatch on mend.wh_warehousestockitembatch
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousestockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.batch_id, 0),
			isnull(trg.fullyallocated, ' '), isnull(trg.fullyIssued, ' '), isnull(trg.status, ' ' ), isnull(trg.autoadjusted, 0), isnull(trg.arriveddate, ' '),
			isnull(trg.receivedquantity, 0), isnull(trg.allocatedquantity, 0), isnull(trg.issuedquantity, 0), isnull(trg.onHoldQuantity, 0), isnull(trg.disposedQuantity, 0), isnull(trg.disposedQuantity, 0), isnull(trg.forwardDemand, 0), isnull(trg.negativeFreeToSell, ' '),isnull(trg.registeredQuantity, 0),
			isnull(trg.groupFIFODate, ' '), isnull(trg.confirmedDate, ' '), isnull(trg.stockRegisteredDate, ' '), isnull(trg.receiptCreatedDate, ' '),
			isnull(trg.productUnitCost, 0), isnull(trg.carriageUnitCost, 0), isnull(trg.inteCoCarriageUnitCost, 0), isnull(trg.dutyUnitCost, 0),isnull(trg.totalUnitCost, 0), isnull(trg.totalUnitCostIncInterCo, 0), isnull(trg.interCoProfitUnitCost, 0),
			isnull(trg.exchangeRate, 0), isnull(trg.currency, ' '),
			isnull(trg.createdDate, ' ') -- isnull(trg.changedDate, ' ')
		intersect
		select 
			isnull(src.batch_id, 0),
			isnull(src.fullyallocated, ' '), isnull(src.fullyIssued, ' '), isnull(src.status, ' '), isnull(trg.autoadjusted, 0), isnull(src.arriveddate, ' '),
			isnull(src.receivedquantity, 0), isnull(src.allocatedquantity, 0), isnull(src.issuedquantity, 0), isnull(src.onHoldQuantity, 0), isnull(src.disposedQuantity, 0), isnull(src.disposedQuantity, 0), isnull(src.forwardDemand, 0), isnull(src.negativeFreeToSell, ' '),isnull(src.registeredQuantity, 0),
			isnull(src.groupFIFODate, ' '), isnull(src.confirmedDate, ' '), isnull(src.stockRegisteredDate, ' '), isnull(src.receiptCreatedDate, ' '),
			isnull(src.productUnitCost, 0), isnull(src.carriageUnitCost, 0), isnull(src.inteCoCarriageUnitCost, 0), isnull(src.dutyUnitCost, 0),isnull(src.totalUnitCost, 0), isnull(src.totalUnitCostIncInterCo, 0), isnull(src.interCoProfitUnitCost, 0),
			isnull(src.exchangeRate, 0), isnull(src.currency, ' '),
			isnull(src.createdDate, ' ') -- isnull(src.changedDate, ' ')
			)
			
		then
			update set
				trg.batch_id = src.batch_id,
				trg.fullyallocated = src.fullyallocated, trg.fullyIssued = src.fullyIssued, trg.status = src.status, trg.autoadjusted = src.autoadjusted, trg.arriveddate = src.arriveddate,
				trg.receivedquantity = src.receivedquantity, trg.allocatedquantity = src.allocatedquantity, trg.issuedquantity = src.issuedquantity, trg.onHoldQuantity = src.onHoldQuantity, trg.disposedQuantity = src.disposedQuantity, trg.forwardDemand = src.forwardDemand, trg.negativeFreeToSell = src.negativeFreeToSell, trg.registeredQuantity = src.registeredQuantity, 
				trg.groupFIFODate = src.groupFIFODate, trg.confirmedDate = src.confirmedDate, trg.stockRegisteredDate = src.stockRegisteredDate, trg.receiptCreatedDate = src.receiptCreatedDate, 
				trg.productUnitCost = src.productUnitCost, trg.carriageUnitCost = src.carriageUnitCost, trg.inteCoCarriageUnitCost = src.inteCoCarriageUnitCost, trg.dutyUnitCost = src.dutyUnitCost, trg.totalUnitCost = src.totalUnitCost, trg.totalUnitCostIncInterCo = src.totalUnitCostIncInterCo, trg.interCoProfitUnitCost = src.interCoProfitUnitCost, 
				trg.exchangeRate = src.exchangeRate, trg.currency = src.currency,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				batch_id,
				fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
				receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
				groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
				productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
				exchangeRate, currency,
				createdDate, changedDate,
				idETLBatchRun)

				values (src.id,
					src.batch_id,
					src.fullyallocated, src.fullyIssued, src.status, src.autoadjusted, src.arriveddate,  
					src.receivedquantity, src.allocatedquantity, src.issuedquantity, src.onHoldQuantity, src.disposedQuantity, src.forwardDemand, src.negativeFreeToSell, src.registeredQuantity,
					src.groupFIFODate, src.confirmedDate, src.stockRegisteredDate, src.receiptCreatedDate, 
					src.productUnitCost, src.carriageUnitCost, src.inteCoCarriageUnitCost, src.dutyUnitCost, src.totalUnitCost, src.totalUnitCostIncInterCo, src.interCoProfitUnitCost, 
					src.exchangeRate, src.currency,
					src.createdDate, src.changedDate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_warehousestockitembatch_aud;
go 

create trigger mend.trg_wh_warehousestockitembatch_aud on mend.wh_warehousestockitembatch_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_warehousestockitembatch_aud_hist (id,
		batch_id,
		fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
		groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
		productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
		exchangeRate, currency,
		createdDate, changedDate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.batch_id,
			d.fullyallocated, d.fullyIssued, d.status, d.autoadjusted, d.arriveddate,  
			d.receivedquantity, d.allocatedquantity, d.issuedquantity, d.onHoldQuantity, d.disposedQuantity, d.forwardDemand, d.negativeFreeToSell, d.registeredQuantity,
			d.groupFIFODate, d.confirmedDate, d.stockRegisteredDate, d.receiptCreatedDate, 
			d.productUnitCost, d.carriageUnitCost, d.inteCoCarriageUnitCost, d.dutyUnitCost, d.totalUnitCost, d.totalUnitCostIncInterCo, d.interCoProfitUnitCost, 
			d.exchangeRate, d.currency,
			d.createdDate, d.changedDate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$warehousestockitembatch_warehousestockitem ------------

drop trigger mend.trg_wh_warehousestockitembatch_warehousestockitem;
go 

create trigger mend.trg_wh_warehousestockitembatch_warehousestockitem on mend.wh_warehousestockitembatch_warehousestockitem
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousestockitembatch_warehousestockitem_aud with (tablock) as trg
	using inserted src
		on (trg.warehousestockitembatchid = src.warehousestockitembatchid and trg.warehousestockitemid = src.warehousestockitemid)
	when not matched 
		then
			insert (warehousestockitembatchid, warehousestockitemid, 
				idETLBatchRun)

				values (src.warehousestockitembatchid, src.warehousestockitemid,
					src.idETLBatchRun);
end; 
go




------------ inventory$batchstockissue ------------

drop trigger mend.trg_wh_batchstockissue;
go 

create trigger mend.trg_wh_batchstockissue on mend.wh_batchstockissue
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockissue_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.issueid, 0), isnull(trg.createddate, ' '), -- isnull(trg.changedDate, ' '),
			isnull(trg.issuedquantity, 0),
			isnull(trg.cancelled, ' '), isnull(trg.shippeddate, ' ')
		intersect
		select 
			isnull(src.issueid, 0), isnull(src.createddate, ' '), -- isnull(src.changedDate, ' '),
			isnull(src.issuedquantity, 0),
			isnull(src.cancelled, ' '), isnull(src.shippeddate, ' '))
			
		then
			update set
				trg.issueid = src.issueid, trg.createddate = src.createddate, trg.changeddate = src.changeddate, 
				trg.issuedquantity = src.issuedquantity,
				trg.cancelled = src.cancelled, trg.shippeddate = src.shippeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				issueid, createddate, changeddate, issuedquantity, 
				cancelled, shippeddate, 
				idETLBatchRun)

				values (src.id,
					src.issueid, src.createddate, src.changeddate, src.issuedquantity, 
					src.cancelled, src.shippeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_batchstockissue_aud;
go 

create trigger mend.trg_wh_batchstockissue_aud on mend.wh_batchstockissue_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_batchstockissue_aud_hist (id,
		issueid, createddate, changeddate, issuedquantity, 
		cancelled, shippeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.issueid, d.createddate, d.changeddate, d.issuedquantity, 
			d.cancelled, d.shippeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$batchstockissue_warehousestockitembatch ------------

drop trigger mend.trg_wh_batchstockissue_warehousestockitembatch;
go 

create trigger mend.trg_wh_batchstockissue_warehousestockitembatch on mend.wh_batchstockissue_warehousestockitembatch
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockissue_warehousestockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockissueid = src.batchstockissueid and trg.warehousestockitembatchid = src.warehousestockitembatchid)
	when not matched 
		then
			insert (batchstockissueid, warehousestockitembatchid, 
				idETLBatchRun)

				values (src.batchstockissueid, src.warehousestockitembatchid,
					src.idETLBatchRun);
end; 
go

------------ inventory$batchstockallocation ------------

drop trigger mend.trg_wh_batchstockallocation;
go 

create trigger mend.trg_wh_batchstockallocation on mend.wh_batchstockallocation
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockallocation_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.fullyissued, ' '), isnull(trg.cancelled, ' '), isnull(trg.allocatedquantity, 0), isnull(trg.allocatedunits, 0), isnull(trg.issuedquantity, 0),
			isnull(trg.createddate, ' ') -- , isnull(trg.changeddate, ' ')
		intersect
		select 
			isnull(src.fullyissued, ' '), isnull(src.cancelled, ' '), isnull(src.allocatedquantity, 0), isnull(src.allocatedunits, 0), isnull(src.issuedquantity, 0),
			isnull(src.createddate, ' ') -- , isnull(src.changeddate, ' ')
			)
			
		then
			update set
				trg.fullyissued = src.fullyissued, trg.cancelled = src.cancelled, trg.allocatedquantity = src.allocatedquantity, trg.allocatedunits = src.allocatedunits, trg.issuedquantity = src.issuedquantity,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
				createddate, changeddate, 
				idETLBatchRun)

				values (src.id,
					src.fullyissued, src.cancelled, src.allocatedquantity, src.allocatedunits, src.issuedquantity, 
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_batchstockallocation_aud;
go 

create trigger mend.trg_wh_batchstockallocation_aud on mend.wh_batchstockallocation_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_batchstockallocation_aud_hist (id,
		fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.fullyissued, d.cancelled, d.allocatedquantity, d.allocatedunits, d.issuedquantity, 
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$batchstockissue_batchstockallocation ------------

drop trigger mend.trg_wh_batchstockissue_batchstockallocation;
go 

create trigger mend.trg_wh_batchstockissue_batchstockallocation on mend.wh_batchstockissue_batchstockallocation
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockissue_batchstockallocation_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockissueid = src.batchstockissueid and trg.batchstockallocationid = src.batchstockallocationid)
	when not matched 
		then
			insert (batchstockissueid, batchstockallocationid, 
				idETLBatchRun)

				values (src.batchstockissueid, src.batchstockallocationid,
					src.idETLBatchRun);
end; 
go

------------ inventory$batchtransaction_orderlinestockallocationtransaction ------------

drop trigger mend.trg_wh_batchtransaction_orderlinestockallocationtransaction;
go 

create trigger mend.trg_wh_batchtransaction_orderlinestockallocationtransaction on mend.wh_batchtransaction_orderlinestockallocationtransaction
after insert
as
begin
	set nocount on

	merge into mend.wh_batchtransaction_orderlinestockallocationtransaction_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockallocationid = src.batchstockallocationid and trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid)
	when not matched 
		then
			insert (batchstockallocationid, orderlinestockallocationtransactionid, 
				idETLBatchRun)

				values (src.batchstockallocationid, src.orderlinestockallocationtransactionid,
					src.idETLBatchRun);
end; 
go


------------ inventory$batchtransaction_warehousestockitembatch ------------

drop trigger mend.trg_wh_batchtransaction_warehousestockitembatch;
go 

create trigger mend.trg_wh_batchtransaction_warehousestockitembatch on mend.wh_batchtransaction_warehousestockitembatch
after insert
as
begin
	set nocount on

	merge into mend.wh_batchtransaction_warehousestockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockallocationid = src.batchstockallocationid and trg.warehousestockitembatchid = src.warehousestockitembatchid)
	when not matched 
		then
			insert (batchstockallocationid, warehousestockitembatchid, 
				idETLBatchRun)

				values (src.batchstockallocationid, src.warehousestockitembatchid,
					src.idETLBatchRun);
end; 
go





------------ inventory$repairstockbatchs ------------

drop trigger mend.trg_wh_repairstockbatchs;
go 

create trigger mend.trg_wh_repairstockbatchs on mend.wh_repairstockbatchs
after insert
as
begin
	set nocount on

	merge into mend.wh_repairstockbatchs_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.batchtransaction_id, 0), isnull(trg.reference, 0), isnull(trg.date, ' '),
			isnull(trg.oldallocation, 0),  isnull(trg.newallocation, 0), isnull(trg.oldissue, 0), isnull(trg.newissue, 0), isnull(trg.processed, ' ')
		intersect
		select 
			isnull(src.batchtransaction_id, 0), isnull(src.reference, 0), isnull(src.date, ' '),
			isnull(src.oldallocation, 0),  isnull(src.newallocation, 0), isnull(src.oldissue, 0), isnull(src.newissue, 0), isnull(src.processed, ' '))
			
		then
			update set
				trg.batchtransaction_id = src.batchtransaction_id, trg.reference = src.reference, trg.date = src.date,
				trg.oldallocation = src.oldallocation, trg.newallocation = src.newallocation, trg.oldissue = src.oldissue, trg.newissue = src.newissue, trg.processed = src.processed,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				batchtransaction_id, reference, date,
				oldallocation, newallocation, oldissue, newissue, processed, 
				idETLBatchRun)

				values (src.id,
					src.batchtransaction_id, src.reference, src.date,
					src.oldallocation, src.newallocation, src.oldissue, src.newissue, src.processed,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_repairstockbatchs_aud;
go 

create trigger mend.trg_wh_repairstockbatchs_aud on mend.wh_repairstockbatchs_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_repairstockbatchs_aud_hist (id,
		batchtransaction_id, reference, date,
		oldallocation, newallocation, oldissue, newissue, processed,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.batchtransaction_id, d.reference, d.date,
			d.oldallocation, d.newallocation, d.oldissue, d.newissue, d.processed,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$repairstockbatchs_warehousestockitembatch ------------

drop trigger mend.trg_wh_repairstockbatchs_warehousestockitembatch;
go 

create trigger mend.trg_wh_repairstockbatchs_warehousestockitembatch on mend.wh_repairstockbatchs_warehousestockitembatch
after insert
as
begin
	set nocount on

	merge into mend.wh_repairstockbatchs_warehousestockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.repairstockbatchsid = src.repairstockbatchsid and trg.warehousestockitembatchid = src.warehousestockitembatchid)
	when not matched 
		then
			insert (repairstockbatchsid, warehousestockitembatchid, 
				idETLBatchRun)

				values (src.repairstockbatchsid, src.warehousestockitembatchid,
					src.idETLBatchRun);
end; 
go







------------ inventory$stockmovement ------------

drop trigger mend.trg_wh_stockmovement;
go 

create trigger mend.trg_wh_stockmovement on mend.wh_stockmovement
after insert
as
begin
	set nocount on

	merge into mend.wh_stockmovement_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.moveid, ' '), isnull(trg.movelineid, ' '), 
			isnull(trg.stockadjustment, ' '), isnull(trg.movementtype, ' '), isnull(trg.movetype, ' '), isnull(trg._class, ' '), isnull(trg.reasonid, ' '), isnull(trg.status, ' '),
			isnull(trg.skuid, ' '), isnull(trg.qtyactioned, 0),
			isnull(trg.fromstate, ' '), isnull(trg.tostate, ' '),
			isnull(trg.fromstatus, ' '), isnull(trg.tostatus, ' '),
			isnull(trg.operator, ' '), isnull(trg.supervisor, ' '),
			isnull(trg.dateCreated, ' '),  isnull(trg.dateClosed, ' ')
			-- , isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '),
		intersect
		select 
			isnull(src.moveid, ' '), isnull(src.movelineid, ' '), 
			isnull(src.stockadjustment, ' '), isnull(src.movementtype, ' '), isnull(src.movetype, ' '), isnull(src._class, ' '), isnull(src.reasonid, ' '), isnull(src.status, ' '),
			isnull(src.skuid, ' '), isnull(src.qtyactioned, 0),
			isnull(src.fromstate, ' '), isnull(src.tostate, ' '),
			isnull(src.fromstatus, ' '), isnull(src.tostatus, ' '),
			isnull(src.operator, ' '), isnull(src.supervisor, ' '),
			isnull(src.dateCreated, ' '),  isnull(src.dateClosed, ' ')
			-- , isnull(src.createdDate, ' '), isnull(src.changedDate, ' '),
			)
			
		then
			update set
				trg.moveid = src.moveid, trg.movelineid = src.movelineid,
				trg.stockadjustment = src.stockadjustment, trg.movementtype = src.movementtype, trg.movetype = src.movetype, trg._class = src._class, trg.reasonid = src.reasonid, trg.status = src.status,
				trg.skuid = src.skuid, trg.qtyactioned = src.qtyactioned,
				trg.fromstate = src.fromstate, trg.tostate = src.tostate,
				trg.fromstatus = src.fromstatus, trg.tostatus = src.tostatus,
				trg.operator = src.operator, trg.supervisor = src.supervisor,
				trg.dateCreated = src.dateCreated, trg.dateClosed = src.dateClosed,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				moveid, movelineid, 
				stockadjustment, movementtype, movetype, _class, reasonid, status, 
				skuid, qtyactioned, 
				fromstate, tostate, 
				fromstatus, tostatus, 
				operator, supervisor,
				dateCreated, dateClosed,
				createdDate, changedDate, 
				idETLBatchRun)

				values (src.id,
					src.moveid, src.movelineid, 
					src.stockadjustment, src.movementtype, src.movetype, src._class, src.reasonid, src.status, 
					src.skuid, src.qtyactioned, 
					src.fromstate, src.tostate, 
					src.fromstatus, src.tostatus, 
					src.operator, src.supervisor,
					src.dateCreated, src.dateClosed,
					src.createdDate, src.changedDate, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_stockmovement_aud;
go 

create trigger mend.trg_wh_stockmovement_aud on mend.wh_stockmovement_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_stockmovement_aud_hist (id, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, 
		fromstatus, tostatus, 
		operator, supervisor,
		dateCreated, dateClosed,
		createdDate, changedDate, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.moveid, d.movelineid, 
			d.stockadjustment, d.movementtype, d.movetype, '_d.class', d.reasonid, d.status, 
			d.skuid, d.qtyactioned, 
			d.fromstate, d.tostate, 
			d.fromstatus, d.tostatus, 
			d.operator, d.supervisor,
			d.dateCreated, d.dateClosed,
			d.createdDate, d.changedDate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$stockmovement_warehousestockitem ------------

drop trigger mend.trg_wh_stockmovement_warehousestockitem;
go 

create trigger mend.trg_wh_stockmovement_warehousestockitem on mend.wh_stockmovement_warehousestockitem
after insert
as
begin
	set nocount on

	merge into mend.wh_stockmovement_warehousestockitem_aud with (tablock) as trg
	using inserted src
		on (trg.stockmovementid = src.stockmovementid and trg.warehousestockitemid = src.warehousestockitemid)
	when not matched 
		then
			insert (stockmovementid, warehousestockitemid, 
				idETLBatchRun)

				values (src.stockmovementid, src.warehousestockitemid,
					src.idETLBatchRun);
end; 
go







------------ inventory$batchstockmovement ------------

drop trigger mend.trg_wh_batchstockmovement;
go 

create trigger mend.trg_wh_batchstockmovement on mend.wh_batchstockmovement
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockmovement_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.batchstockmovement_id, ' '), isnull(trg.batchstockmovementtype, ' '), 
			isnull(trg.quantity, ' '), isnull(trg.comment, ' '), 
			isnull(trg.createddate, ' '), -- isnull(trg.changedDate, ' '),
			isnull(trg.system$owner, 0)
		intersect
		select 
			isnull(src.batchstockmovement_id, ' '), isnull(src.batchstockmovementtype, ' '), 
			isnull(src.quantity, ' '), isnull(src.comment, ' '),
			isnull(src.createddate, ' '), -- isnull(src.changedDate, ' '),
			isnull(src.system$owner, 0))
			
		then
			update set
				trg.batchstockmovement_id = src.batchstockmovement_id, trg.batchstockmovementtype = src.batchstockmovementtype, 
				trg.quantity = src.quantity, trg.comment = src.comment,
				trg.createddate = src.createddate, trg.changedDate = src.changedDate, 
				trg.system$owner = src.system$owner,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				batchstockmovement_id, batchstockmovementtype, 
				quantity, comment,
				createddate, changedDate, system$owner,
				idETLBatchRun)

				values (src.id,
					src.batchstockmovement_id, src.batchstockmovementtype, 
					src.quantity, src.comment,
					src.createddate, src.changedDate, src.system$owner,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_batchstockmovement_aud;
go 

create trigger mend.trg_wh_batchstockmovement_aud on mend.wh_batchstockmovement_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_batchstockmovement_aud_hist (id, 
		batchstockmovement_id, batchstockmovementtype, 
		quantity, comment,
		createddate, changedDate, system$owner,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.batchstockmovement_id, d.batchstockmovementtype, 
			d.quantity, d.comment,
			d.createddate, d.changedDate, d.system$owner,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$batchstockmovement_stockmovement ------------

drop trigger mend.trg_wh_batchstockmovement_stockmovement;
go 

create trigger mend.trg_wh_batchstockmovement_stockmovement on mend.wh_batchstockmovement_stockmovement
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockmovement_stockmovement_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockmovementid = src.batchstockmovementid and trg.stockmovementid = src.stockmovementid)
	when not matched 
		then
			insert (batchstockmovementid, stockmovementid, 
				idETLBatchRun)

				values (src.batchstockmovementid, src.stockmovementid,
					src.idETLBatchRun);
end; 
go

------------ inventory$batchstockmovement_warehousestockitembatch ------------

drop trigger mend.trg_wh_batchstockmovement_warehousestockitembatch;
go 

create trigger mend.trg_wh_batchstockmovement_warehousestockitembatch on mend.wh_batchstockmovement_warehousestockitembatch
after insert
as
begin
	set nocount on

	merge into mend.wh_batchstockmovement_warehousestockitembatch_aud with (tablock) as trg
	using inserted src
		on (trg.batchstockmovementid = src.batchstockmovementid and trg.warehousestockitembatchid = src.warehousestockitembatchid)
	when not matched 
		then
			insert (batchstockmovementid, warehousestockitembatchid, 
				idETLBatchRun)

				values (src.batchstockmovementid, src.warehousestockitembatchid,
					src.idETLBatchRun);
end; 
go