
------------------------ Drop tables -------------------------------

use Landing
go

------------ product$productcategory ------------

drop table mend.prod_productcategory;
go
drop table mend.prod_productcategory_aud;
go
drop table mend.prod_productcategory_aud_hist;
go

------------ product$productfamily ------------

drop table mend.prod_productfamily;
go
drop table mend.prod_productfamily_aud;
go
drop table mend.prod_productfamily_aud_hist;
go

------------ product$productfamily_productcategory ------------

drop table mend.prod_productfamily_productcategory;
go
drop table mend.prod_productfamily_productcategory_aud;
go

------------ product$product ------------

drop table mend.prod_product;
go
drop table mend.prod_product_aud;
go
drop table mend.prod_product_aud_hist;
go

------------ product$product_productfamily ------------

drop table mend.prod_product_productfamily;
go
drop table mend.prod_product_productfamily_aud;
go

------------ product$stockitem ------------

drop table mend.prod_stockitem;
go
drop table mend.prod_stockitem_aud;
go
drop table mend.prod_stockitem_aud_hist;
go

------------ product$stockitem_product ------------

drop table mend.prod_stockitem_product;
go
drop table mend.prod_stockitem_product_aud;
go

------------ product$contactlens ------------

drop table mend.prod_contactlens;
go
drop table mend.prod_contactlens_aud;
go
drop table mend.prod_contactlens_aud_hist;
go

------------ product$packsize ------------

drop table mend.prod_packsize;
go
drop table mend.prod_packsize_aud;
go
drop table mend.prod_packsize_aud_hist;
go

------------ product$packsize_productfamily ------------

drop table mend.prod_packsize_productfamily;
go
drop table mend.prod_packsize_productfamily_aud;
go

------------ product$stockitem_packsize ------------

drop table mend.prod_stockitem_packsize;
go
drop table mend.prod_stockitem_packsize_aud;
go