------------------------ Drop tables -------------------------------

use Landing
go

------------ companyorganisation$company ------------

drop table mend.comp_company;
go
drop table mend.comp_company_aud;
go
drop table mend.comp_company_aud_hist;
go

------------ companyorganisation$magwebstore ------------

drop table mend.comp_magwebstore;
go
drop table mend.comp_magwebstore_aud;
go
drop table mend.comp_magwebstore_aud_hist;
go



------------ countrymanagement$country ------------

drop table mend.comp_country;
go
drop table mend.comp_country_aud;
go
drop table mend.comp_country_aud_hist;
go


------------ countrymanagement$currency ------------

drop table mend.comp_currency;
go
drop table mend.comp_currency_aud;
go
drop table mend.comp_currency_aud_hist;
go


------------ countrymanagement$country_currency ------------

drop table mend.comp_country_currency;
go
drop table mend.comp_country_currency_aud;
go



------------ countrymanagement$spotrate ------------

drop table mend.comp_spotrate;
go
drop table mend.comp_spotrate_aud;
go
drop table mend.comp_spotrate_aud_hist;
go

------------ countrymanagement$currentspotrate_currency ------------

drop table mend.comp_currentspotrate_currency;
go
drop table mend.comp_currentspotrate_currency_aud;
go

------------ countrymanagement$historicalspotrate_currency ------------

drop table mend.comp_historicalspotrate_currency;
go
drop table mend.comp_historicalspotrate_currency_aud;
go
