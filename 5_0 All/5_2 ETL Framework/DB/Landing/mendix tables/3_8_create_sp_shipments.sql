
------------------------------ SP -----------------------------------

use Landing
go

------------------- customershipments$customershipment -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipment
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipment
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				shipmentID, partitionID0_120,
				shipmentNumber, orderIncrementID, 
				status, statusChange, 
				processingFailed, warehouseMethod, labelRenderer, 
				shippingMethod, shippingDescription, paymentMethod, 	
				notify, fullyShipped, syncedToMagento, 	
				shipmentValue, shippingTotal, toFollowValue, 
				hold, intersite, 
				dispatchDate, expectedShippingDate, expectedDeliveryDate,  
				createdDate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id,
						shipmentID, partitionID0_120,
						shipmentNumber, orderIncrementID, 
						status, statusChange, 
						processingFailed, warehouseMethod, labelRenderer, 
						shippingMethod, shippingDescription, paymentMethod, 	
						notify, fullyShipped, syncedToMagento, 	
						shipmentValue, shippingTotal, toFollowValue, 
						hold, intersite, 
						dispatchDate, expectedShippingDate, expectedDeliveryDate,  
						createdDate, changeddate 
					from public."customershipments$customershipment"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')														
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$customershipment_warehouse -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipment_warehouse
go

-- ==============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ==============================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipment_warehouse
-- ==============================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipment_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$customershipmentid customershipmentid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select sw."customershipments$customershipmentid", sw."inventory$warehouseid"
				from 
						public."customershipments$customershipment_warehouse" sw
					inner join	
						public."customershipments$customershipment" s on sw."customershipments$customershipmentid" = s.id
				where s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$customershipment_order -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipment_order
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipment_order
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipment_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$customershipmentid customershipmentid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select so."customershipments$customershipmentid", so."orderprocessing$orderid"
				from 
						public."customershipments$customershipment_order" so
					inner join	
						public."customershipments$customershipment" s on so."customershipments$customershipmentid" = s.id
					inner join
						public."orderprocessing$order" o on so."orderprocessing$orderid" = o.id
				where (s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or 
						(o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))				
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------------- customershipments$lifecycle_update -------------------

drop procedure mend.srcmend_lnd_get_ship_lifecycle_update
go

-- ====================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ====================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_lifecycle_update
-- ====================================================================================

create procedure mend.srcmend_lnd_get_ship_lifecycle_update
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				status, timestamp, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select lcu.id,
					lcu.status, lcu.timestamp 
				from 
						public."customershipments$lifecycle_update" lcu
					inner join
						public."customershipments$customershipment_progress" cslcu on lcu.id = cslcu."customershipments$lifecycle_updateid"
					inner join
						public."customershipments$customershipment" s on cslcu."customershipments$customershipmentid" = s.id
				where lcu.status = ''''Complete''''
				and (s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))		
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$customershipment_progress -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipment_progress
go

-- =============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- =============================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipment_progress
-- =============================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipment_progress
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$lifecycle_updateid lifecycle_updateid, customershipments$customershipmentid customershipmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select cslcu."customershipments$lifecycle_updateid", cslcu."customershipments$customershipmentid"
				from 
						public."customershipments$customershipment_progress" cslcu
					inner join	
						public."customershipments$lifecycle_update" lcu on cslcu."customershipments$lifecycle_updateid" = lcu.id 
					inner join
						public."customershipments$customershipment" s on cslcu."customershipments$customershipmentid" = s.id
				where lcu.status = ''''Complete''''
				and (s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))						
				'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$api_updates -------------------

drop procedure mend.srcmend_lnd_get_ship_api_updates
go

-- =============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Return NO rows 
-- =============================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_api_updates
-- =============================================================================================

create procedure mend.srcmend_lnd_get_ship_api_updates
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				service, status, operation, 
				timestamp, 
				detail, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, 
					service, status, operation, 
					timestamp, 
					detail
				from public."customershipments$api_updates"
				where 1 = 0
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$api_updates_customershipment -------------------

drop procedure mend.srcmend_lnd_get_ship_api_updates_customershipment
go

-- ================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Return NO rows
-- ================================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_api_updates_customershipment
-- ================================================================================================

create procedure mend.srcmend_lnd_get_ship_api_updates_customershipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$api_updatesid api_updatesid, customershipments$customershipmentid customershipmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "customershipments$api_updatesid", "customershipments$customershipmentid"
				from public."customershipments$api_updates_customershipment"
				where 1 = 0
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------------- customershipments$consignment -------------------

drop procedure mend.srcmend_lnd_get_ship_consignment
go

-- ===============================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ===============================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_consignment
-- ===============================================================================

create procedure mend.srcmend_lnd_get_ship_consignment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				order_number, identifier, 
				warehouse_id, 
				consignment_number, 
				shipping_method, service_id, carrier, service, 
				delivery_instructions, tracking_url, 
				order_value, currency,
				create_date, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_LOCAL,''
				select id,  
					order_number, identifier, 
					warehouse_id, 
					consignment_number, 
					shipping_method, service_id, carrier, service, 
					delivery_instructions, tracking_url, 
					order_value, currency,
					create_date
				from public."customershipments$consignment" limit 10000'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$scurriconsignment_shipment -------------------

drop procedure mend.srcmend_lnd_get_ship_scurriconsignment_shipment
go

-- ==============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==============================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_scurriconsignment_shipment
-- ==============================================================================================

create procedure mend.srcmend_lnd_get_ship_scurriconsignment_shipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$consignmentid consignmentid, customershipments$customershipmentid customershipmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_LOCAL,''
				select "customershipments$consignmentid", "customershipments$customershipmentid"
				from public."customershipments$scurriconsignment_shipment"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$packagedetail -------------------

drop procedure mend.srcmend_lnd_get_ship_packagedetail
go

-- =================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- =================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_packagedetail
-- =================================================================================

create procedure mend.srcmend_lnd_get_ship_packagedetail
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				tracking_number, description, 
				width, length, height, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_LOCAL,''
				select id, 
					tracking_number, description, 
					width, length, height
				from public."customershipments$packagedetail"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$consignmentpackages -------------------

drop procedure mend.srcmend_lnd_get_ship_consignmentpackages
go

-- =======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- =======================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_consignmentpackages
-- =======================================================================================

create procedure mend.srcmend_lnd_get_ship_consignmentpackages
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$packagedetailid packagedetailid, customershipments$consignmentid consignmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_LOCAL,''
				select "customershipments$packagedetailid","customershipments$consignmentid"
				from public."customershipments$consignmentpackages"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------------- customershipments$customershipmentline -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipmentline
go

-- ========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ========================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipmentline
-- ========================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipmentline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				shipmentLineID, lineNumber, status, variableBreakdown, 
				stockItemID, productName, productDescription, eye, 
				quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
				packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
				price, priceFormatted, 
				subTotal, subTotal_formatted, 
				VATRate, VAT, VAT_formatted,
				fullyShipped,  
				BOMLine, 
				system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select sl.id, 
					sl.shipmentLineID, sl.lineNumber, sl.status, sl.variableBreakdown, 
					sl.stockItemID, sl.productName, sl.productDescription, sl.eye, 
					sl.quantityDescription, sl.itemQuantityOrdered, sl.itemQuantityIssued, sl.itemQuantityShipped, sl.itemQuantityPreviouslyShipped, 
					sl.packQuantityOrdered, sl.packQuantityIssued, sl.packQuantityShipped, 
					sl.price, sl.priceFormatted, 
					sl.subTotal, sl.subTotal_formatted, 
					sl.VATRate, sl.VAT, sl.VAT_formatted,
					sl.fullyShipped,  
					sl.BOMLine, 
					sl."system$changedby"
				from 
						public."customershipments$customershipmentline" sl
					inner join
						public."customershipments$customershipmentline_customershipment" sls on sl.id = sls."customershipments$customershipmentlineid"
					inner join	
						public."customershipments$customershipment" s on sls."customershipments$customershipmentid" = s.id
				where s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')							
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$customershipmentline_customershipment -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipmentline_customershipment
go

-- =========================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- =========================================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipmentline_customershipment
-- =========================================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipmentline_customershipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$customershipmentlineid customershipmentlineid, customershipments$customershipmentid customershipmentid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select sls."customershipments$customershipmentlineid", sls."customershipments$customershipmentid"
				from 
						public."customershipments$customershipmentline_customershipment" sls
					inner join	
						public."customershipments$customershipment" s on sls."customershipments$customershipmentid" = s.id
				where s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')			
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- customershipments$customershipmentline_product -------------------

drop procedure mend.srcmend_lnd_get_ship_customershipmentline_product
go

-- ================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 05-06-2017
-- Changed: 
	--	12-02-2018	Joseba Bayona Sola	Connection to Restore DB - Add INCR Filtering
-- ================================================================================================
-- Description: Reads data from Mendix Table through Open Query - ship_customershipmentline_product
-- ================================================================================================

create procedure mend.srcmend_lnd_get_ship_customershipmentline_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select customershipments$customershipmentlineid customershipmentlineid, product$productid productid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select slp."customershipments$customershipmentlineid", slp."product$productid"
				from 
						public."customershipments$customershipmentline_product" slp
					inner join
						public."customershipments$customershipmentline_customershipment" sls on slp."customershipments$customershipmentlineid" = sls."customershipments$customershipmentlineid"
					inner join	
						public."customershipments$customershipment" s on sls."customershipments$customershipmentid" = s.id
				where s.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
					or s.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')				
				'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go