
------------------------------ SP -----------------------------------

use Landing
go

------------------- stockallocation$orderlinestockallocationtransaction -------------------

drop procedure mend.srcmend_lnd_get_all_orderlinestockallocationtransaction
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_orderlinestockallocationtransaction
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_all_orderlinestockallocationtransaction
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
					transactionID, timestamp,
					allocationType, recordType, 
					allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
					stockAllocated, cancelled, 
					createddate, changeddate, 
					issuedDateTime, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id,
						transactionID, timestamp,
						allocationType, recordType, 
						allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
						stockAllocated, cancelled, 
						issuedDateTime, 
						createddate, changeddate 
					from public."stockallocation$orderlinestockallocationtransaction"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')									
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocation$at_order ------------

drop procedure mend.srcmend_lnd_get_all_at_order
go

-- ==================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_order
-- ==================================================================================

create procedure mend.srcmend_lnd_get_all_at_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select sato."stockallocation$orderlinestockallocationtransactionid", sato."orderprocessing$orderid" 
					from 
							public."stockallocation$at_order" sato
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on sato."stockallocation$orderlinestockallocationtransactionid" = sat.id
						inner join
							public."orderprocessing$order" o on sato."orderprocessing$orderid" = o.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or	
						  (o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocation$at_orderline ------------

drop procedure mend.srcmend_lnd_get_all_at_orderline
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_orderline
-- ======================================================================================

create procedure mend.srcmend_lnd_get_all_at_orderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, orderprocessing$orderlineid orderlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select satol."stockallocation$orderlinestockallocationtransactionid", satol."orderprocessing$orderlineid" 
					from 
							public."stockallocation$at_orderline" satol
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on satol."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 	
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocation$at_warehouse ------------

drop procedure mend.srcmend_lnd_get_all_at_warehouse
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_warehouse
-- ======================================================================================

create procedure mend.srcmend_lnd_get_all_at_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select satw."stockallocation$orderlinestockallocationtransactionid", satw."inventory$warehouseid" 
					from 
							public."stockallocation$at_warehouse" satw
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on satw."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 						
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocation$at_warehousestockitem ------------

drop procedure mend.srcmend_lnd_get_all_at_warehousestockitem
go

-- ===============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ===============================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_warehousestockitem
-- ===============================================================================================

create procedure mend.srcmend_lnd_get_all_at_warehousestockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, inventory$warehousestockitemid warehousestockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select satwsi."stockallocation$orderlinestockallocationtransactionid", satwsi."inventory$warehousestockitemid" 
					from 
							public."stockallocation$at_warehousestockitem" satwsi
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on satwsi."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 							
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ stockallocation$at_batch ------------

drop procedure mend.srcmend_lnd_get_all_at_batch
go

-- ===============================================================================================
-- Author: Joseba Bayona Sola
-- Date: 13-02-2018
-- Changed: 
-- ===============================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_batch
-- ===============================================================================================

create procedure mend.srcmend_lnd_get_all_at_batch
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, inventory$warehousestockitembatchid warehousestockitembatchid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select satwsib."stockallocation$orderlinestockallocationtransactionid", satwsib."inventory$warehousestockitembatchid" 
					from 
							public."stockallocation$at_batch" satwsib
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on satwsib."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 						
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocation$at_packsize ------------

drop procedure mend.srcmend_lnd_get_all_at_packsize
go

-- =====================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- =====================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_at_packsize
-- =====================================================================================

create procedure mend.srcmend_lnd_get_all_at_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, product$packsizeid packsizeid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select satps."stockallocation$orderlinestockallocationtransactionid", satps."product$packsizeid" 
					from 
							public."stockallocation$at_packsize" satps
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on satps."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 						
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocatio$orderlinestockallocationtransa_customershipmentl ------------

drop procedure mend.srcmend_lnd_get_all_orderlinestockallocationtransa_customershipmentl
go

-- ==========================================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_orderlinestockallocationtransa_customershipmentl
-- ==========================================================================================================================

create procedure mend.srcmend_lnd_get_all_orderlinestockallocationtransa_customershipmentl
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, customershipments$customershipmentlineid customershipmentlineid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select sats."stockallocation$orderlinestockallocationtransactionid", sats."customershipments$customershipmentlineid"
					from 
							public."stockallocatio$orderlinestockallocationtransa_customershipmentl" sats
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on sats."stockallocation$orderlinestockallocationtransactionid" = sat.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 					
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go







------------ stockallocation$magentoallocation ------------

drop procedure mend.srcmend_lnd_get_all_magentoallocation
go

-- ===========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_magentoallocation
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_all_magentoallocation
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
					shipped, cancelled, 
					quantity, 
					createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id,
						shipped, cancelled, 
						quantity, 
						createddate, changeddate
					from public."stockallocation$magentoallocation"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')								
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ stockallocatio$magentoallocatio_orderlinestockallocationtransac ------------

drop procedure mend.srcmend_lnd_get_all_magentoallocatio_orderlinestockallocationtransac
go

-- ===========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-06-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_magentoallocatio_orderlinestockallocationtransac
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_all_magentoallocatio_orderlinestockallocationtransac
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$magentoallocationid magentoallocationid, stockallocation$orderlinestockallocationtransactionid orderlinestockallocationtransactionid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select masat."stockallocation$magentoallocationid", masat."stockallocation$orderlinestockallocationtransactionid" 
					from 
							public."stockallocatio$magentoallocatio_orderlinestockallocationtransac" masat
						inner join
							public."stockallocation$orderlinestockallocationtransaction" sat on masat."stockallocation$orderlinestockallocationtransactionid" = sat.id
						inner join
							public."stockallocation$magentoallocation" ma on masat."stockallocation$magentoallocationid" = ma.id
					where (sat.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or sat.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or
						  (ma.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ma.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 					
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ magentoallocation_orderlinemagento ------------

drop procedure mend.srcmend_lnd_get_all_magentoallocation_orderlinemagento
go

-- ===========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-08-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXX
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_magentoallocation_orderlinemagento
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_all_magentoallocation_orderlinemagento
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$magentoallocationid magentoallocationid, orderprocessing$orderlinemagentoid orderlinemagentoid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select masat."stockallocation$magentoallocationid", masat."orderprocessing$orderlinemagentoid" 
					from 
							public."stockallocation$magentoallocation_orderlinemagento" masat
						inner join
							public."stockallocation$magentoallocation" ma on masat."stockallocation$magentoallocationid" = ma.id
					where (ma.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ma.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 					
					'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ magentoallocation_warehouse ------------

drop procedure mend.srcmend_lnd_get_all_magentoallocation_warehouse
go

-- ===========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 28-08-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXX
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - all_magentoallocation_warehouse
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_all_magentoallocation_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select stockallocation$magentoallocationid magentoallocationid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select masat."stockallocation$magentoallocationid", masat."inventory$warehouseid" 
					from 
							public."stockallocation$magentoallocation_warehouse" masat
						inner join
							public."stockallocation$magentoallocation" ma on masat."stockallocation$magentoallocationid" = ma.id
					where (ma.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ma.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 					
					'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go