use Landing
go 

------------------------------------------------------------------------
----------------------- purchaseorders$shortageheader ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortageheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_shortageheader_aud

	select idETLBatchRun, count(*) num_rows
	from mend.short_shortageheader_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_shortageheader_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			wholesale, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_shortageheader_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

---------------------------------------------------------------------------------
----------------------- purchaseorders$shortageheader_supplier ------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortageheader_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------------------
----------------------- purchaseorders$shortageheader_sourcewarehouse ------------------
----------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortageheader_sourcewarehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

---------------------------------------------------------------------------------------------
----------------------- purchaseorders$shortageheader_destinationwarehouse ------------------
---------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortageheader_destinationwarehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

---------------------------------------------------------------------------------
----------------------- purchaseorders$shortageheader_packsize ------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortageheader_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun






------------------------------------------------------------------
----------------------- purchaseorders$shortage ------------------
------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_shortage_aud

	select idETLBatchRun, count(*) num_rows
	from mend.short_shortage_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_shortage_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			shortageid, purchaseordernumber, ordernumbers,
			wholesale, 
			status, 
			unitquantity, packquantity, 
			requireddate, 
			createdDate, changedDate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_shortage_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

---------------------------------------------------------------------------------
----------------------- purchaseorders$shortage_shortageheader ------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_shortageheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------------------
----------------------- purchaseorders$shortage_purchaseorderline ------------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_purchaseorderline_aud
	group by idETLBatchRun
	order by idETLBatchRun

---------------------------------------------------------------------------
----------------------- purchaseorders$shortage_supplier ------------------
---------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

--------------------------------------------------------------------------
----------------------- purchaseorders$shortage_product ------------------
--------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_product_aud
	group by idETLBatchRun
	order by idETLBatchRun

--------------------------------------------------------------------------------
----------------------- purchaseorders$shortage_standardprice ------------------
--------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_standardprice_aud
	group by idETLBatchRun
	order by idETLBatchRun

----------------------------------------------------------------------------
----------------------- purchaseorders$shortage_stockitem ------------------
----------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun

-------------------------------------------------------------------------------------
----------------------- purchaseorders$shortage_warehousestockitem ------------------
-------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_shortage_warehousestockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun







---------------------------------------------------------------------------
----------------------- purchaseorders$orderlineshortage ------------------
---------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_orderlineshortage_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_orderlineshortage_aud

	select idETLBatchRun, count(*) num_rows
	from mend.short_orderlineshortage_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.short_orderlineshortage_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			orderlineshortageid,
			orderlineshortagetype,
			unitquantity, packquantity,
			shortageprogress, adviseddate, 
			createdDate, changedDate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.short_orderlineshortage_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------------------
----------------------- purchaseorders$orderlineshortage_shortage ------------------
------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_orderlineshortage_shortage_aud
	group by idETLBatchRun
	order by idETLBatchRun

-------------------------------------------------------------------------------------
----------------------- purchaseorders$orderlineshortage_orderline ------------------
-------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.short_orderlineshortage_orderline_aud
	group by idETLBatchRun
	order by idETLBatchRun