
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ inventory$warehouse ------------

select *
from mend.wh_warehouse

select id, 
	code, name, shortName, warehouseType, snapWarehouse, 
	snapCode, scurriCode, 
	active, wms_active, useHoldingLabels, stockcheckignore,
	idETLBatchRun, ins_ts
from mend.wh_warehouse

select id, 
	code, name, shortName, warehouseType, snapWarehouse, 
	snapCode, scurriCode, 
	active, wms_active, useHoldingLabels, stockcheckignore,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehouse_aud

select id, 
	code, name, shortName, warehouseType, snapWarehouse, 
	snapCode, scurriCode, 
	active, wms_active, useHoldingLabels, stockcheckignore,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_warehouse_aud_hist




------------ inventory$warehousesupplier_warehouse ------------

select warehousesupplierid, warehouseid, 
	idETLBatchRun, ins_ts
from mend.wh_warehousesupplier_warehouse

select warehousesupplierid, warehouseid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousesupplier_warehouse_aud


------------ inventory$warehousesupplier_supplier ------------

select warehousesupplierid, supplierid, 
	idETLBatchRun, ins_ts
from mend.wh_warehousesupplier_supplier

select warehousesupplierid, supplierid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousesupplier_supplier_aud






------------ inventory$supplierroutine ------------

select *
from mend.wh_supplierroutine

select id,
	supplierName,
	mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
	tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
	wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
	thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
	fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
	saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
	sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
	idETLBatchRun, ins_ts
from mend.wh_supplierroutine

select id,
	supplierName,
	mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
	tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
	wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
	thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
	fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
	saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
	sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_supplierroutine_aud

select id,
	supplierName,
	mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
	tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
	wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
	thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
	fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
	saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
	sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_supplierroutine_aud_hist

------------ inventory$supplierroutine_warehousesupplier ------------

select *
from mend.wh_supplierroutine_warehousesupplier

select supplierRoutineID, warehouseSupplierID,
	idETLBatchRun, ins_ts
from mend.wh_supplierroutine_warehousesupplier

select supplierRoutineID, warehouseSupplierID,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_supplierroutine_warehousesupplier_aud




------------ inventory$preference ------------

select *
from mend.wh_preference

select id, order_num,
	idETLBatchRun, ins_ts
from mend.wh_preference

select id, order_num,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_preference_aud

select id, order_num,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_preference_aud_hist




------------ inventory$warehouse_preference ------------

select preferenceid, warehouseid, 
	idETLBatchRun, ins_ts
from mend.wh_warehouse_preference

select preferenceid, warehouseid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehouse_preference_aud


------------ inventory$warehouse_country ------------

select preferenceid, countryid, 
	idETLBatchRun, ins_ts
from mend.wh_country_preference

select preferenceid, countryid, 
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_country_preference_aud