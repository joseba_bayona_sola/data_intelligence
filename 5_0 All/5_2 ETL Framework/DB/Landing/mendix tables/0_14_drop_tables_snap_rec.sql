
------------------------ Drop tables -------------------------------

use Landing
go

------------ snapreceipts$snapreceipt ------------

drop table mend.rec_snapreceipt;
go
drop table mend.rec_snapreceipt_aud;
go
drop table mend.rec_snapreceipt_aud_hist;
go

------------ snapreceipts$snapreceipt_receipt ------------

drop table mend.rec_snapreceipt_receipt;
go
drop table mend.rec_snapreceipt_receipt_aud;
go




------------ snapreceipts$snapreceiptline ------------

drop table mend.rec_snapreceiptline;
go
drop table mend.rec_snapreceiptline_aud;
go
drop table mend.rec_snapreceiptline_aud_hist;
go

------------ snapreceipts$snapreceiptline_snapreceipt ------------

drop table mend.rec_snapreceiptline_snapreceipt;
go
drop table mend.rec_snapreceiptline_snapreceipt_aud;
go

------------ snapreceipts$snapreceiptline_stockitem ------------

drop table mend.rec_snapreceiptline_stockitem;
go
drop table mend.rec_snapreceiptline_stockitem_aud;
go