use Landing
go 

------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorder --------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorder_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorder_aud

	select idETLBatchRun, count(*) num_rows
	from mend.purc_purchaseorder_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorder_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep, potype,
			id, 
			ordernumber, source, status, 
			leadtime, 
			itemcost, totalprice, formattedprice, 
			duedate, receiveddate, createddate, 
			approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby, 
			transferleadtime, transitleadtime, 
			spoleadtime, spoduedate, 
			tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate, 
			requiredshipdate, requiredtransferdate, 
			estimatedpaymentdate, estimateddepositdate,
			supplierreference,
			changeddate,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorder_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

---------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorder_supplier --------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorder_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

---------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorder_warehouse -------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorder_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun






---------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderlineheader -------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderlineheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorderlineheader_aud

	select idETLBatchRun, count(*) num_rows
	from mend.purc_purchaseorderlineheader_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorderlineheader_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			status, problems, 
			lines, items, totalprice, 
			createddate, changeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorderlineheader_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-----------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderlineheader_purchaseorder -------------------
-----------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderlineheader_purchaseorder_aud
	group by idETLBatchRun
	order by idETLBatchRun

-----------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderlineheader_productfamily -------------------
-----------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderlineheader_productfamily_aud
	group by idETLBatchRun
	order by idETLBatchRun

-----------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderlineheader_packsize ------------------------
-----------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderlineheader_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun

-----------------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderlineheader_supplierprices ------------------------
-----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderlineheader_supplierprices_aud
	group by idETLBatchRun
	order by idETLBatchRun






---------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderline -------------------------
---------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorderline_aud

	select idETLBatchRun, count(*) num_rows
	from mend.purc_purchaseorderline_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.purc_purchaseorderline_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			po_lineID, stockItemID,
			quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
			lineprice, 
			backtobackduedate,
			createdDate, changedDate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.purc_purchaseorderline_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

-----------------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderline_purchaseorderlineheader ------------------------
-----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderline_purchaseorderlineheader_aud
	group by idETLBatchRun
	order by idETLBatchRun

-----------------------------------------------------------------------------------------------------
---------------------- purchaseorders$purchaseorderline_stockitem -----------------------------------
-----------------------------------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.purc_purchaseorderline_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun