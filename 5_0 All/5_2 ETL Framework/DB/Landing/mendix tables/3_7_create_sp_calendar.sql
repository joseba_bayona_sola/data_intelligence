
------------------------------ SP -----------------------------------

use Landing
go

------------------- calendar$shippingmethod -------------------

drop procedure mend.srcmend_lnd_get_cal_shippingmethod
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_shippingmethod
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_cal_shippingmethod
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
					magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
					leadtime, letterRate1, letterRate2, tracked, invoicedocs,
					defaultweight, minweight, scurrimappable,
					mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
					mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
					mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
					mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id,
						magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
						leadtime, letterRate1, letterRate2, tracked, invoicedocs,
						defaultweight, minweight, scurrimappable,
						mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
						mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
						mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
						mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime 
					from public."calendar$shippingmethod"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- calendar$shippingmethods_warehouse -------------------

drop procedure mend.srcmend_lnd_get_cal_shippingmethods_warehouse
go

-- ============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ============================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_shippingmethods_warehouse
-- ============================================================================================

create procedure mend.srcmend_lnd_get_cal_shippingmethods_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select calendar$shippingmethodid shippingmethodid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "calendar$shippingmethodid", "inventory$warehouseid" 
					from public."calendar$shippingmethods_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- calendar$shippingmethods_destinationcountry -------------------

drop procedure mend.srcmend_lnd_get_cal_shippingmethods_destinationcountry
go

-- =====================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =====================================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_shippingmethods_destinationcountry
-- =====================================================================================================

create procedure mend.srcmend_lnd_get_cal_shippingmethods_destinationcountry
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select calendar$shippingmethodid shippingmethodid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "calendar$shippingmethodid", "countrymanagement$countryid" 
					from public."calendar$shippingmethods_destinationcountry"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------------- calendar$defaultcalendar -------------------

drop procedure mend.srcmend_lnd_get_cal_defaultcalendar
go

-- ==================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_defaultcalendar
-- ==================================================================================

create procedure mend.srcmend_lnd_get_cal_defaultcalendar
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
				mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
				timeHorizonWeeks,
				createdDate, changedDate, 
				system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
						mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
						timeHorizonWeeks,
						createdDate, changedDate, 
						"system$changedby" 
					from public."calendar$defaultcalendar"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- calendar$defaultcalendar_warehouse -------------------

drop procedure mend.srcmend_lnd_get_cal_defaultcalendar_warehouse
go

-- ============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ============================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_defaultcalendar_warehouse
-- ============================================================================================

create procedure mend.srcmend_lnd_get_cal_defaultcalendar_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select calendar$defaultcalendarid defaultcalendarid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "calendar$defaultcalendarid", "inventory$warehouseid" 
					from public."calendar$defaultcalendar_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------------- calendar$defaultwarehousesuppliercalender -------------------

drop procedure mend.srcmend_lnd_get_cal_defaultwarehousesuppliercalender
go

-- ===================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_defaultwarehousesuppliercalender
-- ===================================================================================================

create procedure mend.srcmend_lnd_get_cal_defaultwarehousesuppliercalender
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
				mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
				mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
				timeHorizonWeeks,
				createdDate, changedDate, 
				system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
						mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
						mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
						timeHorizonWeeks,
						createdDate, changedDate, 
						"system$changedby" 
					from public."calendar$defaultwarehousesuppliercalender"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- calendar$defaultwarehousesuppliercalender_warehouse -------------------

drop procedure mend.srcmend_lnd_get_cal_defaultwarehousesuppliercalender_warehouse
go

-- =============================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 02-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =============================================================================================================
-- Description: Reads data from Mendix Table through Open Query - cal_defaultwarehousesuppliercalender_warehouse
-- =============================================================================================================

create procedure mend.srcmend_lnd_get_cal_defaultwarehousesuppliercalender_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime2, @dateTo datetime2
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select defaultwarehousesuppliercalenderid, warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select "calendar$defaultwarehousesuppliercalenderid" defaultwarehousesuppliercalenderid, "inventory$warehouseid" warehouseid 
					from public."calendar$defaultwarehousesuppliercalender_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go