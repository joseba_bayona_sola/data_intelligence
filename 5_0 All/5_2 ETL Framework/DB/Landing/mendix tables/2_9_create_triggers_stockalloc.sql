
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- stockallocation$orderlinestockallocationtransaction -------------------

drop trigger mend.trg_all_orderlinestockallocationtransaction;
go 

create trigger mend.trg_all_orderlinestockallocationtransaction on mend.all_orderlinestockallocationtransaction
after insert
as
begin
	set nocount on

	merge into mend.all_orderlinestockallocationtransaction_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.transactionID, 0), isnull(trg.timestamp, ' '),
			isnull(trg.allocationType, ' '), isnull(trg.recordType, ' '),
			isnull(trg.allocatedUnits, 0), isnull(trg.allocatedStockQuantity, 0), isnull(trg.issuedQuantity, 0), isnull(trg.packSize, 0),
			isnull(trg.stockAllocated, ' '), isnull(trg.cancelled, ' '),
			isnull(trg.issuedDateTime, ' '), 
			isnull(trg.createddate, ' ')-- , isnull(trg.changeddate, ' ') 
		intersect
		select 
			isnull(src.transactionID, 0), isnull(src.timestamp, ' '),
			isnull(src.allocationType, ' '), isnull(src.recordType, ' '),
			isnull(src.allocatedUnits, 0), isnull(src.allocatedStockQuantity, 0), isnull(src.issuedQuantity, 0), isnull(src.packSize, 0),
			isnull(src.stockAllocated, ' '), isnull(src.cancelled, ' '),
			isnull(src.issuedDateTime, ' '), 
			isnull(src.createddate, ' ')-- , isnull(src.changeddate, ' ') 
			)
			
		then
			update set
				trg.transactionID = src.transactionID, trg.timestamp = src.timestamp,
				trg.allocationType = src.allocationType, trg.recordType = src.recordType,
				trg.allocatedUnits = src.allocatedUnits, trg.allocatedStockQuantity = src.allocatedStockQuantity, trg.issuedQuantity = src.issuedQuantity, trg.packSize = src.packSize,
				trg.stockAllocated = src.stockAllocated, trg.cancelled = src.cancelled,
				trg.issuedDateTime = src.issuedDateTime,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				transactionID, timestamp,
				allocationType, recordType, 
				allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
				stockAllocated, cancelled, 
				issuedDateTime, 
				createddate, changeddate,
				idETLBatchRun)

				values (src.id,
					src.transactionID, src.timestamp,
					src.allocationType, src.recordType, 
					src.allocatedUnits, src.allocatedStockQuantity, src.issuedQuantity, src.packSize, 
					src.stockAllocated, src.cancelled, 
					src.issuedDateTime,
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_all_orderlinestockallocationtransaction_aud;
go 

create trigger mend.trg_all_orderlinestockallocationtransaction_aud on mend.all_orderlinestockallocationtransaction_aud
for update, delete
as
begin
	set nocount on

	insert mend.all_orderlinestockallocationtransaction_aud_hist (id,
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime,
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.transactionID, d.timestamp,
			d.allocationType, d.recordType, 
			d.allocatedUnits, d.allocatedStockQuantity, d.issuedQuantity, d.packSize, 
			d.stockAllocated, d.cancelled, 
			d.issuedDateTime,
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ stockallocation$at_order ------------

drop trigger mend.trg_all_at_order;
go 

create trigger mend.trg_all_at_order on mend.all_at_order
after insert
as
begin
	set nocount on

	merge into mend.all_at_order_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, orderid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.orderid,
					src.idETLBatchRun);
end; 
go

------------ stockallocation$at_orderline ------------

drop trigger mend.trg_all_at_orderline;
go 

create trigger mend.trg_all_at_orderline on mend.all_at_orderline
after insert
as
begin
	set nocount on

	merge into mend.all_at_orderline_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.orderlineid = src.orderlineid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, orderlineid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.orderlineid,
					src.idETLBatchRun);
end; 
go

------------ stockallocation$at_warehouse ------------

drop trigger mend.trg_all_at_warehouse;
go 

create trigger mend.trg_all_at_warehouse on mend.all_at_warehouse
after insert
as
begin
	set nocount on

	merge into mend.all_at_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, warehouseid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------ stockallocation$at_warehousestockitem ------------

drop trigger mend.trg_all_at_warehousestockitem;
go 

create trigger mend.trg_all_at_warehousestockitem on mend.all_at_warehousestockitem
after insert
as
begin
	set nocount on

	merge into mend.all_at_warehousestockitem_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.warehousestockitemid = src.warehousestockitemid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, warehousestockitemid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.warehousestockitemid,
					src.idETLBatchRun);
end; 
go


------------ stockallocation$at_batch ------------

drop trigger mend.trg_all_at_batch;
go 

create trigger mend.trg_all_at_batch on mend.all_at_batch
after insert
as
begin
	set nocount on

	merge into mend.all_at_batch_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.warehousestockitembatchid = src.warehousestockitembatchid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, warehousestockitembatchid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.warehousestockitembatchid,
					src.idETLBatchRun);
end; 
go


------------ stockallocation$at_packsize ------------

drop trigger mend.trg_all_at_packsize;
go 

create trigger mend.trg_all_at_packsize on mend.all_at_packsize
after insert
as
begin
	set nocount on

	merge into mend.all_at_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.packsizeid = src.packsizeid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, packsizeid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.packsizeid,
					src.idETLBatchRun);
end; 
go

------------ stockallocatio$orderlinestockallocationtransa_customershipmentl ------------

drop trigger mend.trg_all_orderlinestockallocationtransa_customershipmentl;
go 

create trigger mend.trg_all_orderlinestockallocationtransa_customershipmentl on mend.all_orderlinestockallocationtransa_customershipmentl
after insert
as
begin
	set nocount on

	merge into mend.all_orderlinestockallocationtransa_customershipmentl_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid and trg.customershipmentlineid = src.customershipmentlineid)
	when not matched 
		then
			insert (orderlinestockallocationtransactionid, customershipmentlineid, 
				idETLBatchRun)

				values (src.orderlinestockallocationtransactionid, src.customershipmentlineid,
					src.idETLBatchRun);
end; 
go





------------ stockallocation$magentoallocation ------------

drop trigger mend.trg_all_magentoallocation;
go 

create trigger mend.trg_all_magentoallocation on mend.all_magentoallocation
after insert
as
begin
	set nocount on

	merge into mend.all_magentoallocation_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.shipped, ' '), isnull(trg.cancelled, ' '),
			isnull(trg.quantity, 0), 
			isnull(trg.createddate, ' ')-- , isnull(trg.changeddate, ' ') 
		intersect
		select 
			isnull(src.shipped, ' '), isnull(src.cancelled, ' '),
			isnull(src.quantity, 0), 
			isnull(src.createddate, ' ')-- , isnull(src.changeddate, ' ') 
			)
			
		then
			update set
				trg.shipped = src.shipped, trg.cancelled = src.cancelled,
				trg.quantity = src.quantity,
				trg.createddate = src.createddate, trg.changeddate = src.changeddate,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				shipped, cancelled, 
				quantity,
				createddate, changeddate,
				idETLBatchRun)

				values (src.id,
					src.shipped, src.cancelled, 
					src.quantity,
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_all_magentoallocation_aud;
go 

create trigger mend.trg_all_magentoallocation_aud on mend.all_magentoallocation_aud
for update, delete
as
begin
	set nocount on

	insert mend.all_magentoallocation_aud_hist (id,
		shipped, cancelled, 
		quantity,
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, 
			d.shipped, d.cancelled, 
			d.quantity,
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ stockallocatio$magentoallocatio_orderlinestockallocationtransac ------------

drop trigger mend.trg_all_magentoallocatio_orderlinestockallocationtransac;
go 

create trigger mend.trg_all_magentoallocatio_orderlinestockallocationtransac on mend.all_magentoallocatio_orderlinestockallocationtransac
after insert
as
begin
	set nocount on

	merge into mend.all_magentoallocatio_orderlinestockallocationtransac_aud with (tablock) as trg
	using inserted src
		on (trg.magentoallocationid = src.magentoallocationid and trg.orderlinestockallocationtransactionid = src.orderlinestockallocationtransactionid)
	when not matched 
		then
			insert (magentoallocationid, orderlinestockallocationtransactionid, 
				idETLBatchRun)

				values (src.magentoallocationid, src.orderlinestockallocationtransactionid,
					src.idETLBatchRun);
end; 
go



------------ stockallocation$magentoallocation_orderlinemagento ------------

drop trigger mend.trg_all_magentoallocation_orderlinemagento;
go 

create trigger mend.trg_all_magentoallocation_orderlinemagento on mend.all_magentoallocation_orderlinemagento
after insert
as
begin
	set nocount on

	merge into mend.all_magentoallocation_orderlinemagento_aud with (tablock) as trg
	using inserted src
		on (trg.magentoallocationid = src.magentoallocationid and trg.orderlinemagentoid = src.orderlinemagentoid)
	when not matched 
		then
			insert (magentoallocationid, orderlinemagentoid, 
				idETLBatchRun)

				values (src.magentoallocationid, src.orderlinemagentoid,
					src.idETLBatchRun);
end; 
go


------------ stockallocation$magentoallocation_warehouse ------------

drop trigger mend.trg_all_magentoallocation_warehouse;
go 

create trigger mend.trg_all_magentoallocation_warehouse on mend.all_magentoallocation_warehouse
after insert
as
begin
	set nocount on

	merge into mend.all_magentoallocation_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.magentoallocationid = src.magentoallocationid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (magentoallocationid, warehouseid, 
				idETLBatchRun)

				values (src.magentoallocationid, src.warehouseid,
					src.idETLBatchRun);
end; 
go
