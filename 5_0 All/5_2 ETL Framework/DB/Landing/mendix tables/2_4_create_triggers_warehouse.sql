
------------------------------- Triggers ----------------------------------

use Landing
go

------------ inventory$warehouse ------------

drop trigger mend.trg_wh_warehouse;
go 

create trigger mend.trg_wh_warehouse on mend.wh_warehouse
after insert
as
begin
	set nocount on

	merge into mend.wh_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.code, 0), isnull(trg.name, ' '), isnull(trg.shortName, ' '), isnull(trg.warehouseType, ' '), isnull(trg.snapWarehouse, 0),
			isnull(trg.snapCode, ' '), isnull(trg.scurriCode, ' '),
			isnull(trg.active, 0), isnull(trg.wms_active, 0), isnull(trg.useHoldingLabels, 0), isnull(trg.stockcheckignore, 0)
		intersect
		select 
			isnull(src.code, 0), isnull(src.name, ' '), isnull(src.shortName, ' '), isnull(src.warehouseType, ' '), isnull(src.snapWarehouse, 0),
			isnull(src.snapCode, ' '), isnull(src.scurriCode, ' '),
			isnull(src.active, 0), isnull(src.wms_active, 0), isnull(src.useHoldingLabels, 0), isnull(src.stockcheckignore, 0))	
		then
			update set
				trg.code = src.code, trg.name = src.name, trg.shortName = src.shortName, trg.warehouseType = src.warehouseType, trg.snapWarehouse = src.snapWarehouse,
				trg.snapCode = src.snapCode, trg.scurriCode = src.scurriCode, 
				trg.active = src.active, trg.wms_active = src.wms_active, trg.useHoldingLabels = src.useHoldingLabels, trg.stockcheckignore = src.stockcheckignore, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				code, name, shortName, warehouseType, snapWarehouse, 
				snapCode, scurriCode, 
				active, wms_active, useHoldingLabels, stockcheckignore,
				idETLBatchRun)

				values (src.id,
					src.code, src.name, src.shortName, src.warehouseType, src.snapWarehouse, 
					src.snapCode, src.scurriCode, 
					src.active, src.wms_active, src.useHoldingLabels, src.stockcheckignore,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_warehouse_aud;
go 

create trigger mend.trg_wh_warehouse_aud on mend.wh_warehouse_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_warehouse_aud_hist (id,
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.code, d.name, d.shortName, d.warehouseType, d.snapWarehouse,
			d.snapCode, d.scurriCode, 
			d.active, d.wms_active, d.useHoldingLabels, d.stockcheckignore,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go



------------ inventory$warehousesupplier_warehouse ------------

drop trigger mend.trg_wh_warehousesupplier_warehouse;
go 

create trigger mend.trg_wh_warehousesupplier_warehouse on mend.wh_warehousesupplier_warehouse
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousesupplier_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.warehousesupplierid = src.warehousesupplierid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (warehousesupplierid, warehouseid, 
				idETLBatchRun)

				values (src.warehousesupplierid, src.warehouseid,
					src.idETLBatchRun);
end; 
go



------------ inventory$warehousesupplier_supplier ------------

drop trigger mend.trg_wh_warehousesupplier_supplier;
go 

create trigger mend.trg_wh_warehousesupplier_supplier on mend.wh_warehousesupplier_supplier
after insert
as
begin
	set nocount on

	merge into mend.wh_warehousesupplier_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.warehousesupplierid = src.warehousesupplierid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (warehousesupplierid, supplierid, 
				idETLBatchRun)

				values (src.warehousesupplierid, src.supplierid,
					src.idETLBatchRun);
end; 
go








------------ inventory$supplierroutine ------------

drop trigger mend.trg_wh_supplierroutine;
go 

create trigger mend.trg_wh_supplierroutine on mend.wh_supplierroutine
after insert
as
begin
	set nocount on

	merge into mend.wh_supplierroutine_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select
			isnull(trg.supplierName, ' '), 
			isnull(trg.mondayDefaultCanSupply, 0), isnull(trg.mondayOrderCutOffTime, ' '), isnull(trg.mondayDefaultCanOrder, 0),
			isnull(trg.tuesdayDefaultCanSupply, 0), isnull(trg.tuesdayOrderCutOffTime, ' '), isnull(trg.tuesdayDefaultCanOrder, 0),
			isnull(trg.wednesdayDefaultCanSupply, 0), isnull(trg.wednesdayOrderCutOffTime, ' '), isnull(trg.wednesdayDefaultCanOrder, 0),
			isnull(trg.thursdayDefaultCanSupply, 0), isnull(trg.thursdayOrderCutOffTime, ' '), isnull(trg.thursdayDefaultCanOrder, 0),
			isnull(trg.fridayDefaultCanSupply, 0), isnull(trg.fridayOrderCutOffTime, ' '), isnull(trg.fridayDefaultCanOrder, 0),
			isnull(trg.saturdayDefaultCanSupply, 0), isnull(trg.saturdayOrderCutOffTime, ' '), isnull(trg.saturdayDefaultCanOrder, 0),
			isnull(trg.sundayDefaultCanSupply, 0), isnull(trg.sundayOrderCutOffTime, ' '), isnull(trg.sundayDefaultCanOrder, 0)
		intersect
		select
			isnull(src.supplierName, ' '), 
			isnull(src.mondayDefaultCanSupply, 0), isnull(src.mondayOrderCutOffTime, ' '), isnull(src.mondayDefaultCanOrder, 0),
			isnull(src.tuesdayDefaultCanSupply, 0), isnull(src.tuesdayOrderCutOffTime, ' '), isnull(src.tuesdayDefaultCanOrder, 0),
			isnull(src.wednesdayDefaultCanSupply, 0), isnull(src.wednesdayOrderCutOffTime, ' '), isnull(src.wednesdayDefaultCanOrder, 0),
			isnull(src.thursdayDefaultCanSupply, 0), isnull(src.thursdayOrderCutOffTime, ' '), isnull(src.thursdayDefaultCanOrder, 0),
			isnull(src.fridayDefaultCanSupply, 0), isnull(src.fridayOrderCutOffTime, ' '), isnull(src.fridayDefaultCanOrder, 0),
			isnull(src.saturdayDefaultCanSupply, 0), isnull(src.saturdayOrderCutOffTime, ' '), isnull(src.saturdayDefaultCanOrder, 0),
			isnull(src.sundayDefaultCanSupply, 0), isnull(src.sundayOrderCutOffTime, ' '), isnull(src.sundayDefaultCanOrder, 0))	
		then
			update set
				trg.supplierName = src.supplierName,
				trg.mondayDefaultCanSupply = src.mondayDefaultCanSupply, trg.mondayOrderCutOffTime = src.mondayOrderCutOffTime, trg.mondayDefaultCanOrder = src.mondayDefaultCanOrder,
				trg.tuesdayDefaultCanSupply = src.tuesdayDefaultCanSupply, trg.tuesdayOrderCutOffTime = src.tuesdayOrderCutOffTime, trg.tuesdayDefaultCanOrder = src.tuesdayDefaultCanOrder,
				trg.wednesdayDefaultCanSupply = src.wednesdayDefaultCanSupply, trg.wednesdayOrderCutOffTime = src.wednesdayOrderCutOffTime, trg.wednesdayDefaultCanOrder = src.wednesdayDefaultCanOrder,
				trg.thursdayDefaultCanSupply = src.thursdayDefaultCanSupply, trg.thursdayOrderCutOffTime = src.thursdayOrderCutOffTime, trg.thursdayDefaultCanOrder = src.thursdayDefaultCanOrder,
				trg.fridayDefaultCanSupply = src.fridayDefaultCanSupply, trg.fridayOrderCutOffTime = src.fridayOrderCutOffTime, trg.fridayDefaultCanOrder = src.fridayDefaultCanOrder,
				trg.saturdayDefaultCanSupply = src.saturdayDefaultCanSupply, trg.saturdayOrderCutOffTime = src.saturdayOrderCutOffTime, trg.saturdayDefaultCanOrder = src.saturdayDefaultCanOrder,
				trg.sundayDefaultCanSupply = src.sundayDefaultCanSupply, trg.sundayOrderCutOffTime = src.sundayOrderCutOffTime, trg.sundayDefaultCanOrder = src.sundayDefaultCanOrder,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				supplierName,
				mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
				tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
				wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
				thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
				fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
				saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
				sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
				idETLBatchRun)

				values (src.id,
					src.supplierName,
					src.mondayDefaultCanSupply, src.mondayOrderCutOffTime, src.mondayDefaultCanOrder, 
					src.tuesdayDefaultCanSupply, src.tuesdayOrderCutOffTime, src.tuesdayDefaultCanOrder, 
					src.wednesdayDefaultCanSupply, src.wednesdayOrderCutOffTime, src.wednesdayDefaultCanOrder, 
					src.thursdayDefaultCanSupply, src.thursdayOrderCutOffTime, src.thursdayDefaultCanOrder, 
					src.fridayDefaultCanSupply, src.fridayOrderCutOffTime, src.fridayDefaultCanOrder,  
					src.saturdayDefaultCanSupply, src.saturdayOrderCutOffTime, src.saturdayDefaultCanOrder,
					src.sundayDefaultCanSupply, src.sundayOrderCutOffTime, src.sundayDefaultCanOrder,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_supplierroutine_aud;
go 

create trigger mend.trg_wh_supplierroutine_aud on mend.wh_supplierroutine_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_supplierroutine_aud_hist (id,
		supplierName,
		mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
		tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
		wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
		thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
		fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
		saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
		sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.supplierName,
			d.mondayDefaultCanSupply, d.mondayOrderCutOffTime, d.mondayDefaultCanOrder, 
			d.tuesdayDefaultCanSupply, d.tuesdayOrderCutOffTime, d.tuesdayDefaultCanOrder, 
			d.wednesdayDefaultCanSupply, d.wednesdayOrderCutOffTime, d.wednesdayDefaultCanOrder, 
			d.thursdayDefaultCanSupply, d.thursdayOrderCutOffTime, d.thursdayDefaultCanOrder, 
			d.fridayDefaultCanSupply, d.fridayOrderCutOffTime, d.fridayDefaultCanOrder,  
			d.saturdayDefaultCanSupply, d.saturdayOrderCutOffTime, d.saturdayDefaultCanOrder,
			d.sundayDefaultCanSupply, d.sundayOrderCutOffTime, d.sundayDefaultCanOrder, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ inventory$supplierroutine_warehousesupplier ------------

drop trigger mend.trg_wh_supplierroutine_warehousesupplier;
go 

create trigger mend.trg_wh_supplierroutine_warehousesupplier on mend.wh_supplierroutine_warehousesupplier
after insert
as
begin
	set nocount on

	merge into mend.wh_supplierroutine_warehousesupplier_aud with (tablock) as trg
	using inserted src
		on (trg.supplierRoutineID = src.supplierRoutineID and trg.warehouseSupplierID = src.warehouseSupplierID)
	when not matched 
		then
			insert (supplierRoutineID, warehouseSupplierID, 
				idETLBatchRun)

				values (src.supplierRoutineID, src.warehouseSupplierID,
					src.idETLBatchRun);
end; 
go




------------ inventory$preference ------------

drop trigger mend.trg_wh_preference;
go 

create trigger mend.trg_wh_preference on mend.wh_preference
after insert
as
begin
	set nocount on

	merge into mend.wh_preference_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.order_num, 0)
		intersect
		select 
			isnull(src.order_num, 0))	
		then
			update set
				trg.order_num = src.order_num, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, order_num,
				idETLBatchRun)

				values (src.id, src.order_num,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_wh_preference_aud;
go 

create trigger mend.trg_wh_preference_aud on mend.wh_preference_aud
for update, delete
as
begin
	set nocount on

	insert mend.wh_preference_aud_hist (id, order_num,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.order_num,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go



------------ inventory$warehouse_preference ------------

drop trigger mend.trg_wh_warehouse_preference;
go 

create trigger mend.trg_wh_warehouse_preference on mend.wh_warehouse_preference
after insert
as
begin
	set nocount on

	merge into mend.wh_warehouse_preference_aud with (tablock) as trg
	using inserted src
		on (trg.preferenceid = src.preferenceid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (preferenceid, warehouseid, 
				idETLBatchRun)

				values (src.preferenceid, src.warehouseid,
					src.idETLBatchRun);
end; 
go


------------ inventory$country_preference ------------

drop trigger mend.trg_wh_country_preference;
go 

create trigger mend.trg_wh_country_preference on mend.wh_country_preference
after insert
as
begin
	set nocount on

	merge into mend.wh_country_preference_aud with (tablock) as trg
	using inserted src
		on (trg.preferenceid = src.preferenceid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (preferenceid, countryid, 
				idETLBatchRun)

				values (src.preferenceid, src.countryid,
					src.idETLBatchRun);
end; 
go


