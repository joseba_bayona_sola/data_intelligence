use Landing
go 

------------------- orderprocessing$order -------------------

drop view mend.order_order_aud_v
go

create view mend.order_order_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		incrementID, magentoOrderID, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
		allocatedDate, promisedShippingDate, promisedDeliveryDate,
		labelRenderer,  
		couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
		createdDate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			incrementID, magentoOrderID, orderStatus, 
			orderLinesMagento, orderLinesDeduped,
			_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
			allocatedDate, promisedShippingDate, promisedDeliveryDate,
			labelRenderer,  
			couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
			createdDate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_order_aud
		union
		select 'H' record_type, id, 
			incrementID, magentoOrderID, orderStatus, 
			orderLinesMagento, orderLinesDeduped,
			_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
			allocatedDate, promisedShippingDate, promisedDeliveryDate,
			labelRenderer,  
			couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
			createdDate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_order_aud_hist) t
go


------------ orderprocessing$basevalues ------------

drop view mend.order_basevalues_aud_v
go

create view mend.order_basevalues_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
		discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
		shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
		adjustmentNegative, adjustmentPositive, 				
		custBalanceAmount, 
		customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
		grandTotal, totalInvoiced, totalCanceled, totalRefunded,
		toOrderRate, toGlobalRate, currencyCode, 
		mutualamount,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
			discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
			shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
			adjustmentNegative, adjustmentPositive, 				
			custBalanceAmount, 
			customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
			grandTotal, totalInvoiced, totalCanceled, totalRefunded,
			toOrderRate, toGlobalRate, currencyCode, 
			mutualamount,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_basevalues_aud
		union
		select 'H' record_type, id, 
			subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
			discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
			shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
			adjustmentNegative, adjustmentPositive, 				
			custBalanceAmount, 
			customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
			grandTotal, totalInvoiced, totalCanceled, totalRefunded,
			toOrderRate, toGlobalRate, currencyCode, 
			mutualamount,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_basevalues_aud_hist) t
go



------------ orderprocessing$order_statusupdate ------------

drop view mend.order_order_statusupdate_aud_v
go

create view mend.order_order_statusupdate_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		status, timestamp, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			status, timestamp, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_order_statusupdate_aud
		union
		select 'H' record_type, id, 
			status, timestamp, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_order_statusupdate_aud_hist) t
go

------------ orderprocessing$orderprocessingtransaction ------------

drop view mend.order_orderprocessingtransaction_aud_v
go

create view mend.order_orderprocessingtransaction_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		orderReference, status, detail, timestamp, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			orderReference, status, detail, timestamp, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_orderprocessingtransaction_aud
		union
		select 'H' record_type, id, 
			orderReference, status, detail, timestamp, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderprocessingtransaction_aud_hist) t
go



------------ orderprocessing$orderlineabstract ------------

drop view mend.order_orderlineabstract_aud_v
go

create view mend.order_orderlineabstract_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		magentoItemID, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName,
		sku, eye, lensGroupID, 
		netPrice, netRowPrice,
		createdDate, changedDate, 
		system$owner, system$changedBy, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			magentoItemID, 
			quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
			basePrice, baseRowPrice, 
			allocated, 
			orderLineType, subMetaObjectName,
			sku, eye, lensGroupID, 
			netPrice, netRowPrice,
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_orderlineabstract_aud
		union
		select 'H' record_type, id, 
			magentoItemID, 
			quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
			basePrice, baseRowPrice, 
			allocated, 
			orderLineType, subMetaObjectName,
			sku, eye, lensGroupID, 
			netPrice, netRowPrice,
			createdDate, changedDate, 
			system$owner, system$changedBy,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderlineabstract_aud_hist) t
go

------------ orderprocessing$orderlinemagento ------------

drop view mend.order_orderlinemagento_aud_v
go 

create view mend.order_orderlinemagento_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		lineAdded, deduplicate, fulfilled, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			lineAdded, deduplicate, fulfilled, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_orderlinemagento_aud
		union
		select 'H' record_type, id, 
			lineAdded, deduplicate, fulfilled,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderlinemagento_aud_hist) t
go

------------ orderprocessing$orderline ------------

drop view mend.order_orderline_aud_v
go 

create view mend.order_orderline_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		status, packsOrdered, packsAllocated, packsShipped, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			status, packsOrdered, packsAllocated, packsShipped, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_orderline_aud
		union
		select 'H' record_type, id, 
			status, packsOrdered, packsAllocated, packsShipped,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_orderline_aud_hist) t
go



------------ orderprocessing$shippinggroup ------------

drop view mend.order_shippinggroup_aud_v
go

create view mend.order_shippinggroup_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		dispensingFee, 
		allocationStatus, shipmentStatus, 
		wholesale, 
		letterBoxAble, onHold, adHoc, cancelled,
		createddate, changeddate,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			dispensingFee, 
			allocationStatus, shipmentStatus, 
			wholesale, 
			letterBoxAble, onHold, adHoc, cancelled,
			createddate, changeddate,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.order_shippinggroup_aud
		union
		select 'H' record_type, id, 
			dispensingFee, 
			allocationStatus, shipmentStatus, 
			wholesale, 
			letterBoxAble, onHold, adHoc, cancelled,
			createddate, changeddate,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.order_shippinggroup_aud_hist) t
go