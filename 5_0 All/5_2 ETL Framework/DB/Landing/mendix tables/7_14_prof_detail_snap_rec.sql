use Landing
go 

------------------------------------------------------------------------
----------------------- snapreceipts$snapreceipt -----------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.rec_snapreceipt_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.rec_snapreceipt_aud

	select idETLBatchRun, count(*) num_rows
	from mend.rec_snapreceipt_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.rec_snapreceipt_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			lines, lineqty, 	
			receiptStatus, receiptprocessStatus, processDetail,  priority, 
			status, stockstatus, 
			ordertype, orderclass, 
			suppliername, consignmentID, 
			weight, actualWeight, volume, 
			dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
			datesfixed, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.rec_snapreceipt_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- snapreceipts$snapreceipt_receipt --------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.rec_snapreceipt_receipt_aud
	group by idETLBatchRun
	order by idETLBatchRun







------------------------------------------------------------------------
----------------------- snapreceipts$snapreceiptline -------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.rec_snapreceiptline_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.rec_snapreceiptline_aud

	select idETLBatchRun, count(*) num_rows
	from mend.rec_snapreceiptline_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.rec_snapreceiptline_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			line, skuid, 
			status, level, unitofmeasure, 
			qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.rec_snapreceiptline_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
-------------- snapreceipts$snapreceiptline_snapreceipt ----------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.rec_snapreceiptline_snapreceipt_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
-------------- snapreceipts$snapreceiptline_stockitem ------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.rec_snapreceiptline_stockitem_aud
	group by idETLBatchRun
	order by idETLBatchRun