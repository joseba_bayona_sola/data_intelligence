
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ intersitetransfer$transferheader ------------

select *
from mend.inter_transferheader

select id, 
	transfernum, allocatedstrategy, 
	totalremaining,
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.inter_transferheader

select id, 
	transfernum, allocatedstrategy, 
	totalremaining,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_transferheader_aud

select id, 
	transfernum, allocatedstrategy, 
	totalremaining,
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.inter_transferheader_aud_hist

------------ inter_supplypo_transferheader ------------

select *
from mend.inter_supplypo_transferheader

select transferheaderid, purchaseorderid,
	idETLBatchRun, ins_ts
from mend.inter_supplypo_transferheader

select transferheaderid, purchaseorderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_supplypo_transferheader_aud

------------ purchaseorders$transferpo_transferheader ------------

select *
from mend.inter_transferpo_transferheader

select transferheaderid, purchaseorderid,
	idETLBatchRun, ins_ts
from mend.inter_transferpo_transferheader

select transferheaderid, purchaseorderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_transferpo_transferheader_aud


------------ orderprocessing$order_transferheader ------------

select *
from mend.inter_order_transferheader

select orderid, transferheaderid,
	idETLBatchRun, ins_ts
from mend.inter_order_transferheader

select orderid, transferheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_order_transferheader_aud








------------------- intersitetransfer$intransitbatchheader -------------------

select *
from mend.inter_intransitbatchheader

select id, createddate,
	idETLBatchRun, ins_ts
from mend.inter_intransitbatchheader

select id, createddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitbatchheader_aud

select id, createddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.inter_intransitbatchheader_aud_hist

------------------- intersitetransfer$intransitbatchheader_transferheader -------------------

select *
from mend.inter_intransitbatchheader_transferheader

select intransitbatchheaderid, transferheaderid,
	idETLBatchRun, ins_ts
from mend.inter_intransitbatchheader_transferheader

select intransitbatchheaderid, transferheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitbatchheader_transferheader_aud

------------------- intersitetransfer$intransitbatchheader_customershipment -------------------

select *
from mend.inter_intransitbatchheader_customershipment

select intransitbatchheaderid, customershipmentid,
	idETLBatchRun, ins_ts
from mend.inter_intransitbatchheader_customershipment

select intransitbatchheaderid, customershipmentid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitbatchheader_customershipment_aud









------------------- intersitetransfer$intransitstockitembatch -------------------

select *
from mend.inter_intransitstockitembatch

select id, 
	issuedquantity, remainingquantity, 
	used,
	productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
	currency,
	groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
	createddate,
	idETLBatchRun, ins_ts
from mend.inter_intransitstockitembatch

select id, 
	issuedquantity, remainingquantity, 
	used,
	productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
	currency,
	groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
	createddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitstockitembatch_aud

select id, 
	issuedquantity, remainingquantity, 
	used,
	productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
	currency,
	groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
	createddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.inter_intransitstockitembatch_aud_hist

------------------- intersitetransfer$intransitstockitembatch_transferheader -------------------

select *
from mend.inter_intransitstockitembatch_transferheader

select intransitstockitembatchid, transferheaderid,
	idETLBatchRun, ins_ts
from mend.inter_intransitstockitembatch_transferheader

select intransitstockitembatchid, transferheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitstockitembatch_transferheader_aud

------------------- intersitetransfer$intransitstockitembatch_intransitbatchheader -------------------

select *
from mend.inter_intransitstockitembatch_intransitbatchheader

select intransitstockitembatchid, intransitbatchheaderid,
	idETLBatchRun, ins_ts
from mend.inter_intransitstockitembatch_intransitbatchheader

select intransitstockitembatchid, intransitbatchheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitstockitembatch_intransitbatchheader_aud

------------------- intersitetransfer$intransitstockitembatch_stockitem -------------------

select *
from mend.inter_intransitstockitembatch_stockitem

select intransitstockitembatchid, stockitemid,
	idETLBatchRun, ins_ts
from mend.inter_intransitstockitembatch_stockitem

select intransitstockitembatchid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitstockitembatch_stockitem_aud

------------------- intersitetransfer$intransitstockitembatch_receipt -------------------

select *
from mend.inter_intransitstockitembatch_receipt

select intransitstockitembatchid, receiptid,
	idETLBatchRun, ins_ts
from mend.inter_intransitstockitembatch_receipt

select intransitstockitembatchid, receiptid,
	idETLBatchRun, ins_ts, upd_ts
from mend.inter_intransitstockitembatch_receipt_aud
