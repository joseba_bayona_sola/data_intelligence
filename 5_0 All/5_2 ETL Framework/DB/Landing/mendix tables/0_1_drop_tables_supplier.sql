use Landing
go

------------------------ Drop tables -------------------------------

--------------------- suppliers$supplier ----------------------------

drop table mend.supp_supplier;
go
drop table mend.supp_supplier_aud;
go
drop table mend.supp_supplier_aud_hist;
go

--------------------- suppliers$supplier_country ----------------------------

drop table mend.supp_supplier_country;
go
drop table mend.supp_supplier_country_aud;
go

------------------- suppliers$supplier_company -------------------------

drop table mend.supp_supplier_company;
go
drop table mend.supp_supplier_company_aud;
go

------------------- suppliers$supplier_currency -------------------------

drop table mend.supp_supplier_currency;
go
drop table mend.supp_supplier_currency_aud;
go





------------------- suppliers$supplierprice -------------------------

drop table mend.supp_supplierprice;
go
drop table mend.supp_supplierprice_aud;
go
drop table mend.supp_supplierprice_aud_hist;
go

------------------- suppliers$standardprice -------------------------

drop table mend.supp_standardprice;
go
drop table mend.supp_standardprice_aud;
go
drop table mend.supp_standardprice_aud_hist;
go

------------------- suppliers$supplierprices_supplier -------------------------

drop table mend.supp_supplierprices_supplier;
go
drop table mend.supp_supplierprices_supplier_aud;
go

------------------- suppliers$supplierprices_packsize -------------------------

drop table mend.supp_supplierprices_packsize;
go
drop table mend.supp_supplierprices_packsize_aud;
go




--------------------- suppliers$manufacturer ----------------------------

drop table mend.supp_manufacturer;
go
drop table mend.supp_manufacturer_aud;
go
drop table mend.supp_manufacturer_aud_hist;
go

--------------------- product$packsize_manufacturer ----------------------------

drop table mend.supp_packsize_manufacturer;
go
drop table mend.supp_packsize_manufacturer_aud;
go