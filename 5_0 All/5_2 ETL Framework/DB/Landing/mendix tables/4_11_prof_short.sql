
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ purchaseorders$shortageheader ------------

select *
from mend.short_shortageheader

select id, wholesale,
	idETLBatchRun, ins_ts
from mend.short_shortageheader

select id, wholesale,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortageheader_aud

select id, wholesale,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.short_shortageheader_aud_hist

------------ purchaseorders$shortageheader_supplier ------------

select *
from mend.short_shortageheader_supplier

select shortageheaderid, supplierid,
	idETLBatchRun, ins_ts
from mend.short_shortageheader_supplier

select shortageheaderid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortageheader_supplier_aud

------------ purchaseorders$shortageheader_sourcewarehouse ------------

select *
from mend.short_shortageheader_sourcewarehouse

select shortageheaderid, warehouseid,
	idETLBatchRun, ins_ts
from mend.short_shortageheader_sourcewarehouse

select shortageheaderid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortageheader_sourcewarehouse_aud

------------ purchaseorders$shortageheader_destinationwarehouse ------------

select *
from mend.short_shortageheader_destinationwarehouse

select shortageheaderid, warehouseid,
	idETLBatchRun, ins_ts
from mend.short_shortageheader_destinationwarehouse

select shortageheaderid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortageheader_destinationwarehouse_aud

------------------- purchaseorders$shortageheader_packsize -------------------

select *
from mend.short_shortageheader_packsize

select shortageheaderid, packsizeid,
	idETLBatchRun, ins_ts
from mend.short_shortageheader_packsize

select shortageheaderid, packsizeid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortageheader_packsize_aud








------------------- purchaseorders$shortage -------------------

select *
from mend.short_shortage

select id, 
	shortageid, purchaseordernumber, ordernumbers,
	wholesale, 
	status, 
	unitquantity, packquantity, 
	requireddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts
from mend.short_shortage

select id, 
	shortageid, purchaseordernumber, ordernumbers,
	wholesale, 
	status, 
	unitquantity, packquantity, 
	requireddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_aud

select id, 
	shortageid, purchaseordernumber, ordernumbers,
	wholesale, 
	status, 
	unitquantity, packquantity, 
	requireddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.short_shortage_aud_hist

------------------- purchaseorders$shortage_shortageheader -------------------

select *
from mend.short_shortage_shortageheader

select shortageid, shortageheaderid,
	idETLBatchRun, ins_ts
from mend.short_shortage_shortageheader

select shortageid, shortageheaderid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_shortageheader_aud

------------------- purchaseorders$shortage_purchaseorderline -------------------

select *
from mend.short_shortage_purchaseorderline

select shortageid, purchaseorderlineid,
	idETLBatchRun, ins_ts
from mend.short_shortage_purchaseorderline

select shortageid, purchaseorderlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_purchaseorderline_aud

------------------- purchaseorders$shortage_supplier -------------------

select *
from mend.short_shortage_supplier

select shortageid, supplierid,
	idETLBatchRun, ins_ts
from mend.short_shortage_supplier

select shortageid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_supplier_aud

------------------- purchaseorders$shortage_product -------------------

select *
from mend.short_shortage_product

select shortageid, productid,
	idETLBatchRun, ins_ts
from mend.short_shortage_product

select shortageid, productid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_product_aud

------------------- purchaseorders$shortage_standardprice -------------------

select *
from mend.short_shortage_standardprice

select shortageid, standardpriceid,
	idETLBatchRun, ins_ts
from mend.short_shortage_standardprice

select shortageid, standardpriceid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_standardprice_aud

------------------- purchaseorders$shortage_stockitem -------------------

select *
from mend.short_shortage_stockitem

select shortageid, stockitemid,
	idETLBatchRun, ins_ts
from mend.short_shortage_stockitem

select shortageid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_stockitem_aud

------------------- purchaseorders$shortage_warehousestockitem -------------------

select *
from mend.short_shortage_warehousestockitem

select shortageid, warehousestockitemid,
	idETLBatchRun, ins_ts
from mend.short_shortage_warehousestockitem

select shortageid, warehousestockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_shortage_warehousestockitem_aud






------------------- purchaseorders$orderlineshortage -------------------

select *
from mend.short_orderlineshortage

select id, 
	orderlineshortageid,
	orderlineshortagetype,
	unitquantity, packquantity,
	shortageprogress, adviseddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts
from mend.short_orderlineshortage

select id, 
	orderlineshortageid,
	orderlineshortagetype,
	unitquantity, packquantity,
	shortageprogress, adviseddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_orderlineshortage_aud

select id, 
	orderlineshortageid,
	orderlineshortagetype,
	unitquantity, packquantity,
	shortageprogress, adviseddate, 
	createdDate, changedDate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.short_orderlineshortage_aud_hist

------------------- purchaseorders$orderlineshortage_shortage -------------------

select *
from mend.short_orderlineshortage_shortage

select orderlineshortageid, shortageid,
	idETLBatchRun, ins_ts
from mend.short_orderlineshortage_shortage

select orderlineshortageid, shortageid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_orderlineshortage_shortage_aud

------------------- purchaseorders$orderlineshortage_orderline -------------------

select *
from mend.short_orderlineshortage_orderline

select orderlineshortageid, orderlineid,
	idETLBatchRun, ins_ts
from mend.short_orderlineshortage_orderline

select orderlineshortageid, orderlineid,
	idETLBatchRun, ins_ts, upd_ts
from mend.short_orderlineshortage_orderline_aud