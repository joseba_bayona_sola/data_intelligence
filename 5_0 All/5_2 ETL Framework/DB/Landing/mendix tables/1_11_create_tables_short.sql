
---------- Script creacion de tablas ----------

use Landing
go

------------------- purchaseorders$shortageheader -------------------

create table mend.short_shortageheader (
	id								bigint NOT NULL, 
	wholesale						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortageheader add constraint [PK_mend_short_shortageheader]
	primary key clustered (id);
go
alter table mend.short_shortageheader add constraint [DF_mend_short_shortageheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortageheader_aud(
	id								bigint NOT NULL, 
	wholesale						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortageheader_aud add constraint [PK_mend_short_shortageheader_aud]
	primary key clustered (id);
go
alter table mend.short_shortageheader_aud add constraint [DF_mend_short_shortageheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.short_shortageheader_aud_hist(
	id								bigint NOT NULL, 
	wholesale						bit,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.short_shortageheader_aud_hist add constraint [DF_mend.short_shortageheader_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortageheader_supplier -------------------

create table mend.short_shortageheader_supplier (
	shortageheaderid				bigint NOT NULL, 
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortageheader_supplier add constraint [PK_mend_short_shortageheader_supplier]
	primary key clustered (shortageheaderid,supplierid);
go
alter table mend.short_shortageheader_supplier add constraint [DF_mend_short_shortageheader_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortageheader_supplier_aud(
	shortageheaderid				bigint NOT NULL, 
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortageheader_supplier_aud add constraint [PK_mend_short_shortageheader_supplier_aud]
	primary key clustered (shortageheaderid,supplierid);
go
alter table mend.short_shortageheader_supplier_aud add constraint [DF_mend_short_shortageheader_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortageheader_sourcewarehouse -------------------

create table mend.short_shortageheader_sourcewarehouse (
	shortageheaderid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortageheader_sourcewarehouse add constraint [PK_mend_short_shortageheader_sourcewarehouse]
	primary key clustered (shortageheaderid,warehouseid);
go
alter table mend.short_shortageheader_sourcewarehouse add constraint [DF_mend_short_shortageheader_sourcewarehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortageheader_sourcewarehouse_aud(
	shortageheaderid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortageheader_sourcewarehouse_aud add constraint [PK_mend_short_shortageheader_sourcewarehouse_aud]
	primary key clustered (shortageheaderid,warehouseid);
go
alter table mend.short_shortageheader_sourcewarehouse_aud add constraint [DF_mend_short_shortageheader_sourcewarehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortageheader_destinationwarehouse -------------------

create table mend.short_shortageheader_destinationwarehouse (
	shortageheaderid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortageheader_destinationwarehouse add constraint [PK_mend_short_shortageheader_destinationwarehouse]
	primary key clustered (shortageheaderid,warehouseid);
go
alter table mend.short_shortageheader_destinationwarehouse add constraint [DF_mend_short_shortageheader_destinationwarehouse_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortageheader_destinationwarehouse_aud(
	shortageheaderid				bigint NOT NULL, 
	warehouseid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortageheader_destinationwarehouse_aud add constraint [PK_mend_short_shortageheader_destinationwarehouse_aud]
	primary key clustered (shortageheaderid,warehouseid);
go
alter table mend.short_shortageheader_destinationwarehouse_aud add constraint [DF_mend_short_shortageheader_destinationwarehouse_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortageheader_packsize -------------------

create table mend.short_shortageheader_packsize (
	shortageheaderid				bigint NOT NULL, 
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortageheader_packsize add constraint [PK_mend_short_shortageheader_packsize]
	primary key clustered (shortageheaderid,packsizeid);
go
alter table mend.short_shortageheader_packsize add constraint [DF_mend_short_shortageheader_packsize_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortageheader_packsize_aud(
	shortageheaderid				bigint NOT NULL, 
	packsizeid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortageheader_packsize_aud add constraint [PK_mend_short_shortageheader_packsize_aud]
	primary key clustered (shortageheaderid,packsizeid);
go
alter table mend.short_shortageheader_packsize_aud add constraint [DF_mend_short_shortageheader_packsize_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go








------------------- purchaseorders$shortage -------------------

create table mend.short_shortage (
	id								bigint NOT NULL, 
	shortageid						bigint, 
	purchaseordernumber				varchar(9), 
	ordernumbers					varchar(200),
	wholesale						bit, 
	status							varchar(9), 
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8), 
	requireddate					datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage add constraint [PK_mend_short_shortage]
	primary key clustered (id);
go
alter table mend.short_shortage add constraint [DF_mend_short_shortage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_aud(
	id								bigint NOT NULL, 
	shortageid						bigint, 
	purchaseordernumber				varchar(9), 
	ordernumbers					varchar(200),
	wholesale						bit, 
	status							varchar(9), 
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8), 
	requireddate					datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_aud add constraint [PK_mend_short_shortage_aud]
	primary key clustered (id);
go
alter table mend.short_shortage_aud add constraint [DF_mend_short_shortage_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.short_shortage_aud_hist(
	id								bigint NOT NULL, 
	shortageid						bigint, 
	purchaseordernumber				varchar(9), 
	ordernumbers					varchar(200),
	wholesale						bit, 
	status							varchar(9), 
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8), 
	requireddate					datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.short_shortage_aud_hist add constraint [DF_mend.short_shortage_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_shortageheader -------------------

create table mend.short_shortage_shortageheader (
	shortageid						bigint NOT NULL, 
	shortageheaderid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_shortageheader add constraint [PK_mend_short_shortage_shortageheader]
	primary key clustered (shortageid,shortageheaderid);
go
alter table mend.short_shortage_shortageheader add constraint [DF_mend_short_shortage_shortageheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_shortageheader_aud(
	shortageid						bigint NOT NULL, 
	shortageheaderid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_shortageheader_aud add constraint [PK_mend_short_shortage_shortageheader_aud]
	primary key clustered (shortageid,shortageheaderid);
go
alter table mend.short_shortage_shortageheader_aud add constraint [DF_mend_short_shortage_shortageheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_purchaseorderline -------------------

create table mend.short_shortage_purchaseorderline (
	shortageid						bigint NOT NULL, 
	purchaseorderlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_purchaseorderline add constraint [PK_mend_short_shortage_purchaseorderline]
	primary key clustered (shortageid,purchaseorderlineid);
go
alter table mend.short_shortage_purchaseorderline add constraint [DF_mend_short_shortage_purchaseorderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_purchaseorderline_aud(
	shortageid						bigint NOT NULL, 
	purchaseorderlineid				bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_purchaseorderline_aud add constraint [PK_mend_short_shortage_purchaseorderline_aud]
	primary key clustered (shortageid,purchaseorderlineid);
go
alter table mend.short_shortage_purchaseorderline_aud add constraint [DF_mend_short_shortage_purchaseorderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_supplier -------------------

create table mend.short_shortage_supplier (
	shortageid						bigint NOT NULL, 
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_supplier add constraint [PK_mend_short_shortage_supplier]
	primary key clustered (shortageid,supplierid);
go
alter table mend.short_shortage_supplier add constraint [DF_mend_short_shortage_supplier_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_supplier_aud(
	shortageid						bigint NOT NULL, 
	supplierid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_supplier_aud add constraint [PK_mend_short_shortage_supplier_aud]
	primary key clustered (shortageid,supplierid);
go
alter table mend.short_shortage_supplier_aud add constraint [DF_mend_short_shortage_supplier_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_product -------------------

create table mend.short_shortage_product (
	shortageid						bigint NOT NULL, 
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_product add constraint [PK_mend_short_shortage_product]
	primary key clustered (shortageid,productid);
go
alter table mend.short_shortage_product add constraint [DF_mend_short_shortage_product_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_product_aud(
	shortageid						bigint NOT NULL, 
	productid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_product_aud add constraint [PK_mend_short_shortage_product_aud]
	primary key clustered (shortageid,productid);
go
alter table mend.short_shortage_product_aud add constraint [DF_mend_short_shortage_product_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_standardprice -------------------

create table mend.short_shortage_standardprice (
	shortageid						bigint NOT NULL, 
	standardpriceid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_standardprice add constraint [PK_mend_short_shortage_standardprice]
	primary key clustered (shortageid,standardpriceid);
go
alter table mend.short_shortage_standardprice add constraint [DF_mend_short_shortage_standardprice_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_standardprice_aud(
	shortageid						bigint NOT NULL, 
	standardpriceid					bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_standardprice_aud add constraint [PK_mend_short_shortage_standardprice_aud]
	primary key clustered (shortageid,standardpriceid);
go
alter table mend.short_shortage_standardprice_aud add constraint [DF_mend_short_shortage_standardprice_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_stockitem -------------------

create table mend.short_shortage_stockitem (
	shortageid						bigint NOT NULL, 
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_stockitem add constraint [PK_mend_short_shortage_stockitem]
	primary key clustered (shortageid,stockitemid);
go
alter table mend.short_shortage_stockitem add constraint [DF_mend_short_shortage_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_stockitem_aud(
	shortageid						bigint NOT NULL, 
	stockitemid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_stockitem_aud add constraint [PK_mend_short_shortage_stockitem_aud]
	primary key clustered (shortageid,stockitemid);
go
alter table mend.short_shortage_stockitem_aud add constraint [DF_mend_short_shortage_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$shortage_warehousestockitem -------------------

create table mend.short_shortage_warehousestockitem (
	shortageid						bigint NOT NULL, 
	warehousestockitemid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_shortage_warehousestockitem add constraint [PK_mend_short_shortage_warehousestockitem]
	primary key clustered (shortageid,warehousestockitemid);
go
alter table mend.short_shortage_warehousestockitem add constraint [DF_mend_short_shortage_warehousestockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_shortage_warehousestockitem_aud(
	shortageid						bigint NOT NULL, 
	warehousestockitemid			bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_shortage_warehousestockitem_aud add constraint [PK_mend_short_shortage_warehousestockitem_aud]
	primary key clustered (shortageid,warehousestockitemid);
go
alter table mend.short_shortage_warehousestockitem_aud add constraint [DF_mend_short_shortage_warehousestockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go







------------------- purchaseorders$orderlineshortage -------------------

create table mend.short_orderlineshortage (
	id								bigint NOT NULL,
	orderlineshortageid				bigint,
	orderlineshortagetype			varchar(8),
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8),
	shortageprogress				varchar(250), 
	adviseddate						datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_orderlineshortage add constraint [PK_mend_short_orderlineshortage]
	primary key clustered (id);
go
alter table mend.short_orderlineshortage add constraint [DF_mend_short_orderlineshortage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_orderlineshortage_aud(
	id								bigint NOT NULL,
	orderlineshortageid				bigint,
	orderlineshortagetype			varchar(8),
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8),
	shortageprogress				varchar(250), 
	adviseddate						datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_orderlineshortage_aud add constraint [PK_mend_short_orderlineshortage_aud]
	primary key clustered (id);
go
alter table mend.short_orderlineshortage_aud add constraint [DF_mend_short_orderlineshortage_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.short_orderlineshortage_aud_hist (
	id								bigint NOT NULL,
	orderlineshortageid				bigint,
	orderlineshortagetype			varchar(8),
	unitquantity					decimal(28,8), 
	packquantity					decimal(28,8),
	shortageprogress				varchar(250), 
	adviseddate						datetime2, 
	createdDate						datetime2, 
	changedDate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.short_orderlineshortage_aud_hist add constraint [DF_mend.short_orderlineshortage_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$orderlineshortage_shortage -------------------

create table mend.short_orderlineshortage_shortage (
	orderlineshortageid				bigint NOT NULL, 
	shortageid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_orderlineshortage_shortage add constraint [PK_mend_short_orderlineshortage_shortage]
	primary key clustered (orderlineshortageid,shortageid);
go
alter table mend.short_orderlineshortage_shortage add constraint [DF_mend_short_orderlineshortage_shortage_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_orderlineshortage_shortage_aud(
	orderlineshortageid				bigint NOT NULL, 
	shortageid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_orderlineshortage_shortage_aud add constraint [PK_mend_short_orderlineshortage_shortage_aud]
	primary key clustered (orderlineshortageid,shortageid);
go
alter table mend.short_orderlineshortage_shortage_aud add constraint [DF_mend_short_orderlineshortage_shortage_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$orderlineshortage_orderline -------------------

create table mend.short_orderlineshortage_orderline (
	orderlineshortageid				bigint NOT NULL, 
	orderlineid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.short_orderlineshortage_orderline add constraint [PK_mend_short_orderlineshortage_orderline]
	primary key clustered (orderlineshortageid,orderlineid);
go
alter table mend.short_orderlineshortage_orderline add constraint [DF_mend_short_orderlineshortage_orderline_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.short_orderlineshortage_orderline_aud(
	orderlineshortageid				bigint NOT NULL, 
	orderlineid						bigint NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.short_orderlineshortage_orderline_aud add constraint [PK_mend_short_orderlineshortage_orderline_aud]
	primary key clustered (orderlineshortageid,orderlineid);
go
alter table mend.short_orderlineshortage_orderline_aud add constraint [DF_mend_short_orderlineshortage_orderline_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go