use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-LND' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'Mendix Data' name, 'Taking Mendix Source Data to Landing' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

   
-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_general_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_whstockitem_tables' name, 'SSIS PKG for reading Warehouse Stock Items Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_order_tables' name, 'SSIS PKG for reading Customers Orders data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_shipments_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_wh_allocation' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_purchase_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_shortage_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_wh_shipments' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_intersite_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_receipt_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			union
			select 'SRC-LND' codPackageType, 'iirujo' developer,  'Mendix Data' flow_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_vat_tables' name, 'SSIS PKG for reading General Tables data from Mendix to Landing' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'ETLBatchRunMendix' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'test_Mendix_db' name, 'GET SP for testing Mendix DB Connection' description
			union

			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplier_country' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplier_company' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplier_currency' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplierprice' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_standardprice' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplierprices_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_supplierprices_packsize' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_manufacturer' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_supp_packsize_manufacturer' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_company' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_magwebstore' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_country' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_currency' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_country_currency' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_spotrate' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_currentspotrate_currency' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_comp_historicalspotrate_currency' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_productcategory' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_productfamily' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_productfamily_productcategory' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_product' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_product_productfamily' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_stockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_stockitem_product' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_contactlens' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_packsize' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_packsize_productfamily' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_prod_stockitem_packsize' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousesupplier_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousesupplier_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_supplierroutine' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_supplierroutine_warehousesupplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_preference' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehouse_preference' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_country_preference' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousestockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_located_at' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousestockitem_stockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousestockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousestockitembatch_warehousestockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockissue' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockissue_warehousestockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockallocation' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockissue_batchstockallocation' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchtransaction_orderlinestockallocationtransaction' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchtransaction_warehousestockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_repairstockbatchs' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_repairstockbatchs_warehousestockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_stockmovement' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_stockmovement_warehousestockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockmovement' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockmovement_stockmovement' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_batchstockmovement_warehousestockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_whstockitem_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_wh_warehousestockitembatch_full' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_order_customer' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_order_magwebstore' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_basevalues' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_basevalues_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_order_statusupdate' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_statusupdate_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderprocessingtransaction' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderprocessingtransaction_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderlineabstract' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderlinemagento' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderlinemagento_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderline_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderlinemagento_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_shippinggroup' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_shippinggroup_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderlineabstract_shippinggroup' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderline_product' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderline_requiredstockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_address_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_order_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_order_orderaddress_country' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_shippingmethod' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_shippingmethods_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_shippingmethods_destinationcountry' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_defaultcalendar' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_defaultcalendar_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_defaultwarehousesuppliercalender' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_general_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_cal_defaultwarehousesuppliercalender_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_customershipment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_customershipment_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_customershipment_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_lifecycle_update' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_customershipment_progress' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_api_updates' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_ship_api_updates_customershipment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_consignment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_packagedetail' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_consignmentpackages' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_scurriconsignment_shipment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_customershipmentline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_customershipmentline_customershipment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shipments_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'ship_customershipmentline_product' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_orderlinestockallocationtransaction' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_order' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_orderline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_warehousestockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_batch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_at_packsize' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_orderlinestockallocationtransa_customershipmentl' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_magentoallocation' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_magentoallocatio_orderlinestockallocationtransac' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_magentoallocation_orderlinemagento' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_allocation' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_all_magentoallocation_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
		
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorder' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorder_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorder_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderlineheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderlineheader_purchaseorder' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderlineheader_productfamily' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderlineheader_packsize' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderlineheader_supplierprices' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderline_purchaseorderlineheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_purchase_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_purc_purchaseorderline_stockitem' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortageheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortageheader_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortageheader_sourcewarehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortageheader_destinationwarehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortageheader_packsize' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_shortageheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_purchaseorderline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_product' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_standardprice' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_stockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_shortage_warehousestockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_orderlineshortage' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_orderlineshortage_shortage' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_shortage_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_short_orderlineshortage_orderline' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receipt_warehouse' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receipt_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_warehousestockitembatch_receipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_warehousestockitembatch_receipthistory' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_backtobackreceipt_purchaseorder' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptforcancelleditems' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline_receipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline_supplier' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline_purchaseorderline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline_snapreceiptline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_warehousestockitembatch_receiptline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptlineheader' name, 'GET SP for reading data from Mendix to Landing' description			
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptlineheader_receipt' name, 'GET SP for reading data from Mendix to Landing' description			
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptlineheader_productfamily' name, 'GET SP for reading data from Mendix to Landing' description			
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptlineheader_packsize' name, 'GET SP for reading data from Mendix to Landing' description			
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmend_lnd_wh_shipments' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_whship_receiptline_receiptlineheader' name, 'GET SP for reading data from Mendix to Landing' description			

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_supplypo_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_transferpo_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_order_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitbatchheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitbatchheader_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitbatchheader_customershipment' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitstockitembatch' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitstockitembatch_transferheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitstockitembatch_intransitbatchheader' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitstockitembatch_stockitem' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_intersite_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_inter_intransitstockitembatch_receipt' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_receipt_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_rec_snapreceipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_receipt_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_rec_snapreceipt_receipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_receipt_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_rec_snapreceiptline' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_receipt_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_rec_snapreceiptline_snapreceipt' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_receipt_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_rec_snapreceiptline_stockitem' name, 'GET SP for reading data from Mendix to Landing' description

			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_commodity' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_productfamily_commodity' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_dispensingservicesrate' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_dispensingservices_commodity' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_dispensingservicesrate_country' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatrating' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatrating_country' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatrate' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatrate_vatrating' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatschedule' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatschedule_vatrating' name, 'GET SP for reading data from Mendix to Landing' description
			union
			select 'GET' codSPType, 'iirujo' developer,  'srcmend_lnd_vat_tables' package_name, 'mendix_db' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmend_lnd_get_vat_vatschedule_commodity' name, 'GET SP for reading data from Mendix to Landing' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 
