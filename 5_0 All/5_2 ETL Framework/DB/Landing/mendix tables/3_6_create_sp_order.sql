
------------------------------ SP -----------------------------------

use Landing
go

------------ orderprocessing$order ------------

drop procedure mend.srcmend_lnd_get_order_order
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_order
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				incrementID, magentoOrderID, orderStatus, 
				orderLinesMagento, orderLinesDeduped,
				_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
				allocatedDate, promisedShippingDate, promisedDeliveryDate,
				labelRenderer,  
				couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
				createdDate, changedDate, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						incrementID, magentoOrderID, orderStatus, 
						orderLinesMagento, orderLinesDeduped,
						_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
						allocatedDate, promisedShippingDate, promisedDeliveryDate,
						labelRenderer,  
						couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
						createdDate, changedDate  
					from public."orderprocessing$order"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')									
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$order_customer ------------

drop procedure mend.srcmend_lnd_get_order_order_customer
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_order_customer
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_order_customer
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderid orderid, customer$retailcustomerid retailcustomerid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select oc."orderprocessing$orderid", oc."customer$retailcustomerid" 
					from 
							public."orderprocessing$order_customer" oc
						inner join
							public."orderprocessing$order" o on oc."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')	
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$order_magwebstore ------------

drop procedure mend.srcmend_lnd_get_order_order_magwebstore
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_order_magwebstore
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_order_magwebstore
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderid orderid, companyorganisation$magwebstoreid magwebstoreid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select os."orderprocessing$orderid", os."companyorganisation$magwebstoreid" 
					from 
							public."orderprocessing$order_magwebstore" os
						inner join
							public."orderprocessing$order" o on os."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')								
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ orderprocessing$basevalues ------------

drop procedure mend.srcmend_lnd_get_order_basevalues
go

-- ===============================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ===============================================================================
-- Description: Reads data from Mendix Table through Open Query - order_basevalues
-- ===============================================================================

create procedure mend.srcmend_lnd_get_order_basevalues
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
				discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
				shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
				adjustmentNegative, adjustmentPositive, 				
				custBalanceAmount, 
				customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
				grandTotal, totalInvoiced, totalCanceled, totalRefunded,
				toOrderRate, toGlobalRate, currencyCode, 
				mutualamount, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select bv.id, 
						bv.subtotal, bv.subtotalInclTax, bv.subtotalInvoiced, bv.subtotalCanceled, bv.subtotalRefunded, 
						bv.discountAmount, bv.discountInvoiced, bv.discountCanceled, bv.discountRefunded, 
						bv.shippingAmount, bv.shippingInvoiced, bv.shippingCanceled, bv.shippingRefunded, 					 
						bv.adjustmentNegative, bv.adjustmentPositive, 				
						bv.custBalanceAmount, 
						bv.customerBalanceAmount, bv.customerBalanceInvoiced, bv.customerBalanceRefunded, bv.customerBalanceTotRefunded, bv.customerBalanceTotalRefunded, 
						bv.grandTotal, bv.totalInvoiced, bv.totalCanceled, bv.totalRefunded,
						bv.toOrderRate, bv.toGlobalRate, bv.currencyCode, 
						bv.mutualamount  
					from 
							public."orderprocessing$basevalues" bv
						inner join	
							public."orderprocessing$basevalues_order" obv on bv.id = obv."orderprocessing$basevaluesid"
						inner join
							public."orderprocessing$order" o on obv."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')															
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$basevalues_order ------------

drop procedure mend.srcmend_lnd_get_order_basevalues_order
go

-- =====================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- =====================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_basevalues_order
-- =====================================================================================

create procedure mend.srcmend_lnd_get_order_basevalues_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$basevaluesid basevaluesid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select obv."orderprocessing$basevaluesid", obv."orderprocessing$orderid" 
					from 
							public."orderprocessing$basevalues_order" obv
						inner join
							public."orderprocessing$order" o on obv."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')															
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------ orderprocessing$order_statusupdate ------------

drop procedure mend.srcmend_lnd_get_order_order_statusupdate
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_order_statusupdate
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_order_statusupdate
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, status, timestamp, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id, status, "timestamp" 
					from public."orderprocessing$order_statusupdate"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$statusupdate_order ------------

drop procedure mend.srcmend_lnd_get_order_statusupdate_order
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_statusupdate_order
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_statusupdate_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$order_statusupdateid order_statusupdateid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select "orderprocessing$order_statusupdateid", "orderprocessing$orderid" 
					from public."orderprocessing$statusupdate_order"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ orderprocessing$orderprocessingtransaction ------------

drop procedure mend.srcmend_lnd_get_order_orderprocessingtransaction
go

-- ===============================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===============================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderprocessingtransaction
-- ===============================================================================================

create procedure mend.srcmend_lnd_get_order_orderprocessingtransaction
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, orderReference, status, detail, timestamp, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id, orderReference, status, detail, "timestamp" 
					from public."orderprocessing$orderprocessingtransaction"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderprocessingtransaction_order ------------

drop procedure mend.srcmend_lnd_get_order_orderprocessingtransaction_order
go

-- =====================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- =====================================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderprocessingtransaction_order
-- =====================================================================================================

create procedure mend.srcmend_lnd_get_order_orderprocessingtransaction_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderprocessingtransactionid orderprocessingtransactionid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select "orderprocessing$orderprocessingtransactionid", "orderprocessing$orderid" 
					from public."orderprocessing$orderprocessingtransaction_order"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------ orderprocessing$orderlineabstract ------------

drop procedure mend.srcmend_lnd_get_order_orderlineabstract
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderlineabstract
-- ======================================================================================

create procedure mend.srcmend_lnd_get_order_orderlineabstract
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				magentoItemID, 
				quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
				basePrice, baseRowPrice, 
				allocated, 
				orderLineType, subMetaObjectName,
				sku, eye, lensGroupID, 
				netPrice, netRowPrice,
				createdDate, changedDate, 
				system$owner, system$changedBy, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						magentoItemID, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
						basePrice, baseRowPrice, 
						allocated, 
						orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						netPrice, netRowPrice,
						createdDate, changedDate, 
						"system$owner", "system$changedby" 
					from public."orderprocessing$orderlineabstract"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')									
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderlinemagento ------------

drop procedure mend.srcmend_lnd_get_order_orderlinemagento
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderlinemagento
-- ======================================================================================

create procedure mend.srcmend_lnd_get_order_orderlinemagento
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, lineAdded, deduplicate, fulfilled, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select olm.id, olm.lineAdded, olm.deduplicate, olm.fulfilled 
					from 
							public."orderprocessing$orderlinemagento" olm
						inner join
							public."orderprocessing$orderlineabstract" ola on olm.id = ola.id
					where ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')														
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderlinemagento_order ------------

drop procedure mend.srcmend_lnd_get_order_orderlinemagento_order
go

-- ===========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ===========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderlinemagento_order
-- ===========================================================================================

create procedure mend.srcmend_lnd_get_order_orderlinemagento_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderlinemagentoid orderlinemagentoid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select olmo."orderprocessing$orderlinemagentoid",  olmo."orderprocessing$orderid"
					from 
							public."orderprocessing$orderlinemagento_order" olmo
						inner join
							public."orderprocessing$orderlineabstract" ola on olmo."orderprocessing$orderlinemagentoid" = ola.id
						inner join
							public."orderprocessing$order" o on olmo."orderprocessing$orderid" = o.id
					where 
						(ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or
						(o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 																									
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderline ------------

drop procedure mend.srcmend_lnd_get_order_orderline
go

-- ==============================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==============================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderline
-- ==============================================================================

create procedure mend.srcmend_lnd_get_order_orderline
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, status, packsOrdered, packsAllocated, packsShipped, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped
					from 
							public."orderprocessing$orderline" ol
						inner join
							public."orderprocessing$orderlineabstract" ola on ol.id = ola.id
					where ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')														
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderline_order ------------

drop procedure mend.srcmend_lnd_get_order_orderline_order
go

-- ====================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ====================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderline_order
-- ====================================================================================

create procedure mend.srcmend_lnd_get_order_orderline_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderlineid orderlineid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select olo."orderprocessing$orderlineid", olo."orderprocessing$orderid"
					from 
							public."orderprocessing$orderline_order" olo
						inner join
							public."orderprocessing$orderlineabstract" ola on olo."orderprocessing$orderlineid" = ola.id
						inner join
							public."orderprocessing$order" o on olo."orderprocessing$orderid" = o.id
					where 
						(ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or
						(o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS''''))
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ orderprocessing$shippinggroup ------------

drop procedure mend.srcmend_lnd_get_order_shippinggroup
go

-- ==================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ==================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_shippinggroup
-- ==================================================================================

create procedure mend.srcmend_lnd_get_order_shippinggroup
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, 
				dispensingFee, 
				allocationStatus, shipmentStatus, 
				wholesale, 
				letterBoxAble, onHold, adHoc, cancelled, 
				createdDate, changedDate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select id, 
						dispensingFee, 
						allocationStatus, shipmentStatus, 
						wholesale, 
						letterBoxAble, onHold, adHoc, cancelled, 
						createdDate, changedDate
					from public."orderprocessing$shippinggroup"
					where createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')									
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$shippinggroup_order ------------

drop procedure mend.srcmend_lnd_get_order_shippinggroup_order
go

-- ========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_shippinggroup_order
-- ========================================================================================

create procedure mend.srcmend_lnd_get_order_shippinggroup_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$shippinggroupid shippinggroupid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select shgo."orderprocessing$shippinggroupid", shgo."orderprocessing$orderid"
					from 
							public."orderprocessing$shippinggroup_order" shgo
						inner join	
							public."orderprocessing$shippinggroup" shg on shgo."orderprocessing$shippinggroupid" = shg.id
						inner join
							public."orderprocessing$order" o on shgo."orderprocessing$orderid" = o.id						
					where 
						(shg.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or shg.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or						
						(o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 				
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderlineabstract_shippinggroup ------------

drop procedure mend.srcmend_lnd_get_order_orderlineabstract_shippinggroup
go

-- ====================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ====================================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderlineabstract_shippinggroup
-- ====================================================================================================

create procedure mend.srcmend_lnd_get_order_orderlineabstract_shippinggroup
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderlineabstractid orderlineabstractid, orderprocessing$shippinggroupid shippinggroupid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select shgol."orderprocessing$orderlineabstractid", shgol."orderprocessing$shippinggroupid"
					from 
							public."orderprocessing$orderlineabstract_shippinggroup" shgol
						inner join	
							public."orderprocessing$shippinggroup" shg on shgol."orderprocessing$shippinggroupid" = shg.id
						inner join
							public."orderprocessing$orderlineabstract" ola on shgol."orderprocessing$orderlineabstractid" = ola.id
					where 
						(shg.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or shg.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) or						
						(ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
							or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')) 											
						'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go





------------ orderprocessing$orderline_product ------------

drop procedure mend.srcmend_lnd_get_order_orderline_product
go

-- ======================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ======================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderline_product
-- ======================================================================================

create procedure mend.srcmend_lnd_get_order_orderline_product
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderlineabstractid orderlineabstractid, product$productid productid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select olp."orderprocessing$orderlineabstractid", olp."product$productid"
					from 
							public."orderprocessing$orderline_product" olp
						inner join
							public."orderprocessing$orderlineabstract" ola on olp."orderprocessing$orderlineabstractid" = ola.id
					where ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')																								
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------ orderprocessing$orderline_requiredstockitem ------------

drop procedure mend.srcmend_lnd_get_order_orderline_requiredstockitem
go

-- ================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 23-05-2017
-- Changed: 
	--	11-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	12-02-2018	Joseba Bayona Sola	Add INCR Filtering
-- ================================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderline_requiredstockitem
-- ================================================================================================

create procedure mend.srcmend_lnd_get_order_orderline_requiredstockitem
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderlineabstractid orderlineabstractid, product$stockitemid stockitemid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
					select olrsi."orderprocessing$orderlineabstractid", olrsi."product$stockitemid"
					from 
							public."orderprocessing$orderline_requiredstockitem" olrsi
						inner join
							public."orderprocessing$orderlineabstract" ola on olrsi."orderprocessing$orderlineabstractid" = ola.id
					where ola.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or ola.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')																			
					'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ orderprocessing$address_order ------------

drop procedure mend.srcmend_lnd_get_order_address_order
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_address_order
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_address_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderaddressid orderaddressid, orderprocessing$orderid orderid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select oc."orderprocessing$orderaddressid", oc."orderprocessing$orderid" 
					from 
							public."orderprocessing$address_order" oc
						inner join
							public."orderprocessing$order" o on oc."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')	
					'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.srcmend_lnd_get_order_orderaddress_country
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - order_orderaddress_country
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_order_orderaddress_country
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select orderprocessing$orderaddressid orderaddressid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' '
			+ 'from openquery(MENDIX_DB_RESTORE,''
					select oac."orderprocessing$orderaddressid", oac."countrymanagement$countryid" 
					from 
							public."orderprocessing$orderaddress_country" oac
						inner join
							public."orderprocessing$address_order" oc on oac."orderprocessing$orderaddressid" = oc."orderprocessing$orderaddressid"
						inner join
							public."orderprocessing$order" o on oc."orderprocessing$orderid" = o.id
					where o.createddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')
						or o.changeddate between to_timestamp(''''' + @dateFromV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''') and to_timestamp(''''' + @dateToV + ''''', ''''YYYY-MM-DD HH24:MI:SS'''')	
					'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go
