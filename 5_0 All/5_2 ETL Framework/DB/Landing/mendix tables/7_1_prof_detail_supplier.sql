use Landing
go 

------------------------------------------------------------------------
----------------------- supp_supplier ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_supplier_aud

	select idETLBatchRun, count(*) num_rows
	from mend.supp_supplier_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_supplier_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
			vatCode, defaultLeadTime,
			email, telephone, fax,
			flatFileConfigurationToUse,
			street1, street2, street3, city, region, postcode,
			vendorCode, vendorShortName, vendorStoreNumber, 
			defaulttransitleadtime,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_supplier_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


------------------------------------------------------------------------
----------------------- supp_supplier_country ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplier_country_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
----------------------- supp_supplier_company ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplier_company_aud
	group by idETLBatchRun
	order by idETLBatchRun


------------------------------------------------------------------------
----------------------- supp_supplier_currency ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplier_currency_aud
	group by idETLBatchRun
	order by idETLBatchRun





------------------------------------------------------------------------
----------------------- supp_supplierprice ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplierprice_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_supplierprice_aud

	select idETLBatchRun, count(*) num_rows
	from mend.supp_supplierprice_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_supplierprice_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			subMetaObjectName,
			unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
			directPrice, expiredPrice,
			totallt, shiplt, transferlt,
			comments,
			lastUpdated,
			createdDate, changedDate,
			system$owner, system$changedBy, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_supplierprice_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- supp_standardprice ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_standardprice_aud

	select idETLBatchRun, count(*) num_rows
	from mend.supp_standardprice_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_standardprice_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			moq, effectiveDate, expiryDate, maxqty,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_standardprice_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- supp_supplierprices_supplier ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplierprices_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- supp_supplierprices_packsize ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_supplierprices_packsize_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
----------------------- supp_manufacturer ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_manufacturer_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_manufacturer_aud

	select idETLBatchRun, count(*) num_rows
	from mend.supp_manufacturer_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.supp_manufacturer_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			manufacturerid, manufacturername, createddate, changeddate, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.supp_manufacturer_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


------------------------------------------------------------------------
----------------------- product$packsize_manufacturer ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.supp_packsize_manufacturer_aud
	group by idETLBatchRun
	order by idETLBatchRun