
------------------------ Drop tables -------------------------------

use Landing
go

------------ stockallocation$orderlinestockallocationtransaction ------------

drop table mend.all_orderlinestockallocationtransaction;
go
drop table mend.all_orderlinestockallocationtransaction_aud;
go
drop table mend.all_orderlinestockallocationtransaction_aud_hist;
go

------------ stockallocation$at_order ------------

drop table mend.all_at_order;
go
drop table mend.all_at_order_aud;
go

------------ stockallocation$at_orderline ------------

drop table mend.all_at_orderline;
go
drop table mend.all_at_orderline_aud;
go

------------ stockallocation$at_warehouse ------------

drop table mend.all_at_warehouse;
go
drop table mend.all_at_warehouse_aud;
go

------------ stockallocation$at_warehousestockitem ------------

drop table mend.all_at_warehousestockitem;
go
drop table mend.all_at_warehousestockitem_aud;
go

------------ stockallocation$at_packsize ------------

drop table mend.all_at_batch;
go
drop table mend.all_at_batch_aud;
go

------------ stockallocation$at_packsize ------------

drop table mend.all_at_packsize;
go
drop table mend.all_at_packsize_aud;
go

------------ stockallocatio$orderlinestockallocationtransa_customershipmentl ------------

drop table mend.all_orderlinestockallocationtransa_customershipmentl;
go
drop table mend.all_orderlinestockallocationtransa_customershipmentl_aud;
go




------------ stockallocation$magentoallocation ------------

drop table mend.all_magentoallocation;
go
drop table mend.all_magentoallocation_aud;
go
drop table mend.all_magentoallocation_aud_hist;
go

------------ stockallocatio$magentoallocatio_orderlinestockallocationtransac ------------

drop table mend.all_magentoallocatio_orderlinestockallocationtransac;
go
drop table mend.all_magentoallocatio_orderlinestockallocationtransac_aud;
go

------------ stockallocation$magentoallocation_orderlinemagento ------------

drop table mend.all_magentoallocation_orderlinemagento;
go
drop table mend.all_magentoallocation_orderlinemagento_aud;
go

------------ stockallocation$magentoallocation_warehouse ------------

drop table mend.all_magentoallocation_warehouse;
go
drop table mend.all_magentoallocation_warehouse_aud;
go

