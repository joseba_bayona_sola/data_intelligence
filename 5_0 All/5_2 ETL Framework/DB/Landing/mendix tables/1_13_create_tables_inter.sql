
---------- Script creacion de tablas ----------

use Landing
go

------------------- intersitetransfer$transferheader -------------------

create table mend.inter_transferheader (
	id								bigint NOT NULL, 
	transfernum						varchar(200), 
	allocatedstrategy				varchar(19), 
	totalremaining					decimal(28,8),
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_transferheader add constraint [PK_mend_inter_transferheader]
	primary key clustered (id);
go
alter table mend.inter_transferheader add constraint [DF_mend_inter_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mend.inter_transferheader_aud(
	id								bigint NOT NULL, 
	transfernum						varchar(200), 
	allocatedstrategy				varchar(19), 
	totalremaining					decimal(28,8),
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_transferheader_aud add constraint [PK_mend_inter_transferheader_aud]
	primary key clustered (id);
go
alter table mend.inter_transferheader_aud add constraint [DF_mend_inter_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_transferheader_aud_hist(
	id								bigint NOT NULL, 
	transfernum						varchar(200), 
	allocatedstrategy				varchar(19), 
	totalremaining					decimal(28,8),
	createddate						datetime2, 
	changeddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.inter_transferheader_aud_hist add constraint [DF_mend.inter_transferheader_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$supplypo_transferheader -------------------

create table mend.inter_supplypo_transferheader (
	transferheaderid				bigint NOT NULL, 
	purchaseorderid					bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_supplypo_transferheader add constraint [PK_mend_inter_supplypo_transferheader]
	primary key clustered (transferheaderid,purchaseorderid);
go
alter table mend.inter_supplypo_transferheader add constraint [DF_mend_inter_supplypo_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_supplypo_transferheader_aud(
	transferheaderid				bigint NOT NULL, 
	purchaseorderid					bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_supplypo_transferheader_aud add constraint [PK_mend_inter_supplypo_transferheader_aud]
	primary key clustered (transferheaderid,purchaseorderid);
go
alter table mend.inter_supplypo_transferheader_aud add constraint [DF_mend_inter_supplypo_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- purchaseorders$transferpo_transferheader -------------------

create table mend.inter_transferpo_transferheader (
	transferheaderid				bigint NOT NULL, 
	purchaseorderid					bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_transferpo_transferheader add constraint [PK_mend_inter_transferpo_transferheader]
	primary key clustered (transferheaderid,purchaseorderid);
go
alter table mend.inter_transferpo_transferheader add constraint [DF_mend_inter_transferpo_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_transferpo_transferheader_aud(
	transferheaderid				bigint NOT NULL, 
	purchaseorderid					bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_transferpo_transferheader_aud add constraint [PK_mend_inter_transferpo_transferheader_aud]
	primary key clustered (transferheaderid,purchaseorderid);
go
alter table mend.inter_transferpo_transferheader_aud add constraint [DF_mend_inter_transferpo_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



------------------- orderprocessing$order_transferheader -------------------

create table mend.inter_order_transferheader (
	orderid							bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_order_transferheader add constraint [PK_mend_inter_order_transferheader]
	primary key clustered (orderid, transferheaderid);
go
alter table mend.inter_order_transferheader add constraint [DF_mend_inter_order_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_order_transferheader_aud(
	orderid							bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_order_transferheader_aud add constraint [PK_mend_inter_order_transferheader_aud]
	primary key clustered (orderid, transferheaderid);
go
alter table mend.inter_order_transferheader_aud add constraint [DF_mend_inter_order_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go






------------------- intersitetransfer$intransitbatchheader -------------------

create table mend.inter_intransitbatchheader (
	id								bigint NOT NULL, 
	createddate						datetime2, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitbatchheader add constraint [PK_mend_inter_intransitbatchheader]
	primary key clustered (id);
go
alter table mend.inter_intransitbatchheader add constraint [DF_mend_inter_intransitbatchheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitbatchheader_aud(
	id								bigint NOT NULL, 
	createddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitbatchheader_aud add constraint [PK_mend_inter_intransitbatchheader_aud]
	primary key clustered (id);
go
alter table mend.inter_intransitbatchheader_aud add constraint [DF_mend_inter_intransitbatchheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitbatchheader_aud_hist(
	id								bigint NOT NULL, 
	createddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.inter_intransitbatchheader_aud_hist add constraint [DF_mend.inter_intransitbatchheader_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitbatchheader_transferheader -------------------

create table mend.inter_intransitbatchheader_transferheader (
	intransitbatchheaderid			bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitbatchheader_transferheader add constraint [PK_mend_inter_intransitbatchheader_transferheader]
	primary key clustered (intransitbatchheaderid,transferheaderid);
go
alter table mend.inter_intransitbatchheader_transferheader add constraint [DF_mend_inter_intransitbatchheader_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitbatchheader_transferheader_aud(
	intransitbatchheaderid			bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitbatchheader_transferheader_aud add constraint [PK_mend_inter_intransitbatchheader_transferheader_aud]
	primary key clustered (intransitbatchheaderid,transferheaderid);
go
alter table mend.inter_intransitbatchheader_transferheader_aud add constraint [DF_mend_inter_intransitbatchheader_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitbatchheader_customershipment -------------------

create table mend.inter_intransitbatchheader_customershipment (
	intransitbatchheaderid			bigint NOT NULL, 
	customershipmentid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitbatchheader_customershipment add constraint [PK_mend_inter_intransitbatchheader_customershipment]
	primary key clustered (intransitbatchheaderid,customershipmentid);
go
alter table mend.inter_intransitbatchheader_customershipment add constraint [DF_mend_inter_intransitbatchheader_customershipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitbatchheader_customershipment_aud(
	intransitbatchheaderid			bigint NOT NULL, 
	customershipmentid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitbatchheader_customershipment_aud add constraint [PK_mend_inter_intransitbatchheader_customershipment_aud]
	primary key clustered (intransitbatchheaderid,customershipmentid);
go
alter table mend.inter_intransitbatchheader_customershipment_aud add constraint [DF_mend_inter_intransitbatchheader_customershipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go








------------------- intersitetransfer$intransitstockitembatch -------------------

create table mend.inter_intransitstockitembatch (
	id								bigint NOT NULL, 
	issuedquantity					decimal(28,8), 
	remainingquantity				decimal(28,8), 
	used							bit,
	productunitcost					decimal(28,8), 
	carriageUnitCost				decimal(28,8), 
	totalunitcost					decimal(28,8), 
	totalUnitCostIncInterCo			decimal(28,8),
	currency						varchar(4),
	groupFIFOdate					datetime2, 
	arriveddate						datetime2, 
	confirmeddate					datetime2, 
	stockregistereddate				datetime2, 
	createddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitstockitembatch add constraint [PK_mend_inter_intransitstockitembatch]
	primary key clustered (id);
go
alter table mend.inter_intransitstockitembatch add constraint [DF_mend_inter_intransitstockitembatch_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_aud(
	id								bigint NOT NULL, 
	issuedquantity					decimal(28,8), 
	remainingquantity				decimal(28,8), 
	used							bit,
	productunitcost					decimal(28,8), 
	carriageUnitCost				decimal(28,8), 
	totalunitcost					decimal(28,8), 
	totalUnitCostIncInterCo			decimal(28,8),
	currency						varchar(4),
	groupFIFOdate					datetime2, 
	arriveddate						datetime2, 
	confirmeddate					datetime2, 
	stockregistereddate				datetime2, 
	createddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitstockitembatch_aud add constraint [PK_mend_inter_intransitstockitembatch_aud]
	primary key clustered (id);
go
alter table mend.inter_intransitstockitembatch_aud add constraint [DF_mend_inter_intransitstockitembatch_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_aud_hist(
	id								bigint NOT NULL, 
	issuedquantity					decimal(28,8), 
	remainingquantity				decimal(28,8), 
	used							bit,
	productunitcost					decimal(28,8), 
	carriageUnitCost				decimal(28,8), 
	totalunitcost					decimal(28,8), 
	totalUnitCostIncInterCo			decimal(28,8),
	currency						varchar(4),
	groupFIFOdate					datetime2, 
	arriveddate						datetime2, 
	confirmeddate					datetime2, 
	stockregistereddate				datetime2, 
	createddate						datetime2,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mend.inter_intransitstockitembatch_aud_hist add constraint [DF_mend.inter_intransitstockitembatch_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitstockitembatch_transferheader -------------------

create table mend.inter_intransitstockitembatch_transferheader (
	intransitstockitembatchid		bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitstockitembatch_transferheader add constraint [PK_mend_inter_intransitstockitembatch_transferheader]
	primary key clustered (intransitstockitembatchid,transferheaderid);
go
alter table mend.inter_intransitstockitembatch_transferheader add constraint [DF_mend_inter_intransitstockitembatch_transferheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_transferheader_aud(
	intransitstockitembatchid			bigint NOT NULL, 
	transferheaderid				bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitstockitembatch_transferheader_aud add constraint [PK_mend_inter_intransitstockitembatch_transferheader_aud]
	primary key clustered (intransitstockitembatchid,transferheaderid);
go
alter table mend.inter_intransitstockitembatch_transferheader_aud add constraint [DF_mend_inter_intransitstockitembatch_transferheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitstockitembatch_intransitbatchheader -------------------

create table mend.inter_intransitstockitembatch_intransitbatchheader (
	intransitstockitembatchid		bigint NOT NULL, 
	intransitbatchheaderid			bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitstockitembatch_intransitbatchheader add constraint [PK_mend_inter_intransitstockitembatch_intransitbatchheader]
	primary key clustered (intransitstockitembatchid,intransitbatchheaderid);
go
alter table mend.inter_intransitstockitembatch_intransitbatchheader add constraint [DF_mend_inter_intransitstockitembatch_intransitbatchheader_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_intransitbatchheader_aud(
	intransitstockitembatchid		bigint NOT NULL, 
	intransitbatchheaderid			bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitstockitembatch_intransitbatchheader_aud add constraint [PK_mend_inter_intransitstockitembatch_intransitbatchheader_aud]
	primary key clustered (intransitstockitembatchid,intransitbatchheaderid);
go
alter table mend.inter_intransitstockitembatch_intransitbatchheader_aud add constraint [DF_mend_inter_intransitstockitembatch_intransitbatchheader_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitstockitembatch_stockitem -------------------

create table mend.inter_intransitstockitembatch_stockitem (
	intransitstockitembatchid		bigint NOT NULL, 
	stockitemid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitstockitembatch_stockitem add constraint [PK_mend_inter_intransitstockitembatch_stockitem]
	primary key clustered (intransitstockitembatchid,stockitemid);
go
alter table mend.inter_intransitstockitembatch_stockitem add constraint [DF_mend_inter_intransitstockitembatch_stockitem_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_stockitem_aud(
	intransitstockitembatchid		bigint NOT NULL, 
	stockitemid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitstockitembatch_stockitem_aud add constraint [PK_mend_inter_intransitstockitembatch_stockitem_aud]
	primary key clustered (intransitstockitembatchid,stockitemid);
go
alter table mend.inter_intransitstockitembatch_stockitem_aud add constraint [DF_mend_inter_intransitstockitembatch_stockitem_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

------------------- intersitetransfer$intransitstockitembatch_receipt -------------------

create table mend.inter_intransitstockitembatch_receipt (
	intransitstockitembatchid		bigint NOT NULL, 
	receiptid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mend.inter_intransitstockitembatch_receipt add constraint [PK_mend_inter_intransitstockitembatch_receipt]
	primary key clustered (intransitstockitembatchid,receiptid);
go
alter table mend.inter_intransitstockitembatch_receipt add constraint [DF_mend_inter_intransitstockitembatch_receipt_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

create table mend.inter_intransitstockitembatch_receipt_aud(
	intransitstockitembatchid		bigint NOT NULL, 
	receiptid						bigint NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mend.inter_intransitstockitembatch_receipt_aud add constraint [PK_mend_inter_intransitstockitembatch_receipt_aud]
	primary key clustered (intransitstockitembatchid,receiptid);
go
alter table mend.inter_intransitstockitembatch_receipt_aud add constraint [DF_mend_inter_intransitstockitembatch_receipt_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go