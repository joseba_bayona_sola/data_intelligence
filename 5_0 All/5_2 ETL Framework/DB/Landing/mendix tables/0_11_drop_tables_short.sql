
------------------------ Drop tables -------------------------------

use Landing
go

------------ purchaseorders$shortageheader ------------

drop table mend.short_shortageheader;
go
drop table mend.short_shortageheader_aud;
go
drop table mend.short_shortageheader_aud_hist;
go

------------ purchaseorders$shortageheader_supplier ------------

drop table mend.short_shortageheader_supplier;
go
drop table mend.short_shortageheader_supplier_aud;
go

------------ purchaseorders$shortageheader_sourcewarehouse ------------

drop table mend.short_shortageheader_sourcewarehouse;
go
drop table mend.short_shortageheader_sourcewarehouse_aud;
go

------------ purchaseorders$shortageheader_destinationwarehouse ------------

drop table mend.short_shortageheader_destinationwarehouse;
go
drop table mend.short_shortageheader_destinationwarehouse_aud;
go

------------ purchaseorders$shortageheader_packsize ------------

drop table mend.short_shortageheader_packsize;
go
drop table mend.short_shortageheader_packsize_aud;
go






------------ purchaseorders$shortage ------------

drop table mend.short_shortage;
go
drop table mend.short_shortage_aud;
go
drop table mend.short_shortage_aud_hist;
go

------------ purchaseorders$shortage_shortageheader ------------

drop table mend.short_shortage_shortageheader;
go
drop table mend.short_shortage_shortageheader_aud;
go

------------ purchaseorders$shortage_purchaseorderline ------------

drop table mend.short_shortage_purchaseorderline;
go
drop table mend.short_shortage_purchaseorderline_aud;
go

------------ purchaseorders$shortage_supplier ------------

drop table mend.short_shortage_supplier;
go
drop table mend.short_shortage_supplier_aud;
go

------------ purchaseorders$shortage_product ------------

drop table mend.short_shortage_product;
go
drop table mend.short_shortage_product_aud;
go

------------ purchaseorders$shortage_standardprice ------------

drop table mend.short_shortage_standardprice;
go
drop table mend.short_shortage_standardprice_aud;
go

------------ purchaseorders$shortage_stockitem ------------


drop table mend.short_shortage_stockitem;
go
drop table mend.short_shortage_stockitem_aud;
go

------------ purchaseorders$shortage_warehousestockitem ------------

drop table mend.short_shortage_warehousestockitem;
go
drop table mend.short_shortage_warehousestockitem_aud;
go







------------ purchaseorders$orderlineshortage ------------

drop table mend.short_orderlineshortage;
go
drop table mend.short_orderlineshortage_aud;
go
drop table mend.short_orderlineshortage_aud_hist;
go

------------ purchaseorders$orderlineshortage_shortage ------------

drop table mend.short_orderlineshortage_shortage;
go
drop table mend.short_orderlineshortage_shortage_aud;
go

------------ purchaseorders$orderlineshortage_orderline ------------

drop table mend.short_orderlineshortage_orderline;
go
drop table mend.short_orderlineshortage_orderline_aud;
go