use Landing
go 

------------------- product$productcategory -------------------

drop view mend.prod_productcategory_aud_v
go 

create view mend.prod_productcategory_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		code, name, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			code, name, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_productcategory_aud
		union
		select 'H' record_type, id, 
			code, name, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_productcategory_aud_hist) t
go

------------------- product$productfamily -------------------

drop view mend.prod_productfamily_aud_v
go 

create view mend.prod_productfamily_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
		productFamilyType, 
		promotionalProduct, updateSNAPDescription, createToOrder, 
		createdDate, changedDate, 
		system$owner, system$changedBy, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
			productFamilyType, 
			promotionalProduct, updateSNAPDescription, createToOrder, 
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_productfamily_aud
		union
		select 'H' record_type, id, 
			magentoProductID, magentoSKU, name, manufacturerProductFamily, manufacturer, status, 
			productFamilyType, 
			promotionalProduct, updateSNAPDescription, createToOrder, 
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_productfamily_aud_hist) t
go

------------------- product$product -------------------

drop view mend.prod_product_aud_v
go 

create view mend.prod_product_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		productType, valid, 
		SKU, oldSKU, description, 
		isBOM, displaySubProductOnPackingSlip,
		createdDate, changedDate, 
		system$owner, system$changedBy, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			productType, valid, 
			SKU, oldSKU, description, 
			isBOM, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_product_aud
		union
		select 'H' record_type, id, 
			productType, valid, 
			SKU, oldSKU, description, 
			isBOM, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_product_aud_hist) t
go

------------------- product$stockitem -------------------

drop view mend.prod_stockitem_aud_v
go 

create view mend.prod_stockitem_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		SKU, oldSKU, SNAPDescription, packSize, 
		manufacturerArticleID, manufacturerCodeNumber,
		SNAPUploadStatus, 
		changed, 
		createdDate, changedDate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			SKU, oldSKU, SNAPDescription, packSize, 
			manufacturerArticleID, manufacturerCodeNumber,
			SNAPUploadStatus, 
			changed, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_stockitem_aud
		union
		select 'H' record_type, id, 
			SKU, oldSKU, SNAPDescription, packSize, 
			manufacturerArticleID, manufacturerCodeNumber,
			SNAPUploadStatus, 
			changed, 
			createdDate, changedDate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_stockitem_aud_hist) t
go

------------------- product$contactlens -------------------

drop view mend.prod_contactlens_aud_v 
go 

create view mend.prod_contactlens_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		bc, di, po, 
		cy, ax,  
		ad, _do, co, co_EDI, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			bc, di, po, 
			cy, ax,  
			ad, _do, co, co_EDI, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_contactlens_aud
		union
		select 'H' record_type, id, 
			bc, di, po, 
			cy, ax,  
			ad, _do, co, co_EDI, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_contactlens_aud_hist) t
go

------------------- product$packsize -------------------

drop view mend.prod_packsize_aud_v 
go 

create view mend.prod_packsize_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		description, size, 
		packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
		height, width, weight, depth, 
		productGroup1, productGroup2, productGroup2Type, 
		breakable, 
		packsperpallet, packspercarton, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			description, size, 
			packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
			height, width, weight, depth, 
			productGroup1, productGroup2, productGroup2Type, 
			breakable, 
			packsperpallet, packspercarton, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.prod_packsize_aud
		union
		select 'H' record_type, id, 
			description, size, 
			packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
			height, width, weight, depth, 
			productGroup1, productGroup2, productGroup2Type, 
			breakable, 
			packsperpallet, packspercarton, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.prod_packsize_aud_hist) t
go