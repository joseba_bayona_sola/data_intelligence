
--------------- WAREHOUSE RELATED -----------------------------

	select warehouseid, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore
	from Landing.mend.gen_wh_warehouse_v
	order by code

	select warehouseid, defaultcalendarid, 
		code, name, warehouseType, 
		mondayWorking, mondayCutOff, 
		tuesdayWorking, tuesdayCutOff, 
		wednesdayWorking, wednesdayCutOff, 
		thursdayWorking, thursdayCutOff, 
		fridayWorking, fridayCutOff, 
		saturdayWorking, saturdayCutOff, 
		sundayWorking, sundayCutOff, 
		timeHorizonWeeks
	from Landing.mend.gen_wh_warehouse_calendar_v
	order by code

	select warehouseid, shippingmethodid, 
		code, name, warehouseType, 
		magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
		leadtime, letterRate1, letterRate2, tracked, invoicedocs,
		defaultweight, minweight, scurrimappable,
		mondayDefaultCutOff, mondayWorking, mondayDeliveryWorking, mondayCollectionTime, 
		tuesdayDefaultCutOff, tuesdayWorking, tuesdayDeliveryWorking, tuesdayCollectionTime,
		wednesdayDefaultCutOff, wednesdayWorking, wednesdayDeliveryWorking, wednesdayCollectionTime,
		thursdayDefaultCutOff, thursdayWorking, thursdayDeliveryWorking, thursdayCollectionTime,
		fridayDefaultCutOff, fridayWorking, fridayDeliveryWorking, fridayCollectionTime,
		saturdayDefaultCutOff, saturdayWorking, saturdayDeliveryWorking, saturdayCollectionTime,
		sundayDefaultCutOff, sundayWorking, sundayDeliveryWorking, sundayCollectionTime
	from Landing.mend.gen_wh_warehouse_shippingmethod_v 
	order by code, magentoShippingDescription

	select warehouseid, supplierroutineid, 
		code, name, warehouseType, 
		supplierName, 
		mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
		tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
		wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
		thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
		fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder, 
		saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder, 
		sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder
	from Landing.mend.gen_wh_warehouse_supplier_v
	order by code, supplierName

	select warehouseid, preferenceid, countryid, 
		code, name, warehouseType, 
		country_code, countryname, order_num
	from Landing.mend.gen_wh_warehouse_country_v
	order by country_code, order_num

--------------- SUPPLIER RELATED -----------------------------

	select supplier_id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
		defaultLeadTime,
		flatFileConfigurationToUse,		
		vendorCode, vendorShortName, vendorStoreNumber,
		defaulttransitleadtime
	from Landing.mend.gen_supp_supplier_v
	order by supplierName

	select supplier_id, supplierpriceid,
		supplierID, supplierName, currencyCode, supplierType, 
		subMetaObjectName,
		unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
		directPrice, expiredPrice,
		totallt, shiplt, transferlt,
		comments,
		lastUpdated,
		createdDate, changedDate, 
		moq, effectiveDate, expiryDate, maxqty
	from Landing.mend.gen_supp_supplierprice_v
	order by supplierName, subMetaObjectName, unitPrice

--------------- PRODUCT RELATED -----------------------------

	select productfamilyid, 
		magentoProductID_int, 
		magentoProductID, magentoSKU, name, status
	from Landing.mend.gen_prod_productfamily_v
	order by magentoProductID

	select productfamilyid, packsizeid, 
		productfamilysizerank,
		magentoProductID_int,
		magentoProductID, magentoSKU, name, status,
		description, size, 
		packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
		weight, height, width, depth 
	from Landing.mend.gen_prod_productfamilypacksize_v 
	order by magentoProductID_int, productFamilySizeRank
	-- order by packsizeid

	select productfamilyid, packsizeid, supplier_id, supplierpriceid,
		magentoProductID_int, magentoProductID, magentoSKU, name, status, productFamilySizeRank, size, 
		supplierID, supplierName, subMetaObjectName, unitPrice, currencyCode, leadTime, 
		effectiveDate, expiryDate, active
	from Landing.mend.gen_prod_productfamilypacksize_supplierprice_v
	order by magentoProductID_int, productFamilySizeRank, supplierName


	select top 1000 productfamilyid, productid,
		magentoProductID_int, magentoProductID,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI
	from Landing.mend.gen_prod_product_v
	where magentoProductID_int = 1083
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co

	select top 1000 productfamilyid, productid, stockitemid, 
		magentoProductID_int, magentoProductID,
		magentoSKU, name, 
		valid, SKU_product, oldSKU, description, 
		packSize, SKU, stockitemdescription, -- si.oldSKU, 
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		manufacturerArticleID, manufacturerCodeNumber,
		SNAPUploadStatus
	from Landing.mend.gen_prod_stockitem_v
	where magentoProductID_int = 1083
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize


--------------- STOCK RELATED -----------------------------

	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid,
		warehouseid,
		magentoProductID_int, magentoProductID,
		magentoSKU, name, SKU_product, description, packSize, SKU, stockitemdescription,
		bc, di, po, cy, ax, ad, _do, co, co_EDI, 
		code, warehouse_name, id_string, 
		actualstockqty, availableqty, outstandingallocation, allocatedstockqty, onhold, dueinqty, forwarddemand, 
		stocked,
		stockingmethod
	from Landing.mend.gen_wh_warehousestockitem_v 
	where magentoProductID_int = 1083 
	order by magentoProductID_int, bc, di, po, cy, ax, ad, _do, co, packSize, code

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		receiptlineid, batchstockmovement_f, batchrepair_f,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription, packSize,
		code, warehouse_name, id_string, 
		batch_id,
		fullyallocated, fullyIssued, status, autoadjusted, 
		receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, registeredQuantity,
		groupFIFODate, arriveddate, confirmedDate, stockregisteredDate, receiptCreatedDate,
		productUnitCost, carriageUnitCost, dutyUnitCost, totalUnitCost, 
		interCoCarriageUnitCost, totalUnitCostIncInterCo,
		exchangeRate, currency
	from Landing.mend.gen_wh_warehousestockitembatch_v 
	where magentoProductID_int = 1083
		and stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO+0.50 PS30'
	order by magentoProductID_int, stockitemdescription, code, batch_id



	select repairstockbatchsid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, 
		batchtransaction_id, reference, date,
		oldallocation, newallocation, oldissue, newissue, processed, 

		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string, 
		batch_id
	from Landing.mend.gen_wh_warehousestockitembatch_repair_v
	where magentoProductID_int = 1083
	order by magentoProductID_int, stockitemdescription, code, batch_id, reference

	select stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, 
		moveid, movelineid, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		stockmovement_createdate, stockmovement_closeddate,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string

	from Landing.mend.gen_wh_stockmovement_v
	where magentoProductID_int = 1083
	order by stockmovement_createdate desc
	
	select batchstockmovementid, stockmovementid, productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid,
		moveid, movelineid, batchstockmovement_id, 
		stockadjustment, movementtype, movetype, _class, reasonid, status, 
		skuid, qtyactioned, 
		fromstate, tostate, fromstatus, tostatus, operator, supervisor,
		stockmovement_createdate, stockmovement_closeddate,
		batchstockmovement_createdate, batchstockmovementtype, quantity, comment,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription, 
		code, warehouse_name, id_string, 
		batch_id
	from Landing.mend.gen_wh_batchstockmovement_v
	order by moveid



--------------- ORDER RELATED -----------------------------

	select top 1000 orderid, basevaluesid, -- retailcustomerid, magwebstoreid,
		magentoOrderID_int, magentoOrderID,
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		_type, shipmentStatus, 
		allocationStatus, allocationStrategy, allocatedDate, 
		labelRenderer,  
		shippingMethod, shippingDescription, 
		subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
		discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
		shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
		adjustmentNegative, adjustmentPositive, 					
		custBalanceAmount, 
		customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
		grandTotal, totalInvoiced, totalCanceled, totalRefunded,
		toOrderRate, toGlobalRate, currencyCode
	from Landing.mend.gen_order_order_v
	-- where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	-- where magentoOrderID_int = 5485780847
	where _type = 'Wholesale_order' and substring(incrementID, 1, 1) = 'W' -- W - I
	order by orderid desc
	

	select top 1000 orderid, basevaluesid, orderlineid, productid, productfamilyid, -- retailcustomerid, magwebstoreid, 
		magentoOrderID_int, magentoOrderID,
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		status, packsOrdered, packsAllocated, packsShipped,
		magentoProductID_int, magentoProductID, 
		name, sku, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName
	from Landing.mend.gen_order_orderline_v
	-- where magentoOrderID_int between 5280000 and 5280393
	-- where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	order by orderid desc

	select top 1000 orderid, basevaluesid, orderlinemagentoid, productid, productfamilyid, -- retailcustomerid, magwebstoreid, 
		magentoOrderID_int, 
		incrementID, createdDate, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		lineAdded, deduplicate, fulfilled,
		magentoProductID_int, magentoProductID, 
		name, sku, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName
	from Landing.mend.gen_order_orderlinemagento_v
	-- where magentoOrderID_int between 5280000 and 5280393
	-- where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)
	order by orderid desc

--------------- SHIPMENT RELATED -----------------------------

	select top 1000 customershipmentid, orderid, 
		magentoOrderID_int, incrementID, createdDate_order, orderStatus,
		orderIncrementID, shipmentNumber, 
		status, warehouseMethod, labelRenderer, 
		shippingMethod, shippingDescription, 
		notify, fullyShipped, syncedToMagento, 	
		dispatchDate, expectedShippingDate, expectedDeliveryDate,  
		snap_timestamp,
		createdDate, changedDate
	from Landing.mend.gen_ship_customershipment_v
	order by magentoOrderID_int desc

--------------- ALLOCATION RELATED -----------------------------

	select top 1000 orderlinestockallocationtransactionid, -- batchstockallocationid
		warehouseid, orderid, orderlineid, warehousestockitemid, packsizeid, -- customershipmentlineid, -- warehousestockitembatchid, 
		magentoOrderID_int, incrementID, wh_name,
		transactionID, timestamp,
		allocationType, recordType, 
		allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
		stockAllocated, cancelled, 
		issuedDateTime
	from Landing.mend.gen_all_stockallocationtransaction_v 
	where orderid in (48695171003422761, 48695171003422760, 48695171003422655, 48695171003293824, 48695171003293822)

	select top 1000 magentoallocationid, orderlinestockallocationtransactionid,
		warehouseid, orderlinemagentoid,
		wh_name,
		shipped, cancelled_magentoall, 
		quantity
	from Landing.mend.gen_all_magentoallocation_v


	select top 1000 productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, 
		batchstockallocationid, orderlinestockallocationtransactionid,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription,
		code, warehouse_name, id_string, 
		batch_id, 
		fullyissued_alloc, cancelled, allocatedquantity_alloc, allocatedunits, issuedquantity_alloc, 
		allocation_date
	from Landing.mend.gen_wh_warehousestockitembatch_alloc_v
	where magentoProductID_int = 1083
		and stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO+0.50 PS30'
	order by magentoProductID_int, stockitemdescription, batch_id, allocation_date

	select productfamilyid, productid, stockitemid, warehousestockitemid, warehousestockitembatchid, 
		batchstockallocationid, batchstockissueid,
		magentoProductID_int, magentoProductID,
		name, stockitemdescription, 
		code, warehouse_name, id_string, 
		batch_id,
		issueid, issuedquantity_issue, issue_date
	from Landing.mend.gen_wh_warehousestockitembatch_issue_v
	where magentoProductID_int = 1083
		and stockitemdescription = '1-Day Acuvue Moist BC8.5 DI14.2 PO+0.50 PS30'
	order by magentoProductID_int, stockitemdescription, batch_id, issueid




--------------- PURCHASE ORDERS RELATED -----------------------------

	select purchaseorderid, warehouseid, supplier_id, 
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype,
		leadtime, 
		itemcost, totalprice, formattedprice, 
		createddate, duedate, receiveddate, 
		approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby
	from Landing.mend.gen_purc_purchaseorder_v 
	order by ordernumber desc

	select purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype, createddate,
		leadtime, 
		itemcost, totalprice, 
		status_lh, problems, 
		lines, items, total_price_lh
	from Landing.mend.gen_purc_purchaseorderlineheader_v
	order by ordernumber desc, supplierid

	select top 1000 purchaseorderlineid, purchaseorderlineheaderid, purchaseorderid, warehouseid, supplier_id, productfamilyid, packsizeid, supplierpriceid, stockitemid,
		code, warehouse_name, 
		supplierID, supplierName,
		ordernumber, source, status, potype, createddate,
		leadtime, 
		itemcost, totalprice, 
		status_lh, problems, 
		lines, items, total_price_lh, 
		po_lineID, 
		quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected,
		lineprice, 
		backtobackduedate
	from Landing.mend.gen_purc_purchaseorderline_v
	order by ordernumber, supplierid, po_lineid

--------------- SHORTAGE RELATED -----------------------------

	select shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName
	from Landing.mend.gen_short_shortageheader_v 
	order by shortageheaderid

	select shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate
	from Landing.mend.gen_short_shortage_v
	order by shortageheaderid, shortageid

	select orderlineshortageid, shortage_id, shortageheaderid, supplier_id, warehouseid_src, warehouse_dest, packsizeid,
		purchaseorderlineid, productid, stockitemid, warehousestockitemid, orderlineid, -- standardpriceid, 
		wholesale, 
		code_src, warehouse_name_src, code_dest, warehouse_name_dest, supplierID, supplierName, 
		shortageid, purchaseordernumber, ordernumbers,
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate,
		orderlineshortage_id,
		orderlineshortagetype,
		unitquantity_ol, packquantity_ol,
		shortageprogress, adviseddate, 
		createdDate_ol
	from Landing.mend.gen_short_orderlineshortage_v
	order by shortageheaderid, shortageid, orderlineshortage_id


--------------- WAREHOUSE SHIPMENT RELATED -----------------------------

	select receiptid, warehouseid, supplier_id, -- warehousestockitembatchid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, invoiceRef,
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, 
		dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
		totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
		lock,
		auditComment,
		createdDate
	from Landing.mend.gen_whship_receipt_v 
	order by createddate desc

	select receiptlineheaderid, receiptid, warehouseid, supplier_id, productfamilyid, packsizeid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, totalItemPrice, 
		createdDate,
		totalquantityordered, totalquantityaccepted, 
		unitcost, totalcostordered, totalcostaccepted
	from Landing.mend.gen_whship_receiptlineheader_v
	order by createdDate desc

	select top 1000 receiptlineid, receiptid, warehouseid, supplier_id, purchaseorderlineid, warehousestockitembatchid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, totalItemPrice, 
		createdDate, 
		line, stockitem, status_rl, syncstatus, syncresolution,
		unitCost, quantityordered, quantityreceived, quantityaccepted, quantityreturned, quantityrejected, 
		created
	from Landing.mend.gen_whship_receiptline_1_v 
	order by createdDate desc

	select top 1000 receiptlineid, receiptid, warehouseid, supplier_id, productfamilyid, packsizeid,
		purchaseorderlineid, warehousestockitembatchid,
		shipment_ID, receipt_ID, receiptNumber, orderNumber, 
		code, warehouse_name, supplierID, supplierName,
		status, shipmentType, totalItemPrice, 
		createdDate, 
		line, stockitem, status_rl, syncstatus, syncresolution,
		unitCost, quantityordered, quantityreceived, quantityaccepted, quantityreturned, quantityrejected, 
		created
	from Landing.mend.gen_whship_receiptline_2_v
	order by createdDate desc

--------------- INTERSITE TRANSFERS RELATED -----------------------------
	
	select transferheaderid, purchaseorderid_s, purchaseorderid_t, orderid,
		transfernum, allocatedstrategy, 
		totalremaining,
		createddate, 
		code_s, warehouse_name_s, supplierID_s, supplierName_s, ordernumber_s, 
		code_t, warehouse_name_t, supplierID_t, supplierName_t, ordernumber_t
	from Landing.mend.gen_inter_transferheader_v
	order by transfernum

	select intransitbatchheaderid, transferheaderid, purchaseorderid_s, purchaseorderid_t, -- customershipmentid, orderid, 
		transfernum, allocatedstrategy, totalremaining, createddate, ordernumber_s, ordernumber_t,
		createddate_ibh
		--, magentoOrderID_int, incrementID
	from Landing.mend.gen_inter_intransitbatchheader_v
	order by transfernum, createddate_ibh

	select intransitstockitembatchid, transferheaderid, intransitbatchheaderid, purchaseorderid_s, purchaseorderid_t, customershipmentid, stockitemid, shipmentid,
		transfernum, allocatedstrategy, totalremaining, createddate, ordernumber_s, ordernumber_t, 
		createddate_ibh, magentoOrderID_int, incrementID,
		issuedquantity, remainingquantity, 
		used,
		productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
		currency,
		groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
		createddate_isib
	from Landing.mend.gen_inter_intransitstockitembatch_v
	order by transfernum, createddate_ibh, stockitemid

--------------- SNAP RECEIPT RELATED -----------------------------

	select snapreceiptid, shipmentid,
		receiptid, 
		lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail,  priority, 
		status, stockstatus, 
		ordertype, orderclass, 
		suppliername, consignmentID, 
		weight, actualWeight, volume, 
		dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
		datesfixed
	from Landing.mend.gen_rec_snapreceipt_v
	order by receiptid

	select snapreceiptlineid, snapreceiptid, shipmentid, stockitemid,
		receiptid, lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail, ordertype, orderclass, 
		line, skuid, 
		status, level, unitofmeasure, 
		qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected
	from Landing.mend.gen_rec_snapreceiptline_v
	order by receiptid, line

--------------- VAT RELATED -----------------------------

	select commodityid, productfamilyid,
		commoditycode, commodityname, 
		magentoProductID_int, magentoProductID, magentoSKU, name
	from Landing.mend.gen_vat_commodity_v
	order by commoditycode, magentoProductID_int

	select dispensingservicesrateid, commodityid, productfamilyid, countryid,
		commoditycode, commodityname, dispensingservicesrate, rateeffectivedate, rateenddate,
		magentoProductID_int, magentoProductID, magentoSKU, name
	from Landing.mend.gen_vat_dispensingservicesrate_v
	order by commoditycode, magentoProductID_int

	select vatratingid, countryid, vatrateid,
		vatratingcode, vatratingdescription, 
		countryname, 
		vatrate, vatrateeffectivedate, vatrateenddate
	from Landing.mend.gen_vat_vatrating_v
	order by countryname, vatratingdescription

	select vatscheduleid, vatratingid, countryid, vatrateid, commodityid,
		vatratingcode, vatratingdescription, countryname, vatrate, vatrateeffectivedate, vatrateenddate,
		commoditycode, commodityname,
		vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate
	from Landing.mend.gen_vat_vatschedule_v
	order by countryname, vatratingdescription, commoditycode, vatsaletype

