use Landing
go 

------------ snapreceipts$snapreceipt ------------

drop view mend.rec_snapreceipt_aud_v
go

create view mend.rec_snapreceipt_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		receiptid, 
		lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail,  priority, 
		status, stockstatus, 
		ordertype, orderclass, 
		suppliername, consignmentID, 
		weight, actualWeight, volume, 
		dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
		datesfixed, 
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, id, 
			receiptid, 
			lines, lineqty, 	
			receiptStatus, receiptprocessStatus, processDetail,  priority, 
			status, stockstatus, 
			ordertype, orderclass, 
			suppliername, consignmentID, 
			weight, actualWeight, volume, 
			dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
			datesfixed, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.rec_snapreceipt_aud
		union
		select 'H' record_type, id, 
			receiptid, 
			lines, lineqty, 	
			receiptStatus, receiptprocessStatus, processDetail,  priority, 
			status, stockstatus, 
			ordertype, orderclass, 
			suppliername, consignmentID, 
			weight, actualWeight, volume, 
			dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
			datesfixed, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.rec_snapreceipt_aud_hist) t
go






------------ snapreceipts$snapreceiptline ------------

drop view mend.rec_snapreceiptline_aud_v
go

create view mend.rec_snapreceiptline_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		line, skuid, 
		status, level, unitofmeasure, 
		qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, 
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, id, 
			line, skuid, 
			status, level, unitofmeasure, 
			qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.rec_snapreceiptline_aud
		union
		select 'H' record_type, id, 
			line, skuid, 
			status, level, unitofmeasure, 
			qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.rec_snapreceiptline_aud_hist) t
go

