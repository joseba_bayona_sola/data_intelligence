
------------------------ Profile Scripts ----------------------------

use Landing
go

------------ inventory$warehousestockitem ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_warehousestockitem

select id, 
	id_string, 
	availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
	outstandingallocation, onhold,
	stockingmethod, 
	changed,
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.wh_warehousestockitem

select top 1000 id, 
	id_string, 
	availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
	outstandingallocation, onhold,
	stockingmethod, 
	changed,
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousestockitem_aud

select id, 
	id_string, 
	availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
	outstandingallocation, onhold,
	stockingmethod, 
	changed,
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_warehousestockitem_aud_hist

------------ inventory$located_at ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_located_at

select warehousestockitemid, warehouseid,
	idETLBatchRun, ins_ts
from mend.wh_located_at

select warehousestockitemid, warehouseid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_located_at_aud

------------ inventory$warehousestockitem_stockitem ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_warehousestockitem_stockitem

select warehousestockitemid, stockitemid,
	idETLBatchRun, ins_ts
from mend.wh_warehousestockitem_stockitem

select warehousestockitemid, stockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousestockitem_stockitem_aud





------------ inventory$warehousestockitembatch ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_warehousestockitembatch

select id,
	batch_id,
	fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
	receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
	groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
	productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
	exchangeRate, currency,
	createdDate, changeddate,
	idETLBatchRun, ins_ts
from mend.wh_warehousestockitembatch

select top 1000 id,
	batch_id,
	fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
	receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
	groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
	productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
	exchangeRate, currency,
	createdDate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousestockitembatch_aud

select id,
	batch_id,
	fullyallocated, fullyIssued, status, autoadjusted, arriveddate,  
	receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
	groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
	productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
	exchangeRate, currency,
	createdDate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_warehousestockitembatch_aud_hist

------------ inventory$warehousestockitembatch_warehousestockitem ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_warehousestockitembatch_warehousestockitem

select warehousestockitembatchid, warehousestockitemid,
	idETLBatchRun, ins_ts
from mend.wh_warehousestockitembatch_warehousestockitem

select warehousestockitembatchid, warehousestockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_warehousestockitembatch_warehousestockitem_aud



------------ inventory$warehousestockitembatch ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_warehousestockitembatch_full

select id,
	batch_id,
	idETLBatchRun, ins_ts
from mend.wh_warehousestockitembatch_full


------------ inventory$batchstockissue ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchstockissue

select id, 
	issueid, createddate, changeddate, issuedquantity, 
	cancelled, shippeddate,
	idETLBatchRun, ins_ts
from mend.wh_batchstockissue

select top 1000 id, 
	issueid, createddate, changeddate, issuedquantity, 
	cancelled, shippeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockissue_aud

select id, 
	issueid, createddate, changeddate, issuedquantity, 
	cancelled, shippeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_batchstockissue_aud_hist

------------ inventory$batchstockissue_warehousestockitembatch ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchstockissue_warehousestockitembatch

select batchstockissueid, warehousestockitembatchid,
	idETLBatchRun, ins_ts
from mend.wh_batchstockissue_warehousestockitembatch

select top 1000 batchstockissueid, warehousestockitembatchid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockissue_warehousestockitembatch_aud




------------ inventory$batchstockallocation ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchstockallocation

select id, 
	fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
	createddate, changeddate,
	idETLBatchRun, ins_ts
from mend.wh_batchstockallocation

select top 1000 id, 
	fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockallocation_aud

select id, 
	fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
	createddate, changeddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_batchstockallocation_aud_hist

------------ inventory$batchstockissue_batchstockallocation ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchstockissue_batchstockallocation

select batchstockissueid, batchstockallocationid,
	idETLBatchRun, ins_ts
from mend.wh_batchstockissue_batchstockallocation

select batchstockissueid, batchstockallocationid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockissue_batchstockallocation_aud


------------ inventory$batchtransaction_orderlinestockallocationtransaction ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchtransaction_orderlinestockallocationtransaction

select batchstockallocationid, orderlinestockallocationtransactionid,
	idETLBatchRun, ins_ts
from mend.wh_batchtransaction_orderlinestockallocationtransaction

select batchstockallocationid, orderlinestockallocationtransactionid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchtransaction_orderlinestockallocationtransaction_aud


------------ inventory$batchtransaction_warehousestockitembatch ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_batchtransaction_warehousestockitembatch

select batchstockallocationid, warehousestockitembatchid,
	idETLBatchRun, ins_ts
from mend.wh_batchtransaction_warehousestockitembatch

select batchstockallocationid, warehousestockitembatchid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchtransaction_warehousestockitembatch_aud






------------ inventory$repairstockbatchs ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_repairstockbatchs

select id, 
	batchtransaction_id, reference, date,
	oldallocation, newallocation, oldissue, newissue, processed,
	idETLBatchRun, ins_ts
from mend.wh_repairstockbatchs

select id, 
	batchtransaction_id, reference, date,
	oldallocation, newallocation, oldissue, newissue, processed,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_repairstockbatchs_aud

select id, 
	batchtransaction_id, reference, date,
	oldallocation, newallocation, oldissue, newissue, processed,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_repairstockbatchs_aud_hist


------------ inventory$repairstockbatchs_warehousestockitembatch ------------

select top 1000 count(*) over () num_tot, *
from mend.wh_repairstockbatchs_warehousestockitembatch

select repairstockbatchsid, warehousestockitembatchid,
	idETLBatchRun, ins_ts
from mend.wh_repairstockbatchs_warehousestockitembatch

select repairstockbatchsid, warehousestockitembatchid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_repairstockbatchs_warehousestockitembatch_aud





------------ inventory$stockmovement ------------

select *
from mend.wh_stockmovement

select id, 
	moveid, movelineid, 
	stockadjustment, movementtype, movetype, _class, reasonid, status, 
	skuid, qtyactioned, 
	fromstate, tostate, 
	fromstatus, tostatus, 
	operator, supervisor,
	dateCreated, dateClosed,
	createddate, changeddate, 
	idETLBatchRun, ins_ts
from mend.wh_stockmovement

select id, 
	moveid, movelineid, 
	stockadjustment, movementtype, movetype, _class, reasonid, status, 
	skuid, qtyactioned, 
	fromstate, tostate, 
	fromstatus, tostatus, 
	operator, supervisor,
	dateCreated, dateClosed,
	createddate, changeddate, 
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_stockmovement_aud

select id, 
	moveid, movelineid, 
	stockadjustment, movementtype, movetype, _class, reasonid, status, 
	skuid, qtyactioned, 
	fromstate, tostate, 
	fromstatus, tostatus, 
	operator, supervisor,
	dateCreated, dateClosed,
	createddate, changeddate, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_stockmovement_aud_hist

------------ inventory$stockmovement_warehousestockitem ------------

select *
from mend.wh_stockmovement_warehousestockitem

select stockmovementid, warehousestockitemid,
	idETLBatchRun, ins_ts
from mend.wh_stockmovement_warehousestockitem

select stockmovementid, warehousestockitemid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_stockmovement_warehousestockitem_aud





------------ inventory$batchstockmovement ------------

select *
from mend.wh_batchstockmovement

select id, batchstockmovement_id, batchstockmovementtype, 
	quantity, comment,
	createddate, changeddate, system$owner,
	idETLBatchRun, ins_ts
from mend.wh_batchstockmovement

select id, batchstockmovement_id, batchstockmovementtype, 
	quantity, comment,
	createddate, changeddate, system$owner,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockmovement_aud

select id, batchstockmovement_id, batchstockmovementtype, 
	quantity, comment,
	createddate, changeddate, system$owner,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.wh_batchstockmovement_aud_hist

------------ inventory$batchstockmovement_stockmovement ------------

select *
from mend.wh_batchstockmovement_stockmovement

select batchstockmovementid, stockmovementid,
	idETLBatchRun, ins_ts
from mend.wh_batchstockmovement_stockmovement

select batchstockmovementid, stockmovementid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockmovement_stockmovement_aud

------------ inventory$batchstockmovement_warehousestockitembatch ------------

select *
from mend.wh_batchstockmovement_warehousestockitembatch

select batchstockmovementid, warehousestockitembatchid,
	idETLBatchRun, ins_ts
from mend.wh_batchstockmovement_warehousestockitembatch

select batchstockmovementid, warehousestockitembatchid,
	idETLBatchRun, ins_ts, upd_ts
from mend.wh_batchstockmovement_warehousestockitembatch_aud