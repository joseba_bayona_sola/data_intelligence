use Landing
go 

------------------------------------------------------------------------
----------------------- product$warehouse ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehouse_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_warehouse_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehouse_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			code, name, shortName, warehouseType, snapWarehouse, 
			snapCode, scurriCode, 
			active, wms_active, useHoldingLabels, stockcheckignore,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehouse_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


------------------------------------------------------------------------
----------------------- inventory$warehousesupplier_warehouse ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousesupplier_warehouse_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- inventory$warehousesupplier_supplier ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehousesupplier_supplier_aud
	group by idETLBatchRun
	order by idETLBatchRun




------------------------------------------------------------------------
----------------------- inventory$supplierroutine ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_supplierroutine_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_supplierroutine_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_supplierroutine_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_supplierroutine_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, 
			supplierName,
			mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
			tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
			wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
			thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
			fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
			saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
			sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_supplierroutine_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom

------------------------------------------------------------------------
----------------------- inventory$supplierroutine_warehousesupplier ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_supplierroutine_warehousesupplier_aud
	group by idETLBatchRun
	order by idETLBatchRun



------------------------------------------------------------------------
----------------------- inventory$preference ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_preference_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_preference_aud

	select idETLBatchRun, count(*) num_rows
	from mend.wh_preference_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mend.wh_warehouse_aud_hist

	select *
	from
		(select count(*) over (partition by id) num_rows_rep,
			id, order_num,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_preference_aud_hist) t
	where num_rows_rep > 1
	order by id, aud_dateFrom


------------------------------------------------------------------------
----------------------- inventory$warehouse_preference ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_warehouse_preference_aud
	group by idETLBatchRun
	order by idETLBatchRun

------------------------------------------------------------------------
----------------------- inventory$country_preference ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mend.wh_country_preference_aud
	group by idETLBatchRun
	order by idETLBatchRun
