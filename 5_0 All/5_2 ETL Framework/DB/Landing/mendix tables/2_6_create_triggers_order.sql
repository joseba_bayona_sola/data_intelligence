
------------------------------- Triggers ----------------------------------

use Landing
go

------------ orderprocessing$order ------------

drop trigger mend.trg_order_order;
go 

create trigger mend.trg_order_order on mend.order_order
after insert
as
begin
	set nocount on

	merge into mend.order_order_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.incrementID, ' '), isnull(trg.magentoOrderID, ' '), isnull(trg.orderStatus, ' '), 
			isnull(trg.orderLinesMagento, 0), isnull(trg.orderLinesDeduped, 0),
			isnull(trg._type, ' '), isnull(trg.shipmentStatus, ' '), isnull(trg.statusUpdated, ' '), isnull(trg.allocationStatus, ' '), isnull(trg.allocationStrategy, ' '), 
			isnull(trg.allocatedDate, ' '), isnull(trg.promisedShippingDate, ' '), isnull(trg.promisedDeliveryDate, ' '), 
			isnull(trg.labelRenderer, ' '),
			isnull(trg.couponCode, ' '), isnull(trg.shippingMethod, ' '), isnull(trg.shippingDescription, ' '), isnull(trg.paymentMethod, ' '), isnull(trg.automatic_reorder, ' '), 
			isnull(trg.createdDate, ' ')-- , isnull(trg.changedDate, ' ') 
		intersect
		select 
			isnull(src.incrementID, ' '), isnull(src.magentoOrderID, ' '), isnull(src.orderStatus, ' '), 
			isnull(src.orderLinesMagento, 0), isnull(src.orderLinesDeduped, 0),
			isnull(src._type, ' '), isnull(src.shipmentStatus, ' '), isnull(src.statusUpdated, ' '), isnull(src.allocationStatus, ' '), isnull(src.allocationStrategy, ' '), 
			isnull(src.allocatedDate, ' '), isnull(src.promisedShippingDate, ' '), isnull(src.promisedDeliveryDate, ' '), 
			isnull(src.labelRenderer, ' '),
			isnull(src.couponCode, ' '), isnull(src.shippingMethod, ' '), isnull(src.shippingDescription, ' '), isnull(src.paymentMethod, ' '), isnull(src.automatic_reorder, ' '), 
			isnull(src.createdDate, ' ')-- , isnull(src.changedDate, ' ') 
			)
			
		then
			update set
				trg.incrementID = src.incrementID, trg.magentoOrderID = src.magentoOrderID, trg.orderStatus = src.orderStatus,
				trg.orderLinesMagento = src.orderLinesMagento, trg.orderLinesDeduped = src.orderLinesDeduped, 
				trg._type = src._type, trg.shipmentStatus = src.shipmentStatus, trg.statusUpdated = src.statusUpdated, trg.allocationStatus = src.allocationStatus, trg.allocationStrategy = src.allocationStrategy,  
				trg.allocatedDate = src.allocatedDate, trg.promisedShippingDate = src.promisedShippingDate, trg.promisedDeliveryDate = src.promisedDeliveryDate, 
				trg.labelRenderer = src.labelRenderer, 
				trg.couponCode = src.couponCode, trg.shippingMethod = src.shippingMethod, trg.shippingDescription = src.shippingDescription, trg.paymentMethod = src.paymentMethod, trg.automatic_reorder = src.automatic_reorder, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				incrementID, magentoOrderID, orderStatus, 
				orderLinesMagento, orderLinesDeduped,
				_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
				allocatedDate, promisedShippingDate, promisedDeliveryDate,
				labelRenderer,  
				couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
				createdDate, changedDate, 
				idETLBatchRun)

				values (src.id,
					src.incrementID, src.magentoOrderID, src.orderStatus,
					src.orderLinesMagento, src.orderLinesDeduped,
					src._type, src.shipmentStatus, src.statusUpdated, src.allocationStatus, src.allocationStrategy,
					src.allocatedDate, src.promisedShippingDate, src.promisedDeliveryDate,
					src.labelRenderer,
					src.couponCode, src.shippingMethod, src.shippingDescription, src.paymentMethod, src.automatic_reorder,
					src.createdDate, src.changedDate, 
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_order_aud;
go 

create trigger mend.trg_order_order_aud on mend.order_order_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_order_aud_hist (id,
		incrementID, magentoOrderID, orderStatus, 
		orderLinesMagento, orderLinesDeduped,
		_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
		allocatedDate, promisedShippingDate, promisedDeliveryDate,
		labelRenderer,  
		couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder,
		createdDate, changedDate, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.incrementID, d.magentoOrderID, d.orderStatus, 
			d.orderLinesMagento, d.orderLinesDeduped,
			d._type, d.shipmentStatus, d.statusUpdated, d.allocationStatus, d.allocationStrategy,
			d.allocatedDate, d.promisedShippingDate, d.promisedDeliveryDate,
			d.labelRenderer,  
			d.couponCode, d.shippingMethod, d.shippingDescription, d.paymentMethod, d.automatic_reorder,
			d.createdDate, d.changedDate, 
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$order_customer ------------

drop trigger mend.trg_order_order_customer;
go 

create trigger mend.trg_order_order_customer on mend.order_order_customer
after insert
as
begin
	set nocount on

	merge into mend.order_order_customer_aud with (tablock) as trg
	using inserted src
		on (trg.orderid = src.orderid and trg.retailcustomerid = src.retailcustomerid)
	when not matched 
		then
			insert (orderid, retailcustomerid, 
				idETLBatchRun)

				values (src.orderid, src.retailcustomerid,
					src.idETLBatchRun);
end; 
go

------------ orderprocessing$order_magwebstore ------------

drop trigger mend.trg_order_order_magwebstore;
go 

create trigger mend.trg_order_order_magwebstore on mend.order_order_magwebstore
after insert
as
begin
	set nocount on

	merge into mend.order_order_magwebstore_aud with (tablock) as trg
	using inserted src
		on (trg.orderid = src.orderid and trg.magwebstoreid = src.magwebstoreid)
	when not matched 
		then
			insert (orderid, magwebstoreid, 
				idETLBatchRun)

				values (src.orderid, src.magwebstoreid,
					src.idETLBatchRun);
end; 
go



------------ orderprocessing$basevalues ------------

drop trigger mend.trg_order_basevalues;
go 

create trigger mend.trg_order_basevalues on mend.order_basevalues
after insert
as
begin
	set nocount on

	merge into mend.order_basevalues_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.subtotal, 0), isnull(trg.subtotalInclTax, 0), isnull(trg.subtotalInvoiced, 0), isnull(trg.subtotalCanceled, 0), isnull(trg.subtotalRefunded, 0), 
			isnull(trg.discountAmount, 0), isnull(trg.discountInvoiced, 0), isnull(trg.discountCanceled, 0), isnull(trg.discountRefunded, 0),
			isnull(trg.shippingAmount, 0), isnull(trg.shippingInvoiced, 0), isnull(trg.shippingCanceled, 0), isnull(trg.shippingRefunded, 0), 
			isnull(trg.adjustmentNegative, 0), isnull(trg.adjustmentPositive, 0),
			isnull(trg.custBalanceAmount, 0), 
			isnull(trg.customerBalanceAmount, 0), isnull(trg.customerBalanceInvoiced, 0), isnull(trg.customerBalanceRefunded, 0), isnull(trg.customerBalanceTotRefunded, 0), isnull(trg.customerBalanceTotalRefunded, 0),
			isnull(trg.grandTotal, 0), isnull(trg.totalInvoiced, 0), isnull(trg.totalCanceled, 0), isnull(trg.totalRefunded, 0),
			isnull(trg.toOrderRate, 0), isnull(trg.toGlobalRate, 0), isnull(trg.currencyCode, ' '), 
			isnull(trg.mutualamount, 0)
		intersect
		select 
			isnull(src.subtotal, 0), isnull(src.subtotalInclTax, 0), isnull(src.subtotalInvoiced, 0), isnull(src.subtotalCanceled, 0), isnull(src.subtotalRefunded, 0), 
			isnull(src.discountAmount, 0), isnull(src.discountInvoiced, 0), isnull(src.discountCanceled, 0), isnull(src.discountRefunded, 0),
			isnull(src.shippingAmount, 0), isnull(src.shippingInvoiced, 0), isnull(src.shippingCanceled, 0), isnull(src.shippingRefunded, 0), 
			isnull(src.adjustmentNegative, 0), isnull(src.adjustmentPositive, 0),
			isnull(src.custBalanceAmount, 0), 
			isnull(src.customerBalanceAmount, 0), isnull(src.customerBalanceInvoiced, 0), isnull(src.customerBalanceRefunded, 0), isnull(src.customerBalanceTotRefunded, 0), isnull(src.customerBalanceTotalRefunded, 0),
			isnull(src.grandTotal, 0), isnull(src.totalInvoiced, 0), isnull(src.totalCanceled, 0), isnull(src.totalRefunded, 0),
			isnull(src.toOrderRate, 0), isnull(src.toGlobalRate, 0), isnull(src.currencyCode, ' '), 
			isnull(src.mutualamount, 0))
			
		then
			update set
				trg.subtotal = src.subtotal, trg.subtotalInclTax = src.subtotalInclTax, trg.subtotalInvoiced = src.subtotalInvoiced, trg.subtotalCanceled = src.subtotalCanceled, trg.subtotalRefunded = src.subtotalRefunded, 
				trg.discountAmount = src.discountAmount, trg.discountInvoiced = src.discountInvoiced, trg.discountCanceled = src.discountCanceled, trg.discountRefunded = src.discountRefunded,
				trg.shippingAmount = src.shippingAmount, trg.shippingInvoiced = src.shippingInvoiced, trg.shippingCanceled = src.shippingCanceled, trg.shippingRefunded = src.shippingRefunded, 
				trg.adjustmentNegative = src.adjustmentNegative, trg.adjustmentPositive = src.adjustmentPositive, 
				trg.custBalanceAmount = src.custBalanceAmount,
				trg.customerBalanceAmount = src.customerBalanceAmount, trg.customerBalanceInvoiced = src.customerBalanceInvoiced, trg.customerBalanceRefunded = src.customerBalanceRefunded, trg.customerBalanceTotRefunded = src.customerBalanceTotRefunded, trg.customerBalanceTotalRefunded = src.customerBalanceTotalRefunded, 
				trg.grandTotal = src.grandTotal, trg.totalInvoiced = src.totalInvoiced, trg.totalCanceled = src.totalCanceled, trg.totalRefunded = src.totalRefunded, 
				trg.toOrderRate = src.toOrderRate, trg.toGlobalRate = src.toGlobalRate, trg.currencyCode = src.currencyCode, 
				trg.mutualamount = src.mutualamount, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
				discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
				shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
				adjustmentNegative, adjustmentPositive, 				
				custBalanceAmount, 
				customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
				grandTotal, totalInvoiced, totalCanceled, totalRefunded,
				toOrderRate, toGlobalRate, currencyCode,
				mutualamount,
				idETLBatchRun)

				values (src.id,
					src.subtotal, src.subtotalInclTax, src.subtotalInvoiced, src.subtotalCanceled, src.subtotalRefunded, 
					src.discountAmount, src.discountInvoiced, src.discountCanceled, src.discountRefunded, 
					src.shippingAmount, src.shippingInvoiced, src.shippingCanceled, src.shippingRefunded, 					 
					src.adjustmentNegative, src.adjustmentPositive, 				
					src.custBalanceAmount, 
					src.customerBalanceAmount, src.customerBalanceInvoiced, src.customerBalanceRefunded, src.customerBalanceTotRefunded, src.customerBalanceTotalRefunded, 
					src.grandTotal, src.totalInvoiced, src.totalCanceled, src.totalRefunded,
					src.toOrderRate, src.toGlobalRate, src.currencyCode,
					src.mutualamount,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_basevalues_aud;
go 

create trigger mend.trg_order_basevalues_aud on mend.order_basevalues_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_basevalues_aud_hist (id,
		subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
		discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
		shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
		adjustmentNegative, adjustmentPositive, 				
		custBalanceAmount, 
		customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
		grandTotal, totalInvoiced, totalCanceled, totalRefunded,
		toOrderRate, toGlobalRate, currencyCode,
		mutualamount,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.subtotal, d.subtotalInclTax, d.subtotalInvoiced, d.subtotalCanceled, d.subtotalRefunded, 
			d.discountAmount, d.discountInvoiced, d.discountCanceled, d.discountRefunded, 
			d.shippingAmount, d.shippingInvoiced, d.shippingCanceled, d.shippingRefunded, 					 
			d.adjustmentNegative, d.adjustmentPositive, 				
			d.custBalanceAmount, 
			d.customerBalanceAmount, d.customerBalanceInvoiced, d.customerBalanceRefunded, d.customerBalanceTotRefunded, d.customerBalanceTotalRefunded, 
			d.grandTotal, d.totalInvoiced, d.totalCanceled, d.totalRefunded,
			d.toOrderRate, d.toGlobalRate, d.currencyCode,
			d.mutualamount,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$basevalues_order ------------

drop trigger mend.trg_order_basevalues_order;
go 

create trigger mend.trg_order_basevalues_order on mend.order_basevalues_order
after insert
as
begin
	set nocount on

	merge into mend.order_basevalues_order_aud with (tablock) as trg
	using inserted src
		on (trg.basevaluesid = src.basevaluesid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (basevaluesid, orderid, 
				idETLBatchRun)

				values (src.basevaluesid, src.orderid,
					src.idETLBatchRun);
end; 
go







------------ orderprocessing$order_statusupdate ------------

drop trigger mend.trg_order_order_statusupdate;
go 

create trigger mend.trg_order_order_statusupdate on mend.order_order_statusupdate
after insert
as
begin
	set nocount on

	merge into mend.order_order_statusupdate_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.status, ' '), isnull(trg.timestamp, ' ')
		intersect
		select 
			isnull(src.status, ' '), isnull(src.timestamp, ' '))
			
		then
			update set
				trg.status = src.status, trg.timestamp = src.timestamp, trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				status, timestamp,
				idETLBatchRun)

				values (src.id,
					src.status, src.timestamp,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_order_statusupdate_aud;
go 

create trigger mend.trg_order_order_statusupdate_aud on mend.order_order_statusupdate_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_order_statusupdate_aud_hist (id,
		status, timestamp,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.status, d.timestamp,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$statusupdate_order ------------

drop trigger mend.trg_order_statusupdate_order;
go 

create trigger mend.trg_order_statusupdate_order on mend.order_statusupdate_order
after insert
as
begin
	set nocount on

	merge into mend.order_statusupdate_order_aud with (tablock) as trg
	using inserted src
		on (trg.order_statusupdateid = src.order_statusupdateid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (order_statusupdateid, orderid, 
				idETLBatchRun)

				values (src.order_statusupdateid, src.orderid,
					src.idETLBatchRun);
end; 
go


------------ orderprocessing$orderprocessingtransaction ------------

drop trigger mend.trg_order_orderprocessingtransaction;
go 

create trigger mend.trg_order_orderprocessingtransaction on mend.order_orderprocessingtransaction
after insert
as
begin
	set nocount on

	merge into mend.order_orderprocessingtransaction_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.orderReference, ' '), isnull(trg.status, ' '), isnull(trg.detail, ' '), isnull(trg.timestamp, ' ')
		intersect
		select 
			isnull(src.orderReference, ' '), isnull(src.status, ' '), isnull(src.detail, ' '), isnull(src.timestamp, ' '))
			
		then
			update set
				trg.orderReference = src.orderReference, trg.status = src.status, trg.detail = src.detail, trg.timestamp = src.timestamp, trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				orderReference, status, detail, timestamp,
						idETLBatchRun)

				values (src.id,
					src.orderReference, src.status, src.detail, src.timestamp,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_orderprocessingtransaction_aud;
go 

create trigger mend.trg_order_orderprocessingtransaction_aud on mend.order_orderprocessingtransaction_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_orderprocessingtransaction_aud_hist (id,
		orderReference, status, detail, timestamp,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.orderReference, d.status, d.detail, d.timestamp,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$orderprocessingtransaction_order ------------

drop trigger mend.trg_order_orderprocessingtransaction_order;
go 

create trigger mend.trg_order_orderprocessingtransaction_order on mend.order_orderprocessingtransaction_order
after insert
as
begin
	set nocount on

	merge into mend.order_orderprocessingtransaction_order_aud with (tablock) as trg
	using inserted src
		on (trg.orderprocessingtransactionid = src.orderprocessingtransactionid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (orderprocessingtransactionid, orderid, 
				idETLBatchRun)

				values (src.orderprocessingtransactionid, src.orderid,
					src.idETLBatchRun);
end; 
go






------------ orderprocessing$orderlineabstract ------------

drop trigger mend.trg_order_orderlineabstract;
go 

create trigger mend.trg_order_orderlineabstract on mend.order_orderlineabstract
after insert
as
begin
	set nocount on

	merge into mend.order_orderlineabstract_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.magentoItemID, 0),
			isnull(trg.quantityOrdered, 0), isnull(trg.quantityInvoiced, 0), isnull(trg.quantityRefunded, 0), isnull(trg.quantityIssued, 0), isnull(trg.quantityAllocated, 0), isnull(trg.quantityShipped, 0), 
			isnull(trg.basePrice, 0), isnull(trg.baseRowPrice, 0),
			isnull(trg.allocated, 0), 
			isnull(trg.orderLineType, 0), isnull(trg.subMetaObjectName, 0), 
			isnull(trg.sku, 0), isnull(trg.eye, 0), isnull(trg.lensGroupID, 0),
			isnull(trg.netPrice, 0), isnull(trg.netRowPrice, 0),
			isnull(trg.createdDate, ' '), -- isnull(trg.changedDate, ' '), 
			isnull(trg.system$owner, 0), isnull(trg.system$changedBy, 0)
		intersect
		select 
			isnull(src.magentoItemID, 0),
			isnull(src.quantityOrdered, 0), isnull(src.quantityInvoiced, 0), isnull(src.quantityRefunded, 0), isnull(src.quantityIssued, 0), isnull(src.quantityAllocated, 0), isnull(src.quantityShipped, 0), 
			isnull(src.basePrice, 0), isnull(src.baseRowPrice, 0),
			isnull(src.allocated, 0), 
			isnull(src.orderLineType, 0), isnull(src.subMetaObjectName, 0), 
			isnull(src.sku, 0), isnull(src.eye, 0), isnull(src.lensGroupID, 0),
			isnull(src.netPrice, 0), isnull(src.netRowPrice, 0),
			isnull(src.createdDate, ' '), -- isnull(src.changedDate, ' '), 
			isnull(src.system$owner, 0), isnull(src.system$changedBy, 0))
			
		then
			update set
				trg.magentoItemID = src.magentoItemID, 
				trg.quantityOrdered = src.quantityOrdered, trg.quantityInvoiced = src.quantityInvoiced, trg.quantityRefunded = src.quantityRefunded, trg.quantityIssued = src.quantityIssued, trg.quantityAllocated = src.quantityAllocated, trg.quantityShipped = src.quantityShipped, 
				trg.basePrice = src.basePrice, trg.baseRowPrice = src.baseRowPrice,
				trg.allocated = src.allocated,
				trg.orderLineType = src.orderLineType, trg.subMetaObjectName = src.subMetaObjectName,
				trg.sku = src.sku, trg.eye = src.eye, trg.lensGroupID = src.lensGroupID, 
				trg.netPrice = src.netPrice, trg.netRowPrice = src.netRowPrice,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$owner = src.system$owner, trg.system$changedBy = src.system$changedBy,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				magentoItemID, 
				quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
				basePrice, baseRowPrice, 
				allocated, 
				orderLineType, subMetaObjectName,
				sku, eye, lensGroupID, 
				netPrice, netRowPrice,
				createdDate, changedDate, 
				system$owner, system$changedBy,
				idETLBatchRun)

				values (src.id,
					src.magentoItemID, 
					src.quantityOrdered, src.quantityInvoiced, src.quantityRefunded, src.quantityIssued, src.quantityAllocated, src.quantityShipped, 
					src.basePrice, src.baseRowPrice, 
					src.allocated, 
					src.orderLineType, src.subMetaObjectName,
					src.sku, src.eye, src.lensGroupID, 
					src.netPrice, src.netRowPrice,
					src.createdDate, src.changedDate, 
					src.system$owner, src.system$changedBy,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_orderlineabstract_aud;
go 

create trigger mend.trg_order_orderlineabstract_aud on mend.order_orderlineabstract_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_orderlineabstract_aud_hist (id,
		magentoItemID, 
		quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
		basePrice, baseRowPrice, 
		allocated, 
		orderLineType, subMetaObjectName,
		sku, eye, lensGroupID, 
		netPrice, netRowPrice,
		createdDate, changedDate, 
		system$owner, system$changedBy,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.magentoItemID, 
			d.quantityOrdered, d.quantityInvoiced, d.quantityRefunded, d.quantityIssued, d.quantityAllocated, d.quantityShipped, 
			d.basePrice, d.baseRowPrice, 
			d.allocated, 
			d.orderLineType, d.subMetaObjectName,
			d.sku, d.eye, d.lensGroupID, 
			d.netPrice, d.netRowPrice,
			d.createdDate, d.changedDate, 
			d.system$owner, d.system$changedBy,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$orderlinemagento ------------

drop trigger mend.trg_order_orderlinemagento;
go 

create trigger mend.trg_order_orderlinemagento on mend.order_orderlinemagento
after insert
as
begin
	set nocount on

	merge into mend.order_orderlinemagento_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.lineAdded, ' '), isnull(trg.deduplicate, ' '), isnull(trg.fulfilled, ' ')
		intersect
		select 
			isnull(src.lineAdded, ' '), isnull(src.deduplicate, ' '), isnull(src.fulfilled, ' '))
			
		then
			update set
				trg.lineAdded = src.lineAdded, trg.deduplicate = src.deduplicate, trg.fulfilled = src.fulfilled,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, lineAdded, deduplicate, fulfilled,
				idETLBatchRun)

				values (src.id,
					src.lineAdded, src.deduplicate, src.fulfilled,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_orderlinemagento_aud;
go 

create trigger mend.trg_order_orderlinemagento_aud on mend.order_orderlinemagento_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_orderlinemagento_aud_hist (id,
		lineAdded, deduplicate, fulfilled,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.lineAdded, d.deduplicate, d.fulfilled,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$orderlinemagento_order ------------

drop trigger mend.trg_order_orderlinemagento_order;
go 

create trigger mend.trg_order_orderlinemagento_order on mend.order_orderlinemagento_order
after insert
as
begin
	set nocount on

	merge into mend.order_orderlinemagento_order_aud with (tablock) as trg
	using inserted src
		on (trg.orderlinemagentoid = src.orderlinemagentoid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (orderlinemagentoid, orderid, 
				idETLBatchRun)

				values (src.orderlinemagentoid, src.orderid,
					src.idETLBatchRun);
end; 
go

------------ orderprocessing$orderline ------------

drop trigger mend.trg_order_orderline;
go 

create trigger mend.trg_order_orderline on mend.order_orderline
after insert
as
begin
	set nocount on

	merge into mend.order_orderline_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.status, ' '), isnull(trg.packsOrdered, 0), isnull(trg.packsAllocated, 0), isnull(trg.packsShipped, 0)
		intersect
		select 
			isnull(src.status, ' '), isnull(src.packsOrdered, 0), isnull(src.packsAllocated, 0), isnull(src.packsShipped, 0))
			
		then
			update set
				trg.status = src.status, trg.packsOrdered = src.packsOrdered, trg.packsAllocated = src.packsAllocated, trg.packsShipped = src.packsShipped,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, status, packsOrdered, packsAllocated, packsShipped,
				idETLBatchRun)

				values (src.id,
					src.status, src.packsOrdered, src.packsAllocated, src.packsShipped,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_orderline_aud;
go 

create trigger mend.trg_order_orderline_aud on mend.order_orderline_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_orderline_aud_hist (id,
		status, packsOrdered, packsAllocated, packsShipped,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.status, d.packsOrdered, d.packsAllocated, d.packsShipped,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$orderline_order ------------

drop trigger mend.trg_order_orderline_order;
go 

create trigger mend.trg_order_orderline_order on mend.order_orderline_order
after insert
as
begin
	set nocount on

	merge into mend.order_orderline_order_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineid = src.orderlineid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (orderlineid, orderid, 
				idETLBatchRun)

				values (src.orderlineid, src.orderid,
					src.idETLBatchRun);
end; 
go





------------ orderprocessing$shippinggroup ------------

drop trigger mend.trg_order_shippinggroup;
go 

create trigger mend.trg_order_shippinggroup on mend.order_shippinggroup
after insert
as
begin
	set nocount on

	merge into mend.order_shippinggroup_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.dispensingFee, ' '),
			isnull(trg.allocationStatus, ' '), isnull(trg.shipmentStatus, ' '),
			isnull(trg.wholesale, ' '),
			isnull(trg.letterBoxAble, ' '), isnull(trg.onHold, ' '), isnull(trg.adHoc, ' '), isnull(trg.cancelled, ' '), 
			isnull(trg.createdDate, ' ')-- , isnull(trg.changedDate, ' '), 
		intersect
		select 
			isnull(src.dispensingFee, ' '),
			isnull(src.allocationStatus, ' '), isnull(src.shipmentStatus, ' '),
			isnull(src.wholesale, ' '),
			isnull(src.letterBoxAble, ' '), isnull(src.onHold, ' '), isnull(src.adHoc, ' '), isnull(src.cancelled, ' '), 
			isnull(src.createdDate, ' ')-- , isnull(trg.changedDate, ' '), 
			)
			
		then
			update set
				trg.dispensingFee = src.dispensingFee,
				trg.allocationStatus = src.allocationStatus, trg.shipmentStatus = src.shipmentStatus,
				trg.wholesale = src.wholesale,
				trg.letterBoxAble = src.letterBoxAble, trg.onHold = src.onHold, trg.adHoc = src.adHoc, trg.cancelled = src.cancelled, 
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, dispensingFee, 
				allocationStatus, shipmentStatus, 
				wholesale, 
				letterBoxAble, onHold, adHoc, cancelled,
				createddate, changeddate,
				idETLBatchRun)

				values (src.id,
					src.dispensingFee, 
					src.allocationStatus, src.shipmentStatus, 
					src.wholesale, 
					src.letterBoxAble, src.onHold, src.adHoc, src.cancelled, 
					src.createddate, src.changeddate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_order_shippinggroup_aud;
go 

create trigger mend.trg_order_shippinggroup_aud on mend.order_shippinggroup_aud
for update, delete
as
begin
	set nocount on

	insert mend.order_shippinggroup_aud_hist (id,
		dispensingFee, 
		allocationStatus, shipmentStatus, 
		wholesale, 
		letterBoxAble, onHold, adHoc, cancelled,
		createddate, changeddate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.dispensingFee, 
			d.allocationStatus, d.shipmentStatus, 
			d.wholesale, 
			d.letterBoxAble, d.onHold, d.adHoc, d.cancelled,
			d.createddate, d.changeddate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ orderprocessing$shippinggroup_order ------------

drop trigger mend.trg_order_shippinggroup_order;
go 

create trigger mend.trg_order_shippinggroup_order on mend.order_shippinggroup_order
after insert
as
begin
	set nocount on

	merge into mend.order_shippinggroup_order_aud with (tablock) as trg
	using inserted src
		on (trg.shippinggroupid = src.shippinggroupid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (shippinggroupid, orderid, 
				idETLBatchRun)

				values (src.shippinggroupid, src.orderid,
					src.idETLBatchRun);
end; 
go

------------ orderprocessing$orderlineabstract_shippinggroup ------------

drop trigger mend.trg_order_orderlineabstract_shippinggroup;
go 

create trigger mend.trg_order_orderlineabstract_shippinggroup on mend.order_orderlineabstract_shippinggroup
after insert
as
begin
	set nocount on

	merge into mend.order_orderlineabstract_shippinggroup_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineabstractid = src.orderlineabstractid and trg.shippinggroupid = src.shippinggroupid)
	when not matched 
		then
			insert (orderlineabstractid, shippinggroupid, 
				idETLBatchRun)

				values (src.orderlineabstractid, src.shippinggroupid,
					src.idETLBatchRun);
end; 
go




------------ orderprocessing$orderline_product ------------

drop trigger mend.trg_order_orderline_product;
go 

create trigger mend.trg_order_orderline_product on mend.order_orderline_product
after insert
as
begin
	set nocount on

	merge into mend.order_orderline_product_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineabstractid = src.orderlineabstractid and trg.productid = src.productid)
	when not matched 
		then
			insert (orderlineabstractid, productid,
				idETLBatchRun)

				values (src.orderlineabstractid, src.productid,
					src.idETLBatchRun);
end; 
go

------------ orderprocessing$orderline_requiredstockitem ------------

drop trigger mend.trg_order_orderline_requiredstockitem;
go 

create trigger mend.trg_order_orderline_requiredstockitem on mend.order_orderline_requiredstockitem
after insert
as
begin
	set nocount on

	merge into mend.order_orderline_requiredstockitem_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineabstractid = src.orderlineabstractid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (orderlineabstractid, stockitemid,
				idETLBatchRun)

				values (src.orderlineabstractid, src.stockitemid,
					src.idETLBatchRun);
end; 
go


------------ orderprocessing$address_order ------------

drop trigger mend.trg_order_address_order;
go 

create trigger mend.trg_order_address_order on mend.order_address_order
after insert
as
begin
	set nocount on

	merge into mend.order_address_order_aud with (tablock) as trg
	using inserted src
		on (trg.orderaddressid = src.orderaddressid and trg.orderid = src.orderid)
	when not matched 
		then
			insert (orderaddressid, orderid,
				idETLBatchRun)

				values (src.orderaddressid, src.orderid,
					src.idETLBatchRun);
end; 
go


------------ orderprocessing$orderaddress_country ------------

drop trigger mend.trg_order_orderaddress_country;
go 

create trigger mend.trg_order_orderaddress_country on mend.order_orderaddress_country
after insert
as
begin
	set nocount on

	merge into mend.order_orderaddress_country_aud with (tablock) as trg
	using inserted src
		on (trg.orderaddressid = src.orderaddressid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (orderaddressid, countryid,
				idETLBatchRun)

				values (src.orderaddressid, src.countryid,
					src.idETLBatchRun);
end; 
go

-----------------------------------------------------------------------

drop trigger mend.trg_order_orderlineabstract_ol;
go 

create trigger mend.trg_order_orderlineabstract_ol on mend.order_orderlineabstract
after insert
as
begin
	set nocount on

	merge into mend.order_orderlineabstract_ol_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and src.subMetaObjectName = 'OrderProcessing.OrderLine' and not exists
		(select 
			isnull(trg.magentoItemID, 0),
			isnull(trg.quantityOrdered, 0), isnull(trg.quantityInvoiced, 0), isnull(trg.quantityRefunded, 0), isnull(trg.quantityIssued, 0), isnull(trg.quantityAllocated, 0), isnull(trg.quantityShipped, 0), 
			isnull(trg.basePrice, 0), isnull(trg.baseRowPrice, 0),
			isnull(trg.allocated, 0), 
			isnull(trg.orderLineType, 0), isnull(trg.subMetaObjectName, 0), 
			isnull(trg.sku, 0), isnull(trg.eye, 0), isnull(trg.lensGroupID, 0),
			isnull(trg.netPrice, 0), isnull(trg.netRowPrice, 0),
			isnull(trg.createdDate, ' '), -- isnull(trg.changedDate, ' '), 
			isnull(trg.system$owner, 0), isnull(trg.system$changedBy, 0)
		intersect
		select 
			isnull(src.magentoItemID, 0),
			isnull(src.quantityOrdered, 0), isnull(src.quantityInvoiced, 0), isnull(src.quantityRefunded, 0), isnull(src.quantityIssued, 0), isnull(src.quantityAllocated, 0), isnull(src.quantityShipped, 0), 
			isnull(src.basePrice, 0), isnull(src.baseRowPrice, 0),
			isnull(src.allocated, 0), 
			isnull(src.orderLineType, 0), isnull(src.subMetaObjectName, 0), 
			isnull(src.sku, 0), isnull(src.eye, 0), isnull(src.lensGroupID, 0),
			isnull(src.netPrice, 0), isnull(src.netRowPrice, 0),
			isnull(src.createdDate, ' '), -- isnull(src.changedDate, ' '), 
			isnull(src.system$owner, 0), isnull(src.system$changedBy, 0))
			
		then
			update set
				trg.magentoItemID = src.magentoItemID, 
				trg.quantityOrdered = src.quantityOrdered, trg.quantityInvoiced = src.quantityInvoiced, trg.quantityRefunded = src.quantityRefunded, trg.quantityIssued = src.quantityIssued, trg.quantityAllocated = src.quantityAllocated, trg.quantityShipped = src.quantityShipped, 
				trg.basePrice = src.basePrice, trg.baseRowPrice = src.baseRowPrice,
				trg.allocated = src.allocated,
				trg.orderLineType = src.orderLineType, trg.subMetaObjectName = src.subMetaObjectName,
				trg.sku = src.sku, trg.eye = src.eye, trg.lensGroupID = src.lensGroupID, 
				trg.netPrice = src.netPrice, trg.netRowPrice = src.netRowPrice,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$owner = src.system$owner, trg.system$changedBy = src.system$changedBy,
				trg.upd_ts = getutcdate()

	when not matched and src.subMetaObjectName = 'OrderProcessing.OrderLine' 
		then
			insert (id, 
				magentoItemID, 
				quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
				basePrice, baseRowPrice, 
				allocated, 
				orderLineType, subMetaObjectName,
				sku, eye, lensGroupID, 
				netPrice, netRowPrice,
				createdDate, changedDate, 
				system$owner, system$changedBy,
				idETLBatchRun)

				values (src.id,
					src.magentoItemID, 
					src.quantityOrdered, src.quantityInvoiced, src.quantityRefunded, src.quantityIssued, src.quantityAllocated, src.quantityShipped, 
					src.basePrice, src.baseRowPrice, 
					src.allocated, 
					src.orderLineType, src.subMetaObjectName,
					src.sku, src.eye, src.lensGroupID, 
					src.netPrice, src.netRowPrice,
					src.createdDate, src.changedDate, 
					src.system$owner, src.system$changedBy,
					src.idETLBatchRun);
end; 
go



drop trigger mend.trg_order_orderlineabstract_olm;
go 

create trigger mend.trg_order_orderlineabstract_olm on mend.order_orderlineabstract
after insert
as
begin
	set nocount on

	merge into mend.order_orderlineabstract_olm_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and src.subMetaObjectName = 'OrderProcessing.OrderLineMagento' and not exists
		(select 
			isnull(trg.magentoItemID, 0),
			isnull(trg.quantityOrdered, 0), isnull(trg.quantityInvoiced, 0), isnull(trg.quantityRefunded, 0), isnull(trg.quantityIssued, 0), isnull(trg.quantityAllocated, 0), isnull(trg.quantityShipped, 0), 
			isnull(trg.basePrice, 0), isnull(trg.baseRowPrice, 0),
			isnull(trg.allocated, 0), 
			isnull(trg.orderLineType, 0), isnull(trg.subMetaObjectName, 0), 
			isnull(trg.sku, 0), isnull(trg.eye, 0), isnull(trg.lensGroupID, 0),
			isnull(trg.netPrice, 0), isnull(trg.netRowPrice, 0),
			isnull(trg.createdDate, ' '), -- isnull(trg.changedDate, ' '), 
			isnull(trg.system$owner, 0), isnull(trg.system$changedBy, 0)
		intersect
		select 
			isnull(src.magentoItemID, 0),
			isnull(src.quantityOrdered, 0), isnull(src.quantityInvoiced, 0), isnull(src.quantityRefunded, 0), isnull(src.quantityIssued, 0), isnull(src.quantityAllocated, 0), isnull(src.quantityShipped, 0), 
			isnull(src.basePrice, 0), isnull(src.baseRowPrice, 0),
			isnull(src.allocated, 0), 
			isnull(src.orderLineType, 0), isnull(src.subMetaObjectName, 0), 
			isnull(src.sku, 0), isnull(src.eye, 0), isnull(src.lensGroupID, 0),
			isnull(src.netPrice, 0), isnull(src.netRowPrice, 0),
			isnull(src.createdDate, ' '), -- isnull(src.changedDate, ' '), 
			isnull(src.system$owner, 0), isnull(src.system$changedBy, 0))
			
		then
			update set
				trg.magentoItemID = src.magentoItemID, 
				trg.quantityOrdered = src.quantityOrdered, trg.quantityInvoiced = src.quantityInvoiced, trg.quantityRefunded = src.quantityRefunded, trg.quantityIssued = src.quantityIssued, trg.quantityAllocated = src.quantityAllocated, trg.quantityShipped = src.quantityShipped, 
				trg.basePrice = src.basePrice, trg.baseRowPrice = src.baseRowPrice,
				trg.allocated = src.allocated,
				trg.orderLineType = src.orderLineType, trg.subMetaObjectName = src.subMetaObjectName,
				trg.sku = src.sku, trg.eye = src.eye, trg.lensGroupID = src.lensGroupID, 
				trg.netPrice = src.netPrice, trg.netRowPrice = src.netRowPrice,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$owner = src.system$owner, trg.system$changedBy = src.system$changedBy,
				trg.upd_ts = getutcdate()

	when not matched and src.subMetaObjectName = 'OrderProcessing.OrderLineMagento' 
		then
			insert (id, 
				magentoItemID, 
				quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
				basePrice, baseRowPrice, 
				allocated, 
				orderLineType, subMetaObjectName,
				sku, eye, lensGroupID, 
				netPrice, netRowPrice,
				createdDate, changedDate, 
				system$owner, system$changedBy,
				idETLBatchRun)

				values (src.id,
					src.magentoItemID, 
					src.quantityOrdered, src.quantityInvoiced, src.quantityRefunded, src.quantityIssued, src.quantityAllocated, src.quantityShipped, 
					src.basePrice, src.baseRowPrice, 
					src.allocated, 
					src.orderLineType, src.subMetaObjectName,
					src.sku, src.eye, src.lensGroupID, 
					src.netPrice, src.netRowPrice,
					src.createdDate, src.changedDate, 
					src.system$owner, src.system$changedBy,
					src.idETLBatchRun);
end; 
go