
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- snapreceipts$snapreceipt -------------------

drop trigger mend.trg_rec_snapreceipt;
go 

create trigger mend.trg_rec_snapreceipt on mend.rec_snapreceipt
after insert
as
begin
	set nocount on

	merge into mend.rec_snapreceipt_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.receiptid, ' '), 
			isnull(trg.lines, ' '), isnull(trg.lineqty, 0),
			isnull(trg.receiptStatus, ' '), isnull(trg.receiptprocessStatus, ' '), isnull(trg.processDetail, ' '), isnull(trg.priority, ' '),
			isnull(trg.status, ' '), isnull(trg.stockstatus, ' '),
			isnull(trg.ordertype, ' '), isnull(trg.orderclass, ' '),
			isnull(trg.suppliername, ' '), isnull(trg.consignmentID, ' '), 
			isnull(trg.weight, 0), isnull(trg.actualWeight, 0), isnull(trg.volume, 0),
			isnull(trg.dateDueIn, ' '), isnull(trg.dateReceipt, ' '), isnull(trg.dateCreated, ' '), isnull(trg.dateArrival, ' '), isnull(trg.dateClosed, ' '),
			isnull(trg.datesfixed, ' ')
		intersect
		select 
			isnull(src.receiptid, ' '), 
			isnull(src.lines, ' '), isnull(src.lineqty, 0),
			isnull(src.receiptStatus, ' '), isnull(src.receiptprocessStatus, ' '), isnull(src.processDetail, ' '), isnull(src.priority, ' '),
			isnull(src.status, ' '), isnull(src.stockstatus, ' '),
			isnull(src.ordertype, ' '), isnull(src.orderclass, ' '),
			isnull(src.suppliername, ' '), isnull(src.consignmentID, ' '), 
			isnull(src.weight, 0), isnull(src.actualWeight, 0), isnull(src.volume, 0),
			isnull(src.dateDueIn, ' '), isnull(src.dateReceipt, ' '), isnull(src.dateCreated, ' '), isnull(src.dateArrival, ' '), isnull(src.dateClosed, ' '),
			isnull(src.datesfixed, ' '))
			
		then
			update set
				trg.receiptid = src.receiptid, 
				trg.lines = src.lines, trg.lineqty = src.lineqty,
				trg.receiptStatus = src.receiptStatus, trg.receiptprocessStatus = src.receiptprocessStatus, trg.processDetail = src.processDetail, trg.priority = src.priority,
				trg.status = src.status, trg.stockstatus = src.stockstatus,
				trg.ordertype = src.ordertype, trg.orderclass = src.orderclass,
				trg.suppliername = src.suppliername, trg.consignmentID = src.consignmentID,
				trg.weight = src.weight, trg.actualWeight = src.actualWeight, trg.volume = src.volume,
				trg.dateDueIn = src.dateDueIn, trg.dateReceipt = src.dateReceipt, trg.dateCreated = src.dateCreated, trg.dateArrival = src.dateArrival, trg.dateClosed = src.dateClosed,
				trg.datesfixed = src.datesfixed,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				receiptid, 
				lines, lineqty, 	
				receiptStatus, receiptprocessStatus, processDetail, priority, 
				status, stockstatus, 
				ordertype, orderclass, 
				suppliername, consignmentID, 
				weight, actualWeight, volume, 
				dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
				datesfixed, 
				idETLBatchRun)

				values (src.id,
					src.receiptid, 
					src.lines, src.lineqty, 	
					src.receiptStatus, src.receiptprocessStatus, src.processDetail, src.priority, 
					src.status, src.stockstatus, 
					src.ordertype, src.orderclass, 
					src.suppliername, src.consignmentID, 
					src.weight, src.actualWeight, src.volume, 
					src.dateDueIn, src.dateReceipt, src.dateCreated, src.dateArrival, src.dateClosed,  
					src.datesfixed,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_rec_snapreceipt_aud;
go 

create trigger mend.trg_rec_snapreceipt_aud on mend.rec_snapreceipt_aud
for update, delete
as
begin
	set nocount on

	insert mend.rec_snapreceipt_aud_hist (id,
		receiptid, 
		lines, lineqty, 	
		receiptStatus, receiptprocessStatus, processDetail,  priority, 
		status, stockstatus, 
		ordertype, orderclass, 
		suppliername, consignmentID, 
		weight, actualWeight, volume, 
		dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
		datesfixed,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.receiptid, 
			d.lines, d.lineqty, 	
			d.receiptStatus, d.receiptprocessStatus, d.processDetail,  d.priority, 
			d.status, d.stockstatus, 
			d.ordertype, d.orderclass, 
			d.suppliername, d.consignmentID, 
			d.weight, d.actualWeight, d.volume, 
			d.dateDueIn, d.dateReceipt, d.dateCreated, d.dateArrival, d.dateClosed,  
			d.datesfixed,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ snapreceipts$snapreceipt_receipt ------------

drop trigger mend.trg_rec_snapreceipt_receipt;
go 

create trigger mend.trg_rec_snapreceipt_receipt on mend.rec_snapreceipt_receipt
after insert
as
begin
	set nocount on

	merge into mend.rec_snapreceipt_receipt_aud with (tablock) as trg
	using inserted src
		on (trg.snapreceiptid = src.snapreceiptid and trg.receiptid = src.receiptid)
	when not matched 
		then
			insert (snapreceiptid, receiptid, 
				idETLBatchRun)

				values (src.snapreceiptid, src.receiptid,
					src.idETLBatchRun);
end; 
go






------------ snapreceipts$snapreceiptline ------------

drop trigger mend.trg_rec_snapreceiptline;
go 

create trigger mend.trg_rec_snapreceiptline on mend.rec_snapreceiptline
after insert
as
begin
	set nocount on

	merge into mend.rec_snapreceiptline_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.line, ' '), isnull(trg.skuid, ' '),
			isnull(trg.status, ' '), isnull(trg.level, ' '), isnull(trg.unitofmeasure, ' '),
			isnull(trg.qtyOrdered, 0), isnull(trg.qtyAdvised, 0), isnull(trg.qtyReceived, 0), isnull(trg.qtyDueIn, 0), isnull(trg.qtyRejected, 0)
		intersect
		select 
			isnull(src.line, ' '), isnull(src.skuid, ' '),
			isnull(src.status, ' '), isnull(src.level, ' '), isnull(src.unitofmeasure, ' '),
			isnull(src.qtyOrdered, 0), isnull(src.qtyAdvised, 0), isnull(src.qtyReceived, 0), isnull(src.qtyDueIn, 0), isnull(src.qtyRejected, 0))
			
		then
			update set
				trg.line = src.line, trg.skuid = src.skuid,
				trg.status = src.status, trg.level = src.level, trg.unitofmeasure = src.unitofmeasure,
				trg.qtyOrdered = src.qtyOrdered, trg.qtyAdvised = src.qtyAdvised, trg.qtyReceived = src.qtyReceived, trg.qtyDueIn = src.qtyDueIn, trg.qtyRejected = src.qtyRejected,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				line, skuid, 
				status, level, unitofmeasure, 
				qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected, 
				idETLBatchRun)

				values (src.id,
					src.line, src.skuid, 
					src.status, src.level, src.unitofmeasure, 
					src.qtyOrdered, src.qtyAdvised, src.qtyReceived, src.qtyDueIn, src.qtyRejected,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_rec_snapreceiptline_aud;
go 

create trigger mend.trg_rec_snapreceiptline_aud on mend.rec_snapreceiptline_aud
for update, delete
as
begin
	set nocount on

	insert mend.rec_snapreceiptline_aud_hist (id,
		line, skuid, 
		status, level, unitofmeasure, 
		qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.line, d.skuid, 
			d.status, d.level, d.unitofmeasure, 
			d.qtyOrdered, d.qtyAdvised, d.qtyReceived, d.qtyDueIn, d.qtyRejected,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ snapreceipts$snapreceiptline_snapreceipt ------------

drop trigger mend.trg_rec_snapreceiptline_snapreceipt;
go 

create trigger mend.trg_rec_snapreceiptline_snapreceipt on mend.rec_snapreceiptline_snapreceipt
after insert
as
begin
	set nocount on

	merge into mend.rec_snapreceiptline_snapreceipt_aud with (tablock) as trg
	using inserted src
		on (trg.snapreceiptlineid = src.snapreceiptlineid and trg.snapreceiptid = src.snapreceiptid)
	when not matched 
		then
			insert (snapreceiptlineid, snapreceiptid, 
				idETLBatchRun)

				values (src.snapreceiptlineid, src.snapreceiptid,
					src.idETLBatchRun);
end; 
go

------------ snapreceipts$snapreceiptline_stockitem ------------

drop trigger mend.trg_rec_snapreceiptline_stockitem;
go 

create trigger mend.trg_rec_snapreceiptline_stockitem on mend.rec_snapreceiptline_stockitem
after insert
as
begin
	set nocount on

	merge into mend.rec_snapreceiptline_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.snapreceiptlineid = src.snapreceiptlineid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (snapreceiptlineid, stockitemid, 
				idETLBatchRun)

				values (src.snapreceiptlineid, src.stockitemid,
					src.idETLBatchRun);
end; 
go