use Landing
go

------------------------------ SP -----------------------------------

---------------------- suppliers$supplier ---------------------------

drop procedure mend.srcmend_lnd_get_supp_supplier
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	25-05-2018	Joseba Bayona Sola	Remove isEDIEnabled
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplier
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				supplierID, supplierincrementid, supplierName, currencyCode, supplierType, NULL isEDIEnabled,
				vatCode, defaultLeadTime,
				email, telephone, fax,
				NULL flatFileConfigurationToUse,
				street1, street2, street3, city, region, postcode,
				vendorCode, vendorShortName, vendorStoreNumber, 
				defaulttransitleadtime, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					supplierID, supplierincrementid, supplierName, currencyCode, supplierType, 
					vatCode, defaultLeadTime,
					email, telephone, fax,
					
					street1, street2, street3, city, region, postcode,
					vendorCode, vendorShortName, vendorStoreNumber, 
					defaulttransitleadtime 
				from public."suppliers$supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- suppliers$supplier_country -------------------------

drop procedure mend.srcmend_lnd_get_supp_supplier_country
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplier_country
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplier_country
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select suppliers$supplierid supplierid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "suppliers$supplierid", "countrymanagement$countryid" 
				from public."suppliers$supplier_country"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- suppliers$supplier_company -------------------------

drop procedure mend.srcmend_lnd_get_supp_supplier_company
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplier_company
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplier_company
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select suppliers$supplierid supplierid, companyorganisation$companyid companyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "suppliers$supplierid", "companyorganisation$companyid" 
				from public."suppliers$supplier_company"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


drop procedure mend.srcmend_lnd_get_supp_supplier_currency
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-12-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplier_currency
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplier_currency
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select suppliers$supplierid supplierid, countrymanagement$currencyid currencyid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "suppliers$supplierid", "countrymanagement$currencyid" 
				from public."suppliers$supplier_currency"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go






------------------- suppliers$supplierprice -------------------------

drop procedure mend.srcmend_lnd_get_supp_supplierprice
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	23-01-2017	Joseba Bayona Sola	NULL on comments as problems with length of the attribute
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplierprice
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplierprice
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				subMetaObjectName,
				unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
				directPrice, expiredPrice,
				totallt, shiplt, transferlt,
				NULL comments,
				lastUpdated,
				createdDate, changedDate,
				system$owner, system$changedby, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					subMetaObjectName,
					unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
					directPrice, expiredPrice,
					totallt, shiplt, transferlt,
					comments,
					lastUpdated,
					createdDate, changedDate,
					"system$owner", "system$changedby" 
				from public."suppliers$supplierprice"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- suppliers$standardprice -------------------------

drop procedure mend.srcmend_lnd_get_supp_standardprice
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
	--	19-01-2017	Joseba Bayona Sola	Changes on names of attributes on Mendix Table + New (minordervalue, maxordervalue)
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_standardprice
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_standardprice
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select id,
					minqty moq, startdate effectiveDate, enddate expiryDate, maxqty, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
				'from openquery(MENDIX_DB_RESTORE,''
					select id,
						minqty, startdate, enddate, maxqty
					from public."suppliers$standardprice"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- suppliers$supplierprices_supplier -------------------------

drop procedure mend.srcmend_lnd_get_supp_supplierprices_supplier
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 27-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supplierprices_supplier
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplierprices_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select suppliers$supplierpriceid supplierpriceid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
				'from openquery(MENDIX_DB_RESTORE,''
					select "suppliers$supplierpriceid", "suppliers$supplierid" 
					from public."suppliers$supplierprices_supplier"'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go

------------------- suppliers$supplierprices_packsize -------------------------

drop procedure mend.srcmend_lnd_get_supp_supplierprices_packsize
go 

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 24-04-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_supplierprices_packsize
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_supplierprices_packsize
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select suppliers$supplierpriceid supplierpriceid, product$packsizeid packsizeid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
				'from openquery(MENDIX_DB_RESTORE,''
					select "suppliers$supplierpriceid", "product$packsizeid" 
					from public."suppliers$supplierprices_packsize"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.srcmend_lnd_get_supp_manufacturer
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_manufacturer
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_manufacturer
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, manufacturerid, manufacturername, createddate, changeddate, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select id, manufacturerid, manufacturername, createddate, changeddate  
				from public."suppliers$manufacturer"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



drop procedure mend.srcmend_lnd_get_supp_packsize_manufacturer
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - supp_packsize_manufacturer
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_supp_packsize_manufacturer
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select product$packsizeid packsizeid, suppliers$manufacturerid manufacturerid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' + 
			'from openquery(MENDIX_DB_RESTORE,''
				select "product$packsizeid", "suppliers$manufacturerid"  
				from public."product$packsize_manufacturer"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go
