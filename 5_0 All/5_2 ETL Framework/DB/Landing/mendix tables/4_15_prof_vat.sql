
------------------------ Profile Scripts ----------------------------

use Landing
go

------------------- vat$commodity ------------------- 

select *
from mend.vat_commodity

select id, commoditycode, commodityname,
	idETLBatchRun, ins_ts
from mend.vat_commodity

select id, commoditycode, commodityname,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_commodity_aud

select id, commoditycode, commodityname,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.vat_commodity_aud_hist

------------------- product$productfamily_commodity ------------------- 

select *
from mend.vat_productfamily_commodity

select productfamilyid, commodityid,
	idETLBatchRun, ins_ts
from mend.vat_productfamily_commodity

select productfamilyid, commodityid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_productfamily_commodity_aud




------------------- vat$dispensingservicesrate -------------------

select *
from mend.vat_dispensingservicesrate

select id, 
	dispensingservicesrate, rateeffectivedate, rateenddate,
	idETLBatchRun, ins_ts
from mend.vat_dispensingservicesrate

select id, 
	dispensingservicesrate, rateeffectivedate, rateenddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_dispensingservicesrate_aud

select id, 
	dispensingservicesrate, rateeffectivedate, rateenddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.vat_dispensingservicesrate_aud_hist

------------------- vat$dispensingservices_commodity -------------------

select *
from mend.vat_dispensingservices_commodity

select dispensingservicesrateid, commodityid,
	idETLBatchRun, ins_ts
from mend.vat_dispensingservices_commodity

select dispensingservicesrateid,commodityid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_dispensingservices_commodity_aud

------------------- vat$dispensingservicesrate_country -------------------

select *
from mend.vat_dispensingservicesrate_country

select dispensingservicesrateid, countryid,
	idETLBatchRun, ins_ts
from mend.vat_dispensingservicesrate_country

select dispensingservicesrateid,countryid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_dispensingservicesrate_country_aud








------------------- vat$vatrating -------------------

select *
from mend.vat_vatrating

select id, 
	vatratingcode, vatratingdescription,
	idETLBatchRun, ins_ts
from mend.vat_vatrating

select id, 
	vatratingcode, vatratingdescription,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatrating_aud

select id, 
	vatratingcode, vatratingdescription,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.vat_vatrating_aud_hist

------------------- vat$vatrating_country -------------------

select *
from mend.vat_vatrating_country

select vatratingid, countryid,
	idETLBatchRun, ins_ts
from mend.vat_vatrating_country

select vatratingid, countryid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatrating_country_aud




------------------- vat$vatrate -------------------

select *
from mend.vat_vatrate

select id, 
	vatrate, vatrateeffectivedate, vatrateenddate, 
	createddate, changeddate, 
	system$owner, system$changedby,
	idETLBatchRun, ins_ts
from mend.vat_vatrate

select id, 
	vatrate, vatrateeffectivedate, vatrateenddate, 
	createddate, changeddate, 
	system$owner, system$changedby,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatrate_aud

select id, 
	vatrate, vatrateeffectivedate, vatrateenddate, 
	createddate, changeddate, 
	system$owner, system$changedby,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.vat_vatrate_aud_hist

------------------- vat$vatrate_vatrating -------------------

select *
from mend.vat_vatrate_vatrating

select vatrateid, vatratingid,
	idETLBatchRun, ins_ts
from mend.vat_vatrate_vatrating

select vatrateid, vatratingid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatrate_vatrating_aud






------------------- vat$vatschedule -------------------

select *
from mend.vat_vatschedule

select id, 
	vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate,
	idETLBatchRun, ins_ts
from mend.vat_vatschedule

select id, 
	vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatschedule_aud

select id, 
	vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.vat_vatschedule_aud_hist

------------------- vat$vatschedule_vatrating -------------------

select *
from mend.vat_vatschedule_vatrating

select vatscheduleid, vatratingid,
	idETLBatchRun, ins_ts
from mend.vat_vatschedule_vatrating

select vatscheduleid, vatratingid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatschedule_vatrating_aud

------------------- vat$vatschedule_commodity -------------------

select *
from mend.vat_vatschedule_commodity

select vatscheduleid, commodityid,
	idETLBatchRun, ins_ts
from mend.vat_vatschedule_commodity

select vatscheduleid, commodityid,
	idETLBatchRun, ins_ts, upd_ts
from mend.vat_vatschedule_commodity_aud
