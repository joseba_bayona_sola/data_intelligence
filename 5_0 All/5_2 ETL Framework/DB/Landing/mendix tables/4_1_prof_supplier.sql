use Landing
go

------------------------ Profile Scripts ----------------------------

------------------- suppliers$supplier -------------------------

select *
from mend.supp_supplier

select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
	vatCode, defaultLeadTime,
	email, telephone, fax,
	flatFileConfigurationToUse,
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber,
	defaulttransitleadtime,
	idETLBatchRun, ins_ts
from mend.supp_supplier

select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
	vatCode, defaultLeadTime,
	email, telephone, fax,
	flatFileConfigurationToUse,
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber,
	defaulttransitleadtime,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplier_aud

select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, isEDIEnabled,
	vatCode, defaultLeadTime,
	email, telephone, fax,
	flatFileConfigurationToUse,
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber,
	defaulttransitleadtime,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.supp_supplier_aud_hist

------------------- suppliers$supplier_country -------------------------

select *
from mend.supp_supplier_country

select supplierid, countryid,
	idETLBatchRun, ins_ts
from mend.supp_supplier_country

select supplierid, countryid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplier_country_aud

------------------- suppliers$supplier_company -------------------------

select *
from mend.supp_supplier_company

select supplierid, companyid,
	idETLBatchRun, ins_ts
from mend.supp_supplier_company

select supplierid, companyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplier_company_aud


------------------- suppliers$supplier_currency -------------------------

select *
from mend.supp_supplier_currency

select supplierid, currencyid,
	idETLBatchRun, ins_ts
from mend.supp_supplier_currency

select supplierid, currencyid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplier_currency_aud




------------------- suppliers$supplierprice -------------------------

select *
from mend.supp_supplierprice

select id,
	subMetaObjectName,
	unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
	directPrice, expiredPrice,
	totallt, shiplt, transferlt,
	comments,
	lastUpdated,
	createdDate, changedDate,
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts
from mend.supp_supplierprice

select id,
	subMetaObjectName,
	unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
	directPrice, expiredPrice,
	totallt, shiplt, transferlt,
	comments,
	lastUpdated,
	createdDate, changedDate,
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplierprice_aud

select id,
	subMetaObjectName,
	unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple,
	directPrice, expiredPrice,
	totallt, shiplt, transferlt,
	comments,
	lastUpdated,
	createdDate, changedDate,
	system$owner, system$changedBy,
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.supp_supplierprice_aud_hist

------------------- suppliers$standardprice -------------------------

select *
from mend.supp_standardprice

select id,
		moq, effectiveDate, expiryDate, maxqty,
		idETLBatchRun, ins_ts
from mend.supp_standardprice

select id,
		moq, effectiveDate, expiryDate, maxqty,
		idETLBatchRun, ins_ts, upd_ts
from mend.supp_standardprice_aud

select id,
		moq, effectiveDate, expiryDate, maxqty,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.supp_standardprice_aud_hist

------------------- suppliers$supplierprices_supplier -------------------------

select *
from mend.supp_supplierprices_supplier

select supplierpriceid, supplierid,
	idETLBatchRun, ins_ts
from mend.supp_supplierprices_supplier

select supplierpriceid, supplierid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplierprices_supplier_aud

------------------- suppliers$supplierprices_packsize -------------------------

select *
from mend.supp_supplierprices_packsize

select supplierpriceid, packsizeid,
	idETLBatchRun, ins_ts
from mend.supp_supplierprices_packsize

select supplierpriceid, packsizeid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_supplierprices_packsize_aud



------------------- suppliers$manufacturer -------------------------

select *
from mend.supp_manufacturer

select id, manufacturerid, manufacturername, createddate, changeddate, 
	idETLBatchRun, ins_ts
from mend.supp_manufacturer

select id, manufacturerid, manufacturername, createddate, changeddate,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_manufacturer_aud

select id, manufacturerid, manufacturername, createddate, changeddate, 
	idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
from mend.supp_manufacturer_aud_hist


------------------- product$packsize_manufacturer -------------------------

select *
from mend.supp_packsize_manufacturer

select packsizeid, manufacturerid,
	idETLBatchRun, ins_ts
from mend.supp_packsize_manufacturer

select packsizeid, manufacturerid,
	idETLBatchRun, ins_ts, upd_ts
from mend.supp_packsize_manufacturer_aud
