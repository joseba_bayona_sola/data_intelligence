
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- calendar$shippingmethod -------------------

drop trigger mend.trg_cal_shippingmethod;
go 

create trigger mend.trg_cal_shippingmethod on mend.cal_shippingmethod
after insert
as
begin
	set nocount on

	merge into mend.cal_shippingmethod_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.magentoShippingDescription, ' '), isnull(trg.magentoShippingMethod, ' '), isnull(trg.scurriShippingMethod, ' '), isnull(trg.snapShippingMethod, ' '), 
			isnull(trg.leadtime, 0), isnull(trg.letterRate1, ' '), isnull(trg.letterRate2, ' '), isnull(trg.tracked, ' '), isnull(trg.invoicedocs, ' '),
			isnull(trg.defaultweight, 0), isnull(trg.minweight, 0), isnull(trg.scurrimappable, ' '),
			isnull(trg.mondayDefaultCutOff, ' '), isnull(trg.tuesdayDefaultCutOff, ' '), isnull(trg.wednesdayDefaultCutOff, ' '), isnull(trg.thursdayDefaultCutOff, ' '), 
				isnull(trg.fridayDefaultCutOff, ' '), isnull(trg.saturdayDefaultCutOff, ' '), isnull(trg.sundayDefaultCutOff, ' '), 
			isnull(trg.mondayWorking, ' '), isnull(trg.tuesdayWorking, ' '), isnull(trg.wednesdayWorking, ' '), isnull(trg.thursdayWorking, ' '), isnull(trg.fridayWorking, ' '), 
				isnull(trg.saturdayWorking, ' '), isnull(trg.sundayWorking, ' '),
			isnull(trg.mondayDeliveryWorking, ' '), isnull(trg.tuesdayDeliveryWorking, ' '), isnull(trg.wednesdayDeliveryWorking, ' '), isnull(trg.thursdayDeliveryWorking, ' '), 
				isnull(trg.fridayDeliveryWorking, ' '), isnull(trg.saturdayDeliveryWorking, ' '), isnull(trg.sundayDeliveryWorking, ' '),
			isnull(trg.mondayCollectionTime, ' '), isnull(trg.tuesdayCollectionTime, ' '), isnull(trg.wednesdayCollectionTime, ' '), isnull(trg.thursdayCollectionTime, ' '), 
				isnull(trg.fridayCollectionTime, ' '), isnull(trg.saturdayCollectionTime, ' '), isnull(trg.sundayCollectionTime, ' ')
		intersect
		select 
			isnull(src.magentoShippingDescription, ' '), isnull(src.magentoShippingMethod, ' '), isnull(src.scurriShippingMethod, ' '), isnull(src.snapShippingMethod, ' '), 
			isnull(src.leadtime, 0), isnull(src.letterRate1, ' '), isnull(src.letterRate2, ' '), isnull(src.tracked, ' '), isnull(src.invoicedocs, ' '),
			isnull(src.defaultweight, 0), isnull(src.minweight, 0), isnull(src.scurrimappable, ' '),
			isnull(src.mondayDefaultCutOff, ' '), isnull(src.tuesdayDefaultCutOff, ' '), isnull(src.wednesdayDefaultCutOff, ' '), isnull(src.thursdayDefaultCutOff, ' '), 
				isnull(src.fridayDefaultCutOff, ' '), isnull(src.saturdayDefaultCutOff, ' '), isnull(src.sundayDefaultCutOff, ' '), 
			isnull(src.mondayWorking, ' '), isnull(src.tuesdayWorking, ' '), isnull(src.wednesdayWorking, ' '), isnull(src.thursdayWorking, ' '), isnull(src.fridayWorking, ' '), 
				isnull(src.saturdayWorking, ' '), isnull(src.sundayWorking, ' '),
			isnull(src.mondayDeliveryWorking, ' '), isnull(src.tuesdayDeliveryWorking, ' '), isnull(src.wednesdayDeliveryWorking, ' '), isnull(src.thursdayDeliveryWorking, ' '), 
				isnull(src.fridayDeliveryWorking, ' '), isnull(src.saturdayDeliveryWorking, ' '), isnull(src.sundayDeliveryWorking, ' '),
			isnull(src.mondayCollectionTime, ' '), isnull(src.tuesdayCollectionTime, ' '), isnull(src.wednesdayCollectionTime, ' '), isnull(src.thursdayCollectionTime, ' '), 
				isnull(src.fridayCollectionTime, ' '), isnull(src.saturdayCollectionTime, ' '), isnull(src.sundayCollectionTime, ' '))
		then
			update set
				trg.magentoShippingDescription = src.magentoShippingDescription, trg.magentoShippingMethod = src.magentoShippingMethod, trg.scurriShippingMethod = src.scurriShippingMethod, trg.snapShippingMethod = src.snapShippingMethod,
				trg.leadtime = src.leadtime, trg.letterRate1 = src.letterRate1, trg.letterRate2 = src.letterRate2, trg.tracked = src.tracked, trg.invoicedocs = src.invoicedocs, 
				trg.defaultweight = src.defaultweight, trg.minweight = src.minweight, trg.scurrimappable = src.scurrimappable, 
				trg.mondayDefaultCutOff = src.mondayDefaultCutOff, trg.tuesdayDefaultCutOff = src.tuesdayDefaultCutOff, trg.wednesdayDefaultCutOff = src.wednesdayDefaultCutOff, trg.thursdayDefaultCutOff = src.thursdayDefaultCutOff, 
					trg.fridayDefaultCutOff = src.fridayDefaultCutOff, trg.saturdayDefaultCutOff = src.saturdayDefaultCutOff, trg.sundayDefaultCutOff = src.sundayDefaultCutOff,  
				trg.mondayWorking = src.mondayWorking, trg.tuesdayWorking = src.tuesdayWorking, trg.wednesdayWorking = src.wednesdayWorking, trg.thursdayWorking = src.thursdayWorking, 
					trg.fridayWorking = src.fridayWorking, trg.saturdayWorking = src.saturdayWorking, trg.sundayWorking = src.sundayWorking,
				trg.mondayDeliveryWorking = src.mondayDeliveryWorking, trg.tuesdayDeliveryWorking = src.tuesdayDeliveryWorking, trg.wednesdayDeliveryWorking = src.wednesdayDeliveryWorking, trg.thursdayDeliveryWorking = src.thursdayDeliveryWorking, 
					trg.fridayDeliveryWorking = src.fridayDeliveryWorking, trg.saturdayDeliveryWorking = src.saturdayDeliveryWorking, trg.sundayDeliveryWorking = src.sundayDeliveryWorking,
				trg.mondayCollectionTime = src.mondayCollectionTime, trg.tuesdayCollectionTime = src.tuesdayCollectionTime, trg.wednesdayCollectionTime = src.wednesdayCollectionTime, trg.thursdayCollectionTime = src.thursdayCollectionTime, 
					trg.fridayCollectionTime = src.fridayCollectionTime, trg.saturdayCollectionTime = src.saturdayCollectionTime, trg.sundayCollectionTime = src.sundayCollectionTime,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
				magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
				leadtime, letterRate1, letterRate2, tracked, invoicedocs,
				defaultweight, minweight, scurrimappable,
				mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
				mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
				mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
				mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime,
				idETLBatchRun)

				values (src.id,
					src.magentoShippingDescription, src.magentoShippingMethod, src.scurriShippingMethod, src.snapShippingMethod, 
					src.leadtime, src.letterRate1, src.letterRate2, src.tracked, src.invoicedocs,
					src.defaultweight, src.minweight, src.scurrimappable,
					src.mondayDefaultCutOff, src.tuesdayDefaultCutOff, src.wednesdayDefaultCutOff, src.thursdayDefaultCutOff, src.fridayDefaultCutOff, src.saturdayDefaultCutOff, src.sundayDefaultCutOff, 
					src.mondayWorking, src.tuesdayWorking, src.wednesdayWorking, src.thursdayWorking, src.fridayWorking, src.saturdayWorking, src.sundayWorking, 
					src.mondayDeliveryWorking, src.tuesdayDeliveryWorking, src.wednesdayDeliveryWorking, src.thursdayDeliveryWorking, src.fridayDeliveryWorking, src.saturdayDeliveryWorking, src.sundayDeliveryWorking,
					src.mondayCollectionTime, src.tuesdayCollectionTime, src.wednesdayCollectionTime, src.thursdayCollectionTime, src.fridayCollectionTime, src.saturdayCollectionTime, src.sundayCollectionTime,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_cal_shippingmethod_aud;
go 

create trigger mend.trg_cal_shippingmethod_aud on mend.cal_shippingmethod_aud
for update, delete
as
begin
	set nocount on

	insert mend.cal_shippingmethod_aud_hist (id,
		magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
		leadtime, letterRate1, letterRate2, tracked, invoicedocs,
		defaultweight, minweight, scurrimappable,
		mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
		mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
		mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking,
		mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.magentoShippingDescription, d.magentoShippingMethod, d.scurriShippingMethod, d.snapShippingMethod, 
			d.leadtime, d.letterRate1, d.letterRate2, d.tracked, d.invoicedocs,
			d.defaultweight, d.minweight, d.scurrimappable,
			d.mondayDefaultCutOff, d.tuesdayDefaultCutOff, d.wednesdayDefaultCutOff, d.thursdayDefaultCutOff, d.fridayDefaultCutOff, d.saturdayDefaultCutOff, d.sundayDefaultCutOff, 
			d.mondayWorking, d.tuesdayWorking, d.wednesdayWorking, d.thursdayWorking, d.fridayWorking, d.saturdayWorking, d.sundayWorking, 
			d.mondayDeliveryWorking, d.tuesdayDeliveryWorking, d.wednesdayDeliveryWorking, d.thursdayDeliveryWorking, d.fridayDeliveryWorking, d.saturdayDeliveryWorking, d.sundayDeliveryWorking,
			d.mondayCollectionTime, d.tuesdayCollectionTime, d.wednesdayCollectionTime, d.thursdayCollectionTime, d.fridayCollectionTime, d.saturdayCollectionTime, d.sundayCollectionTime,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- calendar$shippingmethods_warehouse -------------------

drop trigger mend.trg_cal_shippingmethods_warehouse;
go 

create trigger mend.trg_cal_shippingmethods_warehouse on mend.cal_shippingmethods_warehouse
after insert
as
begin
	set nocount on

	merge into mend.cal_shippingmethods_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.shippingmethodid = src.shippingmethodid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (shippingmethodid, warehouseid, 
				idETLBatchRun)

				values (src.shippingmethodid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------------- calendar$shippingmethods_destinationcountry -------------------

drop trigger mend.trg_cal_shippingmethods_destinationcountry;
go 

create trigger mend.trg_cal_shippingmethods_destinationcountry on mend.cal_shippingmethods_destinationcountry
after insert
as
begin
	set nocount on

	merge into mend.cal_shippingmethods_destinationcountry_aud with (tablock) as trg
	using inserted src
		on (trg.shippingmethodid = src.shippingmethodid and trg.countryid = src.countryid)
	when not matched 
		then
			insert (shippingmethodid, countryid, 
				idETLBatchRun)

				values (src.shippingmethodid, src.countryid,
					src.idETLBatchRun);
end; 
go






------------------- calendar$defaultcalendar -------------------

drop trigger mend.trg_cal_defaultcalendar;
go 

create trigger mend.trg_cal_defaultcalendar on mend.cal_defaultcalendar
after insert
as
begin
	set nocount on

	merge into mend.cal_defaultcalendar_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.mondayWorking, ' '), isnull(trg.tuesdayWorking, ' '), isnull(trg.wednesdayWorking, ' '), isnull(trg.thursdayWorking, ' '), 
				isnull(trg.fridayWorking, ' '), isnull(trg.saturdayWorking, ' '), isnull(trg.sundayWorking, ' '), 
			isnull(trg.mondayCutOff, ' '), isnull(trg.tuesdayCutOff, ' '), isnull(trg.wednesdayCutOff, ' '), isnull(trg.thursdayCutOff, ' '), isnull(trg.fridayCutOff, ' '), 
				isnull(trg.saturdayCutOff, ' '), isnull(trg.sundayCutOff, ' '),
			isnull(trg.timeHorizonWeeks, 0), 
			isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '), 
			isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.mondayWorking, ' '), isnull(src.tuesdayWorking, ' '), isnull(src.wednesdayWorking, ' '), isnull(src.thursdayWorking, ' '), 
				isnull(src.fridayWorking, ' '), isnull(src.saturdayWorking, ' '), isnull(src.sundayWorking, ' '), 
			isnull(src.mondayCutOff, ' '), isnull(src.tuesdayCutOff, ' '), isnull(trg.wednesdayCutOff, ' '), isnull(src.thursdayCutOff, ' '), 
				isnull(src.fridayCutOff, ' '), isnull(src.saturdayCutOff, ' '), isnull(src.sundayCutOff, ' '),
			isnull(src.timeHorizonWeeks, 0),
			isnull(src.createdDate, ' '), isnull(src.changedDate, ' '),
			isnull(src.system$changedby, 0))
		then
			update set
				trg.mondayWorking = src.mondayWorking, trg.tuesdayWorking = src.tuesdayWorking, trg.wednesdayWorking = src.wednesdayWorking, trg.thursdayWorking = src.thursdayWorking, 
					trg.fridayWorking = src.fridayWorking, trg.saturdayWorking = src.saturdayWorking, trg.sundayWorking = src.sundayWorking,
				trg.mondayCutOff = src.mondayCutOff, trg.tuesdayCutOff = src.tuesdayCutOff, trg.wednesdayCutOff = src.wednesdayCutOff, trg.thursdayCutOff = src.thursdayCutOff, 
					trg.fridayCutOff = src.fridayCutOff, trg.saturdayCutOff = src.saturdayCutOff, trg.sundayCutOff = src.sundayCutOff, 
				trg.timeHorizonWeeks = src.timeHorizonWeeks,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$changedby = src.system$changedby,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
					mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
					mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
					timeHorizonWeeks,
					createdDate, changedDate, 
					system$changedby,
					idETLBatchRun)

				values (src.id,
					src.mondayWorking, src.tuesdayWorking, src.wednesdayWorking, src.thursdayWorking, src.fridayWorking, src.saturdayWorking, src.sundayWorking, 
					src.mondayCutOff, src.tuesdayCutOff, src.wednesdayCutOff, src.thursdayCutOff, src.fridayCutOff, src.saturdayCutOff, src.sundayCutOff, 
					src.timeHorizonWeeks,
					src.createdDate, src.changedDate, 
					src.system$changedby,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_cal_defaultcalendar_aud;
go 

create trigger mend.trg_cal_defaultcalendar_aud on mend.cal_defaultcalendar_aud
for update, delete
as
begin
	set nocount on

	insert mend.cal_defaultcalendar_aud_hist (id,
		mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
		mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
		timeHorizonWeeks,
		createdDate, changedDate, 
		system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.mondayWorking, d.tuesdayWorking, d.wednesdayWorking, d.thursdayWorking, d.fridayWorking, d.saturdayWorking, d.sundayWorking, 
			d.mondayCutOff, d.tuesdayCutOff, d.wednesdayCutOff, d.thursdayCutOff, d.fridayCutOff, d.saturdayCutOff, d.sundayCutOff, 
			d.timeHorizonWeeks,
			d.createdDate, d.changedDate, 
			d.system$changedby,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- calendar$defaultcalendar_warehouse -------------------

drop trigger mend.trg_cal_defaultcalendar_warehouse;
go 

create trigger mend.trg_cal_defaultcalendar_warehouse on mend.cal_defaultcalendar_warehouse
after insert
as
begin
	set nocount on

	merge into mend.cal_defaultcalendar_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.defaultcalendarid = src.defaultcalendarid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (defaultcalendarid, warehouseid, 
				idETLBatchRun)

				values (src.defaultcalendarid, src.warehouseid,
					src.idETLBatchRun);
end; 
go






------------------- calendar$defaultwarehousesuppliercalender -------------------

drop trigger mend.trg_cal_defaultwarehousesuppliercalender;
go 

create trigger mend.trg_cal_defaultwarehousesuppliercalender on mend.cal_defaultwarehousesuppliercalender
after insert
as
begin
	set nocount on

	merge into mend.cal_defaultwarehousesuppliercalender_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.mondayCanSupply, ' '), isnull(trg.tuesdayCanSupply, ' '), isnull(trg.wednesdayCanSupply, ' '), isnull(trg.thursdayCanSupply, ' '), 
				isnull(trg.fridayCanSupply, ' '), isnull(trg.saturdayCanSupply, ' '), isnull(trg.sundayCanSupply, ' '), 
			isnull(trg.mondayCutOffTime, ' '), isnull(trg.tuesdayCutOffTime, ' '), isnull(trg.wednesdayCutOffTime, ' '), isnull(trg.thursdayCutOffTime, ' '), 
				isnull(trg.fridayCutOffTime, ' '), isnull(trg.saturdayCutOffTime, ' '), isnull(trg.sundayCutOffTime, ' '),
			isnull(trg.mondayCanOrder, ' '), isnull(trg.tuesdayCanOrder, ' '), isnull(trg.wednesdayCanOrder, ' '), isnull(trg.thursdayCanOrder, ' '), 
				isnull(trg.fridayCanOrder, ' '), isnull(trg.saturdayCanOrder, ' '), isnull(trg.sundayCanOrder, ' '),
			isnull(trg.timeHorizonWeeks, 0), 
			isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' '), 
			isnull(trg.system$changedby, 0)
		intersect
		select 
			isnull(src.mondayCanSupply, ' '), isnull(src.tuesdayCanSupply, ' '), isnull(src.wednesdayCanSupply, ' '), isnull(src.thursdayCanSupply, ' '), 
				isnull(src.fridayCanSupply, ' '), isnull(src.saturdayCanSupply, ' '), isnull(src.sundayCanSupply, ' '), 
			isnull(src.mondayCutOffTime, ' '), isnull(src.tuesdayCutOffTime, ' '), isnull(src.wednesdayCutOffTime, ' '), isnull(src.thursdayCutOffTime, ' '), 
				isnull(src.fridayCutOffTime, ' '), isnull(src.saturdayCutOffTime, ' '), isnull(src.sundayCutOffTime, ' '),
			isnull(src.mondayCanOrder, ' '), isnull(src.tuesdayCanOrder, ' '), isnull(src.wednesdayCanOrder, ' '), isnull(src.thursdayCanOrder, ' '), 
				isnull(src.fridayCanOrder, ' '), isnull(src.saturdayCanOrder, ' '), isnull(src.sundayCanOrder, ' '),
			isnull(src.timeHorizonWeeks, 0),
			isnull(src.createdDate, ' '), isnull(src.changedDate, ' '),
			isnull(src.system$changedby, 0))
		then
			update set
				trg.mondayCanSupply = src.mondayCanSupply, trg.tuesdayCanSupply = src.tuesdayCanSupply, trg.wednesdayCanSupply = src.wednesdayCanSupply, trg.thursdayCanSupply = src.thursdayCanSupply, 
					trg.fridayCanSupply = src.fridayCanSupply, trg.saturdayCanSupply = src.saturdayCanSupply, trg.sundayCanSupply = src.sundayCanSupply,
				trg.mondayCutOffTime = src.mondayCutOffTime, trg.tuesdayCutOffTime = src.tuesdayCutOffTime, trg.wednesdayCutOffTime = src.wednesdayCutOffTime, trg.thursdayCutOffTime = src.thursdayCutOffTime, 
					trg.fridayCutOffTime = src.fridayCutOffTime, trg.saturdayCutOffTime = src.saturdayCutOffTime, trg.sundayCutOffTime = src.sundayCutOffTime,
				trg.mondayCanOrder = src.mondayCanOrder, trg.tuesdayCanOrder = src.tuesdayCanOrder, trg.wednesdayCanOrder = src.wednesdayCanOrder, trg.thursdayCanOrder = src.thursdayCanOrder, 
					trg.fridayCanOrder = src.fridayCanOrder, trg.saturdayCanOrder = src.saturdayCanOrder, trg.sundayCanOrder = src.sundayCanOrder, 
				trg.timeHorizonWeeks = src.timeHorizonWeeks,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate,
				trg.system$changedby = src.system$changedby,
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, 
					mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
					mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
					mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
					timeHorizonWeeks,
					createdDate, changedDate, 
					system$changedby,
					idETLBatchRun)

				values (src.id,
					src.mondayCanSupply, src.tuesdayCanSupply, src.wednesdayCanSupply, src.thursdayCanSupply, src.fridayCanSupply, src.saturdayCanSupply, src.sundayCanSupply, 
					src.mondayCutOffTime, src.tuesdayCutOffTime, src.wednesdayCutOffTime, src.thursdayCutOffTime, src.fridayCutOffTime, src.saturdayCutOffTime, src.sundayCutOffTime, 
					src.mondayCanOrder, src.tuesdayCanOrder, src.wednesdayCanOrder, src.thursdayCanOrder, src.fridayCanOrder, src.saturdayCanOrder, src.sundayCanOrder, 
					src.timeHorizonWeeks,
					src.createdDate, src.changedDate, 
					src.system$changedby,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_cal_defaultwarehousesuppliercalender_aud;
go 

create trigger mend.trg_cal_defaultwarehousesuppliercalender_aud on mend.cal_defaultwarehousesuppliercalender_aud
for update, delete
as
begin
	set nocount on

	insert mend.cal_defaultwarehousesuppliercalender_aud_hist (id,
		mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
		mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
		mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
		timeHorizonWeeks,
		createdDate, changedDate, 
		system$changedby,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.mondayCanSupply, d.tuesdayCanSupply, d.wednesdayCanSupply, d.thursdayCanSupply, d.fridayCanSupply, d.saturdayCanSupply, d.sundayCanSupply, 
			d.mondayCutOffTime, d.tuesdayCutOffTime, d.wednesdayCutOffTime, d.thursdayCutOffTime, d.fridayCutOffTime, d.saturdayCutOffTime, d.sundayCutOffTime, 
			d.mondayCanOrder, d.tuesdayCanOrder, d.wednesdayCanOrder, d.thursdayCanOrder, d.fridayCanOrder, d.saturdayCanOrder, d.sundayCanOrder, 
			d.timeHorizonWeeks,
			d.createdDate, d.changedDate, 
			d.system$changedby,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- calendar$defaultwarehousesuppliercalender_warehouse -------------------

drop trigger mend.trg_cal_defaultwarehousesuppliercalender_warehouse;
go 

create trigger mend.trg_cal_defaultwarehousesuppliercalender_warehouse on mend.cal_defaultwarehousesuppliercalender_warehouse
after insert
as
begin
	set nocount on

	merge into mend.cal_defaultwarehousesuppliercalender_warehouse_aud with (tablock) as trg
	using inserted src
		on (trg.defaultwarehousesuppliercalenderid = src.defaultwarehousesuppliercalenderid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (defaultwarehousesuppliercalenderid, warehouseid, 
				idETLBatchRun)

				values (src.defaultwarehousesuppliercalenderid, src.warehouseid,
					src.idETLBatchRun);
end; 
go