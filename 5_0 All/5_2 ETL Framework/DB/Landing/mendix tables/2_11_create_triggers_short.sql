
------------------------------- Triggers ----------------------------------

use Landing
go

------------------- purchaseorders$shortageheader -------------------

drop trigger mend.trg_short_shortageheader;
go 

create trigger mend.trg_short_shortageheader on mend.short_shortageheader
after insert
as
begin
	set nocount on

	merge into mend.short_shortageheader_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.wholesale, ' ')
		intersect
		select 
			isnull(src.wholesale, ' '))
			
		then
			update set
				trg.wholesale = src.wholesale, trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id, wholesale, idETLBatchRun)
				values (src.id, src.wholesale, src.idETLBatchRun);
end; 
go


drop trigger mend.trg_short_shortageheader_aud;
go 

create trigger mend.trg_short_shortageheader_aud on mend.short_shortageheader_aud
for update, delete
as
begin
	set nocount on

	insert mend.short_shortageheader_aud_hist (id, wholesale,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id, d.wholesale,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------ purchaseorders$shortageheader_supplier ------------

drop trigger mend.trg_short_shortageheader_supplier;
go 

create trigger mend.trg_short_shortageheader_supplier on mend.short_shortageheader_supplier
after insert
as
begin
	set nocount on

	merge into mend.short_shortageheader_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.shortageheaderid = src.shortageheaderid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (shortageheaderid, supplierid, 
				idETLBatchRun)

				values (src.shortageheaderid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------ purchaseorders$shortageheader_sourcewarehouse ------------

drop trigger mend.trg_short_shortageheader_sourcewarehouse;
go 

create trigger mend.trg_short_shortageheader_sourcewarehouse on mend.short_shortageheader_sourcewarehouse
after insert
as
begin
	set nocount on

	merge into mend.short_shortageheader_sourcewarehouse_aud with (tablock) as trg
	using inserted src
		on (trg.shortageheaderid = src.shortageheaderid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (shortageheaderid, warehouseid, 
				idETLBatchRun)

				values (src.shortageheaderid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------ purchaseorders$shortageheader_destinationwarehouse ------------

drop trigger mend.trg_short_shortageheader_destinationwarehouse;
go 

create trigger mend.trg_short_shortageheader_destinationwarehouse on mend.short_shortageheader_destinationwarehouse
after insert
as
begin
	set nocount on

	merge into mend.short_shortageheader_destinationwarehouse_aud with (tablock) as trg
	using inserted src
		on (trg.shortageheaderid = src.shortageheaderid and trg.warehouseid = src.warehouseid)
	when not matched 
		then
			insert (shortageheaderid, warehouseid, 
				idETLBatchRun)

				values (src.shortageheaderid, src.warehouseid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortageheader_packsize -------------------

drop trigger mend.trg_short_shortageheader_packsize;
go 

create trigger mend.trg_short_shortageheader_packsize on mend.short_shortageheader_packsize
after insert
as
begin
	set nocount on

	merge into mend.short_shortageheader_packsize_aud with (tablock) as trg
	using inserted src
		on (trg.shortageheaderid = src.shortageheaderid and trg.packsizeid = src.packsizeid)
	when not matched 
		then
			insert (shortageheaderid, packsizeid, 
				idETLBatchRun)

				values (src.shortageheaderid, src.packsizeid,
					src.idETLBatchRun);
end; 
go







------------------- purchaseorders$shortage -------------------

drop trigger mend.trg_short_shortage;
go 

create trigger mend.trg_short_shortage on mend.short_shortage
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.shortageid, 0), isnull(trg.purchaseordernumber, ' '), isnull(trg.ordernumbers, ' '),
			isnull(trg.wholesale, ' '),
			isnull(trg.status, ' '),
			isnull(trg.unitquantity, 0), isnull(trg.packquantity, 0),
			isnull(trg.requireddate, ' '),
			isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' ')
		intersect
		select 
			isnull(src.shortageid, 0), isnull(src.purchaseordernumber, ' '), isnull(src.ordernumbers, ' '),
			isnull(src.wholesale, ' '),
			isnull(src.status, ' '),
			isnull(src.unitquantity, 0), isnull(src.packquantity, 0),
			isnull(src.requireddate, ' '),
			isnull(src.createdDate, ' '), isnull(src.changedDate, ' '))
			
		then
			update set
				trg.shortageid = src.shortageid, trg.purchaseordernumber = src.purchaseordernumber, trg.ordernumbers = src.ordernumbers, 
				trg.wholesale = src.wholesale,
				trg.status = src.status,
				trg.unitquantity = src.unitquantity, trg.packquantity = src.packquantity,
				trg.requireddate = src.requireddate,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				shortageid, purchaseordernumber, ordernumbers,
				wholesale, 
				status, 
				unitquantity, packquantity, 
				requireddate, 
				createdDate, changedDate,
				idETLBatchRun)

				values (src.id,
					src.shortageid, src.purchaseordernumber, src.ordernumbers,
					src.wholesale, 
					src.status, 
					src.unitquantity, src.packquantity, 
					src.requireddate, 
					src.createdDate, src.changedDate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_short_shortage_aud;
go 

create trigger mend.trg_short_shortage_aud on mend.short_shortage_aud
for update, delete
as
begin
	set nocount on

	insert mend.short_shortage_aud_hist (id,
		shortageid, purchaseordernumber, ordernumbers,
		wholesale, 
		status, 
		unitquantity, packquantity, 
		requireddate, 
		createdDate, changedDate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.shortageid, d.purchaseordernumber, d.ordernumbers,
			d.wholesale, 
			d.status, 
			d.unitquantity, d.packquantity, 
			d.requireddate, 
			d.createdDate, d.changedDate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$shortage_shortageheader -------------------

drop trigger mend.trg_short_shortage_shortageheader;
go 

create trigger mend.trg_short_shortage_shortageheader on mend.short_shortage_shortageheader
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_shortageheader_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.shortageheaderid = src.shortageheaderid)
	when not matched 
		then
			insert (shortageid, shortageheaderid, 
				idETLBatchRun)

				values (src.shortageid, src.shortageheaderid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_purchaseorderline -------------------

drop trigger mend.trg_short_shortage_purchaseorderline;
go 

create trigger mend.trg_short_shortage_purchaseorderline on mend.short_shortage_purchaseorderline
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_purchaseorderline_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.purchaseorderlineid = src.purchaseorderlineid)
	when not matched 
		then
			insert (shortageid, purchaseorderlineid, 
				idETLBatchRun)

				values (src.shortageid, src.purchaseorderlineid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_supplier -------------------

drop trigger mend.trg_short_shortage_supplier;
go 

create trigger mend.trg_short_shortage_supplier on mend.short_shortage_supplier
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_supplier_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.supplierid = src.supplierid)
	when not matched 
		then
			insert (shortageid, supplierid, 
				idETLBatchRun)

				values (src.shortageid, src.supplierid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_product -------------------

drop trigger mend.trg_short_shortage_product;
go 

create trigger mend.trg_short_shortage_product on mend.short_shortage_product
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_product_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.productid = src.productid)
	when not matched 
		then
			insert (shortageid, productid, 
				idETLBatchRun)

				values (src.shortageid, src.productid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_standardprice -------------------

drop trigger mend.trg_short_shortage_standardprice;
go 

create trigger mend.trg_short_shortage_standardprice on mend.short_shortage_standardprice
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_standardprice_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.standardpriceid = src.standardpriceid)
	when not matched 
		then
			insert (shortageid, standardpriceid, 
				idETLBatchRun)

				values (src.shortageid, src.standardpriceid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_stockitem -------------------

drop trigger mend.trg_short_shortage_stockitem;
go 

create trigger mend.trg_short_shortage_stockitem on mend.short_shortage_stockitem
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_stockitem_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.stockitemid = src.stockitemid)
	when not matched 
		then
			insert (shortageid, stockitemid, 
				idETLBatchRun)

				values (src.shortageid, src.stockitemid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$shortage_warehousestockitem -------------------

drop trigger mend.trg_short_shortage_warehousestockitem;
go 

create trigger mend.trg_short_shortage_warehousestockitem on mend.short_shortage_warehousestockitem
after insert
as
begin
	set nocount on

	merge into mend.short_shortage_warehousestockitem_aud with (tablock) as trg
	using inserted src
		on (trg.shortageid = src.shortageid and trg.warehousestockitemid = src.warehousestockitemid)
	when not matched 
		then
			insert (shortageid, warehousestockitemid, 
				idETLBatchRun)

				values (src.shortageid, src.warehousestockitemid,
					src.idETLBatchRun);
end; 
go








------------------- purchaseorders$orderlineshortage -------------------

drop trigger mend.trg_short_orderlineshortage;
go 

create trigger mend.trg_short_orderlineshortage on mend.short_orderlineshortage
after insert
as
begin
	set nocount on

	merge into mend.short_orderlineshortage_aud with (tablock) as trg
	using inserted src
		on (trg.id = src.id)
	when matched and not exists
		(select 
			isnull(trg.orderlineshortageid, 0), 
			isnull(trg.orderlineshortagetype, ' '), 
			isnull(trg.unitquantity, 0), isnull(trg.packquantity, 0),
			isnull(trg.shortageprogress, ' '), isnull(trg.adviseddate, ' '),
			isnull(trg.createdDate, ' '), isnull(trg.changedDate, ' ')
		intersect
		select 
			isnull(src.orderlineshortageid, 0), 
			isnull(src.orderlineshortagetype, ' '), 
			isnull(src.unitquantity, 0), isnull(src.packquantity, 0),
			isnull(src.shortageprogress, ' '), isnull(src.adviseddate, ' '),
			isnull(src.createdDate, ' '), isnull(src.changedDate, ' '))
			
		then
			update set
				trg.orderlineshortageid = src.orderlineshortageid, 
				trg.orderlineshortagetype = src.orderlineshortagetype, 
				trg.unitquantity = src.unitquantity, trg.packquantity = src.packquantity,
				trg.shortageprogress = src.shortageprogress, trg.adviseddate = src.adviseddate,
				trg.createdDate = src.createdDate, trg.changedDate = src.changedDate, 
				trg.upd_ts = getutcdate()

	when not matched 
		then
			insert (id,
				orderlineshortageid,
				orderlineshortagetype,
				unitquantity, packquantity,
				shortageprogress, adviseddate, 
				createdDate, changedDate,
				idETLBatchRun)

				values (src.id,
					src.orderlineshortageid,
					src.orderlineshortagetype,
					src.unitquantity, src.packquantity,
					src.shortageprogress, src.adviseddate, 
					src.createdDate, src.changedDate,
					src.idETLBatchRun);
end; 
go


drop trigger mend.trg_short_orderlineshortage_aud;
go 

create trigger mend.trg_short_orderlineshortage_aud on mend.short_orderlineshortage_aud
for update, delete
as
begin
	set nocount on

	insert mend.short_orderlineshortage_aud_hist (id,
		orderlineshortageid,
		orderlineshortagetype,
		unitquantity, packquantity,
		shortageprogress, adviseddate, 
		createdDate, changedDate,
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
		select d.id,
			d.orderlineshortageid,
			d.orderlineshortagetype,
			d.unitquantity, d.packquantity,
			d.shortageprogress, d.adviseddate, 
			d.createdDate, d.changedDate,
			d.idETLBatchRun,
			case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
			deleted d
		left join 
			inserted i on d.id = i.id;
end;
go

------------------- purchaseorders$orderlineshortage_shortage -------------------

drop trigger mend.trg_short_orderlineshortage_shortage;
go 

create trigger mend.trg_short_orderlineshortage_shortage on mend.short_orderlineshortage_shortage
after insert
as
begin
	set nocount on

	merge into mend.short_orderlineshortage_shortage_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineshortageid = src.orderlineshortageid and trg.shortageid = src.shortageid)
	when not matched 
		then
			insert (orderlineshortageid, shortageid, 
				idETLBatchRun)

				values (src.orderlineshortageid, src.shortageid,
					src.idETLBatchRun);
end; 
go

------------------- purchaseorders$orderlineshortage_orderline -------------------

drop trigger mend.trg_short_orderlineshortage_orderline;
go 

create trigger mend.trg_short_orderlineshortage_orderline on mend.short_orderlineshortage_orderline
after insert
as
begin
	set nocount on

	merge into mend.short_orderlineshortage_orderline_aud with (tablock) as trg
	using inserted src
		on (trg.orderlineshortageid = src.orderlineshortageid and trg.orderlineid = src.orderlineid)
	when not matched 
		then
			insert (orderlineshortageid, orderlineid, 
				idETLBatchRun)

				values (src.orderlineshortageid, src.orderlineid,
					src.idETLBatchRun);
end; 
go