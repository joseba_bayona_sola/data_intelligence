use Landing
go 

----------------------- comp_company ----------------------------

select * 
from mend.comp_company_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- comp_magwebstore ----------------------------

select * 
from mend.comp_magwebstore_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- comp_country ----------------------------

select * 
from mend.comp_country_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- comp_currency ----------------------------

select * 
from mend.comp_currency_aud_v
where num_records > 1
order by id, idETLBatchRun

----------------------- comp_spotrate ----------------------------

select * 
from mend.comp_spotrate_aud_v
where num_records > 1
order by id, idETLBatchRun

