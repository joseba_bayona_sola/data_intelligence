use Landing
go 

------------------- inventory$warehouse -------------------

drop view mend.wh_warehouse_aud_v
go

create view mend.wh_warehouse_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		code, name, shortName, warehouseType, snapWarehouse, 
		snapCode, scurriCode, 
		active, wms_active, useHoldingLabels, stockcheckignore,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			code, name, shortName, warehouseType, snapWarehouse, 
			snapCode, scurriCode, 
			active, wms_active, useHoldingLabels, stockcheckignore,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_warehouse_aud
		union
		select 'H' record_type, id, 
			code, name, shortName, warehouseType, snapWarehouse, 
			snapCode, scurriCode, 
			active, wms_active, useHoldingLabels, stockcheckignore,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_warehouse_aud_hist) t
go

------------ inventory$supplierroutine ------------

drop view mend.wh_supplierroutine_aud_v
go

create view mend.wh_supplierroutine_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, 
		supplierName,
		mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
		tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
		wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
		thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
		fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
		saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
		sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, 
			supplierName,
			mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
			tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
			wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
			thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
			fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
			saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
			sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_supplierroutine_aud
		union
		select 'H' record_type, id, 
			supplierName,
			mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
			tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
			wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
			thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
			fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
			saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
			sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_supplierroutine_aud_hist) t
go



------------------- inventory$preference -------------------

drop view mend.wh_preference_aud_v
go

create view mend.wh_preference_aud_v as
	select  record_type, count(*) over (partition by id) num_records, id, order_num,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, order_num,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mend.wh_preference_aud
		union
		select 'H' record_type, id, order_num,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mend.wh_preference_aud_hist) t
go