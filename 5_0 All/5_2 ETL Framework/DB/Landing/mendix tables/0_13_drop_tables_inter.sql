
------------------------ Drop tables -------------------------------

use Landing
go

------------ intersitetransfer$transferheader ------------

drop table mend.inter_transferheader;
go
drop table mend.inter_transferheader_aud;
go
drop table mend.inter_transferheader_aud_hist;
go

------------ purchaseorders$supplypo_transferheader ------------

drop table mend.inter_supplypo_transferheader;
go
drop table mend.inter_supplypo_transferheader_aud;
go

------------ purchaseorders$transferpo_transferheader ------------

drop table mend.inter_transferpo_transferheader;
go
drop table mend.inter_transferpo_transferheader_aud;
go


------------ orderprocessing$order_transferheader ------------

drop table mend.inter_order_transferheader;
go
drop table mend.inter_order_transferheader_aud;
go






------------ intersitetransfer$intransitbatchheader ------------

drop table mend.inter_intransitbatchheader;
go
drop table mend.inter_intransitbatchheader_aud;
go
drop table mend.inter_intransitbatchheader_aud_hist;
go

------------ intersitetransfer$intransitbatchheader_transferheader ------------

drop table mend.inter_intransitbatchheader_transferheader;
go
drop table mend.inter_intransitbatchheader_transferheader_aud;
go

------------ intersitetransfer$intransitbatchheader_customershipment ------------

drop table mend.inter_intransitbatchheader_customershipment;
go
drop table mend.inter_intransitbatchheader_customershipment_aud;
go






------------ intersitetransfer$intransitstockitembatch ------------

drop table mend.inter_intransitstockitembatch;
go
drop table mend.inter_intransitstockitembatch_aud;
go
drop table mend.inter_intransitstockitembatch_aud_hist;
go

------------ intersitetransfer$intransitstockitembatch_transferheader ------------

drop table mend.inter_intransitstockitembatch_transferheader;
go
drop table mend.inter_intransitstockitembatch_transferheader_aud;
go

------------ intersitetransfer$intransitstockitembatch_intransitbatchheader ------------

drop table mend.inter_intransitstockitembatch_intransitbatchheader;
go
drop table mend.inter_intransitstockitembatch_intransitbatchheader_aud;
go

------------ intersitetransfer$intransitstockitembatch_stockitem ------------

drop table mend.inter_intransitstockitembatch_stockitem;
go
drop table mend.inter_intransitstockitembatch_stockitem_aud;
go

------------ intersitetransfer$intransitstockitembatch_receipt ------------

drop table mend.inter_intransitstockitembatch_receipt;
go
drop table mend.inter_intransitstockitembatch_receipt_aud;
go
