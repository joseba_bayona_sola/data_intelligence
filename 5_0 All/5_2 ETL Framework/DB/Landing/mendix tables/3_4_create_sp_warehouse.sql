
------------------------------ SP -----------------------------------

use Landing
go

------------ inventory$warehouse ------------

drop procedure mend.srcmend_lnd_get_wh_warehouse
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehouse
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				code, name, shortName, warehouseType, snapWarehouse, 
				snapCode, scurriCode, 
				active, wms_active, useHoldingLabels, stockcheckignore, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					code, name, shortName, warehouseType, snapWarehouse, 
					snapCode, scurriCode, 
					active, wms_active, useHoldingLabels, stockcheckignore 
				from public."inventory$warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ inventory$warehousesupplier_warehouse ------------

drop procedure mend.srcmend_lnd_get_wh_warehousesupplier_warehouse
go

-- ===================================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================================
-- Description: Reads data from Mendix Table through Open Query - warehousesupplier_warehouse
-- ===================================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousesupplier_warehouse
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select inventory$warehousesupplierid warehousesupplierid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "inventory$warehousesupplierid", "inventory$warehouseid" 
			from public."inventory$warehousesupplier_warehouse"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ inventory$warehousesupplier_supplier ------------

drop procedure mend.srcmend_lnd_get_wh_warehousesupplier_supplier
go

-- ===================================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-06-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================================
-- Description: Reads data from Mendix Table through Open Query - warehousesupplier_supplier
-- ===================================================================================================

create procedure mend.srcmend_lnd_get_wh_warehousesupplier_supplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select inventory$warehousesupplierid warehousesupplierid, suppliers$supplierid supplierid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "inventory$warehousesupplierid", "suppliers$supplierid" 
			from public."inventory$warehousesupplier_supplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


















------------ inventory$supplierroutine ------------

drop procedure mend.srcmend_lnd_get_wh_supplierroutine
go

-- ==========================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_supplierroutine
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_supplierroutine
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id,
				supplierName,
				mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
				tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
				wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
				thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
				fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
				saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
				sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id,
					supplierName,
					mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
					tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
					wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
					thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
					fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
					saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
					sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder 
				from public."inventory$supplierroutine"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ inventory$supplierroutine_warehousesupplier ------------

drop procedure mend.srcmend_lnd_get_wh_supplierroutine_warehousesupplier
go

-- ===================================================================================================
-- Author: Isabel Irujo Cia
-- Date: 12-05-2017
-- Changed: 
	--	06-12-2017	Joseba Bayona Sola	Connection to Restore DB
-- ===================================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_supplierroutine_warehousesupplier
-- ===================================================================================================

create procedure mend.srcmend_lnd_get_wh_supplierroutine_warehousesupplier
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
		select inventory$supplierroutineid supplierRoutineID, inventory$warehousesupplierid warehouseSupplierID, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
		'from openquery(MENDIX_DB_RESTORE,''
			select "inventory$supplierroutineid", "inventory$warehousesupplierid" 
			from public."inventory$supplierroutine_warehousesupplier"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go




------------ inventorypreference ------------

drop procedure mend.srcmend_lnd_get_wh_preference
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_preference
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_preference
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, "order" order_num, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select id, "order"
				from public."inventory$preference"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go


------------ inventorywarehouse_preference ------------

drop procedure mend.srcmend_lnd_get_wh_warehouse_preference
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_warehouse_preference
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_warehouse_preference
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$preferenceid preferenceid, inventory$warehouseid warehouseid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$preferenceid", "inventory$warehouseid"
				from public."inventory$warehouse_preference"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go



------------ inventorycountry_preference ------------

drop procedure mend.srcmend_lnd_get_wh_country_preference
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 23-05-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Mendix Table through Open Query - wh_country_preference
-- ==========================================================================================

create procedure mend.srcmend_lnd_get_wh_country_preference
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select inventory$preferenceid preferenceid, countrymanagement$countryid countryid, ' + cast(@idETLBatchRun as varchar(20)) + ' ' +
			'from openquery(MENDIX_DB_RESTORE,''
				select "inventory$preferenceid", "countrymanagement$countryid"
				from public."inventory$country_preference"'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go
