
-- Looking for INTERESTING CUSTOMERS (Having orders in both HIST - MIGRATE and also in NEW DWH)
select o1.new_customer_id, 
	o1.num_orders, o2.num_orders, 
	o1.dist_website, o1.dist_source, o1.etl_name
from
		(select new_customer_id, count(*) num_orders, 
			count(distinct migra_website_name) dist_website, count(distinct etl_name) dist_source, max(etl_name) etl_name
		from Landing.migra.match_order_header
		group by new_customer_id
		having count(distinct migra_website_name) > 1) o1
	left join
		(select customer_id_bk, count(*) num_orders
		from Landing.aux.sales_dim_order_header_aud
		group by customer_id_bk) o2 on o1.new_customer_id = o2.customer_id_bk
where o2.customer_id_bk is not null and o1.etl_name = 'hist'
-- order by o1.dist_source desc, o1.dist_website desc, o1.num_orders desc
order by o1.dist_source desc, o1.etl_name, o1.num_orders desc

----------------------------------------

-- PROFILING CUSTOMER ORDERS
select top 1000 *
from Landing.migra.match_order_header
where new_customer_id in (51032, 663304)
order by new_customer_id, etl_name, migra_website_name


select *
from Landing.aux.sales_dim_order_header_aud
where customer_id_bk in (51032, 75171, 663304)
order by customer_id_bk, order_date

select *
from Warehouse.sales.dim_order_header_v
where customer_id in (51032, 75171, 663304)
order by customer_id, order_date

select *
from Warehouse.sales.fact_order_line_v
where customer_id in (51032, 75171, 663304)
order by customer_id, order_date, product_id_magento


select *
from Warehouse.act.fact_activity_sales_v
where customer_id in (51032, 75171, 663304)
order by customer_id, activity_date

select *
from Warehouse.act.fact_customer_signature_v
where customer_id in (51032, 75171, 663304)
order by customer_id


----------------------------------------

	---- INSERT: migra.sales_flat_order_hist
	delete from Landing.migra.sales_flat_order_hist

	insert into Landing.migra.sales_flat_order_hist(migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_grand_total, 
		customer_email,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total, sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal
				from Landing.aux.migra_hist_dw_hist_order_item_v
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name
		where moh.new_customer_id = 75171



	delete from Landing.migra.sales_flat_order_migrate

	-- INSERT: migra.sales_flat_order_migrate
	insert into migra.sales_flat_order_migrate (migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
		customer_email,
		shipping_description, channel, 
		tax_rate, order_currency, exchangeRate,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.base_shipping_amount, hoi.base_discount_amount, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			hoi.shipping_description, hoi.channel, 
			hoi.tax_rate, hoi.order_currency, hoi.exchangeRate,
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate
				from Landing.aux.migra_migrate_order_item_v
				where etl_name = 'migrate' 
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					base_shipping_amount, base_discount_amount, grand_total, 
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name
		where moh.new_customer_id in (75171, 663304)


---------------------------------------------------------------------

