use Landing
go 


drop view aux.migra_hist_dw_hist_order_item_v
go 

create view aux.migra_hist_dw_hist_order_item_v as

	select ho.migra_website_name, ho.etl_name,
		ho.order_id, row_number() over (partition by hoi.order_id order by hoi.product_id, hoi.sku) num_order_line,
		ho.source, 
		ho.store_id_hist, ho.store_id, 
		ho.customer_id_hist, ho.customer_id, ho.customer_email, 
		ho.created_at, ho.grand_total, 
		hoi.product_id_hist, hoi.product_id, hoi.sku, hoi.product_family_name, hoi.category,
		hoi.qty_ordered, hoi.original_qty_ordered, hoi.row_total, hoi.original_row_total,
		sum(hoi.row_total) over (partition by ho.order_id) sum_row_total, 
		abs(ho.grand_total - sum(hoi.row_total) over (partition by ho.order_id)) diff_oh_ol
	from
			(select 
				case 
					when (source = 'postoptics_db') then 'postoptics' 
					when (source in ('getlenses_db', 'getlenses_nl_')) then 'getlenses'
					when (source = 'masterlens_db') then 'masterlens'
					when (source = 'asda_db') then 'asda'
					when (source = 'vhi_db') then 'vh1'
				end migra_website_name, 'hist' etl_name, 
				ho.order_id, 
				ho.source, ho.store_id store_id_hist, 
				case when (s2.store_id is not null) then s2.store_id else s.store_id end store_id,
				ho.customer_id customer_id_hist, c.entity_id customer_id, c.email customer_email,
				ho.created_at, ho.grand_total		
			from 
					Landing.migra_hist.dw_hist_order ho
				left join
					Landing.aux.mag_gen_store_v s on ho.store_id = s.store_id -- ho.source <> 'masterlens_db' and 
				left join
					Landing.aux.mag_gen_store_v s2 on ho.source = 'masterlens_db' and s2.store_name like 'masterlens%' and s.tld = s2.tld 

				left join
					Landing.mag.customer_entity_flat_aud c on ho.customer_id = c.entity_id
			where source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'asda_db', 'vhi_db')) ho 
		left join
			(select hoi.order_id, 
				hoi.source, 
				hoi.product_id product_id_hist,  isnull(p_d.entity_id, hoi.product_id) product_id, hoi.sku, 
				p.product_family_name, p.category_bk category,
				case
					when (hoi.source = 'postoptics_db' and p.category_bk = 'Daily') then hoi.qty_ordered * 90  
					when (hoi.source = 'postoptics_db' and p.category_bk = 'Monthlies') then hoi.qty_ordered * 3 
					when (hoi.source = 'postoptics_db' and p.category_bk = 'Two Weeklies') then hoi.qty_ordered * 6 
					when (hoi.source like 'getlenses%' and p.category_bk = 'Daily') then hoi.qty_ordered * 30 
					when (hoi.source like 'getlenses%' and p.category_bk = 'Monthlies') then hoi.qty_ordered * 1 
					when (hoi.source like 'getlenses%' and p.category_bk = 'Two Weeklies') then hoi.qty_ordered * 2 
					else hoi.qty_ordered
				end qty_ordered, 
				hoi.qty_ordered original_qty_ordered, 
				case 
					when (hoi.source = 'postoptics_db' and p.category_bk in ('Daily', 'Monthlies', 'Two Weeklies')) then hoi.row_total * hoi.qty_ordered
					else hoi.row_total
				end row_total, 
				hoi.row_total original_row_total
			from 
					(select row_number() over(order by order_id) id, 
						order_id, source, product_id, sku, qty_ordered, row_total
					from Landing.migra_hist.dw_hist_order_item
					where source in ('postoptics_db', 'getlenses_db', 'getlenses_nl_', 'masterlens_db', 'vhi_db') 
						or (source in ('asda_db') and product_id <> 1136)
					union
					select row_number() over(order by order_id) id, 
						order_id, source, product_id, sku, qty_ordered, row_total
					from
						(select order_id, source, product_id, sku, qty_ordered, row_total, 
							min(qty_ordered) over (partition by order_id, sku) min_qty_ordered
						from Landing.migra_hist.dw_hist_order_item
						where source in ('asda_db') and product_id = 1136) t
					where qty_ordered = min_qty_ordered) hoi					
				left join
					(select p.entity_id, t.old_product_id, t.old_sku, p.sku
					from
						(select -1 old_product_id, 'GEN-DAILIES' old_sku, 'DUMMYDAILYLENS' sku
						union
						select -2 old_product_id, 'GEN-MONTHLY' old_sku, 'DUMMYMONTHLYLENS' sku
						union
						select -3 old_product_id, 'GEN-SOLUTION' old_sku, 'DUMMYSOLUTION' sku
						union
						select -3 old_product_id, 'GEN-SOL' old_sku, 'DUMMYSOLUTION' sku
						union
						select -3 old_product_id, 'GEN-OTHER' old_sku, 'DUMMYSOLUTION' sku
						union
						select -4 old_product_id, 'GEN-OTHER' old_sku, 'DUMMYSOLUTION' sku
						union
						select -4 old_product_id, '\N' old_sku, 'DUMMYSOLUTION' sku) t
					inner join
						Landing.mag.catalog_product_entity_flat_aud p on t.sku = p.sku) p_d on hoi.product_id = p_d.old_product_id and hoi.sku = p_d.old_sku
				left join
					Landing.aux.mag_prod_product_family_v p on isnull(p_d.entity_id, hoi.product_id) = p.product_id_bk) hoi on ho.order_id = hoi.order_id and ho.source = hoi.source
go

			


drop view aux.migra_migrate_order_item_v
go 

create view aux.migra_migrate_order_item_v as
	select od.migra_website_name, od.etl_name,
		od.order_id, odl.num_order_line,
		od.source, 
		od.store_id, 
		od.customer_id_hist, case when (od.customer_id = 0) then null else od.customer_id end customer_id, od.customer_email, 
		od.created_at, 
		od.migrated, od.orderStatusName, od.order_status_migrate,
		od.grand_total, od.base_shipping_amount, od.base_discount_amount, 
		od.shipping_description, od.channel,
		od.tax_rate, od.tax_free_amount, 
		od.order_currency, od.exchangeRate, 
		odl.product_id_hist, odl.product_id, odl.sku, odl.productName_hist, odl.product_family_name, odl.category,
		odl.eye, 
		odl.base_curve, odl.diameter, odl.power, odl.cylinder, odl.axis, odl.[add], odl.dominant, odl.color,
		odl.quantity_ordered qty_ordered, odl.row_total, odl.row_discount,
		sum(odl.row_total) over (partition by od.migra_website_name, od.order_id) sum_row_total, 
		abs(od.grand_total - od.base_shipping_amount + od.base_discount_amount - sum(odl.row_total) over (partition by od.migra_website_name, od.order_id)) diff_oh_ol
	from 
			(select 
				od.db migra_website_name, 'migrate' etl_name, 
				od.old_order_id order_id,
				od.db source,
				s.store_id,
				od.old_customer_id customer_id_hist, od.magento_customer_id customer_id, od.email customer_email, 
				od.created_at, 
				od.migrated, od.orderStatusName, os.description order_status_migrate,
				od.base_grand_total grand_total, od.base_shipping_amount, 
				case when (od.base_discount_amount < 0) then od.base_discount_amount * -1 else od.base_discount_amount end base_discount_amount, 
				od.shipping_description, od.channel,
				od.tax_rate, od.tax_free_amount, 
				od.order_currency, od.exchangeRate
			from 
					migra.migrate_orderdata_v od
				inner join
					aux.mag_gen_store_v s on 
						case 
							when (od.db = 'lh150324' and od.domainName is null) then 'lenshome.es' 
							when (od.db = 'VisioOptik') then 'visiooptik.fr' 
							when (od.db = 'lensway') then 'lensway.co.uk' 
							when (od.db = 'lenson') then 'lenson.co.uk' 
							when (od.db = 'lensonnl') then 'lenson.nl' 
							when (od.db = 'lenswaynl') and (od.website_name = 'lensway.nl') then 'lensway.nl' 
							when (od.db = 'lenswaynl') and (od.website_name = 'YourLenses') then 'yourlenses.nl' 
							when (od.domainName = 'visiondirect.co.uk') then 'visiondirect.co.uk (A)' 
							when (od.domainName = 'visiondirect.ie') then 'visiondirect.ie (A)' 
							else od.domainName end = s.store_name 
						and s.acquired = 'Y'
				inner join
					Landing.map.migra_order_status os on 
						case when (od.db in ('lensway', 'lenson', 'lensonnl', 'lenswaynl')) then 'D' else od.orderStatusName end = os.orderstatusname
			where 
				(db in ('vd150324', 'lb150324', 'lh150324', 'VisioOptik') and od.orderstatusname not in ('C', 'R', 'F')) or -- ) or 
				(db in ('lensway', 'lenson')) or 
				(db in ('lensonnl', 'lenswaynl'))
				) od
		left join
			(select 
				odl.old_order_id order_id, odl.old_item_id num_order_line, 
				odl.db source,
				case when (db in ('VisioOptik', 'lensway', 'lenson', 'lensonnl', 'lenswaynl')) then odl.product_id else odl.productID end product_id_hist, 
				isnull(p.product_id_bk, 2327) product_id, 
				null sku, 
				odl.productName productName_hist, isnull(p.product_family_name, 'Ad-hoc Product') product_family_name, isnull(p.category_bk, 'Other') category,
				case when (db in ('lensonnl')) then isnull(odl.quantity, 1) else odl.quantity end quantity_ordered, 
				case when (db = 'lh150324') then odl.row_total * odl.quantity else odl.row_total end row_total, row_discount,
				eye, 
				base_curve, diameter, power, cylinder, axis, [add], dominant, color
			from 
					migra.migrate_orderlinedata_v odl
				left join
					aux.mag_prod_product_family_v p on 
						case when (db in ('VisioOptik', 'lensway', 'lenson', 'lensonnl', 'lenswaynl')) then odl.product_id_def else odl.productID end = p.product_id_bk
			where db in ('vd150324', 'lb150324', 'lh150324', 'VisioOptik', 'lensway', 'lenson', 'lensonnl', 'lenswaynl')) odl on od.order_id = odl.order_id and od.source = odl.source
go

