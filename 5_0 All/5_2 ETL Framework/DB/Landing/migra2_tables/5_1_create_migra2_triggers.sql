
use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- sales_flat_order_hist ----------------------------

	drop trigger migra.trg_sales_flat_order_hist;
	go 

	create trigger migra.trg_sales_flat_order_hist on migra.sales_flat_order_hist
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_hist_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.increment_id, ''),  isnull(trg.store_id, 0), isnull(trg.customer_id, 0), isnull(trg.created_at, ''),  isnull(trg.updated_at, ''),  
				isnull(trg.status, ''),  
				isnull(trg.total_qty_ordered, 0), 
				isnull(trg.base_subtotal, 0), isnull(trg.base_grand_total, 0), 
				isnull(trg.customer_email, '') 
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.increment_id, ''),  isnull(src.store_id, 0), isnull(src.customer_id, 0), isnull(src.created_at, ''),  isnull(src.updated_at, ''),  
				isnull(src.status, ''),  
				isnull(src.total_qty_ordered, 0), 
				isnull(src.base_subtotal, 0), isnull(src.base_grand_total, 0), 
				isnull(src.customer_email, ''))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.increment_id = src.increment_id, trg.store_id = src.store_id, trg.customer_id = src.customer_id, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.status = src.status, 
					trg.total_qty_ordered = src.total_qty_ordered, 
					trg.base_subtotal = src.base_subtotal, trg.base_grand_total = src.base_grand_total, 
					trg.customer_email = src.customer_email, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (
					migra_website_name, etl_name, 
					entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					status, 
					total_qty_ordered, 
					base_subtotal, base_grand_total, 
					customer_email,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 
						src.entity_id, 	src.increment_id, src.store_id, src.customer_id, src.created_at, src.updated_at, 
						src.status, 
						src.total_qty_ordered, 
						src.base_subtotal, src.base_grand_total, 
						src.customer_email,
						src.idETLBatchRun);
	end; 
	go



------------------------- sales_flat_order_item_hist ----------------------------

	drop trigger migra.trg_sales_flat_order_item_hist;
	go 

	create trigger migra.trg_sales_flat_order_item_hist on migra.sales_flat_order_item_hist
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_item_hist_aud with (tablock) as trg
		using inserted src
			on (trg.item_id = src.item_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.order_id, 0),  isnull(trg.store_id, 0), isnull(trg.created_at, ''),  isnull(trg.updated_at, ''),  
				isnull(trg.product_id, 0),  isnull(trg.sku, ''),  
				isnull(trg.qty_ordered, 0),  isnull(trg.base_row_total, 0)
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.order_id, 0),  isnull(src.store_id, 0), isnull(src.created_at, ''),  isnull(src.updated_at, ''),  
				isnull(src.product_id, 0),  isnull(src.sku, ''),  
				isnull(src.qty_ordered, 0),  isnull(src.base_row_total, 0))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.order_id = src.order_id, trg.store_id = src.store_id, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.product_id = src.product_id, trg.sku = src.sku, 
					trg.qty_ordered = src.qty_ordered, trg.base_row_total = src.base_row_total, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (migra_website_name, etl_name, 	
					item_id, order_id, store_id, created_at, updated_at,
					product_id, sku,
					qty_ordered, base_row_total,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 	
						src.item_id, src.order_id, src.store_id, src.created_at, src.updated_at,
						src.product_id, src.sku,
						src.qty_ordered, src.base_row_total,
						src.idETLBatchRun);
	end; 
	go



------------------------- sales_flat_order_address_hist ----------------------------

	drop trigger migra.trg_sales_flat_order_address_hist;
	go 

	create trigger migra.trg_sales_flat_order_address_hist on migra.sales_flat_order_address_hist
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_address_hist_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.parent_id, 0),  isnull(trg.customer_id, 0), 
				isnull(trg.postcode, ''),  isnull(trg.country_id, '')
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.parent_id, 0),  isnull(src.customer_id, 0), 
				isnull(src.postcode, ''),  isnull(src.country_id, ''))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.parent_id = src.parent_id, trg.customer_id = src.customer_id,
					trg.postcode = src.postcode, trg.country_id = src.country_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (migra_website_name, etl_name, 	
					entity_id, parent_id, customer_id, 
					postcode, country_id,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 	
						src.entity_id, src.parent_id, src.customer_id, 
						src.postcode, src.country_id,
						src.idETLBatchRun);
	end; 
	go


---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------


------------------------- sales_flat_order_migrate ----------------------------

	drop trigger migra.trg_sales_flat_order_migrate;
	go 

	create trigger migra.trg_sales_flat_order_migrate on migra.sales_flat_order_migrate
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_migrate_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.increment_id, ''),  isnull(trg.store_id, 0), isnull(trg.customer_id, 0), isnull(trg.created_at, ''),  isnull(trg.updated_at, ''),  
				isnull(trg.status, ''),  
				isnull(trg.total_qty_ordered, 0), 
				isnull(trg.base_subtotal, 0), isnull(trg.base_shipping_amount, 0), isnull(trg.base_discount_amount, 0), isnull(trg.base_grand_total, 0), 
				isnull(trg.customer_email, ''), 
				isnull(trg.shipping_description, ''), isnull(trg.channel, ''), 
				isnull(trg.tax_rate, 0), isnull(trg.order_currency, ''), isnull(trg.exchangeRate, 0)
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.increment_id, ''),  isnull(src.store_id, 0), isnull(src.customer_id, 0), isnull(src.created_at, ''),  isnull(src.updated_at, ''),  
				isnull(src.status, ''),  
				isnull(src.total_qty_ordered, 0), 
				isnull(src.base_subtotal, 0), isnull(src.base_shipping_amount, 0), isnull(src.base_discount_amount, 0), isnull(src.base_grand_total, 0), 
				isnull(src.customer_email, ''),
				isnull(src.shipping_description, ''), isnull(src.channel, ''), 
				isnull(src.tax_rate, 0), isnull(src.order_currency, ''), isnull(src.exchangeRate, 0))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.increment_id = src.increment_id, trg.store_id = src.store_id, trg.customer_id = src.customer_id, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.status = src.status, 
					trg.total_qty_ordered = src.total_qty_ordered, 
					trg.base_subtotal = src.base_subtotal, trg.base_shipping_amount = src.base_shipping_amount, trg.base_discount_amount = src.base_discount_amount, trg.base_grand_total = src.base_grand_total, 
					trg.customer_email = src.customer_email, 
					trg.shipping_description = src.shipping_description, trg.channel = src.channel, 
					trg.tax_rate = src.tax_rate, trg.order_currency = src.order_currency, trg.exchangeRate = src.exchangeRate, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (
					migra_website_name, etl_name, 
					entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					status, 
					total_qty_ordered, 
					base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
					customer_email,
					shipping_description, channel, 
					tax_rate, order_currency, exchangeRate,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 
						src.entity_id, 	src.increment_id, src.store_id, src.customer_id, src.created_at, src.updated_at, 
						src.status, 
						src.total_qty_ordered, 
						src.base_subtotal, src.base_shipping_amount, src.base_discount_amount, src.base_grand_total, 
						src.customer_email,
						src.shipping_description, src.channel, 
						src.tax_rate, src.order_currency, src.exchangeRate,
						src.idETLBatchRun);
	end; 
	go



------------------------- sales_flat_order_item_migrate ----------------------------

	drop trigger migra.trg_sales_flat_order_item_migrate;
	go 

	create trigger migra.trg_sales_flat_order_item_migrate on migra.sales_flat_order_item_migrate
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_item_migrate_aud with (tablock) as trg
		using inserted src
			on (trg.item_id = src.item_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.order_id, 0),  isnull(trg.store_id, 0), isnull(trg.created_at, ''),  isnull(trg.updated_at, ''),  
				isnull(trg.product_id, 0),  isnull(trg.sku, ''),  
				isnull(trg.qty_ordered, 0),  isnull(trg.base_row_total, 0), isnull(trg.base_discount_amount, 0),  
				isnull(trg.eye, ''),  
				isnull(trg.base_curve, ''),  isnull(trg.diameter, ''),  isnull(trg.power, ''),  
				isnull(trg.cylinder, ''),  isnull(trg.axis, ''),  isnull(trg.[add], ''),  isnull(trg.dominant, ''),  
				isnull(trg.color, '')  
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.order_id, 0),  isnull(src.store_id, 0), isnull(src.created_at, ''),  isnull(src.updated_at, ''),  
				isnull(src.product_id, 0),  isnull(src.sku, ''),  
				isnull(src.qty_ordered, 0),  isnull(src.base_row_total, 0), isnull(src.base_discount_amount, 0), 
				isnull(src.eye, ''),  
				isnull(src.base_curve, ''),  isnull(src.diameter, ''),  isnull(src.power, ''),  
				isnull(src.cylinder, ''),  isnull(src.axis, ''),  isnull(src.[add], ''),  isnull(src.dominant, ''),  
				isnull(src.color, ''))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.order_id = src.order_id, trg.store_id = src.store_id, trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.product_id = src.product_id, trg.sku = src.sku, 
					trg.qty_ordered = src.qty_ordered, trg.base_row_total = src.base_row_total, trg.base_discount_amount = src.base_discount_amount, 
					trg.eye = src.eye, 
					trg.base_curve = src.base_curve, trg.diameter = src.diameter, trg.power = src.power, 
					trg.cylinder = src.cylinder, trg.axis = src.axis, trg.[add] = src.[add], trg.dominant = src.dominant, 
					trg.color = src.color, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (migra_website_name, etl_name, 	
					item_id, order_id, store_id, created_at, updated_at,
					product_id, sku,
					qty_ordered, base_row_total, base_discount_amount,
					eye, 
					base_curve, diameter, power, cylinder, axis, [add], dominant, color,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 	
						src.item_id, src.order_id, src.store_id, src.created_at, src.updated_at,
						src.product_id, src.sku,
						src.qty_ordered, src.base_row_total, src.base_discount_amount,
						src.eye, 
						src.base_curve, src.diameter, src.power, src.cylinder, src.axis, src.[add], src.dominant, src.color,
						src.idETLBatchRun);
	end; 
	go



	------------------------- sales_flat_order_address_migrate ----------------------------

	drop trigger migra.trg_sales_flat_order_address_migrate;
	go 

	create trigger migra.trg_sales_flat_order_address_migrate on migra.sales_flat_order_address_migrate
	after insert
	as
	begin
		set nocount on

		merge into migra.sales_flat_order_address_migrate_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.migra_website_name, ''),  isnull(trg.etl_name, ''),  
				isnull(trg.parent_id, 0),  isnull(trg.customer_id, 0), 
				isnull(trg.postcode, ''),  isnull(trg.country_id, '')
			intersect
			select isnull(src.migra_website_name, ''),  isnull(src.etl_name, ''),  
				isnull(src.parent_id, 0),  isnull(src.customer_id, 0), 
				isnull(src.postcode, ''),  isnull(src.country_id, ''))
		
			then
				update set
					trg.migra_website_name = src.migra_website_name, trg.etl_name = src.etl_name, 
					trg.parent_id = src.parent_id, trg.customer_id = src.customer_id,
					trg.postcode = src.postcode, trg.country_id = src.country_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (migra_website_name, etl_name, 	
					entity_id, parent_id, customer_id, 
					postcode, country_id,
					idETLBatchRun)

					values (src.migra_website_name, src.etl_name, 	
						src.entity_id, src.parent_id, src.customer_id, 
						src.postcode, src.country_id,
						src.idETLBatchRun);
	end; 
	go



