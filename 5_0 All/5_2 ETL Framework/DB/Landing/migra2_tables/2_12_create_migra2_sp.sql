
	-- INSERT: migra.match_customer
	insert into Landing.migra.match_customer (migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun)

		select t.migra_website_name, t.etl_name, 
			t.old_customer_id, t.new_magento_id, 
			t.proforma_customer_id, 
			t.new_customer_id, t.customer_email,
			@idETLBatchRun idETLBatchRun 
		from 
				(select distinct migra_website_name, etl_name,
					moi.customer_id_hist old_customer_id, moi.customer_id new_magento_id, 
					oh.customer_id proforma_customer_id, 
					oh.customer_id new_customer_id, c.email customer_email
				from
						(select migra_website_name, etl_name,
							order_id, 600000000 + order_id proforma_order_id, num_order_line,
							source, 
							store_id, 
							customer_id_hist, customer_id, customer_email, 
							created_at 
						from Landing.aux.migra_migrate_order_item_v) moi
					inner join
						Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
					inner join
						Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
				where migra_website_name = 'VisioOptik') t
			left join
				Landing.migra.match_customer mc on t.migra_website_name = mc.migra_website_name and t.new_customer_id = mc.new_customer_id and t.old_customer_id = mc.old_customer_id
		where mc.new_customer_id is null
		order by new_magento_id, migra_website_name

	-- INSERT: migra.match_order_header
	insert into Landing.migra.match_order_header (migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun)

	select oh.migra_website_name, oh.etl_name, 
		oh.order_id old_order_id, od.magento_order_id, 
		oh.proforma_order_id, oh.order_no proforma_order_no, 
		(prev_new_order_id + rank() over (order by oh.created_at, oh.order_id)) * -1 new_order_id, oh.order_no new_order_no, 
		oh.customer_id_hist old_customer_id, oh.customer_id proforma_customer_id, oh.customer_id new_customer_id, oh.customer_email,
		@idETLBatchRun idETLBatchRun
	from
			(select distinct 
				migra_website_name, etl_name,
				moi.order_id, moi.created_at,
				moi.proforma_order_id, oh.order_no, 
				moi.customer_id_hist, oh.customer_id, moi.customer_email		
			from
					(select migra_website_name, etl_name,
						order_id, 600000000 + order_id proforma_order_id, num_order_line,
						source, 
						store_id, 
						customer_id_hist, customer_id, isnull(customer_email, '') customer_email,
						created_at 
					from Landing.aux.migra_migrate_order_item_v
					where sum_row_total is not null) moi
				inner join
					Landing.migra_pr.order_headers oh on moi.proforma_order_id = oh.order_id
				inner join -- left: orders from customers not in magento
					Landing.mag.customer_entity_flat_aud c on oh.customer_id = c.entity_id
			where migra_website_name = 'VisioOptik') oh
		inner join
			Landing.migra.migrate_orderdata_v od on oh.order_id = od.old_order_id
		left join
			Landing.migra.match_order_header mmoh on oh.migra_website_name = mmoh.migra_website_name and oh.order_id = mmoh.old_order_id,			
			(select isnull(min(new_order_id), 0)*-1 prev_new_order_id from Landing.migra.match_order_header) moh
	where mmoh.old_order_id is null
	order by new_order_id

	-- INSERT: migra.match_order_line
	insert into Landing.migra.match_order_line (migra_website_name, etl_name, 
		old_item_id, num_order_line, 
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun)

		select moi.migra_website_name, moi.etl_name, 
			moi.num_order_line old_item_id, moi.num_order_line,
			null proforma_line_id, 
			(prev_new_order_line_id + row_number() over (order by ho.new_order_id)) * -1 new_order_line_id, ho.new_order_id, 
			moi.product_id_hist old_product_id, null proforma_product_id, moi.product_id,
			@idETLBatchRun idETLBatchRun
		from 
				(select *
				from Landing.aux.migra_migrate_order_item_v
				where migra_website_name = 'VisioOptik')  moi
			inner join
				Landing.migra.match_order_header ho on moi.order_id = ho.old_order_id and moi.migra_website_name = ho.migra_website_name
			left join	
				Landing.migra.match_order_line molh on moi.migra_website_name = molh.migra_website_name and moi.num_order_line = molh.old_item_id and moi.num_order_line = molh.num_order_line, 
			(select isnull(min(new_order_line_id), 0)*-1 prev_new_order_line_id from Landing.migra.match_order_line) mol
		where molh.old_item_id is null