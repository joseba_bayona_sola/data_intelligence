use Landing
go 

------------------------------------------------------------------------
----------------------- migra.match_customer ----------------------------
------------------------------------------------------------------------

	select top 1000 migra_website_name, etl_name, 
		old_customer_id, new_magento_id, 
		proforma_customer_id, 
		new_customer_id, customer_email,
		idETLBatchRun, ins_ts
	from Landing.migra.match_customer
	-- where new_customer_id in (60885, 70172, 1962592)
	where old_customer_id in (903000784, 900764, 901187217)
	order by new_customer_id, migra_website_name, old_customer_id

	select idETLBatchRun, count(*) num_rows
	from Landing.migra.match_customer
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct new_customer_id) num_dist_rows
	from Landing.migra.match_customer

	select migra_website_name, etl_name, count(*) num_rows, count(distinct new_customer_id) num_dist_rows
	from Landing.migra.match_customer
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	-- INSIDE SAME MIG STORE
	select migra_website_name, etl_name, count(*) num_rows, 
		count(distinct old_customer_id) num_dist_rows_old, count(distinct new_customer_id) num_dist_rows_new
	from Landing.migra.match_customer
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select top 1000 migra_website_name, etl_name, new_customer_id, count(*) num_rows, count(distinct customer_email) num_dist_rows_email
	from Landing.migra.match_customer
	group by migra_website_name, etl_name, new_customer_id
	order by num_dist_rows_email desc, etl_name, migra_website_name, new_customer_id

	select top 1000 migra_website_name, etl_name, 
		old_customer_id, new_customer_id, customer_email, 
		count(*) over (partition by old_customer_id) cnt_n_new, 
		count(*) over (partition by new_customer_id) cnt_n_old, 
		count(*) over (partition by customer_email) cnt_n_email
	from Landing.migra.match_customer
	where migra_website_name = 'lb150324'
	-- order by cnt_n_new desc, old_customer_id, new_customer_id
	-- order by cnt_n_old desc, new_customer_id, old_customer_id
	order by cnt_n_email desc, new_customer_id, old_customer_id




	-- INSIDE DIFFERENT MIG STORE
	select new_customer_id, count(*) num_rows, count(distinct migra_website_name) num_dist_stores, count(distinct old_customer_id) num_dist_old
	from Landing.migra.match_customer
	group by new_customer_id
	having count(*) > 1
	order by count(*) desc, new_customer_id

	select top 1000 new_customer_id, count(*) num_rows, count(distinct customer_email) num_dist_rows_email
	from Landing.migra.match_customer
	group by new_customer_id
	order by num_dist_rows_email desc, new_customer_id


	select top 1000 migra_website_name, etl_name, 
		old_customer_id, new_customer_id, customer_email, 
		count(*) over (partition by new_customer_id) cnt_n_old, 
		count(*) over (partition by customer_email) cnt_n_email
	from Landing.migra.match_customer
	order by cnt_n_old desc, new_customer_id, old_customer_id
	-- order by cnt_n_email desc, new_customer_id, old_customer_id



	select migra_website_name, etl_name, new_customer_id, count(*) num_rows, count(distinct customer_email) num_dist_rows
	from Landing.migra.match_customer
	group by migra_website_name, etl_name, new_customer_id
	having count(*) > 1 and count(distinct customer_email) > 1
	order by count(*) desc, etl_name, migra_website_name, new_customer_id



	select migra_website_name, etl_name, old_customer_id, count(*) num_rows, count(distinct new_customer_id) num_dist_rows
	from Landing.migra.match_customer
	group by migra_website_name, etl_name, old_customer_id
	having count(*) > 1
	order by count(*) desc, etl_name, migra_website_name, old_customer_id

------------------------------------------------------------------------
----------------------- migra.match_order_header----------------------------
------------------------------------------------------------------------

	select top 1000 migra_website_name, etl_name, 
		old_order_id, magento_order_id, 
		proforma_order_id, proforma_order_no, 
		new_order_id, new_order_no, 
		old_customer_id, proforma_customer_id, new_customer_id, customer_email,
		idETLBatchRun, ins_ts
	from Landing.migra.match_order_header

	select idETLBatchRun, count(*) num_rows
	from Landing.migra.match_order_header
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct old_order_id) num_dist_rows, count(distinct new_order_id) num_dist_rows
	from Landing.migra.match_order_header

	select migra_website_name, etl_name, count(*) num_rows, count(distinct new_order_id) num_dist_rows
	from Landing.migra.match_order_header
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name





------------------------------------------------------------------------
----------------------- migra.match_order_line ----------------------------
------------------------------------------------------------------------

	select top 1000 migra_website_name, etl_name, 
		old_item_id, num_order_line,
		proforma_line_id, 
		new_order_line_id, new_order_id, 
		old_product_id, proforma_product_id, new_product_id,
		idETLBatchRun, ins_ts
	from Landing.migra.match_order_line

	select idETLBatchRun, count(*) num_rows
	from Landing.migra.match_order_line
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct new_order_line_id) num_dist_rows
	from Landing.migra.match_order_line

	select migra_website_name, etl_name, count(*) num_rows, count(distinct new_order_line_id) num_dist_rows
	from Landing.migra.match_order_line
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


	
	select top 1000 mol.migra_website_name, pr_aura.product_id, count(*)
	from 
			Landing.migra.match_order_line mol
		inner join
			Landing.map.prod_product_aura_aud pr_aura on mol.new_product_id = pr_aura.product_id
	group by mol.migra_website_name, pr_aura.product_id
	order by mol.migra_website_name, pr_aura.product_id	


--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_hist
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_hist_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_item_hist
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_item_hist_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_address_hist
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_address_hist_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


--------------------------------------------------

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_migrate
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_migrate_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_item_migrate
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_item_migrate_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name


	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_address_migrate
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name

	select migra_website_name, etl_name, count(*) num_rows
	from Landing.migra.sales_flat_order_address_migrate_aud
	group by migra_website_name, etl_name
	order by etl_name, migra_website_name
