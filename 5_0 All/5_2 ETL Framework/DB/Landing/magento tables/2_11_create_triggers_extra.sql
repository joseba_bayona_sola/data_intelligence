------------------------------- Triggers ----------------------------------

use Landing
go


------------------------- review_entity ----------------------------

	drop trigger mag.trg_review_entity;
	go

	create trigger mag.trg_review_entity on mag.review_entity
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.review_entity_aud with (tablock) as trg 
		using inserted src 
			on (trg.entity_id = src.entity_id)
		when matched and not exists 
			(select isnull(trg.entity_code, '')
			intersect
			select isnull(src.entity_code, ''))
		
			then 
				update set
					trg.entity_code = src.entity_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (entity_id, entity_code, idETLBatchRun)
					values (src.entity_id, src.entity_code, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_rewiew_entity_aud;		
	go

	create trigger mag.trg_rewiew_entity_aud on mag.review_entity_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.review_entity_aud_hist (entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.entity_id, d.entity_code, 
				d.idETLBatchRun, 
			   case when (d.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.entity_id = i.entity_id;
	end;
	go	
	
------------------------- review_status ----------------------------

	drop trigger mag.trg_review_status;
	go

	create trigger mag.trg_review_status on mag.review_status
	after insert
	as
	begin
	
		set nocount on
	
		merge into mag.review_status_aud with (tablock) as trg
		using inserted src
			on trg.status_id = src.status_id
		when matched and not exists
			(select isnull(trg.status_code, '')
			intersect
			select isnull(src.status_code, ''))
	
			then 
				update set
					trg.status_code = src.status_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.ins_ts = getutcdate()
	
		when not matched 
		then
			insert (status_id, status_code, idETLBatchRun) 
				values (src.status_id, src.status_code, src.idETLBatchRun);
	
	end;
	go



	drop trigger mag.trg_review_status_aud;
	go

	create trigger mag.trg_review_status_aud on mag.review_status_aud
	for update, delete
	as 
	begin

		set nocount on
	
		insert into mag.review_status_aud_hist (status_id, status_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)

			select d.status_id, d.status_code, 
				d.idETLBatchRun, 
				case when (d.status_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d 
				left join
					inserted i on d.status_id = i.status_id;
		
	end;
	go		




	drop trigger mag.trg_review;
	go 

	create trigger mag.trg_review on mag.review
	after insert
	as
	begin
		set nocount on

		merge into mag.review_aud with (tablock) as trg
		using inserted src
			on (trg.review_id = src.review_id)
		when matched and not exists
			(select isnull(trg.created_at, ' '), isnull(trg.entity_id, 0), isnull(trg.status_id, 0), isnull(trg.entity_pk_value, 0)
			intersect
			select isnull(src.created_at, ' '), isnull(src.entity_id, 0), isnull(src.status_id, 0), isnull(src.entity_pk_value, 0))
			then
				update set
					trg.created_at = src.created_at, trg.entity_id = src.entity_id, trg.status_id = src.status_id, trg.entity_pk_value = src.entity_pk_value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		
		when not matched 
			then
				insert (review_id, created_at, entity_id, status_id, entity_pk_value, idETLBatchRun)
					values (src.review_id, src.created_at, src.status_id, src.entity_id, src.entity_pk_value, src.idETLBatchRun);
	end; 
	go

------------------------- review ----------------------------


	drop trigger mag.trg_review_aud;
	go 

	create trigger mag.trg_review_aud on mag.review_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.review_aud_hist (review_id, created_at, entity_id, status_id, entity_pk_value, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
					
		select d.review_id, d.created_at, d.entity_id, d.status_id, d.entity_pk_value, 
			d.idETLBatchRun, case when (d.review_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
		from 
				deleted d
			left join 
				inserted i on d.review_id = i.review_id;
	end;
	go

------------------------- review_store ----------------------------

	drop trigger mag.trg_review_store;
	go

	create trigger mag.trg_review_store on mag.review_store
	after insert 
	as 
	begin

		set nocount on
	
		merge into mag.review_store_aud with (tablock) as trg
		using inserted src
			on (trg.review_id = src.review_id and trg.store_id = src.store_id)

		when not matched then
			insert (review_id, store_id, idETLBatchRun)
				values (src.review_id, src.store_id, src.idETLBatchRun);
	end;
	go


------------------------- review_detail ----------------------------


	drop trigger mag.trg_review_detail;
	go 

	create trigger mag.trg_review_detail on mag.review_detail
	after insert
	as
	begin
		set nocount on

		merge into mag.review_detail_aud with (tablock) as trg
		using inserted src
			on (trg.detail_id = src.detail_id)
		when matched and not exists
			(select isnull(trg.review_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.title, ' '), isnull(trg.detail, ' '), isnull(trg.nickname, ' '), isnull(trg.customer_id, 0)
			intersect
			select isnull(src.review_id, 0), isnull(src.store_id, 0), 
				isnull(src.title, ' '), isnull(src.detail, ' '), isnull(src.nickname, ' '), isnull(src.customer_id, 0))
			then
				update set
					trg.review_id = src.review_id, trg.store_id = src.store_id, 
					trg.title = src.title, trg.detail = src.detail, trg.nickname = src.nickname, trg.customer_id = src.customer_id,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (detail_id, review_id, store_id, 
					title, detail, nickname, customer_id, idETLBatchRun)

					values (src.detail_id, src.review_id, src.store_id, 
						src.title, src.detail, src.nickname, src.customer_id, src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_review_detail_aud;
	go 

	create trigger mag.trg_review_detail_aud on mag.review_detail_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.review_detail_aud_hist(detail_id, review_id, store_id, 
			title, detail, nickname, customer_id,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
			select d.detail_id, d.review_id, d.store_id, 
				d.title, d.detail, d.nickname, d.customer_id, 
				d.idETLBatchRun, 
				case when (d.review_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i 
			on d.review_id = i.review_id;
	end;
	go





------------------------- rating_entity ----------------------------

	drop trigger mag.trg_rating_entity;
	go 

	create trigger mag.trg_rating_entity on mag.rating_entity
	after insert 
	as
	begin

		set nocount on

		merge into mag.rating_entity_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.entity_code, ' ')
			 intersect
			 select isnull(trg.entity_code, ' '))

		then update set
			trg.entity_code = src.entity_code,
			trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (entity_id, entity_code, idETLBatchRun)
				values (src.entity_id, src.entity_code, src.idETLBatchRun);

	end;
	go


	drop trigger mag.trg_rating_entity_aud;
	go 

	create trigger mag.trg_rating_entity_aud on mag.rating_entity_aud
	for update, delete
	as
	begin

		set nocount on

		insert into mag.rating_entity_aud_hist (entity_id, entity_code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.entity_code, 
				d.idETLBatchRun, case when (d.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.entity_id = i.entity_id
	end;
	go


------------------------- rating ----------------------------

	drop trigger mag.trg_rating;
	go 

	create trigger mag.trg_rating on mag.rating
	after insert
	as 
	begin
	
		set nocount on

		merge into mag.rating_aud with (tablock) as trg
		using inserted src
			on (trg.rating_id = src.rating_id)
		when matched and not exists
			(select isnull(trg.entity_id, 0), isnull(trg.rating_code, ' '), isnull(trg.position, 0)
			 intersect
			 select isnull(src.entity_id, 0), isnull(src.rating_code, ' '), isnull(src.position, 0))

				then 
					update set
						trg.entity_id = src.entity_id, trg.rating_code = src.rating_code, trg.position = src.position,
						trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (rating_id, entity_id, rating_code, position, idETLBatchRun)
				values (src.rating_id, src.entity_id, src.rating_code, src.position, src.idETLBatchRun);

	end;
	go



	drop trigger mag.trg_rating_aud;
	go 

	create trigger mag.trg_rating_aud on mag.rating_aud
	for delete, update
	as
	begin

		set nocount on

		insert into mag.rating_aud_hist (rating_id, entity_id, rating_code, position, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
		
			select  d.rating_id, d.entity_id, d.rating_code, d.position, 
				d.idETLBatchRun, case when (d.rating_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d 
				left join
					inserted i on  d.rating_id = i.rating_id
	end;
	go


------------------------- rating_option ----------------------------

	drop trigger mag.trg_rating_option;
	go 

	create trigger mag.trg_rating_option on mag.rating_option
	after insert
	as
	begin

		set nocount on

		merge into mag.rating_option_aud with (tablock) as trg
		using inserted src
			on (trg.option_id = src.option_id)
		when matched and not exists
			(select isnull(trg.rating_id, 0), isnull(trg.code, ' '), isnull(trg.value, 0), isnull(trg.position, 0)
			intersect
			select isnull(src.rating_id, 0), isnull(src.code, ' '), isnull(src.value, 0), isnull(src.position, 0))
			
		then 
			update set
				trg.rating_id = src.rating_id, trg.code = src.code, trg.value = src.value, trg.position = src.position,
				trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched then
			insert (option_id, rating_id, code, value, position, idETLBatchRun)
				values (src.option_id, src.rating_id, src.code, src.value, src.position, src.idETLBatchRun);
	end;
	go
			

	drop trigger mag.trg_rating_option_aud;
	go 

	create trigger mag.trg_rating_option_aud on mag.rating_option_aud		
	for update, delete
	as
	begin

		set nocount on 

		insert into mag.rating_option_aud_hist (option_id, rating_id, code, value, position, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)

		select d.option_id, d.rating_id, d.code, d.value, d.position, 
			d.idETLBatchRun, 
			case when (d.option_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
		from
				deleted d	
			left join
				inserted i on d.option_id = i.option_id;
	end;
	go


------------------------- rating_option_vote ----------------------------

	drop trigger mag.trg_rating_option_vote;
	go 

	create trigger mag.trg_rating_option_vote on mag.rating_option_vote
	after insert 
	as
	begin
	
		set nocount on 
		
		merge into mag.rating_option_vote_aud with (tablock) as trg
		using inserted src 
			on trg.vote_id = src.vote_id
		when matched and not exists
			(select
				isnull(trg.review_id, 0), isnull(trg.remote_ip, ' '), isnull(trg.customer_id, 0), isnull(trg.entity_pk_value, 0),
				isnull(trg.rating_id, 0), isnull(trg.option_id, 0), isnull(trg.percent_rating, 0), isnull(trg.value, 0)
			intersect
			select 
				isnull(src.review_id, 0), isnull(src.remote_ip, ' '), isnull(src.customer_id, 0), isnull(src.entity_pk_value, 0),
				isnull(src.rating_id, 0), isnull(src.option_id, 0), isnull(src.percent_rating, 0), isnull(src.value, 0)) 
			
			then 
				update set
					trg.review_id = src.review_id, trg.remote_ip = src.remote_ip, trg.customer_id = src.customer_id, trg.entity_pk_value = src.entity_pk_value, 
					trg.rating_id = src.rating_id, trg.option_id = src.option_id, trg.percent_rating = src.percent_rating, trg.value = src.value,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched then		
		insert
			(vote_id, review_id, remote_ip, customer_id, entity_pk_value, 
				rating_id, option_id, percent_rating, value, idETLBatchRun)
		 values
			(src.vote_id, src.review_id, src.remote_ip, src.customer_id, src.entity_pk_value, 
				src.rating_id, src.option_id, src.percent_rating, src.value, src.idETLBatchRun);
			 
	end;
	go
	
	drop trigger mag.trg_rating_option_vote_aud;
	go 

	create trigger mag.trg_rating_option_vote_aud on mag.rating_option_vote_aud
	after insert
	as
	begin
	
		set nocount on
		
		insert into mag.rating_option_vote_aud_hist (vote_id, review_id, remote_ip, customer_id, entity_pk_value, 
			rating_id, option_id, percent_rating, value,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)
		
			select d.vote_id, d.review_id, d.remote_ip, d.customer_id, d.entity_pk_value, 
				d.rating_id, d.option_id, d.percent_rating, d.value,
				d.idETLBatchRun, case when (d.vote_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.vote_id = i.vote_id;

	end;
	go	

	
	
------------------------- wearer ----------------------------

	drop trigger mag.trg_wearer;
	go

	create trigger mag.trg_wearer on mag.wearer
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.wearer_aud with (tablock) as trg 
		using inserted src 
			on (trg.id = src.id)
		when matched and not exists 
			(select isnull(trg.customer_id, 0), isnull(trg.name, ''), isnull(trg.dob, '')
			intersect
			select isnull(src.customer_id, 0), isnull(src.name, ''), isnull(src.dob, ''))
		
			then 
				update set
					trg.customer_id = src.customer_id, trg.name = src.name, trg.dob = src.dob, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (id, customer_id, name, dob, idETLBatchRun)
					values (src.id, src.customer_id, src.name, src.dob, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_wearer_aud;		
	go

	create trigger mag.trg_wearer_aud on mag.wearer_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.wearer_aud_hist (id, customer_id, name, dob,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.id, d.customer_id, d.name, d.dob, 
				d.idETLBatchRun, 
			   case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.id = i.id;
	end;
	go	




------------------------- wearer_prescription_method ----------------------------

	drop trigger mag.trg_wearer_prescription_method;
	go

	create trigger mag.trg_wearer_prescription_method on mag.wearer_prescription_method
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.wearer_prescription_method_aud with (tablock) as trg 
		using inserted src 
			on (trg.id = src.id)
		when matched and not exists 
			(select isnull(trg.method, ''), isnull(trg.code, '')
			intersect
			select isnull(src.method, ''), isnull(src.code, ''))
		
			then 
				update set
					trg.method = src.method, trg.code = src.code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (id, method, code, idETLBatchRun)
					values (src.id, src.method, src.code, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_wearer_prescription_method_aud;		
	go

	create trigger mag.trg_wearer_prescription_method_aud on mag.wearer_prescription_method_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.wearer_prescription_method_aud_hist (id, method, code, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.id, d.method, d.code,
				d.idETLBatchRun, 
			   case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.id = i.id;
	end;
	go	




------------------------- wearer_prescription ----------------------------

	drop trigger mag.trg_wearer_prescription;
	go

	create trigger mag.trg_wearer_prescription on mag.wearer_prescription
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.wearer_prescription_aud with (tablock) as trg 
		using inserted src 
			on (trg.id = src.id)
		when matched and not exists 
			(select isnull(trg.wearer_id, 0), isnull(trg.method_id, 0), 
				isnull(trg.date_last_prescription, ''), isnull(trg.date_start_prescription, ''), 
				isnull(trg.optician_name, ''), isnull(trg.optician_address1, ''), isnull(trg.optician_address2, ''), isnull(trg.optician_city, ''), isnull(trg.optician_state, ''), 
				isnull(trg.optician_country, ''), isnull(trg.optician_postcode, ''), isnull(trg.optician_phone, ''), isnull(trg.optician_lookup_id, 0), 
				isnull(trg.reminder_sent, 0)
			intersect
			select isnull(src.wearer_id, 0), isnull(src.method_id, 0), 
				isnull(src.date_last_prescription, ''), isnull(src.date_start_prescription, ''), 
				isnull(src.optician_name, ''), isnull(src.optician_address1, ''), isnull(src.optician_address2, ''), isnull(src.optician_city, ''), isnull(src.optician_state, ''), 
				isnull(src.optician_country, ''), isnull(src.optician_postcode, ''), isnull(src.optician_phone, ''), isnull(src.optician_lookup_id, 0), 
				isnull(src.reminder_sent, 0))
		
			then 
				update set
					trg.wearer_id = src.wearer_id, trg.method_id = src.method_id, 
					trg.date_last_prescription = src.date_last_prescription, trg.date_start_prescription = src.date_start_prescription, 
					trg.optician_name = src.optician_name, trg.optician_address1 = src.optician_address1, trg.optician_address2 = src.optician_address2, trg.optician_city = src.optician_city, trg.optician_state = src.optician_state, 
					trg.optician_country = src.optician_country, trg.optician_postcode = src.optician_postcode, trg.optician_phone = src.optician_phone, trg.optician_lookup_id = src.optician_lookup_id, 
					trg.reminder_sent = src.reminder_sent, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (id, 
					wearer_id, method_id, 
					date_last_prescription, date_start_prescription, 
					optician_name, optician_address1, optician_address2, optician_city, optician_state, optician_country, 
					optician_postcode, optician_phone, optician_lookup_id, 
					reminder_sent, idETLBatchRun)
					
					values (src.id, 
						src.wearer_id, src.method_id, 
						src.date_last_prescription, src.date_start_prescription, 
						src.optician_name, src.optician_address1, src.optician_address2, src.optician_city, src.optician_state, src.optician_country, 
						src.optician_postcode, src.optician_phone, src.optician_lookup_id, 
						src.reminder_sent, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_wearer_prescription_aud;		
	go

	create trigger mag.trg_wearer_prescription_aud on mag.wearer_prescription_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.wearer_prescription_aud_hist (id, 
			wearer_id, method_id, 
			date_last_prescription, date_start_prescription, 
			optician_name, optician_address1, optician_address2, optician_city, optician_state, optician_country, 
			optician_postcode, optician_phone, optician_lookup_id, 
			reminder_sent,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.id, 
				d.wearer_id, d.method_id, 
				d.date_last_prescription, d.date_start_prescription, 
				d.optician_name, d.optician_address1, d.optician_address2, d.optician_city, d.optician_state, d.optician_country, 
				d.optician_postcode, d.optician_phone, d.optician_lookup_id, 
				d.reminder_sent,
				d.idETLBatchRun, 
			   case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.id = i.id;
	end;
	go	




------------------------- wearer_order_item ----------------------------

	drop trigger mag.trg_wearer_order_item;
	go

	create trigger mag.trg_wearer_order_item on mag.wearer_order_item
	after insert 
	as
	begin

		set nocount  on 
	
		merge into mag.wearer_order_item_aud with (tablock) as trg 
		using inserted src 
			on (trg.id = src.id)
		when matched and not exists 
			(select isnull(trg.order_id, ''), isnull(trg.item_id, 0), isnull(trg.quote_item_id, 0), 
				isnull(trg.prescription_type, ''), 
				isnull(trg.wearer_prescription_id, 0), isnull(trg.prescription_flag, '')
			intersect
			select isnull(src.order_id, ''), isnull(src.item_id, 0), isnull(src.quote_item_id, 0), 
				isnull(src.prescription_type, ''), 
				isnull(src.wearer_prescription_id, 0), isnull(src.prescription_flag, ''))
		
			then 
				update set
					trg.order_id = src.order_id, trg.item_id = src.item_id, trg.quote_item_id = src.quote_item_id, 
					trg.prescription_type = src.prescription_type, 
					trg.wearer_prescription_id = src.wearer_prescription_id, trg.prescription_flag = src.prescription_flag, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
			
		when not matched
			then 
				insert (id, 
					order_id, item_id, quote_item_id, 
					prescription_type, 
					wearer_prescription_id, prescription_flag, idETLBatchRun)
					
					values (src.id, 
						src.order_id, src.item_id, src.quote_item_id, 
						src.prescription_type, 
						src.wearer_prescription_id, src.prescription_flag, src.idETLBatchRun);	
		
	end;
	go


	drop trigger mag.trg_wearer_order_item_aud;		
	go

	create trigger mag.trg_wearer_order_item_aud on mag.wearer_order_item_aud
	for update, delete
	as 
	begin
		set nocount on
	
		insert into mag.wearer_order_item_aud_hist (id, 
			order_id, item_id, quote_item_id, 
			prescription_type, 
			wearer_prescription_id, prescription_flag,
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo)	
		
			select d.id, 
				d.order_id, d.item_id, d.quote_item_id, 
				d.prescription_type, 
				d.wearer_prescription_id, d.prescription_flag,
				d.idETLBatchRun, 
				case when (d.id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo
			from 
					deleted d
				left join
					inserted i on d.id = i.id;
	end;
	go	

	


