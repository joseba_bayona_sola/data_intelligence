use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_shipments ----------------------------
------------------------------------------------------------------------

select top 1000 entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	total_qty, 
	total_weight, 
	shipping_description, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_shipment_aud;

	select store_id, count(*)
	from mag.sales_flat_shipment_aud
	group by store_id
	order by store_id

	select shipping_description, count(*)
	from mag.sales_flat_shipment_aud
	group by shipping_description
	order by shipping_description

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.sales_flat_shipment_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.sales_flat_shipment_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	-- total_qty, 
	select top 1000 entity_id, count(distinct total_qty) num_dist
	from mag.sales_flat_shipment_aud_v
	group by entity_id
	having count(distinct total_qty) > 1
	order by num_dist desc;

	-- total_weight
	select top 1000 entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		total_qty, 
		total_weight, 
		shipping_description, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_shipment_aud
	where total_weight is not null;

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ----------------------------------

	select top 1000 count(*) over () num_tot,
		oh.entity_id, oh.increment_id, sh.entity_id entity_id_s, sh.increment_id increment_id_s, 
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, sh.created_at created_at_s, sh.updated_at updated_at_s, 
		oh.state, oh.status, 
		oh.total_qty_ordered, sh.total_qty, 
		oh.base_grand_total, oh.base_total_refunded
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud
			-- where created_at > getutcdate() - 20
			) oh
		full join
			(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				total_qty
			from mag.sales_flat_shipment_aud) sh on oh.entity_id = sh.order_id
	where sh.order_id is null

		select top 1000 oh.state, oh.status, count(*)
		-- select top 1000 year(oh.created_at) yyyy, month(oh.created_at) mm, count(*)
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_order_aud
				where created_at > getutcdate() - 20
				) oh
			full join
				(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
					total_qty
				from mag.sales_flat_invoice_aud) sh on oh.entity_id = sh.order_id
		where sh.order_id is null
		group by oh.state, oh.status
		order by oh.state, oh.status
		-- group by year(oh.created_at), month(oh.created_at)
		-- order by year(oh.created_at), month(oh.created_at)


	select top 1000 count(*) over () num_tot,
		count(sh.entity_id) over (partition by oh.entity_id) num_ships,
		oh.entity_id, oh.increment_id, sh.entity_id entity_id_s, sh.increment_id increment_id_s, 
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, sh.created_at created_at_s, sh.updated_at updated_at_s, 
		oh.state, oh.status, 
		oh.total_qty_ordered, sh.total_qty, 
		oh.base_grand_total, oh.base_total_refunded
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud
			-- where created_at > getutcdate() - 20
			) oh
		inner join
			(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				total_qty
			from mag.sales_flat_shipment_aud) sh on oh.entity_id = sh.order_id
	where oh.total_qty_ordered <> sh.total_qty
	-- order by oh.entity_id
	order by num_ships desc, oh.entity_id

	select *
	from
		(select count(*) over () num_tot,
			count(sh.entity_id) over (partition by oh.entity_id) num_ships,
			oh.entity_id, oh.increment_id, sh.entity_id entity_id_s, sh.increment_id increment_id_s, 
			oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, sh.created_at created_at_s, sh.updated_at updated_at_s, 
			oh.state, oh.status, 
			oh.total_qty_ordered, sh.total_qty, sum(sh.total_qty) over (partition by oh.entity_id) qty_sum, 
			oh.base_grand_total, oh.base_total_refunded
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_order_aud
				-- where created_at > getutcdate() - 20
				) oh
			inner join
				(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
					total_qty
				from mag.sales_flat_shipment_aud) sh on oh.entity_id = sh.order_id) t
	where num_ships > 1
		and total_qty_ordered <> qty_sum
	order by entity_id

------------------------------------------------------------------------
----------------------- sales_flat_shipment_item ----------------------------
------------------------------------------------------------------------

	select top 1000 entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, 
		weight, 
		price, 
		row_total, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_shipment_item_aud;

	-- base_row_total
	select top 1000 entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, 
		weight, 
		price, 
		row_total, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_shipment_item_aud
	where row_total is not null;

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ---------------------------------- 

	select top 1000 
		sh.entity_id, sh.increment_id, si.entity_id, sh.store_id, sh.created_at, sh.updated_at, 
		sh.total_qty, si.qty
	from
			(select *
			from mag.sales_flat_shipment_aud) sh
		full join
			(select *
			from mag.sales_flat_shipment_item_aud) si on sh.entity_id = si.parent_id
	where sh.entity_id is null or si.parent_id is null
	order by sh.entity_id desc, si.entity_id


	select top 1000 count(*) over () num_tot,
		count(*) over(partition by sh.entity_id) num_lines,
		sh.entity_id, sh.increment_id, si.entity_id, si.order_item_id, sh.store_id, sh.created_at, sh.updated_at, 
		sh.total_qty, si.qty, sum(si.qty) over (partition by sh.entity_id) qty_sum
	from
			(select *
			from mag.sales_flat_shipment_aud) sh
		inner join
			(select *
			from mag.sales_flat_shipment_item_aud) si on sh.entity_id = si.parent_id
	order by sh.entity_id desc, si.entity_id

	select 
		count(*) over () num_tot, count(*) over(partition by entity_id) num_lines, * 
	from
		(select top 1000 
			sh.entity_id, sh.increment_id, si.entity_id item_id, si.order_item_id, sh.store_id, sh.created_at, sh.updated_at, 
			sh.total_qty, si.qty, sum(si.qty) over (partition by sh.entity_id) qty_sum
		from
				(select *
				from mag.sales_flat_shipment_aud) sh
			inner join
				(select *
				from mag.sales_flat_shipment_item_aud) si on sh.entity_id = si.parent_id) t
	where total_qty <> qty_sum 
	order by entity_id desc, item_id
