use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- customer_entity ----------------------------

	drop trigger mag.trg_customer_entity;
	go 

	create trigger mag.trg_customer_entity on mag.customer_entity
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select isnull(trg.increment_id, ''), isnull(trg.email, ''), 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.group_id, 0), isnull(trg.website_group_id, 0), isnull(trg.website_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.is_active, 0), isnull(trg.disable_auto_group_change, 0), isnull(trg.created_at, ''), isnull(trg.updated_at, '') 
			intersect
			select isnull(src.increment_id, ''), isnull(src.email, ''), 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.group_id, 0), isnull(src.website_group_id, 0), isnull(src.website_id, 0), isnull(src.store_id, 0), 
				isnull(src.is_active, 0), isnull(src.disable_auto_group_change, 0), isnull(src.created_at, ''), isnull(src.updated_at, ''))
			
			then
				update set
					trg.increment_id = src.increment_id, trg.email = src.email, 
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.group_id = src.group_id, trg.website_group_id = src.website_group_id, trg.website_id = src.website_id, trg.store_id = src.store_id, 
					trg.is_active = src.is_active, trg.disable_auto_group_change = src.disable_auto_group_change, trg.created_at = src.created_at, trg.updated_at = src.updated_at,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then
				insert (entity_id, increment_id, email, 
					entity_type_id, attribute_set_id, 
					group_id, website_group_id, website_id, store_id, 
					is_active, disable_auto_group_change, created_at, updated_at, 
					idETLBatchRun)
					
					values (src.entity_id, src.increment_id, src.email, 
						src.entity_type_id, src.attribute_set_id, 
						src.group_id, src.website_group_id, src.website_id, src.store_id, 
						src.is_active, src.disable_auto_group_change, src.created_at, src.updated_at, 						
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_aud;
	go

	create trigger mag.trg_customer_entity_aud on mag.customer_entity_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_aud_hist (entity_id, increment_id, email, 
			entity_type_id, attribute_set_id, 
			group_id, website_group_id, website_id, store_id, 
			is_active, disable_auto_group_change, created_at, updated_at,  
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, d.increment_id, d.email, 
				d.entity_type_id, d.attribute_set_id, 
				d.group_id, d.website_group_id, d.website_id, d.store_id, 
				d.is_active, d.disable_auto_group_change, d.created_at, d.updated_at,  
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go


-----

------------------------- customer_entity_datetime ----------------------------

	drop trigger mag.trg_customer_entity_datetime;
	go 

	create trigger mag.trg_customer_entity_datetime on mag.customer_entity_datetime
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_datetime_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), 
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_datetime_aud;
	go

	create trigger mag.trg_customer_entity_datetime_aud on mag.customer_entity_datetime_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_datetime_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- customer_entity_decimal ----------------------------

	drop trigger mag.trg_customer_entity_decimal;
	go 

	create trigger mag.trg_customer_entity_decimal on mag.customer_entity_decimal
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_decimal_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), 
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_decimal_aud;
	go

	create trigger mag.trg_customer_entity_decimal_aud on mag.customer_entity_decimal_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_decimal_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- customer_entity_int ----------------------------

	drop trigger mag.trg_customer_entity_int;
	go 

	create trigger mag.trg_customer_entity_int on mag.customer_entity_int
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_int_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), 
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_int_aud;
	go

	create trigger mag.trg_customer_entity_int_aud on mag.customer_entity_int_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_int_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- customer_entity_text ----------------------------

	drop trigger mag.trg_customer_entity_text;
	go 

	create trigger mag.trg_customer_entity_text on mag.customer_entity_text
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_text_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), 
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_text_aud;
	go

	create trigger mag.trg_customer_entity_text_aud on mag.customer_entity_text_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_text_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- customer_entity_varchar ----------------------------

	drop trigger mag.trg_customer_entity_varchar;
	go 

	create trigger mag.trg_customer_entity_varchar on mag.customer_entity_varchar
	after insert
	as
	begin
		set nocount on

		merge into mag.customer_entity_varchar_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), 
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_customer_entity_varchar_aud;
	go

	create trigger mag.trg_customer_entity_varchar_aud on mag.customer_entity_varchar_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.customer_entity_varchar_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

