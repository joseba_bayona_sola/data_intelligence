use Landing
go 

----------------------- catalog_product_entity ----------------------------

select * 
from mag.catalog_product_entity_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_product_entity_datetime ----------------------------

select * 
from mag.catalog_product_entity_decimal_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_product_entity_decimal ----------------------------

select * 
from mag.catalog_product_entity_int_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_product_entity_text ----------------------------

select * 
from mag.catalog_product_entity_text_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_product_entity_varchar ----------------------------

select * 
from mag.catalog_product_entity_varchar_aud_v
where num_records > 1
order by entity_id, idETLBatchRun




----------------------- catalog_product_entity_tier_price ----------------------------

select * 
from mag.catalog_product_entity_tier_price_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_product ----------------------------

select * 
from mag.catalog_category_product_aud_v
where num_records > 1
order by category_id, product_id, idETLBatchRun

----------------------- catalog_product_website ----------------------------

select * 
from mag.catalog_product_website_aud_v
where num_records > 1
order by product_id, website_id, idETLBatchRun

