use Landing
go 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_eav_entity_type
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - eav_entity_type
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_eav_entity_type
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select entity_type_id, entity_type_code, 
			entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
			is_data_sharing, data_sharing_key, default_attribute_set_id, 
			increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
			additional_attribute_table, entity_attribute_collection, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_type_id, entity_type_code, 
				entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
				is_data_sharing, data_sharing_key, default_attribute_set_id, 
				increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
				additional_attribute_table, entity_attribute_collection 
			from magento01.eav_entity_type'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----- 

drop procedure mag.srcmag_lnd_get_eav_attribute
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - eav_attribute
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_eav_attribute
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select attribute_id, entity_type_id, attribute_code, attribute_model,
			 backend_model, backend_type, backend_table, 
			 frontend_model, frontend_input, frontend_label, frontend_class,
			 source_model, 
			 is_required, is_user_defined, default_value, is_unique, note, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select attribute_id, entity_type_id, attribute_code, attribute_model,
				 backend_model, backend_type, backend_table, 
				 frontend_model, frontend_input, frontend_label, frontend_class,
				 source_model, 
				 is_required, is_user_defined, default_value, is_unique, note 
			from magento01.eav_attribute'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


----- 

drop procedure mag.srcmag_lnd_get_eav_entity_attribute
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - eav_entity_attribute
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_eav_entity_attribute
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select entity_attribute_id, 
			entity_type_id, 
			attribute_id, attribute_set_id, attribute_group_id, 
			sort_order, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_attribute_id, 
				entity_type_id, 
				attribute_id, attribute_set_id, attribute_group_id, 
				sort_order 
			from magento01.eav_entity_attribute'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


----- 

drop procedure mag.srcmag_lnd_get_eav_attribute_option
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - eav_attribute_option
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_eav_attribute_option
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select option_id, 
			attribute_id, sort_order , ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select option_id, 
				attribute_id, sort_order 
			from magento01.eav_attribute_option'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 


----- 

drop procedure mag.srcmag_lnd_get_eav_attribute_option_value
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - eav_attribute_option_value
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_eav_attribute_option_value
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = 'select value_id, 
			option_id, store_id, 
			value, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select value_id, 
				option_id, store_id, 
				value 
			from magento01.eav_attribute_option_value'')'
	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

