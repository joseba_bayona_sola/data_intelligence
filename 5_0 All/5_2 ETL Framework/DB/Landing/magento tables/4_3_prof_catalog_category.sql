
use Landing
go 

----------------------- catalog_category_entity ----------------------------

	select entity_id, parent_id, 
		entity_type_id, attribute_set_id, 
		position, level, children_count, path, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity;

	select entity_id, parent_id, 
		entity_type_id, attribute_set_id, 
		position, level, children_count, path, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_aud;

	select entity_id, parent_id, 
		entity_type_id, attribute_set_id, 
		position, level, children_count, path, 
		created_at, updated_at, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_aud_hist;


----------------------- catalog_category_entity_datetime ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity_datetime

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_datetime_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_datetime_aud_hist

----------------------- catalog_category_entity_decimal ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity_decimal

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_decimal_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_decimal_aud_hist

----------------------- catalog_category_entity_int ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity_int

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_int_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_int_aud_hist

----------------------- catalog_category_entity_text ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity_text

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_text_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_text_aud_hist

----------------------- catalog_category_entity_varchar ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_entity_varchar

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_entity_varchar_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_entity_varchar_aud_hist
