use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_creditmemo ----------------------------
------------------------------------------------------------------------

select top 1000 entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
	base_grand_total, 
	base_adjustment, base_adjustment_positive, base_adjustment_negative, 
	base_to_global_rate, base_to_order_rate, order_currency_code, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_creditmemo_aud
where order_id = 4987236;

	select store_id, count(*)
	from mag.sales_flat_creditmemo_aud
	group by store_id
	order by store_id

	select state, count(*)
	from mag.sales_flat_creditmemo_aud
	group by state
	order by state

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.sales_flat_creditmemo_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.sales_flat_creditmemo_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	-- base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
	select top 1000 entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
		base_grand_total, 
		base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0) base_grand_calc
	from mag.sales_flat_creditmemo_aud
	where base_grand_total <> base_subtotal + base_shipping_amount + base_discount_amount - isnull(base_customer_balance_amount, 0);

	-- base_adjustment, base_adjustment_positive, base_adjustment_negative
	select top 1000 entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		base_adjustment, base_adjustment_positive, base_adjustment_negative
	from mag.sales_flat_creditmemo_aud
	where base_adjustment <> 0
	order by base_adjustment desc

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ---------------------------------- 

	select top 1000 count(*) over () num_tot,
		oh.entity_id, oh.increment_id, crh.entity_id entity_id_cr, crh.increment_id increment_id_cr, crh.order_id,
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, crh.created_at created_at_cr, crh.updated_at updated_at_cr, 
		oh.state, oh.status, 
		oh.total_qty_ordered,  
		oh.base_grand_total, oh.base_total_refunded, crh.base_grand_total base_grand_total_cr
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud
			where base_total_refunded is not null
			) oh
		full join
			(select entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, 
				base_adjustment
			from mag.sales_flat_creditmemo_aud) crh on oh.entity_id = crh.order_id
	where crh.order_id is null order by oh.created_at
	-- where oh.entity_id is null order by crh.created_at


	select top 1000 count(*) over () num_tot,
		count(crh.entity_id) over (partition by oh.entity_id) num_crmemo,
		oh.entity_id, oh.increment_id, crh.entity_id entity_id_cr, crh.increment_id increment_id_cr, crh.order_id,
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, crh.created_at created_at_cr, crh.updated_at updated_at_cr, 
		oh.state, oh.status, 
		oh.total_qty_ordered,  
		oh.base_grand_total, oh.base_total_refunded, crh.base_grand_total base_grand_total_cr
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud
			where created_at > getutcdate() - 20
			) oh
		inner join
			(select entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
				base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
				base_grand_total, 
				base_adjustment
			from mag.sales_flat_creditmemo_aud) crh on oh.entity_id = crh.order_id
	-- where oh.base_total_refunded <> crh.base_grand_total 
	where oh.base_grand_total <> crh.base_grand_total 
	-- order by oh.entity_id
	order by num_crmemo desc, oh.entity_id

	select *
	from
		(select top 1000 count(*) over () num_tot,
			count(crh.entity_id) over (partition by oh.entity_id) num_crmemo,
			oh.entity_id, oh.increment_id, crh.entity_id entity_id_cr, crh.increment_id increment_id_cr, crh.order_id,
			oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, crh.created_at created_at_cr, crh.updated_at updated_at_cr, 
			oh.state, oh.status, 
			oh.total_qty_ordered,  
			oh.base_grand_total, oh.base_total_refunded, crh.base_grand_total base_grand_total_cr
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_order_aud
				where created_at > getutcdate() - 20
				) oh
			inner join
				(select entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
					base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
					base_grand_total, 
					base_adjustment
				from mag.sales_flat_creditmemo_aud) crh on oh.entity_id = crh.order_id) t
	where num_crmemo > 1
	order by entity_id

------------------------------------------------------------------------
----------------------- sales_flat_creditmemo_item ----------------------------
------------------------------------------------------------------------

	select top 1000 entity_id, order_item_id, parent_id,
		product_id, sku, name, description, 
		qty, 
		base_price, price, base_price_incl_tax, 
		base_cost, 
		base_row_total, base_discount_amount,
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_creditmemo_item_aud;

	-- base_price, price, base_price_incl_tax, 
	select top 1000 count(*) over () num_tot,
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		base_price, price, base_price_incl_tax, 
		idETLBatchRun, ins_ts, upd_ts 
	from mag.sales_flat_creditmemo_item_aud
	where base_price <> price or base_price <> base_price_incl_tax;

	-- base_row_total,
	select top 1000 count(*) over () num_tot,
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, base_price, base_row_total,
		qty * base_price base_row_total_calc, abs(base_row_total - (qty * base_price)) dif_calc,
		idETLBatchRun, ins_ts, upd_ts 
	from mag.sales_flat_creditmemo_item_aud
	where qty * base_price <> base_row_total
	order by dif_calc, entity_id;	 

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ---------------------------------- 

	select top 1000 count(*) over () num_tot,
		count(*) over(partition by crh.entity_id) num_lines,
		crh.entity_id, crh.increment_id, cri.entity_id, crh.store_id, crh.created_at, crh.updated_at, 
		cri.qty, crh.base_subtotal, cri.base_row_total, 
		crh.base_shipping_amount, crh.base_discount_amount, crh.base_customer_balance_amount, crh.base_adjustment, crh.base_grand_total
	from
			(select *
			from mag.sales_flat_creditmemo_aud) crh
		full join
			(select *
			from mag.sales_flat_creditmemo_item_aud) cri on crh.entity_id = cri.parent_id
	where crh.entity_id is null or cri.parent_id is null
	order by crh.entity_id desc, cri.entity_id

	select top 1000 *,
		abs(base_subtotal - base_row_total_sum) dif_1
	from 
		(select count(*) over () num_tot,
			count(*) over(partition by crh.entity_id) num_lines,
			crh.entity_id, crh.increment_id, cri.entity_id item_id, crh.store_id, crh.created_at, crh.updated_at, 
			cri.qty, crh.base_subtotal, cri.base_row_total, sum(cri.base_row_total) over (partition by crh.entity_id) base_row_total_sum,
			crh.base_shipping_amount, crh.base_discount_amount, crh.base_customer_balance_amount, crh.base_adjustment, crh.base_grand_total
		from
				(select *
				from mag.sales_flat_creditmemo_aud) crh
			full join
				(select *
				from mag.sales_flat_creditmemo_item_aud) cri on crh.entity_id = cri.parent_id) t
	where entity_id = 1248902
	-- where base_subtotal <> base_row_total_sum and abs(base_subtotal - base_row_total_sum) > 1 
	order by dif_1 desc, entity_id desc, item_id
	

