
use Landing
go 

----------------------- customer_address_entity ----------------------------

	select entity_id, increment_id, parent_id, 
		entity_type_id, attribute_set_id, is_active, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity;

	select entity_id, increment_id, parent_id, 
		entity_type_id, attribute_set_id, is_active, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_aud;

	select entity_id, increment_id, parent_id, 
		entity_type_id, attribute_set_id, is_active, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_aud_hist;


----------------------- customer_address_entity_datetime ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity_datetime

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_datetime_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_datetime_aud_hist

----------------------- customer_address_entity_decimal ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity_decimal

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_decimal_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_decimal_aud_hist

----------------------- customer_address_entity_int ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity_int

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_int_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_int_aud_hist

----------------------- customer_address_entity_text ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity_text

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_text_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_text_aud_hist

----------------------- customer_address_entity_varchar ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts
	from mag.customer_address_entity_varchar

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.customer_address_entity_varchar_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.customer_address_entity_varchar_aud_hist
