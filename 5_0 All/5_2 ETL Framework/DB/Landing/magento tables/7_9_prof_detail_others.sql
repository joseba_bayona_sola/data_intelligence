use Landing
go 

------------------------------------------------------------------------
----------------------- core_store ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.core_store_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct store_id) num_dist_rows
	from mag.core_store_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.core_store_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct store_id) num_dist_rows
	from mag.core_store_aud_hist

	select *
	from
		(select count(*) over(partition by store_id) num_rep, *
		from mag.core_store_aud_hist) t
	where num_rep > 1
	order by num_rep desc, store_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- core_website ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.core_website_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct website_id) num_dist_rows
	from mag.core_website_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.core_website_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct website_id) num_dist_rows
	from mag.core_website_aud_hist

	select *
	from
		(select count(*) over(partition by website_id) num_rep, *
		from mag.core_website_aud_hist) t
	where num_rep > 1
	order by num_rep desc, website_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- core_config_data ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.core_config_data_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct config_id) num_dist_rows
	from mag.core_config_data_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.core_config_data_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct config_id) num_dist_rows
	from mag.core_config_data_aud_hist

	select *
	from
		(select count(*) over(partition by config_id) num_rep, *
		from mag.core_config_data_aud_hist) t
	where num_rep > 1
	order by num_rep desc, config_id, aud_dateFrom




------------------------------------------------------------------------
----------------------- edi_manufacturer ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.edi_manufacturer_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct manufacturer_id) num_dist_rows
	from mag.edi_manufacturer_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.edi_manufacturer_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct manufacturer_id) num_dist_rows
	from mag.edi_manufacturer_aud_hist

	select *
	from
		(select count(*) over(partition by manufacturer_id) num_rep, *
		from mag.edi_manufacturer_aud_hist) t
	where num_rep > 1
	order by num_rep desc, manufacturer_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- edi_supplier_account ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.edi_supplier_account_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct account_id) num_dist_rows
	from mag.edi_supplier_account_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.edi_supplier_account_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct account_id) num_dist_rows
	from mag.edi_supplier_account_aud_hist

	select *
	from
		(select count(*) over(partition by account_id) num_rep, *
		from mag.edi_supplier_account_aud_hist) t
	where num_rep > 1
	order by num_rep desc, account_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- edi_packsize_pref ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.edi_packsize_pref_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct packsize_pref_id) num_dist_rows
	from mag.edi_packsize_pref_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.edi_packsize_pref_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct packsize_pref_id) num_dist_rows
	from mag.edi_packsize_pref_aud_hist

	select *
	from
		(select count(*) over(partition by packsize_pref_id) num_rep, *
		from mag.edi_packsize_pref_aud_hist) t
	where num_rep > 1
	order by num_rep desc, packsize_pref_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- edi_stock_item ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.edi_stock_item_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.edi_stock_item_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.edi_stock_item_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.edi_stock_item_aud_hist

	select *
	from
		(select count(*) over(partition by id) num_rep, *
		from mag.edi_stock_item_aud_hist) t
	where num_rep > 1
	order by num_rep desc, id, aud_dateFrom


------------------------------------------------------------------------
----------------------- directory_currency_rate ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.directory_currency_rate_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct currency_from + '' + currency_to) num_dist_rows
	from mag.directory_currency_rate_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.directory_currency_rate_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct currency_from + '' + currency_to) num_dist_rows
	from mag.directory_currency_rate_aud_hist


	select *
	from
		(select count(*) over(partition by currency_from + '' + currency_to) num_rep, *
		from mag.directory_currency_rate_aud_hist) t
	where num_rep > 1
	order by num_rep desc, currency_from + '' + currency_to, aud_dateFrom

------------------------------------------------------------------------
----------------------- salesrule ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.salesrule_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct rule_id) num_dist_rows
	from mag.salesrule_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.salesrule_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct rule_id) num_dist_rows
	from mag.salesrule_aud_hist

	select *
	from
		(select count(*) over(partition by rule_id) num_rep, *
		from mag.salesrule_aud_hist) t
	where num_rep > 1
	order by num_rep desc, rule_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- salesrule_coupon ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.salesrule_coupon_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct coupon_id) num_dist_rows
	from mag.salesrule_coupon_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.salesrule_coupon_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct coupon_id) num_dist_rows
	from mag.salesrule_coupon_aud_hist

	select *
	from
		(select count(*) over(partition by coupon_id) num_rep, *
		from mag.salesrule_coupon_aud_hist) t
	where num_rep > 1
	order by num_rep desc, coupon_id, aud_dateFrom



------------------------------------------------------------------------
----------------------- po_reorder_profile ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.po_reorder_profile_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.po_reorder_profile_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.po_reorder_profile_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.po_reorder_profile_aud_hist

	select *
	from
		(select count(*) over(partition by id) num_rep, *
		from mag.po_reorder_profile_aud_hist) t
	where num_rep > 1
	order by num_rep desc, id, aud_dateFrom

------------------------------------------------------------------------
----------------------- po_reorder_quote_payment ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.po_reorder_quote_payment_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct payment_id) num_dist_rows
	from mag.po_reorder_quote_payment_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.po_reorder_quote_payment_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct payment_id) num_dist_rows
	from mag.po_reorder_quote_payment_aud_hist

	select *
	from
		(select count(*) over(partition by payment_id) num_rep, *
		from mag.po_reorder_quote_payment_aud_hist) t
	where num_rep > 1
	order by num_rep desc, payment_id, aud_dateFrom



------------------------------------------------------------------------
----------------------- admin_user ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.admin_user_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct user_id) num_dist_rows
	from mag.admin_user_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.admin_user_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct user_id) num_dist_rows
	from mag.admin_user_aud_hist

	select *
	from
		(select count(*) over(partition by user_id) num_rep, *
		from mag.admin_user_aud_hist) t
	where num_rep > 1
	order by num_rep desc, user_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- log_customer ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.log_customer_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct log_id) num_dist_rows
	from mag.log_customer_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.log_customer_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct log_id) num_dist_rows
	from mag.log_customer_aud_hist

	select *
	from
		(select count(*) over(partition by log_id) num_rep, *
		from mag.log_customer_aud_hist) t
	where num_rep > 1
	order by num_rep desc, log_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- po_sales_pla_item ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.po_sales_pla_item_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.po_sales_pla_item_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.po_sales_pla_item_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.po_sales_pla_item_aud_hist

	select *
	from
		(select count(*) over(partition by id) num_rep, *
		from mag.po_sales_pla_item_aud_hist) t
	where num_rep > 1
	order by num_rep desc, id, aud_dateFrom




------------------------------------------------------------------------
----------------------- enterprise_customerbalance ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.enterprise_customerbalance_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct balance_id) num_dist_rows
	from mag.enterprise_customerbalance_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.enterprise_customerbalance_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct balance_id) num_dist_rows
	from mag.enterprise_customerbalance_aud_hist

	select *
	from
		(select count(*) over(partition by balance_id) num_rep, *
		from mag.enterprise_customerbalance_aud_hist) t
	where num_rep > 1
	order by num_rep desc, balance_id, aud_dateFrom




------------------------------------------------------------------------
----------------------- enterprise_customerbalance_history ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.enterprise_customerbalance_history_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct history_id) num_dist_rows
	from mag.enterprise_customerbalance_history_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.enterprise_customerbalance_history_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct history_id) num_dist_rows
	from mag.enterprise_customerbalance_history_aud_hist

	select *
	from
		(select count(*) over(partition by history_id) num_rep, *
		from mag.enterprise_customerbalance_history_aud_hist) t
	where num_rep > 1
	order by num_rep desc, history_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- cybersourcestored_token ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.cybersourcestored_token_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.cybersourcestored_token_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.cybersourcestored_token_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct id) num_dist_rows
	from mag.cybersourcestored_token_aud_hist

	select *
	from
		(select count(*) over(partition by id) num_rep, *
		from mag.cybersourcestored_token_aud_hist) t
	where num_rep > 1
	order by num_rep desc, id, aud_dateFrom


------------------------------------------------------------------------
----------------------- ga_entity_transaction_data ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.ga_entity_transaction_data_aud
	group by idETLBatchRun
	order by idETLBatchRun desc

	select count(*) num_rows, count(distinct transactionId) num_dist_rows
	from mag.ga_entity_transaction_data_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.ga_entity_transaction_data_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct transactionId) num_dist_rows
	from mag.ga_entity_transaction_data_aud_hist

	select *
	from
		(select count(*) over(partition by transactionId) num_rep, *
		from mag.ga_entity_transaction_data_aud_hist) t
	where num_rep > 1
	order by num_rep desc, transactionId, aud_dateFrom


------------------------------------------------------------------------
----------------------- mutual ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.mutual_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct mutual_id) num_dist_rows
	from mag.mutual_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.mutual_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct mutual_id) num_dist_rows
	from mag.mutual_aud_hist

	select *
	from
		(select count(*) over(partition by mutual_id) num_rep, *
		from mag.mutual_aud_hist) t
	where num_rep > 1
	order by num_rep desc, mutual_id, aud_dateFrom


------------------------------------------------------------------------
----------------------- mutual_quotation ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.mutual_quotation_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct quotation_id) num_dist_rows
	from mag.mutual_quotation_aud

	-- 

	select idETLBatchRun, count(*) num_rows
	from mag.mutual_quotation_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct quotation_id) num_dist_rows
	from mag.mutual_quotation_aud_hist

	select *
	from
		(select count(*) over(partition by quotation_id) num_rep, *
		from mag.mutual_quotation_aud_hist) t
	where num_rep > 1
	order by num_rep desc, quotation_id, aud_dateFrom