------------------------- eav_entity_attribute ----------------------------

	drop trigger mag.trg_eav_entity_attribute;
	go 

	create trigger mag.trg_eav_entity_attribute on mag.eav_entity_attribute
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_entity_attribute_aud with (tablock) as trg
		using inserted src
			on (trg.entity_attribute_id = src.entity_attribute_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, ' '), 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, ' '), 
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_attribute_id, 
					entity_type_id, attribute_id, 
					idETLBatchRun)
					
					values (src.entity_attribute_id, 
						src.entity_type_id, src.attribute_id, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_entity_attribute_aud;
	go

	create trigger mag.trg_eav_entity_attribute_aud on mag.eav_entity_attribute_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_entity_attribute_aud_hist (entity_attribute_id, 
			entity_type_id, attribute_id, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_attribute_id, 
				d.entity_type_id, d.attribute_id, 
				d.idETLBatchRun,
				case when (i.entity_type_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_attribute_id = i.entity_attribute_id;
	end;
	go
