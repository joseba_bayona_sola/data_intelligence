
use Landing
go

--------------------- Tables ----------------------------------

----------------------- sales_flat_invoice ----------------------------

	create table mag.sales_flat_invoice(
		entity_id						int NOT NULL, 
		increment_id					varchar(50),
		order_id						int,
		store_id						int,
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							int,
		total_qty						decimal(12, 4),
		base_subtotal					decimal(12, 4),
		base_shipping_amount			decimal(12, 4),
		base_discount_amount			decimal(12, 4),
		base_customer_balance_amount	decimal(12, 4),
		base_grand_total				decimal(12, 4),
		base_total_refunded				decimal(12, 4),
		mutual_amount					decimal(12, 4), 	
		base_to_global_rate				decimal(12, 4),
		base_to_order_rate				decimal(12, 4),
		order_currency_code				varchar(3), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);
	go

	alter table mag.sales_flat_invoice add constraint [PK_mag_sales_flat_invoice]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_invoice add constraint [DF_mag_sales_flat_invoice_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_invoice_aud(
		entity_id						int NOT NULL, 
		increment_id					varchar(50),
		order_id						int,
		store_id						int,
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							int,
		total_qty						decimal(12, 4),
		base_subtotal					decimal(12, 4),
		base_shipping_amount			decimal(12, 4),
		base_discount_amount			decimal(12, 4),
		base_customer_balance_amount	decimal(12, 4),
		base_grand_total				decimal(12, 4),
		base_total_refunded				decimal(12, 4),
		mutual_amount					decimal(12, 4), 
		base_to_global_rate				decimal(12, 4),
		base_to_order_rate				decimal(12, 4),
		order_currency_code				varchar(3), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		upd_ts							datetime);
	go

	alter table mag.sales_flat_invoice_aud add constraint [PK_mag_sales_flat_invoice_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_invoice_aud add constraint [DF_mag_sales_flat_invoice_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_invoice_aud_hist(
		entity_id						int NOT NULL, 
		increment_id					varchar(50),
		order_id						int,
		store_id						int,
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							int,
		total_qty						decimal(12, 4),
		base_subtotal					decimal(12, 4),
		base_shipping_amount			decimal(12, 4),
		base_discount_amount			decimal(12, 4),
		base_customer_balance_amount	decimal(12, 4),
		base_grand_total				decimal(12, 4),
		base_total_refunded				decimal(12, 4),
		mutual_amount					decimal(12, 4), 
		base_to_global_rate				decimal(12, 4),
		base_to_order_rate				decimal(12, 4),
		order_currency_code				varchar(3), 
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);
	go

	alter table mag.sales_flat_invoice_aud_hist add constraint [DF_mag_sales_flat_invoice_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 



----------------------- sales_flat_invoice_item ----------------------------

	create table mag.sales_flat_invoice_item(
		entity_id					int NOT NULL,
		order_item_id				int, 
		parent_id					int,
		product_id					int,
		sku							varchar(255),
		name						varchar(255),
		description					varchar(1000),
		qty							decimal(12, 4),
		base_price					decimal(12, 4),
		price						decimal(12, 4),
		base_price_incl_tax			decimal(12, 4),
		base_cost					decimal(12, 4),
		base_row_total				decimal(12, 4),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.sales_flat_invoice_item add constraint [PK_mag_sales_flat_invoice_item]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_invoice_item add constraint [DF_mag_sales_flat_invoice_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_invoice_item_aud(
		entity_id					int NOT NULL,
		order_item_id				int, 
		parent_id					int,
		product_id					int,
		sku							varchar(255),
		name						varchar(255),
		description					varchar(1000),
		qty							decimal(12, 4),
		base_price					decimal(12, 4),
		price						decimal(12, 4),
		base_price_incl_tax			decimal(12, 4),
		base_cost					decimal(12, 4),
		base_row_total				decimal(12, 4),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go

	alter table mag.sales_flat_invoice_item_aud add constraint [PK_mag_sales_flat_invoice_item_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_invoice_item_aud add constraint [DF_mag_sales_flat_invoice_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_invoice_item_aud_hist(
		entity_id					int NOT NULL,
		order_item_id				int, 
		parent_id					int,
		product_id					int,
		sku							varchar(255),
		name						varchar(255),
		description					varchar(1000),
		qty							decimal(12, 4),
		base_price					decimal(12, 4),
		price						decimal(12, 4),
		base_price_incl_tax			decimal(12, 4),
		base_cost					decimal(12, 4),
		base_row_total				decimal(12, 4),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL,
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go


	alter table mag.sales_flat_invoice_item_aud_hist add constraint [DF_mag_sales_flat_invoice_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


