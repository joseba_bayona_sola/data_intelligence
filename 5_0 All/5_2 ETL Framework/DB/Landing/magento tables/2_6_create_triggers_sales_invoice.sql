
use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- sales_flat_invoice ----------------------------

	drop trigger mag.trg_sales_flat_invoice;
	go 

	create trigger mag.trg_sales_flat_invoice on mag.sales_flat_invoice
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_invoice_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.increment_id, ' '), isnull(trg.order_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.created_at, ' '), -- isnull(trg.updated_at, ' '), 
				isnull(trg.shipping_address_id, 0), isnull(trg.billing_address_id, 0), 
				isnull(trg.state, 0), 
				isnull(trg.total_qty, 0), 
				isnull(trg.base_subtotal, 0), isnull(trg.base_shipping_amount, 0), isnull(trg.base_discount_amount, 0), isnull(trg.base_customer_balance_amount, 0), 
				isnull(trg.base_grand_total, 0), isnull(trg.base_total_refunded, 0), 
				isnull(trg.mutual_amount, 0), 
				isnull(trg.base_to_global_rate, 0), isnull(trg.base_to_order_rate, 0), isnull(trg.order_currency_code, ' ')
			intersect
			select 
				isnull(src.increment_id, ' '), isnull(src.order_id, 0), isnull(src.store_id, 0), 
				isnull(src.created_at, ' '), -- isnull(src.updated_at, ' '), 
				isnull(src.shipping_address_id, 0), isnull(src.billing_address_id, 0), 
				isnull(src.state, 0), 
				isnull(src.total_qty, 0), 
				isnull(src.base_subtotal, 0), isnull(src.base_shipping_amount, 0), isnull(src.base_discount_amount, 0), isnull(src.base_customer_balance_amount, 0), 
				isnull(src.base_grand_total, 0), isnull(src.base_total_refunded, 0), 
				isnull(src.mutual_amount, 0), 
				isnull(src.base_to_global_rate, 0), isnull(src.base_to_order_rate, 0), isnull(src.order_currency_code, ' '))
			
			then
				update set
					trg.increment_id = src.increment_id, trg.order_id = src.order_id, trg.store_id = src.store_id, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.shipping_address_id = src.shipping_address_id, trg.billing_address_id = src.billing_address_id, 
					trg.state = src.state, 
					trg.total_qty = src.total_qty, 
					trg.base_subtotal = src.base_subtotal, trg.base_shipping_amount = src.base_shipping_amount, trg.base_discount_amount = src.base_discount_amount, trg.base_customer_balance_amount = src.base_customer_balance_amount, 
					trg.base_grand_total = src.base_grand_total, trg.base_total_refunded = src.base_total_refunded, 
					trg.mutual_amount = src.mutual_amount, 
					trg.base_to_global_rate = src.base_to_global_rate, trg.base_to_order_rate = src.base_to_order_rate, trg.order_currency_code = src.order_currency_code, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					increment_id, order_id, store_id, created_at, updated_at, 
					shipping_address_id, billing_address_id, 
					state, 
					total_qty, 
					base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
					base_grand_total, base_total_refunded, 
					mutual_amount, 
					base_to_global_rate, base_to_order_rate, order_currency_code, 
					idETLBatchRun)
					
					values (src.entity_id, 
						src.increment_id, src.order_id, src.store_id, src.created_at, src.updated_at, 
						src.shipping_address_id, src.billing_address_id, 
						src.state, 
						src.total_qty, 
						src.base_subtotal, src.base_shipping_amount, src.base_discount_amount, src.base_customer_balance_amount, 
						src.base_grand_total, src.base_total_refunded, 
						src.mutual_amount, 
						src.base_to_global_rate, src.base_to_order_rate, src.order_currency_code, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_invoice_aud;
	go

	create trigger mag.trg_sales_flat_invoice_aud on mag.sales_flat_invoice_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_invoice_aud_hist (entity_id, 
			increment_id, order_id, store_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			state, 
			total_qty, 
			base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
			base_grand_total, base_total_refunded, 
			mutual_amount,
			base_to_global_rate, base_to_order_rate, order_currency_code, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.increment_id, d.order_id, d.store_id, d.created_at, d.updated_at, 
				d.shipping_address_id, d.billing_address_id, 
				d.state, 
				d.total_qty, 
				d.base_subtotal, d.base_shipping_amount, d.base_discount_amount, d.base_customer_balance_amount, 
				d.base_grand_total, d.base_total_refunded, 
				d.mutual_amount,
				d.base_to_global_rate, d.base_to_order_rate, d.order_currency_code, 
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

----

------------------------- sales_flat_invoice_item ----------------------------

	drop trigger mag.trg_sales_flat_invoice_item;
	go 

	create trigger mag.trg_sales_flat_invoice_item on mag.sales_flat_invoice_item
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_invoice_item_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.order_item_id, 0), isnull(trg.parent_id, 0), 
				isnull(trg.product_id, 0), isnull(trg.sku, ' '), isnull(trg.name, ' '), isnull(trg.description, ' '), 
				isnull(trg.qty, 0), 
				isnull(trg.base_price, 0), isnull(trg.price, 0), isnull(trg.base_price_incl_tax, 0), 
				isnull(trg.base_cost, 0), 
				isnull(trg.base_row_total, 0) 
			intersect
			select 
				isnull(src.order_item_id, 0), isnull(src.parent_id, 0), 
				isnull(src.product_id, 0), isnull(src.sku, ' '), isnull(src.name, ' '), isnull(src.description, ' '), 
				isnull(src.qty, 0), 
				isnull(src.base_price, 0), isnull(src.price, 0), isnull(src.base_price_incl_tax, 0), 
				isnull(src.base_cost, 0), 
				isnull(src.base_row_total, 0)) 
			
			then
				update set
					trg.order_item_id = src.order_item_id, trg.parent_id = src.parent_id, 
					trg.product_id = src.product_id, trg.sku = src.sku, trg.name = src.name, trg.description = src.description, 
					trg.qty = src.qty, 
					trg.base_price = src.base_price, trg.price = src.price, trg.base_price_incl_tax = src.base_price_incl_tax, 
					trg.base_cost = src.base_cost, 
					trg.base_row_total = src.base_row_total, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					order_item_id, parent_id, 
					product_id, sku, name, description, 
					qty, 
					base_price, price, base_price_incl_tax, 
					base_cost, 
					base_row_total,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.order_item_id, src.parent_id, 
						src.product_id, src.sku, src.name, src.description, 
						src.qty, 
						src.base_price, src.price, src.base_price_incl_tax, 
						src.base_cost, 
						src.base_row_total,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_invoice_item_aud;
	go

	create trigger mag.trg_sales_flat_invoice_item_aud on mag.sales_flat_invoice_item_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_invoice_item_aud_hist (entity_id, 
			order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			base_price, price, base_price_incl_tax, 
			base_cost, 
			base_row_total,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.order_item_id, d.parent_id, 
				d.product_id, d.sku, d.name, d.description, 
				d.qty, 
				d.base_price, d.price, d.base_price_incl_tax, 
				d.base_cost, 
				d.base_row_total,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go