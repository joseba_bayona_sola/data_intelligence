use Landing
go 

----------------------- sales_flat_shipment ----------------------------

create view mag.sales_flat_shipment_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, 
		entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		total_qty, 
		total_weight, 
		shipping_description, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			total_qty, 
			total_weight, 
			shipping_description, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.sales_flat_shipment_aud
		union
		select 'H' record_type, entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			total_qty, 
			total_weight, 
			shipping_description, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.sales_flat_shipment_aud_hist) t
go

----------------------- sales_flat_shipment_item ----------------------------

create view mag.sales_flat_shipment_item_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, 
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, 
		weight, 
		price, 
		row_total, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_id, order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			weight, 
			price, 
			row_total, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.sales_flat_shipment_item_aud
		union
		select 'H' record_type, entity_id, order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			weight, 
			price, 
			row_total, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.sales_flat_shipment_item_aud_hist) t
go

