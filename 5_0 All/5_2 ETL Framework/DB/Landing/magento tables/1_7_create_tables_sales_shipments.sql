
use Landing
go

--------------------- Tables ----------------------------------

----------------------- sales_flat_shipment ----------------------------

	create table mag.sales_flat_shipment(
		entity_id					int NOT NULL,
		increment_id				varchar(50),
		order_id					int, 
		store_id					int,
		customer_id					int,
		created_at					datetime,
		updated_at					datetime,
		shipping_address_id			int,
		billing_address_id			int,	
		total_qty					decimal(12, 4),
		total_weight				decimal(12, 4),
		shipping_description		varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.sales_flat_shipment add constraint [PK_mag_sales_flat_shipment]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_shipment add constraint [DF_mag_sales_flat_shipment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_shipment_aud(
		entity_id					int NOT NULL,
		increment_id				varchar(50),
		order_id					int, 
		store_id					int,
		customer_id					int,
		created_at					datetime,
		updated_at					datetime,
		shipping_address_id			int,
		billing_address_id			int,	
		total_qty					decimal(12, 4),
		total_weight				decimal(12, 4),
		shipping_description		varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go

	alter table mag.sales_flat_shipment_aud add constraint [PK_mag_sales_flat_shipment_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_shipment_aud add constraint [DF_mag_sales_flat_shipment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_shipment_aud_hist(
		entity_id					int NOT NULL,
		increment_id				varchar(50),
		order_id					int, 
		store_id					int,
		customer_id					int,
		created_at					datetime,
		updated_at					datetime,
		shipping_address_id			int,
		billing_address_id			int,	
		total_qty					decimal(12, 4),
		total_weight				decimal(12, 4),
		shipping_description		varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.sales_flat_shipment_aud_hist add constraint [DF_mag_sales_flat_shipment_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- sales_flat_shipment_item ----------------------------

	create table mag.sales_flat_shipment_item(
		entity_id				int NOT NULL,
		order_item_id			int,
		parent_id				int,
		product_id				int,
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty						decimal(12, 4),
		weight					decimal(12, 4),
		price					decimal(12, 4),
		row_total				decimal(12, 4), 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go
	alter table mag.sales_flat_shipment_item add constraint [PK_mag_sales_flat_shipment_item]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_shipment_item add constraint [DF_mag_sales_flat_shipment_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_shipment_item_aud(
		entity_id				int NOT NULL,
		order_item_id			int,
		parent_id				int,
		product_id				int,
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty						decimal(12, 4),
		weight					decimal(12, 4),
		price					decimal(12, 4),
		row_total				decimal(12, 4), 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		upd_ts					datetime);
	go
	alter table mag.sales_flat_shipment_item_aud add constraint [PK_mag_sales_flat_shipment_item_aud]
		primary key clustered (entity_id);
	go
	alter table mag.sales_flat_shipment_item_aud add constraint [DF_mag_sales_flat_shipment_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.sales_flat_shipment_item_aud_hist(
		entity_id				int NOT NULL,
		order_item_id			int,
		parent_id				int,
		product_id				int,
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty						decimal(12, 4),
		weight					decimal(12, 4),
		price					decimal(12, 4),
		row_total				decimal(12, 4), 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go

	alter table mag.sales_flat_shipment_item_aud_hist add constraint [DF_mag_sales_flat_shipment_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

