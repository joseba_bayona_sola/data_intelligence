use Landing
go

--------------------- Tables ----------------------------------

----------------------- customer_entity_flat ----------------------------

	create table mag.customer_entity_flat(
		entity_id					int NOT NULL, 
		increment_id				varchar(50), 
		email						varchar(255),
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL, 
		group_id					int NOT NULL, 
		website_group_id			int NOT NULL, 
		website_id					int, 
		store_id					int, 
		is_active					int NOT NULL, 
		disable_auto_group_change	int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		created_in					varchar(255), 
		prefix						varchar(255), 
		firstname					varchar(255), 
		middlename					varchar(255), 
		lastname					varchar(255), 
		suffix						varchar(255), 
		cus_phone					varchar(255), 
		dob							datetime2, 
		gender						int,
		default_billing				int, 
		default_shipping			int,
		unsubscribe_all				varchar(255), 
		unsubscribe_all_date		datetime, 
		days_worn_info				int, 
		old_access_cust_no			varchar(255), 
		old_web_cust_no				varchar(255), 
		old_customer_id				varchar(255), 
		found_us_info				varchar(255), 
		referafriend_code			varchar(255),	
		taxvat						varchar(255),	
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.customer_entity_flat add constraint [PK_mag_customer_entity_flat]
		primary key clustered (entity_id);
	go
	alter table mag.customer_entity_flat add constraint [DF_mag_customer_entity_flat_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	create table mag.customer_entity_flat_aud(
		entity_id					int NOT NULL, 
		increment_id				varchar(50), 
		email						varchar(255),
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL, 
		group_id					int NOT NULL, 
		website_group_id			int NOT NULL, 
		website_id					int, 
		store_id					int, 
		is_active					int NOT NULL, 
		disable_auto_group_change	int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		created_in					varchar(255), 
		prefix						varchar(255), 
		firstname					varchar(255), 
		middlename					varchar(255), 
		lastname					varchar(255), 
		suffix						varchar(255), 
		cus_phone					varchar(255), 
		dob							datetime2, 
		gender						int,
		default_billing				int, 
		default_shipping			int,
		unsubscribe_all				varchar(255), 
		unsubscribe_all_date		datetime, 
		days_worn_info				int, 
		old_access_cust_no			varchar(255), 
		old_web_cust_no				varchar(255), 
		old_customer_id				varchar(255), 
		found_us_info				varchar(255), 
		referafriend_code			varchar(255),	
		taxvat						varchar(255),	
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.customer_entity_flat_aud add constraint [PK_mag_customer_entity_flat_aud]
		primary key clustered (entity_id);
	go
	alter table mag.customer_entity_flat_aud add constraint [DF_mag_customer_entity_flat_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	create table mag.customer_entity_flat_aud_hist(
		entity_id					int NOT NULL, 
		increment_id				varchar(50), 
		email						varchar(255),
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL, 
		group_id					int NOT NULL, 
		website_group_id			int NOT NULL, 
		website_id					int, 
		store_id					int, 
		is_active					int NOT NULL, 
		disable_auto_group_change	int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		created_in					varchar(255), 
		prefix						varchar(255), 
		firstname					varchar(255), 
		middlename					varchar(255), 
		lastname					varchar(255), 
		suffix						varchar(255), 
		cus_phone					varchar(255), 
		dob							datetime2, 
		gender						int,
		default_billing				int, 
		default_shipping			int,
		unsubscribe_all				varchar(255), 
		unsubscribe_all_date		datetime, 
		days_worn_info				int, 
		old_access_cust_no			varchar(255), 
		old_web_cust_no				varchar(255), 
		old_customer_id				varchar(255), 
		found_us_info				varchar(255), 
		referafriend_code			varchar(255),		
		taxvat						varchar(255),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.customer_entity_flat_aud_hist add constraint [DF_mag_customer_entity_flat_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- customer_address_entity_flat ----------------------------

	create table mag.customer_address_entity_flat(
		entity_id					int NOT NULL,
		increment_id				varchar(50),  
		parent_id					int, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		is_active					int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		prefix						varchar(255), 
		firstname					varchar(255),
		middlename					varchar(255),
		lastname					varchar(255),
		suffix						varchar(255),
		company						varchar(255),
		street						varchar(1000),
		city						varchar(255),
		postcode					varchar(255),
		region_id					int, 
		region						varchar(255),
		country_id					varchar(255), 
		telephone					varchar(255),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.customer_address_entity_flat add constraint [PK_mag_customer_entity_flat_address]
		primary key clustered (entity_id);
	go
	alter table mag.customer_address_entity_flat add constraint [DF_mag_customer_entity_flat_address_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.customer_address_entity_flat_aud(
		entity_id					int NOT NULL,
		increment_id				varchar(50),  
		parent_id					int, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		is_active					int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		prefix						varchar(255), 
		firstname					varchar(255),
		middlename					varchar(255),
		lastname					varchar(255),
		suffix						varchar(255),
		company						varchar(255),
		street						varchar(1000),
		city						varchar(255),
		postcode					varchar(255),
		region_id					int, 
		region						varchar(255),
		country_id					varchar(255), 
		telephone					varchar(255),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go

	alter table mag.customer_address_entity_flat_aud add constraint [PK_mag_customer_entity_address_flat_aud]
		primary key clustered (entity_id);
	go
	alter table mag.customer_address_entity_flat_aud add constraint [DF_mag_customer_entity_address_flat_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	
	create table mag.customer_address_entity_flat_aud_hist(
		entity_id					int NOT NULL,
		increment_id				varchar(50),  
		parent_id					int, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		is_active					int NOT NULL, 
		created_at					datetime NOT NULL, 
		updated_at					datetime NOT NULL, 
		prefix						varchar(255), 
		firstname					varchar(255),
		middlename					varchar(255),
		lastname					varchar(255),
		suffix						varchar(255),
		company						varchar(255),
		street						varchar(1000),
		city						varchar(255),
		postcode					varchar(255),
		region_id					int, 
		region						varchar(255),
		country_id					varchar(255), 
		telephone					varchar(255),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.customer_address_entity_flat_aud_hist add constraint [DF_mag_customer_entity_address_flat_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

----------------------- catalog_category_entity_flat ----------------------------

	create table mag.catalog_category_entity_flat(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		is_active					int, 
		name						varchar(255), 
		url_path					varchar(255), 
		url_key						varchar(255), 
		display_mode				varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_flat add constraint [PK_mag_catalog_category_entity_flat]
		primary key clustered (entity_id, store_id);
	go
	alter table mag.catalog_category_entity_flat add constraint [DF_mag_catalog_category_entity_flat_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_flat_aud(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		is_active					int, 
		name						varchar(255), 
		url_path					varchar(255), 
		url_key						varchar(255), 
		display_mode				varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 

	alter table mag.catalog_category_entity_flat_aud add constraint [PK_mag_catalog_category_entity_flat_aud]
		primary key clustered (entity_id, store_id);
	go
	alter table mag.catalog_category_entity_flat_aud add constraint [DF_mag_catalog_category_entity_flat_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_category_entity_flat_aud_hist(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		parent_id					int NOT NULL, 
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		position					int NOT NULL, 
		level						int NOT NULL, 
		children_count				int NOT NULL, 
		path						varchar(255) NOT NULL,
		created_at					datetime, 
		updated_at					datetime, 
		is_active					int, 
		name						varchar(255), 
		url_path					varchar(255), 
		url_key						varchar(255), 
		display_mode				varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 

	alter table mag.catalog_category_entity_flat_aud_hist add constraint [DF_mag_catalog_category_entity_flat_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

----------------------- catalog_product_entity_flat ----------------------------

	create table mag.catalog_product_entity_flat(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		type_id						varchar(32) NOT NULL, 
		sku							varchar(64),
		required_options			int NOT NULL,
		has_options					int NOT NULL, 
		created_at					datetime, 
		updated_at					datetime, 
		manufacturer				int, 
		name						varchar(255),
		url_path					varchar(255),
		url_key						varchar(255),
		status						int, 
		product_type				varchar(1000),
		product_lifecycle			varchar(1000),
		promotional_product			int, 
		daysperlens					varchar(255),
		tax_class_id				int, 
		visibility					int, 
		is_lens						int, 
		stocked_lens				int, 
		telesales_only				int, 
		condition					int, 
		equivalence					varchar(255),
		equivalent_sku				varchar(255),
		price_comp_price			varchar(255),
		glasses_colour				int,
		google_shopping_gtin 		varchar(255),		
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go 


	alter table mag.catalog_product_entity_flat add constraint [PK_mag_catalog_product_entity_flat]
		primary key clustered (entity_id, store_id);
	go
	alter table mag.catalog_product_entity_flat add constraint [DF_mag_catalog_product_entity_flat_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_product_entity_flat_aud(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		type_id						varchar(32) NOT NULL, 
		sku							varchar(64),
		required_options			int NOT NULL,
		has_options					int NOT NULL, 
		created_at					datetime, 
		updated_at					datetime, 
		manufacturer				int, 
		name						varchar(255),
		url_path					varchar(255),
		url_key						varchar(255),
		status						int, 
		product_type				varchar(1000),
		product_lifecycle			varchar(1000),
		promotional_product			int, 
		daysperlens					varchar(255),
		tax_class_id				int, 
		visibility					int, 
		is_lens						int, 
		stocked_lens				int, 
		telesales_only				int, 
		condition					int, 
		equivalence					varchar(255),
		equivalent_sku				varchar(255),
		price_comp_price			varchar(255),
		glasses_colour				int,
		google_shopping_gtin 		varchar(255),		
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		upd_ts						datetime);
	go 


	alter table mag.catalog_product_entity_flat_aud add constraint [PK_mag_catalog_product_entity_flat_aud]
		primary key clustered (entity_id, store_id);
	go
	alter table mag.catalog_product_entity_flat_aud add constraint [DF_mag_catalog_product_entity_flat_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.catalog_product_entity_flat_aud_hist(
		entity_id					int NOT NULL,
		store_id					int NOT NULL,
		entity_type_id				int NOT NULL, 
		attribute_set_id			int NOT NULL,
		type_id						varchar(32) NOT NULL, 
		sku							varchar(64),
		required_options			int NOT NULL,
		has_options					int NOT NULL, 
		created_at					datetime, 
		updated_at					datetime, 
		manufacturer				int, 
		name						varchar(255),
		url_path					varchar(255),
		url_key						varchar(255),
		status						int, 
		product_type				varchar(1000),
		product_lifecycle			varchar(1000),
		promotional_product			int, 
		daysperlens					varchar(255),
		tax_class_id				int, 
		visibility					int, 
		is_lens						int, 
		stocked_lens				int, 
		telesales_only				int, 
		condition					int, 
		equivalence					varchar(255),
		equivalent_sku				varchar(255),
		price_comp_price			varchar(255),
		glasses_colour				int,
		google_shopping_gtin 		varchar(255),
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go 


	alter table mag.catalog_product_entity_flat_aud_hist add constraint [DF_mag_catalog_product_entity_flat_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


