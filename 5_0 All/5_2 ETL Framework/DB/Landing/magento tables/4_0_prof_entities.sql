
use Landing
go 

----------------------- eav_entity_type ----------------------------

	select entity_type_id, entity_type_code, 
		entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
		is_data_sharing, data_sharing_key, default_attribute_set_id, 
		increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
		additional_attribute_table, entity_attribute_collection, 
		idETLBatchRun, ins_ts
	from mag.eav_entity_type

	select entity_type_id, entity_type_code, 
		entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
		is_data_sharing, data_sharing_key, default_attribute_set_id, 
		increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
		additional_attribute_table, entity_attribute_collection, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.eav_entity_type_aud

	select entity_type_id, entity_type_code, 
		entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
		is_data_sharing, data_sharing_key, default_attribute_set_id, 
		increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
		additional_attribute_table, entity_attribute_collection, 
		idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
	from mag.eav_entity_type_aud_hist


----------------------- eav_attribute ----------------------------

	select attribute_id, entity_type_id, attribute_code, attribute_model,
		 backend_model, backend_type, backend_table, 
		 frontend_model, frontend_input, frontend_label, frontend_class,
		 source_model, 
		 is_required, is_user_defined, default_value, is_unique, note, 
		 idETLBatchRun, ins_ts
	from mag.eav_attribute

	select attribute_id, entity_type_id, attribute_code, attribute_model,
		 backend_model, backend_type, backend_table, 
		 frontend_model, frontend_input, frontend_label, frontend_class,
		 source_model, 
		 is_required, is_user_defined, default_value, is_unique, note, 
		 idETLBatchRun, ins_ts, upd_ts
	from mag.eav_attribute_aud

	select attribute_id, entity_type_id, attribute_code, attribute_model,
		 backend_model, backend_type, backend_table, 
		 frontend_model, frontend_input, frontend_label, frontend_class,
		 source_model, 
		 is_required, is_user_defined, default_value, is_unique, note, 
		 idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
	from mag.eav_attribute_aud_hist

----------------------- eav_entity_attribute ----------------------------

	select entity_attribute_id, 
		entity_type_id, 
		attribute_id, attribute_set_id, attribute_group_id, 
		sort_order, 
		idETLBatchRun, ins_ts
	from mag.eav_entity_attribute

	select entity_attribute_id, 
		entity_type_id, 
		attribute_id, attribute_set_id, attribute_group_id, 
		sort_order, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.eav_entity_attribute_aud

	select entity_attribute_id, 
		entity_type_id, 
		attribute_id, attribute_set_id, attribute_group_id, 
		sort_order, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 	
	from mag.eav_entity_attribute_aud_hist

----------------------- eav_attribute_option ----------------------------

	select option_id, 
		attribute_id, sort_order, 
		idETLBatchRun, ins_ts
	from mag.eav_attribute_option

	select option_id, 
		attribute_id, sort_order, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.eav_attribute_option_aud

	select option_id, 
		attribute_id, sort_order, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 	
	from mag.eav_attribute_option_aud_hist

----------------------- eav_attribute_option_value ----------------------------

	select value_id, 
		option_id, store_id, 
		value, 
		idETLBatchRun, ins_ts
	from mag.eav_attribute_option_value

	select value_id, 
		option_id, store_id, 
		value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.eav_attribute_option_value_aud

	select value_id, 
		option_id, store_id, 
		value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo 
	from mag.eav_attribute_option_value_aud_hist

