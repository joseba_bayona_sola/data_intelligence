
use Landing
go 

----------------------- catalog_product_entity ----------------------------

	select entity_id, 
		entity_type_id, attribute_set_id, 
		type_id, sku, required_options, has_options, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity;

	select entity_id, 
		entity_type_id, attribute_set_id, 
		type_id, sku, required_options, has_options, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_aud;

	select entity_id, 
		entity_type_id, attribute_set_id, 
		type_id, sku, required_options, has_options, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_aud_hist;


----------------------- catalog_product_entity_datetime ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_datetime

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_datetime_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_datetime_aud_hist

----------------------- catalog_product_entity_decimal ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_decimal

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_decimal_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_decimal_aud_hist

----------------------- catalog_product_entity_int ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_int

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_int_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_int_aud_hist

----------------------- catalog_product_entity_text ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_text

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_text_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_text_aud_hist

----------------------- catalog_product_entity_varchar ----------------------------

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_varchar

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_varchar_aud

	select value_id, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_varchar_aud_hist


-------------------------------------------------------------------------------

----------------------- catalog_product_entity_tier_price ----------------------------

	select value_id, 
		entity_id, customer_group_id, all_groups, website_id, 
		value, qty, promo_key, 
		idETLBatchRun, ins_ts
	from mag.catalog_product_entity_tier_price

	select value_id, 
		entity_id, customer_group_id, all_groups, website_id, 
		value, qty, promo_key, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_entity_tier_price_aud

	select value_id, 
		entity_id, customer_group_id, all_groups, website_id, 
		value, qty, promo_key, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_entity_tier_price_aud_hist

----------------------- catalog_category_product ----------------------------

	select category_id, product_id, position, 
		idETLBatchRun, ins_ts
	from mag.catalog_category_product

	select category_id, product_id, position, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_category_product_aud

	select category_id, product_id, position, 
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_category_product_aud_hist

----------------------- catalog_product_website ----------------------------

	select product_id, website_id,
		idETLBatchRun, ins_ts
	from mag.catalog_product_website

	select product_id, website_id,
		idETLBatchRun, ins_ts, upd_ts
	from mag.catalog_product_website_aud

	select product_id, website_id,
		idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
	from mag.catalog_product_website_aud_hist

