use Landing
go 

----------------------- customer_entity_flat ----------------------------

select * 
from mag.customer_entity_flat_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_address_entity_flat ----------------------------

select * 
from mag.customer_address_entity_flat_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_category_entity_flat ----------------------------

select * 
from mag.catalog_category_entity_flat_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- catalog_product_entity_flat ----------------------------

select * 
from mag.catalog_product_entity_flat_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

