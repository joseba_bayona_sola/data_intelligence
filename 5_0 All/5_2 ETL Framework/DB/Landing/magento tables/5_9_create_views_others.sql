
use Landing
go 

----------------------- core_store ----------------------------

create view mag.core_store_aud_v as
	select record_type, count(*) over (partition by store_id) num_records, 
		store_id, code, website_id, group_id, name, sort_order, is_active, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, store_id, code, website_id, group_id, name, sort_order, is_active, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.core_store_aud
		union
		select 'H' record_type, store_id, code, website_id, group_id, name, sort_order, is_active, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.core_store_aud_hist) t
go

----------------------- core_website ----------------------------

create view mag.core_website_aud_v as
	select record_type, count(*) over (partition by website_id) num_records, 
		website_id, code, name, sort_order, 
		is_default, is_staging, visibility, default_group_id, website_group_id,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, website_id, code, name, sort_order, 
			is_default, is_staging, visibility, default_group_id, website_group_id,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.core_website_aud
		union
		select 'H' record_type, website_id, code, name, sort_order, 
			is_default, is_staging, visibility, default_group_id, website_group_id,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.core_website_aud_hist) t
go

----------------------- core_config_data ----------------------------

create view mag.core_config_data_aud_v as
	select record_type, count(*) over (partition by config_id) num_records, 
		config_id, scope_id, scope, path, value,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, config_id, scope_id, scope, path, value,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.core_config_data_aud
		union
		select 'H' record_type, config_id, scope_id, scope, path, value,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.core_config_data_aud_hist) t
go



----------------------- edi_manufacturer ----------------------------

create view mag.edi_manufacturer_aud_v as
	select record_type, count(*) over (partition by manufacturer_id) num_records, 
		manufacturer_id, manufacturer_code, manufacturer_name,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, manufacturer_id, manufacturer_code, manufacturer_name,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.edi_manufacturer_aud
		union
		select 'H' record_type, manufacturer_id, manufacturer_code, manufacturer_name,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.edi_manufacturer_aud_hist) t
go

----------------------- edi_supplier_account ----------------------------

create view mag.edi_supplier_account_aud_v as
	select record_type, count(*) over (partition by account_id) num_records, 
		account_id, manufacturer_id, 
		account_ref, account_name, account_description, 
		account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, account_id, manufacturer_id, 
			account_ref, account_name, account_description, 
			account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.edi_supplier_account_aud
		union
		select 'H' record_type, account_id, manufacturer_id, 
			account_ref, account_name, account_description, 
			account_edi_format, account_edi_strategy, account_edi_cutofftime, account_edi_ref, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.edi_supplier_account_aud_hist) t
go

----------------------- edi_packsize_pref ----------------------------

create view mag.edi_packsize_pref_aud_v as
	select record_type, count(*) over (partition by packsize_pref_id) num_records, 
		packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
		stocked, cost, supply_days, supply_days_range, 
		enabled, strategy,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
			stocked, cost, supply_days, supply_days_range, 
			enabled, strategy,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.edi_packsize_pref_aud
		union
		select 'H' record_type, packsize_pref_id, packsize, product_id, priority, supplier_account_id, 
			stocked, cost, supply_days, supply_days_range, 
			enabled, strategy,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.edi_packsize_pref_aud_hist) t
go

----------------------- edi_stock_item ----------------------------

create view mag.edi_stock_item_aud_v as
	select record_type, count(*) over (partition by id) num_records, 
		id, product_id, packsize, sku, 
		product_code, pvx_product_code, stocked, 
		BC, DI, PO, CY, AX, AD, DO, CO, 
		edi_conf1, edi_conf2,
		manufacturer_id, 
		supply_days, supply_days_range, disabled,  
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, product_id, packsize, sku, 
			product_code, pvx_product_code, stocked, 
			BC, DI, PO, CY, AX, AD, DO, CO, 
			edi_conf1, edi_conf2,
			manufacturer_id, 
			supply_days, supply_days_range, disabled,  
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.edi_stock_item_aud
		union
		select 'H' record_type, id, product_id, packsize, sku, 
			product_code, pvx_product_code, stocked, 
			BC, DI, PO, CY, AX, AD, DO, CO, 
			edi_conf1, edi_conf2,
			manufacturer_id, 
			supply_days, supply_days_range, disabled,  
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.edi_stock_item_aud_hist) t
go



----------------------- directory_currency_rate ----------------------------

create view mag.directory_currency_rate_aud_v as
	select record_type, count(*) over (partition by currency_from, currency_to) num_records, 
		currency_from, currency_to, rate, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, currency_from, currency_to, rate, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.directory_currency_rate_aud
		union
		select 'H' record_type, currency_from, currency_to, rate, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.directory_currency_rate_aud_hist) t
go

----------------------- salesrule ----------------------------

create view mag.salesrule_aud_v as
	select record_type, count(*) over (partition by rule_id) num_records, 
		rule_id, 
		coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
		discount_amount, discount_qty, discount_step, 
		uses_per_customer, uses_per_coupon, 
		is_active, is_advanced, is_rss, stop_rules_processing, 
		product_ids, simple_free_shipping, apply_to_shipping, 
		referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
		times_used,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, rule_id, 
			coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
			discount_amount, discount_qty, discount_step, 
			uses_per_customer, uses_per_coupon, 
			is_active, is_advanced, is_rss, stop_rules_processing, 
			product_ids, simple_free_shipping, apply_to_shipping, 
			referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
			times_used,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.salesrule_aud
		union
		select 'H' record_type, rule_id, 
			coupon_type, channel, name, description, from_date, to_date, sort_order, simple_action, 
			discount_amount, discount_qty, discount_step, 
			uses_per_customer, uses_per_coupon, 
			is_active, is_advanced, is_rss, stop_rules_processing, 
			product_ids, simple_free_shipping, apply_to_shipping, 
			referafriend_credit, postoptics_only_new_customer, reminder_reorder_only, use_auto_generation,
			times_used,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.salesrule_aud_hist) t
go

----------------------- salesrule_coupon ----------------------------

create view mag.salesrule_coupon_aud_v as
	select record_type, count(*) over (partition by coupon_id) num_records, 
		coupon_id, rule_id, 
		type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
		is_primary, referafriend_credit,
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, coupon_id, rule_id, 
			type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
			is_primary, referafriend_credit,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.salesrule_coupon_aud
		union
		select 'H' record_type, coupon_id, rule_id, 
			type, code, usage_limit, usage_per_customer, created_at, expiration_date, 
			is_primary, referafriend_credit,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.salesrule_coupon_aud_hist) t
go


----------------------- po_reorder_profile ----------------------------

create view mag.po_reorder_profile_aud_v as
	select record_type, count(*) over (partition by id) num_records, 
		id,	
		created_at, updated_at, customer_id, name, 
		startdate, enddate, interval_days, next_order_date, 
		reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id,	
			created_at, updated_at, customer_id, name, 
			startdate, enddate, interval_days, next_order_date, 
			reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.po_reorder_profile_aud
		union
		select 'H' record_type, id,	
			created_at, updated_at, customer_id, name, 
			startdate, enddate, interval_days, next_order_date, 
			reorder_quote_id, coupon_rule, completed_profile, cache_generation_date, cached_reorder_quote_id,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.po_reorder_profile_aud_hist) t
go

----------------------- po_reorder_quote_payment ----------------------------

create view mag.po_reorder_quote_payment_aud_v as
	select record_type, count(*) over (partition by payment_id) num_records, 
		payment_id, quote_id, cc_exp_month, cc_exp_year,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, payment_id, quote_id, cc_exp_month, cc_exp_year,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.po_reorder_quote_payment_aud
		union
		select 'H' record_type, payment_id, quote_id, cc_exp_month, cc_exp_year,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.po_reorder_quote_payment_aud_hist) t
go



----------------------- admin_user ----------------------------

create view mag.admin_user_aud_v as
	select record_type, count(*) over (partition by user_id) num_records, 
		user_id, username, 
		email, firstname, lastname, lognum,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, user_id, username, 
			email, firstname, lastname, lognum,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.admin_user_aud
		union
		select 'H' record_type, user_id, username, 
			email, firstname, lastname, lognum,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.admin_user_aud_hist) t
go

----------------------- log_customer ----------------------------

create view mag.log_customer_aud_v as
	select record_type, count(*) over (partition by log_id) num_records, 
		log_id, visitor_id, customer_id, 
		login_at, logout_at, store_id,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, log_id, visitor_id, customer_id, 
			login_at, logout_at, store_id,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.log_customer_aud
		union
		select 'H' record_type, log_id, visitor_id, customer_id, 
			login_at, logout_at, store_id,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.log_customer_aud_hist) t
go


----------------------- po_sales_pla_item ----------------------------

create view mag.po_sales_pla_item_aud_v as
	select record_type, count(*) over (partition by id) num_records, 
		id, order_id, order_item_id, promo_key, price, product_id, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, order_id, order_item_id, promo_key, price, product_id, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.po_sales_pla_item_aud
		union
		select 'H' record_type, id, order_id, order_item_id, promo_key, price, product_id, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.po_sales_pla_item_aud_hist) t
go


----------------------- enterprise_customerbalance ----------------------------

create view mag.enterprise_customerbalance_aud_v as
	select record_type, count(*) over (partition by balance_id) num_records, 
		balance_id, customer_id, website_id, amount, base_currency_code,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, balance_id, customer_id, website_id, amount, base_currency_code,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.enterprise_customerbalance_aud
		union
		select 'H' record_type, balance_id, customer_id, website_id, amount, base_currency_code,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.enterprise_customerbalance_aud_hist) t
go

----------------------- enterprise_customerbalance_history ----------------------------

create view mag.enterprise_customerbalance_history_aud_v as
	select record_type, count(*) over (partition by history_id) num_records, 
		history_id, balance_id, updated_at, action, 
		balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, history_id, balance_id, updated_at, action, 
			balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.enterprise_customerbalance_history_aud
		union
		select 'H' record_type, history_id, balance_id, updated_at, action, 
			balance_amount, balance_delta, additional_info, frontend_comment, is_customer_notified, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.enterprise_customerbalance_history_aud_hist) t
go


----------------------- cybersourcestored_token ----------------------------

create view mag.cybersourcestored_token_aud_v as
	select record_type, count(*) over (partition by id) num_records, 
		id, ctype, clogo, created_at, updated_at,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, id, ctype, clogo, created_at, updated_at, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.cybersourcestored_token_aud
		union
		select 'H' record_type, id, ctype, clogo, created_at, updated_at, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.cybersourcestored_token_aud_hist) t
go


----------------------- ga_entity_transaction_data ----------------------------

create view mag.ga_entity_transaction_data_aud_v as
	select record_type, count(*) over (partition by transactionId) num_records, 
		transactionId, transaction_date, source, medium, channel,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, transactionId, transaction_date, source, medium, channel,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.ga_entity_transaction_data_aud
		union
		select 'H' record_type, transactionId, transaction_date, source, medium, channel,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.ga_entity_transaction_data_aud_hist) t
go


----------------------- mutual ----------------------------

create view mag.mutual_aud_v as
	select record_type, count(*) over (partition by mutual_id) num_records, 
		mutual_id, code, name, is_online, 
		amc_number, opening_hours, api_id, priority,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, mutual_id, code, name, is_online, 
			amc_number, opening_hours, api_id, priority,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.mutual_aud
		union
		select 'H' record_type, mutual_id, code, name, is_online, 
			amc_number, opening_hours, api_id, priority,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.mutual_aud_hist) t
go


----------------------- mutual_quotation ----------------------------

create view mag.mutual_quotation_aud_v as
	select record_type, count(*) over (partition by quotation_id) num_records, 
		quotation_id, 
		mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
		reference_number, amount, 
		expired_at, created_at, updated_at,
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, quotation_id, 
			mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
			reference_number, amount, 
			expired_at, created_at, updated_at,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from Landing.mag.mutual_quotation_aud
		union
		select 'H' record_type, quotation_id, 
			mutual_id, mutual_customer_id, type_id, is_online, status_id, error_id, 
			reference_number, amount, 
			expired_at, created_at, updated_at,
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from Landing.mag.mutual_quotation_aud_hist) t
go

