use Landing
go

--------------------- Tables ----------------------------------

----------------------- core_store ----------------------------

	create table mag.core_store(
		store_id		int NOT NULL, 
		code			varchar(32), 
		website_id		int NOT NULL, 
		group_id		int NOT NULL, 
		name			varchar(255) NOT NULL, 
		sort_order		int NOT NULL, 
		is_active		int	NOT NULL, 
		idETLBatchRun	bigint NOT NULL, 
		ins_ts			datetime NOT NULL);
	go 

	alter table mag.core_store add constraint [PK_mag_core_store]
		primary key clustered (store_id);
	go
	alter table mag.core_store add constraint [DF_mag_core_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_store_aud(
		store_id		int NOT NULL, 
		code			varchar(32), 
		website_id		int NOT NULL, 
		group_id		int NOT NULL, 
		name			varchar(255) NOT NULL, 
		sort_order		int NOT NULL, 
		is_active		int	NOT NULL, 
		idETLBatchRun	bigint NOT NULL, 
		ins_ts			datetime NOT NULL, 
		upd_ts			datetime);
	go 

	alter table mag.core_store_aud add constraint [PK_mag_core_store_aud]
		primary key clustered (store_id);
	go
	alter table mag.core_store_aud add constraint [DF_mag_core_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_store_aud_hist(
		store_id		int NOT NULL, 
		code			varchar(32), 
		website_id		int NOT NULL, 
		group_id		int NOT NULL, 
		name			varchar(255) NOT NULL, 
		sort_order		int NOT NULL, 
		is_active		int	NOT NULL, 
		idETLBatchRun	bigint NOT NULL, 
		ins_ts			datetime NOT NULL, 
		aud_type		char(1) NOT NULL, 
		aud_dateFrom	datetime NOT NULL, 
		aud_dateTo		datetime NOT NULL);
	go 

	alter table mag.core_store_aud_hist add constraint [DF_mag_core_store_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

----------------------- core_website ----------------------------

	create table mag.core_website(
		website_id			int NOT NULL, 
		code				varchar(32),
		name				varchar(64),
		sort_order			int,
		is_default			int,
		is_staging			int,
		visibility			varchar(40),
		default_group_id	int,
		website_group_id	int, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);
	go

	alter table mag.core_website add constraint [PK_mag_core_website]
		primary key clustered (website_id);
	go
	alter table mag.core_website add constraint [DF_mag_core_website_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_website_aud(
		website_id			int NOT NULL, 
		code				varchar(32),
		name				varchar(64),
		sort_order			int,
		is_default			int,
		is_staging			int,
		visibility			varchar(40),
		default_group_id	int,
		website_group_id	int, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL,
		upd_ts				datetime);
	go

	alter table mag.core_website_aud add constraint [PK_mag_core_website_aud]
		primary key clustered (website_id);
	go
	alter table mag.core_website_aud add constraint [DF_mag_core_website_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_website_aud_hist(
		website_id			int NOT NULL, 
		code				varchar(32),
		name				varchar(64),
		sort_order			int,
		is_default			int,
		is_staging			int,
		visibility			varchar(40),
		default_group_id	int,
		website_group_id	int, 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);
	go

	alter table mag.core_website_aud_hist add constraint [DF_mag_core_website_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- core_config_data ----------------------------

	create table mag.core_config_data(
		config_id			int NOT NULL,
		scope_id			int NOT NULL,
		scope				varchar(8) NOT NULL,
		path				varchar(255) NOT NULL,
		value				varchar(1000), 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);
	go

	alter table mag.core_config_data add constraint [PK_mag_core_config_data]
		primary key clustered (config_id);
	go
	alter table mag.core_config_data add constraint [DF_mag_core_config_data_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_config_data_aud(
		config_id			int NOT NULL,
		scope_id			int NOT NULL,
		scope				varchar(8) NOT NULL,
		path				varchar(255) NOT NULL,
		value				varchar(1000), 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL,
		upd_ts				datetime);
	go

	alter table mag.core_config_data_aud add constraint [PK_mag_core_config_data_aud]
		primary key clustered (config_id);
	go
	alter table mag.core_config_data_aud add constraint [DF_mag_core_config_data_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.core_config_data_aud_hist(
		config_id			int NOT NULL,
		scope_id			int NOT NULL,
		scope				varchar(8) NOT NULL,
		path				varchar(255) NOT NULL,
		value				varchar(1000), 
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);
	go

	alter table mag.core_config_data_aud_hist add constraint [DF_mag_core_config_data_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- edi_manufacturer ----------------------------

	create table mag.edi_manufacturer(
		manufacturer_id				int NOT NULL, 
		manufacturer_code			varchar(255),
		manufacturer_name			varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.edi_manufacturer add constraint [PK_mag_edi_manufacturer]
		primary key clustered (manufacturer_id);
	go
	alter table mag.edi_manufacturer add constraint [DF_mag_edi_manufacturer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_manufacturer_aud(
		manufacturer_id				int NOT NULL, 
		manufacturer_code			varchar(255),
		manufacturer_name			varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL,
		upd_ts						datetime);
	go

	alter table mag.edi_manufacturer_aud add constraint [PK_mag_edi_manufacturer_aud]
		primary key clustered (manufacturer_id);
	go
	alter table mag.edi_manufacturer_aud add constraint [DF_mag_edi_manufacturer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_manufacturer_aud_hist(
		manufacturer_id				int NOT NULL, 
		manufacturer_code			varchar(255),
		manufacturer_name			varchar(255), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.edi_manufacturer_aud_hist add constraint [DF_mag_edi_manufacturer_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

----------------------- edi_supplier_account ----------------------------

	create table mag.edi_supplier_account(
		account_id					int NOT NULL,
		manufacturer_id				int NOT NULL,
		account_ref					varchar(45),
		account_name				varchar(255),
		account_description			varchar(1000),
		account_edi_format			varchar(45),
		account_edi_strategy		varchar(45),
		account_edi_cutofftime		time,
		account_edi_ref				varchar(80), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.edi_supplier_account add constraint [PK_mag_edi_supplier_account]
		primary key clustered (account_id);
	go
	alter table mag.edi_supplier_account add constraint [DF_mag_edi_supplier_account_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_supplier_account_aud(
		account_id					int NOT NULL,
		manufacturer_id				int NOT NULL,
		account_ref					varchar(45),
		account_name				varchar(255),
		account_description			varchar(1000),
		account_edi_format			varchar(45),
		account_edi_strategy		varchar(45),
		account_edi_cutofftime		time,
		account_edi_ref				varchar(80), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL,
		upd_ts						datetime);
	go

	alter table mag.edi_supplier_account_aud add constraint [PK_mag_edi_supplier_account_aud]
		primary key clustered (account_id);
	go
	alter table mag.edi_supplier_account_aud add constraint [DF_mag_edi_supplier_account_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_supplier_account_aud_hist(
		account_id					int NOT NULL,
		manufacturer_id				int NOT NULL,
		account_ref					varchar(45),
		account_name				varchar(255),
		account_description			varchar(1000),
		account_edi_format			varchar(45),
		account_edi_strategy		varchar(45),
		account_edi_cutofftime		time,
		account_edi_ref				varchar(80), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.edi_supplier_account_aud_hist add constraint [DF_mag_edi_supplier_account_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- edi_packsize_pref ----------------------------

	create table mag.edi_packsize_pref(
		packsize_pref_id			int NOT NULL, 
		packsize					int,
		product_id					int,
		priority					int,
		supplier_account_id			int NOT NULL,
		stocked						varchar(45) NOT NULL,
		cost						decimal(12, 4),
		supply_days					int NOT NULL,
		supply_days_range			int NOT NULL,
		enabled						int NOT NULL,
		strategy					varchar(20), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);
	go

	alter table mag.edi_packsize_pref add constraint [PK_mag_edi_packsize_pref]
		primary key clustered (packsize_pref_id);
	go
	alter table mag.edi_packsize_pref add constraint [DF_mag_edi_packsize_pref_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_packsize_pref_aud(
		packsize_pref_id			int NOT NULL, 
		packsize					int,
		product_id					int,
		priority					int,
		supplier_account_id			int NOT NULL,
		stocked						varchar(45) NOT NULL,
		cost						decimal(12, 4),
		supply_days					int NOT NULL,
		supply_days_range			int NOT NULL,
		enabled						int NOT NULL,
		strategy					varchar(20), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL,
		upd_ts						datetime);
	go

	alter table mag.edi_packsize_pref_aud add constraint [PK_mag_edi_packsize_pref_aud]
		primary key clustered (packsize_pref_id);
	go
	alter table mag.edi_packsize_pref_aud add constraint [DF_mag_edi_packsize_pref_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_packsize_pref_aud_hist(
		packsize_pref_id			int NOT NULL, 
		packsize					int,
		product_id					int,
		priority					int,
		supplier_account_id			int NOT NULL,
		stocked						varchar(45) NOT NULL,
		cost						decimal(12, 4),
		supply_days					int NOT NULL,
		supply_days_range			int NOT NULL,
		enabled						int NOT NULL,
		strategy					varchar(20), 
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL, 
		aud_type					char(1) NOT NULL, 
		aud_dateFrom				datetime NOT NULL, 
		aud_dateTo					datetime NOT NULL);
	go

	alter table mag.edi_packsize_pref_aud_hist add constraint [DF_mag_edi_packsize_pref_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- edi_stock_item ----------------------------

	create table mag.edi_stock_item(
		id						int NOT NULL,
		product_id				int NOT NULL,
		packsize				varchar(45) NOT NULL,
		sku						varchar(45),
		product_code			varchar(277) NOT NULL,
		pvx_product_code		varchar(277) NOT NULL,
		stocked					varchar(45),
		BC						varchar(100),
		DI						varchar(100),
		PO						varchar(100),
		CY						varchar(100),
		AX						varchar(100),
		AD						varchar(100),
		DO						varchar(100),
		CO						varchar(100),
		edi_conf1				varchar(100),
		edi_conf2				varchar(100),
		manufacturer_id			int NOT NULL,
		supply_days				int NOT NULL,
		supply_days_range		int NOT NULL,
		disabled				int, 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go

	alter table mag.edi_stock_item add constraint [PK_mag_edi_stock_item]
		primary key clustered (id);
	go
	alter table mag.edi_stock_item add constraint [DF_mag_edi_stock_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_stock_item_aud(
		id						int NOT NULL,
		product_id				int NOT NULL,
		packsize				varchar(45) NOT NULL,
		sku						varchar(45),
		product_code			varchar(277) NOT NULL,
		pvx_product_code		varchar(277) NOT NULL,
		stocked					varchar(45),
		BC						varchar(100),
		DI						varchar(100),
		PO						varchar(100),
		CY						varchar(100),
		AX						varchar(100),
		AD						varchar(100),
		DO						varchar(100),
		CO						varchar(100),
		edi_conf1				varchar(100),
		edi_conf2				varchar(100),
		manufacturer_id			int NOT NULL,
		supply_days				int NOT NULL,
		supply_days_range		int NOT NULL,
		disabled				int, 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);
	go

	alter table mag.edi_stock_item_aud add constraint [PK_mag_edi_stock_item_aud]
		primary key clustered (id);
	go
	alter table mag.edi_stock_item_aud add constraint [DF_mag_edi_stock_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.edi_stock_item_aud_hist(
		id						int NOT NULL,
		product_id				int NOT NULL,
		packsize				varchar(45) NOT NULL,
		sku						varchar(45),
		product_code			varchar(277) NOT NULL,
		pvx_product_code		varchar(277) NOT NULL,
		stocked					varchar(45),
		BC						varchar(100),
		DI						varchar(100),
		PO						varchar(100),
		CY						varchar(100),
		AX						varchar(100),
		AD						varchar(100),
		DO						varchar(100),
		CO						varchar(100),
		edi_conf1				varchar(100),
		edi_conf2				varchar(100),
		manufacturer_id			int NOT NULL,
		supply_days				int NOT NULL,
		supply_days_range		int NOT NULL,
		disabled				int, 
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go

	alter table mag.edi_stock_item_aud_hist add constraint [DF_mag_edi_stock_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- directory_currency_rate ----------------------------

	create table mag.directory_currency_rate(
		currency_from		varchar(3) NOT NULL,
		currency_to			varchar(3) NOT NULL,
		rate				decimal(24, 12),
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);
	go

	alter table mag.directory_currency_rate add constraint [PK_mag_directory_currency_rate]
		primary key clustered (currency_from, currency_to);
	go
	alter table mag.directory_currency_rate add constraint [DF_mag_directory_currency_rate_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.directory_currency_rate_aud(
		currency_from		varchar(3) NOT NULL,
		currency_to			varchar(3) NOT NULL,
		rate				decimal(24, 12),
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL,
		upd_ts				datetime);
	go

	alter table mag.directory_currency_rate_aud add constraint [PK_mag_directory_currency_rate_aud]
		primary key clustered (currency_from, currency_to);
	go
	alter table mag.directory_currency_rate_aud add constraint [DF_mag_directory_currency_rate_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.directory_currency_rate_aud_hist(
		currency_from		varchar(3) NOT NULL,
		currency_to			varchar(3) NOT NULL,
		rate				decimal(24, 12),
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);
	go

	alter table mag.directory_currency_rate_aud_hist add constraint [DF_mag_directory_currency_rate_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- salesrule ----------------------------

	create table mag.salesrule(
		rule_id							int NOT NULL,
		coupon_type						int NOT NULL,
		channel							varchar(255),
		name							varchar(255),
		description						varchar(1000),
		from_date						date, 
		to_date							date,
		sort_order						int NOT NULL,
		simple_action					varchar(32), 
		discount_amount					decimal(12, 4) NOT NULL,
		discount_qty					decimal(12, 4),
		discount_step					int NOT NULL,
		uses_per_customer				int NOT NULL,
		uses_per_coupon					int NOT NULL,
		is_active						int NOT NULL,
		is_advanced						int NOT NULL,
		is_rss							int NOT NULL,
		stop_rules_processing			int NOT NULL,
		product_ids						varchar(1000), 
		simple_free_shipping			int NOT NULL,
		apply_to_shipping				int NOT NULL,
		referafriend_credit				decimal(12, 4),
		postoptics_only_new_customer	int, 
		reminder_reorder_only			int,
		use_auto_generation				int NOT NULL,
		times_used						int NOT NULL,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);
	go	
	

	alter table mag.salesrule add constraint [PK_mag_salesrule]
		primary key clustered (rule_id);
	go
	alter table mag.salesrule add constraint [DF_mag_salesrule_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.salesrule_aud(
		rule_id							int NOT NULL,
		coupon_type						int NOT NULL,
		channel							varchar(255),
		name							varchar(255),
		description						varchar(1000),
		from_date						date, 
		to_date							date,
		sort_order						int NOT NULL,
		simple_action					varchar(32), 
		discount_amount					decimal(12, 4) NOT NULL,
		discount_qty					decimal(12, 4),
		discount_step					int NOT NULL,
		uses_per_customer				int NOT NULL,
		uses_per_coupon					int NOT NULL,
		is_active						int NOT NULL,
		is_advanced						int NOT NULL,
		is_rss							int NOT NULL,
		stop_rules_processing			int NOT NULL,
		product_ids						varchar(1000), 
		simple_free_shipping			int NOT NULL,
		apply_to_shipping				int NOT NULL,
		referafriend_credit				decimal(12, 4),
		postoptics_only_new_customer	int, 
		reminder_reorder_only			int,
		use_auto_generation				int NOT NULL,
		times_used						int NOT NULL,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		upd_ts							datetime);
	go	
	

	alter table mag.salesrule_aud add constraint [PK_mag_salesrule_aud]
		primary key clustered (rule_id);
	go
	alter table mag.salesrule_aud add constraint [DF_mag_salesrule_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.salesrule_aud_hist(
		rule_id							int NOT NULL,
		coupon_type						int NOT NULL,
		channel							varchar(255),
		name							varchar(255),
		description						varchar(1000),
		from_date						date, 
		to_date							date,
		sort_order						int NOT NULL,
		simple_action					varchar(32), 
		discount_amount					decimal(12, 4) NOT NULL,
		discount_qty					decimal(12, 4),
		discount_step					int NOT NULL,
		uses_per_customer				int NOT NULL,
		uses_per_coupon					int NOT NULL,
		is_active						int NOT NULL,
		is_advanced						int NOT NULL,
		is_rss							int NOT NULL,
		stop_rules_processing			int NOT NULL,
		product_ids						varchar(1000), 
		simple_free_shipping			int NOT NULL,
		apply_to_shipping				int NOT NULL,
		referafriend_credit				decimal(12, 4),
		postoptics_only_new_customer	int, 
		reminder_reorder_only			int,
		use_auto_generation				int NOT NULL,
		times_used						int NOT NULL,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);
	go	
	

	alter table mag.salesrule_aud_hist add constraint [DF_mag_salesrule_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- salesrule_coupon ----------------------------

	create table mag.salesrule_coupon(
		coupon_id				int NOT NULL,
		rule_id					int NOT NULL,
		type					int,
		code					varchar(255),
		usage_limit				int,
		usage_per_customer		int,
		created_at				datetime,
		expiration_date			datetime,
		is_primary				int,
		referafriend_credit		decimal(12, 4),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);

	alter table mag.salesrule_coupon add constraint [PK_mag_salesrule_coupon]
		primary key clustered (coupon_id);
	go
	alter table mag.salesrule_coupon add constraint [DF_mag_salesrule_coupon_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.salesrule_coupon_aud(
		coupon_id				int NOT NULL,
		rule_id					int NOT NULL,
		type					int,
		code					varchar(255),
		usage_limit				int,
		usage_per_customer		int,
		created_at				datetime,
		expiration_date			datetime,
		is_primary				int,
		referafriend_credit		decimal(12, 4),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);

	alter table mag.salesrule_coupon_aud add constraint [PK_mag_salesrule_coupon_aud]
		primary key clustered (coupon_id);
	go
	alter table mag.salesrule_coupon_aud add constraint [DF_mag_salesrule_coupon_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.salesrule_coupon_aud_hist(
		coupon_id				int NOT NULL,
		rule_id					int NOT NULL,
		type					int,
		code					varchar(255),
		usage_limit				int,
		usage_per_customer		int,
		created_at				datetime,
		expiration_date			datetime,
		is_primary				int,
		referafriend_credit		decimal(12, 4),
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);

	alter table mag.salesrule_coupon_aud_hist add constraint [DF_mag_salesrule_coupon_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- po_reorder_profile ----------------------------

	create table mag.po_reorder_profile(
		id								int NOT NULL,
		created_at						datetime NOT NULL,
		updated_at						datetime NOT NULL,
		customer_id						int NOT NULL,
		name							varchar(255) NOT NULL,
		startdate						datetime NOT NULL,
		enddate							datetime,
		interval_days					int NOT NULL,
		next_order_date					datetime,
		reorder_quote_id				int NOT NULL,
		coupon_rule						int,
		completed_profile				int,
		cache_generation_date			datetime,
		cached_reorder_quote_id			int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL);

	alter table mag.po_reorder_profile add constraint [PK_mag_po_reorder_profile]
		primary key clustered (id);
	go
	alter table mag.po_reorder_profile add constraint [DF_mag_po_reorder_profile_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.po_reorder_profile_aud(
		id								int NOT NULL,
		created_at						datetime NOT NULL,
		updated_at						datetime NOT NULL,
		customer_id						int NOT NULL,
		name							varchar(255) NOT NULL,
		startdate						datetime NOT NULL,
		enddate							datetime,
		interval_days					int NOT NULL,
		next_order_date					datetime,
		reorder_quote_id				int NOT NULL,
		coupon_rule						int,
		completed_profile				int,
		cache_generation_date			datetime,
		cached_reorder_quote_id			int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL,
		upd_ts							datetime);

	alter table mag.po_reorder_profile_aud add constraint [PK_mag_po_reorder_profile_aud]
		primary key clustered (id);
	go
	alter table mag.po_reorder_profile_aud add constraint [DF_mag_po_reorder_profile_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.po_reorder_profile_aud_hist(
		id								int NOT NULL,
		created_at						datetime NOT NULL,
		updated_at						datetime NOT NULL,
		customer_id						int NOT NULL,
		name							varchar(255) NOT NULL,
		startdate						datetime NOT NULL,
		enddate							datetime,
		interval_days					int NOT NULL,
		next_order_date					datetime,
		reorder_quote_id				int NOT NULL,
		coupon_rule						int,
		completed_profile				int,
		cache_generation_date			datetime,
		cached_reorder_quote_id			int,
		idETLBatchRun					bigint NOT NULL, 
		ins_ts							datetime NOT NULL, 
		aud_type						char(1) NOT NULL, 
		aud_dateFrom					datetime NOT NULL, 
		aud_dateTo						datetime NOT NULL);

	alter table mag.po_reorder_profile_aud_hist add constraint [DF_mag_po_reorder_profile_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


	/*
	create table mag.po_reorder_quote(
		entity_id					int NOT NULL,
		idETLBatchRun				bigint NOT NULL, 
		ins_ts						datetime NOT NULL);

	alter table mag.po_reorder_quote add constraint [PK_mag_po_reorder_quote]
		primary key clustered (entity_id);
	go

	alter table mag.po_reorder_quote add constraint [DF_mag_po_reorder_quote_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	*/

----------------------- po_reorder_quote_payment ----------------------------

	create table mag.po_reorder_quote_payment(
		payment_id				int NOT NULL,
		quote_id				int NOT NULL,
		cc_exp_month			int,
		cc_exp_year				int,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL);
	go

	alter table mag.po_reorder_quote_payment add constraint [PK_mag_po_reorder_quote_payment]
		primary key clustered (payment_id);
	go
	alter table mag.po_reorder_quote_payment add constraint [DF_mag_po_reorder_quote_payment_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.po_reorder_quote_payment_aud(
		payment_id				int NOT NULL,
		quote_id				int NOT NULL,
		cc_exp_month			int,
		cc_exp_year				int,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL,
		upd_ts					datetime);
	go

	alter table mag.po_reorder_quote_payment_aud add constraint [PK_mag_po_reorder_quote_payment_aud]
		primary key clustered (payment_id);
	go
	alter table mag.po_reorder_quote_payment_aud add constraint [DF_mag_po_reorder_quote_payment_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.po_reorder_quote_payment_aud_hist(
		payment_id				int NOT NULL,
		quote_id				int NOT NULL,
		cc_exp_month			int,
		cc_exp_year				int,
		idETLBatchRun			bigint NOT NULL, 
		ins_ts					datetime NOT NULL, 
		aud_type				char(1) NOT NULL, 
		aud_dateFrom			datetime NOT NULL, 
		aud_dateTo				datetime NOT NULL);
	go

	alter table mag.po_reorder_quote_payment_aud_hist add constraint [DF_mag_po_reorder_quote_payment_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 



----------------------- admin_user ----------------------------

	create table mag.admin_user(
		user_id				int NOT NULL,
		username			varchar(40),
		email				varchar(128),
		firstname			varchar(32),
		lastname			varchar(32),
		lognum				int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);
	go

	alter table mag.admin_user add constraint [PK_mag_admin_user]
		primary key clustered (user_id);
	go
	alter table mag.admin_user add constraint [DF_mag_admin_user_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.admin_user_aud(
		user_id				int NOT NULL,
		username			varchar(40),
		email				varchar(128),
		firstname			varchar(32),
		lastname			varchar(32),
		lognum				int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL,
		upd_ts				datetime);
	go

	alter table mag.admin_user_aud add constraint [PK_mag_admin_user_aud]
		primary key clustered (user_id);
	go
	alter table mag.admin_user_aud add constraint [DF_mag_admin_user_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.admin_user_aud_hist(
		user_id				int NOT NULL,
		username			varchar(40),
		email				varchar(128),
		firstname			varchar(32),
		lastname			varchar(32),
		lognum				int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);
	go

	alter table mag.admin_user_aud_hist add constraint [DF_mag_admin_user_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 



----------------------- log_customer ----------------------------

	create table mag.log_customer(
		log_id				int NOT NULL,
		visitor_id			bigint,
		customer_id			int NOT NULL,
		login_at			datetime NOT NULL,
		logout_at			datetime,
		store_id			int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL);

	go

	alter table mag.log_customer add constraint [PK_mag_log_customer]
		primary key clustered (log_id);
	go
	alter table mag.log_customer add constraint [DF_mag_log_customer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.log_customer_aud(
		log_id				int NOT NULL,
		visitor_id			bigint,
		customer_id			int NOT NULL,
		login_at			datetime NOT NULL,
		logout_at			datetime,
		store_id			int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL,
		upd_ts				datetime);

	go

	alter table mag.log_customer_aud add constraint [PK_mag_log_customer_aud]
		primary key clustered (log_id);
	go
	alter table mag.log_customer_aud add constraint [DF_mag_log_customer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 

	create table mag.log_customer_aud_hist(
		log_id				int NOT NULL,
		visitor_id			bigint,
		customer_id			int NOT NULL,
		login_at			datetime NOT NULL,
		logout_at			datetime,
		store_id			int NOT NULL,
		idETLBatchRun		bigint NOT NULL, 
		ins_ts				datetime NOT NULL, 
		aud_type			char(1) NOT NULL, 
		aud_dateFrom		datetime NOT NULL, 
		aud_dateTo			datetime NOT NULL);

	go

	alter table mag.log_customer_aud_hist add constraint [DF_mag_log_customer_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
	go 


----------------------- po_sales_pla_item ----------------------------


	create table mag.po_sales_pla_item(
	id								int NOT NULL,
	order_id						int NOT NULL,
	order_item_id					int NOT NULL,
	promo_key						varchar(40),
	price							decimal(16,8),
	product_id						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mag.po_sales_pla_item add constraint [PK_mag_po_sales_pla_item]
	primary key clustered (id);
go
alter table mag.po_sales_pla_item add constraint [DF_mag_po_sales_pla_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	
	
create table mag.po_sales_pla_item_aud(
	id								int NOT NULL,
	order_id						int NOT NULL,
	order_item_id					int NOT NULL,
	promo_key						varchar(40),
	price							decimal(16,8),
	product_id						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mag.po_sales_pla_item_aud add constraint [PK_mag_po_sales_pla_item_aud]
	primary key clustered (id);
go
alter table mag.po_sales_pla_item_aud add constraint [DF_mag_po_sales_pla_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	

create table mag.po_sales_pla_item_aud_hist(
	id								int NOT NULL,
	order_id						int NOT NULL,
	order_item_id					int NOT NULL,
	promo_key						varchar(40),
	price							decimal(16,8),
	product_id						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mag.po_sales_pla_item_aud_hist add constraint [DF_mag_po_sales_pla_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


----------------------- enterprise_customerbalance ----------------------------


create table mag.enterprise_customerbalance(
	balance_id						int NOT NULL,
	customer_id						int NOT NULL,
	website_id						int,
	amount							decimal(12,4) NOT NULL,
	base_currency_code				varchar(3), 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mag.enterprise_customerbalance add constraint [PK_mag_enterprise_customerbalance]
	primary key clustered (balance_id);
go
alter table mag.enterprise_customerbalance add constraint [DF_mag_enterprise_customerbalance_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	
	
create table mag.enterprise_customerbalance_aud(
	balance_id						int NOT NULL,
	customer_id						int NOT NULL,
	website_id						int,
	amount							decimal(12,4) NOT NULL,
	base_currency_code				varchar(3), 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mag.enterprise_customerbalance_aud add constraint [PK_mag_enterprise_customerbalance_aud]
	primary key clustered (balance_id);
go
alter table mag.enterprise_customerbalance_aud add constraint [DF_mag_enterprise_customerbalance_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	

create table mag.enterprise_customerbalance_aud_hist(
	balance_id						int NOT NULL,
	customer_id						int NOT NULL,
	website_id						int,
	amount							decimal(12,4) NOT NULL,
	base_currency_code				varchar(3), 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mag.enterprise_customerbalance_aud_hist add constraint [DF_mag_enterprise_customerbalance_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



----------------------- enterprise_customerbalance_history ----------------------------


create table mag.enterprise_customerbalance_history(
	history_id						int NOT NULL, 
	balance_id						int NOT NULL, 
	updated_at						datetime,
	action							int NOT NULL, 
	balance_amount					decimal(12,4) NOT NULL, 
	balance_delta					decimal(12,4) NOT NULL,
	additional_info					varchar(255), 
	frontend_comment				varchar(200), 
	is_customer_notified			int NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mag.enterprise_customerbalance_history add constraint [PK_mag_enterprise_customerbalance_history]
	primary key clustered (history_id);
go
alter table mag.enterprise_customerbalance_history add constraint [DF_mag_enterprise_customerbalance_history_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	
	
create table mag.enterprise_customerbalance_history_aud(
	history_id						int NOT NULL, 
	balance_id						int NOT NULL, 
	updated_at						datetime,
	action							int NOT NULL, 
	balance_amount					decimal(12,4) NOT NULL, 
	balance_delta					decimal(12,4) NOT NULL,
	additional_info					varchar(255), 
	frontend_comment				varchar(200), 
	is_customer_notified			int NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mag.enterprise_customerbalance_history_aud add constraint [PK_mag_enterprise_customerbalance_history_aud]
	primary key clustered (history_id);
go
alter table mag.enterprise_customerbalance_history_aud add constraint [DF_mag_enterprise_customerbalance_history_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	

create table mag.enterprise_customerbalance_history_aud_hist(
	history_id						int NOT NULL, 
	balance_id						int NOT NULL, 
	updated_at						datetime,
	action							int NOT NULL, 
	balance_amount					decimal(12,4) NOT NULL, 
	balance_delta					decimal(12,4) NOT NULL,
	additional_info					varchar(255), 
	frontend_comment				varchar(200), 
	is_customer_notified			int NOT NULL, 
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mag.enterprise_customerbalance_history_aud_hist add constraint [DF_mag_enterprise_customerbalance_history_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



----------------------- cybersourcestored_token ----------------------------


create table mag.cybersourcestored_token(
	id								int NOT NULL, 
	ctype							varchar(10),
	clogo							varchar(4),
	created_at						datetime,
	updated_at						datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mag.cybersourcestored_token add constraint [PK_mag_cybersourcestored_token]
	primary key clustered (id);
go
alter table mag.cybersourcestored_token add constraint [DF_mag_cybersourcestored_token_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	
	
create table mag.cybersourcestored_token_aud(
	id								int NOT NULL, 
	ctype							varchar(10),
	clogo							varchar(4),
	created_at						datetime,
	updated_at						datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mag.cybersourcestored_token_aud add constraint [PK_mag_cybersourcestored_token_aud]
	primary key clustered (id);
go
alter table mag.cybersourcestored_token_aud add constraint [DF_mag_cybersourcestored_token_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	

create table mag.cybersourcestored_token_aud_hist(
	id								int NOT NULL, 
	ctype							varchar(10),
	clogo							varchar(4),
	created_at						datetime,
	updated_at						datetime,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mag.cybersourcestored_token_aud_hist add constraint [DF_mag_cybersourcestored_token_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




----------------------- ga_entity_transaction_data ----------------------------


create table mag.ga_entity_transaction_data(
	transactionId					varchar(20) NOT NULL,
	transaction_date				int,
	source							varchar(100),
	medium							varchar(100),
	channel							varchar(100),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table mag.ga_entity_transaction_data add constraint [PK_mag_ga_entity_transaction_data]
	primary key clustered (transactionId);
go
alter table mag.ga_entity_transaction_data add constraint [DF_mag_ga_entity_transaction_data_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	
	
create table mag.ga_entity_transaction_data_aud(
	transactionId					varchar(20) NOT NULL,
	transaction_date				int,
	source							varchar(100),
	medium							varchar(100),
	channel							varchar(100),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go 

alter table mag.ga_entity_transaction_data_aud add constraint [PK_mag_ga_entity_transaction_data_aud]
	primary key clustered (transactionId);
go
alter table mag.ga_entity_transaction_data_aud add constraint [DF_mag_ga_entity_transaction_data_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go
	

create table mag.ga_entity_transaction_data_aud_hist(
	transactionId					varchar(20) NOT NULL,
	transaction_date				int,
	source							varchar(100),
	medium							varchar(100),
	channel							varchar(100),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go 

alter table mag.ga_entity_transaction_data_aud_hist add constraint [DF_mag_ga_entity_transaction_data_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go



----------------------- mutual ----------------------------

create table mag.mutual(
	mutual_id				int NOT NULL,
	code					varchar(200) NOT NULL,
	name					varchar(200) NOT NULL,
	is_online				int NOT NULL,
	amc_number				varchar(100),
	opening_hours			varchar(255),
	api_id					int,
	priority				int	NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go 

alter table mag.mutual add constraint [PK_mag_mutual]
	primary key clustered (mutual_id);
go
alter table mag.mutual add constraint [DF_mag_mutual_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mag.mutual_aud(
	mutual_id				int NOT NULL,
	code					varchar(200) NOT NULL,
	name					varchar(200) NOT NULL,
	is_online				int NOT NULL,
	amc_number				varchar(100),
	opening_hours			varchar(255),
	api_id					int,
	priority				int	NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go 

alter table mag.mutual_aud add constraint [PK_mag_mutual_aud]
	primary key clustered (mutual_id);
go
alter table mag.mutual_aud add constraint [DF_mag_mutual_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mag.mutual_aud_hist(
	mutual_id				int NOT NULL,
	code					varchar(200) NOT NULL,
	name					varchar(200) NOT NULL,
	is_online				int NOT NULL,
	amc_number				varchar(100),
	opening_hours			varchar(255),
	api_id					int,
	priority				int	NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	aud_type				char(1) NOT NULL, 
	aud_dateFrom			datetime NOT NULL, 
	aud_dateTo				datetime NOT NULL);
go 

alter table mag.mutual_aud_hist add constraint [DF_mag_mutual_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




----------------------- mutual_quotation ----------------------------

create table mag.mutual_quotation(
	quotation_id			int NOT NULL, 
	mutual_id				int NOT NULL, 
	mutual_customer_id		int NOT NULL, 
	type_id					int NOT NULL, 
	is_online				int NOT NULL, 
	status_id				int NOT NULL, 
	error_id				int NOT NULL, 
	reference_number		varchar(100),
	amount					decimal(12, 4) NOT NULL, 
	expired_at				datetime, 
	created_at				datetime NOT NULL,
	updated_at				datetime NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL);
go 

alter table mag.mutual_quotation add constraint [PK_mag_mutual_quotation]
	primary key clustered (quotation_id);
go
alter table mag.mutual_quotation add constraint [DF_mag_mutual_quotation_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mag.mutual_quotation_aud(
	quotation_id			int NOT NULL, 
	mutual_id				int NOT NULL, 
	mutual_customer_id		int NOT NULL, 
	type_id					int NOT NULL, 
	is_online				int NOT NULL, 
	status_id				int NOT NULL, 
	error_id				int NOT NULL, 
	reference_number		varchar(100),
	amount					decimal(12, 4) NOT NULL, 
	expired_at				datetime, 
	created_at				datetime NOT NULL,
	updated_at				datetime NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	upd_ts					datetime);
go 

alter table mag.mutual_quotation_aud add constraint [PK_mag_mutual_quotation_aud]
	primary key clustered (quotation_id);
go
alter table mag.mutual_quotation_aud add constraint [DF_mag_mutual_quotation_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go


create table mag.mutual_quotation_aud_hist(
	quotation_id			int NOT NULL, 
	mutual_id				int NOT NULL, 
	mutual_customer_id		int NOT NULL, 
	type_id					int NOT NULL, 
	is_online				int NOT NULL, 
	status_id				int NOT NULL, 
	error_id				int NOT NULL, 
	reference_number		varchar(100),
	amount					decimal(12, 4) NOT NULL, 
	expired_at				datetime, 
	created_at				datetime NOT NULL,
	updated_at				datetime NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL,
	aud_type				char(1) NOT NULL, 
	aud_dateFrom			datetime NOT NULL, 
	aud_dateTo				datetime NOT NULL);
go 

alter table mag.mutual_quotation_aud_hist add constraint [DF_mag_mutual_quotation_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go

