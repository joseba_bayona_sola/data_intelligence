use Landing
go 

----------------------- customer_address_entity ----------------------------

create view mag.customer_address_entity_aud_v as
	select record_type, count(*) over (partition by entity_id) num_records, entity_id, increment_id, parent_id, 
		entity_type_id, attribute_set_id, is_active, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_id, increment_id, parent_id, 
			entity_type_id, attribute_set_id, is_active, 
			created_at, updated_at, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_aud
		union
		select 'H' record_type, entity_id, increment_id, parent_id, 
			entity_type_id, attribute_set_id, is_active, 
			created_at, updated_at, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_aud_hist) t
go


----------------------- customer_address_entity_datetime ----------------------------

create view mag.customer_address_entity_datetime_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_datetime_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_datetime_aud_hist) t
go 

----------------------- customer_address_entity_decimal ----------------------------

create view mag.customer_address_entity_decimal_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_decimal_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_decimal_aud_hist) t
go 

----------------------- customer_address_entity_int ----------------------------

create view mag.customer_address_entity_int_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_int_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_int_aud_hist) t
go 

----------------------- customer_address_entity_text ----------------------------

create view mag.customer_address_entity_text_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_text_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_text_aud_hist) t
go 

----------------------- customer_address_entity_varchar ----------------------------

create view mag.customer_address_entity_varchar_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.customer_address_entity_varchar_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.customer_address_entity_varchar_aud_hist) t
go 

