use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- catalog_product_entity ----------------------------

	drop trigger mag.trg_catalog_product_entity;
	go 

	create trigger mag.trg_catalog_product_entity on mag.catalog_product_entity
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_set_id, 0), 
				isnull(trg.type_id, ' '), isnull(trg.sku, ' '), isnull(trg.required_options, 0), isnull(trg.has_options, 0), 
				isnull(trg.created_at, ' '), isnull(trg.updated_at, ' ')
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_set_id, 0), 
				isnull(src.type_id, ' '), isnull(src.sku, ' '), isnull(src.required_options, 0), isnull(src.has_options, 0), 
				isnull(src.created_at, ' '), isnull(src.updated_at, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_set_id = src.attribute_set_id, 
					trg.type_id = src.type_id, trg.sku = src.sku, trg.required_options = src.required_options, trg.has_options = src.has_options, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					entity_type_id, attribute_set_id, 
					type_id, sku, required_options, has_options, 
					created_at, updated_at, 
					idETLBatchRun)
					
					values (src.entity_id, 
						src.entity_type_id, src.attribute_set_id, 
						src.type_id, src.sku, src.required_options, src.has_options, 
						src.created_at, src.updated_at, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_aud;
	go

	create trigger mag.trg_catalog_product_entity_aud on mag.catalog_product_entity_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_aud_hist (entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.entity_type_id, d.attribute_set_id, 
				d.type_id, d.sku, d.required_options, d.has_options, 
				d.created_at, d.updated_at, 
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go


-----

------------------------- catalog_product_entity_datetime ----------------------------

	drop trigger mag.trg_catalog_product_entity_datetime;
	go 

	create trigger mag.trg_catalog_product_entity_datetime on mag.catalog_product_entity_datetime
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_datetime_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_datetime_aud;
	go

	create trigger mag.trg_catalog_product_entity_datetime_aud on mag.catalog_product_entity_datetime_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_datetime_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_product_entity_decimal ----------------------------

	drop trigger mag.trg_catalog_product_entity_decimal;
	go 

	create trigger mag.trg_catalog_product_entity_decimal on mag.catalog_product_entity_decimal
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_decimal_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_decimal_aud;
	go

	create trigger mag.trg_catalog_product_entity_decimal_aud on mag.catalog_product_entity_decimal_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_decimal_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_product_entity_int ----------------------------

	drop trigger mag.trg_catalog_product_entity_int;
	go 

	create trigger mag.trg_catalog_product_entity_int on mag.catalog_product_entity_int
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_int_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, 0) 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_int_aud;
	go

	create trigger mag.trg_catalog_product_entity_int_aud on mag.catalog_product_entity_int_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_int_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_product_entity_text ----------------------------

	drop trigger mag.trg_catalog_product_entity_text;
	go 

	create trigger mag.trg_catalog_product_entity_text on mag.catalog_product_entity_text
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_text_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_text_aud;
	go

	create trigger mag.trg_catalog_product_entity_text_aud on mag.catalog_product_entity_text_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_text_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

-----

------------------------- catalog_product_entity_varchar ----------------------------

	drop trigger mag.trg_catalog_product_entity_varchar;
	go 

	create trigger mag.trg_catalog_product_entity_varchar on mag.catalog_product_entity_varchar
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_varchar_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_id, 0), isnull(trg.entity_id, 0), isnull(trg.store_id, 0), 
				isnull(trg.value, ' ') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_id, 0), isnull(src.entity_id, 0), isnull(src.store_id, 0),  
				isnull(src.value, ' '))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_id = src.attribute_id, trg.entity_id = src.entity_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_type_id, attribute_id, entity_id, store_id, value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_type_id, src.attribute_id, src.entity_id, src.store_id, src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_varchar_aud;
	go

	create trigger mag.trg_catalog_product_entity_varchar_aud on mag.catalog_product_entity_varchar_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_varchar_aud_hist (value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_type_id, d.attribute_id, d.entity_id, d.store_id, d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go


----

------------------------- catalog_product_entity_tier_price ----------------------------

	drop trigger mag.trg_catalog_product_entity_tier_price;
	go 

	create trigger mag.trg_catalog_product_entity_tier_price on mag.catalog_product_entity_tier_price
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_entity_tier_price_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select 
				isnull(trg.entity_id, 0), isnull(trg.customer_group_id, 0), isnull(trg.all_groups, 0), isnull(trg.website_id, 0), 
				isnull(trg.value, 0), isnull(trg.qty, 0), isnull(trg.promo_key, ' ')
			intersect
			select 
				isnull(src.entity_id, 0), isnull(src.customer_group_id, 0), isnull(src.all_groups, 0), isnull(src.website_id, 0), 
				isnull(src.value, 0), isnull(src.qty, 0), isnull(src.promo_key, ' '))
			
			then
				update set
					trg.entity_id = src.entity_id, trg.customer_group_id = src.customer_group_id, trg.all_groups = src.all_groups, trg.website_id = src.website_id, 
					trg.value = src.value, trg.qty = src.qty, trg.promo_key = src.promo_key, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					entity_id, customer_group_id, all_groups, website_id, 
					value, qty, promo_key, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.entity_id, src.customer_group_id, src.all_groups, src.website_id, 
						src.value, src.qty, src.promo_key, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_entity_tier_price_aud;
	go

	create trigger mag.trg_catalog_product_entity_tier_price_aud on mag.catalog_product_entity_tier_price_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_entity_tier_price_aud_hist (value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.entity_id, d.customer_group_id, d.all_groups, d.website_id, 
				d.value, d.qty, d.promo_key, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

----

------------------------- catalog_category_product ----------------------------

	drop trigger mag.trg_catalog_category_product;
	go 

	create trigger mag.trg_catalog_category_product on mag.catalog_category_product
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_category_product_aud with (tablock) as trg
		using inserted src
			on (trg.category_id = src.category_id and trg.product_id = src.product_id)
		when matched and not exists
			(select 
				isnull(trg.position, 0)
			intersect
			select 
				isnull(src.position, 0))
			
			then
				update set
					trg.position = src.position, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (category_id, product_id,
					position, 
					idETLBatchRun)
					
					values (src.category_id, src.product_id,
						src.position, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_category_product_aud;
	go

	create trigger mag.trg_catalog_category_product_aud on mag.catalog_category_product_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_category_product_aud_hist (category_id, product_id,
			position,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.category_id, d.product_id, 
				d.position, 
				d.idETLBatchRun,
				case when (i.category_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.category_id = i.category_id and d.product_id = i.product_id;
	end;
	go

----

------------------------- catalog_product_website ----------------------------

	drop trigger mag.trg_catalog_product_website;
	go 

	create trigger mag.trg_catalog_product_website on mag.catalog_product_website
	after insert
	as
	begin
		set nocount on

		merge into mag.catalog_product_website_aud with (tablock) as trg
		using inserted src
			on (trg.product_id = src.product_id and trg.website_id = src.website_id)

		when not matched 
			then
				insert (product_id, website_id, 
					idETLBatchRun)
					
					values (src.product_id, src.website_id,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_catalog_product_website_aud;
	go

	create trigger mag.trg_catalog_product_website_aud on mag.catalog_product_website_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.catalog_product_website_aud_hist (product_id, website_id, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.product_id, d.website_id, 
				d.idETLBatchRun,
				case when (i.product_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.product_id = i.product_id and d.website_id = i.website_id;
	end;
	go
