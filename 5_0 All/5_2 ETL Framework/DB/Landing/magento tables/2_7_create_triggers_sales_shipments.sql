
use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- sales_flat_shipment ----------------------------

	drop trigger mag.trg_sales_flat_shipment;
	go 

	create trigger mag.trg_sales_flat_shipment on mag.sales_flat_shipment
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_shipment_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.increment_id, ' '), isnull(trg.order_id, 0), isnull(trg.store_id, 0), isnull(trg.customer_id, 0), 
				isnull(trg.created_at, ' '), -- isnull(trg.updated_at, ' '), 
				isnull(trg.shipping_address_id, 0), isnull(trg.billing_address_id, 0), 
				isnull(trg.total_qty, 0), 
				isnull(trg.total_weight, 0), 
				isnull(trg.shipping_description, ' ')
			intersect
			select 
				isnull(src.increment_id, ' '), isnull(src.order_id, 0), isnull(src.store_id, 0), isnull(src.customer_id, 0), 
				isnull(src.created_at, ' '), -- isnull(src.updated_at, ' '), 
				isnull(src.shipping_address_id, 0), isnull(src.billing_address_id, 0), 
				isnull(src.total_qty, 0), 
				isnull(src.total_weight, 0), 
				isnull(src.shipping_description, ' '))
			
			then
				update set
					trg.increment_id = src.increment_id, trg.order_id = src.order_id, trg.store_id = src.store_id, trg.customer_id = src.customer_id, 
					trg.created_at = src.created_at, trg.updated_at = src.updated_at, 
					trg.shipping_address_id = src.shipping_address_id, trg.billing_address_id = src.billing_address_id, 
					trg.total_qty = src.total_qty, 
					trg.total_weight = src.total_weight, 
					trg.shipping_description = src.shipping_description, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					increment_id, order_id, store_id, customer_id, created_at, updated_at, 
					shipping_address_id, billing_address_id, 
					total_qty, 
					total_weight, 
					shipping_description,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.increment_id, src.order_id, src.store_id, src.customer_id, src.created_at, src.updated_at, 
						src.shipping_address_id, src.billing_address_id, 
						src.total_qty, 
						src.total_weight, 
						src.shipping_description,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_shipment_aud;
	go

	create trigger mag.trg_sales_flat_shipment_aud on mag.sales_flat_shipment_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_shipment_aud_hist (entity_id, 
			increment_id, order_id, store_id, customer_id, created_at, updated_at, 
			shipping_address_id, billing_address_id, 
			total_qty, 
			total_weight, 
			shipping_description,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.increment_id, d.order_id, d.store_id, d.customer_id, d.created_at, d.updated_at, 
				d.shipping_address_id, d.billing_address_id, 
				d.total_qty, 
				d.total_weight, 
				d.shipping_description,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go

----

------------------------- sales_flat_shipment_item ----------------------------

	drop trigger mag.trg_sales_flat_shipment_item;
	go 

	create trigger mag.trg_sales_flat_shipment_item on mag.sales_flat_shipment_item
	after insert
	as
	begin
		set nocount on

		merge into mag.sales_flat_shipment_item_aud with (tablock) as trg
		using inserted src
			on (trg.entity_id = src.entity_id)
		when matched and not exists
			(select 
				isnull(trg.order_item_id, 0), isnull(trg.parent_id, 0), 
				isnull(trg.product_id, 0), isnull(trg.sku, ' '), isnull(trg.name, ' '), isnull(trg.description, ' '), 
				isnull(trg.qty, 0), 
				isnull(trg.weight, 0), 
				isnull(trg.price, 0), 
				isnull(trg.row_total, 0)
			intersect
			select 
				isnull(src.order_item_id, 0), isnull(src.parent_id, 0), 
				isnull(src.product_id, 0), isnull(src.sku, ' '), isnull(src.name, ' '), isnull(src.description, ' '), 
				isnull(src.qty, 0), 
				isnull(src.weight, 0), 
				isnull(src.price, 0), 
				isnull(src.row_total, 0))
			
			then
				update set
					trg.order_item_id = src.order_item_id, trg.parent_id = src.parent_id, 
					trg.product_id = src.product_id, trg.sku = src.sku, trg.name = src.name, trg.description = src.description, 
					trg.qty = src.qty, 
					trg.weight = src.weight, 
					trg.price = src.price, 
					trg.row_total = src.row_total, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_id, 
					order_item_id, parent_id, 
					product_id, sku, name, description, 
					qty, 
					weight, 
					price, 
					row_total,
					idETLBatchRun)
					
					values (src.entity_id, 
						src.order_item_id, src.parent_id, 
						src.product_id, src.sku, src.name, src.description, 
						src.qty, 
						src.weight, 
						src.price, 
						src.row_total,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_sales_flat_shipment_item_aud;
	go

	create trigger mag.trg_sales_flat_shipment_item_aud on mag.sales_flat_shipment_item_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.sales_flat_shipment_item_aud_hist (entity_id, 
			order_item_id, parent_id, 
			product_id, sku, name, description, 
			qty, 
			weight, 
			price, 
			row_total,
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_id, 
				d.order_item_id, d.parent_id, 
				d.product_id, d.sku, d.name, d.description, 
				d.qty, 
				d.weight, 
				d.price, 
				d.row_total,
				d.idETLBatchRun,
				case when (i.entity_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_id = i.entity_id;
	end;
	go