use Landing
go 

----------------------- customer_entity ----------------------------

select * 
from mag.customer_entity_aud_v
where num_records > 1
order by entity_id, idETLBatchRun

----------------------- customer_entity_datetime ----------------------------

select * 
from mag.customer_entity_datetime_aud_v
where num_records > 1
order by entity_id, value_id, idETLBatchRun

----------------------- customer_entity_decimal ----------------------------

select * 
from mag.customer_entity_decimal_aud_v
where num_records > 1
order by entity_id, value_id, idETLBatchRun

----------------------- customer_entity_int ----------------------------

select * 
from mag.customer_entity_int_aud_v
where num_records > 1
order by entity_id, value_id, idETLBatchRun

----------------------- customer_entity_text ----------------------------

select * 
from mag.customer_entity_text_aud_v
where num_records > 1
order by entity_id, value_id, idETLBatchRun

----------------------- customer_entity_varchar ----------------------------

select * 
from mag.customer_entity_varchar_aud_v
where num_records > 1
order by entity_id, value_id, idETLBatchRun

