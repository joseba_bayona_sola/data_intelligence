use Landing
go 

----------------------- core_store ----------------------------

select * 
from mag.core_store_aud_v
where num_records > 1
order by store_id, idETLBatchRun

----------------------- core_website ----------------------------

select * 
from mag.core_website_aud_v
where num_records > 1
order by website_id, idETLBatchRun

----------------------- core_config_data ----------------------------

select * 
from mag.core_config_data_aud_v
where num_records > 1
order by config_id, idETLBatchRun



----------------------- edi_manufacturer ----------------------------

select * 
from mag.edi_manufacturer_aud_v
where num_records > 1
order by manufacturer_id, idETLBatchRun

----------------------- edi_supplier_account ----------------------------

select * 
from mag.edi_supplier_account_aud_v
where num_records > 1
order by account_id, idETLBatchRun

----------------------- edi_packsize_pref ----------------------------

select * 
from mag.edi_packsize_pref_aud_v
where num_records > 1
order by packsize_pref_id, idETLBatchRun

----------------------- edi_stock_item ----------------------------

select * 
from mag.edi_stock_item_aud_v
where num_records > 1
order by id, idETLBatchRun



----------------------- directory_currency_rate ----------------------------

select * 
from mag.directory_currency_rate_aud_v
where num_records > 1
order by currency_from, currency_to, idETLBatchRun

----------------------- salesrule ----------------------------

select * 
from mag.salesrule_aud_v
where num_records > 1
order by rule_id, idETLBatchRun

----------------------- salesrule_coupon ----------------------------

select * 
from mag.salesrule_coupon_aud_v
where num_records > 1
order by coupon_id, idETLBatchRun



----------------------- po_reorder_profile ----------------------------

select * 
from mag.po_reorder_profile_aud_v
where num_records > 1
order by num_records desc, id, idETLBatchRun

----------------------- po_reorder_quote_payment ----------------------------

select * 
from mag.po_reorder_quote_payment_aud_v
where num_records > 1
order by payment_id, idETLBatchRun



----------------------- admin_user ----------------------------

select * 
from mag.admin_user_aud_v
where num_records > 1
order by num_records desc, user_id, idETLBatchRun

----------------------- log_customer ----------------------------

select * 
from mag.log_customer_aud_v
where num_records > 1
order by num_records desc, log_id, idETLBatchRun

----------------------- po_sales_pla_item ----------------------------

select * 
from mag.po_sales_pla_item_aud_v
where num_records > 1
order by num_records desc, id, idETLBatchRun

----------------------- enterprise_customerbalance ----------------------------

select * 
from mag.enterprise_customerbalance_aud_v
where num_records > 1
order by num_records desc, balance_id, idETLBatchRun

----------------------- enterprise_customerbalance_history ----------------------------

select * 
from mag.enterprise_customerbalance_history_aud_v
where num_records > 1
order by num_records desc, history_id, idETLBatchRun

----------------------- cybersourcestored_token ----------------------------

select * 
from mag.cybersourcestored_token_aud_v
where num_records > 1
order by num_records desc, id, idETLBatchRun

----------------------- ga_entity_transaction_data ----------------------------

select * 
from mag.ga_entity_transaction_data_aud_v
where num_records > 1
order by num_records desc, transactionId, idETLBatchRun


----------------------- mutual ----------------------------

select * 
from mag.mutual_aud_v
where num_records > 1
order by num_records desc, mutual_id, idETLBatchRun


----------------------- mutual_quotation ----------------------------

select * 
from mag.mutual_quotation_aud_v
where num_records > 1
order by num_records desc, quotation_id, idETLBatchRun

