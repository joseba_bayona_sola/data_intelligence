
--- 1: Orders with ADJUSTMENT as productID

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_adjustment_exc_vat
from DW_GetLenses.dbo.order_lines
where product_id = 0
order by document_date desc

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_line_subtotal_inc_vat, global_line_subtotal_exc_vat,
	global_store_credit_inc_vat, global_store_credit_exc_vat,
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_line_total_exc_vat
--select *
from DW_GetLenses.dbo.order_lines
where order_id in (4366675, 5037626, 5052601, 5072647, 5093289)
	--and product_id = 0
order by order_id, document_date desc

	-- 4366675, 5037626, 5093289: Special Refund --> Value NULL
	-- 5072647, 5052601: Refund for Shipment --> Have a Value

--- 2: Orders with ADJUSTMENT with a VALUE on it

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_adjustment_exc_vat
from DW_GetLenses.dbo.order_lines
where global_adjustment_inc_vat is not null and global_adjustment_inc_vat <> 0
order by document_date desc

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_line_subtotal_inc_vat, global_line_subtotal_exc_vat,
	global_store_credit_inc_vat, global_store_credit_exc_vat,
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_line_total_exc_vat
--select *
from DW_GetLenses.dbo.order_lines
where order_id in (5090456, 5085825, 5066045, 5040393, 4775369)
	--and product_id = 0
order by order_id, document_date desc

--- 3: Orders with STORE CREDIT as productID
select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_adjustment_exc_vat
from DW_GetLenses.dbo.order_lines
where product_id = 2950
order by document_date desc

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_line_subtotal_inc_vat, global_line_subtotal_exc_vat,
	global_store_credit_inc_vat, global_store_credit_exc_vat,
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_line_total_exc_vat
--select *
from DW_GetLenses.dbo.order_lines
where order_id in (5109096, 5108772, 5108122, 5105563, 5104761)
	--and product_id = 0
order by order_id, document_date desc

--- 4: Orders using STORE CREDIT for paying

select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, 
	product_id, name, 
	global_line_subtotal_inc_vat, global_line_subtotal_exc_vat,
	global_store_credit_inc_vat, global_store_credit_exc_vat,
	global_adjustment_inc_vat, global_adjustment_exc_vat,
	global_line_total_inc_vat, global_line_total_exc_vat
--select *
from DW_GetLenses.dbo.order_lines
where order_id in (5109008, 5107384, 5101690, 5102863)
	--and product_id = 0
order by order_id, document_date desc