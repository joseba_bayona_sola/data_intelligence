	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	where entity_id in (4366675, 5037626, 5052601, 5072647, 5093289)
	order by entity_id, record_type, ins_ts

	select top 1000 entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
		base_grand_total, 
		base_adjustment, base_adjustment_positive, base_adjustment_negative, 
		base_to_global_rate, base_to_order_rate, order_currency_code, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_creditmemo_aud
	where order_id in (4366675, 5037626, 5052601, 5072647, 5093289);

	select top 1000 entity_id, order_item_id, parent_id,
		product_id, sku, name, description, 
		qty, 
		base_price, price, base_price_incl_tax, 
		base_cost, 
		base_row_total, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_creditmemo_item_aud
	where parent_id in (1251258, 1251514, 1251560, 1251646, 1251669);

---------------------------------------------------------------------------------------

	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	where entity_id in (5090456, 5085825, 5066045, 5040393, 4775369)
	order by entity_id, record_type, ins_ts

	select top 1000 entity_id, increment_id, order_id, invoice_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
		base_grand_total, 
		base_adjustment, base_adjustment_positive, base_adjustment_negative, 
		base_to_global_rate, base_to_order_rate, order_currency_code, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_creditmemo_aud
	where order_id in (5090456, 5085825, 5066045, 5040393, 4775369);

	select top 1000 entity_id, order_item_id, parent_id,
		product_id, sku, name, description, 
		qty, 
		base_price, price, base_price_incl_tax, 
		base_cost, 
		base_row_total, 
		idETLBatchRun, ins_ts, upd_ts
	from mag.sales_flat_creditmemo_item_aud
	where parent_id in (1250527, 1250675, 1251211, 1251429, 1251455);

---------------------------------------------------------------------------------------

	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	where entity_id in (5109096, 5108772, 5108122, 5105563, 5104761)
	order by entity_id, record_type, ins_ts

---------------------------------------------------------------------------------------

	select top 1000 record_type, num_records,
		entity_id, increment_id, created_at, state, status, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, 
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		ins_ts
	from mag.sales_flat_order_aud_v
	-- where base_customer_balance_amount > 0
	where entity_id in (5109008, 5107384, 5101690, 5102863)
	order by entity_id desc, record_type, ins_ts