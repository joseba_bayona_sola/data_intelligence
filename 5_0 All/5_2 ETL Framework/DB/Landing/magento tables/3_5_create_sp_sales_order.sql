use Landing
go 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_sales_flat_order
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	17-03-2017	Joseba Bayona Sola	Add bs_customer_bal_total_refunded column
	--	26-04-2017	Joseba Bayona Sola	Add applied_rule_ids column
	--	27-06-2017	Joseba Bayona Sola	Add old_order_id column
	--	01-09-2017	Joseba Bayona Sola	Add mutual_amount column
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_order
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_order
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
				base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
				base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
				base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
				base_grand_total, 
				base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
				base_to_global_rate, base_to_order_rate, order_currency_code,
				customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
				customer_taxvat, customer_gender, customer_note, customer_note_notify, 
				shipping_description, 
				coupon_code, applied_rule_ids,
				affilBatch, affilCode, affilUserRef, 
				reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
				reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
				presc_verification_method, prescription_verification_type, 
				referafriend_code, referafriend_referer, 
				telesales_method_code, telesales_admin_username, 
				remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
				old_order_id, 
				mutual_amount, mutual_quotation_id, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
				base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
				base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
				base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
				base_grand_total, 
				base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
				base_to_global_rate, base_to_order_rate, order_currency_code,
				customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
				customer_taxvat, customer_gender, customer_note, customer_note_notify, 
				shipping_description, 
				coupon_code, applied_rule_ids,
				affilBatch, affilCode, affilUserRef, 
				reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
				reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
				presc_verification_method, prescription_verification_type, 
				referafriend_code, referafriend_referer, 
				telesales_method_code, telesales_admin_username, 
				remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
				old_order_id, 
				mutual_amount, mutual_quotation_id
			from magento01.sales_flat_order  ' +  -- ' + // where entity_id > 5400000 '')'
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	-- insert into Landing.dbo.log_sp (sql_string) values (@sql)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----

drop procedure mag.srcmag_lnd_get_sales_flat_order_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	01-09-2017	Joseba Bayona Sola	Add mutual_amount column
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_order_item
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_order_item
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select item_id, order_id, store_id, created_at, updated_at,
				product_id, sku, name, description, 
				qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
				weight, row_weight, 
				base_price, price, base_price_incl_tax, 
				base_cost, 
				base_row_total, 
				base_discount_amount, discount_percent, 
				base_amount_refunded,
				mutual_amount, 
				product_type, product_options, is_virtual, is_lens, lens_group_eye, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select oi.item_id, oi.order_id, oi.store_id, oi.created_at, oi.updated_at,
				oi.product_id, oi.sku, oi.name, oi.description, 
				oi.qty_ordered, oi.qty_canceled, oi.qty_invoiced, oi.qty_refunded, oi.qty_shipped, oi.qty_backordered, oi.qty_returned, 
				oi.weight, oi.row_weight, 
				oi.base_price, oi.price, oi.base_price_incl_tax, 
				oi.base_cost, 
				oi.base_row_total, 
				oi.base_discount_amount, oi.discount_percent, 
				oi.base_amount_refunded,
				oi.mutual_amount, 
				oi.product_type, oi.product_options, oi.is_virtual, oi.is_lens, oi.lens_group_eye 
			from 
				magento01.sales_flat_order o
			inner join
				magento01.sales_flat_order_item oi on o.entity_id = oi.order_id	 ' + -- ' + // where oi.item_id >= 11000000 '')'
			'where o.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or o.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----

drop procedure mag.srcmag_lnd_get_sales_flat_order_payment
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_order_payment
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_order_payment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, parent_id, 
				method, 
				amount_authorized, amount_canceled, 
				base_amount_ordered, base_amount_canceled, base_amount_refunded, base_amount_refunded_online, 
				cc_type, cc_status, cc_status_description, cc_avs_status, 
				cc_ss_start_year, cc_ss_start_month, cc_exp_year, cc_exp_month, 
				cc_last4, cc_owner, cc_ss_issue, 
				additional_data, cybersource_stored_id, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select op.entity_id, op.parent_id, 
				op.method, 
				op.amount_authorized, op.amount_canceled, 
				op.base_amount_ordered, op.base_amount_canceled, op.base_amount_refunded, op.base_amount_refunded_online, 
				op.cc_type, op.cc_status, op.cc_status_description, op.cc_avs_status, 
				op.cc_ss_start_year, op.cc_ss_start_month, op.cc_exp_year, op.cc_exp_month, 
				op.cc_last4, op.cc_owner, op.cc_ss_issue, 
				op.additional_data, op.cybersource_stored_id	
			from 
				magento01.sales_flat_order o
			inner join
				magento01.sales_flat_order_payment op on o.entity_id = op.parent_id	' + -- ' + // where o.entity_id > 5400000	 '')'
			'where o.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or o.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----

drop procedure mag.srcmag_lnd_get_sales_flat_order_address
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_order_address
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_order_address
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, parent_id, customer_address_id, customer_id, 
				address_type, 
				email, company, 
				firstname, lastname, prefix, middlename, suffix, 
				street, city, postcode, region, 
				fax, telephone, 
				region_id, country_id,   ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select oa.entity_id, oa.parent_id, oa.customer_address_id, oa.customer_id, 
				oa.address_type, 
				oa.email, company, 
				oa.firstname, oa.lastname, oa.prefix, oa.middlename, oa.suffix, 
				oa.street, oa.city, oa.postcode, oa.region, 
				oa.fax, oa.telephone, 
				oa.region_id, oa.country_id
			from 
				magento01.sales_flat_order o
			inner join
				magento01.sales_flat_order_address oa on o.entity_id = oa.parent_id ' +  -- ' + // where o.entity_id > 5400000	 '')'
			'where o.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or o.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

---- 

drop procedure mag.srcmag_lnd_get_sales_order_log
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 15-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_order_log
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_order_log
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select id, order_id, order_no, 
				state, status, 
				user_name, updated_at,  ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select id, order_id, order_no, 
				state, status, 
				user user_name, updated_at
			from magento01.sales_order_log  ' + -- ' + // where order_id > 5400000	'')'
			'where updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 
