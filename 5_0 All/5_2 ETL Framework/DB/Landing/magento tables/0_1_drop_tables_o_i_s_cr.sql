
use Landing
go

drop table mag.sales_flat_order;
go
drop table mag.sales_flat_order_aud;
go
drop table mag.sales_flat_order_aud_hist;
go


drop table mag.sales_flat_order_item;
go
drop table mag.sales_flat_order_item_aud;
go
drop table mag.sales_flat_order_item_aud_hist;
go


drop table mag.sales_flat_order_payment;
go
drop table mag.sales_flat_order_payment_aud;
go
drop table mag.sales_flat_order_payment_aud_hist;
go


drop table mag.sales_flat_order_address;
go
drop table mag.sales_flat_order_address_aud;
go
drop table mag.sales_flat_order_address_aud_hist;
go

drop table mag.sales_order_log;
go
drop table mag.sales_order_log_aud;
go
drop table mag.sales_order_log_aud_hist;
go

---- 

drop table mag.sales_flat_invoice;
go
drop table mag.sales_flat_invoice_aud;
go
drop table mag.sales_flat_invoice_aud_hist;
go

drop table mag.sales_flat_invoice_item;
go
drop table mag.sales_flat_invoice_item_aud;
go
drop table mag.sales_flat_invoice_item_aud_hist;
go

---- 

drop table mag.sales_flat_shipment;
go
drop table mag.sales_flat_shipment_aud;
go
drop table mag.sales_flat_shipment_aud_hist;
go

drop table mag.sales_flat_shipment_item;
go
drop table mag.sales_flat_shipment_item_aud;
go
drop table mag.sales_flat_shipment_item_aud_hist;
go

---- 

drop table mag.sales_flat_creditmemo;
go
drop table mag.sales_flat_creditmemo_aud;
go
drop table mag.sales_flat_creditmemo_aud_hist;
go

drop table mag.sales_flat_creditmemo_item;
go
drop table mag.sales_flat_creditmemo_item_aud;
go
drop table mag.sales_flat_creditmemo_item_aud_hist;
go
