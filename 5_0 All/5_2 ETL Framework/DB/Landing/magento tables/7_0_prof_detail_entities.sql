use Landing
go 

----------------------- eav_entity_type ----------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.eav_entity_type_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------- eav_attribute ----------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.eav_attribute_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------- eav_entity_attribute ----------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.eav_entity_attribute_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------- eav_attribute_option ----------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.eav_attribute_option_aud
	group by idETLBatchRun
	order by idETLBatchRun


----------------------- eav_attribute_option_value ----------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.eav_attribute_option_value_aud
	group by idETLBatchRun
	order by idETLBatchRun


