
use Landing
go 

----------------------- eav_entity_type ----------------------------

create view mag.eav_entity_type_aud_v as
	select record_type, count(*) over (partition by entity_type_id) num_records, 
		entity_type_id, entity_type_code, 
		entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
		is_data_sharing, data_sharing_key, default_attribute_set_id, 
		increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
		additional_attribute_table, entity_attribute_collection, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_type_id, entity_type_code, 
			entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
			is_data_sharing, data_sharing_key, default_attribute_set_id, 
			increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
			additional_attribute_table, entity_attribute_collection, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.eav_entity_type_aud
		union
		select 'H' record_type, entity_type_id, entity_type_code, 
			entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
			is_data_sharing, data_sharing_key, default_attribute_set_id, 
			increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
			additional_attribute_table, entity_attribute_collection, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.eav_entity_type_aud_hist) t
go

----------------------- eav_attribute ----------------------------

create view mag.eav_attribute_aud_v as
	select record_type, count(*) over (partition by attribute_id) num_records, 
		attribute_id, entity_type_id, attribute_code, attribute_model,
		backend_model, backend_type, backend_table, 
		frontend_model, frontend_input, frontend_label, frontend_class,
		source_model, 
		is_required, is_user_defined, default_value, is_unique, note, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, attribute_id, entity_type_id, attribute_code, attribute_model,
			 backend_model, backend_type, backend_table, 
			 frontend_model, frontend_input, frontend_label, frontend_class,
			 source_model, 
			 is_required, is_user_defined, default_value, is_unique, note, 
			 idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.eav_attribute_aud
		union
		select 'H' record_type, attribute_id, entity_type_id, attribute_code, attribute_model,
			 backend_model, backend_type, backend_table, 
			 frontend_model, frontend_input, frontend_label, frontend_class,
			 source_model, 
			 is_required, is_user_defined, default_value, is_unique, note, 
			 idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.eav_attribute_aud_hist) t
go

----------------------- eav_entity_attribute ----------------------------

create view mag.eav_entity_attribute_aud_v as
	select record_type, count(*) over (partition by entity_attribute_id) num_records, 
		entity_attribute_id, 
		entity_type_id, 
		attribute_id, attribute_set_id, attribute_group_id, 
		sort_order, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, entity_attribute_id, 
			entity_type_id, 
			attribute_id, attribute_set_id, attribute_group_id, 
			sort_order, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.eav_entity_attribute_aud
		union
		select 'H' record_type, entity_attribute_id, 
			entity_type_id, 
			attribute_id, attribute_set_id, attribute_group_id, 
			sort_order, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo 	
		from mag.eav_entity_attribute_aud_hist) t

go

----------------------- eav_attribute_option ----------------------------

create view mag.eav_attribute_option_aud_v as
	select record_type, count(*) over (partition by option_id) num_records, 
		option_id, 
		attribute_id, sort_order, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, option_id, 
			attribute_id, sort_order, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.eav_attribute_option_aud
		union
		select 'H' record_type, option_id, 
			attribute_id, sort_order, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo 	
		from mag.eav_attribute_option_aud_hist) t
go

----------------------- eav_attribute_option_value ----------------------------

create view mag.eav_attribute_option_value_aud_v as
	select record_type, count(*) over (partition by value_id) num_records, 
		value_id, 
		option_id, store_id, 
		value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			option_id, store_id, 
			value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.eav_attribute_option_value_aud
		union
		select 'H' record_type, value_id, 
			option_id, store_id, 
			value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo 
		from mag.eav_attribute_option_value_aud_hist) t
go

