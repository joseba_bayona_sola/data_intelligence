use Landing
go 

--------------------- SP ----------------------------------

drop procedure mag.srcmag_lnd_get_sales_flat_shipment
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_shipment
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_shipment
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				total_qty, 
				total_weight, 
				shipping_description, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select entity_id, increment_id, order_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				total_qty, 
				total_weight, 
				shipping_description 
			from magento01.sales_flat_shipment ' + -- ' + // '')'
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 

----

drop procedure mag.srcmag_lnd_get_sales_flat_shipment_item
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Reads data from Magento Table through Open Query - sales_flat_shipment_item
-- ==========================================================================================

create procedure mag.srcmag_lnd_get_sales_flat_shipment_item
 	@idETLBatchRun bigint, @idPackageRun bigint, 
	@dateFrom datetime, @dateTo datetime
as
begin
	set nocount on

	DECLARE @idSPRun bigint, @sp_name varchar(100) = OBJECT_NAME(@@procid)
	DECLARE @rowAmountSelect int, @message varchar(500)
	DECLARE @sql varchar(max)
	
	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	exec ControlDB.logging.logSPRun_start_sp @sp_name = @sp_name, @idETLBatchRun = @idETLBatchRun, @idPackageRun = @idPackageRun, @idSPRun = @idSPRun output

	set @sql = '
			select entity_id, order_item_id, parent_id, 
				product_id, sku, name, description, 
				qty, 
				weight, 
				price, 
				row_total, ' + cast(@idETLBatchRun as varchar(20)) + ' '
		+ 'from openquery(MAGENTO, 
			''select si.entity_id, si.order_item_id, si.parent_id, 
				si.product_id, si.sku, si.name, si.description, 
				si.qty, 
				si.weight, 
				si.price, 
				si.row_total
			from 
				magento01.sales_flat_shipment s
			inner join
				magento01.sales_flat_shipment_item si on s.entity_id = si.parent_id	 ' +  -- ' + // '')'
			'where s.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or s.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)

	set @rowAmountSelect = @@ROWCOUNT
	set @message = 'SELECT # Rows: ' + CONVERT(VARCHAR(10), @rowAmountSelect)

	exec ControlDB.logging.logSPRun_message_sp @idSPRun = @idSPRun, @rowAmountSelect = @rowAmountSelect, @message = @message

	exec ControlDB.logging.logSPRun_stop_sp @idSPRun = @idSPRun, @runStatus = 'COMPLETE'

end;
go 