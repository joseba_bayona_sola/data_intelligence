use Landing
go

--------------------- Tables ----------------------------------

drop table mag.eav_entity_type;
go
drop table mag.eav_entity_type_aud;
go
drop table mag.eav_entity_type_aud_hist;
go

drop table mag.eav_attribute;
go
drop table mag.eav_attribute_aud;
go
drop table mag.eav_attribute_aud_hist;
go

drop table mag.eav_entity_attribute;
go
drop table mag.eav_entity_attribute_aud;
go
drop table mag.eav_entity_attribute_aud_hist;
go

drop table mag.eav_attribute_option;
go
drop table mag.eav_attribute_option_aud;
go
drop table mag.eav_attribute_option_aud_hist;
go

drop table mag.eav_attribute_option_value;
go
drop table mag.eav_attribute_option_value_aud;
go
drop table mag.eav_attribute_option_value_aud_hist;
go


-- 

drop table mag.customer_entity;
go
drop table mag.customer_entity_aud;
go
drop table mag.customer_entity_aud_hist;
go

drop table mag.customer_entity_datetime
go
drop table mag.customer_entity_datetime_aud;
go
drop table mag.customer_entity_datetime_aud_hist;
go

drop table mag.customer_entity_decimal
go
drop table mag.customer_entity_decimal_aud;
go
drop table mag.customer_entity_decimal_aud_hist;
go

drop table mag.customer_entity_int
go
drop table mag.customer_entity_int_aud;
go
drop table mag.customer_entity_int_aud_hist;
go

drop table mag.customer_entity_text
go
drop table mag.customer_entity_text_aud;
go
drop table mag.customer_entity_text_aud_hist;
go

drop table mag.customer_entity_varchar
go
drop table mag.customer_entity_varchar_aud;
go
drop table mag.customer_entity_varchar_aud_hist;
go

-- 

drop table mag.customer_address_entity;
go
drop table mag.customer_address_entity_aud;
go
drop table mag.customer_address_entity_aud_hist;
go

drop table mag.customer_address_entity_datetime
go
drop table mag.customer_address_entity_datetime_aud;
go
drop table mag.customer_address_entity_datetime_aud_hist;
go

drop table mag.customer_address_entity_decimal
go
drop table mag.customer_address_entity_decimal_aud;
go
drop table mag.customer_address_entity_decimal_aud_hist;
go

drop table mag.customer_address_entity_int
go
drop table mag.customer_address_entity_int_aud;
go
drop table mag.customer_address_entity_int_aud_hist;
go

drop table mag.customer_address_entity_text
go
drop table mag.customer_address_entity_text_aud;
go
drop table mag.customer_address_entity_text_aud_hist;
go

drop table mag.customer_address_entity_varchar
go
drop table mag.customer_address_entity_varchar_aud;
go
drop table mag.customer_address_entity_varchar_aud_hist;
go

-- 

drop table mag.catalog_category_entity;
go
drop table mag.catalog_category_entity_aud;
go
drop table mag.catalog_category_entity_aud_hist;
go

drop table mag.catalog_category_entity_datetime
go
drop table mag.catalog_category_entity_datetime_aud;
go
drop table mag.catalog_category_entity_datetime_aud_hist;
go

drop table mag.catalog_category_entity_decimal
go
drop table mag.catalog_category_entity_decimal_aud;
go
drop table mag.catalog_category_entity_decimal_aud_hist;
go

drop table mag.catalog_category_entity_int
go
drop table mag.catalog_category_entity_int_aud;
go
drop table mag.catalog_category_entity_int_aud_hist;
go

drop table mag.catalog_category_entity_text
go
drop table mag.catalog_category_entity_text_aud;
go
drop table mag.catalog_category_entity_text_aud_hist;
go

drop table mag.catalog_category_entity_varchar
go
drop table mag.catalog_category_entity_varchar_aud;
go
drop table mag.catalog_category_entity_varchar_aud_hist;
go

-- 

drop table mag.catalog_product_entity;
go
drop table mag.catalog_product_entity_aud;
go
drop table mag.catalog_product_entity_aud_hist;
go

drop table mag.catalog_product_entity_datetime
go
drop table mag.catalog_product_entity_datetime_aud;
go
drop table mag.catalog_product_entity_datetime_aud_hist;
go

drop table mag.catalog_product_entity_decimal
go
drop table mag.catalog_product_entity_decimal_aud;
go
drop table mag.catalog_product_entity_decimal_aud_hist;
go

drop table mag.catalog_product_entity_int
go
drop table mag.catalog_product_entity_int_aud;
go
drop table mag.catalog_product_entity_int_aud_hist;
go

drop table mag.catalog_product_entity_text
go
drop table mag.catalog_product_entity_text_aud;
go
drop table mag.catalog_product_entity_text_aud_hist;
go

drop table mag.catalog_product_entity_varchar
go
drop table mag.catalog_product_entity_varchar_aud;
go
drop table mag.catalog_product_entity_varchar_aud_hist;
go


drop table mag.catalog_product_entity_tier_price
go
drop table mag.catalog_product_entity_tier_price_aud;
go
drop table mag.catalog_product_entity_tier_price_aud_hist;
go

drop table mag.catalog_category_product
go
drop table mag.catalog_category_product_aud;
go
drop table mag.catalog_category_product_aud_hist;
go

drop table mag.catalog_product_website
go
drop table mag.catalog_product_website_aud;
go
drop table mag.catalog_product_website_aud_hist;
go

---------------------------------------------------------------------

drop table mag.customer_entity_flat;
go
drop table mag.customer_entity_flat_aud;
go
drop table mag.customer_entity_flat_aud_hist;
go

drop table mag.customer_address_entity_flat;
go
drop table mag.customer_address_entity_flat_aud;
go
drop table mag.customer_address_entity_flat_aud_hist;
go

drop table mag.catalog_category_entity_flat;
go
drop table mag.catalog_category_entity_flat_aud;
go
drop table mag.catalog_category_entity_flat_aud_hist;
go

drop table mag.catalog_product_entity_flat;
go
drop table mag.catalog_product_entity_flat_aud;
go
drop table mag.catalog_product_entity_flat_aud_hist;
go