use Landing
go 

------------------------------------------------------------------------
----------------------- catalog_product_entity ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_product_entity_aud

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct entity_id) num_dist_rows
	from mag.catalog_product_entity_aud_hist

	select *
	from
		(select count(*) over (partition by entity_id) num_rows_rep,
			entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			idETLBatchRun, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_aud_hist) t
	where num_rows_rep > 1
	order by entity_id, aud_dateFrom

------------------------------------------------------------------------
----------------------- catalog_product_entity_datetime ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_datetime_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_datetime_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_datetime_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_datetime_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_datetime_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_datetime_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_product_entity_datetime_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_product_entity_datetime ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_decimal_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_decimal_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_decimal_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_decimal_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_decimal_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_decimal_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_product_entity_decimal_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_product_entity_int ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_int_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_int_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_int_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_int_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_int_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_int_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_product_entity_int_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_product_entity_text ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_text_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_text_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_text_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_text_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_text_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_text_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_product_entity_text_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id

------------------------------------------------------------------------
----------------------- catalog_product_entity_varchar ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_varchar_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_varchar_aud

	select *
	from
		(select count(*) over (partition by entity_id, attribute_id, store_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_varchar_aud) t
	where num_rep > 1
	order by entity_id, attribute_id, store_id

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_varchar_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_varchar_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_varchar_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	----

	select t.attribute_id, a.attribute_code, t.num
	from
			(select entity_type_id, attribute_id, count(*) num
			from mag.catalog_product_entity_varchar_aud
			group by entity_type_id, attribute_id) t
		inner join
			mag.eav_attribute_aud a on t.entity_type_id = a.entity_type_id and t.attribute_id = a.attribute_id
	order by t.attribute_id



---------------------------------------------------------------------------------------------

------------------------------------------------------------------------
----------------------- catalog_product_entity_tier_price ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_entity_tier_price_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_tier_price_aud

	select *
	from
		(select count(*) over (partition by entity_id, website_id, qty, promo_key) num_rep,
			value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_product_entity_tier_price_aud) t
	where num_rep > 1
	order by entity_id, website_id, qty, promo_key, value

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_entity_tier_price_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct value_id) num_dist_rows
	from mag.catalog_product_entity_tier_price_aud_hist
	
	select *
	from
		(select count(*) over (partition by value_id) num_rep,
			value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_tier_price_aud_hist) t
	where num_rep > 1
	order by value_id, aud_dateFrom

	---- 

	select * 
	from mag.catalog_product_entity_tier_price_aud_v
	where num_records > 1
	order by entity_id, website_id, qty, promo_key, idETLBatchRun


------------------------------------------------------------------------
----------------------- catalog_category_product ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_category_product_aud
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct convert(varchar, category_id) + convert(varchar, product_id)) num_dist_rows
	from mag.catalog_category_product_aud

	select *
	from
		(select count(*) over (partition by category_id, product_id, position) num_rep,
			category_id, product_id, position,
			idETLBatchRun, ins_ts, upd_ts
		from mag.catalog_category_product_aud) t
	where num_rep > 1
	order by category_id, product_id, position

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_category_product_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

	select count(*) num_rows, count(distinct convert(varchar, category_id) + convert(varchar, product_id)) num_dist_rows
	from mag.catalog_category_product_aud_hist
	
	select *
	from
		(select count(*) over (partition by category_id, product_id, position) num_rep,
			category_id, product_id, position, 
			idETLBatchRun, ins_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_product_aud_hist) t
	where num_rep > 1
	order by category_id, product_id, position

------------------------------------------------------------------------
----------------------- catalog_product_website ----------------------------
------------------------------------------------------------------------

	select idETLBatchRun, count(*) num_rows, count(upd_ts) num_upd
	from mag.catalog_product_website_aud
	group by idETLBatchRun
	order by idETLBatchRun

	----

	select idETLBatchRun, count(*) num_rows
	from mag.catalog_product_website_aud_hist
	group by idETLBatchRun
	order by idETLBatchRun

