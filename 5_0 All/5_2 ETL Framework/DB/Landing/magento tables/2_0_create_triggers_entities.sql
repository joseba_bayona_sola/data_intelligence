use Landing
go 

--------------------- Triggers ----------------------------------

------------------------- eav_entity_type ----------------------------

	drop trigger mag.trg_eav_entity_type;
	go 

	create trigger mag.trg_eav_entity_type on mag.eav_entity_type
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_entity_type_aud with (tablock) as trg
		using inserted src
			on (trg.entity_type_id = src.entity_type_id)
		when matched and not exists
			(select isnull(trg.entity_type_code, ''), 
				isnull(trg.entity_model, ''), isnull(trg.attribute_model, ''), isnull(trg.entity_table, ''), isnull(trg.value_table_prefix, ''), isnull(trg.entity_id_field, ''), 
				isnull(trg.is_data_sharing, 0), isnull(trg.data_sharing_key, ''), isnull(trg.default_attribute_set_id, 0), 
				isnull(trg.increment_model, ''), isnull(trg.increment_per_store, 0), isnull(trg.increment_pad_length, 0), isnull(trg.increment_pad_char, ''), 
				isnull(trg.additional_attribute_table, ''), isnull(trg.entity_attribute_collection, '')
			intersect
			select isnull(src.entity_type_code, ''), 
				isnull(src.entity_model, ''), isnull(src.attribute_model, ''), isnull(src.entity_table, ''), isnull(src.value_table_prefix, ''), isnull(src.entity_id_field, ''), 
				isnull(src.is_data_sharing, 0), isnull(src.data_sharing_key, ''), isnull(src.default_attribute_set_id, 0), 
				isnull(src.increment_model, ''), isnull(src.increment_per_store, 0), isnull(src.increment_pad_length, 0), isnull(src.increment_pad_char, ''), 
				isnull(src.additional_attribute_table, ''), isnull(src.entity_attribute_collection, ''))
			
			then
				update set
					trg.entity_type_code = src.entity_type_code, 
					trg.entity_model = src.entity_model, trg.attribute_model = src.attribute_model, trg.entity_table = src.entity_table, 
					trg.value_table_prefix = src.value_table_prefix, trg.entity_id_field = src.entity_id_field,				
					trg.is_data_sharing = src.is_data_sharing, trg.data_sharing_key = src.data_sharing_key, trg.default_attribute_set_id = src.default_attribute_set_id, 
					trg.increment_model = src.increment_model, trg.increment_per_store = src.increment_per_store, 
					trg.increment_pad_length = src.increment_pad_length, trg.increment_pad_char = src.increment_pad_char, 
					trg.additional_attribute_table = src.additional_attribute_table, trg.entity_attribute_collection = src.entity_attribute_collection,
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then
				insert (entity_type_id, entity_type_code, 
					entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
					is_data_sharing, data_sharing_key, default_attribute_set_id, 
					increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
					additional_attribute_table, entity_attribute_collection, idETLBatchRun)
					
					values (src.entity_type_id, src.entity_type_code, 
						src.entity_model, src.attribute_model, src.entity_table, src.value_table_prefix, src.entity_id_field, 
						src.is_data_sharing, src.data_sharing_key, src.default_attribute_set_id, 
						src.increment_model, src.increment_per_store, src.increment_pad_length, src.increment_pad_char, 
						src.additional_attribute_table, src.entity_attribute_collection, src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_entity_type_aud;
	go

	create trigger mag.trg_eav_entity_type_aud on mag.eav_entity_type_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_entity_type_aud_hist (entity_type_id, entity_type_code, 
			entity_model, attribute_model, entity_table, value_table_prefix, entity_id_field, 
			is_data_sharing, data_sharing_key, default_attribute_set_id, 
			increment_model, increment_per_store, increment_pad_length, increment_pad_char, 
			additional_attribute_table, entity_attribute_collection, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_type_id, d.entity_type_code, 
				d.entity_model, d.attribute_model, d.entity_table, d.value_table_prefix, d.entity_id_field, 
				d.is_data_sharing, d.data_sharing_key, d.default_attribute_set_id, 
				d.increment_model, d.increment_per_store, d.increment_pad_length, d.increment_pad_char, 
				d.additional_attribute_table, d.entity_attribute_collection, 
				d.idETLBatchRun,
				case when (i.entity_type_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_type_id = i.entity_type_id;
	end;
	go


-----

------------------------- eav_attribute ----------------------------

	drop trigger mag.trg_eav_attribute;
	go 

	create trigger mag.trg_eav_attribute on mag.eav_attribute
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_attribute_aud with (tablock) as trg
		using inserted src
			on (trg.attribute_id = src.attribute_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), isnull(trg.attribute_code, ''), isnull(trg.attribute_model, ''), 
				isnull(trg.backend_model, ''), isnull(trg.backend_type, ''), isnull(trg.backend_table, ''), 
				isnull(trg.frontend_model, ''), isnull(trg.frontend_input, ''), isnull(trg.frontend_label, ''), isnull(trg.frontend_class, ''), 
				isnull(trg.source_model, ''), 
				isnull(trg.is_required, 0), isnull(trg.is_user_defined, 0), isnull(trg.default_value, ''), 
				isnull(trg.is_unique, 0), isnull(trg.note, '') 
			intersect
			select 
				isnull(src.entity_type_id, 0), isnull(src.attribute_code, ''), isnull(src.attribute_model, ''), 
				isnull(src.backend_model, ''), isnull(src.backend_type, ''), isnull(src.backend_table, ''), 
				isnull(src.frontend_model, ''), isnull(src.frontend_input, ''), isnull(src.frontend_label, ''), isnull(src.frontend_class, ''), 
				isnull(src.source_model, ''), 
				isnull(src.is_required, 0), isnull(src.is_user_defined, 0), isnull(src.default_value, ''), 
				isnull(src.is_unique, 0), isnull(src.note, ''))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, trg.attribute_code = src.attribute_code, trg.attribute_model = src.attribute_model, 
					trg.backend_model = src.backend_model, trg.backend_type = src.backend_type, trg.backend_table = src.backend_table, 
					trg.frontend_model = src.frontend_model, trg.frontend_input = src.frontend_input, trg.frontend_label = src.frontend_label, trg.frontend_class = src.frontend_class, 
					trg.source_model = src.source_model, 
					trg.is_required = src.is_required, trg.is_user_defined = src.is_user_defined, trg.default_value = src.default_value, 
					trg.is_unique = src.is_unique, trg.note = src.note, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()
		when not matched 
			then
				insert (attribute_id, 
					entity_type_id, attribute_code, attribute_model,
					backend_model, backend_type, backend_table, 
					frontend_model, frontend_input, frontend_label, frontend_class,
					source_model, 
					is_required, is_user_defined, default_value, is_unique, note,
					idETLBatchRun)
					
					values (src.attribute_id, 
						src.entity_type_id, src.attribute_code, src.attribute_model,
						src.backend_model, src.backend_type, src.backend_table, 
						src.frontend_model, src.frontend_input, src.frontend_label, src.frontend_class,
						src.source_model, 
						src.is_required, src.is_user_defined, src.default_value, src.is_unique, src.note,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_attribute_aud;
	go

	create trigger mag.trg_eav_attribute_aud on mag.eav_attribute_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_attribute_aud_hist (attribute_id, 
			entity_type_id, attribute_code, attribute_model,
			backend_model, backend_type, backend_table, 
			frontend_model, frontend_input, frontend_label, frontend_class,
			source_model, 
			is_required, is_user_defined, default_value, is_unique, note, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.attribute_id, 
				d.entity_type_id, d.attribute_code, d.attribute_model,
				d.backend_model, d.backend_type, d.backend_table, 
				d.frontend_model, d.frontend_input, d.frontend_label, d.frontend_class,
				d.source_model, 
				d.is_required, d.is_user_defined, d.default_value, d.is_unique, d.note, 
				d.idETLBatchRun,
				case when (i.attribute_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.attribute_id = i.attribute_id;
	end;
	go

-----

------------------------- eav_entity_attribute ----------------------------

	drop trigger mag.trg_eav_entity_attribute;
	go 

	create trigger mag.trg_eav_entity_attribute on mag.eav_entity_attribute
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_entity_attribute_aud with (tablock) as trg
		using inserted src
			on (trg.entity_attribute_id = src.entity_attribute_id)
		when matched and not exists
			(select 
				isnull(trg.entity_type_id, 0), 
				isnull(trg.attribute_id, 0), isnull(trg.attribute_set_id, 0), isnull(trg.attribute_group_id, 0), 
				isnull(trg.sort_order, 0)
			intersect
			select 
				isnull(src.entity_type_id, 0), 
				isnull(src.attribute_id, 0), isnull(src.attribute_set_id, 0), isnull(src.attribute_group_id, 0), 
				isnull(src.sort_order, 0))
			
			then
				update set
					trg.entity_type_id = src.entity_type_id, 
					trg.attribute_id = src.attribute_id, trg.attribute_set_id = src.attribute_set_id, trg.attribute_group_id = src.attribute_group_id, 
					trg.sort_order = src.sort_order, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (entity_attribute_id, 
					entity_type_id, 
					attribute_id, attribute_set_id, attribute_group_id, 
					sort_order,
					idETLBatchRun)
					
					values (src.entity_attribute_id, 
						src.entity_type_id, 
						src.attribute_id, src.attribute_set_id, src.attribute_group_id, 
						src.sort_order,
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_entity_attribute_aud;
	go

	create trigger mag.trg_eav_entity_attribute_aud on mag.eav_entity_attribute_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_entity_attribute_aud_hist (entity_attribute_id, 
			entity_type_id, 
			attribute_id, attribute_set_id, attribute_group_id, 
			sort_order, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.entity_attribute_id, 
				d.entity_type_id, 
				d.attribute_id, d.attribute_set_id, d.attribute_group_id, 
				d.sort_order, 
				d.idETLBatchRun,
				case when (i.entity_attribute_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.entity_attribute_id = i.entity_attribute_id;
	end;
	go

-----

------------------------- eav_attribute_option ----------------------------

	drop trigger mag.trg_eav_attribute_option;
	go 

	create trigger mag.trg_eav_attribute_option on mag.eav_attribute_option
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_attribute_option_aud with (tablock) as trg
		using inserted src
			on (trg.option_id = src.option_id)
		when matched and not exists
			(select isnull(trg.attribute_id, 0), isnull(trg.sort_order, 0)
			intersect
			select isnull(src.attribute_id, 0), isnull(src.sort_order, 0))
			
			then
				update set
					trg.attribute_id = src.attribute_id, trg.sort_order = src.sort_order, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (option_id, 
					attribute_id, sort_order, 
					idETLBatchRun)
					
					values (src.option_id, 
						src.attribute_id, src.sort_order, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_attribute_option_aud;
	go

	create trigger mag.trg_eav_attribute_option_aud on mag.eav_attribute_option_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_attribute_option_aud_hist (option_id, 
			attribute_id, sort_order, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.option_id, 
				d.attribute_id, d.sort_order, 
				d.idETLBatchRun,
				case when (i.option_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.option_id = i.option_id;
	end;
	go

-----

------------------------- eav_attribute_option_value ----------------------------

	drop trigger mag.trg_eav_attribute_option_value;
	go 

	create trigger mag.trg_eav_attribute_option_value on mag.eav_attribute_option_value
	after insert
	as
	begin
		set nocount on

		merge into mag.eav_attribute_option_value_aud with (tablock) as trg
		using inserted src
			on (trg.value_id = src.value_id)
		when matched and not exists
			(select isnull(trg.option_id, 0), isnull(trg.store_id, 0),
				isnull(trg.value, '')
			intersect
			select isnull(src.option_id, 0), isnull(src.store_id, 0),
				isnull(src.value, ''))
			
			then
				update set
					trg.option_id = src.option_id, trg.store_id = src.store_id, 
					trg.value = src.value, 
					trg.idETLBatchRun = src.idETLBatchRun, trg.upd_ts = getutcdate()

		when not matched 
			then
				insert (value_id, 
					option_id, store_id, 
					value, 
					idETLBatchRun)
					
					values (src.value_id, 
						src.option_id, src.store_id, 
						src.value, 
						src.idETLBatchRun);
	end; 
	go


	drop trigger mag.trg_eav_attribute_option_value_aud;
	go

	create trigger mag.trg_eav_attribute_option_value_aud on mag.eav_attribute_option_value_aud
	for update, delete
	as
	begin
		set nocount on

		insert mag.eav_attribute_option_value_aud_hist (value_id, 
			option_id, store_id, 
			value, 
			idETLBatchRun, 
			aud_type, aud_dateFrom, aud_dateTo)
			
			select d.value_id, 
				d.option_id, d.store_id, 
				d.value, 
				d.idETLBatchRun,
				case when (i.value_id is null) then 'D' else 'U' end aud_type, isnull(d.upd_ts, d.ins_ts) aud_dateFrom, i.upd_ts aud_dateTo 
			from 
				deleted d
			left join 
				inserted i on d.value_id = i.value_id;
	end;
	go

