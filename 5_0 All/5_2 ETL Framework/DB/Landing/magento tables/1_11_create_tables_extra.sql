use Landing
go

create table mag.review_entity (
	entity_id						int NOT NULL,
	entity_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go
alter table mag.review_entity add constraint [PK_mag_review_entity_entity_id]
	primary key clustered (entity_id);
go
alter table mag.review_entity add constraint [DF_mag_review_entity_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_entity_aud (
	entity_id						int NOT NULL,
	entity_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go
alter table mag.review_entity_aud add constraint [PK_mag_review_entity_aud_entity_id]
	primary key clustered (entity_id);
go
alter table mag.review_entity_aud add constraint [DF_mag_review_entity_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_entity_aud_hist (
	entity_id						int NOT NULL,
	entity_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go
alter table mag.review_entity_aud_hist add constraint [DF_mag_review_entity_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	


--===========================================================================================================================================


create table mag.review_status (
	status_id						int NOT NULL,
	status_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go
alter table mag.review_status add constraint [PK_mag_review_status_status_id]
	primary key clustered (status_id);
go
alter table mag.review_status add constraint [PK_mag_review_status_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_status_aud (
	status_id						int NOT NULL,
	status_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go
alter table mag.review_status_aud add constraint [PK_mag_review_status_aud_status_id]
	primary key clustered (status_id);
go
alter table mag.review_status_aud add constraint [DF_mag_review_status_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_status_aud_hist (
	status_id						int NOT NULL,
	status_code						varchar(32) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go
alter table mag.review_status_aud_hist add constraint [DF_mag_review_status_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	


--===========================================================================================================================================


create table mag.review (
	review_id						bigint NOT NULL,
	created_at						datetime,
	entity_id 						int NOT NULL,
	status_id 						int NOT NULL,
	entity_pk_value 				int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go
alter table mag.review add constraint [PK_mag_review_review_id]
	primary key clustered (review_id);
go
alter table mag.review add constraint [DF_mag_review_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_aud (
	review_id						bigint NOT NULL,
	created_at						datetime,
	entity_id 						int NOT NULL,
	status_id 						int NOT NULL,
	entity_pk_value 				int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go
go
alter table mag.review_aud add constraint [PK_mag_review_aud_review_id]
	primary key clustered (review_id);
alter table mag.review_aud add constraint [DF_mag_review_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_aud_hist (
	review_id						bigint NOT NULL,
	created_at						datetime,
	entity_id 						int NOT NULL,
	status_id 						int NOT NULL,
	entity_pk_value 				int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go
alter table mag.review_aud_hist add constraint [DF_mag_review_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

--===========================================================================================================================================


create table mag.review_store (
	review_id						bigint not null,
	store_id						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go
alter table mag.review_store add constraint [PK_mag_review_store_review_id_store_id]
	primary key clustered (review_id, store_id);
go
alter table mag.review_store add constraint [DF_mag_review_store_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	


create table mag.review_store_aud (
	review_id						bigint not null,
	store_id						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go
alter table mag.review_store_aud add constraint [PK_mag_review_store_aud_review_id_store_id]
	primary key clustered (review_id, store_id);
go
alter table mag.review_store_aud add constraint [DF_mag_review_store_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	


create table mag.review_store_aud_hist (
	review_id						bigint not null,
	store_id						int  NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go
alter table mag.review_store_aud_hist add constraint [DF_mag_review_store_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	


--===========================================================================================================================================

create table mag.review_detail (
	detail_id						bigint NOT NULL,
	review_id						bigint NOT NULL,
	store_id 						int,
	title 							varchar(255),
	detail 							varchar(5000),
	nickname						varchar(128),
	customer_id						int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go
alter table mag.review_detail add constraint [PK_mag_review_detail_detail_id]
	primary key clustered (detail_id);
go
alter table mag.review_detail add constraint [DF_mag_review_detail_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_detail_aud (
	detail_id						bigint NOT NULL,
	review_id						bigint NOT NULL,
	store_id 						int,
	title 							varchar(255),
	detail 							varchar(5000),
	nickname						varchar(128),
	customer_id						int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go
alter table mag.review_detail_aud add constraint [PK_mag_review_detail_aud_detail_id]
	primary key clustered (detail_id);
go
alter table mag.review_detail_aud add constraint [DF_mag_review_detail_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.review_detail_aud_hist (
	detail_id						bigint NOT NULL,
	review_id						bigint NOT NULL,
	store_id 						int,
	title 							varchar(255),
	detail 							varchar(5000),
	nickname						varchar(128),
	customer_id						int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go
alter table mag.review_detail_aud_hist add constraint [DF_mag_review_detail_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	









--===========================================================================================================================================


create table mag.rating_entity
(
	entity_id						int NOT NULL,
	entity_code						varchar(64) NOT NULL ,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL
);
go

alter table mag.rating_entity add constraint [PK_mag_rating_entity_id]
	primary key clustered (entity_id);

alter table mag.rating_entity add constraint [DF_mag_rating_entity_ins_ts]
	default (getutcdate()) for ins_ts;

create table mag.rating_entity_aud
(
	entity_id						int NOT NULL,
	entity_code						varchar(64) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.rating_entity_aud add constraint [PK_mag_rating_entity_aud_id]
	primary key clustered (entity_id);

alter table mag.rating_entity_aud add constraint [DF_mag_rating_entity_aud_ins_ts]
	default (getutcdate()) for ins_ts;

create table mag.rating_entity_aud_hist
(
	entity_id						int NOT NULL,
	entity_code						varchar(64) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.rating_entity_aud_hist add constraint [DF_mag_rating_entity_aud_hist_ins_ts]
	default (getutcdate()) for ins_ts;

--===========================================================================================================================================


create table mag.rating (
	rating_id							int NOT NULL,
	entity_id							int NOT NULL,
	rating_code							varchar(64) NOT NULL,
	position							int NOT NULL,
	idETLBatchRun						bigint NOT NULL, 
	ins_ts								datetime NOT NULL);
go

alter table mag.rating add constraint [PK_mag_rating_rating_id] 
	primary key clustered (rating_id);
go

alter table mag.rating add constraint [DF_mag_rating_ins_ts] default (getutcdate()) for ins_ts;
go

create table mag.rating_aud (
	rating_id						int NOT NULL,
	entity_id						int NOT NULL,
	rating_code						varchar(64) NOT NULL,
	position						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.rating_aud add constraint [PK_mag_rating_aud_rating_id] 
	primary key clustered (rating_id);
go

alter table mag.rating_aud add constraint [DF_mag_rating_aud_ins_ts] default (getutcdate()) for ins_ts;
go

create table mag.rating_aud_hist (
	rating_id						int NOT NULL,
	entity_id						int NOT NULL,
	rating_code						varchar(64) NOT NULL,
	position						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.rating_aud_hist add constraint [DF_mag_rating_aud_hist_ins_ts] default (getutcdate()) for ins_ts;
go

--===========================================================================================================================================

create table mag.rating_option (
	option_id						int NOT NULL,
	rating_id						int NOT NULL,
	code							varchar(32) NOT NULL,
	value							int NOT NULL,
	position						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.rating_option add constraint [PK_mag_rating_option_option_id] 
		primary key clustered (option_id);
go

alter table mag.rating_option add constraint [DF_mag_rating_option_ins_ts] default (getutcdate()) for ins_ts;
go

create table mag.rating_option_aud (
	option_id						int NOT NULL,
	rating_id						int NOT NULL,
	code							varchar(32) NOT NULL,
	value							int NOT NULL,
	position						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.rating_option_aud add constraint [PK_mag_rating_option_aud_option_id] 
		primary key clustered (option_id);
go

alter table mag.rating_option_aud add constraint [DF_mag_rating_option_aud_inst_ts] default (getutcdate()) for ins_ts;
go

create table mag.rating_option_aud_hist (
	option_id						int NOT NULL,
	rating_id						int NOT NULL,
	code							varchar(32) NOT NULL,
	value							int NOT NULL,
	position						int NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.rating_option_aud_hist add constraint [DF_mag_rating_option_aud_hist_ins_ts] default (getutcdate()) for ins_ts;



--===========================================================================================================================================


create table mag.rating_option_vote (
	vote_id							bigint not null,
	review_id						int,
	remote_ip						varchar(50),
	customer_id						int,
	entity_pk_value					bigint,
	rating_id						int,
	option_id						int,
	percent_rating					int,
	value							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.rating_option_vote add constraint [PK_mag_rating_option_vote_vote_id] 
		primary key clustered (vote_id);
go

alter table mag.rating_option_vote add constraint [DF_mag_rating_option_vote_ins_ts] default (getutcdate()) for ins_ts;
go

create table mag.rating_option_vote_aud (
	vote_id							bigint not null,
	review_id						int,
	remote_ip						varchar(50),
	customer_id						int,
	entity_pk_value					bigint,
	rating_id						int,
	option_id						int,
	percent_rating					int,
	value							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.rating_option_vote_aud add constraint [PK_mag_rating_option_vote_aud_vote_id] 
		primary key clustered (vote_id);
go

alter table mag.rating_option_vote_aud add constraint [DF_mag_rating_option_vote_aud_ins_ts] default (getutcdate()) for ins_ts;
go


create table mag.rating_option_vote_aud_hist (
	vote_id							bigint,
	review_id						int,
	remote_ip						varchar(50),
	customer_id						int,
	entity_pk_value					bigint,
	rating_id						int,
	option_id						int,
	percent_rating					int,
	value							int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.rating_option_vote_aud_hist add constraint [DF_mag_rating_option_vote_aud_hist_ins_ts] default (getutcdate()) for ins_ts;
go



---------------------------------------------------------

-- wearer
create table mag.wearer (
	id								int NOT NULL, 
	customer_id						int NOT NULL, 
	name							varchar(45) NOT NULL,
	dob								datetime NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.wearer add constraint [PK_mag_wearer_]
	primary key clustered (id);
go

alter table mag.wearer add constraint [DF_mag_wearer_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_aud (
	id								int NOT NULL, 
	customer_id						int NOT NULL, 
	name							varchar(45) NOT NULL,
	dob								datetime NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.wearer_aud add constraint [PK_mag_wearer_aud_]
	primary key clustered (id);
go

alter table mag.wearer_aud add constraint [DF_mag_wearer_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_aud_hist (
	id								int NOT NULL, 
	customer_id						int NOT NULL, 
	name							varchar(45) NOT NULL,
	dob								datetime NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.wearer_aud_hist add constraint [DF_mag_wearer_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	



-- wearer_prescription_method
create table mag.wearer_prescription_method (
	id								int NOT NULL, 
	method							varchar(50) NOT NULL, 
	code							varchar(20),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.wearer_prescription_method add constraint [PK_mag_wearer_prescription_method_]
	primary key clustered (id);
go

alter table mag.wearer_prescription_method add constraint [DF_mag_wearer_prescription_method_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_prescription_method_aud (
	id								int NOT NULL, 
	method							varchar(50) NOT NULL, 
	code							varchar(20),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.wearer_prescription_method_aud add constraint [PK_mag_wearer_prescription_method_aud_]
	primary key clustered (id);
go

alter table mag.wearer_prescription_method_aud add constraint [DF_mag_wearer_prescription_method_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_prescription_method_aud_hist (
	id								int NOT NULL, 
	method							varchar(50) NOT NULL, 
	code							varchar(20),
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.wearer_prescription_method_aud_hist add constraint [DF_mag_wearer_prescription_method_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	



-- wearer_prescription
create table mag.wearer_prescription (
	id								int NOT NULL, 
	wearer_id						int NOT NULL, 
	method_id						int NOT NULL,
	date_last_prescription			datetime,
	date_start_prescription			datetime, 
	optician_name					varchar(45) NOT NULL,
	optician_address1				varchar(55),
	optician_address2				varchar(55),
	optician_city					varchar(55),
	optician_state					varchar(45),
	optician_country				varchar(55),
	optician_postcode				varchar(55),
	optician_phone					varchar(55),
	optician_lookup_id				int,
	reminder_sent					int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.wearer_prescription add constraint [PK_mag_wearer_prescription_]
	primary key clustered (id);
go

alter table mag.wearer_prescription add constraint [DF_mag_wearer_prescription_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_prescription_aud (
	id								int NOT NULL, 
	wearer_id						int NOT NULL, 
	method_id						int NOT NULL,
	date_last_prescription			datetime,
	date_start_prescription			datetime, 
	optician_name					varchar(45) NOT NULL,
	optician_address1				varchar(55),
	optician_address2				varchar(55),
	optician_city					varchar(55),
	optician_state					varchar(45),
	optician_country				varchar(55),
	optician_postcode				varchar(55),
	optician_phone					varchar(55),
	optician_lookup_id				int,
	reminder_sent					int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.wearer_prescription_aud add constraint [PK_mag_wearer_prescription_aud_]
	primary key clustered (id);
go

alter table mag.wearer_prescription_aud add constraint [DF_mag_wearer_prescription_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_prescription_aud_hist (
	id								int NOT NULL, 
	wearer_id						int NOT NULL, 
	method_id						int NOT NULL,
	date_last_prescription			datetime,
	date_start_prescription			datetime, 
	optician_name					varchar(45) NOT NULL,
	optician_address1				varchar(55),
	optician_address2				varchar(55),
	optician_city					varchar(55),
	optician_state					varchar(45),
	optician_country				varchar(55),
	optician_postcode				varchar(55),
	optician_phone					varchar(55),
	optician_lookup_id				int,
	reminder_sent					int,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.wearer_prescription_aud_hist add constraint [DF_mag_wearer_prescription_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	



-- wearer_order_item
create table mag.wearer_order_item (
	id								int NOT NULL, 
	order_id						varchar(50) NOT NULL, 
	item_id							int NOT NULL,
	quote_item_id					int	NOT NULL,
	prescription_type				varchar(20) NOT NULL,
	wearer_prescription_id			int NOT NULL,
	prescription_flag				char(1) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go

alter table mag.wearer_order_item add constraint [PK_mag_wearer_order_item_]
	primary key clustered (id);
go

alter table mag.wearer_order_item add constraint [DF_mag_wearer_order_item_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_order_item_aud (
	id								int NOT NULL, 
	order_id						varchar(50) NOT NULL, 
	item_id							int NOT NULL,
	quote_item_id					int	NOT NULL,
	prescription_type				varchar(20) NOT NULL,
	wearer_prescription_id			int NOT NULL,
	prescription_flag				char(1) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	upd_ts							datetime);
go

alter table mag.wearer_order_item_aud add constraint [PK_mag_wearer_order_item_aud_]
	primary key clustered (id);
go

alter table mag.wearer_order_item_aud add constraint [DF_mag_wearer_order_item_aud_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	

create table mag.wearer_order_item_aud_hist (
	id								int NOT NULL, 
	order_id						varchar(50) NOT NULL, 
	item_id							int NOT NULL,
	quote_item_id					int	NOT NULL,
	prescription_type				varchar(20) NOT NULL,
	wearer_prescription_id			int NOT NULL,
	prescription_flag				char(1) NOT NULL,
	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL,
	aud_type						char(1) NOT NULL, 
	aud_dateFrom					datetime NOT NULL, 
	aud_dateTo						datetime NOT NULL);
go

alter table mag.wearer_order_item_aud_hist add constraint [DF_mag_wearer_order_item_aud_hist_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go	
