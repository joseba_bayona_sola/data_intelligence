
use Landing
go 

----------------------- catalog_product_entity ----------------------------

create view mag.catalog_product_entity_aud_v as
	select  record_type, count(*) over (partition by entity_id) num_records, entity_id, 
		entity_type_id, attribute_set_id, 
		type_id, sku, required_options, has_options, 
		created_at, updated_at, 
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_aud
		union
		select 'H' record_type, entity_id, 
			entity_type_id, attribute_set_id, 
			type_id, sku, required_options, has_options, 
			created_at, updated_at, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_aud_hist) t
go

----------------------- catalog_product_datetime ----------------------------

create view mag.catalog_product_entity_datetime_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_datetime_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_datetime_aud_hist) t
go 

----------------------- catalog_product_decimal ----------------------------

create view mag.catalog_product_entity_decimal_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_decimal_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_decimal_aud_hist) t
go 

----------------------- catalog_product_int ----------------------------

create view mag.catalog_product_entity_int_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_int_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_int_aud_hist) t
go 

----------------------- catalog_product_text ----------------------------

create view mag.catalog_product_entity_text_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_text_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_text_aud_hist) t
go 

----------------------- catalog_product_varchar ----------------------------

create view mag.catalog_product_entity_varchar_aud_v as
	select record_type, value_id, count(*) over (partition by value_id) num_records, 
		entity_type_id, attribute_id, entity_id, store_id, value, 
		idETLBatchRun, ins_ts, upd_ts, aud_type, aud_dateFrom, aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_varchar_aud
		union
		select 'H' record_type, value_id, 
			entity_type_id, attribute_id, entity_id, store_id, value, 
			idETLBatchRun, ins_ts, null upd_ts, aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_varchar_aud_hist) t
go 


---------------------------------

----------------------- catalog_product_entity_tier_price ----------------------------

create view mag.catalog_product_entity_tier_price_aud_v as
	select record_type, count(*) over (partition by value_id) num_records, value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_entity_tier_price_aud
		union
		select 'H' record_type, value_id, 
			entity_id, customer_group_id, all_groups, website_id, 
			value, qty, promo_key, 
			idETLBatchRun, ins_ts, null upd_ts,aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_entity_tier_price_aud_hist) t
go

----------------------- catalog_category_product ----------------------------

create view mag.catalog_category_product_aud_v as
	select record_type, count(*) over (partition by category_id, product_id) num_records, category_id, product_id, position, 
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, category_id, product_id, position, 
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_category_product_aud
		union
		select 'H' record_type, category_id, product_id, position, 
			idETLBatchRun, ins_ts, null upd_ts,aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_category_product_aud_hist) t
go

----------------------- catalog_product_website ----------------------------

create view mag.catalog_product_website_aud_v as
	select record_type, product_id, website_id, count(*) over (partition by product_id, website_id) num_records,
		idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
	from
		(select 'N' record_type, product_id, website_id,
			idETLBatchRun, ins_ts, upd_ts, null aud_type, null aud_dateFrom, null aud_dateTo
		from mag.catalog_product_website_aud
		union
		select 'H' record_type, product_id, website_id,
			idETLBatchRun, ins_ts, null upd_ts,aud_type, aud_dateFrom, aud_dateTo
		from mag.catalog_product_website_aud_hist) t
go
