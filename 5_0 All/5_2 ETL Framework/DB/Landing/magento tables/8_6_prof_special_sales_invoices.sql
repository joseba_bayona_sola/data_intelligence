use Landing
go 

------------------------------------------------------------------------
----------------------- sales_flat_invoice ----------------------------
------------------------------------------------------------------------

select top 1000 entity_id, increment_id, order_id, store_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, 
	total_qty, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
	base_grand_total, base_total_refunded, 
	base_to_global_rate, base_to_order_rate, order_currency_code, 
	idETLBatchRun, ins_ts, upd_ts
from mag.sales_flat_invoice_aud;

	select store_id, count(*)
	from mag.sales_flat_invoice_aud
	group by store_id
	order by store_id

	select state, count(*)
	from mag.sales_flat_invoice_aud
	group by state
	order by state

	-- created_at - updated_at
	select year(created_at) yyyy, month(created_at) mm, count(*)
	from mag.sales_flat_invoice_aud
	group by year(created_at), month(created_at)
	order by year(created_at), month(created_at)

	select year(updated_at) yyyy, month(updated_at) mm, count(*)
	from mag.sales_flat_invoice_aud
	group by year(updated_at), month(updated_at)
	order by year(updated_at), month(updated_at)

	-- total_qty, 
	select top 1000 entity_id, count(distinct total_qty) num_dist
	from mag.sales_flat_invoice_aud_v
	group by entity_id
	having count(distinct total_qty) > 1
	order by num_dist desc;

	-- base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
	select top 1000 entity_id, increment_id, order_id, store_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, 
		total_qty, 
		base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 
		base_grand_total, 
		base_subtotal + base_shipping_amount + base_discount_amount - base_customer_balance_amount base_grand_calc
	from mag.sales_flat_invoice_aud
	where base_grand_total <> base_subtotal + base_shipping_amount + base_discount_amount - base_customer_balance_amount;

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ----------------------------------

	select top 1000 count(*) over () num_tot,
		oh.entity_id, oh.increment_id, ih.entity_id entity_id_i, ih.increment_id increment_id_i, 
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, ih.created_at created_at_i, ih.updated_at updated_at_i, 
		oh.state, oh.status, 
		oh.total_qty_ordered, ih.total_qty, 
		oh.base_grand_total, ih.base_grand_total base_grand_total_i, oh.base_total_refunded, ih.base_total_refunded base_total_refunded_i
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud
			where created_at > getutcdate() - 20
			) oh
		full join
			(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				total_qty, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_invoice_aud) ih on oh.entity_id = ih.order_id
	where ih.order_id is null
	-- where oh.entity_id is null

		select top 1000 oh.state, oh.status, count(*)
		-- select top 1000 year(oh.created_at) yyyy, month(oh.created_at) mm, count(*)
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_order_aud
				where created_at > getutcdate() - 20
				) oh
			full join
				(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
					total_qty, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_invoice_aud) ih on oh.entity_id = ih.order_id
		where ih.order_id is null
		group by oh.state, oh.status
		order by oh.state, oh.status
		-- group by year(oh.created_at), month(oh.created_at)
		-- order by year(oh.created_at), month(oh.created_at)



	select top 1000 count(*) over () num_tot,
		count(ih.entity_id) over (partition by oh.entity_id) num_invs,
		oh.entity_id, oh.increment_id, ih.entity_id entity_id_i, ih.increment_id increment_id_i, 
		oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, ih.created_at created_at_i, ih.updated_at updated_at_i, 
		oh.state, oh.status, 
		oh.total_qty_ordered, ih.total_qty, 
		oh.base_grand_total, ih.base_grand_total base_grand_total_i, oh.base_total_refunded, ih.base_total_refunded base_total_refunded_i
	from
			(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				state, status, 
				total_qty_ordered, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_order_aud) oh
		inner join
			(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
				total_qty, 
				base_grand_total, base_total_refunded
			from mag.sales_flat_invoice_aud) ih on oh.entity_id = ih.order_id
	-- where oh.total_qty_ordered <> ih.total_qty
	-- where oh.base_grand_total <> ih.base_grand_total
	where oh.base_total_refunded <> ih.base_total_refunded
	-- order by oh.entity_id
	order by num_invs desc, oh.entity_id

	select *
	from
		(select count(*) over () num_tot,
			count(ih.entity_id) over (partition by oh.entity_id) num_invs,
			oh.entity_id, oh.increment_id, ih.entity_id entity_id_i, ih.increment_id increment_id_i, 
			oh.store_id, oh.customer_id, oh.created_at, oh.updated_at, ih.created_at created_at_i, ih.updated_at updated_at_i, 
			oh.state, oh.status, 
			oh.total_qty_ordered, ih.total_qty, sum(ih.total_qty) over (partition by oh.entity_id) qty_sum, 
			oh.base_grand_total, ih.base_grand_total base_grand_total_i, oh.base_total_refunded, ih.base_total_refunded base_total_refunded_i
		from
				(select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
					state, status, 
					total_qty_ordered, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_order_aud) oh
			inner join
				(select entity_id, increment_id, order_id, store_id, created_at, updated_at, 
					total_qty, 
					base_grand_total, base_total_refunded
				from mag.sales_flat_invoice_aud) ih on oh.entity_id = ih.order_id) t
	where num_invs > 1
		-- and total_qty_ordered = qty_sum
	order by entity_id

------------------------------------------------------------------------
----------------------- sales_flat_invoice_item ----------------------------
------------------------------------------------------------------------

	select top 1000 count(*) over() num_tot,
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, 
		base_price, price, base_price_incl_tax, 
		base_cost, 
		base_row_total, 
		idETLBatchRun, ins_ts, upd_ts 
	from mag.sales_flat_invoice_item_aud;

	-- base_price, price, base_price_incl_tax, 
	select top 1000 count(*) over () num_tot,
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		base_price, price, base_price_incl_tax, 
		idETLBatchRun, ins_ts, upd_ts 
	from mag.sales_flat_invoice_item_aud
	where base_price <> price or base_price <> base_price_incl_tax;

	-- base_row_total, 
	select top 1000 count(*) over () num_tot,
		entity_id, order_item_id, parent_id, 
		product_id, sku, name, description, 
		qty, base_price, base_row_total,
		qty * base_price base_row_total_calc, abs(base_row_total - (qty * base_price)) dif_calc,
		idETLBatchRun, ins_ts, upd_ts 
	from mag.sales_flat_invoice_item_aud
	where qty * base_price <> base_row_total
	order by dif_calc, entity_id;

	---------------------------------------------------------------------------
	------------------------- CARDINALITIES ----------------------------------

	select top 1000 
		ih.entity_id, ih.increment_id, ii.entity_id, ih.store_id, ih.created_at, ih.updated_at, 
		ih.total_qty, ii.qty, ih.base_subtotal, ii.base_row_total 
	from
			(select *
			from mag.sales_flat_invoice_aud) ih
		full join
			(select *
			from mag.sales_flat_invoice_item_aud) ii on ih.entity_id = ii.parent_id
	where ih.entity_id is null or ii.parent_id is null
	order by ih.entity_id desc, ii.entity_id

	select top 1000 count(*) over () num_tot,
		count(*) over(partition by ih.entity_id) num_lines,
		ih.entity_id, ih.increment_id, ii.entity_id, ih.store_id, ih.created_at, ih.updated_at, 
		ih.total_qty, ii.qty, sum(ii.qty) over (partition by ih.entity_id) qty_sum, 
		ih.base_subtotal, ii.base_row_total, sum(ii.base_row_total) over (partition by ih.entity_id) base_row_total_sum
	from
			(select *
			from mag.sales_flat_invoice_aud) ih
		inner join
			(select *
			from mag.sales_flat_invoice_item_aud) ii on ih.entity_id = ii.parent_id
	order by ih.entity_id desc, ii.entity_id

	select
		count(*) over () num_tot, count(*) over(partition by entity_id) num_lines, *, 
		abs(base_subtotal - base_row_total_sum) dif_1
	from
		(select count(*) over () num_tot,
			ih.entity_id, ih.increment_id, ii.entity_id item_id, ih.store_id, ih.created_at, ih.updated_at, 
			ih.total_qty, ii.qty, sum(ii.qty) over (partition by ih.entity_id) qty_sum, 
			ih.base_subtotal, ii.base_row_total, sum(ii.base_row_total) over (partition by ih.entity_id) base_row_total_sum
		from
				(select *
				from mag.sales_flat_invoice_aud) ih
			inner join
				(select *
				from mag.sales_flat_invoice_item_aud) ii on ih.entity_id = ii.parent_id) t
	-- where total_qty <> qty_sum order by entity_id desc, item_id
	where base_subtotal <> base_row_total_sum and abs(base_subtotal - base_row_total_sum) > 1 order by dif_1 desc, entity_id desc, item_id
