
-- srcmend_lnd_get_purc_purchaseorderline
-- srcmend_lnd_get_whship_receiptline
-- srcmend_lnd_get_rec_snapreceiptline

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, 
		datediff(minute, spm.startTime, spm.finishTime) duration, avg(datediff(minute, spm.startTime, spm.finishTime)) over (),
		spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmend_lnd_get_purc_purchaseorderline' --  srcmend_lnd_get_purc_purchaseorderline - srcmend_lnd_get_whship_receiptline
	order by spm.finishTime desc, spm.messageTime

		select top 10000 year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*), count(*) / convert(decimal(12, 4), 9984)
		from Landing.mend.purc_purchaseorderline
		group by year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
		order by year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)

		select top 10000 year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts), count(*), count(*) / convert(decimal(12, 4), 8031)
		from Landing.mend.whship_receiptline
		group by year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)
		order by year(ins_ts), month(ins_ts), day(ins_ts), datepart(hour, ins_ts), datepart(minute, ins_ts)


--

-- lnd_stg_stock_tables
-- lnd_stg_alloc_tables
	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where package_name = 'lnd_stg_alloc_tables' --
	order by idPackageRun desc

-- lnd_stg_get_stock_wh_stock_item
-- lnd_stg_get_stock_wh_stock_item_batch
-- lnd_stg_get_stock_intransit_stock_item_batch

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_stock_wh_stock_item' --  lnd_stg_get_stock_wh_stock_item - lnd_stg_get_stock_wh_stock_item_batch - lnd_stg_get_stock_intransit_stock_item_batch
	order by spm.finishTime desc, spm.messageTime

-- lnd_stg_get_aux_alloc_order_line_erp_issue_measures
-- lnd_stg_get_aux_alloc_order_line_mag_erp_measures
-- lnd_stg_get_alloc_wholesale_order_invoice_line

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_aux_alloc_order_line_mag_erp_measures' --  
	order by spm.finishTime desc, spm.messageTime

-- stg_dwh_merge_sales_order_header_erp_data
-- stg_dwh_merge_sales_order_line_erp_data

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_merge_sales_order_line_erp_data' --  stg_dwh_merge_sales_order_header_erp_data - stg_dwh_merge_sales_order_line_erp_data
	order by spm.finishTime desc, spm.messageTime

-- lnd_stg_get_aux_alloc_order_header_erp_o
-- lnd_stg_get_alloc_order_header_erp
-- lnd_stg_get_aux_alloc_order_line_erp_measures
-- lnd_stg_get_aux_alloc_order_line_mag_erp_measures
-- lnd_stg_get_aux_alloc_order_header_erp_dim_order
-- lnd_stg_get_aux_alloc_order_line_erp_ol
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_aux_alloc_order_header_erp_dim_order' --  stg_dwh_merge_sales_order_header_erp_data - stg_dwh_merge_sales_order_line_erp_data
	order by spm.finishTime desc, spm.messageTime


	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_merge_alloc_order_line_erp_issue_all' --  stg_dwh_merge_sales_order_line_all -  stg_dwh_merge_alloc_order_line_erp_issue_all
	order by spm.finishTime desc, spm.messageTime

---------------------------------------------------------------

-- lnd_stg_get_aux_act_activity_sales (lnd_stg_sales_fact_order)
-- lnd_stg_get_aux_sales_order_line_o_i_s_cr
-- lnd_stg_get_aux_sales_order_line_product

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'lnd_stg_get_aux_sales_order_line_o_i_s_cr' --  
	order by spm.finishTime desc, spm.messageTime

---------------------------------------------------------------

-- FM (packages)

	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where flow_name = 'Futurmaster Tables'
		-- and package_name = '' -- VisionDirectFMLoad - fm_lnd_files_all - lnd_stg_fm_tables - stg_dwh_fm_tables 
	order by idPackageRun desc

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, datediff(minute, spm.startTime, spm.finishTime) duration, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_get_fm_fm_data' --  lnd_stg_get_fm_fm_data - stg_dwh_merge_fm_fm_data
	order by spm.finishTime desc, spm.messageTime


---------------------------------------------------------------

-- stg_dwh_merge_sales_order_line_all
-- stg_dwh_merge_alloc_order_line_erp_issue_all

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, 
		datediff(minute, spm.startTime, spm.finishTime) duration, avg(datediff(minute, spm.startTime, spm.finishTime)) over (),
		spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'stg_dwh_merge_alloc_order_line_erp_issue_all' --  stg_dwh_merge_sales_order_line_all - stg_dwh_merge_alloc_order_line_erp_issue_all
	order by spm.finishTime desc, spm.messageTime

-- srcmag_lnd_merge_customer_entity_duplicates
-- srcmag_lndrt_merge_customer_entity_duplicates

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, 
		datediff(minute, spm.startTime, spm.finishTime) duration, avg(datediff(minute, spm.startTime, spm.finishTime)) over (),
		spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmag_lnd_merge_customer_entity_duplicates' --  stg_dwh_merge_sales_order_line_all - stg_dwh_merge_alloc_order_line_erp_issue_all
	order by spm.finishTime desc, spm.messageTime

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, 
		datediff(minute, spm.startTime, spm.finishTime) duration, avg(datediff(minute, spm.startTime, spm.finishTime)) over (),
		spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmag_lndrt_merge_customer_entity_duplicates' --  stg_dwh_merge_sales_order_line_all - stg_dwh_merge_alloc_order_line_erp_issue_all
	order by spm.finishTime desc, spm.messageTime