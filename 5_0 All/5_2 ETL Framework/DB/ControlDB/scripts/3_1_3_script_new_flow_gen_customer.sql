use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'Customer' name, 'Data about Customer Dimension' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Customer' flow_name, 'dim_customer_type' name, 'Staging Table for Customer Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer_type' name, 'Warehouse Table for Customer Type: Customer Type Dimension' description
			union
			select 'Staging' database_name, 'Customer' flow_name, 'dim_customer_status' name, 'Staging Table for Customer Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer_status' name, 'Warehouse Table for Customer Status: Customer Status Dimension' description
			union
			select 'Staging' database_name, 'Customer' flow_name, 'dim_customer' name, 'Staging Table for Customer Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer' name, 'Warehouse Table for Customer: Customer Dimension' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer_SCD' name, 'Warehouse Table for Customer SCD: Customer SCD Dimension' description
			
			union
			select 'Staging' database_name, 'Customer' flow_name, 'dim_customer_unsubscribe' name, 'Staging Table for Customer Unsubscribe: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer_unsubscribe' name, 'Warehouse Table for Customer Unsubscribe: Customer Unsubscribe Dimension' description
			union
			select 'Staging' database_name, 'Customer' flow_name, 'dim_customer_origin' name, 'Staging Table for Customer Origin: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Customer' flow_name, 'dim_customer_origin' name, 'Warehouse Table for Customer Origin: Customer Origin Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Customer' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_gen_customer_tables' name, 'SSIS PKG for reading Customer data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Customer' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_gen_customer_tables' name, 'SSIS PKG for reading Customer data from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_customer_type' name, 'GET SP for reading Customer Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_customer_type' name, 'GET SP for reading Customer Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer_type' name, 'MERGE SP for upading Customer Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_customer_status' name, 'GET SP for reading Customer Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_customer_status' name, 'GET SP for reading Customer Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer_status' name, 'MERGE SP for upading Customer Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_customer' name, 'GET SP for reading Customer data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_customer' name, 'GET SP for reading Customer data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer' name, 'MERGE SP for upading Customer Dimension data' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer_scd' name, 'MERGE SP for upading Customer SCD Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_gen_customer_unsubscribe_magento' name, 'GET SP for reading Customer data related with Unsubscribe from Magento' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_gen_customer_unsubscribe_dotmailer' name, 'GET SP for reading Customer data related with Unsubscribe from DotMailer' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_gen_customer_unsubscribe_textmessage' name, 'GET SP for reading Customer data related with Unsubscribe from Text Message' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_gen_customer_reminder' name, 'GET SP for reading Customer data related with Unsubscribe from Reminder' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_gen_customer_reorder' name, 'GET SP for reading Customer data related with Unsubscribe from Reorder' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_customer_unsubscribe' name, 'GET SP for reading Customer Unsubscribe data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_customer_unsubscribe' name, 'GET SP for reading Customer Unsubscribe data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer_unsubscribe' name, 'MERGE SP for upading Customer Unsubscribe Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_customer_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_customer_origin' name, 'GET SP for reading Customer Origin data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_customer_origin' name, 'GET SP for reading Customer Origin data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_customer_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_customer_origin' name, 'MERGE SP for upading Customer Origin Dimension data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 

