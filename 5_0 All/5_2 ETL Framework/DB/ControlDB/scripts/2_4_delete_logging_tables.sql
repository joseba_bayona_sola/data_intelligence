use ControlDB
go 

-- t_SPRunMessage
delete from ControlDB.logging.t_SPRunMessage;

-- t_SPRun
delete from ControlDB.logging.t_SPRun;

-- t_PackageRunMessage
delete from ControlDB.logging.t_PackageRunMessage;

-- t_PackageRun
delete from ControlDB.logging.t_PackageRun;

-- t_ETLBatchRun
delete from ControlDB.logging.t_ETLBatchRun;

 

