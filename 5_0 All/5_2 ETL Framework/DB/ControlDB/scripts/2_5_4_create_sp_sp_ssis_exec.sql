USE ControlDB
GO


drop procedure dbo.sp_ssis_exec
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 21-02-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: SP that can be used in order to call a SSIS package
-- Different parameters can be set up: Folder - Project - Package - Environment - SSIS Parameters
-- ==========================================================================================

create procedure dbo.sp_ssis_exec
	@folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100),  
	@pars varchar(1000) 
as

begin
	declare @cmd varchar(4000), @returnCode int
	declare @reference_id int

	-- select @cmd = 'DTexec /ISSERVER "\SSISDB\DWH\ETL\ETLBatchRun.dtsx" /SERVER "." /Par "\"$Project::dateFrom(DateTime)\"";"\"2017-02-16 00:00:00\"" /Par "\"$Project::dateTo(DateTime)\"";"\"2017-02-17 00:00:00\"" /Par "\"description\"";"\"Prueba\""'  

	-- select @cmd = 'DTexec /ISSERVER "\SSISDB\' + @folder_name + '\' + @project_name + '\' + @package_name + '" /SERVER "." ' + 
	-- 	'/Par "\"$Project::dateFrom(DateTime)\"";"\"2017-02-16 00:00:00\"" /Par "\"$Project::dateTo(DateTime)\"";"\"2017-02-17 00:00:00\"" /Par "\"description\"";"\"Prueba\""'  

	select @reference_id = reference_id
	from 
		(select f.name folder_name, p.name project_name, e.name environment_name, ee.reference_id
		from 
				SSISDB.catalog.folders f
			inner join
				SSISDB.catalog.projects p on f.folder_id = p.folder_id
			inner join 
				SSISDB.catalog.environments e on f.folder_id = e.folder_id
			inner join
				SSISDB.catalog.environment_references ee on p.project_id = ee.project_id and e.name = ee.environment_name) e
	where folder_name = @folder_name and project_name = @project_name and environment_name = @environment_name

	select @cmd = 'DTexec /ISSERVER "\SSISDB\' + @folder_name + '\' + @project_name + '\' + @package_name + '" /SERVER "." /Envreference ' + cast(@reference_id as varchar(20)) + ' ' + 
		@pars
		-- '/Par "\"$Project::dateFrom(DateTime)\"";"\"2017-02-16 00:00:00\"" /Par "\"$Project::dateTo(DateTime)\"";"\"2017-02-17 00:00:00\"" /Par "\"description\"";"\"Prueba\""'  

		
	exec @ReturnCode = xp_cmdshell @Cmd

	-- insert into Landing.dbo.log_sp(sql_string) values (@Cmd)
end

