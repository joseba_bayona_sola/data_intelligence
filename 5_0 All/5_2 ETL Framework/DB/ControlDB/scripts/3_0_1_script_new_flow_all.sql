use ControlDB
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'all_ba_gen' name, 'SSIS PKG for executing packages at BA: General' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		left join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		left join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		left join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRun' name, 'SSIS PKG for starting the ETL Batch Run and calling rest of packages' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRunPackage' name, 'SSIS PKG for starting the ETL Batch Run and calling a certain package' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRunMigration' name, 'SSIS PKG for starting the ETL Batch Run and calling rest of packages' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRun_prepare' name, 'SSIS PKG for starting the ETL Batch Run and calling a Migration prepare SP' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRunMendix' name, 'SSIS PKG for starting the ETL Batch Run and calling a Mendix Landing PKG' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'ETLBatchRunMendixProv' name, 'SSIS PKG for starting the ETL Batch Run and calling a Mendix Landing PKG - PROVISIONAL FOR TIMEOUT TESTING' description

			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoad' name, 'SSIS PKG for executing all stages packages' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoadNoSRC' name, 'SSIS PKG for executing all stages packages - No SRC to LND' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoadMigration' name, 'SSIS PKG for executing all stages packages - No SRC to LND and call to Migration pkg (Hist - Migrate)' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoadCustomers' name, 'SSIS PKG for executing all stages packages - No SRC to LND and no call Sales Order Fact' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoadERP' name, 'SSIS PKG for executing all stages packages - No SRC to LND on ERP related flows' description
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectDataWarehouseLoadERPNoSRC' name, 'SSIS PKG for executing all stages packages - No SRC to LND on ERP related flows' description
				
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_src_lnd' name, 'SSIS PKG for executing SRC - LND packages' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_lnd_stg' name, 'SSIS PKG for executing LND - STG packages' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_stg_dwh' name, 'SSIS PKG for executing STG - DWH packages' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_lnd_stg_migrate' name, 'SSIS PKG for executing LND - STG packages - call to Migration pkg (Hist - Migrate)' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_lnd_stg_hist' name, 'SSIS PKG for executing LND - STG packages - call to Migration pkg (Hist - Migrate)' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_lnd_stg_customers' name, 'SSIS PKG for executing LND - STG packages - no call to Sales Orders Fact' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_src_lnd_mendix' name, 'SSIS PKG for executing SRC - LND packages: Mendix Data' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_src_lnd_mendix_one' name, 'SSIS PKG for executing SRC - LND packages: Mendix Data - PROVISIONAL FOR ONLY CALLING ONE PKG' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_lnd_stg_erp' name, 'SSIS PKG for executing LND - STG packages - ERP Flow' description	
			union
			select 'EXEC' codPackageType, 'jsola' developer,  null flow_name, null databaseFrom_name, null databaseTo_name, 
				'LoadAll_stg_dwh_erp' name, 'SSIS PKG for executing STG - DWH packages - ERP Flow' description	
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		left join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		left join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		left join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

