
	-- t_PackageRun_v
	select package_name, 
		idETLBatchRun, etlB_desc, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idExecution, startTime, finishTime, duration, runStatus
	from ControlDB.logging.t_PackageRun_v
	where idETLBatchRun in (949, 950, 951, 952, 953, 954, 955) 
		and package_name in ('VisionDirectDataWarehouseLoadNoSRC')

	select package_name, sp_name,
		idETLBatchRun,  
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_SPRun_v
	where idETLBatchRun in (949, 950, 951, 952, 953, 954, 955) 
		and package_name in ('lnd_stg_act_fact_activity', 'lnd_stg_sales_fact_order', 'stg_dwh_act_fact_activity', 'stg_dwh_sales_fact_order')
	order by package_name, sp_name, idETLBatchRun

	-- t_SPRunMessage_v
	select spm.package_name, spm.sp_name, idETLBatchRun,
		spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where idETLBatchRun in (949, 950, 951, 952, 953, 954, 955) 
		and spm.package_name in ('lnd_stg_act_fact_activity', 'lnd_stg_sales_fact_order', 'stg_dwh_act_fact_activity', 'stg_dwh_sales_fact_order')
	order by spm.package_name, spm.sp_name, idETLBatchRun
