use ControlDB
go 

-- t_ETLBatchRun
create table ControlDB.logging.t_ETLBatchRun (
	idETLBatchRun			bigint NOT NULL IDENTITY(1, 1), 
	idETLBatchType			int NOT NULL, 
	idFlowType				int NOT NULL, 
	idPackage				int NOT NULL,
	description				varchar(255) NOT NULL, 
	dateFrom				dateTime NOT NULL,
	dateTo					dateTime NOT NULL,
	startTime				dateTime NOT NULL,
	finishTime				dateTime,
	runStatus				varchar(50),
	-- message					varchar(500), 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_ETLBatchRun_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_ETLBatchRun add constraint PK_ETLBatchRun
	primary key clustered (idETLBatchRun);	 
go 

alter table ControlDB.logging.t_ETLBatchRun add constraint FK_ETLBatchRun_ETLBatchType
	foreign key (idETLBatchType) references ControlDB.config.cat_ETLBatchType (idETLBatchType);	 
go 
alter table ControlDB.logging.t_ETLBatchRun add constraint FK_ETLBatchRun_FlowType
	foreign key (idFlowType) references ControlDB.config.cat_FlowType (idFlowType);	 
go 
alter table ControlDB.logging.t_ETLBatchRun add constraint FK_ETLBatchRun_Package
	foreign key (idPackage) references ControlDB.config.t_Package (idPackage);	 
go 


-- t_PackageRun
create table ControlDB.logging.t_PackageRun (
	idPackageRun			bigint NOT NULL IDENTITY(1, 1), 	
	idPackage				int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	idPackageRun_parent		bigint,
	idExecution				uniqueidentifier NOT NULL,
	startTime				dateTime NOT NULL,
	finishTime				dateTime,
	runStatus				varchar(50),
	ins_ts					datetime NOT NULL CONSTRAINT [DF_PackageRun_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_PackageRun add constraint PK_PackageRun
	primary key clustered (idPackageRun);	 
go 

alter table ControlDB.logging.t_PackageRun add constraint FK_PackageRun_Package
	foreign key (idPackage) references ControlDB.config.t_Package (idPackage);;	 
go 
alter table ControlDB.logging.t_PackageRun add constraint FK_PackageRun_ETLBatchRun
	foreign key (idETLBatchRun) references ControlDB.logging.t_ETLBatchRun (idETLBatchRun);	 
go 
alter table ControlDB.logging.t_PackageRun add constraint FK_PackageRun_PackageRun_Parent
	foreign key (idPackageRun_parent) references ControlDB.logging.t_PackageRun (idPackageRun);	 
go 


-- t_PackageRunMessage
create table ControlDB.logging.t_PackageRunMessage (
	idPackageRunMessage		bigint NOT NULL IDENTITY(1, 1), 	
	idPackageRun			bigint NOT NULL, 	
	messageTime				dateTime NOT NULL,
	messageType				varchar(50),
	rowAmount				int,
	message					varchar(500), 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_PackageRunMessage_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_PackageRunMessage add constraint PK_PackageRunMessage
	primary key clustered (idPackageRunMessage);	 
go 

alter table ControlDB.logging.t_PackageRunMessage add constraint FK_PackageRunMessage_PackageRun
	foreign key (idPackageRun) references ControlDB.logging.t_PackageRun (idPackageRun); 
go 

-- t_PackageRunParameters??


-- t_SPRun
create table ControlDB.logging.t_SPRun (
	idSPRun					bigint NOT NULL IDENTITY(1, 1), 
	idSP					int NOT NULL,
	idETLBatchRun			bigint NOT NULL, 
	idPackageRun			bigint NOT NULL,
	startTime				dateTime NOT NULL,
	finishTime				dateTime,
	runStatus				varchar(50),
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SPRun_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_SPRun add constraint PK_SPRun
	primary key clustered (idSPRun);	 
go 

alter table ControlDB.logging.t_SPRun add constraint FK_SPRun_SP
	foreign key (idSP) references ControlDB.config.t_SP (idSP);;	 
go 
alter table ControlDB.logging.t_SPRun add constraint FK_SPRun_ETLBatchRun
	foreign key (idETLBatchRun) references ControlDB.logging.t_ETLBatchRun (idETLBatchRun);	 
go 
alter table ControlDB.logging.t_SPRun add constraint FK_SPRun_PackageRun
	foreign key (idPackageRun) references ControlDB.logging.t_PackageRun (idPackageRun);	 
go 

-- t_SPRunMessage
create table ControlDB.logging.t_SPRunMessage (
	idSPRunMessage			bigint NOT NULL IDENTITY(1, 1), 	
	idSPRun					bigint NOT NULL, 	
	messageTime				dateTime NOT NULL,
	rowAmountSelect			int,
	rowAmountInsert			int,
	rowAmountUpdate			int,
	rowAmountDelete			int,
	message					varchar(500), 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SPRunMessage_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_SPRunMessage add constraint PK_SPRunMessage
	primary key clustered (idSPRunMessage);	 
go 

alter table ControlDB.logging.t_SPRunMessage add constraint FK_SPRunMessage_SPRun
	foreign key (idSPRun) references ControlDB.logging.t_SPRun (idSPRun); 
go 


-- t_SPRunParameters ??




-- t_SPRun_ssrs
create table ControlDB.logging.t_SPRun_ssrs (
	idSPRun_ssrs			bigint NOT NULL IDENTITY(1, 1), 
	idSP_ssrs				int NOT NULL,
	startTime				dateTime NOT NULL,
	finishTime				dateTime,
	runStatus				varchar(50),
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SPRun_ssrs_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_SPRun_ssrs add constraint PK_SPRun_ssrs
	primary key clustered (idSPRun_ssrs);	 
go 

alter table ControlDB.logging.t_SPRun_ssrs add constraint FK_SPRun_ssrs_SP_ssrs
	foreign key (idSP_ssrs) references ControlDB.config.t_SP_ssrs (idSP_ssrs);	 
go 



-- t_SPRunMessage_ssrs
create table ControlDB.logging.t_SPRunMessage_ssrs (
	idSPRunMessage_ssrs		bigint NOT NULL IDENTITY(1, 1), 	
	idSPRun_ssrs			bigint NOT NULL, 	
	messageTime				dateTime NOT NULL,
	rowAmountSelect			int,
	rowAmountInsert			int,
	rowAmountUpdate			int,
	rowAmountDelete			int,
	message					varchar(500), 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SPRunMessage_ssrs_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_SPRunMessage_ssrs add constraint PK_SPRunMessage_ssrs
	primary key clustered (idSPRunMessage_ssrs);	 
go 

alter table ControlDB.logging.t_SPRunMessage_ssrs add constraint FK_SPRunMessage_ssrs_SPRun_ssrs
	foreign key (idSPRun_ssrs) references ControlDB.logging.t_SPRun_ssrs (idSPRun_ssrs); 
go 



-- t_SPRunParameter_ssrs
create table ControlDB.logging.t_SPRunParameter_ssrs (
	idSPRunParameter_ssrs	bigint NOT NULL IDENTITY(1, 1), 	
	idSPRun_ssrs			bigint NOT NULL, 	
	messageTime				dateTime NOT NULL,
	parameter				varchar(50),
	parameter_value			varchar(max), 
	ins_ts					datetime NOT NULL CONSTRAINT [DF_SPRunParameter_ssrs_ins_ts] DEFAULT (getutcdate()));
go 

alter table ControlDB.logging.t_SPRunParameter_ssrs add constraint PK_t_SPRunParameter_ssrs
	primary key clustered (idSPRunParameter_ssrs);	 
go 

alter table ControlDB.logging.t_SPRunParameter_ssrs add constraint FK_SPRunParameter_ssrs_SPRun_ssrs
	foreign key (idSPRun_ssrs) references ControlDB.logging.t_SPRun_ssrs (idSPRun_ssrs); 
go 

