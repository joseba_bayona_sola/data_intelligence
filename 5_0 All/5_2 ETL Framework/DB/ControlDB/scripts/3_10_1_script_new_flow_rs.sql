

-- t_SP
insert into ControlDB.config.t_SP_ssrs (idSP_ssrs, idSPType, idDeveloper, 
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax_ssrs idSP_ssrs, st.idSPType, d.idDeveloper, 
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  
				'dwh_rs_data_finance_oh' name, 'GET SP for reading OH Test data' description
			union

			select 'GET' codSPType, 'jsola' developer,  
				'dwh_rs_data_finance_retention' name, 'GET SP for reading RETENTION report data' description

			) t
		left join
			ControlDB.config.t_SP_ssrs t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer,
			(select isnull(max(idSP_ssrs), 0) idSPMax_ssrs
			from ControlDB.config.t_SP_ssrs) id 
	where t2.idSP_ssrs is null
go 
