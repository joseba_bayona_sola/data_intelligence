
select idDMLoadRun, 
	description, run_type, dateFrom, dateTo, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_DMLoadRun;

select idDMLoadRunAccount, idDMLoadRun, 
	store_id, -- website, 
	import_id, filename, info, 
	startTime, finishTime, rowAmount, runStatus, 
	ins_ts
from ControlDB.logging.t_DMLoadRunAccount;

select idDMLoadRunAccountMessage, idDMLoadRunAccount, 
	messageTime, messageType, rowAmount, message, 
	ins_ts
from ControlDB.logging.t_DMLoadRunAccountMessage;

	select idDMLoadRun, 
		description, run_type, dateFrom, dateTo, 
		startTime, finishTime, runStatus, 
		ins_ts
	from ControlDB.logging.t_DMLoadRun_v
	order by idDMLoadRun desc;

	select idDMLoadRun,
		idDMLoadRunAccount, 
		store_id, -- website, 
		import_id, filename, info, rowCount_new, rowCount_update,
		description, run_type, dateFrom, dateTo, 
		dlmr_startTime, dlmr_finishTime,
		startTime, finishTime, duration, rowAmount, runStatus, 
		ins_ts
	from ControlDB.logging.t_DMLoadRunAccount_v
	where idDMLoadRun = 19
	order by store_id, filename;

	select idDMLoadRunAccountMessage, idDMLoadRunAccount, 
		store_id, -- website,
		import_id, filename, 
		description, run_type, dateFrom, dateTo, 
		startTime, finishTime, duration, rowAmount, runStatus, 
		messageTime, messageType, rowAmount, message, 
		ins_ts
	from ControlDB.logging.t_DMLoadRunAccountMessage_v;


----------------------------------------------------------------

select top 1000 *
from Warehouse.dm.EMV_sync_list
where ins_ts between convert(datetime, '2019-09-25 11:21:51.000') and convert(datetime, '2019-09-26 04:00:01.000') 
	or upd_ts between convert(datetime, '2019-09-25 11:21:51.000') and convert(datetime, '2019-09-26 04:00:01.000') 

select top 1000 store_id, count(*)
from Warehouse.dm.EMV_sync_list
where ins_ts between convert(datetime, '2019-09-25 11:21:51.000') and convert(datetime, '2019-09-26 04:00:01.000') 
	or upd_ts between convert(datetime, '2019-09-25 11:21:51.000') and convert(datetime, '2019-09-26 04:00:01.000') 
group by store_id
order by store_id

---------------------------------------------

-- Autologing NULL

select top 1000 *
-- from Warehouse.dm.EMV_sync_list
from Warehouse.dm.fact_customer_signature_dm
where segment_3 is null
-- where segment_3 = ''

--

select top 1000 *
from Warehouse.dm.EMV_sync_list
-- from Warehouse.dm.fact_customer_signature_dm
where email = 'a.brasseur70@gmail.com'

-- 

select top 1000 client_id, email, 
	dailies_sku1, dailies_last_order_date, monthlies_sku1, monthlies_last_order_date, colours_sku1, colours_last_order_date, 
	lens_last_order_date, last_product_last_order_date
from Warehouse.dm.EMV_sync_list

-- 

select top 1000 *
-- from Warehouse.dm.EMV_sync_list
from Warehouse.dm.fact_customer_signature_dm
where email = 'zyna00@gmail.com'
