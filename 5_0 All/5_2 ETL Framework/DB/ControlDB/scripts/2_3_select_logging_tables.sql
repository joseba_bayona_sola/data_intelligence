
-- t_ETLBatchRun
select idETLBatchRun, idETLBatchType, idFlowType, idPackage,
	description, dateFrom, dateTo, 
	startTime, finishTime, runStatus, -- message,
	ins_ts
from ControlDB.logging.t_ETLBatchRun; 

-- t_PackageRun
select idPackageRun, idPackage, idETLBatchRun, idPackageRun_parent, 
	idExecution, startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_PackageRun

-- t_PackageRunMessage
select idPackageRunMessage, idPackageRun, 
	messageTime, messageType, rowAmount, message, 
	ins_ts
from ControlDB.logging.t_PackageRunMessage 

-- t_SPRun
select idSPRun, idSP, idETLBatchRun, idPackageRun, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_SPRun

-- t_SPRunMessage
select idSPRunMessage, idSPRun, 
	messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
	ins_ts	
from ControlDB.logging.t_SPRunMessage

-- 

select top 1000 max(createdDate), getutcdate() -1
from Landing.mend.order_order

	-- t_ETLBatchRun_v
	select idETLBatchRun, codETLBatchType, codFlowType, idPackage, flow_name, package_name,
		description, dateFrom, dateTo, 
		startTime, finishTime, duration, runStatus, -- message,
		ins_ts
	from ControlDB.logging.t_ETLBatchRun_v
	-- where package_name = 'VisionDirectDataWarehouseLoad' and description = 'VD Data Warehouse Load - Daily'
	-- where package_name = 'VisionDirectDataWarehouseLoadERP' and description = 'VD Mendix Landing - Daily'
	-- where package_name in ('VisionDirectDataWarehouseLoadNoSRC', 'VisionDirectDataWarehouseLoadCustomers')
	-- where package_name = 'VisionDirectDataWarehouseLoadERP' and runStatus = 'OK'
	-- where package_name = 'VisionDirectDataWarehouseLoadCustomers'
	order by idETLBatchRun desc

	-- t_PackageRun_v
	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRun_v
	where idETLBatchRun = 25143

		select idPackageRun, idPackage, flow_name, package_name, 
			idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
			idPackageRun_parent, 
			idExecution, startTime, finishTime, duration, runStatus, 
			ins_ts
		from ControlDB.logging.t_PackageRun_v
		where package_name = 'LoadAll_src_lnd'
		order by idETLBatchRun desc

	-- t_PackageRunMessage_v
	select idPackageRunMessage, prm.idPackageRun, prm.flow_name, prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunMessage_v prm
		inner join
			ControlDB.logging.t_PackageRun_v pr on prm.idPackageRun = pr.idPackageRun
	where pr.idETLBatchRun = 5241
		--and prm.package_name = 'lnd_stg_sales_fact_order' --  // stg_dwh_sales_fact_order
	order by prm.package_name, prm.message



	-- t_SPRun_v
	select idSPRun, idSP, package_name, sp_name,
		idETLBatchRun, idPackageRun, idExecution, pkg_startTime, pkg_finishTime, pkg_runStatus, 
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_SPRun_v
	where idETLBatchRun = 3563
	order by package_name, sp_name


	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, datediff(minute, spm.startTime, spm.finishTime) duration,
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where sp.idETLBatchRun = 25143 -- 3683
		--and spm.package_name = 'lnd_stg_sales_fact_order' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime, spm.messageTime

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	

	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.package_name = 'stg_dwh_act_fact_activity' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
		-- spm.sp_name = 'lnd_stg_get_aux_act_activity_sales'
	order by spm.package_name, spm.sp_name, spm.messageTime desc

	select *
	from ControlDB.dbo.sysssislog
	where executionid = 'D35BC5C4-414D-4C92-91B1-62B7BB4FD084'
	order by event, source, message

	---------------------------------------------------

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_v spm
		inner join
			ControlDB.logging.t_SPRun_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmend_lnd_get_order_order'
	order by spm.finishTime desc
	
------------------------------------------------------------

	select order_id_bk, product_type_oh_bk, 
		order_percentage, order_flag, order_qty_time
	from Landing.aux.sales_order_header_oh_pt
	where order_id_bk in
		(select order_id_bk
		from Landing.aux.sales_dim_order_header)
