
use ControlDB
go 

drop table ControlDB.logging.t_SPRunParameter_ssrs;
go 

drop table ControlDB.logging.t_SPRunMessage_ssrs;
go 

drop table ControlDB.logging.t_SPRunMessage;
go 

drop table ControlDB.logging.t_SPRun_ssrs;
go 

drop table ControlDB.logging.t_SPRun;
go 

drop table ControlDB.logging.t_PackageRunMessage;
go 

drop table ControlDB.logging.t_PackageRun;
go 

drop table ControlDB.logging.t_ETLBatchRun;
go 
