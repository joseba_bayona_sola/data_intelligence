use ControlDB
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Start record for the SP Run SSRS to the logging Table - Return Unique Record Number
-- ==========================================================================================

create procedure logging.logSPRun_ssrs_start_sp
	@sp_name varchar(100), 
	@idSPRun_ssrs bigint OUTPUT
as

begin
	set nocount on;

	insert into ControlDB.logging.t_SPRun_ssrs(idSP_ssrs, 
		startTime, runStatus)

		select sp.idSP_ssrs, 
			getutcdate() startTime, 'RUNNING' runStatus
		from
				(select @sp_name sp_name) t
			inner join
				ControlDB.config.t_SP_ssrs sp on t.sp_name = sp.sp_name;

	set @idSPRun_ssrs = SCOPE_IDENTITY();

	return @idSPRun_ssrs;
end 

go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Finish record for the SP Run SSRS to the logging Table 
-- ==========================================================================================

create procedure logging.logSPRun_ssrs_stop_sp
	@idSPRun_ssrs bigint, 
	@runStatus varchar(50)
as

begin
	set nocount on;

	update ControlDB.logging.t_SPRun_ssrs
	set
		finishTime = getutcdate(),
		runStatus = @runStatus
	where idSPRun_ssrs = @idSPRun_ssrs;

end 

go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 16-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Message record for the SP Run SSRS Message to the logging Table 
-- ==========================================================================================

create procedure logging.logSPRun_ssrs_message_sp
	@idSPRun_ssrs bigint, 
	@rowAmountSelect int = NULL, @rowAmountInsert int = NULL, @rowAmountUpdate int = NULL, @rowAmountDelete int = NULL,  
	@message varchar(500)
as

begin
	set nocount on;

	insert into ControlDB.logging.t_SPRunMessage_ssrs (idSPRun_ssrs, messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message) values
		(@idSPRun_ssrs, getutcdate(), @rowAmountSelect, @rowAmountInsert, @rowAmountUpdate, @rowAmountDelete, @message);

end 

go 




-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 17-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Message record for the SP Run SSRS Parameter to the logging Table 
-- ==========================================================================================

create procedure logging.logSPRun_ssrs_parameter_sp
	@idSPRun_ssrs bigint, 
	@parameter varchar(50), @parameter_value varchar(max)
as

begin
	set nocount on;

	insert into ControlDB.logging.t_SPRunParameter_ssrs (idSPRun_ssrs, messageTime, parameter, parameter_value) values
		(@idSPRun_ssrs, getutcdate(), @parameter, @parameter_value);

end 

go 

