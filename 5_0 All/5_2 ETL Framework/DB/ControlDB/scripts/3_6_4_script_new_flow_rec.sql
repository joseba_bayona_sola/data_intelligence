use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'rec' codBusinessArea, 
				'Receipts' name, 'Data about Receipts in ERP: WH Shipments - Receipts - Receipt Lines' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Receipts' flow_name, 'dim_wh_shipment_type' name, 'Staging Table for WH Shipment Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_wh_shipment_type' name, 'Warehouse Table for WH Shipment Type: WH Shipment Type Dimension' description
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'dim_receipt_status' name, 'Staging Table for Receipt Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_receipt_status' name, 'Warehouse Table for Receipt Status: Receipt Status Dimension' description
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'dim_receipt_line_status' name, 'Staging Table for Receipt Line Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_receipt_line_status' name, 'Warehouse Table for Receipt Line Status: Receipt Line Status Dimension' description
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'dim_receipt_line_sync_status' name, 'Staging Table for Receipt Line Sync Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_receipt_line_sync_status' name, 'Warehouse Table for Receipt Line Sync Status: Receipt Line Sync Status Dimension' description
			
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'dim_wh_shipment' name, 'Staging Table for WH Shipment: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_wh_shipment' name, 'Warehouse Table for WH Shipment: WH Shipment Dimension' description
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'dim_receipt' name, 'Staging Table for Receipt: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'dim_receipt' name, 'Warehouse Table for Receipt: Receipt Dimension' description
			union
			select 'Staging' database_name, 'Receipts' flow_name, 'fact_receipt_line' name, 'Staging Table for Receipt Line: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Receipts' flow_name, 'fact_receipt_line' name, 'Warehouse Table for Receipt Line: Receipt Line Fact' description
			
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Receipts' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_rec_tables' name, 'SSIS PKG for reading Receipts data (ERP) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Receipts' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_rec_tables' name, 'SSIS PKG for reading Receipts data (ERP) from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_wh_shipment_type' name, 'GET SP for reading WH Shipment Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_wh_shipment_type' name, 'GET SP for reading WH Shipment Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_wh_shipment_type' name, 'MERGE SP for upading WH Shipment Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_receipt_status' name, 'GET SP for reading Receipt Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_receipt_status' name, 'GET SP for reading Receipt Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_receipt_status' name, 'MERGE SP for upading Receipt Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_receipt_line_status' name, 'GET SP for reading Receipt Line Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_receipt_line_status' name, 'GET SP for reading Receipt Line Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_receipt_line_status' name, 'MERGE SP for upading Receipt Line Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_receipt_line_sync_status' name, 'GET SP for reading Receipt Line Sync Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_receipt_line_sync_status' name, 'GET SP for reading Receipt Line Sync Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_receipt_line_sync_status' name, 'MERGE SP for upading Receipt Line Sync Status Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_wh_shipment' name, 'GET SP for reading WH Shipment data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_wh_shipment' name, 'GET SP for reading WH Shipment data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_wh_shipment' name, 'MERGE SP for upading WH Shipment Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_receipt' name, 'GET SP for reading Receipt data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_receipt' name, 'GET SP for reading Receipt data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_receipt' name, 'MERGE SP for upading Receipt Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_rec_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_rec_receipt_line' name, 'GET SP for reading Receipt Line data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_rec_receipt_line' name, 'GET SP for reading Receipt Line data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_rec_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_rec_receipt_line' name, 'MERGE SP for upading Receipt Line Fact data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 


