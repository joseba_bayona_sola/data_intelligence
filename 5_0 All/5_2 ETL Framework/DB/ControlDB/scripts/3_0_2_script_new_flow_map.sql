use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'MAP-LND' codFlowType, 'MAP' codDWHTableType, 'map' codBusinessArea, 
				'Mapping Tables' name, 'Taking data of Mapping Tables from excel files placed on the server' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'EXEC' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, null databaseFrom_name, null databaseTo_name, 
				'VisionDirectMappingLoad' name, 'SSIS PKG for executing all Mapping packages' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		left join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		left join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 

------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_website_type' name, 'Mapping Table Store: Website Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_group_type' name, 'Mapping Table Store: Group Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_masterlens_store' name, 'Mapping Table Store: Masterlens store list' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_store_company' name, 'Mapping Table Store: Relation between Companies and Stores' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_countries' name, 'Mapping Table Countries: Countries that are used in current DWH' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_customer_type' name, 'Mapping Table Customer Type: Different Customer Types' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_customer_status' name, 'Mapping Table Customer Status: Different Customer Statuses' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_migrate_customer_store' name, 'Mapping Table Mig. Customer Store: Relation between Migration Codes and Migration Stores' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_migrate_store' name, 'Mapping Table Mig. Store: Acquired or Migrated stores going into DWH with their store_id' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_market' name, 'Mapping Table Market: Different Market where we can classify customers' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_store_market_rel' name, 'Mapping Table Store Market Rel: Relationship between Store - Country - Market' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_customer_unsubscribe' name, 'Mapping Table Customer Unsubscribe: Differnet statuses on Customer Unsubscribe' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_customer_origin' name, 'Mapping Table Customer Origin: Different origins on Customer Origin' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_prefix_gender' name, 'Mapping Table Prefix Gender: Relation between prefix and gender' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'gen_gdpr_customer_list' name, 'Mapping Table Prefix GDPR Customer List: Customers that want to be forgotten' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_gen_all' name, 'SSIS PKG for reading Mapping General data from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'mag_eav_attributes' name, 'Mapping Table EAV Entities: List of wanted attributes' description) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_mag_all' name, 'SSIS PKG for reading Mapping data related to Magento from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_type' name, 'Mapping Table Product Types: List of Product Types' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_category' name, 'Mapping Table Product Categories: List of Categories' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_cl_type' name, 'Mapping Table Contact Lens Types: List of CL Types' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_cl_feature' name, 'Mapping Table Contact Lens Features: List of CL Features' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_exclude_products' name, 'Mapping Table Excluded Products: Products not wanted into DWH' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_visibility' name, 'Mapping Table Product Visibility: Options for Product Visibility' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_base_curve' name, 'Mapping Table Param BC: Base Curve' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_diameter' name, 'Mapping Table Param DI: Diameter' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_power' name, 'Mapping Table Param PO: Power' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_cylinder' name, 'Mapping Table Param CY: Cylinder' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_axis' name, 'Mapping Table Param AX: Axis' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_addition' name, 'Mapping Table Param AD: Addition' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_param_dominance' name, 'Mapping Table Param DO: Dominance' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_30_90' name, 'Mapping Table Products in 30 - 90 package size' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_aura' name, 'Mapping Table Products Aura' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_type_oh' name, 'Mapping Table Product Types for OH' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_category_rel' name, 'Mapping Table Product and Category relationship for Products with not Magento Category' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_colour' name, 'Mapping Table Product for Products with not Magento Category and that are Colour Feature' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_family_group' name, 'Mapping Table for Product Family Groups' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_family_group_pf' name, 'Mapping Table for relation betweeen Product Family Groups and Product Family' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'prod_product_family_erp' name, 'Mapping Table for relation betweeen Product Family Groups and Product Family' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_prod_all' name, 'SSIS PKG for reading Mapping Product data from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_order_stage' name, 'Mapping Table Sales Order Stage: List of different stages for an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_order_status' name, 'Mapping Table Sales Order Status: List of different statuses for an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_order_type' name, 'Mapping Table Sales Order Type: List of different types for an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_order_status_magento' name, 'Mapping Table Sales Order Statuses in Magento: List of different statuses that Magento has for an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_payment_method' name, 'Mapping Table Sales Payment Method: List of different payment methods for paying an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_cc_type' name, 'Mapping Table Sales Credit Card Type: List of different cards that can be used' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_shipping_method' name, 'Mapping Table Sales Shipping Method: List of different carriers and methods for shipping an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_price_type' name, 'Mapping Table Sales Price Type: List of different price types for Orders and Products in an Order' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_channel' name, 'Mapping Table Sales Channel: List of different channels from where the customer can arrive' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_marketing_channel' name, 'Mapping Table Sales Marketing Channel: List of different channels from where the customer can arrive - flexible' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_affiliate' name, 'Mapping Table Sales Affiliate: List of different affiliate websites' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_prescription_method' name, 'Mapping Table Sales Prescription Method: List of different presc. methods' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_rank_customer_order_seq_no' name, 'Mapping Table Sales Rank Customer Order Seq No: List of different Ranks' description			
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_rank_qty_time' name, 'Mapping Table Sales Rank Qty Time: List of different Ranks' description			
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_rank_freq_time' name, 'Mapping Table Sales Rank Freq Time: List of different Ranks' description		
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_rank_shipping_days' name, 'Mapping Table Sales Rank Shipping Days: List of different Ranks' description		
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_summer_time_period' name, 'Mapping Table Sales Summer Time Period: Period Ranges per year for the summer period' description		
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_sales_all' name, 'SSIS PKG for reading Mapping data related to Sales from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_ga_medium_ch' name, 'Mapping Table Sales Channel: Mapping between GA Medium and Channel' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_ga_source_mch' name, 'Mapping Table Sales Channel: Mapping between GA Source and Marketing Channel' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_affil_code_mch' name, 'Mapping Table Sales Channel: Mapping between Affil Code (BSC) and Marketing Channel' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_affil_code_like_mch' name, 'Mapping Table Sales Channel: Mapping between Affil Code (BSC) and Marketing Channel for LIKE' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_cc_channel_mch' name, 'Mapping Table Sales Channel: Mapping between Coupon Code Channel and Marketing Channel' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'sales_ch_migrate_channel_mch' name, 'Mapping Table Sales Channel: Mapping between Migrate Channels and Marketing Channel' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_sales_ch_all' name, 'SSIS PKG for reading Mapping data related to Sales Channel from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_activity_group_type' name, 'Mapping Table Activity Group Type: List of different Activity Types and corresponding Groups' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_migrate_store_date' name, 'Mapping Table Migrate Store Date: List of date when migration happened for different stores' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_lapsed_num_days' name, 'Mapping Table Lapsed Num Days: Number of days in order to consider a customer lapsed' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_website_migrate_date' name, 'Mapping Table Website Migrate Date: Migration date for different Website Groups' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_website_register_mapping' name, 'Mapping Table Website Register Mapping: Relation between old stores and VD stores for Register' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_website_old_access_cust_no' name, 'Mapping Table Website Old Access Cust No: Relation Migration Code by Steve O and Website' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'act_rank_cr_time_mm' name, 'Mapping Table Act Rank CR Time: List of different Ranks of months for Create Time of customer' description			
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_act_all' name, 'SSIS PKG for reading Mapping data related to Activity from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 



------------------------------------------------------------------------------------------------------------------------------------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_countries_registered' name, 'Mapping Table VAT Reg. Countries: VAT Registered Countries' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_vat_type_rate' name, 'Mapping Table VAT Type Rate: VAT Type Rates' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_product_type_vat' name, 'Mapping Table Product Type VAT: VAT related Product Types' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_vat_rate' name, 'Mapping Table VAT Rates: VAT Rates Table' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_prof_fee_rate' name, 'Mapping Table Prof Fee Rates: Prof Fee Rates Table' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_prof_fee_fixed_value' name, 'Mapping Table Prof Fee Fixed Value: Prof Fee Fixed Value Table' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'vat_countries_registered_store' name, 'Mapping Table Vat Reg. Countries and Store relation: VAT Registered Countries - Store' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_vat_all' name, 'SSIS PKG for reading Mapping data related to VAT Logic from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 



------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'migra_website' name, 'Mapping Table Migra Website: Website to Migrate: Hist + Migrate' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'migra_order_status' name, 'Mapping Table Migra Order Status: Order Statuses in Migrate Tables' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'migra_website_proforma' name, 'Mapping Table Website Proforma: Proforma orders date' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_migra_all' name, 'SSIS PKG for reading Mapping Migration data from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 



------------

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_stock_method_type' name, 'Mapping Table ERP Stock Method Type: Stock Method Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_stock_batch_type' name, 'Mapping Table ERP Stock Batch Type: Stock Batch Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_stock_adjustment_type' name, 'Mapping Table ERP Stock Adjustment Type: Stock Adjustment Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_stock_movement_type' name, 'Mapping Table ERP Stock Movement Type: Stock Movement Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_stock_movement_period' name, 'Mapping Table ERP Stock Movement Period: Stock Movement Period' description
			union

			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_order_type_erp' name, 'Mapping Table ERP Order Type ERP: Order Type ERP' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_order_status_erp' name, 'Mapping Table ERP Order Status ERP: Order Status ERP' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_shipment_status_erp' name, 'Mapping Table ERP Shipment Status ERP: Shipment Status ERP' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_allocation_status' name, 'Mapping Table ERP Allocation Status: Allocation Status' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_allocation_strategy' name, 'Mapping Table ERP Allocation Strategy: Allocation Strategy' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_allocation_type' name, 'Mapping Table ERP Allocation Type: Allocation Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_allocation_record_type' name, 'Mapping Table ERP Allocation Record Type: Allocation Record Type' description
			union

			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_po_source' name, 'Mapping Table ERP PO Source: PO Source' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_po_type' name, 'Mapping Table ERP PO Type: PO Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_po_status' name, 'Mapping Table ERP PO Status: PO Status' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_pol_status' name, 'Mapping Table ERP POL Status: POL Status' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_pol_problems' name, 'Mapping Table ERP POL Problems: POL Problems' description
			union

			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_wh_shipment_type' name, 'Mapping Table ERP WH Shipment Type: WH Shipment Type' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_receipt_status' name, 'Mapping Table ERP Receipt Status: Receipt Status' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_receipt_line_status' name, 'Mapping Table ERP Receipt Line Status: Receipt Line Status' description
			union
			select 'Landing' database_name, 'Mapping Tables' flow_name, 'erp_receipt_line_sync_status' name, 'Mapping Table ERP Receipt Line Sync Status: Receipt Line Sync Status' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MAP-LND' codPackageType, 'jsola' developer,  'Mapping Tables' flow_name, 'Excel' databaseFrom_name, 'Landing' databaseTo_name, 
				'map_lnd_erp_all' name, 'SSIS PKG for reading Mapping ERP data from Excel Files to Landing' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 