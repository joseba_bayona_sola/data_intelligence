use ControlDB
go

select idMulesoftBatchRun, idMulesoftBatchType, idETLBatchRun, 
	description, dateFrom, dateTo, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_MulesoftBatchRun 

select idMulesoftFlowRun, idMulesoftFlow, idMulesoftBatchRun, 
	mulesoft_flow_id, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_MulesoftFlowRun

select idMulesoftFlowRunMessage, idMulesoftFlowRun, 
	messageTime, messageType, rowAmount, message, 
	ins_ts
from ControlDB.logging.t_MulesoftFlowRunMessage


select idMulesoftSPRun, idMulesoftSP, idMulesoftBatchRun, idMulesoftFlowRun, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_MulesoftSPRun

select idMulesoftSPRunMessage, idMulesoftSPRun, 
	messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
	ins_ts
from ControlDB.logging.t_MulesoftSPRunMessage

--

	-- t_MulesoftBatchRun_v
	select idMulesoftBatchRun, -- idMulesoftBatchType, codMulesoftBatchType, 
		idETLBatchRun, description_rt, dateFrom_rt, dateTo_rt, startTime_rt, finishTime_rt, duration_rt, runStatus_rt, 
		description, dateFrom, dateTo, 
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_MulesoftBatchRun_v
	-- where description = 'vd-customers-sync Data Sync'
	-- where description = 'sc-dw-data-ingestion Delta'
	-- where description = 'sc-dw-data-ingestion Full'
	order by idMulesoftBatchRun desc

	-- t_MulesoftFlowRun_v
	select idMulesoftFlowRun, idMulesoftFlow, mulesoft_app_name, mulesoft_flow_name,
		idMulesoftBatchRun, mB_desc, mB_dateFrom, mB_dateTo, mB_startTime, mB_finishTime, 
		mB_runStatus,
		mulesoft_flow_id, 
		startTime, finishTime, runStatus, 
		ins_ts
	from ControlDB.logging.t_MulesoftFlowRun_v
	where idMulesoftBatchRun = 4477
	order by finishTime

	-- t_MulesoftFlowRunMessage_v
	select idMulesoftFlowRunMessage, idMulesoftFlowRun, idMulesoftBatchRun, mulesoft_app_name, mulesoft_flow_name, startTime, finishTime, runStatus, 
		messageTime, messageType, rowAmount, message, 
		ins_ts
	from ControlDB.logging.t_MulesoftFlowRunMessage_v
	where idMulesoftBatchRun = 4477
	order by messageTime

	-- t_MulesoftSPRun_v
	select idMulesoftSPRun, idMulesoftSP, mulesoft_flow_name, mulesoft_sp_name,
		idMulesoftBatchRun, idMulesoftFlowRun, flow_startTime, flow_finishTime, flow_runStatus, 
		startTime, finishTime, runStatus, 
		ins_ts
	from ControlDB.logging.t_MulesoftSPRun_v
	where idMulesoftBatchRun = 2
	order by startTime

	-- t_MulesoftSPRunMessage_v
	select idMulesoftSPRunMessage, idMulesoftSPRun, mulesoft_flow_name, mulesoft_sp_name, startTime, finishTime, runStatus, 
		-- idMulesoftBatchRun, idMulesoftFlowRun,
		messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
		ins_ts
	from ControlDB.logging.t_MulesoftSPRunMessage_v
	where idMulesoftBatchRun = 3
	order by messageTime