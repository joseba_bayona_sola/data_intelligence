use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'prod' codBusinessArea, 
				'Product' name, 'Data about Product Top Hierarchies, Product Families, Product Stock Items' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Product' flow_name, 'dim_product_type' name, 'Staging Table for Product Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_type' name, 'Warehouse Table for Product Type: Product Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_category' name, 'Staging Table for Category: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_category' name, 'Warehouse Table for Category: Category Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_cl_type' name, 'Staging Table for CL Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_cl_type' name, 'Warehouse Table for CL Type: CL Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_cl_feature' name, 'Staging Table for CL Feature: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_cl_feature' name, 'Warehouse Table for CL Feature: CL Feature Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_manufacturer' name, 'Staging Table for Manufacturer: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_manufacturer' name, 'Warehouse Table for Manufacturer Type: Manufacturer Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_family' name, 'Staging Table for Product Family: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_family' name, 'Warehouse Table for Product Family: Product Family Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_lifecycle' name, 'Staging Table for Product Lifecycle: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_lifecycle' name, 'Warehouse Table for Product Lifecycle: Product Lifecycle Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_visibility' name, 'Staging Table for Product Visibility: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_visibility' name, 'Warehouse Table for Product Visibility: Product Visibility Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_glass_vision_type' name, 'Staging Table for Glass Vision Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_glass_vision_type' name, 'Warehouse Table for Glass Vision Type: Glass Vision Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_glass_package_type' name, 'Staging Table for Glass Package Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_glass_package_type' name, 'Warehouse Table for Glass Package Type: Glass Package Type Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_BC' name, 'Staging Table for Parameter - Base Curve: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_BC' name, 'Warehouse Table for Parameter - Base Curve: BC Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_DI' name, 'Staging Table for Parameter - Diameter: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_DI' name, 'Warehouse Table for Parameter - Diameter: DI Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_PO' name, 'Staging Table for Parameter - Power: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_PO' name, 'Warehouse Table for Parameter - Power: PO Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_CY' name, 'Staging Table for Parameter - Cylinder: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_CY' name, 'Warehouse Table for Parameter - Cylinder: CY Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_AX' name, 'Staging Table for Parameter - Axis: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_AX' name, 'Warehouse Table for Parameter - Axis: AX Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_AD' name, 'Staging Table for Parameter - Addition: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_AD' name, 'Warehouse Table for Parameter - Addition: AD Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_DO' name, 'Staging Table for Parameter - Dominance: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_DO' name, 'Warehouse Table for Parameter - Dominance: DO Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_param_COL' name, 'Staging Table for Parameter - Colour: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_param_COL' name, 'Warehouse Table for Parameter - Colour: COL Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_family' name, 'Staging Table for Product Family: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_family' name, 'Warehouse Table for Product Family: Product Family Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_type_oh' name, 'Staging Table for Product Type OH: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_type_oh' name, 'Warehouse Table for Product Type OH: Product Type Dimension' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_family_group' name, 'Warehouse Table for Product Family Group: Product Type Dimension' description

			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_family_pack_size' name, 'Staging Table for Product Family Pack Size: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_family_pack_size' name, 'Warehouse Table for Product Family Pack Size: Product Family Pack Size Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_price_type_pf' name, 'Staging Table for Price Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_price_type_pf' name, 'Warehouse Table for Price Type: Price Type Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product_family_price' name, 'Staging Table for Product Family Price: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product_family_price' name, 'Warehouse Table for Product Family Price: Product Family Price Dimension' description

			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_product' name, 'Staging Table for Product: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_product' name, 'Warehouse Table for Product: Product Dimension' description
			union
			select 'Staging' database_name, 'Product' flow_name, 'dim_stock_item' name, 'Staging Table for Stock Item: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Product' flow_name, 'dim_stock_item' name, 'Warehouse Table for Stock Item: Stock Item Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Product' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_prod_parent_tables' name, 'SSIS PKG for reading Product data (Parent Dim Tables) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Product' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_prod_parent_tables' name, 'SSIS PKG for reading Product data (Parent Dim Tables) from Staging to Warehouse - Update Dimension' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'Product' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_prod_product_tables' name, 'SSIS PKG for reading Product data (Product Family and Product) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Product' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_prod_product_tables' name, 'SSIS PKG for reading Product data (Product Family and Product) from Staging to Warehouse - Update Dimension' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'Product' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_prod_product_erp_tables' name, 'SSIS PKG for reading Product data (ERP) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Product' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_prod_product_erp_tables' name, 'SSIS PKG for reading Product data (ERP) from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_type' name, 'GET SP for reading Product Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_type' name, 'GET SP for reading Product Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_type' name, 'MERGE SP for upading Product Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_category' name, 'GET SP for reading Category data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_category' name, 'GET SP for reading Category data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_category' name, 'MERGE SP for upading Category Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_cl_type' name, 'GET SP for reading CL Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_cl_type' name, 'GET SP for reading CL Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_cl_type' name, 'MERGE SP for upading CL Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_cl_feature' name, 'GET SP for reading CL Feature data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_cl_feature' name, 'GET SP for reading CL Feature data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_cl_feature' name, 'MERGE SP for upading CL Feature Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_manufacturer' name, 'GET SP for reading Manufacturer data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_manufacturer' name, 'GET SP for reading Manufacturer data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_manufacturer' name, 'MERGE SP for upading Manufacturer Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_lifecycle' name, 'GET SP for reading Product Lifecycle data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_lifecycle' name, 'GET SP for reading Product Lifecycle data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_lifecycle' name, 'MERGE SP for upading Product Lifecycle Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_visibility' name, 'GET SP for reading Product Visibility data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_visibility' name, 'GET SP for reading Product Visibility data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_visibility' name, 'MERGE SP for upading Product Visibility Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_glass_vision_type' name, 'GET SP for reading Glass Vision Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_glass_vision_type' name, 'GET SP for reading Glass Vision Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_glass_vision_type' name, 'MERGE SP for upading Glass Vision Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_glass_package_type' name, 'GET SP for reading Glass Package Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_glass_package_type' name, 'GET SP for reading Glass Package Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_glass_package_type' name, 'MERGE SP for upading Glass Package Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_bc' name, 'GET SP for reading Parameter - BC data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_bc' name, 'GET SP for reading Parameter - BC data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_bc' name, 'MERGE SP for upading Parameter - BC Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_di' name, 'GET SP for reading Parameter - DI data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_di' name, 'GET SP for reading Parameter - DI data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_di' name, 'MERGE SP for upading Parameter - DI Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_po' name, 'GET SP for reading Parameter - PO data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_po' name, 'GET SP for reading Parameter - PO data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_po' name, 'MERGE SP for upading Parameter - PO Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_cy' name, 'GET SP for reading Parameter - CY data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_cy' name, 'GET SP for reading Parameter - CY data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_cy' name, 'MERGE SP for upading Parameter - CY Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_ax' name, 'GET SP for reading Parameter - AX data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_ax' name, 'GET SP for reading Parameter - AX data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_ax' name, 'MERGE SP for upading Parameter - AX Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_ad' name, 'GET SP for reading Parameter - AD data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_ad' name, 'GET SP for reading Parameter - AD data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_ad' name, 'MERGE SP for upading Parameter - AD Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_do' name, 'GET SP for reading Parameter - DO data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_do' name, 'GET SP for reading Parameter - DO data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_do' name, 'MERGE SP for upading Parameter - DO Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_param_col' name, 'GET SP for reading Parameter - COL data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_param_col' name, 'GET SP for reading Parameter - COL data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_param_col' name, 'MERGE SP for upading Parameter - COL Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_family' name, 'GET SP for reading Product Family data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_family' name, 'GET SP for reading Product Family data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_family' name, 'MERGE SP for upading Product Family Dimension data' description
			
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_aux_prod_product_category' name, 'AUX SP for relating Products with Categories' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_aux_prod_product_product_type_oh' name, 'AUX SP for relating Products with Product Type OH' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_aux_prod_product_product_type_vat' name, 'AUX SP for relating Products with Product Type VAT' description
			
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_type_oh' name, 'GET SP for reading Product Type OH data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_type_oh' name, 'GET SP for reading Product Type OH data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_type_oh' name, 'MERGE SP for upading Product Type OH Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_parent_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_family_group' name, 'GET SP for reading Product Family Group data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_family_group' name, 'GET SP for reading Product Family Group data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_parent_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_family_group' name, 'MERGE SP for upading Product Family Group Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_erp_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_family_pack_size' name, 'GET SP for reading Product Family Pack Size data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_family_pack_size' name, 'GET SP for reading Product Family Pack Size data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_family_pack_size' name, 'MERGE SP for upading Product Family Pack Size Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_erp_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_price_type_pf' name, 'GET SP for reading Price Type PF data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_price_type_pf' name, 'GET SP for reading Price Type PF data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_price_type_pf' name, 'MERGE SP for upading Price Type PF Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_erp_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product_family_price' name, 'GET SP for reading Product Family Price data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product_family_price' name, 'GET SP for reading Product Family Price data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product_family_price' name, 'MERGE SP for upading Product Family Price Dimension data' description

			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_erp_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_product' name, 'GET SP for reading Product data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_product' name, 'GET SP for reading Product data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_product' name, 'MERGE SP for upading Product Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_prod_product_erp_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_prod_stock_item' name, 'GET SP for reading Stock Item data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_prod_stock_item' name, 'GET SP for reading Stock Item data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_prod_product_erp_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_prod_stock_item' name, 'MERGE SP for upading Stock Item Dimension data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 

