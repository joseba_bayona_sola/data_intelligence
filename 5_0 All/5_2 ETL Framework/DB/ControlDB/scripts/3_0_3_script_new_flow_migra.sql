use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'MIGRA-LND' codFlowType, 'MIGRA' codDWHTableType, 'migra' codBusinessArea, 
				'Migration Proforma' name, 'Taking data from DW_Proforma Data Warehouse' description
			union
			select 'MIGRA-LND' codFlowType, 'MIGRA' codDWHTableType, 'migra' codBusinessArea, 
				'Migration GetLenses' name, 'Taking data from DW_GetLenses Data Warehouse' description
				
			union
			select 'MIGRA-LND' codFlowType, 'MIGRA' codDWHTableType, 'migra' codBusinessArea, 
				'Migration Hist Tables' name, 'Taking data from DW_GetLenses Data Warehouse - Hist Tables' description

			union
			select 'MIGRA-LND' codFlowType, 'MIGRA' codDWHTableType, 'migra' codBusinessArea, 
				'Migration Migration Data' name, 'Taking data from MySQL DB from different Migrated Companies' description

				) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Migration Proforma' flow_name, 'dw_stores_full' name, 'Landing Table for dw_stores_full table from DW_Proforma' description
			union
			select 'Landing' database_name, 'Migration Proforma' flow_name, 'order_headers' name, 'Landing Table for order_headers table from DW_Proforma' description
			union
			select 'Landing' database_name, 'Migration Proforma' flow_name, 'order_lines' name, 'Landing Table for order_lines table from DW_Proforma' description

			union
			select 'Landing' database_name, 'Migration Hist Tables' flow_name, 'dw_hist_order' name, 'Landing Table for dw_hist_order table from DW_GetLenses' description
			union
			select 'Landing' database_name, 'Migration Hist Tables' flow_name, 'dw_hist_order_item' name, 'Landing Table for dw_hist_order_item table from DW_GetLenses' description
			union
			select 'Landing' database_name, 'Migration Hist Tables' flow_name, 'dw_hist_shipment' name, 'Landing Table for dw_hist_shipment table from DW_GetLenses' description
			union
			select 'Landing' database_name, 'Migration Hist Tables' flow_name, 'dw_hist_shipment_item' name, 'Landing Table for dw_hist_shipment_item table from DW_GetLenses' description

			union
			select 'Landing' database_name, 'Migration Migration Data' flow_name, 'migrate_customerdata' name, 'Landing Table for migrate_customerdata table from MySQL: Different Companies' description
			union
			select 'Landing' database_name, 'Migration Migration Data' flow_name, 'migrate_orderdata' name, 'Landing Table for migrate_orderdata table from MySQL: Different Companies' description
			union
			select 'Landing' database_name, 'Migration Migration Data' flow_name, 'migrate_orderlinedata' name, 'Landing Table for migrate_orderlinedata table from MySQL: Different Companies' description
			union
			select 'Landing' database_name, 'Migration Migration Data' flow_name, 'migrate_order_id_mapping' name, 'Landing Table for migrate_order_id_mapping table from MySQL: Different Companies' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'MIGRA-LND' codPackageType, 'jsola' developer,  'Migration Proforma' flow_name, 'DW_Proforma' databaseFrom_name, 'Landing' databaseTo_name, 
				'migpr_lnd_all_tables' name, 'SSIS PKG for reading DW_Proforma tables to Landing' description
			union
			select 'MIGRA-LND' codPackageType, 'jsola' developer,  'Migration GetLenses' flow_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'miggl_lnd_all_tables' name, 'SSIS PKG for reading DW_GetLenses tables to Landing' description
				
			union
			select 'MIGRA-LND' codPackageType, 'jsola' developer,  'Migration Hist Tables' flow_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'mighist_lnd_all_tables' name, 'SSIS PKG for reading DW_GetLenses Hist tables to Landing' description	

			union
			select 'MIGRA-LND' codPackageType, 'jsola' developer,  'Migration Hist Tables' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migmysql_lnd_all_tables' name, 'SSIS PKG for reading MySQL different databases tables to Landing' description	
				) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'migpr_lnd_all_tables' package_name, 'DW_Proforma' databaseFrom_name, 'Landing' databaseTo_name, 
				'migpr_lnd_get_dw_stores_full' name, 'GET SP for reading dw_stores_full data from DW_Proforma to Landing' description
			union	
			select 'GET' codSPType, 'jsola' developer,  'migpr_lnd_all_tables' package_name, 'DW_Proforma' databaseFrom_name, 'Landing' databaseTo_name, 
				'migpr_lnd_get_order_headers' name, 'GET SP for reading order_headers data from DW_Proforma to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migpr_lnd_all_tables' package_name, 'DW_Proforma' databaseFrom_name, 'Landing' databaseTo_name, 
				'migpr_lnd_get_order_lines' name, 'GET SP for reading order_lines data from DW_Proforma to Landing' description

			union
			select 'GET' codSPType, 'jsola' developer,  'mighist_lnd_all_tables' package_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'mighist_lnd_get_dw_hist_order' name, 'GET SP for reading dw_hist_order data from DW_GetLenses to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'mighist_lnd_all_tables' package_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'mighist_lnd_get_dw_hist_order_item' name, 'GET SP for reading dw_hist_order_item data from DW_GetLenses to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'mighist_lnd_all_tables' package_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'mighist_lnd_get_dw_hist_shipment' name, 'GET SP for reading dw_hist_shipment data from DW_GetLenses to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'mighist_lnd_all_tables' package_name, 'DW_GetLenses' databaseFrom_name, 'Landing' databaseTo_name, 
				'mighist_lnd_get_dw_hist_shipment_item' name, 'GET SP for reading dw_hist_shipment_item data from DW_GetLenses to Landing' description
			
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglb150324_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from lb150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglb150324_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from lb150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglb150324_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from lb150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglb150324_lnd_get_migrate_order_id_mapping' name, 'GET SP for reading migrate_order_id_mapping data from lb150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglh150324_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from lh150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglh150324_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from lh150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglh150324_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from lh150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglh150324_lnd_get_migrate_order_id_mapping' name, 'GET SP for reading migrate_order_id_mapping data from lh150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migvd150324_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from vd150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migvd150324_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from vd150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migvd150324_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from vd150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migvd150324_lnd_get_migrate_order_id_mapping' name, 'GET SP for reading migrate_order_id_mapping data from vd150324 to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migVisioOptik_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from VisioOptik to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migVisioOptik_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from VisioOptik to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'migVisioOptik_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from VisioOptik to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensway_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from Lensway to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensway_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from Lensway to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensway_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from Lensway to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenson_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from Lenson to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenson_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from Lenson to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenson_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from Lenson to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenswaynl_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from Lensway NL to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenswaynl_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from Lensway NL to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenswaynl_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from Lensway NL to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensonnl_lnd_get_migrate_customerdata' name, 'GET SP for reading migrate_customerdata data from Lenson NL to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensonnl_lnd_get_migrate_orderdata' name, 'GET SP for reading migrate_orderdata data from Lenson NL to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensonnl_lnd_get_migrate_orderlinedata' name, 'GET SP for reading migrate_orderlinedata data from Lenson NL to Landing' description
			
			union
			select 'MERGE' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglensonnl_lnd_merge_migrate_updates' name, 'MERGE SP for updating LENSON NL data' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenswaynl_lnd_merge_migrate_updates' name, 'MERGE SP for updating LENSWAY NL data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'migmysql_lnd_all_tables' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'miglenswaynl_lnd_get_migrate_orderdata_magento' name, 'GET SP for reading migrate_orderdata data from Lensway NL to Landing' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 