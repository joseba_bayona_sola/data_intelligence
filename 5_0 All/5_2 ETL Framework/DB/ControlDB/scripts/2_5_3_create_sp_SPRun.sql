use ControlDB
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Start record for the SP Run to the logging Table - Return Unique Record Number
-- ==========================================================================================

create procedure logging.logSPRun_start_sp
	@sp_name varchar(100), 
	@idETLBatchRun bigint, @idPackageRun bigint,
	@idSPRun bigint OUTPUT
as

begin
	set nocount on;

	insert into ControlDB.logging.t_SPRun(idSP, idETLBatchRun, idPackageRun, 
		startTime, runStatus)

		select sp.idSP, t.idETLBatchRun, t.idPackageRun, 
			getutcdate() startTime, 'RUNNING' runStatus
		from
				(select @sp_name sp_name, @idETLBatchRun idETLBatchRun, @idPackageRun idPackageRun) t
			inner join
				ControlDB.config.t_SP sp on t.sp_name = sp.sp_name;

	set @idSPRun = SCOPE_IDENTITY();

	return @idSPRun;
end 

go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Finish record for the SP Run to the logging Table 
-- ==========================================================================================

create procedure logging.logSPRun_stop_sp
	@idSPRun bigint, 
	@runStatus varchar(50)
as

begin
	set nocount on;

	update ControlDB.logging.t_SPRun
	set
		finishTime = getutcdate(),
		runStatus = @runStatus
	where idSPRun = @idSPRun;

end 

go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Message record for the SP Run Message to the logging Table 
-- ==========================================================================================

create procedure logging.logSPRun_message_sp
	@idSPRun bigint, 
	@rowAmountSelect int = NULL, @rowAmountInsert int = NULL, @rowAmountUpdate int = NULL, @rowAmountDelete int = NULL,  
	@message varchar(500)
as

begin
	set nocount on;

	insert into ControlDB.logging.t_SPRunMessage (idSPRun, messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message) values
		(@idSPRun, getutcdate(), @rowAmountSelect, @rowAmountInsert, @rowAmountUpdate, @rowAmountDelete, @message);

end 

go 




