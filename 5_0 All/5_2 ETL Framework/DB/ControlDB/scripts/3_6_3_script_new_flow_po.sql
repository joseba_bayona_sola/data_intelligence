use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'po' codBusinessArea, 
				'Purchasing' name, 'Data about Purchasing in ERP: Purchase Orders and Purchase Order Lines' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Purchasing' flow_name, 'dim_po_source' name, 'Staging Table for PO Source: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_po_source' name, 'Warehouse Table for PO Source: PO Source Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_po_type' name, 'Staging Table for PO Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_po_type' name, 'Warehouse Table for PO Type: PO Type Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_po_status' name, 'Staging Table for PO Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_po_status' name, 'Warehouse Table for PO Status: PO Status Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_pol_status' name, 'Staging Table for POL Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_pol_status' name, 'Warehouse Table for POL Status: POL Status Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_pol_problems' name, 'Staging Table for POL Problems: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_pol_problems' name, 'Warehouse Table for POL Problems: POL Problems Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_wh_operator' name, 'Staging Table for WH Operator: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_wh_operator' name, 'Warehouse Table for WH Operator: WH Operator Dimension' description

			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'dim_purchase_order' name, 'Staging Table for Purchase Order: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'dim_purchase_order' name, 'Warehouse Table for Purchase Order: Purchase Order Dimension' description
			union
			select 'Staging' database_name, 'Purchasing' flow_name, 'fact_purchase_order_line' name, 'Staging Table for Purchase Order Line: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Purchasing' flow_name, 'fact_purchase_order_line' name, 'Warehouse Table for Purchase Order Line: Purchase Order Line Fact' description

			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Purchasing' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_po_tables' name, 'SSIS PKG for reading Purchasing data (ERP) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Purchasing' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_po_tables' name, 'SSIS PKG for reading Purchasing data (ERP) from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_po_source' name, 'GET SP for reading PO Source data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_po_source' name, 'GET SP for reading PO Source data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_po_source' name, 'MERGE SP for upading PO Source Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_po_type' name, 'GET SP for reading PO Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_po_type' name, 'GET SP for reading PO Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_po_type' name, 'MERGE SP for upading PO Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_po_status' name, 'GET SP for reading PO Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_po_status' name, 'GET SP for reading PO Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_po_status' name, 'MERGE SP for upading PO Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_pol_status' name, 'GET SP for reading POL Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_pol_status' name, 'GET SP for reading POL Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_pol_status' name, 'MERGE SP for upading POL Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_pol_problems' name, 'GET SP for reading POL Problems data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_pol_problems' name, 'GET SP for reading POL Problems data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_pol_problems' name, 'MERGE SP for upading POL Problems Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_wh_operator' name, 'GET SP for reading WH Operator data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_wh_operator' name, 'GET SP for reading WH Operator data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_wh_operator' name, 'MERGE SP for upading WH Operator Dimension data' description
		
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_purchase_order' name, 'GET SP for reading Purchase Order data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_purchase_order' name, 'GET SP for reading Purchase Order data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_purchase_order' name, 'MERGE SP for upading Purchase Order Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_po_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_po_purchase_order_line' name, 'GET SP for reading Purchase Order Line data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_po_purchase_order_line' name, 'GET SP for reading Purchase Order Line data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_po_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_po_purchase_order_line' name, 'MERGE SP for upading Purchase Order Line Fact data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 


