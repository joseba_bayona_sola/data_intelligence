

-- t_ETLBatchRunRT
select idETLBatchRun, idETLBatchType, idFlowType, idPackage,
	description, dateFrom, dateTo, 
	startTime, finishTime, runStatus, -- message,
	ins_ts
from ControlDB.logging.t_ETLBatchRunRT; 

-- t_PackageRunRT
select idPackageRun, idPackage, idETLBatchRun, idPackageRun_parent, 
	idExecution, startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_PackageRunRT

-- t_PackageRunRTMessage
select idPackageRunMessage, idPackageRun, 
	messageTime, messageType, rowAmount, message, 
	ins_ts
from ControlDB.logging.t_PackageRunRTMessage 

-- t_SPRunRT
select idSPRun, idSP, idETLBatchRun, idPackageRun, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_SPRunRT

-- t_SPRunRTMessage
select idSPRunMessage, idSPRun, 
	messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
	ins_ts	
from ControlDB.logging.t_SPRunRTMessage

-- 

	-- t_ETLBatchRunRT_v
	select idETLBatchRun, codETLBatchType, codFlowType, idPackage, flow_name, package_name,
		description, dateFrom, dateTo, 
		startTime, finishTime, duration, runStatus, -- message,
		ins_ts
	from ControlDB.logging.t_ETLBatchRunRT_v
	-- where package_name = 'VisionDirectDataWarehouseLoad' and description = 'VD Data Warehouse Load - Daily'
	-- where package_name in ('VisionDirectDataWarehouseLoadNoSRC', 'VisionDirectDataWarehouseLoadCustomers')
	-- where package_name = 'VisionDirectDataWarehouseLoadERP' and runStatus = 'OK'
	-- where package_name = 'VisionDirectDataWarehouseLoadCustomers'
	order by idETLBatchRun desc

	-- t_PackageRunRT_v
	select idPackageRun, idPackage, flow_name, package_name, 
		-- idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
		idPackageRun_parent, 
		idExecution, startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_PackageRunRT_v
	where idETLBatchRun = 15

		select idPackageRun, idPackage, flow_name, package_name, 
			idETLBatchRun, etlB_desc, etlB_dateFrom, etlB_dateTo, etlB_startTime, etlB_finishTime, etlB_runStatus, -- etlB_message,
			idPackageRun_parent, 
			idExecution, startTime, finishTime, duration, runStatus, 
			ins_ts
		from ControlDB.logging.t_PackageRunRT_v
		where package_name = 'LoadAll_src_lnd'
		order by idETLBatchRun desc

	-- t_PackageRunMessageRT_v
	select idPackageRunMessage, prm.idPackageRun, prm.flow_name, prm.package_name, prm.idExecution, prm.startTime, prm.finishTime, prm.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunRTMessage_v prm
		inner join
			ControlDB.logging.t_PackageRunRT_v pr on prm.idPackageRun = pr.idPackageRun
	where pr.idETLBatchRun = 1
		--and prm.package_name = 'lnd_stg_sales_fact_order' --  // stg_dwh_sales_fact_order
	order by prm.package_name, prm.message




	-- t_SPRun_v
	select idSPRun, idSP, package_name, sp_name,
		idETLBatchRun, idPackageRun, idExecution, pkg_startTime, pkg_finishTime, pkg_runStatus, 
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_SPRunRT_v
	where idETLBatchRun = 2128
	order by package_name, sp_name

	-- t_SPRunMessage_v
	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where sp.idETLBatchRun = 66
		--and spm.package_name = 'lnd_stg_sales_fact_order' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
	-- order by spm.package_name, spm.sp_name
	order by spm.finishTime

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	

	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where -- spm.package_name = 'srcmag_lndrt_del_customer_entity_flat' --  // lnd_stg_sales_fact_order - stg_dwh_sales_fact_order // lnd_stg_act_fact_activity - stg_dwh_act_fact_activity
		spm.sp_name = 'srcmag_lndrt_del_customer_entity_flat'
	order by spm.package_name, spm.sp_name, spm.messageTime desc

	select *
	from ControlDB.dbo.sysssislog
	where executionid = '86236324-7BFE-4487-BF0F-BB3F5C5048FF'
	order by event, source

	---------------------------------------------------

	select spm.idSPRunMessage, spm.idSPRun, spm.package_name, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunRTMessage_v spm
		inner join
			ControlDB.logging.t_SPRunRT_v sp on spm.idSPRun = sp.idSPRun
	where spm.sp_name = 'srcmend_lnd_get_order_order'
	order by spm.finishTime desc
	
