use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'FACT' codDWHTableType, 'migra' codBusinessArea, 
				'Sales Fact Hist' name, 'Taking data from Migra Hist Tables - Load into new DWH' description

			union
			select 'SRC-DWH' codFlowType, 'FACT' codDWHTableType, 'migra' codBusinessArea, 
				'Sales Fact Migrate' name, 'Taking data from Migra Migrate Tables - Load into new DWH' description

				) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'match_customer' name, 'Matching Table for Customers from Hist - Migrate Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'match_order_header' name, 'Matching Table for Order Headers from Hist - Migrate Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'match_order_line' name, 'Matching Table for Order Lines from Hist - Migrate Tables' description
			union

			select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'sales_flat_order_hist' name, 'Order Header Magento Structured Table for Hist Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'sales_flat_order_item_hist' name, 'Order Line Magento Structured Table for Hist Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Hist' flow_name, 'sales_flat_order_address_hist' name, 'Order Address Magento Structured Table for Hist Tables' description
			union

			select 'Landing' database_name, 'Sales Fact Migrate' flow_name, 'sales_flat_order_migrate' name, 'Order Header Magento Structured Table for Migrate Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Migrate' flow_name, 'sales_flat_order_item_migrate' name, 'Order Line Magento Structured Table for Migrate Tables' description
			union
			select 'Landing' database_name, 'Sales Fact Migrate' flow_name, 'sales_flat_order_address_migrate' name, 'Order Address Magento Structured Table Migrate Hist Tables' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 


insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Sales Fact Hist' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_sales_fact_order_hist' name, 'SSIS PKG for reading Order Fact data (HIST) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'Sales Fact Hist' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_sales_fact_order_migrate' name, 'SSIS PKG for reading Order Fact data (MIGRATE) from Landing to Staging - Prepare to insert in Warehouse' description) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_hist_prepare' name, 'GET SP for preparing HIST orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lb150324_prepare' name, 'GET SP for preparing MIGRATE (Lensbase) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lh150324_prepare' name, 'GET SP for preparing MIGRATE (Lenshome) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_vd150324_prepare' name, 'GET SP for preparing MIGRATE (Vision Direct) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_VisioOptik_prepare' name, 'GET SP for preparing MIGRATE (Visio Optik) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lensway_prepare' name, 'GET SP for preparing MIGRATE (Lensway) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lenson_prepare' name, 'GET SP for preparing MIGRATE (Lenson) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lensonnl_prepare' name, 'GET SP for preparing MIGRATE (Lenson NL) orders for Migration' description
			union
			select 'GET' codSPType, 'jsola' developer,  'ETLBatchRun_prepare' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_migrate_lenswaynl_prepare' name, 'GET SP for preparing MIGRATE (Lensway NL) orders for Migration' description
			union

			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header_hist' name, 'GET SP for reading Order Header data (HIST) from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header_product_type_hist' name, 'GET SP for reading Order Header (HIST) Product Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_line_hist' name, 'GET SP for reading Order Line data (HIST) from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header_migrate' name, 'GET SP for reading Order Header data (HIST) from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_header_product_type_migrate' name, 'GET SP for reading Order Header (HIST) Product Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_order_line_migrate' name, 'GET SP for reading Order Line data (HIST) from Landing (data in different tables) to Staging' description
			union

			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o_hist' name, 'GET SP for reading Order Header data: O entities' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o_i_s_cr_hist' name, 'GET SP for reading Order Header data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_store_cust_hist' name, 'GET SP for reading Order Header data: Store - Customer Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_order_hist' name, 'GET SP for reading Order Header data: Order related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_mark_hist' name, 'GET SP for reading Order Header data: Marketing related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_measures_hist' name, 'GET SP for reading Order Header data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_vat_hist' name, 'GET SP for reading Order Header data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_oh_pt_hist' name, 'GET SP for reading Order Header data: OH - PT relation' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_o_i_s_cr_hist' name, 'GET SP for reading Order Line data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_product_hist' name, 'GET SP for reading Order Line data: Product Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_measures_hist' name, 'GET SP for reading Order Line data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_vat_hist' name, 'GET SP for reading Order Line data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_hist' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_dim_fact_hist' name, 'GET SP for reading preparing Order Dim - Fact Aux Tables using different Auxiliar Tables' description
			union

			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o_migrate' name, 'GET SP for reading Order Header data: O entities' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_o_i_s_cr_migrate' name, 'GET SP for reading Order Header data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_store_cust_migrate' name, 'GET SP for reading Order Header data: Store - Customer Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_order_migrate' name, 'GET SP for reading Order Header data: Order related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_dim_mark_migrate' name, 'GET SP for reading Order Header data: Marketing related DIM data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_measures_migrate' name, 'GET SP for reading Order Header data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_vat_migrate' name, 'GET SP for reading Order Header data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_header_oh_pt_migrate' name, 'GET SP for reading Order Header data: OH - PT relation' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_o_i_s_cr_migrate' name, 'GET SP for reading Order Line data: O-I-S-CR relations' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_product_migrate' name, 'GET SP for reading Order Line data: Product Info' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_measures_migrate' name, 'GET SP for reading Order Line data: Measures Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_line_vat_migrate' name, 'GET SP for reading Order Line data: VAT Attributes' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_fact_order_migrate' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_aux_sales_order_dim_fact_migrate' name, 'GET SP for reading preparing Order Dim - Fact Aux Tables using different Auxiliar Tables' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 