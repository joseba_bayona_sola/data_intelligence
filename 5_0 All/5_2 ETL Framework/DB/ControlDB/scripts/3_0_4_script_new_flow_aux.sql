use ControlDB
go 


-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Magento Data' flow_name, 'mag_core_config_data_store' name, 'Landing Auxilary Table core config data related to stores' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mag_sales_flat_creditmemo' name, 'Landing Auxilary Table - Credit Memo Headers data grouped by Order ID' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mag_sales_flat_creditmemo_item' name, 'Landing Auxilary Table - Credit Memo Items data grouped by Order ID - Item ID' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mag_edi_stock_item' name, 'Landing Auxilary Table - EDI Parameters Clean Data' description
			union
			select 'Landing' database_name, 'Magento Data' flow_name, 'mag_catalog_product_price' name, 'Landing Auxilary Table - Product Prices' description

			union
			select 'Landing' database_name, 'Product' flow_name, 'prod_product_category' name, 'Landing Auxilary Table for Product - Category relationship' description
			union 
			select 'Landing' database_name, 'Product' flow_name, 'prod_product_product_type_oh' name, 'Landing Auxilary Table for Product - Product Type OH relationship' description
			union 
			select 'Landing' database_name, 'Product' flow_name, 'prod_product_product_type_vat' name, 'Landing Auxilary Table for Product - Product Type VAT relationship' description


			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_customer_unsubscribe_magento' name, 'Landing Auxilary Table for Customer Unsubscribe Magento' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_customer_unsubscribe_dotmailer' name, 'Landing Auxilary Table for Customer Unsubscribe DotMailer' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_customer_unsubscribe_textmessage' name, 'Landing Auxilary Table for Customer Unsubscribe Text Message' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_customer_reminder' name, 'Landing Auxilary Table for Customer Reminder' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_customer_reorder' name, 'Landing Auxilary Table for Customer Reorder' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gen_dim_customer' name, 'Landing Auxilary Table for Customer data' description
			union
			select 'Landing' database_name, 'Customer' flow_name, 'gdpr_customer_list' name, 'Landing Auxilary Table for Customer data' description

			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_o' name, 'Landing Auxilary Table for Order Header: O entities' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_o_i_s_cr' name, 'Landing Auxilary Table for Order Header: O-I-S-CR relations' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_store_cust' name, 'Landing Auxilary Table for Order Header: Store - Customer Info' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_dim_order' name, 'Landing Auxilary Table for Order Header: Order related DIM data' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_dim_mark' name, 'Landing Auxilary Table for Order Header: Marketing related DIM data' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_measures' name, 'Landing Auxilary Table for Order Header: Measures Attributes' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_vat' name, 'Landing Auxilary Table for Order Header: VAT Attributes' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_header_oh_pt' name, 'Landing Auxilary Table for Order Header: OH - PT relation' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_line_o_i_s_cr' name, 'Landing Auxilary Table for Order Line: O-I-S-CR relations' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_line_product' name, 'Landing Auxilary Table for Order Line: Product Info' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_line_measures' name, 'Landing Auxilary Table for Order Line: Measures Attributes' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_order_line_vat' name, 'Landing Auxilary Table for Order Line: VAT Attributes' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_dim_order_header' name, 'Landing Auxilary Table for Order Header Dim' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_fact_order_line_aud' name, 'Landing Auxilary Table for Order Line Fact' description
			union
			select 'Landing' database_name, 'Sales Fact' flow_name, 'sales_exchange_rate' name, 'Landing Auxilary Table for Exchange Rate values' description
			
			union
			select 'Landing' database_name, 'Activity Fact' flow_name, 'act_fact_activity_sales' name, 'Landing Auxilary Table for Activity Sales Fact' description
			union
			select 'Landing' database_name, 'Activity Fact' flow_name, 'act_fact_activity_other' name, 'Landing Auxilary Table for Activity Other Fact' description
			union
			select 'Landing' database_name, 'Activity Fact' flow_name, 'act_customer_registration' name, 'Landing Auxilary Table for Customer Register Data' description
			union
			select 'Landing' database_name, 'Activity Fact' flow_name, 'act_customer_cr_reg' name, 'Landing Auxilary Table for Customer Create - Register Data' description
			
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 



-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'LoadAll_src_lnd' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_aux_mag_tables' name, 'GET SP for executing Auxiliary Tables Load' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_aux_mag_sales_flat_creditmemo' name, 'GET SP for inserting data in Credit Memo Order Aux Table' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_aux_mag_sales_flat_creditmemo_item' name, 'GET SP for inserting data in Credit Memo Item Aux Table' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_other_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_aux_mag_edi_stock_item' name, 'GET SP for inserting data in EDI Parameters Clean Data Aux Table' description
			union
			select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_aux_mag_catalog_product_price' name, 'GET SP for inserting data in Product Price Aux Table' description

			union
			select 'MERGE' codSPType, 'jsola' developer,  'LoadAll_src_lnd' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_merge_aux_gdpr_customer_list' name, 'MERGE SP for inserting GDPR Customer List in Aux Table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_merge_aux_gdpr_customer_entity' name, 'MERGE SP for inserting GDPR Customer List in Aux Table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'srcmag_lnd_entity_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_merge_aux_gdpr_customer_address_entity' name, 'MERGE SP for inserting GDPR Customer List in Aux Table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_merge_aux_gdpr_sales_flat_order' name, 'MERGE SP for inserting GDPR Customer List in Aux Table' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'srcmag_lnd_flat_o_i_s_cr_tables' package_name, 'Landing' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_merge_aux_gdpr_sales_flat_order_address' name, 'MERGE SP for inserting GDPR Customer List in Aux Table' description
				) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 
