use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'sales' codBusinessArea, 
				'Sales Marketing' name, 'Data about Sales related Marketing Dimensions' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_reminder_type' name, 'Staging Table for Reminder Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_reminder_type' name, 'Warehouse Table for Reminder Type: Reminder Type Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_reminder_period' name, 'Staging Table for Reminder Period: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_reminder_period' name, 'Warehouse Table for Reminder Period: Reminder Period Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_channel' name, 'Staging Table for Channel: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_channel' name, 'Warehouse Table for Channel: Channel Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_marketing_channel' name, 'Staging Table for Marketing Channel: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_marketing_channel' name, 'Warehouse Table for Marketing Channel: Marketing Channel Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_affiliate' name, 'Staging Table for Affiliate: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_affiliate' name, 'Warehouse Table for Affiliate: Affiliate Period Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_group_coupon_code' name, 'Staging Table for Group Coupon Code: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_group_coupon_code' name, 'Warehouse Table for Group Coupon Code: Group Coupon Code Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_coupon_code' name, 'Staging Table for Coupon Code: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_coupon_code' name, 'Warehouse Table for Coupon Code: Coupon Code Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_device_category' name, 'Staging Table for Device Category: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_device_category' name, 'Warehouse Table for Device Category: Device Category Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_device_brand' name, 'Staging Table for Device Brand: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_device_brand' name, 'Warehouse Table for Device Brand: Device Brand Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_device_model' name, 'Staging Table for Device Model: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_device_model' name, 'Warehouse Table for Device Model: Device Model Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_device_browser' name, 'Staging Table for Device Browser: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_device_browser' name, 'Warehouse Table for Device Browser: Browser Category Dimension' description
			union
			select 'Staging' database_name, 'Sales Marketing' flow_name, 'dim_device_os' name, 'Staging Table for Device OS: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Sales Marketing' flow_name, 'dim_device_os' name, 'Warehouse Table for Device OS: Device OS Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Sales Marketing' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_sales_dim_marketing' name, 'SSIS PKG for reading Sales Marketing DIM data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Sales Marketing' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_sales_dim_marketing' name, 'SSIS PKG for reading Sales Marketing DIM  data from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_reminder_type' name, 'GET SP for reading Reminder Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_reminder_type' name, 'GET SP for reading Reminder Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_reminder_type' name, 'MERGE SP for upading Reminder Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_reminder_period' name, 'GET SP for reading Reminder Period data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_reminder_period' name, 'GET SP for reading Reminder Period data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_reminder_period' name, 'MERGE SP for upading Reminder Period Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_channel' name, 'GET SP for reading Channel data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_channel' name, 'GET SP for reading Channel data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_channel' name, 'MERGE SP for upading Channel Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_marketing_channel' name, 'GET SP for reading Marketing Channel data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_marketing_channel' name, 'GET SP for reading Marketing Channel data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_marketing_channel' name, 'MERGE SP for upading Marketing Channel Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_affiliate' name, 'GET SP for reading Affiliate data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_affiliate' name, 'GET SP for reading Affiliate data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_affiliate' name, 'MERGE SP for upading Affiliate Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_group_coupon_code' name, 'GET SP for reading Group Coupon Code data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_group_coupon_code' name, 'GET SP for reading Group Coupon Code data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_group_coupon_code' name, 'MERGE SP for upading Group Coupon Code Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_coupon_code' name, 'GET SP for reading Coupon Code data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_coupon_code' name, 'GET SP for reading Coupon Code data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_coupon_code' name, 'MERGE SP for upading Coupon Code Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_device_category' name, 'GET SP for reading Device Category data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_device_category' name, 'GET SP for reading Device Category data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_device_category' name, 'MERGE SP for upading Device Category Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_device_brand' name, 'GET SP for reading Device Brand data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_device_brand' name, 'GET SP for reading Device Brand data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_device_brand' name, 'MERGE SP for upading Device Brand Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_device_model' name, 'GET SP for reading Device Model data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_device_model' name, 'GET SP for reading Device Model data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_device_model' name, 'MERGE SP for upading Device Model Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_device_browser' name, 'GET SP for reading Device Browser data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_device_browser' name, 'GET SP for reading Device Browser data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_device_browser' name, 'MERGE SP for upading Device Browser Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_sales_dim_marketing' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_sales_device_os' name, 'GET SP for reading Device OS data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_sales_device_os' name, 'GET SP for reading Device OS data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_sales_dim_marketing' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_sales_device_os' name, 'MERGE SP for upading Device OS Dimension data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 