
-- t_SPRun_ssrs
select idSPRun_ssrs, idSP_ssrs, 
	startTime, finishTime, runStatus, 
	ins_ts
from ControlDB.logging.t_SPRun_ssrs

-- t_SPRunMessage_ssrs
select idSPRunMessage_ssrs, idSPRun_ssrs, 
	messageTime, rowAmountSelect, rowAmountInsert, rowAmountUpdate, rowAmountDelete, message, 
	ins_ts	
from ControlDB.logging.t_SPRunMessage_ssrs

-- t_SPRunParameter_ssrs
select idSPRunParameter_ssrs, idSPRun_ssrs, 
	messageTime, parameter, parameter_value
	ins_ts	
from ControlDB.logging.t_SPRunParameter_ssrs


-- 


	-- t_SPRun_v
	select idSPRun_ssrs, idSP_ssrs, sp_name,
		startTime, finishTime, duration, runStatus, 
		ins_ts
	from ControlDB.logging.t_SPRun_ssrs_v
	order by idSPRun_ssrs desc, sp_name

	-- t_SPRunMessage_v
	select spm.idSPRunMessage_ssrs, spm.idSPRun_ssrs, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.rowAmountSelect, spm.rowAmountInsert, spm.rowAmountUpdate, spm.rowAmountDelete, spm.message, 
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_ssrs_v spm
		inner join
			ControlDB.logging.t_SPRun_ssrs_v sp on spm.idSPRun_ssrs = sp.idSPRun_ssrs
	where spm.idSPRun_ssrs = 187
	-- where spm.idSPRun_ssrs in (4, 5, 6)
	order by spm.finishTime

	-- t_SPRunParameter_v
	select spm.idSPRunParameter_ssrs, spm.idSPRun_ssrs, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.parameter, spm.parameter_value,
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunParameter_ssrs_v spm
		inner join
			ControlDB.logging.t_SPRun_ssrs_v sp on spm.idSPRun_ssrs = sp.idSPRun_ssrs
	where spm.idSPRun_ssrs = 187
	-- where spm.idSPRun_ssrs in (4, 5, 6)
	order by spm.finishTime

	select spm.idSPRunParameter_ssrs, spm.idSPRun_ssrs, spm.sp_name, spm.startTime, spm.finishTime, spm.runStatus, 
		spm.messageTime, spm.parameter, spm.parameter_value,
		spm.ins_ts	
	from 
			ControlDB.logging.t_SPRunParameter_ssrs_v spm
		inner join
			ControlDB.logging.t_SPRun_ssrs_v sp on spm.idSPRun_ssrs = sp.idSPRun_ssrs
	where spm.parameter = 'website'
	order by spm.finishTime desc


	----

		select c.country_name
		from
			(select cast(s.string AS int) idCountry_sk
			from Adhoc.dbo.fnSplitString ('', ',') AS s) t
		inner join
			Warehouse.gen.dim_country c on t.idCountry_sk = c.idCountry_sk
