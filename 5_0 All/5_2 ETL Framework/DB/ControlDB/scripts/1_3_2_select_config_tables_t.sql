use ControlDB
go 

-- t_Database
select idDatabase, idDatabaseType, database_name, description, ins_ts
from ControlDB.config.t_Database 
order by idDatabase; 

-- t_Developer
select idDeveloper, developer, name, ins_ts
from ControlDB.config.t_Developer
order by idDeveloper;

-- t_BusinessArea 
select idBusinessArea, businessArea_name, codBusinessArea, description, ins_ts
from ControlDB.config.t_BusinessArea 
order by idBusinessArea;

-- 

	-- t_Database_v
	select idDatabase, codDatabaseType, database_name, description, ins_ts
	from ControlDB.config.t_Database_v
	order by idDatabase; 
