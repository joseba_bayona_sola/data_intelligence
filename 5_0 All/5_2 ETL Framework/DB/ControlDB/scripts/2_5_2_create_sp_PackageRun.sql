use ControlDB
go 

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Start record for the Package Run to the logging Table - Return Unique Record Number
-- ==========================================================================================

create procedure logging.logPackageRun_start_sp
	@package_name varchar(100), 
	@idETLBatchRun bigint, @idPackageRun_parent bigint = null, 
	@idExecution uniqueidentifier = null,
	@idPackageRun bigint OUTPUT
as

begin
	set nocount on;

	insert into ControlDB.logging.t_PackageRun (idPackage, idETLBatchRun, idPackageRun_parent, idExecution, 
		startTime, runStatus)
		
		select p.idPackage, t.idETLBatchRun, t.idPackageRun_parent, t.idExecution, 
			getutcdate() startTime, 'RUNNING' runStatus
		from
				(select @package_name package_name, @idETLBatchRun idETLBatchRun, @idPackageRun_parent idPackageRun_parent, @idExecution idExecution) t
			inner join
				ControlDB.config.t_Package p on t.package_name = p.package_name;
		 
	set @idPackageRun = SCOPE_IDENTITY();

	if @idPackageRun_parent <> 0
	begin
		update ControlDB.logging.t_PackageRun
		set idPackageRun_parent = @idPackageRun_parent
		where idPackageRun = @idPackageRun;
	end

	return @idPackageRun;
end 

go 


-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Finish record for the Package Run to the logging Table 
-- ==========================================================================================

create procedure logging.logPackageRun_stop_sp
	@idPackageRun bigint, 
	@runStatus varchar(50)
as

begin
	set nocount on;

	update ControlDB.logging.t_PackageRun
	set
		finishTime = getutcdate(), 
		runStatus = @runStatus
	where idPackageRun = @idPackageRun;
end 

go 


-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 12-01-2017
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Writes a Message record for the Package Run Message to the logging Table 
-- ==========================================================================================

create procedure logging.logPackageRun_message_sp
	@idPackageRun bigint, 
	@messageType varchar(50), @rowAmount int, @message varchar(500)
as

begin
	set nocount on;

	insert into ControlDB.logging.t_PackageRunMessage (idPackageRun, messageTime, messageType, rowAmount, message) values
		(@idPackageRun, getutcdate(), @messageType, @rowAmount, @message);

end 

go 

