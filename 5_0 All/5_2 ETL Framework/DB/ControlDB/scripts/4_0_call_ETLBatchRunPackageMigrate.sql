

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @migration_type varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL_migra', @package_name = 'ETLBatchRun.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Migrate Landing - LADLEX', @package_name_call = 'migmysql_lnd_all_tables' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' 	

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
		
-------------------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @migration_type varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL_migra', @package_name = 'ETLBatchRun_prepare.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Migrate Prepare - LADLEX', @package_name_call = 'ETLBatchRun_prepare' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' 	

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
		
-------------------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @migration_type varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMigration.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Migrate - LADLEX', @package_name_call = 'VisionDirectDataWarehouseLoadMigration' -- VisionDirectDataWarehouseLoad

	select @migration_type = 'migrate'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" /Par "\"migration_type\"";"\"' + @migration_type + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars		