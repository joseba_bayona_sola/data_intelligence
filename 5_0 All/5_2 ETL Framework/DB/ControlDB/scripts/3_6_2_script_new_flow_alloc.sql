use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'alloc' codBusinessArea, 
				'Allocation' name, 'Data about Allocation in ERP: ERP Orders + Issue Lines' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Allocation' flow_name, 'dim_order_type_erp' name, 'Staging Table for Order Type ERP: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_order_type_erp' name, 'Warehouse Table for Order Type ERP: Order Type ERP Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_order_status_erp' name, 'Staging Table for Order Status ERP: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_order_status_erp' name, 'Warehouse Table for Order Status ERP: Order Status ERP Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_shipment_status_erp' name, 'Staging Table for Shipment Status ERP: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_shipment_status_erp' name, 'Warehouse Table for Shipment Status ERP: Shipment Status ERP Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_allocation_status' name, 'Staging Table for Allocation Status: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_allocation_status' name, 'Warehouse Table for Allocation Status: Allocation Status Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_allocation_strategy' name, 'Staging Table for Allocation Strategy: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_allocation_strategy' name, 'Warehouse Table for Allocation Strategy: Allocation Strategy Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_allocation_type' name, 'Staging Table for Allocation Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_allocation_type' name, 'Warehouse Table for Allocation Type: Allocation Type Dimension' description
			union
			select 'Staging' database_name, 'Allocation' flow_name, 'dim_allocation_record_type' name, 'Staging Table for Allocation Record Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Allocation' flow_name, 'dim_allocation_record_type' name, 'Warehouse Table for Allocation Record Type: Allocation Record Type Dimension' description
			
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Allocation' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_alloc_tables' name, 'SSIS PKG for reading Allocation data (ERP) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Allocation' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_alloc_tables' name, 'SSIS PKG for reading Allocation data (ERP) from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_order_type_erp' name, 'GET SP for reading Order Type ERP data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_order_type_erp' name, 'GET SP for reading Order Type ERP data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_order_type_erp' name, 'MERGE SP for upading Order Type ERP Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_order_status_erp' name, 'GET SP for reading Order Status ERP data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_order_status_erp' name, 'GET SP for reading Order Status ERP data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_order_status_erp' name, 'MERGE SP for upading Order Status ERP Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_shipment_status_erp' name, 'GET SP for reading Shipment Status ERP data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_shipment_status_erp' name, 'GET SP for reading Shipment Status ERP data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_shipment_status_erp' name, 'MERGE SP for upading Shipment Status ERP Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_allocation_status' name, 'GET SP for reading Allocation Status data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_allocation_status' name, 'GET SP for reading Allocation Status data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_allocation_status' name, 'MERGE SP for upading Allocation Status Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_allocation_strategy' name, 'GET SP for reading Allocation Strategy data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_allocation_strategy' name, 'GET SP for reading Allocation Strategy data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_allocation_strategy' name, 'MERGE SP for upading Allocation Strategy Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_allocation_type' name, 'GET SP for reading Allocation Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_allocation_type' name, 'GET SP for reading Allocation Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_allocation_type' name, 'MERGE SP for upading Allocation Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_alloc_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_alloc_allocation_record_type' name, 'GET SP for reading Allocation Record Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_alloc_allocation_record_type' name, 'GET SP for reading Allocation Record Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_alloc_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_alloc_allocation_record_type' name, 'MERGE SP for upading Allocation Record Type Dimension data' description

			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 


