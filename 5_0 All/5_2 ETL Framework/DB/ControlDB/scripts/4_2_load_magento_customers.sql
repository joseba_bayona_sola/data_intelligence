
use Landing 
go

truncate table Landing.mag.customer_entity_flat
go 

-- DISABLE TRIGGERS
disable trigger mag.trg_customer_entity_flat on Landing.mag.customer_entity_flat
go
disable trigger mag.trg_customer_entity_flat_aud on Landing.mag.customer_entity_flat_aud
go


insert into Landing.mag.customer_entity_flat(entity_id, increment_id, email, 
	entity_type_id, attribute_set_id, 
	group_id, website_group_id, website_id, store_id, 
	is_active, disable_auto_group_change, 
	created_at, updated_at, 
	created_in, 
	prefix, firstname, middlename, lastname, suffix, 
	cus_phone, 
	dob, gender,
	default_billing, default_shipping, 
	unsubscribe_all, unsubscribe_all_date, 
	days_worn_info, 
	old_access_cust_no, old_web_cust_no, old_customer_id, 
	found_us_info, referafriend_code,
	idETLBatchRun, ins_ts)

	select entity_id, increment_id, email, 
		entity_type_id, attribute_set_id, 
		group_id, website_group_id, website_id, store_id, 
		is_active, disable_auto_group_change, 
		created_at, updated_at, 
		created_in, 
		prefix, firstname, middlename, lastname, suffix, 
		cus_phone, 
		dob, gender,
		default_billing, default_shipping, 
		unsubscribe_all, unsubscribe_all_date, 
		days_worn_info, 
		old_access_cust_no, old_web_cust_no, old_customer_id, 
		found_us_info, referafriend_code,
		idETLBatchRun, ins_ts
	from Landing.mag.customer_entity_flat_aud
	-- where entity_id > 500000	
go

-- ENABLE TRIGGERS
enable trigger mag.trg_customer_entity_flat on Landing.mag.customer_entity_flat
go
enable trigger mag.trg_customer_entity_flat_aud on Landing.mag.customer_entity_flat_aud
go

		-- exec Landing.mag.lnd_stg_get_aux_sales_order_dim_fact @idETLBatchRun = 1, @idPackageRun = 1

