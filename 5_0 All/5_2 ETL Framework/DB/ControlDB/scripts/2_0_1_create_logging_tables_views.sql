use ControlDB
go

-- t_ETLBatchRun_v
create view logging.t_ETLBatchRun_v as
	select ebr.idETLBatchRun, ebr.idETLBatchType, ebt.codETLBatchType, ebr.idFlowType, ft.codFlowType, ebr.idPackage, p.flow_name, p.package_name,
		ebr.description, ebr.dateFrom, ebr.dateTo, 
		ebr.startTime, ebr.finishTime, datediff(minute, ebr.startTime, ebr.finishTime) duration, ebr.runStatus, -- ebr.message,
		ebr.ins_ts
	from 
			ControlDB.logging.t_ETLBatchRun ebr
		inner join
			ControlDB.config.cat_ETLBatchType ebt on ebr.idETLBatchType = ebt.idETLBatchType
		inner join
			ControlDB.config.cat_FlowType ft on ebr.idFlowType = ft.idFlowType
		inner join
			ControlDB.config.t_Package_v p on ebr.idPackage = p.idPackage
go 

-- t_PackageRun_v
create view logging.t_PackageRun_v as
	select pr.idPackageRun, pr.idPackage, p.flow_name, p.package_name, 
		pr.idETLBatchRun, ebr.description etlB_desc, ebr.dateFrom etlB_dateFrom, ebr.dateTo etlB_dateTo, ebr.startTime etlB_startTime, ebr.finishTime etlB_finishTime, 
		ebr.runStatus etlB_runStatus, -- ebr.message etlB_message,
		pr.idPackageRun_parent, 
		pr.idExecution, pr.startTime, pr.finishTime, datediff(minute, pr.startTime, pr.finishTime) duration, pr.runStatus, 
		pr.ins_ts
	from 
			ControlDB.logging.t_PackageRun pr
		inner join
			ControlDB.config.t_Package_v p on pr.idPackage = p.idPackage
		inner join
			ControlDB.logging.t_ETLBatchRun_v ebr on pr.idETLBatchRun = ebr.idETLBatchRun
go 

-- t_PackageRunMessage_v
create view logging.t_PackageRunMessage_v as
	select prm.idPackageRunMessage, prm.idPackageRun, pr.flow_name, pr.package_name, pr.idExecution, pr.startTime, pr.finishTime, pr.runStatus, 
		prm.messageTime, prm.messageType, prm.rowAmount, prm.message, 
		prm.ins_ts
	from 
			ControlDB.logging.t_PackageRunMessage  prm
		inner join
			ControlDB.logging.t_PackageRun_v pr on prm.idPackageRun = pr.idPackageRun
go 

-- t_SPRun_v
create view logging.t_SPRun_v as
	select spr.idSPRun, spr.idSP, sp.package_name, sp.sp_name,
		spr.idETLBatchRun, spr.idPackageRun, pr.idExecution, pr.startTime pkg_startTime, pr.finishTime pkg_finishTime, pr.runStatus pkg_runStatus, 
		spr.startTime, spr.finishTime, datediff(minute, spr.startTime, spr.finishTime) duration, spr.runStatus, 
		spr.ins_ts
	from 
			ControlDB.logging.t_SPRun spr
		inner join
			ControlDB.config.t_SP_v sp on spr.idSP = sp.idSP
		inner join
			ControlDB.logging.t_PackageRun_v pr on spr.idPackageRun = pr.idPackageRun
go

-- t_SPRunMessage_v
create view logging.t_SPRunMessage_v as
	select sprm.idSPRunMessage, sprm.idSPRun, spr.package_name, spr.sp_name, spr.startTime, spr.finishTime, spr.runStatus, 
		sprm.messageTime, sprm.rowAmountSelect, sprm.rowAmountInsert, sprm.rowAmountUpdate, sprm.rowAmountDelete, sprm.message, 
		sprm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage sprm
		inner join
			ControlDB.logging.t_SPRun_v spr on sprm.idSPRun = spr.idSPRun
go



-- t_SPRun_ssrs_v
create view logging.t_SPRun_ssrs_v as
	select spr.idSPRun_ssrs, spr.idSP_ssrs, sp.sp_name,
		spr.startTime, spr.finishTime, datediff(minute, spr.startTime, spr.finishTime) duration, spr.runStatus, 
		spr.ins_ts
	from 
			ControlDB.logging.t_SPRun_ssrs spr
		inner join
			ControlDB.config.t_SP_ssrs_v sp on spr.idSP_ssrs = sp.idSP_ssrs
go

-- t_SPRunMessage_ssrs_v
create view logging.t_SPRunMessage_ssrs_v as
	select sprm.idSPRunMessage_ssrs, sprm.idSPRun_ssrs, spr.sp_name, spr.startTime, spr.finishTime, spr.runStatus, 
		sprm.messageTime, sprm.rowAmountSelect, sprm.rowAmountInsert, sprm.rowAmountUpdate, sprm.rowAmountDelete, sprm.message, 
		sprm.ins_ts	
	from 
			ControlDB.logging.t_SPRunMessage_ssrs sprm
		inner join
			ControlDB.logging.t_SPRun_ssrs_v spr on sprm.idSPRun_ssrs = spr.idSPRun_ssrs
go


-- t_SPRunParameter_ssrs_v
create view logging.t_SPRunParameter_ssrs_v as
	select sprm.idSPRunParameter_ssrs, sprm.idSPRun_ssrs, spr.sp_name, spr.startTime, spr.finishTime, spr.runStatus, 
		sprm.messageTime, sprm.parameter, sprm.parameter_value,  
		sprm.ins_ts	
	from 
			ControlDB.logging.t_SPRunParameter_ssrs sprm
		inner join
			ControlDB.logging.t_SPRun_ssrs_v spr on sprm.idSPRun_ssrs = spr.idSPRun_ssrs
go
