	---- INSERT: migra.sales_flat_order_hist
	delete from Landing.migra.sales_flat_order_hist

	insert into Landing.migra.sales_flat_order_hist(migra_website_name, etl_name, 	
		entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		status, 
		total_qty_ordered, 
		base_subtotal, base_grand_total, 
		customer_email,
		idETLBatchRun)

		select moh.migra_website_name, moh.etl_name, 
			moh.new_order_id entity_id, moh.new_order_no increment_id, hoi.store_id, 
			moh.new_customer_id customer_id, hoi.created_at created_at, hoi.created_at updated_at, 
			'shipped' status, 
			hoi.total_qty_ordered total_qty_ordered, 
			hoi.base_subtotal base_subtotal, hoi.grand_total base_grand_total, 
			moh.customer_email, 
			1 idETLBatchRun
		from 
				Landing.migra.match_order_header moh
			inner join
				(select migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total, sum(qty_ordered) total_qty_ordered, sum(row_total) base_subtotal
				from Landing.aux.migra_hist_dw_hist_order_item_v
				group by migra_website_name, etl_name, order_id, 
					store_id, created_at,
					customer_id, customer_email, 
					grand_total) hoi on moh.old_order_id = hoi.order_id and moh.migra_website_name = hoi.migra_website_name
		where moh.migra_website_name in ('postoptics')