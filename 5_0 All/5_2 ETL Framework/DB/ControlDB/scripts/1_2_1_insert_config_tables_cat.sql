

-- cat_DatabaseType
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(1, 'SQL-Server', 'SQL-Server Database');
	go 
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(2, 'MySQL', 'MySQL Database');
	go 
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(3, 'PostgreSQL', 'PostgreSQL Database');
	go 
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(4, 'GoogleAnalytics', 'Google Analytics Reporting API');
	go 
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(5, 'CSVFile', 'CSV or Text Files');
	go 
	insert into ControlDB.config.cat_DatabaseType (idDatabaseType, codDatabaseType, description) values 
		(6, 'ExcelFile', 'Excel Files');
	go 

-- cat_DWHTableType
	insert into ControlDB.config.cat_DWHTableType (idDWHTableType, codDWHTableType, description) values 
		(1, 'DIM', 'Dimension Table in the DWH');
	go 
	insert into ControlDB.config.cat_DWHTableType (idDWHTableType, codDWHTableType, description) values 
		(2, 'FACT', 'Fact Table in the DWH');
	go 
	insert into ControlDB.config.cat_DWHTableType (idDWHTableType, codDWHTableType, description) values 
		(3, 'MIGRA', 'Table with Migration Data');
	go 
	insert into ControlDB.config.cat_DWHTableType (idDWHTableType, codDWHTableType, description) values 
		(4, 'MAP', 'Table in Mapping Data');
	go 



-- cat_FlowType
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(1, 'SRC-DWH', 'The Flow moves data from Source to Warehouse: SRC - LANDING - STAGING - WAREHOUSE');
	go 
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(2, 'WH-DWH', 'The Flow moves data from Warehouse to Warehouse: WAREHOUSE - STAGING - WAREHOUSE');
	go 
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(3, 'MIGRA-LND', 'The Flow moves data from Migration Sources to Landing: MIGRA SRC - LANDING');
	go 
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(4, 'MAP-LND', 'The Flow moves data from Mapping Sources to Landing: MAP SRC - LANDING');
	go 
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(5, 'SRC-LND', 'The Flow moves data only from Source to Landing: SRC - LANDING');
	go 
	insert into ControlDB.config.cat_FlowType (idFlowType, codFlowType, description) values
		(6, 'ADHOC', 'The Flow inserst data to Adhoc: ADHOC');
	go 



-- cat_PackageType
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(1, 'EXEC', 'The package executes (calls) other packages');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(2, 'SRC-LND', 'The package moves data from SRC to LANDING');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(3, 'LND-STG', 'The package moves data from LANDING to STAGING');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(4, 'STG-DWH', 'The package moves data from STAGING to WAREHOUSE');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(5, 'DWH-STG', 'The package moves data from WAREHOUSE to STAGING');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(6, 'MIGRA-LND', 'The package moves data from MIGRA SRC to LANDING');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(7, 'MAP-LND', 'The package moves data from MAP SRC to LANDING');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(8, 'DWH-DWH', 'The package moves data from WAREHOUSE to WAREHOUSE');
	go 
	insert into ControlDB.config.cat_PackageType (idPackageType, codPackageType, description) values
		(9, 'ADHOC', 'The package moves data to ADHOC');
	go 

-- cat_SPType
	insert into ControlDB.config.cat_SPType (idSPType, codSPType, description) values
		(1, 'GET', 'The SP reads data: SELECT');
	go 
	insert into ControlDB.config.cat_SPType (idSPType, codSPType, description) values
		(2, 'MERGE', 'The SP merges data: INSERT - UPDATE - DELETE');
	go 
	insert into ControlDB.config.cat_SPType (idSPType, codSPType, description) values
		(3, 'INSERT', 'The SP inserts data: INSERT');
	go 

-- cat_ETLBatchType
	insert into ControlDB.config.cat_ETLBatchType (idETLBatchType, codETLBatchType, description) values
		(1, 'FULL', 'Full ETL Load: The Package run executes full Load: All Flows');
	go 
	insert into ControlDB.config.cat_ETLBatchType (idETLBatchType, codETLBatchType, description) values
		(2, 'PARTIAL', 'Partial ETL Load: The Package run executes partial Load: Only specific Flows');
	go 

