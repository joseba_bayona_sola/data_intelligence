
use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2018-02-28 00:00:00', 120) 
	select @dateToV = convert(varchar, '2018-03-02 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Reload 1st of March', @package_name_call = 'VisionDirectDataWarehouseLoad' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Test Mutuelle Quot Status Dev', @package_name_call = 'VisionDirectDataWarehouseLoadNoSRC' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Gender - DOB - Login', @package_name_call = 'VisionDirectDataWarehouseLoadCustomers' 

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @migration_type varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMigration.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - Migrate - Lensway NL 2008, 09, 10, 11', @package_name_call = 'VisionDirectDataWarehouseLoadMigration' -- VisionDirectDataWarehouseLoad

	select @migration_type = 'migrate'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" /Par "\"migration_type\"";"\"' + @migration_type + '\""'

		

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMendix.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2018-07-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2018-07-04 00:00:00', 120) 

	-- select @dateToV = convert(varchar, GETUTCDATE(), 120) 

	select @description = 'VD Mendix Landing - Reload', @package_name_call = 'VisionDirectDataWarehouseLoadERP'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-11-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-12-01 00:00:00', 120) 

	select @description = 'VD Mendix Full Load - Inv Reconciliation', @package_name_call = 'LoadAll_src_lnd_mendix_one' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'VD Data Warehouse Load - ERP Flow Dev - PO - REC Currencies', @package_name_call = 'VisionDirectDataWarehouseLoadERPNoSRC' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars

--------------------------------------------
		
use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @num_days varchar(100)

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunFM.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'Futurmaster Files Generation', @package_name_call = 'dwh_dwh_fm_interface_files_sales' -- VisionDirectDataWarehouseLoad

	select @num_days = convert(varchar, 31)

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" /Par "\"num_days(Int16)\"";"\"' + @num_days + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
		
--------------------------------------------
		
use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunSFMC.dtsx', @environment_name = 'Test'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 

	select @description = 'SFMC Files Generation', @package_name_call = 'ETLBatchRunSFMC' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
		
--------------------------------------------

use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 
	declare @stock_hist_load_type char(1)
	declare @stock_hist_load_numV varchar(20)

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunPackage.dtsx', @environment_name = 'Prod'

	select @dateFromV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @dateToV = convert(varchar, '2017-01-01 00:00:00', 120) 
	select @stock_hist_load_type = 'F' 
	select @stock_hist_load_numV = convert(varchar, 18) 

	select @description = 'VD Data Warehouse Load - ERP Flow Dev - Stock Hist Initial - Full', @package_name_call = 'VisionDirectDataWarehouseLoadERPStockHist' -- VisionDirectDataWarehouseLoad

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"$Project::stock_hist_load_type(String)\"";"\"' + @stock_hist_load_type + '\"" ' +
		'/Par "\"$Project::stock_hist_load_num(Int32)\"";"\"' + @stock_hist_load_numV + '\"" ' +
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\"" ' +
		'/Par "\"environment_name\"";"\"' + @environment_name + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars
