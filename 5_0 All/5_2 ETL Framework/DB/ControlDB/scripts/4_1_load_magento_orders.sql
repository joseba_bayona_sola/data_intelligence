
--select *
--from Landing.mag.sales_flat_order

truncate table Landing.mag.sales_flat_order
truncate table Landing.mag.sales_flat_invoice
truncate table Landing.mag.sales_flat_creditmemo
truncate table Landing.mag.sales_flat_shipment
go

-- DISABLE TRIGGERS
disable trigger mag.trg_sales_flat_order on Landing.mag.sales_flat_order
go
disable trigger mag.trg_sales_flat_order_aud on Landing.mag.sales_flat_order_aud
go

insert into Landing.mag.sales_flat_order(entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
	shipping_address_id, billing_address_id, 
	state, status, 
	total_qty_ordered, 
	base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
	base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
	base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
	base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
	base_grand_total, 
	base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
	base_to_global_rate, base_to_order_rate, order_currency_code,
	customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
	customer_taxvat, customer_gender, customer_note, customer_note_notify, 
	shipping_description, 
	coupon_code, applied_rule_ids,
	affilBatch, affilCode, affilUserRef, 
	reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
	reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
	presc_verification_method, prescription_verification_type, 
	referafriend_code, referafriend_referer, 
	telesales_method_code, telesales_admin_username, 
	remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
	idETLBatchRun, ins_ts)

	select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
		shipping_address_id, billing_address_id, 
		state, status, 
		total_qty_ordered, 
		base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
		base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
		base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
		base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
		base_grand_total, 
		base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
		base_to_global_rate, base_to_order_rate, order_currency_code,
		customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
		customer_taxvat, customer_gender, customer_note, customer_note_notify, 
		shipping_description, 
		coupon_code, applied_rule_ids,
		affilBatch, affilCode, affilUserRef, 
		reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
		reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
		presc_verification_method, prescription_verification_type, 
		referafriend_code, referafriend_referer, 
		telesales_method_code, telesales_admin_username, 
		remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
		idETLBatchRun, ins_ts
	from Landing.mag.sales_flat_order_aud
	where created_at between '2018-09-03' and '2018-09-04' or created_at between '2018-09-22' and '2018-09-23' 

	-- where created_at between '2017-11-01' and '2017-12-01'
	-- where entity_id in 	
	-- 	(select distinct order_id_bk
	--	from Warehouse.sales.fact_order_line_v
	--	where product_id_magento in (2326, 2331))
	
	--where entity_id in 
	--	(select distinct order_id_bk
	--	from 
	--			Warehouse.sales.fact_order_line_v ol
	--		inner join
	--			Landing.mend.gen_prod_productfamilypacksize_v pfps on ol.product_id_magento = pfps.magentoProductID_int and pfps.productfamilysizerank = 1
	--	where ol.website_group = 'VisionDirect'
	--		and ol.qty_unit / pfps.size <> ol.qty_pack)

	--where entity_id in 
	--	(select order_id_bk
	--	from Warehouse.act.fact_activity_sales_v
	--	where idETLBatchRun_upd = 1635)

	--where entity_id in 
	--	(select oh.entity_id
	--	from 
	--			Landing.mag.sales_flat_order_aud oh
	--		inner join	 
	--			Landing.map.sales_summer_time_period_aud stp on year(oh.created_at) = stp.year and 
	--				oh.created_at between stp.from_date and stp.to_date
	--	union
	--	select ih.order_id
	--	from 
	--			Landing.mag.sales_flat_invoice_aud ih
	--		inner join	 
	--			Landing.map.sales_summer_time_period_aud stp on year(ih.created_at) = stp.year and 
	--				ih.created_at between stp.from_date and stp.to_date
	--	union
	--	select sh.order_id
	--	from 
	--			Landing.mag.sales_flat_shipment_aud sh
	--		inner join	 
	--			Landing.map.sales_summer_time_period_aud stp on year(sh.created_at) = stp.year and 
	--				sh.created_at between stp.from_date and stp.to_date
	--	union
	--	select ch.order_id
	--	from 
	--			Landing.aux.mag_sales_flat_creditmemo ch
	--		inner join	 
	--			Landing.map.sales_summer_time_period_aud stp on year(ch.created_at) = stp.year and 
	--				ch.created_at between stp.from_date and stp.to_date)

	-- where customer_id in (437782, 778490, 1415280)
go

-- ENABLE TRIGGERS
enable trigger mag.trg_sales_flat_order on Landing.mag.sales_flat_order
go
enable trigger mag.trg_sales_flat_order_aud on Landing.mag.sales_flat_order_aud
go		

