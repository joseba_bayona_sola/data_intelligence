use ControlDB_DE
go

select idAppType, codAppType, name, description, ins_ts
from ControlDB_DE.config.cat_AppType


select idDeveloper, developer, name, ins_ts
from ControlDB_DE.config.t_Developer


select idApp, idAppType, idDeveloper, 
	app_programming_language, app_file_name, app_name, description, ins_ts
from ControlDB_DE.config.t_App

select idAppMethod, idApp, idDeveloper, 
	app_file_name, app_method_name, code_type, description, ins_ts
from ControlDB_DE.config.t_AppMethod
	
	select idApp, codAppType, developer,
		app_programming_language, app_file_name, app_name, description
	from ControlDB_DE.config.t_App_v 

	select idAppMethod, idApp, codAppType, developer,
		app_programming_language, app_name, 
		app_file_name, app_method_name, code_type, description
	from ControlDB_DE.config.t_AppMethod_v 

------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------


select idBatchRun, idApp, 
	description, dateFrom, dateTo, startTime, finishTime, runStatus, ins_ts
from ControlDB_DE.logging.t_BatchRun
order by idBatchRun desc

select idMethodRun, idBatchRun, idAppMethod, 
	startTime, finishTime, runStatus, ins_ts
from ControlDB_DE.logging.t_MethodRun
order by idMethodRun desc

select idMethodRunError, idMethodRun, messageTime, message, ins_ts
from ControlDB_DE.logging.t_MethodRunError

select idMethodRunMessage, idMethodRun, messageTime, rowAmount, message, ins_ts
from ControlDB_DE.logging.t_MethodRunMessage

	select idBatchRun, idApp, app_file_name, app_name, 
		description, dateFrom, dateTo, 
		startTime, finishTime, runStatus, 
		ins_ts
	from ControlDB_DE.logging.t_BatchRun_v
	order by idBatchRun desc

	select idMethodRun, idAppMethod, app_file_name, app_method_name, code_type,
		idBatchRun, app_name, -- B_desc, B_dateFrom, B_dateTo, B_startTime, B_finishTime, 
		startTime, finishTime, runStatus,
		ins_ts
	from ControlDB_DE.logging.t_MethodRun_v
	where idBatchRun = 164 -- and app_file_name not in ('py_api_ga_core_reporting.py', 'py_api_ga_reporting.py', 'py_db_sql_server.py', 'py_trans_ga_data.py')
	order by startTime 

	select idMethodRunMessage, idMethodRun, app_file_name, app_method_name, code_type,
		idBatchRun, app_name, 
		startTime, finishTime, runStatus,
		messageTime, rowAmount, message,
		ins_ts
	from ControlDB_DE.logging.t_MethodRunMessage_v
	where idBatchRun = 164
	order by messageTime

	select idMethodRunError, idMethodRun, app_file_name, app_method_name, code_type,
		idBatchRun, app_name, 
		startTime, finishTime, runStatus,
		messageTime, message,
		ins_ts
	from ControlDB_DE.logging.t_MethodRunError_v
	where idBatchRun = 164
	order by startTime, messageTime

-------------------------------------------------------------------------
-------------------------------------------------------------------------
