
-- t_SP
delete from ControlDB.config.t_SP;
go 

-- t_Package
delete from ControlDB.config.t_Package;
go 

-- t_Table
delete from ControlDB.config.t_Table;
go 

-- t_Flow
delete from ControlDB.config.t_Flow;
go 

-- t_Developer
delete from ControlDB.config.t_Developer;
go 

-- t_Database
delete from ControlDB.config.t_Database;
go 

-- 

-- cat_ETLBatchType
delete from ControlDB.config.cat_ETLBatchType;
go 

-- PK_cat_SPType
delete from ControlDB.config.cat_SPType;
go 

-- PK_cat_PackageType
delete from ControlDB.config.cat_PackageType;
go 

-- PK_cat_DWHTableType
delete from ControlDB.config.cat_DWHTableType;
go 

-- PK_cat_DatabaseType
delete from ControlDB.config.cat_DatabaseType;
go 

