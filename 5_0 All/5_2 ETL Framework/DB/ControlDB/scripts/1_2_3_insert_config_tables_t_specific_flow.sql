use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'Store' name, 'Data about the Website where the order was done by the Customer' description
			union
			select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'gen' codBusinessArea, 
				'Customer' name, 'Data about the person that makes different orders' description	
				) t
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Landing' database_name, 'Store' flow_name, 'store_xxx' name, 'Landing Table for Store: Data coming from Magento' description
			union
			select 'Staging' database_name, 'Store' flow_name, 'dim_store' name, 'Staging Table for Store: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Store' flow_name, 'dim_store' name, 'Warehouse Table for Store: Store Dimension' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
go 

-- t_Package 
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'jsola' developer,  'Store' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_gen_store' name, 'SSIS PKG for reading Store data from Magento to Landing' description
			union
			select 'LND-STG' codPackageType, 'jsola' developer,  'Store' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_gen_store' name, 'SSIS PKG for reading Store data from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Store' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_gen_store' name, 'SSIS PKG for reading Store data from Staging to Warehouse - Update Dimension' description
			) t
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
go 

insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'SRC-LND' codPackageType, 'jsola' developer,  'Store' flow_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'ETLBatchRun' name, 'SSIS PKG for starting the ETL Batch Run and calling rest of packages' description
			) t
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
go 


	
-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'srcmag_lnd_gen_store' package_name, 'Magento01' databaseFrom_name, 'Landing' databaseTo_name, 
				'srcmag_lnd_get_gen_store' name, 'GET SP for reading Store data from Magento to Landing' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_gen_store' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_gen_store' name, 'GET SP for reading Store data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_gen_store' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_gen_store' name, 'GET SP for reading Store data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_gen_store' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_gen_store' name, 'MERGE SP for upading Store Dimension data' description
			) t
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
go 
