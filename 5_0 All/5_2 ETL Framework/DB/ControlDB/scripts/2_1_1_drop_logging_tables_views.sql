use ControlDB
go 

-- t_SPRunParameter_ssrs_v
drop view logging.t_SPRunParameter_ssrs_v;
go 

-- t_SPRunMessage_ssrs_v
drop view logging.t_SPRunMessage_ssrs_v;
go 

-- t_SPRunMessage_v
drop view logging.t_SPRunMessage_v;
go 

-- t_SPRun_ssrs_v
drop view logging.t_SPRun_ssrs_v;
go 

-- t_SPRun_v
drop view logging.t_SPRun_v;
go 

-- t_PackageRunMessage_v
drop view logging.t_PackageRunMessage_v;
go 

-- t_PackageRun_v
drop view logging.t_PackageRun_v;
go 

-- t_ETLBatchRun_v
drop view logging.t_ETLBatchRun_v;
go 


