use ControlDB
go 

-- t_Flow
insert into ControlDB.config.t_Flow (idFlow, idFlowType, idDWHTableType, idBusinessArea, 
	flow_name, description) 

	select row_number() over (order by (select 0)) + id.idFlowMax idFlow, ft.idFlowType, dtt.idDWHTableType, ba.idBusinessArea, 
		t.name, t.description
	from
			(select 'SRC-DWH' codFlowType, 'DIM' codDWHTableType, 'stock' codBusinessArea, 
				'Stock' name, 'Data about Stock in ERP: WH Stock Item, WH Stock Item Batches and Stock Movements' description) t
		left join
			ControlDB.config.t_Flow t2 on t.name = t2.flow_name
		inner join
			ControlDB.config.cat_FlowType ft on t.codFlowType = ft.codFlowType
		inner join
			ControlDB.config.cat_DWHTableType dtt on t.codDWHTableType = dtt.codDWHTableType
		inner join
			ControlDB.config.t_BusinessArea ba on t.codBusinessArea = ba.codBusinessArea,
			(select isnull(max(idFlow), 0) idFlowMax
			from ControlDB.config.t_Flow) id 
	where t2.idFlow is null
go 

-- t_Table
insert into ControlDB.config.t_Table (idTable, idDatabase, idFlow, table_name, description) 

	select row_number() over (order by (select 0)) + id.idTableMax idTable, d.idDatabase, f.idFlow, t.name, t.description
	from
			(select 'Staging' database_name, 'Stock' flow_name, 'dim_stock_method_type' name, 'Staging Table for Stock Method Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_stock_method_type' name, 'Warehouse Table for Stock Method Type: Stock Method Type Dimension' description
			union
			select 'Staging' database_name, 'Stock' flow_name, 'dim_stock_batch_type' name, 'Staging Table for Stock Batch Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_stock_batch_type' name, 'Warehouse Table for Stock Batch Type: Stock Batch Type Dimension' description
			union
			select 'Staging' database_name, 'Stock' flow_name, 'dim_stock_adjustment_type' name, 'Staging Table for Adjustment Method Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_stock_adjustment_type' name, 'Warehouse Table for Stock Adjustment Type: Stock Adjustment Type Dimension' description
			union
			select 'Staging' database_name, 'Stock' flow_name, 'dim_stock_movement_type' name, 'Staging Table for Stock Movement Type: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_stock_movement_type' name, 'Warehouse Table for Stock Movement Type: Stock Movement Type Dimension' description
			union
			select 'Staging' database_name, 'Stock' flow_name, 'dim_stock_movement_period' name, 'Staging Table for Stock Movement Period: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_stock_movement_period' name, 'Warehouse Table for Stock Movement Period: Stock Movement Period Dimension' description
			union

			select 'Staging' database_name, 'Stock' flow_name, 'dim_wh_stock_item' name, 'Staging Table for WH Stock Item: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_wh_stock_item' name, 'Warehouse Table for WH Stock Item: WH Stock Item Dimension' description
			union
			select 'Staging' database_name, 'Stock' flow_name, 'dim_wh_stock_item_batch' name, 'Staging Table for WH Stock Item Batch: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'dim_wh_stock_item_batch' name, 'Warehouse Table for WH Stock Item Batch: WH Stock Item Batch Dimension' description

			union
			select 'Staging' database_name, 'Stock' flow_name, 'fact_wh_stock_item_batch_movement' name, 'Staging Table for WH Stock Item Batch Movement: Data ready to be inserted in WH' description
			union
			select 'Warehouse' database_name, 'Stock' flow_name, 'fact_wh_stock_item_batch_movement' name, 'Warehouse Table for WH Stock Item Batch Movement: WH Stock Item Batch Fact' description
			) t
		inner join
			ControlDB.config.t_Database d on t.database_name = d.database_name
		left join
			ControlDB.config.t_Table t2 on t.name = t2.table_name and d.idDatabase = t2.idDatabase
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name,
			(select isnull(max(idTable), 0) idTableMax
			from ControlDB.config.t_Table) id 
	where t2.idTable is null
go 

-- t_Package
insert into ControlDB.config.t_Package (idPackage, idPackageType, idDeveloper, idFlow, idDatabaseFrom, idDatabaseTo, 
	package_name, description) 

	select row_number() over (order by (select 0)) + id.idPackageMax idPackage, pt.idPackageType, d.idDeveloper, f.idFlow, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'LND-STG' codPackageType, 'jsola' developer,  'Stock' flow_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_stock_tables' name, 'SSIS PKG for reading Stock data (ERP) from Landing to Staging - Prepare to insert in Warehouse' description
			union
			select 'STG-DWH' codPackageType, 'jsola' developer,  'Stock' flow_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_stock_tables' name, 'SSIS PKG for reading Stock data (ERP) from Staging to Warehouse - Update Dimension' description
			) t
		left join
			ControlDB.config.t_Package t2 on t.name = t2.package_name	
		inner join
			ControlDB.config.cat_PackageType pt on t.codPackageType = pt.codPackageType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Flow f on t.flow_name = f.flow_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idPackage), 0) idPackageMax
			from ControlDB.config.t_Package) id 
	where t2.idPackage is null
go 


-- t_SP
insert into ControlDB.config.t_SP (idSP, idSPType, idDeveloper, idPackage, idDatabaseFrom, idDatabaseTo, 		
	sp_name, description) 

	select row_number() over (order by (select 0)) + id.idSPMax idSP, st.idSPType, d.idDeveloper, p.idPackage, db_f.idDatabase, db_t.idDatabase,  
		t.name, t.description
	from
			(select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_stock_method_type' name, 'GET SP for reading Stock Method Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_stock_method_type' name, 'GET SP for reading Stock Method Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_stock_method_type' name, 'MERGE SP for upading Stock Method Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_stock_batch_type' name, 'GET SP for reading Stock Batch Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_stock_batch_type' name, 'GET SP for reading Stock Batch Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_stock_batch_type' name, 'MERGE SP for upading Stock Batch Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_stock_adjustment_type' name, 'GET SP for reading Stock Adjustment Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_stock_adjustment_type' name, 'GET SP for reading Stock Adjustment Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_stock_adjustment_type' name, 'MERGE SP for upading Stock Adjustment Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_stock_movement_type' name, 'GET SP for reading Stock Movement Type data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_stock_movement_type' name, 'GET SP for reading Stock Movement Type data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_stock_movement_type' name, 'MERGE SP for upading Stock Movement Type Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_stock_movement_period' name, 'GET SP for reading Stock Movement Period data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_stock_movement_period' name, 'GET SP for reading Stock Movement Period data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_stock_movement_period' name, 'MERGE SP for upading Stock Movement Period Dimension data' description
			union

			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_wh_stock_item' name, 'GET SP for reading WH Stock Item data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_wh_stock_item' name, 'GET SP for reading WH Stock Item data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_wh_stock_item' name, 'MERGE SP for upading WH Stock Item Dimension data' description
			union
			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_wh_stock_item_batch' name, 'GET SP for reading WH Stock Item Batch data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_wh_stock_item_batch' name, 'GET SP for reading WH Stock Item Batch data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_wh_stock_item_batch' name, 'MERGE SP for upading WH Stock Item Batch Dimension data' description
			union

			select 'GET' codSPType, 'jsola' developer,  'lnd_stg_stock_tables' package_name, 'Landing' databaseFrom_name, 'Staging' databaseTo_name, 
				'lnd_stg_get_stock_wh_stock_item_batch_movement' name, 'GET SP for reading WH Stock Item Batch Movement data from Landing (data in different tables) to Staging' description
			union
			select 'GET' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_get_stock_wh_stock_item_batch_movement' name, 'GET SP for reading WH Stock Item Batch Movement data from Staging to Warehouse' description
			union
			select 'MERGE' codSPType, 'jsola' developer,  'stg_dwh_stock_tables' package_name, 'Staging' databaseFrom_name, 'Warehouse' databaseTo_name, 
				'stg_dwh_merge_stock_wh_stock_item_batch_movement' name, 'MERGE SP for upading WH Stock Item Batch Movement Fact data' description
			) t
		left join
			ControlDB.config.t_SP t2 on t.name = t2.sp_name
		inner join
			ControlDB.config.cat_SPType st on t.codSPType = st.codSPType
		inner join
			ControlDB.config.t_Developer d on t.developer = d.developer
		inner join
			ControlDB.config.t_Package p on t.package_name = p.package_name
		inner join
			ControlDB.config.t_Database db_f on t.databaseFrom_name = db_f.database_name
		inner join
			ControlDB.config.t_Database db_t on t.databaseTo_name = db_t.database_name,	
			(select isnull(max(idSP), 0) idSPMax
			from ControlDB.config.t_SP) id 
	where t2.idSP is null
go 


