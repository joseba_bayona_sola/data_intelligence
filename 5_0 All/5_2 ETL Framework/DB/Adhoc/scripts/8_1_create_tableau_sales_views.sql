
use Adhoc
go

drop view tableau.aux_oh_sihy_daily_v
go 

create view tableau.aux_oh_sihy_daily_v as

	select 
		oh.order_id_bk, oh.order_no, oh.order_date_c, 
		oh.tld, oh.website_group, oh.website, oh.store_name, 
		oh.website_group_create,
		oh.market_name, 
		oh.customer_id, oh.customer_email, 
		oh.country_code_ship, oh.country_code_bill, 
		oh.order_stage_name, oh.order_status_name, 
		oh.channel_name, oh.marketing_channel_name, 
		oh.customer_status_name, oh.rank_seq_no, oh.customer_order_seq_no, oh.rank_seq_no_web, oh.customer_order_seq_no_web, 
		oh.product_type_oh_name, oh.num_diff_product_type_oh, 
		ol.product_type_name, ol.category_name, ol.cl_type_name, ol.cl_feature_name,
		ol.product_id_magento, ol.product_family_name,

		ol.qty_unit, ol.qty_pack, 
		ol.rank_qty_time, ol.qty_time, acs.rank_next_order_freq_time, acs.next_order_freq_time,
		ol.price_type_name, ol.discount_f, 
		ol.local_price_pack, ol.local_price_pack_discount, 
		
		isnull(ee.conversion_f, 'N') conversion_f, isnull(ee.repetition_f, 'N') repetition_f,

		ol.local_total_inc_vat, ol.local_total_exc_vat, ol.local_total_vat, ol.local_total_prof_fee, 
		ol.global_total_inc_vat, ol.global_total_exc_vat, ol.global_total_vat, ol.global_total_prof_fee
	from 
			Warehouse.sales.dim_order_header_v oh
		inner join
			Warehouse.act.fact_activity_sales_v acs on oh.order_id_bk = acs.order_id_bk
		inner join
			Warehouse.sales.fact_order_line_v ol on oh.order_id_bk = ol.order_id_bk
		left join
			Adhoc.sales.aux_oh_everclear_elite ee on oh.order_id_bk = ee.order_id_bk
	where oh.order_date_c > '2017-01-01'
		and oh.website_group = 'VisionDirect'
		and ol.product_id_magento in (3168, 1084, 2301, 2298, 2334, 2417)
go

drop view tableau.aux_oh_shipping_info_v
go 

create view tableau.aux_oh_shipping_info_v as

	select order_id_bk, order_no, 
		order_date_c, order_week_day, order_time_name, order_day_part, 
		shipment_date_c, shipment_week_day, shipment_time_name, shipment_day_part, 
		rank_shipping_days, shipping_days, 
		website, country_code_ship, 
		global_total_inc_vat, global_total_exc_vat
	from Warehouse.sales.dim_order_header_v
	where website_group = 'VisionDirect' 
		and order_date > '2017-01-01'
		and shipment_date is not null
go