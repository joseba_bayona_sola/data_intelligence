use Adhoc
go


drop procedure rs.dwh_rs_param_sales_price_type
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Marketing Channel
-- ==========================================================================================

create procedure rs.dwh_rs_param_sales_price_type
as
begin
	set nocount on

	select idPriceType_sk, price_type_name
	from Warehouse.sales.dim_price_type
	order by price_type_name

end;
go 





drop procedure rs.dwh_rs_param_sales_marketing_channel
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Marketing Channel
-- ==========================================================================================

create procedure rs.dwh_rs_param_sales_marketing_channel
as
begin
	set nocount on

	select idMarketingChannel_sk, marketing_channel_name
	from Warehouse.sales.dim_marketing_channel
	order by marketing_channel_name

end;
go 



drop procedure rs.dwh_rs_param_sales_rank_qty_time
go

-- ==========================================================================================
-- Author: Joseba Bayona Sola
-- Date: 06-04-2018
-- Changed: 
	--	XX-XX-XXXX	XXXX XXXX	XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
-- ==========================================================================================
-- Description: Get PARAM data: Rank QTY Time
-- ==========================================================================================

create procedure rs.dwh_rs_param_sales_rank_qty_time
as
begin
	set nocount on

	select distinct rank_qty_time
	from Warehouse.sales.dim_rank_qty_time
	order by rank_qty_time

end;
go 

