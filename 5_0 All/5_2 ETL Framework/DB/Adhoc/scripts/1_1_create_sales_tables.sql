use Adhoc
go 

create table Adhoc.sales.aux_oh_everclear_elite(
	order_id_bk					bigint NOT NULL,
	customer_id					int NOT NULL, 
	conversion_f				char(1), 
	repetition_f				char(1));

alter table Adhoc.sales.aux_oh_everclear_elite add constraint [PK_sales_aux_oh_everclear_elite]
	primary key clustered (order_id_bk);
go

