
CREATE LOGIN tableau_user WITH PASSWORD = 'tableau_user';

CREATE USER tableau_user FOR LOGIN tableau_user;

ALTER SERVER ROLE sysadmin ADD MEMBER tableau_user
GO
