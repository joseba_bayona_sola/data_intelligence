
-- ga_report - db_schema - db_table - ga_attribute_type - ga_name - db_column_id - db_column_name 


select t1.ga_report,  t1.db_schema, t1.db_table, t2.ga_attribute_type, t2.ga_name, t2.db_column_id, t2.db_column_name 
into DW_GetLenses_jbs.dbo.ga_poc_ga_report
from
	(select 'Transaction Channel' ga_report, 'ga' db_schema, 'transaction_channel_data' db_table) t1,
	(select 'dimensions' ga_attribute_type, 'ga:transactionId' ga_name, '1' db_column_id, 'transactionId' db_column_name
	union
	select 'dimensions' ga_attribute_type, 'ga:date' ga_name, '2' db_column_id, 'transaction_date' db_column_name
	union
	select 'dimensions' ga_attribute_type, 'ga:source' ga_name, '3' db_column_id, 'source' db_column_name
	union
	select 'dimensions' ga_attribute_type, 'ga:medium' ga_name, '4' db_column_id, 'medium' db_column_name
	union
	select 'metrics' ga_attribute_type, 'ga:transactions' ga_name, '5' db_column_id, 'transactions' db_column_name) t2
union
select t1.ga_report,  t1.db_schema, t1.db_table, t2.ga_attribute_type, t2.ga_name, t2.db_column_id, t2.db_column_name 
from
	(select 'Overall Session' ga_report, 'ga' db_schema, 'overall_session_data' db_table) t1,
	(select 'dimensions' ga_attribute_type, 'ga:date' ga_name, '1' db_column_id, 'date' db_column_name
	union
	select 'metrics' ga_attribute_type, 'ga:sessions' ga_name, '2' db_column_id, 'sessions' db_column_name) t2
order by t1.ga_report, t2.db_column_id

	select *
	from DW_GetLenses_jbs.dbo.ga_poc_ga_report

	select distinct ga_report, db_schema, db_table
	from DW_GetLenses_jbs.dbo.ga_poc_ga_report

-- Email performance
	-- dimensions source - campaign
	-- metrics transactions - revenue

select top 1000 *
from Landing.mag.ga_entity_transaction_data_aud

-- DIMENSIONS
-- 'ga:transactionId', 'ga:date', 'ga:source', 'ga:medium'
-- transactionId, transaction_date, source, medium

-- METRICS
-- ga:transactions
-- transactions

select top 1000 *
from Landing.ga.transaction_channel_data_aud
-- where upd_ts is not null
order by ins_ts

	select top 1000 view_id, profile_name, count(*), min(ins_ts), max(ins_ts)
	from Landing.ga.transaction_channel_data_aud
	group by view_id, profile_name
	order by view_id, profile_name

	select top 1000 transaction_date, count(*), min(ins_ts), max(ins_ts)
	from Landing.ga.transaction_channel_data_aud
	group by transaction_date
	order by transaction_date

	select top 1000 source, medium, count(*), min(ins_ts), max(ins_ts)
	from Landing.ga.transaction_channel_data_aud
	group by source, medium
	order by source, medium


select *
from Landing.ga.overall_session_data_aud
-- where upd_ts is not null
order by view_id, date

	select top 1000 view_id, profile_name, count(*)
	from Landing.ga.overall_session_data_aud
	group by view_id, profile_name
	order by view_id, profile_name

	select top 1000 date, count(*)
	from Landing.ga.overall_session_data_aud
	group by date
	order by date
