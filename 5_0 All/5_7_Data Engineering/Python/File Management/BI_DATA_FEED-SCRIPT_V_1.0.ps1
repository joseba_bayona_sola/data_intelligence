﻿
$input_dir_1= "C:\fm_files"

$output_dir= "\\10.0.1.25\Futurmaster\VisionDirect\PROD\_Interfaces\IN"



#Only the above needs to be updated*

#-------------------------------------------------------------------------------------#
$input_dir= "$input_dir_1\*"
$output_dir_del= "$output_dir\*"
$log_dir="c:/SCRIPT-LOG/BI_DATA_FEED-SCRIPT.log"
$log_dir_test ="c:/SCRIPT-LOG" 
$files = Get-ChildItem -Path  $output_dir\* -Include ERP_FMAN*.txt
$files_input_dir = $input_dir_1

if(!(Test-Path -Path $log_dir_test )){
    #we create if the archive folder does not exist
    #mkdir $log_dir
    New-Item -ItemType "directory" -Path "$log_dir_test"
    New-Item -Path $log_dir_test -Name "BI_DATA_FEED-SCRIPT.log" -ItemType "file"
    }
New-Item -Path $output_dir -Name "1 START_$(get-date -f dd-MM-yyyy_HH_mm_ss).jm1" -ItemType "file"

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) -------------------------------------Start!-------------------------------------" >> $log_dir

#Write-Host "Input_dir = $input_dir"
echo "Input_dir = $input_dir_1" >> $log_dir
#Write-Host "Output_dir = $output_dir"
echo "Output_dir = $output_dir
" >> $log_dir
#Write-Host "Output_dir_del = $output_dir_del"
<# echo "Output_dir_del = $output_dir_del
" >> $log_dir #>

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) -------------------------------------NETWORK-TEST-Start!-------------------------------------" >> $log_dir


echo "$(PING -a $output_dir.TrimStart("\\").TrimEnd("\Futurmaster\VisionDirect\PROD\_Interfaces\IN"))" >> $log_dir


echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) -------------------------------------NETWORK-TEST-END!-------------------------------------" >> $log_dir


if ($files.Count -eq 0) {
    echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) **No files to delete from $output_dir **" >> $log_dir
    }

#Get-ChildItem -Path C:\Users\jmiah\Desktop\output >>c:/SCRIPT-LOG/BI_DATA_FEED-SCRIPT.log

if ($files.Count -gt 0) {
Get-ChildItem -Path $files -Include ERP_FMAN*.txt >> $log_dir
echo " $(get-date -f dd-MM-yyyy_HH_mm_ss) *Files before delete in $output_dir *" >> $log_dir
}


echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) BEGINNING of DELETE" >>$log_dir
Remove-Item $output_dir_del -Include ERP_FMAN*.txt
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) END of DELETE" >>$log_dir


if ($files.Count -gt 0) {
    Get-ChildItem -Path $files >> $log_dir
    echo " $(get-date -f dd-MM-yyyy_HH_mm_ss) *Files After delete in $output_dir *" >> $log_dir
    }

    

    if ($input_dir.Count -gt 0) {
    Get-ChildItem -Path $input_dir\ -Include ERP_FMAN*.txt >> $log_dir
    echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) *The above file(s) are to be copied to $output_dir  *SEE BELOW*" >>$log_dir
    }

#echo >> $log_dir "$files *The above files are to be deleted*" 



#echo  "$files_input_dir >> $log_dir $(get-date -f dd-MM-yyyy_HH_mm_ss) *The above files are to be copied*"  


echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) Beginning of COPY" >>$log_dir
Copy-Item $input_dir -Include ERP_FMAN*.txt  -Destination $output_dir
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) END of COPY" >>$log_dir



if ($output_dir.Count -gt 0) {
    Get-ChildItem -Path $output_dir\* >> $log_dir
    echo "$(get-date -f dd-MM-yyyy_HH_mm_ss) Files After Copy in $output_dir " >> $log_dir
    }


New-Item -Path $output_dir -Name "2 END_$(get-date -f dd-MM-yyyy_HH_mm_ss).jm1" -ItemType "file"

Get-ChildItem -path $output_dir\*.jm1 | where {$_.Lastwritetime -lt (date).addminutes(-5)} | remove-item

echo "
$(get-date -f dd-MM-yyyy_HH_mm_ss) -------------------------------------End!-------------------------------------

" >> $log_dir 


