
-- Parameters:

	-- $input_dir_1
	-- $input_dir

	-- $output_dir
	-- $output_dir_del

	-- $files_input_dir
	-- $files

	-- $log_dir_test
	-- $log_dir

	-- $SftpIp
	-- $Password
	-- $Credential
	-- $ThisSession

	-- $get_childitem_INPUT
	-- $get_childitem_output

-- Powershell Functions: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/test-path?view=powershell-7

	-- echo

	-- Microsoft.PowerShell.Management
		-- Test-Path: cmdlet determines whether all elements of the path exist. It returns $True if all elements exist and $False if any are missing
		-- New-Item: cmdlet creates a new item and sets its value. The types of items that can be created depend on the location of the item
		-- Get-ChildItem: cmdlet gets the items in one or more specified locations. If the item is a container, it gets the items inside the container, known as child items
		-- Remove-Item: deletes one or more items. It can delete many different types of items, including files, folders, registry keys, variables, aliases, and functions
		-- Copy-Item: cmdlet copies an item from one location to another location in the same namespace. For instance, it can copy a file to a folder, but it can't copy a file to a certificate drive

	-- Microsoft.PowerShell.Core	
		-- Where-Object: cmdlet selects objects that have particular property values from the collection of objects that are passed to it
		-- ForEach-Object:  cmdlet performs an operation on each item in a collection of input objects. The input objects can be piped to the cmdlet or specified by using the InputObject parameter

	-- Microsoft.PowerShell.Utility
		-- get-date: cmdlet gets a DateTime object that represents the current date or a date that you specify
		-- Write-Host: cmdlet customizes output
		-- Write-Output: cmdlet sends the specified object down the pipeline to the next command. If the command is the last command in the pipeline, the object is displayed in the console
		-- New-Object System.Management.Automation.PSCredential: cmdlet creates an instance of a .NET Framework or COM object

	-- Posh-SSH Module: https://www.business.com/articles/manage-files-over-sftp-powershell/ // https://www.powershellmagazine.com/2014/07/03/posh-ssh-open-source-ssh-powershell-module/
		-- Get-SFTPSession
		-- Remove-SFTPSession
		-- New-SFTPSession
		-- Get-SFTPChildItem
		-- Set-SFTPFile
		-- Remove-SFTPItem

	-- ping -a 161.71.33.231