﻿$SFTP_LOG_DIRECTORY_FILE = "C:\SCRIPT-LOG\BI_MC_SFTP_LOG.log"
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) 
██╗╬╬╬██╗███████╗██████╗╬██╗╬╬╬██╗╬╬╬╬███████╗████████╗╬█████╗╬██████╗╬████████╗╬╬╬╬╬██████╗╬███████╗╬╬╬╬███████╗╬██████╗██████╗╬██╗██████╗╬████████╗
██║╬╬╬██║██╔════╝██╔══██╗╚██╗╬██╔╝╬╬╬╬██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝╬╬╬╬██╔═══██╗██╔════╝╬╬╬╬██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
██║╬╬╬██║█████╗╬╬██████╔╝╬╚████╔╝╬╬╬╬╬███████╗╬╬╬██║╬╬╬███████║██████╔╝╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║█████╗╬╬╬╬╬╬███████╗██║╬╬╬╬╬██████╔╝██║██████╔╝╬╬╬██║╬╬╬
╚██╗╬██╔╝██╔══╝╬╬██╔══██╗╬╬╚██╔╝╬╬╬╬╬╬╚════██║╬╬╬██║╬╬╬██╔══██║██╔══██╗╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║██╔══╝╬╬╬╬╬╬╚════██║██║╬╬╬╬╬██╔══██╗██║██╔═══╝╬╬╬╬██║╬╬╬
╬╚████╔╝╬███████╗██║╬╬██║╬╬╬██║╬╬╬╬╬╬╬███████║╬╬╬██║╬╬╬██║╬╬██║██║╬╬██║╬╬╬██║╬╬╬╬╬╬╬╚██████╔╝██║╬╬╬╬╬╬╬╬╬███████║╚██████╗██║╬╬██║██║██║╬╬╬╬╬╬╬╬██║╬╬╬
╬╬╚═══╝╬╬╚══════╝╚═╝╬╬╚═╝╬╬╬╚═╝╬╬╬╬╬╬╬╚══════╝╬╬╬╚═╝╬╬╬╚═╝╬╬╚═╝╚═╝╬╬╚═╝╬╬╬╚═╝╬╬╬╬╬╬╬╬╚═════╝╬╚═╝╬╬╬╬╬╬╬╬╬╚══════╝╬╚═════╝╚═╝╬╬╚═╝╚═╝╚═╝╬╬╬╬╬╬╬╬╚═╝╬╬╬
╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬


" >> $SFTP_LOG_DIRECTORY_FILE

Write-Host
"
███████╗████████╗╬█████╗╬██████╗╬████████╗╬╬╬╬╬██████╗╬███████╗╬╬╬╬███████╗╬██████╗██████╗╬██╗██████╗╬████████╗
██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝╬╬╬╬██╔═══██╗██╔════╝╬╬╬╬██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
███████╗╬╬╬██║╬╬╬███████║██████╔╝╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║█████╗╬╬╬╬╬╬███████╗██║╬╬╬╬╬██████╔╝██║██████╔╝╬╬╬██║╬╬╬
╚════██║╬╬╬██║╬╬╬██╔══██║██╔══██╗╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║██╔══╝╬╬╬╬╬╬╚════██║██║╬╬╬╬╬██╔══██╗██║██╔═══╝╬╬╬╬██║╬╬╬
███████║╬╬╬██║╬╬╬██║╬╬██║██║╬╬██║╬╬╬██║╬╬╬╬╬╬╬╚██████╔╝██║╬╬╬╬╬╬╬╬╬███████║╚██████╗██║╬╬██║██║██║╬╬╬╬╬╬╬╬██║╬╬╬
╚══════╝╬╬╬╚═╝╬╬╬╚═╝╬╬╚═╝╚═╝╬╬╚═╝╬╬╬╚═╝╬╬╬╬╬╬╬╬╚═════╝╬╚═╝╬╬╬╬╬╬╬╬╬╚══════╝╬╚═════╝╚═╝╬╬╚═╝╚═╝╚═╝╬╬╬╬╬╬╬╬╚═╝╬╬╬
╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬

$(echo "Disconnecting all SFTP Sessions">> $SFTP_LOG_DIRECTORY_FILE)


#Disconnect all SFTP Sessions
$(Write-Output $(Get-SFTPSession | % { Remove-SFTPSession -SessionId ($_.SessionId) } -Verbose) "<= SFTP SESSION(S) SUCCESSFULLY DISCONNECTED?") >>$SFTP_LOG_DIRECTORY_FILE)

$(Write-Output $(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) "SFTP CONNECTION STATUS:" $ThisSession >> $SFTP_LOG_DIRECTORY_FILE)

$(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) STARTING SFTP CONNECTION" >> $SFTP_LOG_DIRECTORY_FILE

# Set the IP of the SFTP server
$SftpIp = '161.71.33.231' 

$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff
# Set the credentials
$Password = ConvertTo-SecureString 'vdpassword@1' -AsPlainText -Force
$Credential = New-Object System.Management.Automation.PSCredential ('500008294', $Password))

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff
# Establishing the SFTP connection
$ThisSession = New-SFTPSession -ComputerName $SftpIp -Credential $Credential -AcceptKey:$true)
" >> $SFTP_LOG_DIRECTORY_FILE

$(Write-Output $(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) "SFTP CONNECTION STATUS:" $ThisSession >> $SFTP_LOG_DIRECTORY_FILE)

)

"

# Set Input and Output Directory
$INPUT = "C:\sfmc_files\*"
$OUTPUT = '/Import/VD_DWH/'
$SFTP_LOG_DIRECTORY = "c:/SCRIPT-LOG"
$SFTP_LOG_FILE = "BI_MC_SFTP_LOG.log"
$SFTP_LOG_DIRECTORY_FILE = "C:\SCRIPT-LOG\BI_MC_SFTP_LOG.log"
$tempFile = [System.IO.Path]::GetTempFileName() 2>> $SFTP_LOG_DIRECTORY_FILE


#Set Variable for input directory
$get_childitem_INPUT = Get-ChildItem $INPUT -Recurse

#Set Variable for output directory
$get_childitem_output = Get-SFTPChildItem -SessionId ($ThisSession).SessionId $OUTPUT -Verbose

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) 
███████╗████████╗╬█████╗╬██████╗╬████████╗╬╬╬╬╬██████╗╬███████╗╬╬╬╬███████╗╬██████╗██████╗╬██╗██████╗╬████████╗
██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝╬╬╬╬██╔═══██╗██╔════╝╬╬╬╬██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
███████╗╬╬╬██║╬╬╬███████║██████╔╝╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║█████╗╬╬╬╬╬╬███████╗██║╬╬╬╬╬██████╔╝██║██████╔╝╬╬╬██║╬╬╬
╚════██║╬╬╬██║╬╬╬██╔══██║██╔══██╗╬╬╬██║╬╬╬╬╬╬╬██║╬╬╬██║██╔══╝╬╬╬╬╬╬╚════██║██║╬╬╬╬╬██╔══██╗██║██╔═══╝╬╬╬╬██║╬╬╬
███████║╬╬╬██║╬╬╬██║╬╬██║██║╬╬██║╬╬╬██║╬╬╬╬╬╬╬╚██████╔╝██║╬╬╬╬╬╬╬╬╬███████║╚██████╗██║╬╬██║██║██║╬╬╬╬╬╬╬╬██║╬╬╬
╚══════╝╬╬╬╚═╝╬╬╬╚═╝╬╬╚═╝╚═╝╬╬╚═╝╬╬╬╚═╝╬╬╬╬╬╬╬╬╚═════╝╬╚═╝╬╬╬╬╬╬╬╬╬╚══════╝╬╚═════╝╚═╝╬╬╚═╝╚═╝╚═╝╬╬╬╬╬╬╬╬╚═╝╬╬╬
╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬


" >> $SFTP_LOG_DIRECTORY_FILE

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ CHECK FOR LOG DIRECTORY/FILE! ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

#TEST/CREATE THE LOG FILE/FOLDER

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) TESTING FOR LOG DIR'/ FILE" >> $SFTP_LOG_DIRECTORY_FILE

if (Test-Path -Path $SFTP_LOG_DIRECTORY)
     { 
     echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) LOG DIRECTORY CHECK PASSED, $SFTP_LOG_DIRECTORY EXISTS :) " >> $SFTP_LOG_DIRECTORY_FILE
    }

    
    if (Test-Path -Path $SFTP_LOG_DIRECTORY_FILE)
    {
     echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) LOG DIRECTORY FILE CHECK PASSED, $SFTP_LOG_DIRECTORY_FILE EXISTS :) " >> $SFTP_LOG_DIRECTORY_FILE
    }



    if (!(Test-Path -Path $SFTP_LOG_DIRECTORY_FILE))
    {
      Test-Path -Path $SFTP_LOG_DIRECTORY
      New-Item -Path $SFTP_LOG_DIRECTORY -ItemType File -Name $SFTP_LOG_FILE

     echo "($(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) The Log file did not exist, it's been created now -> $(Get-ChildItem $SFTP_LOG_DIRECTORY))" >> $SFTP_LOG_DIRECTORY_FILE

    }


if (!(Test-Path -Path $SFTP_LOG_DIRECTORY))
    {New-Item -ItemType Directory -Path  $SFTP_LOG_DIRECTORY 
     New-Item -Path $SFTP_LOG_DIRECTORY -ItemType File -Name $SFTP_LOG_FILE 

    echo "($(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) The Log file and the folder did not exist, it's been created now -> $(Get-ChildItem -path c:\ -Include "$SFTP_LOG_DIRECTORY"), $(Get-ChildItem $SFTP_LOG_DIRECTORY))" >> $SFTP_LOG_DIRECTORY_FILE
    }


    
    
if (!(Test-Path -Path $SFTP_LOG_DIRECTORY))
    {New-Item -ItemType Directory -Path  $SFTP_LOG_DIRECTORY 
    New-Item -Path $SFTP_LOG_DIRECTORY -ItemType File -Name $SFTP_LOG_FILE 

    echo "($(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) The Log file and the folder did not exist, it's been created now -> $(Get-ChildItem -path c:\ -Include "$SFTP_LOG_DIRECTORY"), $(Get-ChildItem $SFTP_LOG_DIRECTORY))" >> $SFTP_LOG_DIRECTORY_FILE
    }



echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff )════════════════════════ TESTING FOR LOG DIR'/ FILE DONE! ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

echo "

$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) NETWORK_TEST_START:
════════════════════════════════════════════════════════════════════════════════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) 

════════════════════════════════════════════════════════════════════════════════════════════════════
$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) NETWORK_TEST_END:

════════════════════════════════════════════════════════════════════════════════════════════════════
Configuration Summary:
Input = $INPUT
Output = $SftpIp$OUTPUT
SFTP Server/IP = $SftpIp
SERVER_HOSTNAME = $(HOSTNAME.EXE)
SFTP_SESSION = $(Write-Output "SessionId:"$ThisSession.SessionId,
                              "Host:" $ThisSession.Host,
                              "Connected:" $ThisSession.Connected)
WHOAMI = $(whoami.exe)
════════════════════════════════════════════════════════════════════════════════════════════════════
$(Write-Output $(ping -a 161.71.33.231) "════════════════════════════════════════════════════════════════════════════════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE)

$(Write-Output $(ping -a 8.8.8.8) >> $SFTP_LOG_DIRECTORY_FILE)

" >> $SFTP_LOG_DIRECTORY_FILE
 
(Get-Date -f dd-MM-yyyy_HH_mm_ss_ffff)

#Get list of SFTP Directory Files
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *Before Upload* Below are the files that are currently in $SftpIp$OUTPUT ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE
Write-Output $(Get-SFTPChildItem -SessionId ($ThisSession).SessionId $OUTPUT -Verbose | Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000) >> $SFTP_LOG_DIRECTORY_FILE
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *Before Upload* Above are the files that are currently in $SftpIp$OUTPUT ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE


#Get list of Local Directory Files
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *Before Upload* Below are the files that are currently in $INPUT ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE
Write-Output $(Get-ChildItem -path $INPUT |Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000)>> $SFTP_LOG_DIRECTORY_FILE
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *Before Upload* Above are the files that are currently in $INPUT ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE

#DELETE FILES IN LOCAL DIRECTORY THAT ARE OLDER THAN 2 DAYS

Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ BELOW ARE FILES OLDER THAN 2 DAYS/IF ANY* IN $INPUT TO BE DELETED  ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

 if ($(Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)}))
    { 
    Write-Output $(echo "FILE OLDER THAN 2 DAYS TO BE DELETED IN (GONE) $INPUT") $(Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)} |Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000)  >> $SFTP_LOG_DIRECTORY_FILE
    }
Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ ABOVE ARE FILES OLDER THAN 2 DAYS/IF ANY* IN $INPUT TO BE DELETED  ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE


Get-ChildItem -path $INPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddDays(-2))
    {  
    Remove-Item -Path $_.fullname -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationAction SilentlyContinue -Confirm:$false -Debug -Force -Recurse -Verbose
    }
    #write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) $(Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)})FILE OLDER THAN 2 DAYS TO BE DELETED IN $INPUT") $_ -Verbose >> $SFTP_LOG_DIRECTORY_FILE

}

<#ORIGINAL####
Get-ChildItem -path $INPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddDays(-2))
    {  
    Remove-Item -Path $_.fullname -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -InformationAction SilentlyContinue -Confirm:$false -Debug -Force -Recurse -Verbose
    }
    write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff)FILE OLDER THAN 2 DAYS TO BE DELETED IN $INPUT") $_ -Verbose >> $SFTP_LOG_DIRECTORY_FILE
}


###############>
#Get list of Local Directory Files
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *After deleting Local files older than 2 days* Below are the files that are currently in $INPUT ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE
Write-Output $(Get-ChildItem -path $INPUT |Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000)>> $SFTP_LOG_DIRECTORY_FILE
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *After deleting Local files older than 2 days* Above are the files that are currently in $INPUT ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE

#COPY FILES OVER TO SFTP LOCATION THAT ARE OLDER THAN 5 MINS
Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ BELOW ARE FILES OLDER THAN 5 MINUTES/IF ANY* IN $INPUT TO BE COPIED TO $SftpIp$OUTPUT ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

 if (Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddMinutes(-5)})
    {  
    Write-Output $(echo "FILE OLDER THAN 5 MINUTES IN $INPUT TO BE COPIED OVER TO $SftpIp$OUTPUT") $(Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddMinutes(-5)} | Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000) >> $SFTP_LOG_DIRECTORY_FILE
    }
Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ ABOVE ARE FILES OLDER THAN 5 MINUTES/IF ANY* IN $INPUT TO BE COPIED TO $SftpIp$OUTPUT ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE


Get-ChildItem -path $INPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddMinutes(-5))
    {  
        Set-SFTPFile -SessionId ($ThisSession).SessionId -LocalFile $_.fullname -RemotePath $OUTPUT -ErrorAction SilentlyContinue -Debug -Verbose    
    }
    #write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) $(Get-ChildItem -Path $INPUT | Where-Object {$_.LastWriteTime -LT (Get-Date).AddMilliseconds(-5)}) FILE TO BE COPIED OVER TO $SftpIp$OUTPUT") $_ -Verbose >> $SFTP_LOG_DIRECTORY_FILE
}

<#ORIGINAL######

Get-ChildItem -path $INPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddMinutes(-5))
    {  
        Set-SFTPFile -SessionId ($ThisSession).SessionId -LocalFile $_.fullname -RemotePath $OUTPUT -ErrorAction SilentlyContinue -Debug -Verbose
    }

    write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) FILE TO BE COPIED OVER TO $SftpIp$OUTPUT") $_ -Verbose >> $SFTP_LOG_DIRECTORY_FILE
}


###############>


#Get list of SFTP Directory Files
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *After File Upload* Below are the files that are currently in $SftpIp$OUTPUT ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE
Write-Output $(Get-SFTPChildItem -SessionId ($ThisSession).SessionId $OUTPUT -Verbose | Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000)>> $SFTP_LOG_DIRECTORY_FILE
echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ *After File Upload* Above are the files that are currently in $SftpIp$OUTPUT ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE

#DELETE FILES OLDER THAN 2 DAYS IN SFTP LOCATION

Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ BELOW ARE FILES OLDER THAN 2 DAYS/IF ANY* IN $SftpIp$OUTPUT TO BE DELETED  ════════════════════════" >> $SFTP_LOG_DIRECTORY_FILE

if ($(Get-SFTPChildItem -Path $OUTPUT ($ThisSession).SessionId | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)}))
    {  
    Write-Output $(echo "FILE OLDER THAN 2 DAYS IN $SftpIp$OUTPUT TO BE DELETED (GONE)") $(Get-SFTPChildItem -Path $OUTPUT ($ThisSession).SessionId | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)} -Verbose | Format-Table -Property FullName,_, CreationTIme,_, LastAccessTime,_, LastWriteTime,_, Length,_ |Out-String -Width 10000) >> $SFTP_LOG_DIRECTORY_FILE
    
    }
Write-Output "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) ════════════════════════ ABOVE ARE FILES OLDER THAN 2 DAYS/IF ANY* IN $SftpIp$OUTPUT TO BE DELETED  ════════════════════════

" >> $SFTP_LOG_DIRECTORY_FILE


#write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) $(Get-SFTPChildItem -Path $OUTPUT ($ThisSession).SessionId | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)}) FILE OLDER THAN 2 DAYS - TO BE DELETED:") -Verbose >> $SFTP_LOG_DIRECTORY_FILE


Get-SFTPChildItem -SessionId ($ThisSession).SessionId -Path $OUTPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddDays(-2))
    {  
    Remove-SFTPItem ($ThisSession).SessionId $_.FullName -Verbose -Force -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -Debug 
    }
    #write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) $(Get-SFTPChildItem -Path $OUTPUT ($ThisSession).SessionId | Where-Object {$_.LastWriteTime -LT (Get-Date).AddDays(-2)}) FILE OLDER THAN 2 DAYS - TO BE DELETED:") -Verbose >> $SFTP_LOG_DIRECTORY_FILE
}



<#ORIGINAL####

Get-SFTPChildItem -SessionId ($ThisSession).SessionId -Path $OUTPUT | ForEach-Object{
    if ($_.LastWriteTime -lt (Get-Date).AddDays(-2))
    {  
    Remove-SFTPItem ($ThisSession).SessionId $_.FullName -Verbose -Force -ErrorAction SilentlyContinue -WarningAction SilentlyContinue -Debug 
    }
    write-output $(echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) FILE OLDER THAN 2 DAYS - TO BE DELETED:") $_ -Verbose >> $SFTP_LOG_DIRECTORY_FILE
}


##############>

#Disconnect all SFTP Sessions
$(echo "Disconnecting all SFTP Sessions | STATUS:")>> $SFTP_LOG_DIRECTORY_FILE
$(Get-SFTPSession | % { Remove-SFTPSession -SessionId ($_.SessionId) } -Verbose)
Write-Output $ThisSession >> $SFTP_LOG_DIRECTORY_FILE

echo "$(get-date -f dd-MM-yyyy_HH_mm_ss_ffff) 
███████╗███╗╬╬╬██╗██████╗╬╬╬╬╬╬██████╗╬███████╗╬╬╬╬███████╗╬██████╗██████╗╬██╗██████╗╬████████╗
██╔════╝████╗╬╬██║██╔══██╗╬╬╬╬██╔═══██╗██╔════╝╬╬╬╬██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
█████╗╬╬██╔██╗╬██║██║╬╬██║╬╬╬╬██║╬╬╬██║█████╗╬╬╬╬╬╬███████╗██║╬╬╬╬╬██████╔╝██║██████╔╝╬╬╬██║╬╬╬
██╔══╝╬╬██║╚██╗██║██║╬╬██║╬╬╬╬██║╬╬╬██║██╔══╝╬╬╬╬╬╬╚════██║██║╬╬╬╬╬██╔══██╗██║██╔═══╝╬╬╬╬██║╬╬╬
███████╗██║╬╚████║██████╔╝╬╬╬╬╚██████╔╝██║╬╬╬╬╬╬╬╬╬███████║╚██████╗██║╬╬██║██║██║╬╬╬╬╬╬╬╬██║╬╬╬
╚══════╝╚═╝╬╬╚═══╝╚═════╝╬╬╬╬╬╬╚═════╝╬╚═╝╬╬╬╬╬╬╬╬╬╚══════╝╬╚═════╝╚═╝╬╬╚═╝╚═╝╚═╝╬╬╬╬╬╬╬╬╚═╝╬╬╬
╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬

" >> $SFTP_LOG_DIRECTORY_FILE


Write-Host "

███████╗███╗╬╬╬██╗██████╗╬╬╬╬╬╬██████╗╬███████╗╬╬╬╬███████╗╬██████╗██████╗╬██╗██████╗╬████████╗
██╔════╝████╗╬╬██║██╔══██╗╬╬╬╬██╔═══██╗██╔════╝╬╬╬╬██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝
█████╗╬╬██╔██╗╬██║██║╬╬██║╬╬╬╬██║╬╬╬██║█████╗╬╬╬╬╬╬███████╗██║╬╬╬╬╬██████╔╝██║██████╔╝╬╬╬██║╬╬╬
██╔══╝╬╬██║╚██╗██║██║╬╬██║╬╬╬╬██║╬╬╬██║██╔══╝╬╬╬╬╬╬╚════██║██║╬╬╬╬╬██╔══██╗██║██╔═══╝╬╬╬╬██║╬╬╬
███████╗██║╬╚████║██████╔╝╬╬╬╬╚██████╔╝██║╬╬╬╬╬╬╬╬╬███████║╚██████╗██║╬╬██║██║██║╬╬╬╬╬╬╬╬██║╬╬╬
╚══════╝╚═╝╬╬╚═══╝╚═════╝╬╬╬╬╬╬╚═════╝╬╚═╝╬╬╬╬╬╬╬╬╬╚══════╝╬╚═════╝╚═╝╬╬╚═╝╚═╝╚═╝╬╬╬╬╬╬╬╬╚═╝╬╬╬
╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬
"