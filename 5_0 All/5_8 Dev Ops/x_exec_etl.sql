
use ControlDB
go

	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRun.dtsx', @environment_name = 'Prod'

	select @dateFromV = convert(varchar, max(dateTo), 120) 
	from ControlDB.logging.t_ETLBatchRun_v
	where package_name = 'VisionDirectDataWarehouseLoad' and description = 'VD Data Warehouse Load - Daily' and runStatus = 'OK'

	-- select @dateToV = convert(varchar, max(dateTo) + 1, 120) 
	-- from ControlDB.logging.t_ETLBatchRun_v
	-- where package_name = 'VisionDirectDataWarehouseLoad' and description = 'VD Data Warehouse Load - Daily' and runStatus = 'OK'

	select @dateToV = convert(varchar, GETUTCDATE(), 120) 

	select @description = 'VD Data Warehouse Load - Daily', @package_name_call = 'VisionDirectDataWarehouseLoad'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


-----------------------------------------------------------


	declare @folder_name varchar(100), @project_name varchar(100), @package_name varchar(100), @environment_name varchar(100)
	declare @pars varchar(1000)
	declare @dateFromV varchar(20), @dateToV varchar(20)
	declare @description varchar(100), @package_name_call varchar(100) 

	select @folder_name = 'DWH', @project_name = 'ETL', @package_name = 'ETLBatchRunMendix.dtsx', @environment_name = 'Prod'

	select @dateFromV = convert(varchar, max(dateTo)-2, 120) 
	from ControlDB.logging.t_ETLBatchRun_v
	where description = 'VD Mendix Landing - Daily' and runStatus = 'OK'

	-- select @dateToV = convert(varchar, max(dateTo)+1, 120) 
	-- from ControlDB.logging.t_ETLBatchRun_v
	-- where description = 'VD Mendix Landing - Daily' and runStatus = 'OK'

	select @dateToV = convert(varchar, GETUTCDATE(), 120) 

	select @description = 'VD Mendix Landing - Daily', @package_name_call = 'VisionDirectDataWarehouseLoadERP'

	select @pars = '/Par "\"$Project::dateFrom(DateTime)\"";"\"' + @dateFromV + '\"" /Par "\"$Project::dateTo(DateTime)\"";"\"' + @dateToV + '\"" ' + 
		'/Par "\"description\"";"\"' + @description + '\"" /Par "\"package_name\"";"\"' + @package_name_call + '\""'

	exec dbo.sp_ssis_exec @folder_name = @folder_name, @project_name = @project_name, @package_name = @package_name, @environment_name = @environment_name, 
		@pars = @pars


-----------------------------------------------------------


