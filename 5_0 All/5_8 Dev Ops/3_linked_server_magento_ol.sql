use master
go

--------------------- Tables ----------------------------------

----------------------- sales_flat_order_item ----------------------------

	drop  table dbo.sales_flat_order_item

	create table dbo.sales_flat_order_item(
		item_id					int NOT NULL,
		order_id				int NOT NULL,
		store_id				int,
		created_at				datetime,
		updated_at				datetime,
		product_id				int, 
		sku						varchar(255),
		name					varchar(255),
		description				varchar(1000),
		qty_ordered				decimal(12, 4),
		qty_canceled			decimal(12, 4),
		qty_invoiced			decimal(12, 4),
		qty_refunded			decimal(12, 4),
		qty_shipped				decimal(12, 4),
		qty_backordered			decimal(12, 4),
		qty_returned			decimal(12, 4),
		weight					decimal(12, 4),
		row_weight				decimal(12, 4),
		base_price				decimal(12, 4),
		price					decimal(12, 4),
		base_price_incl_tax		decimal(12, 4),
		base_cost				decimal(12, 4),
		base_row_total			decimal(12, 4),
		base_discount_amount	decimal(12, 4),		
		discount_percent		decimal(12, 4),
		base_amount_refunded	decimal(12, 4),
		mutual_amount			decimal(12, 4),
		product_type			varchar(255),
		product_options			varchar(1000),
		is_virtual				int,
		is_lens					int, 
		lens_group_eye			varchar(5));
	go 

	alter table dbo.sales_flat_order_item add constraint [PK_mag_sales_flat_order_item]
		primary key clustered (item_id);
	go

	declare @dateFrom datetime, @dateTo datetime
	set @dateFrom = GETUTCDATE() - 1
	set @dateTo = GETUTCDATE()

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	DECLARE @sql varchar(max)


	set @sql = '
			insert into dbo.sales_flat_order_item(item_id, order_id, store_id, created_at, updated_at, 
				product_id, sku, name, description, 
				qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
				weight, row_weight, 
				base_price, price, base_price_incl_tax, 
				base_cost, 
				base_row_total, 
				base_discount_amount, discount_percent, 
				base_amount_refunded,
				mutual_amount, 
				product_type, is_virtual, is_lens, lens_group_eye) -- product_options
			
			select item_id, order_id, store_id, created_at, updated_at, 
				product_id, sku, name, description, 
				qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, qty_backordered, qty_returned, 
				weight, row_weight, 
				base_price, price, base_price_incl_tax, 
				base_cost, 
				base_row_total, 
				base_discount_amount, discount_percent, 
				base_amount_refunded,
				mutual_amount, 
				product_type, is_virtual, is_lens, lens_group_eye ' -- product_options
		+ 'from openquery(MAGENTO, 
			''select oi.item_id, oi.order_id, oi.store_id, oi.created_at, oi.updated_at,
				oi.product_id, oi.sku, oi.name, oi.description, 
				oi.qty_ordered, oi.qty_canceled, oi.qty_invoiced, oi.qty_refunded, oi.qty_shipped, oi.qty_backordered, oi.qty_returned, 
				oi.weight, oi.row_weight, 
				oi.base_price, oi.price, oi.base_price_incl_tax, 
				oi.base_cost, 
				oi.base_row_total, 
				oi.base_discount_amount, oi.discount_percent, 
				oi.base_amount_refunded,
				oi.mutual_amount, 
				oi.product_type, oi.product_options, oi.is_virtual, oi.is_lens, oi.lens_group_eye 
			from 
				magento01.sales_flat_order o
			inner join
				magento01.sales_flat_order_item oi on o.entity_id = oi.order_id	 ' + 
			'where o.created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
			 	or o.updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)