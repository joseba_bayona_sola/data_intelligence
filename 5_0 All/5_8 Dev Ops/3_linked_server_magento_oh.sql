use master
go

--------------------- Tables ----------------------------------

----------------------- sales_flat_order ----------------------------

	drop table dbo.sales_flat_order

	create table dbo.sales_flat_order(
		entity_id						int NOT NULL,
		increment_id					varchar(50),
		store_id						int,
		customer_id						int, 
		created_at						datetime,
		updated_at						datetime,
		shipping_address_id				int,
		billing_address_id				int,
		state							varchar(32),
		status							varchar(32),
		total_qty_ordered				decimal(12,4),
		base_subtotal					decimal(12,4),
		base_subtotal_canceled			decimal(12,4),
		base_subtotal_invoiced			decimal(12,4),
		base_subtotal_refunded			decimal(12,4),
		base_shipping_amount			decimal(12,4),
		base_shipping_canceled			decimal(12,4),
		base_shipping_invoiced			decimal(12,4),
		base_shipping_refunded			decimal(12,4),
		base_discount_amount			decimal(12,4),
		base_discount_canceled			decimal(12,4),
		base_discount_invoiced			decimal(12,4),
		base_discount_refunded			decimal(12,4),
		base_customer_balance_amount	decimal(12,4),
		base_customer_balance_invoiced	decimal(12,4),
		base_customer_balance_refunded	decimal(12,4),
		base_grand_total				decimal(12,4),
		base_total_canceled				decimal(12,4),
		base_total_invoiced				decimal(12,4),
		base_total_invoiced_cost		decimal(12,4),
		base_total_refunded				decimal(12,4),
		base_total_offline_refunded		decimal(12,4),
		base_total_online_refunded		decimal(12,4),
		bs_customer_bal_total_refunded	decimal(12,4),
		base_total_due					decimal(12,4),
		base_to_global_rate				decimal(12,4),
		base_to_order_rate				decimal(12,4),
		order_currency_code				varchar(255),
		customer_dob					datetime2,
		customer_email					varchar(255),
		customer_firstname				varchar(255),
		customer_lastname				varchar(255),
		customer_middlename				varchar(255),
		customer_prefix					varchar(255),
		customer_suffix					varchar(255),
		customer_taxvat					varchar(255),
		customer_gender					int,
		customer_note					varchar(1000),
		customer_note_notify			int,
		shipping_description			varchar(255),
		coupon_code						varchar(255),
		applied_rule_ids				varchar(255),
		affilBatch						varchar(255),	
		affilCode						varchar(255),
		affilUserRef					varchar(255),
		reminder_date					varchar(255),
		reminder_mobile					varchar(255),
		reminder_period					varchar(255),
		reminder_presc					varchar(255),
		reminder_type					varchar(255),
		reminder_follow_sent			int,
		reminder_sent					int,
		reorder_on_flag					int,
		reorder_profile_id				int,
		automatic_reorder				int,
		postoptics_source				varchar(255),
		postoptics_auto_verification	varchar(255),
		presc_verification_method		varchar(255),
		prescription_verification_type	varchar(25),
		referafriend_code				varchar(10),
		referafriend_referer			int,
		telesales_method_code			varchar(255),
		telesales_admin_username		varchar(255),
		remote_ip						varchar(255),
		warehouse_approved_time			datetime,
		partbill_shippingcost_cost		decimal(12,4), 
		old_order_id					bigint,
		mutual_amount					decimal(12, 4),
		mutual_quotation_id				int);
	go 

	alter table dbo.sales_flat_order add constraint [PK_mag_sales_flat_order]
		primary key clustered (entity_id);
	go


	declare @dateFrom datetime, @dateTo datetime
	set @dateFrom = GETUTCDATE() - 1
	set @dateTo = GETUTCDATE()

	declare @dateFromV varchar(20), @dateToV varchar(20)
	set @dateFromV = convert(varchar, @dateFrom, 120)
	set @dateToV = convert(varchar, @dateTo, 120)

	DECLARE @sql varchar(max)

	set @sql = '
			insert into dbo.sales_flat_order(entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
				base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
				base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
				base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
				base_grand_total, 
				base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
				base_to_global_rate, base_to_order_rate, order_currency_code,
				customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
				customer_taxvat, customer_gender, customer_note, customer_note_notify, 
				shipping_description, 
				coupon_code, applied_rule_ids,
				affilBatch, affilCode, affilUserRef, 
				reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
				reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
				presc_verification_method, prescription_verification_type, 
				referafriend_code, referafriend_referer, 
				telesales_method_code, telesales_admin_username, 
				remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
				old_order_id, 
				mutual_amount, mutual_quotation_id) 

			select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
				base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
				base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
				base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
				base_grand_total, 
				base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
				base_to_global_rate, base_to_order_rate, order_currency_code,
				customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
				customer_taxvat, customer_gender, customer_note, customer_note_notify, 
				shipping_description, 
				coupon_code, applied_rule_ids,
				affilBatch, affilCode, affilUserRef, 
				reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
				reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
				presc_verification_method, prescription_verification_type, 
				referafriend_code, referafriend_referer, 
				telesales_method_code, telesales_admin_username, 
				remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
				old_order_id, 
				mutual_amount, mutual_quotation_id ' + 
		+ 'from openquery(MAGENTO, 
			''select entity_id, increment_id, store_id, customer_id, created_at, updated_at, 
				shipping_address_id, billing_address_id, 
				state, status, 
				total_qty_ordered, 
				base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, 
				base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, 
				base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded, 
				base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded,
				base_grand_total, 
				base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_refunded, base_total_offline_refunded, base_total_online_refunded, bs_customer_bal_total_refunded, base_total_due,
				base_to_global_rate, base_to_order_rate, order_currency_code,
				customer_dob, customer_email, customer_firstname, customer_lastname, customer_middlename, customer_prefix, customer_suffix, 
				customer_taxvat, customer_gender, customer_note, customer_note_notify, 
				shipping_description, 
				coupon_code, applied_rule_ids,
				affilBatch, affilCode, affilUserRef, 
				reminder_date, reminder_mobile, reminder_period, reminder_presc, reminder_type, reminder_follow_sent, reminder_sent, 
				reorder_on_flag, reorder_profile_id, automatic_reorder, postoptics_source, postoptics_auto_verification, 
				presc_verification_method, prescription_verification_type, 
				referafriend_code, referafriend_referer, 
				telesales_method_code, telesales_admin_username, 
				remote_ip, warehouse_approved_time, partbill_shippingcost_cost, 
				old_order_id, 
				mutual_amount, mutual_quotation_id
			from magento01.sales_flat_order  ' +  
			'where created_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')
				or updated_at between STR_TO_DATE(''''' + @dateFromV + ''''', ''''%Y-%m-%d %H:%i:%s'''') and STR_TO_DATE(''''' + @dateToV + ''''', ''''%Y-%m-%d %H:%i:%s'''')				
			'')'

	exec(@sql)
