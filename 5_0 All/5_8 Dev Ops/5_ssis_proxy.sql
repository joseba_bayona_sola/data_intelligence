USE [msdb]
GO

/****** Object:  ProxyAccount [SSISCredential]    Script Date: 27/09/2018 12:39:04 ******/
EXEC msdb.dbo.sp_add_proxy @proxy_name=N'SSISCredential',@credential_name=N'dwh4g3nt', 
		@enabled=1
GO

EXEC msdb.dbo.sp_grant_proxy_to_subsystem @proxy_name=N'SSISCredential', @subsystem_id=3
GO

EXEC msdb.dbo.sp_grant_proxy_to_subsystem @proxy_name=N'SSISCredential', @subsystem_id=11
GO


