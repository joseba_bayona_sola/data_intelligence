
USE [master]
GO


/****** Object:  LinkedServer [MAGENTO_PROV]    Script Date: 17/01/2019 17:02:13 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'MAGENTO_PROV', @srvproduct=N'MySQL_repl', @provider=N'MSDASQL', @datasrc=N'MySQL_repl'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'MAGENTO_PROV',@useself=N'False',@locallogin=NULL,@rmtuser=N'root',@rmtpassword='SoMlInexPA'

GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'rpc', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'rpc out', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO_PROV', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO



select store_id, code, website_id, group_id, name, sort_order, is_active
from openquery(MAGENTO_PROV, 'select * from magento01.core_store')
