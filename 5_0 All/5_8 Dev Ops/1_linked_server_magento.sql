

USE [master]
GO




/****** Object:  LinkedServer [MAGENTO]    Script Date: 07/09/2018 09:08:28 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'MAGENTO', @srvproduct=N'MySQL_repl2', @provider=N'MSDASQL', @datasrc=N'MySQL_repl2'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'MAGENTO',@useself=N'False',@locallogin=NULL,@rmtuser=N'root',@rmtpassword='M4g3nt0123'

GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'rpc', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'rpc out', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MAGENTO', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


select store_id, code, website_id, group_id, name, sort_order, is_active
from openquery(MAGENTO, 'select * from magento01.core_store')
