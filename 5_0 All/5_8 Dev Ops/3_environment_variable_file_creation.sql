/**************************************************************************************
    This script creates a script to generate and SSIS Environment and its variables.
    Replace the necessary entries to create a new envrionment
    ***NOTE: variables marked as sensitive have their values masked with '<REPLACE_ME>'.
        These values will need to be replace with the actual values
**************************************************************************************/

DECLARE @ReturnCode INT=0, @folder_id bigint

/*****************************************************
    Variable declarations, make any changes here
*****************************************************/
DECLARE @folder sysname = 'DWH' /* this is the name of the new folder you want to create */
        , @env sysname = 'Test' /* this is the name of the new environment you want to create */
        , @Adhoc_ADO.ConnectionString nvarchar= N'Data Source=SQLFORBI;Initial Catalog=Adhoc;Integrated Security=True;Application Name=SSIS-ETL- SQLFORBI.Adhoc;'
        , @Adhoc_OLEDB.ConnectionString nvarchar= N'Data Source=SQLFORBI;Initial Catalog=Adhoc;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;'
        , @ControlDB_ADO.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=ControlDB;Integrated Security=True;Application Name=SSIS-ETL-localhost.ControlDB;'
        , @ControlDB_OLEDB.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=ControlDB;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;'
        , @Landing_ADO.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Landing;Integrated Security=True;Application Name=SSIS-ETL-localhost.Landing;'
        , @Landing_OLEDB.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Landing;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;'
        , @map_act_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_act_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_alloc_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_alloc_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_gen_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_gen_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_mag_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_mag_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_migra_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_migra_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_po_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_po_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_prod_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_prod_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_rec_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_rec_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_sales_ch_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_sales_ch_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_sales_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_sales_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_stock_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_stock_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @map_vat_tables nvarchar= N'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\SQLFORBI\map_files\map_vat_tables.xls;Extended Properties="EXCEL 8.0;HDR=YES";'
        , @Staging_ADO.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Staging;Integrated Security=True;Application Name=SSIS-ETL-localhost.Staging;'
        , @Staging_OLEDB.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Staging;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;'
        , @Warehouse_ADO.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Warehouse;Integrated Security=True;Application Name=SSIS-ETL-localhost.Warehouse;'
        , @Warehouse_OLEDB.ConnectionString nvarchar= N'Data Source=localhost;Initial Catalog=Warehouse;Provider=SQLNCLI11.1;Integrated Security=SSPI;Auto Translate=False;'
;
/* Starting the transaction */
BEGIN TRANSACTION
    IF NOT EXISTS (SELECT 1 FROM [SSISDB].[catalog].[folders] WHERE name = @folder)
    BEGIN
        RAISERROR('Creating folder: %s ...', 10, 1, @folder) WITH NOWAIT;
        EXEC @ReturnCode = [SSISDB].[catalog].[create_folder] @folder_name=@folder, @folder_id=@folder_id OUTPUT
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;
    END

    RAISERROR('Creating Environtment: %s', 10, 1, @env) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment] @folder_name=@folder, @environment_name=@env
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    /******************************************************
        Variable creation
    ******************************************************/
    RAISERROR('Creating variable: Adhoc_ADO.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Adhoc_ADO.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Adhoc_ADO.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Adhoc_OLEDB.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Adhoc_OLEDB.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Adhoc_OLEDB.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: ControlDB_ADO.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'ControlDB_ADO.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@ControlDB_ADO.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: ControlDB_OLEDB.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'ControlDB_OLEDB.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@ControlDB_OLEDB.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Landing_ADO.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Landing_ADO.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Landing_ADO.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Landing_OLEDB.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Landing_OLEDB.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Landing_OLEDB.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_act_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_act_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_act_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_alloc_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_alloc_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_alloc_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_gen_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_gen_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_gen_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_mag_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_mag_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_mag_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_migra_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_migra_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_migra_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_po_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_po_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_po_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_prod_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_prod_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_prod_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_rec_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_rec_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_rec_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_sales_ch_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_sales_ch_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_sales_ch_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_sales_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_sales_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_sales_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_stock_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_stock_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_stock_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: map_vat_tables ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'map_vat_tables'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@map_vat_tables
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Staging_ADO.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Staging_ADO.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Staging_ADO.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Staging_OLEDB.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Staging_OLEDB.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Staging_OLEDB.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Warehouse_ADO.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Warehouse_ADO.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Warehouse_ADO.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

    RAISERROR('Creating variable: Warehouse_OLEDB.ConnectionString ...', 10, 1) WITH NOWAIT;
    EXEC @ReturnCode = [SSISDB].[catalog].[create_environment_variable]
        @variable_name=N'Warehouse_OLEDB.ConnectionString'
        , @sensitive=0
        , @description=N''
        , @environment_name=@env
        , @folder_name=@folder
        , @value=@Warehouse_OLEDB.ConnectionString
        , @data_type=N'String'
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback;

COMMIT TRANSACTION
RAISERROR(N'Complete!', 10, 1) WITH NOWAIT;
GOTO EndSave

QuitWithRollback:
IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
RAISERROR(N'Variable creation failed', 16,1) WITH NOWAIT;

EndSave:
GO
