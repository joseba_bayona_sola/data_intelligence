
USE [master]
GO

/****** Object:  LinkedServer [MENDIX_DB_RESTORE]    Script Date: 07/09/2018 09:10:52 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'MENDIX_DB_RESTORE', @srvproduct=N'postgreSQL_mendix_db_res', @provider=N'MSDASQL', @datasrc=N'postgreSQL_mendix_db_res'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'MENDIX_DB_RESTORE',@useself=N'False',@locallogin=NULL,@rmtuser=N'username',@rmtpassword='password'

GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MENDIX_DB_RESTORE', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO


select id,
	supplierID, supplierincrementid, supplierName, currencyCode, supplierType, NULL isEDIEnabled,
	vatCode, defaultLeadTime,
	email, telephone, fax,
	NULL flatFileConfigurationToUse,
	street1, street2, street3, city, region, postcode,
	vendorCode, vendorShortName, vendorStoreNumber, 
	defaulttransitleadtime, aggregatable 
from openquery(MENDIX_DB_RESTORE,'
	select id,
		supplierID, supplierincrementid, supplierName, currencyCode, supplierType, 
		vatCode, defaultLeadTime,
		email, telephone, fax,

		street1, street2, street3, city, region, postcode,
		vendorCode, vendorShortName, vendorStoreNumber, 
		defaulttransitleadtime, aggregatable 
	from public."suppliers$supplier"')
