
---- PRODUCT CATALOG TABLE

select id, prd_id, 
	category_name, type, 
	product_name, oracle_name, size, number_of_boxes_to_display, 
	country_iso, organisation_id, 
	article_description, show_on_thanks, article_id
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
order by category_name

	select type, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by type
	order by type

	select size, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by size
	order by size

	select number_of_boxes_to_display, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by number_of_boxes_to_display
	order by number_of_boxes_to_display

	select category_name, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by category_name
	order by count(*) desc, category_name

select product_id, productgroup, product_name, product_model
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog_2
order by productgroup, product_name

select product_id, productgroup, product_name, product_model, category_name, 
	count(*) over (partition by category_name) num_rep
from DW_GetLenses_jbs.dbo.lenswaynl_products_category_name
where category_name <> 'NULL' and category_name <> '0'
order by num_rep desc, productgroup, product_name

	select category_name, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_products_category_name
	group by category_name
	order by count(*) desc

--------------------------------------------------------

---- PRODUCT SKU TABLE

select top 1000 
	id, item_id, item_number, item_desc, 
	category_code, category_desc, 
	number_of_lenses, qty_in_ml, 
	bc, diam, power, cyl, axis, [add], colour, 
	type, material
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
-- from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2

	-- SKU items in both tables
	select s1.*
	from 
			DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s1
		inner join 
			DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2 s2 on s1.item_id = s2.item_id


		-- VIEW 
		drop view dbo.lenswaynl_sku_oracle_v 

		create view dbo.lenswaynl_sku_oracle_v as
			select 
				item_id, item_number, item_desc, 
				category_code, category_desc, 
				number_of_lenses, qty_in_ml, 
				bc, diam, power, cyl, axis, [add], colour, 
				type, material
			from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
			union 
			select 
				item_id, item_number, item_desc, 
				category_code, category_desc, 
				number_of_lenses, qty_in_ml, 
				bc, diam, power, cyl, axis, [add], colour, 
				type, material
			from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2

			select top 1000 *
			from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v

	-- Number of Records			
	select count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v

	-- Different Products (Categories)
	select category_code, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v
	group by category_code, category_desc 
	order by category_code, category_desc

	-- Parameters
	select power, count(*) -- bc, diam, power, cyl, axis, [add], colour, 
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v
	group by power
	order by power

--------------------------------------------------------

---- PRODUCT CATALOG - SKU CATEGORIES

select s.category_code, s.category_desc, s.num_rows, 
	p.id, p.prd_id, p.category_name, p.type, p.product_name
from
		(select category_code, category_desc, count(*) num_rows
		from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v
		group by category_code, category_desc) s 
	full join
		(select * 
		from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog 
		where category_name <> '0' and category_name is not null) p on s.category_code = p.category_name 
-- where s.category_code is not null and p.category_name is not null
-- where s.category_code is null 
-- where p.category_name is null
-- where p.category_name = '1127'
order by s.category_code, s.category_desc, p.category_name

select s.category_code, s.category_desc, s.num_rows, 
	p.productgroup, p.product_id, p.product_name
from 
		(select category_code, category_desc, count(*) num_rows
		from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v
		group by category_code, category_desc) s 
	full join
		DW_GetLenses_jbs.dbo.lenswaynl_products_category_name p on s.category_code = p.category_name 
-- where s.category_code is not null and p.category_name is not null
-- where s.category_code is null 
where p.category_name is null
-- where p.category_name = '1127'
order by s.category_code, s.category_desc, p.category_name
