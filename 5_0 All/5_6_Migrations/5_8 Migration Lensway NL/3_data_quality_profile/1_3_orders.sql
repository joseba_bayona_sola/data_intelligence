
select top 1000 brand, order_number, 
	order_date, shipping_date, 
	order_source, 
	customer_id, email_address, 
	first_name, last_name, street_address, city, postal_code, country, phone_number, customers_dob, 
	payment_terms, ship_method, order_category
from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
order by order_date desc

	select count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full

	select brand, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by brand
	order by brand	

	-- Cardinality Customers // 1584 orders
	select oh.brand, oh.order_number, 
		oh.order_date, oh.shipping_date, 
		oh.order_source, 
		oh.customer_id, oh.email_address
	from 
			DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh
		left join
			DW_GetLenses_jbs.dbo.lenswaynl_customer_full c on oh.customer_id = c.customer_id
	where c.customer_id is null
	order by c.customer_id, order_date

		-- 453 cusotmers
		select oh.customer_id, oh.email_address, count(*) num_orders
		from 
				DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh
			left join
				DW_GetLenses_jbs.dbo.lenswaynl_customer_full c on oh.customer_id = c.customer_id
		where c.customer_id is null
		group by oh.customer_id, oh.email_address
		order by oh.customer_id, oh.email_address

	select top 1000 customer_id, email_address, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by customer_id, email_address
	order by count(*) desc, customer_id, email_address


	select year(order_date), count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by year(order_date)
	order by year(order_date)


	select order_source, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by order_source
	order by count(*) desc, order_source

	select payment_terms, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by payment_terms
	order by payment_terms

	select ship_method, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by ship_method
	order by ship_method

	select order_category, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	group by order_category
	order by order_category


	
	-- Duplicated Orders (Splitted Orders)
	select count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
	where year(order_date) = 2017

	select *
	from
		(select brand, order_number, customer_id, order_date, 
			count(*) over (partition by customer_id, order_date) num_rep
		from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
		where year(order_date) = 2017) t
	where num_rep > 1
		-- and customer_id in (4531707, 792034, 892103, 1646984, 4307557)
	order by customer_id, order_number
	