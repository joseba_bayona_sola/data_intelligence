

select product_id, category_code, magento_id
from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping

select product_id, productgroup, product_name, product_model, category_name
from DW_GetLenses_jbs.dbo.lenswaynl_products_category_name

select p1.product_id, p1.productgroup, p1.product_name, p1.product_model, p1.category_name, p2.category_code
from 
		DW_GetLenses_jbs.dbo.lenswaynl_products_category_name p1
	inner join
		DW_GetLenses_jbs.dbo.lenswaynl_product_mapping p2 on p1.product_id = p2.product_id



	select p.category_code, ol.category_desc, ol.num_lines, ol.sum_sales_inc_vat, 
		p.magento_id,
		s.category_code
	from
			(select inventory_item_id, item_number, category_desc, count(*) num_lines, sum(convert(decimal(12, 4), sales_inc_vat)) sum_sales_inc_vat
			from 
				(select ol.*
				from 
						DW_GetLenses_jbs.dbo.lenswaynl_ol_full ol
					inner join
						DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh on ol.order_number = oh.order_number
				-- where year(oh.order_date) in (2015, 2016, 2017)
				) ol
			group by inventory_item_id, item_number, category_desc) ol
		full join
			DW_GetLenses_jbs.dbo.lensonnl_productdata_v p on ol.inventory_item_id = p.item_id
		inner join
			DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v s on ol.inventory_item_id = s.item_id
	-- where s.category_code is not null and p.category_name is not null
	-- where s.category_code is null 
	where p.category_code is null		
	order by ol.category_desc



	-- lenswaynl_products_category_name not used more (old version)
	select s.category_code, s.category_desc, s.num_lines, s.sum_sales_inc_vat, 
		p.productgroup, p.product_id, p.product_name
	from
			(select p.category_code, p.category_desc, sum(num_lines) num_lines, sum(sum_sales_inc_vat) sum_sales_inc_vat
			from
					(select inventory_item_id, item_number, category_desc, count(*) num_lines, sum(convert(decimal(12, 4), sales_inc_vat)) sum_sales_inc_vat
					from 
						(select ol.*
						from 
								DW_GetLenses_jbs.dbo.lenswaynl_ol_full ol
							inner join
								DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh on ol.order_number = oh.order_number
						where year(oh.order_date) in (2015, 2016, 2017)
						) ol
					group by inventory_item_id, item_number, category_desc) ol
				inner join
					(select 
						category_code, category_desc, number_of_lenses, 
						item_id, item_number, item_desc,
						bc, diam, power, axis, cyl, [add], colour
					from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v) p on ol.inventory_item_id = p.item_id
			group by p.category_code, p.category_desc) s
		full join
			(select p1.product_id, p1.productgroup, p1.product_name, p1.product_model, p1.category_name, p2.category_code
			from 
					DW_GetLenses_jbs.dbo.lenswaynl_products_category_name p1
				inner join
					DW_GetLenses_jbs.dbo.lenswaynl_product_mapping p2 on p1.product_id = p2.product_id) p on s.category_code = p.category_code 
	-- where s.category_code is not null and p.category_name is not null
	-- where s.category_code is null 
	where p.category_code is null		
	order by s.category_code, s.category_desc, p.category_name



