

	select top 1000 order_number, count(distinct total_order_price)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	group by order_number
	order by count(distinct total_order_price) desc, order_number

-- OL Numbers: ordered_quantity * unit_selling_price = sales_inc_vat = row_total
select top 1000 
	*, abs(sales_inc_vat - sales_inc_vat_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		ordered_quantity, unit_selling_price, 
		convert(decimal(12, 4), sales_inc_vat) sales_inc_vat, 
		convert(int, ordered_quantity) * convert(decimal(12, 4), unit_selling_price) sales_inc_vat_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	where ISNUMERIC(line_id) = 1) t
where abs(sales_inc_vat - sales_inc_vat_calc) <> 0

-- OL Numbers: sum(sales_inc_vat) = total_order_price
select top 1000 
	 *, abs(total_order_price - total_order_price_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		convert(decimal(12, 4), sales_inc_vat) sales_inc_vat,  
		convert(decimal(12, 4), total_order_price) total_order_price, 
		sum(convert(decimal(12, 4), sales_inc_vat)) over (partition by order_number) total_order_price_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	where ISNUMERIC(line_id) = 1) t
where abs(total_order_price - total_order_price_calc) <> 0

-- OL Numbers: ordered_quantity * unit_list_price = list_price = ol_subtotal
select top 1000 
	*, abs(list_price - list_price_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		ordered_quantity, unit_list_price, 
		convert(decimal(12, 4), list_price) list_price, 
		convert(int, ordered_quantity) * convert(decimal(12, 4), unit_list_price) list_price_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full) t
where abs(list_price - list_price_calc) <> 0

-- OL Numbers - Discounts OL Level: list_price, sales_inc_vat, discount
select order_number, 
	inventory_item_id, item_number, category_desc, 
	list_price, sales_inc_vat, discount
	, (convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat)) * 100 / convert(decimal(12, 4), list_price) discount_calc, 
	convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat) ol_discount
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
where convert(decimal(12, 4), list_price) <> 0

-- OL Numbers - Discounts OH Level: SUM(list_price), total_order_price, total_order_discount
select top 1000 
	order_number, 
	sum_list_price, total_order_price, total_order_discount, 
	(sum_list_price - total_order_price) * 100 / sum_list_price total_order_discount_calc,
	sum_list_price - total_order_price oh_discount
from
	(select order_number, 
		sum(convert(decimal(12, 4), list_price)) sum_list_price, 
		convert(decimal(12, 4), total_order_price) total_order_price, total_order_discount
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	group by order_number, total_order_price, total_order_discount) t
where sum_list_price <> 0
order by order_number


