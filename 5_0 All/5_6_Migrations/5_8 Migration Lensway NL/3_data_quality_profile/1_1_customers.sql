
select customer_id
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
where org_id not in ('83', '121')
order by customer_id

-- 203.554
select top 1000 count(*) over (), customer_id, 
	first_name, last_name, 
	email, creation_date,
	address1, address2, address3, address4, city, postal_code, country, telephone, 
	brand, org_id, personal_number, party_id, party_type, 
	marketing_via_email, marketing_via_sms, subscriber, 
	last_update_date-- , p_last_update_date
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
-- where email = 'LINDA.BLANK@LIVE.NL'
-- where email = 'm.vander.heijden@chello.nl'
order by customer_id 

select count(distinct email)
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full

	-- Customer ID Repeated (18010)
	select top 1000 count(*) over (), *
	from
		(select customer_id, 
			first_name, last_name, 
			email, creation_date,
			address1, address2, address3, address4, city, postal_code, country, telephone, 
			brand, org_id, personal_number, party_id, party_type, 
			marketing_via_email, marketing_via_sms, subscriber, 
			last_update_date,-- , p_last_update_date,  
			count(*) over (partition by customer_id) num_rep
		from DW_GetLenses_jbs.dbo.lenswaynl_customer_full) t
	where num_rep > 1
	order by num_rep desc, customer_id

	-- Email Repeated (72728)
	select top 10000 count(*) over (), *
	from
		(select customer_id, 
			first_name, last_name, 
			email, creation_date,
			address1, address2, address3, address4, city, postal_code, country, telephone, 
			brand, org_id, personal_number, party_id, party_type, 
			marketing_via_email, marketing_via_sms, subscriber, 
			last_update_date, -- p_last_update_date,  
			count(*) over (partition by customer_id) num_rep,
			count(*) over (partition by email) num_rep_email
		from DW_GetLenses_jbs.dbo.lenswaynl_customer_full) t
	where num_rep = 1 and num_rep_email > 1 
		-- and num_rep_email > 7
	-- order by num_rep desc, email, customer_id
	order by num_rep_email desc, email, customer_id

	select email, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	group by email
	having count(*) > 5
	order by count(*) desc

	-- Date Created
	select creation_date, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	group by creation_date
	order by creation_date

	-- Brand 
	select brand, org_id, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	group by brand, org_id
	order by brand, org_id

	-- Address City 
	select city, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	where org_id in ('83', '121')
	group by city
	order by city

	-- Address Country
	select country, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	where org_id in ('83', '121')
	group by country
	order by country
	
	
	-- Marketing Options
	select marketing_via_email, marketing_via_sms, subscriber, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
	where org_id in ('83', '121')
	group by marketing_via_email, marketing_via_sms, subscriber
	order by marketing_via_email, marketing_via_sms, subscriber
