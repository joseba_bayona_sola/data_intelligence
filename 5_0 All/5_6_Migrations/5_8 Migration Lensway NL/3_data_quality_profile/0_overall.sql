
select top 1000 customer_id, 
	first_name, last_name, 
	email, creation_date,
	address1, address2, address3, address4, city, postal_code, country, telephone, 
	brand, org_id, personal_number, party_id, party_type, 
	marketing_via_email, marketing_via_sms, subscriber, 
	last_update_date, p_last_update_date
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full
order by customer_id

select id, prd_id, 
	category_name, type, 
	product_name, oracle_name, size, number_of_boxes_to_display, 
	country_iso, organisation_id, 
	article_description, show_on_thanks, article_id
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
order by category_name

select top 1000 
	id, item_id, item_number, item_desc, 
	category_code, category_desc, 
	number_of_lenses, qty_in_ml, 
	bc, diam, power, cyl, axis, [add], colour, 
	type, material
-- from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2




select top 1000 brand, order_number, 
	order_date, shipping_date, 
	order_source, 
	customer_id, email_address, 
	first_name, last_name, street_address, city, postal_code, country, phone_number, customers_dob, 
	payment_terms, ship_method, order_category
from DW_GetLenses_jbs.dbo.lenswaynl_oh_full

select top 1000 line_id, order_number, 
	inventory_item_id, item_number, category_desc, 
	base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges, 
	ordered_quantity, unit_list_price, list_price, unit_selling_price, 
	sales_inc_vat, 
	discount, 
	total_order_price, total_order_discount
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full


-----------------------------------------------------


select count(*), count(distinct email)
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full

select count(*), 
	count(distinct customer_id),
	count(distinct email)
from DW_GetLenses_jbs.dbo.lenswaynl_customer_full

select count(*), 
	count(distinct order_number),
	count(distinct customer_id), 
	min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenswaynl_oh_full

select count(*), 
	count(distinct line_id),
	count(distinct order_number), 
	count(distinct inventory_item_id)
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full

