
select top 1000 count(*) over (),
	line_id, order_number, 
	inventory_item_id, item_number, category_desc, 
	base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges, 
	ordered_quantity, unit_list_price, list_price, unit_selling_price, 
	sales_inc_vat, 
	discount, 
	total_order_price, total_order_discount
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
where inventory_item_id = 27495
	and sales_inc_vat <> '0'

select sales_inc_vat, count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
where inventory_item_id = 27495
	and sales_inc_vat <> '0'
group by sales_inc_vat

	select count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full

	-- Cardinality Orders
	select ol.line_id, ol.order_number, ol.inventory_item_id, ol.item_number, 
		oh.order_number, oh.order_date
	from 
			DW_GetLenses_jbs.dbo.lenswaynl_ol_full ol
		full join
			DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh on ol.order_number = oh.order_number
	where oh.order_number is null -- 166 OL with not OH
	order by ol.order_number, ol.line_id
	-- where ol.order_number is null -- 34k // 440 OH with not OL
	-- order by oh.order_date

		select top 10000 *
		from 
				DW_GetLenses_jbs.dbo.lenswaynl_ol_full ol
			inner join
				DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh on ol.order_number = oh.order_number
		order by order_date desc



	-- Cardinality Products
	select inventory_item_id, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	-- where category_desc like 'Freig%'
	group by inventory_item_id, category_desc
	order by count(*) desc, inventory_item_id, category_desc

	select item_number, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	group by item_number, category_desc
	order by item_number, category_desc
	

		select ol.inventory_item_id, ol.item_number, ol.category_desc, ol.num_lines, 
			p.category_code, p.category_desc, p.number_of_lenses, 
			p.item_id, p.item_number, p.item_desc,
			p.bc, p.diam, p.power, p.axis, p.cyl, p.[add], p.colour
		from
				(select inventory_item_id, item_number, category_desc, count(*) num_lines
				from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
				group by inventory_item_id, item_number, category_desc) ol
			left join
				(select 
					category_code, category_desc, number_of_lenses, 
					item_id, item_number, item_desc,
					bc, diam, power, axis, cyl, [add], colour
				from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v) p on ol.inventory_item_id = p.item_id
		where p.item_id is null
		-- order by ol.inventory_item_id, ol.item_number, ol.category_desc
		order by ol.category_desc, ol.inventory_item_id


	select s.category_code, s.category_desc, 
		p.productgroup, p.product_id, p.product_name
	from
			(select distinct p.category_code, p.category_desc
			from
					(select inventory_item_id, item_number, category_desc, count(*) num_lines
					from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
					group by inventory_item_id, item_number, category_desc) ol
				inner join
					(select 
						category_code, category_desc, number_of_lenses, 
						item_id, item_number, item_desc,
						bc, diam, power, axis, cyl, [add], colour
					from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v) p on ol.inventory_item_id = p.item_id) s
		full join
			DW_GetLenses_jbs.dbo.lenswaynl_products_category_name p on s.category_code = p.category_name 
	-- where s.category_code is not null and p.category_name is not null
	-- where s.category_code is null 
	where p.category_name is null		
	order by s.category_code, s.category_desc, p.category_name




	-- Parameters
	select cylinder, count(*)  -- base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	group by cylinder
	order by cylinder

	-- Quantity
	select ordered_quantity, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
	group by ordered_quantity
	order by ordered_quantity

