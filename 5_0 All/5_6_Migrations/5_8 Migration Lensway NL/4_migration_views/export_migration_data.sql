
select customer_id, first_name, last_name, email, 
	creation_date, update_date, 
	address1, address2, address4, address4, city, postal_code, country, telephone, 
	brand, 
	marketing_via_email, marketing_via_sms, subscriber
from DW_GetLenses_jbs.dbo.lenswaynl_customerdata_v


select order_number, website_name, customer_id, email_address, 
	order_date, shipping_date, 
	first_name, last_name, street_address, city, postal_code, country, phone_number, 
	payment_method, shipping_description, 
	base_grand_total, base_shipping_amount, base_discount_amount, order_currency
from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v
-- where order_date > '2015-10-01'
order by order_number

select ol.line_id, ol.order_number, ol.order_date, 
	ol.category_code, ol.product_id, ol.product_name, 
	ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.[add], ol.dominant, ol.color, 
	ol.quantity, ol.row_total
from
		DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v oh
	inner join
		DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v ol on oh.order_number = ol.order_number
-- where oh.order_date > '2015-10-01'
order by ol.order_number, line_id

