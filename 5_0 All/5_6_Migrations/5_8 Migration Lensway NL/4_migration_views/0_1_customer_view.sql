
use DW_GetLenses_jbs
go 

drop view dbo.lenswaynl_sku_oracle_v
go 

create view dbo.lenswaynl_sku_oracle_v as
	select 
		item_id, item_number, item_desc, 
		category_code, category_desc, 
		number_of_lenses, qty_in_ml, 
		bc, diam, power, cyl, axis, [add], colour, 
		type, material
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
	union 
	select 
		item_id, item_number, item_desc, 
		category_code, category_desc, 
		number_of_lenses, qty_in_ml, 
		bc, diam, power, cyl, axis, [add], colour, 
		type, material
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_2
go



update DW_GetLenses_jbs.dbo.lenswaynl_product_mapping
set category_code = 8252
where product_id = 3780

update DW_GetLenses_jbs.dbo.lenswaynl_product_mapping
set magento_id = 3153
where product_id = 6466

		drop view dbo.lenswaynl_productdata_v
		go

		create view dbo.lenswaynl_productdata_v as
			
			select s.item_id, s.item_number, 
				p2.category_code, p2.magento_id, 
				case when (isnumeric(s.number_of_lenses) = 1) then convert(int, s.number_of_lenses) else 1 end num_lenses, 
				s.bc base_curve, s.diam diameter, s.power power, s.cyl cylinder, s.axis axis, s.[add] [add], null dominant, s.colour color 
			from 
					(select distinct category_code, magento_id
					from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping) p2 
				inner join
					DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v s on p2.category_code = s.category_code
		go


			--select s.item_id, s.item_number, 
			--	p2.category_code, p2.magento_id, 
			--	p1.product_id, p1.productgroup, p1.product_name, p1.product_model,
			--	case when (isnumeric(s.number_of_lenses) = 1) then convert(int, s.number_of_lenses) else 1 end num_lenses, 
			--	s.bc base_curve, s.diam diameter, s.power power, s.cyl cylinder, s.axis axis, s.[add] [add], null dominant, s.colour color 
			--from 
			--		DW_GetLenses_jbs.dbo.lenswaynl_products_category_name p1
			--	inner join
			--		DW_GetLenses_jbs.dbo.lenswaynl_product_mapping p2 on p1.product_id = p2.product_id
			--	inner join
			--		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v s on p2.category_code = s.category_code

drop view dbo.lenswaynl_customerdata_v
go

create view dbo.lenswaynl_customerdata_v as
	select customer_id, 
		first_name, last_name, 
		email, creation_date, last_update_date update_date,
		address1, address2, address3, address4, city, postal_code, country, telephone, 
		org_id, brand, 
		marketing_via_email, marketing_via_sms, subscriber, 
		personal_number, party_id, party_type, 
		count(*) over (partition by customer_id) num_rep,
		count(*) over (partition by email) num_rep_email 
	from DW_GetLenses_jbs.dbo.lenswaynl_customer_full -- delta_2
	where email not in ('BOUNCED@', 'INVALID@', '', 'BESTELLEN20143@HOTMAIL.NL', 'SUPERHELD888@HOTMAIL.NL', 'BESTELLEN20142@HOTMAIL.NL')
go

delete from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
where order_number = 9657903 and order_date = '2017/09/16 04:51:49'

delete from DW_GetLenses_jbs.dbo.lenswaynl_oh_full
where order_number = 9657933 and order_date = '2017/09/16 07:51:40'



drop view dbo.lenswaynl_orderdata_v
go

create view dbo.lenswaynl_orderdata_v as
	select oh.order_number, 
		case when oh.brand = 'LW' then 'lensway.nl' else 'YourLenses' end website_name,
		oh.customer_id, email_address, order_date, shipping_date, 
		oh.first_name, oh.last_name, street_address, oh.city, oh.postal_code, oh.country, phone_number, customers_dob, 
		payment_terms payment_method, ship_method shipping_description, 
		ol.base_grand_total, isnull(ol_ship.base_shipping_amount, 0) base_shipping_amount, ol.base_discount_amount, total_order_discount,
		'EUR' order_currency,
		order_source, order_category
	from 
			-- DW_GetLenses_jbs.dbo.lenswaynl_oh_full oh 
			DW_GetLenses_jbs.dbo.lenswaynl_oh_full_delta_2 oh 
		inner join
			(select distinct customer_id 
			from DW_GetLenses_jbs.dbo.lenswaynl_customerdata_v) c on oh.customer_id = c.customer_id
		inner join
			(select order_number, convert(decimal(12, 4), total_order_price) base_grand_total, 
				sum(convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat)) base_discount_amount, 
				total_order_discount
			-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full 
			from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta_2
			group by order_number, total_order_price, total_order_discount) ol on oh.order_number = ol.order_number
		left join
			(select order_number, sum(convert(decimal(12, 4), sales_inc_vat)) base_shipping_amount
			-- from DW_GetLenses_jbs.dbo.lenswaynl_ol_full 
			from DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta_2
			where inventory_item_id = 27495
				and sales_inc_vat <> '0'
			group by order_number) ol_ship on oh.order_number = ol_ship.order_number
go



drop view dbo.lenswaynl_orderlinedata_v
go

create view dbo.lenswaynl_orderlinedata_v as
	select line_id, ol.order_number, oh.order_date,
		p.magento_id product_id, p.category_code, 
		category_desc product_name,
		null eye,  
		p.base_curve, p.diameter, p.power, p.cylinder, p.axis, p.[add], p.dominant, p.color, 
		ordered_quantity * p.num_lenses quantity, ordered_quantity,
		convert(decimal(12, 4), sales_inc_vat) row_total, 
		convert(decimal(12, 4), list_price) row_subtotal, 
		convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat) ol_discount,
		discount 
	from 
			DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v oh 
		inner join
			-- DW_GetLenses_jbs.dbo.lenswaynl_ol_full ol on oh.order_number = ol.order_number -- delta
			DW_GetLenses_jbs.dbo.lenswaynl_ol_full_delta ol on oh.order_number = ol.order_number 
		inner join
			DW_GetLenses_jbs.dbo.lenswaynl_productdata_v p on ol.inventory_item_id = p.item_id
	where ol.inventory_item_id <> 27495
go
