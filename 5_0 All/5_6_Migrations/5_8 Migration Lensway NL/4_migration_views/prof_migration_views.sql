
select top 1000 *
from DW_GetLenses_jbs.dbo.lenswaynl_products_category_name
where product_id in (5013, 3780)

select top 1000 *
from DW_GetLenses_jbs.dbo.lenswaynl_products_category_name
where product_id in (6466, 2576, 2577)

select *
from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping
where category_code in ('8253', '8257')
order by category_code, magento_id, product_id

select category_code, count(distinct magento_id)
from DW_GetLenses_jbs.dbo.lenswaynl_product_mapping
group by category_code
having count(distinct magento_id) > 1


select top 1000 count(*) over (partition by item_id) num_rep, *
from DW_GetLenses_jbs.dbo.lensonnl_productdata_v
order by num_rep desc

select count(*), count(distinct item_id)
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle_v

select count(*), count(distinct item_id), count(distinct category_code), count(distinct magento_id)
from DW_GetLenses_jbs.dbo.lensonnl_productdata_v



		select ol.inventory_item_id, ol.item_number, ol.category_desc, ol.num_lines, 
			p.category_code, p.num_lenses, 
			p.item_id, p.item_number
		from
				(select inventory_item_id, item_number, category_desc, count(*) num_lines
				from DW_GetLenses_jbs.dbo.lenswaynl_ol_full
				group by inventory_item_id, item_number, category_desc) ol
			left join
				DW_GetLenses_jbs.dbo.lensonnl_productdata_v p on ol.inventory_item_id = p.item_id
		where p.item_id is null
		-- order by ol.inventory_item_id, ol.item_number, ol.category_desc
		order by ol.category_desc, ol.inventory_item_id

-----------------------------------------------------


select top 1000 *
from DW_GetLenses_jbs.dbo.lensonnl_customerdata_v
where num_rep_email = 2
order by email

select top 1000 num_rep, count(*)
from DW_GetLenses_jbs.dbo.lensonnl_customerdata_v
group by num_rep
order by num_rep

select top 1000 num_rep_email, count(*)
from DW_GetLenses_jbs.dbo.lensonnl_customerdata_v
group by num_rep_email
order by num_rep_email


	select email, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_customerdata_v
	group by email
	having count(*) > 5
	order by count(*) desc
-----------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v

select top 1000 count(*), count(distinct order_number)
from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v

select top 1000 website_name, count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_orderdata_v
group by website_name
order by website_name

	select category_code, count(*), sum(row_total)
	from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v
	where product_id = ''
		-- and year(order_date) in (2015, 2016, 2017)
	group by category_code
	having sum(row_total) <> 0
	order by category_code

	select category_code, product_name, count(*), sum(row_total)
	from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v
	where product_id = ''
	group by category_code, product_name
	having sum(row_total) <> 0
	order by category_code, product_name
-----------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.lenswaynl_orderlinedata_v
where order_number = 50123959

select top 1000 count(*)
from DW_GetLenses_jbs.dbo.lenswaynl_ol_full

-- 2291023
-- 2290869
-- 2072452
select top 1000 count(*), count(distinct line_id)
from DW_GetLenses_jbs.dbo.lensonnl_orderlinedata_v


----------------------------------------------------------------------------------------------------------

select order_number, base_grand_total - base_shipping_amount base_grand_total
into #lenswaynl_orderdata
from DW_GetLenses_jbs.dbo.lensonnl_orderdata_v
where year(order_date) in (2015, 2016, 2017)

select top 1000 oh.order_number, 
	oh.base_grand_total, ol.oh_grand_total
from
		#lenswaynl_orderdata oh
	full join
		(select order_number, sum(row_total) oh_grand_total
		from DW_GetLenses_jbs.dbo.lensonnl_orderlinedata_v
		group by order_number) ol on oh.order_number = ol.order_number
where oh.base_grand_total <> ol.oh_grand_total



