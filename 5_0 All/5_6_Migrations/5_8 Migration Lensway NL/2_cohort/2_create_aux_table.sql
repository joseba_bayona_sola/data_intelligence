
create table DW_GetLenses_jbs.dbo.lenswaynl_cohort(
	order_id				bigint, 
	customer_id				bigint, 
	order_date				date, 
	item_product_family		varchar(50),
	base_grand_total		decimal(12, 4)); 

	insert into DW_GetLenses_jbs.dbo.lenswaynl_cohort(order_id, customer_id, order_date, item_product_family, base_grand_total)

		select distinct convert(bigint, order_number) order_id,  convert(bigint, customer) customer_id,
			convert(date, convert(datetime, ordered_date)) order_date, 
			null item_product_family,
			total_order_price base_grand_total
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
		where ISNUMERIC(total_order_price) = 1
