
use DW_GetLenses
go

create procedure Finance.spCustomerRetention_LenswayNL(
	@from_date as date, 
	@to_date as date
	) 
as

begin

	select period, cust_seq_ord_num customer_order_seq_no, month_diff, 
		count(distinct customer_id) customer_count,
		sum(base_grand_total) revenue_in_VAT, 
		sum(num_cust) next_order_customer_count, 
		sum(next_base_grand_total) next_revenue_in_VAT 
	from 
		(select customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, 
			case when (period_next > @to_date) then NULL else next_base_grand_total end next_base_grand_total, 
			case when (period_next > @to_date) then NULL else 1 end num_cust
		from
			(select customer_id, 
				order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
				cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
						rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lenswaynl_cohort
					where order_date is not null) t) t
				where order_date between @from_date and @to_date 	
				) t) t
	group by period, cust_seq_ord_num, month_diff
end