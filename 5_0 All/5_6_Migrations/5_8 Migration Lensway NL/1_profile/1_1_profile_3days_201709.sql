
----- OH

select brand, order_number, 
	order_date, shipping_date, 
	order_source, 
	customer_id, email_address, 
	first_name, last_name, street_address, city, postal_code, country, phone_number, customers_dob, 
	payment_terms, ship_method, order_category
from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
-- where order_source = 'AnonymousUser '
-- where order_source = ''
-- where order_source in ('finwar', 'marplu')
where customer_id in (4531707, 792034, 892103, 1646984, 4307557)
-- where payment_terms = 'Klarna'
order by order_number

	select count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days

	select brand, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by brand
	order by brand

	select customer_id, email_address, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by customer_id, email_address
	order by count(*) desc, customer_id, email_address

	select order_source, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by order_source
	order by order_source

	select payment_terms, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by payment_terms
	order by payment_terms

	select ship_method, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by ship_method
	order by ship_method

	select order_category, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
	group by order_category
	order by order_category

------------------------------------------------------------------------------------

----- OL

select order_number, 
	inventory_item_id, item_number, category_desc, 
	base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges, 
	ordered_quantity, unit_list_price, list_price, unit_selling_price, 
	sales_inc_vat, 
	discount, 
	total_order_price, total_order_discount
from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
-- where inventory_item_id = 102732
-- where power = 'Met'
where cylinder = 'Brown'
order by order_number, item_number

	select count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days

	select inventory_item_id, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
	group by inventory_item_id, category_desc
	order by count(*) desc, inventory_item_id, category_desc

	select item_number, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
	group by item_number, category_desc
	order by item_number, category_desc

	select cylinder, count(*)  -- base_curve, diameter, power, cylinder, axis, addition, lenses_fixhinges
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
	group by cylinder
	order by cylinder

	select order_number, count(distinct total_order_price)
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
	group by order_number
	order by count(distinct total_order_price) desc, order_number

------------------------------------------------------------------------------------

---- OH / OL Cardinality

select oh.brand, oh.order_number, 
	oh.order_date, oh.shipping_date, 
	oh.customer_id, oh.email_address, 
	oh.payment_terms, oh.ship_method, 
	ol.inventory_item_id, ol.item_number, ol.category_desc,
	ol.ordered_quantity, ol.unit_list_price, ol.list_price, ol.unit_selling_price, 
	ol.sales_inc_vat, 
	ol.discount, 
	ol.total_order_price, ol.total_order_discount
from 
		DW_GetLenses_jbs.dbo.lenswaynl_oh_3days oh
	full join
		DW_GetLenses_jbs.dbo.lenswaynl_ol_3days ol on oh.order_number = ol.order_number
-- where oh.order_number is null or ol.order_number is null
where customer_id in (4531707, 792034, 892103, 1646984, 4307557)
order by oh.order_number

------------------------------------------------------------------------------------

-- OL Numbers: ordered_quantity * unit_selling_price = sales_inc_vat
select *, abs(sales_inc_vat - sales_inc_vat_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		ordered_quantity, unit_selling_price, 
		convert(decimal(12, 4), sales_inc_vat) sales_inc_vat, 
		convert(int, ordered_quantity) * convert(decimal(12, 4), unit_selling_price) sales_inc_vat_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days) t
where abs(sales_inc_vat - sales_inc_vat_calc) <> 0

-- OL Numbers: sum(sales_inc_vat) = total_order_price
select *, abs(total_order_price - total_order_price_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		convert(decimal(12, 4), sales_inc_vat) sales_inc_vat,  
		convert(decimal(12, 4), total_order_price) total_order_price, 
		sum(convert(decimal(12, 4), sales_inc_vat)) over (partition by order_number) total_order_price_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days) t
where abs(total_order_price - total_order_price_calc) <> 0

-- OL Numbers: ordered_quantity * unit_list_price = list_price
select *, abs(list_price - list_price_calc)
from
	(select order_number, 
		inventory_item_id, item_number, category_desc, 
		ordered_quantity, unit_list_price, 
		convert(decimal(12, 4), list_price) list_price, 
		convert(int, ordered_quantity) * convert(decimal(12, 4), unit_list_price) list_price_calc
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days) t
where abs(list_price - list_price_calc) <> 0

-- OL Numbers - Discounts OL Level: list_price, sales_inc_vat, discount
select order_number, 
	inventory_item_id, item_number, category_desc, 
	list_price, sales_inc_vat, discount
	, (convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat)) * 100 / convert(decimal(12, 4), list_price) discount_calc, 
	convert(decimal(12, 4), list_price) - convert(decimal(12, 4), sales_inc_vat) ol_discount
from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
where convert(decimal(12, 4), list_price) <> 0

-- OL Numbers - Discounts OH Level: SUM(list_price), total_order_price, total_order_discount
select order_number, 
	sum_list_price, total_order_price, total_order_discount, 
	(sum_list_price - total_order_price) * 100 / sum_list_price total_order_discount_calc,
	sum_list_price - total_order_price oh_discount
from
	(select order_number, 
		sum(convert(decimal(12, 4), list_price)) sum_list_price, 
		convert(decimal(12, 4), total_order_price) total_order_price, total_order_discount
	from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
	group by order_number, total_order_price, total_order_discount) t
order by order_number


