
select top 1000 order_number, ordered_date, customer,
	ol.item_number, description, 
	s.category_code, s.category_desc
from 
		DW_GetLenses_jbs.dbo.lenswaynl_order_line ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on ol.item_number = s.item_number
where order_number in (51273551, 51273333)
order by order_number, ol.item_number

select top 1000 order_number, ordered_date, customer,
	bar_code, bar_code_type, description, s.category_code, s.category_desc,
	ol.item_number, sales_inc_vat, 
	total_order_price
from 
		DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on ol.bar_code = s.item_number
where order_number in (51273551, 51273333)
order by order_number, bar_code

select top 1000 order_number, ordered_date, customer, 
	inventory_item_id, description, s.category_code, s.category_desc,
	ordered_quantity, unit_selling_price, sales_inc_vat,
	total_order_price
from 
		DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017 ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on ol.inventory_item_id = s.item_id
where order_number in (51273551, 51273333)
order by order_number, inventory_item_id

-------------------------

select *
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
where oracle_name like 'Acuvue Oasys (6)' or oracle_name like 'Soflens Toric (6)'

select *
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
where category_desc like 'Acuvue Oasys (6)' or category_desc like 'Soflens Toric (6)'
order by category_code, item_number
