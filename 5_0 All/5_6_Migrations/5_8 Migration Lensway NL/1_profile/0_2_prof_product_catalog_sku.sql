
select count(*) -- 1000 
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog

select count(*) -- 180431
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle

select id, prd_id, category_name, 
	type, product_name, oracle_name, size
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
where oracle_name like 'Acuvue Oasys (6)' or oracle_name like 'Soflens Toric (6)'

select item_id, item_number, 
	category_code, category_desc, number_of_lenses, item_desc, 
	bc, diam, power, axis, cyl, [add], colour  
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
where category_desc like 'Acuvue Oasys (6)' or category_desc like 'Soflens Toric (6)'
order by category_code, item_number


-----------------------------------------------------------------

select p.id, prd_id, category_name, 
	p.type, product_name, oracle_name, size, 
	item_id, item_number, 
	category_code, category_desc, number_of_lenses, item_desc
from 
		DW_GetLenses_jbs.dbo.lenswaynl_product_catalog p
	full join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on p.category_name = s.category_code
-- where p.category_name is not null and s.category_code is not null -- 73218
-- where s.category_code is null -- 865
where p.category_name is null -- 107213
order by p.type, category_name, product_name

