
select ol.item_number, 
	s.category_code, s.category_desc, s.item_desc,
	ol.num_rows 
from
		(select item_number, count(*) num_rows
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line 
		group by item_number) ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on ol.item_number = s.item_number
-- where s.item_number is not null -- 37270
where s.item_number is null -- 15617
order by item_number

select ol.inventory_item_id,
	s.category_code, s.category_desc, s.item_desc,
	ol.num_rows
from
		(select inventory_item_id, count(*) num_rows
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017 
		group by inventory_item_id) ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle s on ol.inventory_item_id = s.item_id
-- where s.item_number is not null -- 10285
where s.item_number is null -- 544
order by ol.inventory_item_id
