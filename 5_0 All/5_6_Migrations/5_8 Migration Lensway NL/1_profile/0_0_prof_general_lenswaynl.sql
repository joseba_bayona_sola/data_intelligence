
select *
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
--where product_name like 'Soflens Toric%'
order by id, type, product_name

select id, prd_id, 
	category_name, type, 
	product_name, oracle_name, size, number_of_boxes_to_display, 
	country_iso, organisation_id, 
	article_description, show_on_thanks, article_id
from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
-- where id <> prd_id
--where category_name = '0' or category_name = 'NULL'
where category_name in ('1017', '1104')
order by type, product_name


	select id, count(*) num
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by id
	order by num desc

	select category_name, count(*) num
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by category_name
	order by num desc

	select type, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_product_catalog
	group by type
	order by type

--------------- 

select top 1000 
	id, item_id, item_number, item_desc, 
	category_code, category_desc, 
	number_of_lenses, qty_in_ml, 
	bc, diam, power, cyl, axis, [add], colour, 
	type, material
from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
-- where item_desc like 'Soflens Toric (6)/-2.25/8.5/14.5//-1.25/10/' -- Soflens Toric (6)/-2.25/8.5/14.5//-1.25/10/  -   Acuvue Oasys (6)/-2.50/8.4/14.0////
where category_code in ('1017', '1104')
	and item_id = 5152
order by item_desc, category_code

	select category_code, category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
	group by category_code, category_desc
	order by category_code, category_desc

	select category_desc, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
	group by category_desc
	order by category_desc

	select power, count(*) -- bc, diam, power, cyl, axis, [add]
	from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle
	group by power
	order by power





--------------- 

select top 1000 num_row, uniq_id, 
	person_first_name, person_last_name, 
	address1, address2, address3, address4, 
	city, postal_code, country, telephone, email, 
	brand, org_id, sold_to_org_id, 
	party_id, party_type, 
	account_number, 
	marketing_via_email, marketing_via_mail, marketing_via_3part, marketing_via_sms, subscriber, 
	creation_date
from DW_GetLenses_jbs.dbo.lenswaynl_customer
where person_first_name = 'Berend' and person_last_name = 'Scholten'
order by uniq_id

	select count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer

	select org_id, brand, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer
	group by org_id, brand
	order by org_id, brand

	select country, count(*)
	from DW_GetLenses_jbs.dbo.lenswaynl_customer
	group by country
	order by country

--------------- 


select top 1000 order_number, ordered_date, 
	convert(datetime, ordered_date), year(convert(datetime, ordered_date)) yyyy_order,
	ship_method_meaning, 
	customer, person_first_name, person_last_name, 
	item_number, description
from DW_GetLenses_jbs.dbo.lenswaynl_order_line
--where item_number = '846566555277'
where customer = '1045080'
order by convert(datetime, ordered_date) desc

	select year(convert(datetime, ordered_date)) yyyy_order, count(distinct order_number) num_orders, count(*) num_lines
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line
	group by year(convert(datetime, ordered_date)) 
	order by year(convert(datetime, ordered_date)) 

	select ship_method_meaning, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line
	group by ship_method_meaning
	order by ship_method_meaning
	
	select item_number, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line
	group by item_number
	order by count(*) desc

	select top 1000 customer, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line
	group by customer
	order by count(distinct order_number) desc




--------------- 


select top 1000 order_number, ordered_date, 
	convert(datetime, ordered_date), year(convert(datetime, ordered_date)) yyyy_order,
	ship_method_meaning, 
	customer, person_first_name, person_last_name, 
	item_number, description, bar_code, bar_code_type, 
	sales_inc_vat, list_price, discount, total_order_price
from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
-- where bar_code = '846566555277'
--where bar_code_type = ''
where customer = '1045080'
order by convert(datetime, ordered_date) desc



	select year(convert(datetime, ordered_date)) yyyy_order, count(distinct order_number) num_orders, count(*) num_lines
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	group by year(convert(datetime, ordered_date)) 
	order by year(convert(datetime, ordered_date)) 

	select ship_method_meaning, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	group by ship_method_meaning
	order by ship_method_meaning
	
	select bar_code, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	group by bar_code
	order by count(*) desc

	select bar_code_type, count(*), count(distinct order_number)
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	where ISNUMERIC(total_order_price) = 1
	group by bar_code_type
	order by count(*) desc

---------------------------------------


	------------------ REALLY DISTINCT INFORMATION FOR ORDER -----------------------------

	select distinct order_number,  
		convert(datetime, ordered_date) ordered_date, year(convert(datetime, ordered_date)) yyyy_order,
		ship_method_meaning, 
		customer, person_first_name, person_last_name, 
		total_order_price
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	where ISNUMERIC(total_order_price) = 1

	select count(*) over (partition by order_number) num_rep, 
		order_number,  
		ordered_date, yyyy_order,
		ship_method_meaning, 
		customer, person_first_name, person_last_name, 
		total_order_price
	from 
		(select distinct order_number,  
			convert(datetime, ordered_date) ordered_date, year(convert(datetime, ordered_date)) yyyy_order,
			ship_method_meaning, 
			customer, person_first_name, person_last_name, 
			total_order_price
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
		where ISNUMERIC(total_order_price) = 1) t
	order by num_rep desc

	----------------------------------------------------


select order_number, ordered_date, 
	convert(datetime, ordered_date), year(convert(datetime, ordered_date)) yyyy_order,
	ship_method_meaning, 
	customer, person_first_name, person_last_name, 
	item_number, description
from DW_GetLenses_jbs.dbo.lenswaynl_order_line
where convert(date, convert(datetime, ordered_date)) between '2017-05-01' and '2017-05-31'
	and order_number = 51273333
order by convert(datetime, ordered_date)


select order_number, ordered_date, 
	convert(datetime, ordered_date), year(convert(datetime, ordered_date)) yyyy_order,
	ship_method_meaning, 
	customer, person_first_name, person_last_name, 
	item_number, description, bar_code, bar_code_type, 
	sales_inc_vat, list_price, discount, total_order_price, 
	sum(convert(decimal, sales_inc_vat)) over (partition by order_number) tot_sum_order
from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
where convert(date, convert(datetime, ordered_date)) between '2017-05-01' and '2017-05-31'
	and order_number = 51273551
order by convert(datetime, ordered_date)

select
	case when t1.order_number is null then t2.order_number else t1.order_number end order_number, 
	t1.num_lines_dump1, t2.num_lines_dump2, 
	abs(t1.num_lines_dump1 - t2.num_lines_dump2) diff_lines
from
		(select order_number, count(*) num_lines_dump1
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line
		where convert(date, convert(datetime, ordered_date)) between '2017-05-01' and '2017-05-31'
		group by order_number) t1
	full join
		(select order_number, count(*) num_lines_dump2
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
		where convert(date, convert(datetime, ordered_date)) between '2017-05-01' and '2017-05-31'
		group by order_number) t2 on t1.order_number = t2.order_number
order by order_number



select order_number, order_date, total_order_price, tot_sum_order, 
	abs(total_order_price - tot_sum_order) diff_order_price,
	count(*) num_lines
from
	(select order_number, ordered_date, 
		convert(datetime, ordered_date) order_date, year(convert(datetime, ordered_date)) yyyy_order,
		ship_method_meaning, 
		customer, person_first_name, person_last_name, 
		item_number, description, bar_code, bar_code_type, 
		sales_inc_vat, list_price, discount, convert(decimal, total_order_price) total_order_price, 
		sum(convert(decimal, sales_inc_vat)) over (partition by order_number) tot_sum_order
	from DW_GetLenses_jbs.dbo.lenswaynl_order_line_ok
	where convert(date, convert(datetime, ordered_date)) between '2017-05-01' and '2017-05-31'
		and ISNUMERIC(total_order_price) = 1) t
group by order_number, order_date, total_order_price, tot_sum_order
order by diff_order_price, order_number, order_date


----------------------------------------------------------------------



select top 1000 order_number, ordered_date, convert(date, ordered_date), 
	ship_method_meaning, 
	person_first_name, person_last_name, customer, 
	inventory_item_id, description, ordered_quantity, unit_list_price, unit_selling_price, 
	list_price, sales_inc_vat, discount, 
	total_order_price, 
	sum(convert(decimal(12, 2), sales_inc_vat)) over (partition by order_number) sum_order_price
from DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017
-- where order_number = 9224960
where customer = '1045080'
--where discount in ('20%''') -- 100 20

select ol.inventory_item_id, ol.description, pr.category_code, pr.category_desc, ol.num
from
		(select inventory_item_id, description, count(*) num
		from DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017
		group by inventory_item_id, description) ol
	left join
		DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle pr on ol.inventory_item_id = pr.item_id
where pr.item_id is null
order by ol.inventory_item_id

select year(convert(date, ordered_date)) yyyy, month(convert(date, ordered_date)) mm, count(*), count(distinct order_number)
from DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017
where discount <> '100%'''
group by year(convert(date, ordered_date)), month(convert(date, ordered_date)) 
order by year(convert(date, ordered_date)), month(convert(date, ordered_date)) 

select year(convert(date, ordered_date)) yyyy, month(convert(date, ordered_date)) mm, discount, count(*), count(distinct order_number)
from DW_GetLenses_jbs.dbo.lenswaynl_order_line_2017
group by year(convert(date, ordered_date)), month(convert(date, ordered_date)), discount 
having count(*) > 100
order by year(convert(date, ordered_date)), month(convert(date, ordered_date)), count(*) desc, discount
