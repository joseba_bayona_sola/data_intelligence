
-- Customers
select oh.customer_id, oh.email_address, oh.num_orders, 
	c.email, 
	c.brand, c.creation_date,
	c.person_first_name, c.person_last_name
from
		(select customer_id, email_address, count(*) num_orders
		from DW_GetLenses_jbs.dbo.lenswaynl_oh_3days
		group by customer_id, email_address) oh 
	left join	
		(select num_row, convert(int, account_number) account_number, email, 
			brand, creation_date,
			person_first_name, person_last_name
		from DW_GetLenses_jbs.dbo.lenswaynl_customer) c on oh.customer_id = c.account_number
order by num_orders desc, oh.customer_id, oh.email_address

----------------------------------------------

-- Products
select ol.inventory_item_id, ol.item_number, ol.category_desc, ol.num_lines, 
	p.category_code, p.category_desc, p.number_of_lenses, 
	p.item_id, p.item_number, p.item_desc,
	p.bc, p.diam, p.power, p.axis, p.cyl, p.[add], p.colour
from
		(select inventory_item_id, item_number, category_desc, count(*) num_lines
		from DW_GetLenses_jbs.dbo.lenswaynl_ol_3days
		group by inventory_item_id, item_number, category_desc) ol
	left join
		(select id, 
			category_code, category_desc, number_of_lenses, 
			item_id, item_number, item_desc,
			bc, diam, power, axis, cyl, [add], colour
		from DW_GetLenses_jbs.dbo.lenswaynl_sku_oracle)	p on ol.inventory_item_id = p.item_id
-- where p.item_id is null
order by ol.inventory_item_id, ol.item_number, ol.category_desc
