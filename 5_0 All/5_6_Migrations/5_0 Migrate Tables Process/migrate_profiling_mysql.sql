
select 
  old_customer_id, new_magento_id, 
  email, password, created_at,
  -- store_id, domainID, domainName, website_group_id
  -- firstname, lastname
  -- billing_company, billing_prefix, billing_firstname, billing_lastname, 
  -- billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone
  -- shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  -- shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone
  -- cus_phone, default_billing, default_shipping
  -- store_credit_amount, store_credit_comment, target_store_credit_amount
  -- unsubscribe_newsletter, disable_saved_card, RAF_code
  import_last_order_date, mage_last_order_date 
  -- new_magento_id_bck, migrated, priority-- , is_registered
  -- old_last_order_date, old_customer_lifecycle, hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle
from lenson.migrate_customerdata
limit 1000 


select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  -- store_id, website_name, DomainID, domainName
  -- email, old_customer_id, magento_customer_id
  migrated
  -- billing_company, billing_prefix, billing_firstname, billing_lastname, 
  -- billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone  
  -- shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  -- shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone  
  -- shipping_description
  -- base_grand_total, base_shipping_amount, base_discount_amount
  -- order_currency
  -- comment, priority
  -- reminder_mobile, reminder_date, reminder_type
  -- increment_id
from lensway.migrate_orderdata
limit 1000 

 
select old_item_id, 
  old_order_id, magento_order_id, 
  -- store_id
  migrated, priority, debug
  -- old_product_id, product_id, productCode, productName, productCodeFull, packsize
  -- eye, power, base_curve, diameter, axis, cylinder, dominant, color, params -- add
  -- quote_item_id, quote_sku, quote_is_lens, quote_data_built -- quote_opt,
  -- params_hash
  -- quantity, row_total
from lensway.migrate_orderlinedata
limit 1000 

 