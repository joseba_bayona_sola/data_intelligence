CREATE TABLE migrate_customerdata (
	old_customer_id			bigint(20) NOT NULL DEFAULT '0',
	new_magento_id			int(11) NOT NULL DEFAULT '0',

	email					varchar(50) DEFAULT NULL,
	password				varchar(255) DEFAULT NULL,
	created_at				datetime DEFAULT NULL,

	store_id				int(11) DEFAULT NULL,
	DomainID				varchar(3) DEFAULT NULL,
	DomainName				varchar(39) DEFAULT NULL,
	website_group_id		int(11) DEFAULT NULL,

	firstname				varchar(255) DEFAULT NULL,
	lastname				varchar(255) DEFAULT NULL,

	billing_company			varchar(500) DEFAULT NULL,
	billing_prefix			varchar(255) DEFAULT NULL,
	billing_firstname		varchar(500) DEFAULT NULL,
	billing_lastname		varchar(500) DEFAULT NULL,
	billing_street1			varchar(500) DEFAULT NULL,
	billing_street2			varchar(500) DEFAULT NULL,
	billing_city			varchar(500) DEFAULT NULL,
	billing_region			varchar(500) DEFAULT NULL,
	billing_region_id		binary(0) DEFAULT NULL,
	billing_postcode		varchar(500) DEFAULT NULL,
	billing_country_id		varchar(500) DEFAULT NULL,
	billing_telephone		varchar(500) DEFAULT NULL,

	shipping_company		varchar(500) DEFAULT NULL,
	shipping_prefix			varbinary(255) DEFAULT NULL,
	shipping_firstname		varchar(500) DEFAULT NULL,
	shipping_lastname		varchar(500) DEFAULT NULL,
	shipping_street1		varchar(500) DEFAULT NULL,
	shipping_street2		varchar(500) DEFAULT NULL,
	shipping_city			varchar(500) DEFAULT NULL,
	shipping_region			varchar(500) DEFAULT NULL,
	shipping_region_id		binary(0) DEFAULT NULL,
	shipping_postcode		varchar(500) DEFAULT NULL,
	shipping_country_id		varchar(500) DEFAULT NULL,
	shipping_telephone		varchar(500) DEFAULT NULL,

	cus_phone				varchar(40) DEFAULT NULL,

	default_billing			varchar(255) DEFAULT NULL,
	default_shipping		varchar(255) DEFAULT NULL,

	store_credit_amount		decimal(12,2) DEFAULT NULL,
	store_credit_comment	varchar(255) DEFAULT NULL,
	target_store_credit_amount decimal(12,2) NOT NULL DEFAULT '0.00',

	unsubscribe_newsletter	int(11) DEFAULT NULL,
	disable_saved_card		int(11) DEFAULT NULL,
	RAF_code				varchar(255) NOT NULL DEFAULT '',


	import_last_order_date	datetime DEFAULT NULL,
	mage_last_order_date	datetime DEFAULT NULL,

	new_magento_id_bck		int(11) NOT NULL DEFAULT '0',
	migrated				int(11) NOT NULL DEFAULT '0',
	priority				int(11) NOT NULL DEFAULT '0',
	is_registered			tinyint(4) DEFAULT NULL,

	old_last_order_date		datetime DEFAULT NULL,
	old_customer_lifecycle	varchar(255) DEFAULT NULL,
	hist_last_order_date	datetime DEFAULT NULL,
	hist_customer_lifecycle varchar(255) DEFAULT NULL,
	magento_last_order_date datetime DEFAULT NULL,
	magento_customer_lifecycle varchar(255) DEFAULT NULL,
	vd_last_order_date		datetime DEFAULT NULL,
	vd_customer_lifecycle	varchar(255) DEFAULT NULL)


CREATE TABLE migrate_orderdata (
	old_order_id		varchar(255) NOT NULL DEFAULT '',

	magento_order_id	int(11) NOT NULL DEFAULT '0',
	new_increment_id	varchar(255) NOT NULL DEFAULT '0',

	store_id			int(11) NOT NULL DEFAULT '0',
	website_name		varbinary(20) DEFAULT NULL,
	DomainID			varchar(10) DEFAULT NULL,
	DomainName			varchar(30) DEFAULT NULL,

	email				varchar(255) DEFAULT NULL,
	old_customer_id		varchar(255) DEFAULT NULL,
	magento_customer_id int(11) NOT NULL DEFAULT '0',

	created_at			datetime DEFAULT NULL,

	migrated			int(11) NOT NULL DEFAULT '0',

	billing_company		varchar(255) DEFAULT NULL,
	billing_prefix		varchar(255) DEFAULT NULL,
	billing_firstname	varchar(255) DEFAULT NULL,
	billing_lastname	varchar(255) DEFAULT NULL,
	billing_street1		varchar(255) DEFAULT NULL,
	billing_street2		varchar(255) DEFAULT NULL,
	billing_city		varchar(255) DEFAULT NULL,
	billing_region		varchar(255) DEFAULT NULL,
	billing_region_id	varchar(255) DEFAULT NULL,
	billing_postcode	varchar(255) DEFAULT NULL,
	billing_country		varchar(255) DEFAULT NULL,
	billing_telephone	varchar(255) DEFAULT NULL,
  
	shipping_company	varchar(255) DEFAULT NULL,
	shipping_prefix		varchar(25) DEFAULT NULL,
	shipping_firstname	varchar(255) DEFAULT NULL,
	shipping_lastname	varchar(255) DEFAULT NULL,
	shipping_street1	varchar(255) DEFAULT NULL,
	shipping_street2	varchar(255) DEFAULT NULL,
	shipping_city		varchar(255) DEFAULT NULL,
	shipping_region		varchar(255) DEFAULT NULL,
	shipping_region_id	varchar(255) DEFAULT NULL,
	shipping_postcode	varchar(255) DEFAULT NULL,
	shipping_country	varchar(255) DEFAULT NULL,
	shipping_telephone	varchar(255) DEFAULT NULL,
	shipping_description varchar(255) DEFAULT NULL,

	base_grand_total	decimal(8,2) DEFAULT NULL,  
	base_shipping_amount decimal(8,2) DEFAULT NULL,
	base_discount_amount decimal(6,2) DEFAULT NULL,

	order_currency		varchar(3) NOT NULL DEFAULT '0',

	Comment				varchar(500) DEFAULT NULL,
	priority			int(11) NOT NULL DEFAULT '0',

	reminder_mobile		varchar(50) DEFAULT NULL,
	reminder_date		varchar(255) DEFAULT NULL,
	reminder_type		varchar(255) DEFAULT 'none',

	increment_id		varchar(255) NOT NULL)


CREATE TABLE migrate_orderlinedata (
	old_item_id varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',

	old_order_id varchar(255) CHARACTER SET latin1 NOT NULL,
	magento_order_id int(11) DEFAULT NULL,

	store_id int(11) DEFAULT NULL,

	migrated int(11) NOT NULL DEFAULT '0',
	priority bigint(20) NOT NULL,
	debug text CHARACTER SET latin1,

	old_product_id varchar(255) CHARACTER SET latin1 DEFAULT NULL,
	product_id int(4) DEFAULT '0',
	ProductCode varchar(100) CHARACTER SET utf8 DEFAULT NULL,
	ProductName varchar(255) CHARACTER SET utf8 DEFAULT NULL,
	ProductCodeFull varchar(25) CHARACTER SET utf8 DEFAULT '0',
	packsize int(11) DEFAULT NULL,

	eye varchar(10) CHARACTER SET utf8 DEFAULT NULL,
	power varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	base_curve varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	diameter varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	add varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	axis varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	cylinder varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	dominant varchar(20) CHARACTER SET latin1 DEFAULT NULL,
	color varchar(20) CHARACTER SET utf8 DEFAULT NULL,
	params varchar(255) CHARACTER SET latin1 DEFAULT NULL,

	quote_item_id int(11) DEFAULT NULL,
	quote_sku varchar(255) CHARACTER SET utf8 DEFAULT NULL,
	quote_is_lens int(11) DEFAULT NULL,
	quote_opt text CHARACTER SET utf8,	
	quote_data_built int(11) NOT NULL DEFAULT '0',

	params_hash varchar(255) CHARACTER SET latin1 DEFAULT NULL,

	Quantity int(11) DEFAULT NULL,
	row_total decimal(12,4) DEFAULT NULL)
