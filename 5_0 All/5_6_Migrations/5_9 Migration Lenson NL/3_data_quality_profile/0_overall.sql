
select customers_id, 
	customers_firstname, customers_lastname, 
	date_account_created, customers_email_address, 
	customers_dob, 
	entry_street_address, entry_postcode, entry_city, entry_country, customers_telephone, 
	customers_newsletter, customers_reminder, 
	customers_discount, customers_password
from DW_GetLenses_jbs.dbo.lensonnl_customers

select products_id, 
	ProductGroup, categories_name, products_name, products_model, 
	manufacturers_name, 
	available_for_purchase, to_be_disc_or_discontinued, 
	price_eur, 
	url, product_image, products_description, products_saletext, products_info, products_userinfo, products_expired_text
from DW_GetLenses_jbs.dbo.lensonnl_products
order by ProductGroup, categories_name, products_name, products_model

select top 1000 orders_id, 
	date_purchased, shipped, 
	customers_id, customers_name, customers_email_address, customers_dob,
	customers_street_address, customers_city, customers_postcode, customers_country, customers_telephone, 
	payment_method, delivery_method, 
	referers_name, referers_categories_name
from DW_GetLenses_jbs.dbo.lensonnl_orders


select top 1000 orders_total_id, 
	orders_id, class, title, value
from DW_GetLenses_jbs.dbo.lensonnl_orders_total
order by orders_id


select top 1000 orders_products_id, 
	orders_id, 
	products_id, visma_id, products_name, products_model, info, ProductGroup,
	products_price, products_quantity, 
	total_orders_products_discount, 
	campaignName, campaignDesc, campaignType, campaignGivesFreeShippping
from DW_GetLenses_jbs.dbo.lensonnl_orders_products

select reminders_id, orders_id, customers_id, email_address, 
	date_added, desired_delivery, delivered, 
	is_manual_desired_delivery, is_delivered, times_failed, 
	updated_at
from DW_GetLenses_jbs.dbo.lensonnl_reminders

select product_id, magento_id, multiplier, lenson_packsize, magento_packsize
from DW_GetLenses_jbs.dbo.lensonnl_product_mapping

-----------------------------------------------------

select count(*), count(distinct customers_email_address)
from DW_GetLenses_jbs.dbo.lensonnl_customers

select count(*), 
	count(distinct customers_id),
	count(distinct customers_email_address)
from DW_GetLenses_jbs.dbo.lensonnl_customers

select count(*), 
	count(distinct orders_id),
	count(distinct customers_id), 
	min(date_purchased), max(date_purchased)
from DW_GetLenses_jbs.dbo.lensonnl_orders
where date_purchased not in ('Ideal', 'Card')

select count(*), 
	count(distinct orders_products_id),
	count(distinct orders_id), 
	count(distinct products_id)
from DW_GetLenses_jbs.dbo.lensonnl_orders_products



select count(*) 
from DW_GetLenses_jbs.dbo.lensonnl_product_mapping
where magento_id = ''


-----------------------------------------------------

select top 1000 o1.orders_id, 
	o1.date_purchased, o1.shipped, 
	o1.customers_id, o1.customers_email_address, 
	o1.payment_method, o1.delivery_method
from 
		DW_GetLenses_jbs.dbo.lensonnl_orders_o o1
	left join
		DW_GetLenses_jbs.dbo.lensonnl_orders o2 on o1.orders_id = o2.orders_id
where o2.orders_id is null
order by o1.customers_id, o1.orders_id
