
select old_product_id, product_id, productCode, productName, productCodeFull, packsize
from migrate_orderlinedata

select product_id, productName, packsize, old_product_id, count(*)
from migrate_orderlinedata
group by product_id, productName, packsize, old_product_id
order by product_id, productName, packsize, old_product_id

select product_id, count(*)
from migrate_orderlinedata
group by product_id
order by product_id

select product_id, productName, count(*)
from migrate_orderlinedata
group by product_id, productName
order by product_id, productName


------------------------------------------------------


select old_item_id, 
  old_product_id, product_id, productCode, productName, packsize,
  eye, power, base_curve, diameter, axis, cylinder, dominant, color, params -- add
  
from migrate_orderlinedata

  select cylinder, count(*)  -- base_curve, diameter, power, axis, cylinder, dominant, 
  from migrate_orderlinedata
  group by cylinder
  order by cylinder