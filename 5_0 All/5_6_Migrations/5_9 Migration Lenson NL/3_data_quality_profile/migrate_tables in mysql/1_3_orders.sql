select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  -- store_id, website_name, DomainID, domainName
  -- email, old_customer_id, magento_customer_id
  migrated
  -- billing_company, billing_prefix, billing_firstname, billing_lastname, 
  -- billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone  
  -- shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  -- shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone  
  -- shipping_description
  -- base_grand_total, base_shipping_amount, base_discount_amount
  -- order_currency
  -- comment, priority
  -- reminder_mobile, reminder_date, reminder_type
  -- increment_id
from migrate_orderdata
order by created_at


select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  store_id, website_name, DomainID, domainName,
  email, old_customer_id, magento_customer_id,
  migrated, 
  increment_id
from migrate_orderdata
-- where magento_order_id is null or magento_order_id = 0
where not(magento_order_id is null or magento_order_id = 0)
order by created_at


select store_id, count(*)
from migrate_orderdata
group by store_id
order by store_id

select DomainID, domainName, count(*)
from migrate_orderdata
group by DomainID, domainName
order by DomainID, domainName

select website_name, count(*)
from migrate_orderdata
group by website_name
order by website_name

select billing_country, count(*)
from migrate_orderdata
group by billing_country
order by billing_country

select shipping_country, count(*)
from migrate_orderdata
group by shipping_country
order by shipping_country

  select *
  from sales_flat_order
  where entity_id = 15281
  
  select *
  from sales_flat_order_address
  where entity_id = 26183

select shipping_description, count(*)
from migrate_orderdata
group by shipping_description
order by shipping_description


select order_currency, count(*)
from migrate_orderdata
group by order_currency
order by order_currency

---------------------------------------------------------

select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  store_id, 
  base_grand_total, base_shipping_amount, base_discount_amount
from migrate_orderdata
order by created_at