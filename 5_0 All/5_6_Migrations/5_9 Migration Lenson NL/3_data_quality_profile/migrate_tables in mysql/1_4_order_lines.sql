
select old_item_id, 
  old_order_id, magento_order_id, 
  store_id,
  migrated, priority, debug
  -- old_product_id, product_id, productCode, productName, productCodeFull, packsize
  -- eye, power, base_curve, diameter, axis, cylinder, dominant, color, params -- add
  -- quote_item_id, quote_sku, quote_is_lens, quote_data_built -- quote_opt,
  -- params_hash
  -- quantity, row_total
from migrate_orderlinedata
order by magento_order_id

select old_item_id, 
  old_order_id, magento_order_id, 
  store_id,
  migrated
from migrate_orderlinedata
where magento_order_id = 0 or magento_order_id is null

select old_item_id, 
  old_order_id, magento_order_id, 
  store_id,
  migrated, 
  old_product_id, product_id, 
  debug
from migrate_orderlinedata
where debug is not null

  select product_id, count(*)
  from migrate_orderlinedata
  where debug is not null
  group by product_id

select migrated, count(*)
from migrate_orderlinedata
where debug is not null
group by migrated


select old_item_id, 
  old_order_id, magento_order_id, 
  quantity, row_total
from migrate_orderlinedata
order by magento_order_id

select quantity, count(*)
from migrate_orderlinedata
group by quantity
order by quantity