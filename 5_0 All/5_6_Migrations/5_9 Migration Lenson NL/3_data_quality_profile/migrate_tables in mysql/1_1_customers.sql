
select 
  old_customer_id, new_magento_id, 
  email, password, created_at,
  store_id, domainID, domainName, website_group_id,
  firstname, lastname
  -- billing_company, billing_prefix, billing_firstname, billing_lastname, 
  -- billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone
  -- shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  -- shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone
  -- cus_phone, default_billing, default_shipping
  -- store_credit_amount, store_credit_comment, target_store_credit_amount
  -- unsubscribe_newsletter, disable_saved_card, RAF_code
  -- import_last_order_date, mage_last_order_date 
  -- new_magento_id_bck, migrated, priority-- , is_registered
  -- old_last_order_date, old_customer_lifecycle, hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle
from migrate_customerdata
order by old_customer_id
limit 1000 

select 
  old_customer_id, new_magento_id, 
  email, password, created_at
from migrate_customerdata
where new_magento_id <> 0
order by new_magento_id

select store_id, count(*)
from migrate_customerdata
group by store_id
order by store_id

select store_id, count(*)
from migrate_customerdata
group by store_id
order by store_id

select domainID, domainName, count(*)
from migrate_customerdata
group by domainID, domainName
order by domainID, domainName

select website_group_id, count(*)
from migrate_customerdata
group by website_group_id
order by website_group_id

select migrated, count(*)
from migrate_customerdata
group by migrated
order by migrated

------------------------------------------------


select 
  old_customer_id, new_magento_id, 
  email, password, created_at, 
  store_credit_amount, store_credit_comment, target_store_credit_amount
from migrate_customerdata
where store_credit_amount <> 0
order by new_magento_id

select 
  old_customer_id, new_magento_id, 
  email, password, created_at, 
  import_last_order_date, mage_last_order_date 
from migrate_customerdata
where import_last_order_date is not null
order by new_magento_id

select 
  old_customer_id, new_magento_id, 
  email, password, created_at, 
  old_last_order_date, old_customer_lifecycle, hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle
from migrate_customerdata
where old_last_order_date is not null
order by new_magento_id
