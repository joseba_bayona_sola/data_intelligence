
select top 1000 orders_id, 
	date_purchased, shipped, 
	customers_id, customers_name, customers_email_address, customers_dob,
	customers_street_address, customers_city, customers_postcode, customers_country, customers_telephone, 
	payment_method, delivery_method, 
	referers_name, referers_categories_name
from DW_GetLenses_jbs.dbo.lensonnl_orders
where payment_method like '%@%'

	-- Cardinality Customers
	select *
	from 
			DW_GetLenses_jbs.dbo.lensonnl_orders oh
		left join
			DW_GetLenses_jbs.dbo.lensonnl_customers c on oh.customers_id = c.customers_id
	where c.customers_id is null

	-- Order Status 
	select orders_id, date_purchased, shipped
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	where 
		(isdate(date_purchased) <> 1 or date_purchased is null) or (isdate(shipped) <> 1 or shipped is null)

	-- Order - Shipped Date
	select orders_id, 
		date_purchased, convert(datetime, date_purchased, 120),
		shipped, convert(datetime, shipped, 120)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	where 
		(isdate(date_purchased) = 1) and (isdate(shipped) = 1)

	-- Payment Method
	select payment_method, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by payment_method
	order by payment_method

	-- Delivery Method 
	select delivery_method, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by delivery_method
	order by delivery_method
	
	-- Referes Name 
	select orders_id, 
		date_purchased, 
		referers_name, referers_categories_name, 
		count(*) over ()
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	where referers_name <> 'null'
	order by referers_categories_name, referers_name, orders_id

	select top 1000 referers_categories_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	where referers_name <> 'null'
	group by referers_categories_name
	order by referers_categories_name
