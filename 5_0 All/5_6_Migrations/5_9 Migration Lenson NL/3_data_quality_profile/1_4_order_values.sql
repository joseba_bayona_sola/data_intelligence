
select top 1000 orders_total_id, 
	orders_id, class, title, value
from DW_GetLenses_jbs.dbo.lensonnl_orders_total
order by orders_id

	-- Cardinality Orders
	select orders_total_id, 
		ov.orders_id, class, title, value
	from 
			DW_GetLenses_jbs.dbo.lensonnl_orders_total ov
		full join
			DW_GetLenses_jbs.dbo.lensonnl_orders oh on ov.orders_id = oh.orders_id
	where oh.orders_id is null
	-- where ov.orders_id is null
	order by ov.orders_id, class

	-- Different Values
	select class, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_total
	group by class 
	order by class

		-- Repeted
		select *
		from
			(select orders_total_id, 
				orders_id, class, title, value, 
				count(*) over (partition by orders_id, class) num_rep
			from DW_GetLenses_jbs.dbo.lensonnl_orders_total) t
		where num_rep > 1
		order by orders_id, class

		-- Decimal Conversion
		select top 1000 orders_total_id, 
			orders_id, class, title, value
		from DW_GetLenses_jbs.dbo.lensonnl_orders_total
		where ISNUMERIC(value) <> 1 
		order by orders_id

		-- Detail
		select top 1000 orders_total_id, 
			orders_id, class, title, value, convert(decimal(12, 4), value), 
			count(*) over ()
		from DW_GetLenses_jbs.dbo.lensonnl_orders_total
		where class = 'ot_total' -- ot_subtotal - ot_shipping - ot_discount - ot_campaign_discount - ot_payment_chg - ot_total - ot_custom 
			and convert(decimal(12, 4), value) = 0
		order by convert(decimal(12, 4), value), orders_id

		---------------------------------------------------------------------------
		select convert(bigint, orders_id) order_id, convert(bigint, customers_id) customer_id, 
			convert(decimal(12, 4), isnull(ot_subtotal, 0)) ot_subtotal, convert(decimal(12, 4), isnull(ot_shipping, 0)) ot_shipping, convert(decimal(12, 4), isnull(ot_discount, 0)) ot_discount,
			convert(decimal(12, 4), isnull(ot_campaign_discount, 0)) ot_campaign_discount, convert(decimal(12, 4), isnull(ot_payment_chg, 0)) ot_payment_chg,
			convert(decimal(12, 4), isnull(ot_custom, 0)) ot_custom, convert(decimal(12, 4), isnull(ot_total, 0)) ot_total
		into #lensonnl_oh_values
		from
				(select o.orders_id, o.date_purchased, o.shipped,
					o.customers_id, o.customers_email_address, o.customers_dob, o.customers_postcode, o.customers_city, o.customers_country, 
					o.currency, o.payment_method, o.delivery_method, o.referers_name, o.referers_categories_name, 
					ot.class, ot.value
				from 
						DW_GetLenses_jbs.dbo.lensonnl_orders o
					inner join
						DW_GetLenses_jbs.dbo.lensonnl_orders_total ot on o.orders_id = ot.orders_id) t
			pivot
				(min(value) for 
				class in (ot_subtotal, ot_shipping, ot_discount, ot_campaign_discount, ot_payment_chg, ot_custom, ot_total)) pvt

		select *, abs(ot_total - ot_total_calc)
		from
			(select order_id, customer_id, 
				ot_subtotal, ot_shipping, ot_discount, ot_campaign_discount, ot_payment_chg, ot_custom, 
				ot_total, 
				ot_subtotal + ot_shipping + ot_discount + ot_campaign_discount + ot_payment_chg + ot_custom ot_total_calc
			from #lensonnl_oh_values) t
		where abs(ot_total - ot_total_calc) > 1
		order by abs(ot_total - ot_total_calc) desc
