
select products_id, 
	ProductGroup, categories_name, products_name, products_model, 
	manufacturers_name, 
	available_for_purchase, to_be_disc_or_discontinued, 
	price_eur, 
	url, product_image, products_description, products_saletext, products_info, products_userinfo, products_expired_text
from DW_GetLenses_jbs.dbo.lensonnl_products
order by ProductGroup, categories_name, products_name, products_model

	-- Product Group - Category
	select ProductGroup, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by ProductGroup
	order by ProductGroup

	select ProductGroup, categories_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by ProductGroup, categories_name
	order by ProductGroup, categories_name

	-- Products Model
	select products_model, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by products_model
	order by count(*) desc, products_model

	-- Product Rep Model
	select *
	from
		(select  products_id, 
			ProductGroup, categories_name, products_name, products_model, 
			manufacturers_name, 
			available_for_purchase, to_be_disc_or_discontinued, 
			price_eur, 
			count(*) over (partition by products_name) num_rep
		from DW_GetLenses_jbs.dbo.lensonnl_products) t
	where num_rep <> 1
	order by ProductGroup, categories_name, products_name, products_model

	-- Available - Discontinued
	select available_for_purchase, to_be_disc_or_discontinued, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by available_for_purchase, to_be_disc_or_discontinued
	order by available_for_purchase, to_be_disc_or_discontinued

	-----------------------------------------------------

	-- Product Mapping
	select p.products_id, 
		p.ProductGroup, p.categories_name, p.products_name, p.products_model, 
		pm.magento_id, p2.product_family_name, 
		count(*) over (partition by products_name) num_rep
	from 
			DW_GetLenses_jbs.dbo.lensonnl_products p
		full join
			DW_GetLenses_jbs.dbo.lensonnl_product_mapping pm on p.products_id = pm.product_id
		left join
			DW_GetLenses_jbs.dbo.new_dwh_products p2 on pm.magento_id = p2.product_id 
	-- where not(pm.magento_id = '' or pm.magento_id in (3130, 3139, 3135, 3153, 3154)) -- Real VD Magento Product
	-- where (pm.magento_id in (3130, 3139, 3135, 3153, 3154)) and p.products_id is null -- Generic Products
	-- where (pm.magento_id = '')  -- NO OL Products
	order by ProductGroup, categories_name, products_name, products_model

	-- Check 0 products with OL
	select p.products_id, 
		p.ProductGroup, p.categories_name, p.products_name, p.products_model, 
		pm.magento_id,  
		count(distinct ol.orders_id)	
	from 
			DW_GetLenses_jbs.dbo.lensonnl_products p
		full join
			DW_GetLenses_jbs.dbo.lensonnl_product_mapping pm on p.products_id = pm.product_id
		left join
			DW_GetLenses_jbs.dbo.new_dwh_products p2 on pm.magento_id = p2.product_id 
		inner join
			DW_GetLenses_jbs.dbo.lensonnl_orders_products ol on p.products_id = ol.products_id
	where pm.magento_id = '' 
	group by p.products_id, 
		p.ProductGroup, p.categories_name, p.products_name, p.products_model, 
		pm.magento_id
	order by ProductGroup, categories_name, products_name, products_model

--------------------------------------------------------------------------------------

-- VIEW
drop view dbo.lensonnl_product_mapping_v

create view dbo.lensonnl_product_mapping_v as
	select p2.category_name,
		pm.product_id, isnull(p.products_name, t.products_name) products_name, p.products_model, 
		pm.magento_id, p2.product_family_name, 
		pm.lenson_packsize, pm.magento_packsize, pm.multiplier
	from 
			DW_GetLenses_jbs.dbo.lensonnl_product_mapping pm 
		inner join
			DW_GetLenses_jbs.dbo.new_dwh_products p2 on pm.magento_id = p2.product_id 
		left join
			DW_GetLenses_jbs.dbo.lensonnl_products p on pm.product_id = p.products_id 
		left join
			(select products_id, products_name
			from
				(select ol.products_id, ol.products_name, count(*) num, 
					rank() over (partition by ol.products_id order by ol.products_name) num_rank
				from 
						DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
					full join
						DW_GetLenses_jbs.dbo.lensonnl_products p on ol.products_id = p.products_id
				where p.products_id is null
				group by ol.products_id, ol.products_name) t
			where num_rank = 1) t on pm.product_id = t.products_id
	where pm.magento_id <> 0

	select *
	from DW_GetLenses_jbs.dbo.lensonnl_product_mapping_v 
	order by category_name, magento_id, product_id		

