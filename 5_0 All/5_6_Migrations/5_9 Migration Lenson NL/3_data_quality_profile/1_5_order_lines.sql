
select top 1000 orders_products_id, 
	orders_id, 
	products_id, visma_id, products_model, info, ProductGroup,
	products_price, products_quantity, 
	total_orders_products_discount, 
	campaignName, campaignDesc, campaignType, campaignGivesFreeShippping
from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o

	-- Cardinality Orders
	select orders_products_id, ol.orders_id, products_id
	from 
			DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
		full join
			DW_GetLenses_jbs.dbo.lensonnl_orders oh on ol.orders_id = oh.orders_id
	where oh.orders_id is null
	-- where ol.orders_id is null
	order by ol.orders_products_id

	-- Cardinality Products
	select orders_products_id, ol.orders_id, ol.products_id, -- p.products_id, 
		ol.products_name,
		ol.products_model, info, ol.ProductGroup, products_price, products_quantity
	from 
			DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
		full join
			DW_GetLenses_jbs.dbo.lensonnl_products p on ol.products_id = p.products_id
	-- where p.products_id is null
	where ol.products_id is null
	order by ol.products_id, p.products_id, ol.orders_products_id

		select ol.products_id, pm.magento_id, p2.name, count(*)
		from 
				DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
			full join
				DW_GetLenses_jbs.dbo.lensonnl_products p on ol.products_id = p.products_id
			left join
				DW_GetLenses_jbs.dbo.lensonnl_product_mapping pm on ol.products_id = pm.product_id
			left join
				DW_GetLenses.dbo.products p2 on pm.magento_id = p2.product_id and p2.store_name = 'default'
		where p.products_id is null
		group by ol.products_id, pm.magento_id, p2.name 
		order by ol.products_id, pm.magento_id, p2.name

		select ol.products_id, ol.products_name, count(*)
		from 
				DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
			full join
				DW_GetLenses_jbs.dbo.lensonnl_products p on ol.products_id = p.products_id
		where p.products_id is null
		group by ol.products_id, ol.products_name
		order by ol.products_id, ol.products_name

		select ol.products_id, ol.products_name, pm.magento_id, p2.name, count(*)
		from 
				DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
			full join
				DW_GetLenses_jbs.dbo.lensonnl_products p on ol.products_id = p.products_id
			left join
				DW_GetLenses_jbs.dbo.lensonnl_product_mapping pm on ol.products_id = pm.product_id
			left join
				DW_GetLenses.dbo.products p2 on pm.magento_id = p2.product_id and p2.store_name = 'default'
		where p.products_id is null
			and pm.magento_id is null
		group by ol.products_id, ol.products_name, pm.magento_id, p2.name
		order by ol.products_id, ol.products_name, pm.magento_id, p2.name


	-- Parameters
	select top 1000 count(*) over (),
		orders_products_id, 
		orders_id, 
		products_id, visma_id, products_model, info, ProductGroup,
		products_price, products_quantity
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
	-- where (info <> '' and info <> 'NULL') and productGroup <> 'CONTACT LENSES'
	where (info <> '' and info <> 'NULL') and productGroup = 'CONTACT LENSES'

		select orders_products_id, 
			orders_id, 
			products_id, 
			case when (charindex('BC', info) <> 0) then 
				substring(info, charindex('BC', info) + 5, charindex('|', info, charindex('BC', info)) - (charindex('BC', info) + 5))
				else null
			end bc, 

			case when (charindex('DIA', info) <> 0) then 
				substring(info, charindex('DIA', info) + 6, case when (charindex('|', info, charindex('DIA', info)) = 0) then len(info) + 1
					else charindex('|', info, charindex('DIA', info)) end - (charindex('DIA', info) + 6))
				else null
			end di, 
	
			case when (charindex('PWR', info) <> 0) then 
				substring(info, charindex('PWR', info) + 6, charindex('|', info, charindex('PWR', info)) - (charindex('PWR', info) + 6))
				else null
			end po, 

			case when (charindex('CYL', info) <> 0) then 
				substring(info, charindex('CYL', info) + 6, charindex('|', info, charindex('CYL', info)) - (charindex('CYL', info) + 6))
				else null
			end cy,

			case when (charindex('AXIS', info) <> 0) then 
				substring(info, charindex('AXIS', info) + 7, case when (charindex('|', info, charindex('AXIS', info)) = 0) then len(info) + 1
					else charindex('|', info, charindex('AXIS', info)) end  - (charindex('AXIS', info) + 7))
				else null
			end ax, 

			case when (charindex('X-PWR', info) <> 0) then 
				substring(info, charindex('X-PWR', info) + 8, case when (charindex('|', info, charindex('X-PWR', info)) = 0) then len(info) + 1
					else charindex('|', info, charindex('X-PWR', info)) end  - (charindex('X-PWR', info) + 8))
				else null
			end ad
		into #lensonnl_parameters
		from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
		where (info <> '' and info <> 'NULL') and productGroup = 'CONTACT LENSES'

		select bc, count(*) -- bc - di - po - cy - ax - ad
		from #lensonnl_parameters
		group by bc
		order by bc

	-- Quantity + Products Model
	select top 1000 orders_products_id, 
		orders_id, 
		products_id, visma_id, products_model, info, ProductGroup,
		products_price, products_quantity
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
	-- where products_id in (37, 39)
	-- where productGroup = 'CONTACT LENSES'
	where convert(int, products_quantity) > 9

	select convert(int, products_quantity), count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
	group by convert(int, products_quantity)
	order by convert(int, products_quantity)

	-- Line Subtotal: Price + Quantity (Price * Quantity)
	select top 1000 orders_products_id, 
		orders_id, 
		products_id, products_price, products_quantity, total_orders_products_discount, 
		convert(decimal(12, 4), products_price) * convert(int, products_quantity) line_subtotal
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
	order by orders_id, orders_products_id

	select top 1000 oh.order_id, oh.customer_id, 
		oh.ot_subtotal, oh.ot_shipping, oh.ot_discount, oh.ot_campaign_discount, oh.ot_payment_chg, oh.ot_custom, 
		oh.ot_total, 
		ol.oh_subtotal, ol.oh_products_discount
	from 
			#lensonnl_oh_values oh
		full join	
			(select orders_id, 
				sum(convert(decimal(12, 4), products_price) * convert(int, products_quantity)) oh_subtotal, 
				sum(convert(decimal(12, 4), total_orders_products_discount)) oh_products_discount
			from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
			group by orders_id) ol on oh.order_id = ol.orders_id
	where oh.order_id is not null and ol.orders_id is not null
		and ol.oh_products_discount <> 0
	order by oh.order_id

	select oh.order_id, oh.customer_id, 
		oh.ot_subtotal, oh.ot_shipping, oh.ot_discount, oh.ot_campaign_discount, oh.ot_payment_chg, oh.ot_custom, 
		oh.ot_total, 
		ol.oh_subtotal, ol.oh_products_discount
	into #lensonnl_oh_ol_values
	from 
			#lensonnl_oh_values oh
		inner join	
			(select orders_id, 
				sum(convert(decimal(12, 4), products_price) * convert(int, products_quantity)) oh_subtotal, 
				sum(convert(decimal(12, 4), total_orders_products_discount)) oh_products_discount
			from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
			group by orders_id) ol on oh.order_id = ol.orders_id
	
	select *, abs(ot_campaign_discount + oh_products_discount)
	from #lensonnl_oh_ol_values
	-- where abs(ot_subtotal - oh_subtotal) > 1
	-- where abs(ot_campaign_discount + oh_products_discount) between -1 and 1 
	where not (abs(ot_campaign_discount + oh_products_discount) between -1 and 1) and oh_products_discount = 0
	-- where not (abs(ot_campaign_discount + oh_products_discount) between -1 and 1) and oh_products_discount <> 0
	order by abs(ot_campaign_discount + oh_products_discount)

	-- Campaign Name
	select campaignName, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products_o
	group by campaignName
	order by count(*) desc, campaignName



