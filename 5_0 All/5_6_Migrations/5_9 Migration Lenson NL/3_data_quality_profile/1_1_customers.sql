
select top 1000 customers_id, 
	customers_firstname, customers_lastname, 
	date_account_created, customers_email_address, 
	customers_dob, 
	entry_street_address, entry_postcode, entry_city, entry_country, customers_telephone, 
	customers_newsletter, customers_reminder, 
	customers_discount, customers_password
from DW_GetLenses_jbs.dbo.lensonnl_customers
where customers_id = 694253
where entry_city = '''s-Hertogenbosch'

	-- Email Repeated
	select *
	from
		(select customers_id, 
			customers_firstname, customers_lastname, 
			date_account_created, customers_email_address, 
			customers_dob, 
			entry_street_address, entry_postcode, entry_city, entry_country, customers_telephone,  
			count(*) over (partition by customers_email_address) num_rep
		from DW_GetLenses_jbs.dbo.lensonnl_customers) t
	where num_rep > 1
	order by num_rep, customers_email_address, customers_id

	-- Date Created
	select customers_id, date_account_created
		-- convert(datetime, date_account_created, 120)
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	where isdate(date_account_created) = 1

	-- DOB
	select customers_id, customers_dob, len(customers_dob)
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	order by len(customers_dob), customers_id

	-- Address Street
	select customers_id, entry_street_address, len(entry_street_address), 
		min(len(entry_street_address)) over (),
		max(len(entry_street_address)) over ()
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	order by len(entry_street_address), customers_id

	-- Address Postcode
	select customers_id, entry_postcode, len(entry_postcode), 
		min(len(entry_postcode)) over (),
		max(len(entry_postcode)) over ()
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	order by len(entry_postcode), customers_id

	-- Address City
	select entry_city, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	group by entry_city
	order by count(*) desc, entry_city

	-- Address Country
	select entry_country, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	group by entry_country
	order by count(*) desc, entry_country

	-- Address Postcode
	select customers_id, customers_telephone, len(customers_telephone), 
		min(len(customers_telephone)) over (),
		max(len(customers_telephone)) over ()
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	order by len(customers_telephone), customers_id

	-- Newsletter - Reminder
	select customers_newsletter, customers_reminder, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_customers
	group by customers_newsletter, customers_reminder
	order by customers_newsletter, customers_reminder

	------------------------------------------------------------

select c.customers_id, 
	c.customers_firstname, c.customers_lastname, 
	c.date_account_created, c.customers_email_address, max(date_purchased)
from
		(select customers_id, 
			customers_firstname, customers_lastname, 
			date_account_created, customers_email_address
		from
			(select customers_id, 
				customers_firstname, customers_lastname, 
				date_account_created, customers_email_address, 
				customers_dob, 
				entry_street_address, entry_postcode, entry_city, entry_country, customers_telephone,  
				count(*) over (partition by customers_email_address) num_rep
			from DW_GetLenses_jbs.dbo.lensonnl_customers) t
		where num_rep > 1) c
	inner join
		DW_GetLenses_jbs.dbo.lensonnl_orders oh on c.customers_id = oh.customers_id
group by c.customers_id, 
	c.customers_firstname, c.customers_lastname, 
	c.date_account_created, c.customers_email_address
order by c.customers_email_address, c.customers_id


select *
from DW_GetLenses_jbs.dbo.lensonnl_orders
-- where customers_id in (828845, 983117)
where customers_id in (548870, 1000121)
order by date_purchased
