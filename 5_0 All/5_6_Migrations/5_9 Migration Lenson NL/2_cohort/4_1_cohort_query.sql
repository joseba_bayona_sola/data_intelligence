
select top 1000 customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
	rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
from DW_GetLenses_jbs.dbo.lensonnl_cohort
where order_date is not null
order by customer_id, order_date

select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
	lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
	lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
	lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
from
	(select top 1000 customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
		rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
	from DW_GetLenses_jbs.dbo.lensonnl_cohort
	where order_date is not null) t
order by customer_id, cust_seq_ord_num


select customer_id, 
	order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
	base_grand_total, cust_seq_ord_num, 
	next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
	cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
	next_base_grand_total, next_cust_seq_ord_num
from
	(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
		lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
		lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
		lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
	from
		(select top 1000 customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
			rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
		from DW_GetLenses_jbs.dbo.lensonnl_cohort
		where order_date is not null) t) t
order by customer_id, cust_seq_ord_num

select customer_id, cust_seq_ord_num, 
	period, period_next, datediff(month, period, period_next) as month_diff, 
	base_grand_total, next_base_grand_total
from
	(select customer_id, 
		order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
		base_grand_total, cust_seq_ord_num, 
		next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
		cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
		next_base_grand_total, next_cust_seq_ord_num
	from
		(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
			lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
			lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
			lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
		from
			(select top 1000 customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
				rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
			from DW_GetLenses_jbs.dbo.lensonnl_cohort
			where order_date is not null) t) t) t
order by customer_id, cust_seq_ord_num

--------------------------------------------------------------------------------------------------------------- 

select period, cust_seq_ord_num,
	sum(base_grand_total) base_grand_total_s, count(num_cust) num_orders, count(distinct customer_id) num_customers
from
	(select customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 1 num_cust
	from
		(select customer_id, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
			cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
					rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lensonnl_cohort
				where order_date is not null) t) t) t) t
group by period, cust_seq_ord_num
order by period, cust_seq_ord_num

select period, cust_seq_ord_num, month_diff, 
	sum(base_grand_total) base_grand_total_s, sum(next_base_grand_total) next_base_grand_total_s, 
	count(num_cust) num_orders, count(distinct customer_id) num_customers
from
	(select customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 1 num_cust
	from
		(select customer_id, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
			cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
					rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lensonnl_cohort
				where order_date is not null) t) t) t) t
group by period, cust_seq_ord_num, month_diff
order by cust_seq_ord_num, period, month_diff
