
drop table DW_GetLenses_jbs.dbo.lensonnl_cohort
go 

create table DW_GetLenses_jbs.dbo.lensonnl_cohort(
	order_id				bigint, 
	customer_id				bigint, 
	order_date				date, 
	item_product_family		varchar(50),
	base_grand_total		decimal(12, 4)); 

	insert into DW_GetLenses_jbs.dbo.lensonnl_cohort(order_id, customer_id, order_date, item_product_family, base_grand_total)

		select convert(bigint, orders_id) order_id, convert(bigint, customers_id) customer_id, 
			convert(date, convert(datetime, date_purchased)) order_date, 
			null item_product_family, 
			ot_total base_grand_total
		from
				(select o.orders_id, o.date_purchased, o.shipped,
					o.customers_id, o.customers_email_address, o.customers_dob, o.customers_postcode, o.customers_city, o.customers_country, 
					o.currency, o.payment_method, o.delivery_method, o.referers_name, o.referers_categories_name, 
					ot.class, ot.value
				from 
						DW_GetLenses_jbs.dbo.lensonnl_orders o
					inner join
						DW_GetLenses_jbs.dbo.lensonnl_orders_total ot on o.orders_id = ot.orders_id) t
			pivot
				(min(value) for 
				class in (ot_subtotal, ot_shipping, ot_discount, ot_campaign_discount, ot_payment_chg, ot_total)) pvt
