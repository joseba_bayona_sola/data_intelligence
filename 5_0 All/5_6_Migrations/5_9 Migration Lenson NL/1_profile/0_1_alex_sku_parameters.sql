select orders_products_id, o.orders_id, 
	convert(date, o.date_purchased) date_purchased,
	ol.products_id, 
	ol.products_name, ol.products_model, ol.info, 

	case when (charindex('BC', info) <> 0) then 
		substring(info, charindex('BC', info) + 5, charindex('|', info, charindex('BC', info)) - (charindex('BC', info) + 5))
		else null
	end bc, 

	case when (charindex('DIA', info) <> 0) then 
		substring(info, charindex('DIA', info) + 6, case when (charindex('|', info, charindex('DIA', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('DIA', info)) end - (charindex('DIA', info) + 6))
		else null
	end di, 
	
	case when (charindex('PWR', info) <> 0) then 
		substring(info, charindex('PWR', info) + 6, charindex('|', info, charindex('PWR', info)) - (charindex('PWR', info) + 6))
		else null
	end po, 

	case when (charindex('CYL', info) <> 0) then 
		substring(info, charindex('CYL', info) + 6, charindex('|', info, charindex('CYL', info)) - (charindex('CYL', info) + 6))
		else null
	end cy,

	case when (charindex('AXIS', info) <> 0) then 
		substring(info, charindex('AXIS', info) + 7, case when (charindex('|', info, charindex('AXIS', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('AXIS', info)) end  - (charindex('AXIS', info) + 7))
		else null
	end ax, 

	case when (charindex('X-PWR', info) <> 0) then 
		substring(info, charindex('X-PWR', info) + 8, case when (charindex('|', info, charindex('X-PWR', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('X-PWR', info)) end  - (charindex('X-PWR', info) + 8))
		else null
	end ad, 

	ol.products_quantity, ol.products_price
from 
		DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
	inner join
		DW_GetLenses_jbs.dbo.lensonnl_orders o on ol.orders_id = o.orders_id
where info <> 'NULL' and info <> ''
	and convert(datetime, o.date_purchased) > '2016-05-01'
	--and products_name like 'DAILIES TOTAL1'
order by products_id, date_purchased