

select top 1000 ol.orders_id, 
	products_id, products_name, products_model, products_price, products_quantity, 
	convert(decimal(12, 4), products_price) * convert(decimal(12, 4), products_quantity) subtotal, 
	convert(date, o.date_purchased) date_purchased
from
		DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
	inner join
		DW_GetLenses_jbs.dbo.lensonnl_orders o on ol.orders_id = o.orders_id
where products_id = 37 and convert(decimal(12, 4), products_price) = 0
order by products_id, products_name


select products_id, products_name, products_model, products_price, -- charindex(products_model, '/'),
	sum(convert(integer, products_quantity)) total_quantity, 
	sum(convert(decimal(12, 4), products_price) * convert(decimal(12, 4), products_quantity)) total_subtotal, 
	min(convert(date, o.date_purchased)) min_date_purchased, max(convert(date, o.date_purchased)) max_date_purchased
from
		DW_GetLenses_jbs.dbo.lensonnl_orders_products ol
	inner join
		DW_GetLenses_jbs.dbo.lensonnl_orders o on ol.orders_id = o.orders_id
where convert(datetime, o.date_purchased) > '2016-05-01'
group by products_id, products_name, products_model, products_price
order by products_id, products_name, products_model, min_date_purchased
