
select products_id, 
	ProductGroup, categories_name, 
	manufacturers_name, 
	products_name, products_model, price_gbp
	available_for_purchase, to_be_disc_or_discontinued
from DW_GetLenses_jbs.dbo.lensonnl_products
order by productgroup, products_name

	select ProductGroup, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by ProductGroup

	select ProductGroup, categories_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_products
	group by ProductGroup, categories_name
	order by ProductGroup, categories_name


select customers_id, 
	date_account_created,
	customers_firstname, customers_email_address, 
	customers_dob,
	entry_postcode, entry_city, entry_country,
	customers_newsletter, customers_reminder, customers_discount
from DW_GetLenses_jbs.dbo.lensonnl_customers
where customers_id = 100624

	select count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_customers

	
select top 1000 orders_id, date_purchased, shipped,
	customers_id, customers_email_address, customers_dob, customers_postcode, customers_city, customers_country, 
	currency, payment_method, delivery_method, referers_name, referers_categories_name
from DW_GetLenses_jbs.dbo.lensonnl_orders
-- where customers_id = 100624
order by orders_id desc

	select count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders

	select year(convert(datetime, date_purchased)) yyyy_order, count(*) num_orders
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by year(convert(datetime, date_purchased)) 
	order by year(convert(datetime, date_purchased)) 

	select customers_id, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by customers_id
	order by count(*) desc

	select payment_method, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by payment_method
	order by payment_method

	select delivery_method, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by delivery_method
	order by delivery_method

	select referers_categories_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders
	group by referers_categories_name
	order by referers_categories_name


select top 1000 orders_total_id, orders_id, 
	class, title, value
from DW_GetLenses_jbs.dbo.lensonnl_orders_total

	select class, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_total
	group by class
	order by class


select orders_id, date_purchased, shipped,
	customers_id, customers_email_address, customers_dob, customers_postcode, customers_city, customers_country, 
	currency, payment_method, delivery_method, referers_name, referers_categories_name, 
	ot_subtotal, ot_shipping, ot_discount, ot_campaign_discount, ot_payment_chg, ot_total
from
		(select o.orders_id, o.date_purchased, o.shipped,
			o.customers_id, o.customers_email_address, o.customers_dob, o.customers_postcode, o.customers_city, o.customers_country, 
			o.currency, o.payment_method, o.delivery_method, o.referers_name, o.referers_categories_name, 
			ot.class, ot.value
		from 
				DW_GetLenses_jbs.dbo.lensonnl_orders o
			inner join
				DW_GetLenses_jbs.dbo.lensonnl_orders_total ot on o.orders_id = ot.orders_id) t
	pivot
		(min(value) for 
		class in (ot_subtotal, ot_shipping, ot_discount, ot_campaign_discount, ot_payment_chg, ot_total)) pvt
where orders_id = 3159763
order by orders_id desc






select top 1000 orders_products_id, orders_id, products_id, 
	visma_id, 
	productGroup,
	products_name, products_model, info, products_quantity, products_price, 
	total_orders_products_discount, 
	campaignName, campaignDesc, campaignType, campaignGivesFreeShippping
from DW_GetLenses_jbs.dbo.lensonnl_orders_products
where orders_id = 3159763
-- where ProductGroup = 'CONTACT LENSES' and (info = 'NULL' or info = '')
order by products_id, products_name, orders_id

	select count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	where info <> 'NULL' and info <> ''

	select productGroup, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	where info = 'NULL' or info = ''
	group by productGroup
	order by productGroup

	select products_id, products_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	where info = 'NULL' or info = ''
	group by products_id, products_name
	order by products_id, products_name

	select products_id, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	group by products_id
	order by products_id

	select products_name, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	group by products_name
	order by products_name

	select campaignType, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	group by campaignType
	order by campaignType

	select campaignName, count(*)
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	group by campaignName
	order by campaignName


---------------------------------------

select orders_products_id, orders_id, products_id, 
	products_name, products_model, info, products_quantity, products_price,

	case when (charindex('BC', info) <> 0) then 
		substring(info, charindex('BC', info) + 5, charindex('|', info, charindex('BC', info)) - (charindex('BC', info) + 5))
		else null
	end bc, 

	case when (charindex('DIA', info) <> 0) then 
		substring(info, charindex('DIA', info) + 6, case when (charindex('|', info, charindex('DIA', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('DIA', info)) end - (charindex('DIA', info) + 6))
		else null
	end di, 
	
	case when (charindex('PWR', info) <> 0) then 
		substring(info, charindex('PWR', info) + 6, charindex('|', info, charindex('PWR', info)) - (charindex('PWR', info) + 6))
		else null
	end po, 

	case when (charindex('CYL', info) <> 0) then 
		substring(info, charindex('CYL', info) + 6, charindex('|', info, charindex('CYL', info)) - (charindex('CYL', info) + 6))
		else null
	end cy,

	case when (charindex('AXIS', info) <> 0) then 
		substring(info, charindex('AXIS', info) + 7, case when (charindex('|', info, charindex('AXIS', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('AXIS', info)) end  - (charindex('AXIS', info) + 7))
		else null
	end ax, 

	case when (charindex('X-PWR', info) <> 0) then 
		substring(info, charindex('X-PWR', info) + 8, case when (charindex('|', info, charindex('X-PWR', info)) = 0) then len(info) + 1
			else charindex('|', info, charindex('X-PWR', info)) end  - (charindex('X-PWR', info) + 8))
		else null
	end ad
from DW_GetLenses_jbs.dbo.lensonnl_orders_products
where info <> 'NULL' and info <> ''
order by po


select ad, count(*) 
from
	(select orders_products_id, orders_id, products_id, 
		products_name, products_model, info, products_quantity, products_price,

		case when (charindex('BC', info) <> 0) then 
			substring(info, charindex('BC', info) + 5, charindex('|', info, charindex('BC', info)) - (charindex('BC', info) + 5))
			else null
		end bc, 
		case when (charindex('DIA', info) <> 0) then 
			substring(info, charindex('DIA', info) + 6, case when (charindex('|', info, charindex('DIA', info)) = 0) then len(info) + 1
				else charindex('|', info, charindex('DIA', info)) end - (charindex('DIA', info) + 6))
			else null
		end di, 
		case when (charindex('PWR', info) <> 0) then 
			substring(info, charindex('PWR', info) + 6, charindex('|', info, charindex('PWR', info)) - (charindex('PWR', info) + 6))
			else null
		end po, 
		case when (charindex('CYL', info) <> 0) then 
			substring(info, charindex('CYL', info) + 6, charindex('|', info, charindex('CYL', info)) - (charindex('CYL', info) + 6))
			else null
		end cy,
		case when (charindex('AXIS', info) <> 0) then 
			substring(info, charindex('AXIS', info) + 7, case when (charindex('|', info, charindex('AXIS', info)) = 0) then len(info) + 1
				else charindex('|', info, charindex('AXIS', info)) end  - (charindex('AXIS', info) + 7))
			else null
		end ax, 
		case when (charindex('X-PWR', info) <> 0) then 
			substring(info, charindex('X-PWR', info) + 8, case when (charindex('|', info, charindex('X-PWR', info)) = 0) then len(info) + 1
				else charindex('|', info, charindex('X-PWR', info)) end  - (charindex('X-PWR', info) + 8))
			else null
		end ad
	from DW_GetLenses_jbs.dbo.lensonnl_orders_products
	where info <> 'NULL' and info <> '') t
group by ad
order by ad