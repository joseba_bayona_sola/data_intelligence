
drop table DW_GetLenses_jbs.dbo.lentidom_cohort
go 

create table DW_GetLenses_jbs.dbo.lentidom_cohort(
	order_id				bigint, 
	customer_id				bigint, 
	order_date				date, 
	item_product_family		varchar(50),
	base_grand_total		decimal(12, 4)); 

	insert into DW_GetLenses_jbs.dbo.lentidom_cohort(order_id, customer_id, order_date, item_product_family, base_grand_total)

		select convert(int, orderid) order_id, convert(int, customerId) customer_id, convert(date, orderDate) order_date, null item_product_family, 
			convert(decimal(12, 4), order_grand_total) order_grand_total
		from DW_GetLenses_jbs.dbo.lentidom_orders
