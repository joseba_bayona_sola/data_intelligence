
select count(distinct customer_id) -- 23385
from DW_GetLenses_jbs.dbo.lentidom_cohort
where order_date is not null

select top 1000 count(*) over () num_tot,
	customer_id, cust_seq_ord_num, 
	period, period_next, datediff(month, period, period_next) as month_diff, 
	base_grand_total, next_base_grand_total, 
	case when (period_next is null) then 0 else 1 end as rep, 
	max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
from
	(select customer_id, 
		order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
		base_grand_total, cust_seq_ord_num, 
		next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
		cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
		next_base_grand_total, next_cust_seq_ord_num
	from
		(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
			lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
			lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
			lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
		from
			(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
				rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
			from DW_GetLenses_jbs.dbo.lentidom_cohort
			where order_date is not null) t) t) t
order by customer_id, cust_seq_ord_num

--------------------------

select rep, count(*) num
from
	(select count(*) over () num_tot,
		customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 
		case when (period_next is null) then 0 else 1 end as rep, 
		max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
	from
		(select customer_id, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
			cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
					rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lentidom_cohort
				where order_date is not null) t) t) t) t
group by rep
order by rep

select max_seq_ord_num, count(*) num, count(*) *100/convert(decimal(10,2), 23385) perc_num_tot
from
	(select distinct 
		customer_id, max_seq_ord_num
	from
		(select count(*) over () num_tot,
			customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, next_base_grand_total, 
			case when (period_next is null) then 0 else 1 end as rep, 
			max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
		from
			(select customer_id, 
				order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
				cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
						rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lentidom_cohort
					where order_date is not null) t) t) t) t) t
group by max_seq_ord_num
order by max_seq_ord_num


select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
from
	(select distinct 
		customer_id, max_seq_ord_num
	from
		(select count(*) over () num_tot,
			customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, next_base_grand_total, 
			case when (period_next is null) then 0 else 1 end as rep, 
			max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
		from
			(select customer_id, 
				order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
				cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
						rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lentidom_cohort
					where order_date is not null) t) t) t) t) t
group by max_seq_ord_num
order by max_seq_ord_num

select sum(num_orders_no_rep), sum(num_orders_rep)
from
	(select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
	from
		(select distinct 
			customer_id, max_seq_ord_num
		from
			(select count(*) over () num_tot,
				customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 
				case when (period_next is null) then 0 else 1 end as rep, 
				max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
			from
				(select customer_id, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_order_date, year(next_order_date) yyyy_next, month(next_order_date) mm_next, 
					cast(cast(year(next_order_date) as varchar) + '-' + cast(month(next_order_date) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, order_date, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by customer_id order by cust_seq_ord_num) next_order_date, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, order_date, year(order_date) yyyy, month(order_date) mm, base_grand_total, 
							rank() over (partition by customer_id order by order_date, order_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lentidom_cohort
						where order_date is not null) t) t) t) t) t
	group by max_seq_ord_num) t