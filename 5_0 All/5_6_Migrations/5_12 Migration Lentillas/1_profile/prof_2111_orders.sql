
SELECT 
  -- id_comanda, id_usuari, 
  referencia, 	
  email_usuari, enviament_nom, enviament_cognom1, 
  CASE 
	WHEN (estat = 1) THEN 'Processing' 
	WHEN (estat = 2) THEN 'Shipped' 
	WHEN (estat = 3) THEN 'Delivered' 
	WHEN (estat = 4) THEN 'Received' 
	WHEN (estat = 70) THEN 'Shipped Partially'
	WHEN (estat = 80) THEN 'Delivered Partially' 
  END estat,
  FROM_UNIXTIME(data_alta) data_alta, FROM_UNIXTIME(data_enviament) data_enviament, FROM_UNIXTIME(data_entrega) data_entrega, 
  -- pagat, 
  preu_total_comanda,
  CASE WHEN (metode_enviament = 1) THEN 'MRW' ELSE 'GLS' END metode_enviament
FROM slg_comandes
WHERE pagat = 1 AND FROM_UNIXTIME(data_alta) > '2019-11-21'
	-- and data_enviament is not null
	-- AND estat = 3
ORDER BY id_comanda DESC
LIMIT 10000;


SELECT 
  c.id_comanda, -- id_usuari, 
  c.referencia, 	
  c.email_usuari, c.enviament_nom, c.enviament_cognom1, 
  CASE 
	WHEN (c.estat = 1) THEN 'Processing' 
	WHEN (c.estat = 2) THEN 'Shipped' 
	WHEN (c.estat = 3) THEN 'Delivered' 
	WHEN (c.estat = 4) THEN 'Received' 
	WHEN (c.estat = 70) THEN 'Shipped Partially'
	WHEN (c.estat = 80) THEN 'Delivered Partially' 
  END estat,
  FROM_UNIXTIME(c.data_alta) data_alta, FROM_UNIXTIME(c.data_enviament) data_enviament, FROM_UNIXTIME(c.data_entrega) data_entrega, 
  -- pagat, 
  c.preu_total_comanda,
  CASE WHEN (c.metode_enviament = 1) THEN 'MRW' ELSE 'GLS' END metode_enviament, 
  
  i.nom_es, 
  cdl.ojo, cdl.curvabase, cdl.diametro, cdl.cilindro, cdl.eje, cdl.esfera, cdl.esfera_lejos, cdl.color, -- cl.nom_es color_str,
  cdl.adicion_dn, 
    CASE 
      WHEN (cdl.adicion_hl = 1) THEN 'High'
      WHEN (cdl.adicion_hl = 2) THEN 'Low'
      WHEN (cdl.adicion_hml = 1) THEN 'High'
      WHEN (cdl.adicion_hml = 2) THEN 'Medium'
      WHEN (cdl.adicion_hml = 3) THEN 'Low'
      ELSE cdl.adicion
    END adicion_str,  
    cdl.adicion, cdl.adicion_hl, cdl.adicion_hml  
FROM 
	slg_comandes c
  INNER JOIN
      slg_comandes_items ci ON c.id_comanda = ci.id_comanda
  INNER JOIN
      slg_comandes_detalls cd ON ci.id_comanda_item = cd.id_comanda_item	
  INNER JOIN 
      slg_items i ON cd.id_item = i.id_item
  LEFT JOIN
      slg_comandes_detalls_lentilles cdl ON cd.id_comanda_detall = cdl.id_comanda_detall      
WHERE c.pagat = 1 AND FROM_UNIXTIME(c.data_alta) > '2019-11-21' -- and c.id_comanda = 111843
	-- and data_enviament is not null
	-- AND estat = 3
	-- AND c.id_comanda IN (112486, 112564, 112509, 112568, 112472, 112430, 112469, 112479, 112482, 112536, 112505, 112520, 112518, 112514, 112544, 112551, 112567, 112576, 112578, 112579)
	AND c.id_comanda IN (23426, 23416, 23438)
ORDER BY c.id_comanda DESC
LIMIT 10000;

-- ------------------------------------------------------------------------


SELECT c.id_comanda,
  -- c.id_comanda, id_usuari, 
  c.referencia order_no, 	
  -- c.email_usuari customer_email, 
  c.enviament_nom customer_first_name, c.enviament_cognom1 customer_last_name, 
  CASE 
	WHEN (c.estat = 1) THEN 'Processing' 
	WHEN (c.estat = 2) THEN 'Shipped' 
	WHEN (c.estat = 3) THEN 'Delivered' 
	WHEN (c.estat = 4) THEN 'Received' 
	WHEN (c.estat = 70) THEN 'Shipped Partially'
	WHEN (c.estat = 80) THEN 'Delivered Partially' 
  END order_status,
  FROM_UNIXTIME(c.data_alta) created_date, FROM_UNIXTIME(c.data_enviament) shipped_date, FROM_UNIXTIME(c.data_entrega) delivered_date, 
  -- pagat, 
  c.preu_total_comanda order_value,
  CASE WHEN (c.metode_enviament = 1) THEN 'MRW' ELSE 'GLS' END shipping_method, 
  
  i.nom_es product_name, 
  cdl.curvabase base_curve, cdl.diametro diameter, CASE WHEN (cdl.esfera IS NULL) THEN cdl.esfera_lejos ELSE cdl.esfera END POWER, 
  cdl.eje axis, cdl.cilindro cylinder, 
  -- cdl.color, -- cl.nom_es color_str,  
    CASE 
      WHEN (cdl.adicion_hl = 1) THEN 'High'
      WHEN (cdl.adicion_hl = 2) THEN 'Low'
      WHEN (cdl.adicion_hml = 1) THEN 'High'
      WHEN (cdl.adicion_hml = 2) THEN 'Medium'
      WHEN (cdl.adicion_hml = 3) THEN 'Low'
      ELSE cdl.adicion
    END addition,  
  CASE 
      WHEN (cdl.adicion_dn = 1) THEN 'D' 
      WHEN (cdl.adicion_dn = 2) THEN 'N' 
      ELSE NULL
  END dominant, 
  cl.nom_es color, 
  pm.lad_packsize,
  ci.quantitat quantity, 
  
  c.enviament_adreca AS `shipping_street1`, c.enviament_poblacio AS `shipping_city`, c.enviament_provincia AS `shipping_region`, 
    c.enviament_codipostal AS `shipping_postcode`,
    CASE
      WHEN LEFT(c.enviament_pais, 1) IN (1, 2, 3) THEN 'ES'
      WHEN LEFT(c.enviament_pais, 1) IN (4) THEN 'PT'
      ELSE NULL
    END `shipping_country`, c.enviament_telefon AS `shipping_telephone`
FROM 
	slg_comandes c
  INNER JOIN
      slg_comandes_items ci ON c.id_comanda = ci.id_comanda
  INNER JOIN
      slg_comandes_detalls cd ON ci.id_comanda_item = cd.id_comanda_item	
  INNER JOIN 
      slg_items i ON cd.id_item = i.id_item
  LEFT JOIN
      slg_comandes_detalls_lentilles cdl ON cd.id_comanda_detall = cdl.id_comanda_detall      
  LEFT JOIN
      slg_items_colors_lentilles cl ON cdl.color = cl.id_item_color_lentilla      
  LEFT JOIN
    lad_lex.lad_product_map pm ON cd.id_item = pm.lad_id
WHERE c.pagat = 1 AND FROM_UNIXTIME(c.data_alta) > '2019-11-21' -- and c.id_comanda = 111843
	-- and data_enviament is not null
	-- AND estat = 3
	-- AND c.id_comanda IN (112486, 112564, 112509, 112568, 112472, 112430, 112469, 112479, 112482, 112536, 112505, 112520, 112518, 112514, 112544, 112551, 112567, 112576, 112578, 112579)	
	-- AND c.id_comanda IN (112579, 112567, 112544, 112536, 112518, 112514)
	AND c.id_comanda IN (23426, 23416, 23438)
ORDER BY c.id_comanda DESC;