# LEX TOTAL DISTINCT USERS WITH A PURCHASE GROUPED BY YEAR
select count(distinct(id_usuari)), YEAR(from_unixtime(data_alta)) as `year`
from slg_comandes
where 1
and pagat = 1
group by YEAR(from_unixtime(data_alta))