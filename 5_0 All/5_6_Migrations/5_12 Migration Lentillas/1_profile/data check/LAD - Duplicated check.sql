# LAD - Unique custormers and unique emails
select count(*) num_customer_records, count(distinct email) num_email_records
  from
    (select id_usuari, login email -- login / enviament_mail / facturacio_mail
    from cfg_usuaris) u;