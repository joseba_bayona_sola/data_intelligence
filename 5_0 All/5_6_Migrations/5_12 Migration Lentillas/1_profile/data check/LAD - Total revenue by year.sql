# LAD TOTAL REVENUE BY YEAR
select format(sum(preu_total_comanda),2, 'en_UK') as `#totalRevenue` , YEAR(from_unixtime(data_alta)) as `year`
from slg_comandes
where 1
and pagat = 1
group by YEAR(from_unixtime(data_alta))