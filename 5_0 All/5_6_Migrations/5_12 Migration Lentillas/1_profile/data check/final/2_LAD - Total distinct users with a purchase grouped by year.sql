# LAD TOTAL DISTINCT USERS WITH A PURCHASE GROUPED BY YEAR

SELECT COUNT(DISTINCT(id_usuari)) -- , YEAR(from_unixtime(data_alta)) as `year`
FROM slg_comandes
WHERE 1
AND pagat = 1
AND data_alta BETWEEN UNIX_TIMESTAMP('2014/07/11') AND UNIX_TIMESTAMP('2019/07/12')
-- group by YEAR(from_unixtime(data_alta))
