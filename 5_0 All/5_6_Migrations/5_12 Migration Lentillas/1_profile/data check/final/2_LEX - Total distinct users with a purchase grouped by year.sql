# LEX TOTAL DISTINCT USERS WITH A PURCHASE GROUPED BY YEAR
select count(distinct(id_usuari)), YEAR(from_unixtime(data_alta)) as `year`
from slg_comandes
where 1
and pagat = 1
group by YEAR(from_unixtime(data_alta))


select count(distinct(id_usuari)) --, YEAR(from_unixtime(data_alta)) as `year`
from slg_comandes
where 1
and pagat = 1
and data_alta between unix_timestamp('2016/11/16') and unix_timestamp('2019/07/12')
-- group by YEAR(from_unixtime(data_alta))
