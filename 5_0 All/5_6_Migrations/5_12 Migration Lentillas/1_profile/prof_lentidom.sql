
select top 1000 orderid, orderDate, customerId, shippingCountry, invoicingCountry, 
	order_grand_total, orderShippingAmount, orderDiscountAmount, orderProductsTotal
from DW_GetLenses_jbs.dbo.lentidom_orders

	select convert(int, orderid) orderid, convert(date, orderDate) orderDate, convert(int, customerId) customerId, shippingCountry, convert(int, invoicingCountry) invoicingCountry, 
		convert(decimal(12, 4), order_grand_total) order_grand_total, convert(decimal(12, 4), orderShippingAmount) orderShippingAmount, 
		convert(decimal(12, 4), orderDiscountAmount) orderDiscountAmount, convert(decimal(12, 4), orderProductsTotal) orderProductsTotal
	into #lentidom_orders
	from DW_GetLenses_jbs.dbo.lentidom_orders

	select orderid, orderDate, customerId, shippingCountry, invoicingCountry, 
		order_grand_total, orderShippingAmount, orderDiscountAmount, orderProductsTotal
	from #lentidom_orders

	select count(*), count(distinct orderid), count(distinct customerId)
	from #lentidom_orders

	select count(distinct customerId), min(customerID), max(customerID)
	from #lentidom_orders

	select year(orderDate), count(*), sum(order_grand_total), sum(order_grand_total) / count(*)
	from #lentidom_orders
	group by year(orderDate)
	order by year(orderDate)

	select year(orderDate), month(orderDate), count(*), sum(order_grand_total)
	from #lentidom_orders
	group by year(orderDate), month(orderDate)
	order by year(orderDate), month(orderDate)

	select shippingCountry, invoicingCountry, count(*)
	from #lentidom_orders
	group by shippingCountry, invoicingCountry
	order by shippingCountry, invoicingCountry

	select orderid, orderDate, customerId, shippingCountry, invoicingCountry, 
		order_grand_total, orderShippingAmount, orderDiscountAmount, orderProductsTotal, 
		abs (order_grand_total - (orderProductsTotal + orderShippingAmount - orderDiscountAmount)) diff
	from #lentidom_orders
	where order_grand_total <> orderProductsTotal + orderShippingAmount - orderDiscountAmount
	order by orderid desc

select top 1000 orderId, lineId, customerId, orderDate,
	categoryId, itemid, name, url, 
	qty, lineTotal
from DW_GetLenses_jbs.dbo.lentidom_order_lines

	select convert(int, orderId) orderId, convert(int, lineId) lineId, convert(int, customerId) customerId, convert(date, orderDate) orderDate,
		convert(int, categoryId) categoryId, convert(int, itemid) itemid, name, url, 
		convert(int, qty) qty, convert(decimal(12, 4), lineTotal) lineTotal
	into #lentidom_order_lines
	from DW_GetLenses_jbs.dbo.lentidom_order_lines

	select orderId, lineId, customerId, orderDate,
		categoryId, itemid, name, url, 
		qty, lineTotal
	from #lentidom_order_lines

	select count(*), count(distinct lineid), count(distinct orderid), count(distinct customerId)
	from #lentidom_order_lines

	select categoryId, itemid, name, url, count(*), sum(lineTotal) sum_total, sum(sum(lineTotal)) over (), 
		convert(decimal(12, 4), sum(lineTotal)) * 100 / sum(sum(lineTotal)) over () perc_per_product
	from #lentidom_order_lines
	group by categoryId, itemid, name, url
	order by name, url
	-- order by categoryId, itemid, name, url

	select oh.*, ol.num_lines, ol.sum_total
	from
			(select orderid, count(*) num_lines, sum(lineTotal) sum_total
			from #lentidom_order_lines
			group by orderid) ol
		full join
			#lentidom_orders oh on ol.orderId = oh.orderid
	-- where oh.orderid is null
	-- where ol.orderId is null
	-- where oh.orderProductsTotal <> ol.sum_total
	where order_grand_total <> orderProductsTotal + orderShippingAmount - orderDiscountAmount
	order by oh.orderid desc

select orderid, lineid, productid, eyeid, 
	baseCurve, diameter, sph, sphNear, cyl, axis, adition, adicion_dn, adicion_hl, adicion_hml, colorId
from DW_GetLenses_jbs.dbo.lentidom_order_lines_parameters
where adition <> 'NULL'

	select convert(int, orderId) orderId, convert(int, lineId) lineId, convert(int, productid) productid, convert(int, eyeid) eyeid, 
		baseCurve, diameter, sph, sphNear, cyl, axis, adition, adicion_dn, adicion_hl, adicion_hml, colorId
	into #lentidom_order_lines_parameters
	from DW_GetLenses_jbs.dbo.lentidom_order_lines_parameters

	select ol.orderId, ol.lineId, ol.customerId, ol.orderDate,
		ol.categoryId, ol.itemid, ol.name, 
		olp.eyeid, olp.baseCurve, olp.diameter, olp.sph, olp.sphNear, olp.cyl, olp.axis, olp.adition, olp.adicion_dn, olp.adicion_hl, olp.adicion_hml, olp.colorId,
		ol.qty, ol.lineTotal, 

		olp.orderId, olp.lineId, olp.productid
	from 
			#lentidom_order_lines ol
		full join
			#lentidom_order_lines_parameters olp on ol.lineid = olp.lineid
	where ol.lineid is null
	-- where olp.lineId is null
	order by ol.lineId desc


	select count(distinct olp.orderid)
		from 
			#lentidom_order_lines ol
		full join
			#lentidom_order_lines_parameters olp on ol.lineid = olp.lineid
	where ol.lineid is null

------------------------------------------

select *
from DW_GetLenses_jbs.dbo.lentiexp_product_map

	-- select t1.categoryId, t1.itemid, t2.lentidom_id, t2.magento_id, t1.name, t2.lentidom_name, t2.magento_name, t1.url, t1.num, t1.sum_total, t1.perc_per_product
	select t1.itemid, t2.magento_id, t1.name, t2.lentidom_name, t2.magento_name, t1.num num_lines, t1.sum_total, t1.perc_per_product, 
		case when (t2.lentidom_id is null) then 'Y' else 'N' end flag_not_in_file, 
		case when (t2.lentidom_id is not null and t2.magento_id = '') then 'Y' else 'N' end flag_in_file_not_mapped
	from
			(select categoryId, itemid, name, url, count(*) num, sum(lineTotal) sum_total, 
				convert(decimal(12, 4), sum(lineTotal)) * 100 / sum(sum(lineTotal)) over () perc_per_product
			from #lentidom_order_lines
			group by categoryId, itemid, name, url) t1
		left join
			DW_GetLenses_jbs.dbo.lentidom_product_map t2 on t1.itemid = t2.lentidom_id
	-- where t2.lentidom_id is null -- 44
	-- where t2.lentidom_id is not null and t2.magento_id = '' -- 309
	-- where t2.lentidom_id is not null and t2.magento_id <> '' -- 83
	-- where t1.itemid is null -- 305
	order by perc_per_product desc
	-- order by t1.itemid

	-- select 44 + 309 + 83 + 293
	-- select 266 + 63 + 293