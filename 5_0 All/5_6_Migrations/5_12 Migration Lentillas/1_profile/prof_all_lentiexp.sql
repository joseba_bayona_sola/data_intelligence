
-- customers

select id_pais, nombre_es, code_iso2, code_iso3, id_iva_enviament
from lex.cfg_paises;

select id_usuari, 
  estat, tipus_client, 
  data_alta, -- data_modificacio, 
  login, password, 
  enviament_mail, enviament_nom, enviament_cognom1, enviament_cognom2, enviament_raosocial, enviament_telefon, 
  enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, -- enviament_regularitzat, 
  facturacio_mail, facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_raosocial, facturacio_telefon, 
  facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, -- facturacio_regularitzat, 
  facturacio_nif, 
  accepta_mail, comanda_successiva  
from lex.cfg_usuaris
where facturacio_pais <> 724
order by id_usuari desc
limit 1000;

  select count(*), min(id_usuari), max(id_usuari), count(distinct login)
  from lex.cfg_usuaris;

  -- enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais
  select enviament_poblacio, count(*)
  from lex.cfg_usuaris
  group by enviament_poblacio
  order by enviament_poblacio;

  select enviament_provincia, count(*)
  from lex.cfg_usuaris
  group by enviament_provincia
  order by enviament_provincia;
  
  select enviament_pais, count(*)
  from lex.cfg_usuaris
  group by enviament_pais
  order by enviament_pais;  
  
  -- facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais
  select facturacio_poblacio, count(*)
  from lex.cfg_usuaris
  group by facturacio_poblacio
  order by facturacio_poblacio;

  select facturacio_provincia, count(*)
  from lex.cfg_usuaris
  group by facturacio_provincia
  order by facturacio_provincia;
  
  select facturacio_pais, count(*)
  from lex.cfg_usuaris
  group by facturacio_pais
  order by facturacio_pais;
  
  -- 

  select accepta_mail, count(*)
  from lex.cfg_usuaris
  group by accepta_mail
  order by accepta_mail;
  
  select comanda_successiva, count(*)
  from lex.cfg_usuaris
  group by comanda_successiva
  order by comanda_successiva;
  

-- products

select id_categoria, 
  data_alta, 
  visible, iva, 
  permalink_es, permalink_pt, nom_es, nom_pt
from lex.slg_categories
order by id_categoria;

select id_proveidor, 
  data_alta, 
  visible, 
  permalink_es, permalink_pt, nom_es, nom_pt
from lex.slg_proveidors
order by id_proveidor;

select id_marca, 
  data_alta, 
  visible, 
  permalink_es, permalink_pt, nom_es, nom_pt
from lex.slg_marques
order by id_marca;


select id_item, 
  id_categoria, id_proveidor, id_marca, id_disponibilitat, 
  data_alta, 
  ean, en_portada, prioritat, 
  visible_es, visible_pt, oferta_es, oferta_pt, recomanat_es, recomanat_pt, merchants_es, merchants_pt, 
  preu_cost, 
  -- pvp_tarifa_es, pvp_tarifa_pt,
  pvp_es, pvp_pt, 
  pvp_desc1_es, pvp_desc1_pt, pvp_desc2_es, pvp_desc2_pt, pvp_desc3_es, pvp_desc3_pt, 
  permalink_es, permalink_pt, permalink_es_antic, permalink_es_nou, 
  nom_es, nom_pt, 
  descripcio_es, descripcio_pt, descripcio_bbcode_es, descripcio_html_es, descripcio_bbcode_pt, descripcio_html_pt, 
  -- beneficis_html_es, beneficis_html_pt, parametres_html_es, parametres_html_pt, pensades_html_es, pensades_html_pt, altres_html_es, altres_html_pt, 
  meta_descripcio_es, meta_descripcio_pt, meta_keywords_es, meta_keywords_pt, 
  denominacio_es, denominacio_pt, 
  ratings_opinions, ratings_estrelles, 
  transit_permes, -- sense_graduar, 
  id_item_ps, ps_items_sinrelacion, 
  -- millor_preu_es, millor_preu_pt, descatalogat_es, descatalogat_pt, 
  -- id_item_alternatiu, 
  agrupat_mestre, id_item_importat, 
  -- pov_puente, pov_varilla, 
  merchants_preu_es, merchants_preu_pt,
  num_caixes
from lex.slg_items
order by id_item;

select id_item_color, 
  data_alta, 
  visible, 
  nom_es, nom_pt
from lex.slg_items_colors
order by id_item_color;

select id_item_talla, 
  data_alta, 
  visible, 
  nom_es, nom_pt
from lex.slg_items_talles
order by id_item_talla;

-- orders


select id_comanda, 
  id_usuari, id_promocodi, id_factura, 
  -- email_usuari, 
  data_factura, data_alta, data_baixa, data_avis, data_avis_nopagat, data_entrega, -- data_sortida,
  pagat, referencia, pasarela_pagament, info_pagament, 
  array_cistella, array_dades_pagament, 
  comanda_anterior, 
  import_productes, import_enviament, import_contrareemborsament, import_descompte, preu_total_comanda, 
  promocodi_tipus, promocodi_valor, promocodi_nom, 
  metode_enviament, 
  tipus_client, 
  enviament_nom, enviament_cognom1, enviament_cognom2, enviament_raosocial, enviament_telefon, 
  enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, 
  facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_raosocial, facturacio_telefon, 
  facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, 
  facturacio_nif, 
  comentaris, comanda_nova, 
  nom_host, dispositiu, 
  estat, te_etiqueta, anotacions, compra_rapida, validada, 
  ctrl_usuari_pagina_resultat, ctrl_email_comanda_enviat, ctrl_email_cron_enviat, ctrl_analytics_cron, 
  -- id_max_disponibilitat, 
  id_estat_enviament_mrw, data_estat_enviament_mrw, data_enviament,
  -- id_estat_enviament_asm, data_estat_enviament_asm, 
  -- id_missatger_enviament, 
  factura_prn -- , data_canvi_a_en_tramit  
from lex.slg_comandes
order by id_comanda desc
limit 1000;

  select count(*), min(id_comanda), max(id_comanda), count(distinct id_usuari)
  from lex.slg_comandes;

  select pagat, count(*)
  from lex.slg_comandes
  group by pagat
  order by pagat
  
  select pasarela_pagament, count(*)
  from lex.slg_comandes
  group by pasarela_pagament
  order by pasarela_pagament
  
  select enviament_pais, left(enviament_pais, 1), count(*)
  from lex.slg_comandes
  group by enviament_pais
  order by enviament_pais
  
  select facturacio_pais, count(*)
  from lex.slg_comandes
  group by facturacio_pais
  order by facturacio_pais  
  
  -- metode_enviament, tipus_client, nom_host, dispositiu, estat, id_max_disponibilitat
  select metode_enviament, count(*)
  from lex.slg_comandes
  group by metode_enviament
  order by metode_enviament
  
  select tipus_client, count(*)
  from lex.slg_comandes
  group by tipus_client
  order by tipus_client
  
  select nom_host, count(*)
  from lex.slg_comandes
  group by nom_host
  order by nom_host
  
  select dispositiu, count(*)
  from lex.slg_comandes
  group by dispositiu
  order by dispositiu
  
  select estat, count(*)
  from lex.slg_comandes
  group by estat
  order by estat
  


select id_comanda_item, 
  id_comanda, id_item, id_categoria, 
  quantitat, preu_unitari, preu_subtotal
from lex.slg_comandes_items
limit 1000;

  select count(*), count(distinct id_comanda_item), count(distinct id_comanda)
  from lex.slg_comandes_items

  select id_comanda, id_item, count(*)
  from lex.slg_comandes_items
  group by id_comanda, id_item
  order by count(*) desc
  limit 1000

  select id_item, count(*)
  from lex.slg_comandes_items
  group by id_item
  order by id_item

select id_comanda_detall, 
  id_comanda_item, id_item, id_categoria
from lex.slg_comandes_detalls
limit 1000;

  select count(*), count(distinct id_comanda_detall), count(distinct id_comanda_item)
  from lex.slg_comandes_detalls

  select id_comanda_item, count(*)
  from lex.slg_comandes_detalls
  group by id_comanda_item
  order by count(*) desc

  select id_item, count(*)
  from lex.slg_comandes_detalls
  group by id_item
  order by id_item
  
select id_comanda_detall_lentilla, 
  id_comanda_detall, 
  ojo, curvabase, diametro, cilindro, eje, esfera, esfera_lejos, color, 
  adicion, adicion_dn, adicion_hl, adicion_hml, 
  hidrogel_silicona
from lex.slg_comandes_detalls_lentilles
limit 1000;

  select count(*), count(distinct id_comanda_detall_lentilla), count(distinct id_comanda_detall)
  from lex.slg_comandes_detalls_lentilles

  select ojo, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by ojo

  select curvabase, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by curvabase
  order by curvabase

  select diametro, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by diametro
  order by diametro

  select cilindro, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by cilindro
  order by cilindro
  
  select eje, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by eje
  order by eje
  
  select esfera, esfera_lejos, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by esfera, esfera_lejos
  order by esfera, esfera_lejos

  select adicion, adicion_dn, adicion_hl, adicion_hml, count(*)
  from lex.slg_comandes_detalls_lentilles
  group by adicion, adicion_dn, adicion_hl, adicion_hml
  order by adicion, adicion_dn, adicion_hl, adicion_hml
  
  select cdl.color, cl.nom_es, cdl.num_rows
  from
      (select color, count(*) num_rows
      from lex.slg_comandes_detalls_lentilles
      group by color) cdl
    left join
      lad.slg_items_colors_lentilles cl on cdl.color = cl.id_item_color_lentilla
  order by color

select id_comanda_detall_ullera, 
  id_comanda_detall, 
  id_item_talla, id_item_color
from lex.slg_comandes_detalls_ulleres
limit 1000;

select id_comanda_detall_ullera_graduada, 
  id_comanda_detall, 
  esfera_i, esfera_d, cilindro_i, cilindro_d, eje_i, eje_d, dist_naso_pupilar_i, dist_naso_pupilar_d, 
  vidre, -- reflexant, 
  talla, color
from lex.slg_comandes_detalls_ulleres_graduades
limit 1000;

select id_comanda_detall_ullera_presbicia, 
  id_comanda_detall, 
  esfera_i, esfera_d, cilindro_i, cilindro_d, eje_i, eje_d, dist_naso_pupilar_i, dist_naso_pupilar_d, 
  vidre, -- reflexant, 
  talla, color
from lex.slg_comandes_detalls_ulleres_presbicia
limit 1000;



select id_promocodi, 
  actiu, data_alta, data_inici, data_final, 
  codi, tipus, valor, 
  usos, primera_compra, aplicacio, 
  id_categoria, id_comanda
from lex.slg_promocodis
limit 1000;

-- ---------------------------------------------------

SELECT lex_id, magento_id_lad, magento_id, magento_name, lex_name, lex_packsize, magento_packsize, multiplier
FROM lad_lex.lex_product_map
where lex_packsize = '#N/A';

  select count(*), count(distinct lex_id)
  from lad_lex.lex_product_map2

select lex_packsize, count(*)
from lad_lex.lex_product_map
group by lex_packsize
order by lex_packsize;


select t1.id_item, t2.lex_id, t1.nom_es, t2.magento_id, t2.magento_name, t2.lex_packsize, t2.magento_packsize, t2.multiplier
from
    (select id_item, nom_es
    from lex.slg_items) t1
  left join  
    lad_lex.lex_product_map t2 on t1.id_item = t2.lex_id
-- where t2.lex_id is null -- 355
order by t1.id_item

-- select t1.id_item, t2.lex_id, t1.nom_es, t1.num_rows, t2.magento_id, t2.magento_name, t2.lex_packsize, t2.magento_packsize, t2.multiplier
select t1.id_item, t1.nom_es, t1.num_rows num_ol
from
    (select t1.id_item, t1.nom_es, ol.num_rows
    from
        (select id_item, count(*) num_rows
        from lex.slg_comandes_detalls
        group by id_item) ol
      inner join
        lex.slg_items t1 on ol.id_item = t1.id_item) t1
  left join  
    lad_lex.lex_product_map t2 on t1.id_item = t2.lex_id
where t2.lex_id is null -- 92
-- where t2.magento_id = ''
order by t1.id_item
