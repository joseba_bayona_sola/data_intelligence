
-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------

-- If there multiple accounts with the same email addresses 
	
	-- In SAME website
  SELECT COUNT(*) num_customer_records, COUNT(DISTINCT email) num_email_records
  FROM
    (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
    FROM cfg_usuaris) u;
  
  SELECT email, COUNT(*) num_records
  FROM
    (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
    FROM cfg_usuaris) u
  GROUP BY email  
  ORDER BY num_records DESC;
  
	-- Between DIFFERENT websites 
    -- NOTE: only possible to run if Lentillas a Domicilio - Lenti Express DB in same MySQL instance / need to replace lentidom - lentiexp with DB names

  SELECT u1.email
  FROM
    (SELECT DISTINCT email
    FROM
      (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
      FROM lad.cfg_usuaris) u) u1
  INNER JOIN    
    (SELECT DISTINCT email
    FROM
      (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
      FROM led.cfg_usuaris) u) u2 ON u1.email = u2.email;  

-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------

-- YTD NUM orders + revenue (for all years) 	

  SELECT YEAR(FROM_UNIXTIME(data_alta)) yyyy, COUNT(DISTINCT id_comanda) num_orders, SUM(preu_total_comanda) sum_revenue
  FROM slg_comandes
  WHERE pagat = 1
  GROUP BY YEAR(FROM_UNIXTIME(data_alta))
  ORDER BY YEAR(FROM_UNIXTIME(data_alta));
	
-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------
	
-- YTD + 2018 number of active customers (email_usuari only can run on Lentillas a Domicilio DB)

  SELECT COUNT(DISTINCT id_usuari) num_dist_customer -- , count(distinct email_usuari) num_dist_email
  FROM slg_comandes
  WHERE pagat = 1 AND data_alta > '2018-01-01';

-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------

-- Selecting randomly 20 unique customer and to look if all personal informations are complete
  
  create temporary table random_customers as
    select num_oh cohort, min(id_usuari) id_usuari, count(*) num_customers
    from
      (select id_usuari, count(*) num_oh
      from slg_comandes
      group by id_usuari) c
    where num_oh >= 20
    group by num_oh
    order by num_oh;
    
  create temporary table products as    
    select i.id_item, 
      c.nom_es category_name, p.nom_es provider_name, m.nom_es brand_name, i.nom_es product_name
    from 
        slg_items i
      left join  
        slg_categories c on i.id_categoria = c.id_categoria
      left join  
        slg_proveidors p on i.id_proveidor = p.id_proveidor
      left join
        slg_marques m on i.id_marca = m.id_marca;
    
  -- customer data  
  select c.cohort, c.id_usuari, 
    u.estat customer_status, u.tipus_client customer_type, 
    u.data_alta created_date, -- u.data_modificacio updated_date, 
    u.login, u.password, 

    u.facturacio_mail billing_email, u.facturacio_nom billing_name, u.facturacio_cognom1 billing_last_name1, u.facturacio_cognom2 billing_last_name2, 
    u.facturacio_adreca billing_address, u.facturacio_poblacio billing_city, u.facturacio_provincia billing_region, u.facturacio_codipostal billing_postcode, u.facturacio_pais billing_country, 

    u.enviament_mail shipping_email, u.enviament_nom shipping_name, u.enviament_cognom1 shipping_last_name1, u.enviament_cognom2 shipping_last_name2, 
    u.enviament_adreca shipping_address, u.enviament_poblacio shipping_city, u.enviament_provincia shipping_region, u.enviament_codipostal shipping_postcode, u.enviament_pais shipping_country, 

    u.facturacio_nif nif, 
    u.accepta_mail email_subscription_f
  from 
      random_customers c
    inner join
      cfg_usuaris u on c.id_usuari = u.id_usuari
  order by c.cohort;    
  
  -- orders

  select c.cohort, c.id_usuari, 
    oh.id_comanda, oh.referencia order_no, oh.data_alta order_date, oh.data_factura invoice_date,  
    oh.pagat payment_f, oh.pasarela_pagament payment_type, oh.estat order_status,

    -- oh.email_usuari email,
    oh.facturacio_nom billing_name, oh.facturacio_cognom1 billing_last_name1, oh.facturacio_cognom2 billing_last_name2, 
    oh.facturacio_adreca billing_address, oh.facturacio_poblacio billing_city, oh.facturacio_provincia billing_region, oh.facturacio_codipostal billing_postcode, oh.facturacio_pais billing_country, 
    
    oh.import_productes subtotal, oh.import_enviament shipping, oh.import_descompte discount, oh.preu_total_comanda total
  from 
      random_customers c
    inner join
      slg_comandes oh on c.id_usuari = oh.id_usuari
  order by c.cohort, c.id_usuari, oh.data_alta;     
      
  -- order lines
  select c.cohort, c.id_usuari, 
    oh.id_comanda, oh.referencia order_no, oh.data_alta order_date, oh.data_factura invoice_date, 
  
    -- oh.email_usuari email, 
    oh.import_productes subtotal_oh,
    
    p.id_item, 
    p.category_name, p.provider_name, p.brand_name, p.product_name,

    ol3.ojo eye, 
    ol3.curvabase base_curve, ol3.diametro diameter, ol3.esfera power_1, ol3.esfera_lejos power_2,
    ol3.cilindro cylinder, ol3.eje axis,
    ol3.adicion_dn dominance, ol3.adicion addition, ol3.adicion_hl addition_hl, ol3.adicion_hml addition_hml,
    ol3.color colour,
    
    ol1.quantitat quantity, ol1.preu_unitari pack_price, ol1.preu_subtotal subtotal
    
  from 
      random_customers c
    inner join
      slg_comandes oh on c.id_usuari = oh.id_usuari
    inner join
      slg_comandes_items ol1 on oh.id_comanda = ol1.id_comanda 
    inner join
      products p on ol1.id_item = p.id_item
    inner join
      slg_comandes_detalls ol2 on ol1.id_comanda_item = ol2.id_comanda_item
    left join
      slg_comandes_detalls_lentilles ol3 on ol2.id_comanda_detall = ol3.id_comanda_detall
  order by c.cohort, c.id_usuari, oh.data_alta, ol1.id_item;
 
  
  
  drop table random_customers;
  drop table products;
  
  
  