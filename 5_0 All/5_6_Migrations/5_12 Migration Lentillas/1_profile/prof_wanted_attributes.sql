
-- migrate_customerdata
	-- old_customer_id
	-- email -- account_created
	-- firstname, lastname, street_address, postcode, city, country, telephone
	-- dob
	-- others: unsubscribe - RAF - reminder

-- products
	-- old_product_id
	-- name
	-- product_type - category - manufacturer
	-- available_for_purchae - discontinued

	-- mapping for all products

-- migrate_orderdata
	-- old_order_id
	-- store_id - order_date
	-- customer: old_customer_id, customer_id, email
	-- orderStatusName ??
	-- payment_method - shipping_description - channel - source_code
	-- coupon code information
	-- billing address
	-- shipping address

	-- grand_total - shipping_amount - discount_amount
	-- tax + exc vat values
	-- currency - exchange rate

-- migrate_orderlinedata
	-- old_item_id

	-- product: old_product_id, product_id, name
	-- parameters
	-- pack_size
	-- eye ??
	
	-- quantity
	-- row_total - row_discount