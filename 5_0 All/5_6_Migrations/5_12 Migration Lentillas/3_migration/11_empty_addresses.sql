
create temporary table old_customer_empty_address as
  select old_customer_id, 
    -- email, old_customer_id_prefix, firstname, lastname, 
    --  shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone, 
    old_last_order_date
  from lad_lex.migrate_customerdata
  where CHAR_LENGTH(shipping_street1) = 0
    and old_last_order_date is not null;

select t1.old_customer_id, t1.old_last_order_date, t2.*
from 
  old_customer_empty_address t1
inner join
  lad_lex.migrate_orderdata t2 on t1.old_customer_id = t2.old_customer_id and t1.old_last_order_date = t2.created_at
  
update old_customer_empty_address t1, old_customer_empty_address t2
set
  tshipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone
where t1.old_customer_id = t2.old_customer_id and t1.old_last_order_date = t2.created_at;