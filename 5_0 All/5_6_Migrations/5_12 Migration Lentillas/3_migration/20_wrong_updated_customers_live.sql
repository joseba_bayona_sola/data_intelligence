
select old_customer_id, new_magento_id, migrated,
  email, password, created_at,
  firstname, lastname,
  old_last_order_date, old_customer_lifecycle, 
  vd_last_order_date, vd_customer_lifecycle
from migrate_customerdata  
where new_magento_id < 2977869
  and (vd_customer_lifecycle IS NULL or (vd_customer_lifecycle IN ('LAPSED', 'RNB') AND `old_customer_lifecycle` NOT IN ('LAPSED', 'RNB')))
order by new_magento_id

select t1.old_customer_id, t1.new_magento_id, t1.migrated,

  t1.email, t2.email, t1.created_at, t2.created_at, t2.updated_at,
  t1.firstname, t1.lastname,
  t1.old_last_order_date, t1.old_customer_lifecycle, 
  t1.vd_last_order_date, t1.vd_customer_lifecycle
from 
    migrate_customerdata t1  
  inner join
    customer_entity t2 on t1.new_magento_id = t2.entity_id
where t1.new_magento_id < 2977869
  and (vd_customer_lifecycle IS NULL or (vd_customer_lifecycle IN ('LAPSED', 'RNB') AND `old_customer_lifecycle` NOT IN ('LAPSED', 'RNB')))
order by t1.new_magento_id desc
