USE lad_lex;

DROP TABLE lad_product_map;

CREATE TABLE lad_product_map(
	lad_id			      VARCHAR(255), 
	magento_id	      VARCHAR(255), 
	magento_name	    VARCHAR(255),
	lad_name		      VARCHAR(255),
	lad_packsize	    VARCHAR(255),
	magento_packsize	VARCHAR(255),
	multiplier		    VARCHAR(255),
	lex_id_eq		      VARCHAR(255)
);

DROP TABLE lex_product_map;

CREATE TABLE lex_product_map(
	lex_id			      VARCHAR(255), 
	magento_id_lad		VARCHAR(255), 
	magento_id		    VARCHAR(255), 
	magento_name		  VARCHAR(255),
	lex_name		      VARCHAR(255),
	lex_packsize		  VARCHAR(255),
	magento_packsize	VARCHAR(255),
	multiplier		    VARCHAR(255)
);


DROP TABLE lad_colour_map;

CREATE TABLE lad_colour_map(
	lad_colour			  VARCHAR(255), 
	magento_colour		VARCHAR(255)
);

