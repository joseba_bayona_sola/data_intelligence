
insert into lad_lex.customer_dupl (email, 
  id_usuari_lad, num_oh_lad, max_oh_lad, id_usuari_lex, num_oh_lex, max_oh_lex, id_usuari, source)
  
  select t.email, 
   t.id_usuari_lad, t.num_oh_lad, t.max_oh_lad, t.id_usuari_lex, t.num_oh_lex, t.max_oh_lex, 
   case when (mcd.old_customer_id is not null) then mcd.old_customer_id else t.id_usuari end id_usuari, 
   case 
    when (mcd.old_customer_id is not null and mcd.DomainName = 'lentillasadomicilio.com') then 'lad' 
    when (mcd.old_customer_id is not null and mcd.DomainName = 'lentiexpress.es') then 'lex' 
    else t.source end source
   -- , t.id_usuari, t.source
   -- , mcd.old_customer_id, mcd.DomainName
   -- , cd.id_usuari
  from
    (select u.email, 
      u.id_usuari_lad, c1.num_oh num_oh_lad, c1.max_oh max_oh_lad,
      u.id_usuari_lex + 1000000 id_usuari_lex, c2.num_oh num_oh_lex, c2.max_oh max_oh_lex, 
      case 
        when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh >= c2.max_oh then u.id_usuari_lad
        when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh < c2.max_oh then u.id_usuari_lex + 1000000
        when c1.max_oh is not null and c2.max_oh is null then u.id_usuari_lad
        when c1.max_oh is null and c2.max_oh is not null then u.id_usuari_lex + 1000000   
        when c1.max_oh is null and c2.max_oh is null then u.id_usuari_lad
      end id_usuari, 
      case 
        when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh >= c2.max_oh then 'lad'
        when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh < c2.max_oh then 'lex'
        when c1.max_oh is not null and c2.max_oh is null then 'lad'
        when c1.max_oh is null and c2.max_oh is not null then 'lex'
        when c1.max_oh is null and c2.max_oh is null then 'lad'
      end source    
    from
      (SELECT u1.email, u1.id_usuari id_usuari_lad, u2.id_usuari id_usuari_lex
      FROM
        (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
        FROM lad.cfg_usuaris u) u1
      INNER JOIN    
        (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
        FROM lex.cfg_usuaris u) u2 ON u1.email = u2.email) u 
    left join
      (select id_usuari, count(*) num_oh, max(from_unixtime(data_alta)) max_oh
      from lad.slg_comandes
      group by id_usuari) c1 on u.id_usuari_lad = c1.id_usuari
    left join
      (select id_usuari, count(*) num_oh, max(from_unixtime(data_alta)) max_oh
      from lex.slg_comandes
      group by id_usuari) c2 on u.id_usuari_lex = c2.id_usuari) t
  left join
    lad_lex.customer_dupl cd on t.email = cd.email
  left join
    magento01.migrate_customerdata mcd on t.email = mcd.email
  where cd.email is null -- -- customers that created a new account in other store in delta days
  -- where cd.id_usuari <> t.id_usuari -- customers that placed a new order in other store in delta days
  ;
  
  update lad_lex.customer_dupl
  set id_usuari = 1038382, source = 'lex'
  where email = 'anaga89@hotmail.com';
  
  update lad_lex.customer_dupl
  set id_usuari = 33437, source = 'lad'
  where email = 'luispedraza@gmail.com';
  
  update lad_lex.customer_dupl
  set id_usuari = 1000243, source = 'lex'
  where email = 'mariajosecarbonell@gmail.com';
  
  update lad_lex.customer_dupl
  set id_usuari = 34130, source = 'lad'
  where email = 'mjose.vasallo@gmail.com';
  
  update lad_lex.customer_dupl
  set id_usuari = 30317, source = 'lad'
  where email = 'yddi60@gmail.com';
  
  