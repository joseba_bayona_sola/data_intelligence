use lad_lex;

-- cfg_usuaris_v
drop view lad_lex.cfg_usuaris_v;

create or replace view lad_lex.cfg_usuaris_v as
  select 'lad' source,
    u.id_usuari, from_unixtime(data_alta) data_alta, from_unixtime(data_modificacio) data_modificacio, login, password, 
    enviament_nom, enviament_cognom1, enviament_cognom2, enviament_telefon, 
    enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, 
    facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_telefon, 
    facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, p.code_iso2 facturacio_pais_cod,
    accepta_mail
  from 
      lad.cfg_usuaris u
    left join
      lad_lex.customer_dupl cd on u.id_usuari = cd.id_usuari_lad
    inner join
      lad.cfg_paises p on u.facturacio_pais = p.id_pais
  where cd.source is null or cd.source = 'lad' 
  union
  select 'lex' source,
    u.id_usuari + 1000000, from_unixtime(data_alta) data_alta, null data_modificacio, login, password, 
    enviament_nom, enviament_cognom1, enviament_cognom2, enviament_telefon, 
    enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, 
    facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_telefon, 
    facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, p.code_iso2 facturacio_pais_cod,
    accepta_mail
  from 
      lex.cfg_usuaris u
    left join
      lad_lex.customer_dupl cd on u.id_usuari + 1000000 = cd.id_usuari_lex
    inner join
      lex.cfg_paises p on u.facturacio_pais = p.id_pais
  where cd.source is null or cd.source = 'lex';  

-- slg_comandes_lad

drop view lad_lex.slg_comandes_lad_v;

create or replace view lad_lex.slg_comandes_lad_v as
  select 'lad' source, id_comanda, 
    ifnull(cd.source, 'lad') source_u, ifnull(cd.id_usuari, c.id_usuari) id_usuari, 
    from_unixtime(data_alta) data_alta, referencia, pagat, 
    import_productes, import_enviament, import_contrareemborsament, import_descompte, preu_total_comanda, 
    enviament_nom, enviament_cognom1, enviament_cognom2, enviament_telefon, 
    enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, 
    facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_telefon, 
    facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, p.code_iso2 facturacio_pais_cod, 
    metode_enviament
  from 
      lad.slg_comandes c
    left join  
      lad_lex.customer_dupl cd on c.id_usuari = cd.id_usuari_lad
    inner join
      lad.cfg_paises p on c.facturacio_pais = p.id_pais;  

drop view lad_lex.slg_comandes_lex_v;

create or replace view lad_lex.slg_comandes_lex_v as
  select 'lex' source, id_comanda + 1000000 id_comanda, 
    ifnull(cd.source, 'lex') source_u, ifnull(cd.id_usuari, c.id_usuari + 1000000) id_usuari, 
    from_unixtime(data_alta) data_alta, referencia, pagat, 
    import_productes, import_enviament, import_contrareemborsament, import_descompte, preu_total_comanda, 
    enviament_nom, enviament_cognom1, enviament_cognom2, enviament_telefon, 
    enviament_adreca, enviament_poblacio, enviament_provincia, enviament_codipostal, enviament_pais, 
    facturacio_nom, facturacio_cognom1, facturacio_cognom2, facturacio_telefon, 
    facturacio_adreca, facturacio_poblacio, facturacio_provincia, facturacio_codipostal, facturacio_pais, p.code_iso2 facturacio_pais_cod, 
    metode_enviament
  from 
      lex.slg_comandes c
    left join  
      lad_lex.customer_dupl cd on c.id_usuari + 1000000 = cd.id_usuari_lex
    inner join
      lad.cfg_paises p on c.facturacio_pais = p.id_pais;  


-- slg_comandes_items

drop view lad_lex.slg_comandes_items_lad_v;

create or replace view lad_lex.slg_comandes_items_lad_v as
  select 'lad' source, cd.id_comanda_detall, ci.id_comanda_item, ci.id_comanda, 
    from_unixtime(c.data_alta) data_alta, c.referencia, c.pagat, c.import_productes,
    
    ci.id_item id_item_ol, cd.id_item, i.nom_es, i.magento_id, i.magento_name, i.lad_packsize,
    cdl.ojo, cdl.curvabase, cdl.diametro, cdl.cilindro, cdl.eje, cdl.esfera, cdl.esfera_lejos, cdl.color, cl.nom_es color_str,
    cdl.adicion_dn, 
    case 
      when (cdl.adicion_hl = 1) then 'High'
      when (cdl.adicion_hl = 2) then 'Low'
      when (cdl.adicion_hml = 1) then 'High'
      when (cdl.adicion_hml = 2) then 'Medium'
      when (cdl.adicion_hml = 3) then 'Low'
      else cdl.adicion
    end adicion_str,  
    cdl.adicion, cdl.adicion_hl, cdl.adicion_hml, 
    
    ci.quantitat, ci.preu_unitari, ci.preu_subtotal, cd2.num_rows
 
  from 
      lad.slg_comandes c
    inner join
      lad.slg_comandes_items ci on c.id_comanda = ci.id_comanda
    inner join
      lad.slg_comandes_detalls cd on ci.id_comanda_item = cd.id_comanda_item
    inner join
      (select t1.id_item, t2.lad_id, t1.nom_es, t2.magento_id, t2.magento_name, 
        case when (t2.lad_packsize = 0) then 1 else t2.lad_packsize end lad_packsize, 
        t2.magento_packsize, t2.multiplier
      from
          (select id_item, nom_es
          from lad.slg_items) t1
        left join  
          lad_lex.lad_product_map t2 on t1.id_item = t2.lad_id) i on cd.id_item = i.id_item -- ci.id_item
    inner join         
      lad_lex.slg_comandes_detalls_lad cd2 on ci.id_comanda_item = cd2.id_comanda_item
    left join
      lad.slg_comandes_detalls_lentilles cdl on cd.id_comanda_detall = cdl.id_comanda_detall
    left join
      lad.slg_items_colors_lentilles cl on cdl.color = cl.id_item_color_lentilla;


drop view lad_lex.slg_comandes_items_lex_v;

create or replace view lad_lex.slg_comandes_items_lex_v as
  select 'lex' source, cd.id_comanda_detall + 1000000 id_comanda_detall, ci.id_comanda_item, ci.id_comanda + 1000000 id_comanda, 
    from_unixtime(c.data_alta) data_alta, c.referencia, c.pagat, c.import_productes,
    
    ci.id_item id_item_ol, cd.id_item id_item, i.nom_es, i.magento_id, i.magento_name, i.lex_packsize,
    cdl.ojo, cdl.curvabase, cdl.diametro, cdl.cilindro, cdl.eje, cdl.esfera, cdl.esfera_lejos, cdl.color, cl.nom_es color_str,
    cdl.adicion_dn, 
    case 
      when (cdl.adicion_hl = 1) then 'High'
      when (cdl.adicion_hl = 2) then 'Low'
      when (cdl.adicion_hml = 1) then 'High'
      when (cdl.adicion_hml = 2) then 'Medium'
      when (cdl.adicion_hml = 3) then 'Low'
      else cdl.adicion
    end adicion_str,  
    cdl.adicion, cdl.adicion_hl, cdl.adicion_hml, 
    
    ci.quantitat, ci.preu_unitari, ci.preu_subtotal, cd2.num_rows
    
  from 
      lex.slg_comandes c
    inner join
      lex.slg_comandes_items ci on c.id_comanda = ci.id_comanda
    inner join
      lex.slg_comandes_detalls cd on ci.id_comanda_item = cd.id_comanda_item
    inner join
      (select t1.id_item, t2.lex_id, t1.nom_es, t2.magento_id, t2.magento_name, 
        case when (t2.lex_packsize = 0) then 1 else t2.lex_packsize end lex_packsize, 
        t2.magento_packsize, t2.multiplier
      from
          (select id_item, nom_es
          from lex.slg_items) t1
        left join  
          lad_lex.lex_product_map t2 on t1.id_item = t2.lex_id) i on cd.id_item = i.id_item
    inner join         
      lad_lex.slg_comandes_detalls_lex cd2 on ci.id_comanda_item = cd2.id_comanda_item          
    left join
      lex.slg_comandes_detalls_lentilles cdl on cd.id_comanda_detall = cdl.id_comanda_detall
    left join
      lad.slg_items_colors_lentilles cl on cdl.color = cl.id_item_color_lentilla;


