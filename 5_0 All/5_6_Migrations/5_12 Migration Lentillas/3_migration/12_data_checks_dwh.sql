
drop table #lad_lex_customers

			select old_customer_id, migrated,
				new_magento_id, email, password, created_at, 
				domainID, domainName, old_customer_id_prefix,
				firstname, lastname,
				unsubscribe_newsletter, disable_saved_card, RAF_code
			into #lad_lex_customers
			from openquery(MAGENTO, 
				'select old_customer_id, migrated,
					new_magento_id, email, password, created_at, 
					domainID, domainName, old_customer_id_prefix, 
					firstname, lastname,
					unsubscribe_newsletter, disable_saved_card, RAF_code 
				from magento01.migrate_customerdata')	

select c1.old_customer_id, 
	c1.migrated, c1.domainName,
	c1.new_magento_id, lead(c1.new_magento_id) over (order by new_magento_id) - c1.new_magento_id diff_new_magento_id,
	c1.email, c1.password, 
	c1.firstname, c1.lastname
from #lad_lex_customers c1
order by new_magento_id

select migrated, count(*)
from #lad_lex_customers
group by migrated
order by migrated

select migrated, domainName, count(*)
from #lad_lex_customers
group by migrated, domainName
order by migrated, domainName


select c1.old_customer_id, c2.customer_id_bk,
	c1.migrated, c1.domainName,
	c1.new_magento_id, c1.email, c1.password, c2.password_hash,
	c1.firstname, c1.lastname, c2.first_name, c2.last_name,
	c2.migrate_customer_store, c2.migrate_customer_id

from 
		#lad_lex_customers c1
	left join
		Landing.aux.gen_dim_customer_aud c2 on c1.new_magento_id = c2.customer_id_bk 
where c1.new_magento_id >= 2977869 -- and
	-- c1.migrated = -1
	-- c2.customer_id_bk is null
	-- c1.migrated = 1 and c2.migrate_customer_store is null
	-- c1.migrated = 1 and c2.migrate_customer_store is not null
	-- c1.migrated = 1 and c1.password <> c2.password_hash
	-- c1.migrated = 0
order by c1.new_magento_id desc

	select c1.domainName, c2.migrate_customer_store, count(*)
	from 
			#lad_lex_customers c1
		left join
			Landing.aux.gen_dim_customer_aud c2 on c1.new_magento_id = c2.customer_id_bk 
	where c1.new_magento_id >= 2977869 and c1.migrated = 1
	group by c1.domainName, c2.migrate_customer_store
	order by c1.domainName, c2.migrate_customer_store

	select *
	from Landing.mag.customer_entity_flat_aud
	where entity_id in (2993204, 3017546)

	select *
	from Landing.aux.gen_dim_customer_aud
	where customer_id_bk in (2993204, 3017546)

	select top 1000 *
	from Warehouse.gen.dim_customer
	where customer_id_bk in (2993204, 3017546)

	select top 1000 *
	from Warehouse.gen.dim_customer_scd_v
	where customer_id_bk in (2993204, 3017546)

	select top 100 *
	from Warehouse.dm.fact_customer_signature_dm
	where client_id in (2993204, 3017546)
	

	select *
	from Landing.mag.customer_entity_varchar_aud
	where entity_id in (2993204, 3017546)
		and attribute_id in (532, 534)
	order by attribute_id, entity_id

	-- mysql
	--select *, replace(value, 'LED', 'LEX')
	--from customer_entity_varchar
	--where attribute_id in (532, 534)
	--  and value like 'LED%' -- LAD LED
 --   and entity_id >= 2977869
	--order by entity_id, attribute_id
	--limit 100000
	
	select cv.*, replace(cv.value, 'LED', 'LEX')
	from 
    customer_entity_varchar cv
  inner join
    migrate_customerdata cd on cv.entity_id = cd.new_magento_id
	where cv.attribute_id in (532, 534)
	  and cv.value like 'LED%' -- LAD LED
    -- and cv.entity_id >= 2977869
	order by cv.entity_id, cv.attribute_id
	limit 100000	
	
-- 7seg
update customer_entity_varchar trg, migrate_customerdata src
set trg.value = replace(trg.value, 'LED', 'LEX')
where trg.entity_id = src.new_magento_id
  and trg.attribute_id in (532, 534)
	and trg.value like 'LED%';
	
  
--------------------------------------------------------------------

	select c2.customer_id_bk CLIENT_ID, 
		c2.first_name FIRSTNAME, c2.last_name LASTNAME, 
		c2.customer_email EMAIL, cdm.segment_3 SEGMENT_3, cdm.segment_geog
		, cdm.segment_lifecycle
	from 
			#lad_lex_customers c1
		inner join 
			Warehouse.gen.dim_customer c2 on c1.new_magento_id = c2.customer_id_bk 
		inner join
			Warehouse.dm.fact_customer_signature_dm cdm on c2.idCustomer_sk = cdm.idCustomer_sk_fk
	where c1.new_magento_id >= 2977869 and c1.migrated = 1
		and c1.domainName = 'lentiexpress.es' -- lentillasadomicilio.com // lentiexpress.es
		-- and cdm.segment_lifecycle  <> 'RNB'
		-- and c2.customer_email = 'a_m_mancilla@hotmail.com'
