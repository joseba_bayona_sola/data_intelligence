
DROP TABLE lad_lex.slg_comandes_detalls_lad;

CREATE TABLE lad_lex.slg_comandes_detalls_lad(
  id_comanda_item         int, 
  num_rows                int);

alter table lad_lex.slg_comandes_detalls_lad add primary key (id_comanda_item);

insert into lad_lex.slg_comandes_detalls_lad (id_comanda_item, num_rows)
  select id_comanda_item, count(*) num_rows
  from lad.slg_comandes_detalls
  group by id_comanda_item;
  
---   

DROP TABLE lad_lex.slg_comandes_detalls_lex;

CREATE TABLE lad_lex.slg_comandes_detalls_lex(
  id_comanda_item         int, 
  num_rows                int);

alter table lad_lex.slg_comandes_detalls_lex add primary key (id_comanda_item);

insert into lad_lex.slg_comandes_detalls_lex (id_comanda_item, num_rows)
  select id_comanda_item, count(*) num_rows
  from lex.slg_comandes_detalls
  group by id_comanda_item;
  
---   

DROP TABLE lad_lex.migrate_orderdata_last;

CREATE TABLE lad_lex.migrate_orderdata_last(
  old_order_id         int);  
  
alter table lad_lex.migrate_orderdata_last add primary key (old_order_id);  