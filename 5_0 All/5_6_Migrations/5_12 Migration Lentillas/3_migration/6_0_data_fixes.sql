use lad_lex;

----------------------------------------------------------  
-- migrate_customerdata

  -- remove invalid emails: 2 rows 
  DELETE FROM migrate_customerdata WHERE `email` NOT REGEXP '^[^@]+@[^@]+\.[^@]{2,}$';

  -- get rid of duplicates and relink the old records: NO NEEDED (already done)
    SELECT email, MAX(old_customer_id) customer_id 
    FROM lad_lex.`migrate_customerdata`
    GROUP BY email
    HAVING COUNT(*) > 1;  
    
 -- fix customer address when fields are missnig
  -- 1409 rows 
  UPDATE lad_lex.migrate_customerdata
  SET billing_street1 = shipping_street1 
  WHERE CHAR_LENGTH(billing_street1) = 0;
  
  -- 2117 rows
  UPDATE lad_lex.migrate_customerdata
  SET billing_city = shipping_city
  WHERE CHAR_LENGTH(billing_city) = 0;
  
  -- 764 rows
  UPDATE lad_lex.migrate_customerdata
  SET shipping_street1 = billing_street1
  WHERE CHAR_LENGTH(shipping_street1) = 0;

  -- 1278 rows
  UPDATE lad_lex.migrate_customerdata
  SET shipping_city = billing_city
  WHERE CHAR_LENGTH(shipping_city) = 0;        

  ----------------------

  UPDATE lad_lex.migrate_customerdata
  set billing_country_id = 'ES'
  WHERE CHAR_LENGTH(billing_country_id) = 0 or billing_country_id is null;  

  UPDATE lad_lex.migrate_customerdata
  set shipping_country_id = 'ES'
  WHERE CHAR_LENGTH(shipping_country_id) = 0 or shipping_country_id is null;  
  
----------------------------------------------------------  
-- migrate_orderdata

-- delete OH - OL records attahced to customers deleted

  DELETE FROM migrate_orderdata WHERE old_customer_id NOT IN (SELECT old_customer_id FROM migrate_customerdata);
  DELETE FROM migrate_orderlinedata WHERE old_order_id NOT IN (SELECT old_order_id FROM migrate_orderdata);  

  -- OH with not OL ??
  
-- fix totals  

-- fix customer address when fields are missnig 

  UPDATE lad_lex.migrate_orderdata
  SET billing_street1 = shipping_street1 
  WHERE CHAR_LENGTH(billing_street1) = 0;
  
  UPDATE lad_lex.migrate_orderdata
  SET billing_city = shipping_city
  WHERE CHAR_LENGTH(billing_city) = 0;
  
  UPDATE lad_lex.migrate_orderdata
  SET shipping_street1 = billing_street1
  WHERE CHAR_LENGTH(shipping_street1) = 0;

  UPDATE lad_lex.migrate_orderdata
  SET shipping_city = billing_city
  WHERE CHAR_LENGTH(shipping_city) = 0;    

-- update shipping description when missing


----------------------------------------------------------  
-- migrate_orderlinedata  

  -- force products with bad packsizes to be a non matched product
  
  -- update colour to match vd lables: DONE
  
  -- fixes for products that will never be posible to map
  
  -- params, params_hash
  
  UPDATE `migrate_orderlinedata`
  SET params = IFNULL(
  	CONCAT(
      CASE WHEN `eye` IS NOT NULL AND `eye` != '' THEN CONCAT(' Eye:', CAST(eye AS CHAR CHARACTER SET utf8)) ELSE '' END,
  	  CASE WHEN `add` IS NOT NULL AND `add` != '' THEN CONCAT(' Addition:', `add`) ELSE '' END,
  	  CASE WHEN `axis` IS NOT NULL AND `axis` != '' THEN CONCAT(' Axis:', `axis`) ELSE '' END,
  	  CASE WHEN `base_curve` IS NOT NULL AND `base_curve` != '' THEN CONCAT(' Base Curve:', `base_curve`) ELSE '' END,
  	  CASE WHEN `color` IS NOT NULL AND `color` != '' THEN CONCAT(' Colour:', `color`) ELSE '' END,
  	  CASE WHEN `cylinder` IS NOT NULL AND `cylinder` != '' THEN CONCAT(' Cylinder:', `cylinder`) ELSE '' END,
  	  CASE WHEN `diameter` IS NOT NULL AND `diameter` != '' THEN CONCAT(' Diameter:', `diameter`) ELSE '' END,
  	  CASE WHEN `dominant` IS NOT NULL AND `dominant` != '' THEN CONCAT(' Dominant:', `dominant`) ELSE '' END,
  	  CASE WHEN `power` IS NOT NULL AND `power` != '' THEN CONCAT(' Power:', `power`) ELSE '' END), '');  
  
UPDATE `migrate_orderlinedata`
SET params_hash =
  MD5(CONCAT(IFNULL(`product_id`, ''), IFNULL(CAST(eye AS CHAR CHARACTER SET utf8), ''), 
    IFNULL(`power`, ''), IFNULL(`base_curve`, ''), IFNULL(`diameter`, ''), IFNULL(`add`, ''), IFNULL(`axis`, ''), IFNULL(`cylinder`, ''), IFNULL(`dominant`, ''), IFNULL(`color`, ''),
    IFNULL(TRIM(
      CONCAT(
        CASE WHEN `eye` IS NOT NULL AND `eye` != '' THEN CONCAT(' Eye:', CAST(eye AS CHAR CHARACTER SET utf8)) ELSE '' END,
        CASE WHEN `add` IS NOT NULL AND `add` != '' THEN CONCAT(' Addition:', `add`) ELSE '' END,
        CASE WHEN `axis` IS NOT NULL AND `axis` != '' THEN CONCAT(' Axis:', `axis`) ELSE '' END,
        CASE WHEN `base_curve` IS NOT NULL AND `base_curve` != '' THEN CONCAT(' Base Curve:', `base_curve`) ELSE '' END,
        CASE WHEN `color` IS NOT NULL AND `color` != '' THEN CONCAT(' Colour:', `color`) ELSE '' END,
        CASE WHEN `cylinder` IS NOT NULL AND `cylinder` != '' THEN CONCAT(' Cylinder:', `cylinder`) ELSE '' END,
        CASE WHEN `diameter` IS NOT NULL AND `diameter` != '' THEN CONCAT(' Diameter:', `diameter`) ELSE '' END,
        CASE WHEN `dominant` IS NOT NULL AND `dominant` != '' THEN CONCAT(' Dominant:', `dominant`) ELSE '' END,
        CASE WHEN `power` IS NOT NULL AND `power` != '' THEN CONCAT(' Power:', `power`) ELSE '' END)) , '')));  
  
  -- insert into migrate_orderlinedata_params
  delete from migrate_orderlinedata_params
  
  INSERT IGNORE INTO migrate_orderlinedata_params (
    `product_id`, productname, `eye`,
    `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`,	`dominant`, `color`,
    `params`, params_hash)

    SELECT DISTINCT
      `product_id`, productname, `eye`,
      `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`, `dominant`, `color`,
      `params`, params_hash
    FROM migrate_orderlinedata t1;
    
    
    
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------

-- migrate_customerdata_address
delete from migrate_customerdata_address;

insert into migrate_customerdata_address (old_address_id, old_customer_id, 
  created_at, updated_at, 
  firstname, lastname, company, 
  street1, street2, city, region, region_id, postcode, country_id, telephone)   
   
  select old_customer_id old_address_id, old_customer_id, 
    created_at, created_at updated_at, 
    firstname, lastname, ' ' company, 
    shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone
  from migrate_customerdata;    
  