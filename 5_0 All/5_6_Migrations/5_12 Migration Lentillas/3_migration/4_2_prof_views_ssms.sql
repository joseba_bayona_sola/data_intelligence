select 
	count(*) over (partition by id_comanda_item) num_rep1, 
	count(*) over (partition by id_comanda, id_item) num_rep2, 
	*
from openquery(MAGENTO_PROV, 
	'select *
	from lad_lex.slg_comandes_items_lad_v')
-- order by num_rep2 desc, id_comanda, id_item	
order by num_rep1 desc, id_comanda

select id_item, nom_es, count(*)
from
	(select 
		count(*) over (partition by id_comanda_item) num_rep1, 
		count(*) over (partition by id_comanda, id_item) num_rep2, 
		*
	from openquery(MAGENTO_PROV, 
		'select *
		from lad_lex.slg_comandes_items_lad_v')) t
where num_rep1 > 1
group by id_item, nom_es
order by id_item, nom_es

