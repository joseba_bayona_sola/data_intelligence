
select *
from lad_lex.cfg_usuaris_v
where password in ('ecommingfirstloggin2013', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6fa2e8a145ceecd246a55757967ce8f57a6a61ec', '40b21e45c78ab5cf51e7931a80a5a2af')
order by id_usuari;

  select count(*), count(distinct login)
  from lad_lex.cfg_usuaris_v;
  
  select source, count(*), min(id_usuari), max(id_usuari), min(data_alta), max(data_alta)
  from lad_lex.cfg_usuaris_v
  -- where password in ('ecommingfirstloggin2013', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6fa2e8a145ceecd246a55757967ce8f57a6a61ec', '40b21e45c78ab5cf51e7931a80a5a2af')
  group by source
  order by source;
  
  select password, count(*)
  from lad_lex.cfg_usuaris_v
  group by password
  order by count(*) desc;
  
-- -----------------------------------------------------  
-- -----------------------------------------------------

select *
from lad_lex.slg_comandes_lad_v
order by id_usuari, data_alta
limit 1000;

  select count(*)
  from lad_lex.slg_comandes_lad_v;  
  
  select source, source_u, count(*), min(id_usuari), max(id_usuari), min(id_comanda), max(id_comanda)
  from lad_lex.slg_comandes_lad_v  
  group by source, source_u
  order by source, source_u;
  
  select pagat, count(*)
  from lad_lex.slg_comandes_lad_v  
  group by pagat
  order by pagat;  
  
  select metode_enviament, count(*)
  from lad_lex.slg_comandes_lad_v  
  group by metode_enviament
  order by metode_enviament;  
  
select *
from lad_lex.slg_comandes_lex_v
order by id_usuari, data_alta
limit 1000;

  select count(*)
  from lad_lex.slg_comandes_lex_v;  
  
  select source, source_u, count(*), min(id_usuari), max(id_usuari), min(id_comanda), max(id_comanda)
  from lad_lex.slg_comandes_lex_v  
  group by source, source_u
  order by source, source_u;
  
  select pagat, count(*)
  from lad_lex.slg_comandes_lex_v  
  group by pagat
  order by pagat;  

  select metode_enviament, count(*)
  from lad_lex.slg_comandes_lex_v  
  group by metode_enviament
  order by metode_enviament;    
-- -----------------------------------------------------  
-- -----------------------------------------------------

select *
from lad_lex.slg_comandes_items_lad_v
-- where magento_id = 1084
where id_item_ol = 182
-- where id_comanda_item = 179486
order by id_item_ol, id_comanda, id_comanda_item, id_comanda_detall
limit 10000;

  select count(*)
  from lad_lex.slg_comandes_items_lad_v
  where pagat = 1;    
  
  select magento_id, id_item_ol, id_item, nom_es, lad_packsize, num_rows, count(*)
  from lad_lex.slg_comandes_items_lad_v
  -- where id_item_ol <> id_item
  -- group by magento_id, id_item, id_item2, nom_es, lad_packsize;      
  group by id_item_ol, magento_id, id_item, nom_es, lad_packsize, num_rows;      
  
  select adicion_dn, adicion_str, count(*)
  from lad_lex.slg_comandes_items_lad_v
  group by adicion_dn, adicion_str
  order by adicion_dn, adicion_str  

select *
from lad_lex.slg_comandes_items_lex_v
-- where magento_id = 1084
where id_item_ol = 348
-- where id_comanda_item = 179486
order by id_item_ol, id_comanda, id_comanda_item, id_comanda_detall
limit 10000;

  select count(*)
  from lad_lex.slg_comandes_items_lex_v
  where pagat = 1;    
  
  select magento_id, id_item_ol, id_item, nom_es, lex_packsize, num_rows, count(*)
  from lad_lex.slg_comandes_items_lex_v
  -- where id_item_ol <> id_item
  -- group by magento_id, id_item, id_item2, nom_es, lad_packsize;      
  group by id_item_ol, magento_id, id_item, nom_es, lex_packsize, num_rows;      
  
  select adicion_dn, adicion_str, count(*)
  from lad_lex.slg_comandes_items_lex_v
  group by adicion_dn, adicion_str
  order by adicion_dn, adicion_str