
DROP TABLE lad_lex.customer_dupl;

CREATE TABLE lad_lex.customer_dupl(
	email			      VARCHAR(255), 
	id_usuari_lad		int, 
	num_oh_lad	    int,
	max_oh_lad		  datetime,
	id_usuari_lex		int, 
	num_oh_lex	    int,
	max_oh_lex		  datetime,
	id_usuari		    int, 
  source          varchar(10)
);

insert into lad_lex.customer_dupl (email, 
  id_usuari_lad, num_oh_lad, max_oh_lad, id_usuari_lex, num_oh_lex, max_oh_lex, id_usuari, source)
  
  select u.email, 
    u.id_usuari_lad, c1.num_oh num_oh_lad, c1.max_oh max_oh_lad,
    u.id_usuari_lex + 1000000, c2.num_oh num_oh_lex, c2.max_oh max_oh_lex, 
    case 
      when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh >= c2.max_oh then u.id_usuari_lad
      when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh < c2.max_oh then u.id_usuari_lex + 1000000
      when c1.max_oh is not null and c2.max_oh is null then u.id_usuari_lad
      when c1.max_oh is null and c2.max_oh is not null then u.id_usuari_lex + 1000000   
      when c1.max_oh is null and c2.max_oh is null then u.id_usuari_lad
    end id_usuari, 
    case 
      when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh >= c2.max_oh then 'lad'
      when c1.max_oh is not null and c2.max_oh is not null and c1.max_oh < c2.max_oh then 'lex'
      when c1.max_oh is not null and c2.max_oh is null then 'lad'
      when c1.max_oh is null and c2.max_oh is not null then 'lex'
      when c1.max_oh is null and c2.max_oh is null then 'lad'
    end source    
  from
    (SELECT u1.email, u1.id_usuari id_usuari_lad, u2.id_usuari id_usuari_lex
    FROM
      (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
      FROM lad.cfg_usuaris u) u1
    INNER JOIN    
      (SELECT id_usuari, login email -- login / enviament_mail / facturacio_mail
      FROM lex.cfg_usuaris u) u2 ON u1.email = u2.email) u 
  left join
    (select id_usuari, count(*) num_oh, max(from_unixtime(data_alta)) max_oh
    from lad.slg_comandes
    group by id_usuari) c1 on u.id_usuari_lad = c1.id_usuari
  left join
    (select id_usuari, count(*) num_oh, max(from_unixtime(data_alta)) max_oh
    from lex.slg_comandes
    group by id_usuari) c2 on u.id_usuari_lex = c2.id_usuari;


