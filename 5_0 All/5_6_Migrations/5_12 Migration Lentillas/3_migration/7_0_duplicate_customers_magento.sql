
use lad_lex;

-- update overlap customers
UPDATE magento01.customer_entity t1, migrate_customerdata t2
SET t2.new_magento_id = t1.entity_id
WHERE t1.email = t2.email -- AND t1.website_group_id = t2.website_group_id;
  -- and t1.entity_id <= 2972798 -- applied in first migration when preparing for Live because Staging already had LAD-LEX customers already
  ;

create temporary table old_customer_oh as
  select t2.old_customer_id, max(oh.created_at) last_order_date
  from 
      migrate_customerdata t2 
    inner join
      migrate_orderdata oh on t2.old_customer_id = oh.old_customer_id
  group by t2.old_customer_id;

-- update old_last_order_date, old_customer_lifecycle
UPDATE migrate_customerdata t1,
  old_customer_oh t2

SET t1.`old_last_order_date` = t2.last_order_date,
    t1.`old_customer_lifecycle` = CASE
      WHEN t2.last_order_date IS NULL THEN 'RNB'
      WHEN TIMESTAMPDIFF(MONTH, t2.last_order_date, NOW()) > 15 THEN 'LAPSED'
      ELSE 'ACTIVE' END
WHERE t1.`old_customer_id` = t2.old_customer_id;

update migrate_customerdata
set old_customer_lifecycle = 'RNB'
where old_customer_lifecycle is null;

-- update hist_last_order_date, hist_customer_lifecycle: NO NEEDED

-- update magento_last_order_date, magento_customer_lifecycle
UPDATE migrate_customerdata t1,
  (select t1.entity_id, max(oh.created_at) last_order_date
  from 
      magento01.customer_entity t1 
    inner join  
      migrate_customerdata t2 on t1.entity_id = t2.new_magento_id -- t1.email = t2.email
    left join
      magento01.sales_flat_order oh on t1.entity_id = oh.customer_id
  -- where status <> 'archived'
 --  where oh.entity_id <= 10471074 -- applied in first migration when preparing for Live because Staging already had LAD-LEX orders already
  group by t1.entity_id) t2

SET t1.`magento_last_order_date` = t2.last_order_date,
    t1.`magento_customer_lifecycle` = CASE
      WHEN t2.last_order_date IS NULL THEN 'RNB'
      WHEN TIMESTAMPDIFF(MONTH, t2.last_order_date, NOW()) > 15 THEN 'LAPSED'
      ELSE 'ACTIVE' END
WHERE t1.`new_magento_id` = t2.entity_id;

-- update vd_last_order_date, vd_customer_lifecycle
UPDATE `migrate_customerdata` t1
SET 
  `vd_last_order_date` = magento_last_order_date,
  vd_customer_lifecycle = magento_customer_lifecycle
where new_magento_id <> 0;


--  active vd customer should be left alone
UPDATE `migrate_customerdata` t1
SET migrated = 1
WHERE vd_customer_lifecycle IS NOT NULL AND vd_customer_lifecycle IN ('ACTIVE');

-- lapsed in both then keep vd
UPDATE `migrate_customerdata` t1
SET migrated = 1
WHERE vd_customer_lifecycle IS NOT NULL AND vd_customer_lifecycle IN ('LAPSED', 'RNB') AND `old_customer_lifecycle` IN ('LAPSED', 'RNB');

