
use lad_lex;

DROP TABLE `migrate_customerdata_bck2`;

CREATE TABLE `migrate_customerdata_bck2` (
  `old_customer_id` 			BIGINT(20) NOT NULL DEFAULT '0',
  `new_magento_id` 			  INT(11) NOT NULL DEFAULT '0',
  `new_magento_id_bck` 		INT(11) NOT NULL DEFAULT '0',
  `migrated` 				      INT(11) NOT NULL DEFAULT '0',
  `email` 				        VARCHAR(50) DEFAULT NULL,
  `password` 				      VARCHAR(255) DEFAULT NULL,
  `created_at` 				    DATETIME DEFAULT NULL,
  `store_id` 				      INT(11) DEFAULT NULL,
  `DomainID` 				      VARCHAR(3) DEFAULT NULL,
  `DomainName` 				    VARCHAR(39) DEFAULT NULL,
  `website_group_id` 			INT(11) DEFAULT NULL,
  old_customer_id_prefix 	VARCHAR(50) DEFAULT NULL,
  
  `firstname` 				    VARCHAR(255) DEFAULT NULL,
  `lastname` 				      VARCHAR(255) DEFAULT NULL,
  `billing_company` 			VARCHAR(500) DEFAULT NULL,
  `billing_prefix` 			  VARCHAR(255) DEFAULT NULL,
  `billing_firstname` 		VARCHAR(500) DEFAULT NULL,
  `billing_lastname` 			VARCHAR(500) DEFAULT NULL,  
  `billing_street1` 			VARCHAR(500) DEFAULT NULL,
  `billing_street2` 			VARCHAR(500) DEFAULT NULL,
  `billing_city` 			    VARCHAR(500) DEFAULT NULL,
  `billing_region` 			  VARCHAR(500) DEFAULT NULL,
  `billing_region_id` 		int(11) DEFAULT NULL, -- BINARY(0) DEFAULT NULL,
  `billing_postcode` 			VARCHAR(500) DEFAULT NULL,
  `billing_country_id` 		VARCHAR(500) DEFAULT NULL,
  `billing_telephone`			VARCHAR(500) DEFAULT NULL,
  `shipping_company` 			VARCHAR(500) DEFAULT NULL,
  `shipping_prefix` 			VARBINARY(255) DEFAULT NULL,
  `shipping_firstname` 		VARCHAR(500) DEFAULT NULL,
  `shipping_lastname` 		VARCHAR(500) DEFAULT NULL,
  `shipping_street1` 			VARCHAR(500) DEFAULT NULL,  
  `shipping_street2` 			VARCHAR(500) DEFAULT NULL,
  `shipping_city` 			  VARCHAR(500) DEFAULT NULL,
  `shipping_region` 			VARCHAR(500) DEFAULT NULL,
  `shipping_region_id` 		int(11) DEFAULT NULL, -- BINARY(0) DEFAULT NULL,
  `shipping_postcode` 		VARCHAR(500) DEFAULT NULL,
  `shipping_country_id` 	VARCHAR(500) DEFAULT NULL,
  `shipping_telephone` 		VARCHAR(500) DEFAULT NULL,
  `cus_phone` 				    VARCHAR(40) DEFAULT NULL,
  `default_billing` 			VARCHAR(255) DEFAULT NULL,
  `default_shipping` 			VARCHAR(255) DEFAULT NULL,  

  `unsubscribe_newsletter` INT(11) DEFAULT NULL,  
  `disable_saved_card` 		INT(11) DEFAULT NULL,
  `RAF_code` 				      VARCHAR(255) NOT NULL DEFAULT '',
  `is_registered` 			  TINYINT(4) DEFAULT NULL,
  `priority` 				      INT(11) NOT NULL DEFAULT '0',  

  `mage_last_order_date` 		DATETIME DEFAULT NULL,
  `import_last_order_date` 	DATETIME DEFAULT NULL,
  `old_last_order_date` 		DATETIME DEFAULT NULL,
  `old_customer_lifecycle` 	VARCHAR(255) DEFAULT NULL,
  `hist_last_order_date` 		DATETIME DEFAULT NULL,
  `hist_customer_lifecycle` VARCHAR(255) DEFAULT NULL,
  `magento_last_order_date` DATETIME DEFAULT NULL,
  `magento_customer_lifecycle` 		VARCHAR(255) DEFAULT NULL,
  `vd_last_order_date` 			DATETIME DEFAULT NULL,
  `vd_customer_lifecycle` 	VARCHAR(255) DEFAULT NULL,
  
  `store_credit_amount` 		DECIMAL(12,2) DEFAULT NULL,
  `store_credit_comment` 		VARCHAR(255) DEFAULT NULL,	
  `target_store_credit_amount` 		DECIMAL(12,2) NOT NULL DEFAULT '0.00',
  
  PRIMARY KEY (`old_customer_id`),
  KEY `Email` (`email`),
  KEY `new_magento_id` (`new_magento_id`),
  KEY `website_group_id` (`website_group_id`),
  KEY `migrated` (`migrated`),
  KEY `default_billing` (`default_billing`),
  KEY `default_shipping` (`default_shipping`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------


DROP TABLE IF EXISTS `migrate_orderdata_bck2`;

CREATE TABLE `migrate_orderdata_bck2` (
  `old_order_id` 		        VARCHAR(255) NOT NULL DEFAULT '',
  `magento_order_id` 		    INT(11) NOT NULL DEFAULT '0',
  `new_increment_id` 		    VARCHAR(255) NOT NULL DEFAULT '0',
  `created_at` 			        DATETIME DEFAULT NULL,
  `store_id` 			          INT(11) NOT NULL DEFAULT '0',
  `website_name` 		        VARCHAR(50) DEFAULT NULL, -- VARBINARY(20) DEFAULT NULL,
  `DomainID` 			          VARCHAR(10) DEFAULT NULL,
  `DomainName` 			        VARCHAR(30) DEFAULT NULL,
  `email` 			            VARCHAR(255) DEFAULT NULL,
  `old_customer_id` 		    VARCHAR(255) DEFAULT NULL,
  `magento_customer_id` 	  INT(11) NOT NULL DEFAULT '0',
  `migrated` 			          INT(11) NOT NULL DEFAULT '0',
  
  `billing_company` 		    VARCHAR(255) DEFAULT NULL,
  `billing_prefix` 		      VARCHAR(255) DEFAULT NULL,
  `billing_firstname` 		  VARCHAR(255) DEFAULT NULL,
  `billing_lastname` 		    VARCHAR(255) DEFAULT NULL,  
  `billing_street1` 		    VARCHAR(255) DEFAULT NULL,
  `billing_street2` 		    VARCHAR(255) DEFAULT NULL,
  `billing_city` 		        VARCHAR(255) DEFAULT NULL,
  `billing_region` 		      VARCHAR(255) DEFAULT NULL,
  `billing_region_id` 		  int(11) DEFAULT NULL, -- VARCHAR(255) DEFAULT NULL,
  `billing_postcode` 		    VARCHAR(255) DEFAULT NULL,
  `billing_country` 		    VARCHAR(255) DEFAULT NULL,
  `billing_telephone` 		  VARCHAR(255) DEFAULT NULL,
  `shipping_company` 		    VARCHAR(255) DEFAULT NULL,
  `shipping_prefix` 		    VARCHAR(25) DEFAULT NULL,
  `shipping_firstname` 		  VARCHAR(255) DEFAULT NULL,
  `shipping_lastname` 		  VARCHAR(255) DEFAULT NULL,  
  `shipping_street1` 		    VARCHAR(255) DEFAULT NULL,
  `shipping_street2` 		    VARCHAR(255) DEFAULT NULL,
  `shipping_city` 		      VARCHAR(255) DEFAULT NULL,
  `shipping_region` 		    VARCHAR(255) DEFAULT NULL,
  `shipping_region_id` 		  int(11) DEFAULT NULL, -- VARCHAR(255) DEFAULT NULL,
  `shipping_postcode` 		  VARCHAR(255) DEFAULT NULL,
  `shipping_country` 		    VARCHAR(255) DEFAULT NULL,
  `shipping_telephone` 		  VARCHAR(255) DEFAULT NULL,
  `shipping_description` 	  VARCHAR(255) DEFAULT NULL,
  base_shipping_method 		  VARCHAR(255) DEFAULT NULL,

  `base_shipping_amount` 	  DECIMAL(8,2) DEFAULT NULL,
  `base_grand_total` 		    DECIMAL(8,2) DEFAULT NULL,
  `base_discount_amount` 	  DECIMAL(6,2) DEFAULT NULL,
  `order_currency` 		      VARCHAR(3) NOT NULL DEFAULT '0',
      	
  `Comment` 			          VARCHAR(500) DEFAULT NULL,
  `priority` 			          INT(11) NOT NULL DEFAULT '0',
  `reminder_mobile` 		    VARCHAR(50) DEFAULT NULL,
  `reminder_date` 		      VARCHAR(255) DEFAULT NULL,
  `reminder_type` 		      VARCHAR(255) DEFAULT 'none',
  `increment_id` 		        VARCHAR(255) NOT NULL,
  base_order_id  		        VARCHAR(255),
  PRIMARY KEY (`old_order_id`),
  KEY `old_customer_id` (`old_customer_id`),
  KEY `magento_order_id` (`magento_order_id`),
  KEY `magento_customer_id` (`magento_customer_id`),
  KEY `increment_id` (`increment_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;

-- -----------------------------------------------

DROP TABLE `migrate_orderlinedata_bck2`;

CREATE TABLE `migrate_orderlinedata_bck2` (
  `old_item_id` 		        VARCHAR(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `old_order_id` 		        VARCHAR(255) CHARACTER SET latin1 NOT NULL,
  `magento_order_id` 		    INT(11) DEFAULT NULL,
  `store_id` 			          INT(11) DEFAULT NULL,
  
  `migrated` 			          INT(11) NOT NULL DEFAULT '0',
  `priority` 			          BIGINT(20) NOT NULL,
  `debug` 			            TEXT CHARACTER SET latin1,
  `params_hash` 		        VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL,
    
  `old_product_id` 		      VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL,
  `product_id` 			        INT(4) DEFAULT '0',
  `ProductCode` 		        VARCHAR(100) CHARACTER SET utf8 DEFAULT NULL,
  `ProductName` 		        VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL,
  `ProductCodeFull` 		    VARCHAR(25) CHARACTER SET utf8 DEFAULT '0',  
  `packsize` 			          INT(11) DEFAULT NULL,

  `eye` 			              VARCHAR(10) CHARACTER SET utf8 DEFAULT NULL,
  `base_curve` 			        VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,
  `diameter` 			          VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,
  `power` 			            VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL, 
  `cylinder` 			          VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,
  `axis` 			              VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,
  `add` 			              VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,  
  `dominant` 			          VARCHAR(20) CHARACTER SET latin1 DEFAULT NULL,
  `color` 			            VARCHAR(20) CHARACTER SET utf8 DEFAULT NULL,
  `params` 			            VARCHAR(255) CHARACTER SET latin1 DEFAULT NULL,
  
  `quote_item_id` 		      INT(11) DEFAULT NULL,  
  `quote_sku` 			        VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL,
  `quote_is_lens` 		      INT(11) DEFAULT NULL,
  `quote_data_built` 		    INT(11) NOT NULL DEFAULT '0',
  `quote_opt` 			        TEXT CHARACTER SET utf8,  

  `Quantity` 			          INT(11) DEFAULT NULL,
  `row_total` 			        DECIMAL(12,4) DEFAULT NULL,

  PRIMARY KEY (`old_item_id`),
  KEY `old_order_id` (`old_order_id`),
  KEY `migrated` (`migrated`),
  KEY `product_id` (`product_id`),
  KEY `magento_order_id` (`magento_order_id`),
  KEY `quote_data_built` (`quote_data_built`),
  KEY `priority` (`priority`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4;