
-- Landing.aux.sales_dim_order_header
create table DW_GetLenses_jbs.dbo.sales_dim_order_header_ladlex(
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	idCalendarOrderDate				int NOT NULL, 
	idTimeOrderDate					varchar(10) NOT NULL,
	order_date						datetime NOT NULL,
	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime,

	store_id_bk						int NOT NULL,
	market_id_bk					int,
	customer_id_bk					int NOT NULL,
	shipping_address_id				int,
	billing_address_id				int,
	country_id_shipping_bk			char(2), 
	region_id_shipping_bk			int,
	postcode_shipping				varchar(255), 
	country_id_billing_bk			char(2),
	postcode_billing				varchar(255), 
	region_id_billing_bk			int,
	customer_unsubscribe_name_bk	varchar(10),

	order_stage_name_bk				varchar(20) NOT NULL,
	order_status_name_bk			varchar(20) NOT NULL,
	adjustment_order				char(1),
	order_type_name_bk				varchar(20),
	status_bk						varchar(32),
	payment_method_name_bk			varchar(20),
	cc_type_name_bk					varchar(50),
	payment_id						int,
	payment_method_code				varchar(255),
	shipping_description_bk			varchar(255),
	shipping_description_code		varchar(255),
	telesales_admin_username_bk		varchar(40), 
	prescription_method_name_bk		varchar(50),
	reminder_type_name_bk			varchar(50),
	reminder_period_bk				int, 
	reminder_date					datetime,
	reminder_sent					int,
	reorder_f						char(1) NOT NULL,
	reorder_date					datetime,
	reorder_profile_id				int,
	automatic_reorder				int,
	order_source					char(1),
	proforma						char(1),
	platform						char(1),
	aura_product_p					decimal(12, 4),
	product_type_oh_bk				varchar(50),
	order_qty_time					int,
	order_qty_time_cl				int,  
	order_qty_time_ncl				int,
	num_diff_product_type_oh		int,
	referafriend_code				varchar(10),
	referafriend_referer			int,
	postoptics_auto_verification	varchar(255),
	presc_verification_method		varchar(255),
	warehouse_approved_time			datetime,

	channel_name_bk					varchar(50), 
	marketing_channel_name_bk		varchar(50), 
	publisher_id_bk					int, 
	affilCode						varchar(255),
	ga_medium						varchar(100),
	ga_source						varchar(100),
	ga_channel						varchar(100), 
	telesales_f						char(1), 
	sms_auto_f						char(1),
	mutuelles_f						char(1),
	coupon_id_bk					int,
	coupon_code						varchar(255),
	applied_rule_ids				varchar(255),
	device_model_name_bk			varchar(255), 
	device_browser_name_bk			varchar(50),  
	device_os_name_bk				varchar(50), 
	ga_device_model					varchar(255),
	ga_device_browser				varchar(50),
	ga_device_os					varchar(50),

	reimbursement_type_name_bk		varchar(20),
	reimburser_id_bk				int,

	countries_registered_code		varchar(10), 

	price_type_name_bk				varchar(40),
	discount_f						char(1),
	total_qty_unit					decimal(12, 4),
	total_qty_unit_refunded			decimal(12, 4),
	total_qty_pack					int,
	weight							decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_total_refund				decimal(12, 4),
	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_inc_vat_2 decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_payment_online			decimal(12, 4),
	local_payment_refund_online		decimal(12, 4),
	local_reimbursement				decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_freight_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_product_cost_discount		decimal(12, 4),
	local_total_cost_discount		decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),

	num_lines						int, 


	idETLBatchRun					bigint NOT NULL, 
	ins_ts							datetime NOT NULL);
go 

alter table DW_GetLenses_jbs.dbo.sales_dim_order_header_ladlex add constraint [PK_aux_sales_dim_order_header_ladlex]
	primary key clustered (order_id_bk);
go

alter table DW_GetLenses_jbs.dbo.sales_dim_order_header_ladlex add constraint [DF_aux_sales_dim_order_header_ladlex_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go




-- DW_GetLenses_jbs.dbo.sales_fact_order_line
create table DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex(
	order_line_id_bk				bigint NOT NULL,
	order_id_bk						bigint NOT NULL, 
	invoice_id						int,
	shipment_id						int,
	creditmemo_id					int,
	invoice_line_id					int,
	shipment_line_id				int,
	creditmemo_line_id				int,
	order_no						varchar(50) NOT NULL, 
	invoice_no						varchar(50), 
	shipment_no						varchar(50), 
	creditmemo_no					varchar(50), 
	
	num_shipment_lines				int,
	num_creditmemo_lines			int,

	idCalendarInvoiceDate			int, 
	invoice_date					datetime,
	idCalendarShipmentDate			int, 
	idTimeShipmentDate				varchar(10),
	shipment_date					datetime,
	idCalendarRefundDate			int, 
	refund_date						datetime, 
	
	warehouse_id_bk					bigint NOT NULL, 

	product_id_bk					int NOT NULL, 
	productid_erp_bk				bigint,
	base_curve_bk					char(3),
	diameter_bk						char(4),
	power_bk						char(6),
	cylinder_bk						char(5),
	axis_bk							char(3), 
	addition_bk						char(4), 
	dominance_bk					char(1), 
	colour_bk						varchar(50), 
	eye								char(1),
	sku_magento						varchar(255),
	sku_erp							varchar(255),

	glass_vision_type_bk			varchar(50),
	order_line_id_vision_type		bigint, 
	glass_package_type_bk			varchar(50),
	order_line_id_package_type		bigint, 

	aura_product_f					char(1), 

	countries_registered_code		varchar(10),
	product_type_vat				varchar(255),

	order_status_name_bk			varchar(20) NOT NULL,
	price_type_name_bk				varchar(40),
	discount_f						char(1),
	pack_size						int,
	qty_unit						decimal(12, 4),
	qty_unit_refunded				decimal(12, 4),
	qty_pack						int,
	qty_time						int,
	weight							decimal(12, 4),

	local_price_unit				decimal(12, 4),
	local_price_pack				decimal(12, 4),
	local_price_pack_discount		decimal(12, 4),

	local_subtotal					decimal(12, 4),
	local_shipping					decimal(12, 4),
	local_discount					decimal(12, 4),
	local_store_credit_used			decimal(12, 4),
	local_total_inc_vat				decimal(12, 4),
	local_total_exc_vat				decimal(12, 4),
	local_total_vat					decimal(12, 4),
	local_total_prof_fee			decimal(12, 4),

	local_store_credit_given		decimal(12, 4),
	local_bank_online_given			decimal(12, 4),
	local_subtotal_refund			decimal(12, 4),
	local_shipping_refund			decimal(12, 4),
	local_discount_refund			decimal(12, 4),
	local_store_credit_used_refund	decimal(12, 4),
	local_adjustment_refund			decimal(12, 4),

	local_total_aft_refund_inc_vat	decimal(12, 4),
	local_total_aft_refund_exc_vat	decimal(12, 4),
	local_total_aft_refund_vat		decimal(12, 4),
	local_total_aft_refund_prof_fee	decimal(12, 4),

	local_payment_online			decimal(12, 4),
	local_payment_refund_online		decimal(12, 4),
	local_reimbursement				decimal(12, 4),

	local_product_cost				decimal(12, 4),
	local_shipping_cost				decimal(12, 4),
	local_freight_cost				decimal(12, 4),
	local_total_cost				decimal(12, 4),
	local_margin					decimal(12, 4),

	local_product_cost_discount		decimal(12, 4),
	local_total_cost_discount		decimal(12, 4),

	local_to_global_rate			decimal(12, 4),
	order_currency_code				varchar(255),

	discount_percent				decimal(12, 4),
	vat_percent						decimal(12, 4),
	prof_fee_percent				decimal(12, 4),
	vat_rate						decimal(12, 4),
	prof_fee_rate					decimal(12, 4),

	order_allocation_rate			decimal(12, 4),
	order_allocation_rate_aft_refund decimal(12, 4),
	order_allocation_rate_refund	decimal(12, 4),
	num_erp_allocation_lines		int, 
	
	
	idETLBatchRun			bigint NOT NULL, 
	ins_ts					datetime NOT NULL)
go

alter table DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex add constraint [PK_aux_sales_fact_order_line_ladlex]
	primary key clustered (order_line_id_bk);
go

alter table DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex add constraint [DF_aux_sales_fact_order_line_ladlex_ins_ts] DEFAULT (getutcdate()) for ins_ts;
go 

------------------------------------------------------------------------------

	delete from DW_GetLenses_jbs.dbo.sales_dim_order_header_ladlex

	insert into DW_GetLenses_jbs.dbo.sales_dim_order_header_ladlex (order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no, 
		idCalendarOrderDate, idTimeOrderDate, order_date, 
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date,

		store_id_bk, market_id_bk, customer_id_bk, shipping_address_id, billing_address_id,
		country_id_shipping_bk, region_id_shipping_bk, postcode_shipping, 
		country_id_billing_bk, region_id_billing_bk, postcode_billing,
		customer_unsubscribe_name_bk,

		order_stage_name_bk, order_status_name_bk, adjustment_order, order_type_name_bk, status_bk, 
		payment_method_name_bk, cc_type_name_bk, payment_id, payment_method_code,
		shipping_description_bk, shipping_description_code, telesales_admin_username_bk, prescription_method_name_bk,
		reminder_type_name_bk, reminder_period_bk, reminder_date, reminder_sent, 
		reorder_f, reorder_date, reorder_profile_id, automatic_reorder,
		order_source, proforma, platform,
		product_type_oh_bk, order_qty_time, order_qty_time_cl, order_qty_time_ncl, num_diff_product_type_oh, aura_product_p, 

		referafriend_code, referafriend_referer, 
		postoptics_auto_verification, presc_verification_method, warehouse_approved_time,

		channel_name_bk, marketing_channel_name_bk, publisher_id_bk, 
		affilCode, ga_medium, ga_source, ga_channel, telesales_f, sms_auto_f, mutuelles_f,
		coupon_id_bk, coupon_code, applied_rule_ids,
		device_model_name_bk, device_browser_name_bk, device_os_name_bk, 
		ga_device_model, ga_device_browser, ga_device_os,
		reimbursement_type_name_bk, reimburser_id_bk, 

		countries_registered_code, 

		price_type_name_bk, discount_f,
		total_qty_unit, total_qty_unit_refunded, total_qty_pack, weight, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_total_refund, local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_inc_vat_2, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_payment_online, local_payment_refund_online, local_reimbursement, 
		local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost, local_margin, 
		local_product_cost_discount, local_total_cost_discount,
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent,
		num_lines, 
		idETLBatchRun)

		select oh.order_id_bk, 
			oh.invoice_id, oh.shipment_id, oh.creditmemo_id, 
			oh.order_no, oh.invoice_no, oh.shipment_no, oh.creditmemo_no, 
			oh.idCalendarOrderDate, oh.idTimeOrderDate, oh.order_date, 
			oh.idCalendarInvoiceDate, oh.invoice_date, 
			oh.idCalendarShipmentDate, oh.idTimeShipmentDate, oh.shipment_date, 
			oh.idCalendarRefundDate, oh.refund_date,

			oh_sc.store_id_bk, oh_sc.market_id_bk, oh_sc.customer_id_bk, oh_sc.shipping_address_id, oh_sc.billing_address_id,
			oh_sc.country_id_shipping_bk, oh_sc.region_id_shipping_bk, oh_sc.postcode_shipping, 
			oh_sc.country_id_billing_bk, oh_sc.region_id_billing_bk, oh_sc.postcode_billing,
			oh_sc.customer_unsubscribe_name_bk,

			oh_o.order_stage_name_bk, oh_o.order_status_name_bk, oh_o.adjustment_order, oh_o.order_type_name_bk, oh_o.status_bk, 
			oh_o.payment_method_name_bk, oh_o.cc_type_name_bk, oh_o.payment_id, oh_o.payment_method_code,
			oh_o.shipping_description_bk, oh_o.shipping_description_code, oh_o.telesales_admin_username_bk, oh_o.prescription_method_name_bk,
			oh_o.reminder_type_name_bk, oh_o.reminder_period_bk, oh_o.reminder_date, oh_o.reminder_sent, 
			oh_o.reorder_f, oh_o.reorder_date, oh_o.reorder_profile_id, oh_o.automatic_reorder,
			oh_o.order_source, oh_o.proforma, oh_o.platform,
			oh_o.product_type_oh_bk, oh_o.order_qty_time, oh_o.order_qty_time_cl, oh_o.order_qty_time_ncl, oh_o.num_diff_product_type_oh, oh_o.aura_product_p, 

			oh_o.referafriend_code, oh_o.referafriend_referer, 
			oh_o.postoptics_auto_verification, oh_o.presc_verification_method, oh_o.warehouse_approved_time,

			oh_m.channel_name_bk, oh_m.marketing_channel_name_bk, oh_m.publisher_id_bk, 
			oh_m.affilCode, oh_m.ga_medium, oh_m.ga_source, oh_m.ga_channel, oh_m.telesales_f, oh_m.sms_auto_f, oh_m.mutuelles_f, 
			oh_m.coupon_id_bk, oh_m.coupon_code, oh_m.applied_rule_ids,
			oh_m.device_model_name_bk, oh_m.device_browser_name_bk, oh_m.device_os_name_bk, 
			oh_m.ga_device_model, oh_m.ga_device_browser, oh_m.ga_device_os,
			oh_o.reimbursement_type_name_bk, oh_o.reimburser_id_bk, 

			oh_vat.countries_registered_code, 

			oh_me.price_type_name_bk, oh_me.discount_f,
			oh_me.total_qty_unit, oh_me.total_qty_unit_refunded, oh_me.total_qty_pack, oh_me.weight, 
			oh_me.local_subtotal, oh_me.local_shipping, oh_me.local_discount, oh_me.local_store_credit_used, 
			oh_me.local_total_inc_vat, oh_vat.local_total_exc_vat, oh_vat.local_total_vat, oh_vat.local_total_prof_fee, 
			oh_me.local_total_refund, oh_me.local_store_credit_given, oh_me.local_bank_online_given, 
			oh_me.local_subtotal_refund, oh_me.local_shipping_refund, oh_me.local_discount_refund, oh_me.local_store_credit_used_refund, oh_me.local_adjustment_refund, 
			oh_me.local_total_aft_refund_inc_vat, oh_me.local_total_aft_refund_inc_vat_2, oh_vat.local_total_aft_refund_exc_vat, oh_vat.local_total_aft_refund_vat, oh_vat.local_total_aft_refund_prof_fee, 
			oh_me.local_payment_online, oh_me.local_payment_refund_online, oh_me.local_reimbursement, 
			oh_me.local_product_cost, oh_me.local_shipping_cost, oh_me.local_freight_cost, oh_me.local_total_cost, oh_me.local_margin, 
			oh_me.local_product_cost_discount, oh_me.local_total_cost_discount,
			oh_me.local_to_global_rate, oh_me.order_currency_code, 
			oh_me.discount_percent, oh_vat.vat_percent, oh_vat.prof_fee_percent,
			oh_me.num_lines, 

			1 idETLBatchRun
	from 
			Landing.aux.sales_order_header_o_i_s_cr oh
		inner join
			Landing.aux.sales_order_header_store_cust oh_sc	on oh.order_id_bk = oh_sc.order_id_bk
		inner join
			Landing.aux.sales_order_header_dim_order oh_o on oh.order_id_bk = oh_o.order_id_bk		
		inner join
			Landing.aux.sales_order_header_dim_mark oh_m on oh.order_id_bk = oh_m.order_id_bk
		inner join
			Landing.aux.sales_order_header_measures oh_me on oh.order_id_bk = oh_me.order_id_bk
		inner join
			Landing.aux.sales_order_header_vat oh_vat on oh.order_id_bk = oh_vat.order_id_bk 

	delete from DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex

	insert into DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex (order_line_id_bk, order_id_bk, 
		invoice_id, shipment_id, creditmemo_id, 
		order_no, invoice_no, shipment_no, creditmemo_no,
		invoice_line_id, shipment_line_id, creditmemo_line_id,  	
		num_shipment_lines, num_creditmemo_lines,
		idCalendarInvoiceDate, invoice_date, 
		idCalendarShipmentDate, idTimeShipmentDate, shipment_date, 
		idCalendarRefundDate, refund_date, 
		warehouse_id_bk, 

		product_id_bk, productid_erp_bk,
		base_curve_bk, diameter_bk, power_bk, cylinder_bk, axis_bk, addition_bk, dominance_bk, colour_bk, eye,
		sku_magento, sku_erp, 
		glass_vision_type_bk, order_line_id_vision_type, glass_package_type_bk, order_line_id_package_type,
		aura_product_f, 

		countries_registered_code, product_type_vat,

		order_status_name_bk, price_type_name_bk, discount_f,
		pack_size,
		qty_unit, qty_unit_refunded, qty_pack, qty_time, weight, 
		local_price_unit, local_price_pack, local_price_pack_discount, 
		local_subtotal, local_shipping, local_discount, local_store_credit_used, 
		local_total_inc_vat, local_total_exc_vat, local_total_vat, local_total_prof_fee, 
		local_store_credit_given, local_bank_online_given, 
		local_subtotal_refund, local_shipping_refund, local_discount_refund, local_store_credit_used_refund, local_adjustment_refund, 
		local_total_aft_refund_inc_vat, local_total_aft_refund_exc_vat, local_total_aft_refund_vat, local_total_aft_refund_prof_fee, 
		local_payment_online, local_payment_refund_online, local_reimbursement, 
		local_product_cost, local_shipping_cost, local_freight_cost, local_total_cost, local_margin, 
		local_product_cost_discount, local_total_cost_discount,
		local_to_global_rate, order_currency_code, 
		discount_percent, vat_percent, prof_fee_percent, vat_rate, prof_fee_rate,
		order_allocation_rate, order_allocation_rate_aft_refund, order_allocation_rate_refund, 
		num_erp_allocation_lines, 

		idETLBatchRun)

		select ol.order_line_id_bk, ol.order_id_bk, 
			ol.invoice_id, ol.shipment_id, ol.creditmemo_id, 
			ol.order_no, ol.invoice_no, ol.shipment_no, ol.creditmemo_no,
			ol.invoice_line_id, ol.shipment_line_id, ol.creditmemo_line_id,  	
			ol.num_shipment_lines, ol.num_creditmemo_lines,
			ol.idCalendarInvoiceDate, ol.invoice_date, 
			ol.idCalendarShipmentDate, ol.idTimeShipmentDate, ol.shipment_date, 
			ol.idCalendarRefundDate, ol.refund_date, 
			ol.warehouse_id_bk, 

			ol_p.product_id_bk, ol_p.productid_erp_bk,
			ol_p.base_curve_bk, ol_p.diameter_bk, ol_p.power_bk, ol_p.cylinder_bk, ol_p.axis_bk, ol_p.addition_bk, ol_p.dominance_bk, ol_p.colour_bk, ol_p.eye,
			ol_p.sku_magento, ol_p.sku_erp, 
			ol_p.glass_vision_type_bk, ol_p.order_line_id_vision_type, ol_p.glass_package_type_bk, ol_p.order_line_id_package_type,
			ol_p.aura_product_f, 

			ol_vat.countries_registered_code, ol_vat.product_type_vat,

			ol_me.order_status_name_bk, ol_me.price_type_name_bk, ol_me.discount_f,
			ol_me.pack_size,
			ol_me.qty_unit, ol_me.qty_unit_refunded, ol_me.qty_pack, ol_me.qty_time, ol_me.weight, 
			ol_me.local_price_unit, ol_me.local_price_pack, ol_me.local_price_pack_discount, 
			ol_me.local_subtotal, ol_me.local_shipping, ol_me.local_discount, ol_me.local_store_credit_used, 
			ol_me.local_total_inc_vat, ol_vat.local_total_exc_vat, ol_vat.local_total_vat, ol_vat.local_total_prof_fee, 
			ol_me.local_store_credit_given, ol_me.local_bank_online_given, 
			ol_me.local_subtotal_refund, ol_me.local_shipping_refund, ol_me.local_discount_refund, ol_me.local_store_credit_used_refund, ol_me.local_adjustment_refund, 
			ol_me.local_total_aft_refund_inc_vat, ol_vat.local_total_aft_refund_exc_vat, ol_vat.local_total_aft_refund_vat, ol_vat.local_total_aft_refund_prof_fee, 
			ol_me.local_payment_online, ol_me.local_payment_refund_online, ol_me.local_reimbursement, 
			ol_me.local_product_cost, ol_me.local_shipping_cost, ol_me.local_freight_cost, ol_me.local_total_cost, ol_me.local_margin, 
			ol_me.local_product_cost_discount, ol_me.local_total_cost_discount,
			ol_me.local_to_global_rate, ol_me.order_currency_code, 
			ol_me.discount_percent, ol_vat.vat_percent, ol_vat.prof_fee_percent, ol_vat.vat_rate, ol_vat.prof_fee_rate,
			ol_me.order_allocation_rate, ol_me.order_allocation_rate_aft_refund, ol_me.order_allocation_rate_refund, 
			ol_me.num_erp_allocation_lines, 

			1 idETLBatchRun

		from 
				Landing.aux.sales_order_line_o_i_s_cr ol
			inner join
				Landing.aux.sales_order_line_product ol_p on ol.order_line_id_bk = ol_p.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_measures ol_me on ol.order_line_id_bk = ol_me.order_line_id_bk
			inner join
				Landing.aux.sales_order_line_vat ol_vat on ol.order_line_id_bk = ol_vat.order_line_id_bk	

------------------------------------------------------------------------------

select top 1000 *
from DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex

select t1.order_line_id_bk, order_no, replace(order_no, 'LADLEX', '') old_order_id,
	invoice_date, product_id_bk, productid_erp_bk,
	base_curve_bk, t2.base_curve, diameter_bk, t2.diameter, power_bk, t2.power, cylinder_bk, t2.cylinder, axis_bk, t2.axis, 
	addition_bk, t2.[add], dominance_bk, t2.dominant, colour_bk, t2.color, t1.eye,
	sku_magento, sku_erp
from 
		DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
	inner join
		Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
where sku_erp is null
	-- and invoice_date > '2019-11-01'
	and product_id_bk not in (3128, 3129, 3130, 3135, 3139, 3140, 3152, 3153, 3154)
order by product_id_bk

select product_id_bk, count(*), count(sku_erp), min(invoice_date), max(invoice_date), sum(count(*)) over ()
from DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex
where sku_erp is null
	and product_id_bk not in (3128, 3129, 3130, 3135, 3139, 3140, 3152, 3153, 3154)
group by product_id_bk
order by product_id_bk

-----------------------------------------------

select t1.base_curve_bk, t1.base_curve, t2.base_curve_bk, t1.num
from
		(select base_curve_bk, t2.base_curve, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by base_curve_bk, t2.base_curve) t1
	left join
		Warehouse.prod.dim_param_bc t2 on t1.base_curve_bk = t2.base_curve_bk
order by t1.base_curve_bk, t1.base_curve

select t1.diameter_bk, t1.diameter, t2.diameter_bk, t1.num
from
		(select diameter_bk, t2.diameter, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by diameter_bk, t2.diameter) t1
	left join
		Warehouse.prod.dim_param_di t2 on t1.diameter_bk = t2.diameter_bk
order by t1.diameter_bk, t1.diameter

select t1.power_bk, t1.power, t2.power_bk, t1.num
from
		(select power_bk, t2.power, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by power_bk, t2.power) t1
	full join
		Warehouse.prod.dim_param_po t2 on t1.power_bk = t2.power_bk
order by t1.power_bk, t1.power, t2.power_bk

select t1.cylinder_bk, t1.cylinder, t2.cylinder_bk, t1.num
from
		(select cylinder_bk, t2.cylinder, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by cylinder_bk, t2.cylinder) t1
	full join
		Warehouse.prod.dim_param_cy t2 on t1.cylinder_bk = t2.cylinder_bk
order by t1.cylinder_bk, t1.cylinder, t2.cylinder_bk

select t1.axis_bk, t1.axis, t2.axis_bk, t1.num
from
		(select axis_bk, t2.axis, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by axis_bk, t2.axis) t1
	full join
		Warehouse.prod.dim_param_ax t2 on t1.axis_bk = t2.axis_bk
order by t1.axis_bk, t1.axis, t2.axis_bk

select t1.addition_bk, t1.[add], t2.addition_bk, t1.num
from
		(select addition_bk, t2.[add], count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by addition_bk, t2.[add]) t1
	full join
		Warehouse.prod.dim_param_ad t2 on t1.addition_bk = t2.addition_bk
order by t1.addition_bk, t1.[add], t2.addition_bk

select t1.dominance_bk, t1.dominant, t2.dominance_bk, t1.num
from
		(select dominance_bk, t2.dominant, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by dominance_bk, t2.dominant) t1
	full join
		Warehouse.prod.dim_param_do t2 on t1.dominance_bk = t2.dominance_bk
order by t1.dominance_bk, t1.dominant, t2.dominance_bk

select t1.colour_bk, t1.color, t2.colour_bk, t1.num
from
		(select colour_bk, t2.color, count(*) num
		from 
				DW_GetLenses_jbs.dbo.sales_fact_order_line_ladlex t1
			inner join
				Landing.migra.sales_flat_order_item_migrate t2 on t1.order_line_id_bk = t2.item_id
		group by colour_bk, t2.color) t1
	full join
		Warehouse.prod.dim_param_col t2 on t1.colour_bk = t2.colour_bk
order by t1.colour_bk, t1.color, t2.colour_bk