
use lad_lex;


  select t2.old_customer_id, max(oh.created_at) last_order_date
  from 
      migrate_customerdata t2 
    inner join
      migrate_orderdata oh on t2.old_customer_id = oh.old_customer_id
  group by t2.old_customer_id;
  
----------------------------------
  
select t1.entity_id, t2.*
from 
    magento01.customer_entity t1 
  right join  
    migrate_customerdata t2 on t1.email = t2.email
where t1.email is null
order by t2.old_customer_id;

-- Identifying DELTA customers
select *
from migrate_customerdata
where 
  ((CONVERT(old_customer_id, SIGNED) > 37705 and old_customer_id_prefix = 'LAD') or
  (CONVERT(old_customer_id, SIGNED) > 1045329 and old_customer_id_prefix = 'LEX'))
  and new_magento_id <> 0
order by migrated, created_at

select t1.entity_id, max(oh.created_at) last_order_date
from 
    magento01.customer_entity t1 
  inner join  
    migrate_customerdata t2 on t1.email = t2.email
  inner join
    magento01.sales_flat_order oh on t1.entity_id = oh.customer_id
group by t1.entity_id    
order by t1.entity_id;


----------------------------------

select old_customer_id, new_magento_id, migrated, email, created_at, 
  old_last_order_date, old_customer_lifecycle, 
  magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle
from migrate_customerdata
where new_magento_id <> 0;


  select migrated, magento_customer_lifecycle, vd_customer_lifecycle, old_customer_lifecycle, count(*)
  from migrate_customerdata
  -- where new_magento_id <> 0
  group by migrated, magento_customer_lifecycle, vd_customer_lifecycle, old_customer_lifecycle;
  
  select migrated, count(*)
  from migrate_customerdata
  -- where new_magento_id <> 0
  group by migrated;
