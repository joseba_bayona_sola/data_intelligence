create table #act_customer_cr_reg(
		customer_id_bk						int NOT NULL,
		email								varchar(255),
		store_id							int, 
		created_at							datetime NOT NULL,

		old_access_cust_no					varchar(255),
		store_id_oacn						int,
		website_group_oacn					varchar(255),

		migrate_db							varchar(25),
		migrate_old_customer_id				bigint, 
		migrate_created_at					datetime,
				
		num_dist_websites					int,

		store_id_first_all					int, 
		first_all_date						datetime,

		store_id_first_vd					int,
		first_vd_date						datetime,

		store_id_first_non_vd				int,
		first_non_vd_date					datetime,

		store_id_last_non_vd				int,
		last_non_vd_date					datetime,

		non_vd_customer_status_at_migration_date	varchar(50),

		store_id_last_vd_before_migration	int,
		last_vd_before_migration_date		datetime,
		customer_status_at_migration_date	varchar(50),

		migration_date						datetime,

		store_id_last_vd_before_oacn		int,
		last_vd_before_oacn_date			datetime,
		customer_status_at_oacn_date		varchar(50),

		migration_date_oacn					datetime,

		store_id_bk_cr						int,
		create_date							datetime,	

		store_id_bk_reg						int,
		register_date						datetime, 
		
		customer_origin_name_bk				varchar(50), 
		num_mig_orders_last_year			int)


		insert into #act_customer_cr_reg(customer_id_bk, email, store_id, created_at, 
			old_access_cust_no, store_id_oacn, website_group_oacn, 
			migrate_db, migrate_old_customer_id, migrate_created_at)

			select c.entity_id customer_id_bk, c.email, c.store_id, c.created_at, 
				c.old_access_cust_no, oacn.store_id store_id_oacn, oacn.website_group website_group_oacn, 
				mc.db migrate_db, mc.old_customer_id, mc.created_at migrate_created_at
			from 
					-- (select new_magento_id customer_id
					-- from Landing.migra_ladlex.migrate_customerdata) t
					(select entity_id customer_id
					from Landing.mag.customer_entity_flat
					union
					select distinct customer_id_bk customer_id
					from Landing.aux.sales_order_header_store_cust) t
				inner join					
					Landing.mag.customer_entity_flat_aud c on t.customer_id = c.entity_id
				left join
					(select oacn.old_access_cust_no, s.store_id, s.website_group
					from 
							Landing.map.act_website_old_access_cust_no_aud oacn
						inner join
							Landing.aux.mag_gen_store_v s on oacn.store_id = s.store_id) oacn on c.old_access_cust_no = oacn.old_access_cust_no
				left join
					(select db, new_magento_id, old_customer_id, created_at
					from
						(select db, new_magento_id, old_customer_id, created_at, 
							count(*) over (partition by new_magento_id) num_rep, 
							rank() over (partition by new_magento_id order by created_at, db, old_customer_id) ord_rep -- also old_customer_id as a Migr. customer could have more than 1 old account
						from Landing.migra.migrate_customerdata_v
						where new_magento_id <> 0) c
					where ord_rep = 1) mc on c.entity_id = mc.new_magento_id


	-- Step 2:  

		-- num_dist_websites
		merge into #act_customer_cr_reg trg
		using
			(select t.customer_id_bk, count(distinct s.website_group) dist_website
			from 
					#act_customer_cr_reg t
				left join
					Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				left join
					(select store_id, case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group
					from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
			group by t.customer_id_bk) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set trg.num_dist_websites = src.dist_website;

		-- store_id_first_all, first_all_date 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where r_all = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_all = src.store_id, 
				trg.first_all_date = src.order_date;

		-- store_id_first_vd, first_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 1 and r_non_vd = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_vd = src.store_id, 
				trg.first_vd_date = src.order_date;

		-- store_id_first_non_vd, first_non_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 0 and r_non_vd = 1) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_first_non_vd = src.store_id, 
				trg.first_non_vd_date = src.order_date;

		-- store_id_last_non_vd, last_non_vd_date		 
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all, 
					rank() over (partition by t.customer_id_bk, s.website_group_f order by oh.order_date, oh.order_id_bk) r_non_vd, 
					count(*) over (partition by t.customer_id_bk, s.website_group_f) num_non_vd
				from 
						#act_customer_cr_reg t
					inner join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					inner join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id) t
			where website_group_f = 0 and r_non_vd = num_non_vd) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_non_vd = src.store_id, 
				trg.last_non_vd_date = src.order_date;


	-- Step 3: migration_date: All Cust with first order in Non Current VD

		merge into #act_customer_cr_reg trg
		using
			(select c.customer_id_bk, wmd.migration_date
			from 
					#act_customer_cr_reg c
				inner join
					Landing.aux.mag_gen_store_v s on c.store_id_last_non_vd = s.store_id -- first or last ??
				inner join
					Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.migration_date = src.migration_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_non_vd_date, migration_date) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end non_vd_customer_status_at_migration_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where migration_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.non_vd_customer_status_at_migration_date = src.non_vd_customer_status_at_migration_date;


	-- Step 4.1: store_id_last_vd_before_migration, last_vd_before_migration_date, customer_status_at_migration_date
		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					t.migration_date, 
					count(*) over (partition by t.customer_id_bk) num_rows, 
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all
				from 
						#act_customer_cr_reg t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					left join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
				where t.migration_date is not null
					and s.website_group_f = 1 and oh.order_date < t.migration_date) t
			where num_rows = r_all) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_vd_before_migration = src.store_id, 
				trg.last_vd_before_migration_date = src.order_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_vd_before_migration_date, migration_date) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end customer_status_at_migration_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where last_vd_before_migration_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.customer_status_at_migration_date = src.customer_status_at_migration_date;

	-- Step 4.2: migration_date_oacn // store_id_last_vd_before_oacn, last_vd_before_oacn_date, customer_status_at_oacn_date

		merge into #act_customer_cr_reg trg
		using
			(select t.customer_id_bk, wmd.migration_date migration_date_oacn
			from 
					#act_customer_cr_reg t
				inner join
					Landing.map.act_website_old_access_cust_no_aud oacn on t.old_access_cust_no = oacn.old_access_cust_no
				inner join
					Landing.aux.mag_gen_store_v s on oacn.store_id = s.store_id
				inner join
					Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group
			where 
				t.old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX')) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.migration_date_oacn = src.migration_date_oacn;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, store_id, order_date
			from
				(select t.customer_id_bk, t.old_access_cust_no, t.migrate_db, 
					oh.order_id_bk, oh.order_no, oh.order_date, s.store_id, s.website_group, s.website_group_f,
					t.migration_date_oacn, 
					count(*) over (partition by t.customer_id_bk) num_rows, 
					rank() over (partition by t.customer_id_bk order by oh.order_date, oh.order_id_bk) r_all
				from 
						#act_customer_cr_reg t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
					left join
						(select store_id, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI')) then 'Visiondirect' else website_group end website_group, 
							case when (website_group in ('GetLenses', 'ASDA', 'EyePlan', 'VHI', 'Visiondirect')) then 1 else 0 end website_group_f
						from Landing.aux.mag_gen_store_v) s on oh.store_id_bk = s.store_id
				where t.migration_date_oacn is not null
					and s.website_group_f = 1 and oh.order_date < t.migration_date_oacn) t
			where num_rows = r_all) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.store_id_last_vd_before_oacn = src.store_id, 
				trg.last_vd_before_oacn_date = src.order_date;

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, 
				case when (datediff(day, last_vd_before_oacn_date, migration_date_oacn) >= l.lapsed_num_days) then 'LAPSED' else 'ACTIVE' end customer_status_at_oacn_date
			from #act_customer_cr_reg, 
				Landing.map.act_lapsed_num_days l
			where last_vd_before_oacn_date is not null) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set 
				trg.customer_status_at_oacn_date = src.customer_status_at_oacn_date;


	-- Step 5: store_id_bk_cr, create_date 
	
		-- 5.1: 0 Orders
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = created_at -- Need to add logic on store_id
			where store_id_first_all is null and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null)
	
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, create_date = created_at
			where store_id_first_all is null and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX')

		-- 5.2: XX Orders - Only in VD
			
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null)
			
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') 
			

		-- 5.3: XX Orders - Only Migrate Website


			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_non_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null) and
				store_id_first_non_vd not in (select store_id
										 from Landing.map.act_website_old_access_cust_no_aud
										 where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX'))
			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null) and
				store_id_first_non_vd in (select store_id
										 from Landing.map.act_website_old_access_cust_no_aud
										 where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX'))

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') 

		-- 5.4: XX Orders - Both VD - Migrate

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_last_non_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end 
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null) and
				(non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED') and
				store_id_last_non_vd not in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX'))

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null) and
				(non_vd_customer_status_at_migration_date = 'ACTIVE' and customer_status_at_migration_date = 'LAPSED') and
				store_id_last_non_vd in (select store_id
									from Landing.map.act_website_old_access_cust_no_aud
									where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX'))


			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_first_vd, create_date = case when (created_at <= first_all_date) then created_at else first_all_date end 
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				(old_access_cust_no not in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') or old_access_cust_no is null) and
				(customer_status_at_migration_date = 'ACTIVE' or non_vd_customer_status_at_migration_date = 'LAPSED')

			update #act_customer_cr_reg
			set store_id_bk_cr = store_id_oacn, -- create_date = created_at -- Check about created_date
				create_date = case when (created_at <= first_all_date) then created_at else first_all_date end
			where (store_id_first_all is not null and store_id_last_non_vd is not null and last_vd_before_migration_date is not null) and
				old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL', 'LAD', 'LEX') 

	-- Step 6: store_id_bk_reg, register_date

	merge into #act_customer_cr_reg trg
	using
		(select customer_id_bk,
			isnull(wrm.store_id_register, t.store_id) store_id_bk_reg, 
			isnull(wmd.migration_date, t.create_date) register_date
		from 
				#act_customer_cr_reg t
			left join
				Landing.map.act_website_register_mapping_aud wrm on t.store_id = wrm.store_id
			left join
				Landing.aux.mag_gen_store_v s on t.store_id_bk_cr = s.store_id
			left join
				Landing.map.act_website_migrate_date_aud wmd on s.website_group = wmd.website_group) src on trg.customer_id_bk = src.customer_id_bk
	when matched then
		update 
		set  
			trg.store_id_bk_reg = src.store_id_bk_reg, 
			trg.register_date = src.register_date;

	-- Step 7: customer_origin_name_bk, num_mig_orders_last_year

		merge into #act_customer_cr_reg trg
		using
			(select customer_id_bk, count(num_mig_orders_last_year) num_mig_orders_last_year
			from
				(select t.customer_id_bk, t.store_id_bk_cr, t.migration_date_oacn, oh.order_id_bk, oh.order_date, 
					case when (oh.order_date > dateadd(year, -1, t.migration_date_oacn)) then 1 else null end num_mig_orders_last_year
				from 
						(select customer_id_bk, store_id_bk_cr, migration_date_oacn
						from #act_customer_cr_reg
						where old_access_cust_no in ('LENSONCOUK', 'LENSONIE', 'LENSWAYUK', 'LENSONNL', 'LENSWAYNL', 'YOURLENSESNL')) t
					left join
						Landing.aux.sales_dim_order_header_aud oh on t.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND') and t.store_id_bk_cr = oh.store_id_bk) t
			group by customer_id_bk) src on trg.customer_id_bk = src.customer_id_bk
		when matched then
			update 
			set trg.num_mig_orders_last_year = src.num_mig_orders_last_year;

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'MIGRATION'
		where num_mig_orders_last_year >=2

		update #act_customer_cr_reg
		set customer_origin_name_bk = 'ORGANIC'
		where num_mig_orders_last_year in (0, 1) or num_mig_orders_last_year is null

----------------------------------------------------------
----------------------------------------------------------

	select *
	from #act_customer_cr_reg
	order by customer_id_bk

	select old_access_cust_no, count(*)
	from #act_customer_cr_reg
	group by old_access_cust_no
	order by old_access_cust_no

	select t.old_customer_id, t.new_magento_id, t.email, 
		c.old_access_cust_no, cs.website_group_create, cs.website_create, cs.customer_status_name,
		t.old_last_order_date, t.old_customer_lifecycle, t.vd_last_order_date, t.vd_customer_lifecycle
	from 
			Landing.migra_ladlex.migrate_customerdata t
		inner join					
			Landing.mag.customer_entity_flat_aud c on t.new_magento_id = c.entity_id
		inner join
			Warehouse.act.dim_customer_signature_v cs on t.new_magento_id = cs.customer_id
	order by t.new_magento_id

