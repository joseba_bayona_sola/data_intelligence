use lad_lex;

----------------------------------------------------------
-- migrate_customerdata

  -- remove invalid emails: 2 rows 
  select *
  from lad_lex.migrate_customerdata 
  WHERE `email` NOT REGEXP '^[^@]+@[^@]+\.[^@]{2,}$';
  
 -- fix customer address when fields are missnig  
  select *, 
    case when CHAR_LENGTH(billing_street1) = 0 then shipping_street1 else billing_street1 end billing_street1_new,
    case when CHAR_LENGTH(billing_city) = 0 then shipping_city else billing_city end billing_city_new,
    case when CHAR_LENGTH(shipping_street1) = 0 then billing_street1 else shipping_street1 end shipping_street1_new,
    case when CHAR_LENGTH(shipping_city) = 0 then billing_city else shipping_city end shipping_city_new
  from lad_lex.migrate_customerdata  
  -- WHERE CHAR_LENGTH(billing_street1) = 0 -- 763
  -- WHERE CHAR_LENGTH(billing_city) = 0 -- 1274
  -- WHERE CHAR_LENGTH(shipping_street1) = 0 -- 763
  WHERE CHAR_LENGTH(shipping_city) = 0 -- 1274
  order by created_at desc;
  
----------------------------------------------------------  
-- migrate_orderdata

  -- delete OH - OL records attahced to customers deleted
  select *
  from lad_lex.migrate_orderdata 
  WHERE old_customer_id NOT IN (SELECT old_customer_id FROM lad_lex.migrate_customerdata);  

  -- NO
  select *
  from lad_lex.migrate_orderdata 
  WHERE old_order_id NOT IN (SELECT distinct old_order_id FROM lad_lex.migrate_orderlinedata);  
  
  select *
  FROM lad_lex.migrate_orderlinedata 
  WHERE old_order_id NOT IN (SELECT old_order_id FROM lad_lex.migrate_orderdata);  
  
  -- fix totals
  
 -- fix customer address when fields are missnig  
 -- fix customer address when fields are missnig  
  select *, 
    case when CHAR_LENGTH(billing_street1) = 0 then shipping_street1 else billing_street1 end billing_street1_new,
    case when CHAR_LENGTH(billing_city) = 0 then shipping_city else billing_city end billing_city_new,
    case when CHAR_LENGTH(shipping_street1) = 0 then billing_street1 else shipping_street1 end shipping_street1_new,
    case when CHAR_LENGTH(shipping_city) = 0 then billing_city else shipping_city end shipping_city_new
  from lad_lex.migrate_orderdata  
  -- WHERE CHAR_LENGTH(billing_street1) = 0 -- 111
  -- WHERE CHAR_LENGTH(billing_city) = 0 -- 421
  WHERE CHAR_LENGTH(shipping_street1) = 0 -- 111
  -- WHERE CHAR_LENGTH(shipping_city) = 0 -- 421
  order by created_at desc;
  
  -- update shipping description when missing
  
----------------------------------------------------------  
-- migrate_orderlinedata  

  -- force products with bad packsizes to be a non matched product
  select *
  from lad_lex.migrate_orderlinedata 
  WHERE MOD(Quantity,packsize) <> 0  
  
  -- update colour to match vd lables
  
  -- fixes for products that will never be posible to map
  
  -- params

    SELECT DISTINCT
      `product_id`, -- productname, 
      `eye`,
      `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`, `dominant`, `color`,
      `params`, params_hash
    FROM migrate_orderlinedata t1
    where params_hash = '3e7479383065a6e0860c2700bbdc0451'
    
    select *
    from migrate_orderlinedata_params
    where params_hash = '3e7479383065a6e0860c2700bbdc0451'
    
    select count(*), count(distinct params_hash)
    from migrate_orderlinedata_params    