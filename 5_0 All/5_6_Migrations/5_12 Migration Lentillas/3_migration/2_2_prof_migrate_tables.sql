
use lad_lex;

select *
from migrate_customerdata
limit 1000;

select old_customer_id, new_magento_id, new_magento_id_bck, migrated,
  email, password, created_at,
  store_id, domainID, domainName, website_group_id, old_customer_id_prefix,
  firstname, lastname,
  billing_company, billing_prefix, billing_firstname, billing_lastname, 
  billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone,
  shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone,
  cus_phone, default_billing, default_shipping,
  unsubscribe_newsletter, disable_saved_card, RAF_code, is_registered, priority,
  mage_last_order_date, import_last_order_date, old_last_order_date, old_customer_lifecycle, 
  hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle,
  store_credit_amount, store_credit_comment, target_store_credit_amount
from migrate_customerdata
-- where billing_country_id = 'ZW'
-- where billing_street1 is null or CHAR_LENGTH(billing_street1) = 0
-- where password in ('ecommingfirstloggin2013', '7c4a8d09ca3762af61e59520943dc26494f8941b', '6fa2e8a145ceecd246a55757967ce8f57a6a61ec', '40b21e45c78ab5cf51e7931a80a5a2af')
-- where email = 'ana_96.palas@hotmail.com'
-- where migrated = -1
order by new_magento_id desc
-- order by created_at desc
limit 10000;

  select count(*)
  from migrate_customerdata;

  select password, count(*)
  from migrate_customerdata
  group by password
  order by count(*) desc;

  select domainName, count(*), min(old_customer_id), max(old_customer_id)
  from migrate_customerdata
  group by domainName
  order by domainName;
  
  select store_id, domainID, domainName, website_group_id, old_customer_id_prefix, 
    count(*)
  from migrate_customerdata
  group by store_id, domainID, domainName, website_group_id, old_customer_id_prefix
  order by store_id, domainID, domainName, website_group_id, old_customer_id_prefix;

 
  select billing_region, billing_region_id, count(*)
  from migrate_customerdata
  group by billing_region, billing_region_id
  order by billing_region, billing_region_id;
  
  select shipping_region, shipping_region_id, count(*)
  from migrate_customerdata
  group by shipping_region, shipping_region_id
  order by shipping_region, shipping_region_id;  

  select billing_country_id, shipping_country_id, count(*)
  from migrate_customerdata
  group by billing_country_id, shipping_country_id
  order by billing_country_id, shipping_country_id;

  ---------------------------------
  
  select migrated, count(*)
  from migrate_customerdata
  group by migrated
  order by migrated;  
  
  select c1.old_customer_id, c1.new_magento_id, c1.migrated, c1.email, c1.website_group_id, c1.created_at, 
    c2.entity_id, c2.email, c2.created_at
  from 
    migrate_customerdata c1
  left join
    magento01.customer_entity c2 on c1.email = c2.email and c1.website_group_id = c2.website_group_id
  where c1.migrated = 0
    -- and c1.created_at <> c2.created_at
  order by c1.created_at desc;
  
  select migrated, domainName, count(*)
  from migrate_customerdata
  group by migrated, domainName
  order by migrated, domainName;  
  
  
  select *
  from migrate_customerdata
  where old_customer_id in (1011838,1017448)
  -- where new_magento_id is null or new_magento_id = 0
  -- where default_billing is null or default_billing = 0  
  
-- 

select *
from migrate_orderdata
limit 1000;

select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  store_id, website_name, DomainID, domainName, 
  email, old_customer_id, magento_customer_id, 
  migrated, 
  billing_company, billing_prefix, billing_firstname, billing_lastname, 
  billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone,
  shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone,
  shipping_description, base_shipping_method, 
  base_grand_total, base_shipping_amount, base_discount_amount, base_grand_total - base_shipping_amount + base_discount_amount, 
  order_currency, 
  comment, priority, 
  reminder_mobile, reminder_date, reminder_type, 
  increment_id, BASE_ORDER_ID
from migrate_orderdata
where billing_street1 is null or CHAR_LENGTH(billing_street1) = 0
-- where base_discount_amount <> 0 and old_order_id = 100725
order by old_order_id
limit 1000;

  select count(*)
  from migrate_orderdata
  where created_at > '2019-10-01';

  select store_id, website_name, DomainID, domainName, count(*)
  from migrate_orderdata  
  group by store_id, website_name, DomainID, domainName
  order by store_id, website_name, DomainID, domainName;

  select billing_region, billing_region_id, count(*)
  from migrate_orderdata
  group by billing_region, billing_region_id
  order by billing_region, billing_region_id;
  
  select shipping_region, shipping_region_id, count(*)
  from migrate_orderdata
  group by shipping_region, shipping_region_id
  order by shipping_region, shipping_region_id;  
  
  select shipping_description, base_shipping_method, count(*)
  from migrate_orderdata  
  group by shipping_description, base_shipping_method
  order by shipping_description, base_shipping_method;

  select reminder_type, count(*)
  from migrate_orderdata  
  group by reminder_type
  order by reminder_type;  

  select migrated, count(*)
  from migrate_orderdata  
  group by migrated
  order by migrated;    
  
  -- 

select *
from migrate_orderlinedata
limit 1000;

select old_item_id, 
  old_order_id, magento_order_id, 
  store_id, 
  migrated, priority, debug, params_hash, 
  old_product_id, product_id, productCode, productName, productCodeFull, packsize, 
  eye, power, base_curve, diameter, 'add', axis, cylinder, dominant, color, params, -- add
  quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
  quantity, row_total
  -- , backup_add
from migrate_orderlinedata
-- where old_product_id = 526
limit 1000;

  select count(*)
  from migrate_orderlinedata;

  select eye, count(*)
  from migrate_orderlinedata
  group by eye
  order by eye;
  
  select base_curve, count(*)
  from migrate_orderlinedata
  group by base_curve
  order by base_curve;

  select diameter, count(*)
  from migrate_orderlinedata
  group by diameter
  order by diameter;

  select power, count(*)
  from migrate_orderlinedata
  group by power
  order by power;

  select axis, count(*)
  from migrate_orderlinedata
  group by axis
  order by axis;

  select cylinder, count(*)
  from migrate_orderlinedata
  group by cylinder
  order by cylinder;

  select `add`, count(*)
  from migrate_orderlinedata
  group by `add`
  order by `add`;

  select dominant, count(*)
  from migrate_orderlinedata
  group by dominant
  order by dominant;

  select color, count(*)
  from migrate_orderlinedata
  group by color
  order by count(*) desc, color;  

  select product_id, old_product_id, productCode, productName, productCodeFull, packsize, count(*)
  from migrate_orderlinedata
  -- where product_id in (2327, 3128, 3129, 3130, 3131, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3153, 3154)
  -- where product_id in (1215, 1229, 1265, 1095, 1116, 1112)
  -- where product_id in (1882, 2312, 2298, 1888, 1108)
  -- WHERE product_id IN(1113, 1115, 1106, 1088, 1094, 1101, 1120, 1095, 1115, 1086, 1086, 1114, 1083, 1265, 1136, 1136, 1125, 1092, 1083, 1089, 1133, 1090, 1085, 1108, 1116, 1113, 1117, 1119, 1121, 1130, 1129, 1135, 1113, 1139, 1141, 1134, 1120, 1101, 1094, 2327, 1103, 1105, 1104, 1099, 1109, 1110, 1122, 1264, 1087, 1126)
  group by product_id, old_product_id, productCode, productName, productCodeFull, packsize
  order by product_id, old_product_id, productCode, productName, productCodeFull, packsize;
  
  create temporary table ol_row_total as
    select old_order_id, sum(row_total) row_total
    from migrate_orderlinedata
    group by old_order_id;
  
  select od.old_order_id, od.domainName, od.created_at, od.base_grand_total - od.base_shipping_amount + od.base_discount_amount base_subtotal, odl.row_total, 
    abs((od.base_grand_total - od.base_shipping_amount + od.base_discount_amount) - odl.row_total)
  from 
      migrate_orderdata od
    inner join  
      ol_row_total odl on od.old_order_id = odl.old_order_id
  where abs((od.base_grand_total - od.base_shipping_amount + od.base_discount_amount) - odl.row_total) > 0.01
  -- where odl.old_order_id is null and od.domainName = 'lentillasadomicilio.com'
  -- where odl.old_order_id is null
  order by od.created_at desc;
  
  --------------------------------------
  



-- 

select *
from migrate_customerdata_address
limit 1000;

select old_address_id, old_customer_id, 
  created_at, updated_at, 
  firstname, lastname, company, 
  street1, street2, city, region, region_id, postcode, country_id, telephone
from migrate_customerdata_address
limit 1000;

  select count(*)
  from migrate_customerdata_address;   
  
  
-- ----------------------------------------------------------

select email, 
  id_usuari_lad, num_oh_lad, max_oh_lad, id_usuari_lex, num_oh_lex, max_oh_lex, id_usuari, source
from lad_lex.customer_dupl

order by id_usuari;