USE magento01;

-------------------------------
-- migrate_customerdata

  SELECT migrated, COUNT(*)
  FROM migrate_customerdata
  GROUP BY migrated
  ORDER BY migrated;  
  
  SELECT *
  FROM migrate_customerdata
  WHERE new_magento_id = 3002822
  -- where old_customer_id in (1011838,1017448)
  -- where new_magento_id is null or new_magento_id = 0
  WHERE default_billing IS NULL OR default_billing = 0;  
  
  SELECT c1.old_customer_id, c1.new_magento_id, c1.email, c2.email
  FROM 
    migrate_customerdata  c1
  LEFT JOIN
    customer_entity c2 ON c1.new_magento_id = c2.entity_id
  WHERE c2.entity_id IS NULL
  ORDER BY new_magento_id

  SELECT c2.attribute_id, COUNT(*)
  FROM 
    migrate_customerdata  c1
  INNER JOIN
    customer_entity_int c2 ON c1.new_magento_id = c2.entity_id
  -- where c2.attribute_id in (13, 14)
  GROUP BY c2.attribute_id
  ORDER BY c2.attribute_id
  
  SELECT *
  FROM customer_entity_varchar
  WHERE attribute_id IN (532, 534)
  ORDER BY value_id DESC
  LIMIT 1000

  SELECT c2.attribute_id, c2.value, COUNT(*)
  FROM 
    migrate_customerdata  c1
  INNER JOIN
    customer_entity_varchar c2 ON c1.new_magento_id = c2.entity_id
  WHERE c2.attribute_id IN (532)
  GROUP BY c2.attribute_id, c2.value
  ORDER BY c2.attribute_id, c2.value
  
  -- --------------------------------
  
  SELECT *
  FROM customer_address_entity
  WHERE entity_id = 4477278
  ORDER BY entity_id DESC
  LIMIT 1000

  SELECT c1.old_customer_id, c1.new_magento_id, c1.email, c2.customer_address_id
  FROM 
    migrate_customerdata  c1
  LEFT JOIN
    (SELECT parent_id, MAX(entity_id) customer_address_id
    FROM customer_address_entity  
    GROUP BY parent_id) c2 ON c1.new_magento_id = c2.parent_id
  WHERE c2.customer_address_id IS NULL
  ORDER BY new_magento_id


-------------------------------
-- migrate_orderdata

  SELECT migrated, COUNT(*)
  FROM migrate_orderdata
  GROUP BY migrated
  ORDER BY migrated;  

-------------------------------
-- migrate_orderlinedata

  SELECT quote_data_built, COUNT(*)
  FROM migrate_orderlinedata
  GROUP BY quote_data_built
  ORDER BY quote_data_built

  SELECT old_item_id, 
    old_order_id, 
    migrated, debug, 
    old_product_id, product_id, productName, 
    eye, POWER, base_curve, diameter, `add`, axis, cylinder, dominant, color, params, -- add
    quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt
  FROM migrate_orderlinedata
  WHERE quote_data_built = -1 -- and params is null
    -- and product_id = 1114 -- and color = 'Blue' -- AND power = '-0.75'
    -- and product_id not in ( 1114, 1115, 1121, 2312, 2338, 2343)
  ORDER BY product_id, eye, POWER, base_curve, diameter, `add`, axis, cylinder, dominant, color

  SELECT ol.product_id, pm.magento_name, ol.num
  FROM
    (SELECT product_id, COUNT(*) num
    FROM migrate_orderlinedata
    WHERE quote_data_built = -1
    GROUP BY product_id) ol
  LEFT JOIN
    (SELECT DISTINCT magento_id, magento_name
    FROM lad_lex.lad_product_map) pm ON ol.product_id = pm.magento_id
  ORDER BY product_id
  
  
  SELECT migrated, COUNT(*)
  FROM migrate_orderlinedata
  GROUP BY migrated
  ORDER BY migrated;   
