use magento01;

-- -------------------------------------------------------

delete from lad_lex.migrate_orderdata_last;

insert into lad_lex.migrate_orderdata_last (old_order_id)
  select oh2.old_order_id
  from
      (select old_customer_id, max(created_at) last_order
      from lad_lex.migrate_orderdata
      group by old_customer_id) oh1
    inner join  
      lad_lex.migrate_orderdata oh2 on oh1.old_customer_id = oh2.old_customer_id and oh1.last_order = oh2.created_at
  union
  select old_order_id
  from lad_lex.migrate_orderdata
  where created_at between '2019-10-01' and '2019-12-01';

-- -------------------------------------------------------
-- copy migrate tables

	-- migrate_customerdata
  delete from magento01.migrate_customerdata;
  
  insert into magento01.migrate_customerdata (old_customer_id, new_magento_id, new_magento_id_bck, migrated,
    email, password, created_at,
    store_id, domainID, domainName, website_group_id, old_customer_id_prefix,
    firstname, lastname,
    billing_company, billing_prefix, billing_firstname, billing_lastname, 
    billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone,
    shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
    shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone,
    cus_phone, default_billing, default_shipping,
    unsubscribe_newsletter, disable_saved_card, RAF_code, is_registered, priority,
    mage_last_order_date, import_last_order_date, old_last_order_date, old_customer_lifecycle, 
    hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle,
    store_credit_amount, store_credit_comment, target_store_credit_amount)
    
    select old_customer_id, new_magento_id, new_magento_id_bck, migrated,
      email, password, created_at,
      store_id, domainID, domainName, website_group_id, old_customer_id_prefix,
      firstname, lastname,
      billing_company, billing_prefix, billing_firstname, billing_lastname, 
      billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone,
      shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
      shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone,
      cus_phone, default_billing, default_shipping,
      unsubscribe_newsletter, disable_saved_card, RAF_code, is_registered, priority,
      mage_last_order_date, import_last_order_date, old_last_order_date, old_customer_lifecycle, 
      hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle,
      store_credit_amount, store_credit_comment, target_store_credit_amount
    from lad_lex.migrate_customerdata;
  
	-- migrate_customerdata_address
  delete from magento01.migrate_customerdata_address;
  
  insert into magento01.migrate_customerdata_address (old_address_id, old_customer_id, 
    created_at, updated_at, 
    firstname, lastname, company, 
    street1, street2, city, region, region_id, postcode, country_id, telephone)
  
    select old_address_id, old_customer_id, 
      created_at, updated_at, 
      firstname, lastname, company, 
      street1, street2, city, region, region_id, postcode, country_id, telephone
    from lad_lex.migrate_customerdata_address;
  
	-- migrate_orderdata
	delete from magento01.migrate_orderdata;
  
  alter table magento01.migrate_orderdata modify website_name varchar(50) NOT NULL;
  
  insert into magento01.migrate_orderdata(old_order_id, 
    magento_order_id, new_increment_id, created_at, 
    store_id, website_name, DomainID, domainName, 
    email, old_customer_id, magento_customer_id, 
    migrated, 
    billing_company, billing_prefix, billing_firstname, billing_lastname, 
    billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone,
    shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
    shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone,
    shipping_description, base_shipping_method, 
    base_grand_total, base_shipping_amount, base_discount_amount, 
    order_currency, 
    comment, priority, 
    reminder_mobile, reminder_date, reminder_type, 
    increment_id, BASE_ORDER_ID) 
  
    select oh1.old_order_id, 
      magento_order_id, new_increment_id, created_at, 
      store_id, website_name, DomainID, domainName, 
      email, old_customer_id, magento_customer_id, 
      migrated, 
      billing_company, billing_prefix, billing_firstname, billing_lastname, 
      billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone,
      shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
      shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone,
      shipping_description, base_shipping_method, 
      base_grand_total, base_shipping_amount, base_discount_amount, 
      order_currency, 
      comment, priority, 
      reminder_mobile, reminder_date, reminder_type, 
      increment_id, BASE_ORDER_ID
    from 
      lad_lex.migrate_orderdata oh1
    inner join
      lad_lex.migrate_orderdata_last oh2 on oh1.old_order_id = oh2.old_order_id;
    -- where created_at between '2019-10-01' and '2019-12-01';
  
	-- migrate_orderlinedata
	delete from magento01.migrate_orderlinedata;
  
  insert into magento01.migrate_orderlinedata (old_item_id, 
    old_order_id, magento_order_id, 
    store_id, 
    migrated, priority, debug, params_hash, 
    old_product_id, product_id, productCode, productName, productCodeFull, packsize, 
    eye, power, base_curve, diameter, `add`, axis, cylinder, dominant, color, params, -- add
    quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
    quantity, row_total)
  
    select ol.old_item_id, 
      ol.old_order_id, ol.magento_order_id, 
      ol.store_id, 
      ol.migrated, ol.priority, ol.debug, ol.params_hash, 
      ol.old_product_id, ol.product_id, ol.productCode, ol.productName, ol.productCodeFull, ol.packsize, 
      ol.eye, ol.power, ol.base_curve, ol.diameter, ol.`add`, ol.axis, ol.cylinder, ol.dominant, ol.color, ol.params, -- add
      ol.quote_item_id, ol.quote_sku, ol.quote_is_lens, ol.quote_data_built, ol.quote_opt,
      ol.quantity, ol.row_total
    from 
        lad_lex.migrate_orderlinedata ol
      inner join
        lad_lex.migrate_orderdata_last oh on ol.old_order_id = oh.old_order_id;
        -- lad_lex.migrate_orderdata oh on ol.old_order_id = oh.old_order_id
    -- where oh.created_at between '2019-10-01' and '2019-12-01';
  
  update magento01.migrate_orderlinedata
  set migrated = -1
  where quote_data_built = -1;
  
	-- migrate_orderlinedata_params - v1
  delete from magento01.migrate_orderlinedata_params;
  
  insert into magento01.migrate_orderlinedata_params (`product_id`, productname, `eye`,
    `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`,	`dominant`, `color`,
    `params`, params_hash)
  
    select `product_id`, productname, `eye`,
      `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`,	`dominant`, `color`,
      `params`, params_hash
    from lad_lex.migrate_orderlinedata_params;
  
	-- migrate_orderlinedata_params - v2
  delete from magento01.migrate_orderlinedata_params;
  
  insert IGNORE into magento01.migrate_orderlinedata_params (`product_id`, productname, `eye`,
    `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`,	`dominant`, `color`,
    `params`, params_hash)
  
    select DISTINCT
      `product_id`, productname, `eye`,
      `power`, `base_curve`, `diameter`, `add`, `axis`, `cylinder`, `dominant`, `color`,
      `params`, params_hash
    from 
        lad_lex.migrate_orderlinedata ol
      inner join
        lad_lex.migrate_orderdata_last oh on ol.old_order_id = oh.old_order_id;
        -- lad_lex.migrate_orderdata oh on ol.old_order_id = oh.old_order_id
    -- where oh.created_at between '2019-10-01' and '2019-12-01';
    
-- -------------------------------------------------------  
-- truncate migrate_failed tables

	-- migrate_customerdata_failed
	delete from magento01.migrate_customerdata_failed;
  
	-- migrate_orderdata_failed
	delete from magento01.migrate_orderdata_failed;
  
	-- migrate_orderlinedata_failed	
	delete from magento01.migrate_orderlinedata_failed;
    
-- -------------------------------------------------------
-- truncate aux tables

	-- _migrate_last_order_date
	delete from magento01._migrate_last_order_date;
  
	-- _delete_migrated_customer
	delete from magento01._delete_migrated_customer;
  
	-- _remove_migrated_customer
	delete from magento01._remove_migrated_customer;
  
	-- _delete_migrated_orders
	
	-- _remove_migrated_orders
	