
	exec dm.dwh_dwh_get_dm_customer_signature_dm_aux_cust @idETLBatchRun = 1, @idPackageRun = 1

	exec dm.dwh_dwh_get_dm_customer_signature_dm_aux_comm @idETLBatchRun = 1, @idPackageRun = 1

	exec dm.dwh_dwh_get_dm_customer_signature_dm_aux_behaviour @idETLBatchRun = 1, @idPackageRun = 1

	exec dm.dwh_dwh_get_dm_customer_signature_dm_aux_last @idETLBatchRun = 1, @idPackageRun = 1

	exec dm.dwh_dwh_get_dm_customer_signature_dm_aux_product @idETLBatchRun = 1, @idPackageRun = 1

	---------------------------------------------------------
	---------------------------------------------------------

	truncate table Warehouse.dm.fact_customer_signature_dm_wrk
	go

	insert into Warehouse.dm.fact_customer_signature_dm_wrk (idCustomer_sk_fk, 
		client_id, 
		website_id, store_id, store_url, 
		email,
		title, firstname,  lastname, gender, dateofbirth, language, emvcellphone, 
		currency, country, 
		datejoin, dateunjoin, 
		email_origine, email_origin, seed, clienturn, source, code, 
		product_id, 
		order_reminder_id, order_reminder_source, order_reminder_date, order_reminder_no, order_reminder_pref, order_reminder_product, order_reminder_reorder_url, 
		signed_up_to_autoreorder, date_of_autoreorder,

		prescription_expiry_date, prescription_wearer, prescription_optician, 
		bounce, card_expiry_date, 
		email_preferences, unsubscribe, website_unsubscribe, w_unsub_date, 
		trustpilot_service_review, trustpilot_service_review_rating, magento_product_review_rating,
		registration_date, first_order_date, 
		last_logged_in_date, last_order_date, last_order_id, last_order_source, last_order_del_date, num_of_orders,
		aov, lifetime_spend, discount_code_used, num_of_referrals, 

		segment_lifecycle, segment_usage, segment_geog, segment_purch_behaviour, segment_eysight, segment_sport, segment_professional, segment_lifestage, segment_vanity, 
		segment, segment_2, segment_3, segment_4, 
		emvadmin1, emvadmin2, emvadmin3, emvadmin4, emvadmin5, 
		check_sum, 

		lens_sku1, lens_sku2, lens_name1, lens_name2, lens_url1, lens_url2, lens_last_order_date, 
		lens_google_shopping_gtin_sku1, lens_google_shopping_gtin_sku2,

		dailies_store_id1, dailies_store_id2,
		dailies_sku1, dailies_sku2, dailies_name1, dailies_name2, dailies_url1, dailies_url2, dailies_last_order_date, 
		google_shopping_gtin_daily_sku1, google_shopping_gtin_daily_sku2,

		monthlies_store_id1, monthlies_store_id2,
		monthlies_sku1, monthlies_sku2, monthlies_name1, monthlies_name2, monthlies_url1, monthlies_url2, monthlies_category1, monthlies_category2, monthlies_last_order_date, 
		google_shopping_gtin_monthly_sku1, google_shopping_gtin_monthly_sku2, 

		colours_store_id1, colours_store_id2,
		colours_sku1, colours_sku2, colours_name1, colours_name2, colours_url1, colours_url2, colours_last_order_date, 
		google_shopping_gtin_colours_sku1, google_shopping_gtin_colours_sku2, 

		solutions_store_id1, solutions_store_id2,
		solutions_sku1, solutions_sku2, solutions_name1, solutions_name2, solutions_url1, solutions_url2, solutions_category1, solutions_category2, solutions_last_order_date, 
		google_shopping_gtin_solutions_sku1, google_shopping_gtin_solutions_sku2, 

		other_store_id1, other_store_id2,
		other_sku1, other_sku2, other_name1, other_name2, other_url1, other_url2, other_category1, other_category2, other_last_order_date, 
		google_shopping_gtin_other_sku1, google_shopping_gtin_other_sku2, 

		sunglasses_store_id1, sunglasses_store_id2,
		sunglasses_sku1, sunglasses_sku2, sunglasses_name1, sunglasses_name2, sunglasses_url1, sunglasses_url2, sunglasses_category1, sunglasses_category2, sunglasses_last_order_date, 

		glasses_store_id1, glasses_store_id2,
		glasses_sku1, glasses_sku2, glasses_name1, glasses_name2, glasses_url1, glasses_url2, glasses_category1, glasses_category2, glasses_last_order_date,

		last_product_type,
		last_product_sku1, last_product_sku2, last_product_name1, last_product_name2, last_product_url1, last_product_url2, last_product_last_order_date, 
		last_product_google_shopping_gtin_sku1, last_product_google_shopping_gtin_sku2, 
		last_order_lens_qty,
	
		idETLBatchRun_ins)

	select csdm_cu.idCustomer_sk_fk, 
		csdm_cu.client_id,
		csdm_cu.website_id, csdm_cu.store_id, csdm_cu.store_url,
		csdm_cu.email, 
		csdm_cu.title, csdm_cu.firstname,  csdm_cu.lastname, csdm_cu.gender, csdm_cu.dateofbirth, csdm_cu.language, csdm_cu.emvcellphone, 
		csdm_cu.currency, csdm_cu.country, 
		csdm_cu.datejoin, csdm_cu.dateunjoin, 
		csdm_cu.email_origine, csdm_cu.email_origin, csdm_cu.seed, csdm_cu.clienturn, csdm_cu.source, csdm_cu.code, 
		csdm_cu.product_id, 
		
		csdm_co.order_reminder_id, csdm_co.order_reminder_source, csdm_co.order_reminder_date, csdm_co.order_reminder_no, csdm_co.order_reminder_pref, csdm_co.order_reminder_product, csdm_co.order_reminder_reorder_url, 
		csdm_co.signed_up_to_autoreorder, csdm_co.date_of_autoreorder,

		csdm_co.prescription_expiry_date, csdm_co.prescription_wearer, csdm_co.prescription_optician, 
		csdm_co.bounce, csdm_co.card_expiry_date, 
		csdm_co.email_preferences, csdm_co.unsubscribe, csdm_co.website_unsubscribe, csdm_co.w_unsub_date, 
		csdm_co.trustpilot_service_review, csdm_co.trustpilot_service_review_rating, csdm_co.magento_product_review_rating,
		
		csdm_be.registration_date, csdm_be.first_order_date, 
		csdm_be.last_logged_in_date, csdm_be.last_order_date, csdm_be.last_order_id, csdm_be.last_order_source, csdm_be.last_order_del_date, csdm_be.num_of_orders, 
		csdm_be.aov, csdm_be.lifetime_spend, csdm_be.discount_code_used, csdm_be.num_of_referrals, 

		csdm_be.segment_lifecycle, csdm_be.segment_usage, csdm_be.segment_geog, csdm_be.segment_purch_behaviour, csdm_be.segment_eysight, csdm_be.segment_sport, csdm_be.segment_professional, csdm_be.segment_lifestage, csdm_be.segment_vanity, 
		csdm_be.segment, csdm_be.segment_2, csdm_be.segment_3, csdm_be.segment_4, 
		csdm_be.emvadmin1, csdm_be.emvadmin2, csdm_be.emvadmin3, csdm_be.emvadmin4, csdm_be.emvadmin5, 
		
		hashbytes('MD5', coalesce(csdm_cu.source, '') + coalesce(csdm_cu.email_origine, '') + coalesce(csdm_be.segment_lifecycle, '') + coalesce(cast(csdm_be.last_order_date AS varchar(20)), '') +
			coalesce(cast(csdm_co.website_unsubscribe AS varchar(10)), '') + coalesce(csdm_cu.country, '') + coalesce(cast(csdm_be.registration_date AS varchar(20)), '') + coalesce(csdm_be.segment_geog, '') +
			coalesce(csdm_cu.firstname, '') + coalesce(csdm_be.segment_2, '') + coalesce(csdm_be.segment_3, '') + coalesce(cast(csdm_co.w_unsub_date AS varchar(20)), '')) check_sum, 

		csdm_l.lens_sku1, csdm_l.lens_sku2, csdm_l.lens_name1, csdm_l.lens_name2, csdm_l.lens_url1, csdm_l.lens_url2, csdm_l.lens_last_order_date, 
		csdm_l.lens_google_shopping_gtin_sku1, csdm_l.lens_google_shopping_gtin_sku2,

		csdm_prod.dailies_store_id1, csdm_prod.dailies_store_id2,
		csdm_prod.dailies_sku1, csdm_prod.dailies_sku2, csdm_prod.dailies_name1, csdm_prod.dailies_name2, csdm_prod.dailies_url1, csdm_prod.dailies_url2, csdm_prod.dailies_last_order_date, 
		csdm_prod.google_shopping_gtin_daily_sku1, csdm_prod.google_shopping_gtin_daily_sku2,

		csdm_prod.monthlies_store_id1, csdm_prod.monthlies_store_id2,
		csdm_prod.monthlies_sku1, csdm_prod.monthlies_sku2, csdm_prod.monthlies_name1, csdm_prod.monthlies_name2, csdm_prod.monthlies_url1, csdm_prod.monthlies_url2, csdm_prod.monthlies_category1, csdm_prod.monthlies_category2, csdm_prod.monthlies_last_order_date, 
		csdm_prod.google_shopping_gtin_monthly_sku1, csdm_prod.google_shopping_gtin_monthly_sku2,

		csdm_prod.colours_store_id1, csdm_prod.colours_store_id2,
		csdm_prod.colours_sku1, csdm_prod.colours_sku2, csdm_prod.colours_name1, csdm_prod.colours_name2, csdm_prod.colours_url1, csdm_prod.colours_url2, csdm_prod.colours_last_order_date, 
		csdm_prod.google_shopping_gtin_colours_sku1, csdm_prod.google_shopping_gtin_colours_sku2,

		csdm_prod.solutions_store_id1, csdm_prod.solutions_store_id2,
		csdm_prod.solutions_sku1, csdm_prod.solutions_sku2, csdm_prod.solutions_name1, csdm_prod.solutions_name2, csdm_prod.solutions_url1, csdm_prod.solutions_url2, csdm_prod.solutions_category1, csdm_prod.solutions_category2, csdm_prod.solutions_last_order_date, 
		csdm_prod.google_shopping_gtin_solutions_sku1, csdm_prod.google_shopping_gtin_solutions_sku2, 

		csdm_prod.other_store_id1, csdm_prod.other_store_id2,
		csdm_prod.other_sku1, csdm_prod.other_sku2, csdm_prod.other_name1, csdm_prod.other_name2, csdm_prod.other_url1, csdm_prod.other_url2, csdm_prod.other_category1, csdm_prod.other_category2, csdm_prod.other_last_order_date, 
		csdm_prod.google_shopping_gtin_other_sku1, csdm_prod.google_shopping_gtin_other_sku2, 

		csdm_prod.sunglasses_store_id1, csdm_prod.sunglasses_store_id2,
		csdm_prod.sunglasses_sku1, csdm_prod.sunglasses_sku2, csdm_prod.sunglasses_name1, csdm_prod.sunglasses_name2, csdm_prod.sunglasses_url1, csdm_prod.sunglasses_url2, csdm_prod.sunglasses_category1, csdm_prod.sunglasses_category2, csdm_prod.sunglasses_last_order_date, 

		csdm_prod.glasses_store_id1, csdm_prod.glasses_store_id2,
		csdm_prod.glasses_sku1, csdm_prod.glasses_sku2, csdm_prod.glasses_name1, csdm_prod.glasses_name2, csdm_prod.glasses_url1, csdm_prod.glasses_url2, csdm_prod.glasses_category1, csdm_prod.glasses_category2, csdm_prod.glasses_last_order_date,

		csdm_l.last_product_type,
		csdm_l.last_product_sku1, csdm_l.last_product_sku2, csdm_l.last_product_name1, csdm_l.last_product_name2, csdm_l.last_product_url1, csdm_l.last_product_url2, csdm_l.last_product_last_order_date, 
		csdm_l.last_product_google_shopping_gtin_sku1, csdm_l.last_product_google_shopping_gtin_sku2, 
		csdm_l.last_order_lens_qty, 
		1 idETLBatchRun
	from 
			Warehouse.dm.fact_customer_signature_dm_aux_cust csdm_cu
		inner join
			Warehouse.dm.fact_customer_signature_dm_aux_comm csdm_co on csdm_cu.idCustomer_sk_fk = csdm_co.idCustomer_sk_fk
		inner join
			Warehouse.dm.fact_customer_signature_dm_aux_behaviour csdm_be on csdm_cu.idCustomer_sk_fk = csdm_be.idCustomer_sk_fk
		inner join
			Warehouse.dm.fact_customer_signature_dm_aux_last csdm_l on csdm_cu.idCustomer_sk_fk = csdm_l.idCustomer_sk_fk
		inner join
			Warehouse.dm.fact_customer_signature_dm_aux_product csdm_prod on csdm_cu.idCustomer_sk_fk = csdm_prod.idCustomer_sk_fk


	---------------------------------------------------------
	---------------------------------------------------------

	exec dm.dwh_dwh_merge_dm_customer_signature_dm @idETLBatchRun = 1, @idPackageRun = 1
	