use lad_lex;


update lad_lex.lad_colour_map
set magento_colour = replace(replace(magento_colour, '\n', ''), '\r', '');


update lad_lex.lad_product_map
set magento_id = 3130, magento_packsize = 1
where lad_id = 106;

update lad_lex.lex_product_map
set magento_id = 3130, magento_packsize = 1
where lex_id = 106;

-- ----------------------------------------------------------------
-- ----------------------------------------------------------------
-- ----------------------------------------------------------------

-- migrate_customerdata

  -- old_customer_id: OK
  -- DomainID, website_group_id: Steve O
  -- billing_region: String OK, billing_region_id, billing_country_id: Try to match one??
  -- shipping_region: String OK, shipping_region_id, shipping_country_id: Try to match one??
  -- unsubscribe_newsletter: 0, 1 to N/Y ??
  
  -- data truncations on cus_phone, email
  
delete from migrate_customerdata;

insert into migrate_customerdata (`old_customer_id`, 
  `email`, `password`, `created_at`, 
  
  `store_id`, `DomainID`, `DomainName`, `website_group_id`, old_customer_id_prefix,
  
  `firstname`, `lastname`, 
  billing_company, `billing_prefix`, `billing_firstname`, `billing_lastname`, 
  `billing_street1`, `billing_street2`, `billing_city`, `billing_region`, `billing_region_id`, `billing_postcode`, 
  `billing_country_id`, `billing_telephone`, 
  shipping_company, `shipping_prefix`, `shipping_firstname`, `shipping_lastname`,
  `shipping_street1`, `shipping_street2`, `shipping_city`, `shipping_region`, `shipping_region_id`, `shipping_postcode`,
  `shipping_country_id`, `shipping_telephone`,
  `cus_phone`, default_billing, default_shipping,
  unsubscribe_newsletter, 
  store_credit_amount)
  
select id_usuari AS `old_customer_id`, 
  login AS `email`, password AS `password`, data_alta AS `created_at`, 
  
  23 AS `store_id`, 
  case when source = 'lad' then 50 else 51 end AS `DomainID`, 
  case when source = 'lad' then 'lentillasadomicilio.com' else 'lentiexpress.es' end AS `DomainName`, 
  6 AS `website_group_id`, 
  case when source = 'lad' then 'LAD' else 'LEX' end AS `old_customer_id_prefix`, -- previous LED
  
  facturacio_nom AS `firstname`, concat(facturacio_cognom1, ' ', facturacio_cognom2) AS `lastname`, 
  NULL billing_company, '-' AS `billing_prefix`, facturacio_nom AS `billing_firstname`, concat(facturacio_cognom1, ' ', facturacio_cognom2) AS `billing_lastname`, 
  facturacio_adreca AS `billing_street1`, null AS `billing_street2`, facturacio_poblacio AS `billing_city`, facturacio_provincia AS `billing_region`, 
    coalesce(dcr1.region_id, 485) `billing_region_id`, facturacio_codipostal AS `billing_postcode`, 
    facturacio_pais_cod `billing_country_id`, facturacio_telefon AS `billing_telephone`, 
  NULL shipping_company, '-' AS `shipping_prefix`, enviament_nom AS `shipping_firstname`, concat(enviament_cognom1, ' ', enviament_cognom2) AS `shipping_lastname`,
  enviament_adreca AS `shipping_street1`, null AS `shipping_street2`, enviament_poblacio AS `shipping_city`, enviament_provincia AS `shipping_region`, 
    coalesce(dcr2.region_id, 485) `shipping_region_id`, enviament_codipostal AS `shipping_postcode`,
    case
      when enviament_pais in (1, 2, 3) then 'ES'
      when enviament_pais in (4) then 'PT'
      else facturacio_pais_cod
    end `shipping_country_id`, 
    enviament_telefon AS `shipping_telephone`,
  facturacio_telefon AS `cus_phone`, NULL default_billing, id_usuari default_shipping,
  accepta_mail unsubscribe_newsletter, 
  0 store_credit_amount
from 
    lad_lex.cfg_usuaris_v u
  left join
    magento01.directory_country_region dcr1 on u.facturacio_provincia = dcr1.code and dcr1.country_id = 'ES'
  left join
    magento01.directory_country_region dcr2 on u.enviament_provincia = dcr2.code and dcr2.country_id = 'ES';

-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------

-- migrate_orderdata
  -- increment_id
  -- shipping_description, base_shipping_method
  -- billing_region: String OK, billing_region_id, billing_country_id: Try to match one??
  -- shipping_region: String OK, shipping_region_id, shipping_country_id: Try to match one??
  
  -- Data Truncation on website_name

delete from migrate_orderdata;

insert into migrate_orderdata (`old_order_id`, 
  created_at, 
  `store_id`, `website_name`, `DomainID`, `DomainName`, 
  `email`, `old_customer_id`, 
  
  billing_company, billing_prefix, `billing_firstname`, `billing_lastname`, 
  `billing_street1`, `billing_street2`, `billing_city`, `billing_region`, `billing_region_id`, `billing_postcode`, 
  `billing_country`, `billing_telephone`, 
  
  shipping_company, `shipping_prefix`, `shipping_firstname`, `shipping_lastname`,
  `shipping_street1`, `shipping_street2`, `shipping_city`, `shipping_region`, `shipping_region_id`, `shipping_postcode`,
  `shipping_country`, `shipping_telephone`,
  
  `shipping_description`, base_shipping_method, reminder_type,
  
  base_grand_total, base_shipping_amount, base_discount_amount, 
  order_currency)

select id_comanda `old_order_id`, 
  data_alta created_at, 
  23 AS `store_id`, 'lentillasadomicilio.com' AS `website_name`, 50 AS `DomainID`, 'lentillasadomicilio.com' AS `DomainName`, 
  cd.email `email`, cd.old_customer_id `old_customer_id`, 
  
  NULL billing_company, '-' AS `billing_prefix`, facturacio_nom AS `billing_firstname`, concat(facturacio_cognom1, ' ', facturacio_cognom2) AS `billing_lastname`, 
  facturacio_adreca AS `billing_street1`, null AS `billing_street2`, facturacio_poblacio AS `billing_city`, facturacio_provincia AS `billing_region`, 
    coalesce(dcr1.region_id, 485) `billing_region_id`, facturacio_codipostal AS `billing_postcode`, 
    facturacio_pais_cod `billing_country`, facturacio_telefon AS `billing_telephone`, 
  NULL shipping_company, '-' AS `shipping_prefix`, enviament_nom AS `shipping_firstname`, enviament_cognom2 AS `shipping_lastname`,
  enviament_adreca AS `shipping_street1`, null AS `shipping_street2`, enviament_poblacio AS `shipping_city`, enviament_provincia AS `shipping_region`, 
    coalesce(dcr2.region_id, 485) `shipping_region_id`, enviament_codipostal AS `shipping_postcode`,
    case
      when left(enviament_pais, 1) in (1, 2, 3) then 'ES'
      when left(enviament_pais, 1) in (4) then 'PT'
      else facturacio_pais_cod
    end `shipping_country`, enviament_telefon AS `shipping_telephone`,
  case when (metode_enviament = 1) then 'MRW' when (metode_enviament = 2) then 'GLS' else null end `shipping_description`, NULL base_shipping_method, 
  'none' reminder_type,
  
  preu_total_comanda base_grand_total, import_enviament + import_contrareemborsament base_shipping_amount, import_descompte base_discount_amount, 
  'EUR' order_currency
  
from 
    lad_lex.slg_comandes_lad_v c
  inner join
    migrate_customerdata cd on c.id_usuari = cd.old_customer_id
  left join
    magento01.directory_country_region dcr1 on c.facturacio_provincia = dcr1.code and dcr1.country_id = 'ES'
  left join
    magento01.directory_country_region dcr2 on c.enviament_provincia = dcr2.code and dcr2.country_id = 'ES'    
where pagat = 1;


insert into migrate_orderdata (`old_order_id`, 
  created_at, 
  `store_id`, `website_name`, `DomainID`, `DomainName`, 
  `email`, `old_customer_id`, 
  
  billing_company, billing_prefix, `billing_firstname`, `billing_lastname`, 
  `billing_street1`, `billing_street2`, `billing_city`, `billing_region`, `billing_region_id`, `billing_postcode`, 
  `billing_country`, `billing_telephone`, 
  
  shipping_company, `shipping_prefix`, `shipping_firstname`, `shipping_lastname`,
  `shipping_street1`, `shipping_street2`, `shipping_city`, `shipping_region`, `shipping_region_id`, `shipping_postcode`,
  `shipping_country`, `shipping_telephone`,
  
  `shipping_description`, base_shipping_method, 
  reminder_type,
  
  base_grand_total, base_shipping_amount, base_discount_amount, 
  order_currency)

select id_comanda `old_order_id`, 
  data_alta created_at, 
  23 AS `store_id`, 'lentiexpress.es' AS `website_name`, 51 AS `DomainID`, 'lentiexpress.es' AS `DomainName`, 
  cd.email `email`, cd.old_customer_id `old_customer_id`, 
  
  NULL billing_company, '-' AS `billing_prefix`, facturacio_nom AS `billing_firstname`, concat(facturacio_cognom1, ' ', facturacio_cognom2) AS `billing_lastname`, 
  facturacio_adreca AS `billing_street1`, null AS `billing_street2`, facturacio_poblacio AS `billing_city`, facturacio_provincia AS `billing_region`, 
    coalesce(dcr1.region_id, 485) `billing_region_id`, facturacio_codipostal AS `billing_postcode`, 
    facturacio_pais_cod `billing_country`, facturacio_telefon AS `billing_telephone`, 
  NULL shipping_company, '-' AS `shipping_prefix`, enviament_nom AS `shipping_firstname`, enviament_cognom2 AS `shipping_lastname`,
  enviament_adreca AS `shipping_street1`, null AS `shipping_street2`, enviament_poblacio AS `shipping_city`, enviament_provincia AS `shipping_region`, 
    coalesce(dcr2.region_id, 485) `shipping_region_id`, enviament_codipostal AS `shipping_postcode`,
    case
      when left(enviament_pais, 1) in (1, 2, 3) then 'ES'
      when left(enviament_pais, 1) in (4) then 'PT'
      else facturacio_pais_cod
    end `shipping_country`, enviament_telefon AS `shipping_telephone`,
  case when (metode_enviament = 1) then 'MRW' when (metode_enviament = 2) then 'GLS' else null end `shipping_description`, NULL base_shipping_method, 
  'none' reminder_type,
  
  preu_total_comanda base_grand_total, import_enviament + import_contrareemborsament base_shipping_amount, import_descompte base_discount_amount, 
  'EUR' order_currency
  
from 
    lad_lex.slg_comandes_lex_v c
  inner join
    migrate_customerdata cd on c.id_usuari = cd.old_customer_id
  left join
    magento01.directory_country_region dcr1 on c.facturacio_provincia = dcr1.code and dcr1.country_id = 'ES'
  left join
    magento01.directory_country_region dcr2 on c.enviament_provincia = dcr2.code and dcr2.country_id = 'ES'        
where pagat = 1;

-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------

-- migrate_orderlinedata

  -- 'add', dominant
  -- color
  -- packsize + quantity

delete from migrate_orderlinedata;

insert into migrate_orderlinedata (old_item_id, 
  old_order_id, 
  store_id, 
  migrated, priority, debug, params_hash, 
  old_product_id, product_id, productCode, productName, productCodeFull, packsize, 
  eye, power, base_curve, diameter, `add`, axis, cylinder, dominant, color, params, -- add
  quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
  quantity, row_total)
  
  select ci.id_comanda_detall old_item_id, 
    ci.id_comanda old_order_id, 
    23 store_id, 
    0 AS `migrated`, 0 AS `priority`, NULL AS `debug`, NULL AS `params_hash`, 
    
    id_item old_product_id, case when (magento_id = '') then 2327 else magento_id end product_id, 
    null productCode, nom_es productName, null productCodeFull, lad_packsize packsize, 
    case when (ojo = 1) then 'Left' when (ojo = 2) then 'Right' else null end eye, 
    case when (ci.esfera is null) then ci.esfera_lejos else ci.esfera end power, ci.curvabase base_curve, ci.diametro diameter, 
    ci.adicion_str 'add', ci.eje axis, ci.cilindro cylinder, 
    case 
      when (ci.adicion_dn = 1) then 'D' 
      when (ci.adicion_dn = 2) then 'N' 
      else null
      end dominant, 
    -- ci.color_str color, 
    cm.magento_colour color,
    null params,
    
    null quote_item_id, null quote_sku, null quote_is_lens, null quote_data_built, null quote_opt,
    ci.quantitat * ci.lad_packsize quantity, ci.preu_subtotal / ci.num_rows row_total  
  from 
      lad_lex.slg_comandes_items_lad_v ci
    left join
      lad_lex.lad_colour_map cm on ci.color_str = cm.lad_colour      
  where ci.pagat = 1;    
    -- inner join
      -- lad_lex.migrate_orderdata o on ci.id_comanda = o.old_order_id;  
      
insert into migrate_orderlinedata (old_item_id, 
  old_order_id, 
  store_id, 
  migrated, priority, debug, params_hash, 
  old_product_id, product_id, productCode, productName, productCodeFull, packsize, 
  eye, power, base_curve, diameter, `add`, axis, cylinder, dominant, color, params, -- add
  quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
  quantity, row_total)
  
  
  select ci.id_comanda_detall old_item_id, 
    ci.id_comanda old_order_id, 
    23 store_id, 
    0 AS `migrated`, 0 AS `priority`, NULL AS `debug`, NULL AS `params_hash`, 
    
    id_item old_product_id, case when (magento_id = '') then 2327 else magento_id end product_id, 
    null productCode, nom_es productName, null productCodeFull, lex_packsize packsize, 
    case when (ojo = 1) then 'Left' when (ojo = 2) then 'Right' else null end eye, 
    case when (ci.esfera is null) then ci.esfera_lejos else ci.esfera end power, ci.curvabase base_curve, ci.diametro diameter, 
    ci.adicion_str 'add', ci.eje axis, ci.cilindro cylinder, 
    case 
      when (ci.adicion_dn = 1) then 'D' 
      when (ci.adicion_dn = 2) then 'N' 
      else null
      end dominant, 
    -- ci.color_str color, 
    cm.magento_colour color, 
    null params,
    null quote_item_id, null quote_sku, null quote_is_lens, null quote_data_built, null quote_opt,
    ci.quantitat * ci.lex_packsize quantity, ci.preu_subtotal / ci.num_rows row_total  
  from 
      lad_lex.slg_comandes_items_lex_v ci
    left join
      lad_lex.lad_colour_map cm on ci.color_str = cm.lad_colour
  where ci.pagat = 1;    
    -- inner join
      -- lad_lex.migrate_orderdata o on ci.id_comanda = o.old_order_id;        
  