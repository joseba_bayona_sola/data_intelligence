
-- update overlap customers
UPDATE magento01.customer_entity t1, magento01_target.migrate_customerdata t2
SET t2.new_magento_id = t1.entity_id
WHERE t1.email = t2.email AND t1.website_group_id = t2.website_group_id;

-- 

-- set lifceycle from magento only records 
UPDATE magento01_target.migrate_customerdata t1,
  (SELECT customer_id, MAX(created_at) last_order_date
  FROM `magento01`.`sales_flat_order`
  -- WHERE STATUS <> 'archived' dont usally do this but duplicate make this a useful fix
  GROUP BY customer_id) t2
SET 
  t1.`hist_last_order_date` = t2.last_order_date,
  t1.`hist_customer_lifecycle` =
    CASE
      WHEN t2.last_order_date IS NULL THEN 'RNB'
      WHEN TIMESTAMPDIFF(MONTH, t2.last_order_date, NOW()) > 15 THEN 'LAPSED'
      ELSE 'ACTIVE'
    END
WHERE t1.`new_magento_id` = t2.customer_id;

 -- set lifecycle for historical db pre magento records
UPDATE magento01_target.migrate_customerdata t1,
  `dw_flat`.`hist_last_order_date` t2
SET 
  t1.`hist_last_order_date` = t2.last_order_date,
  t1.`hist_customer_lifecycle` =
    CASE
      WHEN t2.last_order_date IS NULL THEN 'RNB'
      WHEN TIMESTAMPDIFF(MONTH, t2.last_order_date, NOW()) > 15 THEN 'LAPSED' ELSE 'ACTIVE' END
WHERE t1.`new_magento_id` = t2.customer_id;

-- get final date / version for lifecyvle
UPDATE `magento01_target`.`migrate_customerdata` t1
SET 
  `vd_last_order_date` =
    CASE WHEN IFNULL(magento_last_order_date, '0000-00-00') > IFNULL(hist_last_order_date, '0000-00-00') THEN magento_last_order_date ELSE hist_last_order_date END,
  vd_customer_lifecycle =
    CASE WHEN IFNULL(magento_last_order_date, '0000-00-00') > IFNULL(hist_last_order_date, '0000-00-00') THEN magento_customer_lifecycle ELSE hist_customer_lifecycle END;
    
--  active vd customer should be left alone
UPDATE `magento01_target`.`migrate_customerdata` t1
SET migrated = 1
WHERE vd_customer_lifecycle IS NOT NULL AND vd_customer_lifecycle IN ('ACTIVE');

-- lapsed in both then keep vd
UPDATE `magento01_target`.`migrate_customerdata` t1
SET migrated = 1
WHERE vd_customer_lifecycle IS NOT NULL AND vd_customer_lifecycle IN ('LAPSED', 'RNB') AND `old_customer_lifecycle` IN ('LAPSED', 'RNB');    