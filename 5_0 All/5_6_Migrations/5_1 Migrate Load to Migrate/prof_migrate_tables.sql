
select *
from migrate_customerdata
limit 1000

select old_customer_id, new_magento_id, new_magento_id_bck, migrated,
  email, password, created_at,
  store_id, domainID, domainName, website_group_id, old_customer_id_prefix,
  firstname, lastname,
  billing_company, billing_prefix, billing_firstname, billing_lastname, 
  billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country_id, billing_telephone,
  shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country_id, shipping_telephone,
  cus_phone, default_billing, default_shipping,
  unsubscribe_newsletter, disable_saved_card, RAF_code, is_registered, priority,
  mage_last_order_date, import_last_order_date, old_last_order_date, old_customer_lifecycle, 
  hist_last_order_date, hist_customer_lifecycle, magento_last_order_date, magento_customer_lifecycle, vd_last_order_date, vd_customer_lifecycle,
  store_credit_amount, store_credit_comment, target_store_credit_amount
from migrate_customerdata
-- from lensway_nl.migrate_customerdata
order by created_at desc
limit 1000

  select count(*)
  from migrate_customerdata

  select count(*)
  from lensway_nl.migrate_customerdata

  select store_id, domainID, domainName, website_group_id, old_customer_id_prefix, count(*)
  from lensway_nl.migrate_customerdata
  group by store_id, domainID, domainName, website_group_id, old_customer_id_prefix
  order by store_id, domainID, domainName, website_group_id, old_customer_id_prefix

  select migrated, count(*)
  from lensway_nl.migrate_customerdata
  group by migrated
  order by migrated  
  
-- 

select *
from migrate_orderdata
limit 1000

select old_order_id, 
  magento_order_id, new_increment_id, created_at, 
  store_id, website_name, DomainID, domainName, 
  email, old_customer_id, magento_customer_id, 
  migrated, 
  billing_company, billing_prefix, billing_firstname, billing_lastname, 
  billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_country, billing_telephone,
  shipping_company, shipping_prefix, shipping_firstname, shipping_lastname, 
  shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_telephone,
  shipping_description, base_shipping_method, 
  base_grand_total, base_shipping_amount, base_discount_amount, 
  order_currency, 
  comment, priority, 
  reminder_mobile, reminder_date, reminder_type, 
  increment_id, BASE_ORDER_ID
from migrate_orderdata
limit 1000

  select count(*)
  from migrate_orderdata

  select count(*)
  from lensway_nl.migrate_orderdata

  select store_id, website_name, DomainID, domainName, count(*)
  from lensway_nl.migrate_orderdata  
  group by store_id, website_name, DomainID, domainName
  order by store_id, website_name, DomainID, domainName

  select shipping_description, base_shipping_method, count(*)
  from lensway_nl.migrate_orderdata  
  group by shipping_description, base_shipping_method
  order by shipping_description, base_shipping_method

  select reminder_type, count(*)
  from lensway_nl.migrate_orderdata  
  group by reminder_type
  order by reminder_type
  
-- 

select *
from migrate_orderlinedata
limit 1000

select old_item_id, 
  old_order_id, magento_order_id, 
  store_id, 
  migrated, priority, debug, params_hash, 
  old_product_id, product_id, productCode, productName, productCodeFull, packsize, 
  eye, power, base_curve, diameter, 'add', axis, cylinder, dominant, color, params, -- add
  quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
  quantity, row_total,
  backup_add
from migrate_orderlinedata
limit 1000

  select count(*)
  from migrate_orderlinedata

  select count(*)
  from lensway_nl.migrate_orderlinedata

  select product_id, old_product_id, productCode, productName, productCodeFull, packsize, count(*)
  from lensway_nl.migrate_orderlinedata
  where product_id not in (2327, 3128, 3129, 3130, 3131, 3134, 3135, 3136, 3137, 3138, 3139, 3140, 3141, 3153, 3154)
  group by product_id, old_product_id, productCode, productName, productCodeFull, packsize
  order by product_id, old_product_id, productCode, productName, productCodeFull, packsize
  
-- 

select *
from migrate_customerdata_address
limit 1000

select old_address_id, old_customer_id, 
  created_at, updated_at, 
  firstname, lastname, company, 
  street1, street2, city, region, region_id, postcode, country_id, telephone
from migrate_customerdata_address
limit 1000

  select count(*)
  from migrate_customerdata_address 