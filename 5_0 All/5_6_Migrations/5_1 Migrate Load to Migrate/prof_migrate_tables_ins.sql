
-- migrate_customerdata
select `customers_id` AS `old_customer_id`, NULL new_magento_id_bck, NULL migrated,
  `customers_email_address` AS `email`, `customers_password` AS `password`, `customers_info_date_account_created` AS `created_at`, 
  22 AS `store_id`, 13 AS `DomainID`, 'lenson.nl' AS `DomainName`, 6 AS `website_group_id`, NULL old_customer_id_prefix,
  NULL billing_company, '-' AS `billing_prefix`, `customers_firstname` AS `firstname`, `customers_lastname` AS `lastname`, 
  `customers_firstname` AS `billing_firstname`, `customers_lastname` AS `billing_lastname`, 
  `entry_street_address` AS `billing_street1`, `entry_street_address_2` AS `billing_street2`, `entry_city` AS `billing_city`, `entry_city_2` AS `billing_region`, 
    NULL AS `billing_region_id`, `entry_postcode` AS `billing_postcode`, 
    (case `customers`.`entry_country` when 'Nederland' then 'NL' when 'Belgium' then 'BE' else 'NL' end) AS `billing_country_id`, `customers_telephone` AS `billing_telephone`, 
  NULL shipping_company, '-' AS `shipping_prefix`, `customers_firstname` AS `shipping_firstname`, `customers_lastname` AS `shipping_lastname`,
  `entry_street_address` AS `shipping_street1`, `entry_street_address_2` AS `shipping_street2`, `entry_city` AS `shipping_city`, `entry_city_2` AS `shipping_region`, 
    NULL AS `shipping_region_id`, `entry_postcode` AS `shipping_postcode`,
    (case `customers`.`entry_country` when 'Nederland' then 'NL' when 'Belgium' then 'BE' else 'NL' end) AS `shipping_country_id`,`customers_telephone` AS `shipping_telephone`,
  `customers_telephone` AS `cus_phone`, NULL default_billing, NULL default_shipping,
  NULL unsubscribe_newsletter, NULL disable_saved_card, NULL RAF_code, NULL is_registered, priority,
  NULL mage_last_order_date, NULL import_last_order_date,
  NULL old_last_order_date, NULL old_customer_lifecycle, NULL hist_last_order_date, NULL hist_customer_lifecycle, NULL magento_last_order_date, NULL magento_customer_lifecycle, NULL vd_last_order_date, NULL vd_customer_lifecycle,    
  `customers`.`customers_discount` AS `store_credit_amount`, NULL store_credit_comment, NULL target_store_credit_amount 
from `customers` 

  -- remove invalid emails 
  DELETE FROM magento01_target.migrate_customerdata WHERE `email` NOT REGEXP '^[^@]+@[^@]+\.[^@]{2,}$';

  -- get rid of duplicates and relink the old records
    -- _tmp_dup_customer
    SELECT email, MAX(old_customer_id) customer_id -- TODO : change this to min to deal with new duplicates
    FROM magento01_target.`migrate_customerdata`
    GROUP BY email
    HAVING COUNT(*) > 1;

    -- _tmp_removed_dup_customers
    SELECT t1.email, t2.old_customer_id customer_id, t1.customer_id target_customer_id
    FROM 
        magento01_target._tmp_dup_customer t1 
      INNER JOIN 
        magento01_target.`migrate_customerdata` t2 ON t1.email = t2.email
    WHERE t1.customer_id != t2.old_customer_id;
  
    UPDATE `magento01_target`._tmp_removed_dup_customers t1, `magento01_target`.`migrate_orderdata` t2
    SET t2.old_customer_id = t1.target_customer_id
    WHERE t1.customer_id = t2.old_customer_id;

    DELETE FROM magento01_target.migrate_customerdata
    WHERE email IN (SELECT email FROM magento01_target._tmp_dup_customer)
       AND old_customer_id NOT IN (SELECT customer_id FROM magento01_target._tmp_dup_customer);  
       
  -- fix customer address when fields are missnig
 
  UPDATE magento01_target.migrate_customerdata
  SET billing_street1 = billing_street2, billing_street2 = ''
  WHERE CHAR_LENGTH(billing_street1) = 0;

  UPDATE magento01_target.migrate_customerdata
  SET billing_city = billing_region, billing_region = ''
  WHERE CHAR_LENGTH(billing_city) = 0;
  
  UPDATE magento01_target.migrate_customerdata
  SET shipping_street1 = shipping_street2, shipping_street2 = ''
  WHERE CHAR_LENGTH(shipping_street1) = 0;

  UPDATE magento01_target.migrate_customerdata
  SET shipping_city = shipping_region, shipping_region = ''
  WHERE CHAR_LENGTH(shipping_city) = 0;     
  
-- ------------------------------------------------------------------------------------

-- migrate_orderdata
select `lenson_latest`.`orders`.`orders_id` AS `old_order_id`, 
  null magento_order_id, null new_increment_id, `lenson_latest`.`orders`.`date_purchased` AS `created_at`, 
  22 AS `store_id`, 'lenson.nl' AS `website_name`, 13 AS `DomainID`, 'lenson.nl' AS `DomainName`, 
  `lenson_latest`.`orders`.`customers_email_address` AS `email`, `lenson_latest`.`orders`.`customers_id` AS `old_customer_id`, null magento_customer_id,
  null migrated, 
  
  
  NULL AS `billing_company`, '-' AS `billing_prefix`, `lenson_latest`.`orders`.`customers_name` AS `billing_firstname`, `lenson_latest`.`orders`.`customers_name_2` AS `billing_lastname`, 
  `lenson_latest`.`orders`.`customers_street_address` AS `billing_street1`, `lenson_latest`.`orders`.`customers_street_address_2` AS `billing_street2`, 
  `lenson_latest`.`orders`.`customers_city` AS `billing_city`, `lenson_latest`.`orders`.`customers_city_2` AS `billing_region`, NULL AS `billing_region_id`, 
  `lenson_latest`.`orders`.`customers_postcode` AS `billing_postcode`, 
  (case `lenson_latest`.`customers`.`entry_country` when 'Nederland' then 'NL' when 'Belgium' then 'BE' else 'NL' end) AS `billing_country`, 
  `lenson_latest`.`orders`.`customers_telephone` AS `billing_telephone`, 
  
  NULL AS `shipping_company`, '-' AS `shipping_prefix`, `lenson_latest`.`orders`.`customers_name` AS `shipping_firstname`, `lenson_latest`.`orders`.`customers_name_2` AS `shipping_lastname`, 
  `lenson_latest`.`orders`.`customers_street_address` AS `shipping_street1`, `lenson_latest`.`orders`.`customers_street_address_2` AS `shipping_street2`, 
  `lenson_latest`.`orders`.`customers_city` AS `shipping_city`, `lenson_latest`.`orders`.`customers_city_2` AS `shipping_region`, NULL AS `shipping_region_id`, 
  `lenson_latest`.`orders`.`customers_postcode` AS `shipping_postcode`, 
  (case `lenson_latest`.`customers`.`entry_country` when 'Nederland' then 'NL' when 'Belgium' then 'BE' else 'NL' end) AS `shipping_country`, 
  `lenson_latest`.`orders`.`customers_telephone` AS `shipping_telephone`, 
  
  `lenson_latest`.`orders`.`delivery_method` AS `shipping_description`, NULL base_shipping_method,
  ifnull(`order_totals_grand_total`.`value`,0) AS `base_grand_total`, 
  ifnull(`order_totals_shipping_sub`.`VALUE`,0) AS `base_shipping_amount`, 
  ifnull(`order_totals_discount_sub`.`VALUE`,0) AS `base_discount_amount`,
  `lenson_latest`.`orders`.`currency` AS `order_currency`,
  
  NULL AS `Comment`, NULL priority,
  `lenson_latest`.`orders`.`customers_telephone` AS `reminder_mobile`, `reminders_sub`.`reminder_date` AS `reminder_date`,
  case when (`reminders_sub`.`reminder_date` is not null) then 'email' else 'none' end AS `reminder_type`, 
  `lenson_latest`.`orders`.`orders_id` AS `increment_id`, NULL BASE_ORDER_ID  

from 
    `lenson_latest`.`orders` 
  left join 
    `lenson_latest`.`customers` on `lenson_latest`.`orders`.`customers_id` = `lenson_latest`.`customers`.`customers_id` 
  left join 
    (select `lenson_latest`.`order_totals`.`orders_id` AS `orders_id`,abs(sum(`lenson_latest`.`order_totals`.`value`)) AS `VALUE` 
    from `lenson_latest`.`order_totals` 
    where `lenson_latest`.`order_totals`.`class` in ('ot_shipping','ot_payment_chg') 
    group by `lenson_latest`.`order_totals`.`orders_id`) `order_totals_shipping_sub` on `order_totals_shipping_sub`.`orders_id` = `lenson_latest`.`orders`.`orders_id` 
  left join 
    `lenson_latest`.`order_totals` `order_totals_grand_total` 
      on `lenson_latest`.`orders`.`orders_id` = `order_totals_grand_total`.`orders_id` and `order_totals_grand_total`.`class` = 'ot_total' 
  left join 
    (select `lenson_latest`.`order_totals`.`orders_id` AS `orders_id`,-(abs(sum(`lenson_latest`.`order_totals`.`value`))) AS `VALUE` 
    from `lenson_latest`.`order_totals` 
    where `lenson_latest`.`order_totals`.`class` in ('ot_campaign_discount','ot_discount','ot_custom') 
    group by `lenson_latest`.`order_totals`.`orders_id`) `order_totals_discount_sub` on `order_totals_discount_sub`.`orders_id` = `lenson_latest`.`orders`.`orders_id` 
  left join 
    (select `lenson_latest`.`reminders`.`orders_id` AS `orders_id`,max(`lenson_latest`.`reminders`.`desired_delivery`) AS `reminder_date` 
    from `lenson_latest`.`reminders` 
    group by `lenson_latest`.`reminders`.`orders_id`) `reminders_sub` on `reminders_sub`.`orders_id` = `lenson_latest`.`orders`.`orders_id`

where (`lenson_latest`.`orders`.`shipped` is not null) 

  -- add a limti for reminder to avoid sending from both systems
  UPDATE  magento01_target.migrate_orderdata  SET  reminder_type = 'none' WHERE CAST(reminder_date  AS DATE) <= '2017.10.05';

  -- delete records attahced to 
  DELETE FROM magento01_target.migrate_orderdata 
  WHERE old_customer_id NOT IN (SELECT old_customer_id FROM magento01_target.migrate_customerdata);

  -- fix totals
  UPDATE magento01_target.migrate_orderdata LEFT JOIN (
      SELECT old_order_id, SUM(row_total) row_subtotal
      FROM migrate_orderlinedata
      GROUP BY old_order_id) row_agg ON row_agg.old_order_id = migrate_orderdata.old_order_id 
  SET 
  	base_discount_amount =
  	  CASE WHEN (ABS(base_discount_amount) - ((base_grand_total + ABS(base_discount_amount) - base_shipping_amount) - row_agg.row_subtotal)) > 0 
  	    THEN -((base_discount_amount) +  (ABS(base_discount_amount) -  ((base_grand_total + ABS(base_discount_amount) - base_shipping_amount) - row_agg.row_subtotal)))
  	  ELSE 0 END,
  	base_shipping_amount = 
  	  CASE WHEN (ABS(base_discount_amount) - ((base_grand_total + ABS(base_discount_amount) - base_shipping_amount) - row_agg.row_subtotal)) < 0 
  	    THEN ABS(base_shipping_amount) + ABS(ABS(base_discount_amount) -  ((base_grand_total + ABS(base_discount_amount) - base_shipping_amount) - row_agg.row_subtotal))
  	  ELSE 0 END 
  WHERE (base_grand_total + ABS(base_discount_amount) - base_shipping_amount) <> row_subtotal;

  -- fix order address fields are missing
  UPDATE magento01_target.migrate_orderdata
  SET billing_street1 = billing_street2, billing_street2 = ''
  WHERE CHAR_LENGTH(billing_street1) = 0;

  UPDATE magento01_target.migrate_orderdata
  SET billing_city = billing_region, billing_region = ''
  WHERE CHAR_LENGTH(billing_city) = 0;
  		
  UPDATE magento01_target.migrate_orderdata
  SET shipping_street1 = shipping_street2, shipping_street2 = ''
  WHERE CHAR_LENGTH(shipping_street1) = 0;

  UPDATE magento01_target.migrate_orderdata
  SET shipping_city = shipping_region, shipping_region = ''
  WHERE CHAR_LENGTH(shipping_city) = 0;
  
  -- update shipping description when missing
  UPDATE magento01_target.migrate_orderdata SET  shipping_description = 'Standard Delivery'
  WHERE shipping_description IS NULL;  

-- ------------------------------------------------------------------------------------


-- migrate_orderlinedata
select `order_products`.`orders_products_id` AS `old_item_id`,
  `order_products`.`orders_id` AS `old_order_id`, NULL AS `magento_order_id`,
  22 AS `store_id`,
  0 AS `migrated`, 0 AS `priority`, NULL AS `debug`, NULL AS `params_hash` 
  
  `order_products`.`products_id` AS `old_product_id`, `product_mapping`.`magento_product_id` AS `product_id`,
  `order_products`.`info` AS `ProductCode`, `order_products`.`products_name` AS `ProductName`, 
  NULL AS `ProductCodeFull`, `product_mapping`.`magento_packsize` AS `packsize`, 
   
  `order_products`.`EYE` AS `eye`, 
  `order_products`.`PWR` AS `power`, `order_products`.`BC` AS `base_curve`, `order_products`.`DIA` AS `diameter`,
  `order_products`.`X-PWR` AS `add`, `order_products`.`AXIS` AS `axis`, `order_products`.`CYL` AS `cylinder`, `order_products`.`X-PWR2` AS `dominant`, 
  `order_products`.`COLOR` AS `color`, NULL AS `params`,
   
  NULL AS `quote_item_id`, NULL AS `quote_sku`, NULL AS `quote_is_lens`, 0 AS `quote_data_built`, NULL AS `quote_opt`, 
   
  (`order_products`.`products_quantity` * `product_mapping`.`lenson_packsize`) AS `Quantity`,
  round((`order_products`.`products_price` * `order_products`.`products_quantity`),2) AS `row_total`,
  null backup_add
from 
    `order_products` 
  left join 
    `product_mapping` on `product_mapping`.`product_id` = `order_products`.`products_id`
    
  -- delete records attahced to 
  DELETE FROM magento01_target.migrate_orderlinedata 
  WHERE old_order_id NOT IN (SELECT old_order_id FROM magento01_target.migrate_orderdata);
  
  -- force products with bad packsizes to be a non matched product
  UPDATE magento01_target.migrate_orderlinedata 
  SET product_id = @adhoc
  WHERE MOD(Quantity,packsize) <>0;  
    
  -- update colour to match vd lables
  UPDATE magento01_target.migrate_orderlinedata t1, `lenson_latest`.`colour_mapping` t2
  SET  t1.color = t2.`vd_label`
  WHERE t1.product_id = t2.product_id AND t1.color = t2.lenson_label;    

  -- fixes for products that will never be posible to map
  -- Opti-Free Express NoRub disabled
  UPDATE	magento01_target.migrate_orderlinedata 
  SET product_id = 2327 
  WHERE product_id IN (1215, 1229, 1265, 1095, ...);

  -- set params used for any non matching products: params, params_hash
  UPDATE magento01_target.`migrate_orderlinedata`
  SET params = IFNULL(
  	CONCAT(
      CASE WHEN `eye` IS NOT NULL AND `eye` != '' THEN CONCAT(' Eye:', CAST(eye AS CHAR CHARACTER SET utf8)) ELSE '' END,
  	  CASE WHEN `add` IS NOT NULL AND `add` != '' THEN CONCAT(' Addition:', `add`) ELSE '' END,
  	  CASE WHEN `axis` IS NOT NULL AND `axis` != '' THEN CONCAT(' Axis:', `axis`) ELSE '' END,
  	  CASE WHEN `base_curve` IS NOT NULL AND `base_curve` != '' THEN CONCAT(' Base Curve:', `base_curve`) ELSE '' END,
  	  CASE WHEN `color` IS NOT NULL AND `color` != '' THEN CONCAT(' Colour:', `color`) ELSE '' END,
  	  CASE WHEN `cylinder` IS NOT NULL AND `cylinder` != '' THEN CONCAT(' Cylinder:', `cylinder`) ELSE '' END,
  	  CASE WHEN `diameter` IS NOT NULL AND `diameter` != '' THEN CONCAT(' Diameter:', `diameter`) ELSE '' END,
  	  CASE WHEN `dominant` IS NOT NULL AND `dominant` != '' THEN CONCAT(' Dominant:', `dominant`) ELSE '' END,
  	  CASE WHEN `power` IS NOT NULL AND `power` != '' THEN CONCAT(' Power:', `power`) ELSE '' END), '');

