
select old_customer_id, email, failed_at
from magento01.migrate_customerdata_failed;

select old_order_id, info, failed_at
from magento01.migrate_orderdata_failed;

select old_item_id, failed_at
from magento01.migrate_orderlinedata_failed;

-- --------------------------------------------- 

select customer_id, last_order_date
from magento01._migrate_last_order_date

select new_customer_id, updated_at
from magento01._delete_migrated_customer

select old_id, new_id, migrated_at, info, updated_at, 
  last_logged_in, last_order_at, 
  valid_last_order, valid_logged_id, valid_store_id, valid
from magento01._remove_migrated_customer

select new_order_id, customer_id
from magento01._delete_migrated_orders

select old_id, new_id, migrated_at, info, 
  status, store_id, customer_id, 
  valid_status, valid_store_id, valid_customer_id, valid
from magento01._remove_migrated_orders


-- --------------------------------------------- 

select id, 
  product_id, productName, 
  eye, power, base_curve, diameter, 'add', axis, cylinder, dominant, color, params, params_hash, 
  quote_item_id, quote_sku, quote_is_lens, quote_data_built, quote_opt,
  debug, updated_at
from magento01.migrate_orderlinedata_params

-- --------------------------------------------- 

select old_customer_id, email, customer_id
from magento01_target._tmp_dup_customer
-- where email = 'ciwuhevo@nincsmail.com'
order by email

select email, customer_id, target_customer_id
from magento01_target._tmp_removed_dup_customers
-- where email = 'ciwuhevo@nincsmail.com'
order by email, customer_id

select old_last_order_data, old_customer_id
from magento01_target._tmp_old_last_order

select old_order_id
from magento01_target._broken_add_orders