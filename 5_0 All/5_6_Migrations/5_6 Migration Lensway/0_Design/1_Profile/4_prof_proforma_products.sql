
select top 1000 product_type, product_id, name, sku
from DW_Proforma.dbo.order_lines
where left(store_name, charindex('.', store_name)-1) = 'visiooptik' -- (lensbase, lenshome, visiondirect, visiooptik)

select top 1000 product_type, product_id, name, count(*)
from DW_Proforma.dbo.order_lines
--where left(store_name, charindex('.', store_name)-1) = 'visiooptik' -- (lensbase, lenshome, visiondirect, visiooptik)
group by product_type, product_id, name
order by product_type, product_id, name

select p.product_id,
	t.product_type, t.product_id, t.name, t.num
from
		(select product_type, product_id, name, count(*) num
		from DW_Proforma.dbo.order_lines
		where left(store_name, charindex('.', store_name)-1) = 'visiondirect' -- (lensbase, lenshome, visiondirect, visiooptik)
		group by product_type, product_id, name) t
	left join
		DW_GetLenses.dbo.products p on t.product_id = p.product_id and p.store_name = 'default'
order by t.num desc
--order by t.product_type, t.product_id, t.name

--

select top 1000 *
from DW_Proforma.dbo.order_lines
where left(store_name, charindex('.', store_name)-1) = 'lensbase' -- (lensbase, lenshome, visiondirect, visiooptik)
	and product_id = 2327
