
select *
from temptblexrate
order by created_at, rate;

select od.old_customer_id, r.id_order, r.invoice_date, 
  r.base_grand_total, r.base_grand_total_exc_vat, 
  r.base_shipping_amount, r.base_shipping_amount_exc_vat, 
  r.tax_rate, 
  r.payment_method
from 
    Revenues r
  inner join
    migrate_orderdata od on r.id_order = od.old_order_id
where od.old_customer_id in (958, 7101, 6132)
order by old_customer_id, id_order
limit 1000