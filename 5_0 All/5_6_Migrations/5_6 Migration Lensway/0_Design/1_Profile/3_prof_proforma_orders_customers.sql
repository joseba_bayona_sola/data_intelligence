
-- Info about Customers in DW_GetLenses that make orders in Migrated Brands (Proforma)
-- Look for Customer Special Attributes

select top 100 count(*) over () num_tot, oh.store_name, 
	oh.order_id, oh.order_no, oh.document_type, oh.order_date, oh.customer_id, oh.customer_email, 
	c.website_group, c.store_name, c.created_in, 
	c.customer_id, c.email, c.created_at, 
	c.old_access_cust_no, c.old_web_cust_no, c.old_customer_id
from 
		DW_Proforma.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where left(oh.store_name, charindex('.', oh.store_name)-1) = 'visiondirect' -- (lensbase, lenshome, visiondirect, visiooptik)
	and document_type = 'ORDER'
order by order_no

-- CUSTOMER NULL
-- Info about orders in Migrated Brands (Proforma) that are not related with any Customer in DW_GetLenses

select top 100 count(*) over () num_tot, oh.store_name, 
	oh.order_id, oh.order_no, oh.document_type, oh.order_date, oh.customer_id, oh.customer_email, 
	c.website_group, c.store_name, c.created_in, 
	c.customer_id, c.email, c.created_at, 
	c.old_access_cust_no, c.old_web_cust_no, c.old_customer_id
from 
		DW_Proforma.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where c.customer_id is null
order by order_no

select top 100 oh.store_name, count(*), count(distinct oh.customer_id)
from 
		DW_Proforma.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where c.customer_id is null
group by oh.store_name
order by oh.store_name
