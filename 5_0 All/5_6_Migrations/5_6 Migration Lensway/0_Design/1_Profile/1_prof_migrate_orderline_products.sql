
select *
from migrate_orderlinedata
where old_order_id = 7084351
limit 100


select old_item_id, old_order_id, 
  product_id, eye, 
  power, base_curve, diameter, axis, 'add', cylinder, dominant, color, 
  quantity, row_total
from migrate_orderlinedata
limit 100


select product_id, productName, count(*)
from migrate_orderlinedata
group by product_id, productName
order by product_id, productName

select eye, count(*)
from migrate_orderlinedata
group by eye
order by eye

select power, count(*)
from migrate_orderlinedata
group by power
order by power

select base_curve, count(*)
from migrate_orderlinedata
group by base_curve
order by base_curve

select diameter, count(*)
from migrate_orderlinedata
group by diameter
order by diameter

select axis, count(*)
from migrate_orderlinedata
group by axis
order by axis

select 'add', count(*)
from migrate_orderlinedata
group by 'add'
order by 'add'

select cylinder, count(*)
from migrate_orderlinedata
group by cylinder
order by cylinder

select dominant, count(*)
from migrate_orderlinedata
group by dominant
order by dominant

select color, count(*)
from migrate_orderlinedata
group by color
order by color



select quantity, count(*)
from migrate_orderlinedata
group by quantity
order by quantity