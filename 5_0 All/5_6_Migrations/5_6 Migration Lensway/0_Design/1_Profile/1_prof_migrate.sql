
-- migrate_customerdata
  -- VO: 11.323
  -- LB: 208.313 (904000025 - 912262801)
  -- LH: 9.963 (914000007 - 914010234)
  -- VD: 360.587 (901000001 - 902360620)
  -- LW: 92.883 (466501 - 19965509649)
  
select *
from migrate_customerdata
limit 1000;

select old_customer_id, new_magento_id, email, password, 
  store_id, domainID, domainName, website_group_id
from migrate_customerdata
order by old_customer_id desc
limit 1000;

select count(*)
from migrate_customerdata;

select min(old_customer_id), max(old_customer_id)
from migrate_customerdata;



-- migrate_orderdata
  -- VO: 19.341 (202 - 20476)
  -- LB: 529.298 (6995883 - 7975772)
  -- LH: 15.937 (8901941 - 8917891)
  -- VD: 959.265 (901925 - 1861214)
  -- LW: 186.878 (3534651392 - 19965313024)
  
select *
from migrate_orderdata
limit 1000;

select count(*)
from migrate_orderdata;

select min(cast(old_order_id as unsigned)), max(cast(old_order_id as unsigned))
from migrate_orderdata;



-- migrate_customerdata
  -- VO: 33.442 (202 - 20476) - (1 - 33442)
  -- LB: 1.118.177 (6995883 - 7975772) (3000000 - 4810237)
  -- LH: 38.198 (8901941 - 8917891) (6000000 - 6038197)
  -- VD: 1.884.399 (901925 - 1861214) (1 - 1884457)
  -- LW: 523.184 (3534749696 - 19965411329)
  
select *
from migrate_orderlinedata
limit 1000;

select count(*)
from migrate_orderlinedata;

select min(cast(old_order_id as unsigned)), max(cast(old_order_id as unsigned))
from migrate_orderlinedata;

select min(cast(old_item_id as unsigned)), max(cast(old_item_id as unsigned))
from migrate_orderlinedata;


