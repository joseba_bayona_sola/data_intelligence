
-- Info about how status is set in Proforma using mapping_order_status
-- Focus on CANCEL - CREDITMEMO

select *
from DW_Proforma.dbo.mapping_order_status
order by dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment

-- CANCEL ORDERS with corresponding status
select top 100 oh.store_name, 
	oh.order_id, oh.order_no, oh.document_type, oh.order_date, oh.customer_id, oh.customer_email, 
	oh.status
from DW_Proforma.dbo.order_headers oh
where document_type = 'CANCEL'
order by order_no

	select status, count(*)
	from DW_Proforma.dbo.order_headers oh
	where document_type = 'CANCEL'
	group by status
	order by status

-- CREDITMEMO ORDERS with corresponding status
select top 100 oh.store_name, 
	oh.order_id, oh.order_no, oh.document_type, oh.order_date, oh.customer_id, oh.customer_email, 
	oh.status
from DW_Proforma.dbo.order_headers oh
where document_type = 'CREDITMEMO'
order by order_no

	select status, count(*)
	from DW_Proforma.dbo.order_headers oh
	where document_type = 'CREDITMEMO'
	group by status
	order by status

