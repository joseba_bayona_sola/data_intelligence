
-- Look for how Customers from Migrated Brands are created in DW_GetLenses

select 	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where customer_id in (1052181, 1130880, 1299113, 1059340, 1052677, 908972, 609740, 489042, 665264, 1356764, 1362791, 1361474)
order by old_web_cust_no

select top 1000 count(*) over() num_tot, website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_customer_id like 'LB%' -- VISIO% - LB%
order by old_customer_id 

select 	top 1000 website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_access_cust_no = 'LENSWAYUK'
order by old_web_cust_no





-- 705.449
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_customer_id is not null
order by created_at desc

-- 778.953
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_customer_id is null
order by created_at desc


----------------------------------------------------------------------------

-- 705.449
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_customer_id is not null

	select website_group, store_name, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is not null
	group by website_group, store_name
	order by website_group, store_name

	select created_in, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is not null
	group by created_in
	order by created_in

	-- Info about how this customers were created in Migrated Brands (OK for Lensbase - Visio / What about VisionDirect)
	select old_access_cust_no, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is not null
	group by old_access_cust_no 
	order by old_access_cust_no

----------------------------------------------------------------------------

-- 778.953
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_GetLenses.dbo.customers
where old_customer_id is null

	select website_group, store_name, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is null
	group by website_group, store_name
	order by website_group, store_name

	select created_in, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is null
	group by created_in
	order by created_in

	select old_access_cust_no, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.customers
	where old_customer_id is null
	group by old_access_cust_no 
	order by old_access_cust_no