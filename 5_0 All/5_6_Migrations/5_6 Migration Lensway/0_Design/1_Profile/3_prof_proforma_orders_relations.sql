
-- Relation to Order Type
select top 100 order_type, count(*)
from DW_Proforma.dbo.order_headers
group by order_type
order by order_type

-- Relation to Order Lifecycle
select top 100 order_lifecycle, count(*)
from DW_Proforma.dbo.order_headers
group by order_lifecycle
order by order_lifecycle

-- Customer Order Seq
select top 100 customer_order_seq_no, count(*)
from DW_Proforma.dbo.order_headers
group by customer_order_seq_no
order by customer_order_seq_no



-- Relation to Coupon Code 
select top 100 coupon_code, count(*)
from DW_Proforma.dbo.order_headers
group by coupon_code
order by coupon_code

-- Relation to Business Channel
select top 100 business_channel, count(*)
from DW_Proforma.dbo.order_headers
group by business_channel
order by business_channel

select left(store_name, charindex('.', store_name)-1) store, business_channel, count(*)
from DW_Proforma.dbo.order_headers
group by left(store_name, charindex('.', store_name)-1), business_channel
order by left(store_name, charindex('.', store_name)-1), business_channel

-- Relation to Shipping Carrier
select top 100 shipping_carrier, count(*)
from DW_Proforma.dbo.order_headers
group by shipping_carrier
order by shipping_carrier

select left(store_name, charindex('.', store_name)-1) store, shipping_carrier, count(*)
from DW_Proforma.dbo.order_headers
group by left(store_name, charindex('.', store_name)-1), shipping_carrier
order by left(store_name, charindex('.', store_name)-1), shipping_carrier


-- Relation to Shipping Method
select top 100 shipping_method, count(*)
from DW_Proforma.dbo.order_headers
group by shipping_method
order by shipping_method

select left(store_name, charindex('.', store_name)-1) store, shipping_method, count(*)
from DW_Proforma.dbo.order_headers
group by left(store_name, charindex('.', store_name)-1), shipping_method
order by left(store_name, charindex('.', store_name)-1), shipping_method


-- Relation to Reminder Type
select top 100 reminder_type, count(*)
from DW_Proforma.dbo.order_headers
group by reminder_type
order by reminder_type
