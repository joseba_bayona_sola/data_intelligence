
-- Numbers about Orders - Invoices - Shipments per Document Type

select top 100 *
from DW_Proforma.dbo.order_headers
where left(store_name, charindex('.', store_name)-1) = 'visiooptik' -- (lensbase, lenshome, visiondirect, visiooptik)
	and document_type = 'ORDER'
order by order_no

select count(*) -- (1.523.839)
from DW_Proforma.dbo.order_headers

select store_name, count(*) 
from DW_Proforma.dbo.order_headers
group by store_name
order by store_name

select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.order_headers
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)

select left(store_name, charindex('.', store_name)-1), document_type, count(*) 
from DW_Proforma.dbo.order_headers
group by left(store_name, charindex('.', store_name)-1), document_type
order by left(store_name, charindex('.', store_name)-1), document_type

--

select top 100 *
from DW_Proforma.dbo.order_lines

select count(*) -- (3.070.363)
from DW_Proforma.dbo.order_lines

select store_name, count(*) 
from DW_Proforma.dbo.order_lines
group by store_name
order by store_name

select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.order_lines
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)

select left(store_name, charindex('.', store_name)-1), document_type, count(*) 
from DW_Proforma.dbo.order_lines
group by left(store_name, charindex('.', store_name)-1), document_type
order by left(store_name, charindex('.', store_name)-1), document_type

---- 

select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.invoice_headers
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)

select left(store_name, charindex('.', store_name)-1), document_type, count(*) 
from DW_Proforma.dbo.invoice_headers
group by left(store_name, charindex('.', store_name)-1), document_type
order by left(store_name, charindex('.', store_name)-1), document_type


select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.shipment_headers
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)

select left(store_name, charindex('.', store_name)-1), document_type, count(*) 
from DW_Proforma.dbo.shipment_headers
group by left(store_name, charindex('.', store_name)-1), document_type
order by left(store_name, charindex('.', store_name)-1), document_type

---- 

select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.invoice_lines
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)

select left(store_name, charindex('.', store_name)-1), count(*) 
from DW_Proforma.dbo.shipment_lines
group by left(store_name, charindex('.', store_name)-1)
order by left(store_name, charindex('.', store_name)-1)
