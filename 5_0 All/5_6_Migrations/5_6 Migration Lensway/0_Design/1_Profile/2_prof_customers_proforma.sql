
-- Info about Missing Customers inserted in Proforma

-- 1018
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_Proforma.dbo.customers
where old_customer_id is not null
order by created_at desc

-- 0
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_Proforma.dbo.customers
where old_customer_id is null
order by created_at desc

----------------------------------------------------------------------------

-- 1018
select top 1000 count(*) over () num_tot, 
	website_group, store_name, created_in, 
	customer_id, email, created_at, 
	old_access_cust_no, old_web_cust_no, old_customer_id
from DW_Proforma.dbo.customers
where old_customer_id is not null

	select website_group, store_name, count(*), min(created_at), max(created_at)
	from DW_Proforma.dbo.customers
	where old_customer_id is not null
	group by website_group, store_name
	order by website_group, store_name

	select created_in, count(*), min(created_at), max(created_at)
	from DW_Proforma.dbo.customers
	where old_customer_id is not null
	group by created_in
	order by created_in

	select old_access_cust_no, count(*), min(created_at), max(created_at)
	from DW_Proforma.dbo.customers
	where old_customer_id is not null
	group by old_access_cust_no 
	order by old_access_cust_no
