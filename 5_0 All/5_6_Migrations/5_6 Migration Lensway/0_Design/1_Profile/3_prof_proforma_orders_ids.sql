

-- Lensbase: 
	-- Original ID:	7652656	(Lenght: 7-8) (6995883 - 7975772)
	-- Order - Invoice - Shipment - Document ID: 807652656 (80 + OriginalID) 
	-- Order - Invoice - Shipment NO: LH8902131 (LB + OriginalID)

-- Lenshome: 
	-- Original ID:	8902131	(Lenght: 7-8) (8901941 - 8917891)
	-- Order - Invoice - Shipment - Document ID: 908902131 (90 + OriginalID) 
	-- Order - Invoice - Shipment NO: LH8902131 (LH + OriginalID)

-- Vision Direct: 
	-- Original ID:	935640	(Lenght: 6-7) (901925 - 1861214)
	-- Order - Invoice - Shipment - Document ID: 700935640 (700 + OriginalID) 
	-- Order - Invoice - Shipment NO: VD935640 (VD + OriginalID)

-- Visio Optik: 
	-- Original ID:	20048	(Lenght: 5) (202 - 201476)
	-- Order - Invoice - Shipment - Document ID: 600020048 (6000 + OriginalID) 
	-- Order - Invoice - Shipment NO: VISIO20048 (VISIO + OriginalID)


select store_name, customer_id, document_type,
	document_id, order_id, order_no, order_date
from DW_Proforma.dbo.order_headers
where customer_id in (1052181, 1130880, 1299113, 1059340, 1052677, 908972, 609740, 489042, 665264, 1356764, 1362791, 1361474)
order by customer_id, order_date

select store_name, customer_id, document_type,
	document_id, order_id, invoice_id, order_no, invoice_no, order_date, invoice_date
from DW_Proforma.dbo.invoice_headers
where customer_id in (1052181, 1130880, 1299113, 1059340, 1052677, 908972, 609740, 489042, 665264, 1356764, 1362791, 1361474)
order by customer_id, invoice_date

select store_name, customer_id, document_type,
	document_id, order_id, invoice_id, shipment_id, order_no, invoice_no, shipment_no, order_date, invoice_date, shipment_date
from DW_Proforma.dbo.shipment_headers
where customer_id in (1052181, 1130880, 1299113, 1059340, 1052677, 908972, 609740, 489042, 665264, 1356764, 1362791, 1361474)
order by customer_id, shipment_date

-----------


select top 1000 ol.store_id, oh.store_name, ol.customer_id, ol.order_id, ol.order_no, ol.created_at, 
	ol.source, ol.status, ol.prev_status, 
	ol.order_lifecycle, order_lifecycle_cancel, order_lifecycle_all, 
	order_rank_all, order_rank_cancel, order_rank_cancel_only, 
	order_rank
from  
		dw_proforma.dbo.order_lifecycle ol
	inner join
		dw_proforma.dbo.order_headers oh on ol.order_id = oh.order_id
where ol.customer_id in (1052181, 1130880, 1299113, 1059340, 1052677, 908972, 609740, 489042, 665264, 1356764, 1362791, 1361474)
order by ol.customer_id, created_at