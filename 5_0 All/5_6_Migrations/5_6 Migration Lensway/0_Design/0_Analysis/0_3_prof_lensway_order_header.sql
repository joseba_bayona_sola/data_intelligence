
select entity_id, 
  increment_id, state, status,
  STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
  base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total
from lensway.lensway_order
-- order by entity_id
limit 1000;

select item_id, order_id, 
  STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
  item_product_family, item_product_type,
  product_id, sku, name, 
  qty_ordered, base_price, base_row_total
from lensway.lensway_order_item
where order_id = 3636363272
limit 1000;

select o.entity_id, oi.item_id,
    o.increment_id, o.state, o.status, o.created_at_d,
    oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name,
    oi.qty_ordered, oi.base_price, oi.base_row_total,
    o.base_subtotal, o.base_shipping_amount, o.base_discount_amount, o.handling_charge, o.base_grand_total 
    
from
    (select entity_id, 
      increment_id, state, status,
      STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
      base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total
    from lensway.lensway_order) o
  inner join
    (select item_id, order_id, 
      STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
      item_product_family, item_product_type,
      product_id, sku, name, 
      qty_ordered, base_price, base_row_total
    from lensway.lensway_order_item) oi on o.entity_id = oi.order_id
where o.entity_id = '10197958662'
order by o.entity_id, oi.item_id
limit 1000;

-- ------------------------------------------------------------------

select o.entity_id, oi.item_id,
    o.increment_id, o.state, o.status, o.created_at_d,
    oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name,
    oi.qty_ordered, oi.base_price, oi.base_row_total,
    o.base_subtotal, o.base_shipping_amount, o.base_discount_amount, o.handling_charge, o.base_grand_total 
    
from
    (select entity_id, 
      increment_id, state, status,
      STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
      base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total
    from lensway.lensway_order) o
  left join
    (select item_id, order_id, 
      STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
      item_product_family, item_product_type,
      product_id, sku, name, 
      qty_ordered, base_price, base_row_total
    from lensway.lensway_order_item) oi on o.entity_id = oi.order_id
where oi.order_id is null
order by o.entity_id, oi.item_id
limit 1000;
