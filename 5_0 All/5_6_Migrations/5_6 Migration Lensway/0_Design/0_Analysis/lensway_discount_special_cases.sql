
select entity_id, 
  increment_id, state, status, 
  store_id, store_name, customer_id, 
  created_at, 
  base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total, 
  base_tax_amount, base_customer_balance_amount 
  base_subtotal_refunded, base_total_refunded
from lensway.lensway_order
where entity_id IN (3668344851, 3518267393, 3518267392, 3831792261, 10008395797, 10146611237)
order by entity_id
limit 1000;

select item_id, order_id, created_at, updated_at, 
  item_product_family, item_product_type,
  product_id, product_type, sku, name, description, 
  qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, weight, row_weight, 
  base_price, base_tax_amount, base_discount_amount, base_amount_refunded, base_row_total
from lensway.lensway_order_item
where order_id IN (3668344851, 3518267393, 3518267392, 3831792261, 10008395797, 10146611237)
order by order_id, item_id
limit 1000;