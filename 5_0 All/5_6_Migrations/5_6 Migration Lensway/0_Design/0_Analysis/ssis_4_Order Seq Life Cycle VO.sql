
-- (1) Get Order Info for VISIO Customers from DW_GetLenses (OH, HIST Tables)
-- (2) Get Order Info for VISIO Customers from DW_Proforma (OH)
--> UNION THEM
--> INSERT IN order_seq, order_lifecycle

	-- (1)
	select 
		cast('getlenses' as varchar(20)) as db, 
		cast(order_id as int) as order_id, cast(order_no as varchar(25)) as order_no,
		isnull(store_id,0) as store_id, 
		cast(status as varchar(25)) as status, 
		cast(document_id as int) as document_id, cast(document_type as varchar(10)) as document_type, 
		customer_id, cast(document_date as smalldatetime) as created_at 
	from 
			DW_GetLenses.dbo.order_headers oh
		left outer join 
			DW_GetLenses.dbo.dw_stores_full stores on stores.store_name=oh.store_name
	where customer_id in 
		(select distinct customer_id from DW_Proforma.dbo.order_headers where left(order_no,5)='VISIO')
	union
	select orders.source as db,
		cast(order_id as int) as order_id, cast(order_id as varchar(25)) as order_no,
		orders.store_id,
		cast(case when shipments.shipment_id is null then 'processing' else 'shipped' end as varchar(25)) as status, 
		cast(orders.order_id as int) as document_id, 'ORDER' as document_type,
		orders.customer_id, cast(orders.created_at as smalldatetime) as created_at 
	from 
			DW_GetLenses.dbo.dw_hist_order orders
		left outer join 
			DW_GetLenses.dbo.dw_hist_shipment shipments on orders.order_id=shipments.shipment_id
	where orders.customer_id in 
		(select distinct customer_id from DW_Proforma.dbo.order_headers where left(order_no,5)='VISIO') and 
		not exists 
			(select * from DW_Proforma.dbo.order_headers oh where left(order_no,5)='VISIO' and oh.order_id =600000000+orders.order_id)

	-- (2)
	select 
		cast(customer_id as int) as customer_id,
		cast(document_date as smalldatetime) as created_at,
		stores.store_id ,
		cast([status] as varchar(25)) as [status],
		cast(document_type as varchar(10)) as document_type,
		cast(('amexVISIO') as varchar(9)) as db, cast(order_no as varchar(25)) as order_no, document_id, order_id
	from 
			DW_Proforma.dbo.order_headers oh
		inner join 
			DW_GetLenses.dbo.V_Stores_All stores on stores.store_name = oh.store_name 
	where left(order_no,5)='VISIO'
