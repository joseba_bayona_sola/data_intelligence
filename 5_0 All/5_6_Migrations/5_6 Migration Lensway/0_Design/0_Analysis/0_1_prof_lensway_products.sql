-- lensway_product_mapping (738 - 10160)
select *
from lensway.lensway_product_mapping
limit 100;

  select count(*)
  from lensway.lensway_product_mapping;

  select product_id, count(*)
  from lensway.lensway_product_mapping
  group by product_id
  order by count(*) desc;

  select sku, count(*)
  from lensway.lensway_product_mapping
  group by sku
  order by count(*) desc;

  select magento_packsize, count(*)
  from lensway.lensway_product_mapping
  group by magento_packsize
  order by magento_packsize;

  select magento_id, magento_name, count(*)
  from lensway.lensway_product_mapping
  group by magento_id, magento_name
  -- order by magento_id, magento_name;
  order by count(*) desc;

  select 
    product_id, sku, lensway_name, lensway_packsize,
    magento_id, magento_name, magento_packsize
  from lensway.lensway_product_mapping
  where magento_id = 3135 -- (3135 - 2327 - 3140 - 3141 - 3153 - 3134)
  -- where magento_id in (3139, 3130, 3152, 3129, 3128)
  order by magento_id, lensway_name
  limit 1000;

  select 
    product_id, sku, lensway_name, lensway_packsize,
    magento_id, magento_name, magento_packsize
  from lensway.lensway_product_mapping
  where product_id in (11882037248, 3364716544, 676429829, 1969029120)
  order by magento_id, lensway_name
  limit 1000;  
  
  select item_product_family, item_product_type, product_id, sku, name, count(*) -- 54722k
  from lensway.lensway_order_item
  where product_id in (11882037248, 3364716544, 676429829, 1969029120)
  group by item_product_family, item_product_type, product_id, sku, name
  order by item_product_family, item_product_type, product_id, sku, name
  
  -- -------------------------------

-- JOIN WITH ORDERS AND PRODUCTS

select oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name, 
  pm.product_id, pm.sku, pm.lensway_name, pm.magento_id, pm.magento_name,
  oi.num, oi.num_orders -- 54722k
from
    (select item_product_family, item_product_type, product_id, sku, name, count(*) num, count(distinct order_id) num_orders -- 54722k
    from lensway.lensway_order_item
    -- where item_product_family = 'LENSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
    group by item_product_family, item_product_type, product_id, sku, name) oi
  left join
    lensway.lensway_product_mapping pm on oi.product_id = pm.product_id
-- where pm.magento_id < 3000 and pm.magento_id <> 2327   
where pm.product_id is null
order by oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name


select oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name, 
  pm.product_id, pm.sku, pm.lensway_name, pm.magento_id, pm.magento_name,
  oi.num, oi.num_orders -- 54722k
from
    (select item_product_family, item_product_type, product_id, sku, name, count(*) num, count(distinct order_id) num_orders -- 54722k
    from lensway.lensway_order_item
    group by item_product_family, item_product_type, product_id, sku, name) oi
  left join
    lensway.lensway_product_mapping pm on oi.product_id = pm.product_id
where 
  -- pm.magento_name = 'Ad-hoc Product' -- 'Ad-hoc Product'   
  -- pm.magento_name = '' -- 'Daily contact lens (generic for import)' - 'Weekly contact lens (generic for import)' - 'Monthly contact lens (generic for import)'
  -- pm.magento_name = '' -- 'Solution (generic for import)' - 'Eye Care (generic for import)'
  -- pm.magento_name = '' -- 'Sunglasses (generic for import)'
  -- pm.magento_name = '' -- 'Glasses frame (generic for import)' - 'Right Glasses Lens (generic for import)' - 'Made to order/RGP lens (generic for import)'
order by oi.item_product_family, oi.item_product_type, oi.product_id, oi.sku, oi.name
