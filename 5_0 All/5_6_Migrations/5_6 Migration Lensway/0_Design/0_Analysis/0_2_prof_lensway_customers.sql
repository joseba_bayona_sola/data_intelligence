
select *
from lensway.lensway_customer
-- where firstname = 'Lafesteerne' and lastname = 'Lafesteerne'
where email = 'a.chalupnik@op.pl'
order by email
limit 1000;

select entity_id, email, firstname, lastname, created_at, updated_at, store_id, created_in, 
  unsubscribe_all, unsubscribe_all_date, referafriend_code, reviewer, found_us_info
from lensway.lensway_customer
-- where entity_id = 3636264962
order by email
limit 1000;

select email, count(*)
from lensway.lensway_customer
group by email
order by count(*) desc
limit 1000;

select firstname, lastname, count(*)
from lensway.lensway_customer
group by firstname, lastname
order by count(*) desc
limit 1000;

-- -------------------------------

-- JOIN WITH ORDERS AND CUSTOMERS

select o.entity_id, o.increment_id, o.created_at, 
  o.customer_id, o.customer_email, c.entity_id, c.email, c.firstname, c.lastname, c.created_at,
  o.base_subtotal, o.base_shipping_amount, o.base_discount_amount, o.handling_charge, o.base_grand_total
from 
    lensway.lensway_order o
  left join
    lensway.lensway_customer c on o.customer_id = c.entity_id
where 
  c.entity_id is null -- (526)
  -- c.entity_id is not null and o.customer_email <> c.email (47)
order by STR_TO_DATE(o.created_at, '%b %d %Y %h:%i%p')  
limit 10000;

select o.entity_id, o.increment_id, o.created_at, 
  o.customer_id, o.customer_email, c.entity_id, c.email, c.firstname, c.lastname, c.created_at,
  o.base_subtotal, o.base_shipping_amount, o.base_discount_amount, o.handling_charge, o.base_grand_total
from 
    lensway.lensway_order o
  right join
    lensway.lensway_customer c on o.customer_id = c.entity_id
where 
  o.entity_id is null -- (526)
order by c.created_at  
limit 10000;


