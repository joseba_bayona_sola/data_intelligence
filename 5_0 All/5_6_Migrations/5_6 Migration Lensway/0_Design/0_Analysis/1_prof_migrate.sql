
-- migrate_customerdata
  -- VO: 11.323
  -- LB: 208.313
  -- LH: 9.963
  -- VO: 360.587
  
select *
from migrate_customerdata
limit 1000;

select old_customer_id, email, 
  login, password
from migrate_customerdata
limit 1000;

select count(*)
from migrate_customerdata;


-- migrate_orderdata
  -- VO: 19.341
  -- LB: 529.298
  -- LH: 15.937
  -- VO: 959.265
  
select *
from migrate_orderdata
limit 1000;

select count(*)
from migrate_orderdata;


-- migrate_customerdata
  -- VO: 33.442
  -- LB: 1.118.177
  -- LH: 38.198
  -- VO: 1.884.399
  
select *
from migrate_orderlinedata
limit 1000;

select count(*)
from migrate_orderlinedata;


