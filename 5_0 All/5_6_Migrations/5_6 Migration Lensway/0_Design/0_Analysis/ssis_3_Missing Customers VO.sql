
select customer_id, 
	customer_firstname, customer_lastname, 
	customer_email, 
	billing_prefix, 
	store_name, 
	customer_first_order_date
from 
	(select customer_id, 
		customer_firstname, customer_lastname, customer_email, 
		billing_prefix, store_name, customer_first_order_date, 
		row_number() over(partition by customer_id order by document_date desc) as rno 
	from DW_Proforma.dbo.order_headers
	where customer_id not in 
		(select customer_id from DW_GetLenses.dbo.customers) and 
		left(order_no,5)='VISIO') rs 
where rno=1

-- Flow
	
	-- Lookup STORES: DW_GetLenses.dbo.v_stores_all (on store_name) get (website_id, website_group)

	-- Derived
		-- customer_id: (DT_I4)customer_id
		-- old_customer_id: (DT_STR,50,1252)(@[User::store_global_pk] + (DT_STR,50,1252)customer_id)
		-- updated_at: GETDATE()
		-- store_name: (DT_STR,100,1252)store_name
		-- email: (DT_STR,100,1252)customer_email
		-- prefix: (DT_STR,100,1252)billing_prefix
		-- firstname: (DT_STR,100,1252)customer_firstname
		-- lastname: (DT_STR,100,1252)customer_lastname
		-- gender: (DT_STR,1,1252)(REPLACE(UPPER(billing_prefix),".","") == "MR" ? "M" : (REPLACE(UPPER(billing_prefix),".","") == "MRS" || REPLACE(UPPER(billing_prefix),".","") == "MS" ? "F" : ""))
		-- created_at: customer_first_order_date

	-- Insert