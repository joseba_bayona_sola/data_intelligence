


ALTER PROCEDURE [dbo].[spCreateCustomerOrderSeqLifeCycle] 

AS
BEGIN

declare @prev_val as varchar(200)



/* Update Order_lifecycle */
	-- ORDER_RANK_ALL: Order ALL Customer Orders 
	-- ORDER_RANK_CANCEL - ORDER_RANK_CANCEL_ONLY: Order Customer Orders (OK) - Order Customer Orders (CANCEL) / using status
	-- ORDER_RANK
	-- prev_status
	-- ORDER_LIFECYCLE_ALL - ORDER_LIFECYCLE_CANCEL
	-- ORDER_LIFECYCLE

	---update ORDER_RANK_ALL
	update olc
	set olc.ORDER_RANK_ALL = rank_all.rank_all
	from 
			order_lifecycle olc 
		inner join
			(select customer_id, order_id,
				row_number() over(partition by customer_id order by created_at asc) as [rank_all]
			from order_lifecycle 
			where store_id not in
				 (select store_id 
				 from DW_GetLenses.dbo.dw_core_config_data 
				 where path = 'surveysweb/surveys/active' and cast(value as varchar(max))=1)) rank_all on rank_all.CUSTOMER_ID = olc.CUSTOMER_ID and rank_all.ORDER_ID = olc.ORDER_ID

	--update ORDER_RANK_CANCEL
	update olc
	set olc.ORDER_RANK_CANCEL=rank_all.rank_all
	from
			order_lifecycle olc 
		inner join
			(select customer_id, order_id, 
				row_number() over(partition by customer_id order by created_at asc) as rank_all
			from order_lifecycle 
			where ORDER_RANK_ALL is not null and status not in ('canceled', 'closed')) rank_all on rank_all.CUSTOMER_ID = olc.CUSTOMER_ID and rank_all.ORDER_ID = olc.ORDER_ID

	--update ORDER_RANK_CANCEL_ONLY
	update olc
	set olc.ORDER_RANK_CANCEL_ONLY=rank_all.rank_all
	from
			order_lifecycle olc 
		inner join
			(select customer_id, order_id, 
				row_number() over(partition by customer_id order by created_at asc) as [rank_all]
			from order_lifecycle 
			where ORDER_RANK_ALL is not null and [status] in ('canceled', 'closed')) rank_all on rank_all.CUSTOMER_ID = olc.CUSTOMER_ID and rank_all.ORDER_ID = olc.ORDER_ID


	--update ORDER_RANK
	update order_lifecycle
	set ORDER_RANK = 
		CASE 
			WHEN ORDER_RANK_CANCEL_ONLY IS NULL THEN ISNULL(ORDER_RANK_CANCEL,0) 
			when (ORDER_RANK_ALL-ORDER_RANK_CANCEL_ONLY)>1 then ORDER_RANK_ALL-ORDER_RANK_CANCEL_ONLY
			else 1
		end


	--update prev_status
	update rs
	set rs.prev_status = prev_status.status 
	from 
			order_lifecycle rs
		left outer join
			(select * 
			from order_lifecycle) prev_status on rs.ORDER_RANK_ALL-1 = prev_status.ORDER_RANK_ALL and rs.customer_id = prev_status.customer_id

	--update ORDER_LIFECYCLE_ALL
	update rs1
	set rs1.ORDER_LIFECYCLE_ALL=
		case 
			when rs1.ORDER_RANK_ALL=1 then 'New'
			when datediff(month, rs2.created_at, rs1.created_at)<=15 then 'Regular'
			when datediff(month, rs2.created_at, rs1.created_at)>15 then 'Reactivated'
		end
	from 
			order_lifecycle rs1
		left outer join 
			order_lifecycle rs2 on rs1.ORDER_RANK_ALL-1 = rs2.ORDER_RANK_ALL and rs1.customer_id = rs2.customer_id

	--update ORDER_LIFECYCLE_CANCEL
	update rs1
	set rs1.ORDER_LIFECYCLE_CANCEL=
		case 
			when rs1.ORDER_RANK_CANCEL=1 then 'New'
			when datediff(month, rs2.created_at, rs1.created_at)<=15 then 'Regular'
			when datediff(month, rs2.created_at, rs1.created_at)>15 then 'Reactivated'
		end
	from 
			order_lifecycle rs1
		left outer join 
			order_lifecycle rs2 on rs1.ORDER_RANK_CANCEL-1 = rs2.ORDER_RANK_CANCEL and rs1.customer_id=rs2.customer_id


	--update ORDER_LIFECYCLE_CANCEL and ORDER_LIFECYCLE_ALL to Survey wherever applicable
	update order_lifecycle
	set 
		ORDER_LIFECYCLE_CANCEL = case when ORDER_RANK_ALL is null then 'Survey' else ORDER_LIFECYCLE_CANCEL end,
		ORDER_LIFECYCLE_ALL = case when ORDER_RANK_ALL is null then 'Survey' else ORDER_LIFECYCLE_ALL end



	--update ORDER_LIFECYCLE
	update order_lifecycle
	set ORDER_LIFECYCLE= isnull(ISNULL(ORDER_LIFECYCLE_CANCEL, ORDER_LIFECYCLE_ALL), 'Regular')








/* Update Order_Seq */
	-- ORDER_RANK
	-- NEXT_DOCUMENT_TYPE, PREV_DOCUMENT_TYPE
	-- FULL_CREDIT, PREV_FULL_CREDIT
	-- ORDER_SEQ_PLUS - ORDER_SEQ_MINUS
	-- ORDER_SEQ

	--update ORDER_RANK
	update os
	set os.ORDER_RANK = rank_all.rank_all
	from
			order_seq os
		inner join
			(select customer_id, DOCUMENT_ID, DOCUMENT_TYPE, 
				row_number() over(partition by customer_id order by created_at asc) as [rank_all]
			from order_seq) rank_all on rank_all.CUSTOMER_ID = os.CUSTOMER_ID and rank_all.DOCUMENT_ID = os.DOCUMENT_ID and rank_all.DOCUMENT_TYPE = os.DOCUMENT_TYPE


	--update NEXT_DOCUMENT_TYPE
	update rs
	set rs.NEXT_DOCUMENT_TYPE = next_doc_type.DOCUMENT_TYPE
	from 
			order_seq rs
		left outer join
			(select * 
			from order_seq) next_doc_type on rs.ORDER_RANK+1 = next_doc_type.ORDER_RANK and rs.customer_id = next_doc_type.customer_id

	--update PREV_DOCUMENT_TYPE
	update rs
	set rs.PREV_DOCUMENT_TYPE = prev_doc_type.DOCUMENT_TYPE
	from 
			order_seq rs
		left outer join
			(select * 
			from order_seq) prev_doc_type on rs.ORDER_RANK-1 = prev_doc_type.ORDER_RANK and rs.customer_id=prev_doc_type.customer_id


	--update FULL_CREDIT
	update os
	set os.FULL_CREDIT = case when full_refund.creditmemo_id is not null or os.DOCUMENT_TYPE='CANCEL' then 1 else 0 end
	from 
			order_seq os
		left outer join 
			(select * 
			from openquery(magento,'select * from dw_flat.dw_full_creditmemo')) full_refund on full_refund.order_id = os.order_id and os.DOCUMENT_TYPE = 'CREDITMEMO'

	--update PREV_FULL_CREDIT
	update rs
	set rs.PREV_FULL_CREDIT = prev_full_credit.FULL_CREDIT
	from 
			order_seq rs
		left outer join
			(select * 
			from order_seq) prev_full_credit on rs.ORDER_RANK-1 = prev_full_credit.ORDER_RANK and rs.customer_id = prev_full_credit.customer_id


	--update ORDER_SEQ_PLUS
	declare @var as int=0, @prev_customer_id as int=0

		update order_seq
		set 
			@var = case when customer_id <> @prev_customer_id then 0 else @var end,
			@var = case DOCUMENT_TYPE when 'ORDER' then @var+1 else @var end,
			
			ORDER_SEQ_PLUS=@var,
			@prev_customer_id=customer_id

	--update ORDER_SEQ_MINUS
	set @var=0
	set @prev_customer_id=0

		update order_seq 
		set 
			@var = case when customer_id <> @prev_customer_id then 0 else @var end,
			@var = case when PREV_DOCUMENT_TYPE in ('CANCEL', 'CREDITMEMO') and PREV_FULL_CREDIT=1 then @var-1 else @var end, 
			
			ORDER_SEQ_MINUS=@var,
			@prev_customer_id=customer_id

	--update ORDER_SEQ
	update order_seq
	set ORDER_SEQ = case when (ORDER_SEQ_PLUS + ORDER_SEQ_MINUS)>1 then ORDER_SEQ_PLUS+ORDER_SEQ_MINUS else 1 end

END
