
-- lensway_product_mapping (738 - 10160 - 10231)
select *
from lensway.lensway_product_mapping
limit 100;

  select count(*)
  from lensway.lensway_product_mapping;

  select 
    product_id, sku, lensway_name, lensway_packsize,
    magento_id, magento_name, magento_packsize
  from lensway.lensway_product_mapping
  order by magento_id, sku
  limit 1000;


-- lensway_product_options (474.318)
select *
from lensway.lensway_product_options
limit 1000;

select count(*)
from lensway.lensway_product_options;

select product_uidpk, product_code, skucode, product_sku_uid, 
  baseCurve, diameter, power, cylinder, axis, addPower, optionDN, color, sfxColor
from lensway.lensway_product_options
where product_code = 91505
limit 1000

-- ----------------------------------------------------------------------

-- lensway_customer (92.008 - 92.016 - 93089)
select *
from lensway.lensway_customer
limit 1000;

select count(*)
from lensway.lensway_customer;


-- lensway_customer_address (106.831 - 107.796)
select *
from lensway.lensway_customer_address
limit 1000;

select count(*)
from lensway.lensway_customer_address;

-- lensway_address_region (27 - 0)
select *
from lensway.lensway_address_region
limit 1000;

select count(*)
from lensway.lensway_address_region;


-- ----------------------------------------------------------------------

-- lensway_customercredit_marketing (14.061)
select *
from lensway.lensway_customercredit_marketing
limit 1000;

select count(*)
from lensway.lensway_customercredit_marketing;


-- lensway_customercredit_referral (289)
select *
from lensway.lensway_customercredit_referral
limit 1000;

select count(*)
from lensway.lensway_customercredit_referral;


-- lensway_customercredit_service (380 - 832)
select *
from lensway.lensway_customercredit_service
limit 1000;

select count(*)
from lensway.lensway_customercredit_service;


-- ----------------------------------------------------------------------

-- lensway_order (191.204 - 191.108 - 191.879 - 192.148)
select *
from lensway.lensway_order
limit 1000;

select count(*)
from lensway.lensway_order;

-- lensway_order (191.519)
select *
from lensway.lensway_order_raw
-- where order_number = 35523408
limit 1000;

select count(*)
from lensway.lensway_order_raw;

select customer_uid, count(*)
from lensway.lensway_order_raw
group by customer_uid
order by customer_uid;

-- ----------------------

-- lensway_order_item (537.740 - 539.097 - 540.027 - 540.958)
select *
from lensway.lensway_order_item
order by item_id
limit 1000;

select count(*)
from lensway.lensway_order_item;

-- lensway_order_item_raw (538.654)
select *
from lensway.lensway_order_item_raw
limit 1000;

select count(*)
from lensway.lensway_order_item_raw;

-- lensway_order_item_status_raw (378.473)
select *
from lensway.lensway_order_item_status_raw
limit 1000;

select count(*)
from lensway.lensway_order_item_status_raw;


-- lensway_order_item_vision (226.215)
select *
from lensway.lensway_order_item_vision
limit 1000;

select count(*)
from lensway.lensway_order_item_vision;

-- lensway_order_item_glasses_bundle (72.941)
select *
from lensway.lensway_order_item_glasses_bundle
limit 1000;

select count(*)
from lensway.lensway_order_item_glasses_bundle;

-- lensway_order_item_glasses_frame (72.941)
select *
from lensway.lensway_order_item_glasses_frame
limit 1000;

select count(*)
from lensway.lensway_order_item_glasses_frame;


-- -----------------------------------------------

-- lensway_order_address (382.408 - 384.308)
select *
from lensway.lensway_order_address
limit 1000;

select count(*)
from lensway.lensway_order_address;

