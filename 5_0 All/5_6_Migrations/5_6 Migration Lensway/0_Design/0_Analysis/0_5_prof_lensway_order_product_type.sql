
select item_id, order_id, created_at, updated_at, 
    item_product_family, item_product_type,
  product_id, product_type, sku, name, description, 
  qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, weight, row_weight, 
  base_price, base_tax_amount, base_discount_amount, base_amount_refunded, base_row_total
from lensway.lensway_order_item
where order_id IN (3668344851, 3518267393, 3518267392)
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p'), item_product_family, item_product_type 
limit 1000;

select order_id, count(distinct item_product_family)
from lensway.lensway_order_item
where order_id IN (3668344851, 3518267393, 3518267392)
  and item_product_family <> 'LENSES'
group by order_id
limit 1000;

-- --------------------------------------

select order_id, count(distinct item_product_family)
from lensway.lensway_order_item
where item_product_family <> 'LENSES'
group by order_id
limit 1000;

select num_dist_orders, count(*)
from
  (select order_id, count(distinct item_product_family) num_dist_orders
  from lensway.lensway_order_item
  where item_product_family <> 'LENSES'
  group by order_id) t
group by num_dist_orders
order by num_dist_orders

-- --------------------------------------
create table lensway.lensway_aux_order_pr_family as
  select distinct oi.order_id, oi_p.num_dif_pr, oi.item_product_family
  from
      (select order_id, count(distinct item_product_family) num_dif_pr
      from lensway.lensway_order_item
      where item_product_family <> 'LENSES'
      group by order_id) oi_p
    inner join
      lensway.lensway_order_item oi on oi_p.order_id = oi.order_id and oi.item_product_family <> 'LENSES';

select *
from lensway.lensway_aux_order_pr_family
order by order_id, item_product_family
limit 1000

  select num_dif_pr, item_product_family, count(*)
  from lensway.lensway_aux_order_pr_family
  group by num_dif_pr, item_product_family
  order by num_dif_pr, item_product_family
  limit 1000

select count(*), avg(o.base_grand_total)
from
    lensway.lensway_aux_order_pr_family oi
  inner join
    lensway.lensway_order o on oi.order_id = o.entity_id
where oi.item_product_family = 'ACCESSORIES' and oi.num_dif_pr = 1 

----------------------------------------------

select order_id, item_product_family
from lensway.lensway_aux_order_pr_family
where num_dif_pr = 2
order by order_id, item_product_family
limit 10000

select t1.order_id, concat(t1.item_product_family, ' - ', t2.item_product_family)
from
    (select order_id, item_product_family
    from lensway.lensway_aux_order_pr_family
    where num_dif_pr = 2) t1
  inner join
    (select order_id, item_product_family
    from lensway.lensway_aux_order_pr_family
    where num_dif_pr = 2) t2 on t1.order_id = t2.order_id and t1.item_product_family <> t2.item_product_family 
      and t1.item_product_family < t2.item_product_family
where concat(t1.item_product_family, ' - ', t2.item_product_family) = 'CONTACTS - GLASSES'

select conc_product_family, count(*)
from 
  (select t1.order_id, concat(t1.item_product_family, ' - ', t2.item_product_family) conc_product_family
  from
      (select order_id, item_product_family
      from lensway.lensway_aux_order_pr_family
      where num_dif_pr = 2) t1
    inner join
      (select order_id, item_product_family
      from lensway.lensway_aux_order_pr_family
      where num_dif_pr = 2) t2 on t1.order_id = t2.order_id and t1.item_product_family <> t2.item_product_family 
        and t1.item_product_family < t2.item_product_family) t
group by conc_product_family
order by conc_product_family