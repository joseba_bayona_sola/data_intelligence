
-- lensway_customercredit_marketing (14.061)
select *
from lensway.lensway_customercredit_marketing
limit 1000;

select uidpk, customer_uid, creditAmount, currency, 
  createdDate, expiry_date, description
from lensway.lensway_customercredit_marketing
-- where customer_uid in (4788322308, 6036193521)
order by customer_uid, createdDate, customer_uid
limit 1000;

select count(*)
from lensway.lensway_customercredit_marketing;

select o.*, 
  cc.uidpk, cc.customer_uid, cc.creditAmount, cc.currency, cc.createdDate, cc.expiry_date, cc.description
from 
  (select entity_id, increment_id, customer_id, created_at,
    base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total,
    -- base_tax_amount, base_customer_balance_amount,
    
    base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount bs
  from lensway.lensway_order
  where (base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount) <> 0) o
inner join
  lensway.lensway_customercredit_marketing cc on o.customer_id = cc.customer_uid and o.bs = cc.creditAmount
order by o.entity_id


-- lensway_customercredit_referral (289)
select *
from lensway.lensway_customercredit_referral
limit 1000;

select uidpk, customer_uid, creditAmount, currency, 
  createdDate, expiry_date, description
from lensway.lensway_customercredit_referral
-- where customer_uid in (4788322308, 6036193521)
order by customer_uid, createdDate, customer_uid
limit 1000;

select count(*)
from lensway.lensway_customercredit_referral;

select o.*, 
  cc.uidpk, cc.customer_uid, cc.creditAmount, cc.currency, cc.createdDate, cc.expiry_date, cc.description
from 
  (select entity_id, increment_id, customer_id, created_at,
    base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total,
    -- base_tax_amount, base_customer_balance_amount,
    
    base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount bs
  from lensway.lensway_order
  where (base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount) <> 0) o
inner join
  lensway.lensway_customercredit_referral cc on o.customer_id = cc.customer_uid and o.bs = cc.creditAmount
order by o.entity_id


-- lensway_customercredit_service (380)
select *
from lensway.lensway_customercredit_service
limit 1000;

select uidpk, customer_uid, creditAmount, currency, 
  createdDate, expiry_date, description
from lensway.lensway_customercredit_service
-- where customer_uid in (4788322308, 6036193521)
order by customer_uid, createdDate, customer_uid
limit 1000;

select count(*)
from lensway.lensway_customercredit_service;

select o.*, 
  cc.uidpk, cc.customer_uid, cc.creditAmount, cc.currency, cc.createdDate, cc.expiry_date, cc.description
from 
  (select entity_id, increment_id, customer_id, created_at,
    base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total,
    -- base_tax_amount, base_customer_balance_amount,
    
    base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount bs
  from lensway.lensway_order
  where (base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount) <> 0) o
inner join
  lensway.lensway_customercredit_service cc on o.customer_id = cc.customer_uid and o.bs = cc.creditAmount
order by o.entity_id