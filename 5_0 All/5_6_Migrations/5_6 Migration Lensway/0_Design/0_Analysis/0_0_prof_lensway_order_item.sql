
select entity_id, 
  increment_id, state, status, 
  store_id, store_name, customer_id, 
  created_at, STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
  base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total, 
  base_tax_amount, base_customer_balance_amount 
  base_subtotal_refunded, base_total_refunded
-- select *
from lensway.lensway_order
where entity_id IN (3668344851, 3518267393, 3518267392, 3831792261, 10008395797, 10146611237)
-- order by created_at_d
-- order by increment_id
limit 1000;

select *
from lensway.lensway_order_item
where order_id IN (3668344851, 3518267393, 3518267392, 3831792261, 10008395797, 10146611237)
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') 
limit 1000;

select item_id, order_id, created_at, updated_at, 
  item_product_family, item_product_type,
  product_id, product_type, sku, name, description, 
  qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, weight, row_weight, 
  base_price, base_tax_amount, base_discount_amount, base_amount_refunded, base_row_total
from lensway.lensway_order_item
where order_id IN (3668344851, 3518267393, 3518267392, 3831792261, 10008395797, 10146611237)
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p'), item_product_family, item_product_type 
limit 1000;

  select item_id, order_id, created_at, updated_at, 
    item_product_family, item_product_type,
    product_id, product_type, sku, name, description, 
    qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, weight, row_weight, 
    base_price, base_tax_amount, base_discount_amount, base_amount_refunded, base_row_total
  from lensway.lensway_order_item
  where 
    qty_ordered <> 1
    -- qty_canceled <> 0 and qty_invoiced <> 0 and qty_refunded <> 0 and qty_shipped <> 0 -- (0)
    -- weight <> 0 and row_weight <> 0 -- (0)
    -- base_tax_amount <> 0 -- (0)
    -- base_discount_amount <> 0 -- (0)
    -- base_amount_refunded <> 0 -- (300k)
    -- base_row_total - (base_price * qty_ordered) <> 0 -- (70k)
  order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') 
  limit 1000;

-- --------------------------------

select product_type, count(*)
from lensway.lensway_order_item
group by product_type
order by product_type;

select item_product_family, count(*)
from lensway.lensway_order_item
group by item_product_family
order by item_product_family;

select item_product_type, count(*)
from lensway.lensway_order_item
group by item_product_type
order by item_product_type;

select item_product_family, item_product_type, count(*)
from lensway.lensway_order_item
group by item_product_family, item_product_type
order by item_product_family, item_product_type;

  select item_id, order_id, created_at, updated_at, 
    item_product_family, item_product_type,
    product_id, product_type, sku, name, description, 
    qty_ordered, qty_canceled, qty_invoiced, qty_refunded, qty_shipped, weight, row_weight, 
    base_price, base_tax_amount, base_discount_amount, base_amount_refunded, base_row_total
  from lensway.lensway_order_item
  where 
    -- item_product_family = 'LENSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
    item_product_type = 'EyeglassesLensType' -- EyeglassesLensType - EyeglassesLensWithAddPower - EyeglassesOption - EyeglassesTint
  order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') 
  limit 1000;
  
 -- -------------------------------------------- 
 
  select item_product_family, item_product_type, product_id, -- sku, 
    name, base_price, 
    count(*)
  from lensway.lensway_order_item
  where 
    item_product_family = 'GLASSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
    -- item_product_type = 'EyeglassesLensType' -- EyeglassesLensType - EyeglassesLensWithAddPower - EyeglassesOption - EyeglassesTint
    
  group by item_product_family, item_product_type, product_id-- ,  sku
    , name, base_price
  order by item_product_family, item_product_type, product_id-- , sku
    -- , name, base_price

 -- --------------------------------------------   
 
  select item_product_family, item_product_type, product_id, sku, name, count(*) -- 54722k
  from lensway.lensway_order_item
  -- where item_product_family = 'LENSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
  group by item_product_family, item_product_type, product_id, sku, name
  order by item_product_family, item_product_type, product_id, sku, name

  select item_product_family, item_product_type, product_id, sku, count(distinct name), count(*) -- 52831k
  from lensway.lensway_order_item
  -- where item_product_family = 'LENSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
  group by item_product_family, item_product_type, product_id, sku
  having count(distinct name) > 1 -- 758
  order by item_product_family, item_product_type, product_id, sku
 
  select item_product_family, item_product_type, product_id, count(distinct sku), count(distinct name), count(*) -- 9810
  from lensway.lensway_order_item
  -- where item_product_family = 'LENSES' -- ACCESSORIES - CONTACTS - GLASSES - LENSES - SUNGLASSES
  group by item_product_family, item_product_type, product_id
  having count(distinct sku) > 1 -- 77
  order by item_product_family, item_product_type, product_id

 -- --------------------------------------------    

select *
from lensway.lensway_order_item
where item_product_family = 'GLASSES' and item_product_type = 'EyeglassesFrame'
  and base_price <> base_row_total
  -- and qty_ordered <> 1
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p'), item_product_family, item_product_type
limit 1000;


select item_id, order_id, created_at, 
  item_product_family, item_product_type,
  product_id, sku, name, 
  qty_ordered, 
  base_price, base_row_total, base_tax_amount, base_discount_amount, base_amount_refunded
from lensway.lensway_order_item
where item_product_family = 'GLASSES' and item_product_type = 'EyeglassesFrame' 
  and base_price <> base_row_total
  -- and qty_ordered <> 1
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p'), item_product_family, item_product_type;

select count(*), count(distinct order_id), sum(base_price), sum(base_row_total)
from lensway.lensway_order_item
where item_product_family = 'GLASSES' and item_product_type = 'EyeglassesFrame'
  and base_price <> base_row_total;
  

select item_id, order_id, created_at, 
  item_product_family, item_product_type,
  product_id, sku, name, 
  qty_ordered, 
  base_price, base_row_total, base_tax_amount, base_discount_amount, base_amount_refunded
from lensway.lensway_order_item
where item_product_family <> 'GLASSES' 
  and (base_price * qty_ordered) <> base_row_total
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p'), item_product_family, item_product_type;

select item_product_family, count(*), count(distinct order_id), sum(base_price), sum(base_price * qty_ordered), 
  sum(base_row_total)
from lensway.lensway_order_item
where item_product_family <> 'GLASSES' 
  and (base_price * qty_ordered) <> base_row_total
group by item_product_family
order by item_product_family

-- ----------------------------------------------------------------------------------

select oi.*,
  o.entity_id, o.increment_id, o.customer_id, c.email, ch.password_val
from
     (select item_id, order_id, created_at, 
      item_product_family, item_product_type,
      product_id, sku, name, 
      qty_ordered, 
      base_price, base_row_total, base_tax_amount, base_discount_amount, base_amount_refunded
    from lensway.lensway_order_item
    where item_product_family = 'GLASSES' and item_product_type = 'EyeglassesFrame'
      and base_price <> base_row_total) oi
  inner join
    lensway.lensway_order o on oi.order_id = o.entity_id
  inner join
    lensway.lensway_customer c on o.customer_id = c.entity_id
  inner join
    lensway.lensway_common_hashes ch on c.password_hash = ch.lensway_hash
   
order by c.entity_id;
  