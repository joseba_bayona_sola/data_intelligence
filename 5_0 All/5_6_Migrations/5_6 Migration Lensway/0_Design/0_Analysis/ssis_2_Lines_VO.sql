
SELECT 
	mo.*,
	CAST(CAST(mold.power AS DECIMAL(6,2)) AS CHAR(20))AS POWER,
	CASE mold.base_curve WHEN 'M' THEN '8.6' WHEN 'FM' THEN '8.4' WHEN 'SM' THEN '7.5' ELSE mold.base_curve END base_curve,
	CAST(CAST(mold.diameter AS DECIMAL(6,1)) AS CHAR(20)) AS diameter,
	CASE WHEN CAST(mold.`add` AS DECIMAL(6,2))=0.00 THEN LEFT(mold.`add`,1) ELSE CAST(mold.`add` AS DECIMAL(6,2)) END AS `add`,
	CAST(CAST(mold.axis AS UNSIGNED)  AS CHAR(20)) AS axis,
	CASE WHEN mold.cylinder IN ('D','N') THEN mold.cylinder ELSE '' END AS dominant,
	CASE WHEN mold.cylinder NOT IN ('D','N') THEN mold.cylinder ELSE '' END AS cylinder,
	mold.old_item_id,
	mold.product_id AS ProductID,
	mold.Quantity,
	row_total,
	mold.eye,
	mold.color

FROM 
		migrate_orderlinedata mold
	INNER JOIN 
		(SELECT old_order_id, 
			base_discount_amount, base_discount_amount_exc_vat, base_grand_total, base_shipping_amount,
			created_at, invoice_date, shipment_date,
			order_currency AS CurrencyID, REPLACE(DomainName,'www.','') AS DomainName, ExchangeRate, 
			tax_rate, IFNULL(tax_free_amount,0) AS tax_free_amount,
			orderstatusname,
			base_grand_total+base_discount_amount_exc_vat* (1+tax_rate/100)- base_shipping_amount_exc_vat * (1+tax_rate/100)
				-IFNULL(tax_free_amount,0)-(base_discount_amount_exc_vat * (1+ tax_rate/100) - base_discount_amount) AS base_sub_total_inc_vat
		FROM migrate_orderdata) mo ON mo.old_order_id=mold.old_order_id;

-- Flow

	-- Lookup ORDER STATUS: DW_Proforma.dbo.mapping_order_status (on orderStatusName) get (dw_status, dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment)

	-- Fuzzy Lookup PRODUCT COLOURS: DW_Proforma.dbo.vwProductColours (on product_id, color) get (map_colour)

	-- Derived Column
		-- product_params: (DT_STR,255,1252)UPPER((REPLACENULL(add,"") != "" ? "AD" + add : "") + (REPLACENULL(axis,"") != "" ? "AX" + axis : "") + (REPLACENULL(base_curve,"") != "" ? "BC" + base_curve : "") + (_Similarity_color >= 0.65 ? "CO" + colour : "") + (REPLACENULL(cylinder,"") != "" ? "CY" + cylinder : "") + (REPLACENULL(diameter,"") != "" ? "DI" + diameter : "") + (REPLACENULL(dominant,"") != "" ? "DO" + dominant : "") + (REPLACENULL(power,"") != "" ? "PO" + power : ""))
		-- paymenttypetotal: (base_discount_amount_exc_vat + base_discount_amount_exc_vat * tax_rate / 100) - base_discount_amount
		-- product_params_sub1: (DT_STR,255,1252)((REPLACENULL(base_curve,"") != "" ? "BC" + base_curve : "") + (REPLACENULL(diameter,"") != "" ? "DI" + diameter : "") + (REPLACENULL(power,"") != "" ? "PO" + power : ""))
		-- invoice_date: @[User::override_invoice_date_with_shipment_date] ==  TRUE  ? shipment_date : invoice_date

	-- Lookup PRODUCT SKU: DW_Proforma.dbo.vwProductSKUs (on product_id, product_params) get (sku, is_lens, category_id, product_type, name, lens_days, product_params_lookup, AD, CO)

	-- Conditional Split SKU
		-- SKU Found

		-- SKU Not Found
			-- Lookup PRODUCT: DW_Proforma.dbo.vwProductSKUs (on product_id) get (sku, is_lens, category_id, product_type, name, lens_days) 

		-- Union: SKU Found + SKU Not Found		

	-- Derived Column
		-- store_name: (DT_STR,100,1252)(TRIM(REPLACENULL(DomainName,"")) == "" ? @[User::def_domain_name] : TRIM(REPLACENULL(DomainName,"")))
		-- order_no: (DT_STR,50,1252)(@[User::store_global_pk] + (DT_STR,50,1252)(old_order_id)) 
		-- document_date_order: created_at
		-- document_updated_at: created_at
		-- document_id: (DT_R8)((DT_STR,50,1252)(@[User::id_prefix]) + REPLICATE("0",(@[User::id_length]) - (LEN((DT_STR,50,1252)(old_order_id)) + LEN((DT_STR,50,1252)(@[User::id_prefix])))) + (DT_STR,50,1252)(old_order_id))
		-- order_date: created_at
		-- updated_at: created_at
		-- local_to_global_rate: (DT_STR,255,1252)CurrencyID
		-- order_currency_code: (DT_STR,255,1252)CurrencyID

		-- line_id: (DT_R8)((DT_STR,50,1252)(@[User::id_prefix]) + REPLICATE("0",(@[User::id_length]) - (LEN((DT_STR,50,1252)(old_item_id)) + LEN((DT_STR,50,1252)(@[User::id_prefix])))) + (DT_STR,50,1252)(old_item_id))
		-- lens_eye: (DT_STR,1,1252)LEFT(eye,1)
		-- lens_base_curve: (DT_STR,100,1252)base_curve
		-- lens_diameter: (DT_STR,100,1252)diameter
		-- lens_power: (DT_STR,100,1252)power
		-- lens_cylinder: (DT_STR,100,1252)cylinder
		-- lens_axis: (DT_STR,100,1252)axis
		-- lens_addition: (DT_STR,100,1252)AD
		-- lens_dominance: (DT_STR,100,1252)DO
		-- lens_colour: (DT_STR,100,1252)(REPLACENULL(CO,"") == "" ? color : CO)
		-- qty: (DT_NUMERIC,13,4)Quantity
		-- free_shipping: base_shipping_amount > 0 ? 0 : 1
		-- product_size: (DT_STR,1,1252)""
		-- product_colour: (DT_STR,1,1252)""

		-- document_date_invoice: REPLACENULL(invoice_date,created_at)
		-- document_date_shipment: shipment_date
		-- document_updated_at_invoice: REPLACENULL(invoice_date,created_at) 
		-- document_updated_at_shipment: shipment_date

		-- order_id: (DT_R8)((DT_STR,50,1252)(@[User::id_prefix]) + REPLICATE("0",(@[User::id_length]) - (LEN((DT_STR,50,1252)(old_order_id)) + LEN((DT_STR,50,1252)(@[User::id_prefix])))) + (DT_STR,50,1252)(old_order_id))
		-- productID: REPLACENULL(sku,"") == "" ? 2327 : ProductID
		-- sku: ((REPLACENULL(sku,"") == "") || (REPLACENULL(product_params,"") != REPLACENULL(product_params_lookup,""))) ? "ADHOCPROD" : sku
		
		-- base_shipping_amount: base_shipping_amount + paymenttypetotal
		-- base_discount_amount: base_discount_amount + paymenttypetotal	
		
	-- Script ADD SIGN + TO PARAMS			

		-- If Not IsNothing(Row.lenspower) Then Row.lenspower = IIf(Val(Row.lenspower) > 0, "+" + Row.lenspower, Row.lenspower).ToString
        -- If Not IsNothing(Row.lenscylinder) Then Row.lenscylinder = IIf(Val(Row.lenscylinder) > 0, "+" + Row.lenscylinder, Row.lenscylinder).ToString

	-- Local Calculations (Derived Column)
		-- local_price_inc_vat: (DT_NUMERIC,13,4)(Quantity == 0 ? 0 : (row_total / Quantity))
		-- local_shipping_inc_vat: (DT_NUMERIC,13,4)(base_sub_total_inc_vat == 0 ? 0 : (row_total / base_sub_total_inc_vat) * base_shipping_amount)
		-- local_discount_inc_vat: (DT_NUMERIC,13,4)(base_sub_total_inc_vat == 0 ? 0 : -ABS((row_total / base_sub_total_inc_vat) * base_discount_amount))
		-- local_subtotal_inc_vat: (DT_NUMERIC,13,4)row_total
		-- local_subtotal_exc_vat: (DT_NUMERIC,13,4)(row_total / (1 + tax_rate / 100))
		-- local_prof_fee: (DT_NUMERIC,13,4)(base_sub_total_inc_vat == 0 ? 0 : (row_total / base_sub_total_inc_vat) * tax_free_amount)

		-- local_price_exc_vat: (DT_NUMERIC,13,4)(local_price_inc_vat / (1 + tax_rate / 100))
		-- local_shipping_exc_vat: (DT_NUMERIC,13,4)(local_shipping_inc_vat / (1 + tax_rate / 100))
		-- local_discount_exc_vat: (DT_NUMERIC,13,4)-ABS(local_discount_inc_vat / (1 + tax_rate / 100))
		-- local_subtotal_vat: (DT_NUMERIC,13,4)(local_line_subtotal_inc_vat - local_line_subtotal_exc_vat)
		-- local_line_total_inc_vat: (DT_NUMERIC,13,4)(row_total + local_prof_fee + local_shipping_inc_vat - ABS(local_discount_inc_vat) * SIGN(base_discount_amount))

		-- local_price_vat: (DT_NUMERIC,13,4)(local_price_inc_vat - local_price_exc_vat)
		-- local_shipping_vat: (DT_NUMERIC,13,4)(local_shipping_inc_vat - local_shipping_exc_vat)
		-- local_discount_vat: (DT_NUMERIC,13,4)-ABS(local_discount_inc_vat - local_discount_exc_vat)
		-- local_line_total_exc_vat: (DT_NUMERIC,13,4)(local_line_total_inc_vat / (1 + tax_rate / 100))

		-- local_line_total_vat: (DT_NUMERIC,13,4)(local_line_total_inc_vat - local_line_total_exc_vat)
		-- local_margin_amount: local_line_total_exc_vat
		-- local_margin_percent: (DT_NUMERIC,12,4)1

	-- Global Calculations (Derived Column)
		-- global_discount_inc_vat: (DT_NUMERIC,13,4)(local_discount_inc_vat * local_to_global_rate)
		-- global_discount_exc_vat: (DT_NUMERIC,13,4)(local_discount_exc_vat * local_to_global_rate)

		-- global_discount_vat: (DT_NUMERIC,13,4)-ABS(global_discount_inc_vat - global_discount_exc_vat)

	-- Price Type (Derived Column)
		-- price_type: (DT_STR,20,1252)(local_discount_exc_vat != 0 ? "Discounted" : "Regular")

	-- Copy Columns - Invoice
		-- order_id, order_no to invoice_id, invoice_no

	-- Copy Columns - Shipment
		-- order_id, order_no to shipment_id, shipment_no

	-- Multicast

	-- Conditional Split: Order - Invoice - Shipment
		-- valid_orders: ISNULL(dw_document_type_order) ==  FALSE  && ISNULL(document_date_order) ==  FALSE 
		-- valid_invoices: ISNULL(dw_document_type_invoice) ==  FALSE  && ISNULL(dw_document_type_invoice) ==  FALSE 
		-- valid_shipments: ISNULL(dw_document_type_shipment) ==  FALSE  && ISNULL(document_date_shipment) ==  FALSE 

	-- Insert
