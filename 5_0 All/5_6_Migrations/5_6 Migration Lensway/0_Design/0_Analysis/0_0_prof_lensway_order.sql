
select *
from lensway.lensway_order
-- where customer_id = 6979452937
where entity_id = 4458348585
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') 
limit 1000;


select entity_id, 
  increment_id, state, status, 
  store_id, store_name, customer_id, 
  created_at, STR_TO_DATE(created_at, '%b %d %Y %h:%i%p') created_at_d,
  base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total, 
  base_tax_amount, base_customer_balance_amount 
  
from lensway.lensway_order
where customer_id = 6979452937
order by created_at_d
-- order by increment_id
limit 1000;

select state, count(*)
from lensway.lensway_order
group by state
order by state;

select status, count(*)
from lensway.lensway_order
group by status
order by status;

select 'Dec  6 2011  5:31AM', STR_TO_DATE('Dec  6 2011  5:31AM', '%b %d %Y %h:%i%p');


select *
from lensway.lensway_order
where base_tax_amount > 0 or tax_amount > 0
limit 1000;

select coupon_code, count(*), sum(base_discount_amount)
from lensway.lensway_order
-- where base_discount_amount > 0 
group by coupon_code
order by count(*) desc
limit 1000;

select coupon_code, count(*), sum(base_discount_amount)
from lensway.lensway_order
where base_discount_amount = 0 
group by coupon_code
order by count(*) desc
limit 1000;

-- -------------

select entity_id, increment_id, customer_id, created_at,
  base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total, 
  base_tax_amount, base_customer_balance_amount,
  
  base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount bs
from lensway.lensway_order
where (base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount) <> 0
order by STR_TO_DATE(created_at, '%b %d %Y %h:%i%p')  
limit 10000;

select bs, count(*)
from
  (select 
    base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total, 
    base_tax_amount, base_customer_balance_amount,
    
    base_grand_total - base_subtotal - base_shipping_amount - handling_charge + base_discount_amount bs
  from lensway.lensway_order) t
group by bs
order by count(*) desc, bs
