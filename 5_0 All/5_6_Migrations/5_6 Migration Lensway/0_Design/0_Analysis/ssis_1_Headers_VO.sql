

select old_customer_id, case when email ='' then null else email end as email
from migrate_customerdata

SELECT 
	mo.old_order_id,
	created_at, invoice_date, shipment_date,
	mo.email,
	OrderStatusName,
	mo.old_customer_id,
	cfod.customer_first_order_date,
	billing_prefix, billing_firstname, billing_lastname, billing_company,
	billing_street1, billing_street2, billing_city, billing_region, billing_region_id, billing_postcode, billing_telephone, billing_country,
	shipping_prefix, shipping_firstname, shipping_lastname, shipping_company, 
	shipping_street1, shipping_street2, shipping_city, shipping_region, shipping_region_id, shipping_postcode, shipping_country, shipping_description, shipping_telephone,
	order_currency as CurrencyID,
	IFNULL(total_qty,0) AS total_qty,
	CAST((base_grand_total+base_discount_amount- base_shipping_amount) AS DECIMAL(29,4)) AS sub_total,
	CASE WHEN IFNULL(has_lens,0)>0 THEN 1 ELSE 0 END has_lens,
	ExchangeRate,
	base_shipping_amount, base_grand_total, base_discount_amount,
	replace(DomainName,'www.','') as DomainName,
	base_grand_total_exc_vat, base_shipping_amount_exc_vat, base_discount_amount_exc_vat,
	tax_free_amount, tax_rate
FROM 
		migrate_orderdata mo
	LEFT OUTER JOIN 
		(SELECT old_order_id,
			MAX(
				CASE WHEN POWER IS NOT NULL THEN 1 ELSE 0 END+
				CASE WHEN base_curve IS NOT NULL THEN 1 ELSE 0 END+
				CASE WHEN diameter IS NOT NULL THEN 1 ELSE 0 END+
				CASE WHEN `add` IS NOT NULL THEN 1 ELSE 0 END+
				CASE WHEN axis IS NOT NULL THEN 1 ELSE 0 END+
				CASE WHEN cylinder IS NOT NULL THEN 1 ELSE 0 END) AS has_lens, 
			SUM(Quantity) AS total_qty 
		FROM migrate_orderlinedata 
		GROUP BY old_order_id) mol ON mol.old_order_id=mo.old_order_id
	LEFT OUTER JOIN
		(SELECT old_customer_id, MIN(created_at) AS customer_first_order_date 
		FROM migrate_orderdata
		GROUP BY old_customer_id) cfod ON cfod.old_customer_id=mo.old_customer_id
where orderstatusname is not null;

-- Flow

	-- Join Customer - Order sources by FIRST customer_id (old_customer_id), SECOND customer_email (email)

	-- Derived Column
		-- email_ucase: UPPER(REPLACENULL(replace_order_customer_email,order_email))
		-- email: order_email
		-- old_customer_id: ISNULL(replace_order_customer_email) ==  TRUE  ? REPLACENULL(replace_order_old_customer_id,old_customer_id) : old_customer_id
		-- invoice_date: @[User::override_invoice_date_with_shipment_date] ==  TRUE  ? shipment_date : invoice_date

	-- Lookup ORDER STATUS: DW_Proforma.dbo.mapping_order_status (on orderStatusName) get (dw_status, dw_document_type_order, dw_document_type_invoice, dw_document_type_shipment)

	-- Lookup SHIPPING CARRIER, METHOD: DW_Proforma.dbo.mapping_shipping_carrier_method (on shipping_description) get (new_shipping_carrier, new_shipping_method)

	-- Lookup CUSTOMER: DW_GetLenses.dbo.customers (on email) get (customer_id)

	-- Lookup CUSTOMER OLD: DW_GetLenses.dbo.customers (left(old_customer_id,5)='VISIO') (on old_customer_id) get (customer_id)

	-- Derived Column
		-- store_name: (DT_STR,100,1252)(TRIM(REPLACENULL(DomainName,"")) == "" ? @[User::def_domain_name] : TRIM(REPLACENULL(DomainName,"")))
		-- order_id: (DT_R8)((DT_STR,50,1252)(@[User::id_prefix]) + REPLICATE("0",(@[User::id_length]) - (LEN((DT_STR,50,1252)(old_order_id)) + LEN((DT_STR,50,1252)(@[User::id_prefix])))) + (DT_STR,50,1252)(old_order_id))
		-- order_no: (DT_STR,50,1252)(@[User::store_global_pk] + (DT_STR,50,1252)(old_order_id)) 
		-- document_date_order: created_at
		-- document_updated_at: created_at
		-- document_id: (DT_R8)((DT_STR,50,1252)(@[User::id_prefix]) + REPLICATE("0",(@[User::id_length]) - (LEN((DT_STR,50,1252)(old_order_id)) + LEN((DT_STR,50,1252)(@[User::id_prefix])))) + (DT_STR,50,1252)(old_order_id))
		-- order_date: created_at
		-- updated_at: created_at
		-- customer_id: ISNULL([Lookup customer_id by old_customer_id].customer_id) ==  FALSE  ? [Lookup customer_id by old_customer_id].customer_id : (ISNULL([Lookup customer_id by email].customer_id) ==  FALSE  ? [Lookup customer_id by email].customer_id : old_customer_id)
		-- shipping_carrier: (DT_STR,255,1252)(TRIM(REPLACENULL(new_shipping_carrier,"")) != "" ? new_shipping_carrier : shipping_description)
		-- shipping_method: (DT_STR,255,1252)(TRIM(REPLACENULL(new_shipping_method,"")) != "" ? new_shipping_method : shipping_description)
		-- billing
		-- shipping
		-- local_to_global_rate: (DT_STR,255,1252)CurrencyID
		-- order_currency_code: (DT_STR,255,1252)CurrencyID
		-- business_channel: (DT_STR,100,1252)""
		-- order_type: (DT_STR,255,1252)"Web"
		-- customer_email - customer_firstname - customer_lastname
		-- document_no:  (DT_STR,50,1252)(@[User::store_global_pk] + (DT_STR,50,1252)(old_order_id))
		-- total_qty_new: (DT_NUMERIC,12,4)total_qty
		-- business_source: (DT_STR,255,1252)""
		-- document_date_invoice: REPLACENULL(invoice_date,created_at)
		-- document_date_shipment: shipment_date
		-- document_updated_at_invoice: REPLACENULL(invoice_date,created_at) 
		-- document_updated_at_shipment: shipment_date
		-- paymenttypetotal: (base_discount_amount_exc_vat + base_discount_amount_exc_vat * tax_rate / 100) - base_discount_amount
		-- length_of_time_to_invoice: DATEDIFF("d",created_at,invoice_date)
		-- length_of_time_invoice_to_this_shipment: DATEDIFF("d",invoice_date,shipment_date)

	-- Fuzzy Lookup BUSINESS CHANNEL: DW_GetLenses.dbo.v_business_channels (on business_channel)

	-- Derived Column
		-- business_channel: _Similarity_business_channel >= 0.8 ? business_channel_flookup : business_channel

	-- Local Calculations (Derived Column)
		-- local_shipping_inc_vat: (DT_NUMERIC,12,4)(base_shipping_amount + paymenttypetotal)
		-- local_shipping_exc_vat: (DT_NUMERIC,12,4)(base_shipping_amount_exc_vat + paymenttypetotal / (1 + tax_rate / 100))
		-- local_discount_inc_vat: (DT_NUMERIC,13,4)-ABS(base_discount_amount + paymenttypetotal)
		-- local_discount_exc_vat: (DT_NUMERIC,13,4)-ABS(base_discount_amount_exc_vat)
		-- local_total_inc_vat: (DT_NUMERIC,12,2)base_grand_total
		-- local_total_exc_vat: (DT_NUMERIC,12,2)base_grand_total_exc_vat
		-- local_subtotal_inc_vat: (DT_NUMERIC,12,4)sub_total
		-- local_subtotal_exc_vat: (DT_NUMERIC,12,4)(sub_total / (1 + tax_rate / 100))
		-- local_prof_fee: (DT_NUMERIC,12,4)tax_free_amount

		-- local_shipping_vat: (DT_NUMERIC,12,4)(local_shipping_inc_vat - local_shipping_exc_vat)
		-- local_discount_vat: (DT_NUMERIC,13,4)-ABS(local_discount_inc_vat - local_discount_exc_vat)
		-- local_total_vat: (DT_NUMERIC,12,4)(local_total_inc_vat - local_total_exc_vat)
		-- local_subtotal_vat: (DT_NUMERIC,12,4)(local_subtotal_inc_vat - local_subtotal_exc_vat)

	-- Global Calculations (Derived Column)
		-- global_discount_inc_vat: (DT_NUMERIC,13,4)(local_discount_inc_vat * local_to_global_rate)
		-- global_discount_exc_vat: (DT_NUMERIC,13,4)(local_discount_exc_vat * local_to_global_rate)

		-- global_discount_vat: (DT_NUMERIC,13,4)-ABS(global_discount_inc_vat - global_discount_exc_vat)

	-- Copy Columns - Invoice
		-- order_id, order_no to invoice_id, invoice_no

	-- Copy Columns - Shipment
		-- order_id, order_no to shipment_id, shipment_no


	-- Multicast

	-- Conditional Split: Order - Invoice - Shipment
		-- valid_orders: ISNULL(dw_document_type_order) ==  FALSE  && ISNULL(document_date_order) ==  FALSE 
		-- valid_invoices: ISNULL(dw_document_type_invoice) ==  FALSE  && ISNULL(dw_document_type_invoice) ==  FALSE 
		-- valid_shipments: ISNULL(dw_document_type_shipment) ==  FALSE  && ISNULL(document_date_shipment) ==  FALSE 

	-- Insert



