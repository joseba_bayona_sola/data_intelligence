
select *
from DW_GetLenses.dbo.customers
where old_access_cust_no = 'LENSWAYUK'
where created_in is null

select top 1000 store_name, order_id, order_no, document_type, document_date, customer_id, 
	business_source, business_channel, customer_order_seq_no, 
	total_qty, local_total_exc_vat
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 15
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK'
	-- where created_in is null
	)
--order by business_channel, business_source, order_id
order by order_id

select sum(local_total_exc_vat)
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 15
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')

select business_channel, business_source, count(*) num_orders, sum(local_total_exc_vat) value_exc_vat
from DW_GetLenses.dbo.order_headers
where document_date > getutcdate() - 15
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')
group by business_source, business_channel
order by business_channel, business_source

--- 

select top 1000 store_name, order_id, order_no, document_no, document_type, document_date, customer_id, 
	business_source, business_channel, customer_order_seq_no, 
	total_qty, local_total_exc_vat
from DW_GetLenses.dbo.invoice_headers
where document_date > getutcdate() - 15
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')
	and year(document_date) = 2017 and month(document_date) = 2 and day(document_date) = 15
order by local_total_exc_vat
-- order by order_id

select count(*), sum(global_total_exc_vat), sum(local_total_exc_vat)
from DW_GetLenses.dbo.invoice_headers
where document_date > getutcdate() - 27
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')

select year(document_date) yyyy,  month(document_date) mm, day(document_date) dd, count(*), sum(global_total_exc_vat), sum(local_total_exc_vat)
from DW_GetLenses.dbo.invoice_headers
where document_date > getutcdate() - 27
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')
group by year(document_date), month(document_date), day(document_date)
order by year(document_date), month(document_date), day(document_date)


--- 

select count(*), sum(global_total_exc_vat), sum(local_total_exc_vat)
from DW_GetLenses.dbo.shipment_headers
where document_date > getutcdate() - 27
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')

select year(document_date) yyyy,  month(document_date) mm, day(document_date) dd, count(*), sum(global_total_exc_vat), sum(local_total_exc_vat)
from DW_GetLenses.dbo.shipment_headers
where document_date > getutcdate() - 27
	and customer_id in
	(select customer_id
	from DW_GetLenses.dbo.customers
	where old_access_cust_no = 'LENSWAYUK')
group by year(document_date), month(document_date), day(document_date)
order by year(document_date), month(document_date), day(document_date)


--------------------------------------------

select top 1000 oh.store_name, order_id, order_no, document_type, document_date, oh.customer_id, 
	business_source, oh.business_channel, customer_order_seq_no, 
	total_qty, local_total_exc_vat, 
	c.store_name, c.created_at, c.created_in, c.old_access_cust_no
from 
	DW_GetLenses.dbo.order_headers oh
inner join
	DW_GetLenses.dbo.customers c on oh.customer_id = c.customer_id
where document_date > getutcdate() - 15
	and order_no in ('8002654220', '8002654223', '8002656261', '8002657494', '8002656955')