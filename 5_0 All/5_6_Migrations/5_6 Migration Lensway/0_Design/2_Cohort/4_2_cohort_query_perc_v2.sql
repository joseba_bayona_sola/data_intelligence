
select t2.cust_seq_ord_num, t2.period, t2.month_diff, 
	sum(t2.base_grand_total_s) over (partition by t2.period) tot_m_base_grand_total_s,
	sum(t2.num_orders) over (partition by t2.period) tot_m_num_orders,
	t1.base_grand_total_s tot_base_grand_total_s, t1.num_orders tot_num_orders, 
	t2.base_grand_total_s, t2.base_grand_total_s*100/convert(decimal(10,2), t1.base_grand_total_s) perc_base_grand_total,
	t2.next_base_grand_total_s, t2.next_base_grand_total_s*100/convert(decimal(10,2), t1.base_grand_total_s) perc_next_base_grand_total,
	t2.num_orders, t2.num_orders*100/convert(decimal(10,2), t1.num_orders) perc_num_orders

from
		(select period, cust_seq_ord_num,
			sum(base_grand_total) base_grand_total_s, count(num_cust) num_orders, count(distinct customer_id) num_customers
		from
			(select customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 1 num_cust
			from
				(select customer_id, 
					created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
							rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lensway_cohort_v2
						where created_at_dt is not null
							 and item_product_family <> 'GLASSES'
							) t) t) t) t
		group by period, cust_seq_ord_num) t1
	inner join
		(select period, cust_seq_ord_num, month_diff, 
			sum(base_grand_total) base_grand_total_s, sum(next_base_grand_total) next_base_grand_total_s, 
			count(num_cust) num_orders, count(distinct customer_id) num_customers
		from
			(select customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 1 num_cust
			from
				(select customer_id, 
					created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
							rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lensway_cohort_v2
						where created_at_dt is not null 
							and item_product_family <> 'GLASSES'
							) t) t) t) t
		group by period, cust_seq_ord_num, month_diff) t2 on t1.period = t2.period and t1.cust_seq_ord_num = t2.cust_seq_ord_num
--where t1.num_orders <> t1.num_customers
where t2.period >= '2014-12-01'
order by t2.cust_seq_ord_num, t2.period, t2.month_diff


--------------------------------------------

select cust_seq_ord_num, period, month_diff,
	tot_m_base_grand_total_s,  
	tot_m_num_orders, 
	tot_base_grand_total_s, tot_base_grand_total_s*100/convert(decimal(10,2), tot_m_base_grand_total_s) perc_tot_base_grand_total,
	tot_num_orders, tot_num_orders*100/convert(decimal(10,2), tot_m_num_orders) perc_tot_num_orders,
	num_orders, perc_num_orders
from
	(select t2.cust_seq_ord_num, t2.period, t2.month_diff, 
		sum(t2.base_grand_total_s) over (partition by t2.period) tot_m_base_grand_total_s,
		sum(t2.num_orders) over (partition by t2.period) tot_m_num_orders,
		t1.base_grand_total_s tot_base_grand_total_s, t1.num_orders tot_num_orders, 
		t2.base_grand_total_s, t2.base_grand_total_s*100/convert(decimal(10,2), t1.base_grand_total_s) perc_base_grand_total,
		t2.next_base_grand_total_s, t2.next_base_grand_total_s*100/convert(decimal(10,2), t1.base_grand_total_s) perc_next_base_grand_total,
		t2.num_orders, t2.num_orders*100/convert(decimal(10,2), t1.num_orders) perc_num_orders

	from
			(select period, cust_seq_ord_num,
				sum(base_grand_total) base_grand_total_s, count(num_cust) num_orders, count(distinct customer_id) num_customers
			from
				(select customer_id, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					base_grand_total, next_base_grand_total, 1 num_cust
				from
					(select customer_id, 
						created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						base_grand_total, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_base_grand_total, next_cust_seq_ord_num
					from
						(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
							lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
						from
							(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
								rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lensway_cohort_v2
							where created_at_dt is not null 
								and item_product_family NOT IN ('GLASSES', 'SUNGLASSES')
								) t) t) t) t
			group by period, cust_seq_ord_num) t1
		inner join
			(select period, cust_seq_ord_num, month_diff, 
				sum(base_grand_total) base_grand_total_s, sum(next_base_grand_total) next_base_grand_total_s, 
				count(num_cust) num_orders, count(distinct customer_id) num_customers
			from
				(select customer_id, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					base_grand_total, next_base_grand_total, 1 num_cust
				from
					(select customer_id, 
						created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						base_grand_total, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_base_grand_total, next_cust_seq_ord_num
					from
						(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
							lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
						from
							(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
								rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lensway_cohort_v2
							where created_at_dt is not null 
								and item_product_family NOT IN ('GLASSES', 'SUNGLASSES')
								) t) t) t) t
			group by period, cust_seq_ord_num, month_diff) t2 on t1.period = t2.period and t1.cust_seq_ord_num = t2.cust_seq_ord_num) t
where cust_seq_ord_num = 1 and month_diff is null
order by cust_seq_ord_num, period, month_diff