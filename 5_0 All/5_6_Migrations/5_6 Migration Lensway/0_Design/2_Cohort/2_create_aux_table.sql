
create table DW_GetLenses_jbs.dbo.lensway_cohort(
	entity_id				bigint, 
	increment_id			int, 
	state					varchar(50), 
	status					varchar(50), 
	store_name				varchar(50), 
	customer_id				bigint, 
	created_at_dt			datetime, 
	created_at				date, 
	base_subtotal			decimal(12, 2), 
	base_shipping_amount	decimal(12, 2), 
	base_discount_amount	decimal(12, 2),
	base_grand_total		decimal(12, 2)); 

insert into DW_GetLenses_jbs.dbo.lensway_cohort(entity_id,
	increment_id, state, status, 
	store_name, customer_id, 
	created_at_dt, created_at,
	base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total)

	select cast(entity_id as bigint) entity_id,
		cast(increment_id as int) increment_id, state, status, 
		store_name, cast(customer_id as bigint) customer_id, 
		created_at_d created_at_dt, convert(date, created_at_d) created_at,
		cast(base_subtotal as decimal(12,2)) base_subtotal, cast(base_shipping_amount as decimal(12,2)) base_shipping_amount, 
		cast(base_discount_amount as decimal(12,2)) base_discount_amount, cast(base_grand_total as decimal(12,2)) base_grand_total
	from openquery(MYSQL_TEST, 
		'select entity_id, increment_id, state, status, 
			store_id, store_name, customer_id, 
			created_at, STR_TO_DATE(created_at, ''%b %d %Y %h:%i%p'') created_at_d,
			base_subtotal, base_shipping_amount, base_discount_amount, base_tax_amount, base_grand_total, base_customer_balance_amount
		from lensway.lensway_order limit 500000')


