
select count(*), count(distinct customer_id)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
where item_product_family = 'CONTACTS' and
	created_at between '2015-02-01' and '2016-12-31'

select count(*), count(distinct customer_id)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
where item_product_family = 'GLASSES' and
	created_at between '2015-02-01' and '2016-12-31'

--------------------------------------------------------

select top 1000 order_id, order_no, document_date, period, 
	customer_id, customer_order_seq_no,
	lead(customer_order_seq_no) over (partition by customer_id order by document_date, order_id) next_cust_seq_ord_num,
	count(*) over (partition by period) num_tot_month,
	count(*) over (partition by period, customer_order_seq_no) num_tot_month_ord_seq
from
	(select order_id, order_no, document_date, 
		cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
		customer_id, customer_order_seq_no 
	from DW_GetLenses.dbo.order_headers
	where store_name = 'visiondirect.co.uk'
		and document_type = 'ORDER'
		and document_date between '2015-02-01' and '2016-02-01') t
order by customer_id, document_date, customer_order_seq_no

--

select top 1000 order_id, order_no, document_date, period, 
		customer_id, customer_order_seq_no,
		next_cust_seq_ord_num, 
		num_tot_month, num_tot_month_ord_seq, 
		count(*) over (partition by period) num_tot_no_rep
from
	(select order_id, order_no, document_date, period, 
		customer_id, customer_order_seq_no,
		lead(customer_order_seq_no) over (partition by customer_id order by document_date, order_id) next_cust_seq_ord_num,
		count(*) over (partition by period) num_tot_month,
		count(*) over (partition by period, customer_order_seq_no) num_tot_month_ord_seq
	from
		(select order_id, order_no, document_date, 
			cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
			customer_id, customer_order_seq_no 
		from DW_GetLenses.dbo.order_headers
		where store_name = 'visiondirect.co.uk'
			and document_type = 'ORDER'
			and document_date between '2015-02-01' and '2016-02-01') t) t
where customer_order_seq_no = 1
	and next_cust_seq_ord_num is null
order by customer_id, document_date, customer_order_seq_no

select period, num_tot_month, 
	num_tot_month_ord_seq, num_tot_month_ord_seq * 100/convert(decimal(10,2), num_tot_month) perc_tot_month_ord_seq, 
	count(*) num_tot_no_rep, count(*) * 100/convert(decimal(10,2), num_tot_month_ord_seq) perc_tot_no_rep
from
	(select order_id, order_no, document_date, period, 
			customer_id, customer_order_seq_no,
			next_cust_seq_ord_num, 
			num_tot_month, num_tot_month_ord_seq
	from
		(select order_id, order_no, document_date, period, 
			customer_id, customer_order_seq_no,
			lead(customer_order_seq_no) over (partition by customer_id order by document_date, order_id) next_cust_seq_ord_num,
			count(*) over (partition by period) num_tot_month,
			count(*) over (partition by period, customer_order_seq_no) num_tot_month_ord_seq
		from
			(select order_id, order_no, document_date, 
				cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
				customer_id, customer_order_seq_no 
			from DW_GetLenses.dbo.order_headers
			where store_name in ('visiondirect.co.uk', 'getlenses.co.uk') -- visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl
				and document_type = 'ORDER'
				and document_date > '2012-01-01') t) t
	where customer_order_seq_no = 1
		and next_cust_seq_ord_num is null) t
group by period, num_tot_month, num_tot_month_ord_seq
order by period, num_tot_month, num_tot_month_ord_seq

-----------------------------------------------------------------------------------------------------


select top 1000 order_id, order_no, document_date, period, 
	customer_id, customer_order_seq_no,
	lead(customer_order_seq_no) over (partition by customer_id order by document_date, order_id) next_cust_seq_ord_num,
	count(*) over (partition by period) num_tot_month,
	count(*) over (partition by period, customer_order_seq_no) num_tot_month_ord_seq
from
	(select order_id, order_no, document_date, 
		cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
		customer_id, customer_order_seq_no 
	from DW_GetLenses.dbo.order_headers
	where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl') 
		and document_type = 'ORDER'
		and document_date > '2012-01-01'
	union
	select order_id, order_no, document_date, 
		cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
		customer_id, customer_order_seq_no 
	from DW_Proforma.dbo.order_headers
	where store_name in ('visiondirect.co.uk') 
		and document_type = 'ORDER'
		and document_date > '2012-01-01') t
order by customer_id, document_date, customer_order_seq_no

select period, num_tot_month, 
	num_tot_month_ord_seq, num_tot_month_ord_seq * 100/convert(decimal(10,2), num_tot_month) perc_tot_month_ord_seq, 
	count(*) num_tot_no_rep, count(*) * 100/convert(decimal(10,2), num_tot_month_ord_seq) perc_tot_no_rep
from
	(select order_id, order_no, document_date, period, 
			customer_id, customer_order_seq_no,
			next_cust_seq_ord_num, 
			num_tot_month, num_tot_month_ord_seq
	from
		(select order_id, order_no, document_date, period, 
			customer_id, customer_order_seq_no,
			lead(customer_order_seq_no) over (partition by customer_id order by document_date, order_id) next_cust_seq_ord_num,
			count(*) over (partition by period) num_tot_month,
			count(*) over (partition by period, customer_order_seq_no) num_tot_month_ord_seq
		from
			(select order_id, order_no, document_date, 
				cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
				customer_id, customer_order_seq_no 
			from DW_GetLenses.dbo.order_headers
			where store_name in ('visiondirect.co.uk', 'getlenses.co.uk', 'gbp.getlenses.nl') 
				and document_type = 'ORDER'
				and document_date > '2012-01-01'
			union
			select order_id, order_no, document_date, 
				cast(cast(year(document_date) as varchar) + '-' + cast(month(document_date) as varchar) + '-01' as date) period,
				customer_id, customer_order_seq_no 
			from DW_Proforma.dbo.order_headers
			where store_name in ('visiondirect.co.uk') 
				and document_type = 'ORDER'
				and document_date > '2012-01-01') t) t
	where customer_order_seq_no = 1
		and next_cust_seq_ord_num is null) t
group by period, num_tot_month, num_tot_month_ord_seq
order by period, num_tot_month, num_tot_month_ord_seq