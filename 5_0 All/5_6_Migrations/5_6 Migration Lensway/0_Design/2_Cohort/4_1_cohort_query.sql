
select top 1000 entity_id, increment_id, 
	state, status, store_name, customer_id, 
	created_at_dt, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_grand_total, 
	rank() over (partition by customer_id order by created_at_dt, increment_id)
from DW_GetLenses_jbs.dbo.lensway_cohort
where created_at_dt is not null and
	customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
order by customer_id, created_at_dt

-- 

select top 1000 customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
	rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
from DW_GetLenses_jbs.dbo.lensway_cohort
where created_at_dt is not null and
	customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
order by customer_id, created_at_dt

-- 

select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
	lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
	lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
	lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
from
	(select top 1000 customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
		rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
	from DW_GetLenses_jbs.dbo.lensway_cohort
	where created_at_dt is not null and
		customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)) t
order by customer_id, cust_seq_ord_num

-- 

select customer_id, 
	created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
	base_grand_total, cust_seq_ord_num, 
	next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
	cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
	next_base_grand_total, next_cust_seq_ord_num
from
	(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
		lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
		lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
		lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
	from
		(select top 1000 customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
			rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
		from DW_GetLenses_jbs.dbo.lensway_cohort
		where created_at_dt is not null and
			customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)) t) t
order by customer_id, cust_seq_ord_num

-- 
select customer_id, cust_seq_ord_num, 
	period, period_next, datediff(month, period, period_next) as month_diff, 
	base_grand_total, next_base_grand_total
from
	(select customer_id, 
		created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
		base_grand_total, cust_seq_ord_num, 
		next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
		cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
		next_base_grand_total, next_cust_seq_ord_num
	from
		(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
			lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
			lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
			lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
		from
			(select top 1000 customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
				rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
			from DW_GetLenses_jbs.dbo.lensway_cohort
			where created_at_dt is not null and
				customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)) t) t) t
order by customer_id, cust_seq_ord_num

--------------------------------------------------------------------------------------------------------------- 

select period, cust_seq_ord_num,
	sum(base_grand_total) base_grand_total_s, count(num_cust) num_orders, count(distinct customer_id) num_customers
from
	(select customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 1 num_cust
	from
		(select customer_id, 
			created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
					rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lensway_cohort
				where created_at_dt is not null 
					-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
					) t) t) t) t
group by period, cust_seq_ord_num
order by period, cust_seq_ord_num

select period, cust_seq_ord_num, month_diff, 
	sum(base_grand_total) base_grand_total_s, sum(next_base_grand_total) next_base_grand_total_s, 
	count(num_cust) num_orders, count(distinct customer_id) num_customers
from
	(select customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 1 num_cust
	from
		(select customer_id, 
			created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
					rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lensway_cohort
				where created_at_dt is not null 
					-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
					) t) t) t) t
group by period, cust_seq_ord_num, month_diff
order by cust_seq_ord_num, period, month_diff

