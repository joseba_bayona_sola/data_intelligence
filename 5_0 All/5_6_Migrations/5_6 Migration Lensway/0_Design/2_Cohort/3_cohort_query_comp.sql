
select count(distinct customer_id)
from DW_GetLenses_jbs.dbo.lensway_cohort
where created_at_dt is not null

select top 1000 count(*) over () num_tot,
	customer_id, cust_seq_ord_num, 
	period, period_next, datediff(month, period, period_next) as month_diff, 
	base_grand_total, next_base_grand_total, 
	case when (period_next is null) then 0 else 1 end as rep, 
	max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
from
	(select customer_id, 
		created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
		base_grand_total, cust_seq_ord_num, 
		next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
		cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
		next_base_grand_total, next_cust_seq_ord_num
	from
		(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
			lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
			lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
			lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
		from
			(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
				rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
			from DW_GetLenses_jbs.dbo.lensway_cohort
			where created_at_dt is not null -- and
				--customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
				) t) t) t
order by customer_id, cust_seq_ord_num

--------------------------

select rep, count(*) num
from
	(select count(*) over () num_tot,
		customer_id, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		base_grand_total, next_base_grand_total, 
		case when (period_next is null) then 0 else 1 end as rep, 
		max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
	from
		(select customer_id, 
			created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			base_grand_total, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_base_grand_total, next_cust_seq_ord_num
		from
			(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
				lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
			from
				(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
					rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lensway_cohort
				where created_at_dt is not null -- and
					--customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
					) t) t) t) t
group by rep
order by rep

-------------------------------

select max_seq_ord_num, count(*) num, count(*) *100/convert(decimal(10,2), 84540) perc_num_tot
from
	(select distinct 
		customer_id, max_seq_ord_num
	from
		(select count(*) over () num_tot,
			customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, next_base_grand_total, 
			case when (period_next is null) then 0 else 1 end as rep, 
			max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
		from
			(select customer_id, 
				created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
				cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
						rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lensway_cohort
					where created_at_dt is not null -- and
						--customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
						) t) t) t) t) t
group by max_seq_ord_num
order by max_seq_ord_num

-------------------------------

select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
from
	(select distinct 
		customer_id, max_seq_ord_num
	from
		(select count(*) over () num_tot,
			customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, next_base_grand_total, 
			case when (period_next is null) then 0 else 1 end as rep, 
			max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
		from
			(select customer_id, 
				created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
				cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
						rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lensway_cohort
					where created_at_dt is not null -- and
						--customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
						) t) t) t) t) t
group by max_seq_ord_num
order by max_seq_ord_num

select sum(num_orders_no_rep), sum(num_orders_rep)
from
	(select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
	from
		(select distinct 
			customer_id, max_seq_ord_num
		from
			(select count(*) over () num_tot,
				customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 
				case when (period_next is null) then 0 else 1 end as rep, 
				max(cust_seq_ord_num) over (partition by customer_id) max_seq_ord_num
			from
				(select customer_id, 
					created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
							rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lensway_cohort
						where created_at_dt is not null -- and
							--customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
							) t) t) t) t) t
	group by max_seq_ord_num) t
