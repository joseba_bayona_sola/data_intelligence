
select customer_id, item_product_family, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, num_orders
from
	(select customer_id, item_product_family, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
		rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num, 
		count(*) over (partition by customer_id) num_orders
	from DW_GetLenses_jbs.dbo.lensway_cohort_v2
	where created_at_dt is not null 
		-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
		) t
where cust_seq_ord_num = num_orders

select yyyy, mm, count(*), sum(count(*)) over () num_total
from 
	(select customer_id, item_product_family, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, num_orders
	from
		(select customer_id, item_product_family, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
			rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num, 
			count(*) over (partition by customer_id) num_orders
		from DW_GetLenses_jbs.dbo.lensway_cohort_v2
		where created_at_dt is not null 
			-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
			) t
	where cust_seq_ord_num = num_orders) t
-- where (yyyy < 2015) or (yyyy = 2015 and mm < 10)
where not((yyyy < 2015) or (yyyy = 2015 and mm < 10))
group by yyyy, mm
order by yyyy, mm

