
select top 1000 entity_id, increment_id, 
	state, status, item_product_family, store_name, customer_id, 
	created_at_dt, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total
from DW_GetLenses_jbs.dbo.lensway_cohort_v2

-- 

select top 1000 entity_id, count(*)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by entity_id
order by count(*) desc

select top 1000 increment_id, count(*)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by increment_id
order by count(*) desc

-- 

select top 1000 entity_id, increment_id, 
	state, status, item_product_family, store_name, customer_id, 
	created_at_dt, created_at, 
	base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
where created_at_dt is null

-- 
	select top 1000 entity_id, increment_id, 
		state, status, store_name, customer_id, 
		created_at_dt, created_at, 
		base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total,
		base_grand_total - base_subtotal + base_discount_amount - base_shipping_amount - handling_charge bs_calc
	from DW_GetLenses_jbs.dbo.lensway_cohort_v2

	select bs_calc, count(*)
	from
		(select entity_id, increment_id, 
			state, status, item_product_family, store_name, customer_id, 
			created_at_dt, created_at, 
			base_subtotal, base_shipping_amount, base_discount_amount, handling_charge, base_grand_total,
			base_grand_total - base_subtotal + base_discount_amount - base_shipping_amount - handling_charge bs_calc
		from DW_GetLenses_jbs.dbo.lensway_cohort_v2) t
	group by bs_calc
	order by bs_calc

-- 

select count(*), min(created_at), max(created_at)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2

select count(distinct customer_id)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2

select year(created_at) yyyy, count(*), min(created_at), max(created_at)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by year(created_at)
order by year(created_at)

select year(created_at) yyyy, month(created_at) mm, count(*), min(created_at), max(created_at)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by year(created_at), month(created_at)
order by year(created_at), month(created_at)

-- 

select top 1000 count(*) over () num_tot, customer_id, count(*), min(created_at), max(created_at)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by customer_id
order by count(*) desc

select top 1000 customer_id, created_at_dt, count(*), min(created_at), max(created_at)
from DW_GetLenses_jbs.dbo.lensway_cohort_v2
group by customer_id, created_at_dt
having count(*) > 1
order by count(*) desc
