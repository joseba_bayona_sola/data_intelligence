
		select *
		from
			(select customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 1 num_cust
			from
				(select customer_id, 
					created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
							rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lensway_cohort
						where created_at_dt is not null 
							-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
							) t) t) t) t
		where period = '2014-12-01' and month_diff = 0
		order by cust_seq_ord_num, month_diff, customer_id

select t.customer_id, t.cust_seq_ord_num, t.base_grand_total, t.next_base_grand_total, 
	c.entity_id, c.increment_id, c.state, c.status, c.created_at_dt, c.base_grand_total
from	
		(select customer_id, cust_seq_ord_num, base_grand_total, next_base_grand_total
		from
			(select customer_id, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				base_grand_total, next_base_grand_total, 1 num_cust
			from
				(select customer_id, 
					created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					base_grand_total, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_base_grand_total, next_cust_seq_ord_num
				from
					(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
						lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
					from
						(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
							rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lensway_cohort
						where created_at_dt is not null 
							-- and customer_id in (6367576085, 9975726094, 3689414854, 10439655436, 4216553524)
							) t) t) t) t
		where period = '2014-12-01' and month_diff = 0) t
	inner join
		DW_GetLenses_jbs.dbo.lensway_cohort c on t.customer_id = c.customer_id
order by t.cust_seq_ord_num, t.customer_id, c.created_at_dt

