
select *
from openquery(MYSQL_TEST, 
	'select entity_id, increment_id, state, status, 
		store_id, store_name, customer_id, 
		created_at, STR_TO_DATE(created_at, ''%b %d %Y %h:%i%p'') created_at_d,
		base_subtotal, base_shipping_amount, base_discount_amount, base_tax_amount, base_grand_total, base_customer_balance_amount
	from lensway.lensway_order
	where customer_id = 6979452937')

select top 1000 entity_id, cast(entity_id as bigint) ed,
	increment_id, state, status, 
	store_id, store_name, customer_id, 
	created_at, created_at_d, convert(date, created_at_d),
	cast(base_grand_total as decimal(12,2)) - cast(base_subtotal as decimal(12,2)) + cast(base_discount_amount as decimal(12,2)) - cast(base_shipping_amount as decimal(12,2)) bs, 
	base_subtotal, base_shipping_amount, base_discount_amount, base_tax_amount, base_grand_total, base_customer_balance_amount
	-- base_grand_total - base_subtotal + base_discount_amount - base_shipping_amount
from openquery(MYSQL_TEST, 
	'select entity_id, increment_id, state, status, 
		store_id, store_name, customer_id, 
		created_at, STR_TO_DATE(created_at, ''%b %d %Y %h:%i%p'') created_at_d,
		base_subtotal, base_shipping_amount, base_discount_amount, base_tax_amount, base_grand_total, base_customer_balance_amount
	from lensway.lensway_order limit 5000')
order by bs desc 


select *
from openquery(MAGENTO, 'select * from magento01.core_store')
where name like 'vision%'