	select period, cust_seq_ord_num customer_order_seq_no, month_diff, 
		count(distinct customer_id) customer_count,
		sum(base_grand_total) revenue_in_VAT, 

		count(distinct customer_id) next_order_customer_count,
		sum(next_base_grand_total) next_revenue_in_VAT 
	
	from
		(select customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, next_base_grand_total, 1 num_cust
		from
			(select customer_id, 
				created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
				cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
						rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lensway_cohort
					where created_at_dt is not null) t) t) t) t
	where period between '2015-02-01' and '2016-11-30'
		and cust_seq_ord_num = 1
	group by period, cust_seq_ord_num, month_diff
	order by period, cust_seq_ord_num, month_diff 




	select period, cust_seq_ord_num customer_order_seq_no, month_diff, 
		count(distinct customer_id) customer_count,
		sum(base_grand_total) revenue_in_VAT, 

		count(distinct customer_id) next_order_customer_count, sum(num_cust) next_order_customer_count, 
		sum(next_base_grand_total) next_revenue_in_VAT 
	
	from
		(select customer_id, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			base_grand_total, 
			case when (period_next > '2016-11-30') then NULL else next_base_grand_total end next_base_grand_total, 
			case when (period_next > '2016-11-30') then NULL else 1 end num_cust
		from
			(select customer_id, 
				created_at, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				base_grand_total, cust_seq_ord_num, 
				next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
				cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
				next_base_grand_total, next_cust_seq_ord_num
			from
				(select customer_id, created_at, yyyy, mm, base_grand_total, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by customer_id order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(created_at) over (partition by customer_id order by cust_seq_ord_num) next_created_at, 
					lead(base_grand_total) over (partition by customer_id order by cust_seq_ord_num) next_base_grand_total
				from
					(select customer_id, created_at, year(created_at) yyyy, month(created_at) mm, base_grand_total, 
						rank() over (partition by customer_id order by created_at_dt, increment_id) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lensway_cohort
					where created_at_dt is not null) t) t
			where created_at between '2015-02-01' and '2016-11-30' 
				-- and ((next_created_at between '2015-02-01' and '2016-11-30') or next_created_at is null)
				) t) t
	where cust_seq_ord_num = 1
	group by period, cust_seq_ord_num, month_diff
	order by period, cust_seq_ord_num, month_diff 