
drop table DW_GetLenses_jbs.dbo.lensbest_cohort
go 

create table DW_GetLenses_jbs.dbo.lensbest_cohort(
	order_id				bigint, 
	customer_id				bigint, 
	order_date				date, 
	item_product_family		varchar(50),
	base_grand_total		decimal(12, 4)); 

	insert into DW_GetLenses_jbs.dbo.lensbest_cohort(order_id, customer_id, order_date, item_product_family, base_grand_total)

		select order_no order_id, customer_no customer_id, order_date, null item_product_family, 
			sum(amount) base_grand_total
		from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
		where product_no is not null
			and b2c is not null
			and (group_2 in ('Contacts', 'Solutions', 'Others') or group_2 is null)
		group by order_no, customer_no , order_date
