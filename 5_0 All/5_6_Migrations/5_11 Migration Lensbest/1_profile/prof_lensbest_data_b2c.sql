
	-- Example of OH - OL (Individual Records)
	select top 1000 order_no, order_date, posting_date, 
		customer_no, name, customers_group_code, 
		item_category_code, product_no, description,
		quantity, amount, amount_including_vat, -- is_zero,
		unit_cost
	from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
	where product_no is not null
		and b2c is not null
		and year(order_date) in (2016, 2017)
		-- and order_no = 4366917
		-- and is_zero is null
	order by order_date desc, order_no, product_no

	-- Num of Records per YEAR
	select top 1000 year(order_date), count(*), count(distinct order_no), count(product_no)
	from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
	group by year(order_date)
	order by year(order_date)

	-- Num of Records per YEAR (Only OH)
	select top 1000 year(order_date), count(*), count(b2c), 
		count(b2c) * 100 / count(*)
	from
		(select distinct order_no, order_date, customer_no, b2c
		from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v) t
	group by year(order_date)
	order by year(order_date)

	-- Num of Records per YEAR (Only OL)
	select top 1000 year(order_date), count(*), count(b2c), 
		count(b2c) * 100 / count(*), 
		count(is_zero), count(is_zero) * 100 / count(*)
	from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
	where product_no is not null
	group by year(order_date)
	order by year(order_date)

	-----------------------

	select top 1000 order_no, count(*), count(*) over () num_tot_orders
	from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
	where product_no is not null
		and b2c is null
	group by order_no

	-- Number of Lines per OH
	select num_lines, count(*), count(*) * 100 / convert(decimal(12, 4), num_tot_orders)
	from
		(select order_no, count(*) num_lines, count(*) over () num_tot_orders
		from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
		where product_no is not null
			and b2c is not null
		group by order_no) t
	group by num_lines, num_tot_orders
	order by num_lines

	-----------------------

