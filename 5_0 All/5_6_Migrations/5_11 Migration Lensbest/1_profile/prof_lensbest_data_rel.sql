
-- Customers with Most Orders

select oh.customer_no, c.name, 
	c.multi_flag, c.country_code, c.customers_group_code,
	oh.num_orders
from
		(select top 1000 customer_no, count(*) num_orders, rank() over (order by count(*) desc) num_r_customer -- asc, desc
		from DW_GetLenses_jbs.dbo.lensbest_oh
		group by customer_no) oh
	inner join
		DW_GetLenses_jbs.dbo.lensbest_customer c on oh.customer_no = c.customer_no
where num_r_customer < 100	
order by num_orders desc


-- Products with Most Lines

select ol.product_no, p.description, p.description_2, 
	p.product_group_code, p.item_category_code,
	ol.num_lines
from
		(select top 1000 product_no, count(*) num_lines, rank() over (order by count(*) asc) num_r_product -- asc, desc
		from DW_GetLenses_jbs.dbo.lensbest_ol
		where product_no <> ''
		group by product_no) ol
	inner join
		DW_GetLenses_jbs.dbo.lensbest_product p on ol.product_no = p.product_no
where num_r_product < 100	
order by num_lines desc

-- Orders with Most Lines

select ol.order_no, oh.order_date, oh.posting_date, 
	oh.customer_no, oh.name, oh.customers_group_code,
	oh.project_flag, oh.prom_med_no, 
	oh.item_category_code, oh.item_used_as, oh.product_no, oh.description,
	oh.quantity, oh.unit_cost, oh.amount, oh.amount_including_vat,
	ol.num_lines
from
		(select top 1000 order_no, count(*) num_lines, rank() over (order by count(*) asc) num_r_order -- asc, desc
		from DW_GetLenses_jbs.dbo.lensbest_ol
		group by order_no) ol
	inner join
		DW_GetLenses_jbs.dbo.lensbest_oh_ol_v oh on ol.order_no = oh.order_no
where num_r_order < 10	
order by num_lines desc, order_no