
-- CUSTOMERS
select top 1000 customer_no, name, 
	multi_flag, country_code, customers_group_code
from DW_GetLenses_jbs.dbo.lensbest_customer

	-- 1.906.725 // 1.847.543
	select count(*)
	from DW_GetLenses_jbs.dbo.lensbest_customer

	select country_code, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_customer
	group by country_code 
	order by count(*) desc, country_code

	select customers_group_code, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_customer
	group by customers_group_code 
	order by -- count(*) desc, 
		customers_group_code

-- PRODUCTS
select item_category_code, group_1, group_2, group_3
from DW_GetLenses_jbs.dbo.lensbest_item_category
order by group_2, group_1, item_category_code

select top 1000 *
from DW_GetLenses_jbs.dbo.lensbest_product
where description like '%acuvue%'

select product_no, description, description_2, 
	product_group_code, item_category_code
from DW_GetLenses_jbs.dbo.lensbest_product
where item_category_code = 'LF' -- LF - SB
order by item_category_code, description

	select count(*)
	from DW_GetLenses_jbs.dbo.lensbest_product

	select product_group_code, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_product
	group by product_group_code
	order by count(*)desc, product_group_code

	select item_category_code, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_product
	group by item_category_code
	order by item_category_code



select ic.group_2, ic.group_1, ic.item_category_code, ic.group_3,
	p.product_no, p.description, p.description_2, p.product_group_code
from 
		DW_GetLenses_jbs.dbo.lensbest_product p
	left join
		DW_GetLenses_jbs.dbo.lensbest_item_category ic on p.item_category_code = ic.item_category_code
order by ic.group_2, ic.group_1, ic.item_category_code, p.description

	select ic.group_2, count(*)
	from 
			DW_GetLenses_jbs.dbo.lensbest_product p
		left join
			DW_GetLenses_jbs.dbo.lensbest_item_category ic on p.item_category_code = ic.item_category_code
	group by ic.group_2
	order by ic.group_2


-- OH
select top 1000 order_no, order_date, posting_date, 
	customer_no, 
	project_flag, prom_med_no
from DW_GetLenses_jbs.dbo.lensbest_oh

	-- OH: 4811421 // CUST: 1279886 (Avg 3 OL)
	select count(*), count(distinct customer_no), count(*) / count(distinct customer_no)
	from DW_GetLenses_jbs.dbo.lensbest_oh

	select top 1000 customer_no, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_oh
	group by customer_no
	order by count(*) desc

	select top 1000 project_flag, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_oh
	group by project_flag
	order by count(*) desc

	select top 1000 prom_med_no, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_oh
	group by prom_med_no
	order by count(*) desc


	select top 1000 year(order_date), count(*)
	from DW_GetLenses_jbs.dbo.lensbest_oh
	group by year(order_date)
	order by year(order_date)

--- OL
select top 1000 order_no, customer_no, shipment_date, 
	item_category_code, item_used_as, product_no, quantity, unit_cost, amount, amount_including_vat
	, convert(decimal(12, 4), replace(unit_cost, ',', '.')) * convert(decimal(12, 4), replace(amount, ',', '.')) amount_including_vat_calc
from DW_GetLenses_jbs.dbo.lensbest_ol
where order_no = 2903354

	-- OL: 3.465.689 // OH: 572.309 (Avg 6 OL)
	select count(*), count(distinct order_no), count(*) / count(distinct order_no)
	from DW_GetLenses_jbs.dbo.lensbest_ol

	select top 1000 order_no, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_ol
	group by order_no
	order by count(*) desc

	select top 1000 product_no, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_ol
	group by product_no
	order by product_no
	-- order by count(*) desc

	select ol.product_no, p.description, p.product_group_code, p.item_category_code,
		ol.num_ol
	from
			(select product_no, count(*) num_ol
			from DW_GetLenses_jbs.dbo.lensbest_ol
			group by product_no) ol
		left join
			DW_GetLenses_jbs.dbo.lensbest_product p on ol.product_no = p.product_no
	-- where p.product_no is null
	-- order by ol.product_no
	order by ol.num_ol desc


	select top 1000 item_category_code, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_ol
	group by item_category_code
	order by count(*) desc

	select top 1000 item_used_as, count(*)
	from DW_GetLenses_jbs.dbo.lensbest_ol
	group by item_used_as
	order by count(*) desc

---- VIEW OH-OL
drop view dbo.lensbest_oh_ol_v
go 

create view dbo.lensbest_oh_ol_v
as
select oh.order_no, oh.order_date, oh.posting_date, 
	oh.customer_no, c.name, c.customers_group_code, case when (c.customers_group_code like 'VH%') then 1 else null end b2c, c.country_code,
	oh.project_flag, oh.prom_med_no, 
	pic.group_2, pic.group_1, pic.group_3, pic.item_category_code, ol.product_no, pic.description, ol.item_used_as, 
	convert(decimal(12, 0), replace(ol.quantity, ',', '.')) quantity,
	convert(decimal(12, 4), replace(ol.unit_cost, ',', '.')) unit_cost, 
	convert(decimal(12, 4), replace(ol.amount, ',', '.')) amount, 
	convert(decimal(12, 4), replace(ol.amount_including_vat, ',', '.')) amount_including_vat, 
	case when (ol.amount_including_vat = ',00000000000000000000') then 0 else null end is_zero
from 
		DW_GetLenses_jbs.dbo.lensbest_oh oh
	left join
		DW_GetLenses_jbs.dbo.lensbest_ol ol on oh.order_no = ol.order_no
	inner join
		DW_GetLenses_jbs.dbo.lensbest_customer c on oh.customer_no = c.customer_no
	left join
		(select ic.group_2, ic.group_1, ic.item_category_code, ic.group_3,
			p.product_no, p.description, p.description_2, p.product_group_code
		from 
				DW_GetLenses_jbs.dbo.lensbest_product p
			left join
				DW_GetLenses_jbs.dbo.lensbest_item_category ic on p.item_category_code = ic.item_category_code) pic on ol.product_no = pic.product_no


-----------------------------

select top 1000 customers_group_code, year(order_date), sum(amount) -- amount_including_vat
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v 
where product_no is not null
	and b2c is not null
group by customers_group_code, year(order_date)
order by customers_group_code, year(order_date)

select customers_group_code, country_code, sum(amount) -- amount_including_vat
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v 
where product_no is not null
	and b2c is not null
group by customers_group_code, country_code
order by customers_group_code, country_code


select top 1000 *
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
	and group_2 = 'Others'

select top 1000 group_2, count(*), sum(amount)
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
group by group_2
order by sum(amount) desc

select top 1000 year(order_date), group_2, group_3, count(*), sum(amount)
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
group by year(order_date), group_3, group_2
order by year(order_date), group_2, group_3

select top 1000 group_2, group_1, item_category_code, product_no, description, count(*), sum(amount)
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
	-- and group_2 = 'Others' -- Contacts - Solutions - Rx - Sun - Readers - Others - NULL
	and group_2 is null
group by group_2, group_1, item_category_code, product_no, description
order by group_2, group_1, item_category_code, product_no, description



select top 1000 product_no, description, count(*), sum(amount)
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
	and group_2 = 'Others' and group_3 = '3rd Party'
group by product_no, description
order by description, product_no



-- Amount Negative
select top 1000 group_2, group_1, product_no, count(*), sum(amount)
from DW_GetLenses_jbs.dbo.lensbest_oh_ol_v
where amount < 0
	and product_no is not null
	and b2c is not null
	and year(order_date) in (2014, 2015, 2016, 2017)
group by group_2, group_1, product_no
order by group_2, group_1, product_no
