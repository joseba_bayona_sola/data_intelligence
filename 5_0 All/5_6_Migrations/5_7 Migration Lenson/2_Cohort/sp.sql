
use DW_GetLenses
go

create procedure Finance.spCustomerRetention_Lenson(
	@from_date as date, 
	@to_date as date
	) 
as

begin

	select period, cust_seq_ord_num customer_order_seq_no, month_diff, 
		count(distinct idCustomer) customer_count,
		sum(base_grand_total) revenue_in_VAT, 

		--count(distinct customer_id) next_order_customer_count,
		sum(num_cust) next_order_customer_count, 
		sum(next_base_grand_total) next_revenue_in_VAT 
	from
		(select idCustomer, cust_seq_ord_num, 
			period, period_next, datediff(month, period, period_next) as month_diff, 
			order_value base_grand_total, -- next_order_value, 1 num_cust
			case when (period_next > @to_date) then NULL else next_order_value end next_base_grand_total, 
			case when (period_next > @to_date) then NULL else 1 end num_cust
		from
			(select idCustomer, 
				order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
				order_value, cust_seq_ord_num, 
				next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
				cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
				next_order_value, next_cust_seq_ord_num
			from
				(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
					lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
				from
					(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
						rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lenson_cohort_orders) t) t
					where order_date between @from_date and @to_date 
					) t) t
	group by period, cust_seq_ord_num, month_diff

end