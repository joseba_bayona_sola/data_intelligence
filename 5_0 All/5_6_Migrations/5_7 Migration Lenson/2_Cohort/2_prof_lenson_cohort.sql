
select top 1000 idOrder, idCustomer, email, order_date, order_value
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
where idCustomer = 45925
-- 

select top 1000 idOrder, count(*)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by idOrder
order by count(*) desc

select idOrder, idCustomer, email, order_date, order_value
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
where order_value = 0
order by order_value 

-- 

select count(*), min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders

select year(order_date) yyyy, count(*), min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by year(order_date)
order by year(order_date)

select year(order_date) yyyy, month(order_date) mm, count(*), min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by year(order_date), month(order_date)
order by year(order_date), month(order_date)

-- 

select top 1000 count(*) over () num_tot, idCustomer, count(*), min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by idCustomer
order by count(*) desc

select top 1000 idCustomer, order_date, count(*), min(order_date), max(order_date)
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by idCustomer, order_date
having count(*) > 1
order by count(*) desc
