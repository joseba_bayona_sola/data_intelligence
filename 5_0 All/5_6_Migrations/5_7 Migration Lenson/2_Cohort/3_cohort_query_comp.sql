
-- Prepare the Dataset: Take orders and rank, sequence and time, value difference between them
select top 1000 count(*) over () num_tot,
	idCustomer, cust_seq_ord_num, 
	period, period_next, datediff(month, period, period_next) as month_diff, 
	order_value, next_order_value, 
	case when (period_next is null) then 0 else 1 end as rep, 
	max(cust_seq_ord_num) over (partition by idCustomer) max_seq_ord_num	
from
	(select idCustomer, 
		order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
		order_value, cust_seq_ord_num, 
		next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
		cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
		next_order_value, next_cust_seq_ord_num
	from
		(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
			lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
			lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
			lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
		from
			(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
				rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
			from DW_GetLenses_jbs.dbo.lenson_cohort_orders
			where idCustomer between 1 and 20
			) t) t) t
order by idCustomer, cust_seq_ord_num

--------------------------
-- Distinguish between orders with Following order or NOT (Only 1 Order + Regular Last Order)
select rep, count(*) num
from
	(select count(*) over () num_tot,
		idCustomer, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		order_value, next_order_value, 
		case when (period_next is null) then 0 else 1 end as rep, 
		max(cust_seq_ord_num) over (partition by idCustomer) max_seq_ord_num	
	from
		(select idCustomer, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			order_value, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_order_value, next_cust_seq_ord_num
		from
			(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
				lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
			from
				(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
					rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lenson_cohort_orders
				-- where idCustomer between 1 and 20
				) t) t) t) t
group by rep
order by rep
-------------------------------

-- Customer Base depending on Repetitions
select max_seq_ord_num, count(*) num, count(*) *100/convert(decimal(10,2), num_tot) perc_num_tot 
from
	(select count(*) over () num_tot, 
		idCustomer, max_seq_ord_num
	from
		(select distinct idCustomer, max_seq_ord_num
		from
			(select count(*) over () num_tot,
				idCustomer, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				order_value, next_order_value, 
				case when (period_next is null) then 0 else 1 end as rep, 
				max(cust_seq_ord_num) over (partition by idCustomer) max_seq_ord_num	
			from
				(select idCustomer, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					order_value, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_order_value, next_cust_seq_ord_num
				from
					(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
						lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
					from
						(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
							rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lenson_cohort_orders
						-- where idCustomer between 1 and 20
						) t) t) t) t) t) t
group by max_seq_ord_num, num_tot
order by max_seq_ord_num

-------------------------------

select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
from
	(select count(*) over () num_tot, 
		idCustomer, max_seq_ord_num
	from
		(select distinct idCustomer, max_seq_ord_num
		from
			(select count(*) over () num_tot,
				idCustomer, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				order_value, next_order_value, 
				case when (period_next is null) then 0 else 1 end as rep, 
				max(cust_seq_ord_num) over (partition by idCustomer) max_seq_ord_num	
			from
				(select idCustomer, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					order_value, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_order_value, next_cust_seq_ord_num
				from
					(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
						lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
					from
						(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
							rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lenson_cohort_orders
						-- where idCustomer between 1 and 20
						) t) t) t) t) t) t
group by max_seq_ord_num
order by max_seq_ord_num

select sum(num_orders_no_rep), sum(num_orders_rep)
from
	(select max_seq_ord_num, count(*) num_orders_no_rep, (max_seq_ord_num -1) * count(*) num_orders_rep
	from
		(select count(*) over () num_tot, 
			idCustomer, max_seq_ord_num
		from
			(select distinct idCustomer, max_seq_ord_num
			from
				(select count(*) over () num_tot,
					idCustomer, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					order_value, next_order_value, 
					case when (period_next is null) then 0 else 1 end as rep, 
					max(cust_seq_ord_num) over (partition by idCustomer) max_seq_ord_num	
				from
					(select idCustomer, 
						order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						order_value, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_order_value, next_cust_seq_ord_num
					from
						(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
							lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
						from
							(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
								rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lenson_cohort_orders
							-- where idCustomer between 1 and 20
							) t) t) t) t) t) t
	group by max_seq_ord_num) t