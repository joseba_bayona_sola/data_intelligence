
-- INFO PER CUST SEQ - PERIOD - MONTH DIFF ABOUT
	-- Numbers of Sales on PERIOD: tot_m_base_grand_total_s, tot_m_num_orders, 
	-- Numbers of Sales on PERIOD for cust seq and %: tot_base_grand_total_s, tot_num_orders (for cust seq)
select t2.cust_seq_ord_num, t2.period, t2.month_diff, 
	sum(t2.base_grand_total_s) over (partition by t2.period) tot_m_base_grand_total_s,
	sum(t2.num_orders) over (partition by t2.period) tot_m_num_orders,
	t1.base_grand_total_s tot_base_grand_total_s, t1.num_orders tot_num_orders, 
	t2.base_grand_total_s, t2.base_grand_total_s*100/convert(decimal(10,2), case when (t1.base_grand_total_s = 0) then 1 else t1.base_grand_total_s end) perc_base_grand_total,
	t2.next_base_grand_total_s, t2.next_base_grand_total_s*100/convert(decimal(10,2), case when (t1.base_grand_total_s = 0) then 1 else t1.base_grand_total_s end) perc_next_base_grand_total,
	t2.num_orders, t2.num_orders*100/convert(decimal(10,2), t1.num_orders) perc_num_orders
from
		(select period, cust_seq_ord_num,
			sum(order_value) base_grand_total_s, count(num_cust) num_orders, count(distinct idCustomer) num_customers
		from
			(select idCustomer, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				order_value, next_order_value, 1 num_cust
			from
				(select idCustomer, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					order_value, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_order_value, next_cust_seq_ord_num
				from
					(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
						lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
					from
						(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
							rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lenson_cohort_orders
						) t) t) t) t
		group by period, cust_seq_ord_num) t1
	inner join
		(select period, cust_seq_ord_num, month_diff, 
			sum(order_value) base_grand_total_s, sum(next_order_value) next_base_grand_total_s, 
			count(num_cust) num_orders, count(distinct idCustomer) num_customers
		from
			(select idCustomer, cust_seq_ord_num, 
				period, period_next, datediff(month, period, period_next) as month_diff, 
				order_value, next_order_value, 1 num_cust
			from
				(select idCustomer, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					order_value, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_order_value, next_cust_seq_ord_num
				from
					(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
						lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
					from
						(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
							rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lenson_cohort_orders
						-- where idCustomer between 1 and 20
						) t) t) t) t
		group by period, cust_seq_ord_num, month_diff) t2 on t1.period = t2.period and t1.cust_seq_ord_num = t2.cust_seq_ord_num

order by t2.cust_seq_ord_num, t2.period, t2.month_diff



-- SELECT FOR FINAL EXCEL
select cust_seq_ord_num, period, month_diff,
	tot_m_base_grand_total_s,  
	tot_m_num_orders, 
	tot_base_grand_total_s, tot_base_grand_total_s*100/convert(decimal(10,2), tot_m_base_grand_total_s) perc_tot_base_grand_total,
	tot_num_orders, tot_num_orders*100/convert(decimal(10,2), tot_m_num_orders) perc_tot_num_orders,
	num_orders, perc_num_orders
from
	(select t2.cust_seq_ord_num, t2.period, t2.month_diff, 
		sum(t2.base_grand_total_s) over (partition by t2.period) tot_m_base_grand_total_s,
		sum(t2.num_orders) over (partition by t2.period) tot_m_num_orders,
		t1.base_grand_total_s tot_base_grand_total_s, t1.num_orders tot_num_orders, 
		t2.base_grand_total_s, t2.base_grand_total_s*100/convert(decimal(10,2), case when (t1.base_grand_total_s = 0) then 1 else t1.base_grand_total_s end) perc_base_grand_total,
		t2.next_base_grand_total_s, t2.next_base_grand_total_s*100/convert(decimal(10,2), case when (t1.base_grand_total_s = 0) then 1 else t1.base_grand_total_s end) perc_next_base_grand_total,
		t2.num_orders, t2.num_orders*100/convert(decimal(10,2), t1.num_orders) perc_num_orders
	from
			(select period, cust_seq_ord_num,
				sum(order_value) base_grand_total_s, count(num_cust) num_orders, count(distinct idCustomer) num_customers
			from
				(select idCustomer, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					order_value, next_order_value, 1 num_cust
				from
					(select idCustomer, 
						order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						order_value, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_order_value, next_cust_seq_ord_num
					from
						(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
							lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
						from
							(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
								rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lenson_cohort_orders
							) t) t) t) t
			group by period, cust_seq_ord_num) t1
		inner join
			(select period, cust_seq_ord_num, month_diff, 
				sum(order_value) base_grand_total_s, sum(next_order_value) next_base_grand_total_s, 
				count(num_cust) num_orders, count(distinct idCustomer) num_customers
			from
				(select idCustomer, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					order_value, next_order_value, 1 num_cust
				from
					(select idCustomer, 
						order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						order_value, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_order_value, next_cust_seq_ord_num
					from
						(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
							lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
						from
							(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
								rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lenson_cohort_orders
							-- where idCustomer between 1 and 20
							) t) t) t) t
			group by period, cust_seq_ord_num, month_diff) t2 on t1.period = t2.period and t1.cust_seq_ord_num = t2.cust_seq_ord_num) t
where cust_seq_ord_num = 1 and month_diff is null
order by cust_seq_ord_num, period, month_diff