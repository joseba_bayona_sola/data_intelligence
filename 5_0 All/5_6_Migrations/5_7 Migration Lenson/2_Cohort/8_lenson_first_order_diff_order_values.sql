
select *
from
	(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
		lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
		lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
		lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
	from
		(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
			rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
		from DW_GetLenses_jbs.dbo.lenson_cohort_orders) t) t
where yyyy = 2014 and mm = 07 and cust_seq_ord_num = 1
	and year(next_created_at) = 2015 and month(next_created_at) = 1
order by next_created_at

select yyyy, mm, cust_seq_ord_num, year(next_created_at) yyyy_next, month(next_created_at) mm_next,
	sum(order_value), sum(next_order_value)
from
	(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
		lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
		lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
		lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
	from
		(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
			rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
		from DW_GetLenses_jbs.dbo.lenson_cohort_orders) t) t
where yyyy = '2014' and mm = '07' and cust_seq_ord_num = 2
group by yyyy, mm, cust_seq_ord_num, year(next_created_at), month(next_created_at)
order by yyyy, mm, cust_seq_ord_num, year(next_created_at), month(next_created_at)


