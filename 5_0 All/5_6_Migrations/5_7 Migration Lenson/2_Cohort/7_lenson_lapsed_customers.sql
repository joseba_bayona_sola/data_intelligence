
select idOrder, idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, num_orders
from
	(select  top 1000 idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value,
		rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num, 
		count(*) over (partition by idCustomer) num_orders
	from DW_GetLenses_jbs.dbo.lenson_cohort_orders) t
where cust_seq_ord_num = num_orders

select yyyy, mm, count(*), sum(count(*)) over () num_total
from 
	(select idOrder, idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, num_orders
	from
		(select  idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value,
			rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num, 
			count(*) over (partition by idCustomer) num_orders
		from DW_GetLenses_jbs.dbo.lenson_cohort_orders) t
	where cust_seq_ord_num = num_orders) t
-- where (yyyy < 2015) or (yyyy = 2015 and mm < 10)
where not((yyyy < 2015) or (yyyy = 2015 and mm < 10))
group by yyyy, mm
order by yyyy, mm
