
create table DW_GetLenses_jbs.dbo.lenson_cohort_customer(
	idCustomer				int, 
	customer_name			varchar(50), 
	email					varchar(255), 
	city					varchar(255), 
	status					varchar(50),
	account_created			date, 
	placed_orders			int, 
	purchase_value			decimal(12, 2), 
	latest_order_channel	varchar(50));
go 
	
create table DW_GetLenses_jbs.dbo.lenson_cohort_orders(
	idOrder					int,
	idCustomer				int, 
	email					varchar(255),
	order_date				date, 
	order_value				decimal(12, 2));
go 

	
insert into DW_GetLenses_jbs.dbo.lenson_cohort_customer(idCustomer, customer_name, email, city, 
	status, account_created, placed_orders, purchase_value, latest_order_channel)

	select distinct idCustomer, customer customer_name, email, city, 
		status, account_created, placed_orders, purchase_value, latest_order_channel
	from 
		(select 
			dense_rank() over (order by email) idCustomer,
			*
		from
			(select name customer, 
				city, email, 
				status, account_created, 
				placed_orders, purchase_value, 
				latest_order_channel,
				-- order_num, 
				order_date, order_value
			from DW_GetLenses_jbs.dbo.lenson_cohort_source
			cross apply
				(
					select [0], [1]
					union all
					select [2], [3]
					union all
					select [4], [5]
					union all
					select [6], [7]
					union all
					select [8], [9]
					union all
					select [10], [11]
					union all
					select [12], [13]
					union all
					select [14], [15]
					union all
					select [16], [17]
					union all
					select [18], [19]
					union all
					select [20], [21]
					union all
					select [22], [23]
					union all
					select [24], [25]
					union all
					select [26], [27]
					union all
					select [28], [29]
					union all
					select [30], [31]
					union all
					select [32], [33]
					union all
					select [34], [35]
					union all
					select [36], [37]
					union all
					select [38], [39]
					union all
					select [40], [41]
					union all
					select [42], [43]
					union all
					select [44], [45]
					union all
					select [46], [47]
					union all
					select [48], [49]
					union all
					select [50], [51]
					union all
					select [52], [53]
					union all
					select [54], [55]
					union all
					select [56], [57]
					union all
					select [58], [59]
					union all
					select [60], [61]
					union all
					select [62], [63]
					union all
					select [64], [65]
					union all
					select [66], [67]
					union all
					select [68], [69]
					union all
					select [70], [71]
					union all
					select [72], [73]
					union all
					select [74], [75]
					union all
					select [76], [77]
					union all
					select [78], [79]

				) c (order_date, order_value)) t	
		where len(order_date) > 1) t
	order by idCustomer

	insert into DW_GetLenses_jbs.dbo.lenson_cohort_orders(idOrder, idCustomer, email, order_date, order_value)

		select rank() over (order by c.idCustomer, t.order_date, t.order_value) idOrder, 
			c.idCustomer, c.email, t.order_date, t.order_value
		from
			(select *
			from
				(select name customer, 
					city, email, 
					status, account_created, 
					placed_orders, purchase_value, 
					latest_order_channel,
					-- order_num, 
					order_date, order_value
				from DW_GetLenses_jbs.dbo.lenson_cohort_source
				cross apply
					(
						select [0], [1]
						union all
						select [2], [3]
						union all
						select [4], [5]
						union all
						select [6], [7]
						union all
						select [8], [9]
						union all
						select [10], [11]
						union all
						select [12], [13]
						union all
						select [14], [15]
						union all
						select [16], [17]
						union all
						select [18], [19]
						union all
						select [20], [21]
						union all
						select [22], [23]
						union all
						select [24], [25]
						union all
						select [26], [27]
						union all
						select [28], [29]
						union all
						select [30], [31]
						union all
						select [32], [33]
						union all
						select [34], [35]
						union all
						select [36], [37]
						union all
						select [38], [39]
						union all
						select [40], [41]
						union all
						select [42], [43]
						union all
						select [44], [45]
						union all
						select [46], [47]
						union all
						select [48], [49]
						union all
						select [50], [51]
						union all
						select [52], [53]
						union all
						select [54], [55]
						union all
						select [56], [57]
						union all
						select [58], [59]
						union all
						select [60], [61]
						union all
						select [62], [63]
						union all
						select [64], [65]
						union all
						select [66], [67]
						union all
						select [68], [69]
						union all
						select [70], [71]
						union all
						select [72], [73]
						union all
						select [74], [75]
						union all
						select [76], [77]
						union all
						select [78], [79]

					) c (order_date, order_value)) t	
			where len(order_date) > 1) t
		inner join	
			(select idCustomer, email
			from
				(select count(*) over (partition by email) num_rep, idCustomer, email
				from DW_GetLenses_jbs.dbo.lenson_cohort_customer) t
			where num_rep = 1) c on t.email = c.email 