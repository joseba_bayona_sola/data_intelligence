
select top 1000 *
from DW_GetLenses_jbs.dbo.lenson_cohort_source
-- where email = '2f5f4964a44dc1af89024cd8f6679d96'
order by cast(placed_orders as int) desc

select top 1000 name, city, email, 
	status, account_created, 
	placed_orders, purchase_value, 
	latest_order_channel, 
	[0], [1], [2], [3], [4], [5], [6], [7]
from DW_GetLenses_jbs.dbo.lenson_cohort_source
order by cast(placed_orders as int) desc

select top 1000 name, placed_orders, 
	count(*) over () num_tot
from DW_GetLenses_jbs.dbo.lenson_cohort_source

select num_tot, placed_orders, 
	count(*) num, count(*)*100/convert(decimal(10,2), num_tot) perc_num, 
	placed_orders * count(*)
from
	(select name, cast(placed_orders as int) placed_orders, 
		count(*) over () num_tot
	from DW_GetLenses_jbs.dbo.lenson_cohort_source) t
group by num_tot, placed_orders
order by num_tot, placed_orders

	select email, count(*)
	from DW_GetLenses_jbs.dbo.lenson_cohort_source
	group by email
	order by count(*) desc

		select *
		from
			(select count(*) over (partition by email) rep_cust, *
			from DW_GetLenses_jbs.dbo.lenson_cohort_source) t
		where rep_cust > 1
		order by rep_cust desc

	select status, count(*)
	from DW_GetLenses_jbs.dbo.lenson_cohort_source
	group by status
	order by status

	select latest_order_channel, count(*)
	from DW_GetLenses_jbs.dbo.lenson_cohort_source
	group by latest_order_channel
	order by latest_order_channel

----------------------------------------------------------

select top 1000 name, placed_orders, purchase_value, 
	[0], [1], [2], [3], [4], [5], [6], [7]
from DW_GetLenses_jbs.dbo.lenson_cohort_source
where placed_orders > 30
order by cast(placed_orders as int) desc

	-- UNPIVOT 1
	select customer, order_num, order_date
	from
		(select top 1000 name customer, --PlacedOrders, PurchaseValue, 
			[0], -- [1], 
			[2], -- [3], 
			[4], -- [5], 
			[6]-- , [7]
		from DW_GetLenses_jbs.dbo.lenson_cohort_source
		where placed_orders > 30) p
	unpivot
		(order_date for order_num in 
			([0], [2], [4], [6])
		) unpvt;

	select customer, placed_orders, purchase_value, 
		order_num, order_date
	from
		(select top 1000 name customer, placed_orders, purchase_value, 
			[0], -- [1], 
			[2], -- [3], 
			[4], -- [5], 
			[6]-- , [7]
		from DW_GetLenses_jbs.dbo.lenson_cohort_source
		where placed_orders > 30) p
	unpivot
		(order_date for order_num in 
			([0], [2], [4], [6])
		) unpvt;

	-- UNPIVOT 2
	select top 1000 name customer, placed_orders, purchase_value, 
		-- order_num, 
		order_date, cast(order_date as date) order_date_d, order_value
	from DW_GetLenses_jbs.dbo.lenson_cohort_source
	cross apply
		(
			select [0], [1]
			union all
			select [2], [3]
			union all
			select [4], [5]
			union all
			select [6], [7]
		) c (order_date, order_value)	
	where placed_orders > 30
	order by placed_orders, customer, order_date

	select top 1000 name customer, 
		city, email, 
		status, account_created, 
		placed_orders, purchase_value, 
		latest_order_channel,
		-- order_num, 
		order_date, order_value
	from DW_GetLenses_jbs.dbo.lenson_cohort_source
	cross apply
		(
			select [0], [1]
			union all
			select [2], [3]
			union all
			select [4], [5]
			union all
			select [6], [7]
			union all
			select [8], [9]
			union all
			select [10], [11]
			union all
			select [12], [13]
			union all
			select [14], [15]
			union all
			select [16], [17]
			union all
			select [18], [19]
			union all
			select [20], [21]
			union all
			select [22], [23]
			union all
			select [24], [25]
			union all
			select [26], [27]
			union all
			select [28], [29]
			union all
			select [30], [31]
			union all
			select [32], [33]
			union all
			select [34], [35]
			union all
			select [36], [37]
			union all
			select [38], [39]
			union all
			select [40], [41]
			union all
			select [42], [43]
			union all
			select [44], [45]
			union all
			select [46], [47]
			union all
			select [48], [49]
			union all
			select [50], [51]
			union all
			select [52], [53]
			union all
			select [54], [55]
			union all
			select [56], [57]
			union all
			select [58], [59]
			union all
			select [60], [61]
			union all
			select [62], [63]
			union all
			select [64], [65]
			union all
			select [66], [67]
			union all
			select [68], [69]
			union all
			select [70], [71]
			union all
			select [72], [73]
			union all
			select [74], [75]
			union all
			select [76], [77]
			union all
			select [78], [79]

		) c (order_date, order_value)	
	-- where placed_orders = 2
	where email = '2f5f4964a44dc1af89024cd8f6679d96'
	order by placed_orders, customer, order_date

	------------------------------------------------------------

	select 
		count(*) over () num_tot, 
		dense_rank() over (order by email) rank_customer,
		*
	from
		(select name customer, 
			city, email, 
			status, account_created, 
			placed_orders, purchase_value, 
			latest_order_channel,
			-- order_num, 
			order_date, order_value
		from DW_GetLenses_jbs.dbo.lenson_cohort_source
		cross apply
			(
				select [0], [1]
				union all
				select [2], [3]
				union all
				select [4], [5]
				union all
				select [6], [7]
				union all
				select [8], [9]
				union all
				select [10], [11]
				union all
				select [12], [13]
				union all
				select [14], [15]
				union all
				select [16], [17]
				union all
				select [18], [19]
				union all
				select [20], [21]
				union all
				select [22], [23]
				union all
				select [24], [25]
				union all
				select [26], [27]
				union all
				select [28], [29]
				union all
				select [30], [31]
				union all
				select [32], [33]
				union all
				select [34], [35]
				union all
				select [36], [37]
				union all
				select [38], [39]
				union all
				select [40], [41]
				union all
				select [42], [43]
				union all
				select [44], [45]
				union all
				select [46], [47]
				union all
				select [48], [49]
				union all
				select [50], [51]
				union all
				select [52], [53]
				union all
				select [54], [55]
				union all
				select [56], [57]
				union all
				select [58], [59]
				union all
				select [60], [61]
				union all
				select [62], [63]
				union all
				select [64], [65]
				union all
				select [66], [67]
				union all
				select [68], [69]
				union all
				select [70], [71]
				union all
				select [72], [73]
				union all
				select [74], [75]
				union all
				select [76], [77]
				union all
				select [78], [79]

			) c (order_date, order_value)) t	
	where len(order_date) > 1
	order by email, order_date

	