
---- PREPARE THE ORDERS DATASET

				select top 1000 idOrder, idCustomer, order_date, order_value, 
					rank() over (partition by idCustomer order by order_date, idOrder)
				from DW_GetLenses_jbs.dbo.lenson_cohort_orders
				where idCustomer between 1 and 20

				select top 1000 idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
					rank() over (partition by idCustomer order by order_date, idOrder)
				from DW_GetLenses_jbs.dbo.lenson_cohort_orders
				where idCustomer between 1 and 20
				order by idCustomer, order_date

				-- 

				select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
					lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
					lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
					lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
				from
					(select top 1000 idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
						rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
					from DW_GetLenses_jbs.dbo.lenson_cohort_orders
					where idCustomer between 1 and 20) t
				order by idCustomer, cust_seq_ord_num

				-- 

				select idCustomer, 
					order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
					order_value, cust_seq_ord_num, 
					next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
					cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
					next_order_value, next_cust_seq_ord_num
				from
					(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
						lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
						lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
						lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
					from
						(select top 1000 idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
							rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
						from DW_GetLenses_jbs.dbo.lenson_cohort_orders
						where idCustomer between 1 and 20) t) t
				order by idCustomer, cust_seq_ord_num

				-- 

				select idCustomer, cust_seq_ord_num, 
					period, period_next, datediff(month, period, period_next) as month_diff, 
					order_value, next_order_value
				from
					(select idCustomer, 
						order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
						order_value, cust_seq_ord_num, 
						next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
						cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
						next_order_value, next_cust_seq_ord_num
					from
						(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
							lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
							lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
							lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
						from
							(select top 1000 idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
								rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
							from DW_GetLenses_jbs.dbo.lenson_cohort_orders
							where idCustomer between 1 and 20) t) t) t
				order by idCustomer, cust_seq_ord_num



--------------------------------------------------------------------------------------------------------------- 
-- NUMBERS (Total Orders - Total Value - Total Customers) based on Period + Cust Order Sequence
select period, cust_seq_ord_num,
	sum(order_value) order_value_s, count(num_cust) num_orders, count(distinct idCustomer) num_customers
from
	(select idCustomer, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		order_value, next_order_value, 1 num_cust
	from
		(select idCustomer, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			order_value, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_order_value, next_cust_seq_ord_num
		from
			(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
				lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
			from
				(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
					rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lenson_cohort_orders
				-- where idCustomer between 1 and 20
				) t) t) t) t
group by period, cust_seq_ord_num
order by period, cust_seq_ord_num

-- NUMBERS (Total Orders and Next Order Value - Total Value - Total Customers) based on Period + Cust Order Sequence + Diff Months with Next Order
select period, cust_seq_ord_num, month_diff, 
	sum(order_value) order_value_s, sum(next_order_value) next_order_value_s, 
	count(num_cust) num_orders, count(distinct idCustomer) num_customers
from
	(select idCustomer, cust_seq_ord_num, 
		period, period_next, datediff(month, period, period_next) as month_diff, 
		order_value, next_order_value, 1 num_cust
	from
		(select idCustomer, 
			order_date, yyyy, mm, cast(cast(yyyy as varchar) + '-' + cast(mm as varchar) + '-01' as date) period, 
			order_value, cust_seq_ord_num, 
			next_created_at, year(next_created_at) yyyy_next, month(next_created_at) mm_next, 
			cast(cast(year(next_created_at) as varchar) + '-' + cast(month(next_created_at) as varchar) + '-01' as date) period_next, 
			next_order_value, next_cust_seq_ord_num
		from
			(select idCustomer, order_date, yyyy, mm, order_value, cust_seq_ord_num, 
				lead(cust_seq_ord_num) over (partition by idCustomer order by cust_seq_ord_num) next_cust_seq_ord_num, 
				lead(order_date) over (partition by idCustomer order by cust_seq_ord_num) next_created_at, 
				lead(order_value) over (partition by idCustomer order by cust_seq_ord_num) next_order_value	
			from
				(select idOrder, idCustomer, order_date, year(order_date) yyyy, month(order_date) mm, order_value, 
					rank() over (partition by idCustomer order by order_date, idOrder) cust_seq_ord_num
				from DW_GetLenses_jbs.dbo.lenson_cohort_orders
				-- where idCustomer between 1 and 20
				) t) t) t) t
group by period, cust_seq_ord_num, month_diff
order by cust_seq_ord_num, period, month_diff
