
select top 1000 idCustomer, customer_name, email, city, 
	status, account_created, purchase_value, latest_order_channel
from DW_GetLenses_jbs.dbo.lenson_cohort_customer
order by idCustomer

select top 1000 
	count(*) over (partition by email) num_rep,
	rank() over (partition by email order by account_created) ord_rep,
	idCustomer, customer_name, email, city, 
	status, account_created, purchase_value, latest_order_channel
from DW_GetLenses_jbs.dbo.lenson_cohort_customer
order by num_rep desc, idCustomer, account_created

select idCustomer
from
	(select count(*) over (partition by email) num_rep, idCustomer
	from DW_GetLenses_jbs.dbo.lenson_cohort_customer) t
where num_rep = 1

---

select top 1000 idOrder, idCustomer, email, order_date, order_value
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
order by idCustomer, order_date

--- 

select top 1000 idCustomer, count(*) num_orders, sum(order_value) sum_order_value
from DW_GetLenses_jbs.dbo.lenson_cohort_orders
group by idCustomer
order by idCustomer

select c.idCustomer, o.num_orders, c.placed_orders, o.sum_order_value, c.purchase_value
from
		(select idCustomer, count(*) num_orders, sum(order_value) sum_order_value
		from DW_GetLenses_jbs.dbo.lenson_cohort_orders
		group by idCustomer) o
	inner join
		DW_GetLenses_jbs.dbo.lenson_cohort_customer c on o.idCustomer = c.idCustomer
where o.num_orders <> c.placed_orders or o.sum_order_value <> c.purchase_value
order by c.idCustomer
