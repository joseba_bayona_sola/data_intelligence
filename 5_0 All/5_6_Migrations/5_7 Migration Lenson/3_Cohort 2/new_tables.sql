USE [DW_GetLenses_jbs]
GO

select convert(int, orders_id) orders_id, convert(int, customers_id) customers_id, 
	try_cast(date_purchased as datetime) date_purchased, 
	customers_name, customers_street_address, customers_city, customers_postcode, customers_country, 
	customers_telephone, customers_email_address, customers_dob,
	currency, 
	payment_method, delivery_method,
	referers_categories_name, referers_name
into DW_GetLenses_jbs.dbo.lenson_order_header_full_def
from DW_GetLenses_jbs.dbo.lenson_order_header_full


select convert(int, orders_products_id) orders_products_id, 
	convert(int, orders_id) orders_id, convert(int, products_id) products_id, visma_id, products_name, products_model, info, 
	convert(decimal(12, 4), products_price) products_price, convert(int, products_quantity) products_quantity
into DW_GetLenses_jbs.dbo.lenson_order_item_full_def
from DW_GetLenses_jbs.dbo.lenson_order_item_full


select convert(int, orders_total_id) orders_total_id, 
	convert(int, orders_id) orders_id, class, title, convert(decimal(12, 4), value) value
into DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def
from DW_GetLenses_jbs.dbo.lenson_order_header_values_full


