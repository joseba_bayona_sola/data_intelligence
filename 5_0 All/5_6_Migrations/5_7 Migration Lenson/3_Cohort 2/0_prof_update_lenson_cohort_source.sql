
select top 100 orders_id, customers_id, 
	date_purchased, 
	customers_name, customers_street_address, customers_city, customers_postcode, customers_country, 
	customers_telephone, customers_email_address, customers_dob,
	currency, 
	payment_method, delivery_method,
	referers_categories_name, referers_name
from DW_GetLenses_jbs.dbo.lenson_order_header_full
where customers_id = 36919

update DW_GetLenses_jbs.dbo.lenson_order_header_full
set
	date_purchased = replace(date_purchased, '"', ''), 
	customers_name = replace(customers_name, '"', ''), 
	customers_street_address = replace(customers_street_address, '"', ''), 
	customers_city = replace(customers_city, '"', ''), 
	customers_postcode = replace(customers_postcode, '"', ''), 
	customers_country = replace(customers_country, '"', ''), 
	delivery_method = replace(delivery_method, '"', ''), 
	referers_categories_name = replace(referers_categories_name, '"', ''), 
	referers_name = replace(referers_name, '"', '')

---------------------------------------------------------------------


select top 100 orders_id, customers_id, 
	date_purchased, -- convert(datetime, date_purchased, 120) date_purchased_dt, 
	try_cast(date_purchased as datetime) date_purchased_dt2,
	customers_name, customers_street_address, customers_city, customers_postcode, customers_country, 
	customers_telephone, customers_email_address, customers_dob,
	currency, 
	payment_method, delivery_method,
	referers_categories_name, referers_name
from DW_GetLenses_jbs.dbo.lenson_order_header_full
where try_cast(date_purchased as datetime) is null

select year(date_purchased_dt) yyyy, month(date_purchased_dt) mm, count(*) num_orders
from
	(select try_cast(date_purchased as datetime) date_purchased_dt
	from DW_GetLenses_jbs.dbo.lenson_order_header_full) t
where date_purchased_dt is not null
group by year(date_purchased_dt), month(date_purchased_dt)
order by year(date_purchased_dt), month(date_purchased_dt)

