
select top 1000 *
from DW_GetLenses_jbs.dbo.lenson_order_header_full_def

select top 1000 *
from DW_GetLenses_jbs.dbo.lenson_order_item_full_def

select top 1000 *
from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def

---------------------------------------------------------

select top 1000 orders_id, 
	date_purchased,
	customers_id, customers_name, customers_street_address, customers_city, customers_postcode, customers_country, customers_telephone, customers_email_address, customers_dob, 
	currency, 
	payment_method, delivery_method, referers_categories_name, referers_name
from DW_GetLenses_jbs.dbo.lenson_order_header_full_def

	select count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def

	select top 1000 orders_id, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
	group by orders_id
	order by count(*) desc

	select top 1000 currency, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
	group by currency
	order by currency

	select top 1000 payment_method, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
	group by payment_method
	order by payment_method

	select top 1000 delivery_method, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
	group by delivery_method
	order by delivery_method

select top 1000 orders_products_id, 
	orders_id, 
	products_id, visma_id, products_name, products_model, info, products_price, products_quantity
from DW_GetLenses_jbs.dbo.lenson_order_item_full_def

	select count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_item_full_def

	select top 1000 orders_products_id, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_item_full_def
	group by orders_products_id
	order by count(*) desc

	select top 1000 products_id, products_name, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_item_full_def
	group by products_id, products_name
	order by products_id, products_name

	select top 1000 products_model, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_item_full_def
	group by products_model
	order by products_model

select top 1000 orders_total_id, 
	orders_id, 
	class, title, value
from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def

	select count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def

	select top 1000 orders_total_id, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def
	group by orders_total_id
	order by count(*) desc

	select top 1000 class, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def
	group by class
	order by class

	select top 1000 class, title, count(*)
	from DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def
	group by class, title
	order by class, title

----------------------------------------------------------------------

select top 1000 orders_id, count(*)
from DW_GetLenses_jbs.dbo.lenson_order_item_full_def
group by orders_id
order by count(*) desc



select top 1000 oh.orders_id, oi.orders_products_id, 
	oh.date_purchased, oh.customers_id, oh.customers_name, oh.currency, oh.payment_method, oh.delivery_method, 
	oi.products_id, oi.products_name, oi.products_model, oi.info, oi.products_price, oi.products_quantity
from 
		DW_GetLenses_jbs.dbo.lenson_order_header_full_def oh
	inner join
		DW_GetLenses_jbs.dbo.lenson_order_item_full_def oi on oh.orders_id = oi.orders_id
where oh.orders_id = 3074033
order by oi.products_id

select top 1000 oh.orders_id, ov.orders_total_id, 
	oh.date_purchased, oh.customers_id, oh.customers_name, oh.currency, oh.payment_method, oh.delivery_method, 
	ov.class, ov.title, ov.value
from 
		DW_GetLenses_jbs.dbo.lenson_order_header_full_def oh
	inner join
		DW_GetLenses_jbs.dbo.lenson_order_header_values_full_def ov on oh.orders_id = ov.orders_id
where oh.orders_id = 3074033
order by ov.class

----------------------------------------------------------------------

select top 1000 orders_id, 
	date_purchased,
	customers_id, customers_name, 
	currency, 
	payment_method, delivery_method, 
	count(*) over (partition by customers_id) num_tot_customer, 
	min(date_purchased) over (partition by customers_id) min_date_purchased_customer, 
	max(date_purchased) over (partition by customers_id) min_date_purchased_customer, 
	rank() over (partition by customers_id order by date_purchased, orders_id) rank_order_customer
from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
order by num_tot_customer desc, customers_id, orders_id

----------------------------------------------------------------------

	select top 1000 customers_id, count(*) num_orders
	from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
	group by customers_id
	order by customers_id desc

	select num_orders, count(*)
	from
		(select customers_id, count(*) num_orders
		from DW_GetLenses_jbs.dbo.lenson_order_header_full_def
		group by customers_id) t
	group by num_orders
	order by num_orders

