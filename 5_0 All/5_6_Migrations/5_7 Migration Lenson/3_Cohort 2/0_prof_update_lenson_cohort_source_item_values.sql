
select top 1000 orders_products_id, 
	orders_id, products_id, visma_id, products_name, products_model, info, products_price, products_quantity
from DW_GetLenses_jbs.dbo.lenson_order_item_full

update DW_GetLenses_jbs.dbo.lenson_order_item_full
set
	products_name = replace(products_name, '"', ''), 
	info = replace(info, '"', ''), 
	products_model = replace(products_model, '"', '')


select top 1000 orders_total_id, 
	orders_id, class, title, value
from DW_GetLenses_jbs.dbo.lenson_order_header_values_full

update DW_GetLenses_jbs.dbo.lenson_order_header_values_full
set
	class = replace(class, '"', ''), 
	title = replace(title, '"', '')