
drop table DW_GetLenses_jbs.dbo.fgc_cohort
go 

create table DW_GetLenses_jbs.dbo.fgc_cohort(
	order_id				bigint, 
	customer_id				bigint, 
	order_date				date, 
	item_product_family		varchar(50),
	base_grand_total		decimal(12, 4)); 

	insert into DW_GetLenses_jbs.dbo.fgc_cohort(order_id, customer_id, order_date, item_product_family, base_grand_total)

		select order_id, customer_id, convert(date, order_date, 103) order_date, 
			null item_product_family,
			convert(decimal(12, 4), order_grand_total) base_grand_total
		from DW_GetLenses_jbs.dbo.fgc_cohort_source
		where order_id <> 0
