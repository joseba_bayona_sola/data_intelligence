
select top 1000 order_id, order_date, 
	convert(date, order_date, 103), 
	customer_id, shipping_country, 
	convert(decimal(12, 4), order_grand_total) order_grand_total, 
	convert(decimal(12, 4), order_shipping_amount) order_shipping_amount, convert(decimal(12, 4), order_discount_amount) order_discount_amount, convert(decimal(12, 4), order_subtotal) order_subtotal
from DW_GetLenses_jbs.dbo.fgc_cohort_source
order by convert(date, order_date, 103)

select top 1000 order_id, order_date, 
	convert(date, order_date, 103), 
	customer_id, shipping_country, order_grand_total, order_shipping_amount, order_discount_amount, order_subtotal
from DW_GetLenses_jbs.dbo.fgc_cohort_source
order by convert(date, order_date, 103)


select count (distinct customer_id), count(*), min(convert(date, order_date, 103))
from DW_GetLenses_jbs.dbo.fgc_cohort_source
