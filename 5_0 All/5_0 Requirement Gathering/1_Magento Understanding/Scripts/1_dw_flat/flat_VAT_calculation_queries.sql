
select count(*)
from dw_updated_customers;

select count(*) 
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id 
	INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = doh.customer_id;

select count(*) 
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day;
    
-- * Set country information for entity
select ov.order_id, ov.country_type, ov.country_id, ov.entity_order_id 
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

  select ov.country_type, count(*)
  from 
      order_headers_vat ov
    INNER JOIN 
  		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day  group by country_type
  order by ov.country_type;

  select ov.country_type, ov.country_id, count(*)
  from
      order_headers_vat ov
    INNER JOIN 
  		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
  group by ov.country_type, ov.country_id
  order by ov.country_type, ov.country_id;

-- * Add aggregated values from lines that will be need for other calcs
select ov.order_id, ov.has_lens, ov.total_weight, ov.total_cost, ov.total_qty, ov.max_row_total, ov.min_row_total
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

-- * Calc all inc_vat totals for entity 
select ov.order_id, 
  ov.local_subtotal_inc_vat, ov.local_shipping_inc_vat, ov.local_discount_inc_vat, ov.local_store_credit_inc_vat, ov.local_adjustment_inc_vat, 
  ov.local_total_inc_vat, ov.magento_grand_total, 
  ov.local_subtotal_cost, ov.local_shipping_cost, 
  ov.local_total_cost, 
  ov.order_date
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

-- Vat Rollup Value
select order_id, total_inc_vat, has_lens
from order_headers_vat_rollup
order by order_id desc
limit 1000;

select ov.order_id, ov.rollup_total, ov.rollup_has_lens
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

-- * Flag Rows by VAT logic based on business rules
select ov.order_id, ov.vat_logic, doh.store_id, ov.country_type, ov.country_id, ov.order_date
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

  select ov.vat_logic, doh.store_id, count(*)
  from 
      order_headers_vat ov
    INNER JOIN 
  		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
  group by ov.vat_logic, doh.store_id
  order by ov.vat_logic, doh.store_id
  limit 1000;
  
  select ov.vat_logic, doh.store_id, ov.country_type, ov.country_id, count(*)
  from 
      order_headers_vat ov
    INNER JOIN 
  		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
  group by ov.vat_logic, doh.store_id, ov.country_type, ov.country_id
  order by ov.vat_logic, doh.store_id, ov.country_type, ov.country_id
  limit 1000;

-- * Set VAT rate professional fee (VAT exempt amount of the order) 
select ov.order_id, ov.vat_logic, ov.rollup_has_lens, ov.rollup_total, 
  ov.prof_fee_percent, ov.calculated_prof_fee, ov.vat_rate
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

  select ov.vat_logic, ov.prof_fee_percent, ov.vat_rate, count(*)
  from 
      order_headers_vat ov
    INNER JOIN 
  		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
  group by ov.vat_logic, ov.prof_fee_percent, ov.vat_rate
  order by ov.vat_logic, ov.prof_fee_percent, ov.vat_rate  
  limit 1000;

      select ov.vat_logic, ov.prof_fee_percent, ov.vat_rate, count(*)
      from order_headers_vat ov
      group by ov.vat_logic, ov.prof_fee_percent, ov.vat_rate
      order by ov.vat_logic, ov.prof_fee_percent, ov.vat_rate  
      limit 1000;
      
-- * Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate)    
select ov.order_id, ov.vat_logic, ov.rollup_has_lens, ov.rollup_total, 
  ov.prof_fee_percent, ov.calculated_prof_fee, ov.vat_rate,  
  ov.prof_fee, ov.calculated_vat, ov.local_total_vat 
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

  select ov.order_id, ov.vat_logic, ov.rollup_has_lens, ov.rollup_total, 
    ov.prof_fee_percent, ov.calculated_prof_fee, ov.vat_rate,  
    ov.prof_fee, ov.calculated_vat, ov.local_total_vat 
  from order_headers_vat ov
  where ov.calculated_prof_fee <> ov.prof_fee or ov.calculated_vat <> ov.local_total_vat
  order by ov.order_id desc
  limit 1000;
  
-- * Calculate Exc VAT and global values   
select ov.order_id, ov.vat_logic, ov.rollup_has_lens, ov.rollup_total, 
  ov.prof_fee_percent, ov.calculated_prof_fee, ov.vat_rate,  
  ov.prof_fee, ov.calculated_vat, ov.local_total_vat, 
  ov.local_subtotal_vat, ov.local_subtotal_inc_vat, ov.local_subtotal_exc_vat, 
  ov.global_subtotal_vat, ov.global_subtotal_inc_vat, ov.global_subtotal_exc_vat, doh.base_to_global_rate
  
from 
    order_headers_vat ov
  INNER JOIN 
		dw_order_headers doh ON ov.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by ov.order_id desc
limit 1000;

-- -------------------------------------------------------------------------- 

select ol.order_id, olv.item_id, olv.local_price_inc_vat, olv.row_total_inc_vat, olv.actual_cost, olv.qty
from 
    order_lines_vat olv
  inner join
    dw_order_lines ol on olv.item_id = ol.item_id and ol.updated_at > curdate() - interval 1 day
order by ol.order_id desc, olv.item_id  
limit 1000;

select a.order_id, a.item_id, a.rate
from 
    order_lines_allocation_rate a
  INNER JOIN 
		dw_order_headers doh ON a.order_id = doh.order_id and doh.updated_at > curdate() - interval 1 day
order by a.order_id desc, a.item_id  
limit 1000;    

