CREATE TABLE IF NOT EXISTS dw_summ_vw_order_headers_order 
  SELECT store_name, document_type, YEAR(document_date) as year ,MONTH(document_date) as month, 
    'global_total_inc_vat' as global_total_inc_vat,SUM(global_total_inc_vat) as SUM_global_total_inc_vat, 
    'global_total_exc_vat' as global_total_exc_vat,SUM(global_total_exc_vat) as SUM_global_total_exc_vat 
  FROM vw_order_headers_order 
  GROUP BY store_name, document_type, YEAR(document_date), MONTH(document_date) 
  ORDER BY store_name, document_type, YEAR(document_date), MONTH(document_date);

CREATE TABLE IF NOT EXISTS dw_summ_vw_order_headers_cancel 
  SELECT store_name, document_type, YEAR(document_date) as year , MONTH(document_date) as month, 
    'global_total_inc_vat' as global_total_inc_vat,SUM(global_total_inc_vat) as SUM_global_total_inc_vat, 
    'global_total_exc_vat' as global_total_exc_vat,SUM(global_total_exc_vat) as SUM_global_total_exc_vat 
  FROM vw_order_headers_cancel 
  GROUP BY store_name, document_type, YEAR(document_date), MONTH(document_date) 
  ORDER BY store_name, document_type, YEAR(document_date), MONTH(document_date);

CREATE TABLE IF NOT EXISTS dw_summ_vw_order_headers_creditmemo 
  SELECT store_name, document_type, YEAR(document_date) as year ,MONTH(document_date) as month, 
    'global_total_inc_vat' as global_total_inc_vat,SUM(global_total_inc_vat) as SUM_global_total_inc_vat, 
    'global_total_exc_vat' as global_total_exc_vat,SUM(global_total_exc_vat) as SUM_global_total_exc_vat 
  FROM vw_order_headers_creditmemo 
  GROUP BY store_name, document_type, YEAR(document_date), MONTH(document_date) 
  ORDER BY store_name, document_type, YEAR(document_date), MONTH(document_date);

-- 

CREATE TABLE IF NOT EXISTS dw_summ_vw_order_lines_order 
  SELECT store_name, document_type, YEAR(document_date) as year ,MONTH(document_date) as month, 
    'global_line_total_inc_vat' as global_line_total_inc_vat,SUM(global_line_total_inc_vat) as SUM_global_line_total_inc_vat, 
    'global_line_total_exc_vat' as global_line_total_exc_vat,SUM(global_line_total_exc_vat) as SUM_global_line_total_exc_vat 
  FROM vw_order_lines_order 
  GROUP BY store_name,document_type,YEAR(document_date),MONTH(document_date) 
  ORDER BY store_name,document_type,YEAR(document_date),MONTH(document_date);

CREATE TABLE IF NOT EXISTS dw_summ_vw_order_lines_cancel 
  SELECT store_name, document_type, YEAR(document_date) as year ,MONTH(document_date) as month, 
    'global_line_total_inc_vat' as global_line_total_inc_vat,SUM(global_line_total_inc_vat) as SUM_global_line_total_inc_vat, 
    'global_line_total_exc_vat' as global_line_total_exc_vat,SUM(global_line_total_exc_vat) as SUM_global_line_total_exc_vat 
  FROM vw_order_lines_cancel 
  GROUP BY store_name,document_type,YEAR(document_date),MONTH(document_date) 
  ORDER BY store_name,document_type,YEAR(document_date),MONTH(document_date);

CREATE TABLE IF NOT EXISTS dw_summ_vw_order_lines_creditmemo 
  SELECT store_name, document_type, YEAR(document_date) as year ,MONTH(document_date) as month, 
    'global_line_total_inc_vat' as global_line_total_inc_vat,SUM(global_line_total_inc_vat) as SUM_global_line_total_inc_vat, 
    'global_line_total_exc_vat' as global_line_total_exc_vat,SUM(global_line_total_exc_vat) as SUM_global_line_total_exc_vat 
  FROM vw_order_lines_creditmemo 
  GROUP BY store_name,document_type,YEAR(document_date),MONTH(document_date) 
  ORDER BY store_name,document_type,YEAR(document_date),MONTH(document_date);
