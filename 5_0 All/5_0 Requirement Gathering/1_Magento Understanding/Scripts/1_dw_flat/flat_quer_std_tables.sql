
-- Invoices
REPLACE INTO dw_invoice_headers (invoice_id,store_id,
  base_grand_total,shipping_tax_amount,tax_amount,base_tax_amount,store_to_order_rate,base_shipping_tax_amount,base_discount_amount,base_to_order_rate,grand_total,shipping_amount,subtotal_incl_tax,base_subtotal_incl_tax,store_to_base_rate,base_shipping_amount,total_qty,base_to_global_rate,subtotal,base_subtotal,discount_amount,billing_address_id,is_used_for_refund,order_id,email_sent,can_void_flag,state,shipping_address_id,cybersource_token,store_currency_code,transaction_id,order_currency_code,base_currency_code,global_currency_code,invoice_no,created_at,updated_at,base_customer_balance_amount,base_gift_cards_amount,base_reward_currency_amount,customer_balance_amount,gift_cards_amount,reward_currency_amount,reward_points_balance,hidden_tax_amount,base_hidden_tax_amount,shipping_hidden_tax_amount,base_shipping_hidden_tax_amnt,shipping_incl_tax,base_shipping_incl_tax,base_total_refunded,discount_description,gw_base_price,gw_price,gw_items_base_price,gw_items_price,gw_card_base_price,gw_card_price,gw_base_tax_amount,gw_tax_amount,gw_items_base_tax_amount,gw_items_tax_amount,gw_card_base_tax_amount,gw_card_tax_amount,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.store_id,
    a.base_grand_total,a.shipping_tax_amount,a.tax_amount,a.base_tax_amount,a.store_to_order_rate,a.base_shipping_tax_amount,a.base_discount_amount,a.base_to_order_rate,a.grand_total,a.shipping_amount,a.subtotal_incl_tax,a.base_subtotal_incl_tax,a.store_to_base_rate,a.base_shipping_amount,a.total_qty,a.base_to_global_rate,a.subtotal,a.base_subtotal,a.discount_amount,a.billing_address_id,a.is_used_for_refund,a.order_id,a.email_sent,a.can_void_flag,a.state,a.shipping_address_id,a.cybersource_token,a.store_currency_code,a.transaction_id,a.order_currency_code,a.base_currency_code,a.global_currency_code,a.increment_id,a.created_at,a.updated_at,a.base_customer_balance_amount,a.base_gift_cards_amount,a.base_reward_currency_amount,a.customer_balance_amount,a.gift_cards_amount,a.reward_currency_amount,a.reward_points_balance,a.hidden_tax_amount,a.base_hidden_tax_amount,a.shipping_hidden_tax_amount,a.base_shipping_hidden_tax_amnt,a.shipping_incl_tax,a.base_shipping_incl_tax,a.base_total_refunded,a.discount_description,a.gw_base_price,a.gw_price,a.gw_items_base_price,a.gw_items_price,a.gw_card_base_price,a.gw_card_price,a.gw_base_tax_amount,a.gw_tax_amount,a.gw_items_base_tax_amount,a.gw_items_tax_amount,a.gw_card_base_tax_amount,a.gw_card_tax_amount,
    0,0,0,null,0 
   FROM 
      magento01.sales_flat_invoice a 
    LEFT JOIN 
      dw_invoice_headers b ON a.entity_id=b.invoice_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
REPLACE INTO dw_invoice_lines (item_id,invoice_id,base_price,
  base_weee_tax_row_disposition,weee_tax_applied_row_amount,base_weee_tax_applied_amount,tax_amount,base_row_total,discount_amount,row_total,weee_tax_row_disposition,base_discount_amount,base_weee_tax_disposition,price_incl_tax,weee_tax_applied_amount,base_tax_amount,base_price_incl_tax,qty,weee_tax_disposition,base_cost,base_weee_tax_applied_row_amnt,price,base_row_total_incl_tax,row_total_incl_tax,product_id,order_line_id,additional_data,description,weee_tax_applied,sku,name,hidden_tax_amount,base_hidden_tax_amount,
  dw_att,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.parent_id,
    a.base_price,a.base_weee_tax_row_disposition,a.weee_tax_applied_row_amount,a.base_weee_tax_applied_amount,a.tax_amount,a.base_row_total,a.discount_amount,a.row_total,a.weee_tax_row_disposition,a.base_discount_amount,a.base_weee_tax_disposition,a.price_incl_tax,a.weee_tax_applied_amount,a.base_tax_amount,a.base_price_incl_tax,a.qty,a.weee_tax_disposition,a.base_cost,a.base_weee_tax_applied_row_amnt,a.price,a.base_row_total_incl_tax,a.row_total_incl_tax,a.product_id,a.order_item_id,a.additional_data,a.description,a.weee_tax_applied,a.sku,a.name,a.hidden_tax_amount,a.base_hidden_tax_amount,
    0,0,null,0 
  FROM 
      magento01.sales_flat_invoice_item a 
    LEFT JOIN 
      dw_invoice_headers b ON a.parent_id = b.invoice_id 
  WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);

  
  
-- CreditMemo  
REPLACE INTO dw_creditmemo_headers (creditmemo_id,store_id,
  adjustment_positive,base_shipping_tax_amount,store_to_order_rate,base_discount_amount,base_to_order_rate,grand_total,base_adjustment_negative,base_subtotal_incl_tax,shipping_amount,subtotal_incl_tax,adjustment_negative,base_shipping_amount,store_to_base_rate,base_to_global_rate,base_adjustment,base_subtotal,discount_amount,subtotal,adjustment,base_grand_total,base_adjustment_positive,base_tax_amount,shipping_tax_amount,tax_amount,order_id,email_sent,creditmemo_status,state,shipping_address_id,billing_address_id,invoice_id,cybersource_token,store_currency_code,order_currency_code,base_currency_code,global_currency_code,transaction_id,creditmemo_no,created_at,updated_at,base_customer_balance_amount,bs_customer_bal_total_refunded,base_gift_cards_amount,base_reward_currency_amount,customer_balance_amount,customer_bal_total_refunded,gift_cards_amount,reward_currency_amount,reward_points_balance,reward_points_balance_refund,hidden_tax_amount,base_hidden_tax_amount,shipping_hidden_tax_amount,base_shipping_hidden_tax_amnt,shipping_incl_tax,base_shipping_incl_tax,discount_description,gw_base_price,gw_price,gw_items_base_price,gw_items_price,gw_card_base_price,gw_card_price,gw_base_tax_amount,gw_tax_amount,gw_items_base_tax_amount,gw_items_tax_amount,gw_card_base_tax_amount,gw_card_tax_amount,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.store_id,
    a.adjustment_positive,a.base_shipping_tax_amount,a.store_to_order_rate,a.base_discount_amount,a.base_to_order_rate,a.grand_total,a.base_adjustment_negative,a.base_subtotal_incl_tax,a.shipping_amount,a.subtotal_incl_tax,a.adjustment_negative,a.base_shipping_amount,a.store_to_base_rate,a.base_to_global_rate,a.base_adjustment,a.base_subtotal,a.discount_amount,a.subtotal,a.adjustment,a.base_grand_total,a.base_adjustment_positive,a.base_tax_amount,a.shipping_tax_amount,a.tax_amount,a.order_id,a.email_sent,a.creditmemo_status,a.state,a.shipping_address_id,a.billing_address_id,a.invoice_id,a.cybersource_token,a.store_currency_code,a.order_currency_code,a.base_currency_code,a.global_currency_code,a.transaction_id,a.increment_id,a.created_at,a.updated_at,a.base_customer_balance_amount,a.bs_customer_bal_total_refunded,a.base_gift_cards_amount,a.base_reward_currency_amount,a.customer_balance_amount,a.customer_bal_total_refunded,a.gift_cards_amount,a.reward_currency_amount,a.reward_points_balance,a.reward_points_balance_refund,a.hidden_tax_amount,a.base_hidden_tax_amount,a.shipping_hidden_tax_amount,a.base_shipping_hidden_tax_amnt,a.shipping_incl_tax,a.base_shipping_incl_tax,a.discount_description,a.gw_base_price,a.gw_price,a.gw_items_base_price,a.gw_items_price,a.gw_card_base_price,a.gw_card_price,a.gw_base_tax_amount,a.gw_tax_amount,a.gw_items_base_tax_amount,a.gw_items_tax_amount,a.gw_card_base_tax_amount,a.gw_card_tax_amount,  
    0,0,0,null,0   
  FROM 
      magento01.sales_flat_creditmemo a 
    LEFT JOIN 
      dw_creditmemo_headers b ON a.entity_id=b.creditmemo_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
REPLACE INTO dw_creditmemo_lines (item_id,creditmemo_id,
  weee_tax_applied_row_amount,base_price,base_weee_tax_row_disposition,tax_amount,base_weee_tax_applied_amount,weee_tax_row_disposition,base_row_total,discount_amount,row_total,weee_tax_applied_amount,base_discount_amount,base_weee_tax_disposition,price_incl_tax,base_tax_amount,weee_tax_disposition,base_price_incl_tax,qty,base_cost,base_weee_tax_applied_row_amnt,price,base_row_total_incl_tax,row_total_incl_tax,product_id,order_line_id,additional_data,description,weee_tax_applied,sku,name,hidden_tax_amount,base_hidden_tax_amount,dw_att,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.parent_id,a.weee_tax_applied_row_amount,a.base_price,a.base_weee_tax_row_disposition,a.tax_amount,a.base_weee_tax_applied_amount,a.weee_tax_row_disposition,a.base_row_total,a.discount_amount,a.row_total,a.weee_tax_applied_amount,a.base_discount_amount,a.base_weee_tax_disposition,a.price_incl_tax,a.base_tax_amount,a.weee_tax_disposition,a.base_price_incl_tax,a.qty,a.base_cost,a.base_weee_tax_applied_row_amnt,a.price,a.base_row_total_incl_tax,a.row_total_incl_tax,a.product_id,a.order_item_id,a.additional_data,a.description,a.weee_tax_applied,a.sku,a.name,a.hidden_tax_amount,a.base_hidden_tax_amount,
    0,0,null,0 
  FROM 
      magento01.sales_flat_creditmemo_item a 
    LEFT JOIN 
      dw_creditmemo_headers b ON a.parent_id = b.creditmemo_id 
  WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);
  
 
-- Shipments  
REPLACE INTO dw_shipment_headers (shipment_id,store_id,
  total_weight,total_qty,email_sent,order_id,customer_id,shipping_address_id,billing_address_id,shipment_status,shipment_no,created_at,updated_at,shipping_description,shipping_printed,pvx_delete_prevent,printed_at,despatched_at,packages,shipping_label,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.store_id,a.total_weight,a.total_qty,a.email_sent,a.order_id,a.customer_id,a.shipping_address_id,a.billing_address_id,a.shipment_status,a.increment_id,a.created_at,a.updated_at,a.shipping_description,a.shipping_printed,a.pvx_delete_prevent,a.printed_at,a.despatched_at,a.packages,a.shipping_label,
    0,0,0,null,0   
  FROM 
      magento01.sales_flat_shipment a 
    LEFT JOIN 
      dw_shipment_headers b ON a.entity_id=b.shipment_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
REPLACE INTO dw_shipment_lines (item_id,shipment_id,
  row_total,price,weight,qty,product_id,order_line_id,additional_data,description,name,sku,
  dw_att,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.parent_id,
    a.row_total,a.price,a.weight,a.qty,a.product_id,a.order_item_id,a.additional_data,a.description,a.name,a.sku,
    0,0,null,0 
  FROM 
      magento01.sales_flat_shipment_item a 
    LEFT JOIN 
      dw_shipment_headers b ON a.parent_id = b.shipment_id 
  WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);
  
 
-- Orders  
REPLACE INTO dw_order_headers (order_id,state,
  status,prescription_verification_type,coupon_code,protect_code,shipping_description,is_virtual,store_id,customer_id,base_discount_amount,base_discount_canceled,base_discount_invoiced,base_discount_refunded,base_grand_total,base_shipping_amount,base_shipping_canceled,base_shipping_invoiced,base_shipping_refunded,base_shipping_tax_amount,base_shipping_tax_refunded,base_subtotal,base_subtotal_canceled,base_subtotal_invoiced,base_subtotal_refunded,base_tax_amount,base_tax_canceled,base_tax_invoiced,base_tax_refunded,base_to_global_rate,base_to_order_rate,base_total_canceled,base_total_invoiced,base_total_invoiced_cost,base_total_offline_refunded,base_total_online_refunded,base_total_paid,base_total_qty_ordered,base_total_refunded,discount_amount,discount_canceled,discount_invoiced,discount_refunded,grand_total,shipping_amount,shipping_canceled,shipping_invoiced,shipping_refunded,shipping_tax_amount,shipping_tax_refunded,store_to_base_rate,store_to_order_rate,subtotal,subtotal_canceled,subtotal_invoiced,subtotal_refunded,tax_amount,tax_canceled,tax_invoiced,tax_refunded,total_canceled,total_invoiced,total_offline_refunded,total_online_refunded,total_paid,total_qty_ordered,total_refunded,can_ship_partially,can_ship_partially_item,customer_is_guest,customer_note_notify,billing_address_id,customer_group_id,edit_increment,email_sent,forced_shipment_with_invoice,gift_message_id,payment_auth_expiration,paypal_ipn_customer_notified,quote_address_id,quote_id,shipping_address_id,adjustment_negative,adjustment_positive,base_adjustment_negative,base_adjustment_positive,base_shipping_discount_amount,base_subtotal_incl_tax,base_total_due,payment_authorization_amount,shipping_discount_amount,subtotal_incl_tax,total_due,weight,customer_dob,order_no,applied_rule_ids,base_currency_code,customer_email,customer_firstname,customer_lastname,customer_middlename,customer_prefix,customer_suffix,customer_taxvat,discount_description,ext_customer_id,ext_order_id,global_currency_code,hold_before_state,hold_before_status,order_currency_code,original_increment_id,relation_child_id,relation_child_real_id,relation_parent_id,relation_parent_real_id,remote_ip,shipping_method,store_currency_code,store_name,x_forwarded_for,customer_note,created_at,updated_at,total_item_count,customer_gender,affilBatch,affilCode,affilUserRef,base_custbalance_amount,base_customer_balance_amount,base_customer_balance_invoiced,base_customer_balance_refunded,bs_customer_bal_total_refunded,base_gift_cards_amount,base_gift_cards_invoiced,base_gift_cards_refunded,base_reward_currency_amount,base_rwrd_crrncy_amt_invoiced,base_rwrd_crrncy_amnt_refnded,custbalance_amount,customer_balance_amount,customer_balance_invoiced,customer_balance_refunded,customer_bal_total_refunded,gift_cards,gift_cards_amount,gift_cards_invoiced,gift_cards_refunded,is_multi_payment,picklist_printed,postoptics_auto_verification,postoptics_colour_code,postoptics_colour_summ,postoptics_source,reminder_date,reminder_mobile,reminder_period,reminder_presc,reminder_type,reward_currency_amount,rwrd_currency_amount_invoiced,rwrd_crrncy_amnt_refunded,reward_points_balance,reward_points_balance_refunded,reward_points_balance_refund,reward_salesrule_points,shipping_inwarehouse,hidden_tax_amount,base_hidden_tax_amount,shipping_hidden_tax_amount,base_shipping_hidden_tax_amnt,hidden_tax_invoiced,base_hidden_tax_invoiced,hidden_tax_refunded,base_hidden_tax_refunded,shipping_incl_tax,base_shipping_incl_tax,warehouse_picklist_time,warehouse_approved_time,telesales_colour_code,telesales_colour_summ,telesales_method_code,telesales_admin_username,partbill_shippingcost_cost,partbill_shippingcost_vat,partbill_packaging_cost,partbill_packaging_vat,partbill_region_code,partbill_region_name,partbill_call_flag,partbill_call_cost,partbill_precverif_cost,partbill_fulfillment_flag,partbill_fulfillment_cost,partbill_transaction_cost,coupon_extraverificationcode,old_order_id,reorder_on_flag,reorder_profile_id,automatic_reorder,presc_verification_method,referafriend_code,referafriend_referer,referafriend_ship_notif,shipment_part_first,shipment_complete_sent,reminder_follow_sent,reminder_sent,order_manufacturers,pvx_allow_partship,pvx_sync_enabled,edi_comment,coupon_rule_name,gw_id,gw_allow_gift_receipt,gw_add_card,gw_base_price,gw_price,gw_items_base_price,gw_items_price,gw_card_base_price,gw_card_price,gw_base_tax_amount,gw_tax_amount,gw_items_base_tax_amount,gw_items_tax_amount,gw_card_base_tax_amount,gw_card_tax_amount,gw_base_price_invoiced,gw_price_invoiced,gw_items_base_price_invoiced,gw_items_price_invoiced,gw_card_base_price_invoiced,gw_card_price_invoiced,gw_base_tax_amount_invoiced,gw_tax_amount_invoiced,gw_items_base_tax_invoiced,gw_items_tax_invoiced,gw_card_base_tax_invoiced,gw_card_tax_invoiced,gw_base_price_refunded,gw_price_refunded,gw_items_base_price_refunded,gw_items_price_refunded,gw_card_base_price_refunded,gw_card_price_refunded,gw_base_tax_amount_refunded,gw_tax_amount_refunded,gw_items_base_tax_refunded,gw_items_tax_refunded,gw_card_base_tax_refunded,gw_card_tax_refunded,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.state,
    a.status,a.prescription_verification_type,a.coupon_code,a.protect_code,a.shipping_description,a.is_virtual,a.store_id,a.customer_id,a.base_discount_amount,a.base_discount_canceled,a.base_discount_invoiced,a.base_discount_refunded,a.base_grand_total,a.base_shipping_amount,a.base_shipping_canceled,a.base_shipping_invoiced,a.base_shipping_refunded,a.base_shipping_tax_amount,a.base_shipping_tax_refunded,a.base_subtotal,a.base_subtotal_canceled,a.base_subtotal_invoiced,a.base_subtotal_refunded,a.base_tax_amount,a.base_tax_canceled,a.base_tax_invoiced,a.base_tax_refunded,a.base_to_global_rate,a.base_to_order_rate,a.base_total_canceled,a.base_total_invoiced,a.base_total_invoiced_cost,a.base_total_offline_refunded,a.base_total_online_refunded,a.base_total_paid,a.base_total_qty_ordered,a.base_total_refunded,a.discount_amount,a.discount_canceled,a.discount_invoiced,a.discount_refunded,a.grand_total,a.shipping_amount,a.shipping_canceled,a.shipping_invoiced,a.shipping_refunded,a.shipping_tax_amount,a.shipping_tax_refunded,a.store_to_base_rate,a.store_to_order_rate,a.subtotal,a.subtotal_canceled,a.subtotal_invoiced,a.subtotal_refunded,a.tax_amount,a.tax_canceled,a.tax_invoiced,a.tax_refunded,a.total_canceled,a.total_invoiced,a.total_offline_refunded,a.total_online_refunded,a.total_paid,a.total_qty_ordered,a.total_refunded,a.can_ship_partially,a.can_ship_partially_item,a.customer_is_guest,a.customer_note_notify,a.billing_address_id,a.customer_group_id,a.edit_increment,a.email_sent,a.forced_shipment_with_invoice,a.gift_message_id,a.payment_auth_expiration,a.paypal_ipn_customer_notified,a.quote_address_id,a.quote_id,a.shipping_address_id,a.adjustment_negative,a.adjustment_positive,a.base_adjustment_negative,a.base_adjustment_positive,a.base_shipping_discount_amount,a.base_subtotal_incl_tax,a.base_total_due,a.payment_authorization_amount,a.shipping_discount_amount,a.subtotal_incl_tax,a.total_due,a.weight,a.customer_dob,a.increment_id,a.applied_rule_ids,a.base_currency_code,a.customer_email,a.customer_firstname,a.customer_lastname,a.customer_middlename,a.customer_prefix,a.customer_suffix,a.customer_taxvat,a.discount_description,a.ext_customer_id,a.ext_order_id,a.global_currency_code,a.hold_before_state,a.hold_before_status,a.order_currency_code,a.original_increment_id,a.relation_child_id,a.relation_child_real_id,a.relation_parent_id,a.relation_parent_real_id,a.remote_ip,a.shipping_method,a.store_currency_code,a.store_name,a.x_forwarded_for,a.customer_note,a.created_at,a.updated_at,a.total_item_count,a.customer_gender,a.affilBatch,a.affilCode,a.affilUserRef,a.base_custbalance_amount,a.base_customer_balance_amount,a.base_customer_balance_invoiced,a.base_customer_balance_refunded,a.bs_customer_bal_total_refunded,a.base_gift_cards_amount,a.base_gift_cards_invoiced,a.base_gift_cards_refunded,a.base_reward_currency_amount,a.base_rwrd_crrncy_amt_invoiced,a.base_rwrd_crrncy_amnt_refnded,a.custbalance_amount,a.customer_balance_amount,a.customer_balance_invoiced,a.customer_balance_refunded,a.customer_bal_total_refunded,a.gift_cards,a.gift_cards_amount,a.gift_cards_invoiced,a.gift_cards_refunded,a.is_multi_payment,a.picklist_printed,a.postoptics_auto_verification,a.postoptics_colour_code,a.postoptics_colour_summ,a.postoptics_source,a.reminder_date,a.reminder_mobile,a.reminder_period,a.reminder_presc,a.reminder_type,a.reward_currency_amount,a.rwrd_currency_amount_invoiced,a.rwrd_crrncy_amnt_refunded,a.reward_points_balance,a.reward_points_balance_refunded,a.reward_points_balance_refund,a.reward_salesrule_points,a.shipping_inwarehouse,a.hidden_tax_amount,a.base_hidden_tax_amount,a.shipping_hidden_tax_amount,a.base_shipping_hidden_tax_amnt,a.hidden_tax_invoiced,a.base_hidden_tax_invoiced,a.hidden_tax_refunded,a.base_hidden_tax_refunded,a.shipping_incl_tax,a.base_shipping_incl_tax,a.warehouse_picklist_time,a.warehouse_approved_time,a.telesales_colour_code,a.telesales_colour_summ,a.telesales_method_code,a.telesales_admin_username,a.partbill_shippingcost_cost,a.partbill_shippingcost_vat,a.partbill_packaging_cost,a.partbill_packaging_vat,a.partbill_region_code,a.partbill_region_name,a.partbill_call_flag,a.partbill_call_cost,a.partbill_precverif_cost,a.partbill_fulfillment_flag,a.partbill_fulfillment_cost,a.partbill_transaction_cost,a.coupon_extraverificationcode,a.old_order_id,a.reorder_on_flag,a.reorder_profile_id,a.automatic_reorder,a.presc_verification_method,a.referafriend_code,a.referafriend_referer,a.referafriend_ship_notif,a.shipment_part_first,a.shipment_complete_sent,a.reminder_follow_sent,a.reminder_sent,a.order_manufacturers,a.pvx_allow_partship,a.pvx_sync_enabled,a.edi_comment,a.coupon_rule_name,a.gw_id,a.gw_allow_gift_receipt,a.gw_add_card,a.gw_base_price,a.gw_price,a.gw_items_base_price,a.gw_items_price,a.gw_card_base_price,a.gw_card_price,a.gw_base_tax_amount,a.gw_tax_amount,a.gw_items_base_tax_amount,a.gw_items_tax_amount,a.gw_card_base_tax_amount,a.gw_card_tax_amount,a.gw_base_price_invoiced,a.gw_price_invoiced,a.gw_items_base_price_invoiced,a.gw_items_price_invoiced,a.gw_card_base_price_invoiced,a.gw_card_price_invoiced,a.gw_base_tax_amount_invoiced,a.gw_tax_amount_invoiced,a.gw_items_base_tax_invoiced,a.gw_items_tax_invoiced,a.gw_card_base_tax_invoiced,a.gw_card_tax_invoiced,a.gw_base_price_refunded,a.gw_price_refunded,a.gw_items_base_price_refunded,a.gw_items_price_refunded,a.gw_card_base_price_refunded,a.gw_card_price_refunded,a.gw_base_tax_amount_refunded,a.gw_tax_amount_refunded,a.gw_items_base_tax_refunded,a.gw_items_tax_refunded,a.gw_card_base_tax_refunded,a.gw_card_tax_refunded,
    0,0,0,null,0   
  FROM 
      magento01.sales_flat_order a 
    LEFT JOIN 
      dw_order_headers b ON a.entity_id=b.order_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
REPLACE INTO dw_order_lines (item_id,order_id,
  parent_item_id,quote_item_id,store_id,created_at,updated_at,product_id,product_type,product_options,weight,is_virtual,sku,name,description,applied_rule_ids,additional_data,free_shipping,is_qty_decimal,no_discount,qty_backordered,qty_canceled,qty_invoiced,qty_ordered,qty_refunded,qty_shipped,base_cost,price,base_price,original_price,base_original_price,tax_percent,tax_amount,base_tax_amount,tax_invoiced,base_tax_invoiced,discount_percent,discount_amount,base_discount_amount,discount_invoiced,base_discount_invoiced,amount_refunded,base_amount_refunded,row_total,base_row_total,row_invoiced,base_row_invoiced,row_weight,gift_message_id,gift_message_available,base_tax_before_discount,tax_before_discount,weee_tax_applied,weee_tax_applied_amount,weee_tax_applied_row_amount,base_weee_tax_applied_amount,base_weee_tax_applied_row_amnt,weee_tax_disposition,weee_tax_row_disposition,base_weee_tax_disposition,base_weee_tax_row_disposition,ext_order_item_id,locked_do_invoice,locked_do_ship,price_incl_tax,base_price_incl_tax,row_total_incl_tax,base_row_total_incl_tax,event_id,hidden_tax_amount,base_hidden_tax_amount,hidden_tax_invoiced,base_hidden_tax_invoiced,hidden_tax_refunded,base_hidden_tax_refunded,is_nominal,tax_canceled,hidden_tax_canceled,tax_refunded,giftregistry_item_id,promotion_rule,promotion_rule2,is_lens,lens_group_id,lens_group_eye,base_tax_refunded,discount_refunded,base_discount_refunded,gw_id,gw_base_price,gw_price,gw_base_tax_amount,gw_tax_amount,gw_base_price_invoiced,gw_price_invoiced,gw_base_tax_amount_invoiced,gw_tax_amount_invoiced,gw_base_price_refunded,gw_price_refunded,gw_base_tax_amount_refunded,gw_tax_amount_refunded,qty_returned,
  dw_att,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.item_id,a.order_id,
    a.parent_item_id,a.quote_item_id,a.store_id,a.created_at,a.updated_at,a.product_id,a.product_type,a.product_options,a.weight,a.is_virtual,a.sku,a.name,a.description,a.applied_rule_ids,a.additional_data,a.free_shipping,a.is_qty_decimal,a.no_discount,a.qty_backordered,a.qty_canceled,a.qty_invoiced,a.qty_ordered,a.qty_refunded,a.qty_shipped,a.base_cost,a.price,a.base_price,a.original_price,a.base_original_price,a.tax_percent,a.tax_amount,a.base_tax_amount,a.tax_invoiced,a.base_tax_invoiced,a.discount_percent,a.discount_amount,a.base_discount_amount,a.discount_invoiced,a.base_discount_invoiced,a.amount_refunded,a.base_amount_refunded,a.row_total,a.base_row_total,a.row_invoiced,a.base_row_invoiced,a.row_weight,a.gift_message_id,a.gift_message_available,a.base_tax_before_discount,a.tax_before_discount,a.weee_tax_applied,a.weee_tax_applied_amount,a.weee_tax_applied_row_amount,a.base_weee_tax_applied_amount,a.base_weee_tax_applied_row_amnt,a.weee_tax_disposition,a.weee_tax_row_disposition,a.base_weee_tax_disposition,a.base_weee_tax_row_disposition,a.ext_order_item_id,a.locked_do_invoice,a.locked_do_ship,a.price_incl_tax,a.base_price_incl_tax,a.row_total_incl_tax,a.base_row_total_incl_tax,a.event_id,a.hidden_tax_amount,a.base_hidden_tax_amount,a.hidden_tax_invoiced,a.base_hidden_tax_invoiced,a.hidden_tax_refunded,a.base_hidden_tax_refunded,a.is_nominal,a.tax_canceled,a.hidden_tax_canceled,a.tax_refunded,a.giftregistry_item_id,a.promotion_rule,a.promotion_rule2,a.is_lens,a.lens_group_id,a.lens_group_eye,a.base_tax_refunded,a.discount_refunded,a.base_discount_refunded,a.gw_id,a.gw_base_price,a.gw_price,a.gw_base_tax_amount,a.gw_tax_amount,a.gw_base_price_invoiced,a.gw_price_invoiced,a.gw_base_tax_amount_invoiced,a.gw_tax_amount_invoiced,a.gw_base_price_refunded,a.gw_price_refunded,a.gw_base_tax_amount_refunded,a.gw_tax_amount_refunded,a.qty_returned,
    0,0,null,0     
  FROM 
      magento01.sales_flat_order_item a 
    LEFT JOIN 
      dw_order_headers b ON a.order_id = b.order_id 
  WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);
  
REPLACE INTO dw_order_address (address_id,order_id,
  customer_address_id,quote_address_id,region_id,customer_id,fax,region,postcode,lastname,street,city,email,telephone,country_id,firstname,address_type,prefix,middlename,suffix,company,giftregistry_item_id,vat_id,vat_is_valid,vat_request_id,vat_request_date,vat_request_success,dw_att,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.parent_id,
    a.customer_address_id,a.quote_address_id,a.region_id,a.customer_id,a.fax,a.region,a.postcode,a.lastname,a.street,a.city,a.email,a.telephone,a.country_id,a.firstname,a.address_type,a.prefix,a.middlename,a.suffix,a.company,a.giftregistry_item_id,a.vat_id,a.vat_is_valid,a.vat_request_id,a.vat_request_date,a.vat_request_success, 
    0,0,null,0   
  FROM 
      magento01.sales_flat_order_address a 
    LEFT JOIN 
      dw_order_headers b ON a.parent_id = b.order_id 
  WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);
  
  
-- Categories  
REPLACE INTO dw_categories (category_id,entity_type_id,
  attribute_set_id,parent_category_id,created_at,updated_at,path,position,level,children_count,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.entity_type_id,
    a.attribute_set_id,a.parent_id,a.created_at,a.updated_at,a.path,a.position,a.level,a.children_count,
    0,0,0,null,0 
  FROM 
      magento01.catalog_category_entity a 
    LEFT JOIN 
      dw_categories b ON a.entity_id=b.category_id WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
UPDATE dw_categories a, magento01.catalog_category_entity_text b 
SET a.description = b.value 
WHERE a.category_id = b.entity_id AND b.attribute_id = 34 AND dw_att=0 AND b.store_id =0;
  
CREATE TABLE IF NOT EXISTS dw_categories_all_stores 
  SELECT a.* 
  FROM dw_categories a;
ALTER TABLE dw_categories_all_stores ADD COLUMN store_id INT DEFAULT 0;
  
INSERT INTO dw_categories_all_stores 
  SELECT a.*, 19 FROM dw_categories a;

UPDATE dw_categories_all_stores a, magento01.catalog_category_entity_text b 
SET a.description = b.value 
WHERE a.category_id = b.entity_id AND b.attribute_id = 34 AND dw_att=0 AND b.store_id =a.store_id;
  

-- Products
REPLACE INTO dw_products (product_id,entity_type_id,
  attribute_set_id,type_id,sku,created_at,updated_at,has_options,required_options,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.entity_type_id,
    a.attribute_set_id,a.type_id,a.sku,a.created_at,a.updated_at,a.has_options,a.required_options,
    0,0,0,null,0 
  FROM 
      magento01.catalog_product_entity a 
    LEFT JOIN 
      dw_products b ON a.entity_id=b.product_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);  
  
UPDATE dw_products a, magento01.catalog_product_entity_text b 
SET a.description = b.value 
WHERE a.product_id = b.entity_id AND b.attribute_id = 57 AND dw_att=0 AND b.store_id =0;

CREATE TABLE IF NOT EXISTS dw_products_all_stores 
  SELECT a.* 
  FROM dw_products a;
ALTER TABLE dw_products_all_stores ADD COLUMN store_id INT DEFAULT 0

INSERT INTO dw_products_all_stores 
  SELECT a.*, 19 FROM dw_products a;

UPDATE dw_products_all_stores a, magento01.catalog_product_entity_text b 
SET a.description = b.value 
WHERE a.product_id = b.entity_id AND b.attribute_id = 57 AND dw_att=0 AND b.store_id =a.store_id;

-- Customers
REPLACE INTO dw_customers (customer_id,entity_type_id,
  attribute_set_id,website_id,email,group_id,increment_id,store_id,created_at,updated_at,is_active,website_group_id,disable_auto_group_change,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.entity_type_id,
    a.attribute_set_id,a.website_id,a.email,a.group_id,a.increment_id,a.store_id,a.created_at,a.updated_at,a.is_active,a.website_group_id,a.disable_auto_group_change,
    0,0,0,null,0 
  FROM 
      magento01.customer_entity a 
    LEFT JOIN 
      dw_customers b ON a.entity_id=b.customer_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
UPDATE dw_customers a, magento01.customer_entity_varchar b 
SET a.lastname = b.value 
WHERE a.customer_id = b.entity_id AND b.attribute_id = 7 AND dw_att=0;

-- Customer Address
REPLACE INTO dw_customer_addresses (address_id,entity_type_id,
  attribute_set_id,address_no,customer_id,created_at,updated_at,is_active,
  dw_att,dw_type,dw_synced,dw_synced_at,dw_proc) 
  
  SELECT a.entity_id,a.entity_type_id,
    a.attribute_set_id,a.increment_id,a.parent_id,a.created_at,a.updated_at,a.is_active,
    0,0,0,null,0 
  FROM 
      magento01.customer_address_entity a 
    LEFT JOIN 
      dw_customer_addresses b ON a.entity_id=b.address_id 
  WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
  
UPDATE dw_customer_addresses a, magento01.customer_address_entity_text b 
SET a.street = b.value 
WHERE a.address_id = b.entity_id AND b.attribute_id = 23 AND dw_att=0;  