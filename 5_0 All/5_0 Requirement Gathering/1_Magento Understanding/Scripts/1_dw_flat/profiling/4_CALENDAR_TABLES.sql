
-- dw_invoice_calender_dates, dw_invoice_calender_dates_staging

select invoice_id, order_id, 
  created_at, order_created_at, length_of_time_to_invoice
from dw_invoice_calender_dates
order by order_id
limit 1000; 

select invoice_id, order_id, 
  created_at, order_created_at, length_of_time_to_invoice
from dw_invoice_calender_dates_staging
order by order_id
limit 1000;

-- dw_shipment_calender_dates, dw_shipment_calender_dates_staging

select shipment_id, order_id, 
  created_at, order_created_at, invoiced_at, length_of_time_to_first_invoice, length_of_time_from_first_invoice_to_this_shipment
from dw_shipment_calender_dates
limit 1000 

select shipment_id, order_id, 
  created_at, order_created_at, invoiced_at, length_of_time_to_first_invoice, length_of_time_from_first_invoice_to_this_shipment
from dw_shipment_calender_dates_staging
limit 1000

-- dw_qty_backordered

select order_line_id, shipment_item_id, qty_prior
from dw_qty_backordered
limit 1000