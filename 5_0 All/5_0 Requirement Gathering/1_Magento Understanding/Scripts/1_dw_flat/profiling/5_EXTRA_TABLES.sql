
-- max_reorder_quote_payment
select quote_id, payment_id
from max_reorder_quote_payment
limit 1000

-- order_status_history_agg
select order_id, status, status_time
from order_status_history_agg
limit 1000

-- edi_stock_item_agg
select product_id, product_code, 
  bc, di, po, cy, ax, do, ad, co
from edi_stock_item_agg
limit 1000

-- first_inv, first_inv_staging, first_ship, first_ship_staging
select order_id, invoice_id, invoiced_at
from first_inv
order by order_id
limit 1000
select order_id, invoice_id, invoiced_at
from first_inv_staging
order by order_id
limit 1000

select order_id, shipped_at
from first_ship
order by order_id
limit 1000
select order_id, shipped_at
from first_ship_staging
order by order_id
limit 1000

-- last_full_ship, last_full_ship_staging

select order_id, shipped_at
from last_full_ship
order by order_id
limit 1000
select order_id, shipped_at
from last_full_ship_staging
order by order_id
limit 1000

-- last_full_payment, last_full_payment_staging
select order_id, payment_time
from last_full_payment
order by order_id
limit 1000
select order_id, payment_time
from last_full_payment_staging
order by order_id
limit 1000

-- dw_category_path, dw_product_category_map, dw_product_category_flat
select category_id, 
  name1, name2, name3, name4, name5, name6, name7, name8
from dw_category_path
order by category_id

select product_id, name, 
  modality_name, feature_name, type_name, 
  product_type_name, 
  telesales_only
from dw_product_category_map
order by product_id

select product_id, category_id
from dw_product_category_flat

-- dw_product_price_qtys
select product_id, store_id, first_tier_qty, multibuy_qty
from dw_product_price_qtys
order by product_id, store_id

-- customer_first_order, customer_first_order_staging
select customer_id, order_id
from customer_first_order
order by customer_id
limit 1000

select customer_id, order_id
from customer_first_order_staging
order by customer_id
limit 1000
