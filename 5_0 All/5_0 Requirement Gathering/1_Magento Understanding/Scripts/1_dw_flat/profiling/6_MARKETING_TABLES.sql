
-- dw_order_headers_marketing, dw_order_headers_marketing_staging

select order_id, order_no, created_at, customer_id, store_id, 
  status, prev_status, 
  order_lifecycle, order_lifecycle_cancel, order_lifecycle_all, 
  order_rank_all, order_rank_cancel, order_rank_cancel_only, order_rank, 
  source
from dw_order_headers_marketing
order by order_id
limit 1000

  select order_id, order_no, created_at, customer_id, store_id, status, source
  from dw_order_headers_marketing
  order by order_id
  limit 1000

  select order_id, order_rank_all, order_rank_cancel, order_rank_cancel_only
  from dw_order_headers_marketing
  order by order_id
  limit 1000

  select order_id, order_no, created_at, customer_id, status, prev_status
  from dw_order_headers_marketing
  order by customer_id, created_at, order_id
  limit 1000
 
  select order_id, order_lifecycle, order_lifecycle_cancel, order_lifecycle_all
  from dw_order_headers_marketing
  order by order_id
  limit 1000
  
select order_id, order_no, created_at, customer_id, store_id, 
  status, prev_status, 
  order_lifecycle, order_lifecycle_cancel, order_lifecycle_all, 
  order_rank_all, order_rank_cancel, order_rank_cancel_only, order_rank, 
  source
from dw_order_headers_marketing_staging
order by order_id
limit 1000

select customer_id, first_order_date, last_order_date, no_of_orders
from dw_order_headers_marketing_agg
limit 1000

-- order_rank_all, order_rank_cancel, order_rank_cancel_only
select order_id, order_no, customer_id, store_id, source, rank
from order_rank_all
order by order_id
limit 1000

select order_id, order_no, customer_id, store_id, source, rank
from order_rank_cancel
order by order_id
limit 1000

select order_id, order_no, customer_id, store_id, source, rank
from order_rank_cancel_only
order by order_id
limit 1000

-- dw_bis_map_generic
select bis_source_code, channel
from dw_bis_map_generic
order by channel, bis_source_code

-- dw_order_channel, dw_order_channel_staging
select order_id, order_no, 
  bis_channel, raf_channel, ga_channel, coupon_channel, 
  channel
from dw_order_channel
order by order_id
limit 1000
select order_id, order_no, 
  bis_channel, raf_channel, ga_channel, coupon_channel, 
  channel
from dw_order_channel_staging
order by order_id
limit 1000

-- dw_full_creditmemo
select order_id, creditmemo_id
from dw_full_creditmemo
order by order_id
limit 1000

-- dw_entity_header_rank, dw_entity_header_rank_all, dw_entity_header_rank_seq
select order_id, document_id, document_type, created_at, customer_id, store_id, source, document_seq
from dw_entity_header_rank
order by order_id, created_at
limit 1000

select 
  order_id, document_id, document_type, created_at, customer_id, store_id, source, document_seq, 
  order_rank, 
  prev_document_type, next_document_type, prev_full_credit, full_credit
from dw_entity_header_rank_all
order by customer_id, created_at, document_seq
limit 1000

select order_id, document_id, document_type, created_at, customer_id, store_id, source, document_seq, 
  order_rank, 
  prev_document_type, next_document_type, prev_full_credit, full_credit, 
  order_seq_plus, order_seq_minus, order_seq
from dw_entity_header_rank_seq
order by customer_id, created_at, document_seq
limit 1000

-- dw_customer_total_orders
select customer_id, 
  min_order_seq_minus, max_order_seq_plus, no_of_orders
from dw_customer_total_orders
order by customer_id
limit 1000
