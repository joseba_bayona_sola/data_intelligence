
-- dw_core_config_date

select config_id, store_id, path, value 
from dw_core_config_data
where path in ('prescription/settings/enable', 'currency/options/base', 'surveysweb/surveys/active')
order by path, store_id;

-- dw_conf_presc_required
select config_id, store_id, path, value 
from dw_conf_presc_required
order by store_id

-- dw_current_vat
select store_id, prof_fee_rate, vat_rate, shipping_cost, 
  base_currency, base_to_global_rate
from dw_current_vat
order by store_id

-- dw_product_prices
select *
from dw_product_prices
order by product_id, store_name

  select price_id, store_id, store_name, product_id,
    qty, pack_size,
    local_unit_price_inc_vat, local_qty_price_inc_vat, local_pack_price_inc_vat, local_product_cost, local_total_cost  
  from dw_product_prices
  order by product_id, store_name

  select price_id, store_id, store_name, product_id,
    local_prof_fee, prof_fee_percent, 
    store_currency_code, local_to_global_rate
  from dw_product_prices
  order by product_id, store_name
  
  