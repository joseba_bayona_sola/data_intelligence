
-- Headers VAT

select *
from order_headers_vat
limit 1000; 

select country_type, country_id, count(*)
from order_headers_vat
group by country_type, country_id
order by country_type, country_id
limit 1000; 

select order_id, has_lens, max_row_total, min_row_total, total_weight, total_cost, total_qty
from order_headers_vat
limit 1000; 

select order_id, rollup_has_lens, rollup_total, local_total_inc_vat
from order_headers_vat
-- where rollup_total <> local_total_inc_vat
limit 1000; 

select order_id, rollup_total, vat_logic, 
  prof_fee_percent, calculated_prof_fee, prof_fee, 
  vat_rate, calculated_vat, local_total_vat, 
  vat_percent, vat_percent_before_prof_fee
from order_headers_vat
limit 1000; 

  select vat_logic, count(*)
  from order_headers_vat
  group by vat_logic
  order by vat_logic
  limit 1000; 

-- -----------------------------------

-- invoice_headers_rank, shipment_headers_rank, creditmemo_headers_rank

select invoice_id, order_id, rank
from invoice_headers_rank
where order_id = 4146462
order by order_id, invoice_id
limit 1000

  select rank, count(*)
  from invoice_headers_rank
  group by rank
  order by rank

select shipment_id, order_id, rank
from shipment_headers_rank
order by order_id, shipment_id
limit 1000

  select rank, count(*)
  from shipment_headers_rank
  group by rank
  order by rank
  
select creditmemo_id, order_id, rank
from creditmemo_headers_rank
where order_id = 3251379
order by order_id, creditmemo_id
limit 1000

  select rank, count(*)
  from creditmemo_headers_rank
  group by rank
  order by rank
  
-- -----------------------------------

-- order_headers_vat_rollup, invoice_headers_vat_rollup, creditmemo_headers_vat_rollup

select order_id, total_inc_vat, has_lens
from order_headers_vat_rollup
limit 1000

select invoice_id, total_inc_vat, has_lens
from invoice_headers_vat_rollup
limit 1000

select creditmemo_id, total_inc_vat, has_lens
from creditmemo_headers_vat_rollup
limit 1000

-- -----------------------------------

-- order_lines_allocation_rate, invoice_lines_allocation_rate, creditmemo_lines_allocation_rate

select item_id, order_id, rate
from order_lines_allocation_rate
limit 1000
  
  select order_id, count(*), sum(rate)
  from order_lines_allocation_rate
  group by order_id
  order by order_id
  limit 1000
  
 -- ------------------------------------------------------------------------------------------------- 
 
 -- creditmemo_lines_with_shipment, creditmemo_shipment_headers_vat, creditmemo_shipment_lines_vat
 
 select item_id, shipment_id, shipment_item_id
 from creditmemo_lines_with_shipment
 order by shipment_id, shipment_item_id
 limit 1000
 
 select *
 from creditmemo_shipment_headers_vat
 limit 1000 
 
 select *
 from creditmemo_shipment_lines_vat
 limit 1000 
 