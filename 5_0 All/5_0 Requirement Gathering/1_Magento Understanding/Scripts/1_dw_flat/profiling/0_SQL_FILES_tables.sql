
-- dw_stores
select store_id, store_name, website_group, store_type, visible
from dw_stores
order by store_id;

-- dw_stores_map
select store_id, store_name, website_group, store_type, visible
from dw_stores_map
order by store_id;

-- dw_calendar
select *
from dw_calendar;

select min(Day_Timestamp), max(Day_Timestamp), count(*)
from dw_calendar;

-- dw_countries
select country_name, country_code, country_type, country_zone, country_continent, country_state
from dw_countries
order by country_code;

  select country_type, count(*)
  from dw_countries
  group by country_type
  order by country_type;

  select country_zone, count(*)
  from dw_countries
  group by country_zone
  order by country_zone;
  
  select country_continent, count(*)
  from dw_countries
  group by country_continent
  order by country_continent;

  select country_state, count(*)
  from dw_countries
  group by country_state
  order by country_state;

-- dw_coupon_code_map
select coupon_code, channel, is_raf
from dw_coupon_code_map
order by channel, coupon_code;

-- dw_bis_source_map
select source_code, channel
from dw_bis_source_map
order by channel, source_code;

-- dw_bis_map_dynamic_types
select prefix, channel
from dw_bis_map_dynamic_types;

-- customer_created_in_map
select customer_created_in, new_customer_created_in
from customer_created_in_map
order by new_customer_created_in, customer_created_in;

-- dw_card_type_map
select card_type, card_type_name
from dw_card_type_map;

-- dw_payment_method_map
select payment_method, payment_method_name
from dw_payment_method_map
order by payment_method_name, payment_method;

-- dw_flat_category
select category_id, category, 
  product_type, product_type_name, 
  modality, modality_name, 
  type, type_name, 
  feature, feature_name, 
  promotional_product, telesales_only
from dw_flat_category
order by product_type, category, modality, type, feature;

  select category, count(*)
  from dw_flat_category
  group by category
  order by category;

  select product_type, product_type_name, count(*)
  from dw_flat_category
  group by product_type, product_type_name
  order by product_type, product_type_name;
  
  select modality, modality_name, count(*)
  from dw_flat_category
  group by modality, modality_name
  order by modality, modality_name;
  
  select type, type_name, count(*)
  from dw_flat_category
  group by type, type_name
  order by type, type_name;
  
  select feature, feature_name, count(*)
  from dw_flat_category
  group by feature, feature_name
  order by feature, feature_name; 
  
 -- dw_glasses_products 
select product_id, name
from dw_glasses_products
