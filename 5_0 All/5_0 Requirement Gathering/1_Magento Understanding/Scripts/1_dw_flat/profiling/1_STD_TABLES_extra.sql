
select shipment_id
from missing_shipment_items;

-- ---------------------------------------------

select invoice_id, order_id, store_id, 
  item_id, order_line_id, 
  product_id, first_inv
from dw_invoice_lines_everclear_empty
order by order_id, invoice_id, item_id;

  select product_id, count(*)
  from dw_invoice_lines_everclear_empty
  group by product_id
  order by product_id;
  
select invoice_id, max_pid, min_pid
from dw_invoice_confirm_empty_everclear;

select order_id, first_invoice_id
from dw_invoice_lines_everclear_first_invoice
order by order_id, first_invoice_id;

select invoice_id, order_id, store_id, 
  item_id, order_line_id, 
  product_id, first_inv
from dw_invoice_lines_everclear_empty_fixed
order by order_id, invoice_id, item_id;


-- ---------------------------------------------

select order_id, order_base_grand_total, creditmemo_base_grand_total,
  creditmeno_created_at, 
  creditmemo_base_customer_balance_amount, creditmemo_customer_bal_total_refunded
from dw_store_credit_refunds
