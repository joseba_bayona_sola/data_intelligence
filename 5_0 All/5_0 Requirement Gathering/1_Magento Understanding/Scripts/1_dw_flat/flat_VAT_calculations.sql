/*** VAT is calculated backwards from gross sales amount and then allocated to items by he 
	amount they account for on order VAT rate is decided based on store or the country goods are dispatched to 
	Note that store credit is not included in VAT calculations, shipping is inculuded and taken at the same rate as the rest of the order 
	
	Basic step are
		* Set country information for entity 
		* Add aggregated values from lines that will be need for other calcs
		* Calc all inc_vat totals for entity 
		* Flag Rows by VAT logic based on business rules 
		* Set VAT rate professional fee (VAT exempt amount of the order) 
		* Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate) 
		* Calculate Exc VAT and global values 
		* Allocate calculated values to entity items based on formula to spread values across items 
		
		* Note as shipments don't have any monetary values attached in magento they are calculated differently 
			this is done by using matching values from (order,invoice,creditmemo) */ 
			
-- * Set country information for entity
REPLACE INTO order_headers_vat (order_id, country_type, country_id, entity_order_id) 
	SELECT oh.order_id, 
	MAX( c.country_type ) country_type, MAX(c.country_code) country_code, 
	MAX(oh.order_id) entity_order_id 
FROM 
		dw_order_headers oh 
	INNER JOIN 
		dw_order_headers doh ON doh.order_id = oh.order_id 
	INNER JOIN 
		dw_updated_customers duc ON duc.customer_id = doh.customer_id 
	LEFT JOIN 
		dw_order_address oa ON oa.address_id = oh.shipping_address_id 
	INNER JOIN 
		dw_countries c ON c.country_code = oa.country_id GROUP BY oh.order_id;


-- * Add aggregated values from lines that will be need for other calcs
UPDATE order_headers_vat ov, 
	(SELECT ol.order_id, IFNULL(MAX(p.is_lens), 0) is_lens, 
		ROUND(MAX(ol.base_row_total), 2) max_row_total, ROUND(MIN(ol.base_row_total), 2) min_row_total, 
		IFNULL(SUM(ol2.weight * ol.qty_ordered), 0) total_weight, 
		IFNULL(SUM(ol.base_cost * ol.qty_ordered), 0) total_cost, 
		IFNULL(SUM(ol.qty_ordered), 0) total_qty 
	FROM 
			dw_order_lines ol 
		INNER JOIN 
			dw_order_headers oh ON oh.order_id = ol.order_id 
		INNER JOIN 
			dw_order_headers doh ON oh.order_id = doh.order_id 
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = doh.customer_id 
		LEFT JOIN 
			dw_products p ON p.product_id = ol.product_id 
		LEFT JOIN 
			dw_order_lines ol2 ON ol2.item_id = ol.item_id 
	GROUP BY order_id) sub 
SET 
	ov.has_lens = sub.is_lens, 
	ov.total_weight = sub.total_weight, 
	ov.total_cost = sub.total_cost, 
	ov.total_qty = sub.total_qty, 
	ov.max_row_total = sub.max_row_total, ov.min_row_total = sub.min_row_total 
WHERE ov.order_id = sub.order_id AND ov.vat_done = 0;

-- * Calc all inc_vat totals for entity 
UPDATE order_headers_vat ov, dw_order_headers oh, dw_order_headers ord 
SET 
	local_subtotal_inc_vat = oh.base_subtotal, 
	local_shipping_inc_vat = oh.base_shipping_amount, 
	local_discount_inc_vat = oh.base_discount_amount, 
	local_store_credit_inc_vat = ABS(IFNULL(oh.base_customer_balance_amount, 0)), 
	local_adjustment_inc_vat = 0 , 
	local_total_inc_vat = ((oh.base_subtotal) + (oh.base_shipping_amount) - (ABS(oh.base_discount_amount)) - (IFNULL(oh.base_customer_balance_amount, 0))), 
	magento_grand_total = oh.base_grand_total, 
	local_subtotal_cost = ov.total_cost * (1/oh.base_to_global_rate), local_shipping_cost = IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0), 
	local_total_cost = (ov.total_cost * (1/oh.base_to_global_rate)) + IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0), 
	ov.order_date = ord.created_at 
WHERE oh.order_id = ov.order_id AND ord.order_id = oh.order_id AND vat_done = 0

-- * Use rank to deal with any part entities and collect rollup value 
	-- (only applied to orders that use a sliding scale professional fee where this a percentage this has no effect 

DROP TABLE IF EXISTS order_headers_vat_rollup

CREATE TABLE IF NOT EXISTS order_headers_vat_rollup (order_id int not null, total_inc_vat decimal(34, 2), has_lens int, PRIMARY KEY(order_id) ) 
	SELECT ohv1.order_id, SUM(ohv2.local_total_inc_vat) total_inc_vat, MAX(ohv2.has_lens) has_lens 
	FROM 
			order_headers_vat ohv1 
		INNER JOIN 
			order_headers_vat ohv2 ON ohv2.entity_order_id = ohv1.entity_order_id AND ohv2.order_id <= ohv1.order_id 
	WHERE ohv1.vat_done = 0 
	GROUP BY ohv1.order_id

UPDATE order_headers_vat ov, order_headers_vat_rollup ovr 
SET ov.rollup_total = ovr.total_inc_vat, ov.rollup_has_lens = ovr.has_lens 
WHERE ov.order_id = ovr.order_id

-- * Flag Rows by VAT logic based on business rules
UPDATE order_headers_vat ov, dw_order_headers oh 
SET vat_logic = 'PO-2010.01.01-2011-01-03' 
WHERE ov.order_id = oh.order_id 
	AND cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' 
	AND cast(ov.order_date as date) <= '2011.08.31' 
	AND vat_logic IS NULL AND oh.store_id = 1

UPDATE order_headers_vat ov, dw_order_headers oh 
SET vat_logic = 'GO-ES-CURENT' 
WHERE ov.order_id = oh.order_id 
	AND cast(ov.order_date as date) BETWEEN '2013.01.01' AND '2100.01.01' 
	AND country_id = 'ES' 
	AND vat_logic IS NULL AND oh.store_id NOT IN (11, 19)

UPDATE order_headers_vat ov, dw_order_headers oh 
SET vat_logic = 'GO-GROUP' 
WHERE ov.order_id = oh.order_id 
	AND vat_logic IS NULL AND oh.store_id NOT IN (11, 19)

-- * Set VAT rate professional fee (VAT exempt amount of the order) 
UPDATE order_headers_vat ov 
SET 
	prof_fee_percent = 0.18, 
	calculated_prof_fee = rollup_total * 0.18, 
	vat_rate = 1.10 
WHERE vat_logic = 'GO-ES-CURENT' AND vat_done =0

UPDATE order_headers_vat ov 
SET 
	prof_fee_percent = 0.18, 
	calculated_prof_fee = rollup_total * 0.18, 
	vat_rate = 1.20 
WHERE vat_logic = 'GO-GROUP' AND vat_done =0

-- * Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate)
UPDATE order_headers_vat ov 
SET calculated_vat = (rollup_total-calculated_prof_fee) - ((rollup_total-calculated_prof_fee) / vat_rate) 
WHERE vat_logic NOT IN ('GO-IE-2011.11.01','GO-IE-2012.01.01-2012.10.31') AND vat_logic != 'NON_EU' AND vat_done =0;

UPDATE order_headers_vat 
SET local_total_vat = calculated_vat 
WHERE vat_done = 0;
			
UPDATE order_headers_vat 
SET prof_fee = calculated_prof_fee 
WHERE vat_done = 0;			

-- * Calculate Exc VAT and global values 
UPDATE order_headers_vat ov 
SET 
	local_subtotal_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_subtotal_inc_vat = 0 THEN 0 
		ELSE ((local_subtotal_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0), 
	local_shipping_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_shipping_inc_vat = 0 THEN 0 
		ELSE ((local_shipping_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0), 
	local_discount_vat = IFNULL(CASE WHEN local_discount_inc_vat = 0 OR local_discount_inc_vat = 0 THEN 0 
		ELSE (-ABS((ABS(local_discount_inc_vat) / local_total_inc_vat ) * local_total_vat)) END ,0), 
	local_store_credit_vat = IFNULL(CASE WHEN local_store_credit_vat = 0 OR local_store_credit_inc_vat = 0 THEN 0 
		ELSE ((local_store_credit_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0), 
	local_adjustment_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_adjustment_inc_vat = 0 THEN 0 
		ELSE ((local_adjustment_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0) 
WHERE vat_done = 0;

UPDATE order_headers_vat ov 
SET 
	local_subtotal_exc_vat = local_subtotal_inc_vat - local_subtotal_vat, 
	local_total_exc_vat = local_total_inc_vat - local_total_vat, 
	local_shipping_exc_vat = local_shipping_inc_vat - local_shipping_vat, 
	local_discount_exc_vat = -ABS(ABS(local_discount_inc_vat) - ABS(local_discount_vat)), 
	local_store_credit_exc_vat = local_store_credit_inc_vat - local_store_credit_vat, 
	local_adjustment_exc_vat = local_adjustment_inc_vat - local_adjustment_vat 
WHERE vat_done = 0

UPDATE order_headers_vat ov 
SET 
	vat_percent = (local_total_vat) / (local_total_exc_vat), 
	vat_percent_before_prof_fee = vat_rate - 1 , 
	discount_percent = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat + ABS(local_discount_exc_vat))), 
	local_margin_amount = local_total_exc_vat - local_total_cost, 
	local_margin_percent = (local_total_exc_vat - local_total_cost) / local_total_exc_vat 
WHERE vat_done = 0

UPDATE order_headers_vat ov, dw_order_headers oh 
SET 
	global_subtotal_inc_vat = local_subtotal_inc_vat * oh.base_to_global_rate, global_subtotal_vat = local_subtotal_vat * oh.base_to_global_rate, global_subtotal_exc_vat = local_subtotal_exc_vat * oh.base_to_global_rate, 
	global_shipping_inc_vat = local_shipping_inc_vat * oh.base_to_global_rate, global_shipping_vat = local_shipping_vat * oh.base_to_global_rate, global_shipping_exc_vat = local_shipping_exc_vat * oh.base_to_global_rate, 
	global_discount_inc_vat = local_discount_inc_vat * oh.base_to_global_rate, global_discount_vat = local_discount_vat * oh.base_to_global_rate, global_discount_exc_vat = local_discount_exc_vat * oh.base_to_global_rate, 
	global_store_credit_inc_vat = local_store_credit_inc_vat * oh.base_to_global_rate, global_store_credit_vat = local_store_credit_vat * oh.base_to_global_rate, global_store_credit_exc_vat = local_store_credit_exc_vat * oh.base_to_global_rate, 
	global_adjustment_inc_vat = local_adjustment_inc_vat * oh.base_to_global_rate, global_adjustment_vat = local_adjustment_vat * oh.base_to_global_rate, global_adjustment_exc_vat = local_adjustment_exc_vat * oh.base_to_global_rate, 
	global_total_inc_vat = local_total_inc_vat * oh.base_to_global_rate, global_total_vat = local_total_vat * oh.base_to_global_rate, global_total_exc_vat = local_total_exc_vat * oh.base_to_global_rate, 
	
	global_prof_fee = prof_fee * oh.base_to_global_rate, 
	global_subtotal_cost = local_subtotal_cost * oh.base_to_global_rate, global_shipping_cost = local_shipping_cost * oh.base_to_global_rate, 
	global_total_cost = local_total_cost * oh.base_to_global_rate, 
	global_margin_amount = local_margin_amount * oh.base_to_global_rate 
WHERE ov.order_id = oh.order_id AND vat_done = 0

UPDATE order_headers_vat ov, dw_order_headers oh 
SET 
	global_margin_percent = global_margin_amount / global_total_inc_vat 
WHERE ov.order_id = oh.order_id AND vat_done = 0

-- * Allocate calculated values to entity items based on formula to spread values across items 
REPLACE INTO order_lines_vat (item_id, local_price_inc_vat, row_total_inc_vat, actual_cost, qty) 
	SELECT oh.item_id, 
		IFNULL(base_price_incl_tax,price), base_row_total, base_cost * oh.qty_ordered , oh.qty_ordered 
	FROM 
			dw_order_lines oh 
		INNER JOIN 
			dw_order_headers eh ON oh.order_id = eh.order_id 
		INNER JOIN 
			dw_order_headers doh ON eh.order_id = doh.order_id 
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = doh.customer_id;

	-- Fill in allocate percent values (allocate by subtotal when available and qty if not)

REPLACE INTO order_lines_allocation_rate 
	SELECT ol.item_id, ov.order_id, 
		CASE 
			WHEN max_row_total = 0 AND min_row_total = 0 AND ROUND(local_subtotal_inc_vat,2) = 0 THEN ol.qty_ordered/ ov.total_qty 
			WHEN ol.sku = 'ADJUSTMENT' THEN 1 
			ELSE ol.base_row_total / ov.local_subtotal_inc_vat END AS rate 
	FROM 
			order_headers_vat ov 
		INNER JOIN 
			dw_order_lines ol ON ol.order_id = ov.order_id 
		INNER JOIN 
			order_lines_vat lv ON lv.item_id = ol.item_id 
	WHERE lv.vat_done = 0;

UPDATE order_lines_vat olv, dw_order_lines ol, order_headers_vat ov, order_lines_allocation_rate ar 
SET 
	-- olv.local_prof_fee = (ov.prof_fee * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_line_subtotal_vat = (ov.local_subtotal_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_line_subtotal_inc_vat = (ov.local_subtotal_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_line_subtotal_exc_vat = (ov.local_subtotal_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_shipping_inc_vat = (ov.local_shipping_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_shipping_exc_vat = (ov.local_shipping_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_shipping_vat = (ov.local_shipping_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_shipping_cost = (ov.local_shipping_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_subtotal_cost = (ov.local_subtotal_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_total_cost = (ov.local_total_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_discount_inc_vat = (ov.local_discount_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_discount_vat = (ov.local_discount_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_discount_exc_vat = (ov.local_discount_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_store_credit_inc_vat = (ov.local_store_credit_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_store_credit_vat = (ov.local_store_credit_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_store_credit_exc_vat = (ov.local_store_credit_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_adjustment_inc_vat = (ov.local_adjustment_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_adjustment_vat = (ov.local_adjustment_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_adjustment_exc_vat = (ov.local_adjustment_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), 
	-- olv.local_line_total_inc_vat = (ov.local_total_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_line_total_vat = (ov.local_total_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)), olv.local_line_total_exc_vat = (ov.local_total_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)) **/ 

	olv.local_prof_fee = (ov.prof_fee * ar.rate), 
	olv.local_line_subtotal_vat = (ov.local_subtotal_vat * ar.rate), olv.local_line_subtotal_inc_vat = (ov.local_subtotal_inc_vat * ar.rate), olv.local_line_subtotal_exc_vat = (ov.local_subtotal_exc_vat * ar.rate), 
	olv.local_shipping_inc_vat = (ov.local_shipping_inc_vat * ar.rate), olv.local_shipping_exc_vat = (ov.local_shipping_exc_vat * ar.rate), olv.local_shipping_vat = (ov.local_shipping_vat * ar.rate), 
	olv.local_shipping_cost = (ov.local_shipping_cost * ar.rate), olv.local_subtotal_cost = (ov.local_subtotal_cost * ar.rate), 
	olv.local_total_cost = (ov.local_total_cost * ar.rate), 
	olv.local_discount_inc_vat = (ov.local_discount_inc_vat * ar.rate), olv.local_discount_vat = (ov.local_discount_vat * ar.rate), olv.local_discount_exc_vat = (ov.local_discount_exc_vat * ar.rate), 
	olv.local_store_credit_inc_vat = (ov.local_store_credit_inc_vat * ar.rate), olv.local_store_credit_vat = (ov.local_store_credit_vat * ar.rate), olv.local_store_credit_exc_vat = (ov.local_store_credit_exc_vat * ar.rate), 
	olv.local_adjustment_inc_vat = (ov.local_adjustment_inc_vat * ar.rate), olv.local_adjustment_vat = (ov.local_adjustment_vat * ar.rate), olv.local_adjustment_exc_vat = (ov.local_adjustment_exc_vat * ar.rate), 
	olv.local_line_total_inc_vat = (ov.local_total_inc_vat * ar.rate), olv.local_line_total_vat = (ov.local_total_vat * ar.rate), olv.local_line_total_exc_vat = (ov.local_total_exc_vat * ar.rate) 
WHERE olv.item_id = ol.item_id 
	AND ol.order_id = ov.order_id AND ol.item_id = ar.item_id AND olv.vat_done = 0

UPDATE order_lines_vat olv, dw_order_lines ol, order_headers_vat ov 
SET 
	/**olv.local_line_total_exc_vat = local_line_total_inc_vat - local_line_total_vat,**/ 
	olv.local_price_exc_vat = local_line_subtotal_exc_vat / olv.qty, 
	olv.vat_percent = olv.local_line_total_vat / (olv.local_line_total_exc_vat) , 
	olv.vat_percent_before_prof_fee = ov.vat_rate - 1 , 
	olv.discount_percent = -1 * (ABS(olv.local_discount_exc_vat) / (olv.local_line_total_exc_vat + ABS(olv.local_discount_exc_vat ))), 
	olv.prof_fee_percent = ov.prof_fee_percent 
WHERE olv.item_id = ol.item_id AND ol.order_id = ov.order_id AND olv.vat_done = 0

UPDATE order_lines_vat olv, dw_order_lines ol, order_headers_vat ov 
SET 
	olv.local_price_vat = local_price_inc_vat - local_price_exc_vat 
WHERE olv.item_id = ol.item_id AND ol.order_id = ov.order_id AND olv.vat_done = 0


UPDATE order_lines_vat olv, dw_order_lines ol, dw_order_headers oh, dw_order_headers soh 
SET 
	olv.local_to_global_rate = oh.base_to_global_rate, olv.order_currency_code = oh.order_currency_code 
WHERE olv.item_id = ol.item_id AND ol.order_id = oh.order_id AND oh.order_id = soh.order_id AND olv.vat_done = 0

UPDATE order_lines_vat olv 
SET 
	local_margin_amount = local_line_total_exc_vat - local_total_cost, 
	local_margin_percent = (local_line_total_exc_vat - local_total_cost) / local_line_total_exc_vat 
WHERE olv.vat_done = 0

UPDATE order_lines_vat 
SET 
	global_prof_fee = local_to_global_rate * local_prof_fee, 
	global_price_inc_vat = local_to_global_rate * local_price_inc_vat, global_price_vat = local_to_global_rate * local_price_vat, global_price_exc_vat = local_to_global_rate * local_price_exc_vat, 
	global_line_subtotal_inc_vat = local_to_global_rate * local_line_subtotal_inc_vat, global_line_subtotal_vat = local_to_global_rate * local_line_subtotal_vat, global_line_subtotal_exc_vat = local_to_global_rate * local_line_subtotal_exc_vat, 
	global_shipping_inc_vat = local_to_global_rate * local_shipping_inc_vat, global_shipping_vat = local_to_global_rate * local_shipping_vat, global_shipping_exc_vat = local_to_global_rate * local_shipping_exc_vat, 
	global_discount_inc_vat = local_to_global_rate * local_discount_inc_vat, global_discount_vat = local_to_global_rate * local_discount_vat, global_discount_exc_vat = local_to_global_rate * local_discount_exc_vat, 
	global_store_credit_inc_vat = local_to_global_rate * local_store_credit_inc_vat, global_store_credit_vat = local_to_global_rate * local_store_credit_vat, global_store_credit_exc_vat = local_to_global_rate * local_store_credit_exc_vat, 
	global_adjustment_inc_vat = local_to_global_rate * local_adjustment_inc_vat, global_adjustment_vat = local_to_global_rate * local_adjustment_vat, global_adjustment_exc_vat = local_to_global_rate * local_adjustment_exc_vat, 
	global_line_total_inc_vat = local_to_global_rate * local_line_total_inc_vat, global_line_total_vat = local_to_global_rate * local_line_total_vat, global_line_total_exc_vat = local_to_global_rate * local_line_total_exc_vat, 
	global_subtotal_cost = local_to_global_rate * local_subtotal_cost, global_shipping_cost = local_to_global_rate * local_shipping_cost, 
	global_total_cost = local_to_global_rate * local_total_cost, 
	global_margin_amount = local_to_global_rate * local_margin_amount 
WHERE vat_done = 0

UPDATE order_lines_vat 
SET global_margin_percent = global_margin_amount / global_line_total_inc_vat 
WHERE vat_done = 0
