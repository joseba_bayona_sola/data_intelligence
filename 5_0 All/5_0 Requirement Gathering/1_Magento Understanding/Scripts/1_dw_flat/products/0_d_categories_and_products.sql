
select c.category_id,	
	c.product_type, c.category, 
	c.modality, c.type, c.feature,  
	c.promotional_product, count(*)
from 
		DW_GetLenses.dbo.categories c
	inner join
		DW_GetLenses.dbo.products p on c.category_id = p.category_id and p.store_name = 'default'
group by c.category_id,	
	c.product_type, c.category, 
	c.modality, c.type, c.feature,  
	c.promotional_product
order by c.product_type, c.category, c.modality, c.type, c.feature


select c.category_id,	
	c.product_type, c.category, 
	c.modality, c.type, c.feature,  
	c.promotional_product, 
	p.product_id, p.name, p.telesales_only, p.promotional_product
from 
		DW_GetLenses.dbo.categories c
	inner join
		DW_GetLenses.dbo.products p on c.category_id = p.category_id and p.store_name = 'default'
where c.category_id in (501, 537, 23)
order by c.product_type, c.category, c.modality, c.type, c.feature, p.product_id
