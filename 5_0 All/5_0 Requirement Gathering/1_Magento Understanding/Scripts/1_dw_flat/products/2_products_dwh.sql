
select product_id, sku, name, status, product_type, product_lifecycle, tax_class_id
from DW_GetLenses.dbo.products
where store_name = 'default'
order by product_id
order by product_type, product_id

select top 1000 product_id, count(*) num_lines, sum(local_line_subtotal_inc_vat) total_value, min(document_date) min, max(document_date) max
from DW_GetLenses.dbo.order_lines
where document_date > getutcdate() - 360
group by product_id
order by product_id

------------------------------------

select p.product_id, p.sku, p.name, p.status, p.product_type, p.product_lifecycle, p.tax_class_id, 
	ol.num_lines, ol.total_value, ol.min_d, ol.max_d
from
		(select product_id, sku, name, status, product_type, product_lifecycle, tax_class_id
		from DW_GetLenses.dbo.products
		where store_name = 'default') p
	left join
		(select product_id, count(*) num_lines, sum(local_line_subtotal_inc_vat) total_value, min(document_date) min_d, max(document_date) max_d
		from DW_GetLenses.dbo.order_lines
		where document_date > getutcdate() - 360
		group by product_id) ol on p.product_id = ol.product_id
-- order by p.product_id
order by p.product_type, p.product_id

------------------------------------

select p.product_id, p.sku, p.name, p.status, p.product_type, p.product_lifecycle, p.tax_class_id, 
	ol.num_lines, ol.total_value, ol.min_d, ol.max_d, 
	mp.magentosku, mp.name
from
		(select product_id, sku, name, status, product_type, product_lifecycle, tax_class_id
		from DW_GetLenses.dbo.products
		where store_name = 'default') p
	left join
		(select product_id, count(*) num_lines, sum(local_line_subtotal_inc_vat) total_value, min(document_date) min_d, max(document_date) max_d
		from DW_GetLenses.dbo.order_lines
		where document_date > getutcdate() - 360
		group by product_id) ol on p.product_id = ol.product_id
	left join
		(select *
		from DW_GetLenses_jbs.dbo.ex_mendix_product_list_export 
		where magentoproductid not like 'P%' and magentoproductid not like 'W%')mp on p.product_id = cast(mp.magentoproductid as int)
order by p.product_type, p.product_id
