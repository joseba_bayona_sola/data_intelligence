
select manufacturer_id, manufacturer_code, manufacturer_name
from Landing.mag.edi_manufacturer_aud
order by manufacturer_id

select manufacturer, count(*)
from Landing.mag.catalog_product_entity_flat_aud
where store_id = 0
group by manufacturer
order by manufacturer

select p.entity_id, p.sku, p.name, p.manufacturer, m.manufacturer_code, m.manufacturer_name
from 
		Landing.mag.catalog_product_entity_flat_aud p
	left join
		Landing.mag.edi_manufacturer_aud m on p.manufacturer = m.manufacturer_id
where p.store_id = 0
order by p.manufacturer, p.entity_id

select p.manufacturer, m.manufacturer_code, m.manufacturer_name, count(*)
from 
		Landing.mag.catalog_product_entity_flat_aud p
	left join
		Landing.mag.edi_manufacturer_aud m on p.manufacturer = m.manufacturer_id
where p.store_id = 0
group by p.manufacturer, m.manufacturer_code, m.manufacturer_name
order by m.manufacturer_name -- p.manufacturer, m.manufacturer_code, m.manufacturer_name

--- 

select p.manufacturer, m.value manufacturer_name, count(*)
from 
		Landing.mag.catalog_product_entity_flat_aud p
	left join
		Landing.mag.eav_attribute_option_value m on p.manufacturer = m.option_id
where p.store_id = 0
group by p.manufacturer, m.value
order by manufacturer_name -- p.manufacturer, m.manufacturer_code, m.manufacturer_name


select *
from Landing.mag.eav_attribute_option_value
where option_id = 252
order by value_id