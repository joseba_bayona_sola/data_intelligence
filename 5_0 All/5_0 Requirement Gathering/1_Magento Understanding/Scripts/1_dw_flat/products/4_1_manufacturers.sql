
select product_id, sku, name, manufacturer, brand
from DW_GetLenses.dbo.products
where store_name = 'Default'
	-- and brand is not null
order by product_id

select manufacturer, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'
group by manufacturer
order by manufacturer

select brand, count(*)
from DW_GetLenses.dbo.products
where store_name = 'Default'
group by brand
order by brand