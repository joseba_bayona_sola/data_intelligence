

select entity_id, sku, name, manufacturer, status, product_type, product_lifecycle, tax_class_id, is_lens
from Landing.mag.catalog_product_entity_flat_aud
where store_id = 0
order by entity_id

select p.entity_id, p.sku, p.name, p.manufacturer, p.status, p.product_type, c.product_type, p.product_lifecycle, p.tax_class_id, p.is_lens
from 
		Landing.mag.catalog_product_entity_flat_aud p
	inner join
		openquery(MAGENTO, 'select * from dw_flat.dw_product_category_flat') pc on p.entity_id = pc.product_id
	inner join
		openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') c on pc.category_id = c.category_id
where p.store_id = 0
order by c.product_type, p.entity_id

