
select magentoproductid, magentosku, name
from DW_GetLenses_jbs.dbo.ex_mendix_product_list_export
order by magentoproductid

update DW_GetLenses_jbs.dbo.ex_mendix_product_list_export
set magentoproductid = replace(magentoproductid, '"', ''), 
	magentosku = replace(magentosku, '"', ''), 
	name = replace(name, '"', '')	 
