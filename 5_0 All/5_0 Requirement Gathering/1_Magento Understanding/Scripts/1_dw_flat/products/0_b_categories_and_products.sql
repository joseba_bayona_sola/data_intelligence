	select product_id, name, category_name, count(*)
	from
		(select product_id, name, category_num, category_name
		from
				(SELECT p.entity_id product_id, p.name,
					cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
				FROM 
						(select entity_id, name, product_type, telesales_only
						from Landing.mag.catalog_product_entity_flat_aud
						where store_id = 0) p 
					LEFT JOIN 
						Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
					LEFT JOIN 
						Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
					LEFT JOIN 
						(select category_id, 
							name1, name2, name3, name4, name5, name6, name7, name8  
						from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
				-- where p.entity_id = 1083
				) t
			unpivot
				(category_name for category_num in 
					(name1, name2, name3, name4, name5, name6, name7, name8)
				) unpvt) t
	-- where category_name = 'Multifocal Toric Lenses'
	-- where product_id = 1252
	where category_name like 'Col%'
	group by product_id, name, category_name
	order by product_id, name, category_name;

	select *
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc

	-- MODALITY
	select modality_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by modality_name
	order by modality_name

	select product_id, name, category_name, num, 
		count(*) over (partition by product_id) num_rep
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			(select modality_name, count(*) n
			from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
			where modality_name <> ''
			group by modality_name) t2 on t1.category_name = t2.modality_name
	order by num_rep desc, product_id, name, category_name;

	-- FEATURE
	select feature_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by feature_name
	order by feature_name

	select feature, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by feature
	order by feature
	
	select product_id, name, category_name, num, 
		count(*) over (partition by product_id) num_rep
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			(select feature, count(*) n -- feature_name - feature
			from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
			where feature <> ''
			group by feature) t2 on t1.category_name = t2.feature
	order by num_rep desc, product_id, name, category_name;

	-- TYPE
	select type_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by type_name
	order by type_name

	select type, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by type
	order by type

	select product_id, name, category_name, num, 
		count(*) over (partition by product_id) num_rep
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			(select type_name, count(*) n -- type_name - type
			from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
			where type_name <> ''
			group by type_name) t2 on t1.category_name = t2.type_name
	-- order by num_rep desc, product_id, name, category_name;
	order by category_name, product_id

	-- PRODUCT TYPE NAME
	select product_type_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by product_type_name

	-- PRODUCT TYPE 
	select product_type
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by product_type

	-- CATEGORY
	select category
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
	group by category
	order by category

	select product_id, name, category_name, num, 
		count(*) over (partition by product_id) num_rep
	from
			(select product_id, name, category_name, count(*) num
			from
				(select product_id, name, category_num, category_name
				from
						(SELECT p.entity_id product_id, p.name,
							cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
						FROM 
								(select entity_id, name, product_type, telesales_only
								from Landing.mag.catalog_product_entity_flat_aud
								where store_id = 0) p 
							LEFT JOIN 
								Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
							LEFT JOIN 
								Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
							LEFT JOIN 
								(select category_id, 
									name1, name2, name3, name4, name5, name6, name7, name8  
								from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
						-- where p.entity_id = 1083
						) t
					unpivot
						(category_name for category_num in 
							(name1, name2, name3, name4, name5, name6, name7, name8)
						) unpvt) t
			group by product_id, name, category_name) t1
		inner join
			(select category, count(*) n
			from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc
			where category <> ''
			group by category) t2 on t1.category_name = t2.category
	--order by num_rep desc, product_id, name, category_name;
	--order by num_rep, category_name, product_id
	order by category_name, product_id