
SELECT TOP 1000 *
from Landing.mag.edi_stock_item_aud


select replace(po, char(13), ''), len(replace(po, char(13), '')), count(*)
from Landing.mag.edi_stock_item_aud
group by replace(po, char(13), '')
order by len(replace(po, char(13), '')), replace(po, char(13), '')
--order by replace(po, char(13), '')

select 
	-- bc, count(*)
	-- di, count(*)
	-- po, count(*)
	right(po), count(*)
	-- cy, count(*)
	-- ax, count(*)
	-- ad, count(*)
	-- do, count(*)
	-- co, count(*)

from Landing.mag.edi_stock_item_aud
group by
	-- bc
	-- di
	-- po
	right(po)
	-- cy
	-- ax
	-- ad
	-- do
	-- co
order by 
	-- bc
	-- di
	-- po
	right(po)	
	-- cy
	-- ax
	-- ad
	-- do
	-- co


 

SELECT *
from Landing.mag.edi_stock_item_aud
where product_id = 2385
order by bc, di, po, cy, ax
where po = '-1'
where co = 'Almond'



select replace(po, char(13), '') po_2, count(*) num
from Landing.mag.edi_stock_item_aud
group by replace(po, char(13), '')
order by replace(po, char(13), '')

select po_2, len(po_2), charindex('.', po_2),
	case 
		when (len(po_2) = 5 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 5)
		when (len(po_2) = 5 and charindex('.', po_2) = 4) then po_2 + '0'

		else po_2
	end po_3, 
	num
from 
	(select replace(po, char(13), '') po_2, count(*) num
	from Landing.mag.edi_stock_item_aud
	group by replace(po, char(13), '')) po
where len(po_2) in (5, 6)
order by po_3


select po_2, len(po_2), charindex('.', po_2),
	case 
		when (len(po_2) = 1) then '+0' + po_2 + '.00'
		when (len(po_2) = 2) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 1) + '.00'
		when (len(po_2) = 3 and charindex('-', po_2) = 1) then po_2 + '.00'
		when (len(po_2) = 3 and charindex('-', po_2) = 0) then '+0' + po_2 + '0'
		when (len(po_2) = 4 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 3) + '0'
		when (len(po_2) = 4 and charindex('.', po_2) = 2) then '+0' + po_2
		when (len(po_2) = 12) then '+0' + substring(po_2, 1, 4)
		when (len(po_2) = 13) then substring(po_2, 1, 1) + '0' + + substring(po_2, 2, 4)
		when (len(po_2) = 14) then substring(po_2, 1, 6)
		else po_2
	end po_3, 
	num
from 
	(select replace(po, char(13), '') po_2, count(*) num
	from Landing.mag.edi_stock_item_aud
	group by replace(po, char(13), '')) po
where len(po_2) not in (5, 6)
order by len(po_2), po_2



select po_2, len(po_2), charindex('.', po_2),
	case 
		when (len(po_2) = 5 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 5)
		when (len(po_2) = 5 and charindex('.', po_2) = 4) then po_2 + '0'
		when (len(po_2) = 1) then '+0' + po_2 + '.00'
		when (len(po_2) = 2) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 1) + '.00'
		when (len(po_2) = 3 and charindex('-', po_2) = 1) then po_2 + '.00'
		when (len(po_2) = 3 and charindex('-', po_2) = 0) then '+0' + po_2 + '0'
		when (len(po_2) = 4 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 3) + '0'
		when (len(po_2) = 4 and charindex('.', po_2) = 2) then '+0' + po_2
		when (len(po_2) = 12) then '+0' + substring(po_2, 1, 4)
		when (len(po_2) = 13) then substring(po_2, 1, 1) + '0' + + substring(po_2, 2, 4)
		when (len(po_2) = 14) then substring(po_2, 1, 6)
		else po_2
	end po_3, 
	num
from 
	(select replace(po, char(13), '') po_2, count(*) num
	from Landing.mag.edi_stock_item_aud
	group by replace(po, char(13), '')) po
order by po_3


select po_3 po_def, sum(num)
from
	(select po_2, len(po_2) po_2_len, charindex('.', po_2) po_2_i,
		case 
			when (len(po_2) = 5 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 5)
			when (len(po_2) = 5 and charindex('.', po_2) = 4) then po_2 + '0'
			when (len(po_2) = 1) then '+0' + po_2 + '.00'
			when (len(po_2) = 2) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 1) + '.00'
			when (len(po_2) = 3 and charindex('-', po_2) = 1) then po_2 + '.00'
			when (len(po_2) = 3 and charindex('-', po_2) = 0) then '+0' + po_2 + '0'
			when (len(po_2) = 4 and charindex('.', po_2) = 3) then substring(po_2, 1, 1) + '0' + substring(po_2, 2, 3) + '0'
			when (len(po_2) = 4 and charindex('.', po_2) = 2) then '+0' + po_2
			when (len(po_2) = 12) then '+0' + substring(po_2, 1, 4)
			when (len(po_2) = 13) then substring(po_2, 1, 1) + '0' + + substring(po_2, 2, 4)
			when (len(po_2) = 14) then substring(po_2, 1, 6)
			else po_2
		end po_3, 
		num
	from 
		(select replace(po, char(13), '') po_2, count(*) num
		from Landing.mag.edi_stock_item_aud
		group by replace(po, char(13), '')) po) t
group by po_3
order by po_3