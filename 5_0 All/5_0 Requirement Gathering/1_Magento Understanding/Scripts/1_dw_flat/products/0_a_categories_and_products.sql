
-- dw_flat_category
	select category_id,	
		product_type_name,
		product_type, category, 
		modality, modality_name, type, type_name, feature, feature_name, 
		promotional_product, telesales_only
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	order by promotional_product, product_type, category, modality, type, feature

	select product_type_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by product_type_name
	order by product_type_name

	select product_type, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by product_type
	order by product_type

	select category, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by category
	order by category

	select modality, modality_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by modality, modality_name
	order by modality, modality_name

	select type, type_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by type, type_name
	order by type, type_name

	select feature, feature_name, count(*)
	from openquery(MAGENTO, 'select * from dw_flat.dw_flat_category')
	group by feature, feature_name
	order by feature, feature_name

-------------------------------------------------------------------------

-- dw_category_path
	select category_id, 
		name1, name2, name3, name4, name5, name6, name7, name8  
	from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')
	order by name1, name2, name3, name4, name5, name6, name7, name8 

	SELECT d1.entity_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
    FROM 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d1 
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d2 ON d1.parent_id = d2.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d3 ON d2.parent_id = d3.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d4 ON d3.parent_id = d4.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d5 ON d4.parent_id = d5.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d6 ON d5.parent_id = d6.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d7 ON d6.parent_id = d7.entity_id
		LEFT JOIN 
			(select entity_id, parent_id, name
			from Landing.mag.catalog_category_entity_flat_aud
			where store_id = 0) d8 ON d7.parent_id = d8.entity_id
	-- order by name1, name2, name3, name4, name5, name6, name7, name8
	order by d1.entity_id;

	SELECT p.entity_id product_id, p.name,
		cp.category_id, cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
    FROM 
			(select entity_id, name, product_type, telesales_only
			from Landing.mag.catalog_product_entity_flat_aud
			where store_id = 0) p 
		LEFT JOIN 
			Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
		LEFT JOIN 
			Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
		LEFT JOIN 
			(select category_id, 
				name1, name2, name3, name4, name5, name6, name7, name8  
			from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
	-- where p.entity_id = 1302
	order by p.entity_id, cp.name8, cp.name7, cp.name6, cp.name5, cp.name4, cp.name3, cp.name2, cp.name1  

-------------------------------------------------------------------------

-- dw_product_category_map

	select product_id, name, 
		product_type_name,
		modality_name, feature_name, type_name, 
		telesales_only
	from openquery(MAGENTO, 'select * from dw_flat.dw_product_category_map')
	order by product_id

	select p.*
    FROM 
			(select entity_id, name, product_type, telesales_only, promotional_product
			from Landing.mag.catalog_product_entity_flat_aud
			where store_id = 0) p 
		LEFT JOIN 
			Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id 
	where cpc.product_id is null
	order by telesales_only, promotional_product, product_type, p.entity_id

	SELECT p.entity_id product_id,
		MAX(p.name) name,
		ISNULL(MAX(fc.modality_name), 'NONE') modality_name, ISNULL(MAX(fc1.feature_name), 'NONE') feature_name, ISNULL(MAX(fc2.type_name), 'NONE')    type_name,
		ISNULL(MAX(p.product_type), 'Other') product_type_name,
		ISNULL(MAX(p.telesales_only), 0) telesales_only, 

		count (distinct fc.modality_name) dist_mod, count (distinct fc1.feature_name) dist_feat, count (distinct fc2.type_name) dist_type
    FROM 
			(select entity_id, name, product_type, telesales_only
			from Landing.mag.catalog_product_entity_flat_aud
			where store_id = 0) p 
		LEFT JOIN 
			Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
		LEFT JOIN 
			Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
		LEFT JOIN 
			(select category_id, 
				name1, name2, name3, name4, name5, name6, name7, name8  
			from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id  
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc ON 
				fc.modality_name = cp.name1 OR fc.modality_name = cp.name2 OR fc.modality_name = cp.name3 OR fc.modality_name = cp.name4 OR 
				fc.modality_name = cp.name5 OR fc.modality_name = cp.name6 OR fc.modality_name = cp.name7 OR fc.modality_name = cp.name8
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc1 ON 
				fc1.feature_name = cp.name1 OR fc1.feature_name = cp.name2 OR fc1.feature_name = cp.name3 OR fc1.feature_name = cp.name4 OR 
				fc1.feature_name = cp.name5 OR fc1.feature_name = cp.name6 OR fc1.feature_name = cp.name7 OR fc1.feature_name = cp.name8
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc2 ON 
				fc2.type_name = cp.name1 OR fc2.type_name = cp.name2 OR fc2.type_name = cp.name3 OR fc2.type_name = cp.name4 OR 
				fc2.type_name = cp.name5 OR fc2.type_name = cp.name6 OR fc2.type_name = cp.name7 OR fc2.type_name = cp.name8
	GROUP BY p.entity_id
	ORDER BY p.entity_id;

	SELECT p.entity_id product_id, p.name,
		fc.modality_name, fc1.feature_name, fc2.type_name,
		p.product_type,
		p.telesales_only
    FROM 
			(select entity_id, name, product_type, telesales_only
			from Landing.mag.catalog_product_entity_flat_aud
			where store_id = 0) p 
		LEFT JOIN 
			Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
		LEFT JOIN 
			Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
		LEFT JOIN 
			(select category_id, 
				name1, name2, name3, name4, name5, name6, name7, name8  
			from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id  
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc ON 
				fc.modality_name = cp.name1 OR fc.modality_name = cp.name2 OR fc.modality_name = cp.name3 OR fc.modality_name = cp.name4 OR 
				fc.modality_name = cp.name5 OR fc.modality_name = cp.name6 OR fc.modality_name = cp.name7 OR fc.modality_name = cp.name8
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc1 ON 
				fc1.feature_name = cp.name1 OR fc1.feature_name = cp.name2 OR fc1.feature_name = cp.name3 OR fc1.feature_name = cp.name4 OR 
				fc1.feature_name = cp.name5 OR fc1.feature_name = cp.name6 OR fc1.feature_name = cp.name7 OR fc1.feature_name = cp.name8
		LEFT JOIN 
			openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc2 ON 
				fc2.type_name = cp.name1 OR fc2.type_name = cp.name2 OR fc2.type_name = cp.name3 OR fc2.type_name = cp.name4 OR 
				fc2.type_name = cp.name5 OR fc2.type_name = cp.name6 OR fc2.type_name = cp.name7 OR fc2.type_name = cp.name8
	where p.entity_id in (1089, 1235, 2305)
	-- where fc.modality_name is not null and fc1.feature_name is not null and fc2.type_name is not null 
	ORDER BY p.entity_id, fc.modality_name, fc1.feature_name, fc2.type_name;

-------------------------------------------------------------------------

-- dw_product_category_flat

	select product_id, category_id
	from openquery(MAGENTO, 'select * from dw_flat.dw_product_category_flat')

 	SELECT product_id, MAX(category_id) category_id
	FROM 
		(SELECT product_id, ISNULL(category_id,(SELECT category_id FROM openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') WHERE product_type = 'Other')) category_id
		FROM    
				openquery(MAGENTO, 'select * from dw_flat.dw_product_category_map') mp     
			LEFT JOIN  
				openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc ON 
					fc.modality_name = mp.modality_name AND fc.feature_name = mp.feature_name AND fc.type_name = mp.type_name AND 
					fc.product_type_name = mp.product_type_name AND fc.telesales_only = mp.telesales_only) a
	GROUP BY product_id;

 	SELECT product_id, category_id, count(*) over (partition by product_id) num_rep
	FROM 
		(SELECT product_id, ISNULL(category_id,(SELECT category_id FROM openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') WHERE product_type = 'Other')) category_id
		FROM    
				openquery(MAGENTO, 'select * from dw_flat.dw_product_category_map') mp     
			LEFT JOIN  
				openquery(MAGENTO, 'select * from dw_flat.dw_flat_category') fc ON 
					fc.modality_name = mp.modality_name AND fc.feature_name = mp.feature_name AND fc.type_name = mp.type_name AND 
					fc.product_type_name = mp.product_type_name AND fc.telesales_only = mp.telesales_only) a
	ORDER BY num_rep desc, product_id;

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
-- UNPIVOT

	SELECT p.entity_id product_id, p.name,
		cp.category_id, cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
    FROM 
			(select entity_id, name, product_type, telesales_only
			from Landing.mag.catalog_product_entity_flat_aud
			where store_id = 0) p 
		LEFT JOIN 
			Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
		LEFT JOIN 
			Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
		LEFT JOIN 
			(select category_id, 
				name1, name2, name3, name4, name5, name6, name7, name8  
			from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
	where p.entity_id = 1083
	order by p.entity_id, cp.name8, cp.name7, cp.name6, cp.name5, cp.name4, cp.name3, cp.name2, cp.name1  

	select product_id, name, category_num, category_name
	from
			(SELECT p.entity_id product_id, p.name,
				cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
			FROM 
					(select entity_id, name, product_type, telesales_only
					from Landing.mag.catalog_product_entity_flat_aud
					where store_id = 0) p 
				LEFT JOIN 
					Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
				LEFT JOIN 
					Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
				LEFT JOIN 
					(select category_id, 
						name1, name2, name3, name4, name5, name6, name7, name8  
					from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
			where p.entity_id = 1083) t
		unpivot
			(category_name for category_num in 
				(name1, name2, name3, name4, name5, name6, name7, name8)
			) unpvt
	order by category_num desc, category_name;
	
	select product_id, name, category_name, count(*)
	from
		(select product_id, name, category_num, category_name
		from
				(SELECT p.entity_id product_id, p.name,
					cp.name1, cp.name2, cp.name3, cp.name4, cp.name5, cp.name6, cp.name7, cp.name8  
				FROM 
						(select entity_id, name, product_type, telesales_only
						from Landing.mag.catalog_product_entity_flat_aud
						where store_id = 0) p 
					LEFT JOIN 
						Landing.mag.catalog_category_product_aud cpc ON p.entity_id = cpc.product_id
					LEFT JOIN 
						Landing.mag.catalog_category_entity_aud c ON cpc.category_id = c.entity_id 
					LEFT JOIN 
						(select category_id, 
							name1, name2, name3, name4, name5, name6, name7, name8  
						from openquery(MAGENTO, 'select * from dw_flat.dw_category_path')) cp ON c.entity_id = cp.category_id 
				where p.entity_id = 1083) t
			unpivot
				(category_name for category_num in 
					(name1, name2, name3, name4, name5, name6, name7, name8)
				) unpvt) t
	group by product_id, name, category_name
	order by product_id, name, category_name;
	