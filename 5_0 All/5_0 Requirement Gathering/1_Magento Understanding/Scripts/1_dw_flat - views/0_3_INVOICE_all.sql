
CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_headers_invoice` AS 
    SELECT 
        `cw`.`store_name` AS `store_name`,
        `inv`.`invoice_id` AS `invoice_id`,
        `inv`.`invoice_no` AS `invoice_no`,
        CAST('INVOICE' AS CHAR (25) CHARSET LATIN1) AS `document_type`,
        `inv`.`created_at` AS `document_date`,
        `inv`.`updated_at` AS `document_updated_at`,
        `inv`.`invoice_id` AS `document_id`,
        `inv`.`invoice_no` AS `document_no`,
        `inv`.`created_at` AS `invoice_date`,
        `inv`.`updated_at` AS `updated_at`,
        `oh`.`order_id` AS `order_id`,
        `oh`.`order_no` AS `order_no`,
        `oh`.`created_at` AS `order_date`,
        `oh`.`customer_id` AS `customer_id`,
        `oh`.`customer_email` AS `customer_email`,
        `oh`.`customer_firstname` AS `customer_firstname`,
        `oh`.`customer_lastname` AS `customer_lastname`,
        `oh`.`customer_middlename` AS `customer_middlename`,
        `oh`.`customer_prefix` AS `customer_prefix`,
        `oh`.`customer_suffix` AS `customer_suffix`,
        `oh`.`customer_taxvat` AS `customer_taxvat`,
        `oh`.`status` AS `status`,
        `oh`.`coupon_code` AS `coupon_code`,
        `ov`.`total_weight` AS `weight`,
        `oh`.`customer_gender` AS `customer_gender`,
        `oh`.`customer_dob` AS `customer_dob`,
        LEFT(`oh`.`shipping_description`,
            LOCATE(' - ', `oh`.`shipping_description`)) AS `shipping_carrier`,
        `oh`.`shipping_description` AS `shipping_method`,
        (CASE `oh`.`warehouse_approved_time`
            WHEN '0000-00-00 00:00:00' THEN NULL
            ELSE `oh`.`warehouse_approved_time`
        END) AS `approved_time`,
        `ic`.`length_of_time_to_invoice` AS `length_of_time_to_invoice`,
        (CASE
            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
            ELSE IFNULL(CONVERT( `pm`.`payment_method_name` USING UTF8),
                    `op`.`method`)
        END) AS `payment_method`,
        `op`.`amount_authorized` AS `local_payment_authorized`,
        `op`.`amount_canceled` AS `local_payment_canceled`,
        `op`.`cc_exp_month` AS `cc_exp_month`,
        `op`.`cc_exp_year` AS `cc_exp_year`,
        `op`.`cc_ss_start_month` AS `cc_start_month`,
        `op`.`cc_ss_start_year` AS `cc_start_year`,
        `op`.`cc_last4` AS `cc_last4`,
        `op`.`cc_status_description` AS `cc_status_description`,
        `op`.`cc_owner` AS `cc_owner`,
        IFNULL(CONVERT( `ctm`.`card_type_name` USING UTF8),
                `op`.`cc_type`) AS `cc_type`,
        `op`.`cc_status` AS `cc_status`,
        `op`.`cc_ss_issue` AS `cc_issue_no`,
        `op`.`cc_avs_status` AS `cc_avs_status`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info1`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info2`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info3`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info4`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info5`,
        `bill`.`email` AS `billing_email`,
        `bill`.`company` AS `billing_company`,
        `bill`.`prefix` AS `billing_prefix`,
        `bill`.`firstname` AS `billing_firstname`,
        `bill`.`middlename` AS `billing_middlename`,
        `bill`.`lastname` AS `billing_lastname`,
        `bill`.`suffix` AS `billing_suffix`,
        SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 1) AS `billing_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `bill`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `bill`.`street`) + 1))
            ELSE ''
        END) AS `billing_street2`,
        `bill`.`city` AS `billing_city`,
        `bill`.`region` AS `billing_region`,
        `bill`.`region_id` AS `billing_region_id`,
        `bill`.`postcode` AS `billing_postcode`,
        `bill`.`country_id` AS `billing_country_id`,
        `bill`.`telephone` AS `billing_telephone`,
        `bill`.`fax` AS `billing_fax`,
        `ship`.`email` AS `shipping_email`,
        `ship`.`company` AS `shipping_company`,
        `ship`.`prefix` AS `shipping_prefix`,
        `ship`.`firstname` AS `shipping_firstname`,
        `ship`.`middlename` AS `shipping_middlename`,
        `ship`.`lastname` AS `shipping_lastname`,
        `ship`.`suffix` AS `shipping_suffix`,
        SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 1) AS `shipping_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `ship`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `ship`.`street`) + 1))
            ELSE ''
        END) AS `shipping_street2`,
        `ship`.`city` AS `shipping_city`,
        `ship`.`region` AS `shipping_region`,
        `ship`.`region_id` AS `shipping_region_id`,
        `ship`.`postcode` AS `shipping_postcode`,
        `ship`.`country_id` AS `shipping_country_id`,
        `ship`.`telephone` AS `shipping_telephone`,
        `ship`.`fax` AS `shipping_fax`,
        `ov`.`has_lens` AS `has_lens`,
        `ov`.`total_qty` AS `total_qty`,
        `ov`.`prof_fee` AS `local_prof_fee`,
        `ov`.`local_subtotal_inc_vat` AS `local_subtotal_inc_vat`,
        `ov`.`local_subtotal_vat` AS `local_subtotal_vat`,
        `ov`.`local_subtotal_exc_vat` AS `local_subtotal_exc_vat`,
        `ov`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,
        `ov`.`local_shipping_vat` AS `local_shipping_vat`,
        `ov`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
        (-(1) * ABS(`ov`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
        (-(1) * ABS(`ov`.`local_discount_vat`)) AS `local_discount_vat`,
        (-(1) * ABS(`ov`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
        (-(1) * ABS(`ov`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
        (-(1) * ABS(`ov`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
        (-(1) * ABS(`ov`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
        `ov`.`local_adjustment_inc_vat` AS `local_adjustment_inc_vat`,
        `ov`.`local_adjustment_vat` AS `local_adjustment_vat`,
        `ov`.`local_adjustment_exc_vat` AS `local_adjustment_exc_vat`,
        `ov`.`local_total_inc_vat` AS `local_total_inc_vat`,
        `ov`.`local_total_vat` AS `local_total_vat`,
        `ov`.`local_total_exc_vat` AS `local_total_exc_vat`,
        `ov`.`local_subtotal_cost` AS `local_subtotal_cost`,
        `ov`.`local_shipping_cost` AS `local_shipping_cost`,
        `ov`.`local_total_cost` AS `local_total_cost`,
        `ov`.`local_margin_amount` AS `local_margin_amount`,
        `ov`.`local_margin_percent` AS `local_margin_percent`,
        `oh`.`base_to_global_rate` AS `local_to_global_rate`,
        `oh`.`order_currency_code` AS `order_currency_code`,
        `ov`.`global_prof_fee` AS `global_prof_fee`,
        `ov`.`global_subtotal_inc_vat` AS `global_subtotal_inc_vat`,
        `ov`.`global_subtotal_vat` AS `global_subtotal_vat`,
        `ov`.`global_subtotal_exc_vat` AS `global_subtotal_exc_vat`,
        `ov`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,
        `ov`.`global_shipping_vat` AS `global_shipping_vat`,
        `ov`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
        (-(1) * ABS(`ov`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
        (-(1) * ABS(`ov`.`global_discount_vat`)) AS `global_discount_vat`,
        (-(1) * ABS(`ov`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
        (-(1) * ABS(`ov`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
        (-(1) * ABS(`ov`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
        (-(1) * ABS(`ov`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
        `ov`.`global_adjustment_inc_vat` AS `global_adjustment_inc_vat`,
        `ov`.`global_adjustment_vat` AS `global_adjustment_vat`,
        `ov`.`global_adjustment_exc_vat` AS `global_adjustment_exc_vat`,
        `ov`.`global_total_inc_vat` AS `global_total_inc_vat`,
        `ov`.`global_total_vat` AS `global_total_vat`,
        `ov`.`global_total_exc_vat` AS `global_total_exc_vat`,
        `ov`.`global_subtotal_cost` AS `global_subtotal_cost`,
        `ov`.`global_shipping_cost` AS `global_shipping_cost`,
        `ov`.`global_total_cost` AS `global_total_cost`,
        `ov`.`global_margin_amount` AS `global_margin_amount`,
        `ov`.`global_margin_percent` AS `global_margin_percent`,
        `ov`.`prof_fee_percent` AS `prof_fee_percent`,
        `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
        `ov`.`vat_percent` AS `vat_percent`,
        ABS(`ov`.`discount_percent`) AS `discount_percent`,
        CAST(`oh`.`customer_note` AS CHAR (8000) CHARSET LATIN1) AS `customer_note`,
        CAST(`oh`.`customer_note_notify` AS CHAR (8000) CHARSET LATIN1) AS `customer_note_notify`,
        `oh`.`remote_ip` AS `remote_ip`,
        `oh`.`affilCode` AS `business_source`,
        `ochan`.`channel` AS `business_channel`,
        `oh`.`affilBatch` AS `affilBatch`,
        `oh`.`affilCode` AS `affilCode`,
        `oh`.`affilUserRef` AS `affilUserRef`,
        `oh`.`postoptics_auto_verification` AS `auto_verification`,
        (CASE
            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
            ELSE `oh`.`postoptics_source`
        END) AS `order_type`,
        `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
        `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
        (CASE
            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
            ELSE NULL
        END) AS `reminder_date`,
        `oh`.`reminder_mobile` AS `reminder_mobile`,
        `oh`.`reminder_period` AS `reminder_period`,
        `oh`.`reminder_presc` AS `reminder_presc`,
        `oh`.`reminder_type` AS `reminder_type`,
        `oh`.`reminder_sent` AS `reminder_sent`,
        `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
        `oh`.`telesales_method_code` AS `telesales_method_code`,
        `oh`.`telesales_admin_username` AS `telesales_admin_username`,
        `oh`.`reorder_on_flag` AS `reorder_on_flag`,
        `oh`.`reorder_profile_id` AS `reorder_profile_id`,
        (CASE
            WHEN
                ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
                    OR (`rp`.`completed_profile` <> 1))
            THEN
                NULL
            ELSE `rp`.`next_order_date`
        END) AS `reorder_date`,
        `rp`.`interval_days` AS `reorder_interval`,
        `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
        `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
        `oh`.`automatic_reorder` AS `automatic_reorder`,
        (CASE
            WHEN
                (((`oh`.`presc_verification_method` = 'call')
                    OR ISNULL(`oh`.`presc_verification_method`))
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Call'
            WHEN
                ((`oh`.`presc_verification_method` = 'upload')
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Upload / Fax / Scan'
            WHEN
                ((`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` = 'Yes'))
            THEN
                'Automatic'
            ELSE 'Not Required'
        END) AS `presc_verification_method`,
        `oh`.`referafriend_code` AS `referafriend_code`,
        `oh`.`referafriend_referer` AS `referafriend_referer`,
        GREATEST(`oh`.`dw_synced_at`,
                `inv`.`dw_synced_at`) AS `dw_synced_at`,
        CAST(CONCAT('INVOICE', `inv`.`invoice_id`) AS CHAR (25) CHARSET LATIN1) AS `unique_id`
    FROM
        ((((((((((((((((((
			`dw_flat`.`dw_invoice_headers` `inv`
        JOIN 
			`dw_flat`.`dw_order_headers` `oh` ON ((`oh`.`order_id` = `inv`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
        LEFT JOIN 
			`magento01`.`vw_sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
        JOIN 
			`dw_flat`.`invoice_headers_vat` `ov` ON ((`ov`.`invoice_id` = `inv`.`invoice_id`)))
        JOIN 
			`dw_flat`.`order_headers_vat` `ov2` ON ((`ov2`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_calender_dates` `ic` ON ((`ic`.`invoice_id` = `inv`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_payment_method_map` `pm` ON ((CONVERT( `pm`.`payment_method` USING UTF8) = `op`.`method`)))
        LEFT JOIN 
			`dw_flat`.`dw_card_type_map` `ctm` ON ((CONVERT( `ctm`.`card_type` USING UTF8) = `op`.`cc_type`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote` `rq` ON ((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
        LEFT JOIN 
			`dw_flat`.`max_reorder_quote_payment` `mp` ON ((`rq`.`entity_id` = `mp`.`quote_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote_payment` `rqp` ON ((`rqp`.`payment_id` = `mp`.`payment_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_headers_marketing` `ohm` ON ((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_entity_header_rank_seq` `seq` ON (((`seq`.`DOCUMENT_ID` = `oh`.`order_id`) AND (`seq`.`DOCUMENT_TYPE` = 'ORDER'))))
        LEFT JOIN 
			`dw_flat`.`dw_conf_presc_required` `conf` ON ((`conf`.`store_id` = `oh`.`store_id`)));

CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_headers_creditmemo` AS 
SELECT * FROM dw_flat.vw_invoice_headers_creditmemo_corrected WHERE document_date>='2015-11-16'
UNION 
SELECT * FROM dw_flat.vw_invoice_headers_creditmemo_wrong WHERE document_date<'2015-11-16'
;

CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_headers_creditmemo_corrected` AS 

SELECT 
        `cw`.`store_name` AS `store_name`,
        IFNULL(`cred`.`invoice_id`, -(1)) AS `invoice_id`,
        `inv`.`invoice_no` AS `invoice_no`,
        CAST('CREDITMEMO' AS CHAR (25) CHARSET LATIN1) AS `document_type`,
        `cred`.`created_at` AS `document_date`,
        `cred`.`updated_at` AS `document_updated_at`,
        `cred`.`creditmemo_id` AS `document_id`,
        `cred`.`creditmemo_no` AS `document_no`,
        `cred`.`created_at` AS `invoice_date`,
        `cred`.`updated_at` AS `updated_at`,
        `oh`.`order_id` AS `order_id`,
        `oh`.`order_no` AS `order_no`,
        `oh`.`created_at` AS `order_date`,
        `oh`.`customer_id` AS `customer_id`,
        `oh`.`customer_email` AS `customer_email`,
        `oh`.`customer_firstname` AS `customer_firstname`,
        `oh`.`customer_lastname` AS `customer_lastname`,
        `oh`.`customer_middlename` AS `customer_middlename`,
        `oh`.`customer_prefix` AS `customer_prefix`,
        `oh`.`customer_suffix` AS `customer_suffix`,
        `oh`.`customer_taxvat` AS `customer_taxvat`,
        `oh`.`status` AS `status`,
        `oh`.`coupon_code` AS `coupon_code`,
        (-(1) * `ov`.`total_weight`) AS `weight`,
        `oh`.`customer_gender` AS `customer_gender`,
        `oh`.`customer_dob` AS `customer_dob`,
        LEFT(`oh`.`shipping_description`,
            LOCATE(' - ', `oh`.`shipping_description`)) AS `shipping_carrier`,
        `oh`.`shipping_description` AS `shipping_method`,
        (CASE `oh`.`warehouse_approved_time`
            WHEN '0000-00-00 00:00:00' THEN NULL
            ELSE `oh`.`warehouse_approved_time`
        END) AS `approved_time`,
        `oc`.`length_of_time_to_invoice` AS `length_of_time_to_invoice`,
        (CASE
            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
            ELSE IFNULL(CONVERT( `pm`.`payment_method_name` USING UTF8),
                    `op`.`method`)
        END) AS `payment_method`,
        `op`.`amount_authorized` AS `local_payment_authorized`,
        `op`.`amount_canceled` AS `local_payment_canceled`,
        `op`.`cc_exp_month` AS `cc_exp_month`,
        `op`.`cc_exp_year` AS `cc_exp_year`,
        `op`.`cc_ss_start_month` AS `cc_start_month`,
        `op`.`cc_ss_start_year` AS `cc_start_year`,
        `op`.`cc_last4` AS `cc_last4`,
        `op`.`cc_status_description` AS `cc_status_description`,
        `op`.`cc_owner` AS `cc_owner`,
        IFNULL(CONVERT( `ctm`.`card_type_name` USING UTF8),
                `op`.`cc_type`) AS `cc_type`,
        `op`.`cc_status` AS `cc_status`,
        `op`.`cc_ss_issue` AS `cc_issue_no`,
        `op`.`cc_avs_status` AS `cc_avs_status`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info1`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info2`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info3`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info4`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info5`,
        `bill`.`email` AS `billing_email`,
        `bill`.`company` AS `billing_company`,
        `bill`.`prefix` AS `billing_prefix`,
        `bill`.`firstname` AS `billing_firstname`,
        `bill`.`middlename` AS `billing_middlename`,
        `bill`.`lastname` AS `billing_lastname`,
        `bill`.`suffix` AS `billing_suffix`,
        SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 1) AS `billing_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `bill`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `bill`.`street`) + 1))
            ELSE ''
        END) AS `billing_street2`,
        `bill`.`city` AS `billing_city`,
        `bill`.`region` AS `billing_region`,
        `bill`.`region_id` AS `billing_region_id`,
        `bill`.`postcode` AS `billing_postcode`,
        `bill`.`country_id` AS `billing_country_id`,
        `bill`.`telephone` AS `billing_telephone`,
        `bill`.`fax` AS `billing_fax`,
        `ship`.`email` AS `shipping_email`,
        `ship`.`company` AS `shipping_company`,
        `ship`.`prefix` AS `shipping_prefix`,
        `ship`.`firstname` AS `shipping_firstname`,
        `ship`.`middlename` AS `shipping_middlename`,
        `ship`.`lastname` AS `shipping_lastname`,
        `ship`.`suffix` AS `shipping_suffix`,
        SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 1)  AS `shiping_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `ship`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `ship`.`street`) + 1))
            ELSE ''
        END) AS `shipping_street2`,
        `ship`.`city` AS `shipping_city`,
        `ship`.`region` AS `shipping_region`,
        `ship`.`region_id` AS `shipping_region_id`,
        `ship`.`postcode` AS `shipping_postcode`,
        `ship`.`country_id` AS `shipping_country_id`,
        `ship`.`telephone` AS `shipping_telephone`,
        `ship`.`fax` AS `shipping_fax`,
        `ov`.`has_lens` AS `has_lens`,
        (-(1) * `ov`.`total_qty`) AS `total_qty`,
        (-(1) * ABS(`ov`.`prof_fee`)) AS `local_prof_fee`,
        (-(1) * ABS(`ov`.`local_subtotal_inc_vat`)) AS `local_subtotal_inc_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_vat`)) AS `local_subtotal_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_exc_vat`)) AS `local_subtotal_exc_vat`,
        (-(1) * ABS(`ov`.`local_shipping_inc_vat`)) AS `local_shipping_inc_vat`,
        (-(1) * ABS(`ov`.`local_shipping_vat`)) AS `local_shipping_vat`,
        (-(1) * ABS(`ov`.`local_shipping_exc_vat`)) AS `local_shipping_exc_vat`,
        ABS(`ov`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
        ABS(`ov`.`local_discount_vat`) AS `local_discount_vat`,
        ABS(`ov`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
        ABS(`ov`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
        ABS(`ov`.`local_store_credit_vat`) AS `local_store_credit_vat`,
        ABS(`ov`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
        (-(1) * `ov`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
        (-(1) * `ov`.`local_adjustment_vat`) AS `local_adjustment_vat`,
        (-(1) * `ov`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
        (-(1) * `ov`.`local_total_inc_vat`) AS `local_total_inc_vat`,
        (-(1) * `ov`.`local_total_vat`) AS `local_total_vat`,
        (-(1) * `ov`.`local_total_exc_vat`) AS `local_total_exc_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_cost`)) AS `local_subtotal_cost`,
        (-(1) * ABS(`ov`.`local_shipping_cost`)) AS `local_shipping_cost`,
        (-(1) * ABS(`ov`.`local_total_cost`)) AS `local_total_cost`,
        (-(1) * ABS(`ov`.`local_margin_amount`)) AS `local_margin_amount`,
        `ov`.`local_margin_percent` AS `local_margin_percent`,
        `oh`.`base_to_global_rate` AS `local_to_global_rate`,
        `oh`.`order_currency_code` AS `order_currency_code`,
        (-(1) * ABS(`ov`.`global_prof_fee`)) AS `global_prof_fee`,
        (-(1) * ABS(`ov`.`global_subtotal_inc_vat`)) AS `global_subtotal_inc_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_vat`)) AS `global_subtotal_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_exc_vat`)) AS `global_subtotal_exc_vat`,
        (-(1) * ABS(`ov`.`global_shipping_inc_vat`)) AS `global_shipping_inc_vat`,
        (-(1) * ABS(`ov`.`global_shipping_vat`)) AS `global_shipping_vat`,
        (-(1) * ABS(`ov`.`global_shipping_exc_vat`)) AS `global_shipping_exc_vat`,
        ABS(`ov`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
        ABS(`ov`.`global_discount_vat`) AS `global_discount_vat`,
        ABS(`ov`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
        ABS(`ov`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
        ABS(`ov`.`global_store_credit_vat`) AS `global_store_credit_vat`,
        ABS(`ov`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
        (-(1) * `ov`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
        (-(1) * `ov`.`global_adjustment_vat`) AS `global_adjustment_vat`,
        (-(1) * `ov`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
        (-(1) * `ov`.`global_total_inc_vat`) AS `global_total_inc_vat`,
        (-(1) * `ov`.`global_total_vat`) AS `global_total_vat`,
        (-(1) * `ov`.`global_total_exc_vat`) AS `global_total_exc_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_cost`)) AS `global_subtotal_cost`,
        (-(1) * ABS(`ov`.`global_shipping_cost`)) AS `global_shipping_cost`,
        (-(1) * ABS(`ov`.`global_total_cost`)) AS `global_total_cost`,
        (-(1) * ABS(`ov`.`global_margin_amount`)) AS `global_margin_amount`,
        `ov`.`global_margin_percent` AS `global_margin_percent`,
        `ov`.`prof_fee_percent` AS `prof_fee_percent`,
        `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
        `ov`.`vat_percent` AS `vat_percent`,
        ABS(`ov`.`discount_percent`) AS `discount_percent`,
        CAST(`oh`.`customer_note` AS CHAR (8000) CHARSET LATIN1) AS `customer_note`,
        CAST(`oh`.`customer_note_notify` AS CHAR (8000) CHARSET LATIN1) AS `customer_note_notify`,
        `oh`.`remote_ip` AS `remote_ip`,
        `oh`.`affilCode` AS `business_source`,
        `ochan`.`channel` AS `business_channel`,
        `oh`.`affilBatch` AS `affilBatch`,
        `oh`.`affilCode` AS `affilCode`,
        `oh`.`affilUserRef` AS `affilUserRef`,
        `oh`.`postoptics_auto_verification` AS `auto_verification`,
        (CASE
            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
            ELSE `oh`.`postoptics_source`
        END) AS `order_type`,
        `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
        `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
        (CASE
            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
            ELSE NULL
        END) AS `reminder_date`,
        `oh`.`reminder_mobile` AS `reminder_mobile`,
        `oh`.`reminder_period` AS `reminder_period`,
        `oh`.`reminder_presc` AS `reminder_presc`,
        `oh`.`reminder_type` AS `reminder_type`,
        `oh`.`reminder_sent` AS `reminder_sent`,
        `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
        `oh`.`telesales_method_code` AS `telesales_method_code`,
        `oh`.`telesales_admin_username` AS `telesales_admin_username`,
        `oh`.`reorder_on_flag` AS `reorder_on_flag`,
        `oh`.`reorder_profile_id` AS `reorder_profile_id`,
        (CASE
            WHEN
                ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
                    OR (`rp`.`completed_profile` <> 1))
            THEN
                NULL
            ELSE `rp`.`next_order_date`
        END) AS `reorder_date`,
        `rp`.`interval_days` AS `reorder_interval`,
        `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
        `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
        `oh`.`automatic_reorder` AS `automatic_reorder`,
        (CASE
            WHEN
                (((`oh`.`presc_verification_method` = 'call')
                    OR ISNULL(`oh`.`presc_verification_method`))
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Call'
            WHEN
                ((`oh`.`presc_verification_method` = 'upload')
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Upload / Fax / Scan'
            WHEN
                ((`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` = 'Yes'))
            THEN
                'Automatic'
            ELSE 'Not Required'
        END) AS `presc_verification_method`,
        `oh`.`referafriend_code` AS `referafriend_code`,
        `oh`.`referafriend_referer` AS `referafriend_referer`,
        GREATEST(`oh`.`dw_synced_at`,
                `cred`.`dw_synced_at`) AS `dw_synced_at`,
        CAST(CONCAT('CREDITMEMO', `cred`.`creditmemo_id`)
            AS CHAR (25) CHARSET LATIN1) AS `unique_id`
    FROM
        ((((((((((((((((((((
			`dw_flat`.`dw_creditmemo_headers` `cred` force index (created_at)
        JOIN 
			`dw_flat`.`dw_order_headers` `oh` ON ((`oh`.`order_id` = `cred`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
        LEFT JOIN 
			`magento01`.`vw_sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
        JOIN 
			`dw_flat`.`creditmemo_headers_vat` `ov` ON ((`ov`.`creditmemo_id` = `cred`.`creditmemo_id`)))
        JOIN 
			`dw_flat`.`order_headers_vat` `ov2` ON ((`ov2`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_calender_dates` `ic` ON ((`ic`.`invoice_id` = `cred`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_headers` `inv` ON ((`inv`.`invoice_id` = `cred`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_calender_dates` `oc` ON ((`oc`.`invoice_id` = `inv`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_payment_method_map` `pm` ON ((CONVERT( `pm`.`payment_method` USING UTF8) = `op`.`method`)))
        LEFT JOIN 
			`dw_flat`.`dw_card_type_map` `ctm` ON ((CONVERT( `ctm`.`card_type` USING UTF8) = `op`.`cc_type`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote` `rq` ON ((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
        LEFT JOIN 
			`dw_flat`.`max_reorder_quote_payment` `mp` ON ((`rq`.`entity_id` = `mp`.`quote_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote_payment` `rqp` ON ((`rqp`.`payment_id` = `mp`.`payment_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_headers_marketing` `ohm` ON ((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_entity_header_rank_seq` `seq` ON (((`seq`.`DOCUMENT_ID` = `cred`.`creditmemo_id`) AND (`seq`.`DOCUMENT_TYPE` = 'CREDITMEMO'))))
        LEFT JOIN 
			`dw_flat`.`dw_conf_presc_required` `conf` ON ((`conf`.`store_id` = `oh`.`store_id`)));


CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_headers_creditmemo_wrong` AS 

SELECT 
        `cw`.`store_name` AS `store_name`,
        IFNULL(`cred`.`invoice_id`, -(1)) AS `invoice_id`,
        `inv`.`invoice_no` AS `invoice_no`,
        CAST('CREDITMEMO' AS CHAR (25) CHARSET LATIN1) AS `document_type`,
        `cred`.`created_at` AS `document_date`,
        `cred`.`updated_at` AS `document_updated_at`,
        `cred`.`creditmemo_id` AS `document_id`,
        `cred`.`creditmemo_no` AS `document_no`,
        `cred`.`created_at` AS `invoice_date`,
        `cred`.`updated_at` AS `updated_at`,
        `oh`.`order_id` AS `order_id`,
        `oh`.`order_no` AS `order_no`,
        `oh`.`created_at` AS `order_date`,
        `oh`.`customer_id` AS `customer_id`,
        `oh`.`customer_email` AS `customer_email`,
        `oh`.`customer_firstname` AS `customer_firstname`,
        `oh`.`customer_lastname` AS `customer_lastname`,
        `oh`.`customer_middlename` AS `customer_middlename`,
        `oh`.`customer_prefix` AS `customer_prefix`,
        `oh`.`customer_suffix` AS `customer_suffix`,
        `oh`.`customer_taxvat` AS `customer_taxvat`,
        `oh`.`status` AS `status`,
        `oh`.`coupon_code` AS `coupon_code`,
        (-(1) * `ov`.`total_weight`) AS `weight`,
        `oh`.`customer_gender` AS `customer_gender`,
        `oh`.`customer_dob` AS `customer_dob`,
        LEFT(`oh`.`shipping_description`,
            LOCATE(' - ', `oh`.`shipping_description`)) AS `shipping_carrier`,
        `oh`.`shipping_description` AS `shipping_method`,
        (CASE `oh`.`warehouse_approved_time`
            WHEN '0000-00-00 00:00:00' THEN NULL
            ELSE `oh`.`warehouse_approved_time`
        END) AS `approved_time`,
        `oc`.`length_of_time_to_invoice` AS `length_of_time_to_invoice`,
        (CASE
            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
            ELSE IFNULL(CONVERT( `pm`.`payment_method_name` USING UTF8),
                    `op`.`method`)
        END) AS `payment_method`,
        `op`.`amount_authorized` AS `local_payment_authorized`,
        `op`.`amount_canceled` AS `local_payment_canceled`,
        `op`.`cc_exp_month` AS `cc_exp_month`,
        `op`.`cc_exp_year` AS `cc_exp_year`,
        `op`.`cc_ss_start_month` AS `cc_start_month`,
        `op`.`cc_ss_start_year` AS `cc_start_year`,
        `op`.`cc_last4` AS `cc_last4`,
        `op`.`cc_status_description` AS `cc_status_description`,
        `op`.`cc_owner` AS `cc_owner`,
        IFNULL(CONVERT( `ctm`.`card_type_name` USING UTF8),
                `op`.`cc_type`) AS `cc_type`,
        `op`.`cc_status` AS `cc_status`,
        `op`.`cc_ss_issue` AS `cc_issue_no`,
        `op`.`cc_avs_status` AS `cc_avs_status`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info1`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info2`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info3`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info4`,
        CAST(NULL AS CHAR (255) CHARSET LATIN1) AS `payment_info5`,
        `bill`.`email` AS `billing_email`,
        `bill`.`company` AS `billing_company`,
        `bill`.`prefix` AS `billing_prefix`,
        `bill`.`firstname` AS `billing_firstname`,
        `bill`.`middlename` AS `billing_middlename`,
        `bill`.`lastname` AS `billing_lastname`,
        `bill`.`suffix` AS `billing_suffix`,
        SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 1) AS `billing_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `bill`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`bill`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `bill`.`street`) + 1))
            ELSE ''
        END) AS `billing_street2`,
        `bill`.`city` AS `billing_city`,
        `bill`.`region` AS `billing_region`,
        `bill`.`region_id` AS `billing_region_id`,
        `bill`.`postcode` AS `billing_postcode`,
        `bill`.`country_id` AS `billing_country_id`,
        `bill`.`telephone` AS `billing_telephone`,
        `bill`.`fax` AS `billing_fax`,
        `ship`.`email` AS `shipping_email`,
        `ship`.`company` AS `shipping_company`,
        `ship`.`prefix` AS `shipping_prefix`,
        `ship`.`firstname` AS `shipping_firstname`,
        `ship`.`middlename` AS `shipping_middlename`,
        `ship`.`lastname` AS `shipping_lastname`,
        `ship`.`suffix` AS `shipping_suffix`,
        SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 1)  AS `shiping_street1`,
        (CASE
            WHEN
                (LOCATE(CHAR(10), `ship`.`street`) <> 0)
            THEN
                SUBSTR(SUBSTRING_INDEX(`ship`.`street`, CHAR(10), 2),
                    (LOCATE(CHAR(10), `ship`.`street`) + 1))
            ELSE ''
        END) AS `shipping_street2`,
        `ship`.`city` AS `shipping_city`,
        `ship`.`region` AS `shipping_region`,
        `ship`.`region_id` AS `shipping_region_id`,
        `ship`.`postcode` AS `shipping_postcode`,
        `ship`.`country_id` AS `shipping_country_id`,
        `ship`.`telephone` AS `shipping_telephone`,
        `ship`.`fax` AS `shipping_fax`,
        `ov`.`has_lens` AS `has_lens`,
        (-(1) * `ov`.`total_qty`) AS `total_qty`,
        (-(1) * ABS(`ov`.`prof_fee`)) AS `local_prof_fee`,
        (-(1) * ABS(`ov`.`local_subtotal_inc_vat`)) AS `local_subtotal_inc_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_vat`)) AS `local_subtotal_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_exc_vat`)) AS `local_subtotal_exc_vat`,
        (-(1) * ABS(`ov`.`local_shipping_inc_vat`)) AS `local_shipping_inc_vat`,
        (-(1) * ABS(`ov`.`local_shipping_vat`)) AS `local_shipping_vat`,
        (-(1) * ABS(`ov`.`local_shipping_exc_vat`)) AS `local_shipping_exc_vat`,
        ABS(`ov`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
        ABS(`ov`.`local_discount_vat`) AS `local_discount_vat`,
        ABS(`ov`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
        ABS(`ov`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
        ABS(`ov`.`local_store_credit_vat`) AS `local_store_credit_vat`,
        ABS(`ov`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
        (-(1) * `ov`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
        (-(1) * `ov`.`local_adjustment_vat`) AS `local_adjustment_vat`,
        (-(1) * `ov`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
        (-(1) * `ov`.`local_total_inc_vat`) AS `local_total_inc_vat`,
        (-(1) * `ov`.`local_total_vat`) AS `local_total_vat`,
        (-(1) * `ov`.`local_total_exc_vat`) AS `local_total_exc_vat`,
        (-(1) * ABS(`ov`.`local_subtotal_cost`)) AS `local_subtotal_cost`,
        (-(1) * ABS(`ov`.`local_shipping_cost`)) AS `local_shipping_cost`,
        (-(1) * ABS(`ov`.`local_total_cost`)) AS `local_total_cost`,
        (-(1) * ABS(`ov`.`local_margin_amount`)) AS `local_margin_amount`,
        `ov`.`local_margin_percent` AS `local_margin_percent`,
        `oh`.`base_to_global_rate` AS `local_to_global_rate`,
        `oh`.`order_currency_code` AS `order_currency_code`,
        (-(1) * ABS(`ov`.`global_prof_fee`)) AS `global_prof_fee`,
        (-(1) * ABS(`ov`.`global_subtotal_inc_vat`)) AS `global_subtotal_inc_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_vat`)) AS `global_subtotal_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_exc_vat`)) AS `global_subtotal_exc_vat`,
        (-(1) * ABS(`ov`.`global_shipping_inc_vat`)) AS `global_shipping_inc_vat`,
        (-(1) * ABS(`ov`.`global_shipping_vat`)) AS `global_shipping_vat`,
        (-(1) * ABS(`ov`.`global_shipping_exc_vat`)) AS `global_shipping_exc_vat`,
        ABS(`ov`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
        ABS(`ov`.`global_discount_vat`) AS `global_discount_vat`,
        ABS(`ov`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
        ABS(`ov`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
        ABS(`ov`.`global_store_credit_vat`) AS `global_store_credit_vat`,
        ABS(`ov`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
        (-(1) * `ov`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
        (-(1) * `ov`.`global_adjustment_vat`) AS `global_adjustment_vat`,
        (-(1) * `ov`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
        (-(1) * `ov`.`global_total_inc_vat`) AS `global_total_inc_vat`,
        (-(1) * `ov`.`global_total_vat`) AS `global_total_vat`,
        (-(1) * `ov`.`global_total_exc_vat`) AS `global_total_exc_vat`,
        (-(1) * ABS(`ov`.`global_subtotal_cost`)) AS `global_subtotal_cost`,
        (-(1) * ABS(`ov`.`global_shipping_cost`)) AS `global_shipping_cost`,
        (-(1) * ABS(`ov`.`global_total_cost`)) AS `global_total_cost`,
        (-(1) * ABS(`ov`.`global_margin_amount`)) AS `global_margin_amount`,
        `ov`.`global_margin_percent` AS `global_margin_percent`,
        `ov`.`prof_fee_percent` AS `prof_fee_percent`,
        `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
        `ov`.`vat_percent` AS `vat_percent`,
        ABS(`ov`.`discount_percent`) AS `discount_percent`,
        CAST(`oh`.`customer_note` AS CHAR (8000) CHARSET LATIN1) AS `customer_note`,
        CAST(`oh`.`customer_note_notify` AS CHAR (8000) CHARSET LATIN1) AS `customer_note_notify`,
        `oh`.`remote_ip` AS `remote_ip`,
        `oh`.`affilCode` AS `business_source`,
        `ochan`.`channel` AS `business_channel`,
        `oh`.`affilBatch` AS `affilBatch`,
        `oh`.`affilCode` AS `affilCode`,
        `oh`.`affilUserRef` AS `affilUserRef`,
        `oh`.`postoptics_auto_verification` AS `auto_verification`,
        (CASE
            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
            ELSE `oh`.`postoptics_source`
        END) AS `order_type`,
        `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
        `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
        (CASE
            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
            ELSE NULL
        END) AS `reminder_date`,
        `oh`.`reminder_mobile` AS `reminder_mobile`,
        `oh`.`reminder_period` AS `reminder_period`,
        `oh`.`reminder_presc` AS `reminder_presc`,
        `oh`.`reminder_type` AS `reminder_type`,
        `oh`.`reminder_sent` AS `reminder_sent`,
        `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
        `oh`.`telesales_method_code` AS `telesales_method_code`,
        `oh`.`telesales_admin_username` AS `telesales_admin_username`,
        `oh`.`reorder_on_flag` AS `reorder_on_flag`,
        `oh`.`reorder_profile_id` AS `reorder_profile_id`,
        (CASE
            WHEN
                ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
                    OR (`rp`.`completed_profile` <> 1))
            THEN
                NULL
            ELSE `rp`.`next_order_date`
        END) AS `reorder_date`,
        `rp`.`interval_days` AS `reorder_interval`,
        `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
        `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
        `oh`.`automatic_reorder` AS `automatic_reorder`,
        (CASE
            WHEN
                (((`oh`.`presc_verification_method` = 'call')
                    OR ISNULL(`oh`.`presc_verification_method`))
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Call'
            WHEN
                ((`oh`.`presc_verification_method` = 'upload')
                    AND (`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` <> 'Yes'))
            THEN
                'Upload / Fax / Scan'
            WHEN
                ((`conf`.`value` = 1)
                    AND (`ov2`.`has_lens` = 1)
                    AND (`oh`.`postoptics_auto_verification` = 'Yes'))
            THEN
                'Automatic'
            ELSE 'Not Required'
        END) AS `presc_verification_method`,
        `oh`.`referafriend_code` AS `referafriend_code`,
        `oh`.`referafriend_referer` AS `referafriend_referer`,
        GREATEST(`oh`.`dw_synced_at`,
                `cred`.`dw_synced_at`) AS `dw_synced_at`,
        CAST(CONCAT('CREDITMEMO', `cred`.`creditmemo_id`)
            AS CHAR (25) CHARSET LATIN1) AS `unique_id`
    FROM
        ((((((((((((((((((((
			`dw_flat`.`dw_creditmemo_headers` `cred` force index (created_at)
        JOIN 
			`dw_flat`.`dw_order_headers` `oh` ON ((`oh`.`order_id` = `cred`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
        LEFT JOIN 
			`magento01`.`vw_sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
        LEFT JOIN 
			`magento01`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
        JOIN 
			`dw_flat`.`creditmemo_shipment_headers_vat` `ov` ON ((`ov`.`creditmemo_id` = `cred`.`creditmemo_id`)))
        JOIN 
			`dw_flat`.`order_headers_vat` `ov2` ON ((`ov2`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_calender_dates` `ic` ON ((`ic`.`invoice_id` = `cred`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_headers` `inv` ON ((`inv`.`invoice_id` = `cred`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_invoice_calender_dates` `oc` ON ((`oc`.`invoice_id` = `inv`.`invoice_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_payment_method_map` `pm` ON ((CONVERT( `pm`.`payment_method` USING UTF8) = `op`.`method`)))
        LEFT JOIN 
			`dw_flat`.`dw_card_type_map` `ctm` ON ((CONVERT( `ctm`.`card_type` USING UTF8) = `op`.`cc_type`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote` `rq` ON ((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
        LEFT JOIN 
			`dw_flat`.`max_reorder_quote_payment` `mp` ON ((`rq`.`entity_id` = `mp`.`quote_id`)))
        LEFT JOIN 
			`magento01`.`po_reorder_quote_payment` `rqp` ON ((`rqp`.`payment_id` = `mp`.`payment_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_order_headers_marketing` `ohm` ON ((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_entity_header_rank_seq` `seq` ON (((`seq`.`DOCUMENT_ID` = `cred`.`creditmemo_id`) AND (`seq`.`DOCUMENT_TYPE` = 'CREDITMEMO'))))
        LEFT JOIN 
			`dw_flat`.`dw_conf_presc_required` `conf` ON ((`conf`.`store_id` = `oh`.`store_id`)));

----------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_lines_invoice` AS 

SELECT
  `ol`.`item_id`                       AS `line_id`,
  `ol`.`invoice_id`                    AS `invoice_id`,
  `cw`.`store_name`                    AS `store_name`,
  `inv`.`invoice_no`                   AS `invoice_no`,
  CAST('INVOICE' AS CHAR(25) CHARSET latin1) AS `document_type`,
  `inv`.`created_at`                   AS `document_date`,
  `inv`.`updated_at`                   AS `document_updated_at`,
  `inv`.`invoice_id`                   AS `document_id`,
  `inv`.`created_at`                   AS `invoice_date`,
  `inv`.`updated_at`                   AS `updated_at`,
  `ol`.`product_id`                    AS `product_id`,
  `cf`.`product_type`                  AS `product_type`,
  CAST(`ol2`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
  `p`.`is_lens`                        AS `is_lens`,
#CAST(CASE WHEN sfoi.lens_group_eye IN ('Right','Left') THEN LEFT(sfoi.lens_group_eye,1) ELSE '' END AS CHAR(1) CHARSET latin1) AS `lens_eye`,
  CAST((CASE
	WHEN INSTR(`ol2`.`product_options`,'s:5:"label";s:3:"Eye";s:5:"value";s:5:"Right"')>0 THEN 'R'
	WHEN INSTR(`ol2`.`product_options`,'s:5:"label";s:3:"Eye";s:5:"value";s:4:"Left"')>0 THEN 'L'
	ELSE '' END) AS CHAR(1) CHARSET LATIN1) as `lens_eye`,
  `esi`.`BC`                           AS `lens_base_curve`,
  `esi`.`DI`                           AS `lens_diameter`,
  `esi`.`PO`                           AS `lens_power`,
  `esi`.`CY`                           AS `lens_cylinder`,
  `esi`.`AX`                           AS `lens_axis`,
  `esi`.`AD`                           AS `lens_addition`,
  `esi`.`DO`                           AS `lens_dominance`,
  `esi`.`CO`                           AS `lens_colour`,
  `p`.`daysperlens`                    AS `lens_days`,
  CAST('' AS CHAR(1) CHARSET latin1)   AS `product_size`,
  CAST('' AS CHAR(1) CHARSET latin1)   AS `product_colour`,
  `ol2`.`weight`                       AS `unit_weight`,
  (`ol2`.`weight` * `ol`.`qty`)        AS `line_weight`,
  `ol2`.`is_virtual`                   AS `is_virtual`,
  `ol`.`sku`                           AS `sku`,
  `ol`.`name`                          AS `name`,
  `pcf`.`category_id`                  AS `category_id`,
  (CASE WHEN (`oh`.`base_shipping_amount` > 0) THEN 1 ELSE 0 END) AS `free_shipping`,
  (CASE WHEN (`oh`.`base_discount_amount` > 0) THEN 1 ELSE 0 END) AS `no_discount`,
  `ol`.`qty`                           AS `qty`,
  `olv`.`local_prof_fee`               AS `local_prof_fee`,
  `olv`.`local_price_inc_vat`          AS `local_price_inc_vat`,
  `olv`.`local_price_vat`              AS `local_price_vat`,
  `olv`.`local_price_exc_vat`          AS `local_price_exc_vat`,
  `olv`.`local_line_subtotal_inc_vat`  AS `local_line_subtotal_inc_vat`,
  `olv`.`local_line_subtotal_vat`      AS `local_line_subtotal_vat`,
  `olv`.`local_line_subtotal_exc_vat`  AS `local_line_subtotal_exc_vat`,
  `olv`.`local_shipping_inc_vat`       AS `local_shipping_inc_vat`,
  `olv`.`local_shipping_vat`           AS `local_shipping_vat`,
  `olv`.`local_shipping_exc_vat`       AS `local_shipping_exc_vat`,
  (-(1) * ABS(`olv`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
  (-(1) * ABS(`olv`.`local_discount_vat`)) AS `local_discount_vat`,
  (-(1) * ABS(`olv`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
  (-(1) * ABS(`olv`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
  (-(1) * ABS(`olv`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
  (-(1) * ABS(`olv`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
  `olv`.`local_adjustment_inc_vat`     AS `local_adjustment_inc_vat`,
  `olv`.`local_adjustment_vat`         AS `local_adjustment_vat`,
  `olv`.`local_adjustment_exc_vat`     AS `local_adjustment_exc_vat`,
  `olv`.`local_line_total_inc_vat`     AS `local_line_total_inc_vat`,
  `olv`.`local_line_total_vat`         AS `local_line_total_vat`,
  `olv`.`local_line_total_exc_vat`     AS `local_line_total_exc_vat`,
  `olv`.`local_subtotal_cost`          AS `local_subtotal_cost`,
  `olv`.`local_shipping_cost`          AS `local_shipping_cost`,
  `olv`.`local_total_cost`             AS `local_total_cost`,
  `olv`.`local_margin_amount`          AS `local_margin_amount`,
  `olv`.`local_margin_percent`         AS `local_margin_percent`,
  `olv`.`local_to_global_rate`         AS `local_to_global_rate`,
  `olv`.`order_currency_code`          AS `order_currency_code`,
  `olv`.`global_prof_fee`              AS `global_prof_fee`,
  `olv`.`global_price_inc_vat`         AS `global_price_inc_vat`,
  `olv`.`global_price_vat`             AS `global_price_vat`,
  `olv`.`global_price_exc_vat`         AS `global_price_exc_vat`,
  `olv`.`global_line_subtotal_inc_vat` AS `global_line_subtotal_inc_vat`,
  `olv`.`global_line_subtotal_vat`     AS `global_line_subtotal_vat`,
  `olv`.`global_line_subtotal_exc_vat` AS `global_line_subtotal_exc_vat`,
  `olv`.`global_shipping_inc_vat`      AS `global_shipping_inc_vat`,
  `olv`.`global_shipping_vat`          AS `global_shipping_vat`,
  `olv`.`global_shipping_exc_vat`      AS `global_shipping_exc_vat`,
  (-(1) * ABS(`olv`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
  (-(1) * ABS(`olv`.`global_discount_vat`)) AS `global_discount_vat`,
  (-(1) * ABS(`olv`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
  (-(1) * ABS(`olv`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
  (-(1) * ABS(`olv`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
  (-(1) * ABS(`olv`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
  `olv`.`global_adjustment_inc_vat`    AS `global_adjustment_inc_vat`,
  `olv`.`global_adjustment_vat`        AS `global_adjustment_vat`,
  `olv`.`global_adjustment_exc_vat`    AS `global_adjustment_exc_vat`,
  `olv`.`global_line_total_inc_vat`    AS `global_line_total_inc_vat`,
  `olv`.`global_line_total_vat`        AS `global_line_total_vat`,
  `olv`.`global_line_total_exc_vat`    AS `global_line_total_exc_vat`,
  `olv`.`global_subtotal_cost`         AS `global_subtotal_cost`,
  `olv`.`global_shipping_cost`         AS `global_shipping_cost`,
  `olv`.`global_total_cost`            AS `global_total_cost`,
  `olv`.`global_margin_amount`         AS `global_margin_amount`,
  `olv`.`global_margin_percent`        AS `global_margin_percent`,
  `olv`.`prof_fee_percent`             AS `prof_fee_percent`,
  `olv`.`vat_percent_before_prof_fee`  AS `vat_percent_before_prof_fee`,
  `olv`.`vat_percent`                  AS `vat_percent`,
  ABS(`olv`.`discount_percent`)        AS `discount_percent`,
  GREATEST(`ol`.`dw_synced_at`,`inv`.`dw_synced_at`,`oh`.`dw_synced_at`,`ol2`.`dw_synced_at`) AS `dw_synced_at`,
  CAST(CONCAT('INVOICE',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) AS `unique_id`,
  pla.order_item_id AS pla_order_item_id

  
FROM (((((((((((((((
		`dw_flat`.`dw_invoice_lines` `ol`
    JOIN 
		`dw_flat`.`dw_invoice_headers` `inv` ON ((`inv`.`invoice_id` = `ol`.`invoice_id`)))
    JOIN 
		`dw_flat`.`dw_order_headers` `oh` ON ((`oh`.`order_id` = `inv`.`order_id`)))
    JOIN 
		`dw_flat`.`invoice_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_order_lines` `ol2` ON ((`ol2`.`item_id` = `ol`.`order_line_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
    LEFT JOIN 
		`magento01`.`vw_sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
    LEFT JOIN 
		`magento01`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
    LEFT JOIN 
		`magento01`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
    JOIN 
		`dw_flat`.`invoice_headers_vat` `ov` ON ((`ov`.`invoice_id` = `inv`.`invoice_id`)))
    LEFT JOIN 
		`magento01`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
    LEFT JOIN 
		`dw_flat`.`dw_invoice_calender_dates` `ic` ON ((`ic`.`invoice_id` = `inv`.`invoice_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
    LEFT JOIN 
		`dw_flat`.`edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`) AND (`esi`.`product_code` = `ol`.`sku`))))
    LEFT JOIN 
		`dw_flat`.`dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	LEFT JOIN 
		`dw_flat`.`dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
    LEFT JOIN  
		magento01.sales_flat_order_item sfoi ON  sfoi.item_id=ol2.item_id
    LEFT JOIN 
		magento01.po_sales_pla_item pla ON pla.order_item_id=ol.order_line_id ;

CREATE OR REPLACE VIEW `dw_flat`.`vw_invoice_lines_creditmemo` AS 

SELECT
  `ol`.`item_id`                      AS `line_id`,
  `oh`.`order_id`                     AS `order_id`,
  `cw`.`store_name`                   AS `store_name`,
  `oh`.`order_no`                     AS `order_no`,
  'CREDITMEMO'                        AS `document_type`,
  `oh2`.`created_at`                  AS `document_date`,
  `oh2`.`updated_at`                  AS `document_updated_at`,
  `oh2`.`creditmemo_id`               AS `document_id`,
  `oh`.`created_at`                   AS `order_date`,
  `oh`.`updated_at`                   AS `updated_at`,
  `ol`.`product_id`                   AS `product_id`,
  `cf`.`product_type`                 AS `product_type`,
  CAST(`ol2`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
  `p`.`is_lens`                       AS `is_lens`,
#CAST(CASE WHEN sfoi.lens_group_eye IN ('Right','Left') THEN LEFT(sfoi.lens_group_eye,1) ELSE '' END AS CHAR(1) CHARSET latin1) AS `lens_eye`,
  CAST((CASE
	WHEN INSTR(`ol2`.`product_options`,'s:5:"label";s:3:"Eye";s:5:"value";s:5:"Right"')>0 THEN 'R'
	WHEN INSTR(`ol2`.`product_options`,'s:5:"label";s:3:"Eye";s:5:"value";s:4:"Left"')>0 THEN 'L'
	ELSE '' END) AS CHAR(1) CHARSET LATIN1) as `lens_eye`,

  `esi`.`BC`                          AS `lens_base_curve`,
  `esi`.`DI`                          AS `lens_diameter`,
  `esi`.`PO`                          AS `lens_power`,
  `esi`.`CY`                          AS `lens_cylinder`,
  `esi`.`AX`                          AS `lens_axis`,
  `esi`.`AD`                          AS `lens_addition`,
  `esi`.`DO`                          AS `lens_dominance`,
  `esi`.`CO`                          AS `lens_colour`,
  `p`.`daysperlens`                   AS `lens_days`,
  CAST('' AS CHAR(1) CHARSET latin1)  AS `product_size`,
  CAST('' AS CHAR(1) CHARSET latin1)  AS `product_colour`,
  (-(1) * `ol2`.`weight`)             AS `unit_weight`,
  ((-(1) * `ol2`.`weight`) * `ol`.`qty`) AS `line_weight`,
  `ol2`.`is_virtual`                  AS `is_virtual`,
  `ol`.`sku`                          AS `sku`,
  `ol`.`name`                         AS `name`,
  `pcf`.`category_id`                 AS `category_id`,
  (CASE WHEN (`oh`.`base_shipping_amount` > 0) THEN 1 ELSE 0 END) AS `free_shipping`,
  (CASE WHEN (`oh`.`base_discount_amount` > 0) THEN 1 ELSE 0 END) AS `no_discount`,
  (-(1) * `ol`.`qty`)                 AS `qty_ordered`,
  (-(1) * `olv`.`local_prof_fee`)     AS `local_prof_fee`,
  `olv`.`local_price_inc_vat`         AS `local_price_inc_vat`,
  `olv`.`local_price_vat`             AS `local_price_vat`,
  `olv`.`local_price_exc_vat`         AS `local_price_exc_vat`,
  (-(1) * `olv`.`local_line_subtotal_inc_vat`) AS `local_line_subtotal_inc_vat`,
  (-(1) * `olv`.`local_line_subtotal_vat`) AS `local_line_subtotal_vat`,
  (-(1) * `olv`.`local_line_subtotal_exc_vat`) AS `local_line_subtotal_exc_vat`,
  (-(1) * `olv`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
  (-(1) * `olv`.`local_shipping_vat`) AS `local_shipping_vat`,
  (-(1) * `olv`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
  ABS(`olv`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
  ABS(`olv`.`local_discount_vat`)     AS `local_discount_vat`,
  ABS(`olv`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
  ABS(`olv`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
  ABS(`olv`.`local_store_credit_vat`) AS `local_store_credit_vat`,
  ABS(`olv`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
  (-(1) * `olv`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
  (-(1) * `olv`.`local_adjustment_vat`) AS `local_adjustment_vat`,
  (-(1) * `olv`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
  (-(1) * `olv`.`local_line_total_inc_vat`) AS `local_line_total_inc_vat`,
  (-(1) * `olv`.`local_line_total_vat`) AS `local_line_total_vat`,
  (-(1) * `olv`.`local_line_total_exc_vat`) AS `local_line_total_exc_vat`,
  (-(1) * `olv`.`local_subtotal_cost`) AS `local_subtotal_cost`,
  (-(1) * `olv`.`local_shipping_cost`) AS `local_shipping_cost`,
  (-(1) * `olv`.`local_total_cost`)   AS `local_total_cost`,
  (-(1) * `olv`.`local_margin_amount`) AS `local_margin_amount`,
  `olv`.`local_margin_percent`        AS `local_margin_percent`,
  `olv`.`local_to_global_rate`        AS `local_to_global_rate`,
  `olv`.`order_currency_code`         AS `order_currency_code`,
  (-(1) * `olv`.`global_prof_fee`)    AS `global_prof_fee`,
  `olv`.`global_price_inc_vat`        AS `global_price_inc_vat`,
  `olv`.`global_price_vat`            AS `global_price_vat`,
  `olv`.`global_price_exc_vat`        AS `global_price_exc_vat`,
  (-(1) * `olv`.`global_line_subtotal_inc_vat`) AS `global_line_subtotal_inc_vat`,
  (-(1) * `olv`.`global_line_subtotal_vat`) AS `global_line_subtotal_vat`,
  (-(1) * `olv`.`global_line_subtotal_exc_vat`) AS `global_line_subtotal_exc_vat`,
  (-(1) * `olv`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
  (-(1) * `olv`.`global_shipping_vat`) AS `global_shipping_vat`,
  (-(1) * `olv`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
  ABS(`olv`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
  ABS(`olv`.`global_discount_vat`)    AS `global_discount_vat`,
  ABS(`olv`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
  ABS(`olv`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
  ABS(`olv`.`global_store_credit_vat`) AS `global_store_credit_vat`,
  ABS(`olv`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
  (-(1) * `olv`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
  (-(1) * `olv`.`global_adjustment_vat`) AS `global_adjustment_vat`,
  (-(1) * `olv`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
  (-(1) * `olv`.`global_line_total_inc_vat`) AS `global_line_total_inc_vat`,
  (-(1) * `olv`.`global_line_total_vat`) AS `global_line_total_vat`,
  (-(1) * `olv`.`global_line_total_exc_vat`) AS `global_line_total_exc_vat`,
  (-(1) * `olv`.`global_subtotal_cost`) AS `global_subtotal_cost`,
  (-(1) * `olv`.`global_shipping_cost`) AS `global_shipping_cost`,
  (-(1) * `olv`.`global_total_cost`)  AS `global_total_cost`,
  (-(1) * `olv`.`global_margin_amount`) AS `global_margin_amount`,
  `olv`.`global_margin_percent`       AS `global_margin_percent`,
  `olv`.`prof_fee_percent`            AS `prof_fee_percent`,
  `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
  `olv`.`vat_percent`                 AS `vat_percent`,
  ABS(`olv`.`discount_percent`)       AS `discount_percent`,
  GREATEST(`ol`.`dw_synced_at`,`ol2`.`dw_synced_at`,`oh2`.`dw_synced_at`,`oh`.`dw_synced_at`) AS `dw_synced_at`,
  CAST(CONCAT('CREDITMEMO',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) AS `unique_id`,
  pla.order_item_id AS pla_order_item_id

FROM (((((((((
		`dw_creditmemo_lines` `ol`
    LEFT JOIN 
		`dw_order_lines` `ol2` ON ((`ol`.`order_line_id` = `ol2`.`item_id`)))
    JOIN 
		`dw_creditmemo_headers` `oh2` ON ((`oh2`.`creditmemo_id` = `ol`.`creditmemo_id`)))
    JOIN 
		`dw_order_headers` `oh` ON ((`oh`.`order_id` = `oh2`.`order_id`)))
    LEFT JOIN 
		`dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
    LEFT JOIN 
		`dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
    LEFT JOIN 
		`edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`) AND (`esi`.`product_code` = `ol`.`sku`))))
    JOIN 
		`creditmemo_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
    LEFT JOIN 
		`dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	LEFT JOIN 
		`dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
	LEFT JOIN  
		magento01.sales_flat_order_item sfoi ON  sfoi.item_id=ol2.item_id 
	LEFT JOIN 
		magento01.po_sales_pla_item pla
ON pla.order_item_id=ol.order_line_id;