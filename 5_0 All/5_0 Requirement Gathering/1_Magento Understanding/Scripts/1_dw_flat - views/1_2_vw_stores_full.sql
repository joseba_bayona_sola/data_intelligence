ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_stores_full` AS 
select 
	`dw_stores`.`store_id` AS `store_id`,
	`dw_stores`.`store_name` AS `store_name`,`dw_stores`.`website_group` AS `website_group`,
	`dw_stores`.`store_type` AS `store_type`,
	`dw_stores`.`visible` AS `visible` 
from `dw_stores`;
