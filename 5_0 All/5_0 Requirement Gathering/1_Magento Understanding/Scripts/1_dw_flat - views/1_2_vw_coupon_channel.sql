ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_coupon_channel` AS 
select 
	`sc`.`code` AS `coupon_code`,
	`sr`.`from_date` AS `start_date`,`sr`.`to_date` AS `end_date`,
	coalesce(NULL,'') AS `source_code`,
	`sr`.`channel` AS `channel`,
	`sr`.`postoptics_only_new_customer` AS `new_customer`,
	cast(`sr`.`description` as char(4000) charset utf8) AS `description` 
	
from (
		`magento01`.`salesrule_coupon` `sc` 
	join 
		`magento01`.`salesrule` `sr` on((`sc`.`rule_id` = `sr`.`rule_id`)));
