ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `order_line_order_test` AS 
select 
	`ol`.`item_id` AS `line_id`,
	`ol`.`order_id` AS `order_id`,`cw`.`store_name` AS `store_name`,`oh`.`order_no` AS `order_no`,
	cast('ORDER' as char(25) charset latin1) AS `document_type`,
	`oh`.`created_at` AS `document_date`,`oh`.`updated_at` AS `document_updated_at`,
	`oh`.`order_id` AS `document_id`,`oh`.`created_at` AS `order_date`,`oh`.`updated_at` AS `updated_at`,
	cast(`ol`.`product_id` as decimal(13,4)) AS `product_id`,`cf`.`product_type` AS `product_type`,
	cast(`ol`.`product_options` as char(8000) charset latin1) AS `product_options`,
	`p`.`is_lens` AS `is_lens`,
	cast((case 
		when (locate('s:5:"label";s:3:"Eye";s:5:"value";s:5:"Right"',`ol`.`product_options`) > 0) then 'R' 
		when (locate('s:5:"label";s:3:"Eye";s:5:"value";s:4:"Left"',`ol`.`product_options`) > 0) then 'L' 
		else '' end) as char(1) charset latin1) AS `lens_eye`,
	`esi`.`BC` AS `lens_base_curve`,`esi`.`DI` AS `lens_diameter`,`esi`.`PO` AS `lens_power`,
	`esi`.`CY` AS `lens_cylinder`,`esi`.`AX` AS `lens_axis`,`esi`.`AD` AS `lens_addition`,`esi`.`DO` AS `lens_dominance`,`esi`.`CO` AS `lens_colour`,
	`p`.`daysperlens` AS `lens_days`,
	cast('' as char(1) charset latin1) AS `product_size`,cast('' as char(1) charset latin1) AS `product_colour`,
	cast(`ol`.`weight` as decimal(13,4)) AS `unit_weight`,cast(`ol`.`row_weight` as decimal(13,4)) AS `line_weight`,
	`ol`.`is_virtual` AS `is_virtual`,`ol`.`sku` AS `sku`,`ol`.`name` AS `name`,
	`pcf`.`category_id` AS `category_id`,
	(case when (cast(`oh`.`base_shipping_amount` as decimal(13,4)) > 0) then 1 else 0 end) AS `free_shipping`,
	(case when (cast(`oh`.`base_discount_amount` as decimal(13,4)) > 0) then 1 else 0 end) AS `no_discount`,
	cast(`ol`.`qty_ordered` as decimal(13,4)) AS `qty`,
	cast(`olv`.`local_prof_fee` as decimal(13,4)) AS `local_prof_fee`,
	cast(`olv`.`local_price_inc_vat` as decimal(12,4)) AS `local_price_inc_vat`,cast(`olv`.`local_price_vat` as decimal(12,4)) AS `local_price_vat`,cast(`olv`.`local_price_exc_vat` as decimal(12,4)) AS `local_price_exc_vat`,
	cast(`olv`.`local_line_subtotal_inc_vat` as decimal(13,4)) AS `local_line_subtotal_inc_vat`,cast(`olv`.`local_line_subtotal_vat` as decimal(13,4)) AS `local_line_subtotal_vat`,cast(`olv`.`local_line_subtotal_exc_vat` as decimal(13,4)) AS `local_line_subtotal_exc_vat`,
	cast(`olv`.`local_shipping_inc_vat` as decimal(13,4)) AS `local_shipping_inc_vat`,cast(`olv`.`local_shipping_vat` as decimal(13,4)) AS `local_shipping_vat`,cast(`olv`.`local_shipping_exc_vat` as decimal(13,4)) AS `local_shipping_exc_vat`,
	cast((-(1) * abs(`olv`.`local_discount_inc_vat`)) as decimal(13,4)) AS `local_discount_inc_vat`,cast((-(1) * abs(`olv`.`local_discount_vat`)) as decimal(13,4)) AS `local_discount_vat`,cast((-(1) * abs(`olv`.`local_discount_exc_vat`)) as decimal(13,4)) AS `local_discount_exc_vat`,
	cast((-(1) * abs(`olv`.`local_store_credit_inc_vat`)) as decimal(13,4)) AS `local_store_credit_inc_vat`,cast((-(1) * abs(`olv`.`local_store_credit_vat`)) as decimal(13,4)) AS `local_store_credit_vat`,cast((-(1) * abs(`olv`.`local_store_credit_exc_vat`)) as decimal(13,4)) AS `local_store_credit_exc_vat`,
	cast(NULL as decimal(12,4)) AS `local_adjustment_inc_vat`,cast(NULL as decimal(12,4)) AS `local_adjustment_vat`,cast(NULL as decimal(12,4)) AS `local_adjustment_exc_vat`,
	cast(`olv`.`local_line_total_inc_vat` as decimal(13,4)) AS `local_line_total_inc_vat`,cast(`olv`.`local_line_total_vat` as decimal(13,4)) AS `local_line_total_vat`,cast(`olv`.`local_line_total_exc_vat` as decimal(13,4)) AS `local_line_total_exc_vat`,
	cast(`olv`.`local_subtotal_cost` as decimal(13,4)) AS `local_subtotal_cost`,cast(`olv`.`local_shipping_cost` as decimal(13,4)) AS `local_shipping_cost`,
	cast(`olv`.`local_total_cost` as decimal(13,4)) AS `local_total_cost`,
	cast(`olv`.`local_margin_amount` as decimal(13,4)) AS `local_margin_amount`,`olv`.`local_margin_percent` AS `local_margin_percent`,
	`olv`.`local_to_global_rate` AS `local_to_global_rate`,`olv`.`order_currency_code` AS `order_currency_code`,
	
	cast(`olv`.`global_prof_fee` as decimal(13,4)) AS `global_prof_fee`,
	cast(`olv`.`global_price_inc_vat` as decimal(13,4)) AS `global_price_inc_vat`,cast(`olv`.`global_price_vat` as decimal(13,4)) AS `global_price_vat`,cast(`olv`.`global_price_exc_vat` as decimal(13,4)) AS `global_price_exc_vat`,
	cast(`olv`.`global_line_subtotal_inc_vat` as decimal(13,4)) AS `global_line_subtotal_inc_vat`,cast(`olv`.`global_line_subtotal_vat` as decimal(13,4)) AS `global_line_subtotal_vat`,cast(`olv`.`global_line_subtotal_exc_vat` as decimal(13,4)) AS `global_line_subtotal_exc_vat`,
	cast(`olv`.`global_shipping_inc_vat` as decimal(13,4)) AS `global_shipping_inc_vat`,cast(`olv`.`global_shipping_vat` as decimal(13,4)) AS `global_shipping_vat`,cast(`olv`.`global_shipping_exc_vat` as decimal(13,4)) AS `global_shipping_exc_vat`,
	cast((-(1) * abs(`olv`.`global_discount_inc_vat`)) as decimal(13,4)) AS `global_discount_inc_vat`,cast((-(1) * abs(`olv`.`global_discount_vat`)) as decimal(13,4)) AS `global_discount_vat`,cast((-(1) * abs(`olv`.`global_discount_exc_vat`)) as decimal(13,4)) AS `global_discount_exc_vat`,
	cast((-(1) * abs(`olv`.`global_store_credit_inc_vat`)) as decimal(13,4)) AS `global_store_credit_inc_vat`,cast((-(1) * abs(`olv`.`global_store_credit_vat`)) as decimal(13,4)) AS `global_store_credit_vat`,cast((-(1) * abs(`olv`.`global_store_credit_exc_vat`)) as decimal(13,4)) AS `global_store_credit_exc_vat`,
	cast(NULL as decimal(12,4)) AS `global_adjustment_inc_vat`,cast(NULL as decimal(12,4)) AS `global_adjustment_vat`,cast(NULL as decimal(12,4)) AS `global_adjustment_exc_vat`,
	cast(`olv`.`global_line_total_inc_vat` as decimal(13,4)) AS `global_line_total_inc_vat`,cast(`olv`.`global_line_total_vat` as decimal(13,4)) AS `global_line_total_vat`,cast(`olv`.`global_line_total_exc_vat` as decimal(13,4)) AS `global_line_total_exc_vat`,
	cast(`olv`.`global_subtotal_cost` as decimal(13,4)) AS `global_subtotal_cost`,cast(`olv`.`global_shipping_cost` as decimal(13,4)) AS `global_shipping_cost`,
	cast(`olv`.`global_total_cost` as decimal(13,4)) AS `global_total_cost`,
	cast(`olv`.`global_margin_amount` as decimal(13,4)) AS `global_margin_amount`,`olv`.`global_margin_percent` AS `global_margin_percent`,
	`olv`.`prof_fee_percent` AS `prof_fee_percent`,	`olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	`olv`.`vat_percent` AS `vat_percent`,abs(`olv`.`discount_percent`) AS `discount_percent`,
	`pla`.`order_item_id` AS `pla_order_item_id` 
from (((((((((
		`dw_flat`.`dw_order_lines` `ol` 
	join 
		`dw_flat`.`dw_order_headers` `oh` on((`oh`.`order_id` = `ol`.`order_id`))) 
	left join 
		`dw_flat`.`dw_stores` `cw` on((`cw`.`store_id` = `oh`.`store_id`))) 
	left join 
		`dw_flat`.`dw_products` `p` on((`p`.`product_id` = `ol`.`product_id`))) 
	left join 
		`dw_flat`.`edi_stock_item_agg` `esi` on(((`esi`.`product_id` = `ol`.`product_id`) and (`esi`.`product_code` = `ol`.`sku`)))) 
	join 
		`dw_flat`.`order_lines_vat` `olv` on((`olv`.`item_id` = `ol`.`item_id`))) 
	left join 
		`dw_flat`.`dw_product_category_flat` `pcf` on((`pcf`.`product_id` = `ol`.`product_id`))) 
	left join 
		`dw_flat`.`dw_flat_category` `cf` on((`cf`.`category_id` = `pcf`.`category_id`))) 
	left join 
		`magento01`.`sales_flat_order_item` `sfoi` on((`sfoi`.`item_id` = `ol`.`item_id`))) 
	left join 
		`magento01`.`po_sales_pla_item` `pla` on((`pla`.`order_item_id` = `ol`.`item_id`))) 
where (`oh`.`status` <> 'archived');
