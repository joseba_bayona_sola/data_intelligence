ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_invoice_lines_invoice` AS 
select 
	`ol`.`item_id` AS `line_id`,
	`ol`.`invoice_id` AS `invoice_id`,`cw`.`store_name` AS `store_name`,`inv`.`invoice_no` AS `invoice_no`,
	cast('INVOICE' as char(25) charset latin1) AS `document_type`,
	`inv`.`created_at` AS `document_date`,`inv`.`updated_at` AS `document_updated_at`,
	`inv`.`invoice_id` AS `document_id`,`inv`.`created_at` AS `invoice_date`,`inv`.`updated_at` AS `updated_at`,
	`ol`.`product_id` AS `product_id`,`cf`.`product_type` AS `product_type`,
	cast(`ol2`.`product_options` as char(8000) charset latin1) AS `product_options`,
	`p`.`is_lens` AS `is_lens`,
	cast((case 
		when (locate('s:5:"label";s:3:"Eye";s:5:"value";s:5:"Right"',`ol2`.`product_options`) > 0) then 'R' 
		when (locate('s:5:"label";s:3:"Eye";s:5:"value";s:4:"Left"',`ol2`.`product_options`) > 0) then 'L' 
		else '' end) as char(1) charset latin1) AS `lens_eye`,
	`esi`.`BC` AS `lens_base_curve`,`esi`.`DI` AS `lens_diameter`,`esi`.`PO` AS `lens_power`,
	`esi`.`CY` AS `lens_cylinder`,`esi`.`AX` AS `lens_axis`,`esi`.`AD` AS `lens_addition`,`esi`.`DO` AS `lens_dominance`,`esi`.`CO` AS `lens_colour`,
	`p`.`daysperlens` AS `lens_days`,
	cast('' as char(1) charset latin1) AS `product_size`,cast('' as char(1) charset latin1) AS `product_colour`,
	`ol2`.`weight` AS `unit_weight`,(`ol2`.`weight` * `ol`.`qty`) AS `line_weight`,
	`ol2`.`is_virtual` AS `is_virtual`,`ol`.`sku` AS `sku`,`ol`.`name` AS `name`,
	`pcf`.`category_id` AS `category_id`,
	(case when (`oh`.`base_shipping_amount` > 0) then 1 else 0 end) AS `free_shipping`,
	(case when (`oh`.`base_discount_amount` > 0) then 1 else 0 end) AS `no_discount`,
	`ol`.`qty` AS `qty`,
	`olv`.`local_prof_fee` AS `local_prof_fee`,
	`olv`.`local_price_inc_vat` AS `local_price_inc_vat`,`olv`.`local_price_vat` AS `local_price_vat`,`olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	`olv`.`local_line_subtotal_inc_vat` AS `local_line_subtotal_inc_vat`,`olv`.`local_line_subtotal_vat` AS `local_line_subtotal_vat`,`olv`.`local_line_subtotal_exc_vat` AS `local_line_subtotal_exc_vat`,
	`olv`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,`olv`.`local_shipping_vat` AS `local_shipping_vat`,`olv`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
	(-(1) * abs(`olv`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,(-(1) * abs(`olv`.`local_discount_vat`)) AS `local_discount_vat`,(-(1) * abs(`olv`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
	(-(1) * abs(`olv`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,(-(1) * abs(`olv`.`local_store_credit_vat`)) AS `local_store_credit_vat`,(-(1) * abs(`olv`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
	`olv`.`local_adjustment_inc_vat` AS `local_adjustment_inc_vat`,`olv`.`local_adjustment_vat` AS `local_adjustment_vat`,`olv`.`local_adjustment_exc_vat` AS `local_adjustment_exc_vat`,
	`olv`.`local_line_total_inc_vat` AS `local_line_total_inc_vat`,`olv`.`local_line_total_vat` AS `local_line_total_vat`,`olv`.`local_line_total_exc_vat` AS `local_line_total_exc_vat`,
	`olv`.`local_subtotal_cost` AS `local_subtotal_cost`,`olv`.`local_shipping_cost` AS `local_shipping_cost`,
	`olv`.`local_total_cost` AS `local_total_cost`,
	`olv`.`local_margin_amount` AS `local_margin_amount`,`olv`.`local_margin_percent` AS `local_margin_percent`,
	`olv`.`local_to_global_rate` AS `local_to_global_rate`,`olv`.`order_currency_code` AS `order_currency_code`,
	
	`olv`.`global_prof_fee` AS `global_prof_fee`,
	`olv`.`global_price_inc_vat` AS `global_price_inc_vat`,`olv`.`global_price_vat` AS `global_price_vat`,`olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	`olv`.`global_line_subtotal_inc_vat` AS `global_line_subtotal_inc_vat`,`olv`.`global_line_subtotal_vat` AS `global_line_subtotal_vat`,`olv`.`global_line_subtotal_exc_vat` AS `global_line_subtotal_exc_vat`,
	`olv`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,`olv`.`global_shipping_vat` AS `global_shipping_vat`,`olv`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
	(-(1) * abs(`olv`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,(-(1) * abs(`olv`.`global_discount_vat`)) AS `global_discount_vat`,(-(1) * abs(`olv`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
	(-(1) * abs(`olv`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,(-(1) * abs(`olv`.`global_store_credit_vat`)) AS `global_store_credit_vat`,(-(1) * abs(`olv`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
	`olv`.`global_adjustment_inc_vat` AS `global_adjustment_inc_vat`,`olv`.`global_adjustment_vat` AS `global_adjustment_vat`,`olv`.`global_adjustment_exc_vat` AS `global_adjustment_exc_vat`,
	`olv`.`global_line_total_inc_vat` AS `global_line_total_inc_vat`,`olv`.`global_line_total_vat` AS `global_line_total_vat`,`olv`.`global_line_total_exc_vat` AS `global_line_total_exc_vat`,
	`olv`.`global_subtotal_cost` AS `global_subtotal_cost`,`olv`.`global_shipping_cost` AS `global_shipping_cost`,
	`olv`.`global_total_cost` AS `global_total_cost`,
	`olv`.`global_margin_amount` AS `global_margin_amount`,`olv`.`global_margin_percent` AS `global_margin_percent`,
	`olv`.`prof_fee_percent` AS `prof_fee_percent`,`olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	`olv`.`vat_percent` AS `vat_percent`,abs(`olv`.`discount_percent`) AS `discount_percent`,
	greatest(`ol`.`dw_synced_at`,`inv`.`dw_synced_at`,`oh`.`dw_synced_at`,`ol2`.`dw_synced_at`) AS `dw_synced_at`,
	cast(concat('INVOICE',`ol`.`item_id`) as char(25) charset latin1) AS `unique_id`,
	`pla`.`order_item_id` AS `pla_order_item_id` 

from (((((((((((((((((
		`dw_flat`.`dw_invoice_lines` `ol` 
	join 
		`dw_flat`.`dw_invoice_headers` `inv` on((`inv`.`invoice_id` = `ol`.`invoice_id`))) 
	join 
		`dw_flat`.`dw_order_headers` `oh` on((`oh`.`order_id` = `inv`.`order_id`))) 
	join 
		`dw_flat`.`invoice_lines_vat` `olv` on((`olv`.`item_id` = `ol`.`item_id`))) 
	left join 
		`dw_flat`.`dw_order_lines` `ol2` on((`ol2`.`item_id` = `ol`.`order_line_id`))) 
	left join 
		`dw_flat`.`dw_stores` `cw` on((`cw`.`store_id` = `oh`.`store_id`))) 
	left join 
		`magento01`.`vw_sales_flat_order_payment` `op` on((`op`.`parent_id` = `oh`.`order_id`))) 
	left join 
		`magento01`.`sales_flat_order_address` `bill` on((`bill`.`entity_id` = `oh`.`billing_address_id`))) 
	left join 
		`magento01`.`sales_flat_order_address` `ship` on((`ship`.`entity_id` = `oh`.`shipping_address_id`))) 
	join 
		`dw_flat`.`invoice_headers_vat` `ov` on((`ov`.`invoice_id` = `inv`.`invoice_id`))) 
	left join 
		`magento01`.`po_reorder_profile` `rp` on((`oh`.`reorder_profile_id` = `rp`.`id`))) 
	left join 
		`dw_flat`.`dw_invoice_calender_dates` `ic` on((`ic`.`invoice_id` = `inv`.`invoice_id`))) 
	left join 
		`dw_flat`.`dw_products` `p` on((`p`.`product_id` = `ol`.`product_id`))) 
	left join 
		`dw_flat`.`edi_stock_item_agg` `esi` on(((`esi`.`product_id` = `ol`.`product_id`) and (`esi`.`product_code` = `ol`.`sku`)))) 
	left join 
		`dw_flat`.`dw_product_category_flat` `pcf` on((`pcf`.`product_id` = `ol`.`product_id`))) 
	left join 
		`dw_flat`.`dw_flat_category` `cf` on((`cf`.`category_id` = `pcf`.`category_id`))) 
	left join 
		`magento01`.`sales_flat_order_item` `sfoi` on((`sfoi`.`item_id` = `ol2`.`item_id`))) 
	left join 
		`magento01`.`po_sales_pla_item` `pla` on((`pla`.`order_item_id` = `ol`.`order_line_id`)));
