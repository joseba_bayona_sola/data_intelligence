
CREATE OR REPLACE VIEW `dw_flat`.`vw_customers` AS 

SELECT
  `c`.`customer_id`                  AS `customer_id`,
  `c`.`email`                        AS `email`,
  `ds`.`store_name`                  AS `store_name`,
  `c`.`created_at`                   AS `created_at`,
  `c`.`updated_at`                   AS `updated_at`,
  `ds`.`website_group`               AS `website_group`,
  (CASE WHEN (`c`.`old_access_cust_no` = 'LENSWAYUK') THEN 'lensway.co.uk' ELSE COALESCE(`cim`.`new_customer_created_in`,`ds2`.`store_name`,`ds`.`store_name`) END) AS `created_in`,
  `c`.`prefix`                       AS `prefix`,
  `c`.`firstname`                    AS `firstname`,
  `c`.`middlename`                   AS `middlename`,
  `c`.`lastname`                     AS `lastname`,
  `c`.`suffix`                       AS `suffix`,
  `c`.`taxvat`                       AS `taxvat`,
  `c`.`postoptics_send_post`         AS `postoptics_send_post`,
  `c`.`facebook_id`                  AS `facebook_id`,
  CAST(`c`.`facebook_permissions` AS CHAR(255) CHARSET latin1) AS `facebook_permissions`,
  CAST('' AS CHAR(1) CHARSET latin1) AS `gender`,
  `c`.`dob`                          AS `dob`,
  `c`.`unsubscribe_all`              AS `unsubscribe_all`,
  `days_worn`.`value`                AS `days_worn`,
  `c`.`parent_customer_id`           AS `parent_customer_id`,
  `c`.`is_parent_customer`           AS `is_parent_customer`,
  `c`.`eyeplan_credit_limit`         AS `eyeplan_credit_limit`,
  `c`.`eyeplan_approved_flag`        AS `eyeplan_approved_flag`,
  `c`.`eyeplan_can_ref_new_customer` AS `eyeplan_can_ref_new_customer`,
  `c`.`cus_phone`                    AS `cus_phone`,
  `c`.`found_us_info`                AS `found_us_info`,
  `c`.`password_hash`                AS `password_hash`,
  `c`.`referafriend_code`            AS `referafriend_code`,
  `c`.`alternate_email`              AS `alternate_email`,
  `c`.`old_access_cust_no`           AS `old_access_cust_no`,
  `c`.`old_web_cust_no`              AS `old_web_cust_no`,
  `c`.`old_customer_id`              AS `old_customer_id`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin1`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin2`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin3`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin4`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin5`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `language`,
  `ohma`.`first_order_date`          AS `first_order_date`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `last_logged_in_date`,
  `ohma`.`last_order_date`           AS `last_order_date`,
  `tord`.`no_of_orders`              AS `num_of_orders`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `card_expiry_date`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifecycle`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_usage`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_geog`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_purch_behaviour`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_eysight`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_sport`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_professional`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifestage`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_vanity`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_2`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_3`,
  CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_4`,
  `c`.`default_billing`              AS `default_billing`,
  `c`.`default_shipping`             AS `default_shipping`,
  `ochan`.`channel`                  AS `business_channel`,
  `c`.`website_id`                   AS `website_id`,
  `c`.`dw_synced_at`                 AS `dw_synced_at`,
  `c`.`customer_id`                  AS `unique_id`,
  `c`.`unsubscribe_all_date`         AS `unsubscribe_all_date`

FROM (((((((((
		`dw_flat`.`dw_customers` `c`
    LEFT JOIN 
		`dw_flat`.`dw_stores` `ds` ON ((`ds`.`store_id` = `c`.`store_id`)))
    LEFT JOIN 
		`magento01`.`eav_attribute_option_value` `days_worn` ON (((`days_worn`.`option_id` = `c`.`days_worn_info`) AND (`days_worn`.`store_id` = 0))))
    LEFT JOIN 
		`dw_flat`.`customer_created_in_map` `cim` ON ((`cim`.`customer_created_in` = `c`.`created_in`)))
    LEFT JOIN 
		`dw_flat`.`customer_first_order` `fo` ON ((`fo`.`customer_id` = `c`.`customer_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_order_headers` `oh` ON ((`oh`.`order_id` = `fo`.`order_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_stores` `ds2` ON ((`oh`.`store_id` = `ds2`.`store_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))
    LEFT JOIN 
		`dw_flat`.`dw_order_headers_marketing_agg` `ohma` ON ((`ohma`.`customer_id` = `c`.`customer_id`)))
	LEFT JOIN 
		`dw_flat`.`dw_customer_total_orders` `tord` ON ((`tord`.`customer_id` = `c`.`customer_id`)))
WHERE (`c`.`website_group_id` <> 100)


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_wearer` AS 
  select `magento01`.`wearer`.`id` AS `id`,`magento01`.`wearer`.`name` AS `name`,
    (case when (`magento01`.`wearer`.`DOB` = '0000-00-00 00:00:00') then NULL else `magento01`.`wearer`.`DOB` end) AS `DOB`,
    `magento01`.`wearer`.`customer_id` AS `customer_id` 
  from `magento01`.`wearer`;


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_wearer_prescription` AS 
  select `id` AS `id`,`wearer_id` AS `wearer_id`,`method_id` AS `method_id`,
    (case when (`date_last_prescription` = '0000-00-00 00:00:00') then NULL else `date_last_prescription` end) AS `date_last_prescription`,
    (case when (`date_start_prescription` = '0000-00-00 00:00:00') then NULL else `date_start_prescription` end) AS `date_start_prescription`,
    `optician_name` AS `optician_name`,`optician_address1` AS `optician_address1`,`optician_address2` AS `optician_address2`,
    `optician_city` AS `optician_city`,`optician_state` AS `optician_state`,`optician_country` AS `optician_country`,
    `optician_postcode` AS `optician_postcode`,`optician_phone` AS `optician_phone`,`optician_lookup_id` AS `optician_lookup_id`,
    `reminder_sent` AS `reminder_sent` 
  from `magento01`.`wearer_prescription` ``;


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_stores` AS 
  select `dw_stores`.`store_name` AS `store_name`,`dw_stores`.`website_group` AS `website_group`,
    `dw_stores`.`store_type` AS `store_type` 
  from `dw_stores` 
  where (`dw_stores`.`visible` = 1);

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_stores_full` AS 
  select `dw_stores`.`store_id` AS `store_id`,`dw_stores`.`store_name` AS `store_name`,`dw_stores`.`website_group` AS `website_group`,
    `dw_stores`.`store_type` AS `store_type`,`dw_stores`.`visible` AS `visible` 
  from `dw_stores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_categories` AS 
  select `dw_flat_category`.`category_id` AS `category_id`,`dw_flat_category`.`promotional_product` AS `promotional_product`,
    `dw_flat_category`.`modality` AS `modality`,`dw_flat_category`.`type` AS `type`,`dw_flat_category`.`feature` AS `feature`,
    `dw_flat_category`.`category` AS `category`,`dw_flat_category`.`product_type` AS `product_type` 
  from `dw_flat_category`;


CREATE OR REPLACE VIEW `dw_flat`.`vw_products` AS 

SELECT 
        `cw`.`store_name` AS `store_name`,
        `pas`.`product_id` AS `product_id`,
        `pas`.`status` AS `status`,
        `pcf`.`category_id` AS `category_id`,
        `pas`.`sku` AS `sku`,
        `br`.`value` AS `brand`,
        `pas`.`created_at` AS `created_at`,
        `pas`.`updated_at` AS `updated_at`,
        `cf`.`product_type` AS `product_type`,
        CAST(`pas`.`product_lifecycle` AS CHAR (255) CHARSET LATIN1) AS `product_lifecycle`,
        CAST(`pas`.`product_lifecycle_text` AS CHAR (8000) CHARSET LATIN1) AS `product_lifecycle_text`,
        `pas`.`telesales_only` AS `telesales_only`,
        `pas`.`stocked_lens` AS `stocked_lens`,
        `pas`.`tax_class_id` AS `tax_class_id`,
        `pas`.`name` AS `name`,
        CAST(`pas`.`description` AS CHAR (8000) CHARSET LATIN1) AS `description`,
        CAST(`pas`.`short_description` AS CHAR (8000) CHARSET LATIN1) AS `short_description`,
        CAST(`pas`.`cat_description` AS CHAR (8000) CHARSET LATIN1) AS `category_list_description`,
        `pas`.`cost` AS `average_cost`,
        `pas`.`weight` AS `weight`,
        `man`.`value` AS `manufacturer`,
        `pas`.`meta_title` AS `meta_title`,
        CAST(`pas`.`meta_keyword` AS CHAR (8000) CHARSET LATIN1) AS `meta_keyword`,
        `pas`.`meta_description` AS `meta_description`,
        `pas`.`image` AS `image`,
        `pas`.`small_image` AS `small_image`,
        `pas`.`thumbnail` AS `thumbnail`,
        `pas`.`url_key` AS `url_key`,
        `pas`.`url_path` AS `url_path`,
        `pas`.`visibility` AS `visibility`,
        `pas`.`image_label` AS `image_label`,
        `pas`.`small_image_label` AS `small_image_label`,
        `pas`.`thumbnail_label` AS `thumbnail_label`,
        `pas`.`equivalent_sku` AS `equivalent_sku`,
        `pas`.`daysperlens` AS `daysperlens`,
        `pas`.`altdaysperlens` AS `alt_daysperlens`,
        `pas`.`equivalence` AS `equivalence`,
        `pas`.`availability` AS `availability`,
        `pas`.`condition` AS `condition`,
        `pas`.`google_base_price` AS `google_base_price`,
        `pas`.`promotional_product` AS `promotional_product`,
        `pas`.`promotion_rule` AS `promotion_rule`,
        `pas`.`promotion_rule2` AS `promotion_rule2`,
        CAST(`pas`.`upsell_text` AS CHAR (8000) CHARSET LATIN1) AS `upsell_text`,
        `pas`.`replacement_sku` AS `replacement_sku`,
        `pas`.`price_comp_price` AS `price_comp_price`,
        `pas`.`google_feed_title_prefix` AS `google_feed_title_prefix`,
        `pas`.`google_feed_title_suffix` AS `google_feed_title_suffix`,
        `pas`.`autoreorder_alter_sku` AS `autoreorder_alter_sku`,
        `pas`.`rrp_price` AS `rrp_price`,
        CAST(`pas`.`promotional_text` AS CHAR (8000) CHARSET LATIN1) AS `promotional_text`,
        `pas`.`glasses_colour` AS `glasses_colour`,
        `pas`.`glasses_material` AS `glasses_material`,
        `pas`.`glasses_size` AS `glasses_size`,
        `pas`.`glasses_sex` AS `glasses_sex`,
        `pas`.`glasses_style` AS `glasses_style`,
        `pas`.`glasses_shape` AS `glasses_shape`,
        `pas`.`glasses_lens_width` AS `glasses_lens_width`,
        `pas`.`glasses_bridge_width` AS `glasses_bridge_width`,
        GREATEST(CAST(`pas`.`packsize` AS UNSIGNED), 1) AS `pack_qty`,
        `pas`.`packtext` AS `packtext`,
        (CASE
            WHEN (CAST(`pas`.`alt_packsize` AS UNSIGNED) > 0) THEN CAST(`pas`.`alt_packsize` AS UNSIGNED)
            ELSE NULL
        END) AS `alt_pack_qty`,
        `pq`.`first_tier_qty` AS `base_pack_qty`,
        (`pq`.`first_tier_qty` / GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))) AS `base_no_packs`,
        `dp1`.`local_unit_price_inc_vat` AS `local_base_unit_price_inc_vat`,
        `dp1`.`local_qty_price_inc_vat` AS `local_base_value_inc_vat`,
        `dp1`.`local_pack_price_inc_vat` AS `local_base_pack_price_inc_vat`,
        ROUND((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`),
                4) AS `local_base_unit_price_exc_vat`,
        `dp1`.`local_qty_price_exc_vat` AS `local_base_value_exc_vat`,
        CAST(ROUND(((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`) * GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))),
                    4)
            AS DECIMAL (12 , 4 )) AS `local_base_pack_price_exc_vat`,
        `dp1`.`local_prof_fee` AS `local_base_prof_fee`,
        `dp1`.`local_product_cost` AS `local_base_product_cost`,
        `dp1`.`local_shipping_cost` AS `local_base_shipping_cost`,
        `dp1`.`local_total_cost` AS `local_base_total_cost`,
        `dp1`.`local_margin_amount` AS `local_base_margin_amount`,
        `dp1`.`local_margin_percent` AS `local_base_margin_percent`,
        `dp1`.`global_unit_price_inc_vat` AS `global_base_unit_price_inc_vat`,
        `dp1`.`global_qty_price_inc_vat` AS `global_base_value_inc_vat`,
        `dp1`.`global_pack_price_inc_vat` AS `global_base_pack_price_inc_vat`,
        ROUND(((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`),
                4) AS `global_base_unit_price_exc_vat`,
        (`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) AS `global_base_value_exc_vat`,
        CAST(ROUND((((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`) * GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))),
                    4)
            AS DECIMAL (12 , 4 )) AS `global_base_pack_price_exc_vat`,
        `dp1`.`global_prof_fee` AS `global_base_prof_fee`,
        `dp1`.`global_product_cost` AS `global_base_product_cost`,
        `dp1`.`global_shipping_cost` AS `global_base_shipping_cost`,
        `dp1`.`global_total_cost` AS `global_base_total_cost`,
        `dp1`.`global_margin_amount` AS `global_base_margin_amount`,
        `dp1`.`global_margin_percent` AS `global_base_margin_percent`,
        `pq`.`multibuy_qty` AS `multi_pack_qty`,
        (`pq`.`multibuy_qty` / GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))) AS `multi_no_packs`,
        `dp2`.`local_unit_price_inc_vat` AS `local_multi_unit_price_inc_vat`,
        `dp2`.`local_qty_price_inc_vat` AS `local_multi_value_inc_vat`,
        `dp2`.`local_pack_price_inc_vat` AS `local_multi_pack_price_inc_vat`,
        ROUND((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`),
                4) AS `local_multi_unit_price_exc_vat`,
        `dp2`.`local_qty_price_exc_vat` AS `local_multi_value_exc_vat`,
        CAST(ROUND(((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`) * GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))),
                    4)
            AS DECIMAL (12 , 4 )) AS `local_multi_pack_price_exc_vat`,
        `dp2`.`local_prof_fee` AS `local_multi_prof_fee`,
        `dp2`.`local_product_cost` AS `local_multi_product_cost`,
        `dp2`.`local_shipping_cost` AS `local_multi_shipping_cost`,
        `dp2`.`local_total_cost` AS `local_multi_total_cost`,
        `dp2`.`local_margin_amount` AS `local_multi_margin_amount`,
        `dp2`.`local_margin_percent` AS `local_multi_margin_percent`,
        `dp2`.`global_unit_price_inc_vat` AS `global_multi_unit_price_inc_vat`,
        `dp2`.`global_qty_price_inc_vat` AS `global_multi_value_inc_vat`,
        `dp2`.`global_pack_price_inc_vat` AS `global_multi_pack_price_inc_vat`,
        ROUND(((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`),
                4) AS `global_multi_unit_price_exc_vat`,
        (`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) AS `global_multi_value_exc_vat`,
        CAST(ROUND((((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`) * GREATEST(1, CAST(`pas`.`packsize` AS UNSIGNED))),
                    4)
            AS DECIMAL (12 , 4 )) AS `global_multi_pack_price_exc_vat`,
        `dp2`.`global_prof_fee` AS `global_multi_prof_fee`,
        `dp2`.`global_product_cost` AS `global_multi_product_cost`,
        `dp2`.`global_shipping_cost` AS `global_multi_shipping_cost`,
        `dp2`.`global_total_cost` AS `global_multi_total_cost`,
        `dp2`.`global_margin_amount` AS `global_multi_margin_amount`,
        `dp2`.`global_margin_percent` AS `global_multi_margin_percent`,
        `dp1`.`local_to_global_rate` AS `local_to_global_rate`,
        `dp1`.`store_currency_code` AS `store_currency_code`,
        `dp1`.`prof_fee_percent` AS `prof_fee_percent`,
        `dp1`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
        `dp1`.`vat_percent` AS `vat_percent`,
        `pas`.`google_shopping_gtin` as `google_shopping_gtin`
    FROM
        (((((((((
			`dw_flat`.`dw_products_all_stores` `pas`
        JOIN 
			`dw_flat`.`vw_catalog_product_website` `cpw` ON (((`pas`.`product_id` = `cpw`.`product_id`) AND (`pas`.`store_id` = `cpw`.`website_id`))))
        LEFT JOIN 
			`dw_flat`.`dw_stores` `cw` ON ((`cw`.`store_id` = `pas`.`store_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `pas`.`product_id`)))
        LEFT JOIN 
			`dw_flat`.`dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
        LEFT JOIN 
			`magento01`.`eav_attribute_option_value` `man` ON ((`man`.`option_id` = `pas`.`manufacturer`)))
        LEFT JOIN 
			`magento01`.`eav_attribute_option_value` `br` ON ((`br`.`option_id` = `pas`.`brand`)))
        LEFT JOIN 
			`dw_flat`.`dw_product_price_qtys` `pq` ON (((`pq`.`product_id` = `pas`.`product_id`) AND (`pq`.`store_id` = `pas`.`store_id`))))
        LEFT JOIN 
			`dw_flat`.`dw_product_prices` `dp1` ON (((`dp1`.`product_id` = `pas`.`product_id`) AND (`dp1`.`store_id` = `pas`.`store_id`) AND (`pq`.`first_tier_qty` = `dp1`.`qty`))))
        LEFT JOIN 
			`dw_flat`.`dw_product_prices` `dp2` ON (((`dp2`.`product_id` = `pas`.`product_id`) AND (`dp2`.`store_id` = `pas`.`store_id`) AND (`pq`.`multibuy_qty` = `dp2`.`qty`))));

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_product_prices` AS 
  select `pas`.`price_id` AS `price_id`,`pas`.`store_name` AS `store_name`,`pas`.`product_id` AS `product_id`,
    `pas`.`qty` AS `qty`,`pas`.`pack_size` AS `pack_size`,
    `pas`.`local_unit_price_inc_vat` AS `local_unit_price_inc_vat`,`pas`.`local_qty_price_inc_vat` AS `local_qty_price_inc_vat`,`pas`.`local_pack_price_inc_vat` AS `local_pack_price_inc_vat`,`pas`.`local_prof_fee` AS `local_prof_fee`,`pas`.`local_product_cost` AS `local_product_cost`,`pas`.`local_shipping_cost` AS `local_shipping_cost`,`pas`.`local_total_cost` AS `local_total_cost`,`pas`.`local_margin_amount` AS `local_margin_amount`,`pas`.`local_margin_percent` AS `local_margin_percent`,`pas`.`local_to_global_rate` AS `local_to_global_rate`,`pas`.`store_currency_code` AS `store_currency_code`,`pas`.`global_unit_price_inc_vat` AS `global_unit_price_inc_vat`,`pas`.`global_qty_price_inc_vat` AS `global_qty_price_inc_vat`,`pas`.`global_pack_price_inc_vat` AS `global_pack_price_inc_vat`,`pas`.`global_prof_fee` AS `global_prof_fee`,`pas`.`global_product_cost` AS `global_product_cost`,`pas`.`global_shipping_cost` AS `global_shipping_cost`,`pas`.`global_total_cost` AS `global_total_cost`,`pas`.`global_margin_amount` AS `global_margin_amount`,`pas`.`global_margin_percent` AS `global_margin_percent`,`pas`.`prof_fee_percent` AS `prof_fee_percent`,`pas`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,`pas`.`vat_percent` AS `vat_percent` 
  from (
		`dw_flat`.`dw_product_prices` `pas` 
	join 
		`magento01`.`catalog_product_website` `cpw` on(((`pas`.`product_id` = `cpw`.`product_id`) and (`pas`.`store_id` = `cpw`.`website_id`))));

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_coupon_channel` AS 
  select `sc`.`code` AS `coupon_code`,`sr`.`from_date` AS `start_date`,`sr`.`to_date` AS `end_date`,coalesce(NULL,'') AS `source_code`,`sr`.`channel` AS `channel`,`sr`.`postoptics_only_new_customer` AS `new_customer`,cast(`sr`.`description` as char(4000) charset utf8) AS `description` 
  from (
      `magento01`.`salesrule_coupon` `sc` 
    join 
      `magento01`.`salesrule` `sr` on((`sc`.`rule_id` = `sr`.`rule_id`)));