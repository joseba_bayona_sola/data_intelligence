ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_stores` AS 
select 
	`dw_stores`.`store_name` AS `store_name`,`dw_stores`.`website_group` AS `website_group`,
	`dw_stores`.`store_type` AS `store_type` 
from `dw_stores` 
where (`dw_stores`.`visible` = 1);
