ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_customers` AS 
select 
	`c`.`customer_id` AS `customer_id`,
	`c`.`email` AS `email`,`ds`.`store_name` AS `store_name`,
	`c`.`created_at` AS `created_at`,`c`.`updated_at` AS `updated_at`,
	`ds`.`website_group` AS `website_group`,coalesce(`cim`.`new_customer_created_in`,`ds2`.`store_name`,`ds`.`store_name`) AS `created_in`,
	`c`.`prefix` AS `prefix`,`c`.`firstname` AS `firstname`,`c`.`middlename` AS `middlename`,`c`.`lastname` AS `lastname`,`c`.`suffix` AS `suffix`,
	`c`.`taxvat` AS `taxvat`,
	`c`.`postoptics_send_post` AS `postoptics_send_post`,
	`c`.`facebook_id` AS `facebook_id`,cast(`c`.`facebook_permissions` as char(255) charset latin1) AS `facebook_permissions`,
	cast('' as char(1) charset latin1) AS `gender`,`c`.`dob` AS `dob`,
	`c`.`unsubscribe_all` AS `unsubscribe_all`,`days_worn`.`value` AS `days_worn`,
	`c`.`parent_customer_id` AS `parent_customer_id`,`c`.`is_parent_customer` AS `is_parent_customer`,
	`c`.`eyeplan_credit_limit` AS `eyeplan_credit_limit`,`c`.`eyeplan_approved_flag` AS `eyeplan_approved_flag`,`c`.`eyeplan_can_ref_new_customer` AS `eyeplan_can_ref_new_customer`,
	`c`.`cus_phone` AS `cus_phone`,
	`c`.`found_us_info` AS `found_us_info`,`c`.`password_hash` AS `password_hash`,
	`c`.`referafriend_code` AS `referafriend_code`,`c`.`alternate_email` AS `alternate_email`,
	`c`.`old_access_cust_no` AS `old_access_cust_no`,`c`.`old_web_cust_no` AS `old_web_cust_no`,`c`.`old_customer_id` AS `old_customer_id`,
	cast(NULL as char(255) charset latin1) AS `emvadmin1`,cast(NULL as char(255) charset latin1) AS `emvadmin2`,cast(NULL as char(255) charset latin1) AS `emvadmin3`,
	cast(NULL as char(255) charset latin1) AS `emvadmin4`,cast(NULL as char(255) charset latin1) AS `emvadmin5`,
	cast(NULL as char(255) charset latin1) AS `language`,
	`ohma`.`first_order_date` AS `first_order_date`,
	cast(NULL as char(255) charset latin1) AS `last_logged_in_date`,`ohma`.`last_order_date` AS `last_order_date`,
	`tord`.`no_of_orders` AS `num_of_orders`,
	cast(NULL as char(255) charset latin1) AS `card_expiry_date`,
	cast(NULL as char(255) charset latin1) AS `segment_lifecycle`,cast(NULL as char(255) charset latin1) AS `segment_usage`,cast(NULL as char(255) charset latin1) AS `segment_geog`,
	cast(NULL as char(255) charset latin1) AS `segment_purch_behaviour`,cast(NULL as char(255) charset latin1) AS `segment_eysight`,cast(NULL as char(255) charset latin1) AS `segment_sport`,
	cast(NULL as char(255) charset latin1) AS `segment_professional`,cast(NULL as char(255) charset latin1) AS `segment_lifestage`,cast(NULL as char(255) charset latin1) AS `segment_vanity`,
	cast(NULL as char(255) charset latin1) AS `segment`,cast(NULL as char(255) charset latin1) AS `segment_2`,
	cast(NULL as char(255) charset latin1) AS `segment_3`,cast(NULL as char(255) charset latin1) AS `segment_4`,
	`c`.`default_billing` AS `default_billing`,`c`.`default_shipping` AS `default_shipping`,
	`ochan`.`channel` AS `business_channel`,
	`c`.`website_id` AS `website_id`,`c`.`dw_synced_at` AS `dw_synced_at`,`c`.`customer_id` AS `unique_id`,
	`c`.`unsubscribe_all_date` AS `unsubscribe_all_date` 
from (((((((((
	`dw_flat`.`dw_customers` `c` 
	left join 
		`dw_flat`.`dw_stores` `ds` on((`ds`.`store_id` = `c`.`store_id`))) 
	left join 
		`magento01`.`eav_attribute_option_value` `days_worn` on(((`days_worn`.`option_id` = `c`.`days_worn_info`) and (`days_worn`.`store_id` = 0)))) 
	left join 
		`dw_flat`.`customer_created_in_map` `cim` on((`cim`.`customer_created_in` = `c`.`created_in`))) 
	left join 
		`dw_flat`.`customer_first_order` `fo` on((`fo`.`customer_id` = `c`.`customer_id`))) 
	left join 
		`dw_flat`.`dw_order_headers` `oh` on((`oh`.`order_id` = `fo`.`order_id`))) 
	left join 
		`dw_flat`.`dw_stores` `ds2` on((`oh`.`store_id` = `ds2`.`store_id`))) 
	left join 
		`dw_flat`.`dw_order_channel` `ochan` on((`ochan`.`order_id` = `oh`.`order_id`))) 
	left join 
		`dw_flat`.`dw_order_headers_marketing_agg` `ohma` on((`ohma`.`customer_id` = `c`.`customer_id`))) 
	left join 
		`dw_flat`.`dw_customer_total_orders` `tord` on((`tord`.`customer_id` = `c`.`customer_id`))) 
where (`c`.`website_group_id` <> 100);
