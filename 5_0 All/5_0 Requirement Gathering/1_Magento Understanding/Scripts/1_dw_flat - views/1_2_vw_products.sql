

lens_group_eye

is_lens, daysperlens, pack_qty

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_products` AS 
select 
	`cw`.`store_name` AS `store_name`,`pas`.`product_id` AS `product_id`,
	`pas`.`status` AS `status`,`pcf`.`category_id` AS `category_id`,`pas`.`sku` AS `sku`,`br`.`value` AS `brand`,
	`pas`.`created_at` AS `created_at`,`pas`.`updated_at` AS `updated_at`,
	`cf`.`product_type` AS `product_type`,
	cast(`pas`.`product_lifecycle` as char(255) charset latin1) AS `product_lifecycle`,cast(`pas`.`product_lifecycle_text` as char(8000) charset latin1) AS `product_lifecycle_text`,
	`pas`.`telesales_only` AS `telesales_only`,
	`pas`.`stocked_lens` AS `stocked_lens`,
	`pas`.`tax_class_id` AS `tax_class_id`,
	`pas`.`name` AS `name`,
	cast(`pas`.`description` as char(8000) charset latin1) AS `description`,cast(`pas`.`short_description` as char(8000) charset latin1) AS `short_description`,
	cast(`pas`.`cat_description` as char(8000) charset latin1) AS `category_list_description`,
	`pas`.`cost` AS `average_cost`,`pas`.`weight` AS `weight`,
	`man`.`value` AS `manufacturer`,
	`pas`.`meta_title` AS `meta_title`,cast(`pas`.`meta_keyword` as char(8000) charset latin1) AS `meta_keyword`,`pas`.`meta_description` AS `meta_description`,
	`pas`.`image` AS `image`,`pas`.`small_image` AS `small_image`,`pas`.`thumbnail` AS `thumbnail`,`pas`.`url_key` AS `url_key`,`pas`.`url_path` AS `url_path`,
	`pas`.`visibility` AS `visibility`,`pas`.`image_label` AS `image_label`,`pas`.`small_image_label` AS `small_image_label`,`pas`.`thumbnail_label` AS `thumbnail_label`,
	`pas`.`equivalent_sku` AS `equivalent_sku`,
	`pas`.`daysperlens` AS `daysperlens`,`pas`.`altdaysperlens` AS `alt_daysperlens`,
	`pas`.`equivalence` AS `equivalence`,`pas`.`availability` AS `availability`,`pas`.`condition` AS `condition`,
	`pas`.`google_base_price` AS `google_base_price`,
	`pas`.`promotional_product` AS `promotional_product`,`pas`.`promotion_rule` AS `promotion_rule`,`pas`.`promotion_rule2` AS `promotion_rule2`,
	cast(`pas`.`upsell_text` as char(8000) charset latin1) AS `upsell_text`,`pas`.`replacement_sku` AS `replacement_sku`,
	`pas`.`price_comp_price` AS `price_comp_price`,
	`pas`.`google_feed_title_prefix` AS `google_feed_title_prefix`,`pas`.`google_feed_title_suffix` AS `google_feed_title_suffix`,
	`pas`.`autoreorder_alter_sku` AS `autoreorder_alter_sku`,`pas`.`rrp_price` AS `rrp_price`,
	cast(`pas`.`promotional_text` as char(8000) charset latin1) AS `promotional_text`,
	`pas`.`glasses_colour` AS `glasses_colour`,`pas`.`glasses_material` AS `glasses_material`,`pas`.`glasses_size` AS `glasses_size`,`pas`.`glasses_sex` AS `glasses_sex`,
	`pas`.`glasses_style` AS `glasses_style`,`pas`.`glasses_shape` AS `glasses_shape`,`pas`.`glasses_lens_width` AS `glasses_lens_width`,`pas`.`glasses_bridge_width` AS `glasses_bridge_width`,
	greatest(cast(`pas`.`packsize` as unsigned),1) AS `pack_qty`,`pas`.`packtext` AS `packtext`,
	(case when (cast(`pas`.`alt_packsize` as unsigned) > 0) then cast(`pas`.`alt_packsize` as unsigned) else NULL end) AS `alt_pack_qty`,
	
	`pq`.`first_tier_qty` AS `base_pack_qty`,(`pq`.`first_tier_qty` / greatest(1,cast(`pas`.`packsize` as unsigned))) AS `base_no_packs`,

	`dp1`.`local_unit_price_inc_vat` AS `local_base_unit_price_inc_vat`,round((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`),4) AS `local_base_unit_price_exc_vat`,
	`dp1`.`local_qty_price_inc_vat` AS `local_base_value_inc_vat`,`dp1`.`local_qty_price_exc_vat` AS `local_base_value_exc_vat`,
	`dp1`.`local_pack_price_inc_vat` AS `local_base_pack_price_inc_vat`,	
	cast(round(((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`) * greatest(1,cast(`pas`.`packsize` as unsigned))),4) as decimal(12,4)) AS `local_base_pack_price_exc_vat`,
	`dp1`.`local_prof_fee` AS `local_base_prof_fee`,
	`dp1`.`local_product_cost` AS `local_base_product_cost`,`dp1`.`local_shipping_cost` AS `local_base_shipping_cost`,`dp1`.`local_total_cost` AS `local_base_total_cost`,
	`dp1`.`local_margin_amount` AS `local_base_margin_amount`,`dp1`.`local_margin_percent` AS `local_base_margin_percent`,
	
	`dp1`.`global_unit_price_inc_vat` AS `global_base_unit_price_inc_vat`,round(((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`),4) AS `global_base_unit_price_exc_vat`,
	`dp1`.`global_qty_price_inc_vat` AS `global_base_value_inc_vat`,(`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) AS `global_base_value_exc_vat`,
	`dp1`.`global_pack_price_inc_vat` AS `global_base_pack_price_inc_vat`,
	cast(round((((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`) * greatest(1,cast(`pas`.`packsize` as unsigned))),4) as decimal(12,4)) AS `global_base_pack_price_exc_vat`,
	`dp1`.`global_prof_fee` AS `global_base_prof_fee`,
	`dp1`.`global_product_cost` AS `global_base_product_cost`,`dp1`.`global_shipping_cost` AS `global_base_shipping_cost`,`dp1`.`global_total_cost` AS `global_base_total_cost`,
	`dp1`.`global_margin_amount` AS `global_base_margin_amount`,`dp1`.`global_margin_percent` AS `global_base_margin_percent`,
	
	`pq`.`multibuy_qty` AS `multi_pack_qty`,(`pq`.`multibuy_qty` / greatest(1,cast(`pas`.`packsize` as unsigned))) AS `multi_no_packs`,
	`dp2`.`local_unit_price_inc_vat` AS `local_multi_unit_price_inc_vat`,round((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`),4) AS `local_multi_unit_price_exc_vat`,
	`dp2`.`local_qty_price_inc_vat` AS `local_multi_value_inc_vat`,`dp2`.`local_qty_price_exc_vat` AS `local_multi_value_exc_vat`,
	`dp2`.`local_pack_price_inc_vat` AS `local_multi_pack_price_inc_vat`,
	cast(round(((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`) * greatest(1,cast(`pas`.`packsize` as unsigned))),4) as decimal(12,4)) AS `local_multi_pack_price_exc_vat`,
	`dp2`.`local_prof_fee` AS `local_multi_prof_fee`,
	`dp2`.`local_product_cost` AS `local_multi_product_cost`,`dp2`.`local_shipping_cost` AS `local_multi_shipping_cost`,`dp2`.`local_total_cost` AS `local_multi_total_cost`,
	`dp2`.`local_margin_amount` AS `local_multi_margin_amount`,`dp2`.`local_margin_percent` AS `local_multi_margin_percent`,
	
	`dp2`.`global_unit_price_inc_vat` AS `global_multi_unit_price_inc_vat`,round(((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`),4) AS `global_multi_unit_price_exc_vat`,
	`dp2`.`global_qty_price_inc_vat` AS `global_multi_value_inc_vat`,(`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) AS `global_multi_value_exc_vat`,
	`dp2`.`global_pack_price_inc_vat` AS `global_multi_pack_price_inc_vat`,
	cast(round((((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`) * greatest(1,cast(`pas`.`packsize` as unsigned))),4) as decimal(12,4)) AS `global_multi_pack_price_exc_vat`,
	`dp2`.`global_prof_fee` AS `global_multi_prof_fee`,
	`dp2`.`global_product_cost` AS `global_multi_product_cost`,`dp2`.`global_shipping_cost` AS `global_multi_shipping_cost`,`dp2`.`global_total_cost` AS `global_multi_total_cost`,
	`dp2`.`global_margin_amount` AS `global_multi_margin_amount`,`dp2`.`global_margin_percent` AS `global_multi_margin_percent`,
	
	`dp1`.`local_to_global_rate` AS `local_to_global_rate`,`dp1`.`store_currency_code` AS `store_currency_code`,
	
	`dp1`.`prof_fee_percent` AS `prof_fee_percent`,`dp1`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,`dp1`.`vat_percent` AS `vat_percent`,
	`pas`.`google_shopping_gtin` AS `google_shopping_gtin` 
from (((((((((
		`dw_flat`.`dw_products_all_stores` `pas` 
	join 
		`dw_flat`.`vw_catalog_product_website` `cpw` on(((`pas`.`product_id` = `cpw`.`product_id`) and (`pas`.`store_id` = `cpw`.`website_id`)))) 
	left join 
		`dw_flat`.`dw_stores` `cw` on((`cw`.`store_id` = `pas`.`store_id`))) 
	left join 
		`dw_flat`.`dw_product_category_flat` `pcf` on((`pcf`.`product_id` = `pas`.`product_id`))) 
	left join 
		`dw_flat`.`dw_flat_category` `cf` on((`cf`.`category_id` = `pcf`.`category_id`))) 
	left join 
		`magento01`.`eav_attribute_option_value` `man` on((`man`.`option_id` = `pas`.`manufacturer`))) 
	left join 
		`magento01`.`eav_attribute_option_value` `br` on((`br`.`option_id` = `pas`.`brand`))) 
	left join 
		`dw_flat`.`dw_product_price_qtys` `pq` on(((`pq`.`product_id` = `pas`.`product_id`) and (`pq`.`store_id` = `pas`.`store_id`)))) 
	left join 
		`dw_flat`.`dw_product_prices` `dp1` on(((`dp1`.`product_id` = `pas`.`product_id`) and (`dp1`.`store_id` = `pas`.`store_id`) and (`pq`.`first_tier_qty` = `dp1`.`qty`)))) 
	left join 
		`dw_flat`.`dw_product_prices` `dp2` on(((`dp2`.`product_id` = `pas`.`product_id`) and (`dp2`.`store_id` = `pas`.`store_id`) and (`pq`.`multibuy_qty` = `dp2`.`qty`))));
