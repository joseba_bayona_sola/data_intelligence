ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_product_prices` AS 
select 
	`pas`.`price_id` AS `price_id`,
	`pas`.`store_name` AS `store_name`,`pas`.`product_id` AS `product_id`,
	`pas`.`qty` AS `qty`,`pas`.`pack_size` AS `pack_size`,
	`pas`.`local_unit_price_inc_vat` AS `local_unit_price_inc_vat`,
	`pas`.`local_qty_price_inc_vat` AS `local_qty_price_inc_vat`,
	`pas`.`local_pack_price_inc_vat` AS `local_pack_price_inc_vat`,
	`pas`.`local_prof_fee` AS `local_prof_fee`,
	`pas`.`local_product_cost` AS `local_product_cost`,`pas`.`local_shipping_cost` AS `local_shipping_cost`,
	`pas`.`local_total_cost` AS `local_total_cost`,
	`pas`.`local_margin_amount` AS `local_margin_amount`,`pas`.`local_margin_percent` AS `local_margin_percent`,
	`pas`.`local_to_global_rate` AS `local_to_global_rate`,`pas`.`store_currency_code` AS `store_currency_code`,
	
	`pas`.`global_unit_price_inc_vat` AS `global_unit_price_inc_vat`,
	`pas`.`global_qty_price_inc_vat` AS `global_qty_price_inc_vat`,
	`pas`.`global_pack_price_inc_vat` AS `global_pack_price_inc_vat`,
	`pas`.`global_prof_fee` AS `global_prof_fee`,
	`pas`.`global_product_cost` AS `global_product_cost`,`pas`.`global_shipping_cost` AS `global_shipping_cost`,
	`pas`.`global_total_cost` AS `global_total_cost`,
	`pas`.`global_margin_amount` AS `global_margin_amount`,`pas`.`global_margin_percent` AS `global_margin_percent`,
	`pas`.`prof_fee_percent` AS `prof_fee_percent`,`pas`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,`pas`.`vat_percent` AS `vat_percent` 
from (
		`dw_flat`.`dw_product_prices` `pas` 
	join 
		`magento01`.`catalog_product_website` `cpw` on(((`pas`.`product_id` = `cpw`.`product_id`) and (`pas`.`store_id` = `cpw`.`website_id`))));
