ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_categories` AS 
select 
	`dw_flat_category`.`category_id` AS `category_id`,
	`dw_flat_category`.`promotional_product` AS `promotional_product`,
	`dw_flat_category`.`modality` AS `modality`,`dw_flat_category`.`type` AS `type`,`dw_flat_category`.`feature` AS `feature`,
	`dw_flat_category`.`category` AS `category`,
	`dw_flat_category`.`product_type` AS `product_type` 
from `dw_flat_category`;
