
  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('customer'); -- entity_type_id = 1

  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'customer' 
  order by attribute_id; -- email: attribute_id = 9 / static

-- ------------------------------------------------- 
select -- *
  entity_id, entity_type_id, attribute_set_id, website_id, 
  email, 
  group_id, increment_id, store_id, 
  is_active, website_group_id, disable_auto_group_change,
  created_at, updated_at
from magento01.customer_entity
limit 100;

  select entity_id, email
  from magento01.customer_entity
  
-- varchar
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.entity_id, -- ped.store_id, 
  ped.value
from 
  magento01.customer_entity_varchar ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1
order by ped.attribute_id -- , ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
    magento01.customer_entity_varchar ped
  inner join 
    magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;