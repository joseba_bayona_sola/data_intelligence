
  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('catalog_product'); -- entity_type_id = 4

  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'catalog_product' 
  order by attribute_id;
  
  -- ----------------------------------------------------------------------
  
select -- *
  entity_id, entity_type_id, attribute_set_id, type_id, 
  sku, 
  created_at, updated_at, 
  has_options, required_options
from magento01.catalog_product_entity
order by entity_id;

  select * 
  from magento01.catalog_product_flat_1
  order by entity_id;
  select * 
  from magento01.catalog_product_flat_2
  order by entity_id;
  select * 
  from magento01.catalog_product_flat_3
  order by entity_id;
  select * 
  from magento01.catalog_product_flat_4
  order by entity_id;
  select * 
  from magento01.catalog_product_flat_5
  order by entity_id;

-- Datetime
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.store_id, ped.entity_id, 
  ped.value
from 
  magento01.catalog_product_entity_datetime ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1083
order by ped.attribute_id, ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
      magento01.catalog_product_entity_datetime ped
    inner join 
      magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;


-- decimal
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.store_id, ped.entity_id, 
  ped.value
from 
  magento01.catalog_product_entity_decimal ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1083
order by ped.attribute_id, ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
      magento01.catalog_product_entity_decimal ped
    inner join 
      magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;

-- int
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.store_id, ped.entity_id, 
  ped.value
from 
  magento01.catalog_product_entity_int ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1083
order by ped.attribute_id, ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
      magento01.catalog_product_entity_int ped
    inner join 
      magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;

-- text
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.store_id, ped.entity_id, 
  ped.value
from 
  magento01.catalog_product_entity_text ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1083
order by ped.attribute_id, ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
      magento01.catalog_product_entity_text ped
    inner join 
      magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;

-- varchar
select ped.value_id, 
  ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, ped.store_id, ped.entity_id, 
  ped.value
from 
  magento01.catalog_product_entity_varchar ped
inner join 
  magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
where ped.entity_id = 1083
order by ped.attribute_id, ped.store_id
limit 100;

  select ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type, count(*)
  from 
      magento01.catalog_product_entity_varchar ped
    inner join 
      magento01.eav_attribute atr on ped.entity_type_id = atr.entity_type_id and ped.attribute_id = atr.attribute_id
  group by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type
  order by ped.entity_type_id, ped.attribute_id, atr.attribute_code, atr.backend_type;


