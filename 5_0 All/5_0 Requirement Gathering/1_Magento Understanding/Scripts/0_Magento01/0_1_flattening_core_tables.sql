
select * 
from magento01.core_store;

select *
from magento01.core_config_data;

select pe.entity_id, pe.sku, 
  pev.attribute_id, atr.attribute_code, pev.store_id, pev.value,
  pe.created_at, pe.updated_at 
from 
    magento01.catalog_product_entity pe
  inner join 
    catalog_product_entity_varchar pev on pe.entity_id = pev.entity_id
  inner join 
    magento01.eav_attribute atr on pev.entity_type_id = atr.entity_type_id and pev.attribute_id = atr.attribute_id    
where pe.entity_id = 1100
  and atr.attribute_code = 'name'
