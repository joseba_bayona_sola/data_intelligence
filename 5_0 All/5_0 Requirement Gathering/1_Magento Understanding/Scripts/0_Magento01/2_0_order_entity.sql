

  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('order'); -- entity_type_id = 11

  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'order' 
  order by attribute_id;
  
   -- ----------------------------------------------------------------------
 
 select *
 from magento01.sales_flat_order
 where increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
 order by increment_id
 limit 100
 
 
