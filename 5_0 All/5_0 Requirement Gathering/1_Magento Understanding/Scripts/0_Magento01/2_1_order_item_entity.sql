

  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('order_item'); -- entity_type_id = 11

  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'order_item' 
  order by attribute_id;
  
   -- ----------------------------------------------------------------------
 
select *
from magento01.sales_flat_order_item
limit 100;
 
select oi.*
from 
    magento01.sales_flat_order_item oi
  inner join 
    magento01.sales_flat_order o on oi.order_id = o.entity_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by oi.order_id, oi.item_id
limit 100
