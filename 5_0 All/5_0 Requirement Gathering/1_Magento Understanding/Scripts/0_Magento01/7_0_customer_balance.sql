

select balance_id, customer_id, website_id, amount, base_currency_code
from magento01.enterprise_customerbalance
-- where customer_id = 67845
limit 1000 

  select count(*), count(distinct balance_id), count(distinct customer_id), count(distinct customer_id, website_id) 
  from magento01.enterprise_customerbalance

  select customer_id, count(*)
  from magento01.enterprise_customerbalance
  group by customer_id
  order by count(*) desc, customer_id
  limit 1000

  select customer_id, website_id, count(*)
  from magento01.enterprise_customerbalance
  group by customer_id, website_id
  order by count(*) desc, customer_id, website_id
  limit 1000

  select base_currency_code, count(*)
  from magento01.enterprise_customerbalance
  group by base_currency_code
  order by base_currency_code
  limit 1000

select history_id, balance_id, updated_at, 
  action, balance_amount, balance_delta, 
  additional_info, frontend_comment, is_customer_notified
from magento01.enterprise_customerbalance_history
where balance_id in (45464, 45337)
order by balance_id, history_id desc
limit 1000 

  select count(*), count(distinct history_id), count(distinct balance_id)
  from magento01.enterprise_customerbalance_history

  select action, count(*)
  from magento01.enterprise_customerbalance_history
  group by action
  order by action
  limit 1000 

  select balance_id, count(*)
  from magento01.enterprise_customerbalance_history
  group by balance_id
  order by count(*) desc
  limit 1000 
