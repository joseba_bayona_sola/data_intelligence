
SELECT * 
FROM magento01.eav_entity_type 
order by entity_type_id;

SELECT * 
FROM magento01.eav_attribute 
order by entity_type_id, attribute_id

SELECT * 
FROM magento01.eav_entity_attribute 
order by entity_type_id, attribute_id

SELECT * 
FROM magento01.eav_attribute_option

SELECT * 
FROM magento01.eav_attribute_option_value 

SELECT * 
FROM magento01.eav_entity_store


  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('customer', 'catalog_category', 'catalog_product', 'order', 'order_item');

-- --------------------------------------------------------------------- 

SELECT et.entity_type_id, et.entity_table, et.entity_type_code, count(*) num_attr
FROM 
    magento01.eav_entity_type et 
  INNER JOIN  
    magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
group by et.entity_type_id, et.entity_table, et.entity_type_code
order by et.entity_type_id, et.entity_table, et.entity_type_code;
  
  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'order' -- 'customer', 'catalog_category', 'catalog_product', 'order', 'order_item'
  order by attribute_id;
  
 -- ---------------------------------------------------------------------  
 
 REPLACE INTO $tableName $fields, dw_att, dw_type, dw_synced, dw_synced_at, dw_proc)
				SELECT $origFields, 0, 0, 0, null, 0
				FROM 
            {$this->dbname}.$entityTable a 
          LEFT JOIN 
            $tableName b ON makeJoinStatement($origKeys,$primKeys)
				WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL);
        
REPLACE INTO $tableName $fields, dw_att, dw_synced, dw_synced_at, dw_proc)
				SELECT $origFields, 0, 0, null, 0
				FROM 
            {$this->dbname}.$entityTable a 
          LEFT JOIN 
            $parentTable b ON a.$childId = b.$parentId
				WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL);        