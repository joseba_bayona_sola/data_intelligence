

  SELECT * -- entity_table
  FROM magento01.eav_entity_type 
  where entity_type_code in ('shipment_item'); -- entity_type_id = 20

  SELECT
    et.entity_type_id, et.entity_table,
    attribute_id, attribute_code,
  	entity_table, 
  	backend_type, backend_table
  FROM 
      magento01.eav_entity_type et 
    INNER JOIN  
      magento01.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
  WHERE et.entity_type_code = 'shipment_item' 
  order by attribute_id;
  
   -- ----------------------------------------------------------------------
 
select *
from magento01.sales_flat_shipment_item
limit 100;
 
select si.*
from 
    magento01.sales_flat_shipment_item si
  inner join   
    magento01.sales_flat_shipment s on si.parent_id = s.entity_id
  inner join 
    magento01.sales_flat_order o on s.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by si.parent_id, si.entity_id
limit 100