

select table_schema, table_name, table_rows
from INFORMATION_SCHEMA.TABLES
where table_type = 'BASE TABLE' and -- BASE TABLE - VIEW
  -- table_schema = 'dw_flat' 
  -- table_schema = 'magento01'
  -- table_schema in ('lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson')
order by -- table_rows desc, 
  table_schema, table_name;

select table_name, column_name, ordinal_position, column_type, column_key, data_type, character_maximum_length, is_nullable
from INFORMATION_SCHEMA.COLUMNS
where table_schema = 'magento01' and table_name = 'sales_flat_order'
order by column_name

select table_name, column_name, ordinal_position, column_type, column_key, data_type, character_maximum_length, is_nullable
from INFORMATION_SCHEMA.COLUMNS
where table_schema = 'vd150324'  -- in ('lb150324', 'lh150324', 'vd150324', 'VisioOptik', 'lensway', 'lenson')
  and table_name = 'migrate_orderdata' -- migrate_customerdata, migrate_orderdata, migrate_orderlinedata
order by ordinal_position
