
create table mag.#customer_entity_flat_varchar(
		entity_id					int NOT NULL, 
		created_in					varchar(255), 
		prefix						varchar(255), 
		firstname					varchar(255), 
		middlename					varchar(255), 
		lastname					varchar(255), 
		suffix						varchar(255), 
		cus_phone					varchar(255), 
		unsubscribe_all				varchar(255), 
		old_access_cust_no			varchar(255), 
		old_web_cust_no				varchar(255), 
		old_customer_id				varchar(255), 
		found_us_info				varchar(255), 
		referafriend_code			varchar(255));
go 

create table mag.#customer_entity_flat_int(
		entity_id					int NOT NULL,
		default_billing				int, 
		default_shipping			int,
		days_worn_info				int, 
		);
go 

create table mag.#customer_entity_flat_datetime(
		entity_id					int NOT NULL,
		unsubscribe_all_date		datetime, 
		);
go 



insert into mag.#customer_entity_flat_varchar(entity_id,
	created_in, prefix, firstname, middlename, lastname, suffix, cus_phone, unsubscribe_all, 
	old_access_cust_no, old_web_cust_no, old_customer_id, found_us_info, referafriend_code)

	select entity_id,
		created_in, prefix, firstname, middlename, lastname, suffix, cus_phone, unsubscribe_all, 
		old_access_cust_no, old_web_cust_no, old_customer_id, found_us_info, referafriend_code
	from 
			(select 
				-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
				atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_code, dt.value
			from
					(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
					from
							(select entity_type_id, entity_type_code
							from Landing.mag.eav_entity_type_aud) ent
						inner join
							(select attribute_id, entity_type_id, attribute_code, backend_type
							from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
						inner join
							Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
					where eatr.entity_type_code = 'customer' and atr.backend_type = 'varchar') atr
				inner join
					(select value_id, entity_type_id, attribute_id, entity_id, value
					from Landing.mag.customer_entity_varchar
					-- where entity_id in (1697740, 354094, 1440560, 62780)
					) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
		pivot
			(min(value) for 
			attribute_code in (created_in, prefix, firstname, middlename, lastname, suffix, cus_phone, unsubscribe_all, 
				old_access_cust_no, old_web_cust_no, old_customer_id, found_us_info, referafriend_code)) pvt




insert into mag.#customer_entity_flat_int(entity_id,
	default_billing, default_shipping, days_worn_info)

	select entity_id,
		default_billing, default_shipping, days_worn_info
	from 
			(select 
				-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
				atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_code, dt.value
			from
					(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
					from
							(select entity_type_id, entity_type_code
							from Landing.mag.eav_entity_type_aud) ent
						inner join
							(select attribute_id, entity_type_id, attribute_code, backend_type
							from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
					where ent.entity_type_id = 1) atr
				inner join
					(select value_id, entity_type_id, attribute_id, entity_id, value
					from Landing.mag.customer_entity_int
					-- where entity_id in (1697740, 354094, 1440560, 62780)
					) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
		pivot
			(min(value) for 
			attribute_code in (default_billing, default_shipping, days_worn_info)) pvt

insert into mag.#customer_entity_flat_datetime(entity_id, unsubscribe_all_date)
	select entity_id, unsubscribe_all_date
	from 
			(select 
				-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
				atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_code, dt.value
			from
					(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
					from
							(select entity_type_id, entity_type_code
							from Landing.mag.eav_entity_type_aud) ent
						inner join
							(select attribute_id, entity_type_id, attribute_code, backend_type
							from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
					where ent.entity_type_id = 1) atr
				inner join
					(select value_id, entity_type_id, attribute_id, entity_id, value
					from Landing.mag.customer_entity_datetime
					-- where entity_id in (1697740, 354094, 1440560, 62780)
					) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
		pivot
			(min(value) for 
			attribute_code in (unsubscribe_all_date)) pvt

select c.entity_id, cv.entity_id, ci.entity_id,
	c.increment_id, c.email, 
	c.entity_type_id, c.attribute_set_id, 
	c.group_id, c.website_group_id, c.store_id, 
	c.is_active, c.disable_auto_group_change, 
	c.created_at, c.updated_at, 
	cv.created_in, 
	cv.prefix, cv.firstname, cv.middlename, cv.lastname, cv.suffix, 
	cv.cus_phone, 
	ci.default_billing, ci.default_shipping, 
	cv.unsubscribe_all, cd.unsubscribe_all_date, 
	ci.days_worn_info, 
	cv.old_access_cust_no, cv.old_web_cust_no, cv.old_customer_id, 
	cv.found_us_info, cv.referafriend_code
from 
		Landing.mag.customer_entity c
	left join
		mag.#customer_entity_flat_varchar cv on c.entity_id = cv.entity_id
	left join
		mag.#customer_entity_flat_int ci on c.entity_id = ci.entity_id
	left join
		mag.#customer_entity_flat_datetime cd on c.entity_id = cd.entity_id


----------------------------------------------------------------------------------

	create table mag.#catalog_category_entity_flat_varchar(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,  
			name						varchar(255), 
			url_path					varchar(255), 
			url_key						varchar(255), 
			display_mode				varchar(255));


	create table mag.#catalog_category_entity_flat_int(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL, 
			is_active					int);

	insert into mag.#catalog_category_entity_flat_varchar(entity_id, store_id, name, url_path, url_key, display_mode)

		select entity_id, store_id, name, url_path, url_key, display_mode
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_category' and atr.backend_type = 'varchar') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_category_entity_varchar) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (name, url_path, url_key, display_mode)) pvt

	select entity_id, store_id, 
		name, url_path, url_key, display_mode
	from mag.#catalog_category_entity_flat_varchar
	order by entity_id, store_id

	select entity_id, count(*)
	from mag.#catalog_category_entity_flat_varchar
	group by entity_id
	order by entity_id

	select store_id, count(*)
	from mag.#catalog_category_entity_flat_varchar
	group by store_id
	order by store_id

	select entity_id, store_id, count(*)
	from mag.#catalog_category_entity_flat_varchar
	group by entity_id, store_id
	order by entity_id, store_id

	create table mag.#catalog_category_entity_flat_int(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL, 
			is_active					int);

	insert into mag.#catalog_category_entity_flat_int(entity_id, store_id, is_active)

		select entity_id, store_id, is_active
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_category' and atr.backend_type = 'int') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_category_entity_int) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (is_active)) pvt

		select *
		from Landing.mag.catalog_category_entity
		where entity_id = 4

		select entity_id, store_id, 
			name, url_path, url_key, display_mode
		from mag.#catalog_category_entity_flat_varchar
		order by entity_id, store_id

		select entity_id, store_id, 
			is_active
		from mag.#catalog_category_entity_flat_int
		order by entity_id, store_id

		select c.entity_id, cvi.store_id, c.parent_id, 
			c.entity_type_id, c.attribute_set_id, 
			c.position, c.level, c.children_count, c.path, 
			c.created_at, c.updated_at, 
			cvi.is_active, 
			cvi.name, cvi.url_path, cvi.url_key, cvi.display_mode 
		from 
				Landing.mag.catalog_category_entity c
			left join
				(select 
					case when (cv.entity_id is null) then ci.entity_id else cv.entity_id end entity_id, 
					case when (cv.store_id is null) then ci.store_id else cv.store_id end store_id, 
					cv.name, cv.url_path, cv.url_key, cv.display_mode, ci.is_active
				from 
						mag.#catalog_category_entity_flat_varchar cv
					full join
						mag.#catalog_category_entity_flat_int ci on cv.entity_id = ci.entity_id and cv.store_id = ci.store_id) cvi on c.entity_id = cvi.entity_id
		where cvi.store_id = 0
		order by c.entity_id, cvi.store_id


		select 
			case when (cv.entity_id is null) then ci.entity_id else cv.entity_id end entity_id, 
			case when (cv.store_id is null) then ci.store_id else cv.store_id end store_id, 
			cv.name, cv.url_path, cv.url_key, cv.display_mode, ci.is_active
		from 
				mag.#catalog_category_entity_flat_varchar cv
			full join
				mag.#catalog_category_entity_flat_int ci on cv.entity_id = ci.entity_id and cv.store_id = ci.store_id
		
		order by entity_id, store_id

		select entity_id, store_id, 
			is_active
		from mag.#catalog_category_entity_flat_int
		where entity_id = 4
		order by entity_id, store_id


-----------------------------------------------------------------------------------------

	drop table mag.#catalog_product_entity_flat_varchar;
	drop table mag.#catalog_product_entity_flat_int;
	drop table mag.#catalog_product_entity_flat_text;


	create table mag.#catalog_product_entity_flat_varchar(
			entity_id					int NOT NULL,
			store_id					int NOT NULL,
			name						varchar(255),
			url_path					varchar(255),
			url_key						varchar(255),
			daysperlens					varchar(255),
			equivalence					varchar(255),
			equivalent_sku				varchar(255),
			price_comp_price			varchar(255));


	create table mag.#catalog_product_entity_flat_int(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,
			manufacturer				int, 		
			status						int, 	
			promotional_product			int, 
			tax_class_id				int, 
			visibility					int, 
			is_lens						int, 
			stocked_lens				int, 
			telesales_only				int, 
			condition					int, 
			glasses_colour				int);


	create table mag.#catalog_product_entity_flat_text(
			entity_id					int NOT NULL, 
			store_id					int NOT NULL,
			product_type				varchar(1000),
			product_lifecycle			varchar(1000));

	insert into mag.#catalog_product_entity_flat_varchar(entity_id, store_id, name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price)

		select entity_id, store_id, name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'varchar') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_varchar_aud) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price)) pvt

	insert into mag.#catalog_product_entity_flat_int(entity_id, store_id, 
		manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, 
		stocked_lens, telesales_only, condition, glasses_colour)

		select entity_id, store_id, 
			manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, stocked_lens, 
			telesales_only, condition, glasses_colour
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'int') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_int_aud) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, stocked_lens, 
					telesales_only, condition, glasses_colour)) pvt

	insert into mag.#catalog_product_entity_flat_text(entity_id, store_id, product_type, product_lifecycle)

		select entity_id, store_id, product_type, product_lifecycle
		from 
				(select atr.entity_type_id, atr.entity_type_code, dt.entity_id, dt.store_id, atr.attribute_code, dt.value
				from
						(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
						from
								(select entity_type_id, entity_type_code
								from Landing.mag.eav_entity_type_aud) ent
							inner join
								(select attribute_id, entity_type_id, attribute_code, backend_type
								from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
							inner join
								Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
						where eatr.entity_type_code = 'catalog_product' and atr.backend_type = 'text') atr
					inner join
						(select value_id, entity_type_id, attribute_id, entity_id, store_id, value
						from Landing.mag.catalog_product_entity_text_aud) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
			pivot
				(min(value) for 
				attribute_code in (product_type, product_lifecycle)) pvt


select entity_id, store_id, 
	name, url_path, url_key, daysperlens, equivalence, equivalent_sku, price_comp_price
from mag.#catalog_product_entity_flat_varchar

select entity_id, store_id, 
	manufacturer, status, promotional_product, tax_class_id, visibility, is_lens, stocked_lens, telesales_only, condition, glasses_colour
from mag.#catalog_product_entity_flat_int

select entity_id, store_id, 
	product_type, product_lifecycle
from mag.#catalog_product_entity_flat_text


select p.entity_id, pvit.store_id, 
	p.entity_type_id, p.attribute_set_id, 
	p.type_id, p.sku, p.required_options, p.has_options, 
	p.created_at, p.updated_at, 
	pvit.manufacturer, 
	pvit.name, pvit.url_path, pvit.url_key, 
	pvit.status, pvit.product_type, pvit.product_lifecycle, 
	pvit.promotional_product, pvit.daysperlens, 
	pvit.tax_class_id, pvit.visibility, pvit.is_lens, pvit.stocked_lens, pvit.telesales_only, pvit.condition, 
	pvit.equivalence, pvit.equivalent_sku, pvit.price_comp_price, pvit.glasses_colour
from 
		Landing.mag.catalog_product_entity_aud p
	left join
		(select
			case when (pvi.entity_id is null) then pt.entity_id else pvi.entity_id end entity_id, 
			case when (pvi.store_id is null) then pt.store_id else pvi.store_id end store_id, 
			pvi.name, pvi.url_path, pvi.url_key, pvi.daysperlens, pvi.equivalence, pvi.equivalent_sku, pvi.price_comp_price,
			pvi.manufacturer, pvi.status, pvi.promotional_product, pvi.tax_class_id, pvi.visibility, pvi.is_lens, pvi.stocked_lens, pvi.telesales_only, pvi.condition, pvi.glasses_colour,
			pt.product_type, pt.product_lifecycle
		from
				(select 
					case when (pv.entity_id is null) then pi.entity_id else pv.entity_id end entity_id, 
					case when (pv.store_id is null) then pi.store_id else pv.store_id end store_id, 
					pv.name, pv.url_path, pv.url_key, pv.daysperlens, pv.equivalence, pv.equivalent_sku, pv.price_comp_price,
					pi.manufacturer, pi.status, pi.promotional_product, pi.tax_class_id, pi.visibility, pi.is_lens, pi.stocked_lens, pi.telesales_only, pi.condition, pi.glasses_colour
				from 
						mag.#catalog_product_entity_flat_varchar pv
					full join
						mag.#catalog_product_entity_flat_int pi on pv.entity_id = pi.entity_id and pv.store_id = pi.store_id) pvi
			full join
				mag.#catalog_product_entity_flat_text pt on pvi.entity_id = pt.entity_id and pvi.store_id = pt.store_id) pvit on p.entity_id = pvit.entity_id
where pvit.store_id = 0		
