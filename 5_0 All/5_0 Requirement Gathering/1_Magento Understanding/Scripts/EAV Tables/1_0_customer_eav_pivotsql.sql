
select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
from
		(select entity_type_id, entity_type_code
		from Landing.mag.eav_entity_type_aud) ent
	inner join
		(select attribute_id, entity_type_id, attribute_code, backend_type
		from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
where ent.entity_type_id = 1
order by ent.entity_type_id, atr.backend_type, atr.attribute_id

select top 100 *
from Landing.mag.customer_entity_aud_v
where num_records > 1
order by num_records desc

select top 100 *
from Landing.mag.customer_entity_aud_v
where entity_id = 1697740
order by ins_ts


select top 100 *
from Landing.mag.customer_entity_datetime_aud_v
where entity_id = 1697740
order by attribute_id

select top 100 *
from Landing.mag.customer_entity_int_aud_v
where entity_id = 1697740
order by attribute_id

select top 100 *
from Landing.mag.customer_entity_varchar_aud_v
where entity_id = 1697740
--where attribute_id = 8
order by attribute_id

----------------------------------------------------------

select atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, 
	dt.entity_id, dt.value
from
		(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
		from
				(select entity_type_id, entity_type_code
				from Landing.mag.eav_entity_type_aud) ent
			inner join
				(select attribute_id, entity_type_id, attribute_code, backend_type
				from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		where ent.entity_type_id = 1) atr
	inner join
		(select value_id, entity_type_id, attribute_id, entity_id, value
		from Landing.mag.customer_entity_datetime_aud
		where entity_id = 1697740) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id


select atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, 
	dt.entity_id, dt.value
from
		(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
		from
				(select entity_type_id, entity_type_code
				from Landing.mag.eav_entity_type_aud) ent
			inner join
				(select attribute_id, entity_type_id, attribute_code, backend_type
				from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		where ent.entity_type_id = 1) atr
	inner join
		(select value_id, entity_type_id, attribute_id, entity_id, value
		from Landing.mag.customer_entity_varchar_aud
		where entity_id = 1697740) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id

select atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, 
	dt.entity_id, dt.value
from
		(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
		from
				(select entity_type_id, entity_type_code
				from Landing.mag.eav_entity_type_aud) ent
			inner join
				(select attribute_id, entity_type_id, attribute_code, backend_type
				from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		where ent.entity_type_id = 1) atr
	inner join
		(select value_id, entity_type_id, attribute_id, entity_id, value
		from Landing.mag.customer_entity_int_aud
		where entity_id = 1697740) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id
