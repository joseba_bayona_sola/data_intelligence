
select atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, 
	dt.entity_id, dt.value
from
		(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
		from
				(select entity_type_id, entity_type_code
				from Landing.mag.eav_entity_type_aud) ent
			inner join
				(select attribute_id, entity_type_id, attribute_code, backend_type
				from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		where ent.entity_type_id = 1) atr
	inner join
		(select value_id, entity_type_id, attribute_id, entity_id, value
		from Landing.mag.customer_entity_varchar_aud
		where entity_id = 1697740) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id


select entity_type_id, entity_type_code, entity_id,
	[3], [4], [5], [7], [12], [687], [715], [716], [718]
from 
		(select 
			-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
			atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_id, dt.value
		from
				(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
				from
						(select entity_type_id, entity_type_code
						from Landing.mag.eav_entity_type_aud) ent
					inner join
						(select attribute_id, entity_type_id, attribute_code, backend_type
						from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
				where ent.entity_type_id = 1) atr
			inner join
				(select value_id, entity_type_id, attribute_id, entity_id, value
				from Landing.mag.customer_entity_varchar_aud
				where entity_id = 1697740) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
	pivot
		(min(value) for 
		attribute_id in ([3], [4], [5], [7], [12], [687], [715], [716], [718])) pvt


select entity_type_id, entity_type_code, entity_id,
	[created_in], [prefix], [firstname], [lastname], [suffix], [password_hash], [unsubscribe_all], [cus_phone], [found_us_info], [referafriend_code]
from 
		(select 
			-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
			atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_code, dt.value
		from
				(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
				from
						(select entity_type_id, entity_type_code
						from Landing.mag.eav_entity_type_aud) ent
					inner join
						(select attribute_id, entity_type_id, attribute_code, backend_type
						from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
				where ent.entity_type_id = 1) atr
			inner join
				(select value_id, entity_type_id, attribute_id, entity_id, value
				from Landing.mag.customer_entity_varchar_aud
				where entity_id in (1697740, 354094, 1440560, 62780)
				) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
	pivot
		(min(value) for 
		attribute_code in ([created_in], [prefix], [firstname], [lastname], [suffix], [password_hash], [unsubscribe_all], [cus_phone], [found_us_info], [referafriend_code])) pvt

select entity_type_id, entity_type_code, entity_id,
	[created_in], [prefix], [firstname], [lastname], [suffix], [password_hash], [unsubscribe_all], [cus_phone], [found_us_info], [referafriend_code]
from 
		(select 
			-- atr.entity_type_id, atr.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, dt.entity_id, dt.value
			atr.entity_type_id, atr.entity_type_code, dt.entity_id, atr.attribute_code, dt.value
		from
				(select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
				from
						(select entity_type_id, entity_type_code
						from Landing.mag.eav_entity_type_aud) ent
					inner join
						(select attribute_id, entity_type_id, attribute_code, backend_type
						from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
				where ent.entity_type_id = 1) atr
			inner join
				(select value_id, entity_type_id, attribute_id, entity_id, value
				from Landing.mag.customer_entity_varchar
				--where entity_id in (1697740, 354094, 1440560, 62780)
				) dt on atr.entity_type_id = dt.entity_type_id and atr.attribute_id = dt.attribute_id) src
	pivot
		(min(value) for 
		attribute_code in ([created_in], [prefix], [firstname], [lastname], [suffix], [password_hash], [unsubscribe_all], [cus_phone], [found_us_info], [referafriend_code])) pvt

