
-- Entity Type

select entity_type_id, entity_type_code
from Landing.mag.eav_entity_type_aud

-- Attribute

select attribute_id, entity_type_id, attribute_code, backend_type
from Landing.mag.eav_attribute_aud

select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
from
		(select entity_type_id, entity_type_code
		from Landing.mag.eav_entity_type_aud) ent
	inner join
		(select attribute_id, entity_type_id, attribute_code, backend_type
		from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
order by ent.entity_type_id, atr.backend_type, atr.attribute_id

	select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
		inner join
			Landing.map.mag_eav_attributes_aud eatr on ent.entity_type_code = eatr.entity_type_code and atr.attribute_code = eatr.attribute_code
	where ent.entity_type_code = 'customer_address'
	order by ent.entity_type_id, atr.backend_type, atr.attribute_id


	-- # Attributes per Entity
	select ent.entity_type_id, ent.entity_type_code, count(*)
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
	group by ent.entity_type_id, ent.entity_type_code
	order by ent.entity_type_id, ent.entity_type_code

	-- # Rows per Backend Type
	select atr.backend_type, count(*)
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
	group by atr.backend_type
	order by atr.backend_type

-- Entity Attribute

select entity_attribute_id, 
	entity_type_id, attribute_id, 
	attribute_set_id, attribute_group_id, sort_order
from Landing.mag.eav_entity_attribute


select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, 
	entatr.entity_attribute_id, entatr.attribute_set_id, entatr.attribute_group_id, entatr.sort_order
from
		(select entity_type_id, entity_type_code
		from Landing.mag.eav_entity_type_aud) ent
	inner join	
		(select entity_attribute_id, 
			entity_type_id, attribute_id, 
			attribute_set_id, attribute_group_id, sort_order
		from Landing.mag.eav_entity_attribute_aud) entatr on ent.entity_type_id = entatr.entity_type_id
	inner join
		(select attribute_id, entity_type_id, attribute_code, backend_type
		from Landing.mag.eav_attribute_aud) atr on entatr.attribute_id = atr.attribute_id
order by ent.entity_type_id, atr.attribute_id

	select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, count(*)
	from
			(select entity_type_id, entity_type_code
			from Landing.mag.eav_entity_type_aud) ent
		inner join	
			(select entity_attribute_id, 
				entity_type_id, attribute_id, 
				attribute_set_id, attribute_group_id, sort_order
			from Landing.mag.eav_entity_attribute_aud) entatr on ent.entity_type_id = entatr.entity_type_id
		inner join
			(select attribute_id, entity_type_id, attribute_code, backend_type
			from Landing.mag.eav_attribute_aud) atr on entatr.attribute_id = atr.attribute_id
	group by ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type
	order by ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type

-- Attribute Option - Value

select option_id, attribute_id, sort_order
from Landing.mag.eav_attribute_option_aud

select value_id, option_id, store_id, value
from Landing.mag.eav_attribute_option_value_aud

select ent.entity_type_id, ent.entity_type_code, atr.attribute_id, atr.attribute_code, atr.backend_type, opt.sort_order, optva.store_id, optva.value
from
		(select entity_type_id, entity_type_code
		from Landing.mag.eav_entity_type_aud) ent
	inner join
		(select attribute_id, entity_type_id, attribute_code, backend_type
		from Landing.mag.eav_attribute_aud) atr on ent.entity_type_id = atr.entity_type_id
	inner join
		(select option_id, attribute_id, sort_order
		from Landing.mag.eav_attribute_option_aud) opt on atr.attribute_id = opt.attribute_id
	inner join
		(select value_id, option_id, store_id, value
		from Landing.mag.eav_attribute_option_value_aud) optva on opt.option_id = optva.option_id
order by ent.entity_type_id, atr.attribute_id, opt.sort_order




