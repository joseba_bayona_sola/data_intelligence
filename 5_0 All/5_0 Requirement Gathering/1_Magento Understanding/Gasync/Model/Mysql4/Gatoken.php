<?php
    class Pointernal_Gasync_Model_Mysql4_Gatoken extends Mage_Core_Model_Mysql4_Abstract
    {
        protected function _construct()
        {
            $this->_init("gasync/gatoken", "id");
        }

		public function loadByStoreId(Pointernal_Gasync_Model_Gatoken $token, $store_id)
		{
			$select = $this->_getReadAdapter()->select()
				->from($this->getTable('gasync/gatoken'), array('id'))
				//->where('email=?', $email);
				->where("store_id=$store_id");
			
			
			if ($id = $this->_getReadAdapter()->fetchOne($select, array('store_id' => $store_id))) {
				$this->load($token, $id);
			}
			else {
				$token->setData(array());
			}
			return $token;
		}
    }
	 
