<?php
    class Pointernal_Gasync_Model_Mysql4_Gadata extends Mage_Core_Model_Mysql4_Abstract
    {
        protected function _construct()
        {
            $this->_init("gasync/gadata", "id");
        }

		public function loadByProfileData(Pointernal_Gasync_Model_Gadata $gadata, $profileData)
		{
			$select = $this->_getReadAdapter()->select()
				->from($this->getTable('gasync/gadata'), array('id'))
				//->where('email=?', $email);
				->where("store_id=".$profileData['store_id'])
				->where("profile_id=".$profileData['profile_id'])
				->where("date='".$profileData['date']."'");
			
			if ($id = $this->_getReadAdapter()->fetchOne(
				$select, array(
								'store_id' => $profileData['store_id'], 
								'profile_id' => $profileData['profile_id'],
								'date' => $profileData['date'])
				)) {
				$this->load($gadata, $id);
			}
			else {
				$gadata->setData(array());
			}
			return $this;
		}
		
		public function addDataRows($dataEntity,$data,$profile) {
			//check the data and see if there are any dimensions in it if there are treat it as transaction
			$tranasctionData = false;
			$insertTemplate = array();
			foreach($data['headers'] as $header) {
				if($header->columnType  == 'DIMENSION') {
					$tranasctionData = true;
				}
				$insertTemplate[]  = str_replace('ga:', '', $header->name);
			}
			if($tranasctionData) {
				foreach($data['rows'] as $row) {
					$insertRow = array();
					foreach($row as $key => $data) {
						$insertRow[$insertTemplate[$key]] = $data;
					}
					
					$channel = Mage::helper('gasync')->getChannel($insertRow['source'],$insertRow['medium']);
					if($channel) {
						$insertRow['channel'] =$channel;
					}
					if($profile) {
						$insertRow['profile'] =$profile;
					}
					$this->_getWriteAdapter()->insertOnDuplicate($this->getTable('gasync/gaentitytransactiondata'),$insertRow);
				}
			} else {
				//delete any records pointing to that data entity 
				$dataId = $dataEntity->getId();
				$this->_getWriteAdapter()->delete($this->getTable('gasync/gaentitydata'),"data_id = $dataId");
				foreach($data['headers'] as $key => $header) {
					$fomattedData = array(
						'data_id' => $dataId,
						'ga_key' => $header->getName(),
						'value' => $data['rows'][0][$key],
					);
					$this->_getWriteAdapter()->insert($this->getTable('gasync/gaentitydata'),$fomattedData);
				}
			}
		}
		
		protected function _getLoadSelect($field, $value, $object)
		{
			$dataKeys = Mage::getSingleton('gasync/client')->getGaKeys();
			$select = parent::_getLoadSelect($field, $value, $object);
			foreach($dataKeys as $dataKey) {
				$select->joinLeft(array("gadata_$dataKey" => $this->getTable('gasync/gaentitydata')), 
				"`{$this->getTable('gasync/gadata')}`.`id`= `gadata_$dataKey`.`data_id` AND `gadata_$dataKey`.`ga_key`= '$dataKey'", array($dataKey => 'value'));
			}
			return $select;
		}
    }
	 
