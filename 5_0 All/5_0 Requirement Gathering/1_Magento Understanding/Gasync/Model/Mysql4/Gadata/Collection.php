<?php
    class Pointernal_Gasync_Model_Mysql4_Gadata_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
    {

		public function _construct(){
			$this->_init("gasync/gadata");
		}
		
		protected function _initSelect()
		{

			$dataKeys = Mage::getSingleton('gasync/client')->getGaKeys();
			parent::_initSelect();
			$select = $this->getSelect();
			foreach($dataKeys as $dataKey) {
				$select->joinLeft(array("gadata_$dataKey" => $this->getResource()->getTable('gasync/gaentitydata')), 
				"`main_table`.`id`= `gadata_$dataKey`.`data_id` AND `gadata_$dataKey`.`ga_key`= '$dataKey'", array($dataKey => 'value'));
			}
			return $this;
		}
    }
	 
