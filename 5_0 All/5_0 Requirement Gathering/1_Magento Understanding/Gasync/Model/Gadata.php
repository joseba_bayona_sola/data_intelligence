<?php

class Pointernal_Gasync_Model_Gadata extends Mage_Core_Model_Abstract
{
    protected function _construct(){

       $this->_init("gasync/gadata");

    }
    
    public function loadByProfileData($profileInfo) {
		$this->_getResource()->loadByProfileData($this, $profileInfo);
		return $this;
	}
	
	public function addGAData($data,$profile) {
		$this->_getResource()->addDataRows($this, $data,$profile);
		return $this;
	}
	

}
	 
