<?php

class Pointernal_Gasync_Model_Gatoken extends Mage_Core_Model_Abstract
{
    protected function _construct(){

       $this->_init("gasync/gatoken");

    }
    
    public function getTokenByStoreId($store_id) {
		$this->_getResource()->loadByStoreId($this, $store_id);
		return $this;
	}

}
	 
