<?php

require_once(Mage::getBaseDir('lib') . '/google-api-php-client/src/apiClient.php');
require_once(Mage::getBaseDir('lib') . '/google-api-php-client/src/contrib/apiAnalyticsService.php');



class Pointernal_Gasync_Model_Client extends Mage_Core_Model_Abstract
{

	private $clientAuth;
	private $ignoreStores = array(4,7,11,1,20);
	private $analytics;
	private $storeId;
	private $queries = array(
		'transaction_data' =>
			array(
				'dimensions' =>  array(
					'ga:source','ga:medium','ga:transactionId','ga:date'
				),
				'metrics' => array(
					'ga:transactions',
				)
			)
	);
 private $activeProfiles = array(
                'Primary View - All Users',
        );

/*
 * Return stores that have google analytics enabled
 * name: getStoreEnabled
 * @param
 * @return
 *
 */
	public function getEnabledStores() {
		$enabledStores = array();
		$storeCollection = Mage::getModel('core/store')->getCollection();
		foreach($storeCollection as $store) {
			if(array_search($store->getId(), $this->ignoreStores) === FALSE) {
				$enabledStores[] = $store->getId();
			}
		}
		return $enabledStores;
	}

	public function setStoreId($storeId) {
		$prevStoreId = $this->storeId ;
		if($prevStoreId  > 0 && $prevStoreId  != $this->storeId ) {
			//change in store id means new token should be loaded
			$this->clientAuth = null;
			$this->getAuthorisedClient();
		}
		$this->storeId = $storeId;

	}

/*
 *
 * name: getStoreId
 * @param
 * @return
 *
 */
	public function getStoreId() {
		if(!$this->storeId) {
			throw new Exception('Store Id needs to be set before authorization');
		}
		return $this->storeId;
	}

/*
 * Return instace of the google api client object with access detail set
 *
 * name: getClientInstance
 * @param
 * @return
 *
 */
	public function getClientInstance() {
		//TODO: add exception handling
		$client = new apiClient();
		$client->setApplicationName('GA Data Sync');

		//TODO:  this needs to come from mage confg
		$client->setClientId(Mage::getStoreConfig('google/analytics/client_id',$this->getStoreId()));
		$client->setClientSecret(Mage::getStoreConfig('google/analytics/client_secret',$this->getStoreId()));
		$client->setRedirectUri(Mage::getUrl('gasync/index/auth'));
		$client->setDeveloperKey(Mage::getStoreConfig('google/analytics/dev_key',$this->getStoreId()));
		$client->setAccessType('offline');
		$client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));

		// Magic. Returns objects from the Analytics Service instead of associative arrays.
		$client->setUseObjects(true);
		return $client;
	}

/*
 * Use token saved in datablase for a given store  to authorise client
 *
 * name: getAuthorisedClient
 * @param
 * @return
 *
 */
	public function getAuthorisedClient() {
		if($this->clientAuth) {
			return $this->clientAuth;
		}
		$storeId = $this->getStoreId();
		$client = $this->getClientInstance();

		$token = Mage::getModel('gasync/gatoken')->getTokenByStoreId($storeId);
		$client->setAccessType('offline');
		$tokenData = $token->getToken();
		if(empty($tokenData)) {
			echo("token missing set up token for store id $storeId \n");
			return false;
		}
		$client->setAccessToken($tokenData);
		return $client;
	}

/*
 *
 * Return all the metrics configured for a given data supplied range not supported as we are not using any
 * data dimenstion currently (date is a dimesntion and changes the response structure_)
 *
 * name: getSyncData
 * @param
 * @return
 *
 */
	public function syncData($startDate = false,$endDate = false,$query= 'transaction_data') {
		//get profile for this account

			$client = $this->getAuthorisedClient();
			if(!$client) {
				echo "could not load authorised client store id:{$this->getStoreId()}\n";
				return false;
			}
			$profiles = $this->getProfiles();
			$analytics = $this->getAnalyticsObject();
			//add query data to profile list
			if($startDate == false || $endDate  == false) {
				$newdate = strtotime ('-15 day', time()) ;
				$calcDate = date('Y-m-d',$newdate);
				$date = $calcDate;
				$calcEndDate = date('Y-m-d');
				$startDate  = !$startDate ? $date : $startDate;
				$endDate =  !$endDate ? $calcEndDate : $endDate;
			}
			$starttimestmap = strtotime($startDate);
			$endtimestmap = strtotime($endDate);

			if($starttimestmap > $endtimestmap) {
				throw new Exception('Invalid date range');
			}
			$currentimestamp = $starttimestmap;
			while($currentimestamp <= $endtimestmap) {

				foreach($profiles as  $key => $profile) {
					//accounts for this store

					$result =  $analytics->data_ga->get(
					'ga:' . $key,
					date('Y-m-d',$currentimestamp),
					date('Y-m-d',$currentimestamp),
					implode(',',$this->queries[$query]['metrics']),
					array(
						'dimensions' => implode(',',$this->queries[$query]['dimensions']),
						'max-results' => 10000,
						//'filters' => 'ga:transactionId==something'
						//'metrics' => implode(',',$this->metrics)

					));
					$profiles[$key]['results']['headers'] = $result->getColumnHeaders();
					$profiles[$key]['results']['rows'] = $result->getRows();
					$profiles[$key]['results']['date'] = $startDate;
				}
				$this->saveSyncData($profiles);
				$dateString = date('Y-m-d',$currentimestamp);
				echo("Synced data for {$dateString} store {$this->getStoreId()}\n");
				$currentimestamp = strtotime('+1 day', $currentimestamp);
			}
			return true;
	}

/*
 * Saves the given data to the database
 *
 * name: saveSyncData
 * @param
 * @return
 *
 */
	public function saveSyncData($profileData) {
		foreach($profileData as $key => $profile) {
			$gaData = Mage::getModel('gasync/gadata');
			$gaData->loadByProfileData(array(
				'date' => $profile['results']['date'],
				'store_id' => $this->getStoreId(),
				'profile_id' =>$key,
			));

			$gaData->setData('store_id',$this->getStoreId());
			$gaData->setData('profile_id',$key);
			$gaData->setData('profile_name',$profile['name']);
			$gaData->setData('date',$profile['results']['date']);
			$gaData->save();
			$gaData->addGAData($profile['results'],$profile['name']);
		}
	}

	public function getAnalyticsObject() {
		if(!$this->analytics) {
			$client = $this->getAuthorisedClient();
			$analytics = new apiAnalyticsService($client);
			$this->analytics = $analytics;
		}
		return $this->analytics;
	}

/*
 * Returns all profiles for a given account and their profile ids so that they can be quired
 *
 * name: getProfiles
 * @param
 * @return
 *
 */
	public function getProfiles() {
		$client = $this->getAuthorisedClient();
		$analytics = $this->getAnalyticsObject();
		$accounts = $analytics->management_accounts->listManagementAccounts();
		$profileData = array();
		//TODO: assume one account for will be
		if (count($accounts->getItems()) > 0) {
			$items = $accounts->getItems();
			foreach($items as $item) {
				$webproperties = $analytics->management_webproperties
				->listManagementWebproperties($item->getId());

				$profileList = array();
				if (count($webproperties->getItems()) > 0) {
					$propItems = $webproperties->getItems();
					//$firstWebpropertyId = $propItems[0]->getId();
					//TODO going to fast call
					foreach($propItems as $webProp) {
					sleep(1);

						$profiles = $analytics->management_profiles
						  ->listManagementProfiles($item->getId(), $webProp->getId());

						if (count($profiles->getItems()) > 0) {
							foreach($profiles->getItems() as $profileItem) {
								if(array_search($profileItem->getName(), $this->activeProfiles) !== false) {
									$profileData[$profileItem->getId()] = array('name' =>$webProp->websiteUrl." - ".$profileItem->getName());
								}
							}

						} else {
							throw new Exception('No profiles found for this user.');
						}
					}
				} else {
					throw new Exception('No webproperties found for this user.');
				}
			}
		} else {
			throw new Exception('No accounts found for this user.');
		}

		var_dump($profileData);
		return $profileData;
	}

	public function getGaKeys() {
		return $this->metrics;
	}

	/*************************************************************************************************
	 * GA Data Fetch for Data Analysis ***************************************************************
	**************************************************************************************************/

	private $_get_data_dimensions = array(
		'event_data' =>
			array(
				'dimensions' =>  array(
					'ga:date', 'ga:nthMinute', 'ga:dimension4', 'ga:sourceMedium', 'ga:deviceCategory', 'ga:eventCategory', 'ga:eventAction'
				),
				'metrics' => array(
					'ga:totalEvents'
				)
			),
		'page_data' =>
			array(
				'dimensions' =>  array(
					'ga:date', 'ga:nthMinute', 'ga:dimension4', 'ga:sourceMedium', 'ga:deviceCategory', 'ga:pagePath', 'ga:contentGroup2'
				),
				'metrics' => array(
					'ga:pageviews'
				)
			)
	);

	private $_allowed_account = array(
		"http://www.visiondirect.co.uk - Primary View - All Users"
	);

	public function getAllData($file_path = '/tmp/', $startDate = false,$endDate = false) {

		$client = $this->getAuthorisedClient();
		if(!$client) {
			echo "could not load authorised client store id:{$this->getStoreId()}\n";
			return false;
		}
		$profiles = $this->getProfiles();
		$analytics = $this->getAnalyticsObject();

		if($startDate == false || $endDate  == false) {
			$newdate = strtotime ('-60 day', time()) ;
			$calcDate = date('Y-m-d',$newdate);
			$date = $calcDate;
			$calcEndDate = date('Y-m-d');
			$startDate  = !$startDate ? $date : $startDate;
			$endDate =  !$endDate ? $calcEndDate : $endDate;
		}
		$starttimestmap = strtotime($startDate);
		$endtimestmap = strtotime($endDate);

		if($starttimestmap > $endtimestmap) {
			throw new Exception('Invalid date range');
		}
		$currentimestamp = $starttimestmap;

		$paging_results = array();

		foreach( $this->_get_data_dimensions as $dimension => $structure ) {

			foreach ($profiles as $key => $profile) {

				//accounts for this store
				if( array_search($profile['name'], $this->_allowed_account) === FALSE ) {
					continue;
				}

				$paging_results[$key] = array(
					'number_processed' => 0,
					'number_left'      => 0,
					'number_total'     => 0,
					'start_index'      => 1
				);

				try {

					while( $paging_results[$key]['number_processed'] < $paging_results[$key]['number_total']
						|| $paging_results[$key]['number_total'] === 0 ) {


						$result = $analytics->data_ga->get(
							'ga:' . $key,
							date('Y-m-d', $starttimestmap),
							date('Y-m-d', $endtimestmap),
							implode(',', $structure['metrics']),
							array(
								'dimensions' => implode(',', $structure['dimensions']),
								'start-index' => $paging_results[$key]['number_total']['start_index'],
								'max-results' => 10000,
								//'filters' => 'ga:transactionId==something'
								//'metrics' => implode(',',$this->metrics)

							)
						);

						//Generate CSV file
						$filename = $file_path.$dimension.'.csv';
						unlink($filename);


						//Column Headers
						$data = array();
						foreach($result->getColumnHeaders() as $columnHeader) {
							$data[] = $columnHeader->getName();
						}

						$this->writeToCSV($filename, array($data));

						//Row lines

						foreach($result->getRows() as $row) {

							$this->writeToCSV($filename, array($row));
						}

						$rows_count = count($result->getRows());

						$paging_results[$key]['number_processed'] += $rows_count;
						$paging_results[$key]['number_left'] = $result->getTotalResults() - $paging_results[$key]['number_processed'];
						$paging_results[$key]['number_total'] = $result->getTotalResults();
					}

				} catch (Exception $ex) {
					echo "\n".$ex->getMessage()."\n";
				}
			}
		}
	}

	private function writeToCSV( $file, $data ) {

		$csv      = new Varien_File_Csv();
		$delimiter = ',';
		$enclosure = '"';

		$csv->setEnclosure('"');

		$fh = fopen($file, 'a');
		foreach ($data as $dataRow) {
			$csv->fputcsv($fh, $dataRow, $delimiter, $enclosure);
		}
		fclose($fh);
	}
}


