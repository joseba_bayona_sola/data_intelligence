<?php
class Pointernal_Gasync_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	public function getChannel($source,$medium) {
		$channel = false;
		switch ($medium) {
			case '(none)':
				if($source == '(direct)') {
					$channel = 'Direct';
				}
				break;
			case 'google shopping':
				$channel = 'Product-listing-shopping';
				break;
			case 'social media':
				$channel = 'Social Media';
				break;
			case 'affiliates':
				$channel = 'Affiliates';
				break;
			case 'cpc':
				if($source == 'google') {
					$channel = 'Adwords';
				}
				else {
					$channel = ucwords($source)	. ' ppc';
				}
				break;
			case 'ppc':
				//if($source == 'google') {
				//	$channel = 'Adwords';
				//}
				//else {
				//	$channel = ucwords($source)	. ' ppc';
				//}
				//as per marketing request all ppc goes to adwords
				$channel = 'Adwords';
				break;
			case 'email':
				//note case sensitive
				if($source == 'marketing') {
					$channel = 'Email-marketing';
				} else if($source == 'Getlenses Ltd' || strtolower($source ) =="visiondirect" || strtolower($source )=="vision direct") {
					$channel = 'Email-marketing';
				}
				else if($source == 'Emailvision') {
					$channel = 'Email-marketing';
				}
				else if($source == 'transactional') {
					$channel = 'Email-transactional';
				}
				else if($source == 'transa_ctional') {
					$channel = 'Email-transactional';
				}
				else if($source == 'reminder') {
					$channel = 'Email-reminder';
				}
				else {
					$channel = 'Email';
				}
				break;
			case 'organic':
				$channel = 'Organic';
				break;
			case 'referral':
				if(strpos($source, 'mail')!== false) {
					$channel = 'Email';
				} else if(strpos($source, 'getlenses') === false && strpos($source, 'getoptics') === false) {
					$channel = 'Other';
				}
				break;
			default:
				$channel = false;
				break;
		}
		return $channel;
	}
}
	 