<?php
$installer = $this;
$installer->startSetup();
$installer->run(" CREATE TABLE  {$this->getTable('gasync/gatoken')} (
		id INT AUTO_INCREMENT NOT NULL,
		store_id INT NOT NULL,
		token text,	
		PRIMARY KEY(id)
	);
");

$installer->run(" CREATE TABLE  {$this->getTable('gasync/gadata')} (
		id INT AUTO_INCREMENT NOT NULL,
        store_id int,
		profile_id INT NOT NULL,
		profile_name varchar(255),
		date date,
		PRIMARY KEY(id),
		UNIQUE(profile_id,date,store_id)
	);
");

$installer->run(" CREATE TABLE  {$this->getTable('gasync/gaentitydata')} (
		id INT AUTO_INCREMENT NOT NULL,
        data_id INT NOT NULL,
		ga_key varchar(255),
        value decimal(12,4),
		PRIMARY KEY(id),
        FOREIGN KEY (data_id) REFERENCES {$this->getTable('gasync/gadata')}(id) ON DELETE CASCADE
	);
");

$installer->endSetup();
	
