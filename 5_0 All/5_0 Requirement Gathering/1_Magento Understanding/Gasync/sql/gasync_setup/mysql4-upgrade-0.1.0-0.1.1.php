<?php
$installer = $this;
$installer->startSetup();

$installer->run(" CREATE TABLE  {$this->getTable('gasync/gaentitytransactiondata')} (
		transactionId VARCHAR(20)  NOT NULL,
		source VARCHAR(100) ,
		medium VARCHAR(100),
		transactions VARCHAR(100),
		channel VARCHAR(100),
		date int,
		profile VARCHAR(100),
		PRIMARY KEY(transactionId)
	)
");

$installer->endSetup();