<?php   
class Pointernal_Gasync_Block_Auth extends Mage_Core_Block_Template{   

	public function getStoreName() {
		$store_id = Mage::registry('gasync_auth_store_id');
		$store = Mage::getModel('core/store')->load($store_id);
		if($store->getStoreId() > 0) {
			return $store->getName();
		}
		return false;
	}
	
	public function showAuthRequest() {
		$gaClient = Mage::getSingleton('gasync/client');
		if(!$gaClient->getIsAuth()) {
			return true;
		}
		return false;
	}
	
	public function getAuthLink() {
		$gaClient = Mage::getSingleton('gasync/client');
		return $gaClient->getAuthLink();
	}
	
	public function getAuthSuccess() {
		$gaClient = Mage::getSingleton('gasync/client');
		return $gaClient->getTokenSaved();
	}
	
	public function getAuthStoresInfo() {
		$authInfo = array();
		$client = Mage::getModel('gasync/client');
		$stores = $client->getEnabledStores();
		foreach($stores as $store_id) {
			$storeToken = Mage::getModel('gasync/gatoken')->getTokenByStoreId($store_id);
			$store = Mage::getModel('core/store')->load($store_id);
			$storeName = $store->getName();
			$hasToken = false;
			if($storeToken->getToken()) {
				$token = $storeToken->getToken();
			} else {
				$token  = FALSE;
			}
			$authInfo[$store_id] = array(
				'name'=>$storeName,
				'token'=>$token,
				'auth_url' => Mage::getUrl('gasync/index/auth',array('store_id'=>$store_id,'clear_session_store'=>1,'code'=>1)),
			);
		}
		
		return $authInfo;
	}
}
