<?php

require_once dirname(__DIR__).'/../../../../../app/Mage.php';
Mage::app();

class Gasync_UnitTest extends PHPUnit_Framework_TestCase
{
    private $_profile_id;
    private $_quote;
    private $_result;

    function setUp()
    {

    }
    
    function tearDown()
    {
        unset($this->_result);
    }

    function testGetStores()
    {
		$client = Mage::getModel('gasync/client');
		var_dump($client->getEnabledStores());
    }
    
    function testLoadStores()
    {
		$client = Mage::getModel('gasync/client');
		$token = Mage::getModel('gasync/gatoken');
		$stores = $client->getEnabledStores();
	
		foreach($stores as $store)  {
			$token->getTokenByStoreId($store);
			var_dump($token->getData());
		}
		//TODO: assert the tokens are actually there for the stores they ned to be 
    }
    
    function testAuthenticatedClient() {
		$client = Mage::getModel('gasync/client');
		$stores = $client->getEnabledStores();
		foreach($stores as $storeId) {
			$client->setStoreId($storeId);
			$startDate = '2012-09-02';
			$endDate = '2012-09-03';
			if(strtotime($endDate) > strtotime($startDate)) {
				while(strtotime($endDate) >= strtotime($startDate)) {
					$data = $client->getSyncData($startDate);
					$client->saveSyncData($data);
					$startDate = strtotime ('+1 day' , strtotime($startDate));
					$startDate = date ('Y-m-d' , $startDate);
				}
			}
		}
	}
}	
?>
