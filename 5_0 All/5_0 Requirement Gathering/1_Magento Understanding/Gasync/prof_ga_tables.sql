
-- ga_token
select id, store_id, token
from magento01.ga_token

-- ga_data
select id, store_id, profile_id, profile_name, date
from magento01.ga_data
order by id desc
limit 1000

  select count(*)
  from magento01.ga_data

  select profile_id, profile_name, count(*), min(date), max(date)
  from magento01.ga_data
  where date > '2020-01-01'
  group by profile_id, profile_name
  order by profile_name

  select store_id, count(*), min(date), max(date)
  from magento01.ga_data
  group by store_id
  order by store_id

  select date, count(*) 
  from magento01.ga_data
  group by date
  order by date desc
  
-- ga_entity_transaction_data
select transactionId, source, medium, transactions, channel, date, profile
from magento01.ga_entity_transaction_data
order by date desc
limit 1000

  select count(*)
  from magento01.ga_entity_transaction_data

  select profile, count(*), min(date), max(date)
  from magento01.ga_entity_transaction_data
  where date > 20200000
  group by profile
  order by profile

  select date, count(*)
  from magento01.ga_entity_transaction_data
  group by date
  order by date desc

-- ga_entity_data
select id, data_id, ga_key, value
from magento01.ga_entity_data

