<?php
class Pointernal_Gasync_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
      $this->renderLayout(); 
	  
    }
    
	public function authAction() {
		$session = Mage::getSingleton('core/session');
        if($this->getRequest()->getParam('clear_session_store')) {
            $session->setGaAuthStoreId(0);
        }
        $sessionStoreId = $session->getGaAuthStoreId();
		if(!$sessionStoreId) {
			$requestStoreId = $this->getRequest()->getParam('store_id');
			if(!$requestStoreId)  {
				$this->getResponse()->setBody('Error no store id found');
				return;
			}
			$session->setGaAuthStoreId($requestStoreId);	
		}
		$storeId =  $requestStoreId != false ? $requestStoreId : $sessionStoreId;
		
		Mage::register('gasync_auth_store_id',$storeId);
		
		
		$gaClient = Mage::getSingleton('gasync/client');
		$gaClient->setStoreId($storeId);
		$client = $gaClient->getClientInstance();
		
		if ($this->getRequest()->getParam('code')) {
			//$client->setAccessType('offline');
		  $client->authenticate();
		  $client->setAccessType('offline');
		  $session->setGaAuthToken($client->getAccessToken());
		  $this->_redirect('gasync/index/auth');
		}

		if ($session->getGaAuthToken()) {
			$client->setAccessToken($session->getGaAuthToken());
		}

		if (!$client->getAccessToken()) {
			$gaClient->setIsAuth(false);
			$authUrl = $client->createAuthUrl();
			$gaClient->setAuthLink($authUrl);
		}
		else {
			$gaClient->setIsAuth(true);
			$token = Mage::getModel('gasync/gatoken');
			$token->getTokenByStoreId($storeId);
			$token->setData('token',$client->getAccessToken());
			$token->setData('store_id',$storeId);
			$token->save();	

			$gaClient->setTokenSaved($client->getAccessToken());
		}

		$this->loadLayout();   
		$this->renderLayout(); 
	}
}
