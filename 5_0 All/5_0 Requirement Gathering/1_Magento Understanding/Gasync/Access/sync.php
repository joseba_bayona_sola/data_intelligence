<?php 
require_once dirname(__DIR__).'/../../../../../app/Mage.php';
Mage::app();


$client = Mage::getModel('gasync/client');
$stores = $client->getEnabledStores();
foreach($stores as $storeId) {
	$client->setStoreId($storeId);
	$data = $client->syncData();
}

?>