
-- $dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

-- dw_payment_method_map -- CREATE TABLE 

INSERT INTO dw_payment_method_map VALUES 
	('checkmo', 'Cheque'),
	('cybersource_soap', 'Card - New'),
    ('cybersourcestored', 'Card - Stored'),
    ('free', 'Free'),
    ('internetkassa_ideal', 'Ideal'),
    ('partnereyeplan', 'Eyeplan'),
    ('paypal_express', 'PayPal'),
    ('pnsofortueberweisung', 'Sofort');

-- dw_card_type_map -- CREATE TABLE 

INSERT INTO dw_card_type_map VALUES
	('LASER', 'Laser'),
    ('MC', 'Mastercard'),
    ('MCI', 'Maestro International'),
    ('SS', 'Switch/Solo/Maestro UK'),
    ('VI', 'Visa');

-- dw_flat_category -- CREATE TABLE 

INSERT INTO dw_flat_category (modality, modality_name, `type`, type_name, feature, feature_name, 
	category, product_type, product_type_name, 
	telesales_only) VALUES 
	
	('Daily','Dailies','Spherical','NONE','Standard','NONE','Dailies','Contact Lenses','lens','0'),
	('Daily','Dailies','Toric','Torics for Astigmatism','Standard','NONE','Dailies','Contact Lenses','lens','0'),
	('Daily','Dailies','Multifocal','Multifocals','Standard','NONE','Dailies','Contact Lenses','lens','0'),
	('Daily','Dailies','Spherical','NONE','SiHy','Silicone Hydrogel','Dailies','Contact Lenses','lens','0'),
	('Daily','Dailies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Dailies','Contact Lenses','lens','0'),
	('Daily','Dailies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Dailies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Spherical','NONE','Standard','NONE','Two Weeklies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','Standard','NONE','Two Weeklies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Multifocal','Multifocals','Standard','NONE','Two Weeklies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Spherical','NONE','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses','lens','0'),
	('Two Weekly','Two-Weekly','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Spherical','NONE','Standard','NONE','Monthlies','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Toric','Torics for Astigmatism','Standard','NONE','Torics for Astigmatism','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Multifocal','Multifocals','Standard','NONE','Multifocals','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','Standard','NONE','Multifocals','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Spherical','NONE','SiHy','Silicone Hydrogel','Monthlies','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Torics for Astigmatism','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses','lens','0'),
	('Daily','Dailies','Spherical','Feshlook','Coloured','NONE','Colours','Contact Lenses','lens','0'),
	('Monthly','Monthlies','Spherical','Monthly Coloured','Coloured','NONE','Colours','Contact Lenses','lens','0'),
	('','NONE','','NONE','','NONE','Telesales Only','Contact Lenses','lens','1'),

	('','NONE','','Multi-purpose solutions','','NONE','Multi-purpose Solutions','Solutions & Eye Care','solution','0'),
	('','NONE','','Hydrogen Peroxide','','NONE','Hydrogen Peroxide','Solutions & Eye Care','solution','0'),
	('','NONE','','Gas Permeable Solutions','','NONE','Gas Permeable Solutions','Solutions & Eye Care','solution','0'),
	('','NONE','','Travel Packs','','NONE','Travel Packs','Solutions & Eye Care','solution','0'),
	('','NONE','','Cleaners & Salines','','NONE','Cleaners & Salines','Solutions & Eye Care','solution','0'),
	('','NONE','','Eye Drops','','NONE','Eye Drops','Solutions & Eye Care','solution','0'),
	('','NONE','','Eye Wash & Sprays','','NONE','Eye Wash & Sprays','Solutions & Eye Care','solution','0'),
	('','NONE','','Dry Eyes','','NONE','Dry Eye Treatments','Solutions & Eye Care','solution','0'),
	('','NONE','','Ocular Vitamins','','NONE','Ocular Vitamins','Solutions & Eye Care','solution','0'),
	('','NONE','','Hygiene & Lid Care','','NONE','Hygiene & Lid Care','Solutions & Eye Care','solution','0'),
	('','NONE','','Eye Care Cosmetics','','NONE','Hypoallegenic Makeup','Solutions & Eye Care','solution','0'),

	('','NONE','','Glasses','','NONE','Glasses Frames','Glasses - Frame','glasses','0'),
	('','NONE','','NONE','','NONE','Glasses Lenses','Glasses - Lens','glasseslens','0'),
	('','NONE','','NONE','','NONE','Other','Other','other','0');

-- vw_categories -- CREATE OR REPLACE VIEW

	SELECT category_id, 
		modality, `type`, feature, 
		category, product_type
	FROM dw_flat_category;

-- dw_stores_map -- CREATE TABLE 
INSERT INTO dw_stores_map (store_id, store_name, website_group, store_type, visible) VALUES 
	('11','asda-contactlenses.co.uk','ASDA','White Label','1'),
	('5','eur.getlenses.nl','GetLenses','Europe','1'),
	('19','EyePlan','EyePlan','White Label','1'),
	('4','gbp.getlenses.nl','Getlenses','UK','1'),
	('6','getlenses.be','Getlenses','Europe','1'),
	('2','getlenses.co.uk','Getlenses','UK','1'),
	('18','getlenses.de','Getlenses','Europe','1'),
	('17','getlenses.es','Getlenses','Europe','1'),
	('3','getlenses.ie','Getlenses','Europe','1'),
	('10','getlenses.it','Getlenses','Europe','1'),
	('1','postoptics.co.uk','Getlenses','UK','1'),
	('8','surveys.getlenses.co.uk','Getlenses','UK','1'),
	('9','surveys.getlenses.nl','Getlenses','UK','1'),
	('7','vhi-contactlenses.ie','VHI','White Label','1'),
	('0','Default','Default','Default','0');

-- CREATE TABLE IF NOT EXISTS dw_stores
	SELECT * FROM dw_stores_map

-- vw_stores -- CREATE OR REPLACE VIEW 
	SELECT store_name, website_group, store_type
	FROM dw_stores
	WHERE visible =1;

-- vw_stores_full -- CREATE OR REPLACE VIEW 
	SELECT *
	FROM dw_stores

-- customer_created_in_map -- CREATE TABLE 
INSERT INTO customer_created_in_map (customer_created_in, new_customer_created_in) VALUES 
	('created_in','new value for created_in'),
	('asda-contactlenses.co.uk','asda-contactlenses.co.uk'),
	('eur.getlenses.nl','eur.getlenses.nl'),
	('EyePlan','EyePlan'),
	('gbp.getlenses.nl','gbp.getlenses.nl'),
	('getlenses.be','getlenses.be'),
	('getlenses.co.uk','getlenses.co.uk'),
	('getlenses.co.uk (English)','getlenses.co.uk'),
	('getlenses.co.uk (GL2 staging)','getlenses.co.uk'),
	('getlenses.de','getlenses.de'),
	('getlenses.dk','getlenses.dk'),
	('getlenses.es','getlenses.es'),
	('getlenses.ie','getlenses.ie'),
	('getlenses.ie (English)','getlenses.ie'),
	('getlenses.it','getlenses.it'),
	('getlenses.nl','getlenses.nl'),
	('postoptics.co.uk','postoptics.co.uk'),
	('postoptics.co.uk (English)','postoptics.co.uk'),
	('surveys','surveys.getlenses.nl'),
	('surveys.getlenses.co.uk','surveys.getlenses.co.uk'),
	('surveys.getlenses.nl','surveys.getlenses.nl'),
	('VHI','vhi-contactlenses.ie'),
	('vhi-contactlenses.ie','vhi-contactlenses.ie');

-- vw_order_status_history -- CREATE OR REPLACE VIEW 
SELECT id AS status_id,  
	order_id AS order_id, order_no AS order_no, 
	sh.`status` AS `status`, 
	sh.updated_at AS status_time,	
	user AS admin_username
FROM {$dbname}.sales_order_log sh;

-- vw_shipment_line_pack_sizes -- CREATE OR REPLACE VIEW 
SELECT pvx_order_item_id AS sub_line_id,
	order_item_id AS line_id, shipment_id AS  shipment_id,
	FLOOR(qty/packs_qty) AS packsize, packs_qty AS no_of_packs, qty AS qty
FROM 
		{$dbname}.po_warehouse_pvx_order_item ol	
	INNER JOIN 	
		{$dbname}.po_warehouse_pvx_order pvx ON pvx.pvx_order_id = ol.pvx_order_id
WHERE `status` != 'closed'
GROUP BY pvx_order_item_id;

-- dw_coupon_code_map -- CREATE TABLE 
REPLACE INTO dw_coupon_code_map VALUEs
	('WELCOME7','','Adwords'),
	('LENSES32','','Affiliates'),
	('WELCOME4','','Adwords'),
	('WELCOME5','','Adwords'),
	('WELCOME9','','Adwords'),
	....

-- vw_coupon_channe -- CREATE OR REPLACE VIEW 
SELECT `code` AS coupon_code,
	from_date AS start_date, to_date AS end_date,
	COALESCE(NULL,cm.business_source,'') AS source_code,
	COALESCE(NULL,cm.channel,'')  AS channel
FROM 
		{$dbname}.salesrule_coupon sc    
	INNER JOIN 
		{$dbname}.salesrule sr ON sc.rule_id = sr.rule_id
    LEFT JOIN 
		dw_coupon_code_map cm ON cm.coupon_code = sc.code;

-- dw_bis_source_map -- CREATE TABLE 
INSERT INTO dw_bis_source_map VALUES 
	('adwords-cb','Adwords'),
	('adwords-cl','Adwords'),
	('adwords-nc','Adwords'),
	('adwords-nc-ae','Adwords'),
	('adwords-ncp','Adwords'),
	('adwords-np','Adwords'),
	('adwords-s','Adwords'),
	('adwords-sb','Adwords'),
	('adwords-sp','Adwords'),
	....

-- dw_calendar -- CREATE TABLE 

	-- insert  into `dw_calendar
	-- insert  into `dw_calendar
	-- insert  into `dw_calendar

-- dw_countries -- CREATE TABLE 
	insert into `dw_countries`(`country_name`,`country_code`,`country_type`,`country_zone`,`country_continent`) values 
     
-----------------------------------------------------------------

-- 0.1.0 --> 0.1.1

-- 0.1.1 --> 0.1.2

	-- vw_coupon_channel -- CREATE OR REPLACE VIEW
	SELECT `code` AS coupon_code,
		from_date AS start_date, to_date AS end_date,
		COALESCE(NULL,cm.business_source,'') AS source_code,
		COALESCE(NULL,cm.channel,'')  AS channel,
		sr.postoptics_only_new_customer AS new_customer, cast(sr.description as char(8000)) description
	FROM 
			{$dbname}.salesrule_coupon sc    
		INNER JOIN 
			{$dbname}.salesrule sr ON sc.rule_id = sr.rule_id
		LEFT JOIN 
			dw_coupon_code_map cm ON cm.coupon_code = sc.code;

-- 0.1.2 --> 0.1.3
	-- dw_glasses_products -- CREATE TABLE 
	INSERT INTO `dw_glasses_products` (`product_id`, `name`) values
		('1762','K/NOR/BK015/M'),
		('1763','K/EMP/BMX54/M'),
		('1765','K/EMP/BMX52/M'),
		('1766','K/EMP/BMX53/M'),
		('1764','K/EMP/BMXTEEN800/P'),
		('1767','K/EMP/BMX50/M'),
		('1768','K/EMP/BMX59/M'),
		('1769','K/EMP/BMX60/M'),
		('1770','K/EMP/BMX28/P'),
		....

	-- dw_deleted_shipment -- CREATE TABLE 

	-- vw_wearer_prescription -- CREATE OR REPLACE VIEW 
	SELECT 	
		`id`, `wearer_id`, `method_id`, 
		CASE WHEN `date_last_prescription` = '0000-00-00 00:00:00' THEN NULL ELSE date_last_prescription END date_last_prescription, 
		CASE WHEN `date_start_prescription` = '0000-00-00 00:00:00' THEN NULL ELSE date_start_prescription END date_start_prescription, 
		`optician_name`, 
		`optician_address1`, `optician_address2`, `optician_city`, `optician_state`, `optician_country`, `optician_postcode`, `optician_phone`, 
		`optician_lookup_id`, 
		`reminder_sent`
	FROM {$dbname}.wearer_prescription`;

	-- vw_wearer -- CREATE OR REPLACE VIEW 
	SELECT id, 
		`name`, CASE WHEN DOB = '0000-00-00 00:00:00' THEN NULL ELSE DOB END DOB, 
		customer_id
	FROM {$dbname}.wearer;

	-- dw_bis_source_map 
	REPLACE INTO dw_bis_source_map VALUES 
		('affiliates-af-za','Affiliates');

-- 0.1.3 --> 0.1.4
	-- dw_flat_category -- CREATE TABLE 
	INSERT INTO dw_flat_category (category_id, promotional_product, 
		modality, modality_name, `type`, type_name, feature, feature_name,
		category, product_type, product_type_name, 
		telesales_only) VALUES 

		('1','0','Daily','Dailies','Spherical','NONE','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
		('2','0','Daily','Dailies','Toric','Torics for Astigmatism','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
		('3','0','Daily','Dailies','Multifocal','Multifocals','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
		('4','0','Daily','Dailies','Spherical','NONE','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
		('5','0','Daily','Dailies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
		('6','0','Daily','Dailies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
		('7','0','Two Weekly','Two-Weekly','Spherical','NONE','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('8','0','Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('9','0','Two Weekly','Two-Weekly','Multifocal','Multifocals','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('10','0','Two Weekly','Two-Weekly','Spherical','NONE','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('11','0','Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('12','0','Two Weekly','Two-Weekly','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
		('13','0','Monthly','Monthlies','Spherical','NONE','Standard','NONE','Monthlies','Contact Lenses - Monthlies & Other','lens','0'),
		('14','0','Monthly','Monthlies','Toric','Torics for Astigmatism','Standard','NONE','Torics for Astigmatism','Contact Lenses - Monthlies & Other','lens','0'),
		('15','0','Monthly','Monthlies','Multifocal','Multifocals','Standard','NONE','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
		('16','0','Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','Standard','NONE','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
		('17','0','Monthly','Monthlies','Spherical','NONE','SiHy','Silicone Hydrogel','Monthlies','Contact Lenses - Monthlies & Other','lens','0'),
		('18','0','Monthly','Monthlies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Torics for Astigmatism','Contact Lenses - Monthlies & Other','lens','0'),
		('19','0','Monthly','Monthlies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
		('20','0','Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
		('21','0','Daily','Dailies','Spherical','Feshlook','Coloured','NONE','Colours','Contact Lenses - Monthlies & Other','lens','0'),
		('22','0','Monthly','Monthlies','Spherical','Monthly Coloured','Coloured','NONE','Colours','Contact Lenses - Monthlies & Other','lens','0'),
		('23','0','','NONE','','NONE','','NONE','Telesales Only','Contact Lenses - Monthlies & Other','lens','1'),
		('24','0','','NONE','','Multi-purpose solutions','','NONE','Multi-purpose Solutions','Solutions & Eye Care','solution','0'),
		('25','0','','NONE','','Hydrogen Peroxide','','NONE','Hydrogen Peroxide','Solutions & Eye Care','solution','0'),
		('26','0','','NONE','','Gas Permeable Solutions','','NONE','Gas Permeable Solutions','Solutions & Eye Care','solution','0'),
		('27','0','','NONE','','Travel Packs','','NONE','Travel Packs','Solutions & Eye Care','solution','0'),
		('28','0','','NONE','','Cleaners & Salines','','NONE','Cleaners & Salines','Solutions & Eye Care','solution','0'),
		('29','0','','NONE','','Eye Drops','','NONE','Eye Drops','Solutions & Eye Care','solution','0'),
		('30','0','','NONE','','Eye Wash & Sprays','','NONE','Eye Wash & Sprays','Solutions & Eye Care','solution','0'),
		('31','0','','NONE','','Dry Eyes','','NONE','Dry Eye Treatments','Solutions & Eye Care','solution','0'),
		('32','0','','NONE','','Ocular Vitamins','','NONE','Ocular Vitamins','Solutions & Eye Care','solution','0'),
		('33','0','','NONE','','Hygiene & Lid Care','','NONE','Hygiene & Lid Care','Solutions & Eye Care','solution','0'),
		('34','0','','NONE','','Eye Care Cosmetics','','NONE','Hypoallegenic Makeup','Solutions & Eye Care','solution','0'),
		('35','0','','NONE','','Glasses','','NONE','Glasses Frames','Glasses - Frame','glasses','0'),
		('36','0','','NONE','','NONE','','NONE','Glasses Lenses','Glasses - Lens','glasseslens','0'),
		('37','0','','NONE','','NONE','','NONE','Other','Other','other','0'),
		('501','1','Daily','','Spherical','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('502','1','Daily','','Toric','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('503','1','Daily','','Multifocal','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('504','1','Daily','','Spherical','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('505','1','Daily','','Toric','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('506','1','Daily','','Multifocal','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
		('507','1','Two Weekly','','Spherical','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('508','1','Two Weekly','','Toric','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('509','1','Two Weekly','','Multifocal','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('510','1','Two Weekly','','Spherical','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('511','1','Two Weekly','','Toric','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('512','1','Two Weekly','','Multifocal','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('513','1','Monthly','','Spherical','','Standard','','{Promo} Monthlies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('514','1','Monthly','','Toric','','Standard','','{Promo} Torics for Astigmatism','{Promo} Contact Lenses - Monthlies & Other','',''),
		('515','1','Monthly','','Multifocal','','Standard','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
		('516','1','Monthly','','Multifocal Toric','','Standard','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
		('517','1','Monthly','','Spherical','','SiHy','','{Promo} Monthlies','{Promo} Contact Lenses - Monthlies & Other','',''),
		('518','1','Monthly','','Toric','','SiHy','','{Promo} Torics for Astigmatism','{Promo} Contact Lenses - Monthlies & Other','',''),
		('519','1','Monthly','','Multifocal','','SiHy','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
		('520','1','Monthly','','Multifocal Toric','','SiHy','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
		('521','1','Daily','','Spherical','','Coloured','','{Promo} Colours','{Promo} Contact Lenses - Monthlies & Other','',''),
		('522','1','Monthly','','Spherical','','Coloured','','{Promo} Colours','{Promo} Contact Lenses - Monthlies & Other','',''),
		('523','1','','','','','','','{Promo} Telesales Only','{Promo} Solutions & Eye Care','',''),
		('524','1','','','','','','','{Promo} Multi-purpose Solutions','{Promo} Solutions & Eye Care','',''),
		('525','1','','','','','','','{Promo} Hydrogen Peroxide','{Promo} Solutions & Eye Care','',''),
		('526','1','','','','','','','{Promo} Gas Permeable Solutions','{Promo} Solutions & Eye Care','',''),
		('527','1','','','','','','','{Promo} Travel Packs','{Promo} Solutions & Eye Care','',''),
		('528','1','','','','','','','{Promo} Cleaners & Salines','{Promo} Solutions & Eye Care','',''),
		('529','1','','','','','','','{Promo} Eye Drops','{Promo} Solutions & Eye Care','',''),
		('530','1','','','','','','','{Promo} Eye Wash & Sprays','{Promo} Solutions & Eye Care','',''),
		('531','1','','','','','','','{Promo} Dry Eye Treatments','{Promo} Solutions & Eye Care','',''),
		('532','1','','','','','','','{Promo} Ocular Vitamins','{Promo} Solutions & Eye Care','',''),
		('533','1','','','','','','','{Promo} Hygiene & Lid Care','{Promo} Solutions & Eye Care','',''),
		('534','1','','','','','','','{Promo} Hypoallegenic Makeup','{Promo} Solutions & Eye Care','',''),
		('535','1','','','','','','','{Promo} Glasses Frames','{Promo} Glasses - Frame','',''),
		('536','1','','','','','','','{Promo} Glasses Lenses','{Promo} Glasses - Lens','',''),
		('537','1','','','','','','','{Promo} Other','{Promo} Other','','');

	-- vw_categories -- CREATE OR REPLACE VIEW
		SELECT category_id, promotional_product
			modality, `type`, feature, 
			category, product_type
		FROM dw_flat_category;

-- 0.1.4 --> 0.1.5
	-- dw_stores - dw_stores_map
	UPDATE dw_stores SET store_type = 'Core UK' WHERE store_type = 'UK'
	UPDATE dw_stores_map SET store_type = 'Core UK' WHERE store_type = 'UK'

	-- vw_categories -- CREATE OR REPLACE VIEW 
	SELECT category_id, promotional_product,
		modality, `type`, feature, 
		category, product_type
	FROM dw_flat_category;

	-- dw_payment_method_map 
	REPLACE INTO dw_payment_method_map VALUES ('imxpodpayment','Payment on Delivery');

	-- salesrule/rule
	ALTER TABLE {$dbname}.{$this->getTable('salesrule/rule')} ADD COLUMN `channel` VARCHAR(255)

	-- dw_coupon_code_map
	REPLACE INTO dw_coupon_code_map VALUES 
		('*REFER-A-FRIEND','','Refer A Friend'),
		('*REFER-FRIEND','','Refer A Friend'),
		('*REFER-FRIEND-IT','','Refer A Friend');

	UPDATE 
		{$dbname}. `{$this->getTable('salesrule/rule')}` sr, 
		dw_coupon_code_map dc, 
		{$dbname}.`{$this->getTable('salesrule/coupon')}` src 
	SET sr.channel =  dc.channel 
	WHERE sr.`rule_id` = src.rule_id AND  dc.coupon_code = src.`code`;

	-- vw_coupon_channel -- CREATE OR REPLACE VIEW 
	SELECT `code` AS coupon_code,
		from_date AS start_date, to_date AS end_date,
		COALESCE(NULL,'') AS source_code,
		sr.channel AS channel,
		sr.postoptics_only_new_customer AS new_customer,
		cast(sr.description as char(8000)) description
	FROM 
			{$dbname}.salesrule_coupon sc    
		INNER JOIN 
			{$dbname}.salesrule sr ON sc.rule_id = sr.rule_id;

	-- dw_bis_source_map 
	REPLACE INTO dw_bis_source_map VALUES
		('adwords-br','Adwords'),
		('adwords-branded-pages','Adwords'),
		('adwords-nc','Adwords'),
		('adwords-cl','Adwords'),
		('adwords-np','Adwords'),
		('TradeDoubler','Affiliates'),
		.... 

-- 0.1.5 --> 0.1.6
	-- dw_coupon_code_map
	ALTER TABLE `dw_coupon_code_map` DROP COLUMN `business_source`
	ALTER TABLE `dw_coupon_code_map` ADD COLUMN is_raf INT NOT NULL  DEFAULT  0;

	replace into `dw_coupon_code_map` (`coupon_code`, `channel`) values 
		('*GLreorder1','Other-marketing'),
		('*REFER-A-FRIEND','Refer-a-friend'),
		('*REFER-FRIEND','Refer-a-friend'),
		('*REFER-FRIEND-IT','Refer-a-friend'),
		('10KORTING','Other-marketing'),
		('2080998272','Staff'),
		('2OOFF','Other-marketing'),
		....

	UPDATE 
			$dbname.salesrule  sr 
		INNER JOIN 
			$dbname.salesrule_coupon src ON sr.rule_id = src.rule_id
		INNER JOIN 
			dw_coupon_code_map mp ON mp.coupon_code = src.code
	SET sr.channel = mp.channel
	WHERE mp.channel != '' AND  mp.channel  IS NOT NULL;

	REPLACE INTO  dw_bis_source_map VALUES 
		('redir_masterlens','Redirect'),
		('redir_postoptics','Redirect'),
		('googleshopping','Product-listing-shopping'),
		('googlebase','Product-listing-shopping'),
		('ciao','Price-comparison'),
		('pricerunner','Price-comparison'),
		('email-transactional','Email-transactional'),
		....

	-- dw_bis_map_dynamic_types -- CREATE TABLE 
	INSERT INTO  dw_bis_map_dynamic_types VALUES
		('adword','Adwords'),
		('affiliate','Affiliates'),
		('googleshopping','Product-listing-shopping')

-- 0.1.6 --> 0.1.7

	-- dw_flat_category -- CREATE TABLE 
	INSERT INTO {$dbname}.`dw_flat_category` 
		(`category_id`, `promotional_product`, 
		`modality`, `modality_name`, `modality_path`, `type`, `type_name`, `type_path`, `feature`, `feature_name`, 
		`category`, `product_type`, `product_type_name`, 
		`telesales_only`, `feature_path`) VALUES

		(1, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
		(2, 0, 'Daily', 'Dailies', '1/2/4/10', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
		(3, 0, 'Daily', 'Dailies', '1/2/4/10', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
		(4, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
		(5, 0, 'Daily', 'Dailies', '1/2/4/10', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
		(6, 0, 'Daily', 'Dailies', '1/2/4/10', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
		(7, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(8, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(9, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(10, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(11, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(12, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(13, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Monthlies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(14, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Torics for Astigmatism', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(15, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(16, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal Toric', 'Multifocal Toric Lenses', '1/2/4/14/177', 'Standard', 'NONE', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
		(17, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Monthlies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(18, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Torics for Astigmatism', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(19, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(20, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal Toric', 'Multifocal Toric Lenses', '1/2/4/14/177', 'SiHy', 'Silicone Hydrogel', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
		(21, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'Feshlook', '', 'Coloured', 'NONE', 'Colours', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/15'),
		(22, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'Monthly Coloured', '', 'Coloured', 'NONE', 'Colours', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/15'),
		(23, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Telesales Only', 'Contact Lenses - Monthlies & Other', 'lens', 1, ''),
		(24, 0, '', 'NONE', '', '', 'Multi-purpose solutions', '1/2/17/19', '', 'NONE', 'Multi-purpose Solutions', 'Solutions & Eye Care', 'solution', 0, ''),
		(25, 0, '', 'NONE', '', '', 'Hydrogen Peroxide', '1/2/17/20', '', 'NONE', 'Hydrogen Peroxide', 'Solutions & Eye Care', 'solution', 0, ''),
		(26, 0, '', 'NONE', '', '', 'Gas Permeable Solutions', '1/2/17/21', '', 'NONE', 'Gas Permeable Solutions', 'Solutions & Eye Care', 'solution', 0, ''),
		(27, 0, '', 'NONE', '', '', 'Travel Packs', '1/2/17/22', '', 'NONE', 'Travel Packs', 'Solutions & Eye Care', 'solution', 0, ''),
		(28, 0, '', 'NONE', '', '', 'Cleaners & Salines', '1/2/17/29', '', 'NONE', 'Cleaners & Salines', 'Solutions & Eye Care', 'solution', 0, ''),
		(29, 0, '', 'NONE', '', '', 'Eye Drops', '1/2/127/27', '', 'NONE', 'Eye Drops', 'Solutions & Eye Care', 'solution', 0, ''),
		(30, 0, '', 'NONE', '', '', 'Eye Wash & Sprays', '1/2/127/101', '', 'NONE', 'Eye Wash & Sprays', 'Solutions & Eye Care', 'solution', 0, ''),
		(31, 0, '', 'NONE', '', '', 'Dry Eyes', '1/2/127/26', '', 'NONE', 'Dry Eye Treatments', 'Solutions & Eye Care', 'solution', 0, ''),
		(32, 0, '', 'NONE', '', '', 'Ocular Vitamins', '1/2/127/25', '', 'NONE', 'Ocular Vitamins', 'Solutions & Eye Care', 'solution', 0, ''),
		(33, 0, '', 'NONE', '', '', 'Hygiene & Lid Care', '1/2/127/28', '', 'NONE', 'Hygiene & Lid Care', 'Solutions & Eye Care', 'solution', 0, ''),
		(34, 0, '', 'NONE', '', '', 'Eye Care Cosmetics', '', '', 'NONE', 'Hypoallegenic Makeup', 'Solutions & Eye Care', 'solution', 0, ''),
		(35, 0, '', 'NONE', '', '', 'Glasses', '', '', 'NONE', 'Glasses Frames', 'Glasses - Frame', 'glasses', 0, ''),
		(36, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Glasses Lenses', 'Glasses - Lens', 'glasseslens', 0, ''),
		(37, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Other', 'Other', 'other', 0, ''),
		(501, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
		(502, 1, 'Daily', '', '1/2/4/10', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
		(503, 1, 'Daily', '', '1/2/4/10', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
		(504, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
		(505, 1, 'Daily', '', '1/2/4/10', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
		(506, 1, 'Daily', '', '1/2/4/10', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
		(507, 1, 'Two Weekly', '', '1/2/4/11', 'Spherical', '', '', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(508, 1, 'Two Weekly', '', '1/2/4/11', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(509, 1, 'Two Weekly', '', '1/2/4/11', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(510, 1, 'Two Weekly', '', '1/2/4/11', 'Spherical', '', '', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(511, 1, 'Two Weekly', '', '1/2/4/11', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(512, 1, 'Two Weekly', '', '1/2/4/11', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(513, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'Standard', '', '{Promo} Monthlies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(514, 1, 'Monthly', '', '1/2/4/12', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Torics for Astigmatism', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(515, 1, 'Monthly', '', '1/2/4/12', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(516, 1, 'Monthly', '', '1/2/4/12', 'Multifocal Toric', '', '1/2/4/14/177', 'Standard', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
		(517, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'SiHy', '', '{Promo} Monthlies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(518, 1, 'Monthly', '', '1/2/4/12', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Torics for Astigmatism', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(519, 1, 'Monthly', '', '1/2/4/12', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(520, 1, 'Monthly', '', '1/2/4/12', 'Multifocal Toric', '', '1/2/4/14/177', 'SiHy', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
		(521, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'Coloured', '', '{Promo} Colours', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/15'),
		(522, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'Coloured', '', '{Promo} Colours', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/15'),
		(523, 1, '', '', '', '', '', '', '', '', '{Promo} Telesales Only', '{Promo} Solutions & Eye Care', '', 0, ''),
		(524, 1, '', '', '', '', '', '', '', '', '{Promo} Multi-purpose Solutions', '{Promo} Solutions & Eye Care', '', 0, ''),
		(525, 1, '', '', '', '', '', '', '', '', '{Promo} Hydrogen Peroxide', '{Promo} Solutions & Eye Care', '', 0, ''),
		(526, 1, '', '', '', '', '', '', '', '', '{Promo} Gas Permeable Solutions', '{Promo} Solutions & Eye Care', '', 0, ''),
		(527, 1, '', '', '', '', '', '', '', '', '{Promo} Travel Packs', '{Promo} Solutions & Eye Care', '', 0, ''),
		(528, 1, '', '', '', '', '', '', '', '', '{Promo} Cleaners & Salines', '{Promo} Solutions & Eye Care', '', 0, ''),
		(529, 1, '', '', '', '', '', '', '', '', '{Promo} Eye Drops', '{Promo} Solutions & Eye Care', '', 0, ''),
		(530, 1, '', '', '', '', '', '', '', '', '{Promo} Eye Wash & Sprays', '{Promo} Solutions & Eye Care', '', 0, ''),
		(531, 1, '', '', '', '', '', '', '', '', '{Promo} Dry Eye Treatments', '{Promo} Solutions & Eye Care', '', 0, ''),
		(532, 1, '', '', '', '', '', '', '', '', '{Promo} Ocular Vitamins', '{Promo} Solutions & Eye Care', '', 0, ''),
		(533, 1, '', '', '', '', '', '', '', '', '{Promo} Hygiene & Lid Care', '{Promo} Solutions & Eye Care', '', 0, ''),
		(534, 1, '', '', '', '', '', '', '', '', '{Promo} Hypoallegenic Makeup', '{Promo} Solutions & Eye Care', '', 0, ''),
		(535, 1, '', '', '', '', '', '', '', '', '{Promo} Glasses Frames', '{Promo} Glasses - Frame', '', 0, ''),
		(536, 1, '', '', '', '', '', '', '', '', '{Promo} Glasses Lenses', '{Promo} Glasses - Lens', '', 0, ''),
		(537, 1, '', '', '', '', '', '', '', '', '{Promo} Other', '{Promo} Other', '', 0, '');");