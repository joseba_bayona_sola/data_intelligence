<?php
//proper table syntax
$installer = $this;
$installer->startSetup();


$installer->run("DROP TABLE IF EXISTS dw_flat_category;");


$installer->run("CREATE TABLE IF NOT EXISTS dw_flat_category(
	category_id INT NOT NULL AUTO_INCREMENT ,
    promotional_product TINYINT,
	modality VARCHAR(100),
	modality_name VARCHAR(100),
	`type` VARCHAR(100),
	type_name VARCHAR(100),
	feature VARCHAR(100),
	feature_name VARCHAR(100),
	category VARCHAR(100), 
	product_type VARCHAR(100),
	product_type_name VARCHAR(100),
	telesales_only INT,
	PRIMARY KEY(category_id)
);");

$installer->run("INSERT INTO dw_flat_category (category_id,promotional_product,modality,modality_name,`type`,type_name,feature,feature_name,category,product_type,product_type_name,telesales_only) 
VALUES 
('1','0','Daily','Dailies','Spherical','NONE','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
('2','0','Daily','Dailies','Toric','Torics for Astigmatism','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
('3','0','Daily','Dailies','Multifocal','Multifocals','Standard','NONE','Dailies','Contact Lenses - Dailies','lens','0'),
('4','0','Daily','Dailies','Spherical','NONE','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
('5','0','Daily','Dailies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
('6','0','Daily','Dailies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Dailies','Contact Lenses - Dailies','lens','0'),
('7','0','Two Weekly','Two-Weekly','Spherical','NONE','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('8','0','Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('9','0','Two Weekly','Two-Weekly','Multifocal','Multifocals','Standard','NONE','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('10','0','Two Weekly','Two-Weekly','Spherical','NONE','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('11','0','Two Weekly','Two-Weekly','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('12','0','Two Weekly','Two-Weekly','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Two Weeklies','Contact Lenses - Monthlies & Other','lens','0'),
('13','0','Monthly','Monthlies','Spherical','NONE','Standard','NONE','Monthlies','Contact Lenses - Monthlies & Other','lens','0'),
('14','0','Monthly','Monthlies','Toric','Torics for Astigmatism','Standard','NONE','Torics for Astigmatism','Contact Lenses - Monthlies & Other','lens','0'),
('15','0','Monthly','Monthlies','Multifocal','Multifocals','Standard','NONE','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
('16','0','Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','Standard','NONE','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
('17','0','Monthly','Monthlies','Spherical','NONE','SiHy','Silicone Hydrogel','Monthlies','Contact Lenses - Monthlies & Other','lens','0'),
('18','0','Monthly','Monthlies','Toric','Torics for Astigmatism','SiHy','Silicone Hydrogel','Torics for Astigmatism','Contact Lenses - Monthlies & Other','lens','0'),
('19','0','Monthly','Monthlies','Multifocal','Multifocals','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
('20','0','Monthly','Monthlies','Multifocal Toric','Multifocal Toric Lenses','SiHy','Silicone Hydrogel','Multifocals','Contact Lenses - Monthlies & Other','lens','0'),
('21','0','Daily','Dailies','Spherical','Feshlook','Coloured','NONE','Colours','Contact Lenses - Monthlies & Other','lens','0'),
('22','0','Monthly','Monthlies','Spherical','Monthly Coloured','Coloured','NONE','Colours','Contact Lenses - Monthlies & Other','lens','0'),
('23','0','','NONE','','NONE','','NONE','Telesales Only','Contact Lenses - Monthlies & Other','lens','1'),
('24','0','','NONE','','Multi-purpose solutions','','NONE','Multi-purpose Solutions','Solutions & Eye Care','solution','0'),
('25','0','','NONE','','Hydrogen Peroxide','','NONE','Hydrogen Peroxide','Solutions & Eye Care','solution','0'),
('26','0','','NONE','','Gas Permeable Solutions','','NONE','Gas Permeable Solutions','Solutions & Eye Care','solution','0'),
('27','0','','NONE','','Travel Packs','','NONE','Travel Packs','Solutions & Eye Care','solution','0'),
('28','0','','NONE','','Cleaners & Salines','','NONE','Cleaners & Salines','Solutions & Eye Care','solution','0'),
('29','0','','NONE','','Eye Drops','','NONE','Eye Drops','Solutions & Eye Care','solution','0'),
('30','0','','NONE','','Eye Wash & Sprays','','NONE','Eye Wash & Sprays','Solutions & Eye Care','solution','0'),
('31','0','','NONE','','Dry Eyes','','NONE','Dry Eye Treatments','Solutions & Eye Care','solution','0'),
('32','0','','NONE','','Ocular Vitamins','','NONE','Ocular Vitamins','Solutions & Eye Care','solution','0'),
('33','0','','NONE','','Hygiene & Lid Care','','NONE','Hygiene & Lid Care','Solutions & Eye Care','solution','0'),
('34','0','','NONE','','Eye Care Cosmetics','','NONE','Hypoallegenic Makeup','Solutions & Eye Care','solution','0'),
('35','0','','NONE','','Glasses','','NONE','Glasses Frames','Glasses - Frame','glasses','0'),
('36','0','','NONE','','NONE','','NONE','Glasses Lenses','Glasses - Lens','glasseslens','0'),
('37','0','','NONE','','NONE','','NONE','Other','Other','other','0'),
('501','1','Daily','','Spherical','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('502','1','Daily','','Toric','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('503','1','Daily','','Multifocal','','Standard','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('504','1','Daily','','Spherical','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('505','1','Daily','','Toric','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('506','1','Daily','','Multifocal','','SiHy','','{Promo} Dailies','{Promo} Contact Lenses - Dailies','',''),
('507','1','Two Weekly','','Spherical','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('508','1','Two Weekly','','Toric','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('509','1','Two Weekly','','Multifocal','','Standard','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('510','1','Two Weekly','','Spherical','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('511','1','Two Weekly','','Toric','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('512','1','Two Weekly','','Multifocal','','SiHy','','{Promo} Two Weeklies','{Promo} Contact Lenses - Monthlies & Other','',''),
('513','1','Monthly','','Spherical','','Standard','','{Promo} Monthlies','{Promo} Contact Lenses - Monthlies & Other','',''),
('514','1','Monthly','','Toric','','Standard','','{Promo} Torics for Astigmatism','{Promo} Contact Lenses - Monthlies & Other','',''),
('515','1','Monthly','','Multifocal','','Standard','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
('516','1','Monthly','','Multifocal Toric','','Standard','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
('517','1','Monthly','','Spherical','','SiHy','','{Promo} Monthlies','{Promo} Contact Lenses - Monthlies & Other','',''),
('518','1','Monthly','','Toric','','SiHy','','{Promo} Torics for Astigmatism','{Promo} Contact Lenses - Monthlies & Other','',''),
('519','1','Monthly','','Multifocal','','SiHy','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
('520','1','Monthly','','Multifocal Toric','','SiHy','','{Promo} Multifocals','{Promo} Contact Lenses - Monthlies & Other','',''),
('521','1','Daily','','Spherical','','Coloured','','{Promo} Colours','{Promo} Contact Lenses - Monthlies & Other','',''),
('522','1','Monthly','','Spherical','','Coloured','','{Promo} Colours','{Promo} Contact Lenses - Monthlies & Other','',''),
('523','1','','','','','','','{Promo} Telesales Only','{Promo} Solutions & Eye Care','',''),
('524','1','','','','','','','{Promo} Multi-purpose Solutions','{Promo} Solutions & Eye Care','',''),
('525','1','','','','','','','{Promo} Hydrogen Peroxide','{Promo} Solutions & Eye Care','',''),
('526','1','','','','','','','{Promo} Gas Permeable Solutions','{Promo} Solutions & Eye Care','',''),
('527','1','','','','','','','{Promo} Travel Packs','{Promo} Solutions & Eye Care','',''),
('528','1','','','','','','','{Promo} Cleaners & Salines','{Promo} Solutions & Eye Care','',''),
('529','1','','','','','','','{Promo} Eye Drops','{Promo} Solutions & Eye Care','',''),
('530','1','','','','','','','{Promo} Eye Wash & Sprays','{Promo} Solutions & Eye Care','',''),
('531','1','','','','','','','{Promo} Dry Eye Treatments','{Promo} Solutions & Eye Care','',''),
('532','1','','','','','','','{Promo} Ocular Vitamins','{Promo} Solutions & Eye Care','',''),
('533','1','','','','','','','{Promo} Hygiene & Lid Care','{Promo} Solutions & Eye Care','',''),
('534','1','','','','','','','{Promo} Hypoallegenic Makeup','{Promo} Solutions & Eye Care','',''),
('535','1','','','','','','','{Promo} Glasses Frames','{Promo} Glasses - Frame','',''),
('536','1','','','','','','','{Promo} Glasses Lenses','{Promo} Glasses - Lens','',''),
('537','1','','','','','','','{Promo} Other','{Promo} Other','','');");


$installer->run("CREATE OR REPLACE VIEW vw_categories AS
SELECT 	category_id, 
    promotional_product
	modality, 
	`type`, 
	feature, 
	category, 
	product_type
FROM 
dw_flat_category;");
    
$installer->endSetup();
	 
