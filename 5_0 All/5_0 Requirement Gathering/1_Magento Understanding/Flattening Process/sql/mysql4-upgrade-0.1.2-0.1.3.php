<?php
//proper table syntax
$installer = $this;
$installer->startSetup();

$dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

$installer->run("DROP TABLE IF EXISTS dw_glasses_products;"); 

$installer->run("CREATE TABLE IF NOT EXISTS dw_glasses_products (
	product_id INT ,  
	name VARCHAR(255),
	PRIMARY KEY (product_id)
);");

$installer->run("INSERT INTO`dw_glasses_products` (`product_id`, `name`) values('1762','K/NOR/BK015/M'),
('1763','K/EMP/BMX54/M'),
('1765','K/EMP/BMX52/M'),
('1766','K/EMP/BMX53/M'),
('1764','K/EMP/BMXTEEN800/P'),
('1767','K/EMP/BMX50/M'),
('1768','K/EMP/BMX59/M'),
('1769','K/EMP/BMX60/M'),
('1770','K/EMP/BMX28/P'),
('1771','K/EMP/BMXTEEN801/M'),
('1772','K/EMP/BMX50/M'),
('1773','K/EMP/BMXTEEN801/M'),
('1774','K/EMP/BMXTEEN802/G'),
('1775','K/EMP/BMXTEEN811/M'),
('1776','K/NOU/OD10/P'),
('1777','K/NOU/OD18/P'),
('1778','K/EMP/HAN4527/M'),
('1779','K/EMP/HAN4530/M'),
('1780','K/EMP/HAN4525/M'),
('1781','K/EMP/HAN4531/P'),
('1782','K/EMP/HAN4525/M'),
('1783','K/CONT/LAZER2070/P'),
('1784','K/CONT/LAZER2964/M'),
('1785','K/CONT/JUNIOR2076/M'),
('1786','K/CONT/JUNIOR2046/P'),
('1787','K/CONT/JUNIOR2060/M'),
('1788','K/CONT/JUNIOR2070/P'),
('1789','K/CONT/LAZER2044/P'),
('1790','K/EMP/KV05/P'),
('1791','K/CONT/LAZER2056/M'),
('1792','K/CONT/JUNIOR2058/M'),
('1793','K/CONT/LAZER2050/M'),
('1794','K/CONT/JUNIOR2080/M'),
('1795','K/CONT/JUNIOR 2080/M'),
('1796','K/EMP/MARV6300/M'),
('1797','K/EMP/MARV6300/M'),
('1798','K/EMP/MARV6305/M'),
('1799','K/EMP/MARV6302/M'),
('1800','K/EMP/MARV6301/M'),
('1801','K/EMP/MARV6303/M'),
('1802','K/NOU/OB06/M'),
('1803','K/NOU/OB01/M'),
('1804','K/NOU/OB04/M'),
('1805','K/NOR/BK006/M'),
('1806','K/NOR/BK015/M'),
('1807','K/NOU/TROUBLE/M'),
('1808','K/NOU/OB02/M'),
('1809','K/NOU/OB13/M'),
('1810','K/NOU/OB13/M'),
('1811','K/NOU/OB10/M'),
('1874','K/NOU/OD10/P'),
('1875','K/NOU/OD18/P'),
('1876','K/NOU/OD01/M'),
('1877','K/NOU/OD19/M'),
('1878','K/NOU/OO02/M'),
('1879','K/NOU/OO02/M'),
('1880','K/NOU/OO03/M');");

 $installer->run("DROP TABLE IF EXISTS dw_deleted_shipment");

 $installer->run("CREATE TABLE IF NOT EXISTS dw_deleted_shipment (
    shipment_id INT NOT NULL,
    deleted_date DATETIME ,
    PRIMARY KEY(shipment_id));");
    
 $installer->run("CREATE OR REPLACE VIEW vw_wearer_prescription AS
SELECT 	`id`, 
	`wearer_id`, 
	`method_id`, 
	CASE WHEN `date_last_prescription` = '0000-00-00 00:00:00' THEN NULL ELSE date_last_prescription END date_last_prescription, 
	CASE WHEN `date_start_prescription` = '0000-00-00 00:00:00' THEN NULL ELSE date_start_prescription END date_start_prescription, 
	`optician_name`, 
	`optician_address1`, 
	`optician_address2`, 
	`optician_city`, 
	`optician_state`, 
	`optician_country`, 
	`optician_postcode`, 
	`optician_phone`, 
	`optician_lookup_id`, 
	`reminder_sent`
	FROM 
	{$dbname}.wearer_prescription`;");
    
  $installer->run("CREATE OR REPLACE VIEW vw_wearer AS
SELECT id, 
	`name`,
	CASE WHEN DOB = '0000-00-00 00:00:00' THEN NULL ELSE DOB END DOB, 
	customer_id
	FROM {$dbname}.wearer;");
       
$installer->run("REPLACE INTO dw_bis_source_map VALUES 
('affiliates-af-za','Affiliates')");

    
$installer->endSetup();
	 
