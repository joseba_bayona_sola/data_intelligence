
-- buildProductPriceTable

	-- dw_product_prices

	-- dw_core_config_data

	-- dw_conf_presc_required

	-- dw_current_vat

	-- Description: 
		-- Write a PHP variable with SQL Code + Run Query (PHP Variable)
		-- SQL Code: CREATE - INSERT TABLES + WORK ON PRODUCT_PRICES (INSERT, UPDATE)

REPLACE INTO dw_current_vat (store_id) 
	SELECT store_id
    FROM {$this->dbname}.core_store WHERE store_id != 0;

UPDATE dw_current_vat cv, dw_core_config_data cd
SET cv.base_currency = cd.value
WHERE cd.path = 'currency/options/base' AND cv.store_id = cd.store_id;

UPDATE dw_current_vat cv, {$this->dbname}.directory_currency_rate cr 
SET cv.base_to_global_rate = cr.rate
WHERE cv.base_currency = cr.currency_from AND currency_to = 'GBP';

UPDATE dw_current_vat
set prof_fee_rate = 0.18;


INSERT INTO dw_product_prices(store_name, product_id,
	qty, pack_size, 
	local_unit_price_inc_vat, local_qty_price_inc_vat, local_pack_price_inc_vat,
	local_product_cost, local_total_cost, 
	store_id)
	
	SELECT cs.name, tp.entity_id,
		tp.qty, GREATEST(1, CAST(p.packsize AS UNSIGNED)),
		tp.value, tp.qty * tp.value, GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
		p.cost, p.cost * tp.qty, 
		cs.store_id
	FROM 
			{$this->dbname}.catalog_product_entity_tier_price tp
		INNER JOIN 
			{$this->dbname}.core_store cs1 ON cs1.website_id = tp.website_id
		CROSS JOIN 
			{$this->dbname}.core_store cs
		INNER JOIN 
			dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
		INNER JOIN 
			{$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
	WHERE tp.website_id = 0 AND cs.store_id != 0;

REPLACE INTO dw_product_prices(store_name, product_id,
	qty, pack_size,
	local_unit_price_inc_vat, local_qty_price_inc_vat, local_pack_price_inc_vat,
	local_product_cost, local_total_cost,
	global_product_cost, global_total_cost,
	store_id)

	SELECT cs.name, tp.entity_id,
		tp.qty, GREATEST(1, CAST(p.packsize AS UNSIGNED)),
		tp.value, tp.qty * tp.value, GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
		p.cost, p.cost * tp.qty,
		p.cost, p.cost * tp.qty,
		cs.store_id
	FROM 
			{$this->dbname}.catalog_product_entity_tier_price tp
		INNER JOIN 
			{$this->dbname}.core_store cs ON cs.website_id = tp.website_id
		INNER JOIN 
			dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
		INNER JOIN 
			{$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
	WHERE tp.website_id != 0;

UPDATE dw_product_prices p, dw_current_vat cv
SET
	p.local_prof_fee = local_qty_price_inc_vat * cv.prof_fee_rate,
	p.prof_fee_percent = cv.prof_fee_rate,
	p.store_currency_code = cv.base_currency,
	p.local_to_global_rate = cv.base_to_global_rate

	p.local_price_vat = (p.local_qty_price_inc_vat) - ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
	p.local_qty_price_exc_vat = ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
	p.local_product_cost = local_product_cost * (1 / local_to_global_rate)

	p.local_shipping_cost = cv.shipping_cost
WHERE cv.store_id = p.store_id;

UPDATE
  dw_product_prices p
SET
  p.global_unit_price_inc_vat = p.local_unit_price_inc_vat * p.local_to_global_rate,
  p.global_qty_price_inc_vat  = p.local_qty_price_inc_vat * p.local_to_global_rate,
  p.global_pack_price_inc_vat = p.local_pack_price_inc_vat * p.local_to_global_rate,
  p.global_prof_fee = p.local_prof_fee * p.local_to_global_rate,
  p.global_shipping_cost = p.local_shipping_cost * p.local_to_global_rate,
  p.global_margin_amount = p.local_margin_amount * p.local_to_global_rate,
  p.global_price_vat = p.local_price_vat * p.local_to_global_rate;

UPDATE
  dw_product_prices p
SET
  p.global_margin_percent = p.global_margin_amount / p.global_qty_price_inc_vat;