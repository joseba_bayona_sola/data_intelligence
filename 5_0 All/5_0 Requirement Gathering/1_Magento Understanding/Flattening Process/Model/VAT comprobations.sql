
select top 1000 line_id, order_id, store_name, order_no, document_type, document_date, product_id, product_type, is_lens, 
	local_prof_fee, local_line_total_inc_vat * 0.18 xx_prof_fee, local_line_total_inc_vat * 0.5 xx_prof_fee_ie, 
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_vat * 5 xx_line_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_vat * 5 xx_shipp_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_vat * 5 xx_disc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_vat * 5 xx_line_vat, 
	(local_line_total_inc_vat - local_prof_fee) * 0.20 vat, 
	(local_line_total_inc_vat - local_prof_fee) - ((local_line_total_inc_vat - local_prof_fee) / 1.2) vat_2
from DW_GetLenses.dbo.order_lines
where 
	product_id = 1356
	--order_id = 5085542
	and store_name = 'visiondirect.ie'
order by document_date desc

select top 1000 line_id, invoice_id, store_name, invoice_no, document_type, document_date, product_id, product_type, is_lens, 
	local_prof_fee, local_line_total_inc_vat * 0.18 xx_prof_fee,
	local_line_subtotal_inc_vat, local_line_subtotal_vat, local_line_subtotal_vat * 5 xx_line_vat, 
	local_shipping_inc_vat, local_shipping_vat, local_shipping_vat * 5 xx_shipp_vat, 
	local_discount_inc_vat, local_discount_vat, local_discount_vat * 5 xx_disc_vat, 
	local_line_total_inc_vat, local_line_total_vat, local_line_total_vat * 5 xx_line_vat, 
	(local_line_total_inc_vat - local_prof_fee) * 0.20 vat, 
	(local_line_total_inc_vat - local_prof_fee) - ((local_line_total_inc_vat - local_prof_fee) / 1.2) vat_2
from DW_GetLenses.dbo.invoice_lines
where 
	product_id = 2704
	-- order_id = 5085542
	and store_name = 'visiondirect.co.uk'
order by document_date desc

select top 1000 *
from DW_GetLenses.dbo.order_headers
where order_id in ('5093017', '5087836')


select top 1000 *
from DW_GetLenses.dbo.order_lines
where order_id in ('5093017')
order by order_id, document_type, line_id