
-- prepSync
-- Add records to config table to show that build procedure has begun will be updated when finished to show that build ran
	-- dw_sync_config

-- flagStdTables
-- Flags tables in magento entities (dw_proc attribute)

-- buildStdtables
-- Given a list of magento tables that represent data for separate magento entities 
-- Create or update a set of table in dw_flat database which will be used for further data processing.  
-- Data is updated incrementally based on which records have been updated since the last run

-- createSyncListTables
-- Create tables that flags customers that have a record that has changed since last build

-- buildVatTables
-- Create VAT Tables
	-- addMissingLineItems
	-- buildVatHeaderTable - updateVatValues for (order - invoice - shipment - creditmemo)

-- buildProductPriceTable() {

-- buildCalTables
-- Create Calendar Tables

-- buildExtraTables	

-- buildOrderMarketingTable


-- createStaticViews


-- createDebugSummary