
-- customer / eav
	-- customer_entity		
		--> dw_customers

-- customer_address / eav
	-- customer_address_entity
		--> dw_customer_addresses


-- catalog_category / eav
	-- catalog_category_entity
		--> dw_categories

-- catalog_product / eav
	-- catalog_product_entity
		--> dw_products



-- order / flat
	-- sales_flat_order
		--> dw_order_headers
-- order_item / items
	-- sales_flat_order_item
		--> dw_order_lines
-- order_address / items
	-- sales_flat_order_address
		--> dw_order_address

-- invoice / flat
	-- sales_flat_invoice
		--> dw_invoice_headers
-- invoice_item / items
	-- sales_flat_invoice_item
		--> dw_invoice_lines


-- shipment / flat
	-- sales_flat_shipment
		--> dw_shipment_headers
-- shipment_item / items
	-- sales_flat_shipment_item
		--> dw_shipment_lines


-- creditmemo / flat
	-- sales_flat_creditmemo
		--> dw_creditmemo_headers
-- creditmemo_item / items
	-- sales_flat_creditmemo_item
		--> dw_creditmemo_headers
