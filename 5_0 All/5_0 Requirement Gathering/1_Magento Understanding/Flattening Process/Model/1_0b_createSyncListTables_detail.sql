
-- insert into dw_updated_customers

	SELECT customer_id, 'customer'
	FROM dw_customers
	WHERE dw_proc = 0;

	SELECT customer_id, 'orders'
	FROM dw_order_headers
	WHERE dw_proc = 0;

	SELECT oh.customer_id, 'creditmemo'
	FROM 
			dw_creditmemo_headers ch 
		INNER JOIN 
			dw_order_headers oh ON oh.order_id = ch.order_id
	WHERE ch.dw_proc = 0;

	SELECT oh.customer_id, 'invoice'
	FROM 
			dw_invoice_headers ch 
		INNER JOIN 
			dw_order_headers oh ON oh.order_id = ch.order_id
	WHERE ch.dw_proc = 0;

	SELECT ch.customer_id, 'shipment'
	FROM 
			dw_shipment_headers ch 
		INNER JOIN 
			dw_order_headers oh ON oh.order_id = ch.order_id
	WHERE ch.dw_proc = 0;