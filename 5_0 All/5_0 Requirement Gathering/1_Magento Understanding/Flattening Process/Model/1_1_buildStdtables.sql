
-- buildStdtables

	-- Take Info about the Entity - Table looking to Entities Variable
	-- Set Entity - Table Type, Name, PK, ...
	-- Look for Entity - Table different attributes and attribute type + Prepare Update Sentence
	-- Run CREATE SQL sentence
	-- Run INSERT SQL sentence
	-- Run UPDATE SQL sentence
		-- (Prepare an array with UPDATE sentences)
		
	-- SPECIAL FOR ALL STORES (category - product): MIMIC STORE INHERITANCE
		-- Run DROP + CREATE + ALTER Table SQL sentence
		-- Run UPDATE SQL sentence
		
	-- Mark records

	-- SPECIAL	
		-- Remove DELETED SHIPMENTS
			-- Using dw_deleted_shipment
			-- Create missing_shipment_items
			-- WHY
		
		-- Fix EVERCLEAR ORDERS (empty invoices)
			-- Create dw_invoice_lines_everclear_empty, dw_invoice_confirm_empty_everclear, dw_invoice_lines_everclear_first_invoice, dw_invoice_lines_everclear_empty_fixed
			-- Update dw_invoice_lines - Delete dw_invoice_headers
			
			-- Using Invoice Header - Items data (ih.`grand_total` = 0 AND il.product_id = 2305)
			-- WHY 
			
		-- Fix STORE CREDIT ORDERS (store credit refunds not getting counted in shipment stream)
			-- include fully refunded to store credit orders in shipped stream
			-- Delete from dw_shipment_lines, dw_shipment_headers (9084400)
			-- Create dw_store_credit_refunds (select from dw_creditmemo_headers + customer_bal_total_refunded` > 0)
			-- Insert into dw_shipment_headers - dw_shipment_lines
		
			