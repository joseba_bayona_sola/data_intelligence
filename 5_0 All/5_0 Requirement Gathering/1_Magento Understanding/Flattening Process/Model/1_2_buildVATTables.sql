
-- VAT is calculated backwards from gross sales amount and then allocated to items by he amount they account for on order
-- VAT rate is decided based on store or the country goods are dispatched to
-- Note: Store Credit not included in VAT calculations - Shipping included (same rate as the rest of the order)

-- Basic step are
	-- Set country information for entity
	-- Calc all inc_vat totals for entity
	-- Flag Rows by VAT logic based on business rules
	-- Set VAT rate professional fee (VAT exempt amount of the order)
	-- Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate)
	-- Calculate Exc VAT and global values
	-- Allocate calculated values to entity items based on formula to spread values across items

	-- Shipments: Don't have any monetary values attached in magento. Calculated differently: using matching values from (order, invoice, creditmemo)
	-- Based each shipment item and the value of these to created a faux shipment header
	-- dummy lines addded for creditmemo adjust prove to be difficult here  they will not match a shipment item **/

	
	
-- buildVatTables
	-- addMissingLineItems
	-- buildVatHeaderTable, updateVatValues for ($item_entity, $entity, $entityId)
		-- ('order_headers', 'order_lines', 'order_id')
		-- ('invoice_headers', 'invoice_lines', 'invoice_id')
		-- ('creditmemo_headers', 'creditmemo_lines', 'creditmemo_id')
		-- ('shipment_headers', 'shipment_lines', 'shipment_id')

-- addMissingLineItems
	-- dw_{$item_entity} ONLY FOR CREDITMEMO (creditmemo_lines)
	SELECT
		{$entityId} + 1000000 AS item_id, {$entityId},
		(oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS base_row_total,
        discount_amount,
        (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS row_total,
        base_discount_amount ,
        
		0 AS qty, 0 AS base_cost,
		(oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS price ,
        (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS base_row_total_incl_tax ,
        (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS row_total_incl_tax ,
        0 AS product_id ,
        0 AS order_line_id ,
        'ADJUSTMENT' AS sku ,
        'ADJUSTMENT' AS name ,
        dw_att, dw_type, dw_synced, dw_synced_at, dw_proc
	FROM dw_{$entity} oh
    WHERE {$entityId} NOT IN (SELECT {$entityId} FROM dw_{$item_entity} WHERE {$entityId} IS NOT NULL)

-- buildVatHeaderTable
	-- {$entity}_vat
	-- {$item_entity}_vat

	-- CREATE TABLES statements

-- updateVatValues
	-- {$entity}_rank (INVOICE - SHIPMENT - CREDITMEMO): Rank Invoice - Shipment - Creditmemos that an Order can have (order by I-S-CR PK)

	
	
	-- 2 different ways: ORDER - INVOICE and SHIPMENT

	-- 1. ORDER - INVOICE - CREDITMEMO
		-- {$entity}_vat (INSERT + UPDATE Aggr Values from Lines + UPDATE incvat
		-- {$entity}_vat_rollup (INSERT + UPDATE)
		-- LOGIC to {$entity}_vat (Based on Store + Order Date)
		-- UPDATE {$entity}_vat (prof_fee, vat_rate values - based on logic, rollup_total)

		-- {$entity}_rank, {$entity}_vat

		-- {$entity}_vat (UPDATE vat values)
		-- {$entity}_vat (UPDATE excvat values)
		-- {$entity}_vat (UPDATE margin, cost values)
		-- {$entity}_vat (UPDATE global values)

		-- {$item_entity}_allocation_rate

	-- 2. SHIPMENT
		-- shipment_lines_vat
		-- shipment_headers_vat
		-- creditmemo_lines_with_shipment
		-- creditmemo_shipment_lines_vat
		-- creditmemo_shipment_headers_vat




