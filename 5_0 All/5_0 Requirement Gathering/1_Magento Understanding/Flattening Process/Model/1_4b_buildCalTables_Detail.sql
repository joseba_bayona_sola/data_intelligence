
-- dw_invoice_calender_dates_staging
-- dw_invoice_calender_dates
    SELECT ih.invoice_id, oh.order_id,
		ih.created_at AS created_at, oh.created_at AS order_created_at
    FROM
			dw_invoice_headers ih
        INNER JOIN 	
			dw_order_headers oh ON oh.order_id = ih.order_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id;

	UPDATE `dw_invoice_calender_dates_staging` 
	SET `length_of_time_to_invoice` =  DATEDIFF(created_at,order_created_at);

	SELECT * FROM dw_invoice_calender_dates_staging;

-- dw_shipment_calender_dates_staging
-- dw_shipment_calender_dates
	SELECT sh.shipment_id, oh.order_id,
		sh.created_at AS created_at, oh.created_at AS order_created_at, inv.invoiced_at AS invoiced_at
	FROM
			dw_shipment_headers sh
        INNER JOIN 	
			dw_order_headers oh ON oh.order_id = sh.order_id
        LEFT JOIN	
			first_inv inv ON inv.order_id = oh.order_id
        INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id;

	UPDATE `dw_shipment_calender_dates_staging` 
	SET `length_of_time_to_first_invoice` =  DATEDIFF(invoiced_at, order_created_at);
    UPDATE `dw_shipment_calender_dates_staging` 
	SET `length_of_time_from_first_invoice_to_this_shipment` = DATEDIFF(created_at, invoiced_at);

	SELECT * FROM dw_shipment_calender_dates_staging;

-- dw_qty_backordered
	SELECT 	sl.order_line_id, sl.item_id shipment_item_id, SUM(sl2.qty) qty_prior
    FROM 	
			dw_shipment_lines sl	
		INNER JOIN 
			dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
        INNER JOIN 
			dw_shipment_headers sh2 ON sh2.order_id = sh.order_id AND sh2.shipment_id < sh.shipment_id
        INNER JOIN 
			dw_shipment_lines sl2 ON sh2.shipment_id = sl2.shipment_id AND sl.order_line_id = sl2.order_line_id
    GROUP BY sl.order_line_id,sl.item_id;
