


-- dw_order_headers_marketing_staging
	-- Insert
	SELECT oh.order_id, oh.order_no, oh.customer_id, oh.store_id, oh.created_at, oh.STATUS, 'current_db'
	FROM	
			dw_order_headers oh
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id
	WHERE STATUS != 'archived';

	SELECT oh.order_id, oh.order_id, oh.customer_id, oh.store_id, oh.created_at, 'shipped', source
	FROM	
			dw_hist_order oh
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id;


-- order_rank_all
	-- Insert
	SELECT * 
	FROM 
		(SELECT ORDER_ID, ORDER_NO, 
			CUSTOMER_ID, oh.STORE_ID, 
			SOURCE,
			(CASE CUSTOMER_ID WHEN @curEventId THEN @curRow := @curRow + 1 ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END) AS rank
		FROM   
				dw_order_headers_marketing_staging oh   
			INNER JOIN 
				(SELECT @curRow := 0, @curEventId := '') r
			INNER JOIN 
				dw_core_config_data cd ON cd.store_id = oh.store_id 
		WHERE path = 'surveysweb/surveys/active' AND VALUE = 0
		ORDER BY CUSTOMER_ID, CREATED_AT) t;

-- order_rank_cancel
	-- Insert
    SELECT * 
	FROM 
		(SELECT ORDER_ID, ORDER_NO,
			CUSTOMER_ID, oh.STORE_ID,
            SOURCE,
			(CASE CUSTOMER_ID WHEN @curEventId THEN @curRow := @curRow + 1 ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END)AS rank
		FROM   
				dw_order_headers_marketing_staging oh   
			INNER JOIN 
				(SELECT @curRow := 0, @curEventId := '') r
            INNER JOIN 
				dw_core_config_data cd ON cd.store_id = oh.store_id
        WHERE path = 'surveysweb/surveys/active' AND VALUE = 0 AND STATUS NOT IN ('canceled','closed')
        ORDER BY CUSTOMER_ID,CREATED_AT) t;

-- order_rank_cancel_only
	-- Insert
    SELECT * 
	FROM 
		(SELECT ORDER_ID, ORDER_NO,
			CUSTOMER_ID, oh.STORE_ID,
            SOURCE,
            (CASE CUSTOMER_ID WHEN @curEventId THEN @curRow := @curRow + 1 ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END)AS rank
        FROM   
				dw_order_headers_marketing_staging oh   
			INNER JOIN 
				(SELECT @curRow := 0, @curEventId := '') r
            INNER JOIN 
				dw_core_config_data cd ON cd.store_id = oh.store_id
		WHERE path = 'surveysweb/surveys/active' AND VALUE = 0 AND STATUS IN ('canceled', 'closed')
        ORDER BY CUSTOMER_ID,CREATED_AT) t;

-- dw_order_headers_marketing_staging
	-- Update
	UPDATE dw_order_headers_marketing_staging om, order_rank_all orank
    SET om.ORDER_RANK_ALL = orank.rank
    WHERE orank.order_id =  om.order_id;
	UPDATE dw_order_headers_marketing_staging om, order_rank_cancel orank
    SET om.ORDER_RANK_CANCEL = orank.rank
    WHERE orank.order_id =  om.order_id;
    UPDATE dw_order_headers_marketing_staging om, order_rank_cancel_only orank
    SET om.ORDER_RANK_CANCEL_ONLY = orank.rank
    WHERE orank.order_id =  om.order_id;

	UPDATE dw_order_headers_marketing_staging om, dw_order_headers_marketing_staging om2
    SET om.PREV_STATUS = om2.status
    WHERE om.customer_id =  om2.customer_id AND om.order_rank_all  = om2.order_rank_all -1;

	UPDATE dw_order_headers_marketing_staging h1, dw_order_headers_marketing h2
    SET		
		h1.ORDER_LIFECYCLE_ALL = 
			CASE WHEN DATE_ADD(h1.created_at, INTERVAL -15 MONTH) >  h2.created_at THEN 'Reactivated' ELSE 'Regular' END
    WHERE h1.order_rank_all > 1 AND h1.customer_id = h2.customer_id AND h2.order_rank_all  = h1.order_rank_all -1;
	UPDATE dw_order_headers_marketing_staging h1, dw_order_headers_marketing h2
	SET		
		h1.ORDER_LIFECYCLE_CANCEL = 
			CASE WHEN DATE_ADD(h1.created_at, INTERVAL -15 MONTH) >  h2.created_at THEN 'Reactivated' ELSE 'Regular' END
	WHERE h1.order_rank_cancel > 1 AND h1.customer_id = h2.customer_id AND h2.order_rank_cancel = h1.order_rank_cancel-1;

	UPDATE dw_order_headers_marketing_staging
    SET	ORDER_LIFECYCLE_ALL = 'New'
    WHERE order_rank_all = 1;
	UPDATE dw_order_headers_marketing_staging
    SET	ORDER_LIFECYCLE_CANCEL = 'New'
    WHERE order_rank_cancel = 1;
	UPDATE dw_order_headers_marketing_staging
    SET	ORDER_LIFECYCLE_ALL = 'Regular' ,ORDER_LIFECYCLE_CANCEL = 'Regular'
    WHERE customer_id IS NULL;

	UPDATE dw_order_headers_marketing_staging oh, dw_core_config_data cd
    SET 
		ORDER_LIFECYCLE_ALL = 'Survey', ORDER_LIFECYCLE_CANCEL = 'Survey' ,
		ORDER_RANK_ALL =1, ORDER_RANK_CANCEL =1
	WHERE cd.store_id = oh.store_id AND path = 'surveysweb/surveys/active' AND `VALUE` = 1;


	UPDATE dw_order_headers_marketing_staging 
	SET ORDER_LIFECYCLE = IFNULL(ORDER_LIFECYCLE_CANCEL,ORDER_LIFECYCLE_ALL);
    UPDATE dw_order_headers_marketing_staging 
	SET ORDER_LIFECYCLE = 'Regular' WHERE ORDER_LIFECYCLE IS NULL;

	UPDATE dw_order_headers_marketing_staging 
	SET ORDER_RANK = CASE WHEN ORDER_RANK_CANCEL_ONLY IS NULL THEN IFNULL(ORDER_RANK_CANCEL,0) ELSE GREATEST(IFNULL(ORDER_RANK_ALL-ORDER_RANK_CANCEL_ONLY,0),1) END;

-- dw_order_headers_marketing_agg
	-- Insert 
	SELECT `customer_id` as customer_id, 
		MIN(`CREATED_AT`) first_order_date, MAX(`CREATED_AT`) last_order_date, MAX(`ORDER_RANK`) AS no_of_orders
	FROM dw_order_headers_marketing_staging
	WHERE customer_id IS NOT NULL
	GROUP BY `customer_id`;

-- dw_order_headers_marketing
	-- Insert
	SELECT * FROM dw_order_headers_marketing_staging;




-- dw_coupon_code_map
	-- Insert
	SELECT src.code, channel,
		CASE WHEN EXISTS(SELECT `value`	FROM `dw_core_config_data` WHERE path = 'referafriend/invite/voucher' AND CAST(`value` AS UNSIGNED) = src.coupon_id) THEN 1 ELSE 0 END is_raf
	FROM 
			{$this->dbname}.salesrule sr 
		INNER JOIN 
			{$this->dbname}.salesrule_coupon src ON sr.rule_id = src.rule_id
	WHERE channel != '' AND  channel  IS NOT NULL;

-- dw_bis_map_generic
	-- Insert 
	SELECT DISTINCT affilCode bis_source_code, NULL AS channel  
	FROM dw_order_headers
	WHERE affilCode IS NOT NULL AND affilCode != '';

	-- Update 
	UPDATE 
			dw_bis_map_generic mg 
		INNER JOIN 
			dw_bis_source_map mp ON mp.source_code = mg.bis_source_code
	SET mg.channel = mp.channel
	WHERE mp.channel != '' AND mp.channel IS NOT NULL;

-- dw_bis_map_dynamic_types
	-- Insert 
	INSERT INTO  dw_bis_map_dynamic_types VALUES('adword','Adwords'),('affiliate','Affiliates');

-- dw_bis_map_generic
	-- Update 
	UPDATE dw_bis_map_generic mg, dw_bis_map_dynamic_types dt
	SET mg.channel = dt.channel
	WHERE bis_source_code LIKE CONCAT(dt.prefix,'%') AND (mg.channel IS NULL OR mg.channel = '');

-- dw_order_channel_staging
	-- Insert
	SELECT oh.order_id, oh.order_no, 
		mg.channel bis_channel, cm.channel raf_channel, 
		ga.channel ga_channel, cm2.channel coupon_channel,
		COALESCE(mg.channel,cm.channel,ga.channel,cm2.channel,'Unknown') channel
	FROM 
		dw_order_headers oh
	INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = oh.customer_id
	LEFT JOIN 
		`dw_bis_map_generic`mg ON `bis_source_code`= affilcode
	LEFT JOIN 
		`dw_coupon_code_map`cm ON cm.`coupon_code`= oh.coupon_code AND cm.`is_raf` =1
	LEFT JOIN 
		{$this->dbname}.`ga_entity_transaction_data` ga ON ga.`transactionId`= oh.order_no
	LEFT JOIN 
		`dw_coupon_code_map`cm2 ON cm2.`coupon_code`= oh.coupon_code AND cm2.`is_raf` =0;

-- dw_order_channel
	-- Insert
	SELECT * FROM dw_order_channel_staging;



-- dw_full_creditmemo
	-- Insert
	SELECT 	oh.order_id, MAX(ch.creditmemo_id) creditmemo_id
	FROM 	
			dw_creditmemo_headers ch 
		INNER JOIN 
			dw_order_headers oh ON oh.order_id = ch.order_id
	WHERE oh.status = 'closed'
	GROUP BY ch.order_id;


-- dw_entity_header_rank
	-- Insert 
	SELECT	order_id, order_id, 'ORDER', created_at,
		t1.customer_id,
		store_id, 'current_db', 
		1
	FROM	
			dw_order_headers t1
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = t1.customer_id
	WHERE STATUS != 'archived'

	SELECT	order_id, order_id, 'CANCEL', created_at,
		t1.customer_id,
		store_id, 'current_db', 
		2
	FROM	
			dw_order_headers t1
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = t1.customer_id
	WHERE STATUS = 'canceled';

	SELECT	ch.order_id, ch.creditmemo_id, 'CREDITMEMO', ch.created_at,
		oh.customer_id, 
		oh.store_id, 'current_db',
		4
	FROM	
			dw_creditmemo_headers ch 
		INNER JOIN 
			dw_order_headers oh ON ch.order_id = oh.order_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id;

	SELECT	order_id, order_id, 'ORDER', created_at,
		t1.customer_id,
		store_id, source,
		1
	FROM	
			dw_hist_order t1
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = t1.customer_id;

-- dw_entity_header_rank_all
	-- Insert 
	SELECT t.* 
	FROM 
		(SELECT a.*,
			(CASE customer_id WHEN @curEventId THEN  @curRow := @curRow + 1 ELSE @curRow := 1 AND @curEventId := customer_id END) AS ORDER_RANK,
		'' NEXT_DOCUMENT_TYPE, '' PREV_DOCUMENT_TYPE,
		0 FULL_CREDIT
    FROM   
			dw_entity_header_rank a    
		INNER JOIN 
			(SELECT @curRow := 0, @curEventId := '') r
    ORDER BY customer_id,created_at,DOCUMENT_SEQ) t;

-- dw_entity_header_rank_seq
	-- Insert 
	SELECT t.* 
	FROM 
		(SELECT a.*, 
			(CASE customer_id WHEN @curEventId THEN 
				CASE WHEN (DOCUMENT_TYPE IN ('ORDER')) THEN @curRow := @curRow + 1 ELSE @curRow := @curRow + 0 END
				ELSE @curRow := 1  END) AS ORDER_SEQ_PLUS,
			(CASE customer_id WHEN @curEventId THEN
				CASE WHEN (PREV_DOCUMENT_TYPE IN ('CANCEL','CREDITMEMO')) AND (PREV_FULL_CREDIT = 1) THEN @curRow2 := @curRow2 - 1 ELSE @curRow2 := @curRow2 + 0 END
				ELSE @curRow2 := 1 AND @curEventId := customer_id END) - 1 AS ORDER_SEQ_MINUS,
			0  AS ORDER_SEQ
		FROM   
				dw_entity_header_rank_all a    
			INNER JOIN 
				(SELECT @curRow := 0, @curRow2 := 0, @curEventId := '') r
		ORDER BY customer_id,created_at,DOCUMENT_SEQ) t;



-- dw_customer_total_orders
	-- Insert 
	SELECT
		seq.customer_id,
		MAX(seq.ORDER_SEQ_PLUS) max_order_seq_plus, MIN(seq.ORDER_SEQ_MINUS) min_order_seq_minus,
		IFNULL((MAX(seq.ORDER_SEQ_PLUS) + MIN(seq.ORDER_SEQ_MINUS)),0) no_of_orders
	FROM 	
			dw_entity_header_rank_seq seq
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = seq.customer_id
	WHERE seq.customer_id IS NOT NULL
	GROUP BY seq.customer_id;